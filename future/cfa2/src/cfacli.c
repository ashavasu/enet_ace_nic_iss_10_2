/* SOURCE FILE HEADER :
 * $Id: cfacli.c,v 1.494.2.1 2018/03/15 12:59:20 siva Exp $
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : cfacli.c                                         |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Aricent Inc.                              |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : CLI                                              |
 * |                                                                           |
 * |  MODULE NAME           : CFA INTERFACE                                    |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for interface specific CLI       |
 * |                          commands. This module has functions to  create   |
 * |                          interfaces, modify interface parameters,         |
 * |                          bring up/down the interfaces, display statistics |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */
#ifndef __CFACLI_C__
#define __CFACLI_C__
#include "cfainc.h"
#include "ifmibcli.h"
#include "stdethcli.h"
#include "isspiinc.h"
#include "fscfacli.h"
#include "isscli.h"
#include "dcbxcli.h"
#ifdef IP_WANTED
#include "fsipcli.h"
#endif /* IP_WANTED */
#ifdef RMON_WANTED
extern UINT4        gRmonEnableStatusFlag;
#endif
#ifdef QOSX_WANTED
extern INT4         QoSShowRunningConfigInterfaceDetails (tCliHandle, INT4);
#endif
PRIVATE VOID        CfaShowBridgePortType
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
PRIVATE INT4        CfaShowILanConfig
PROTO ((tCliHandle CliHandle, UINT4 u4CurrIfIndex));
PRIVATE VOID        CfaCliPrintHex
PROTO ((tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE * pValue));

static INT1         ai1PktValue[CFA_MAX_PACKET_SIZE + 1];
#ifdef MPLS_WANTED
PRIVATE VOID CfaCliActOnLock PROTO ((tCliHandle, BOOL1, UINT4));
#endif

UINT4               gu4UfdFlag = CFA_FALSE;

    /* SUBTYPE changes */
tCfaIfaceInfo       ai1IfTypes[CFA_CLI_MAX_IFXTYPE][CFA_CLI_MAX_IFXSUBTYPE] = {
#ifdef CFA_UNIQUE_INTF_NAME
    {
     {CFA_GI_ENET, "ethernet", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
#else
    {
     {CFA_GI_ENET, "gigabitethernet", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
#endif
    {
     {CFA_FA_ENET, "fastethernet", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_XE_ENET, "extreme-ethernet", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_XL_ENET, "XL-ethernet", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_LVI_ENET, "LVI-ethernet", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_LAGG, "port-channel", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_PROP_VIRTUAL_INTERFACE, "sisp", "0"}
     ,
     {CFA_PROP_VIRTUAL_INTERFACE, "ac", "0"}
     }
    ,
    {
     {CFA_BRIDGED_INTERFACE, "virtual", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
#ifdef PBB_WANTED
    {
     {CFA_ILAN, "internal-lan", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
#endif
    {
     {CFA_PPP, "ppp", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_PSEUDO_WIRE, "pw", "0"}
     ,
     {0, "\0", "0"}
     }
#ifdef VXLAN_WANTED
    ,
    {
     {CFA_VXLAN_NVE, "nve", "0"}
     ,
     {0, "\0", "0"}
     }
#endif
#ifdef HDLC_WANTED
    ,
    {
     {CFA_HDLC, "serial", "0"}
     ,
     {0, "\0", "0"}
     }
#endif
    ,
    {
     {CFA_STATION_FACING_BRIDGE_PORT, "s-channel", "0"}
     ,
     {0, "\0", "0"}
     }

#ifdef VCPEMGR_WANTED
    ,
    {
     {CFA_TAP, "tap", "0"}
     ,
     {0, "\0", "0"}
     }
#endif

};

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_interface_cmd                          */
/*                                                                           */
/*     DESCRIPTION      : This function takes in variable no. of arguments   */
/*                        and process the commands for the CFA module as     */
/*                        defined in cfacmd.h                                */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Command -  Command Identifier                    */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
cli_process_interface_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *pau1PktTxPorts = NULL;
    UINT1              *pau1PktRxPorts = NULL;
    UINT4              *args[CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4Isid;
    UINT4               u4ContextId = 0;
    UINT4               u4ErrCode;
    INT4                i4Inst = 0;
    INT4                i4IfaceIndex = 0;
    UINT4               u4IfType = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4TempIfIndex = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4IpMask = 0;
    UINT1              *pu1IfaceInfo = NULL;
    tPortArray         *pPortArray = NULL;
    INT4                i4Return = CFA_FAILURE;
    UINT4               u4portchannel = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1SwName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1Tmp[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1IvrName[CFA_MAX_PORT_NAME_LENGTH +
                                   VCM_ALIAS_MAX_LEN + 1];
    INT4                i4Flow;
    INT4                i4RetStatus = 0;
    INT4                i4PrevIndex;
    INT4                i4IfaceType;
    INT4                i4BrgPortType;
    INT4                i4PortType;
    INT4                i4IfNum;
    UINT1               u1Flag = 1;
    UINT1              *pu1Alias = NULL;
    UINT1              *pu1Switch = NULL;
    UINT1              *pu1IpCxtName = NULL;
    UINT4               u4ConfigId;
    UINT4               u4TnlMode;
    tMacAddr            PortMacAddr;
    tTnlIpAddr          TnlSrcIpAddr;
    tTnlIpAddr          TnlDestIpAddr;
    tIpConfigInfo       IpInfo;
    UINT4               u4AddrType;
    UINT4               u4SrcIndex;
    UINT4               u4L2CxtId = 0;
    UINT1               u1BridgeStatus = 0;
    tPortList          *pMemberPorts = NULL;
    INT1                i1Operation = 0;
    UINT1               au1Desc[CFA_MAX_INTF_DESC_LEN];
    UINT4               u4IpSubnetMask = 0;
    UINT4               u4PktRxIndex = 0;
    UINT4               u4PktTxIndex = 0;
    UINT4               u4PktTxCount = 0;
    UINT4               u4PktTxInt = 0;
    INT1                ai1PktValuePrompt[] = "Enter Value: ";
    INT1                ai1PktMaskPrompt[] = "Enter Mask: ";
    UINT4               u4SecStatus = 0;
    UINT4               u4IfSubType = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4BcastAddr = 0;
    tCfaIfInfo         *pCfaIfInfo = NULL;
#ifdef PPP_WANTED
    UINT4               u4Temp1 = 0;
    UINT4               u4Temp2 = 0;
    INT4                i4IfIndex = 0;
    INT4                i4PppIfIndex = 0;
    UINT1               u1IfType = 0;
#endif /* PPP_WANTED */
    INT4                i4Args = 0;
    INT4                i4Val = 0;
    UINT4               u4IpIntfStats = 0;
#ifdef HDLC_WANTED
    UINT4               u4ControlIndex = 0;
#endif
    UINT4               u4LinkUpTimer = 0;
    UINT4               u4LinkUpStatus = 0;
    UINT1               u1BrgPortType = 0;

    MEMSET (au1Tmp, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1SwName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&(TnlDestIpAddr), 0, sizeof (tTnlIpAddr));
    MEMSET (&(TnlSrcIpAddr), 0, sizeof (tTnlIpAddr));
    MEMSET (PortMacAddr, 0, sizeof (tMacAddr));

    pu1Alias = &au1Tmp[0];
    pu1Switch = &au1SwName[0];
    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    i4Inst = va_arg (ap, INT4);
    /* Walk through the rest of the arguements and store in args array. 
     * Store four arguments at the max. This is because cfa commands do not
     * take more than four inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == CLI_CFA_MAX_ARGS)
            break;
    }
    va_end (ap);

    CliRegisterLock (CliHandle, CfaLock, CfaUnlock);

    CFA_LOCK ();

    switch (u4Command)
    {
        case CLI_CFA_INTERFACE_CREATE:
            if (CLI_PTR_TO_U4 (args[0]) == CFA_L3IPVLAN)
            {
                u4IfType = (UINT4) CFA_L3IPVLAN;
                u4IfIndex = *args[1];
                pu1IfaceInfo = (UINT1 *) cli_get_iface_type_name (u4IfType);
                SPRINTF ((CHR1 *) pu1Alias, "%s%u", pu1IfaceInfo, u4IfIndex);
                if ((UINT1 *) args[2] != NULL)
                {
                    if (VcmIsSwitchExist ((UINT1 *) args[2], &u4L2CxtId) ==
                        VCM_FALSE)
                    {
                        CliPrintf (CliHandle,
                                   "\r%%L2 context does not exist in the system\r\n");
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                }
                else
                {
                    u4L2CxtId = VCM_DEFAULT_CONTEXT;
                }
                /* Check if an interface is ICCL interface */
                if (u4IfIndex ==
                    (UINT4) CfaIcchApiGetIcclVlanId (CFA_ICCH_DEFAULT_INST))
                {
                    CliPrintf (CliHandle,
                               "\r%% Configurations are blocked on ICCL L3 IPVLAN \r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

            }
            else if (CLI_PTR_TO_U4 (args[0]) == CFA_LOOPBACK)
            {
                u4IfType = (UINT4) CFA_LOOPBACK;
                u4IfIndex = *args[1];
                pu1IfaceInfo = (UINT1 *) cli_get_iface_type_name (u4IfType);
                SPRINTF ((CHR1 *) pu1Alias, "%s%u", pu1IfaceInfo, u4IfIndex);
            }
#ifdef WGS_WANTED
            else if (CLI_PTR_TO_U4 (args[0]) == CFA_L2VLAN)
            {
                u4IfType = (UINT4) CFA_L2VLAN;
                pu1IfaceInfo = (UINT1 *) cli_get_iface_type_name (u4IfType);
                SPRINTF ((CHR1 *) pu1Alias, "%s", pu1IfaceInfo);
            }
#endif
            else if (CLI_PTR_TO_U4 (args[0]) == CFA_TUNNEL)
            {
                u4IfType = (UINT4) CFA_TUNNEL;
                u4IfIndex = *args[1];
                pu1IfaceInfo = (UINT1 *) cli_get_iface_type_name (u4IfType);
                SPRINTF ((CHR1 *) pu1Alias, "%s%u", pu1IfaceInfo, u4IfIndex);
            }
            else if (CLI_PTR_TO_U4 (args[0]) == CFA_LAGG)
            {
                u4IfType = (UINT4) CFA_LAGG;
                u4IfIndex = CLI_PTR_TO_U4 (args[1]);
                pu1IfaceInfo = (UINT1 *) cli_get_iface_type_name (u4IfType);
                SPRINTF ((CHR1 *) pu1Alias, "%s%u", pu1IfaceInfo, u4IfIndex);
            }
            else if (CLI_PTR_TO_U4 (args[0]) == CFA_BRIDGED_INTERFACE)
            {
                u4IfType = (UINT4) CFA_BRIDGED_INTERFACE;
                u4IfIndex = *args[1];
                pu1IfaceInfo = (UINT1 *) cli_get_iface_type_name (u4IfType);
                SPRINTF ((CHR1 *) pu1Alias, "%s%u", pu1IfaceInfo, u4IfIndex);
            }
            else if (CLI_PTR_TO_U4 (args[0]) == CFA_ENET)
            {
                u4IfType = CFA_ENET;
                CfaGetInterfaceNameFromIndex ((UINT4) i4Inst, pu1Alias);
            }
            else if (CLI_PTR_TO_U4 (args[0]) == CFA_L3SUB_INTF)
            {
                u4IfType = CFA_L3SUB_INTF;
                u4portchannel = CLI_PTR_TO_U4 (args[4]);
                if (u4portchannel == CFA_PORTCHANNEL)
                {
                    /* args[3] = Physical interface
                     * args[2] = Logical interface */
                    SPRINTF ((CHR1 *) pu1Alias, "po%d.%d",
                             CLI_PTR_TO_U4 (args[3]), CLI_PTR_TO_U4 (args[2]));
                }
                else
                {
                    /* args[5] = Physical interface 
                     * args[4] = Logical interface 
                     * args[7] = Slot Number */
                    SPRINTF ((CHR1 *) pu1Alias, "Slot%d/%d.%d",
                             CLI_PTR_TO_U4 (args[6]),
                             CLI_PTR_TO_U4 (args[5]), CLI_PTR_TO_U4 (args[4]));
                }
            }
            else if (CLI_PTR_TO_U4 (args[0]) == CFA_PROP_VIRTUAL_INTERFACE)
            {
                u4IfType = (UINT4) CFA_PROP_VIRTUAL_INTERFACE;
                /* SUBTYPE changes */
                if (CLI_PTR_TO_U4 (args[3]) == CFA_SUBTYPE_SISP_INTERFACE)
                {
                    u4IfSubType = CFA_SUBTYPE_SISP_INTERFACE;
                }
                else if (CLI_PTR_TO_U4 (args[3]) == CFA_SUBTYPE_AC_INTERFACE)
                {
                    u4IfSubType = CFA_SUBTYPE_AC_INTERFACE;
                }
                u4IfIndex = *args[1];
                pu1IfaceInfo = (UINT1 *) CfaUtilGetIfaceTypeName (u4IfType,
                                                                  u4IfSubType);
                SPRINTF ((CHR1 *) pu1Alias, "%s%u", pu1IfaceInfo, u4IfIndex);
            }
            else if (CLI_PTR_TO_U4 (args[0]) == CFA_PPP)
            {
                u4IfType = CFA_PPP;
                MEMCPY (&u4IfIndex, args[1], CFA_CLI_IPADDR_MAX_LENGTH);
                pu1IfaceInfo = (UINT1 *) cli_get_iface_type_name (u4IfType);
                SPRINTF ((CHR1 *) pu1Alias, "%s%u", pu1IfaceInfo, u4IfIndex);
            }
            else if (CLI_PTR_TO_U4 (args[0]) == CFA_PSEUDO_WIRE)
            {
                u4IfType = CFA_PSEUDO_WIRE;
                u4IfIndex = *args[1];
                pu1IfaceInfo = (UINT1 *) cli_get_iface_type_name (u4IfType);
                SPRINTF ((CHR1 *) pu1Alias, "%s%u", pu1IfaceInfo, u4IfIndex);
            }
#ifdef VXLAN_WANTED
            else if (CLI_PTR_TO_U4 (args[0]) == CFA_VXLAN_NVE)
            {
                u4IfType = CFA_VXLAN_NVE;
                u4IfIndex = *args[1];
                pu1IfaceInfo = (UINT1 *) cli_get_iface_type_name (u4IfType);
                SPRINTF ((CHR1 *) pu1Alias, "%s%u", pu1IfaceInfo, u4IfIndex);
            }
#endif
#ifdef HDLC_WANTED
            else if (CLI_PTR_TO_U4 (args[0]) == CFA_HDLC)
            {
                u4IfType = CFA_HDLC;
                u4IfIndex = *args[1];
                pu1IfaceInfo = (UINT1 *) cli_get_iface_type_name (u4IfType);
                u4ControlIndex = (UINT4) CLI_GET_IFINDEX ();
                SPRINTF ((CHR1 *) pu1Alias, "%s%u/%u", pu1IfaceInfo,
                         u4ControlIndex, u4IfIndex);
            }
#endif
#ifdef VCPEMGR_WANTED
            else if (CLI_PTR_TO_U4 (args[0]) == CFA_TAP)
            {
                u4IfType = CFA_TAP;
                u4IfIndex = *args[1];
                pu1IfaceInfo = (UINT1 *) cli_get_iface_type_name (u4IfType);
                SPRINTF ((CHR1 *) pu1Alias, "%s%u", pu1IfaceInfo, u4IfIndex);
            }
#endif
            else if (CLI_PTR_TO_U4 (args[0]) == CFA_STATION_FACING_BRIDGE_PORT)
            {
                u4IfType = CFA_STATION_FACING_BRIDGE_PORT;
                u4IfIndex = *args[1];
                pu1IfaceInfo = (UINT1 *) cli_get_iface_type_name (u4IfType);
                SPRINTF ((CHR1 *) pu1Alias, "%s%u", pu1IfaceInfo, u4IfIndex);
            }
            /* Check if an interface already exists with the given name */
            if (CfaGetInterfaceIndexFromNameInCxt (u4L2CxtId, pu1Alias,
                                                   (UINT4 *) &i4Inst)
                == OSIX_SUCCESS)
            {
                if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
                {
                    /* To prevent configuration for STACK VLAN */
                    if (u4IfIndex == CFA_DEFAULT_STACK_VLAN_ID)
                    {
                        CliPrintf (CliHandle,
                                   "\r%%This interface is reserved for Stacking\r\n");
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                }
                i4Return = CfaGetIfBridgedIfaceStatus
                    ((UINT4) i4Inst, &u1BridgeStatus);
                if ((u4IfType == CFA_ENET) ||
                    ((i4Return == CFA_SUCCESS) && (u4IfType == CFA_LAGG)
                     && (u1BridgeStatus != CFA_ENABLED)))
                {
                    if (CfaGetIfBridgedIfaceStatus
                        ((UINT4) i4Inst, &u1BridgeStatus) == CFA_SUCCESS)
                    {
                        if (CfaGetIfSubType
                            ((UINT4) i4Inst,
                             (UINT1 *) &u4IfSubType) == CFA_SUCCESS)
                        {
                            if (u4IfSubType == CFA_SUBTYPE_OPENFLOW_INTERFACE)
                            {
                                SPRINTF ((CHR1 *) au1IfName, "%s%d",
                                         CLI_INTOF_MODE, i4Inst);
                                CliChangePath ((CHR1 *) au1IfName);
                            }
                            else
                            {
                                if (u1BridgeStatus == CFA_ENABLED)
                                {
                                    SPRINTF ((CHR1 *) au1IfName, "%s%d",
                                             CLI_INTF_MODE, i4Inst);
                                    CliChangePath ((CHR1 *) au1IfName);
                                }
                                else
                                {
                                    SPRINTF ((CHR1 *) au1IfName, "%s%d",
                                             CLI_INTR_MODE, i4Inst);
                                    CliChangePath ((CHR1 *) au1IfName);
                                }
                            }
                        }
                        else
                        {
                            if (u1BridgeStatus == CFA_ENABLED)
                            {
                                SPRINTF ((CHR1 *) au1IfName, "%s%d",
                                         CLI_INTF_MODE, i4Inst);
                                CliChangePath ((CHR1 *) au1IfName);
                            }
                            else
                            {
                                SPRINTF ((CHR1 *) au1IfName, "%s%d",
                                         CLI_INTR_MODE, i4Inst);
                                CliChangePath ((CHR1 *) au1IfName);
                            }
                        }
                    }
                }
                if (u4IfType == CFA_STATION_FACING_BRIDGE_PORT)
                {
                    CliChangePath ((CHR1 *) pu1Alias);
                    i4RetStatus = CLI_SUCCESS;
                    break;

                }

                if ((u4IfType == CFA_L3IPVLAN)
#ifdef WGS_WANTED
                    || (u4IfType == CFA_L2VLAN)
#endif
                    )
                {
                    MEMSET (au1IvrName, 0,
                            CFA_MAX_PORT_NAME_LENGTH + VCM_ALIAS_MAX_LEN + 1);

                    CfaCliGetIVRNameFromIfNameAndCxtId (u4L2CxtId, pu1Alias,
                                                        au1IvrName);
                    CliChangePath ((CHR1 *) au1IvrName);
                }
                else if (u4IfType == CFA_L3SUB_INTF)
                {
                    /* Change the mode to iss(config-subif)# mode */
                    SPRINTF ((CHR1 *) au1IfName, "%s%d",
                             CLI_L3IFSUB_MODE, i4Inst);
                    CliChangePath ((CHR1 *) au1IfName);
                }
                else
                {
#ifdef HDLC_WANTED
                    if (u4IfType != CFA_HDLC)
#endif
                    {
                        CliChangePath ((CHR1 *) pu1Alias);
                    }
                }
                break;
            }
            /* Interface not exists. Create the new interface */

#ifdef LNXIP4_WANTED
            /*Check whether index is available. If index is not available do 
             * proceed */
            if (NetIpv4IsIndexAvailable (CFA_ONE) != CFA_SUCCESS)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
#endif
            if (u4IfType == CFA_BRIDGED_INTERFACE)
            {
                /* Internal ports PIP & CBP can be created with alias name 
                 * as virtualX. X should be in the range 1 to MAX_INTERNAL_IFACES.
                 * VIP Interfaces will be created with alias name as virtualY.
                 * Y will be in the range MIN_VIP_IFINDEX to MAX_VIP_IFINDEX i
                 *
                 * Whenever a new Internal interface (PIP or CBP) is tried to 
                 * create with alias name greater than SYS_MAX_INTERNAL_IFACEs,
                 * error will be thrown*/

                i4IfNum = ATOL (&(pu1Alias[7]));

                if (i4IfNum > SYS_DEF_MAX_INTERNAL_IFACES)
                {
                    CliPrintf (CliHandle, "\r%% Cannot create Internal port "
                               "with number greater than maximum internal "
                               "Interfaces \r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }

            if (u4IfType == CFA_PROP_VIRTUAL_INTERFACE)
            {
                if (CfaGetFreeIndexForSubType ((UINT4 *) &i4Inst, u4IfType,
                                               u4IfSubType) != OSIX_SUCCESS)
                {
                    if (u4IfSubType == CFA_SUBTYPE_SISP_INTERFACE)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Max number of SISP "
                                   " interfaces supported in the system is"
                                   " %d \r\n", SYS_DEF_MAX_SISP_IFACES);
                    }
                    if (u4IfSubType == CFA_SUBTYPE_AC_INTERFACE)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Max number of AC "
                                   " interfaces supported in the system is"
                                   " %d \r\n", SYS_DEF_MAX_L2_AC_IFACES);
                    }
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }

            else if (CfaGetFreeInterfaceIndex ((UINT4 *) &i4Inst, u4IfType)
                     != OSIX_SUCCESS)
            {
                if (u4IfType == CFA_ENET)
                {
                    CliPrintf (CliHandle,
                               "\r%% Cannot create Ethernet Interface \r\n");
                }
                else if (u4IfType == CFA_LAGG)
                {
                    CliPrintf (CliHandle,
                               "\r%% Maximum number of port channels"
                               " supported in the system is %d \r\n",
                               LA_DEV_MAX_TRUNK_GROUP);
                }
                else if (u4IfType == CFA_BRIDGED_INTERFACE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Maximum number of internal interfaces"
                               " supported in the system is %d \r\n",
                               SYS_DEF_MAX_INTERNAL_IFACES);
                }
                else if (u4IfType == CFA_PSEUDO_WIRE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Maximum Number of PseudoWire interfaces"
                               " Supported in the system is %d \r \n",
                               SYS_DEF_MAX_L2_PSW_IFACES);
                }
#ifdef VXLAN_WANTED
                else if (u4IfType == CFA_VXLAN_NVE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Maximum Number of NVE interfaces"
                               " Supported in the system is %d \r \n",
                               SYS_DEF_MAX_NVE_IFACES);
                }
#endif
#ifdef VCPEMGR_WANTED
                else if (u4IfType == CFA_TAP)
                {
                    CliPrintf (CliHandle,
                               "\r%% Maximum Number of TAP interfaces"
                               " Supported in the system is %d \r \n",
                               MAX_TAP_INTERFACES);
                }
#endif
                else if (u4IfType == CFA_L3SUB_INTF)
                {
                    CliPrintf (CliHandle,
                               "\r%%Maximum Number"
                               " of L3Subinterfaces Supported in the system"
                               " is %d \r \n", SYS_DEF_MAX_L3SUB_IFACES);
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r%% No free interfaces are available \r\n");
                }
                i4RetStatus = CLI_FAILURE;
                break;
            }

            u4IfIndex = (UINT4) i4Inst;
            i4RetStatus = CfaCreateInterface (CliHandle, u4IfIndex,
                                              u4IfType, pu1Alias, u4L2CxtId,
                                              u4IfSubType);
            if (i4RetStatus == CLI_FAILURE)
            {
                CFA_DS_LOCK ();

                pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

                if (NULL != pCfaIfInfo)
                {
                    CFA_CDB_SET_INTF_VALID (u4IfIndex, CFA_FALSE);

                    CFA_DS_UNLOCK ();

                    /* This means that the node exists and since we
                     * incurred a failure above, we will remove the node
                     * otherwise system will be in an unstable state */
                    CfaIfUtlIfDbRem (u4IfIndex, CFA_CDB_IFDB_REQ);
                }
                else
                {
                    CFA_DS_UNLOCK ();
                }
            }

            break;

        case CLI_CFA_LINUX_VLAN_INTERFACE_CREATE:
            if (CFA_OOB_MGMT_INTF_ROUTING == TRUE)
            {
                if (CLI_PTR_TO_U4 (args[0]) == CFA_L3IPVLAN)
                {
                    u4IfType = (UINT4) CFA_L3IPVLAN;
                    STRNCPY ((CHR1 *) pu1Alias, args[1], STRLEN (args[1]));
                    pu1Alias[STRLEN (args[1])] = '\0';

                    if (CfaIsValidLinuxVlanName (pu1Alias) == FALSE)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Invalid Linux Vlan Name \r\n");
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }

                    if (CfaGetInterfaceIndexFromName
                        (pu1Alias, (UINT4 *) &i4Inst) == OSIX_SUCCESS)
                    {
                        CliChangePath ((CHR1 *) pu1Alias);
                        break;
                    }
                    /* Check if interface already exists */

                    else if (CfaGetFreeInterfaceIndex
                             ((UINT4 *) &i4Inst, u4IfType) != OSIX_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% No free interfaces are available \r\n");
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }

                    u4IfIndex = (UINT4) i4Inst;
                    i4RetStatus = CfaCreateInterface
                        (CliHandle, u4IfIndex, u4IfType, pu1Alias, u4L2CxtId,
                         0);
                }
                else
                {
                    CliPrintf (CliHandle, "\r%% Invalid Interface Type \r\n");
                    i4RetStatus = CLI_FAILURE;
                }
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r%% No Support For Creating"
                           " Linux Vlan Interface \r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            break;

        case CLI_CFA_INTERFACE_DELETE:
            if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
            {
                MEMCPY (&u4IfIndex, args[0], sizeof (UINT4));

                /* Avoid deletion of default stack vlan */
                if (u4IfIndex == CFA_DEFAULT_STACK_VLAN_ID)
                {
                    CliPrintf (CliHandle,
                               "\r%%This interface is reserved for Stacking\r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;

                }
            }
            u4IfIndex = (UINT4) i4Inst;
            i4RetStatus = CfaDeleteInterface (CliHandle, u4IfIndex);
            break;

        case CLI_CFA_INTERFACE_ADMIN_STATUS:
#ifdef WGS_WANTED
            if (CFA_MGMT_IF_INDEX == i4Inst)
            {
                CliPrintf (CliHandle,
                           "\r%% Cannot change management interface status\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }
#endif /* WGS_WANTED */
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CfaSetInterfaceAdminStatus
                (CliHandle, u4IfIndex, CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_CFA_VIP_INTERFACE_ADMIN_STATUS:
            i4RetStatus = CfaSetInterfaceAdminStatus
                (CliHandle, CLI_PTR_TO_U4 (args[1]), CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_CFA_SET_MTU:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CfaSetInterfaceMtu (CliHandle, u4IfIndex, *args[0]);
            break;

        case CLI_CFA_SET_ALIAS:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus =
                CfaSetInterfaceAlias (CliHandle, u4IfIndex, (UINT1 *) args[0]);
            break;

        case CLI_CFA_LINK_STATUS_TRAP:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CfaSetLinkStatusTrap (CliHandle, u4IfIndex,
                                                CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_CFA_FLOWCONTROL:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4Flow = CLI_PTR_TO_I4 (args[0]);
            i4RetStatus = CfaSetFlowcontrol (CliHandle, u4IfIndex, i4Flow);
            break;
        case CLI_CFA_PORT_MODE:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            i4RetStatus = CfaSetPortMode (CliHandle, u4IfIndex, CFA_ENABLED);

            if (i4RetStatus != CLI_FAILURE)
            {
                CliChangePath ("..");

                SPRINTF ((CHR1 *) au1IfName, "%s%u", CLI_INTF_MODE, u4IfIndex);
                CliChangePath ((CHR1 *) au1IfName);

            }
            break;

        case CLI_CFA_NO_PORT_MODE:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            i4RetStatus = CfaSetPortMode (CliHandle, u4IfIndex, CFA_DISABLED);
            if (i4RetStatus != CLI_FAILURE)
            {
                CliChangePath ("..");

                SPRINTF ((CHR1 *) au1IfName, "%s%u", CLI_INTR_MODE, u4IfIndex);
                CliChangePath ((CHR1 *) au1IfName);
            }
            break;

        case CLI_CFA_SET_PORT_VID:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus =
                CfaCliSetPortVlanId (CliHandle, u4IfIndex, (INT4) *args[0]);
            break;

        case CLI_CFA_UNSET_PORT_VID:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CfaCliUnSetPortVlanId (CliHandle, u4IfIndex);
            break;

        case CLI_CFA_PORT_MODE_OF:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus =
                CfaSetOpenflowInterface (CliHandle, u4IfIndex,
                                         CFA_SUBTYPE_OPENFLOW_INTERFACE);
            if (i4RetStatus != CLI_FAILURE)
            {
                CliChangePath ("..");

                SPRINTF ((CHR1 *) au1IfName, "%s%u", CLI_INTOF_MODE, u4IfIndex);
                CliChangePath ((CHR1 *) au1IfName);
            }
            break;
        case CLI_CFA_NO_PORT_MODE_OF:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus =
                CfaSetOpenflowInterface (CliHandle, u4IfIndex,
                                         CFA_SUBTYPE_SISP_INTERFACE);
            if (i4RetStatus != CLI_FAILURE)
            {
                CliChangePath ("..");

                CfaGetIfBridgedIfaceStatus ((UINT4) u4IfIndex, &u1BridgeStatus);
                if (u1BridgeStatus == CFA_DISABLED)
                {
                    SPRINTF ((CHR1 *) au1IfName, "%s%u", CLI_INTR_MODE,
                             u4IfIndex);
                    CliChangePath ((CHR1 *) au1IfName);
                }
                else
                {
                    SPRINTF ((CHR1 *) au1IfName, "%s%u", CLI_INTF_MODE,
                             u4IfIndex);
                    CliChangePath ((CHR1 *) au1IfName);
                }
            }
            break;
        case CLI_CFA_BRG_PORT_TYPE:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4BrgPortType = CLI_PTR_TO_I4 (args[0]);
            i4RetStatus = CfaSetBrgPortType (CliHandle, u4IfIndex,
                                             i4BrgPortType);
            break;

        case CLI_CFA_PORT_TYPE:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4PortType = CLI_PTR_TO_I4 (args[0]);
            i4RetStatus = CfaSetBrgPortType (CliHandle, u4IfIndex, i4PortType);
            break;

        case CLI_CFA_CONNECT:
            pu1IfaceInfo =
                (UINT1 *) cli_get_iface_type_name (CFA_BRIDGED_INTERFACE);

            pMemberPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pMemberPorts == NULL)
            {
                CliPrintf (CliHandle, "\r%% Error in Allocating memory "
                           "for bitlist\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            MEMSET (pMemberPorts, 0, sizeof (tPortList));
            i4RetStatus =
                CfaCliGetIfList ((INT1 *) pu1IfaceInfo, (INT1 *) args[2],
                                 *pMemberPorts, BRG_PORT_LIST_SIZE);
            if (i4RetStatus == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Member" " Port(s) \r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                break;
            }
            if (CLI_PTR_TO_U4 (args[0]) == CFA_ADD_VIRTUAL)
            {
                i1Operation = CFA_ADD_VIRTUAL;
                u4IfIndex = *args[1];
            }
            else if (CLI_PTR_TO_U4 (args[0]) == CFA_DEL_VIRTUAL)
            {
                i1Operation = CFA_DEL_VIRTUAL;
                u4IfIndex = *args[1];
            }

            u4IfType = CFA_ILAN;
            pu1IfaceInfo = (UINT1 *) cli_get_iface_type_name (u4IfType);
            SPRINTF ((CHR1 *) pu1Alias, "%s%u", pu1IfaceInfo, u4IfIndex);

            /* Check if an interface does not exist with the given name 
             * Create it */

            if (CfaGetInterfaceIndexFromName (pu1Alias, (UINT4 *) &i4Inst)
                != OSIX_SUCCESS)
            {
                if (CfaGetFreeInterfaceIndex ((UINT4 *) &i4Inst, u4IfType)
                    != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r%% No free interfaces are available \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                u4IfIndex = (UINT4) i4Inst;
                i4RetStatus = CfaCreateInterface
                    (CliHandle, u4IfIndex, u4IfType, pu1Alias, u4L2CxtId, 0);
                if (i4RetStatus == CLI_FAILURE)
                {
                    /* Release bit list */
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);

                    CFA_DS_LOCK ();

                    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
                    if (NULL != pCfaIfInfo)
                    {
                        CFA_CDB_SET_INTF_VALID (u4IfIndex, CFA_FALSE);
                        CFA_DS_UNLOCK ();

                        /* This means that the node exists and since we
                         * incurred a failure above, we will remove the node
                         * otherwise system will be in an unstable state */
                        CfaIfUtlIfDbRem (u4IfIndex, CFA_CDB_IFDB_REQ);
                    }
                    else
                    {
                        CFA_DS_UNLOCK ();
                    }
                    break;
                }
            }
            u4IfIndex = (UINT4) i4Inst;
            i4RetStatus = CfaConnectPorts (CliHandle, u4IfIndex,
                                           i1Operation, *pMemberPorts);
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            break;

        case CLI_CFA_NO_CONNECT:
            u4IfIndex = (UINT4) i4Inst;
            i4RetStatus = CfaDeleteInterface (CliHandle, u4IfIndex);
            break;

        case CLI_CFA_TLV_CREATE:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = (INT4) CfaSetCustTLV (CliHandle, u4IfIndex,
                                                *(UINT4 *) args[0],
                                                *(UINT4 *) args[1],
                                                (UINT1 *) args[2],
                                                (UINT4) CFA_TLV_CREATE);
            break;
        case CLI_CFA_TLV_DELETE:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CfaSetCustTLV (CliHandle, u4IfIndex,
                                         *args[0], 0, NULL, CFA_TLV_DELETE);
            break;

        case CLI_CFA_SYS_SPECIFIC_PORTID:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = CfaCliSetSysSpecificPortID (CliHandle, i4IfaceIndex,
                                                      *args[0]);
            break;
        case CLI_CFA_OPQ_ATTRIB_CREATE:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = CfaCliSetOpqArribute (CliHandle, i4IfaceIndex,
                                                *(INT4 *) args[0], *args[1]);
            break;

        case CLI_CFA_OPQ_ATTRIB_DELETE:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = CfaCliDeleteOpqArribute (CliHandle, i4IfaceIndex,
                                                   *(INT4 *) args[0]);
            break;
        case CLI_CFA_CUSTOM_PARAM_DELETE:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = CfaCliDelAllCustomParams (CliHandle, i4IfaceIndex);
            break;

        case CLI_CFA_SET_PORT_MAC:

            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            StrToMac ((UINT1 *) args[0], PortMacAddr);

            i4RetStatus = CfaSetInterfaceExtMacAddr (CliHandle, u4IfIndex,
                                                     PortMacAddr);
            break;
        case CLI_CFA_DESC:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus =
                CfaSetInterfaceDesc (CliHandle, u4IfIndex, (UINT1 *) args[0]);
            break;
        case CLI_CFA_NO_DESC:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            MEMSET (au1Desc, 0, CFA_MAX_INTF_DESC_LEN);
            i4RetStatus =
                CfaSetInterfaceDesc (CliHandle, u4IfIndex, (UINT1 *) &au1Desc);
            break;
        case CLI_CFA_LINKUP_PORT_STATUS:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            u4LinkUpStatus = CLI_PTR_TO_U4 (args[0]);
            i4RetStatus =
                CfaLinkUpDelayStatusOnPort (CliHandle, u4IfIndex,
                                            u4LinkUpStatus);
            break;

        case CLI_CFA_SET_LINKUP:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            u4LinkUpTimer = CLI_PTR_TO_U4 (args[0]);
            i4RetStatus =
                CfaLinkUpDelaySetTimerOnPort (CliHandle, u4IfIndex,
                                              u4LinkUpTimer);

            break;

        case CLI_CFA_NO_SET_LINKUP:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus =
                CfaLinkUpDelaySetTimerOnPort (CliHandle, u4IfIndex,
                                              CFA_LINKUP_DELAY_DEFAULT_TIMER);
            break;

        case CLI_CFA_IVR_PRIMARY_IP_ADDR:
            i4Val = CLI_GET_VLANID ();
            if (i4Val == CLI_ERROR)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            u4IfIndex = (UINT4) i4Val;
            u4IpAddr = *(UINT4 *) (args[0]);
            u4IpMask = *(UINT4 *) (args[1]);
            pPortArray = (tPortArray *) MemAllocMemBlk (gCfaPortArrayPoolId);

            if (pPortArray == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% cfa : Memory Allocation failure\r\n");
                return CLI_FAILURE;
            }

            CLI_MEMSET (pPortArray, 0, sizeof (tPortArray));
            if (u4IpAddr == 0)
            {
                CliPrintf (CliHandle, "\r\n%% Invalid Ip Address\r\n");
                CFA_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                MemReleaseMemBlock (gCfaPortArrayPoolId, (UINT1 *) pPortArray);
                return CLI_FAILURE;
            }

            if (u4IpMask == 0)
            {
                CliPrintf (CliHandle, "\r\n%% Invalid Subnet Mask\r\n");
                CFA_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                MemReleaseMemBlock (gCfaPortArrayPoolId, (UINT1 *) pPortArray);
                return CLI_FAILURE;
            }
            i4RetStatus = CfaIvrSetIpAddress (CliHandle, u4IfIndex,
                                              u4IpAddr, u4IpMask);
            MemReleaseMemBlock (gCfaPortArrayPoolId, (UINT1 *) pPortArray);
            break;

        case CLI_CFA_IVR_SECONDARY_ADDR:
            i4Val = CLI_GET_VLANID ();
            if (i4Val == CLI_ERROR)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            u4IfIndex = (UINT4) i4Val;
            u4IpAddr = *(UINT4 *) (args[0]);
            u4IpMask = *(UINT4 *) (args[1]);
            if (u4IpAddr == 0)
            {
                CliPrintf (CliHandle,
                           "\r\n%% Invalid secondary Ip Address\r\n");
                CFA_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }
            if (u4IpMask == 0)
            {
                CliPrintf (CliHandle, "\r\n%% Invalid Subnet Mask\r\n");
                CFA_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }
            i4RetStatus = CfaIvrSetSecondaryIpAddress (CliHandle, u4IfIndex,
                                                       u4IpAddr, u4IpMask);
            break;

        case CLI_CFA_IVR_NO_IP_ADDR:
            i4Val = CLI_GET_VLANID ();
            if (i4Val == CLI_ERROR)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            u4IfIndex = (UINT4) i4Val;

            u4IpAddr = *(UINT4 *) (args[0]);
            i4RetStatus = CfaIvrDeleteIpAddress (CliHandle, u4IfIndex,
                                                 u4IpAddr);
            break;
        case CLI_CFA_IVR_UNNUM_IP:
            i4Val = CLI_GET_VLANID ();
            if (i4Val == CLI_ERROR)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            u4IfIndex = (UINT4) i4Val;

            if (args[0] != NULL)
            {
                StrToMac ((UINT1 *) args[0], PortMacAddr);
                i4RetStatus = CfaIvrSetPeerMacAddress (CliHandle, u4IfIndex,
                                                       PortMacAddr);
            }
            if (*(UINT4 *) (args[1]) != CFA_INVALID_INDEX)
            {
                i4RetStatus =
                    CfaUnnumIntSetAssociatedIPInterface (CliHandle, u4IfIndex,
                                                         *(UINT4 *) (args[1]));
            }

            break;
        case CLI_CFA_IVR_NO_UNNUM_IP:
            i4Val = CLI_GET_VLANID ();
            if (i4Val == CLI_ERROR)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            u4IfIndex = (UINT4) i4Val;
            if (args[0] != NULL)
            {
                StrToMac ((UINT1 *) args[0], PortMacAddr);
            }
            i4RetStatus =
                CfaUnnumIntResetAssociatedIPInterface (CliHandle, u4IfIndex,
                                                       PortMacAddr,
                                                       *(UINT4 *) (args[1]));
            break;

        case CLI_CFA_IVR_NO_IP_ADDR_SUBNET_MASK:
            i4Val = CLI_GET_VLANID ();
            if (i4Val == CLI_ERROR)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            u4IfIndex = (UINT4) i4Val;
            u4IpAddr = *(UINT4 *) (args[0]);
            u4IpSubnetMask = *(UINT4 *) (args[1]);
            i4RetStatus =
                CfaIvrDeleteIpAddressAndSubnetMask (CliHandle, u4IfIndex,
                                                    u4IpAddr, u4IpSubnetMask);
            break;

        case CLI_CFA_IVR_NO_PRIMARY_IP_ADDR_SUBNET_MASK:
            i4Val = CLI_GET_VLANID ();
            if (i4Val == CLI_ERROR)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            u4IfIndex = (UINT4) i4Val;
            u4TempIfIndex = u4IfIndex;
            u4IpAddr = *(UINT4 *) (args[0]);
            u4IpSubnetMask = *(UINT4 *) (args[1]);
            if (CfaIpIfGetIpAddressInfo (u4IpAddr, &u4NetMask,
                                         &u4BcastAddr,
                                         &u4IfIndex) == CFA_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Cannot Delete: IP address not assigned or already deleted\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            if (u4IfIndex != u4TempIfIndex)
            {
                CliPrintf (CliHandle,
                           "\r%% Cannot Delete: IP address not assigned for this Interface\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            /* If Entered Subnet mask Does not match with actual Mask , exit */
            if (u4NetMask != u4IpSubnetMask)
            {
                CliPrintf (CliHandle,
                           "\r%% Invalid Subnet mask for given IP\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            i4RetStatus = CfaIvrDeleteIpAddress (CliHandle, u4IfIndex,
                                                 u4IpAddr);
            break;

        case CLI_CFA_IVR_IP_DHCP:

            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            if (CfaValidateIfIndex (u4IfIndex) == CFA_SUCCESS)
            {
                i4RetStatus =
                    CfaIvrSetAddressAlloc (CliHandle, u4IfIndex,
                                           CFA_IP_ALLOC_POOL,
                                           (UINT1) (CLI_PTR_TO_U4 (args[0])));
            }
            else
            {
                i4RetStatus = CLI_FAILURE;
                CliPrintf (CliHandle, "\r\n%% Invalid Interface Index\r\n");
            }
            break;
        case CLI_CFA_IVR_NO_IP_DHCP:

            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            if (CfaValidateIfIndex (u4IfIndex) == CFA_SUCCESS)
            {
                i4RetStatus =
                    CfaIvrSetAddressAlloc (CliHandle, u4IfIndex,
                                           CFA_IP_ALLOC_MAN, CFA_PROTO_NONE);

            }
            else
            {
                i4RetStatus = CLI_FAILURE;
                CliPrintf (CliHandle, "\r\n%% Invalid Interface Index\r\n");
            }
            break;

        case CLI_CFA_MGMT_VLAN:
#ifdef WGS_WANTED
            pPortArray = (tPortArray *) MemAllocMemBlk (gCfaPortArrayPoolId);

            if (pPortArray == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% cfa : Memory Allocation failure\r\n");
                return CLI_FAILURE;
            }

            CLI_MEMSET (pPortArray, 0, CFA_MGMT_VLAN_LIST_SIZE);

            if (CfaIvrConvertStrToVlanList
                ((UINT1 *) args[0], (UINT1 *) pPortArray) == CFA_FAILURE)
            {
                i4RetStatus = CLI_FAILURE;
            }
            else
            {
                i4RetStatus =
                    CfaSetMgmtVlanList (CliHandle, (UINT1 *) pPortArray,
                                        CLI_ENABLE);
            }
            MemReleaseMemBlock (gCfaPortArrayPoolId, (UINT1 *) pPortArray);

#endif
            break;

        case CLI_CFA_NO_MGMT_VLAN:
#ifdef WGS_WANTED
            pPortArray = (tPortArray *) MemAllocMemBlk (gCfaPortArrayPoolId);

            if (pPortArray == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% cfa : Memory Allocation failure\r\n");
                return CLI_FAILURE;
            }

            CLI_MEMSET (pPortArray, 0, CFA_MGMT_VLAN_LIST_SIZE);

            if (CfaIvrConvertStrToVlanList
                ((UINT1 *) args[0], (UINT1 *) pPortArray) == CFA_FAILURE)
            {
                i4RetStatus = CLI_FAILURE;
            }
            else
            {
                i4RetStatus = CfaSetMgmtVlanList
                    (CliHandle, (UINT1 *) pPortArray, CLI_DISABLE);
            }
            MemReleaseMemBlock (gCfaPortArrayPoolId, (UINT1 *) pPortArray);
#endif
            break;
        case CLI_CFA_SEC_VLAN_LIST:
            pPortArray = (tPortArray *) MemAllocMemBlk (gCfaPortArrayPoolId);

            if (pPortArray == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% cfa : Memory Allocation failure\r\n");
                return CLI_FAILURE;
            }

            CLI_MEMSET (pPortArray, 0, sizeof (tPortArray));

            if (CfaIvrConvertStrToVlanList
                ((UINT1 *) args[0], (UINT1 *) pPortArray) == CFA_FAILURE)
            {
                i4RetStatus = CLI_FAILURE;
            }
            else
            {
                i4RetStatus =
                    CfaSetSecVlanList (CliHandle, (UINT1 *) pPortArray,
                                       CLI_ENABLE);

            }
            MemReleaseMemBlock (gCfaPortArrayPoolId, (UINT1 *) pPortArray);

            break;

        case CLI_CFA_NO_SEC_VLAN_LIST:
            pPortArray = (tPortArray *) MemAllocMemBlk (gCfaPortArrayPoolId);

            if (pPortArray == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% cfa : Memory Allocation failure\r\n");
                return CLI_FAILURE;
            }

            CLI_MEMSET (pPortArray, 0, CFA_SEC_VLAN_LIST_SIZE);

            if (CfaIvrConvertStrToVlanList
                ((UINT1 *) args[0], (UINT1 *) pPortArray) == CFA_FAILURE)
            {
                i4RetStatus = CLI_FAILURE;
            }
            else
            {
                i4RetStatus =
                    CfaSetSecVlanList (CliHandle, (UINT1 *) pPortArray,
                                       CLI_DISABLE);

            }
            MemReleaseMemBlock (gCfaPortArrayPoolId, (UINT1 *) pPortArray);

            break;

        case CLI_CFA_SET_SEC_INTF_VLAN:
            u4IfIndex = *args[0];
            i4RetStatus = CfaSetSecIntfVlan (CliHandle, u4IfIndex);
            break;
        case CLI_CFA_RESET_SEC_INTF_VLAN:
            u4IfIndex = *args[0];
            i4RetStatus = CfaReSetSecIntfVlan (CliHandle);
            break;
        case CLI_CFA_SEC_BRIDGE_STATUS:
            u4SecStatus = CLI_PTR_TO_U4 (args[0]);
            i4RetStatus =
                (INT4) CfaSetSecurityBridgeMode (CliHandle, (INT4) u4SecStatus);
            break;

#ifdef PPP_WANTED
        case CLI_CFA_STACK_PPP:
            i4PppIfIndex = CLI_PTR_TO_I4 (args[0]);
            i4IfIndex = CLI_PTR_TO_U4 (args[1]);

            if (CfaGetIfType (i4IfIndex, &u1IfType) == CFA_FAILURE)
            {
                CliPrintf (CliHandle, "Invalid lower layer interface\r\n");
                i4RetStatus = CLI_FAILURE;
            }

            i4RetStatus = CfaCliStackPpp (CliHandle, i4PppIfIndex,
                                          i4IfIndex, u1IfType);
            break;

        case CLI_CFA_UNSTACK_PPP:
            i4PppIfIndex = CLI_PTR_TO_I4 (args[0]);
            i4RetStatus = CfaCliUnStackPpp (CliHandle, i4PppIfIndex);
            break;

        case CLI_CFA_PPP_IP_ADDR:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            if (CfaValidateIfIndex (u4IfIndex) == CFA_SUCCESS)
            {
                MEMCPY (&u4Temp1, args[0], CFA_CLI_IPADDR_MAX_LENGTH);
                MEMCPY (&u4Temp2, args[1], CFA_CLI_IPADDR_MAX_LENGTH);
                i4RetStatus = CfaCliSetPppIpAddr (CliHandle, u4IfIndex,
                                                  u4Temp1, u4Temp2);
            }
            else
            {
                i4RetStatus = CLI_FAILURE;
                CliPrintf (CliHandle,
                           "\r\n%% Attach the PPP interface to an underlying"
                           " physical interface first\r\n");
            }
            break;

        case CLI_CFA_PPP_IP_ADDR_NEGO:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            if (CfaValidateIfIndex (u4IfIndex) == CFA_SUCCESS)
            {
                i4RetStatus = CfaPppSetAddressAlloc (CliHandle, u4IfIndex,
                                                     CFA_IP_ALLOC_NEGO);
            }
            else
            {
                i4RetStatus = CLI_FAILURE;
                CliPrintf (CliHandle,
                           "\r\n%% Attach the PPP interface to an underlying"
                           " physical interface first\r\n");
            }
            break;
        case CLI_CFA_PPP_NO_IP_ADDR:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            if (CfaValidateIfIndex (u4IfIndex) == CFA_SUCCESS)
            {
                i4RetStatus = CfaCliSetPppNoIpAddr (CliHandle, u4IfIndex);
            }
            else
            {
                i4RetStatus = CLI_FAILURE;
                CliPrintf (CliHandle,
                           "\r\n%% Attach the PPP interface to an underlying"
                           " physical interface first\r\n");
            }
            break;

        case CLI_CFA_PPP_UNNUM_IP:

            u4IfIndex = (UINT4) CLI_GET_VLANID ();
            if (*(UINT4 *) (args[1]) != CFA_INVALID_INDEX)
            {
                i4RetStatus =
                    CfaPppUnnumIntSetAssociatedIPInterface (CliHandle,
                                                            u4IfIndex,
                                                            *(UINT4
                                                              *) (args[1]));
            }
            break;
        case CLI_CFA_PPP_NO_UNNUM_IP:
            u4IfIndex = (UINT4) CLI_GET_VLANID ();
            i4RetStatus =
                CfaPppUnnumIntResetAssociatedIPInterface (CliHandle, u4IfIndex,
                                                          *(UINT4 *) (args[1]));
            break;
#endif /* PPP_WANTED */

        case CLI_CFA_WAN_TYPE:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus =
                CfaCliSetWanType (CliHandle, u4IfIndex,
                                  CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_CFA_SHOW_MTU:
            pu1Alias = (UINT1 *) args[0];
            i4RetStatus = CfaShowInterfaceMtu (CliHandle, i4Inst, pu1Alias);
            break;

        case CLI_CFA_SHOW_BRG_PORT_TYPE:
            i4RetStatus = CfaShowInterfaceBrgPortType (CliHandle, i4Inst);
            break;

        case CLI_CFA_SHOW_CONNECT:
            pu1IfaceInfo = (UINT1 *) cli_get_iface_type_name (CFA_ILAN);

            pMemberPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pMemberPorts == NULL)
            {
                CliPrintf (CliHandle, "\r%% Error in Allocating memory "
                           "for bitlist\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            MEMSET (pMemberPorts, 0, sizeof (tPortList));
            i4RetStatus =
                CfaCliGetIfList ((INT1 *) pu1IfaceInfo, (INT1 *) args[0],
                                 *pMemberPorts, BRG_PORT_LIST_SIZE);
            if (i4RetStatus == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Member" " Port(s) \r\n");
                FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                break;
            }

            i4RetStatus = CfaShowConnectParam (CliHandle, *pMemberPorts);
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            break;

        case CLI_CFA_SHOW_INT_ALL:

            pu1Alias = (UINT1 *) args[1];
            i4RetStatus =
                CfaShowInterfaces (CliHandle, i4Inst, CLI_PTR_TO_U4 (args[0]),
                                   pu1Alias);
            break;

        case CLI_CFA_SHOW_INT_COUNTERS:
            pu1Alias = (UINT1 *) args[0];

            i4RetStatus =
                CfaShowInterfacesCounters (CliHandle, i4Inst, pu1Alias);
            break;

        case CLI_CFA_SHOW_L3VLAN_INT_COUNTERS:
            pu1Alias = (UINT1 *) args[0];

            i4RetStatus =
                CfaShowL3VlanInterfacesCounters (CliHandle, i4Inst, pu1Alias);
            break;

        case CLI_CFA_CLEAR_INT_COUNTERS:

            i4RetStatus = CfaClearInterfacesCounters (CliHandle, i4Inst);
            break;
        case CLI_CFA_SHOW_INT_HC_COUNTERS:
            i4RetStatus = CfaShowInterfacesHcCounters (CliHandle, i4Inst);
            break;

        case CLI_CFA_SHOW_CUSTOM_PARAM:

            i4RetStatus = CfaShowCustomParam (CliHandle);
            break;

        case CLI_CFA_SHOW_SYS_SPECIFIC_PORTID:

            i4RetStatus = CfaShowSysSpecificPortID (CliHandle);
            break;

        case CLI_CFA_SHOW_MGMT_VLAN:
            i4RetStatus = CfaShowMgmtVlanList (CliHandle);
            break;

        case CLI_CFA_SHOW_SEC_VLAN_LIST:
            i4RetStatus = CfaShowSecVlanList (CliHandle);
            break;

        case CLI_CFA_SHOW_SEC_INTF_VLAN:
            CfaShowSecIntfVlan (CliHandle);
            break;

        case CLI_CFA_SHOW_IP_INT:
            pu1IpCxtName = (UINT1 *) args[0];
            pu1Alias = (UINT1 *) args[1];

            if (pu1IpCxtName == NULL)
            {
                u4ContextId = VCM_INVALID_VC;
            }
            else
            {
                if (VcmIsVrfExist (pu1IpCxtName, &u4ContextId) == VCM_FALSE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid VRF name\r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

            }
            i4RetStatus = CfaShowIpInt (CliHandle, (UINT4) i4Inst, u4ContextId,
                                        pu1Alias);
            break;

        case CLI_CFA_SHOW_FLOWCONTROL:

            CliPrintf (CliHandle,
                       "\r\n%-8s%-9s%-9s%-11s%-11s%-15s%-11s\r\n",
                       "Port", "Admin", "Oper",
                       "Tx Pause", "Rx Pause", "HC TxPause", "HC RxPause");

            CliPrintf (CliHandle, "%-8s%-9s%-9s\r\n", "", "Tx   Rx", "Tx   Rx");

            CliPrintf (CliHandle, "%-8s%-9s%-9s%-11s%-11s%-15s%-11s\r\n",
                       "----", "-------", "-------",
                       "--------", "--------", "----------", "----------");

            if (i4Inst == 0)
            {
                if (nmhGetFirstIndexIfTable (&i4Inst) == SNMP_FAILURE)
                {
                    i4RetStatus = CLI_SUCCESS;
                    break;
                }

                u1Flag = 0;
            }
            else
            {
                if (nmhValidateIndexInstanceIfTable (i4Inst) == SNMP_FAILURE)
                {
                    i4RetStatus = CLI_SUCCESS;
                    break;
                }
            }

            do
            {
                nmhGetIfType (i4Inst, &i4IfaceType);
                if ((i4IfaceType != CFA_ENET) && (i4IfaceType != CFA_LAGG))
                {
                    break;
                }
                i4RetStatus = CfaShowInterfacesFlow (CliHandle, i4Inst);

                if (i4RetStatus == CLI_FAILURE)
                {
                    break;
                }
                i4PrevIndex = i4Inst;
            }
            while ((u1Flag == 0) &&
                   (nmhGetNextIndexIfTable (i4PrevIndex, &i4Inst) ==
                    SNMP_SUCCESS));

            break;

        case CFA_CONF_TNL:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            u4TnlMode = *(UINT4 *) args[2];
            u4AddrType = *(UINT4 *) args[3];

            if (args[4] != NULL)
            {
                u4ConfigId = *(UINT4 *) args[4];
            }
            else
            {
                u4ConfigId = 1;
            }

            if (u4AddrType == ADDR_TYPE_IPV4)
            {
                TnlSrcIpAddr.Ip4TnlAddr = *args[0];
                if (args[1] != NULL)
                {
                    TnlDestIpAddr.Ip4TnlAddr = *args[1];
                }
            }
            /* check if the given addr is IPv6 */
            else if (u4AddrType == ADDR_TYPE_IPV6)
            {
                INET_ATON6 (args[0], TnlSrcIpAddr.Ip6TnlAddr);
                if (args[1] != NULL)
                {
                    INET_ATON6 (args[1], TnlDestIpAddr.Ip6TnlAddr);
                }
            }
            else                /* check if it is IfName/IfIndex */
            {
                /*  u4SrcIndex = cli_get_ifc_index (args[0]); */

                if (CfaGetInterfaceIndexFromName ((UINT1 *) args[0],
                                                  &u4SrcIndex) == OSIX_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Not a valid Interface name \r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

                if ((CfaIpIfGetIfInfo (u4SrcIndex, &IpInfo) == CFA_FAILURE) ||
                    (IpInfo.u4Addr == 0))
                {
                    CliPrintf (CliHandle,
                               "\r%% Pls. specify the interface with valid IP address \r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                u4AddrType = ADDR_TYPE_IPV4;
                TnlSrcIpAddr.Ip4TnlAddr = IpInfo.u4Addr;
                if (args[1] != NULL)
                {
                    /* args[1] is NULL when remote addr is not configured */
                    TnlDestIpAddr.Ip4TnlAddr = *args[1];
                }
            }

            i4RetStatus =
                CfaConfTnlParams (CliHandle, u4IfIndex, u4AddrType,
                                  &TnlSrcIpAddr, &TnlDestIpAddr, u4TnlMode,
                                  u4ConfigId);
            break;

        case CFA_DEL_TNL:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            u4TnlMode = *(UINT4 *) args[2];
            u4AddrType = *(UINT4 *) args[3];
            if (args[4] != NULL)
            {
                u4ConfigId = *(UINT4 *) args[4];
            }
            else
            {
                u4ConfigId = 1;
            }

            if (u4AddrType == ADDR_TYPE_IPV4)
            {
                TnlSrcIpAddr.Ip4TnlAddr = *args[0];
                if (args[1] != NULL)
                {
                    TnlDestIpAddr.Ip4TnlAddr = *args[1];
                }
            }
            /* check if the given addr is IPv6 */
            else if (u4AddrType == ADDR_TYPE_IPV6)
            {
                INET_ATON6 (args[0], TnlSrcIpAddr.Ip6TnlAddr);
                if (args[1] != NULL)
                {
                    INET_ATON6 (args[1], TnlDestIpAddr.Ip6TnlAddr);
                }
            }
            else                /* check if it is IfName/IfIndex */
            {
                if (CfaGetInterfaceIndexFromName ((UINT1 *) args[0],
                                                  &u4SrcIndex) == OSIX_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Not a valid Interface name \r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

                if ((CfaIpIfGetIfInfo (u4SrcIndex, &IpInfo) == CFA_FAILURE) ||
                    (IpInfo.u4Addr == 0))
                {
                    CliPrintf (CliHandle,
                               "\r%% Pls. specify the interface with valid IP address \r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                u4AddrType = ADDR_TYPE_IPV4;
                TnlSrcIpAddr.Ip4TnlAddr = IpInfo.u4Addr;
                if (args[1] != NULL)
                {
                    /* args[1] is NULL when remote addr is not configured */
                    TnlDestIpAddr.Ip4TnlAddr = *args[1];
                }
            }

            i4RetStatus = CfaDelTnlParams (CliHandle, u4IfIndex, u4AddrType,
                                           &TnlSrcIpAddr, &TnlDestIpAddr,
                                           u4TnlMode, u4ConfigId);
            break;

        case CFA_ENA_TNL_CKSUM:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus =
                CfaSetTnlCkSumFlag (CliHandle, u4IfIndex, CFA_ENABLED);
            break;
        case CFA_NO_TNL_CKSUM:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus =
                CfaSetTnlCkSumFlag (CliHandle, u4IfIndex, CFA_DISABLED);
            break;
        case CFA_ENA_TNL_PMTU:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus =
                CfaSetTnlPmtuFlag (CliHandle, u4IfIndex, (INT4) *args[0],
                                   CFA_ENABLED);
            break;
        case CFA_TNL_HOP_LIMIT:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus =
                CfaSetTnlHopLimit (CliHandle, u4IfIndex, (INT4) *args[0]);
            break;
        case CFA_NO_TNL_PMTU:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus =
                CfaSetTnlPmtuFlag (CliHandle, u4IfIndex, 0, CFA_DISABLED);
            break;
        case CFA_SET_TNL_DIR:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus =
                CfaSetTnlDir (CliHandle, u4IfIndex, (UINT1) *args[0],
                              (UINT1) *args[1]);
            break;

        case CFA_OOB_CONFIG:
            if (CFA_MGMT_PORT == TRUE)
            {
                CliChangePath (CLI_OOB_CONFIG);
            }
            else
            {
                CliPrintf (CliHandle, "\r\n%%No OOB Port Exists\r\n");
            }
            break;

        case CLI_CFA_OOB_IP_ADDR:
            u4IpAddr = *(UINT4 *) (args[0]);
            u4IpMask = *(UINT4 *) (args[1]);
            i4RetStatus = CfaIvrSetIpAddress (CliHandle, CFA_OOB_MGMT_IFINDEX,
                                              u4IpAddr, u4IpMask);
            break;

        case CLI_CFA_OOB_SEC_IP_ADDR:
        case CLI_CFA_OOB_NO_SEC_IP_ADDR:
            /* args[0] contains ipaddress; args[1] contains ipmask;
             * args[2] denotes to which node ip to be set i.e.,node0 or node1 */
            if ((args[0] != NULL) && (args[1] != NULL))
            {
                u4IpAddr = *(UINT4 *) (args[0]);
                u4IpMask = *(UINT4 *) (args[1]);
            }
            /* In case of adding new secondary IP, check if it is non-zero IP */
            if ((u4Command == CLI_CFA_OOB_SEC_IP_ADDR) &&
                ((u4IpAddr == 0) || (u4IpMask == 0)))
            {
                CliPrintf (CliHandle,
                           "\r\n%%Invalid Ip address or Ip mask\r\n");
                break;
            }
            i4RetStatus = CfaSetOOBSecondaryIpAddress (CliHandle, u4IpAddr,
                                                       u4IpMask,
                                                       CLI_PTR_TO_U4 (args[2]));

            break;

        case CLI_CFA_OOB_IP_DHCP:
            i4RetStatus = CfaIvrSetAddressAlloc (CliHandle,
                                                 CFA_OOB_MGMT_IFINDEX,
                                                 CFA_IP_ALLOC_POOL,
                                                 (UINT1) (CLI_PTR_TO_U4
                                                          (args[0])));

            break;

        case CLI_CFA_OOB_NO_IP_DHCP:
            i4RetStatus =
                CfaIvrSetAddressAlloc (CliHandle, CFA_OOB_MGMT_IFINDEX,
                                       CFA_IP_ALLOC_MAN, CFA_PROTO_NONE);
            break;
        case CLI_CFA_OOB_INTERFACE_ADMIN_STATUS:
            i4RetStatus = CfaSetInterfaceAdminStatus
                (CliHandle, CFA_OOB_MGMT_IFINDEX, CLI_PTR_TO_U4 (args[0]));
            break;
        case CLI_CFA_SHOW_VIP_ADMIN_STATUS:
            u4Isid = *(UINT4 *) (args[0]);
            STRNCPY ((CHR1 *) pu1Switch, args[1], STRLEN (args[1]));
            pu1Switch[STRLEN (args[1])] = '\0';

            i4RetStatus = CfaShowVipAdminStatus
                (CliHandle, u4Isid, u4ContextId, pu1Switch);
            break;
        case CLI_CFA_OOB_DESC:
            i4RetStatus = CfaSetInterfaceDesc
                (CliHandle, CFA_OOB_MGMT_IFINDEX, (UINT1 *) args[0]);
            break;

        case CLI_CFA_OOB_NO_DESC:
            i4RetStatus = CfaSetInterfaceDesc
                (CliHandle, CFA_OOB_MGMT_IFINDEX, (UINT1 *) args[0]);
            break;

        case CLI_CFA_ENABLE_BACKPLNE_STATUS:

            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CfaSetBackPlaneStatus (u4IfIndex, CFA_BACKPLANE_INTF);
            break;

        case CLI_CFA_DISABLE_BACKPLNE_STATUS:

            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CfaSetBackPlaneStatus (u4IfIndex,
                                                 CFA_FRONTPANEL_INTF);
            break;

        case CLI_CFA_ADD_IFTYPE_DENY_PROT_ENTRY:

            u4ContextId = CLI_GET_CXT_ID ();
            i4RetStatus = CfaAddIfTypeDenyProtocolEntry (CliHandle,
                                                         (INT4) u4ContextId,
                                                         CLI_PTR_TO_I4
                                                         (args[0]),
                                                         CLI_PTR_TO_I4
                                                         (args[1]),
                                                         CLI_PTR_TO_I4
                                                         (args[2]));
            break;

        case CLI_CFA_REM_IFTYPE_DENY_PROT_ENTRY:

            u4ContextId = CLI_GET_CXT_ID ();
            i4RetStatus = (INT4) CfaRemIfTypeDenyProtocolEntry (CliHandle,
                                                                (INT4)
                                                                u4ContextId,
                                                                CLI_PTR_TO_I4
                                                                (args[0]),
                                                                CLI_PTR_TO_I4
                                                                (args[1]),
                                                                CLI_PTR_TO_I4
                                                                (args[2]));
            break;
        case CLI_CFA_ADD_MAP_VLANS:
        case CLI_CFA_REM_MAP_VLANS:
        case CLI_CFA_OVERWRITE_MAP_VLANS:
        case CLI_CFA_PURGE_ALL_MAPPED_VLANS:

            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            pPortArray = (tPortArray *) MemAllocMemBlk (gCfaPortArrayPoolId);

            if (pPortArray == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% cfa : Memory Allocation failure\r\n");
                return CLI_FAILURE;
            }

            CLI_MEMSET (pPortArray, 0, sizeof (tPortArray));

            if ((u4Command != CLI_CFA_PURGE_ALL_MAPPED_VLANS) &&
                (CfaIvrConvertStrToVlanList ((UINT1 *) args[0],
                                             (UINT1 *) pPortArray) ==
                 CFA_FAILURE))
            {

                i4RetStatus = CLI_FAILURE;
            }
            else
            {
                i4RetStatus =
                    CfaIvrMapCliUpdate (CliHandle, u4IfIndex,
                                        (UINT1 *) pPortArray,
                                        (UINT1) u4Command);
            }
            MemReleaseMemBlock (gCfaPortArrayPoolId, (UINT1 *) pPortArray);
            break;

        case CLI_CFA_SHOW_IFTYPE_DENY_PROT:

            if (args[0] != NULL)
            {
                if (VcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId) !=
                    VCM_TRUE)
                {
                    CliPrintf (CliHandle, "\r%% Switch %s Does not exist.\r\n",
                               (UINT1 *) args[0]);

                }
                else
                {
                    CfaCliShowIfTypeDenyProtoTable (CliHandle, u4ContextId);
                }
            }
            else
            {
                CfaCliShowIfTypeDenyProtoTable (CliHandle,
                                                SYS_DEF_MAX_NUM_CONTEXTS);
            }
            break;
        case CLI_CFA_DEBUG:
            if (args[1] != NULL)
            {
                i4Args = CLI_PTR_TO_I4 (args[1]);
                CfaCliSetDebugLevel (CliHandle, i4Args);
            }
            i4RetStatus =
                CfaSetDebug (CliHandle, CLI_PTR_TO_U4 (args[0]), CFA_ENABLED);
            break;
        case CLI_CFA_NO_DEBUGS:
            i4RetStatus = CfaSetDebug
                (CliHandle, CLI_PTR_TO_U4 (args[0]), CFA_DISABLED);
            break;
        case CLI_CFA_LOOPBACK_ENABLE:
        case CLI_CFA_LOOPBACK_DISABLE:

            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            CfaGetIfBrgPortType (u4IfIndex, &u1BrgPortType);
            if (CfaIsPhysicalInterface (u4IfIndex) == CFA_TRUE)
            {
                i4RetStatus = CfaSetInterfaceAdminStatus
                    (CliHandle, u4IfIndex, CLI_PTR_TO_U4 (args[0]));
            }
            else if (u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
            {
                /* Loopback cannot be enabled in S-Channel Interfaces */
                CLI_SET_ERR (CLI_CFA_SBP_ERR);
                i4RetStatus = CLI_FAILURE;
            }
            else
            {
                i4RetStatus = CLI_FAILURE;
                CliPrintf (CliHandle, "\r\n%% Invalid Interface Index\r\n");
            }
            break;
        case CLI_CFA_INTERFACE_PORT_STATE:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus =
                CfaSetPortSecState (CliHandle, u4IfIndex,
                                    CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_CFA_NETWORK_TYPE:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CfaCliSetNetworkType (CliHandle, u4IfIndex,
                                                CLI_PTR_TO_U4 (args[0]));
            break;
        case CLI_CFA_PKT_RX_VALUE:
            MEMSET (ai1PktValue, 0, sizeof (ai1PktValue));

            /* CliReadLine sends data out through CFA. If this 
             * command is called from telnet / SSH, data won't go out
             * since CFA_LOCK is already taken here. Hence it is 
             * released. */
            CFA_UNLOCK ();
            i4RetStatus = CliReadLine (ai1PktValuePrompt, ai1PktValue,
                                       CFA_MAX_PACKET_SIZE);
            CFA_LOCK ();
            if (i4RetStatus != CLI_SUCCESS)
            {
                CliPrintf (CliHandle, "\r %%  Input read error  \r\n");
                i4RetStatus = CLI_FAILURE;
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
                u4PktRxIndex = CLI_PTR_TO_U4 (*args[0]);
                i4RetStatus = CfaCliSetPktRxValue (CliHandle, u4PktRxIndex,
                                                   (UINT1 *) ai1PktValue);
            }
            break;
        case CLI_CFA_PKT_RX_MASK:
            MEMSET (ai1PktValue, 0, sizeof (ai1PktValue));
            /* CliReadLine sends data out through CFA. If this 
             * command is called from telnet / SSH, data won't go out
             * since CFA_LOCK is already taken here. Hence it is 
             * released. */
            CFA_UNLOCK ();
            i4RetStatus = CliReadLine (ai1PktMaskPrompt, ai1PktValue,
                                       CFA_MAX_PACKET_SIZE);
            CFA_LOCK ();
            if (i4RetStatus != CLI_SUCCESS)
            {
                CliPrintf (CliHandle, "\r %%  Input read error  \r\n");
                i4RetStatus = CLI_FAILURE;
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
                u4PktRxIndex = CLI_PTR_TO_U4 (*args[0]);
                i4RetStatus = CfaCliSetPktRxMask (CliHandle, u4PktRxIndex,
                                                  (UINT1 *) ai1PktValue);
            }
            break;
        case CLI_CFA_PKT_RX_NO_VALUE:
            u4PktRxIndex = CLI_PTR_TO_U4 (*args[0]);
            i4RetStatus = CfaCliPktRxDisable (CliHandle, u4PktRxIndex);
            break;
        case CLI_CFA_PKT_RX_NO_MASK:
            u4PktRxIndex = CLI_PTR_TO_U4 (*args[0]);
            i4RetStatus = CfaCliClearPktRxMask (CliHandle, u4PktRxIndex);
            break;
        case CLI_CFA_PKT_RX_SHOW_ENTRY:
            u4PktRxIndex = CLI_PTR_TO_U4 (*args[0]);
            CliPrintf (CliHandle, "\r\n          Packet Analyzer \r\n");
            i4RetStatus = CfaCliShowPktRxEntry (CliHandle, u4PktRxIndex);
            break;
        case CLI_CFA_PKT_RX_SHOW_TABLE:
            i4RetStatus = CfaCliShowPktRxTable (CliHandle);
            break;
        case CLI_CFA_PKT_RX_PORT:
            pau1PktRxPorts = FsUtilAllocBitList (BRG_PORT_LIST_SIZE);

            if (pau1PktRxPorts == NULL)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "cli_process_interface_cmd: "
                         "Error in allocating memory for pau1PktRxPorts\r\n");
                CFA_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CFA_FAILURE;
            }
            MEMSET (pau1PktRxPorts, 0, BRG_PORT_LIST_SIZE);
            u4PktTxIndex = CLI_PTR_TO_U4 (*args[0]);
            CLI_MEMSET (pau1PktRxPorts, 0, BRG_PORT_LIST_SIZE);

            if (CliStrToPortList ((UINT1 *) args[1], pau1PktRxPorts,
                                  BRG_PORT_LIST_SIZE,
                                  CFA_L2VLAN) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Port List\r\n");
                i4RetStatus = CLI_FAILURE;
                FsUtilReleaseBitList (pau1PktRxPorts);
                break;
            }

            i4RetStatus = CfaCliSetPortForPktRx (u4PktTxIndex, pau1PktRxPorts);
            FsUtilReleaseBitList (pau1PktRxPorts);
            break;

        case CLI_CFA_PKT_TX_VALUE:
            MEMSET (ai1PktValue, 0, sizeof (ai1PktValue));
            /* CliReadLine sends data out through CFA. If this 
             * command is called from telnet / SSH, data won't go out
             * since CFA_LOCK is already taken here. Hence it is 
             * released. */
            CFA_UNLOCK ();
            i4RetStatus = CliReadLine (ai1PktValuePrompt, ai1PktValue,
                                       CFA_MAX_PACKET_SIZE);
            CFA_LOCK ();
            if (i4RetStatus != CLI_SUCCESS)
            {
                CliPrintf (CliHandle, "\r %%  Input read error  \r\n");
                i4RetStatus = CLI_FAILURE;
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
                u4PktTxIndex = CLI_PTR_TO_U4 (*args[0]);
                i4RetStatus = CfaCliSetPktTxValue (CliHandle, u4PktTxIndex,
                                                   (UINT1 *) ai1PktValue);
            }
            break;
        case CLI_CFA_PKT_TX_PORT:
            u4PktTxIndex = CLI_PTR_TO_U4 (*args[0]);
            pau1PktTxPorts = FsUtilAllocBitList (BRG_PORT_LIST_SIZE);

            if (pau1PktTxPorts == NULL)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "cli_process_interface_cmd: "
                         "Error in allocating memory for pau1PktTxPorts\r\n");
                return CFA_FAILURE;
            }
            MEMSET (pau1PktTxPorts, 0, BRG_PORT_LIST_SIZE);

            CLI_MEMSET (pau1PktTxPorts, 0, BRG_PORT_LIST_SIZE);

            if (CliStrToPortList ((UINT1 *) args[1], pau1PktTxPorts,
                                  BRG_PORT_LIST_SIZE,
                                  CFA_L2VLAN) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Port List\r\n");
                i4RetStatus = CLI_FAILURE;
                FsUtilReleaseBitList (pau1PktTxPorts);
                break;
            }

            i4RetStatus = CfaCliSetPktTxPort (CliHandle, u4PktTxIndex,
                                              pau1PktTxPorts);
            FsUtilReleaseBitList (pau1PktTxPorts);
            break;
        case CLI_CFA_PKT_TX_PORT_CNT:
            u4PktTxIndex = CLI_PTR_TO_U4 (*args[0]);
            pau1PktTxPorts = FsUtilAllocBitList (BRG_PORT_LIST_SIZE);

            if (pau1PktTxPorts == NULL)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "cli_process_interface_cmd: "
                         "Error in allocating memory for pau1PktTxPorts\r\n");
                return CFA_FAILURE;
            }
            MEMSET (pau1PktTxPorts, 0, BRG_PORT_LIST_SIZE);
            CLI_MEMSET (pau1PktTxPorts, 0, BRG_PORT_LIST_SIZE);

            if (CliStrToPortList ((UINT1 *) args[1], pau1PktTxPorts,
                                  BRG_PORT_LIST_SIZE,
                                  CFA_L2VLAN) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Port List\r\n");
                i4RetStatus = CLI_FAILURE;
                FsUtilReleaseBitList (pau1PktTxPorts);
                break;
            }
            u4PktTxCount = CLI_PTR_TO_U4 (*args[2]);
            i4RetStatus = CfaCliSetPktTxPortCount (CliHandle, u4PktTxIndex,
                                                   pau1PktTxPorts,
                                                   u4PktTxCount);
            FsUtilReleaseBitList (pau1PktTxPorts);
            break;
        case CLI_CFA_PKT_TX_PORT_INT_CNT:
            u4PktTxIndex = CLI_PTR_TO_U4 (*args[0]);
            pau1PktTxPorts = FsUtilAllocBitList (BRG_PORT_LIST_SIZE);

            if (pau1PktTxPorts == NULL)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "cli_process_interface_cmd: "
                         "Error in allocating memory for pau1PktTxPorts\r\n");
                return CFA_FAILURE;
            }
            MEMSET (pau1PktTxPorts, 0, BRG_PORT_LIST_SIZE);
            CLI_MEMSET (pau1PktTxPorts, 0, BRG_PORT_LIST_SIZE);

            if (CliStrToPortList ((UINT1 *) args[1], pau1PktTxPorts,
                                  BRG_PORT_LIST_SIZE,
                                  CFA_L2VLAN) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Port List\r\n");
                i4RetStatus = (UINT4) CLI_FAILURE;
                FsUtilReleaseBitList (pau1PktTxPorts);
                break;
            }
            u4PktTxCount = CLI_PTR_TO_U4 (*args[2]);
            u4PktTxInt = CLI_PTR_TO_U4 (*args[3]);
            i4RetStatus = CfaCliSetPktTxAll (CliHandle, u4PktTxIndex,
                                             pau1PktTxPorts, u4PktTxInt,
                                             u4PktTxCount);
            FsUtilReleaseBitList (pau1PktTxPorts);
            break;
        case CLI_CFA_PKT_TX_DISABLE:
            u4PktTxIndex = CLI_PTR_TO_U4 (*args[0]);
            i4RetStatus = CfaCliPktTxDisable (CliHandle, u4PktTxIndex);
            break;
        case CLI_CFA_PKT_TX_SHOW_ENTRY:
            u4PktTxIndex = CLI_PTR_TO_U4 (*args[0]);
            i4RetStatus = CfaCliShowPktTxEntry (CliHandle, u4PktTxIndex);
            break;
        case CLI_CFA_PKT_TX_SHOW_TABLE:
            i4RetStatus = CfaCliShowPktTxTable (CliHandle);
            break;

        case CLI_CFA_AC_IFACE:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            if (args[1] != NULL)
            {
                i4RetStatus = CfaCliSetACIfInfo (CliHandle, u4IfIndex,
                                                 CLI_PTR_TO_U4 (args[0]),
                                                 CLI_PTR_TO_U4 (*args[1]));
            }
            else
            {
                i4RetStatus = CfaCliSetACIfInfo (CliHandle, u4IfIndex,
                                                 CLI_PTR_TO_U4 (args[0]), 0);
            }
            break;
        case CLI_CFA_VLAN_INTF_COUNTERS:
            u4IpIntfStats = CLI_PTR_TO_U4 (args[0]);
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            i4RetStatus =
                CfaCliSetCounters (CliHandle, u4IfIndex, u4IpIntfStats);
            break;
#ifdef HDLC_WANTED
        case CLI_CFA_SET_HDLC_CONTROLLER_ENABLE:
            i4RetStatus = CfaCliSetHdlcControllerEnable (CliHandle,
                                                         CLI_PTR_TO_U4 (args
                                                                        [0]));
            break;
#endif
        case CLI_CFA_UFD_SYS_CTRL:
            i4RetStatus = CfaCliSetUfdSystemCtrl (CliHandle,
                                                  CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_CFA_UFD_STATUS:
            i4RetStatus = CfaCliSetUfdModuleStatus (CliHandle,
                                                    CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_CFA_INTERFACE_ROLE:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CfaCliSetPortRole (CliHandle, u4IfIndex,
                                             CLI_PTR_TO_I4 (args[0]),
                                             CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_CFA_UFD_GROUP_CREATE:
            /* args[0] - UFD Group identifier to create */
            /* args[1] - UFD Group name - optional parameter */
            if (args[1] != NULL)
            {
                i4RetStatus = CfaCliCreateUfdGroup (CliHandle,
                                                    CLI_PTR_TO_U4 (*args[0]),
                                                    (UINT1 *) (VOID
                                                               *) (args[1]));
            }
            else
            {
                i4RetStatus = CfaCliCreateUfdGroup (CliHandle,
                                                    CLI_PTR_TO_U4 (*args[0]),
                                                    0);
            }
            break;

        case CLI_CFA_UFD_GROUP_DELETE:
            /* args[0] - Ufd Group identifier to delete */
            i4RetStatus = CfaCliDeleteUfdGroup (CliHandle,
                                                CLI_PTR_TO_I4 (*args[0]));
            break;

        case CLI_CFA_UFD_GROUP_PORT:

            pMemberPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pMemberPorts == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% Error in Allocating memory for bitlist \r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            MEMSET (pMemberPorts, 0, sizeof (tPortList));

            /* args[0] - interface type for member ports */
            /* args[1] - interface identifier for member ports */
            /* args[2] - interface type for member ports */
            /* args[3] - interface identifier for member ports */
            /* args[4] - CFA_UFD_GROUP_PORTS_ADD/DELETE */

            /* build member port list */

            /* Check if gigabitethernet ports are given */
            if (args[1] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[0], (INT1 *) args[1],
                                     *pMemberPorts, BRG_PORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Member"
                               " Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    break;
                }
                i4RetStatus = CfaCliUfdGroupSetPorts (CliHandle,
                                                      *pMemberPorts,
                                                      CLI_PTR_TO_U4 (args[4]));
                MEMSET (pMemberPorts, 0, sizeof (tPortList));
            }

            /* Check if LAG  ports are given */
            if (args[3] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[2], (INT1 *) args[3],
                                     *pMemberPorts, BRG_PORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Member Port(s) \r\n");
                    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
                    break;
                }
                i4RetStatus = CfaCliUfdGroupSetPorts (CliHandle,
                                                      *pMemberPorts,
                                                      CLI_PTR_TO_U4 (args[4]));

            }
            FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
            break;

        case CLI_CFA_UFD_GROUP_DESIGUPLINK_PORT:
            i4RetStatus = CfaCliSetDesigUplinkPort (CliHandle, i4Inst);
            break;

        case CLI_CFA_DOT1Q_VLAN:
            /* args[0] = VLAN ID to be encapsulated */
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CfaCliSetDot1QEncapForL3SubIf (CliHandle,
                                                         u4IfIndex, (*args[0]));
            break;

        case CLI_CFA_NO_DOT1Q_VLAN:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CfaCliDelDot1QEncapForL3SubIf (CliHandle, u4IfIndex);
            break;

        case CLI_CFA_SHOW_PORT_ROLE:
            i4RetStatus = CfaCliShowPortRole (CliHandle, i4Inst);
            break;

        case CLI_CFA_SHOW_UFD_STATS:
            i4RetStatus = CfaCliShowPortUfdStats (CliHandle, i4Inst);
            break;

        case CLI_CFA_SHOW_UFD:
            i4RetStatus = CfaCliShowUfdDatabase (CliHandle, 0, u4Command);
            break;
        case CLI_CFA_SHOW_UFD_GROUP:
            i4RetStatus = CfaCliShowUfdDatabase (CliHandle,
                                                 CLI_PTR_TO_I4 (*args[0]),
                                                 u4Command);
            break;

        case CLI_CFA_SHOW_UFD_BRIEF:
            i4RetStatus = CfaCliShowUfdDatabase (CliHandle, 0, u4Command);
            break;

        case CLI_CFA_SH_STATUS:
            i4RetStatus =
                CfaCliSetShModuleStatus (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_CFA_SH_SYS_CTRL:
            i4RetStatus =
                CfaCliSetShSystemCtrl (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_CFA_LINKUP_GLOBAL_STATUS:
            i4RetStatus =
                CfaCliSetLinkUpDelayOnSystem (CliHandle,
                                              (UINT4) CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_CFA_SHOW_SH:

            if ((UINT4) i4Inst == 0)
            {
                IssShowSplitHorizon (CliHandle, 0);
            }
            else
            {
                IssShowSplitHorizon (CliHandle, i4Inst);
            }
            break;
        case CLI_CFA_SHOW_LINKUP:
            if ((UINT4) i4Inst == 0)
            {

                i4RetStatus = CfaCliShowLinkUpDelay (CliHandle, 0);
            }
            else
            {

                i4RetStatus = CfaCliShowLinkUpDelay (CliHandle, i4Inst);
            }
            break;

        default:
            break;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_CFA) &&
            (u4ErrCode < CLI_CFA_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s",
                       CfaCliErrString[CLI_ERR_OFFSET_CFA (u4ErrCode)]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    CFA_UNLOCK ();

    CliUnRegisterLock (CliHandle);
    return i4RetStatus;

}

/****************************************************************************
* Function    :  CfaCliSetDebugLevel
* Description :
* Input       :  CliHandle, i4CliDebugLevel
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
CfaCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel)
{
    CFA_GLB_DBG_LVL () = 0;

    UNUSED_PARAM (CliHandle);

    if (i4CliDebugLevel == DEBUG_DEBUG_LEVEL)
    {
        CFA_GLB_DBG_LVL () =
            MGMT_TRC | BUFFER_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC |
            OS_RESOURCE_TRC | CONTROL_PLANE_TRC | DATA_PATH_TRC | DUMP_TRC;

    }
    else if (i4CliDebugLevel == DEBUG_INFO_LEVEL)
    {
        CFA_GLB_DBG_LVL () = CONTROL_PLANE_TRC | DATA_PATH_TRC | DUMP_TRC
            | INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_NOTICE_LEVEL)
    {
        CFA_GLB_DBG_LVL () = INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_WARN_LEVEL)
    {
        CFA_GLB_DBG_LVL () = INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_ERROR_LEVEL)
    {
        CFA_GLB_DBG_LVL () = INIT_SHUT_TRC | ALL_FAILURE_TRC;
    }
    else if ((i4CliDebugLevel == DEBUG_CRITICAL_LEVEL)
             || (i4CliDebugLevel == DEBUG_ALERT_LEVEL)
             || (i4CliDebugLevel == DEBUG_EMERG_LEVEL))
    {
        CFA_GLB_DBG_LVL () = INIT_SHUT_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_DEF_LVL_FLAG)
    {
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowVipAdminStatus                              */
/*                                                                           */
/*     DESCRIPTION      : This function calls the create routines based on   */
/*                        its type                                           */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be created   */
/*                        au1IfName - Name of the interface                  */
/*                        u4IfType  - Type of Interface to be created        */
/*                        CliHandle - CLI Handler                            */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
CfaShowVipAdminStatus (tCliHandle CliHandle, UINT4 u4Isid, UINT4 u4ContextId,
                       UINT1 *pu1Switch)
{
    UINT4               u4IfIndex = 0;
    INT4                i4OperStat = 0;

    if (VcmIsSwitchExist (pu1Switch, &u4ContextId) != VCM_TRUE)
    {
        CliPrintf (CliHandle, "\r%% Switch Does not exist.\r\n");
        return CLI_FAILURE;
    }
    L2IwfGetVipIfIndex (u4ContextId, u4Isid, (UINT4 *) &u4IfIndex);

    nmhGetIfOperStatus ((INT4) u4IfIndex, (INT4 *) &i4OperStat);
    if (i4OperStat == CFA_IF_UP)
    {
        CliPrintf (CliHandle, "\r\nline protocol is up (connected)\r\n");
    }
    else if (i4OperStat == CFA_IF_DOWN)
    {
        CliPrintf (CliHandle, "\r\nline protocol is down (not connect)\r\n");
    }
    else if (i4OperStat == CFA_IF_NP)
    {
        CliPrintf (CliHandle, "\r\nline protocol is down (not present)\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nline protocol is down (other)\r\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCreateInterface                                 */
/*                                                                           */
/*     DESCRIPTION      : This function calls the create routines based on   */
/*                        its type                                           */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be created   */
/*                        au1IfName - Name of the interface                  */
/*                        u4IfType  - Type of Interface to be created        */
/*                        CliHandle - CLI Handler                            */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
CfaCreateInterface (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4IfType,
                    UINT1 *au1IfName, UINT4 u4L2CxtId, UINT4 u4IfSubType)
{
    INT4                i4Status = CLI_SUCCESS;
    UINT1               au1IvrName[58];
    UINT1               au1TempIfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT2               u2IfIvrVlanId = 0;
    UINT4               u4ContextId = 0;

    /* First check the alias name length. If it is success, no need
     * to call test routine before setting alias name */
    UNUSED_PARAM (u4IfIndex);

    if (CLI_STRLEN (au1IfName) >= CFA_MAX_PORT_NAME_LENGTH)
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot Create the interface. "
                   "Check the Interface Name\r\n");
        return (CLI_FAILURE);
    }

    switch (u4IfType)
    {

        case CFA_ENET:
            CliPrintf (CliHandle, "\r%% Cannot create Physical Interface \r\n");
            break;
        case CFA_L3IPVLAN:
            /* Get the VLAN ID from the Interface name */
            u2IfIvrVlanId = (UINT2) ATOI (&au1IfName[4]);

            VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);

            if (CfaIsL3VlanOverLapWithL3SubVlan (u4ContextId, u2IfIvrVlanId) ==
                CFA_TRUE)
            {
                /* Valid L3subinterface index with this VLAN Id exists 
                 * So return FAILURE */
                CliPrintf (CliHandle, "\r%% Cannot Create the interface. "
                           "Vlan %d is associated with L3subinterface"
                           "\r\n", u2IfIvrVlanId);
                return CLI_FAILURE;

            }

            if (CfaCliGetIVRNameFromIfNameAndCxtId (u4L2CxtId, au1IfName,
                                                    au1IvrName) == CLI_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Cannot Create the interface. "
                           "Check the Interface Name\r\n");
                return CLI_FAILURE;
            }

            i4Status = CfaCreateVlanInterface (CliHandle, u4IfIndex, au1IfName,
                                               u4L2CxtId);
            break;

        case CFA_L3SUB_INTF:

            i4Status = CfaCreateL3SubInterface (CliHandle, u4IfIndex, au1IfName,
                                                u4L2CxtId);
            break;

        case CFA_LOOPBACK:
            i4Status = CfaCreateLoopbackInterface (CliHandle, u4IfIndex,
                                                   au1IfName);
            break;

        case CFA_LAGG:
            if (LaIsLaStarted () == LA_TRUE)
            {
                i4Status = CfaCreatePortChannelInterface
                    (CliHandle, u4IfIndex, au1IfName);
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r%% LA module is Shutdown. Cannot create port-channel "
                           "interface\r\n");
                return (CLI_FAILURE);
            }
            break;

        case CFA_TUNNEL:
            i4Status = CfaCliCreateLogicalInterface
                (CliHandle, u4IfIndex, au1IfName);
            break;

        case CFA_BRIDGED_INTERFACE:
            i4Status =
                CfaCreateInternalInterface (CliHandle, u4IfIndex, au1IfName);
            break;

        case CFA_ILAN:
            i4Status = CfaCreateILanInterface (CliHandle, u4IfIndex, au1IfName);
            break;

        case CFA_PROP_VIRTUAL_INTERFACE:
            if (u4IfSubType == CFA_SUBTYPE_SISP_INTERFACE)
            {
                i4Status = CfaCreateSispInterface (CliHandle, u4IfIndex,
                                                   au1IfName);
            }
            else
            {
                i4Status = CfaCliCreateACInterface (CliHandle, u4IfIndex,
                                                    au1IfName);
            }

            break;

#ifdef PPP_WANTED
        case CFA_PPP:
            i4Status = CfaCliCreatePppInterface (CliHandle, u4IfIndex,
                                                 au1IfName);
            break;
#endif /* PPP_WANTED */

        case CFA_PSEUDO_WIRE:
            i4Status = CfaCliCreatePswInterface (CliHandle, u4IfIndex,
                                                 au1IfName);
            break;
#ifdef VXLAN_WANTED
        case CFA_VXLAN_NVE:
            i4Status = CfaCliCreateNveInterface (CliHandle, u4IfIndex,
                                                 au1IfName);
            break;
#endif

#ifdef HDLC_WANTED
        case CFA_HDLC:
            i4Status = CfaCliCreateHdlcInterface (CliHandle, u4IfIndex,
                                                  au1IfName);
            break;
#endif
#ifdef VCPEMGR_WANTED
        case CFA_TAP:
            i4Status = CfaCliCreateTapInterface (CliHandle, u4IfIndex,
                                                 au1IfName);
            break;
#endif
        default:
            CliPrintf (CliHandle, "\r%% Cannot Create the interface. "
                       "Interface type Unknown\r\n");
            return (CLI_FAILURE);
            break;
    }

    if (u4IfType != CFA_ILAN)
    {
        if (u4IfType == CFA_L3IPVLAN)
        {
            CliChangePath ((CHR1 *) au1IvrName);
        }
        else if ((u4IfType == CFA_L3SUB_INTF) && (CLI_SUCCESS == i4Status))
        {
            /* Change the mode to iss(config-subif)# mode */
            SPRINTF ((CHR1 *) au1TempIfName, "%s%d",
                     CLI_L3IFSUB_MODE, u4IfIndex);
            CliChangePath ((CHR1 *) au1TempIfName);
        }
        else
        {
            CliChangePath ((CHR1 *) au1IfName);
        }
    }

    return (i4Status);
}

/*****************************************************************************/
/*     FUNCTION NAME    : CfaSetInterfaceDesc                                */
/*                                                                           */
/*     DESCRIPTION      : This function is used to add/remove the description*/
/*                        of an interface.                                   */
/*                                                                           */
/*     INPUT            : CliHandle - CliHandler                             */
/*                        u4IfaceIndex - Index of the interface              */
/*                        pu1IfDesc - Description string                     */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
CfaSetInterfaceDesc (tCliHandle CliHandle, UINT4 u4IfaceIndex, UINT1 *pu1IfDesc)
{
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE IfDesc;
    MEMSET (&IfDesc, 0, sizeof (IfDesc));
    IfDesc.i4_Length = (INT4) STRLEN (pu1IfDesc);
    IfDesc.pu1_OctetList = pu1IfDesc;

    if (nmhTestv2IfMainDesc (&u4ErrCode, (INT4) u4IfaceIndex, &IfDesc) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIfMainDesc (u4IfaceIndex, &IfDesc) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCreatePhysicalInterface                         */
/*                                                                           */
/*     DESCRIPTION      : This function creates a Physical interface         */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be created   */
/*                        pu1IfName - Name of the interface                  */
/*                        CliHandle, CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
CfaCreatePhysicalInterface (UINT1 *pu1IfName,
                            UINT1 *pu1IfNum, UINT4 *pu4IfIndex)
{

    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE Alias;
    UINT4               u4IfIndex;
    if (VcmGetSystemModeExt (CFA_PROTOCOL_ID) == VCM_SI_MODE)
    {
        return CLI_FAILURE;
    }

    if (CfaGetIfIndexFromIfNameAndIfNum (pu1IfName, pu1IfNum,
                                         pu4IfIndex) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    u4IfIndex = *pu4IfIndex;

    /* Since this function is called before entry function cli_process_interface_cmd,
       cfa lock is taken explicitly here */
    CFA_LOCK ();

    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return (CLI_FAILURE);
    }

    Alias.i4_Length = (INT4) STRLEN (pu1IfName);
    Alias.pu1_OctetList = pu1IfName;

    nmhSetIfAlias (u4IfIndex, &Alias);

    if (nmhTestv2IfMainType (&u4ErrCode, (INT4) u4IfIndex, CFA_ENET) ==
        SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CFA_UNLOCK ();
        return (CLI_FAILURE);
    }

    if (nmhSetIfMainType (u4IfIndex, CFA_ENET) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return (CLI_FAILURE);
    }

    nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE);

    CFA_UNLOCK ();

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCreateVlanInterface                             */
/*                                                                           */
/*     DESCRIPTION      : This function creates a VLAN interface             */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be created   */
/*                        pu1IfName - Name of the interface                  */
/*                        CliHandle, CLI Handler                             */
/*                        u4L2CxtId - L2 context id for the IVR interface    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCreateVlanInterface (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 *pu1IfName,
                        UINT4 u4L2CxtId)
{
    tSNMP_OCTET_STRING_TYPE Alias;
    UINT4               u4ErrCode;
#ifdef PB_WANTED
    UINT2               u2VlanId;
#endif

    if (STRCMP (pu1IfName, "vlan0") == 0)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface Name\r\n");
        return (CLI_FAILURE);
    }

    Alias.i4_Length = (INT4) STRLEN (pu1IfName);
    Alias.pu1_OctetList = pu1IfName;

#ifdef PB_WANTED
    u2VlanId = (UINT2) atol ((const CHR1 *) &pu1IfName[4]);
    if (VlanPbIsCreateIvrValid (u2VlanId) == VLAN_FALSE)
    {
        CliPrintf (CliHandle,
                   "\r%% Customer ports are associated with this VLAN.Hence IP interface creation is not allowed. \r\n");
        return CLI_FAILURE;
    }
#endif

#ifdef VLAN_WANTED
    /* Check for not adding IVR Vlan in Dot1d Mode */
    if ((u4IfIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES) &&
        (VlanGetBaseBridgeMode () == DOT_1D_BRIDGE_MODE))
    {
        CLI_SET_ERR (CLI_CFA_VLAN_BASE_BRIDGE_ENABLED);
        return CLI_FAILURE;
    }
#endif

    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* Set the l2 context Id */
    if (VcmTestL2CxtId (&u4ErrCode, u4IfIndex, u4L2CxtId) == VCM_FAILURE)
    {
        VcmCfaDeleteInterfaceMapping (u4IfIndex);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return CLI_FAILURE;
    }

    if (VcmSetL2CxtId (u4IfIndex, u4L2CxtId) == VCM_FAILURE)
    {
        VcmCfaDeleteInterfaceMapping (u4IfIndex);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* We can directly call set routine, because validation is done already
     * in the function CfaCreateInterface() */
    nmhSetIfAlias (u4IfIndex, &Alias);

    /* Set the Type */
    if (nmhTestv2IfMainType (&u4ErrCode, (INT4) u4IfIndex, CFA_L3IPVLAN) ==
        SNMP_FAILURE)
    {
        VcmCfaDeleteIPIfaceMapping (u4IfIndex, CFA_TRUE);
        VcmCfaDeleteInterfaceMapping (u4IfIndex);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return (CLI_FAILURE);
    }

    if (nmhSetIfMainType (u4IfIndex, CFA_L3IPVLAN) == SNMP_FAILURE)
    {
        VcmCfaDeleteInterfaceMapping (u4IfIndex);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        CLI_SET_ERR (CLI_CFA_RESV_VLAN_ERR);
        return (CLI_FAILURE);
    }
    /* All the mandatory parameters are set. 
     * So make the row active */
    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        VcmCfaDeleteInterfaceMapping (u4IfIndex);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCreateL3SubInterface                            */
/*                                                                           */
/*     DESCRIPTION      : This function creates L3 subinterface              */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be created   */
/*                        pu1IfName - Name of the interface                  */
/*                        CliHandle, CLI Handler                             */
/*                        u4L2CxtId - L2 context id for the IVR interface    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCreateL3SubInterface (tCliHandle CliHandle, UINT4 u4IfIndex,
                         UINT1 *pu1IfName, UINT4 u4L2CxtId)
{
    tSNMP_OCTET_STRING_TYPE Alias;
    UINT4               u4ErrCode;

    Alias.i4_Length = (INT4) STRLEN (pu1IfName);
    Alias.pu1_OctetList = pu1IfName;

    CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "Entering CfaCreateL3SubInterface\n");

    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Cannot Create L3Subinterface\r\n");
        return (CLI_FAILURE);
    }

    /* Set the l2 context Id */
    if (VcmTestL2CxtId (&u4ErrCode, u4IfIndex, u4L2CxtId) == VCM_FAILURE)
    {
        VcmCfaDeleteInterfaceMapping (u4IfIndex);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaCreateL3SubInterface " "Error in VcmTestL2CxtId\r\n");
        return CLI_FAILURE;
    }

    if (VcmSetL2CxtId (u4IfIndex, u4L2CxtId) == VCM_FAILURE)
    {
        VcmCfaDeleteInterfaceMapping (u4IfIndex);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaCreateL3SubInterface " "Error in VcmSetL2CxtId\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* We can directly call set routine, because validation is done already
     * in the function CfaCreateInterface() */
    nmhSetIfAlias (u4IfIndex, &Alias);

    /* Set the Type */
    if (nmhTestv2IfMainType (&u4ErrCode, (INT4) u4IfIndex, CFA_L3SUB_INTF) ==
        SNMP_FAILURE)
    {
        VcmCfaDeleteIPIfaceMapping (u4IfIndex, CFA_TRUE);
        VcmCfaDeleteInterfaceMapping (u4IfIndex);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaCreateL3SubInterface " "Error in nmhTestv2IfMainType\r\n");
        return (CLI_FAILURE);
    }

    if (nmhSetIfMainType (u4IfIndex, CFA_L3SUB_INTF) == SNMP_FAILURE)
    {
        VcmCfaDeleteInterfaceMapping (u4IfIndex);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaCreateL3SubInterface " "Error in nmhSetIfMainType\r\n");
        return (CLI_FAILURE);
    }
    /* All the mandatory parameters are set. 
     * So make the row active */
    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        VcmCfaDeleteInterfaceMapping (u4IfIndex);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaCreateL3SubInterface "
                 "Error in nmhSetIfMainRowStatus\r\n");
        return (CLI_FAILURE);
    }

    CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "Exiting CfaCreateL3SubInterface\n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCreateLoopbackInterface                         */
/*                                                                           */
/*     DESCRIPTION      : This function creates a Loopback interface         */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be created   */
/*                        pu1IfName - Name of the interface                  */
/*                        CliHandle, CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCreateLoopbackInterface (tCliHandle CliHandle, UINT4 u4IfIndex,
                            UINT1 *pu1IfName)
{
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE Alias;

    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    Alias.i4_Length = (INT4) STRLEN (pu1IfName);
    Alias.pu1_OctetList = pu1IfName;

    /* We can directly call set routine, because validation is done already
     * in the function CfaCreateInterface() */

    nmhSetIfAlias (u4IfIndex, &Alias);

    /* Set the Type */
    if (nmhTestv2IfMainType (&u4ErrCode, (INT4) u4IfIndex, CFA_LOOPBACK) ==
        SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return (CLI_FAILURE);
    }

    if (nmhSetIfMainType (u4IfIndex, CFA_LOOPBACK) == SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* All the mandatory parameters are set. 
     * So make the row active */
    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE) == SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCreatePortChannelInterface                      */
/*                                                                           */
/*     DESCRIPTION      : This function creates a LA PortChannel interface   */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be created   */
/*                        pu1IfName - Name of the interface                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCreatePortChannelInterface (tCliHandle CliHandle, UINT4 u4IfIndex,
                               UINT1 *pu1IfName)
{
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE Alias;

    if (STRCMP (pu1IfName, "Po0") == 0)
    {
        CliPrintf (CliHandle, "\rInvalid Interface Name\r\n");
        return (CLI_FAILURE);
    }
    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    Alias.i4_Length = (INT4) STRLEN (pu1IfName);
    Alias.pu1_OctetList = pu1IfName;

    /* we can directly call set routine, because validation is done already
     * in the function CfaCreateInterface() */
    nmhSetIfAlias (u4IfIndex, &Alias);

    /* Set the Type */
    if (nmhTestv2IfMainType (&u4ErrCode, (INT4) u4IfIndex, CFA_LAGG) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return (CLI_FAILURE);
    }

    if (nmhSetIfMainType (u4IfIndex, CFA_LAGG) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    /* All the mandatory parameters are set. 
     * So make the row active */
    nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCreateInternalInterface                         */
/*                                                                           */
/*     DESCRIPTION      : This function creates an internal interface        */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be created   */
/*                        pu1IfName - Name of the interface                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCreateInternalInterface (tCliHandle CliHandle,
                            UINT4 u4IfIndex, UINT1 *pu1IfName)
{
    tSNMP_OCTET_STRING_TYPE Alias;
    UINT4               u4ErrCode = 0;

    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    Alias.i4_Length = (INT4) STRLEN (pu1IfName);
    Alias.pu1_OctetList = pu1IfName;

    /* we can directly call set routine, because validation is done already
     * in the function CfaCreateInterface() */
    nmhSetIfAlias (u4IfIndex, &Alias);

    /* Set the Type */
    if (nmhTestv2IfMainType
        (&u4ErrCode, (INT4) u4IfIndex,
         (INT4) CFA_BRIDGED_INTERFACE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return (CLI_FAILURE);
    }

    if (nmhSetIfMainType (u4IfIndex, CFA_BRIDGED_INTERFACE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    /* All the mandatory parameters are set. 
     * So make the row active */
    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCreateILanInterface                             */
/*                                                                           */
/*     DESCRIPTION      : This function creates an I-LAN interface           */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be created   */
/*                        pu1IfName - Name of the interface                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCreateILanInterface (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 *pu1IfName)
{
    tSNMP_OCTET_STRING_TYPE Alias;

    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    Alias.i4_Length = (INT4) STRLEN (pu1IfName);
    Alias.pu1_OctetList = pu1IfName;

    nmhSetIfAlias (u4IfIndex, &Alias);

    if (nmhSetIfMainType (u4IfIndex, CFA_ILAN) == SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return (SNMP_FAILURE);
    }

    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaDeleteInterface                                 */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the given interface          */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaDeleteInterface (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    UINT4               u4ErrCode;
#ifdef VXLAN_WANTED
    INT4                i4IfType = 0;
#endif

    UNUSED_PARAM (CliHandle);
#ifdef VXLAN_WANTED
    if (nmhGetIfMainType ((INT4) u4IfIndex, &i4IfType) == SNMP_SUCCESS)
    {
        if (i4IfType == CFA_LOOPBACK)
        {
            if (VxlanIsLoopbackUsed (u4IfIndex) == VXLAN_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r\n%% Deletion/Modification of loopback "
                           "interface is not allowed since it is used by VXLAN module\r\n");
                return CLI_FAILURE;

            }

        }
    }
#endif
    if (nmhTestv2IfMainRowStatus (&u4ErrCode, (INT4) u4IfIndex,
                                  (INT4) CFA_RS_DESTROY) == SNMP_FAILURE)
    {
        if ((CLI_SUCCESS == CLI_GET_ERR (&u4ErrCode)) && (u4ErrCode == 0))
        {
            CliPrintf (CliHandle, "\r%% Error: Cannot Delete Interface\r\n");
        }
        return (CLI_FAILURE);
    }

    nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetInterfaceAdminStatus                         */
/*                                                                           */
/*     DESCRIPTION      : This function set the admin status of  the         */
/*                                                       interface           */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
/*                        CliHandle - CLI Handler                            */
/*                        u4AdminStatus - Status                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetInterfaceAdminStatus (tCliHandle CliHandle, UINT4 u4IfIndex,
                            UINT4 u4AdminStatus)
{
    UINT4               u4ErrCode;
    INT4                i4RowStatus;
    UINT1               u1IfType;

    if (nmhGetIfMainRowStatus ((INT4) u4IfIndex, &i4RowStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

    if ((u1IfType == CFA_PPP) && (i4RowStatus != CFA_RS_ACTIVE)
        && (u4AdminStatus == CFA_IF_UP))
    {
        CliPrintf (CliHandle,
                   "\r\n%% Attach the PPP interface to an underlying physical interface first\r\n");
        return (CLI_FAILURE);
    }

    if (nmhTestv2IfAdminStatus
        (&u4ErrCode, (INT4) u4IfIndex, (INT4) u4AdminStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIfAdminStatus (u4IfIndex, u4AdminStatus) == SNMP_FAILURE)
    {
        if ((CLI_SUCCESS == CLI_GET_ERR (&u4ErrCode)) &&
            (u4ErrCode != CLI_CFA_UFD_ERR_DISABLED_ERR))
        {
            CLI_FATAL_ERROR (CliHandle);
        }
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetInterfaceMtu                                 */
/*                                                                           */
/*     DESCRIPTION      : This function sets the MTU for a given interface   */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
/*                        u4Mtu - MTU value                                  */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetInterfaceMtu (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4Mtu)
{

    UINT4               u4ErrCode;
    INT4                i4RowStatus;
    INT4                i4BridgedIfaceStatus;
    UINT1               u1IfType;

    if (nmhGetIfMainRowStatus ((INT4) u4IfIndex, &i4RowStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

    if ((u1IfType == CFA_PPP) && (i4RowStatus != CFA_RS_ACTIVE))
    {
        CliPrintf (CliHandle,
                   "\r\n%% Attach the PPP interface to an underlying physical interface first\r\n");
        return (CLI_FAILURE);
    }

    nmhGetIfIvrBridgedIface ((INT4) u4IfIndex, &i4BridgedIfaceStatus);

    if (u4Mtu > CFA_MAX_L3_INTF_ENET_MTU)
    {
        if ((i4BridgedIfaceStatus == CFA_DISABLED)
            || (CfaIsL3IpVlanInterface (u4IfIndex) == CFA_SUCCESS))
        {
            CliPrintf (CliHandle,
                       "\r\n%% Invalid MTU value for the interface type\r\n");
            return (CLI_FAILURE);
        }
    }

    if (nmhTestv2IfMainMtu (&u4ErrCode, (INT4) u4IfIndex, (INT4) u4Mtu) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIfMainMtu (u4IfIndex, u4Mtu) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetInterfaceAlias                               */
/*                                                                           */
/*     DESCRIPTION      : This function sets the alias for a given interface */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
/*                        u4Alias - Alias String                            */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_SUCCESS                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetInterfaceAlias (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 *pu1IfAlias)
{

    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE InterfaceAlias;

    MEMSET (&InterfaceAlias, 0, sizeof (InterfaceAlias));
    InterfaceAlias.i4_Length = (INT4) STRLEN (pu1IfAlias);
    InterfaceAlias.pu1_OctetList = pu1IfAlias;

    if (nmhTestv2IfAlias (&u4ErrCode, (INT4) u4IfIndex, &InterfaceAlias) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIfAlias (u4IfIndex, &InterfaceAlias) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetInterfaceExtMacAddr                             */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Mac address for             */
/*                        a given interface                                  */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface                 */
/*                        au1PortMacAddr - Port Mac Address                  */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
CfaSetInterfaceExtMacAddr (tCliHandle CliHandle, UINT4 u4IfIndex,
                           tMacAddr PortMac)
{

    UINT4               u4ErrCode;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2IfMainExtMacAddress (&u4ErrCode, (INT4) u4IfIndex, PortMac)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIfMainExtMacAddress (u4IfIndex, PortMac) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetLinkTrapStatus                               */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Link Up/Down Trap status    */
/*                        for the specified port(s).                         */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
/*                        u4LinkStatus- Status                               */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetLinkStatusTrap (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4LinkStatus)
{
    UINT4               u4ErrCode;

    if (nmhTestv2IfLinkUpDownTrapEnable (&u4ErrCode, (INT4) u4IfIndex,
                                         (INT4) u4LinkStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIfLinkUpDownTrapEnable (u4IfIndex, u4LinkStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetFlowControl                                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Link Flow Control           */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
/*                        i4Pause-  status                                   */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
CfaSetFlowcontrol (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4Pause)
{
    UINT4               u4ErrCode;

    INT4                i4ExistingMode;

    UINT1               u1BrgPortType = 0;

    /* As it is not possible to give Pause TxRx enable and Pause TxRx disable
     * in a single command, the given command is changed based on the existing
     * mode. Here are the list of combinations:
     * CLI command        Existing mode       snmp set
     * send off           enableTx            disabled
     * send off           enableRx            nothing to do
     * send off           enableBoth          enableRx
     * send off           disabled            nothing to do
     *
     * send on            enableTx            nothing to do
     * send on            enableRx            enableBoth
     * send on            enableBoth          nothing to do
     * send on            disabled            enableTx
     *
     * receive off        enableTx            nothing to do
     * receive off        enableRx            disable
     * receive off        enableBoth          enableTx
     * receive off        disabled            nothing to do
     *
     * receive on         enableTx            enabledBoth
     * receive on         enableRx            nothing to do
     * receive on         enableBoth          nothing to do
     * receive on         disabled            enableRx
     */

    /* the interface should be admin down */
    if ((CFA_IF_ADMIN (u4IfIndex) == CFA_IF_UP))
    {
        CliPrintf (CliHandle, "\r%% Interface must first be made "
                   "administratively down before setting flow control status\r\n");
        return SNMP_FAILURE;
    }

    nmhGetDot3PauseAdminMode ((INT4) u4IfIndex, &i4ExistingMode);
    CfaGetIfBrgPortType (u4IfIndex, &u1BrgPortType);
    /* Flow Control is Not Supported for S-Channel Interface */
    if (u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        CliPrintf (CliHandle, "\r%% Flow Control Support is not given"
                   " for S-Channel Interface \r\n");
        return SNMP_FAILURE;
    }

    switch (i4Pause)
    {
        case PAUSE_ENABLED_XMIT:
            if (i4ExistingMode == PAUSE_ENABLED_RCV)
            {
                i4Pause = PAUSE_ENABLED_BOTH;
            }
            else
            {
                if ((i4ExistingMode == PAUSE_ENABLED_BOTH) ||
                    (i4ExistingMode == PAUSE_ENABLED_XMIT))
                {
                    return (CLI_SUCCESS);
                }
                /* i4ExistingMode == PAUSE_DISABLED, 
                 * Nothing to do, just pass enable_tx to test routine. */
            }
            break;

        case PAUSE_ENABLED_RCV:
            if ((i4ExistingMode == PAUSE_ENABLED_BOTH) ||
                (i4ExistingMode == PAUSE_ENABLED_RCV))
            {
                /* Already pause Rx is enabled, nothing to do. */
                return (CLI_SUCCESS);
            }
            if (i4ExistingMode == PAUSE_ENABLED_XMIT)
            {
                i4Pause = PAUSE_ENABLED_BOTH;
            }
            /* For i4ExistingMode == PAUSE_DISABLED case nothing to do.
             * Just pass enable_rx to test/set routine. */
            break;

        case PAUSE_DISABLED_XMIT:
            if (i4ExistingMode == PAUSE_ENABLED_BOTH)
            {
                i4Pause = PAUSE_ENABLED_RCV;
            }
            else
            {
                if ((i4ExistingMode == PAUSE_ENABLED_RCV) ||
                    (i4ExistingMode == PAUSE_DISABLED))
                {
                    return (CLI_SUCCESS);
                }
                if (i4ExistingMode == PAUSE_ENABLED_XMIT)
                {
                    i4Pause = PAUSE_DISABLED;
                }
            }
            break;
        case PAUSE_DISABLED_RCV:
            if (i4ExistingMode == PAUSE_ENABLED_BOTH)
            {
                i4Pause = PAUSE_ENABLED_XMIT;
            }
            else
            {
                if ((i4ExistingMode == PAUSE_ENABLED_XMIT) ||
                    (i4ExistingMode == PAUSE_DISABLED))
                {
                    return (CLI_SUCCESS);
                }
                if (i4ExistingMode == PAUSE_ENABLED_RCV)
                {
                    i4Pause = PAUSE_DISABLED;
                }
            }
            break;
#if defined(MRVLLS) || defined(XCAT) || defined(XCAT3)
        case PAUSE_ENABLED_BOTH:
            i4Pause = PAUSE_ENABLED_BOTH;
            break;

        case PAUSE_DISABLED:
            i4Pause = PAUSE_DISABLED;
            break;
#endif
        default:
            return (CLI_FAILURE);
    }

    if (nmhTestv2Dot3PauseAdminMode (&u4ErrCode, (INT4) u4IfIndex,
                                     i4Pause) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetDot3PauseAdminMode ((INT4) u4IfIndex, i4Pause) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetPortMode                                     */
/*                                                                           */
/*     DESCRIPTION      : This function sets the port mode of the            */
/*                        interface(router port or switch port)              */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface                 */
/*                        i4BridgedIfStat (CFA_DISABLED for Router port and  */
/*                                           )                               */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SUCCESS/FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetPortMode (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4BridgedIfStat)
{
    UINT4               u4ErrCode;
    INT4                i4AdminStatus;
    INT4                i4BridgedIfaceStatus;
    INT4                i4MTU;

#ifdef PPP_WANTED
    if (CfaPppCheckPortBinding (u4IfIndex) == CFA_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%% PPP virtual link is already layered over physical                         interface\r\n");
        return CLI_FAILURE;
    }
#endif
    nmhGetIfMainAdminStatus ((INT4) u4IfIndex, &i4AdminStatus);
    if (i4AdminStatus == CFA_IF_UP)
    {
        CliPrintf (CliHandle, "\r%% Make admin status down for setting");
        CliPrintf (CliHandle, " the Bridged Interface Status\r\n");
        return CLI_FAILURE;
    }

    nmhGetIfMainMtu ((INT4) u4IfIndex, &i4MTU);
    if (i4MTU > CFA_MAX_L3_INTF_ENET_MTU)
    {
        CliPrintf (CliHandle,
                   "\r\n %% Changing this Port as Router Port FAILED!!!\r\n");
        CliPrintf (CliHandle, "\r %% Reset MTU size\r\n");
        return (CLI_FAILURE);
    }

    if (nmhGetIfIvrBridgedIface ((INT4) u4IfIndex, &i4BridgedIfaceStatus) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to get Bridge Interface Status\r\n");
        return CLI_FAILURE;
    }

    /* There is no change in Port mode . So Do nothing */
    if (i4BridgedIfaceStatus == i4BridgedIfStat)
    {
        return CLI_SUCCESS;
    }

    if (nmhTestv2IfIvrBridgedIface
        (&u4ErrCode, (INT4) u4IfIndex, i4BridgedIfStat) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Changing this Port as Router Port FAILED!!!\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetIfIvrBridgedIface (u4IfIndex, i4BridgedIfStat) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Changing this Port as Router Port FAILED!!!\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliSetPortVlanId                                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the  PortVlanId of router port  */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface                 */
/*                        i4PortVlanId - PortVlanId that is to be set        */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
CfaCliSetPortVlanId (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4PortVlanId)
{
    UINT4               u4ErrorCode = 0;

    /* Perform nmhtest with the PortVlanId and  Interface-Index */

    if (nmhTestv2IfIpPortVlanId (&u4ErrorCode, (INT4) u4IfIndex, i4PortVlanId)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Perform nmhset with the PortVlanId  */
    if (nmhSetIfIpPortVlanId ((INT4) u4IfIndex, i4PortVlanId) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliUnSetPortVlanId                              */
/*                                                                           */
/*     DESCRIPTION      : This function deassociates the PortVlanId currently*/
/*                        assigned and adds the default PortVlanId from the  */
/*                        reserved range                                     */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface                 */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
CfaCliUnSetPortVlanId (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IfIpPortVlanId (&u4ErrorCode, (INT4) u4IfIndex, 0) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIfIpPortVlanId ((INT4) u4IfIndex, 0) == SNMP_FAILURE)
    {
        /* Set CLI Fatal Error only when CLI Error is not already set */
        if ((CLI_SUCCESS == CLI_GET_ERR (&u4ErrorCode)) && (u4ErrorCode == 0))
        {
            CLI_FATAL_ERROR (CliHandle);
        }
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetBrgPortType                                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the bridge port type for the    */
/*                        given port.                                        */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        u4IfIndex - Index of the interface                 */
/*                        i4BrgPortType - Bridge port type to be set.        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetBrgPortType (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4BrgPortType)
{
    UINT4               u4ErrorCode = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    if (nmhTestv2IfMainBrgPortType (&u4ErrorCode, (INT4) u4IfIndex,
                                    i4BrgPortType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIfMainBrgPortType ((INT4) u4IfIndex, i4BrgPortType)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (i4BrgPortType == CFA_UPLINK_ACCESS_PORT)
    {
        CliChangePath ("..");
        SPRINTF ((CHR1 *) au1IfName, "%s%d", CLI_UAP_MODE, u4IfIndex);
        CliChangePath ((CHR1 *) au1IfName);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaConnectPorts                                    */
/*                                                                           */
/*     DESCRIPTION      : This function connects the two ports.              */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        u4IfIndex - Index of the ILAN interface            */
/*                        i4Operation - Add/Delete operation                 */
/*                        IfPortList - List of ports to be added or deleted  */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaConnectPorts (tCliHandle CliHandle,
                 UINT4 u4IfIndex, INT1 i4Operation, tPortList IfPortList)
{
    UINT2               u2ByteIndex;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag;
    UINT1               u1IfType;
    UINT4               u4ErrorCode = 0;
    UINT4               u4VirtualIfIndex = 0;

    for (u2ByteIndex = 0; u2ByteIndex < BRG_PORT_INTERNAL_LIST_SIZE;
         u2ByteIndex++)
    {
        u1PortFlag = IfPortList[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < INTERNAL_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & INTERNAL_BIT8) != 0)
            {
                u4VirtualIfIndex =
                    (UINT4) ((u2ByteIndex * INTERNAL_PORTS_PER_BYTE) +
                             u2BitIndex + 1);

                if (nmhValidateIndexInstanceIfMainTable
                    ((INT4) u4VirtualIfIndex) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Invalid member port\r\n");
                    return CLI_FAILURE;
                }

                CfaGetIfType (u4VirtualIfIndex, &u1IfType);

                if ((u1IfType != CFA_BRIDGED_INTERFACE)
                    && (u1IfType != CFA_PIP))
                {
                    CliPrintf (CliHandle, "\r%% Invalid interface type\r\n");
                    return CLI_FAILURE;
                }

                switch (i4Operation)
                {
                    case CFA_ADD_VIRTUAL:
                        /* Stack virtual interface over ILAN interface */
                        if (CfaIfmAddDynamicStackEntry (u4VirtualIfIndex,
                                                        u4IfIndex) ==
                            CFA_FAILURE)
                        {
                            CliPrintf (CliHandle,
                                       "\r%% Cannot add virtual port\r\n");
                            return CLI_FAILURE;
                        }
                        break;

                    case CFA_DEL_VIRTUAL:
                        /* Delete virtual interface stacked over ILAN interface */
                        if ((nmhSetIfStackStatus ((INT4) u4VirtualIfIndex,
                                                  (INT4) u4IfIndex,
                                                  CFA_RS_DESTROY))
                            == SNMP_FAILURE)
                        {
                            CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                                     "Error in CfaConnectPorts\n");
                            return (CFA_FAILURE);
                        }

                        break;

                    default:
                        break;
                }

            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }

    if (i4Operation == CFA_DEL_VIRTUAL)
    {
        return CFA_SUCCESS;
    }

    /* now make the row status of ILAN interface as not-in-service
       before making it as active */
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;

    if (nmhTestv2IfMainRowStatus (&u4ErrorCode, (INT4) u4IfIndex, CFA_RS_ACTIVE)
        == SNMP_SUCCESS)
    {
        if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE) == SNMP_SUCCESS)
        {
            /* All the mandatory parameters are set. */
            return CLI_SUCCESS;
        }
    }
    CliPrintf (CliHandle, "\r%% Error in handling ILAN interface\r\n");
    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaIvrSetIpAddress                                 */
/*                                                                           */
/*     DESCRIPTION      : This function sets the IP address of an IVR        */
/*                         interface                                         */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
/*                        u4IpAddr, u4SubnetMask - IPAddress, Subnet Mask    */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SUCCESS/FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIvrSetIpAddress (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4IpAddr,
                    UINT4 u4IpSubnetMask)
{
    UINT4               u4ErrCode;
    UINT4               u4BcastAddr;
    UINT4               u4PrevIpAddr;
    UINT4               u4PrevSubnetMask;
    INT4                i4AdminStatus = 0;
    INT4                i4Status = 0;
#ifdef VXLAN_WANTED
    INT4                i4IfaceType = 0;
#endif

    if ((CfaIsL3SubIfIndex (u4IfIndex) == CFA_TRUE) && (u4IpAddr != 0))
    {
        /* Ip address configuration to l3Subinterface is allowed only if the 
         * encapsulation type is set , So check the encapsualtion */
        CFA_CDB_GET_ENCAP_STATUS (u4IfIndex, i4Status);

        if (CFA_TRUE != i4Status)
        {
            CliPrintf (CliHandle,
                       "\r%% Configuring IP routing on a LAN subinterface"
                       " is only allowed if that subinterface is already configured"
                       " as part of an IEEE 802.1Q.\r\n");
            return CLI_FAILURE;
        }
    }

    if (L2IwfIsPortInPortChannel (u4IfIndex) == CFA_SUCCESS)
    {
        CLI_SET_ERR (CLI_CFA_PORT_IN_AGG);
        return (CLI_FAILURE);

    }
#ifdef VXLAN_WANTED
    if (nmhGetIfMainType ((INT4) u4IfIndex, &i4IfaceType) == SNMP_SUCCESS)
    {
        if (i4IfaceType == CFA_LOOPBACK)
        {
            if (VxlanIsLoopbackUsed (u4IfIndex) == VXLAN_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r\n%% Deletion/Modification of loopback "
                           "interface is not allowed since it is used by VXLAN module\r\n");
                return CLI_FAILURE;

            }
        }
    }
#endif

    if (nmhTestv2IfIpAddr (&u4ErrCode, (INT4) u4IfIndex, u4IpAddr) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);

    }

    /* Test Ip addr and Sub net mask */
    if (CfaTestIfIpAddrAndIfIpSubnetMask ((INT4) u4IfIndex,
                                          u4IpAddr, u4IpSubnetMask)
        == CLI_FAILURE)
    {
        if (CLI_GET_ERR (&u4ErrCode) != CLI_SUCCESS)
        {
            CLI_SET_ERR (CLI_CFA_INVALID_IPADDR);
        }
        return (CLI_FAILURE);
    }

    nmhGetIfMainAdminStatus ((INT4) u4IfIndex, &i4AdminStatus);
    if (i4AdminStatus == CFA_IF_UP)
    {
        nmhSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_DOWN);
    }

    nmhGetIfIpAddr ((INT4) u4IfIndex, &u4PrevIpAddr);
    if (nmhSetIfIpAddr (u4IfIndex, u4IpAddr) == SNMP_FAILURE)
    {
        if (i4AdminStatus == CFA_IF_UP)
        {
            nmhSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_UP);
        }
        if (CLI_GET_ERR (&u4ErrCode) != CLI_SUCCESS)
        {
            CLI_SET_ERR (CLI_CFA_INVALID_IPADDR);
        }
        return (CLI_FAILURE);
    }

    nmhGetIfIpSubnetMask ((INT4) u4IfIndex, &u4PrevSubnetMask);
    if (nmhSetIfIpSubnetMask (u4IfIndex, u4IpSubnetMask) == SNMP_FAILURE)
    {
        if (i4AdminStatus == CFA_IF_UP)
        {
            nmhSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_UP);
        }
        nmhSetIfIpAddr (u4IfIndex, u4PrevIpAddr);
        CLI_SET_ERR (CLI_CFA_INVALID_SUBNET);
        return (CLI_FAILURE);
    }

    u4BcastAddr = u4IpAddr | (~(u4IpSubnetMask));

    if (nmhTestv2IfIpBroadcastAddr (&u4ErrCode, (INT4) u4IfIndex,
                                    u4BcastAddr) == SNMP_FAILURE)
    {
        if (i4AdminStatus == CFA_IF_UP)
        {
            nmhSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_UP);
        }
        nmhSetIfIpAddr (u4IfIndex, u4PrevIpAddr);
        nmhSetIfIpSubnetMask (u4IfIndex, u4PrevSubnetMask);
        return (CLI_FAILURE);
    }

    if (nmhSetIfIpBroadcastAddr (u4IfIndex, u4BcastAddr) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (i4AdminStatus == CFA_IF_UP)
    {
        nmhSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_UP);
    }
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetOOBSecondaryIpAddress                         */
/*                                                                           */
/*     DESCRIPTION      : This function sets the secondary IP address and    */
/*                        ip mask of  OOB interface of node0 or node1        */
/*                                                                           */
/*     INPUT            : u4IpAddr, u4SubnetMask - IPAddress, Subnet Mask    */
/*                        CliHandle - CLI Handler , u4NodeValue - node id    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SUCCESS/FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetOOBSecondaryIpAddress (tCliHandle CliHandle, UINT4 u4IpAddr,
                             UINT4 u4IpSubnetMask, UINT4 u4NodeValue)
{

    UINT4               u4ErrorCode = 0;

    /* Validate ip address and mask for node0 */
    if (u4NodeValue == CFA_NODE0)
    {
        if (nmhTestv2IfOOBNode0SecondaryIpAddress (&u4ErrorCode, u4IpAddr)
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Invalid Secondary Ip for node0 \r\n");
            return (CLI_FAILURE);
        }
        if (u4IpAddr != 0)
        {
            if (nmhTestv2IfOOBNode0SecondaryIpMask
                (&u4ErrorCode, u4IpSubnetMask) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Invalid Secondary IpMask for node0 \r\n");
                return (CLI_FAILURE);
            }
        }
    }
    /* Validate ip address and mask for node1 */
    if (u4NodeValue == CFA_NODE1)
    {
        if (nmhTestv2IfOOBNode1SecondaryIpAddress (&u4ErrorCode, u4IpAddr)
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Invalid Secondary Ip for node1\r\n");
            return (CLI_FAILURE);
        }
        if (u4IpAddr != 0)
        {
            if (nmhTestv2IfOOBNode1SecondaryIpMask
                (&u4ErrorCode, u4IpSubnetMask) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Invalid Secondary IpMask for node1\r\n");
                return (CLI_FAILURE);
            }
        }
    }

    /* Setting ip address and mask for node0 */
    if (u4NodeValue == CFA_NODE0)
    {
        if (nmhSetIfOOBNode0SecondaryIpAddress (u4IpAddr) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r%% Setting Secondary Ip for node0 failed \r\n");
            return (CLI_FAILURE);
        }
        if (u4IpAddr != 0)
        {
            if (nmhSetIfOOBNode0SecondaryIpMask (u4IpSubnetMask) !=
                SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Setting Secondary IpMask for node0 failed \r\n");
                return (CLI_FAILURE);
            }
        }
    }

    /* Setting ip address and mask for node1 */
    if (u4NodeValue == CFA_NODE1)
    {
        if (nmhSetIfOOBNode1SecondaryIpAddress (u4IpAddr) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r%% Setting Secondary Ip for node1 failed \r\n");
            return (CLI_FAILURE);
        }
        if (u4IpAddr != 0)
        {
            if (nmhSetIfOOBNode1SecondaryIpMask (u4IpSubnetMask) !=
                SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Setting Secondary IpMask for node1 failed \r\n");
                return (CLI_FAILURE);
            }
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaIvrSetAddressAlloc                              */
/*                                                                           */
/*     DESCRIPTION      : This function is used to get IP address thro DHCP  */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface Index                        */
/*                        u1AddrAllocMethod - Manual or Dynamic              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SUccess/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIvrSetAddressAlloc (tCliHandle CliHandle, UINT4 u4IfIndex,
                       UINT1 u1AddrAllocMethod, UINT1 u1AddrAllocProto)
{
    INT4                i4RetValue;
    INT4                i4Result;
    UINT4               u4IpAddr;
    UINT4               u4SecondaryAddr = 0;
    UINT4               u4ErrCode;
    INT4                i4NextIfIndex;
    INT4                i4AdminStatus = 0;
    BOOL1               bAdminStatusFlag = CFA_FALSE;

    /* Get the interface Address allocation method, 
     * if the address allocation method is not dynamic then 
     * only proceed otherwise return
     */
    i4Result = nmhGetIfIpAddrAllocMethod ((INT4) u4IfIndex, &i4RetValue);

    if (i4Result == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (u1AddrAllocMethod == CFA_IP_ALLOC_POOL)
    {
        if (i4RetValue == u1AddrAllocMethod)
        {
            nmhGetIfIpAddr ((INT4) u4IfIndex, &u4IpAddr);
            if (u4IpAddr != 0)
            {
                CliPrintf (CliHandle,
                           "\r%% Already bounded to IP, Please release it first \r\n");
                return CLI_SUCCESS;
            }
        }
        /* First time trying to aquire IP address for the interface
         * so set the address allocation method to ALLOC_POOL 
         */
        nmhGetIfMainAdminStatus ((INT4) u4IfIndex, &i4AdminStatus);
        if (i4AdminStatus == CFA_IF_UP)
        {
            bAdminStatusFlag = CFA_TRUE;
            nmhSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_DOWN);
        }
        i4Result = nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);

        if (i4Result == SNMP_FAILURE)
        {
            nmhGetIfMainAdminStatus ((INT4) u4IfIndex, &i4AdminStatus);
            if (i4AdminStatus == CFA_IF_UP)
            {
                nmhSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_UP);
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        i4Result = nmhTestv2IfIpAddrAllocMethod (&u4ErrCode, (INT4) u4IfIndex,
                                                 CFA_IP_ALLOC_POOL);
        if (i4Result == SNMP_FAILURE)
        {
            nmhGetIfMainAdminStatus ((INT4) u4IfIndex, &i4AdminStatus);
            if (i4AdminStatus == CFA_IF_UP)
            {
                nmhSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_UP);
            }
            nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE);
            return CLI_FAILURE;
        }

        i4Result = nmhSetIfIpAddrAllocMethod (u4IfIndex, CFA_IP_ALLOC_POOL);

        if (i4Result == SNMP_FAILURE)
        {
            nmhGetIfMainAdminStatus ((INT4) u4IfIndex, &i4AdminStatus);
            if (i4AdminStatus == CFA_IF_UP)
            {
                nmhSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_UP);
            }
            nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE);
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        i4Result = nmhTestv2IfIpAddrAllocProtocol (&u4ErrCode, (INT4) u4IfIndex,
                                                   u1AddrAllocProto);
        if (i4Result == SNMP_FAILURE)
        {
            nmhGetIfMainAdminStatus ((INT4) u4IfIndex, &i4AdminStatus);
            if (i4AdminStatus == CFA_IF_UP)
            {
                nmhSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_UP);
            }
            nmhSetIfIpAddrAllocMethod (u4IfIndex, i4RetValue);
            nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE);
            return CLI_FAILURE;
        }

        i4Result = nmhSetIfIpAddrAllocProtocol (u4IfIndex, u1AddrAllocProto);

        if (i4Result == SNMP_FAILURE)
        {
            nmhGetIfMainAdminStatus ((INT4) u4IfIndex, &i4AdminStatus);
            if (i4AdminStatus == CFA_IF_UP)
            {
                nmhSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_UP);
            }
            nmhSetIfIpAddrAllocMethod (u4IfIndex, i4RetValue);
            nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE);
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        i4Result = nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE);

        if (i4Result == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (bAdminStatusFlag == CFA_TRUE)
        {
            nmhGetIfMainAdminStatus ((INT4) u4IfIndex, &i4AdminStatus);
            if (i4AdminStatus == CFA_IF_DOWN)
            {
                nmhSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_UP);
            }
        }
    }
    else if (u1AddrAllocMethod == CFA_IP_ALLOC_MAN)
    {
        /* If the change in allocate mode,set it accordingly */
        if (i4RetValue == CFA_IP_ALLOC_POOL)
        {
            i4Result =
                nmhTestv2IfIpAddrAllocMethod (&u4ErrCode, (INT4) u4IfIndex,
                                              (INT4) CFA_IP_ALLOC_MAN);
            if (i4Result == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            i4Result = nmhSetIfIpAddrAllocMethod (u4IfIndex, CFA_IP_ALLOC_MAN);

            if (i4Result == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            return (CfaIvrSetIpAddress (CliHandle, u4IfIndex, 0, 0));
        }
        else
        {

#ifdef RIP_WANTED
            /*before deleting all the Address
             *end the indication to rip */
            nmhGetIfIpAddr ((INT4) u4IfIndex, &u4IpAddr);
            if (u4IpAddr != 0)
            {
                RipDeleteIfRecord (u4IfIndex, u4IpAddr, CFA_DEL_ALL);
            }
#endif

            /* Mode is manual and there is request for deleting all the 
             * IP addresses.So delete all IP addresses configured on 
             * interface */
            while ((nmhGetNextIndexIfSecondaryIpAddressTable ((INT4) u4IfIndex,
                                                              &i4NextIfIndex,
                                                              u4SecondaryAddr,
                                                              &u4SecondaryAddr)
                    == SNMP_SUCCESS) && ((INT4) u4IfIndex == i4NextIfIndex))
            {
                nmhSetIfSecondaryIpRowStatus (u4IfIndex, u4SecondaryAddr,
                                              CFA_RS_DESTROY);
            }

            /* Reset the Primary address */
            return (CfaIvrSetIpAddress (CliHandle, u4IfIndex, 0, 0));
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r%% Cannot disable DHCP/BOOTP/RARP "
                   "on this interface, should enable DHCP/BOOTP/RARP on "
                   "this interface first\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaIvrSetPeerMacAddress                            */
/*                                                                           */
/*     DESCRIPTION      : This function sets the peer mac address of an IVR  */
/*                        unnumbered interface                                         */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SUCCESS/FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIvrSetPeerMacAddress (tCliHandle CliHandle, UINT4 u4IfIndex,
                         tMacAddr PeerMacAddr)
{
    UINT4               u4ErrCode;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2IfIpDestMacAddress (&u4ErrCode, (INT4) u4IfIndex, PeerMacAddr)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIfIpDestMacAddress (u4IfIndex, PeerMacAddr) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaUnnumIntSetAliasIPInterface                     */
/*                                                                           */
/*     DESCRIPTION      : This function sets the alias IP interface of an    */
/*                        unnumbered interface                               */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the unnumbered interface      */
/*                        CliHandle - CLI Handler                            */
/*                        u4AliasIfIndex: Index of alias interface           */
/*                        u4IfType - type of alias interface                 */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SUCCESS/FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaUnnumIntSetAssociatedIPInterface (tCliHandle CliHandle, UINT4 u4IfIndex,
                                     UINT4 u4AliasIfIndex)
{
    UINT4               u4ErrorCode = CFA_NONE;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2IfIpUnnumAssocIPIf
        (&u4ErrorCode, (INT4) u4IfIndex, u4AliasIfIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetIfIpUnnumAssocIPIf (u4IfIndex, u4AliasIfIndex);

    return (CLI_SUCCESS);

}

/*****************************************************************************
*                                                                           
*     FUNCTION NAME    : CfaUnnumIntResetAliasIPInterface                   
*                                                                           
*     DESCRIPTION      : This function Resets the alias IP interface of an  
*                        unnumbered interface                               
*                                                                           
*     INPUT            : u4IfIndex - Index of the interface for which       
*                        alias ip should be deleted                       
*                        CliHandle - CLI Handler                            
*                                                                           
*     OUTPUT           : None                                               
*                                                                           
*     RETURNS          : SUCCESS/FAILURE                                    
*                                                                           
*****************************************************************************/
INT4
CfaUnnumIntResetAssociatedIPInterface (tCliHandle CliHandle, UINT4 u4IfIndex,
                                       tMacAddr PeermacAddr, UINT4 u4AssocAddr)
{
    tMacAddr            SetValIfIpDestMacAddress = { 0 };
    UINT1               au1PeermacTemp[CFA_ENET_ADDR_LEN] = { 0 };

    UNUSED_PARAM (CliHandle);
    if (u4AssocAddr != CFA_INVALID_INDEX)
    {
        nmhSetIfIpUnnumAssocIPIf (u4IfIndex, 0);
    }

    CfaGetIfUnnumPeerMac (u4IfIndex, au1PeermacTemp);
    if (MEMCMP (PeermacAddr, au1PeermacTemp, CFA_ENET_ADDR_LEN) == 0)
    {
        nmhSetIfIpDestMacAddress (u4IfIndex, SetValIfIpDestMacAddress);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetSecurityBridgeMode                           */
/*                                                                           */
/*     DESCRIPTION      : This function Enables or Disables Security for     */
/*                        Bridged Traffic                                    */
/*                                                                           */
/*     INPUT            : u4SecStatus - Status for Security Processing of    */
/*                        Bridged Traffic                                    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetSecurityBridgeMode (tCliHandle CliHandle, INT4 i4SecStatus)
{

    INT4                i4RetValue = SNMP_FAILURE;
    UINT4               u4ErrCode;
    i4RetValue = nmhTestv2IfSecurityBridging (&u4ErrCode, i4SecStatus);
    if (i4RetValue == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "Error in Changing Security Status for Bridged Traffic \n");
        return CLI_FAILURE;
    }

    i4RetValue = nmhSetIfSecurityBridging (i4SecStatus);
    if (i4RetValue == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "Error in Changing Security Status for Bridged Traffic \n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetSecVlanList                                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the VLAN list for the mgmt      */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : pu1IvrVlanList - VLAN list                         */
/*                        u1Flag         - Flag to Set or Reset Vlan list    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SUccess/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetSecVlanList (tCliHandle CliHandle, UINT1 *pu1IvrSetVlanList, UINT1 u1Flag)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE SecVlanList;

    SecVlanList.i4_Length = CFA_SEC_VLAN_LIST_SIZE;
    SecVlanList.pu1_OctetList = pu1IvrSetVlanList;
    if (u1Flag == CLI_ENABLE)
    {
        if (nmhTestv2IfSetSecVlanList (&u4ErrorCode, &SecVlanList)
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "Security for Bridged Traffic is Disabled \r\n");
            CliPrintf (CliHandle,
                       "Enable it to configure Security VLAN List \r\n");
            return (CLI_FAILURE);
        }

        if (nmhSetIfSetSecVlanList (&SecVlanList) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    else
    {
        if (nmhTestv2IfResetSecVlanList (&u4ErrorCode, &SecVlanList) !=
            SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIfResetSecVlanList (&SecVlanList) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaReSetSecIntfVlan                                */
/*                                                                           */
/*     DESCRIPTION      : This function re-sets the VLAN list for the mgmt   */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
CfaReSetSecIntfVlan (tCliHandle CliHandle)
{
    if (nmhSetIfSecIvrIfIndex (CFA_INVALID_INDEX) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);

    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetSecIntfVlan                                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the VLAN list for the mgmt      */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : u4IfIndex - IfIndex of L3 Intf                     */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SUccess/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetSecIntfVlan (tCliHandle CliHandle, UINT4 u4IfIndex)
{

    UINT4               u4ErrorCode;

    /* Fetch the L3 VLAN interface index, corresponding to the L2 VLAN ID. */
    u4IfIndex = CfaGetVlanInterfaceIndex ((tVlanIfaceVlanId) u4IfIndex);

    if (nmhTestv2IfSecIvrIfIndex (&u4ErrorCode, (INT4) u4IfIndex) !=
        SNMP_SUCCESS)
    {

        CliPrintf (CliHandle, "Invalid L3 Interface \n");
        return (CLI_FAILURE);
    }

    if (nmhSetIfSecIvrIfIndex (u4IfIndex) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);

    }
    return CLI_SUCCESS;
}

#ifdef WGS_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetMgmtVlanList                                 */
/*                                                                           */
/*     DESCRIPTION      : This function sets the VLAN list for the mgmt      */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : pu1IvrVlanList - VLAN list to be set fot the given */
/*                                       interface.                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SUccess/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetMgmtVlanList (tCliHandle CliHandle, UINT1 *pu1IvrSetVlanList,
                    UINT1 u1Flag)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE MgmtVlanList;

    MgmtVlanList.i4_Length = CFA_MGMT_VLAN_LIST_SIZE;
    MgmtVlanList.pu1_OctetList = pu1IvrSetVlanList;

    if (u1Flag == CLI_ENABLE)
    {

        if (nmhTestv2IfSetMgmtVlanList (&u4ErrorCode, &MgmtVlanList)
            != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIfSetMgmtVlanList (&MgmtVlanList) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    else
    {
        if (nmhTestv2IfResetMgmtVlanList (&u4ErrorCode, &MgmtVlanList) !=
            SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIfResetMgmtVlanList (&MgmtVlanList) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    return (CLI_SUCCESS);
}
#endif

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaIvrMapCliUpdate                                 */
/*                                                                           */
/*     DESCRIPTION      : This function sets the VLAN list for the mgmt      */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : pu1VlanList - VLAN list to be set fot the given    */
/*                                       interface.                          */
/*                        CliHandle  - CLI handle                            */
/*                        u4IfIndex  - interface index                       */
/*                        u1Flag     - specifies operation type              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIvrMapCliUpdate (tCliHandle CliHandle, UINT4 u4IfIndex,
                    UINT1 *pu1VlanList, UINT1 u1Flag)
{
    INT4                i4NextIfIndex = 0;
    INT4                i4NextVlan = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4PrevVlan = 0;
    INT4                i4RetVal = 0;
    BOOL1               bResult = OSIX_FALSE;

    UNUSED_PARAM (CliHandle);

    switch (u1Flag)
    {
        case CLI_CFA_ADD_MAP_VLANS:
        case CLI_CFA_REM_MAP_VLANS:
        {
            if (CfaIvrMapCliAddRemove (u4IfIndex, pu1VlanList, u1Flag) ==
                CLI_FAILURE)
            {
                return CLI_FAILURE;
            }

            break;
        }
        case CLI_CFA_OVERWRITE_MAP_VLANS:
        {
            MEMSET (gCfaVlanList, 0, CFA_MGMT_VLAN_LIST_SIZE);

            /* Create the VLAN addlist and dellist by scanning
             * the IVR mapping table entries.
             */
            i4PrevIfIndex = (INT4) u4IfIndex;
            i4PrevVlan = 0;

            i4RetVal = nmhGetNextIndexIfIvrMappingTable (i4PrevIfIndex,
                                                         &i4NextIfIndex,
                                                         i4PrevVlan,
                                                         &i4NextVlan);

            while (i4RetVal != SNMP_FAILURE)
            {
                if (u4IfIndex != (UINT4) i4NextIfIndex)
                {
                    break;
                }

                OSIX_BITLIST_IS_BIT_SET (pu1VlanList, i4NextVlan,
                                         VLAN_LIST_SIZE, bResult);

                if (bResult == OSIX_TRUE)
                {
                    /* reset the input vlan bit list for this already 
                     * existing vlan
                     */
                    OSIX_BITLIST_RESET_BIT (pu1VlanList, (UINT2) i4NextVlan,
                                            CFA_MGMT_VLAN_LIST_SIZE);
                }
                else
                {
                    /* add the vlan into the vlan delete list         
                     */

                    OSIX_BITLIST_SET_BIT (gCfaVlanList, i4NextVlan,
                                          CFA_MGMT_VLAN_LIST_SIZE);
                }

                i4PrevIfIndex = i4NextIfIndex;
                i4PrevVlan = i4NextVlan;

                i4RetVal = nmhGetNextIndexIfIvrMappingTable (i4PrevIfIndex,
                                                             &i4NextIfIndex,
                                                             i4PrevVlan,
                                                             &i4NextVlan);

            }                    /* end of while */

            if (CfaIvrMapCliOverwrite (u4IfIndex, pu1VlanList, gCfaVlanList,
                                       CFA_IVR_MAP_TEST) == CFA_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (CfaIvrMapCliOverwrite (u4IfIndex, pu1VlanList, gCfaVlanList,
                                       CFA_IVR_MAP_SET) == CFA_FAILURE)
            {
                return CLI_FAILURE;
            }

            break;
        }
        case CLI_CFA_PURGE_ALL_MAPPED_VLANS:
        {
            if (CfaIvrMapDeleteMappedVlans (u4IfIndex) == CFA_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
        default:
            break;
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaIvrMapCliAddRemove                                   */
/*                                                                           */
/*     DESCRIPTION      : This function adds/removes the IVR mapping entries */
/*                        into the IVR mapping table                         */
/*                                                                           */
/*     INPUT            : pu1VlanList - VLAN list to be set fot the given    */
/*                                       interface.                          */
/*                        u4IfIndex - interface index                        */
/*                        u1Flag    - flag for add / remove                  */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SUccess/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
CfaIvrMapCliAddRemove (UINT4 u4IfIndex, UINT1 *pu1VlanList, UINT1 u1Flag)
{
    UINT4               u4ErrorCode;
    UINT4               u4ByteInd;
    BOOL1               bResult = OSIX_FALSE;

    for (u4ByteInd = VLAN_MIN_VLAN_ID; u4ByteInd <= VLAN_MAX_VLAN_ID;
         u4ByteInd++)
    {
        OSIX_BITLIST_IS_BIT_SET (pu1VlanList, u4ByteInd, VLAN_LIST_SIZE,
                                 bResult);

        if (bResult == OSIX_TRUE)
        {
            if (u1Flag == CLI_CFA_REM_MAP_VLANS)
            {
                if (nmhTestv2IfIvrMappingRowStatus
                    (&u4ErrorCode, (INT4) u4IfIndex, (INT4) u4ByteInd,
                     (INT4) DESTROY) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
            else if (u1Flag == CLI_CFA_ADD_MAP_VLANS)
            {
                if (nmhTestv2IfIvrMappingRowStatus
                    (&u4ErrorCode, (INT4) u4IfIndex, (INT4) u4ByteInd,
                     (INT4) CREATE_AND_GO) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
        }
    }

    for (u4ByteInd = VLAN_MIN_VLAN_ID; u4ByteInd <= VLAN_MAX_VLAN_ID;
         u4ByteInd++)
    {
        OSIX_BITLIST_IS_BIT_SET (pu1VlanList, u4ByteInd, VLAN_LIST_SIZE,
                                 bResult);

        if (bResult == OSIX_TRUE)
        {
            if (u1Flag == CLI_CFA_REM_MAP_VLANS)
            {
                if (nmhSetIfIvrMappingRowStatus
                    ((INT4) u4IfIndex, (INT4) u4ByteInd,
                     (INT4) DESTROY) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
            else if (u1Flag == CLI_CFA_ADD_MAP_VLANS)
            {
                if (nmhSetIfIvrMappingRowStatus
                    ((INT4) u4IfIndex, (INT4) u4ByteInd,
                     (INT4) CREATE_AND_GO) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaIvrMapCliOverwrite                              */
/*                                                                           */
/*     DESCRIPTION      : This function carries on the set/test test routines*/
/*                        for the vlan present in the addlist and dellist    */
/*                                                                           */
/*     INPUT            : pu1AddList - VLAN list to be set fot the given     */
/*                                       interface.                          */
/*                        u4IfIndex - interface index                        */
/*                        pu1DelList - VLAN list to be used for VLAN deletion*/
/*                        u1Flag - Flag to indicates set/test operation      */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
CfaIvrMapCliOverwrite (UINT4 u4IfIndex, UINT1 *pu1AddList,
                       UINT1 *pu1DelList, UINT1 u1Flag)
{
    UINT4               u4ErrorCode;
    UINT4               u4ByteInd;
    BOOL1               bResult = OSIX_FALSE;

    for (u4ByteInd = VLAN_MIN_VLAN_ID; u4ByteInd <= VLAN_MAX_VLAN_ID;
         u4ByteInd++)
    {
        OSIX_BITLIST_IS_BIT_SET (pu1DelList, u4ByteInd, VLAN_LIST_SIZE,
                                 bResult);

        if (bResult == OSIX_TRUE)
        {
            if (u1Flag == CFA_IVR_MAP_SET)
            {
                if (nmhSetIfIvrMappingRowStatus ((INT4) u4IfIndex,
                                                 (INT4) u4ByteInd,
                                                 (INT4) DESTROY) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
            else if (u1Flag == CFA_IVR_MAP_TEST)
            {
                if (nmhTestv2IfIvrMappingRowStatus
                    (&u4ErrorCode, (INT4) u4IfIndex, (INT4) u4ByteInd,
                     (INT4) DESTROY) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
        }

        bResult = OSIX_FALSE;

        OSIX_BITLIST_IS_BIT_SET (pu1AddList, u4ByteInd, VLAN_LIST_SIZE,
                                 bResult);

        if (bResult == OSIX_TRUE)
        {
            if (u1Flag == CFA_IVR_MAP_SET)
            {
                if (nmhSetIfIvrMappingRowStatus
                    ((INT4) u4IfIndex, (INT4) u4ByteInd,
                     (INT4) CREATE_AND_GO) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
            else if (u1Flag == CFA_IVR_MAP_TEST)
            {
                if (nmhTestv2IfIvrMappingRowStatus
                    (&u4ErrorCode, (INT4) u4IfIndex, (INT4) u4ByteInd,
                     (INT4) CFA_RS_CREATEANDGO) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
        }

    }                            /* end of for loop */

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaLinkUpDelayStatusOnPort                         */
/*                                                                           */
/*     DESCRIPTION      : This enables or disables Link Up Delay             */
/*                        functionality in this port. A value of             */
/*                        'enabled'(1) indicates  that, operational          */
/*                        status of the link is suspended                    */
/*                        for a configured delay time. A value of            */
/*                        'disabled' (2) indicates  that  the operational    */
/*                        status of the link is not delayed and indicated    */
/*                        to the higher modules immediately.                 */
/*                                                                           */
/*                                                                           */
/*     INPUT            : CliHandle   - CLI handle                           */
/*                        i4NextIndex - interface index                      */
/*                        u4Status    - Enable/Disable                       */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaLinkUpDelayStatusOnPort (tCliHandle CliHandle,
                            UINT4 u4IfaceIndex, UINT4 u4Status)
{
    UINT4               u4ErrCode;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2IfMainExtLinkUpEnabledStatus (&u4ErrCode, (INT4) u4IfaceIndex,
                                               (INT4) u4Status) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIfMainExtLinkUpEnabledStatus ((INT4) u4IfaceIndex,
                                            (INT4) u4Status) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaLinkUpDelaySetTimerOnPort                       */
/*                                                                           */
/*     DESCRIPTION      : The link up delay timer configures the             */
/*                        delay timer for the link up enabled interface.     */
/*                        It takes the timer minimum value is 1 and          */
/*                        maximum value is 1000                              */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI handle                            */
/*                        i4NextIndex - interface index                      */
/*                        u4Timer     - Timer Value                          */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaLinkUpDelaySetTimerOnPort (tCliHandle CliHandle,
                              UINT4 u4IfaceIndex, UINT4 u4Timer)
{
    UINT4               u4ErrCode;
    UNUSED_PARAM (CliHandle);
    if (nmhTestv2IfMainExtLinkUpDelayTimer (&u4ErrCode, (INT4) u4IfaceIndex,
                                            (UINT4) u4Timer) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIfMainExtLinkUpDelayTimer ((INT4) u4IfaceIndex,
                                         u4Timer) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliSetLinkUpDelayOnSystem                       */
/*                                                                           */
/*     DESCRIPTION      : This enables or disables Link Up Delay             */
/*                        functionality in this system .A value of           */
/*                        'enabled'(1) indicates  that, operational          */
/*                        status of the link is suspended for a              */
/*                        configured delay time. A value of 'disabled'       */
/*                        (2) indicates  that  the operational status of     */
/*                        the link is not delayed and indicated to the       */
/*                        higher modules immediately.                        */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI handle                            */
/*                        u4Status    - Enable/Disable                       */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCliSetLinkUpDelayOnSystem (tCliHandle CliHandle, UINT4 u4Status)
{
    UINT4               u4ErrCode;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2IfLinkUpEnabledStatus (&u4ErrCode,
                                        (INT4) u4Status) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIfLinkUpEnabledStatus ((INT4) u4Status) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliShowLinkUpDelay                              */
/*                                                                           */
/*     DESCRIPTION      : This function display the link up delay table      */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI handle                            */
/*                        i4NextIndex - interface index                      */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCliShowLinkUpDelay (tCliHandle CliHandle, INT4 i4NextIndex)
{
    INT4                u4Continue = CFA_FALSE;
    INT4                i4PrevIndex = 0;
    INT1                ai1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    INT4                i4Index = 0;
    INT4                i4LinkUpEnabledStatus = 0;
    INT4                i4LinkUpEnabledPortStatus = 0;
    UINT4               u4LinkUpDelayTimer = 0;
    UINT4               u4LinkUpRemainingTime = 0;
    INT4                i4Flag = CFA_FALSE;
    INT4                i4Type = CFA_FAILURE;
    UINT1               u1BridgedIfaceStatus;

    if (i4NextIndex == 0)
    {
        if (nmhGetFirstIndexIfTable (&i4NextIndex) == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
        u4Continue = CFA_TRUE;
    }
    i4Index = i4NextIndex;
    if (nmhValidateIndexInstanceIfTable (i4NextIndex) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    do
    {
        if ((i4Index != 0) && (i4Index != i4NextIndex))
        {
            break;
        }
        i4Flag = CFA_FALSE;
        CfaGetIfBridgedIfaceStatus ((UINT4) i4NextIndex, &u1BridgedIfaceStatus);
        i4Type = CfaIsL3IpVlanInterface ((UINT4) i4NextIndex);
        if ((u1BridgedIfaceStatus == CFA_DISABLED) || (i4Type == CFA_SUCCESS)
            || (i4NextIndex > CFA_PHYS_NUM ()))
        {
            i4Flag = CFA_TRUE;
        }
        if (i4Flag == CFA_FALSE)
        {
            MEMSET (ai1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
            CfaCliGetIfName ((UINT4) i4NextIndex, ai1IfName);
            CliPrintf (CliHandle, " LinkUp Delay Table \n");
            CliPrintf (CliHandle, " --------------------\n");
            CliPrintf (CliHandle,
                       " Interface Id                           : %-6s\r\n",
                       ai1IfName);
            nmhGetIfLinkUpEnabledStatus (&i4LinkUpEnabledStatus);
            if (i4LinkUpEnabledStatus == CFA_LINKUP_DELAY_ENABLED)
            {
                CliPrintf (CliHandle,
                           " Link Up Delay System Status            : ENABLED\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           " Link Up Delay System Status            : DISABLED\r\n");
            }
            nmhGetIfMainExtLinkUpEnabledStatus (i4NextIndex,
                                                &i4LinkUpEnabledPortStatus);
            if (i4LinkUpEnabledPortStatus == CFA_LINKUP_DELAY_ENABLED)
            {
                CliPrintf (CliHandle,
                           " Link Up Delay Port Status              : ENABLED\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           " Link Up Delay Port Status              : DISABLED\r\n");
            }
            nmhGetIfMainExtLinkUpDelayTimer (i4NextIndex, &u4LinkUpDelayTimer);
            CliPrintf (CliHandle,
                       " Link Up Delay Port Time                : %d Seconds\r\n",
                       u4LinkUpDelayTimer);
            nmhGetIfMainExtLinkUpRemainingTime (i4NextIndex,
                                                &u4LinkUpRemainingTime);
            CliPrintf (CliHandle,
                       " Link Up Delay Remaining Time           : %d Seconds\r\n",
                       u4LinkUpRemainingTime);

            CliPrintf (CliHandle, "\r\n");
        }
        i4PrevIndex = i4NextIndex;

        if (nmhGetNextIndexIfTable (i4PrevIndex, &i4NextIndex) == SNMP_FAILURE)
        {
            u4Continue = CFA_FALSE;
        }

    }
    while ((u4Continue == CFA_TRUE)
           && (nmhGetNextIndexIfTable (i4PrevIndex, &i4Index) == SNMP_SUCCESS));

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowInterfaceMtu                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays the MTU of the interfaces   */
/*                                                                           */
/*     INPUT            : i4NextIndex - Interface Index                      */
/*                        pu4IfId - Interface Id                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowInterfaceMtu (tCliHandle CliHandle, INT4 i4NextIndex, UINT1 *pu1Alias)
{
    INT4                i4RetVal = 0;
    INT4                i4PrevIndex;

    if (i4NextIndex != 0)
    {
        if (nmhValidateIndexInstanceIfTable (i4NextIndex) == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
        if (CfaShowInterfaceMtuInfo (CliHandle, i4NextIndex) == CLI_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        return CLI_FAILURE;
    }

    if (pu1Alias != NULL)
    {
        i4RetVal =
            (INT4) CfaCliGetFirstIfIndexFromName (pu1Alias,
                                                  (UINT4 *) &i4NextIndex);
        if (i4RetVal == OSIX_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid Vlan Interface  \r\n");
        }
        while (i4RetVal != OSIX_FAILURE)
        {
            if (CfaShowInterfaceMtuInfo (CliHandle, i4NextIndex) == CLI_FAILURE)
            {
                return CLI_FAILURE;
            }
            i4PrevIndex = i4NextIndex;
            i4RetVal =
                (INT4) CfaCliGetNextIfIndexFromName (pu1Alias,
                                                     (UINT4) i4PrevIndex,
                                                     (UINT4 *) &i4NextIndex);
        }
        return CLI_SUCCESS;
    }

    i4RetVal = nmhGetFirstIndexIfTable (&i4NextIndex);

    while (i4RetVal != SNMP_FAILURE)
    {
        if (CfaShowInterfaceMtuInfo (CliHandle, i4NextIndex) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
        i4PrevIndex = i4NextIndex;
        i4RetVal = nmhGetNextIndexIfTable (i4PrevIndex, &i4NextIndex);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowInterfaceMtuInfo                            */
/*                                                                           */
/*     DESCRIPTION      : This function displays the MTU of the interface    */
/*                                                                           */
/*     INPUT            : i4IfIndex   - Interface Index                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowInterfaceMtuInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    INT4                i4Quit = CLI_SUCCESS;
    INT4                i4RetValue = 0;
    INT1               *piIfName;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               u1IfType = 0;
    UINT1               u1BrgPortType = 0;

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    piIfName = (INT1 *) (&au1IfName[0]);

    CfaGetIfType ((UINT4) i4IfIndex, &u1IfType);
    /* MTU Can neither be set or retrieved for S-Channel Interface */
    CfaGetIfBrgPortType ((UINT4) i4IfIndex, &u1BrgPortType);

    if ((CfaIsILanInterface ((UINT4) i4IfIndex) == CFA_FALSE) &&
        (CfaIsIfEntryProgrammingAllowed ((UINT4) i4IfIndex) == CFA_TRUE) &&
        ((u1IfType != CFA_MPLS_TUNNEL) && (u1IfType != CFA_MPLS)) &&
        (u1BrgPortType != CFA_STATION_FACING_BRIDGE_PORT))
    {
        nmhGetIfMtu (i4IfIndex, &i4RetValue);
        MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
        CfaCliGetIfName ((UINT4) i4IfIndex, piIfName);
        CliPrintf (CliHandle, "\r\n%-12s", piIfName);
        i4Quit = CliPrintf (CliHandle, "MTU size is %d\r\n", i4RetValue);

    }
    return i4Quit;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowInterfaceBrgPortType                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays the bridge port type of the */
/*                        interfaces.                                        */
/*                                                                           */
/*     INPUT            : i4NextIndex - Interface Index                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS/ CFA_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowInterfaceBrgPortType (tCliHandle CliHandle, INT4 i4NextIndex)
{
    INT4                i4PrevIndex;
    INT4                u4Continue = CFA_TRUE;
    INT4                i4IfaceType;
    INT4                i4Quit;
    INT1                ai1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1IfBrgPortType[CFA_MAX_BRG_PORT_TYPE_LEN];
    INT4                i4BridgedIfaceStatus = 0;

    if (i4NextIndex == 0)
    {
        if (nmhGetFirstIndexIfTable (&i4NextIndex) == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
    }
    else
    {
        if (nmhValidateIndexInstanceIfTable (i4NextIndex) == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }

        u4Continue = CFA_FALSE;
    }

    do
    {
        nmhGetIfType (i4NextIndex, &i4IfaceType);

        if ((i4IfaceType == CFA_ENET) || (i4IfaceType == CFA_LAGG)
            || (i4IfaceType == CFA_BRIDGED_INTERFACE)
            || (i4IfaceType == CFA_PIP)
            || (i4IfaceType == CFA_PROP_VIRTUAL_INTERFACE)
            || (i4IfaceType == CFA_PSEUDO_WIRE)
            || (i4IfaceType == CFA_VXLAN_NVE) || (i4IfaceType == CFA_TAP))
        {
            nmhGetIfIvrBridgedIface (i4NextIndex, &i4BridgedIfaceStatus);
            if (i4BridgedIfaceStatus == CFA_ENABLED)
            {
                MEMSET (ai1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
                CfaCliGetIfName ((UINT4) i4NextIndex, ai1IfName);
                CliPrintf (CliHandle, "\r\n%-8s", ai1IfName);

                MEMSET (au1IfBrgPortType, 0, CFA_MAX_BRG_PORT_TYPE_LEN);

                CfaGetInterfaceBrgPortTypeString (i4NextIndex,
                                                  au1IfBrgPortType);

                i4Quit = CliPrintf (CliHandle, "Bridge port type is %s\r\n",
                                    au1IfBrgPortType);

                if (i4Quit == CLI_FAILURE)
                {
                    u4Continue = CFA_FALSE;
                }
            }
        }

        i4PrevIndex = i4NextIndex;

        if (nmhGetNextIndexIfTable (i4PrevIndex, &i4NextIndex) == SNMP_FAILURE)
        {
            u4Continue = CFA_FALSE;
        }
    }
    while (u4Continue == CFA_TRUE);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowInterfaces                                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays the interface information   */
/*                                                                           */
/*     INPUT            : i4NextIndex - Index of the interface               */
/*                        CliHandle, CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowInterfaces (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Command,
                   UINT1 *pu1IfAlias)
{

    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4PrevIndex = 0;
    INT4                i4NextIfIndex, i4NextVlan;
    INT4                i4PrevIfIndex, i4PrevVlan;
    INT4                i4RowStatus = 0;

    switch (u4Command)
    {
        case CFA_DESCRIPTION:
            CliPrintf (CliHandle, "\r\n%-13s%-9s%-10s%s\r\n",
                       "Interface", "Status", "Protocol", "Description");
            CliPrintf (CliHandle, "%-13s%-9s%-10s%s\r\n",
                       "---------", "------", "--------", "-----------");
            break;

        case CFA_STATUS:
            CliPrintf (CliHandle,
                       "\r\n%-12s%-17s%-9s%-12s%-16s",
                       "Port", "Status", "Duplex", "Speed", "Negotiation");
#ifdef NPAPI_WANTED
            CliPrintf (CliHandle, "%-13s", "Capability");
#endif
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle,
                       "%-12s%-17s%-9s%-12s%-16s",
                       "----", "------", "------", "-----", "-----------");
#ifdef NPAPI_WANTED
            CliPrintf (CliHandle, "%-13s", "----------");
#endif
            CliPrintf (CliHandle, "\r\n");
            break;

        case CFA_FLOWCONTROL:
            CliPrintf (CliHandle,
                       "\r\n%-8s%-9s%-9s%-11s%-11s%-15s%-11s\r\n",
                       "Port", "Admin", "Oper",
                       "Tx Pause", "Rx Pause", "HC TxPause", "HC RxPause");

            CliPrintf (CliHandle, "%-8s%-9s%-9s\r\n", "", "Tx   Rx", "Tx   Rx");

            CliPrintf (CliHandle, "%-8s%-9s%-9s%-11s%-11s%-15s%-11s\r\n",
                       "----", "-------", "-------",
                       "--------", "--------", "----------", "----------");
            break;

        case CFA_PORT_SEC_STATE:

            CliPrintf (CliHandle, "\r\n%-10s%-10s\r\n", "Interface",
                       "Port-Security-State");
            CliPrintf (CliHandle, "%-10s%-10s\r\n", "---------",
                       "-------------------");
            break;

        case CFA_SHOW_IVR_MAPPING_INFO:
            CliPrintf (CliHandle, "\r\n%-13s%-15s\r\n",
                       "Interface", "Secondary VLAN");
            CliPrintf (CliHandle, "%-13s%-15s\r\n",
                       "---------", "--------------");
            break;
#ifdef PPP_WANTED
        case CFA_PPP_CONFIG:
            i4RetVal = CfaCliPppShowInterfaceConfig (CliHandle, i4Index);
            break;
#endif /* PPP_WANTED */
        default:
            break;
    }

    if (i4Index != 0)
    {
        if (CfaShowIface (CliHandle, i4Index, u4Command) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

    if (u4Command == CFA_SHOW_IVR_MAPPING_INFO)
    {
        i4RetVal = nmhGetFirstIndexIfIvrMappingTable (&i4NextIfIndex,
                                                      &i4NextVlan);

        while (i4RetVal != SNMP_FAILURE)
        {
            CFA_DS_LOCK ();

            CliPrintf (CliHandle, "%-13s%-15d\r\n",
                       CFA_CDB_IF_ALIAS_NAME ((UINT4) i4NextIfIndex),
                       i4NextVlan);
            CFA_DS_UNLOCK ();
            i4PrevIfIndex = i4NextIfIndex;
            i4PrevVlan = i4NextVlan;

            i4RetVal = nmhGetNextIndexIfIvrMappingTable (i4PrevIfIndex,
                                                         &i4NextIfIndex,
                                                         i4PrevVlan,
                                                         &i4NextVlan);
        }

        return CLI_SUCCESS;
    }

    if (pu1IfAlias != NULL)
    {
        i4RetVal =
            (INT4) CfaCliGetFirstIfIndexFromName (pu1IfAlias,
                                                  (UINT4 *) &i4Index);
        if (i4RetVal == OSIX_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid interface type\r\n");
        }

        while (i4RetVal == CLI_SUCCESS)
        {
            if (CfaShowIface (CliHandle, i4Index, u4Command) == CLI_FAILURE)
            {
                return CLI_FAILURE;
            }
            i4PrevIndex = i4Index;
            i4RetVal = (INT4) CfaCliGetNextIfIndexFromName (pu1IfAlias,
                                                            (UINT4) i4PrevIndex,
                                                            (UINT4 *) &i4Index);
        }
        return CLI_SUCCESS;
    }

    i4RetVal = nmhGetFirstIndexIfTable (&i4Index);
    while (i4RetVal == SNMP_SUCCESS)
    {
        nmhGetIfMainRowStatus (i4Index, &i4RowStatus);
        if (i4RowStatus == CFA_RS_ACTIVE)
        {
            if (CfaShowIface (CliHandle, i4Index, u4Command) == CLI_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
        i4PrevIndex = i4Index;
        i4RetVal = nmhGetNextIndexIfTable (i4PrevIndex, &i4Index);
    }
    if (gu4UfdFlag == CFA_TRUE)
    {
        CliPrintf (CliHandle, "\r*Interface is in UFD Error Disabled mode\n");
        gu4UfdFlag = CFA_FALSE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowIface                                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays the interface information   */
/*                                                                           */
/*     INPUT            : i4Index - Index of the interface                   */
/*                        CliHandle, CLI Handler                             */
/*                        u4Command - command type                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT1
CfaShowIface (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Command)
{
    INT4                i4IfaceType = 0;
    INT1                i1RetStatus = 0;

    nmhGetIfType (i4Index, &i4IfaceType);
    /* Interfaces of type CFA_OTHER should not be displayed in Show commands */
    /* At present Connecting Interfaces used in Dual Unit Stacking are
       created with this type */
    if (i4IfaceType == CFA_OTHER)
    {
        return CFA_SUCCESS;
    }
    switch (u4Command)
    {

        case 0:
            /* Details of physical and logical interfaces
             * need to be displayed */
            i1RetStatus = (INT1) CfaShowInterfacesAll (CliHandle, i4Index);

            break;
        case CFA_DESCRIPTION:
            /* Details of physical and logical interfaces
             * need to be displayed */
            i1RetStatus = (INT1) CfaShowInterfacesDesc (CliHandle, i4Index);
            break;

#ifdef ISS_WANTED
        case CFA_STORMCONTROL:
            /* Only details of physical interfaces
             * need to be displayed */
            if ((i4IfaceType != CFA_ENET) && (i4IfaceType != CFA_LAGG))
            {
                break;
            }
            i1RetStatus = (INT1) CfaShowInterfacesStorm (CliHandle, i4Index);
            break;

        case CFA_RATELIMIT:
            /* Only details of physical interfaces
             * need to be displayed */
            if ((i4IfaceType != CFA_ENET) && (i4IfaceType != CFA_LAGG))
            {
                break;
            }
            i1RetStatus =
                (INT1) CfaShowInterfacesRateLimit (CliHandle, i4Index);
            break;

#endif /*ISS_WANTED */

        case CFA_CAPABILITIES:
            /* Only details of physical interfaces
             * need to be displayed */
            if (i4IfaceType != CFA_ENET)
            {
                break;
            }
            i1RetStatus =
                (INT1) CfaShowInterfacesCapabilities (CliHandle, i4Index);
            break;

        case CFA_STATUS:
            /* Only details of physical interfaces
             * need to be displayed */
            if (i4IfaceType != CFA_ENET)
            {
                break;
            }

            i1RetStatus = (INT1) CfaShowInterfacesStatus (CliHandle, i4Index);
            break;

        case CFA_FLOWCONTROL:
            /* Only details of physical interfaces
             * need to be displayed */
            if (i4IfaceType != CFA_ENET)
            {
                break;
            }

            i1RetStatus = (INT1) CfaShowInterfacesFlow (CliHandle, i4Index);
            break;

        case CFA_PORT_SEC_STATE:

            i1RetStatus = (INT1) CfaCliShowPortSecState (CliHandle, i4Index);
            break;

        default:
            break;
    }

    return i1RetStatus;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowInterfacesAll                               */
/*                                                                           */
/*     DESCRIPTION      : This function displays the interface description   */
/*                                                                           */
/*     INPUT            : i4Index - Index of the interface                   */
/*                        CliHandle, CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
CfaShowInterfacesAll (tCliHandle CliHandle, INT4 i4Index)
{
    tSNMP_OCTET_STRING_TYPE IfPhysAddress;
    tStackInfoStruct   *pScanNode = NULL;

    INT4                i4AdminStat = 0;
    INT4                i4OperStat = 0;
    INT4                i4IfMtu = 0;
#ifdef ISS_WANTED
    INT4                i4PortCtlDup = 0;
    INT4                i4PortCtlSpeed = 0;
    INT4                i4PortCtlMode = 0;
    INT4                i4PortMdiOrMdixCap = 0;
    INT4                i4HolStatus = 0;
    INT4                i4CpuCntrlLearn = 0;
    BOOL1               bShowIssInfo = CFA_FALSE;
#endif /*ISS_WANTED */
    INT4                i4PauseOperMode = 0;
    INT4                i4LinkTrap = 0;
    INT4                i4IfaceType = 0;
    UINT4               u4InOctects = 0;
    UINT4               u4InUcastPkts = 0;
    UINT4               u4InMcastPkts = 0;
    UINT4               u4InBcastPkts = 0;
    UINT4               u4InDiscards = 0;
    UINT4               u4InErrors = 0;
    UINT4               u4UnknownProtos = 0;
    UINT4               u4OutOctects = 0;
    UINT4               u4OutUcastPkts = 0;
    UINT4               u4OutMcastPkts = 0;
    UINT4               u4OutBcastPkts = 0;
    UINT4               u4OutDiscards = 0;
    UINT4               u4OutErrors = 0;
    UINT1              *pu1Temp = NULL;
    INT4                i4Mtu = 0;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1HLIfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1IfIP[MAX_MAC_LENGTH];
    UINT1               au1IfMac[MAX_MAC_LENGTH];
    INT1               *piIfName;
    tTnlIfEntry         TnlIfEntry;
    tTnlIfEntry        *pTnlIfEntry = NULL;
    CHR1               *pu1String = NULL;
    UINT1               au1IpId[MAX_MAC_LENGTH];
    UINT1               au1IfBrgPortType[CFA_MAX_BRG_PORT_TYPE_LEN];
    UINT1               au1HCounter[CFA_CLI_U8_STR_LENGTH];
    INT4                i4Quit = 0;
    INT4                i4BridgedIfaceStatus = 0;
    INT4                i4IfMainSubType = -1;
    UINT4               u4HLIfIndex = 0;
    tSNMP_OCTET_STRING_TYPE IntfAlias;
    UINT1               au1IfAlias[CFA_MAX_IFALIAS_LENGTH];
    INT4                i4MinPortRate = 0;
    INT4                i4MaxPortRate = 0;
    INT4                i4PortIndex = 0;
    INT4                i4BridgePortType = 0;
    UINT4               u4ParentIndex = 0;
    INT4                i4EncapStatus = 0;
    FS_UINT8            u8IfHCCounter;
    tSNMP_COUNTER64_TYPE IfHCInOctets;
    tSNMP_COUNTER64_TYPE IfHCInUcastPkts;
    tSNMP_COUNTER64_TYPE IfHCInMulticastPkts;
    tSNMP_COUNTER64_TYPE IfHCInBroadcastPkts;
    tSNMP_COUNTER64_TYPE IfHCInDiscards;
    tSNMP_COUNTER64_TYPE IfHCInErrors;
    tSNMP_COUNTER64_TYPE IfHCOutOctets;
    tSNMP_COUNTER64_TYPE IfHCOutUcastPkts;
    tSNMP_COUNTER64_TYPE IfHCOutMulticastPkts;
    tSNMP_COUNTER64_TYPE IfHCOutBroadcastPkts;
    tSNMP_COUNTER64_TYPE IfHCOutDiscards;
#ifdef NPAPI_WANTED
    tIssPortCounters    IssPortCounters;
    MEMSET (&IssPortCounters, 0, sizeof (tIssPortCounters));
    MEMSET (au1HCounter, 0, CFA_CLI_U8_STR_LENGTH);
#endif
    FSAP_U8_CLR (&u8IfHCCounter);
    MEMCPY (&IfHCInOctets, &u8IfHCCounter, sizeof (tSNMP_COUNTER64_TYPE));
    MEMCPY (&IfHCInUcastPkts, &u8IfHCCounter, sizeof (tSNMP_COUNTER64_TYPE));
    MEMCPY (&IfHCInMulticastPkts, &u8IfHCCounter,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMCPY (&IfHCInBroadcastPkts, &u8IfHCCounter,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMCPY (&IfHCInDiscards, &u8IfHCCounter, sizeof (tSNMP_COUNTER64_TYPE));
    MEMCPY (&IfHCInErrors, &u8IfHCCounter, sizeof (tSNMP_COUNTER64_TYPE));
    MEMCPY (&IfHCOutOctets, &u8IfHCCounter, sizeof (tSNMP_COUNTER64_TYPE));
    MEMCPY (&IfHCOutUcastPkts, &u8IfHCCounter, sizeof (tSNMP_COUNTER64_TYPE));
    MEMCPY (&IfHCOutMulticastPkts, &u8IfHCCounter,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMCPY (&IfHCOutBroadcastPkts, &u8IfHCCounter,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMCPY (&IfHCOutDiscards, &u8IfHCCounter, sizeof (tSNMP_COUNTER64_TYPE));

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    MEMSET (au1IfIP, 0, MAX_MAC_LENGTH);
    MEMSET (au1IfBrgPortType, 0, CFA_MAX_BRG_PORT_TYPE_LEN);
    MEMSET (&IntfAlias, 0, sizeof (IntfAlias));
    MEMSET (au1HCounter, 0, CFA_CLI_U8_STR_LENGTH);
    pu1Temp = &au1IfIP[0];
    piIfName = (INT1 *) &au1IfName[0];
    IfPhysAddress.pu1_OctetList = &au1IfMac[0];

    nmhGetIfType (i4Index, &i4IfaceType);
    nmhGetIfAdminStatus (i4Index, &i4AdminStat);
    nmhGetIfOperStatus (i4Index, (INT4 *) &i4OperStat);
    MEMSET (au1IfAlias, 0, sizeof (au1IfAlias));
    IntfAlias.pu1_OctetList = au1IfAlias;

    if (i4IfaceType == CFA_ENET)
    {
        MEMSET (au1IfMac, 0, MAX_MAC_LENGTH);
#ifdef NPAPI_WANTED
        IssHwGetStat64 (i4Index, &IssPortCounters);
#endif
        nmhGetIfPhysAddress (i4Index, &IfPhysAddress);
        nmhGetIfMtu (i4Index, &i4IfMtu);
        nmhGetDot3PauseOperMode (i4Index, &i4PauseOperMode);
        nmhGetIfInOctets (i4Index, &u4InOctects);
        nmhGetIfInUcastPkts (i4Index, &u4InUcastPkts);
        nmhGetIfInMulticastPkts (i4Index, &u4InMcastPkts);
        nmhGetIfInBroadcastPkts (i4Index, &u4InBcastPkts);
        nmhGetIfInDiscards (i4Index, &u4InDiscards);
        nmhGetIfInErrors (i4Index, &u4InErrors);
        nmhGetIfInUnknownProtos (i4Index, &u4UnknownProtos);
        nmhGetIfOutOctets (i4Index, &u4OutOctects);
        nmhGetIfOutUcastPkts (i4Index, &u4OutUcastPkts);
        nmhGetIfOutMulticastPkts (i4Index, &u4OutMcastPkts);
        nmhGetIfOutBroadcastPkts (i4Index, &u4OutBcastPkts);
        nmhGetIfOutDiscards (i4Index, &u4OutDiscards);
        nmhGetIfOutErrors (i4Index, &u4OutErrors);
#ifdef NPAPI_WANTED
        IssHwGetStat64 (i4Index, &IssPortCounters);
#endif
    }
    pScanNode = (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY ((UINT4) i4Index);

    if (pScanNode != NULL)
    {
        u4HLIfIndex = CFA_IF_STACK_IFINDEX (pScanNode);
    }

    if (u4HLIfIndex != CFA_NONE)
    {
        CfaCliGetIfName (u4HLIfIndex, (INT1 *) au1HLIfName);
    }

#ifdef ISS_WANTED
    if (i4IfaceType == CFA_ENET)
    {
        /*
         * Nested locks MUST NOT be taken. Hence
         * release the CFA Lock before taking ISS Lock.
         */
        CFA_UNLOCK ();
        i4PortIndex =
            ((i4Index -
              1) / ISS_INTF_BREAKOUT_BASE_IDX) * ISS_INTF_BREAKOUT_BASE_IDX + 1;
        if (i4PortIndex != i4Index)
        {
            nmhGetIssPortCtrlSpeed (i4PortIndex, &i4PortCtlSpeed);
            if ((i4PortCtlSpeed == ISS_40GB) || (i4PortCtlSpeed == ISS_100GB))
            {
                return CLI_SUCCESS;
            }
            else if ((i4PortCtlSpeed == ISS_56GB)
                     && (((i4Index - i4PortIndex) % 2) != 0))
            {
                return CLI_SUCCESS;
            }
        }
        ISS_LOCK ();

        /*
         * Since CFA Lock is released, CLI task could be pre-empted
         * and CFA could be scheduled. CFA might delete the interface.
         * Hence we need to validate the interface index in ISS before
         * invoking the low level routines in ISS module.
         */
        if (nmhValidateIndexInstanceIssPortCtrlTable (i4Index) == SNMP_SUCCESS)
        {
            nmhGetIssPortCtrlDuplex (i4Index, &i4PortCtlDup);
            nmhGetIssPortCtrlSpeed (i4Index, &i4PortCtlSpeed);
            nmhGetIssPortCtrlMode (i4Index, &i4PortCtlMode);
            nmhGetIssPortHOLBlockPrevention (i4Index, &i4HolStatus);
            nmhGetIssPortCpuControlledLearning (i4Index, &i4CpuCntrlLearn);
            nmhGetIssPortMdiOrMdixCap (i4Index, &i4PortMdiOrMdixCap);
            nmhGetIssPortCtrlFlowControlMinRate (i4Index, &i4MinPortRate);
            nmhGetIssPortCtrlFlowControlMaxRate (i4Index, &i4MaxPortRate);
            bShowIssInfo = CFA_TRUE;
        }

        ISS_UNLOCK ();

        /* Take the CFA Lock back again. */
        CFA_LOCK ();
    }
#endif /* ISS_WANTED */

    CfaCliGetIfName ((UINT4) i4Index, piIfName);
    CliPrintf (CliHandle, "\r\n%s ", piIfName);

    if (i4AdminStat == CFA_IF_UP)
    {
        CliPrintf (CliHandle, "up, ");
    }
    else
    {
        CliPrintf (CliHandle, "down, ");
    }

    if ((i4IfaceType == CFA_PPP))
    {
        if (i4AdminStat == CFA_IF_UP)
        {
            if (i4OperStat == CFA_IF_UP)
            {
                CliPrintf (CliHandle, "line protocol is up");
            }
            else if (i4OperStat == CFA_IF_UNK)
            {
                CliPrintf (CliHandle, "line protocol is down (negotiating)");
            }
            else if ((i4OperStat == CFA_IF_LLDOWN) && (i4IfaceType == CFA_PPP))
            {
                CliPrintf (CliHandle,
                           "line protocol is down (physical link down)");
            }
            else
            {
                CliPrintf (CliHandle, "line protocol is down");
            }
        }
        else
        {
            CliPrintf (CliHandle, "line protocol is down");
        }
    }
    else
    {
        if (i4OperStat == CFA_IF_UP)
        {
            CliPrintf (CliHandle, "line protocol is up (connected)");
        }
        else if (i4OperStat == CFA_IF_DOWN)
        {
            CliPrintf (CliHandle, "line protocol is down (not connect)");
        }
        else if (i4OperStat == CFA_IF_NP)
        {
            CliPrintf (CliHandle, "line protocol is down (not present)");
        }
        else
        {
            CliPrintf (CliHandle, "line protocol is down (other)");
        }
    }

    nmhGetIfIvrBridgedIface (i4Index, &i4BridgedIfaceStatus);
    if (((i4IfaceType == CFA_ENET) && (i4BridgedIfaceStatus == CFA_ENABLED))
        || (i4IfaceType == CFA_LAGG) || (i4IfaceType == CFA_BRIDGED_INTERFACE)
        || (i4IfaceType == CFA_PIP)
        || (i4IfaceType == CFA_PROP_VIRTUAL_INTERFACE)
        || (i4IfaceType == CFA_PSEUDO_WIRE) || (i4IfaceType == CFA_VXLAN_NVE)
        || (i4IfaceType == CFA_TAP))
    {
        CfaGetInterfaceBrgPortTypeString (i4Index, au1IfBrgPortType);
        CliPrintf (CliHandle, "\r\nBridge Port Type: %s\r\n", au1IfBrgPortType);

    }

    if (i4IfaceType == CFA_L3SUB_INTF)
    {
        CfaGetL3SubIfParentIndex ((UINT4) i4Index, &u4ParentIndex);
        CfaCliGetIfName (u4ParentIndex, piIfName);
        CliPrintf (CliHandle, "\r\n[parent interface is %s]\r", piIfName);
    }

    nmhGetIfMainSubType (i4Index, &i4IfMainSubType);

    if (i4IfaceType == CFA_PROP_VIRTUAL_INTERFACE)
    {
        if (i4IfMainSubType == CFA_SUBTYPE_SISP_INTERFACE)
        {
            CliPrintf (CliHandle, "\r\nInterface SubType: SISP\r\n");
        }
        else if (i4IfMainSubType == CFA_SUBTYPE_AC_INTERFACE)
        {
            CliPrintf (CliHandle,
                       "\r\nInterface SubType: attachment circuit\r\n");
        }
    }

#ifndef CFA_UNIQUE_INTF_NAME
    if (i4IfMainSubType == CFA_FA_ENET)
    {
        CliPrintf (CliHandle, "\r\nInterface SubType: fastEthernet\r\n");
    }
    else if (i4IfMainSubType == CFA_GI_ENET)
    {
        CliPrintf (CliHandle, "\r\nInterface SubType: gigabitEthernet\r\n");
    }
    else if (i4IfMainSubType == CFA_XE_ENET)
    {
        CliPrintf (CliHandle, "\r\nInterface SubType: extremeEthernet\r\n");
    }
    else if (i4IfMainSubType == CFA_XL_ENET)
    {
        CliPrintf (CliHandle, "\r\nInterface SubType: xlEthernet\r\n");
    }
    else if (i4IfMainSubType == CFA_LVI_ENET)
    {
        CliPrintf (CliHandle, "\r\nInterface SubType: lviEthernet\r\n");
    }
#else
    if ((i4IfMainSubType == CFA_FA_ENET)
        || (i4IfMainSubType == CFA_GI_ENET)
        || (i4IfMainSubType == CFA_XE_ENET)
        || (i4IfMainSubType == CFA_XL_ENET)
        || (i4IfMainSubType == CFA_LVI_ENET))
    {
        CliPrintf (CliHandle, "\r\nInterface SubType: Ethernet\r\n");
    }
#endif
#ifdef HDLC_WANTED
    else if (i4IfMainSubType == CFA_HDLC)
    {
        CliPrintf (CliHandle, "\r\nInterface SubType: HDLC\r\n");
    }
#endif
    else if (i4IfMainSubType == CFA_ENET_UNKNOWN)
    {
        CliPrintf (CliHandle, "\r\nInterface SubType: Not Applicable\r\n");
    }

    nmhGetIfAlias (i4Index, &IntfAlias);
    if (i4IfaceType == CFA_LAGG)
    {
        nmhGetIfPhysAddress (i4Index, &IfPhysAddress);
        PrintMacAddress (IfPhysAddress.pu1_OctetList, pu1Temp);
        if (IntfAlias.i4_Length != 0)
        {
            CliPrintf (CliHandle, "Interface Alias: %s \r\n",
                       IntfAlias.pu1_OctetList);
        }
        CliPrintf (CliHandle, "\r\nHardware Address is %s\r\n", pu1Temp);
    }
    if (i4IfaceType == CFA_L3IPVLAN)
    {
        if (IntfAlias.i4_Length != 0)
        {
            CliPrintf (CliHandle, "Interface Alias: %s \r\n",
                       IntfAlias.pu1_OctetList);
        }
    }

    if (i4IfaceType == CFA_L3SUB_INTF)
    {
        if (IntfAlias.i4_Length != 0)
        {
            CliPrintf (CliHandle, "Interface Alias: %s \r\n",
                       IntfAlias.pu1_OctetList);
        }
        CFA_CDB_GET_ENCAP_STATUS ((UINT4) i4Index, i4EncapStatus);
        if (i4EncapStatus == CFA_TRUE)
        {
            CliPrintf (CliHandle,
                       "Encapsulation: DOT1Q, Associated VLAN: %d \r\n",
                       CFA_CDB_IF_VLANID_OF_IVR ((UINT4) i4Index));
        }
        else
        {
            CliPrintf (CliHandle,
                       "Encapsulation: NONE, Associated VLAN: NONE \r\n",
                       CFA_CDB_IF_VLANID_OF_IVR ((UINT4) i4Index));
        }
    }

    if (i4IfaceType == CFA_ENET)
    {
        PrintMacAddress (IfPhysAddress.pu1_OctetList, pu1Temp);
        if (IntfAlias.i4_Length != 0)
        {
            CliPrintf (CliHandle, "Interface Alias: %s \r\n",
                       IntfAlias.pu1_OctetList);
        }
        CliPrintf (CliHandle, "\r\nHardware Address is %s\r\n", pu1Temp);
        CliPrintf (CliHandle, "MTU  %d bytes, ", i4IfMtu);

#ifdef ISS_WANTED
        if (bShowIssInfo == CFA_TRUE)
        {
            if (i4PortCtlDup == ISS_HALFDUP)
            {
                CliPrintf (CliHandle, "Half duplex, ");
            }
            else if (i4PortCtlDup == ISS_FULLDUP)
            {
                CliPrintf (CliHandle, "Full duplex, ");
            }
            else
            {
                CliPrintf (CliHandle, "\r\nError in Duplex status\r\n");
            }

            if (i4PortCtlSpeed == ISS_10MBPS)
            {
                CliPrintf (CliHandle, "10 Mbps, ");
            }
            else if (i4PortCtlSpeed == ISS_100MBPS)
            {
                CliPrintf (CliHandle, "100 Mbps, ");
            }
            else if (i4PortCtlSpeed == ISS_1GB)
            {
                CliPrintf (CliHandle, "1 Gbps, ");
            }
            else if (i4PortCtlSpeed == ISS_2500MBPS)
            {
                CliPrintf (CliHandle, "2.5 Gbps, ");
            }
            else if (i4PortCtlSpeed == ISS_10GB)
            {
                CliPrintf (CliHandle, "10 Gbps, ");
            }
            else if (i4PortCtlSpeed == ISS_40GB)
            {
                CliPrintf (CliHandle, "40 Gbps, ");
            }
            else if (i4PortCtlSpeed == ISS_25GB)
            {
                CliPrintf (CliHandle, "25 Gbps, ");
            }
            else if (i4PortCtlSpeed == ISS_100GB)
            {
                CliPrintf (CliHandle, "100 Gbps, ");
            }
            else if (i4PortCtlSpeed == ISS_56GB)
            {
                CliPrintf (CliHandle, "50 Gbps, ");
            }
            else
            {
                CliPrintf (CliHandle, "Auto-speed, ");
            }

            if (i4PortCtlMode == ISS_NONEGOTIATION)
            {
                CliPrintf (CliHandle, " No-Negotiation\r\n");
            }
            else
            {
                CliPrintf (CliHandle, " Auto-Negotiation\r\n");
            }

            if (i4HolStatus == ISS_ENABLE_HOL)
            {
                CliPrintf (CliHandle, "HOL Block Prevention enabled.\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "HOL Block Prevention disabled.\r\n");
            }

            if (i4CpuCntrlLearn == ISS_CPU_CNTRL_LEARN_ENABLE)
            {
                CliPrintf (CliHandle, "CPU Controlled Learning enabled.\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "CPU Controlled Learning disabled.\r\n");
            }
            if (u4HLIfIndex != CFA_NONE)
            {
                CliPrintf (CliHandle,
                           "Encapsulation PPPoE, virtual-link %s\r\n",
                           au1HLIfName);
            }

            if (i4PortMdiOrMdixCap == ISS_AUTO_MDIX)
            {
                CliPrintf (CliHandle, "Auto-MDIX on\r\n");
            }
            else if (i4PortMdiOrMdixCap == ISS_MDI)
            {
                CliPrintf (CliHandle, "Auto-MDIX off (Port type is MDI)\r\n");
            }
            else if (i4PortMdiOrMdixCap == ISS_MDIX)
            {
                CliPrintf (CliHandle, "Auto-MDIX off (Port type is MDIX)\r\n");
            }
        }
#endif /*ISS_WANTED */
#ifdef NPAPI_WANTED
        switch (i4PauseOperMode)
        {
            case ETH_PAUSE_DISABLED:
                CliPrintf (CliHandle, "Input flow-control is off,"
                           "output flow-control is off\r\n");
                break;
            case ETH_PAUSE_ENABLED_XMIT:
                CliPrintf (CliHandle, "Input flow-control is off,"
                           "output flow-control is on\r\n");
                break;
            case ETH_PAUSE_ENABLED_RCV:
                CliPrintf (CliHandle, "Input flow-control is on,"
                           "output flow-control is off\r\n");
                break;
            case ETH_PAUSE_ENABLED_XMIT_AND_RCV:
                CliPrintf (CliHandle, "Input flow-control is on,"
                           "output flow-control is on\r\n");
                break;
            default:
                CliPrintf (CliHandle, "\r\n Invalid flowcontrol Mode\r\n");
        }

        if (i4MaxPortRate != 0)
        {
            CliPrintf (CliHandle,
                       "\r\nPause Rate : High Water Mark %d Kbps,"
                       "Low Water Mark %d Kbps\r\n",
                       i4MaxPortRate, i4MinPortRate);
        }

#endif

        nmhGetIfLinkUpDownTrapEnable (i4Index, &i4LinkTrap);
        if (i4LinkTrap == CFA_ENABLED)
        {
            CliPrintf (CliHandle, "\r\nLink Up/Down Trap is enabled \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nLink Up/Down Trap is disabled \r\n");
        }
        if (i4PortCtlSpeed <= ISS_10GB)
        {
            CliPrintf (CliHandle, "\r\nReception Counters\r\n");

            CliPrintf (CliHandle, "   Octets                    : %u\r\n",
                       u4InOctects);

            CliPrintf (CliHandle, "   Unicast Packets           : %u\r\n",
                       u4InUcastPkts);

            CliPrintf (CliHandle, "   Multicast Packets         : %u\r\n",
                       u4InMcastPkts);

            CliPrintf (CliHandle, "   Broadcast Packets         : %u\r\n",
                       u4InBcastPkts);

            CliPrintf (CliHandle, "   Discarded Packets         : %u\r\n",
                       u4InDiscards);

            CliPrintf (CliHandle, "   Error Packets             : %u\r\n",
                       u4InErrors);

            CliPrintf (CliHandle, "   Unknown Protocol          : %u\r\n",
                       u4UnknownProtos);
#ifdef NPAPI_WANTED
            FSAP_U8_2STR (&IssPortCounters.cntRxFCSErrors,
                          (CHR1 *) & au1HCounter);
            CliPrintf (CliHandle, "   CRC Errors                : %s\r\n",
                       au1HCounter);

            FSAP_U8_2STR (&IssPortCounters.cntRxSymbolErrors,
                          (CHR1 *) & au1HCounter);
            CliPrintf (CliHandle, "   Symbol Errors             : %s\r\n",
                       au1HCounter);

            FSAP_U8_2STR (&IssPortCounters.cntRxFrameSizeErrors,
                          (CHR1 *) & au1HCounter);
            CliPrintf (CliHandle, "   Good CRC Frame Size Errors: %s\r\n",
                       au1HCounter);

            FSAP_U8_2STR (&IssPortCounters.cntRxJabberPkts,
                          (CHR1 *) & au1HCounter);
            CliPrintf (CliHandle, "   Oversized w/ Bad CRC      : %s\r\n",
                       au1HCounter);
#endif
        }
        else
        {
            MEMSET (au1HCounter, 0, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_CLR (&u8IfHCCounter);
            nmhGetIfHCInOctets (i4Index, &IfHCInOctets);
            FSAP_U8_ASSIGN_HI (&u8IfHCCounter, IfHCInOctets.msn);
            FSAP_U8_ASSIGN_LO (&u8IfHCCounter, IfHCInOctets.lsn);
            FSAP_U8_2STR (&u8IfHCCounter, (CHR1 *) & au1HCounter);
            CliPrintf (CliHandle, "   Octets                    : %s\r\n",
                       au1HCounter);

            MEMSET (au1HCounter, 0, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_CLR (&u8IfHCCounter);
            nmhGetIfHCInUcastPkts (i4Index, &IfHCInUcastPkts);
            FSAP_U8_ASSIGN_HI (&u8IfHCCounter, IfHCInUcastPkts.msn);
            FSAP_U8_ASSIGN_LO (&u8IfHCCounter, IfHCInUcastPkts.lsn);
            FSAP_U8_2STR (&u8IfHCCounter, (CHR1 *) & au1HCounter);
            CliPrintf (CliHandle, "   Unicast Packets           : %s\r\n",
                       au1HCounter);

            MEMSET (au1HCounter, 0, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_CLR (&u8IfHCCounter);
            nmhGetIfHCInMulticastPkts (i4Index, &IfHCInMulticastPkts);
            FSAP_U8_ASSIGN_HI (&u8IfHCCounter, IfHCInMulticastPkts.msn);
            FSAP_U8_ASSIGN_LO (&u8IfHCCounter, IfHCInMulticastPkts.lsn);
            FSAP_U8_2STR (&u8IfHCCounter, (CHR1 *) & au1HCounter);
            CliPrintf (CliHandle, "   Multicast Packets         : %s\r\n",
                       au1HCounter);

            MEMSET (au1HCounter, 0, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_CLR (&u8IfHCCounter);
            nmhGetIfHCInBroadcastPkts (i4Index, &IfHCInBroadcastPkts);
            FSAP_U8_ASSIGN_HI (&u8IfHCCounter, IfHCInBroadcastPkts.msn);
            FSAP_U8_ASSIGN_LO (&u8IfHCCounter, IfHCInBroadcastPkts.lsn);
            FSAP_U8_2STR (&u8IfHCCounter, (CHR1 *) & au1HCounter);
            CliPrintf (CliHandle, "   Broadcast Packets         : %s\r\n",
                       au1HCounter);

            MEMSET (au1HCounter, 0, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_CLR (&u8IfHCCounter);
            FSAP_U8_ASSIGN_HI (&u8IfHCCounter, IfHCInDiscards.msn);
            FSAP_U8_ASSIGN_LO (&u8IfHCCounter, IfHCInDiscards.lsn);
            FSAP_U8_2STR (&u8IfHCCounter, (CHR1 *) & au1HCounter);
            CliPrintf (CliHandle, "   Discarded Packets         : %s\r\n",
                       au1HCounter);

            MEMSET (au1HCounter, 0, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_CLR (&u8IfHCCounter);
            FSAP_U8_ASSIGN_HI (&u8IfHCCounter, IfHCInErrors.msn);
            FSAP_U8_ASSIGN_LO (&u8IfHCCounter, IfHCInErrors.lsn);
            FSAP_U8_2STR (&u8IfHCCounter, (CHR1 *) & au1HCounter);
            CliPrintf (CliHandle, "   Error Packets             : %s\r\n",
                       au1HCounter);
        }
        CliPrintf (CliHandle, "\r\nTransmission Counters\r\n");
        if (i4PortCtlSpeed <= ISS_10GB)
        {
            CliPrintf (CliHandle, "   Octets                    : %u\r\n",
                       u4OutOctects);

            CliPrintf (CliHandle, "   Unicast Packets           : %u\r\n",
                       u4OutUcastPkts);

            CliPrintf (CliHandle, "   Multicast Packets         : %u\r\n",
                       u4OutMcastPkts);

            CliPrintf (CliHandle, "   Broadcast Packets         : %u\r\n",
                       u4OutBcastPkts);

            CliPrintf (CliHandle, "   Discarded Packets         : %u\r\n",
                       u4OutDiscards);
#ifndef NPAPI_WANTED
            CliPrintf (CliHandle, "   Error Packets             : %u\r\n",
                       u4OutErrors);
#else
            CliPrintf (CliHandle, "   Error Packets             : %u\r\n",
                       u4OutErrors);
            FSAP_U8_2STR (&IssPortCounters.cntTxFCSErroredPkts,
                          (CHR1 *) & au1HCounter);
            CliPrintf (CliHandle, "   Bad CRC                   : %s\r\n",
                       au1HCounter);

            FSAP_U8_2STR (&IssPortCounters.cntTxErrorDropPkts,
                          (CHR1 *) & au1HCounter);
            CliPrintf (CliHandle, "   Error Drops               : %s\r\n",
                       au1HCounter);

            FSAP_U8_2STR (&IssPortCounters.cntTxTimeOutPkts,
                          (CHR1 *) & au1HCounter);
            CliPrintf (CliHandle, "   Timeout Drops             : %s\r\n",
                       au1HCounter);
#endif
        }
        else
        {
            MEMSET (au1HCounter, 0, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_CLR (&u8IfHCCounter);
            nmhGetIfHCOutOctets (i4Index, &IfHCOutOctets);
            FSAP_U8_ASSIGN_HI (&u8IfHCCounter, IfHCOutOctets.msn);
            FSAP_U8_ASSIGN_LO (&u8IfHCCounter, IfHCOutOctets.lsn);
            FSAP_U8_2STR (&u8IfHCCounter, (CHR1 *) & au1HCounter);
            CliPrintf (CliHandle, "   Octets                    : %s\r\n",
                       au1HCounter);

            MEMSET (au1HCounter, 0, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_CLR (&u8IfHCCounter);
            nmhGetIfHCOutUcastPkts (i4Index, &IfHCOutUcastPkts);
            FSAP_U8_ASSIGN_HI (&u8IfHCCounter, IfHCOutUcastPkts.msn);
            FSAP_U8_ASSIGN_LO (&u8IfHCCounter, IfHCOutUcastPkts.lsn);
            FSAP_U8_2STR (&u8IfHCCounter, (CHR1 *) & au1HCounter);
            CliPrintf (CliHandle, "   Unicast Packets           : %s\r\n",
                       au1HCounter);

            MEMSET (au1HCounter, 0, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_CLR (&u8IfHCCounter);
            nmhGetIfHCOutMulticastPkts (i4Index, &IfHCOutMulticastPkts);
            FSAP_U8_ASSIGN_HI (&u8IfHCCounter, IfHCOutMulticastPkts.msn);
            FSAP_U8_ASSIGN_LO (&u8IfHCCounter, IfHCOutMulticastPkts.lsn);
            FSAP_U8_2STR (&u8IfHCCounter, (CHR1 *) & au1HCounter);
            CliPrintf (CliHandle, "   Multicast Packets         : %s\r\n",
                       au1HCounter);

            MEMSET (au1HCounter, 0, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_CLR (&u8IfHCCounter);

            nmhGetIfHCOutBroadcastPkts (i4Index, &IfHCOutBroadcastPkts);
            FSAP_U8_ASSIGN_HI (&u8IfHCCounter, IfHCOutBroadcastPkts.msn);
            FSAP_U8_ASSIGN_LO (&u8IfHCCounter, IfHCOutBroadcastPkts.lsn);
            FSAP_U8_2STR (&u8IfHCCounter, (CHR1 *) & au1HCounter);
            CliPrintf (CliHandle, "   Broadcast Packets         : %s\r\n",
                       au1HCounter);

            MEMSET (au1HCounter, 0, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_CLR (&u8IfHCCounter);
            FSAP_U8_ASSIGN_HI (&u8IfHCCounter, IfHCOutDiscards.msn);
            FSAP_U8_ASSIGN_LO (&u8IfHCCounter, IfHCOutDiscards.lsn);
            FSAP_U8_2STR (&u8IfHCCounter, (CHR1 *) & au1HCounter);
            CliPrintf (CliHandle, "   Discarded Packets         : %s\r\n",
                       au1HCounter);

        }
        CliPrintf (CliHandle, "   Error Packets             : %u", u4OutErrors);
    }

    if (i4IfaceType == CFA_TUNNEL)
    {

        pu1String = (CHR1 *) & au1IpId[0];
        CliPrintf (CliHandle, "\r\nHardware is Tunnel\r\n");
        nmhGetIfMtu (i4Index, &i4Mtu);
        CliPrintf (CliHandle, "MTU  %d bytes\r\n", i4Mtu);
        CliPrintf (CliHandle, "Encapsulation TUNNEL\r\n");

        pTnlIfEntry = &TnlIfEntry;
        if (CfaFindTnlEntryFromIfIndex ((UINT4) i4Index, pTnlIfEntry) ==
            CFA_FAILURE)
        {
            pTnlIfEntry = NULL;
        }

        if (pTnlIfEntry == NULL)
        {
            i4Quit =
                CliPrintf (CliHandle, "Tunnel Parameters not configured\r\n");
            return (i4Quit);
        }

        CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                   pTnlIfEntry->LocalAddr.Ip4TnlAddr);
        CliPrintf (CliHandle, "Tunnel Source %s", pu1String);

        CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                   pTnlIfEntry->RemoteAddr.Ip4TnlAddr);
        CliPrintf (CliHandle, ",Destination %s\r\n", pu1String);

        if (pTnlIfEntry->u4EncapsMethod == TNL_TYPE_GRE)
        {
            CliPrintf (CliHandle, "Tunnel Protocol/transport GRE \r\n");
        }
        else if (pTnlIfEntry->u4EncapsMethod == TNL_TYPE_ISATAP)
        {
            CliPrintf (CliHandle, "Tunnel Protocol/transport ISATAP \r\n");
        }
        else if (pTnlIfEntry->u4EncapsMethod == TNL_TYPE_IPV6IP)
        {
            CliPrintf (CliHandle, "Tunnel Protocol/transport IPV6IP \r\n");
        }
        else if (pTnlIfEntry->u4EncapsMethod == TNL_TYPE_COMPAT)
        {
            CliPrintf (CliHandle, "Tunnel Protocol/transport AUTO-COMPAT \r\n");
        }
#ifdef OPENFLOW_WANTED
        else if (pTnlIfEntry->u4EncapsMethod == TNL_TYPE_OPENFLOW)
        {
            CliPrintf (CliHandle, "Tunnel for Openflow Hybrid\r\n");
        }
#endif /* OPENLOW_WANTED */
        else if (pTnlIfEntry->u4EncapsMethod == TNL_TYPE_SIXTOFOUR)
        {
            CliPrintf (CliHandle, "Tunnel Protocol/transport 6TO4 \r\n");
        }

        if (pTnlIfEntry->bCksumFlag == CFA_ENABLED)
        {
            CliPrintf (CliHandle, "Checksumming of packets Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Checksumming of packets Disabled\r\n");
        }

        if (pTnlIfEntry->bPmtuFlag == CFA_ENABLED)
        {
            CliPrintf (CliHandle, "Path MTU Discovery Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Path MTU Discovery Disabled\r\n");
        }
    }

#ifdef PPP_WANTED
    if (i4IfaceType == CFA_PPP)
    {
        CfaCliPppShowInterface (CliHandle, i4Index);
    }
#endif /* PPP_WANTED */

    if (i4IfaceType == CFA_PSEUDO_WIRE)
    {
        if (IntfAlias.i4_Length != 0)
        {
            CliPrintf (CliHandle, "Interface Alias: %s \r\n",
                       IntfAlias.pu1_OctetList);
        }
    }

#ifdef HDLC_WANTED
    if (i4IfaceType == CFA_HDLC)
    {
        if (u4HLIfIndex != CFA_NONE)
        {
            CliPrintf (CliHandle, "Encapsulation PPP, virtual-link %s\r\n",
                       au1HLIfName);
        }
    }
#endif
    if (i4IfaceType == CFA_BRIDGED_INTERFACE)
    {

        nmhGetIfMainBrgPortType (i4Index, &i4BridgePortType);

        if ((IntfAlias.i4_Length != 0) &&
            (i4BridgePortType == CFA_STATION_FACING_BRIDGE_PORT))
        {
            CliPrintf (CliHandle, "Interface Alias: %s \r\n",
                       IntfAlias.pu1_OctetList);
        }
    }

    i4Quit = CliPrintf (CliHandle, "\r\n");

    return (i4Quit);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowInterfacesDesc                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays the interface description   */
/*                                                                           */
/*     INPUT            : i4Index - Index of the interface                   */
/*                        CliHandle, CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowInterfacesDesc (tCliHandle CliHandle, INT4 i4Index)
{
    INT1               *piIfName;
    INT4                i4AdminStat;
    INT4                i4OperStat;
    INT4                i4UfdOperStatus;
    INT4                i4Quit;
    INT4                i4PortIndex = 0;
    INT4                i4PortCtlSpeed = 0;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1IfMainDesc[CFA_MAX_INTF_DESC_LEN];
    tCfaIfInfo          IfInfo;
    tSNMP_OCTET_STRING_TYPE IfDescString;

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
    MEMSET (au1IfMainDesc, 0, sizeof (au1IfMainDesc));
    MEMSET (&IfDescString, 0, sizeof (IfDescString));
    /* if the speed of any one of the Lane is ISS_40GB,
       then 1st index alone is displayed for description */
    i4PortIndex =
        ((i4Index -
          1) / ISS_INTF_BREAKOUT_BASE_IDX) * ISS_INTF_BREAKOUT_BASE_IDX + 1;
    if (i4PortIndex != i4Index)
    {
        /* During ISSU maintenance mode operation , get speed from
         * control plane itself. This is to avoid HW calls  */
        if (IssuGetMaintModeOperation () == OSIX_TRUE)
        {
            /* Retrive the Speed value for port  from CFA */
            if ((CfaGetIfInfo ((UINT4) i4Index, &IfInfo)) == CFA_FAILURE)
            {
                return CLI_FAILURE;
            }
            /* convert the interface speed from CFA format to ISS */
            IssSysConvertCfaSpeedToIssSpeed (IfInfo.u4IfSpeed,
                                             IfInfo.u4IfHighSpeed,
                                             &i4PortCtlSpeed);
        }
        else
        {
            nmhGetIssPortCtrlSpeed (i4PortIndex, &i4PortCtlSpeed);
        }

        if ((i4PortCtlSpeed == ISS_40GB) || (i4PortCtlSpeed == ISS_100GB))
        {
            return (CLI_SUCCESS);
        }
        else if ((i4PortCtlSpeed == ISS_56GB)
                 && (((i4Index - i4PortIndex) % 2) != 0))
        {
            return (CLI_SUCCESS);
        }
    }

    IfDescString.pu1_OctetList = &au1IfMainDesc[0];

    piIfName = (INT1 *) &au1IfName[0];

    nmhGetIfAdminStatus (i4Index, &i4AdminStat);
    nmhGetIfOperStatus (i4Index, (INT4 *) &i4OperStat);
    nmhGetIfMainDesc (i4Index, &IfDescString);
    nmhGetIfMainUfdOperStatus (i4Index, &i4UfdOperStatus);
    if (CfaCliGetIfName ((UINT4) i4Index, piIfName) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "%-13s", piIfName);

    if (i4AdminStat == CFA_IF_UP)
    {
        CliPrintf (CliHandle, "%-9s", "up");
    }
    else if (i4AdminStat == CFA_IF_DOWN)
    {
        if (i4UfdOperStatus == CFA_IF_UFD_ERR_DISABLED)
        {
            gu4UfdFlag = CFA_TRUE;
            CliPrintf (CliHandle, "%-9s", "down*");
        }
        else
        {
            CliPrintf (CliHandle, "%-9s", "down");
        }
    }
    else if (i4AdminStat == CFA_LOOPBACK_LOCAL)
    {
        CliPrintf (CliHandle, "%-9s", "loopback");
    }

    if (i4OperStat == CFA_IF_UP)
    {
        i4Quit = CliPrintf (CliHandle, "%-10s", "up");
    }
    else if (i4OperStat == CFA_IF_DOWN)
    {
        i4Quit = CliPrintf (CliHandle, "%-10s", "down");
    }
    else if (i4OperStat == CFA_IF_NP)
    {
        i4Quit = CliPrintf (CliHandle, "%-10s", "not present");
    }
    else if (i4OperStat == CFA_IF_LLDOWN)
    {
        i4Quit = CliPrintf (CliHandle, "%-10s", "down");
    }
    else if (i4OperStat == CFA_IF_UNK)
    {
        i4Quit = CliPrintf (CliHandle, "%-10s", "down");
    }
    else
    {
        i4Quit = CliPrintf (CliHandle, "%-10s", "other");
    }
    CliPrintf (CliHandle, "%-10s\r\n", IfDescString.pu1_OctetList);
    return (i4Quit);

}

#ifdef ISS_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowInterfacesStorm                             */
/*                                                                           */
/*     DESCRIPTION      : This function displays the interface capabilities  */
/*                                                                           */
/*     INPUT            : i4Index - Index of the interface                   */
/*                        CliHandle, CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowInterfacesStorm (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4Quit;
    INT1               *piIfName;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    piIfName = (INT1 *) &au1IfName[0];
#ifdef MRVLLS
    INT4                i4PktType = 0;
    INT4                i4RateCtrlLimit = 0;
    INT4                i4EgressLimit = 0;

    /*
     * Nested locks MUST NOT be taken. Hence
     * release the CFA Lock before taking ISS Lock.
     */
    CFA_UNLOCK ();

    /*
     * Since CFA Lock is released, CLI task could be pre-empted
     * and CFA could be scheduled. CFA might delete the interface.
     * Hence we need validate the interface index in ISS before
     * invoking the low level routines in ISS module.
     */
    ISS_LOCK ();

    if (nmhValidateIndexInstanceIssPortCtrlTable (i4Index) == SNMP_SUCCESS)
    {
        nmhGetIssExtRateCtrlPackets (i4Index, &i4PktType);
        nmhGetIssExtRateCtrlLimitValue (i4Index, &i4RateCtrlLimit);
        nmhGetIssExtRateCtrlPortRateLimit (i4Index, &i4EgressLimit);
    }

    ISS_UNLOCK ();

    /* Take the CFA Lock back again. */
    CFA_LOCK ();

    CfaCliGetIfName ((UINT4) i4Index, piIfName);
    i4Quit = CliPrintf (CliHandle, "\r\n%s\r\n", piIfName);

    if ((i4PktType == 0) && (i4RateCtrlLimit == 0) && (i4EgressLimit == 0))
    {
        CliPrintf (CliHandle, "Storm Control           : Disabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Storm Control           : Enabled\r\n");

        if (i4PktType == ISS_RATE_DLF_MCAST_BCAST)
        {
            CliPrintf (CliHandle,
                       "Storm Control Type      : DLF_MCAST_BCAST\r\n");
        }
        else if (i4PktType == ISS_RATE_MCAST_BCAST)
        {
            CliPrintf (CliHandle, "Storm Control Type      : MCAST_BCAST\r\n");
        }
        else if (i4PktType == ISS_RATE_BCAST)
        {
            CliPrintf (CliHandle, "Storm Control Type      : BCAST\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Storm Control Type      : All\r\n");
        }

        switch (i4RateCtrlLimit)
        {
            case ISS_STORM_CONTROL_128K:
                CliPrintf (CliHandle, "Storm Control Limit     : 128K\r\n\n");
                break;
            case ISS_STORM_CONTROL_256K:
                CliPrintf (CliHandle, "Storm Control Limit     : 256K\r\n\n");
                break;
            case ISS_STORM_CONTROL_512K:
                CliPrintf (CliHandle, "Storm Control Limit     : 512K\r\n\n");
                break;
            case ISS_STORM_CONTROL_1M:
                CliPrintf (CliHandle, "Storm Control Limit     : 1M\r\n\n");
                break;
            case ISS_STORM_CONTROL_2M:
                CliPrintf (CliHandle, "Storm Control Limit     : 2M\r\n\n");
                break;
            case ISS_STORM_CONTROL_4M:
                CliPrintf (CliHandle, "Storm Control Limit     : 4M\r\n\n");
                break;
            case ISS_STORM_CONTROL_8M:
                CliPrintf (CliHandle, "Storm Control Limit     : 8M\r\n\n");
                break;
            case ISS_STORM_CONTROL_16M:
                CliPrintf (CliHandle, "Storm Control Limit     : 16M\r\n\n");
                break;
            case ISS_STORM_CONTROL_32M:
                CliPrintf (CliHandle, "Storm Control Limit     : 32M\r\n\n");
                break;
            case ISS_STORM_CONTROL_64M:
                CliPrintf (CliHandle, "Storm Control Limit     : 64M\r\n\n");
                break;
            case ISS_STORM_CONTROL_128M:
                CliPrintf (CliHandle, "Storm Control Limit     : 128M\r\n\n");
                break;
            case ISS_STORM_CONTROL_256M:
                CliPrintf (CliHandle, "Storm Control Limit     : 256M\r\n\n");
                break;
            default:
                break;
        }
        switch (i4EgressLimit)
        {
            case ISS_STORM_CONTROL_128K:
                CliPrintf (CliHandle, "Egress Rate Limit     : 128K\r\n\n");
                break;
            case ISS_STORM_CONTROL_256K:
                CliPrintf (CliHandle, "Egress Rate Limit     : 256K\r\n\n");
                break;
            case ISS_STORM_CONTROL_512K:
                CliPrintf (CliHandle, "Egress Rate Limit     : 512K\r\n\n");
                break;
            case ISS_STORM_CONTROL_1M:
                CliPrintf (CliHandle, "Egress Rate Limit     : 1M\r\n\n");
                break;
            case ISS_STORM_CONTROL_2M:
                CliPrintf (CliHandle, "Egress Rate Limit     : 2M\r\n\n");
                break;
            case ISS_STORM_CONTROL_4M:
                CliPrintf (CliHandle, "Egress Rate Limit     : 4M\r\n\n");
                break;
            case ISS_STORM_CONTROL_8M:
                CliPrintf (CliHandle, "Egress Rate Limit     : 8M\r\n\n");
                break;
            case ISS_STORM_CONTROL_16M:
                CliPrintf (CliHandle, "Egress Rate Limit     : 16M\r\n\n");
                break;
            case ISS_STORM_CONTROL_32M:
                CliPrintf (CliHandle, "Egress Rate Limit     : 32M\r\n\n");
                break;
            case ISS_STORM_CONTROL_64M:
                CliPrintf (CliHandle, "Egress Rate Limit     : 64M\r\n\n");
                break;
            case ISS_STORM_CONTROL_128M:
                CliPrintf (CliHandle, "Egress Rate Limit     : 128M\r\n\n");
                break;
            case ISS_STORM_CONTROL_256M:
                CliPrintf (CliHandle, "Egress Rate Limit     : 256M\r\n\n");
                break;
            default:
                break;
        }
    }
#else
    INT4                i4DLFLimit = 0;
    INT4                i4BCASTLimit = 0;
    INT4                i4MCASTLimit = 0;
    INT4                i4CtrlStatus = 0;

    /*
     * Nested locks MUST NOT be taken. Hence
     * release the CFA Lock before taking ISS Lock.
     */
    CFA_UNLOCK ();

    /*
     * Since CFA Lock is released, CLI task could be pre-empted
     * and CFA could be scheduled. CFA might delete the interface.
     * Hence we need validate the interface index in ISS before
     * invoking the low level routines in ISS module.
     */
    ISS_LOCK ();

    if (nmhValidateIndexInstanceIssPortCtrlTable (i4Index) == SNMP_SUCCESS)
    {
        nmhGetIssRateCtrlDLFLimitValue (i4Index, &i4DLFLimit);
        nmhGetIssRateCtrlBCASTLimitValue (i4Index, &i4BCASTLimit);
        nmhGetIssRateCtrlMCASTLimitValue (i4Index, &i4MCASTLimit);
        nmhGetIssExtDefaultRateCtrlStatus (i4Index, &i4CtrlStatus);
    }

    ISS_UNLOCK ();

    /* Take the CFA Lock back again. */
    CFA_LOCK ();

    CfaCliGetIfName ((UINT4) i4Index, piIfName);

    if (i4CtrlStatus == ISS_STORM_CONTROL_DISABLE)
    {
        CliPrintf (CliHandle, "\r\n%s Storm Control         : Disabled\r\n",
                   piIfName);
    }
    else
    {
        CliPrintf (CliHandle, "\r\n%s Storm Control         : Enabled\r\n",
                   piIfName);
    }

    if (i4DLFLimit == 0)
    {
        CliPrintf (CliHandle, "DLF Storm Control           : Disabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "DLF Storm Control           : Enabled\r\n");
        CliPrintf (CliHandle, "DLF Storm Control Limit     : %d\r\n\n",
                   i4DLFLimit);
    }

    if (i4BCASTLimit == 0)
    {
        CliPrintf (CliHandle, "Broadcast Storm Control     : Disabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Broadcast Storm Control     : Enabled\r\n");

        CliPrintf (CliHandle, "Broadcast Storm Control Limit : %d\r\n\n",
                   i4BCASTLimit);
    }

    if (i4MCASTLimit == 0)
    {
        i4Quit = CliPrintf (CliHandle,
                            "Multicast Storm Control     : Disabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Multicast Storm Control     : Enabled\r\n");

        i4Quit = CliPrintf (CliHandle,
                            "Multicast Storm Control Limit : %d\r\n",
                            i4MCASTLimit);
    }
#endif
    return (i4Quit);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowInterfacesRateLimit                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays the interface capabilities  */
/*                                                                           */
/*     INPUT            : i4Index - Index of the interface                   */
/*                        CliHandle, CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowInterfacesRateLimit (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4Quit;
    INT4                i4RateCtrlLimit = 0;
    INT4                i4EgressLimit = 0;
    INT4                i4MinPortRate = 0;
    INT4                i4MaxPortRate = 0;
    INT1               *piIfName;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    piIfName = (INT1 *) &au1IfName[0];

    /*
     * Nested locks MUST NOT be taken. Hence
     * release the CFA Lock before taking ISS Lock.
     */
    CFA_UNLOCK ();

    /*
     * Since CFA Lock is released, CLI task could be pre-empted
     * and CFA could be scheduled. CFA might delete the interface.
     * Hence we need validate the interface index in ISS before
     * invoking the low level routines in ISS module.
     */
    ISS_LOCK ();

    if (nmhValidateIndexInstanceIssPortCtrlTable (i4Index) == SNMP_SUCCESS)
    {
        nmhGetIssExtRateCtrlPortRateLimit (i4Index, &i4RateCtrlLimit);
        nmhGetIssExtRateCtrlPortBurstSize (i4Index, &i4EgressLimit);
        nmhGetIssPortCtrlFlowControlMinRate (i4Index, &i4MinPortRate);
        nmhGetIssPortCtrlFlowControlMaxRate (i4Index, &i4MaxPortRate);
    }

    ISS_UNLOCK ();

    /* Take the CFA Lock back again. */
    CFA_LOCK ();

    CfaCliGetIfName ((UINT4) i4Index, piIfName);
    i4Quit = CliPrintf (CliHandle, "\r\n%s\r\n", piIfName);

    CliPrintf (CliHandle, "Port Control Rate Limit : %d kbps\r\n\n",
               i4RateCtrlLimit);
    CliPrintf (CliHandle, "Port Control Burst Size : %d kbits\r\n\n",
               i4EgressLimit);
    if (i4MaxPortRate != 0)
    {
        CliPrintf (CliHandle, "Port Pause Rate High Water Mark : %d Kbps\r\n\n",
                   i4MaxPortRate);
        CliPrintf (CliHandle, "Port Pause Rate Low Water Mark  : %d Kbps\r\n\n",
                   i4MinPortRate);

    }

    return (i4Quit);
}

#endif /*ISS_WANTED */
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowInterfacesCapabilities                      */
/*                                                                           */
/*     DESCRIPTION      : This function displays the interface capabilities  */
/*                                                                           */
/*     INPUT            : i4Index - Index of the interface                   */
/*                        CliHandle, CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
CfaShowInterfacesCapabilities (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4Quit;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    INT1               *piIfName;
#ifdef NPAPI_WANTED
    INT4                i4AutoNegFlag = 0;
    INT4                i4FastEthFlag = 0;
    INT4                i4Flag = 0;
    INT4                i4AutoNegVal = 0;
    INT4                i4RetVal;
    INT4                ai4Duplexity[2] = { 0 };
    UINT1               au1Duplex[CFA_CLI_MAX_DUPLEX_LENGTH];
    UINT1               au1Type[CFA_CLI_MAX_TYPE_LENGTH];
    UINT1               au1Speed[CFA_CLI_MAX_SPEED_LENGTH];
    UINT1               au1Element[MAU_BYTE_LEN];
    tSNMP_OCTET_STRING_TYPE pElement;

    pElement.pu1_OctetList = au1Element;
    MEMSET (pElement.pu1_OctetList, 0, sizeof (INT4));
    MEMSET (au1Duplex, 0, CFA_CLI_MAX_DUPLEX_LENGTH);
    MEMSET (au1Type, 0, CFA_CLI_MAX_TYPE_LENGTH);
    MEMSET (au1Speed, 0, CFA_CLI_MAX_SPEED_LENGTH);
#endif
    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    piIfName = (INT1 *) &au1IfName[0];

    CfaCliGetIfName ((UINT4) i4Index, piIfName);
    CliPrintf (CliHandle, "\r\n%s\r\n", piIfName);
#ifdef NPAPI_WANTED
    nmhGetIfMauAutoNegSupported (i4Index, 0, &i4AutoNegVal);
    if (i4AutoNegVal != 2)
    {
        i4AutoNegFlag = 1;
    }
    i4RetVal = nmhGetIfMauTypeListBits (i4Index, 0, &pElement);
    if (i4RetVal != SNMP_FAILURE)
    {
        SPRINTF ((CHR1 *) au1Type, "%sType        :", au1Type);
        SPRINTF ((CHR1 *) au1Speed, "%sSpeed       :", au1Speed);
        SPRINTF ((CHR1 *) au1Duplex, "%sDuplex      :", au1Duplex);
        if (pElement.pu1_OctetList[MAU_TYPE_10_BASE_THD] ==
            (1 << (MAU_TYPE_LIST_LEN - (MAU_TYPE_10_BASE_THD % MAU_OCTET_LEN))))
        {
            SPRINTF ((CHR1 *) au1Type, "%s 10 Base THD,", au1Type);
            SPRINTF ((CHR1 *) au1Speed, "%s 10 Mbps,", au1Speed);
            i4FastEthFlag = 1;
            ai4Duplexity[0] = 1;
        }
        if (pElement.pu1_OctetList[MAU_TYPE_10_BASE_TFD] ==
            (1 << (MAU_TYPE_LIST_LEN - (MAU_TYPE_10_BASE_TFD % MAU_OCTET_LEN))))
        {
            SPRINTF ((CHR1 *) au1Type, "%s 10 Base TFD,", au1Type);
            if (i4FastEthFlag != 1)
            {
                SPRINTF ((CHR1 *) au1Speed, "%s 10 Mbps,", au1Speed);
            }
            ai4Duplexity[1] = 2;
        }
        if (pElement.pu1_OctetList[MAU_TYPE_100_BASE_TXHD] ==
            (1 <<
             (MAU_TYPE_LIST_LEN - (MAU_TYPE_100_BASE_TXHD % MAU_OCTET_LEN))))
        {
            SPRINTF ((CHR1 *) au1Type, "%s 100 Base TXHD,", au1Type);
            SPRINTF ((CHR1 *) au1Speed, "%s 100 Mbps,", au1Speed);
            i4Flag = 1;
            ai4Duplexity[0] = 1;
        }
        if (pElement.pu1_OctetList[MAU_TYPE_100_BASE_TXFD] ==
            (1 <<
             (MAU_TYPE_LIST_LEN - (MAU_TYPE_100_BASE_TXFD % MAU_OCTET_LEN))))
        {
            SPRINTF ((CHR1 *) au1Type, "%s 100 Base TXFD,", au1Type);
            if (i4Flag != 1)
            {
                SPRINTF ((CHR1 *) au1Speed, "%s 100 Mbps,", au1Speed);
            }
            ai4Duplexity[1] = 2;
        }
        if (pElement.pu1_OctetList[MAU_TYPE_1000_BASE_TFD] ==
            (1 <<
             (MAU_TYPE_LIST_LEN - (MAU_TYPE_1000_BASE_TFD % MAU_OCTET_LEN))))
        {
            SPRINTF ((CHR1 *) au1Type, "%s 1000 Base TFD,", au1Type);
            SPRINTF ((CHR1 *) au1Speed, "%s 1000 Mbps,", au1Speed);
            ai4Duplexity[1] = 2;
        }
        if (pElement.pu1_OctetList[MAU_TYPE_10GIG_BASE_X] ==
            (1 <<
             (MAU_TYPE_LIST_LEN - (MAU_TYPE_10GIG_BASE_X % MAU_OCTET_LEN))))
        {
            SPRINTF ((CHR1 *) au1Type, "%s 10000 Base X,", au1Type);
            SPRINTF ((CHR1 *) au1Speed, "%s 10000 Mbps,", au1Speed);
            ai4Duplexity[1] = 2;
        }
        if (pElement.pu1_OctetList[MAU_TYPE_2500_BASE_TFD] ==
            (1 <<
             (MAU_TYPE_LIST_LEN - (MAU_TYPE_2500_BASE_TFD % MAU_OCTET_LEN))))
        {
            SPRINTF ((CHR1 *) au1Type, "%s 2500 Base X,", au1Type);
            SPRINTF ((CHR1 *) au1Speed, "%s 2500 Mbps,", au1Speed);
            ai4Duplexity[1] = 2;
        }
        au1Type[(STRLEN (au1Type) - 1)] = '\0';
        if (i4AutoNegFlag == 1)
        {
            SPRINTF ((CHR1 *) au1Speed, "%s Auto", au1Speed);
        }
        else
        {
            au1Speed[(STRLEN (au1Speed) - 1)] = '\0';
        }

        if (ai4Duplexity[0] == 1)
        {
            SPRINTF ((CHR1 *) au1Duplex, "%s Half", au1Duplex);
        }
        if (ai4Duplexity[1] == 2)
        {
            SPRINTF ((CHR1 *) au1Duplex, "%s Full", au1Duplex);
        }
        au1Duplex[(STRLEN (au1Duplex))] = '\0';
        CliPrintf (CliHandle, "%s\r\n", au1Type);
        CliPrintf (CliHandle, "%s\r\n", au1Speed);
        CliPrintf (CliHandle, "%s\r\n", au1Duplex);
    }
    i4Quit = CliPrintf (CliHandle, "FlowControl : Send, Receive\r\n");
#else

    CliPrintf (CliHandle, "Type        : 10/100/1000 Base TX\r\n");
    CliPrintf (CliHandle, "Speed       : 10, 100, 1000, Auto \r\n");
    CliPrintf (CliHandle, "Duplex      : Half, Full \r\n");
    i4Quit = CliPrintf (CliHandle, "FlowControl : Send, Receive\r\n");

#endif
    return (i4Quit);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowInterfacesStatus                            */
/*                                                                           */
/*     DESCRIPTION      : This function displays the interface status        */
/*                                                                           */
/*     INPUT            : i4Index - Index of the interface                   */
/*                        CliHandle, CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowInterfacesStatus (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4OperStat;
#ifdef ISS_WANTED
    INT4                i4PortCtlDup;
    INT4                i4PortCtlSpeed = 0;
    INT4                i4PortCtlMode;
    INT4                i4PortMdiOrMdixCap = 0;
    BOOL1               bShowIssInfo = CFA_FALSE;
    INT4                i4PortIndex = 0;
#endif /*ISS_WANTED */
    INT4                i4Quit;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    INT1               *piIfName;

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);

    piIfName = (INT1 *) &au1IfName[0];

    CfaCliGetIfName ((UINT4) i4Index, piIfName);

    nmhGetIfOperStatus (i4Index, &i4OperStat);

#ifdef ISS_WANTED
    /*
     * Nested locks MUST NOT be taken. Hence
     * release the CFA Lock before taking ISS Lock.
     */
    CFA_UNLOCK ();
    i4PortIndex =
        ((i4Index -
          1) / ISS_INTF_BREAKOUT_BASE_IDX) * ISS_INTF_BREAKOUT_BASE_IDX + 1;
    if (i4PortIndex != i4Index)
    {
        nmhGetIssPortCtrlSpeed (i4PortIndex, &i4PortCtlSpeed);
        if ((i4PortCtlSpeed == ISS_40GB) || (i4PortCtlSpeed == ISS_100GB))
        {
            return (CLI_SUCCESS);
        }
        else if ((i4PortCtlSpeed == ISS_56GB)
                 && (((i4Index - i4PortIndex) % 2) != 0))
        {
            return (CLI_SUCCESS);
        }
    }

    ISS_LOCK ();

    /*
     * Since CFA Lock is released, CLI task could be pre-empted
     * and CFA could be scheduled. CFA might delete the interface.
     * Hence we need validate the interface index in ISS before
     * invoking the low level routines in ISS module.
     */
    if (nmhValidateIndexInstanceIssPortCtrlTable (i4Index) == SNMP_SUCCESS)
    {
        nmhGetIssPortCtrlDuplex (i4Index, &i4PortCtlDup);
        nmhGetIssPortCtrlSpeed (i4Index, &i4PortCtlSpeed);
        nmhGetIssPortCtrlMode (i4Index, &i4PortCtlMode);
        nmhGetIssPortMdiOrMdixCap (i4Index, &i4PortMdiOrMdixCap);
        bShowIssInfo = CFA_TRUE;
    }

    ISS_UNLOCK ();

    /* Take the CFA Lock back again. */
    CFA_LOCK ();
#endif /*ISS_WANTED */

    CliPrintf (CliHandle, "%-12s", piIfName);

    if (i4OperStat == CFA_IF_UP)
    {
        i4Quit = CliPrintf (CliHandle, "%-17s", "connected");
    }
    else if (i4OperStat == CFA_IF_DOWN)
    {
        i4Quit = CliPrintf (CliHandle, "%-17s", "not connected");
    }
    else if (i4OperStat == CFA_IF_NP)
    {
        i4Quit = CliPrintf (CliHandle, "%-17s", "not present");
    }
    else
    {
        i4Quit = CliPrintf (CliHandle, "%-17s", "other");
    }

    /* When the interface is not present, its properties are not
     * shown in CLI */
    if (i4OperStat == CFA_IF_NP)
    {
        i4Quit =
            CliPrintf (CliHandle, "%-9s %-12s %-16s %-12s\r\n", "-",
                       "-", "-", "-");
    }
    else
    {
#ifdef ISS_WANTED
        if (bShowIssInfo == CFA_TRUE)
        {
            if (i4PortCtlDup == ISS_HALFDUP)
            {
                CliPrintf (CliHandle, "%-9s", "Half");
            }
            else if (i4PortCtlDup == ISS_FULLDUP)
            {
                CliPrintf (CliHandle, "%-9s", "Full");
            }
            else
            {
                CliPrintf (CliHandle, "%-9s", "-");

            }

            if ((i4PortCtlSpeed == 0) ||
                ((i4PortCtlDup == ISS_HALFDUP) && (i4PortCtlMode == ISS_AUTO)))
            {
                CliPrintf (CliHandle, "%-12s", "-");
            }
            else if (i4PortCtlSpeed == ISS_10MBPS)
            {
                CliPrintf (CliHandle, "%-12s", "10 Mbps");
            }
            else if (i4PortCtlSpeed == ISS_100MBPS)
            {
                CliPrintf (CliHandle, "%-12s", "100 Mbps");
            }
            else if (i4PortCtlSpeed == ISS_1GB)
            {
                CliPrintf (CliHandle, "%-12s", "1 Gbps");
            }
            else if (i4PortCtlSpeed == ISS_2500MBPS)
            {
                CliPrintf (CliHandle, "%-12s", "2.5 Gbps");
            }
            else if (i4PortCtlSpeed == ISS_10GB)
            {
                CliPrintf (CliHandle, "%-12s", "10 Gbps");
            }
            else if (i4PortCtlSpeed == ISS_40GB)
            {
                CliPrintf (CliHandle, "%-12s", "40 Gbps");
            }
            else if (i4PortCtlSpeed == ISS_25GB)
            {
                CliPrintf (CliHandle, "%-12s", "25 Gbps");
            }
            else if (i4PortCtlSpeed == ISS_100GB)
            {
                CliPrintf (CliHandle, "%-12s", "100 Gbps");
            }
            else if (i4PortCtlSpeed == ISS_56GB)
            {
                CliPrintf (CliHandle, "%-12s", "50 Gbps");
            }
            else
            {
                CliPrintf (CliHandle, "%-12s", "Auto-speed");
            }

            if (i4PortCtlMode == ISS_AUTO)
            {
                i4Quit = CliPrintf (CliHandle, "%-16s", "Auto");
            }
            else
            {
                i4Quit = CliPrintf (CliHandle, "%-16s", "No-Negotiation");
            }

            if (i4PortMdiOrMdixCap == ISS_AUTO_MDIX)
            {
                i4Quit = CliPrintf (CliHandle, "Auto-MDIX on");
            }
            else if (i4PortMdiOrMdixCap == ISS_MDI)
            {
                i4Quit =
                    CliPrintf (CliHandle, "Auto-MDIX off (Port type is MDI)");
            }
            else if (i4PortMdiOrMdixCap == ISS_MDIX)
            {
                i4Quit =
                    CliPrintf (CliHandle, "Auto-MDIX off (Port type is MDIX)");
            }

            CliPrintf (CliHandle, "\r\n");
        }
        else
        {
            i4Quit =
                CliPrintf (CliHandle, "%-8s %-11s %-15s %-12s\r\n", "-",
                           "Auto-speed", "No-Negotiation", "Auto-MDIX on");
        }
    }
#endif /*ISS_WANTED */

    return (i4Quit);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowInterfacesFlow                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays the interface flowcontrol   */
/*                                                                           */
/*     INPUT            : i4Index - Index of the interface                   */
/*                        CliHandle, CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
CfaShowInterfacesFlow (tCliHandle CliHandle, INT4 i4Index)
{
    tSNMP_COUNTER64_TYPE HCInPauseFrames;
    tSNMP_COUNTER64_TYPE HCOutPauseFrames;
    FS_UINT8            u8HCInPauseFrames;
    FS_UINT8            u8HCOutPauseFrames;
    INT4                i4PauseOperMode;
    INT4                i4PauseAdminMode;
    INT4                i4InPauseFrames;
    INT4                i4OutPauseFrames;
    INT4                i4Quit;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    INT1               *piIfName;
    UINT1               au1HCInCount[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1HCOutCount[CFA_CLI_U8_STR_LENGTH];

    FSAP_U8_CLR (&u8HCInPauseFrames);
    FSAP_U8_CLR (&u8HCOutPauseFrames);

    MEMSET (&HCInPauseFrames, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&HCOutPauseFrames, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (au1HCInCount, 0, CFA_CLI_U8_STR_LENGTH);
    MEMSET (au1HCOutCount, 0, CFA_CLI_U8_STR_LENGTH);

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    piIfName = (INT1 *) &au1IfName[0];

    nmhGetDot3PauseOperMode (i4Index, &i4PauseOperMode);
    nmhGetDot3PauseAdminMode (i4Index, &i4PauseAdminMode);
    nmhGetDot3InPauseFrames (i4Index, (UINT4 *) &i4InPauseFrames);
    nmhGetDot3OutPauseFrames (i4Index, (UINT4 *) &i4OutPauseFrames);
    nmhGetDot3HCInPauseFrames (i4Index,
                               (tSNMP_COUNTER64_TYPE *) & HCInPauseFrames);
    nmhGetDot3HCOutPauseFrames (i4Index,
                                (tSNMP_COUNTER64_TYPE *) & HCOutPauseFrames);

    u8HCInPauseFrames.u4Hi = HCInPauseFrames.msn;
    u8HCInPauseFrames.u4Lo = HCInPauseFrames.lsn;

    u8HCOutPauseFrames.u4Hi = HCOutPauseFrames.msn;
    u8HCOutPauseFrames.u4Lo = HCOutPauseFrames.lsn;

    /* Converts the UINT8 value to string */
    FSAP_U8_2STR (&u8HCInPauseFrames, (CHR1 *) & au1HCInCount);
    FSAP_U8_2STR (&u8HCOutPauseFrames, (CHR1 *) & au1HCOutCount);

    CfaCliGetIfName ((UINT4) i4Index, piIfName);
    CliPrintf (CliHandle, "%-8s", piIfName);

    switch (i4PauseAdminMode)
    {
        case ETH_PAUSE_DISABLED:
            CliPrintf (CliHandle, "%-9s", "off off");
            break;
        case ETH_PAUSE_ENABLED_XMIT:
            CliPrintf (CliHandle, "%-9s", "on  off");
            break;
        case ETH_PAUSE_ENABLED_RCV:
            CliPrintf (CliHandle, "%-9s", "off  on");
            break;
        case ETH_PAUSE_ENABLED_XMIT_AND_RCV:
            CliPrintf (CliHandle, "%-9s", " on  on");
            break;
        default:
            CliPrintf (CliHandle, "\r\n Invalid \r\n");
    }

    switch (i4PauseOperMode)
    {
        case ETH_PAUSE_DISABLED:
            CliPrintf (CliHandle, "%-9s", "off off");
            break;
        case ETH_PAUSE_ENABLED_XMIT:
            CliPrintf (CliHandle, "%-9s", "on  off");
            break;
        case ETH_PAUSE_ENABLED_RCV:
            CliPrintf (CliHandle, "%-9s", "off  on");
            break;
        case ETH_PAUSE_ENABLED_XMIT_AND_RCV:
            CliPrintf (CliHandle, "%-9s", " on  on");
            break;
        default:
            CliPrintf (CliHandle, "\r\n Invalid \r\n");
    }

    CliPrintf (CliHandle, "%-11u", i4OutPauseFrames);
    CliPrintf (CliHandle, "%-11u", i4InPauseFrames);
    CliPrintf (CliHandle, "%-15s", au1HCOutCount);
    i4Quit = CliPrintf (CliHandle, "%-11s \r\n", au1HCInCount);

    return (i4Quit);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaGetInterfaceBrgPortTypeString                   */
/*                                                                           */
/*     DESCRIPTION      : This function gets the bridge port type for the    */
/*                        given interface and writes in the given string.    */
/*                                                                           */
/*     INPUT            : i4Index - Index of the interface                   */
/*                                                                           */
/*     OUTPUT           : pu1String - String where the port type is stored.  */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
CfaGetInterfaceBrgPortTypeString (INT4 i4Index, UINT1 *pu1String)
{
    INT4                i4BridgePortType;

    nmhGetIfMainBrgPortType (i4Index, &i4BridgePortType);

    switch (i4BridgePortType)
    {
        case CFA_PROVIDER_NETWORK_PORT:
            SPRINTF ((CHR1 *) pu1String, "%s", "Provider Network Port");
            break;
        case CFA_CNP_PORTBASED_PORT:
            SPRINTF ((CHR1 *) pu1String, "%s",
                     "Customer Network Port(Port-Based)");
            break;
        case CFA_CNP_STAGGED_PORT:
            SPRINTF ((CHR1 *) pu1String, "%s",
                     "Customer Network Port(Stag-Based)");
            break;
        case CFA_CUSTOMER_EDGE_PORT:
            SPRINTF ((CHR1 *) pu1String, "%s", "Customer Edge Port");
            break;
        case CFA_PROP_PROVIDER_NETWORK_PORT:
            SPRINTF ((CHR1 *) pu1String, "%s", "Prop Provider Network Port");
            break;
        case CFA_PROP_CUSTOMER_NETWORK_PORT:
            SPRINTF ((CHR1 *) pu1String, "%s", "Prop  Customer Network Port");
            break;
        case CFA_PROP_CUSTOMER_EDGE_PORT:
            SPRINTF ((CHR1 *) pu1String, "%s", "Prop Customer Edge Port");
            break;
        case CFA_CNP_CTAGGED_PORT:
            SPRINTF ((CHR1 *) pu1String, "%s",
                     "Customer Network Port(Ctag-Based)");
            break;
        case CFA_CUSTOMER_BRIDGE_PORT:
            SPRINTF ((CHR1 *) pu1String, "%s", "Customer Bridge Port");
            break;
        case CFA_CUSTOMER_BACKBONE_PORT:
            SPRINTF ((CHR1 *) pu1String, "%s", "Customer Backbone Port");
            break;
        case CFA_PROVIDER_INSTANCE_PORT:
            SPRINTF ((CHR1 *) pu1String, "%s", "Provider Instance Port");
            break;
        case CFA_VIRTUAL_INSTANCE_PORT:
            SPRINTF ((CHR1 *) pu1String, "%s", "Virtual Instance Port");
            break;
        case CFA_UPLINK_ACCESS_PORT:
            SPRINTF ((CHR1 *) pu1String, "%s", "EVB Uplink Access Port");
            break;
        case CFA_STATION_FACING_BRIDGE_PORT:
            SPRINTF ((CHR1 *) pu1String, "%s", "Station Facing Bridge Port");
            break;

        default:
            SPRINTF ((CHR1 *) pu1String, "%s", "Invalid Bridge Port");
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowInterfacesCounters                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays the counters of the various */
/*                        interfaces                                         */
/*                                                                           */
/*     INPUT            : i4NextIndex - Index of the interface               */
/*                        CliHandle, CLI Handler                             */
/*                        pu1Alias - Alias name                              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowInterfacesCounters (tCliHandle CliHandle, INT4 i4Index, UINT1 *pu1Alias)
{
    UINT1               u1BrgPortType = 0;

    CfaGetIfBrgPortType ((UINT4) i4Index, &u1BrgPortType);

    if (u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        /* Interface Counters are not supported for S-Channel Interface */
        CliPrintf (CliHandle,
                   "\r%%Counters for S-Channel interface - not Supported\r\n");
        return CLI_SUCCESS;
    }

    if (CfaShowIfacesInCounters (CliHandle, i4Index, pu1Alias) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (CfaShowIfacesOutCounters (CliHandle, i4Index, pu1Alias) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowL3VlanInterfacesCounters                    */
/*                                                                           */
/*     DESCRIPTION      : This function displays the counters of the various */
/*                        interfaces                                         */
/*                                                                           */
/*     INPUT            : i4NextIndex - Index of the interface               */
/*                        CliHandle, CLI Handler                             */
/*                        pu1Alias - Alias name                              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowL3VlanInterfacesCounters (tCliHandle CliHandle, INT4 i4Index,
                                 UINT1 *pu1Alias)
{
    if (CfaShowL3VlanIfacesInCounters (CliHandle, i4Index, pu1Alias) ==
        CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowIfacesInCounters                            */
/*                                                                           */
/*     DESCRIPTION      : This function displays the in counters of the      */
/*                        the various interfaces                             */
/*                                                                           */
/*     INPUT            : i4NextIndex - Index of the interface               */
/*                        CliHandle, CLI Handler                             */
/*                        pu1Alias - Alias name                              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
CfaShowIfacesInCounters (tCliHandle CliHandle, INT4 i4Index, UINT1 *pu1Alias)
{
    INT4                i4RetVal = 0;
    INT4                i4PrevIndex = 0;
    UINT1               u1BrgPortType = 0;

    CliPrintf (CliHandle,
               "\r\nPort      InOctet    InUcast    InMcast    InBcast    InDiscard  "
               " InErrs     InHCOctet\r\n");
    CliPrintf (CliHandle,
               "----      -------    -------    -------    -------    ---------  "
               " ------     ---------\r\n");

    if (i4Index != 0)
    {
        if (nmhValidateIndexInstanceIfTable (i4Index) == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
        if (CfaShowInterfaceInCounters (CliHandle, i4Index) == CLI_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }

    if (pu1Alias != NULL)
    {
        i4RetVal =
            (INT4) CfaCliGetFirstIfIndexFromName (pu1Alias, (UINT4 *) &i4Index);
        while (i4RetVal != OSIX_FAILURE)
        {
            if (CfaShowInterfaceInCounters (CliHandle, i4Index) == CLI_FAILURE)
            {
                return CLI_FAILURE;
            }
            i4PrevIndex = i4Index;
            i4RetVal =
                (INT4) CfaCliGetNextIfIndexFromName (pu1Alias,
                                                     (UINT4) i4PrevIndex,
                                                     (UINT4 *) &i4Index);
        }
        return CLI_SUCCESS;
    }

    i4RetVal = nmhGetFirstIndexIfTable (&i4Index);

    while (i4RetVal != SNMP_FAILURE)
    {
        /* Interface Counters are not supported for S-Channel Interface */
        CfaGetIfBrgPortType ((UINT4) i4Index, &u1BrgPortType);
        if (u1BrgPortType != CFA_STATION_FACING_BRIDGE_PORT)
        {
            if (CfaShowInterfaceInCounters (CliHandle, i4Index) == CLI_FAILURE)
            {
                return CLI_FAILURE;
            }
        }

        i4PrevIndex = i4Index;
        i4RetVal = nmhGetNextIndexIfTable (i4PrevIndex, &i4Index);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowL3VlanIfacesInCounters                      */
/*                                                                           */
/*     DESCRIPTION      : This function displays the in counters of the      */
/*                        the various interfaces                             */
/*                                                                           */
/*     INPUT            : i4NextIndex - Index of the interface               */
/*                        CliHandle, CLI Handler                             */
/*                        pu1Alias - Alias name                              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
CfaShowL3VlanIfacesInCounters (tCliHandle CliHandle, INT4 i4Index,
                               UINT1 *pu1Alias)
{
    INT4                i4RetVal = 0;
    INT4                i4PrevIndex = 0;

    CliPrintf (CliHandle, "\r\nPort      InPkt    InOctets\r\n");
    CliPrintf (CliHandle, "----      -----    --------\r\n");

    if (i4Index != 0)
    {
        if (nmhValidateIndexInstanceIfTable (i4Index) == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
        if (CfaShowL3VlanInterfaceInCounters (CliHandle, i4Index) ==
            CLI_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }

    if (pu1Alias != NULL)
    {
        i4RetVal =
            (INT4) CfaCliGetFirstIfIndexFromName (pu1Alias, (UINT4 *) &i4Index);
        while (i4RetVal != OSIX_FAILURE)
        {
            if (CfaShowL3VlanInterfaceInCounters (CliHandle, i4Index) ==
                CLI_FAILURE)
            {
                return CLI_FAILURE;
            }
            i4PrevIndex = i4Index;
            i4RetVal =
                (INT4) CfaCliGetNextIfIndexFromName (pu1Alias,
                                                     (UINT4) i4PrevIndex,
                                                     (UINT4 *) &i4Index);
        }
        return CLI_SUCCESS;
    }

    i4RetVal = nmhGetFirstIndexIfTable (&i4Index);

    while (i4RetVal != SNMP_FAILURE)
    {
        if (CfaShowL3VlanInterfaceInCounters (CliHandle, i4Index) ==
            CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
        i4PrevIndex = i4Index;
        i4RetVal = nmhGetNextIndexIfTable (i4PrevIndex, &i4Index);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowIfacesOutCounters                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays the in counters of the      */
/*                        the various interfaces                             */
/*                                                                           */
/*     INPUT            : i4NextIndex - Index of the interface               */
/*                        CliHandle, CLI Handler                             */
/*                        pu1Alias - Alias name                              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowIfacesOutCounters (tCliHandle CliHandle, INT4 i4Index, UINT1 *pu1Alias)
{
    INT4                i4RetVal = 0;
    INT4                i4PrevIndex = 0;
    UINT1               u1BrgPortType = 0;

    CliPrintf (CliHandle,
               "Port      OutOctet   OutUcast   OutMcast   OutBcast   OutDiscard  OutErrs   "
               " OutHCOctet\r\n");
    CliPrintf (CliHandle,
               "----      --------   --------   --------   --------   ----------  -------   "
               " ----------\r\n");

    if (i4Index != 0)
    {
        if (nmhValidateIndexInstanceIfTable (i4Index) == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
        if (CfaShowInterfaceOutCounters (CliHandle, i4Index) == CLI_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    if (pu1Alias != NULL)
    {
        i4RetVal =
            (INT4) CfaCliGetFirstIfIndexFromName (pu1Alias, (UINT4 *) &i4Index);
        while (i4RetVal != OSIX_FAILURE)
        {
            if (CfaShowInterfaceOutCounters (CliHandle, i4Index) == CLI_FAILURE)
            {
                return CLI_FAILURE;
            }
            i4PrevIndex = i4Index;
            i4RetVal =
                (INT4) CfaCliGetNextIfIndexFromName (pu1Alias,
                                                     (UINT4) i4PrevIndex,
                                                     (UINT4 *) &i4Index);
        }
        return CLI_SUCCESS;
    }

    i4RetVal = nmhGetFirstIndexIfTable (&i4Index);

    while (i4RetVal != SNMP_FAILURE)
    {
        /* Interface Counters are not supported for S-Channel Interface */

        CfaGetIfBrgPortType ((UINT4) i4Index, &u1BrgPortType);
        if (u1BrgPortType != CFA_STATION_FACING_BRIDGE_PORT)
        {
            if (CfaShowInterfaceOutCounters (CliHandle, i4Index) == CLI_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
        i4PrevIndex = i4Index;
        i4RetVal = nmhGetNextIndexIfTable (i4PrevIndex, &i4Index);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowInterfaceInCounters                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays the interface counter       */
/*                                                                           */
/*     INPUT            : i4NextIndex - Index of the interface               */
/*                        CliHandle, CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowInterfaceInCounters (tCliHandle CliHandle, INT4 i4Index)
{
    FS_UINT8            u8IfHCInOctets;
    UINT4               u4InOctects = 0;
    UINT4               u4InUcastPkts = 0;
    UINT4               u4InMcastPkts = 0;
    UINT4               u4InBcastPkts = 0;
    UINT4               u4InDiscards = 0;
    UINT4               u4InErrors = 0;
    INT4                i4Quit = CLI_SUCCESS;
    UINT1               au1HCInCount[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    INT1               *piIfName = NULL;

    tSNMP_COUNTER64_TYPE IfHCInOctets;

    MEMSET (au1HCInCount, 0, CFA_CLI_U8_STR_LENGTH);
    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    MEMSET (&u8IfHCInOctets, 0, sizeof (FS_UINT8));
    piIfName = (INT1 *) &au1IfName[0];

    if ((i4Index != 0) && (CfaIsSispInterface ((UINT4) i4Index) == CFA_TRUE))
    {
        /* Operation not supported over this interface index
         * */
        return CLI_SUCCESS;
    }

    FSAP_U8_CLR (&u8IfHCInOctets);

    if ((CfaIsILanInterface ((UINT4) i4Index) == CFA_FALSE) &&
        (CfaIsIfEntryProgrammingAllowed ((UINT4) i4Index) == CFA_TRUE))
    {
        MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
        MEMSET (au1HCInCount, 0, CFA_CLI_U8_STR_LENGTH);
        FSAP_U8_CLR (&u8IfHCInOctets);
        nmhGetIfInOctets (i4Index, &u4InOctects);
        nmhGetIfInUcastPkts (i4Index, &u4InUcastPkts);
        nmhGetIfInMulticastPkts (i4Index, &u4InMcastPkts);
        nmhGetIfInBroadcastPkts (i4Index, &u4InBcastPkts);
        nmhGetIfInDiscards (i4Index, &u4InDiscards);
        nmhGetIfInErrors (i4Index, &u4InErrors);
        MEMCPY (&IfHCInOctets, &u8IfHCInOctets, sizeof (tSNMP_COUNTER64_TYPE));
        nmhGetIfHCInOctets (i4Index, &IfHCInOctets);

        CfaCliGetIfName ((UINT4) i4Index, piIfName);
        FSAP_U8_ASSIGN_HI (&u8IfHCInOctets, IfHCInOctets.msn);
        FSAP_U8_ASSIGN_LO (&u8IfHCInOctets, IfHCInOctets.lsn);
        /* Converts the UINT8 value to string */
        FSAP_U8_2STR (&u8IfHCInOctets, (CHR1 *) & au1HCInCount);
        CliPrintf (CliHandle, "%-9s ", piIfName);
        CliPrintf (CliHandle, "%-11u", u4InOctects);
        CliPrintf (CliHandle, "%-11u", u4InUcastPkts);
        CliPrintf (CliHandle, "%-11u", u4InMcastPkts);
        CliPrintf (CliHandle, "%-11u", u4InBcastPkts);
        CliPrintf (CliHandle, "%-12u", u4InDiscards);
        CliPrintf (CliHandle, "%-11u", u4InErrors);
        i4Quit = CliPrintf (CliHandle, "%-20s \r\n", au1HCInCount);
    }
    return i4Quit;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowL3VlanInterfaceInCounters                   */
/*                                                                           */
/*     DESCRIPTION      : This function displays the interface counter       */
/*                                                                           */
/*     INPUT            : i4NextIndex - Index of the interface               */
/*                        CliHandle, CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowL3VlanInterfaceInCounters (tCliHandle CliHandle, INT4 i4Index)
{
    UINT4               u4InOctects = 0;
    UINT4               u4InPkts = 0;
    INT4                i4Quit = CLI_SUCCESS;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    INT1               *piIfName = NULL;

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    piIfName = (INT1 *) &au1IfName[0];

    if ((i4Index != 0) && (CfaIsSispInterface ((UINT4) i4Index) == CFA_TRUE))
    {
        /* Operation not supported over this interface index
         * */
        return CLI_SUCCESS;
    }

    if ((CfaIsL3IpVlanInterface ((UINT4) i4Index) == CFA_SUCCESS) &&
        (CfaIsIfEntryProgrammingAllowed ((UINT4) i4Index) == CFA_TRUE))
    {
        MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
        nmhGetIfMainExtInPkts (i4Index, &u4InPkts);
        nmhGetIfInOctets (i4Index, &u4InOctects);

        CfaCliGetIfName ((UINT4) i4Index, piIfName);
        /* Converts the UINT8 value to string */
        CliPrintf (CliHandle, "%-9s ", piIfName);
        CliPrintf (CliHandle, "%-11u", u4InPkts);
        CliPrintf (CliHandle, "%-11u\r\n", u4InOctects);
    }
    return i4Quit;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowInterfaceOutCounters                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays the interface counter       */
/*                                                                           */
/*     INPUT            : i4NextIndex - Index of the interface               */
/*                        CliHandle, CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowInterfaceOutCounters (tCliHandle CliHandle, INT4 i4Index)
{
    FS_UINT8            u8IfHCOutOctets;
    UINT4               u4OutOctects = 0;
    UINT4               u4OutUcastPkts = 0;
    UINT4               u4OutMcastPkts = 0;
    UINT4               u4OutBcastPkts = 0;
    UINT4               u4OutDiscards = 0;
    UINT4               u4OutErrors = 0;
    INT4                i4Quit = CLI_SUCCESS;
    UINT1               au1HCOutCount[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    INT1               *piIfName = NULL;

    tSNMP_COUNTER64_TYPE IfHCOutOctets;

    MEMSET (au1HCOutCount, 0, CFA_CLI_U8_STR_LENGTH);
    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    MEMSET (&u8IfHCOutOctets, 0, sizeof (FS_UINT8));
    piIfName = (INT1 *) &au1IfName[0];

    if ((i4Index != 0) && (CfaIsSispInterface ((UINT4) i4Index) == CFA_TRUE))
    {
        /* Operation not supported over this interface index
         * */
        return CLI_SUCCESS;
    }

    FSAP_U8_CLR (&u8IfHCOutOctets);

    if ((CfaIsILanInterface ((UINT4) i4Index) == CFA_FALSE) &&
        (CfaIsVipInterface ((UINT4) i4Index) == CFA_FALSE))
    {
        MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
        MEMSET (au1HCOutCount, 0, CFA_CLI_U8_STR_LENGTH);
        FSAP_U8_CLR (&u8IfHCOutOctets);
        MEMCPY (&IfHCOutOctets, &u8IfHCOutOctets,
                sizeof (tSNMP_COUNTER64_TYPE));

        nmhGetIfOutOctets (i4Index, &u4OutOctects);
        nmhGetIfHCOutOctets (i4Index, &IfHCOutOctets);
        nmhGetIfOutUcastPkts (i4Index, &u4OutUcastPkts);
        nmhGetIfOutMulticastPkts (i4Index, &u4OutMcastPkts);
        nmhGetIfOutBroadcastPkts (i4Index, &u4OutBcastPkts);
        nmhGetIfOutDiscards (i4Index, &u4OutDiscards);
        nmhGetIfOutErrors (i4Index, &u4OutErrors);

        CfaCliGetIfName ((UINT4) i4Index, piIfName);

        FSAP_U8_ASSIGN_HI (&u8IfHCOutOctets, IfHCOutOctets.msn);
        FSAP_U8_ASSIGN_LO (&u8IfHCOutOctets, IfHCOutOctets.lsn);

        /* Converts the UINT8 value to string */
        FSAP_U8_2STR (&u8IfHCOutOctets, (CHR1 *) au1HCOutCount);
        CliPrintf (CliHandle, "%-9s ", piIfName);
        CliPrintf (CliHandle, "%-10u ", u4OutOctects);
        CliPrintf (CliHandle, "%-10u ", u4OutUcastPkts);
        CliPrintf (CliHandle, "%-10u ", u4OutMcastPkts);
        CliPrintf (CliHandle, "%-10u ", u4OutBcastPkts);
        CliPrintf (CliHandle, "%-11u ", u4OutDiscards);
        CliPrintf (CliHandle, "%-10u ", u4OutErrors);
        i4Quit = CliPrintf (CliHandle, "%-20s \r\n", au1HCOutCount);

    }
    return i4Quit;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaClearInterfacesCounters                         */
/*                                                                           */
/*     DESCRIPTION      : This function clears interface counters            */
/*                                                                           */
/*     INPUT            : i4NextIndex - Index of the interface               */
/*                        CliHandle, CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
CfaClearInterfacesCounters (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4RetVal = 0;

    if ((i4RetVal = CfaUtilClearInterfacesCounters (i4Index)) != CFA_SUCCESS)
    {
        return (CLI_FAILURE);
    }
    UNUSED_PARAM (CliHandle);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowInterfacesHcCounters                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays statistics in 64 bit format */
/*                                                                           */
/*     INPUT            : i4NextIndex - Index of the interface               */
/*                        CliHandle, CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
CfaShowInterfacesHcCounters (tCliHandle CliHandle, INT4 i4Index)
{

    FS_UINT8            u8IfHCInCount;
    FS_UINT8            u8IfHCOutCount;
    UINT1               au1HCInCount[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1HCOutCount[CFA_CLI_U8_STR_LENGTH];
    UINT1               u1Flag = 1;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1BrgPortType = 0;
    INT1               *piIfName = NULL;
    INT4                i4TempIndex = i4Index;
    INT4                i4PrevIndex = 0;
    INT4                i4Quit = CLI_SUCCESS;

    tSNMP_COUNTER64_TYPE IfHCCount;

    MEMSET (au1HCInCount, 0, CFA_CLI_U8_STR_LENGTH);
    MEMSET (au1HCOutCount, 0, CFA_CLI_U8_STR_LENGTH);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[0];

    FSAP_U8_CLR (&u8IfHCInCount);
    FSAP_U8_CLR (&u8IfHCInCount);

    CfaGetIfBrgPortType ((UINT4) i4Index, &u1BrgPortType);
    /* Counters for S-Channel Interface is not supported */
    if ((i4Index != 0) && (u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT))
    {
        CliPrintf (CliHandle,
                   "\r%%Counters for S-Channel interface - not Supported\r\n");
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle,
               "\r\nPort      InHCOctet           InUcastPkts        "
               " InMcastPkts     InBcastPkts\r\n");
    CliPrintf (CliHandle,
               "----      ---------           -----------        "
               " -----------     -----------\r\n");

    if (i4Index == 0)
    {
        if (nmhGetFirstIndexIfTable (&i4Index) == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
        u1Flag = 0;
    }
    do
    {
        CfaGetIfBrgPortType ((UINT4) i4Index, &u1BrgPortType);
        /* Statistics are not supported for S-Channel Interface */
        if ((CfaIsILanInterface ((UINT4) i4Index) == CFA_FALSE) &&
            (CfaIsVipInterface ((UINT4) i4Index) == CFA_FALSE) &&
            (u1BrgPortType != CFA_STATION_FACING_BRIDGE_PORT))
        {
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            CfaCliGetIfName ((UINT4) i4Index, piIfName);
            CliPrintf (CliHandle, "%-10s", piIfName);

            MEMSET (au1HCInCount, 0, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_CLR (&u8IfHCInCount);
            nmhGetIfHCInOctets (i4Index, &IfHCCount);
            FSAP_U8_ASSIGN_HI (&u8IfHCInCount, IfHCCount.msn);
            FSAP_U8_ASSIGN_LO (&u8IfHCInCount, IfHCCount.lsn);
            /* Converts the UINT8 value to string */
            FSAP_U8_2STR (&u8IfHCInCount, (CHR1 *) & au1HCInCount);
            i4Quit = CliPrintf (CliHandle, "%-20s", au1HCInCount);

            MEMSET (au1HCInCount, 0, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_CLR (&u8IfHCInCount);
            nmhGetIfHCInUcastPkts (i4Index, &IfHCCount);
            FSAP_U8_ASSIGN_HI (&u8IfHCInCount, IfHCCount.msn);
            FSAP_U8_ASSIGN_LO (&u8IfHCInCount, IfHCCount.lsn);
            /* Converts the UINT8 value to string */
            FSAP_U8_2STR (&u8IfHCInCount, (CHR1 *) & au1HCInCount);
            i4Quit = CliPrintf (CliHandle, "%-20s", au1HCInCount);

            MEMSET (au1HCInCount, 0, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_CLR (&u8IfHCInCount);
            nmhGetIfHCInMulticastPkts (i4Index, &IfHCCount);
            FSAP_U8_ASSIGN_HI (&u8IfHCInCount, IfHCCount.msn);
            FSAP_U8_ASSIGN_LO (&u8IfHCInCount, IfHCCount.lsn);
            /* Converts the UINT8 value to string */
            FSAP_U8_2STR (&u8IfHCInCount, (CHR1 *) & au1HCInCount);
            i4Quit = CliPrintf (CliHandle, "%-20s", au1HCInCount);

            MEMSET (au1HCInCount, 0, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_CLR (&u8IfHCInCount);
            nmhGetIfHCInBroadcastPkts (i4Index, &IfHCCount);
            FSAP_U8_ASSIGN_HI (&u8IfHCInCount, IfHCCount.msn);
            FSAP_U8_ASSIGN_LO (&u8IfHCInCount, IfHCCount.lsn);
            /* Converts the UINT8 value to string */
            FSAP_U8_2STR (&u8IfHCInCount, (CHR1 *) & au1HCInCount);
            i4Quit = CliPrintf (CliHandle, "%-20s\r\n", au1HCInCount);

        }
        i4PrevIndex = i4Index;

        if (i4Quit == CLI_FAILURE)
        {
            return (CLI_SUCCESS);
        }

    }
    while ((u1Flag == 0)
           && (nmhGetNextIndexIfTable (i4PrevIndex, &i4Index) ==
               SNMP_SUCCESS) && (i4Quit == CLI_SUCCESS));

    i4Index = i4TempIndex;

    i4Quit = CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle,
               "\r\nPort      OutHCOctet          OutUcastPkts       "
               " OutMcastPkts    OutBcastPkts\r\n");
    CliPrintf (CliHandle,
               "----      ----------          ------------      "
               "------------    -------------\r\n");
    if (i4Index == 0)
    {
        if (nmhGetFirstIndexIfTable (&i4Index) == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }

        u1Flag = 0;
    }
    do
    {
        CfaGetIfBrgPortType ((UINT4) i4Index, &u1BrgPortType);
        /* Statistics are not supported for S-Channel Interface */
        if ((CfaIsILanInterface ((UINT4) i4Index) == CFA_FALSE) &&
            (CfaIsVipInterface ((UINT4) i4Index) == CFA_FALSE) &&
            (u1BrgPortType != CFA_STATION_FACING_BRIDGE_PORT))
        {

            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            CfaCliGetIfName ((UINT4) i4Index, piIfName);
            CliPrintf (CliHandle, "%-10s", piIfName);

            MEMSET (au1HCInCount, 0, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_CLR (&u8IfHCOutCount);
            nmhGetIfHCOutOctets (i4Index, &IfHCCount);
            FSAP_U8_ASSIGN_HI (&u8IfHCOutCount, IfHCCount.msn);
            FSAP_U8_ASSIGN_LO (&u8IfHCOutCount, IfHCCount.lsn);
            /* Converts the UINT8 value to string */
            FSAP_U8_2STR (&u8IfHCOutCount, (CHR1 *) & au1HCInCount);
            i4Quit = CliPrintf (CliHandle, "%-20s", au1HCInCount);

            MEMSET (au1HCInCount, 0, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_CLR (&u8IfHCOutCount);
            nmhGetIfHCOutUcastPkts (i4Index, &IfHCCount);
            FSAP_U8_ASSIGN_HI (&u8IfHCOutCount, IfHCCount.msn);
            FSAP_U8_ASSIGN_LO (&u8IfHCOutCount, IfHCCount.lsn);
            /* Converts the UINT8 value to string */
            FSAP_U8_2STR (&u8IfHCOutCount, (CHR1 *) & au1HCInCount);
            i4Quit = CliPrintf (CliHandle, "%-20s", au1HCInCount);

            MEMSET (au1HCInCount, 0, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_CLR (&u8IfHCOutCount);
            nmhGetIfHCOutMulticastPkts (i4Index, &IfHCCount);
            FSAP_U8_ASSIGN_HI (&u8IfHCOutCount, IfHCCount.msn);
            FSAP_U8_ASSIGN_LO (&u8IfHCOutCount, IfHCCount.lsn);
            /* Converts the UINT8 value to string */
            FSAP_U8_2STR (&u8IfHCOutCount, (CHR1 *) & au1HCInCount);
            i4Quit = CliPrintf (CliHandle, "%-20s", au1HCInCount);

            MEMSET (au1HCInCount, 0, CFA_CLI_U8_STR_LENGTH);
            FSAP_U8_CLR (&u8IfHCOutCount);
            nmhGetIfHCOutBroadcastPkts (i4Index, &IfHCCount);
            FSAP_U8_ASSIGN_HI (&u8IfHCOutCount, IfHCCount.msn);
            FSAP_U8_ASSIGN_LO (&u8IfHCOutCount, IfHCCount.lsn);
            /* Converts the UINT8 value to string */
            FSAP_U8_2STR (&u8IfHCOutCount, (CHR1 *) & au1HCInCount);
            i4Quit = CliPrintf (CliHandle, "%-20s\r\n", au1HCInCount);

        }
        i4PrevIndex = i4Index;

    }
    while ((u1Flag == 0)
           && (nmhGetNextIndexIfTable (i4PrevIndex, &i4Index) ==
               SNMP_SUCCESS) && (i4Quit == CLI_SUCCESS));

    CliPrintf (CliHandle, "\r\n");
    return (i4Quit);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    :CfaIvrDeleteIpAddressAndSubnetMask                  */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the secondary IP address of  */
/*                        an IVR interface                                   */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
/*                        u4IpAddr  - IPAddress                              */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIvrDeleteIpAddressAndSubnetMask (tCliHandle CliHandle, UINT4 u4IfIndex,
                                    UINT4 u4IpAddr, UINT4 u4IpSubnetMaskEntered)
{
    UINT4               u4PrimaryAddr = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4IpSubnetMask = 0;
    UINT4               u4ErrCode = 0;

    /* If No Primary IP configured on the Interface, exit */
    if (SNMP_FAILURE == nmhGetIfIpAddr ((INT4) u4IfIndex, &u4PrimaryAddr))
    {
        CliPrintf (CliHandle, "\r%% No IP configured on the interface\r\n");
        return CLI_FAILURE;
    }
    /* If Getting Subnet mask fails, exit */
    if (SNMP_FAILURE ==
        nmhGetIfSecondaryIpSubnetMask ((INT4) u4IfIndex, u4IpAddr,
                                       &u4IpSubnetMask))
    {
        CliPrintf (CliHandle,
                   "\r%% Deletion Failed, No Such IP configured \r\n");
        return CLI_FAILURE;
    }
    /* If Entered Subnet mask Does not match with actual Mask , exit */
    if (u4IpSubnetMask != u4IpSubnetMaskEntered)
    {
        CliPrintf (CliHandle, "\r%% Invalid Subnet mask for given IP\r\n");
        return CLI_FAILURE;
    }
    /* If primary IP is Requsted for Deletion Reject request */
    if (u4PrimaryAddr == u4IpAddr)
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot delete primary IP with secondary option\r\n");
        return CLI_FAILURE;
    }
    /* Else try Deleting the Ip Address */
    else
    {
        i4RetVal = CfaIvrDeleteIpAddress (CliHandle, u4IfIndex, u4IpAddr);
        /* If Deletion Failed Print Error and Exit */
        if (i4RetVal == CLI_FAILURE)
        {
            CLI_GET_ERR (&u4ErrCode);
            if (u4ErrCode == 0)
            {
                CliPrintf (CliHandle, "\r%% IP Address deletion failed\r\n");
            }
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaPrintSecVlanList 
 *
 *    Description         : This function prints the VLAN list associated with 
 *                          the Security L3 Vlan interface.
 *
 *    Input(s)            : None 
 *
 *    Output(s)           : pu1Temp - VLAN list associated with the VLAN
 *                                    interface. 
 *
 *    Global Variables Referred :None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :   None 
 *        
 *****************************************************************************/

INT4
CfaPrintSecVlanList (tCliHandle CliHandle)
{
    UINT1               u1VlanFlag = 0;
    UINT1               u1Count = 0;
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    UINT2               u2VlanId;
    UINT1              *pSecVlanList = NULL;
    INT4                i4Quit = CLI_SUCCESS;

    /* Allocating memory for Vlan List */
    pSecVlanList = UtilVlanAllocVlanListSize (sizeof (tSecVlanList));
    if (pSecVlanList == NULL)
    {
        CliPrintf (CliHandle, "\r\n Error in Allocating Memory for "
                   "Vlan List\r\n");
        return CLI_FAILURE;
    }

    MEMSET (pSecVlanList, 0, sizeof (tSecVlanList));

    if (CfaGetSecVlanList (pSecVlanList) == CFA_FAILURE)
    {
        /* Releasing memory for Vlan List */
        UtilVlanReleaseVlanListSize (pSecVlanList);
        return (CLI_FAILURE);
    }
    for (u2ByteInd = 0; u2ByteInd < CFA_SEC_VLAN_LIST_SIZE; u2ByteInd++)
    {
        if (pSecVlanList[u2ByteInd] != 0)
        {
            u1VlanFlag = pSecVlanList[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < CFA_SEC_VLANS_PER_BYTE) && (u1VlanFlag != 0));
                 u2BitIndex++)
            {
                if ((u1VlanFlag & CFA_SEC_BIT8) != 0)
                {
                    u2VlanId =
                        (UINT2) ((u2ByteInd * CFA_SEC_VLANS_PER_BYTE) +
                                 u2BitIndex + 1);
                    u1Count++;
                    if (!(u1Count % 15))
                    {
                        i4Quit = CliPrintf (CliHandle, "\r\n%d,", u2VlanId);
                    }
                    else
                    {
                        i4Quit = CliPrintf (CliHandle, "%d,", u2VlanId);
                    }
                }
                u1VlanFlag = (UINT1) (u1VlanFlag << 1);
            }
        }
        if (i4Quit == CLI_FAILURE)
        {
            break;
        }
    }
    /* Releasing memory for Vlan List */
    UtilVlanReleaseVlanListSize (pSecVlanList);

    return (i4Quit);
}

/*****************************************************************************/
/*     FUNCTION NAME    : CfaShowSecIntfVlan                                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays the L3 Vlan used for        */
/*                        Security processing                                */
/*                                                                           */
/*     INPUT            : CliHandle, CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
CfaShowSecIntfVlan (tCliHandle CliHandle)
{
    INT1                i1RetValue = 0;
    UINT4               u4SecIvrIfIndex = 0;
    tVlanId             VlanId = 0;

    UNUSED_PARAM (i1RetValue);

    CliPrintf (CliHandle,
               "L3 Interface used for Securing Bridged Traffic \r\n");
    CliPrintf (CliHandle, "--------------------------------------------- \r\n");

    u4SecIvrIfIndex = CfaGetSecIvrIndex ();
    i1RetValue = CfaGetVlanId (u4SecIvrIfIndex, &VlanId);

    if (u4SecIvrIfIndex != CFA_INVALID_INDEX)
    {
        CliPrintf (CliHandle, "Vlan %d \r\n", VlanId);
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : CfaShowSecVlanList                                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Sec Vlan list           */
/*                                                                           */
/*     INPUT            : CliHandle, CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowSecVlanList (tCliHandle CliHandle)
{
    INT4                i4RetValue = CLI_SUCCESS;
    CliPrintf (CliHandle, "Security VLAN-List\r\n");
    CliPrintf (CliHandle, "------------------\r\n");
    i4RetValue = CfaPrintSecVlanList (CliHandle);
    CliPrintf (CliHandle, "\r\n");
    return i4RetValue;
}

/*****************************************************************************/
/*     FUNCTION NAME    : CfaShowMgmtVlanList                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays the mgmt Vlan list          */
/*                                                                           */
/*     INPUT            : CliHandle, CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowMgmtVlanList (tCliHandle CliHandle)
{
    INT4                i4RetValue = CLI_SUCCESS;

#ifdef WGS_WANTED
    CliPrintf (CliHandle, "Management VLAN-List\r\n");
    i4RetValue = CfaPrintMgmtVlanList (CliHandle);
    CliPrintf (CliHandle, "\r\n");
#else
    CliPrintf (CliHandle, "%% This is supported only for L2 switches \r\n");
    i4RetValue = CLI_FAILURE;
#endif

    return i4RetValue;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowIpInt                                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays the details of the IP       */
/*                        interfaces in the system                           */
/*                                                                           */
/*     INPUT            : CliHandle, CLI Handler                             */
/*                        u4IfIndex - Interface index                        */
/*                        u4ContextId - context id                           */
/*                        pu1Alias - Interface name                          */
/*                        must be displayed                                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowIpInt (tCliHandle CliHandle, UINT4 u4Index, UINT4 u4ContextId,
              UINT1 *pu1Alias)
{

    INT4                i4RetVal = CFA_FAILURE;
    UINT4               u4PrevCxt = 0;

    if (u4ContextId != VCM_INVALID_VC)
    {
        i4RetVal =
            CfaShowIpIfaceInCxt (CliHandle, u4ContextId, u4Index, pu1Alias);
        return i4RetVal;
    }

    i4RetVal = VcmGetFirstActiveL3Context (&u4ContextId) == VCM_FAILURE;

    while (i4RetVal != VCM_FAILURE)
    {
        CfaShowIpIfaceInCxt (CliHandle, u4ContextId, u4Index, pu1Alias);
        u4PrevCxt = u4ContextId;
        i4RetVal = VcmGetNextActiveL3Context (u4PrevCxt, &u4ContextId);
    }
    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowIpIfaceInCxt                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays the details of the IP       */
/*                        interfaces in the system mapped to the context     */
/*                                                                           */
/*     INPUT            : CliHandle, CLI Handler                             */
/*                        u4ContextId - context id                           */
/*                        u4IfIndex - Interface index                        */
/*                        pu1Alias - Interface name                          */
/*                        must be displayed                                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowIpIfaceInCxt (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4IfIndex,
                     UINT1 *pu1Alias)
{

    UINT4               u4IfCxtId = 0;
    UINT4               au4IpIfaceList[IPIF_MAX_LOGICAL_IFACES + 1];
    UINT4               u4PrevIndex = 0;
    INT4                i4RetVal = CLI_FAILURE;
    UINT2               u2Var = 0;

    if (u4IfIndex != 0)
    {
        /* Context Id and interface is specified. Check if the specified
         * interface is mapped to the specified context. If so, display.
         * Else return.
         */
        VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4IfCxtId);
        if (u4ContextId != u4IfCxtId)
        {
            /* Interface is not mapped to the specified context */
            return CLI_SUCCESS;
        }
        /* Display the interface information */
        i4RetVal = CfaShowIpIfaceInfo (CliHandle, u4IfIndex);
        return i4RetVal;
    }

    if (pu1Alias == NULL)
    {
        /* Display all the interfaces mapped to the specified context */
        VcmGetCxtIpIfaceList (u4ContextId, &au4IpIfaceList[0]);
        if (au4IpIfaceList[0] == 0)
        {
            /* There is no interface mapped to this context */
            return CLI_SUCCESS;
        }

        for (u2Var = 1; ((u2Var <= au4IpIfaceList[0]) &&
                         (u2Var < IPIF_MAX_LOGICAL_IFACES + 1)); u2Var++)
        {
            if (CfaShowIpIfaceInfo (CliHandle, au4IpIfaceList[u2Var])
                == CLI_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
        return CLI_SUCCESS;
    }

    /* Display all the IVR interfaces with the given vlan id */

    i4RetVal = (INT4) CfaCliGetFirstIfIndexFromName (pu1Alias, &u4IfIndex);
    if (i4RetVal == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Vlan Inteface  \r\n");
    }

    while (i4RetVal != OSIX_FAILURE)
    {
        VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4IfCxtId);
        if (u4ContextId == u4IfCxtId)
        {
            if (CfaShowIpIfaceInfo (CliHandle, u4IfIndex) == CLI_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
        u4PrevIndex = u4IfIndex;
        i4RetVal = (INT4) CfaCliGetNextIfIndexFromName (pu1Alias, u4PrevIndex,
                                                        &u4IfIndex);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowIpIfaceInfo                                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays the details of the IP       */
/*                        interfaces in the switch                           */
/*                                                                           */
/*     INPUT            : CliHandle, CLI Handler                             */
/*                        u4IfIndex - the Vlan Interface for which details   */
/*                        must be displayed                                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowIpIfaceInfo (tCliHandle CliHandle, UINT4 u4IfIndex)
{

    CHR1               *pu1String = NULL;
    INT4                i4AdminStat = 0;
    INT4                i4OperStat = 0;
    INT4                i4AllocMeth = 0;
    INT4                i4Quit = 0;
    INT4                i4AllocProto = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4Index = 0;
    INT4                i4IfaceType = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4IpMask = 0;
    UINT4               u4BcastAddr = 0;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1IpId[MAX_MAC_LENGTH];
    UINT4               u4MaskBits = 0;
    UINT4               u4SecondaryAddr = 0;
    INT1               *piIfName;
    UINT1               u1IvrFlag = FALSE;
    UINT1               u1BridgedIfaceStatus = 0;
    UINT1               au1CxtName[VCM_ALIAS_MAX_LEN];
    UINT1               u1IfType = 0;
    UINT4               u4IfIpUnnumAssocIPIf = 0;
    INT4                i4IpIntfStats = 0;
    UINT1               u1AddrAllocMethod = CFA_IP_IF_MANUAL;

    i4Index = (INT4) u4IfIndex;
    piIfName = (INT1 *) &au1IfName[0];
    pu1String = (CHR1 *) & au1IpId[0];
    MEMSET (au1CxtName, 0, VCM_ALIAS_MAX_LEN);

    nmhGetIfType ((INT4) u4IfIndex, &i4IfaceType);
    /* Interfaces of type CFA_OTHER should not be displayed in Show commands */
    /* At present Connecting Interfaces used in Dual Unit Stacking are
       created with this type */
    if (i4IfaceType == CFA_OTHER)
    {
        return CFA_SUCCESS;
    }
    if ((gu4IsIvrEnabled == CFA_ENABLED) &&
        (CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus)
         == CFA_SUCCESS))
    {

        if (u1BridgedIfaceStatus != CFA_ENABLED
            || (CFA_CDB_IF_TYPE (u4IfIndex) == CFA_TUNNEL))
        {
            u1IvrFlag = TRUE;
        }
        else
        {
            u1IvrFlag = FALSE;
        }

    }
    if (u1IvrFlag == TRUE)
    {
        MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
        MEMSET (au1IpId, 0, MAX_MAC_LENGTH);

        nmhGetIfMainAdminStatus (i4Index, &i4AdminStat);
        nmhGetIfMainOperStatus (i4Index, &i4OperStat);
        nmhGetIfIpAddr (i4Index, &u4IpAddr);
        nmhGetIfIpSubnetMask (i4Index, &u4IpMask);
        nmhGetIfIpBroadcastAddr (i4Index, &u4BcastAddr);
        nmhGetIfIpAddrAllocMethod (i4Index, &i4AllocMeth);
        nmhGetIfIpAddrAllocProtocol (i4Index, &i4AllocProto);
        nmhGetIfIpUnnumAssocIPIf (i4Index, &u4IfIpUnnumAssocIPIf);
        nmhGetIfIpIntfStatsEnable (i4Index, &i4IpIntfStats);

        CfaCliGetIfName ((UINT4) i4Index, piIfName);
        CliPrintf (CliHandle, "\r\n%s", piIfName);

        if (i4AdminStat == CFA_IF_UP)
        {
            CliPrintf (CliHandle, " is up, ");
        }
        else
        {
            CliPrintf (CliHandle, " is down, ");
        }
        if (i4OperStat == CFA_IF_UP)
        {
            CliPrintf (CliHandle, "line protocol is up\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "line protocol is down\r\n");
        }
        if ((0 == u4IpAddr) && (0 != u4IfIpUnnumAssocIPIf))
        {
            /* interface is unnumbered so get ip address of assosiated interface */

            nmhGetIfIpAddr ((INT4) u4IfIpUnnumAssocIPIf, &u4IpAddr);

            CfaCliGetIfName (u4IfIpUnnumAssocIPIf, piIfName);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);
            CliPrintf (CliHandle,
                       "Interface is unnumbered. Associated ip interface-index/primaryaddress %s %s/",
                       piIfName, pu1String);
            nmhGetIfIpSubnetMask ((INT4) u4IfIpUnnumAssocIPIf, &u4IpMask);
        }
        else
        {

            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);
            CliPrintf (CliHandle, "Internet Address is %s/", pu1String);
        }
        MEMSET (au1IpId, 0, MAX_MAC_LENGTH);

        u4MaskBits = CliGetMaskBits (u4IpMask);
        CliPrintf (CliHandle, "%d\r\n", u4MaskBits);
        MEMSET (au1IpId, 0, MAX_MAC_LENGTH);

        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4BcastAddr);
        /* when 31 bit mask supported we need not to display Broadcast Address */
        if (CFA_31BIT_MASK != u4MaskBits)
        {
            i4Quit =
                CliPrintf (CliHandle, "Broadcast Address  %s\r\n", pu1String);
        }
        MEMSET (au1IpId, 0, MAX_MAC_LENGTH);

        CfaGetIfType ((UINT4) i4Index, &u1IfType);

        if (i4AllocMeth == CFA_IP_ALLOC_POOL)
        {
            i4Quit = CliPrintf (CliHandle, "IP address allocation method"
                                " is dynamic\r\n");
            if (i4AllocProto == CFA_PROTO_DHCP)
            {
                i4Quit = CliPrintf (CliHandle, "IP address allocation "
                                    "protocol is dhcp\r\n");
            }
            else if (i4AllocProto == CFA_PROTO_BOOTP)
            {
                i4Quit = CliPrintf (CliHandle, "IP address allocation "
                                    "protocol is bootp\r\n");
            }
            else if (i4AllocProto == CFA_PROTO_RARP)
            {
                i4Quit = CliPrintf (CliHandle, "IP address allocation "
                                    "protocol is rarp\r\n");
            }
        }
        else if ((i4AllocMeth == CFA_IP_ALLOC_NEGO) && (u1IfType == CFA_PPP))
        {
            i4Quit = CliPrintf (CliHandle, "IP address allocation method"
                                " is dynamic\r\n");
        }
        else if ((CfaIsMgmtPort (u4IfIndex) == TRUE) &&
                 (CfaIsDualOobEnabled () == OSIX_TRUE))
        {

            /* If Node0 SecondaryIp is set, display Ip address and mask */
            u4SecondaryAddr = 0;
            nmhGetIfOOBNode0SecondaryIpAddress (&u4SecondaryAddr);
            if (u4SecondaryAddr != 0)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SecondaryAddr);
                CliPrintf (CliHandle, "Node0 Secondary Address  %s/",
                           pu1String);
                nmhGetIfOOBNode0SecondaryIpMask (&u4IpMask);
                u4MaskBits = CliGetMaskBits (u4IpMask);
                CliPrintf (CliHandle, "%d\r\n", u4MaskBits);
            }

            /* If Node1 SecondaryIp is set, display Ip address and mask */
            u4SecondaryAddr = 0;
            nmhGetIfOOBNode1SecondaryIpAddress (&u4SecondaryAddr);
            if (u4SecondaryAddr != 0)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SecondaryAddr);
                CliPrintf (CliHandle, "Node1 Secondary Address  %s/",
                           pu1String);
                nmhGetIfOOBNode1SecondaryIpMask (&u4IpMask);
                u4MaskBits = CliGetMaskBits (u4IpMask);
                CliPrintf (CliHandle, "%d\r\n", u4MaskBits);
            }
        }
        else
        {
            /* Display the secondary IP address configured over the
             * interface */
            u4SecondaryAddr = 0;
            while ((nmhGetNextIndexIfSecondaryIpAddressTable (i4Index,
                                                              &i4NextIfIndex,
                                                              u4SecondaryAddr,
                                                              &u4SecondaryAddr)
                    == SNMP_SUCCESS) && (i4Index == i4NextIfIndex))
            {
                CfaGetAddrAllocMethodForSecIp ((UINT4) i4Index,
                                               u4SecondaryAddr,
                                               &u1AddrAllocMethod);

                if (u1AddrAllocMethod == CFA_IP_IF_MANUAL)
                {
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SecondaryAddr);
                    CliPrintf (CliHandle, "Secondary Address  %s/", pu1String);
                    MEMSET (au1IpId, 0, MAX_MAC_LENGTH);

                    nmhGetIfSecondaryIpSubnetMask (i4Index, u4SecondaryAddr,
                                                   &u4IpMask);
                    u4MaskBits = CliGetMaskBits (u4IpMask);
                    CliPrintf (CliHandle, "%d\r\n", u4MaskBits);
                    MEMSET (au1IpId, 0, MAX_MAC_LENGTH);
                }
                else if (u1AddrAllocMethod == CFA_IP_IF_VIRTUAL)
                {
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SecondaryAddr);
                    CliPrintf (CliHandle, "Secondary Address  %s/", pu1String);
                    MEMSET (au1IpId, 0, MAX_MAC_LENGTH);

                    nmhGetIfSecondaryIpSubnetMask (i4Index, u4SecondaryAddr,
                                                   &u4IpMask);

                    u4MaskBits = CliGetMaskBits (u4IpMask);
                    CliPrintf (CliHandle, "%d (Virtual)\r\n", u4MaskBits);
                    MEMSET (au1IpId, 0, MAX_MAC_LENGTH);
                }
            }
        }
        if (i4IpIntfStats == CFA_ENABLED)
        {
            CliPrintf (CliHandle, "Vlan counters enabled \n");
        }
        else if (i4IpIntfStats == CFA_DISABLED)
        {
            CliPrintf (CliHandle, "Vlan counters disabled \n");
        }
    }
    else
    {
        i4Quit = CLI_SUCCESS;
    }
    return i4Quit;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : InterfaceShowRunningConfig                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays the currently operating     */
/*                        interface configurations  in the system            */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the CLI Context              */
/*                        i4Index   - Interface Index                        */
/*                        u4Module   - Module for which the configuration to */
/*                                     be displayed                          */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
InterfaceShowRunningConfig (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Module)
{
#ifdef DHCP_RLY_WANTED
    UINT4               u4ContextId = 0;
    UINT4               u4Port = 0;
#endif
    INT4                i4PrevIndex = 0;
    INT4                i4RowStatus = 0;
    UINT1               u1Flag = 1;
#ifdef LA_WANTED
    UINT4               u4PortChannelIndex = 0;
#endif
#ifdef EOAM_WANTED
    BOOL1               b1FlgModSpec = OSIX_FALSE;
#endif
    UINT4               u4IcchIfIndex = 0;
    UINT2               u2VlanId = 0;
#ifdef RMON_WANTED
    UINT1               u1Flag1;
#endif
    INT4                i4IfType = 0;
    UINT1               u1HeadFlag = OSIX_FALSE;
#if defined (MPLS_WANTED) && defined (IP6_WANTED)
    UINT4               u4MplsIfIndex = 0;
#endif
#ifdef SYNCE_WANTED
    INT4                i4RetVal = 0;
    INT4                i4SynceIndex = 0;
#endif
#ifdef ECFM_WANTED
    UINT1               u1EcfmFlag = OSIX_FALSE;
#endif
    INT1                i1VlanRetVal = CFA_FAILURE;
    UINT1               u1TempBangStatus = FALSE;
    CliRegisterLock (CliHandle, CfaLock, CfaUnlock);
    CFA_LOCK ();

    /* When no interface index is specified */

    if (i4Index == 0)
    {
        if (nmhGetFirstIndexIfTable (&i4Index) == SNMP_FAILURE)
        {

            CFA_UNLOCK ();
            CliUnRegisterLock (CliHandle);

            return CLI_SUCCESS;
        }

        u1Flag = 0;
        u1TempBangStatus = TRUE;

        /* This flag is used to indicate that the details must be displayed for
         * all interfaces */
    }
#ifdef SYNCE_WANTED
    i4RetVal = nmhGetFirstIndexFsSynceIfTable (&i4SynceIndex);
#endif

    do
    {
        i1VlanRetVal = CfaGetVlanId ((UINT4) i4Index, &u2VlanId);
        CfaIcchGetIcclIfIndex (&u4IcchIfIndex);

        if ((CfaIcchIsIcclVlan (u2VlanId) == OSIX_SUCCESS) ||
            (u4IcchIfIndex == (UINT4) i4Index) || (i1VlanRetVal == CFA_FAILURE))
        {
            i4PrevIndex = i4Index;
            continue;
        }

        nmhGetIfMainRowStatus (i4Index, &i4RowStatus);

        /* if the interface is CFA_MPLS_TUNNEL or CFA_MPLS or CFA_TELINK 
         * then continue */
        if (nmhGetIfMainType (i4Index, &i4IfType) == SNMP_SUCCESS)
        {
            if ((i4IfType == CFA_MPLS) || (i4IfType == CFA_MPLS_TUNNEL) ||
                (i4IfType == CFA_TELINK))
            {
                i4PrevIndex = i4Index;
                continue;
            }
        }

        if (i4RowStatus == CFA_RS_ACTIVE)
        {
            u1HeadFlag = OSIX_FALSE;

            if (u4Module == ISS_MPLS_SHOW_RUNNING_CONFIG
                && i4IfType == CFA_LOOPBACK)
            {

                i4PrevIndex = i4Index;
                continue;
            }

            InterfaceShowRunningConfigDetails (CliHandle, i4Index, u4Module,
                                               &u1HeadFlag);

#if defined (MPLS_WANTED) && defined (IP6_WANTED)
            if (u4Module == ISS_MPLS_SHOW_RUNNING_CONFIG)
            {
                if (CfaUtilGetMplsIfFromIfIndex
                    ((UINT4) i4Index, &u4MplsIfIndex, FALSE) != CFA_FAILURE)
                {
                    Ipv6ShowRunningConfigInterfaceDetails (CliHandle, i4Index,
                                                           &u1HeadFlag);
                }
            }
#endif
            if (u4Module != ISS_MPLS_SHOW_RUNNING_CONFIG)
            {
                CFA_UNLOCK ();
                CliUnRegisterLock (CliHandle);

#ifdef SYNCE_WANTED
                if (i4RetVal == SNMP_SUCCESS)
                {
                    SynceShowRunningConfigFsSynceIfTable (CliHandle, u4Module,
                                                          i4Index);
                }
#endif

#ifdef VLAN_WANTED
#ifdef EVB_WANTED

                if (u4Module == ISS_INTERFACE_SHOW_RUNNING_CONFIG)
                {
                    VlanEvbShowRunningConfigInterface (CliHandle, i4Index);
                }
#endif

                if (u4Module == ISS_INTERFACE_SHOW_RUNNING_CONFIG)
                {
                    VlanGarpShowRunningConfigInterface (CliHandle,
                                                        (UINT4) i4Index,
                                                        &u1HeadFlag);
                }
#endif

#ifdef NPAPI_WANTED
#ifdef ISS_WANTED
                IssIntfShowRunningConfig (CliHandle, i4Index, &u1HeadFlag);
#ifndef MRVLLS
                AclShowRunningConfigInterfaceDetails (CliHandle, i4Index);
#endif

#endif
#ifdef DIFFSRV_WANTED
#ifdef SWC
                DiffservShowRunningConfigInterfaceDetails (CliHandle, i4Index);
#endif
#endif
#endif /* NPAPI_WANTED */

#ifdef IP_WANTED
                FsIpShowRunningConfigInterfaceDetails (CliHandle, i4Index);
#endif
                if (u4Module == ISS_INTERFACE_SHOW_RUNNING_CONFIG)
                {
                    IpvxShowRunningConfigIfDetails (CliHandle, i4Index);
                }
#ifdef IGMP_WANTED
                IgmpShowRunningConfigInterfaceDetails (CliHandle, i4Index);
#endif
#ifdef MLD_WANTED
                MldShowRunningConfigInterfaceDetails (CliHandle, i4Index);
#endif
#ifdef IGMPPRXY_WANTED
                IgmpProxyShowRunningConfigUpIfaceDetails (CliHandle, i4Index);
#endif
#ifdef PNAC_WANTED
                PnacShowRunningConfigInterfaceDetails (CliHandle, i4Index);
#endif
#ifdef RMON_WANTED
                u1Flag1 = 1;
                RmonShowRunningConfigInterface (CliHandle, u1Flag1, i4Index);
#endif

#ifdef RSTP_WANTED
                RstpShowRunningConfigInterfaceDetails (CliHandle, i4Index,
                                                       &u1HeadFlag);
#endif
#ifdef ELMI_WANTED
                ElmShowRunningConfigInterfaceDetails (CliHandle, i4Index);
#endif
#ifdef MSTP_WANTED

                MstpShowRunningConfigInterfaceDetails (CliHandle, i4Index,
                                                       &u1HeadFlag);
                MstpShowRunningConfigInstInterface (CliHandle, i4Index,
                                                    &u1HeadFlag);
#endif
#ifdef ECFM_WANTED
                if (u4Module != ISS_SHOW_ALL_RUNNING_CONFIG)
                {
                    u1EcfmFlag = OSIX_FALSE;
                    EcfmShowRunningConfigInterfaceDetails (CliHandle, i4Index,
                                                           &u1EcfmFlag);
                }
#endif
#ifdef PBB_WANTED
                PbbShowRunningConfigInterfaceDetails (CliHandle, i4Index);
#endif
#ifdef PVRST_WANTED
                PvrstShowRunningConfigInterfaceDetails (CliHandle, i4Index,
                                                        &u1HeadFlag);
                PvrstShowRunningConfigInstInterface (CliHandle, i4Index,
                                                     &u1HeadFlag);
#endif
#ifdef EOAM_WANTED

                EoamCliShowRunningConfigIfInfo (CliHandle, i4Index,
                                                b1FlgModSpec);

#ifdef EOAM_FM_WANTED
                FmCliShowRunningConfigIfInfo (CliHandle, i4Index, b1FlgModSpec);
#endif /* EOAM_FM_WANTED */
#endif /* EOAM_WANTED */

#ifdef LA_WANTED
                if (u4Module == ISS_INTERFACE_SHOW_RUNNING_CONFIG)
                {
                    LaShowRunningConfigPhysicalInterfaceDetails (CliHandle,
                                                                 i4Index,
                                                                 u4PortChannelIndex);

                    LaShowRunningConfigPortChannelInterfaceDetails (CliHandle,
                                                                    i4Index);
                }

#endif

#ifdef MRP_WANTED
                MrpSrcShowRunningConfigIfaceDetails (CliHandle, i4Index);
#endif
#ifdef OSPF_WANTED
                if (u4Module == ISS_INTERFACE_SHOW_RUNNING_CONFIG)
                {
                    OspfShowRunningConfigInterfaceDetails (CliHandle,
                                                           (UINT4) i4Index);
                }
#endif
#ifdef RIP_WANTED
                if (u4Module == ISS_INTERFACE_SHOW_RUNNING_CONFIG)
                {
                    RipShowRunningConfigInterfaceCxt (CliHandle,
                                                      (INT4) u4ContextId);
                }
#endif
#ifdef DHCP_RLY_WANTED
                u4ContextId = 0;
                if (NetIpv4GetPortFromIfIndex ((UINT4) i4Index, &u4Port)
                    != NETIPV4_FAILURE)
                {
                    if (NetIpv4GetCxtId (u4Port, &u4ContextId) !=
                        NETIPV4_FAILURE)
                    {
                        DhrlShowRunningConfigInterfaceDetails (CliHandle,
                                                               (INT4)
                                                               u4ContextId,
                                                               i4Index);
                    }
                }
#endif

#ifdef IP6_WANTED
                u1HeadFlag = OSIX_FALSE;
                if (u4Module != ISS_IP_SHOW_RUNNING_CONFIG)
                {
                    Ipv6ShowRunningConfigInterfaceDetails (CliHandle, i4Index,
                                                           &u1HeadFlag);
                }
#endif

#ifdef DCBX_WANTED
                if (u4Module == ISS_INTERFACE_SHOW_RUNNING_CONFIG)
                {
                    DCBXCliShowRunningConfig (CliHandle, i4Index, OSIX_TRUE);
                }
#endif
#ifdef NAT_WANTED
                if (u4Module == ISS_INTERFACE_SHOW_RUNNING_CONFIG)
                {
                    NatShowRunningConfigInterface (CliHandle, i4Index);
                }
#endif
#ifdef DHCP6_CLNT_WANTED
                if (u4Module == ISS_INTERFACE_SHOW_RUNNING_CONFIG)
                {
                    D6ClCliShowRunningConfigInterfaceDetails (CliHandle,
                                                              i4Index);
                }
#endif
#ifdef DHCP6_SRV_WANTED
                if (u4Module == ISS_INTERFACE_SHOW_RUNNING_CONFIG)
                {
                    D6SrCliShowRunningConfigInterfaceDetails (CliHandle,
                                                              i4Index);
                }
#endif
#ifdef DHCP6_RLY_WANTED
                if (u4Module == ISS_INTERFACE_SHOW_RUNNING_CONFIG)
                {
                    D6RlCliShowRunningConfigInterfaceDetails (CliHandle,
                                                              i4Index);
                }
#endif
#ifdef RIP6_WANTED
                Rip6ShowRunningConfigInterfaceDetails (CliHandle, i4Index,
                                                       OSIX_TRUE);
#endif

#ifdef PIM_WANTED
                PimShowRunningConfigInterfaceDetails (CliHandle, i4Index,
                                                      IPVX_ADDR_FMLY_IPV4);
#endif

#ifdef PIMV6_WANTED
                PimShowRunningConfigInterfaceDetails (CliHandle, i4Index,
                                                      IPVX_ADDR_FMLY_IPV6);
#endif

#ifdef DVMRP_WANTED
                DvmrpShowRunningConfigInterfaceDetails (CliHandle, i4Index);
#endif
#ifdef OSPF3_WANTED
                Ospfv3ShowRunningConfigInterfaceDetails (CliHandle, i4Index);
#endif
#ifdef IGS_WANTED
                SnoopShowRunningConfigInterfaceDetails (CliHandle, i4Index,
                                                        IPVX_ADDR_FMLY_IPV4);
#endif
#ifdef MLDS_WANTED
                SnoopShowRunningConfigInterfaceDetails (CliHandle, i4Index,
                                                        IPVX_ADDR_FMLY_IPV6);
#endif
#ifdef POE_WANTED
                PoeShowRunningConfigInterfaceDetails (CliHandle, i4Index);
#endif
                if (u1TempBangStatus == TRUE)
                {
                    CliPrintf (CliHandle, "! \r\n");
                }
                CliRegisterLock (CliHandle, CfaLock, CfaUnlock);
                CFA_LOCK ();
            }
        }

        i4PrevIndex = i4Index;
    }
    while ((u1Flag == 0)
           && (nmhGetNextIndexIfTable (i4PrevIndex, &i4Index) == SNMP_SUCCESS));

#ifdef ISS_WANTED
    if (u4Module == ISS_INTERFACE_SHOW_RUNNING_CONFIG)
    {
        nmhGetIfMainRowStatus (i4Index, &i4RowStatus);

        /* if the interface is CFA_MPLS_TUNNEL or CFA_MPLS or CFA_TELINK 
         *then continue */
        if (nmhGetIfMainType (i4Index, &i4IfType) == SNMP_SUCCESS)
        {
            if ((i4IfType == CFA_MPLS) || (i4IfType == CFA_MPLS_TUNNEL) ||
                (i4IfType == CFA_TELINK))
            {
                CFA_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_SUCCESS;
            }
        }

        if (i4RowStatus == CFA_RS_ACTIVE)
        {
            IssPICliShowRunningConfig (CliHandle, (UINT4) i4Index);
        }
    }
    else
    {
        if (nmhGetFirstIndexIfTable (&i4Index) == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_SUCCESS;
        }
        do
        {
            i1VlanRetVal = CfaGetVlanId ((UINT4) i4Index, &u2VlanId);
            CfaIcchGetIcclIfIndex (&u4IcchIfIndex);

            if ((CfaIcchIsIcclVlan (u2VlanId) == OSIX_SUCCESS) ||
                (u4IcchIfIndex == (UINT4) i4Index) ||
                (i1VlanRetVal == CFA_FAILURE))
            {
                i4PrevIndex = i4Index;
                continue;
            }
            nmhGetIfMainRowStatus (i4Index, &i4RowStatus);

            /* if the interface is CFA_MPLS_TUNNEL or CFA_MPLS or CFA_TELINK 
             *then continue */
            if (nmhGetIfMainType (i4Index, &i4IfType) == SNMP_SUCCESS)
            {
                if ((i4IfType == CFA_MPLS) || (i4IfType == CFA_MPLS_TUNNEL) ||
                    (i4IfType == CFA_TELINK))
                {
                    i4PrevIndex = i4Index;
                    continue;
                }
            }

            if (i4RowStatus == CFA_RS_ACTIVE)
            {
                IssPICliShowRunningConfig (CliHandle, (UINT4) i4Index);
            }

            i4PrevIndex = i4Index;
        }
        while (nmhGetNextIndexIfTable (i4PrevIndex, &i4Index) == SNMP_SUCCESS);
    }
#endif
#ifdef FSB_WANTED
    FsbShowRunningConfigInterfaceDetails (CliHandle, i4Index);
#endif

#ifdef QOSX_WANTED
    QoSShowRunningConfigInterfaceDetails (CliHandle, i4Index);
#endif
    CFA_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : InterfaceShowRunningConfigMappingDetails           */
/*                                                                           */
/*     DESCRIPTION      : This function displays the currently operating     */
/*                        interface configurations  in the system            */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the CLI Context              */
/*                        i4Index   - Interface Index                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
InterfaceShowRunningConfigMappingDetails (tCliHandle CliHandle, INT4 i4Index)
{
    tCfaIfInfo          CfaIfInfo;
    INT4                i4PrevIndex = 0;
    INT4                i4RowStatus = 0;
    INT4                i4IfaceType = 0;
    INT4                i4AdminKey = 0;
    INT4                i4IfType = 0;
    INT4                i4BridgedIfaceStatus = 0;
    INT4                i4AdminStatus = 0;
    INT4                i4IfMtu;
    UINT4               u4IcchIfIndex = 0;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    INT1               *piIfName;
    CHR1               *token;
    UINT1               u1Flag = 1;
    UINT1               u1BridgedIfaceStatus;
    UINT2               u2VlanId = 0;
    UINT1               u1ShowFlag = FALSE;
    UINT1               u1SubType = 0;
    INT4                i4BridgePortType = CFA_INVALID_BRIDGE_PORT;
    UINT4               u4BridgeMode = L2IWF_INVALID_BRIDGE_MODE;
    UINT4               u4ContextId = L2IWF_DEFAULT_CONTEXT;
    UINT2               u2LocalPortId = 0;
    UINT4               u4IfMapVcNum = 0;
    INT4                i4LinkUpEnableStatus = 0;
    UINT4               u4LinkUpDelayTimer = 0;
    INT1                i1RetVal = CFA_FAILURE;

    CliRegisterLock (CliHandle, CfaLock, CfaUnlock);
    CFA_LOCK ();

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    piIfName = (INT1 *) &au1IfName[0];
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    /* When no interface index is specified */

    if (i4Index == 0)
    {
        if (nmhGetFirstIndexIfTable (&i4Index) == SNMP_FAILURE)
        {

            CFA_UNLOCK ();
            CliUnRegisterLock (CliHandle);

            return CLI_SUCCESS;
        }

        u1Flag = 0;

        /* This flag is used to indicate that the details must be displayed for
         * all interfaces */
    }
    do
    {
        i1RetVal = CfaGetVlanId ((UINT4) i4Index, &u2VlanId);
        if (i1RetVal == CFA_FAILURE)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "CfaGetVlanId Failed.\n");
        }

        CfaIcchGetIcclIfIndex (&u4IcchIfIndex);

        if ((CfaIcchIsIcclVlan (u2VlanId) == OSIX_SUCCESS) ||
            (u4IcchIfIndex == (UINT4) i4Index) || (i1RetVal == CFA_FAILURE))
        {
            i4PrevIndex = i4Index;
            continue;
        }

        if ((CfaIsILanInterface ((UINT4) i4Index) == CFA_FALSE) &&
            (CfaIsIfEntryProgrammingAllowed ((UINT4) i4Index) == CFA_TRUE))
        {
            nmhGetIfMainRowStatus (i4Index, &i4RowStatus);

            /* if the interface is CFA_MPLS_TUNNEL or CFA_MPLS or CFA_TELINK
             * then continue */
            if (nmhGetIfMainType (i4Index, &i4IfType) == SNMP_SUCCESS)
            {
                if ((i4IfType == CFA_MPLS) || (i4IfType == CFA_MPLS_TUNNEL) ||
                    (i4IfType == CFA_TELINK))
                {
                    i4PrevIndex = i4Index;
                    continue;
                }
            }

            if ((VcmGetL2Mode () == VCM_MI_MODE) &&
                (VcmIsIfMapExist ((UINT4) i4Index) == VCM_TRUE))
            {
                if (VcmGetIfMapVcId ((UINT4) i4Index, &u4IfMapVcNum) ==
                    VCM_SUCCESS)
                {
                    if ((VcmIsL3Interface ((UINT4) i4Index) == VCM_FALSE) ||
                        (u4IfMapVcNum != VCM_DEFAULT_CONTEXT))
                    {
                        u1ShowFlag = TRUE;
                    }
                }
            }
#ifdef VCM_WANTED
            VcmGetContextInfoFromIfIndex ((UINT4) i4Index, &u4ContextId,
                                          &u2LocalPortId);
#endif
            L2IwfGetBridgeMode (u4ContextId, &u4BridgeMode);

            /*Bridge Port Type */
            if (u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE ||
                u4BridgeMode == L2IWF_PBB_ICOMPONENT_BRIDGE_MODE ||
                u4BridgeMode == L2IWF_PBB_BCOMPONENT_BRIDGE_MODE ||
                u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE)
            {
                nmhGetIfMainBrgPortType (i4Index, &i4BridgePortType);
                if (i4BridgePortType == CFA_PROVIDER_NETWORK_PORT)
                {
                    u1ShowFlag = TRUE;
                }
            }
            CfaGetIfBridgedIfaceStatus ((UINT4) i4Index, &u1BridgedIfaceStatus);
            nmhGetIfType (i4Index, &i4IfaceType);
            if (((u1ShowFlag == FALSE)
                 && (u1BridgedIfaceStatus == CFA_DISABLED))
                || ((u1ShowFlag == FALSE) && (i4IfaceType == CFA_ENET)))
            {
                /* router port MTU or physical interface */
                CfaCliConfGetIfName ((UINT4) i4Index, piIfName);
                nmhGetIfMtu (i4Index, &i4IfMtu);
                if (i4IfMtu != CFA_ENET_MTU)
                {
                    CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                    CliPrintf (CliHandle, "shutdown\r\n");
                    CliPrintf (CliHandle, " mtu %d \r\n", i4IfMtu);
                    CliPrintf (CliHandle, "!\r\n");
                }

            }

            if ((i4RowStatus == CFA_RS_ACTIVE) && (u1ShowFlag == TRUE))
            {
                nmhGetIfAdminStatus (i4Index, &i4AdminStatus);
                nmhGetIfType (i4Index, &i4IfaceType);
                CfaGetIfSubType ((UINT4) i4Index, &u1SubType);
                nmhGetIfMainExtLinkUpEnabledStatus (i4Index,
                                                    &i4LinkUpEnableStatus);
                nmhGetIfMainExtLinkUpDelayTimer (i4Index, &u4LinkUpDelayTimer);

                if (i4IfaceType == CFA_ENET)
                {
                    nmhGetIfIvrBridgedIface (i4Index, &i4BridgedIfaceStatus);
                    if (i4BridgedIfaceStatus == CFA_ENABLED)
                    {
                        if ((VcmGetL2ModeExt () == VCM_MI_MODE) ||
                            (VcmGetL2ModeExt () == VCM_SI_MODE))
                        {
                            CfaCliConfGetIfName ((UINT4) i4Index, piIfName);
                            CliPrintf (CliHandle, "interface %s\r\n", piIfName);

                            if (i4LinkUpEnableStatus ==
                                CFA_LINKUP_DELAY_ENABLED)
                            {
                                CliPrintf (CliHandle, "linkup-delay\r\n");
                                if (u4LinkUpDelayTimer !=
                                    CFA_LINKUP_DELAY_DEFAULT_TIMER)
                                {
                                    CliPrintf (CliHandle,
                                               "linkup-delay timer %d\r\n",
                                               u4LinkUpDelayTimer);
                                }
                            }

                        }
                    }
                    else
                    {
                        /* router port */
                        VcmGetContextIdFromCfaIfIndex ((UINT4) i4Index,
                                                       &u4ContextId);
                        if ((u4ContextId != VCM_DEFAULT_CONTEXT) &&
                            (VcmGetL3ModeExt () == VCM_MI_MODE))
                        {
                            CfaCliConfGetIfName ((UINT4) i4Index, piIfName);
                            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                        }
                    }
                    nmhGetIfMtu (i4Index, &i4IfMtu);
                    if (i4IfMtu != CFA_ENET_MTU)
                    {
                        CliPrintf (CliHandle, "shutdown\r\n");
                        CliPrintf (CliHandle, " mtu %d \r\n", i4IfMtu);
                    }
                }
                else if ((i4IfaceType == CFA_LAGG) &&
                         ((VcmGetL2ModeExt () == VCM_MI_MODE) ||
                          VcmGetL2ModeExt () == VCM_SI_MODE))

                {
                    CfaCliGetIfName ((UINT4) i4Index, piIfName);
                    token = STRTOK (piIfName, "po");
                    if (token != NULL)
                    {
                        i4AdminKey = STRTOL (token, NULL, BASE_DECIMAL);
                        CliPrintf (CliHandle, "interface port-channel %d\r\n",
                                   i4AdminKey);
                    }
                    nmhGetIfMtu (i4Index, &i4IfMtu);
                    if (i4IfMtu != CFA_ENET_MTU)
                    {
                        CliPrintf (CliHandle, "shutdown\r\n");
                        CliPrintf (CliHandle, " mtu %d \r\n", i4IfMtu);
                    }

                }
                else if (((i4IfaceType == CFA_BRIDGED_INTERFACE)
                          || (i4IfaceType == CFA_PIP)) &&
                         (VcmGetL2ModeExt () == VCM_MI_MODE))
                {
                    CfaCliConfGetIfName ((UINT4) i4Index, piIfName);
                    CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                }
                else if ((i4IfaceType == CFA_L3IPVLAN) &&
                         (VcmGetL3ModeExt () == VCM_MI_MODE))
                {
                    VcmGetContextIdFromCfaIfIndex ((UINT4) i4Index,
                                                   &u4ContextId);
                    if (u4ContextId != VCM_DEFAULT_CONTEXT)
                    {
                        i1RetVal = CfaGetVlanId ((UINT4) i4Index, &u2VlanId);
                        if (i1RetVal == CFA_FAILURE)
                        {
                            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                     "CfaGetVlanId Failed.\n");
                        }
                        CliPrintf (CliHandle, "interface vlan %d\r\n",
                                   u2VlanId);
                    }
                }
                else if ((i4IfaceType == CFA_L3SUB_INTF) &&
                         (VcmGetL3ModeExt () == VCM_MI_MODE))

                {
                    VcmGetContextIdFromCfaIfIndex ((UINT4) i4Index,
                                                   &u4ContextId);
                    if (u4ContextId != VCM_DEFAULT_CONTEXT)
                    {
                        CfaCliConfGetIfName ((UINT4) i4Index, piIfName);
                        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                    }
                }
                else if (i4IfaceType == CFA_PSEUDO_WIRE)
                {
                    nmhGetIfIvrBridgedIface (i4Index, &i4BridgedIfaceStatus);
                    if (i4BridgedIfaceStatus == CFA_ENABLED)
                    {
                        if (VcmGetL2ModeExt () == VCM_MI_MODE)
                        {
                            CfaCliConfGetIfName ((UINT4) i4Index, piIfName);
                            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                        }
                    }
                }
                else if (i4IfaceType == CFA_PROP_VIRTUAL_INTERFACE)
                {
                    if (u1SubType == CFA_SUBTYPE_AC_INTERFACE)
                    {
                        nmhGetIfIvrBridgedIface (i4Index,
                                                 &i4BridgedIfaceStatus);
                        if (i4BridgedIfaceStatus == CFA_ENABLED)
                        {
                            if (VcmGetL2ModeExt () == VCM_MI_MODE)
                            {
                                CfaCliConfGetIfName ((UINT4) i4Index, piIfName);
                                CliPrintf (CliHandle, "interface %s\r\n",
                                           piIfName);
                            }
                        }
                    }
                }
                CFA_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                if (i4IfaceType != CFA_VXLAN_NVE)
                {
#ifdef VCM_WANTED
                    VcmShowInterfaceMapping (CliHandle, (UINT4) i4Index);
#endif
                    CliRegisterLock (CliHandle, CfaLock, CfaUnlock);
                    CFA_LOCK ();
                    if ((i4AdminStatus == CFA_IF_UP) &&
                        (!((i4IfaceType == CFA_ENET)
                           || (i4IfaceType == CFA_LAGG)))
                        && (u4ContextId != VCM_DEFAULT_CONTEXT))
                    {
                        CliPrintf (CliHandle, " no shutdown\r\n");
                    }
                }
                if (((i4IfaceType == CFA_ENET) || (i4IfaceType == CFA_LAGG)
                     || (i4IfaceType == CFA_BRIDGED_INTERFACE)
                     || (i4IfaceType == CFA_PIP)
                     || (i4IfaceType == CFA_L3IPVLAN)
                     || (i4IfaceType == CFA_PSEUDO_WIRE)
                     || (i4IfaceType == CFA_VXLAN_NVE)
                     || (i4IfaceType == CFA_TAP)
                     || (i4IfaceType == CFA_L3SUB_INTF)
                     || ((i4IfaceType == CFA_PROP_VIRTUAL_INTERFACE) &&
                         (u1SubType == CFA_SUBTYPE_AC_INTERFACE))))
                {
                    CfaShowBridgePortType (CliHandle, i4Index);
                    CliPrintf (CliHandle, "!\r\n");
                }
                u1ShowFlag = FALSE;
            }
        }
        i4PrevIndex = i4Index;
    }
    while ((u1Flag == 0)
           && (nmhGetNextIndexIfTable (i4PrevIndex, &i4Index) == SNMP_SUCCESS));

    CFA_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : InterfaceShowRunningConfigDetails                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalar objects in CFA       */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4IfIndex - InterfaceIndex                         */
/*                        u4Module   - Module for which the configuration to */
/*                                     be displayed                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
InterfaceShowRunningConfigDetails (tCliHandle CliHandle, INT4 i4IfIndex,
                                   UINT4 u4Module, UINT1 *pu1Flag)
{

    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1PhyIfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               u1BridgedIfaceStatus = CFA_DISABLED;
    UINT1               au1IpId[MAX_MAC_LENGTH];
    UINT1               au1MaskId[MAX_MAC_LENGTH];
    UINT1               au1IfAlias[CFA_MAX_IFALIAS_LENGTH];
    UINT4               u4ContextId = L2IWF_DEFAULT_CONTEXT;
    UINT4               u4IpAddr = 0;
    UINT4               u4IpMask = 0;
    UINT4               u4SecondaryAddr;
    UINT4               u4IcchIfIndex = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2LoopbackId;
    INT4                i4PortVlanId = 0;
    INT4                i4IfMtu = 0;
    INT4                i4AdminStatus = 0;
    INT4                i4AllocMethod = 0;
    INT4                i4AllocProto = 0;
    INT1                i1RetValue = 0;
    INT4                i4ProxyArpStatus = 0;
    UINT1               au1IfMainDesc[CFA_MAX_INTF_DESC_LEN];
    tSNMP_OCTET_STRING_TYPE IfDescString;

    INT4                i4LinkTrap;
    INT4                i4PauseAdminMode;
    INT4                i4NextIfIndex;
    INT4                i4IfaceType;
    INT4                i4AdminKey = 0;
    INT4                i4UfdOperStatus = 0;
    CHR1               *pu1String = NULL;
    CHR1               *pu1MaskString = NULL;
    INT1               *piIfName;
    CHR1               *token;
#ifdef MPLS_WANTED
    UINT4               u4MplsIfIndex = 0;
    UINT1               u1Display = OSIX_FALSE;
#endif
    UINT1               u1ShowConfig = OSIX_TRUE;
    UINT1               au1L2CxtName[VCM_ALIAS_MAX_LEN];
    INT4                i4NextVlan, i4PrevIfIndex, i4PrevVlan;
    INT4                i4ExistingMode = PAUSE_ENABLED_BOTH;
    tMacAddr            PeerMacAddr = { 0 };
    tMacAddr            zeroAddr = { 0 };
    UINT1               u1WanType = 0;
    INT4                i4SubType = -1;
    UINT4               u4IfIpUnnumAssocIPIf = 0;
    UINT4               u4DefIpAddr = 0;
    UINT4               u4DefIpMask = 0;
    UINT1               u1AddrAllocMethod = CFA_IP_IF_MANUAL;
    INT4                i4IpIntfStats = 0;
    INT4                i4PortRole = 0;
    INT4                i4DesigUplink = 0;
    INT4                i4RetVal = CFA_FAILURE;
    INT1                i1VlanRetVal = CFA_FAILURE;
    INT4                i4EncapStatus = CFA_FALSE;
#ifdef MPLS_WANTED
#ifdef TLM_WANTED
    UINT4               u4TeLinkIf = 0;
#endif
#endif
    UNUSED_PARAM (i1RetValue);
    MEMSET (au1IfAlias, 0, CFA_MAX_IFALIAS_LENGTH);
    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    piIfName = (INT1 *) &au1IfName[0];
    MEMSET (au1PhyIfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    MEMSET (&au1IfMainDesc, 0, sizeof (au1IfMainDesc));
    IfDescString.pu1_OctetList = &au1IfMainDesc[0];

    pu1String = (CHR1 *) & au1IpId[0];

    nmhGetIfType (i4IfIndex, &i4IfaceType);
    nmhGetIfMainSubType (i4IfIndex, &i4SubType);
    nmhGetIfAdminStatus (i4IfIndex, &i4AdminStatus);
    nmhGetIfMtu (i4IfIndex, &i4IfMtu);
    nmhGetIfMainDesc (i4IfIndex, &IfDescString);
    /*Get Assosiated interface Ip address for unnumbered */
    nmhGetIfIpUnnumAssocIPIf (i4IfIndex, &u4IfIpUnnumAssocIPIf);
    nmhGetIfIpIntfStatsEnable (i4IfIndex, &i4IpIntfStats);
    i1VlanRetVal = CfaGetVlanId ((UINT4) i4IfIndex, &u2VlanId);
    CfaIcchGetIcclIfIndex (&u4IcchIfIndex);
    if ((CfaIcchIsIcclVlan (u2VlanId) == OSIX_SUCCESS) ||
        (u4IcchIfIndex == (UINT4) i4IfIndex) || (i1VlanRetVal == CFA_FAILURE))
    {
        return CLI_SUCCESS;
    }

    UNUSED_PARAM (u4ContextId);

    if (u4Module == ISS_MPLS_SHOW_RUNNING_CONFIG)
    {
        u1ShowConfig = OSIX_FALSE;
    }
    CfaGetIfBridgedIfaceStatus ((UINT4) i4IfIndex, &u1BridgedIfaceStatus);
#ifdef MPLS_WANTED
    if (u1BridgedIfaceStatus == CFA_ENABLED)
    {
        CfaCliActOnLock (CliHandle, FALSE, u4Module);

        MplsVpwsShowRunningConfig (CliHandle, i4IfIndex, 0, MPLS_PWVC_TYPE_ETH,
                                   0, 0, &u1Display, &u1ShowConfig);

        CfaCliActOnLock (CliHandle, TRUE, u4Module);
    }
    if (CfaUtilGetMplsIfFromIfIndex ((UINT4) i4IfIndex, &u4MplsIfIndex, FALSE)
        != CFA_FAILURE)
    {
        u1ShowConfig = OSIX_TRUE;
    }
#endif

    if (u1ShowConfig == OSIX_FALSE)
    {
        return CLI_SUCCESS;
    }

    /*Physical Interface Specific Commands */

    if (i4IfaceType == CFA_ENET)
    {
        nmhGetIfLinkUpDownTrapEnable (i4IfIndex, &i4LinkTrap);
        nmhGetDot3PauseAdminMode (i4IfIndex, &i4PauseAdminMode);

        if ((i4AdminStatus == CFA_LOOPBACK_LOCAL) ||
            (i4IfMtu != CFA_ENET_MTU) ||
            (i4LinkTrap != CFA_ENABLED) ||
            (i4PauseAdminMode != ETH_PAUSE_ENABLED_XMIT_AND_RCV) ||
            (IfDescString.i4_Length != 0) ||
            (u1BridgedIfaceStatus == CFA_ENABLED) ||
            (((i4AdminStatus == CFA_IF_UP) || (i4AdminStatus == CFA_IF_DOWN)) &&
             (i4IfaceType != CFA_LOOPBACK) &&
             (!((i4IfaceType == CFA_PROP_VIRTUAL_INTERFACE) &&
                (i4SubType == CFA_SUBTYPE_SISP_INTERFACE)))))
        {
            CfaCliConfGetIfName ((UINT4) i4IfIndex, piIfName);
            MEMSET (au1IfAlias, 0, CFA_MAX_IFALIAS_LENGTH);
            CfaGetIfAlias ((UINT4) i4IfIndex, au1IfAlias);
            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
            if (u1BridgedIfaceStatus != CFA_ENABLED)
            {
                CliPrintf (CliHandle, "shutdown\r\n");
            }
            *pu1Flag = OSIX_TRUE;
            CfaGetIfName ((UINT4) i4IfIndex, au1PhyIfName);
            if (STRCMP (au1PhyIfName, au1IfAlias) != 0)
            {
                CliPrintf (CliHandle, "alias %s\r\n", au1IfAlias);
            }
        }

        if (i4LinkTrap != CFA_ENABLED)
        {
            CliPrintf (CliHandle, " no snmp trap link-status\r\n");
        }

        switch (i4PauseAdminMode)
        {
            case ETH_PAUSE_DISABLED:
                CliPrintf (CliHandle, " flowcontrol " "send off\r\n");
                CliPrintf (CliHandle, " flowcontrol " "receive off\r\n");
                break;
            case ETH_PAUSE_ENABLED_XMIT:
                CliPrintf (CliHandle, " flowcontrol " "send on\r\n");
                CliPrintf (CliHandle, " flowcontrol " "receive off\r\n");
                break;
            case ETH_PAUSE_ENABLED_RCV:
                CliPrintf (CliHandle, " flowcontrol " "receive on\r\n");
                CliPrintf (CliHandle, " flowcontrol " "send off\r\n");
                break;
            case ETH_PAUSE_ENABLED_XMIT_AND_RCV:
                /* initial value is ETH_PAUSE_ENABLED_XMIT_AND_RCV */
                break;
            default:
                break;
        }
#ifdef MPLS_WANTED
        /* if bridged status is not CFA_ENABLED then show mpls l2transport and xconnect */
        if (u1BridgedIfaceStatus == CFA_ENABLED)
        {
            u1Display = OSIX_TRUE;

            CfaCliActOnLock (CliHandle, FALSE, u4Module);

            /* show mpls l2transport/neighbor for physical port(gigabitethernet) case */
            MplsVpwsShowRunningConfig (CliHandle, i4IfIndex, 0,
                                       MPLS_PWVC_TYPE_ETH, 0, 0, &u1Display,
                                       &u1ShowConfig);

            CfaCliActOnLock (CliHandle, TRUE, u4Module);
        }
#endif
        if (IfDescString.i4_Length != 0)
        {
            CliPrintf (CliHandle, "description \"%s\"\r\n",
                       IfDescString.pu1_OctetList);
        }

    }

    /* Portchannel Interface Specific Commands */

    if (i4IfaceType == CFA_LAGG)
    {
        CfaCliGetIfName ((UINT4) i4IfIndex, piIfName);
        CfaGetIfAlias ((UINT4) i4IfIndex, au1IfAlias);
        token = STRTOK (piIfName, "po");
        if (token != NULL)
        {
            i4AdminKey = STRTOL (token, NULL, BASE_DECIMAL);
        }
        CliPrintf (CliHandle, "interface port-channel %d\r\n", i4AdminKey);
        *pu1Flag = OSIX_TRUE;
        if (u1BridgedIfaceStatus != CFA_ENABLED)
        {
            CliPrintf (CliHandle, "shutdown\r\n");
        }

        if (STRCMP (piIfName, au1IfAlias) != 0)
        {
            CliPrintf (CliHandle, "alias %s\r\n", au1IfAlias);
        }

        nmhGetIfLinkUpDownTrapEnable (i4IfIndex, &i4LinkTrap);
        if (i4LinkTrap != CFA_ENABLED)
        {
            CliPrintf (CliHandle, " no snmp trap link-status\r\n");
        }
        if (IfDescString.i4_Length != 0)
        {
            CliPrintf (CliHandle, "description \"%s\"\r\n",
                       IfDescString.pu1_OctetList);
        }

        nmhGetDot3PauseAdminMode (i4IfIndex, &i4ExistingMode);

        switch (i4ExistingMode)
        {
            case PAUSE_DISABLED:
                CliPrintf (CliHandle, " flowcontrol " "send off\r\n");
                CliPrintf (CliHandle, " flowcontrol " "receive off\r\n");
                break;

            case PAUSE_ENABLED_XMIT:
                CliPrintf (CliHandle, " flowcontrol " "receive off\r\n");
                break;

            case PAUSE_ENABLED_RCV:
                CliPrintf (CliHandle, " flowcontrol " "send off\r\n");
                break;

            default:
                break;
        }

    }
    /* L3Subinterface specific Commands */
    if (i4IfaceType == CFA_L3SUB_INTF)
    {
        nmhGetIssDefaultIpAddr (&u4DefIpAddr);
        nmhGetIfIpAddr (i4IfIndex, &u4IpAddr);
        CfaCliGetIfName ((UINT4) i4IfIndex, piIfName);
        CfaGetIfAlias ((UINT4) i4IfIndex, au1IfAlias);
        i1RetValue = CfaGetVlanId ((UINT4) i4IfIndex, &u2VlanId);
        MEMSET (au1L2CxtName, 0, VCM_ALIAS_MAX_LEN);
        nmhGetIfIpAddrAllocMethod (i4IfIndex, &i4AllocMethod);
        nmhGetIfIpAddrAllocProtocol (i4IfIndex, &i4AllocProto);
        if ((i4IfIndex != CFA_MIN_L3SUB_IF_INDEX) || (u4IpAddr != u4DefIpAddr)
            || (i4AllocMethod == CFA_IP_ALLOC_POOL) || (i4IfMtu != CFA_ENET_MTU)
            || (IfDescString.i4_Length != 0)
            || (STRCMP (piIfName, au1IfAlias) != 0))
        {
            CfaCliConfGetIfName ((UINT4) i4IfIndex, piIfName);
            MEMSET (au1IfAlias, 0, CFA_MAX_IFALIAS_LENGTH);
            CfaGetIfAlias ((UINT4) i4IfIndex, au1IfAlias);
            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
            *pu1Flag = OSIX_TRUE;
            CfaGetIfName ((UINT4) i4IfIndex, au1PhyIfName);
            if (STRCMP (au1PhyIfName, au1IfAlias) != 0)
            {
                CliPrintf (CliHandle, "alias %s\r\n", au1IfAlias);
            }

            CFA_CDB_GET_ENCAP_STATUS ((UINT4) i4IfIndex, i4EncapStatus);
            if (i4EncapStatus == CFA_TRUE)
            {
                CliPrintf (CliHandle, "encapsulation dot1q %d\r\n", u2VlanId);
            }
        }
        if (i4AllocMethod == CFA_IP_ALLOC_POOL)
        {
            CliPrintf (CliHandle, " ip address");
            if (i4AllocProto == CFA_PROTO_RARP)
            {
                CliPrintf (CliHandle, " rarp\r\n");
            }
            else if (i4AllocProto == CFA_PROTO_DHCP)
            {
                CliPrintf (CliHandle, " dhcp\r\n");
            }
            else if (i4AllocProto == CFA_PROTO_BOOTP)
            {
                CliPrintf (CliHandle, " bootp\r\n");
            }
        }
        if (i4IfMtu != CFA_ENET_MTU)
        {
            CliPrintf (CliHandle, "shutdown\r\n");
            CliPrintf (CliHandle, " mtu %d \r\n", i4IfMtu);
        }
        if (IfDescString.i4_Length != 0)
        {
            CliPrintf (CliHandle, "description \"%s\"\r\n",
                       IfDescString.pu1_OctetList);
        }
        if (i4IpIntfStats == CFA_ENABLED)
        {
            CliPrintf (CliHandle, "counters enable \n");
        }
    }
    /*L3IP-Vlan  Interface Specific Comamnds */

    if (i4IfaceType == CFA_L3IPVLAN)
    {
        nmhGetIssDefaultIpAddr (&u4DefIpAddr);
        nmhGetIfIpAddr (i4IfIndex, &u4IpAddr);
        CfaCliGetIfName ((UINT4) i4IfIndex, piIfName);
        CfaGetIfAlias ((UINT4) i4IfIndex, au1IfAlias);
        i1RetValue = CfaGetVlanId ((UINT4) i4IfIndex, &u2VlanId);
        MEMSET (au1L2CxtName, 0, VCM_ALIAS_MAX_LEN);
        nmhGetIfIpAddrAllocMethod (i4IfIndex, &i4AllocMethod);
        nmhGetIfIpAddrAllocProtocol (i4IfIndex, &i4AllocProto);
        if ((i4IfIndex != CFA_MIN_IVR_IF_INDEX)
            || (i4AllocMethod == CFA_IP_ALLOC_POOL) || (i4IfMtu != CFA_ENET_MTU)
            || (IfDescString.i4_Length != 0)
            || (STRCMP (piIfName, au1IfAlias) != 0)
            || (u2VlanId == CFA_DEFAULT_VLAN_ID))
        {
            if (VcmCliGetL2CxtNameForIpInterface
                ((UINT4) i4IfIndex, au1L2CxtName) == VCM_FAILURE)
            {
                *pu1Flag = OSIX_TRUE;
                CliPrintf (CliHandle, "interface vlan %d\r\n", u2VlanId);
                if (STRCMP (piIfName, au1IfAlias) != 0)
                {
                    CliPrintf (CliHandle, "alias %s\r\n", au1IfAlias);
                }

            }
            else
            {
                CliPrintf (CliHandle, "interface vlan %d switch %s\r\n",
                           u2VlanId, au1L2CxtName);
                if (STRCMP (piIfName, au1IfAlias) != 0)
                {
                    CliPrintf (CliHandle, "alias %s\r\n", au1IfAlias);
                }
            }
        }
        if (i4AllocMethod == CFA_IP_ALLOC_POOL)
        {
            CliPrintf (CliHandle, " ip address");
            if (i4AllocProto == CFA_PROTO_RARP)
            {
                CliPrintf (CliHandle, " rarp\r\n");
            }
            else if (i4AllocProto == CFA_PROTO_DHCP)
            {
                CliPrintf (CliHandle, " dhcp\r\n");
            }
            else if (i4AllocProto == CFA_PROTO_BOOTP)
            {
                CliPrintf (CliHandle, " bootp\r\n");
            }
        }
        if (i4IfMtu != CFA_ENET_MTU)
        {
            CliPrintf (CliHandle, "shutdown\r\n");
            CliPrintf (CliHandle, " mtu %d \r\n", i4IfMtu);
        }
        if (IfDescString.i4_Length != 0)
        {
            CliPrintf (CliHandle, "description \"%s\"\r\n",
                       IfDescString.pu1_OctetList);
        }
        if (i4IpIntfStats == CFA_ENABLED)
        {
            CliPrintf (CliHandle, "counters enable \n");
        }
    }

    if (i4IfaceType == CFA_LOOPBACK)
    {
        MEMSET (au1IfAlias, 0, CFA_MAX_IFALIAS_LENGTH);
        CfaCliGetIfName ((UINT4) i4IfIndex, piIfName);
        CfaGetIfAlias ((UINT4) i4IfIndex, au1IfAlias);
        CfaGetLoopbackId ((UINT4) i4IfIndex, &u2LoopbackId);
        CliPrintf (CliHandle, "interface loopback %d\r\n", u2LoopbackId);
        *pu1Flag = OSIX_TRUE;
        if (STRCMP (piIfName, au1IfAlias) != 0)
        {
            CliPrintf (CliHandle, "alias %s\r\n", au1IfAlias);
        }
        if (i4AdminStatus == CFA_IF_UP)
        {
            CliPrintf (CliHandle, " no shutdown\r\n");
        }
    }

#ifdef WGS_WANTED
    if (i4IfaceType == CFA_L2VLAN)
    {
        nmhGetIfIpAddrAllocMethod (i4IfIndex, &i4AllocMethod);
        if (i4AllocMethod == CFA_IP_ALLOC_POOL)
        {
            CliPrintf (CliHandle, " ip address dhcp\r\n");
        }
        if (i4IfMtu != CFA_ENET_MTU)
        {
            CliPrintf (CliHandle, "shutdown\r\n");
            CliPrintf (CliHandle, " mtu %d \r\n", i4IfMtu);
        }

        CliPrintf (CliHandle, " management vlan-list");
        CfaPrintMgmtVlanList (CliHandle);
        CliPrintf (CliHandle, "\r\n");
    }
#endif
    if (i4IfaceType == CFA_ILAN)
    {
        return CfaShowILanConfig (CliHandle, (UINT4) i4IfIndex);
    }

    if (i4IfaceType == CFA_PSEUDO_WIRE)
    {
        CfaCliConfGetIfName ((UINT4) i4IfIndex, piIfName);
        CliPrintf (CliHandle, "interface %s \r\n", piIfName);
    }

    if ((gu4IsIvrEnabled == CFA_ENABLED) &&
        (CfaGetIfBridgedIfaceStatus ((UINT4) i4IfIndex, &u1BridgedIfaceStatus)
         == CFA_SUCCESS))
    {
        nmhGetIfIpPortVlanId (i4IfIndex, &i4PortVlanId);
        nmhGetIssDefaultIpAddr (&u4DefIpAddr);
        nmhGetIssDefaultIpSubnetMask (&u4DefIpMask);
        nmhGetIfIpAddr (i4IfIndex, &u4IpAddr);
        nmhGetIfIpSubnetMask (i4IfIndex, &u4IpMask);

        if ((*pu1Flag == OSIX_FALSE) &&
            (((i4IfaceType == CFA_ENET) &&
              (u1BridgedIfaceStatus != CFA_ENABLED)) ||
             ((CfaGetIfWanType ((UINT4) i4IfIndex, &u1WanType) == CFA_SUCCESS)
              && (u1WanType == CFA_WAN_TYPE_PUBLIC))
             ||
             (((u1BridgedIfaceStatus != CFA_ENABLED)
               || (CFA_CDB_IF_TYPE ((UINT4) i4IfIndex) == CFA_TUNNEL))
              && ((u4IpAddr != 0)
                  && ((u4IpAddr != u4DefIpAddr)
                      || (u4IpMask != u4DefIpMask))))))
        {
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            *pu1Flag = OSIX_TRUE;
            CfaCliConfGetIfName ((UINT4) i4IfIndex, piIfName);
            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
        }

        if (((i4IfaceType == CFA_ENET) || (i4IfaceType == CFA_LAGG))
            && (u1BridgedIfaceStatus != CFA_ENABLED))
        {
            if (i4IfIndex != CFA_OOB_MGMT_IFINDEX)
            {
                CliPrintf (CliHandle, " no switchport\r\n");
                if (0 != i4PortVlanId)
                {
                    /*Print the Port-VlanId of the the RouterPort Index */
                    CliPrintf (CliHandle, " port-vid %d\r\n", i4PortVlanId);
                }
            }
        }
        if ((CfaGetIfWanType ((UINT4) i4IfIndex, &u1WanType) == CFA_SUCCESS) &&
            (u1WanType == CFA_WAN_TYPE_PUBLIC) && (i4IfaceType != CFA_L3IPVLAN))
        {
            CliPrintf (CliHandle, " network-type wan\r\n");
        }
        if (u1BridgedIfaceStatus != CFA_ENABLED
            || (CFA_CDB_IF_TYPE ((UINT4) i4IfIndex) == CFA_TUNNEL))
        {
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            MEMSET (au1IpId, 0, MAX_MAC_LENGTH);

            if (u4IpAddr != 0)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);
                CliPrintf (CliHandle, "ip address  %s", pu1String);

                MEMSET (au1IpId, 0, MAX_MAC_LENGTH);

                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpMask);
                CliPrintf (CliHandle, " %s\r\n", pu1String);

                /* Secondary IP address configured over the 
                 * interface */
                u4SecondaryAddr = 0;
                while ((nmhGetNextIndexIfSecondaryIpAddressTable (i4IfIndex,
                                                                  &i4NextIfIndex,
                                                                  u4SecondaryAddr,
                                                                  &u4SecondaryAddr)
                        == SNMP_SUCCESS) && (i4IfIndex == i4NextIfIndex))
                {

                    CfaGetAddrAllocMethodForSecIp ((UINT4) i4IfIndex,
                                                   u4SecondaryAddr,
                                                   &u1AddrAllocMethod);
                    if (u1AddrAllocMethod == CFA_IP_IF_MANUAL)
                    {
                        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SecondaryAddr);
                        CliPrintf (CliHandle, "ip address  %s", pu1String);
                        MEMSET (au1IpId, 0, MAX_MAC_LENGTH);

                        nmhGetIfSecondaryIpSubnetMask (i4IfIndex,
                                                       u4SecondaryAddr,
                                                       &u4IpMask);
                        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpMask);
                        CliPrintf (CliHandle, " %s secondary\r\n", pu1String);
                        MEMSET (au1IpId, 0, MAX_MAC_LENGTH);
                    }
                }

            }
            else if (nmhGetIfIpDestMacAddress (i4IfIndex, &PeerMacAddr) ==
                     SNMP_SUCCESS)
            {
                /* ip unnumbered configured on this interface */
                MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                CfaCliGetIfName (u4IfIpUnnumAssocIPIf, piIfName);

                if (MEMCMP (PeerMacAddr, zeroAddr, CFA_ENET_ADDR_LEN) != 0)
                {

                    PrintMacAddress ((UINT1 *) PeerMacAddr,
                                     (UINT1 *) pu1String);
                    CliPrintf (CliHandle, "ip unnumbered  %s %s \r\n",
                               pu1String, piIfName);
                }

            }
            nmhGetFsMIStdIpProxyArpAdminStatus (i4IfIndex, &i4ProxyArpStatus);
            if (i4ProxyArpStatus == ARP_PROXY_ENABLE)
            {
                CliPrintf (CliHandle, "ip proxy-arp\r\n");
            }
#ifdef MPLS_WANTED
#ifdef TLM_WANTED
            if (CfaApiGetTeLinkIfFromL3If
                ((UINT4) i4IfIndex, &u4TeLinkIf, CFA_ONE, FALSE != CFA_SUCCESS))
            {
#endif
                if (CfaUtilGetMplsIfFromIfIndex
                    ((UINT4) i4IfIndex, &u4MplsIfIndex, FALSE) != CFA_FAILURE)
                {
                    CliPrintf (CliHandle, " mpls ip\r\n");
                }
#ifdef TLM_WANTED
            }
#endif
#endif
        }

        /* Secondary ip address of OOB port for Active and Standby nodes 
         * to be displayed when dual OOB is enabled. */
        if ((CfaIsMgmtPort ((UINT4) i4IfIndex) == TRUE) &&
            (CfaIsDualOobEnabled () == OSIX_TRUE))
        {
            MEMSET (au1IpId, 0, MAX_MAC_LENGTH);
            MEMSET (au1MaskId, 0, MAX_MAC_LENGTH);
            pu1MaskString = (CHR1 *) & au1MaskId[0];

            nmhGetIfOOBNode0SecondaryIpAddress (&u4SecondaryAddr);
            if (u4SecondaryAddr != 0)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SecondaryAddr);
                CliPrintf (CliHandle, "ip address %s", pu1String);
                nmhGetIfOOBNode0SecondaryIpMask (&u4IpMask);
                CLI_CONVERT_IPADDR_TO_STR (pu1MaskString, u4IpMask);
                CliPrintf (CliHandle, " %s secondary node0\r\n", pu1MaskString);
            }

            if (u4SecondaryAddr != 0)
            {
                nmhGetIfOOBNode1SecondaryIpAddress (&u4SecondaryAddr);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SecondaryAddr);
                CliPrintf (CliHandle, "ip address %s", pu1String);
                nmhGetIfOOBNode1SecondaryIpMask (&u4IpMask);
                CLI_CONVERT_IPADDR_TO_STR (pu1MaskString, u4IpMask);
                CliPrintf (CliHandle, " %s secondary node1\r\n", pu1MaskString);
            }
        }

        /* Check if the IVR interface is associated to secondary VLAN
         * of the private vlans
         */

        i4PrevIfIndex = i4IfIndex;
        i4PrevVlan = 0;

        i4RetVal = nmhGetNextIndexIfIvrMappingTable (i4PrevIfIndex,
                                                     &i4NextIfIndex, i4PrevVlan,
                                                     &i4NextVlan);

        if ((i4RetVal != SNMP_FAILURE) && (i4IfIndex == i4NextIfIndex))
        {
            while (i4RetVal != SNMP_FAILURE)
            {
                if (i4IfIndex == i4NextIfIndex)
                {
                    CliPrintf (CliHandle, " %d", i4NextVlan);
                }

                i4PrevIfIndex = i4NextIfIndex;
                i4PrevVlan = i4NextVlan;

                i4RetVal = nmhGetNextIndexIfIvrMappingTable (i4PrevIfIndex,
                                                             &i4NextIfIndex,
                                                             i4PrevVlan,
                                                             &i4NextVlan);
            }
            CliPrintf (CliHandle, "\r\n");
        }
    }

    if ((i4IfaceType == CFA_MPLS) || (i4IfaceType == CFA_MPLS_TUNNEL) ||
        (i4IfaceType == CFA_TELINK))
    {
        return CLI_SUCCESS;
    }

    /* Virtual Instance Ports, Internal Provider Instance Ports ,Internal 
     * Customer Backbone Ports & SISP logical interfaces are printed here 
     * for SRC 
     */
    if ((i4IfaceType == CFA_BRIDGED_INTERFACE) || (i4IfaceType == CFA_PIP)
        || ((i4IfaceType == CFA_PROP_VIRTUAL_INTERFACE) &&
            (i4SubType == CFA_SUBTYPE_SISP_INTERFACE))
        || ((i4IfaceType == CFA_PROP_VIRTUAL_INTERFACE) &&
            (i4SubType == CFA_SUBTYPE_AC_INTERFACE))
        || (i4IfaceType == CFA_PSEUDO_WIRE) || (i4IfaceType == CFA_VXLAN_NVE)
        || (i4IfaceType == CFA_TAP))
    {
        *pu1Flag = OSIX_TRUE;
        CfaCliConfGetIfName ((UINT4) i4IfIndex, piIfName);
        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
    }
    CfaGetIfBridgedIfaceStatus ((UINT4) i4IfIndex, &u1BridgedIfaceStatus);

    if ((*pu1Flag == OSIX_TRUE)
        && ((i4AdminStatus == CFA_IF_UP) && (i4IfaceType != CFA_LOOPBACK)
            &&
            (!((i4IfaceType == CFA_PROP_VIRTUAL_INTERFACE)
               && (i4SubType == CFA_SUBTYPE_SISP_INTERFACE)))))
        if ((i4IfIndex != CFA_MIN_IVR_IF_INDEX)
            || ((i4IfIndex == CFA_MIN_IVR_IF_INDEX)
                && (u1BridgedIfaceStatus == CFA_DISABLED)))

        {
            /* For SISP interfaces, no shutdown command is not applicable
             * */
            CliPrintf (CliHandle, "no shutdown\r\n");
        }

    if (i4AdminStatus == CFA_LOOPBACK_LOCAL)
    {
        CliPrintf (CliHandle, "loopback local\r\n");
    }

    nmhGetIfMainUfdOperStatus (i4IfIndex, (INT4 *) &i4UfdOperStatus);
    if (i4UfdOperStatus == CFA_IF_UFD_ERR_DISABLED)
    {
        CliPrintf (CliHandle, " no shutdown\r\n");
    }

    nmhGetIfMainPortRole (i4IfIndex, &i4PortRole);
    nmhGetIfMainDesigUplinkStatus (i4IfIndex, &i4DesigUplink);
    if (i4PortRole == CFA_PORT_ROLE_UPLINK)
    {
        if (i4DesigUplink == CFA_ENABLED)
        {
            CliPrintf (CliHandle, " set port-role uplink designated\r\n");
        }
        else
        {
            CliPrintf (CliHandle, " set port-role uplink \r\n");
        }

    }
    if (*pu1Flag == OSIX_TRUE)
    {
        CliPrintf (CliHandle, "!\r\n");
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_get_current_ifc_index                          */
/*                                                                           */
/*     DESCRIPTION      : This function returns the current interface number */
/*                        stored in the mode treeThis function displays the  */
/*                        interface flowcontrol                              */
/*                                                                           */
/*     INPUT            : NONE                                               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : i4ifaceIndex                                       */
/*                                                                           */
/*****************************************************************************/

INT4
cli_get_current_ifc_index ()
{
    INT4                i4IfaceIndex;

    i4IfaceIndex = CLI_GET_IFINDEX ();
    if (i4IfaceIndex == CLI_ERROR)
    {
        return 0;
    }

    return i4IfaceIndex;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_get_index_from_ifc_name                        */
/*                                                                           */
/*     DESCRIPTION      : This function returns interface index from name    */
/*                                                                           */
/*     INPUT            : pu1InterfaceName                                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : i4Index                                            */
/*                                                                           */
/*****************************************************************************/
INT4
cli_get_index_from_ifc_name (UINT1 *pu1InterfaceName)
{
    INT4                i4Status = OSIX_FAILURE;
    INT4                i4Index;

    i4Status = (INT4) CfaGetInterfaceIndexFromName (pu1InterfaceName,
                                                    (UINT4 *) &i4Index);

    if (i4Status == OSIX_FAILURE)
    {
        return CLI_ERROR;
    }
    return i4Index;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_get_ifc_index                                  */
/*                                                                           */
/*     DESCRIPTION      : This function is used to get the interface ip      */
/*                        address. If interface  name is not given, it gets  */
/*                        the current interface ipaddress. If interface      */
/*                        name is specified, corresponding interface ipaddr  */
/*                        is extracted. Check is also made whether interface */
/*                        input is a number.                                 */
/*                                                                           */
/*     INPUT            : pu1InterfaceName                                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
cli_get_ifc_index (UINT1 *pu1Interface)
{
    INT4                i4IfaceIndex = (INT4) CLI_PTR_TO_U4 (pu1Interface);

    if (pu1Interface == 0)
    {
        /* get the current interface number */
        i4IfaceIndex = cli_get_current_ifc_index ();

        if (CfaValidateCfaIfIndex ((UINT2) i4IfaceIndex) == CFA_FAILURE)
        {
            return CLI_ERROR;
        }
    }
    else
    {
        /* User has specified an interface number... */
        /*     i4IfaceIndex = CLI_ATOI (pu1Interface); */
        if (CfaValidateCfaIfIndex ((UINT2) i4IfaceIndex) == CFA_FAILURE)
        {
            return CLI_ERROR;
        }

    }
    return i4IfaceIndex;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaGetIfRangeConfigPrompt                          */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the prompt in pi1DispStr if valid.     */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/
INT1
CfaGetIfRangeConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = 0;

    u4Len = STRLEN ("(config-if-range)#");

    if (STRNCMP (pi1ModeName, "(config-if-range)#", u4Len) != 0)
    {
        return FALSE;
    }

    STRNCPY (pi1DispStr, "(config-if-range)#", STRLEN ("(config-if-range)#"));
    pi1DispStr[STRLEN ("(config-if-range)#")] = '\0';
    return TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaGetIfConfigPrompt                               */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the prompt in pi1DispStr if valid.     */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/
INT1
CfaGetIfConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index;
    UINT4               u4Len;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "eth" as the prompt 
     * for the mode ETH */
    if (pi1ModeName == NULL)
    {
        STRNCPY (pi1DispStr, "eth(n),vlan(n),ppp(n),...",
                 STRLEN ("eth(n),vlan(n),ppp(n),..."));
        pi1DispStr[STRLEN ("eth(n),vlan(n),ppp(n),...")] = '\0';
        return TRUE;
    }

    u4Len = STRLEN (CLI_INTF_MODE);

    if (STRNCMP (pi1ModeName, CLI_INTF_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    u4Index = (UINT4) CLI_ATOI (pi1ModeName + u4Len);

    CLI_SET_CXT_ID (0xFFFFFFFF);
    if (CfaValidateCfaIfIndex ((UINT2) u4Index) == CFA_FAILURE)
    {
        return FALSE;
    }

    if (((UINT4) CLI_SET_IFINDEX ((INT4) u4Index)) != (UINT4) u4Index)
        return FALSE;

    STRNCPY (pi1DispStr, "(config-if)#", STRLEN ("(config-if)#"));
    pi1DispStr[STRLEN ("(config-if)#")] = '\0';
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaGetL3SubIfConfigPrompt                          */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the prompt in pi1DispStr if valid.     */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/
INT1
CfaGetL3SubIfConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index;
    UINT4               u4Len;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "eth" as the prompt
     * for the mode ETH */
    if (pi1ModeName == NULL)
    {
        STRNCPY (pi1DispStr, "eth(n),vlan(n),ppp(n),...",
                 STRLEN ("eth(n),vlan(n),ppp(n),..."));
        pi1DispStr[STRLEN ("eth(n),vlan(n),ppp(n),...")] = '\0';
        return TRUE;
    }

    u4Len = STRLEN (CLI_L3IFSUB_MODE);

    if (STRNCMP (pi1ModeName, CLI_L3IFSUB_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    u4Index = (UINT4) CLI_ATOI (pi1ModeName + u4Len);

    CLI_SET_CXT_ID (0xFFFFFFFF);
    if (CfaValidateCfaIfIndex ((UINT2) u4Index) == CFA_FAILURE)
    {
        return FALSE;
    }

    if (((UINT4) CLI_SET_IFINDEX ((INT4) u4Index)) != (UINT4) u4Index)
        return FALSE;

    STRNCPY (pi1DispStr, "(config-subif)#", STRLEN ("(config-subif)#"));
    pi1DispStr[STRLEN ("(config-subif)#")] = '\0';

    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaGetIfOpenflowPrompt                             */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the prompt in pi1DispStr if valid.     */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/
INT1
CfaGetIfOpenflowPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index = 0;
    UINT4               u4Len = 0;

    if (pi1DispStr == NULL)
    {
        return FALSE;
    }

    /* NULL is passed to return "eth" as the prompt
     * for the mode ETH */
    if (pi1ModeName == NULL)
    {
        STRNCPY (pi1DispStr, "eth(n),vlan(n),ppp(n),...",
                 STRLEN ("eth(n),vlan(n),ppp(n),..."));
        pi1DispStr[STRLEN ("eth(n),vlan(n),ppp(n),...")] = '\0';
        return TRUE;
    }

    u4Len = STRLEN (CLI_INTOF_MODE);

    if (STRNCMP (pi1ModeName, CLI_INTOF_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    u4Index = (UINT4) CLI_ATOI (pi1ModeName + u4Len);

    if (CfaValidateCfaIfIndex ((UINT2) u4Index) == CFA_FAILURE)
    {
        return FALSE;
    }

    if (((UINT4) CLI_SET_IFINDEX ((INT4) u4Index)) != (UINT4) u4Index)
        return FALSE;

    STRNCPY (pi1DispStr, "(config-if)#", STRLEN ("(config-if)#"));
    pi1DispStr[STRLEN ("(config-if)#")] = '\0';
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaGetIfRouterPrompt                               */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the prompt in pi1DispStr if valid.     */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/
INT1
CfaGetIfRouterPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index;
    UINT4               u4Len;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "eth" as the prompt 
     * for the mode ETH */
    if (pi1ModeName == NULL)
    {
        STRNCPY (pi1DispStr, "eth(n),vlan(n),ppp(n),...",
                 STRLEN ("eth(n),vlan(n),ppp(n),..."));
        pi1DispStr[STRLEN ("eth(n),vlan(n),ppp(n),...")] = '\0';
        return TRUE;
    }

    u4Len = STRLEN (CLI_INTR_MODE);

    if (STRNCMP (pi1ModeName, CLI_INTR_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    u4Index = (UINT4) CLI_ATOI (pi1ModeName + u4Len);

    if (CfaValidateCfaIfIndex ((UINT2) u4Index) == CFA_FAILURE)
    {
        return FALSE;
    }

    if (((UINT4) CLI_SET_IFINDEX ((INT4) u4Index)) != (UINT4) u4Index)
        return FALSE;

    STRNCPY (pi1DispStr, "(config-if)#", STRLEN ("(config-if)#"));
    pi1DispStr[STRLEN ("(config-if)#")] = '\0';
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaGetUfdGroupPrompt                               */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the prompt in pi1DispStr if valid.     */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/
INT1
CliGetUfdGroupPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4GroupId = 0;
    UINT4               u4Len = 0;

    if (!(pi1DispStr) || !(pi1ModeName))
    {
        return FALSE;
    }
    u4Len = STRLEN (CFA_UFD_MODE);

    if (STRNCMP (pi1ModeName, CFA_UFD_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    u4GroupId = (UINT4) CLI_ATOI (pi1ModeName + u4Len);

    if (((UINT4) CLI_SET_UFD_GROUPID ((INT4) u4GroupId)) != u4GroupId)
        return FALSE;

    CLI_STRCPY (pi1DispStr, "(config-ufd)#");
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliValidateInterfaceName                        */
/*                                                                           */
/*     DESCRIPTION      : This function validates interface  name            */
/*                                                                           */
/*     INPUT            : pi1IfName                                          */
/*                                                                           */
/*     OUTPUT           : pi1RetIfName                                       */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCliValidateInterfaceName (INT1 *pi1IfName, INT1 *pi1RetIfName,
                             UINT4 *pu4RetIfType)
{
    INT4                i4Index;
    INT4                i4Len = 0;
    INT4                i4IfTypeLen = 0;
#define CFA_CLI_MAX_IFTYPES   6
    tCfaIfaceInfo       ai1CfaIfTypes[CFA_CLI_MAX_IFTYPES] = {
#ifdef CFA_UNIQUE_INTF_NAME
        {CFA_GI_ENET, "ethernet", "0"}
        ,
#else
        {CFA_GI_ENET, "gigabitethernet", "0"}
        ,
#endif
        {CFA_FA_ENET, "fastethernet", "0"}
        ,
        {CFA_XE_ENET, "extreme-ethernet", "0"}
        ,
        {CFA_XL_ENET, "XL-ethernet", "0"}
        ,
        {CFA_LVI_ENET, "LVI-ethernet", "0"}
        ,
        {CFA_ILAN, "internal-lan", "0"}
    };

    if (!(pi1IfName) || !(*pi1IfName))
    {
        return CLI_FAILURE;
    }

    i4Len = (INT4) STRLEN (pi1IfName);

    /* Check for longest match with  any interface type's available */

    for (i4Index = 0; i4Index < CFA_CLI_MAX_IFTYPES; i4Index++)
    {
        i4IfTypeLen = (INT4) STRLEN (ai1CfaIfTypes[i4Index].au1IfName);

        i4Len = (i4Len < i4IfTypeLen) ? (i4Len) : (i4IfTypeLen);

        if (STRNCASECMP (ai1CfaIfTypes[i4Index].au1IfName, pi1IfName, i4Len) ==
            0)
        {
            break;
        }
    }
    if (i4Index == CFA_CLI_MAX_IFTYPES)
    {
        return CLI_FAILURE;
    }

    /* check if input iftype is longer than matching type */
    i4Len = (INT4) STRLEN (pi1IfName);
    if (i4Len > i4IfTypeLen)
    {
        return CLI_FAILURE;
    }

    if (pi1RetIfName != NULL)
    {
        STRNCPY (pi1RetIfName, ai1CfaIfTypes[i4Index].au1IfName,
                 STRLEN (ai1CfaIfTypes[i4Index].au1IfName));
        pi1RetIfName[STRLEN (ai1CfaIfTypes[i4Index].au1IfName)] = '\0';
    }
    if (pu4RetIfType != NULL)
    {
        *pu4RetIfType = ai1CfaIfTypes[i4Index].u4IfType;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliValidateXInterfaceName                       */
/*                                                                           */
/*     DESCRIPTION      : This function validates interface names            */
/*                        for Extended interface types which contains        */
/*                        Physical interface and Port channel interface name */
/*                                                                           */
/*     INPUT            : pi1IfXName                                         */
/*                        pi1RetIfXName                                      */
/*                        pu4RetIfXType                                      */
/*                                                                           */
/*     OUTPUT           : pi1RetIfXName                                      */
/*                        pu4RetIfXType                                      */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
CfaCliValidateXInterfaceName (INT1 *pi1IfXName, INT1 *pi1RetIfXName,
                              UINT4 *pu4RetIfXType)
{
    INT4                i4Index;
    INT4                i4Len = 0;
    INT4                i4IfTypeLen = 0;
#define CFA_CLI_MAX_IFXTYPES   14

    tCfaIfaceInfo       ai1CfaIfTypes[CFA_CLI_MAX_IFXTYPES] = {
#ifdef CFA_UNIQUE_INTF_NAME
        {CFA_GI_ENET, "ethernet", "0"}
        ,
#else
        {CFA_GI_ENET, "gigabitethernet", "0"}
        ,
#endif
        {CFA_FA_ENET, "fastethernet", "0"}
        ,
        {CFA_XE_ENET, "extreme-ethernet", "0"}
        ,
        {CFA_XL_ENET, "XL-ethernet", "0"}
        ,
        {CFA_LVI_ENET, "LVI-ethernet", "0"}
        ,
        {CFA_LAGG, "port-channel", "0"}
        ,
        {CFA_PROP_VIRTUAL_INTERFACE, "sisp", "0"}
        ,
        {CFA_BRIDGED_INTERFACE, "virtual", "0"}
        ,
#ifdef PBB_WANTED
        {CFA_ILAN, "internal-lan", "0"}
        ,
#endif
        {CFA_PPP, "ppp", "0"}
        ,
        {CFA_PSEUDO_WIRE, "pw", "0"}
#ifdef VXLAN_WANTED
        ,
        {CFA_VXLAN_NVE, "nve", "0"}
#endif
#ifdef HDLC_WANTED
        ,
        {CFA_HDLC, "serial", "0"}
#endif
        ,
        {CFA_STATION_FACING_BRIDGE_PORT, "s-channel", "0"}
#ifdef VCPEMGR_WANTED
        ,
        {CFA_TAP, "tap", "0"}
#endif

    };

    if (!(pi1IfXName) || !(*pi1IfXName))
    {
        return CLI_FAILURE;
    }

    i4Len = (INT4) STRLEN (pi1IfXName);

    /* Check for longest match with  any interface type's available */

    for (i4Index = 0; i4Index < CFA_CLI_MAX_IFXTYPES; i4Index++)
    {
        i4IfTypeLen = (INT4) STRLEN (ai1CfaIfTypes[i4Index].au1IfName);

        i4Len = (i4Len < i4IfTypeLen) ? (i4Len) : (i4IfTypeLen);
        if (i4Len == 0)
        {
            continue;
        }

        if (STRNCASECMP (ai1CfaIfTypes[i4Index].au1IfName, pi1IfXName, i4Len) ==
            0)
        {
            break;
        }
    }
    if (i4Index == CFA_CLI_MAX_IFXTYPES)
    {
        return CLI_FAILURE;
    }

    /* check if input ifXtype is longer than matching type */
    i4Len = (INT4) STRLEN (pi1IfXName);
    /* if the last character is space ignore it */
    if (pi1IfXName[i4Len - 1] == ' ')
    {
        i4Len--;
    }
    if (i4Len > i4IfTypeLen)
    {
        return CLI_FAILURE;
    }

    if (pi1RetIfXName != NULL)
    {
        STRNCPY (pi1RetIfXName, ai1CfaIfTypes[i4Index].au1IfName,
                 STRLEN (ai1CfaIfTypes[i4Index].au1IfName));
        pi1RetIfXName[STRLEN (ai1CfaIfTypes[i4Index].au1IfName)] = '\0';
    }
    if (pu4RetIfXType != NULL)
    {
        *pu4RetIfXType = ai1CfaIfTypes[i4Index].u4IfType;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliValidateXSubTypeIntName                      */
/*                                                                           */
/*     DESCRIPTION      : This function validates interface names            */
/*                        for Extended interface types which contains        */
/*                        Physical interface and Port channel interface name */
/*                                                                           */
/*     INPUT            : pi1IfXName                                         */
/*                        pi1RetIfXName                                      */
/*                        pu4RetIfXType                                      */
/*                                                                           */
/*     OUTPUT           : pi1RetIfXName                                      */
/*                        pu4RetIfXType                                      */
/*                        pu4RetIfXSubType                                   */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
CfaCliValidateXSubTypeIntName (INT1 *pi1IfXName, INT1 *pi1RetIfXName,
                               UINT4 *pu4RetIfXType, UINT4 *pu4RetIfXSubType)
{
    INT4                i4Index = 0;
    INT4                i4Len = 0;
    INT4                i4IfTypeLen = 0;
    INT4                i4CheckLen = 0;
    INT4                i4SubCount = 0;
    INT1                i1MatchFlag = -1;

    if (!(pi1IfXName) || !(*pi1IfXName))
    {
        return CLI_FAILURE;
    }

    i4Len = (INT4) STRLEN (pi1IfXName);

    /* Check for longest match with  any interface type's available */

    for (i4Index = 0; i4Index < CFA_CLI_MAX_IFXTYPES; i4Index++)
    {
        for (i4SubCount = 0; i4SubCount < CFA_CLI_MAX_IFXSUBTYPE; i4SubCount++)
        {
            i4IfTypeLen =
                (INT4) STRLEN (ai1IfTypes[i4Index][i4SubCount].au1IfName);

            i4CheckLen = (i4Len < i4IfTypeLen) ? (i4Len) : (i4IfTypeLen);

            if (i4CheckLen == 0)
            {
                continue;
            }

            if (STRNCASECMP (ai1IfTypes[i4Index][i4SubCount].au1IfName,
                             pi1IfXName, i4CheckLen) == 0)
            {
                i1MatchFlag = 0;
                break;
            }
        }
        if (i1MatchFlag != -1)
        {
            break;
        }
    }
    if (i4Index == CFA_CLI_MAX_IFXTYPES)
    {
        return CLI_FAILURE;
    }

    /* check if input ifXtype is longer than matching type */
    i4Len = (INT4) STRLEN (pi1IfXName) - 1;
    /* if the last character is space ignore it */
    if (pi1IfXName[i4Len - 1] == ' ')
    {
        i4Len--;
    }
    if (i4Len > i4IfTypeLen)
    {
        return CLI_FAILURE;
    }

    if (pi1RetIfXName != NULL)
    {
        STRNCPY (pi1RetIfXName, ai1IfTypes[i4Index][i4SubCount].au1IfName,
                 STRLEN (ai1IfTypes[i4Index][i4SubCount].au1IfName));
        pi1RetIfXName[STRLEN (ai1IfTypes[i4Index][i4SubCount].au1IfName)] =
            '\0';
    }
    if ((pu4RetIfXType != NULL) && (pu4RetIfXSubType != NULL))
    {
        *pu4RetIfXType = ai1IfTypes[i4Index][i4SubCount].u4IfType;
        *pu4RetIfXSubType = (UINT4) i4SubCount;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME : CfaCliCreateLogicalInterface
 * DESCRIPTION   : Creates a logical interface.
 * INPUT         : CliHandle -  CLI context id to be used by CliPrintf
 *                 u4IfIndex - Index of the interface to be created   *
 *                 pu1IfName - Name of the interface                  *
 * RETURNS       : returns 0 on CFA_SUCCESS
 *                 returns -1 on CFA_FAILURE
 **************************************************************************/
INT4
CfaCliCreateLogicalInterface (tCliHandle CliHandle, UINT4 u4IfIndex,
                              UINT1 *pu1IfName)
{
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE Alias;

    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    Alias.i4_Length = (INT4) STRLEN (pu1IfName);
    Alias.pu1_OctetList = pu1IfName;

    /* we can directly call set routine, because validation is done already
     * in the function CfaCliCreateInterface() */
    nmhSetIfAlias (u4IfIndex, &Alias);

    /* Set the Type */
    if (nmhTestv2IfMainType (&u4ErrCode, (INT4) u4IfIndex, (INT4) CFA_TUNNEL) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return (CLI_FAILURE);
    }
    if (nmhSetIfMainType (u4IfIndex, CFA_TUNNEL) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    /* All the mandatory parameters are set. 
     * Row status should be made active */
    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Cannot activate IfMainRowStatus for "
                   " TnlEntry !\r\n");
        return CLI_FAILURE;
    }
    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaConfTnlParams                                   */
/*                                                                           */
/*     DESCRIPTION      : This function configure the tunnel parameters to   */
/*                        be associated with the tunnel interface            */
/*                                                                           */
/*     INPUT            :                                                    */
/*                        CliHandle - CLI Handler                            */
/*                        u4IfIndex - index of the interface                 */
/*                        u4IfAddrType - IP address Type                     */
/*                        u4IfIndex - Source Address                         */
/*                        u4IfIndex - Destination Address                    */
/*                        u4TnlMode - Tunnel Mode                            */
/*                        u4ConfigId - Configuration ID                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaConfTnlParams (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4AddrType,
                  tTnlIpAddr * pTnlSrcIpAddr, tTnlIpAddr * pTnlDestIpAddr,
                  UINT4 u4TnlMode, UINT4 u4ConfigId)
{
    UINT4               u4ErrCode;
    UINT4               u4IfCxtId = 0;
    tSNMP_OCTET_STRING_TYPE TnlIfSrcAddr;
    tSNMP_OCTET_STRING_TYPE TnlIfDestAddr;
    tSNMP_OCTET_STRING_TYPE TnlIfAlias;
    UINT1               TnlSrcAddr[16];
    UINT1               TnlDestAddr[16];
    UINT1               TnlAlias[CFA_MAX_PORT_NAME_LENGTH];
    MEMSET (&TnlIfDestAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TnlIfSrcAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TnlIfAlias, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        TnlIfSrcAddr.pu1_OctetList = &TnlSrcAddr[0];
        TnlIfSrcAddr.i4_Length = 4;
        TnlIfDestAddr.pu1_OctetList = &TnlDestAddr[0];
        TnlIfDestAddr.i4_Length = 4;

        PTR_ASSIGN4 (TnlIfSrcAddr.pu1_OctetList, pTnlSrcIpAddr->Ip4TnlAddr);
        PTR_ASSIGN4 (TnlIfDestAddr.pu1_OctetList, pTnlDestIpAddr->Ip4TnlAddr);
    }
    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        TnlIfSrcAddr.pu1_OctetList = &TnlSrcAddr[0];
        TnlIfSrcAddr.i4_Length = 16;
        TnlIfDestAddr.pu1_OctetList = &TnlDestAddr[0];
        TnlIfDestAddr.i4_Length = 16;
        MEMCPY (TnlIfSrcAddr.pu1_OctetList,
                pTnlSrcIpAddr->Ip6TnlAddr, TnlIfSrcAddr.i4_Length);
        MEMCPY (TnlIfDestAddr.pu1_OctetList,
                pTnlDestIpAddr->Ip6TnlAddr, TnlIfDestAddr.i4_Length);
    }

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4IfCxtId);
    if (CfaUtilSetTnlCxt (u4IfCxtId) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2FsTunlIfStatus (&u4ErrCode, (INT4) u4AddrType, &TnlIfSrcAddr,
                                 &TnlIfDestAddr, (INT4) u4TnlMode,
                                 (INT4) u4ConfigId,
                                 (INT4) CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
    {
        CfaUtilResetTnlCxt ();
        return CLI_FAILURE;
    }

    if (nmhSetFsTunlIfStatus ((INT4) u4AddrType, &TnlIfSrcAddr, &TnlIfDestAddr,
                              (INT4) u4TnlMode, (INT4) u4ConfigId,
                              CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot create entry in tunnel If Table\r\n");
        CfaUtilResetTnlCxt ();
        return CLI_FAILURE;
    }

    TnlIfAlias.pu1_OctetList = &TnlAlias[0];
    MEMCPY (TnlIfAlias.pu1_OctetList,
            CFA_CDB_IF_ALIAS (u4IfIndex), CFA_MAX_PORT_NAME_LENGTH);
    TnlIfAlias.i4_Length = (INT4) STRLEN (CFA_CDB_IF_ALIAS (u4IfIndex));
    if (nmhTestv2FsTunlIfAlias (&u4ErrCode, (INT4) u4AddrType, &TnlIfSrcAddr,
                                &TnlIfDestAddr, (INT4) u4TnlMode,
                                (INT4) u4ConfigId, &TnlIfAlias) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot create entry in tunnel If Table\r\n");
        CfaUtilResetTnlCxt ();
        return CLI_FAILURE;
    }

    if (nmhSetFsTunlIfAlias ((INT4) u4AddrType, &TnlIfSrcAddr, &TnlIfDestAddr,
                             (INT4) u4TnlMode, (INT4) u4ConfigId,
                             &TnlIfAlias) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot create entry in tunnel If Table\r\n");
        CfaUtilResetTnlCxt ();
        return CLI_FAILURE;
    }

    if (nmhSetFsTunlIfStatus ((INT4) u4AddrType, &TnlIfSrcAddr, &TnlIfDestAddr,
                              (INT4) u4TnlMode, (INT4) u4ConfigId,
                              CFA_RS_ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot set RS Active in Tunnel table !\r\n");
        CfaUtilResetTnlCxt ();
        return CLI_FAILURE;
    }

    if (nmhGetFsTunlIfIndex ((INT4) u4AddrType, &TnlIfSrcAddr, &TnlIfDestAddr,
                             (INT4) u4TnlMode, (INT4) u4ConfigId,
                             (INT4 *) &u4IfIndex) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Cannot get IfIndex of TnlEntry !\r\n");
        CfaUtilResetTnlCxt ();
        return CLI_FAILURE;
    }

    CfaUtilResetTnlCxt ();
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaDelTnlParams                                    */
/*                                                                           */
/*     DESCRIPTION      : This function Deletes the tunnel parameters        */
/*                        associated with the tunnel interface            */
/*                                                                           */
/*     INPUT            :                                                    */
/*                        CliHandle - CLI Handler                            */
/*                        u4IfIndex - index of the interface to be deleted   */
/*                        u4IfAddrType - IP address Type                     */
/*                        u4IfIndex - Source Address                         */
/*                        u4IfIndex - Destination Address                    */
/*                        u4TnlMode - Tunnel Mode                            */
/*                        u4ConfigId - Configuration ID                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaDelTnlParams (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4AddrType,
                 tTnlIpAddr * pTnlSrcIpAddr, tTnlIpAddr * pTnlDestIpAddr,
                 UINT4 u4TnlMode, UINT4 u4ConfigId)
{
    UINT4               u4ErrCode;
    UINT4               u4IfCxtId = 0;
    tSNMP_OCTET_STRING_TYPE TnlIfSrcAddr;
    tSNMP_OCTET_STRING_TYPE TnlIfDestAddr;
    UINT1               TnlSrcAddr[16];
    UINT1               TnlDestAddr[16];
    INT4                i4AdminStatus;

    nmhGetIfMainAdminStatus ((INT4) u4IfIndex, &i4AdminStatus);

    if (i4AdminStatus == CFA_IF_UP)
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot delete entry in tunnel If Table, Check the admin status of the interface\r\n");
        return (CLI_FAILURE);
    }

    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        TnlIfSrcAddr.pu1_OctetList = &TnlSrcAddr[0];
        TnlIfSrcAddr.i4_Length = 4;
        TnlIfDestAddr.pu1_OctetList = &TnlDestAddr[0];
        TnlIfDestAddr.i4_Length = 4;

        PTR_ASSIGN4 (TnlIfSrcAddr.pu1_OctetList, pTnlSrcIpAddr->Ip4TnlAddr);
        PTR_ASSIGN4 (TnlIfDestAddr.pu1_OctetList, pTnlDestIpAddr->Ip4TnlAddr);
    }
    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        TnlIfSrcAddr.pu1_OctetList = &TnlSrcAddr[0];
        TnlIfSrcAddr.i4_Length = 16;
        TnlIfDestAddr.pu1_OctetList = &TnlDestAddr[0];
        TnlIfDestAddr.i4_Length = 16;
        MEMCPY (TnlIfSrcAddr.pu1_OctetList,
                pTnlSrcIpAddr->Ip6TnlAddr, TnlIfSrcAddr.i4_Length);
        MEMCPY (TnlIfDestAddr.pu1_OctetList,
                pTnlDestIpAddr->Ip6TnlAddr, TnlIfDestAddr.i4_Length);
    }

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4IfCxtId);
    if (CfaUtilSetTnlCxt (u4IfCxtId) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2FsTunlIfStatus (&u4ErrCode, (INT4) u4AddrType, &TnlIfSrcAddr,
                                 &TnlIfDestAddr, (INT4) u4TnlMode,
                                 (INT4) u4ConfigId,
                                 (INT4) CFA_RS_DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot delete entry in tunnel If Table\r\n");
        CfaUtilResetTnlCxt ();
        return CLI_FAILURE;
    }

    if (nmhSetFsTunlIfStatus ((INT4) u4AddrType, &TnlIfSrcAddr, &TnlIfDestAddr,
                              (INT4) u4TnlMode, (INT4) u4ConfigId,
                              CFA_RS_DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot delete entry in tunnel If Table\r\n");
        CfaUtilResetTnlCxt ();
        return CLI_FAILURE;
    }
    CfaUtilResetTnlCxt ();
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetTnlCkSumFlag                                 */
/*                                                                           */
/*     DESCRIPTION      : This function configures the tunnel Checksum Flag  */
/*                                                                           */
/*                                                                           */
/*     INPUT            :                                                    */
/*                        CliHandle - CLI Handler                            */
/*                        u4IfIndex - index of the interface                 */
/*                        i4ChkSumFlag - Checksum Flag                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
CfaSetTnlCkSumFlag (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4ChkSumFlag)
{
    UINT4               u4AddrType;
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;
    UINT4               u4ErrCode;
    UINT4               u4IfCxtId = 0;
    tTnlIfEntry         TnlIfEntry;
    tTnlIfEntry        *pTnlIfEntry = NULL;
    tSNMP_OCTET_STRING_TYPE TnlIfSrcAddr;
    tSNMP_OCTET_STRING_TYPE TnlIfDestAddr;
    UINT1               TnlSrcAddr[MAX_MAC_LENGTH];
    UINT1               TnlDestAddr[MAX_MAC_LENGTH];

    pTnlIfEntry = &TnlIfEntry;
    if (CfaFindTnlEntryFromIfIndex (u4IfIndex, pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }

    if (pTnlIfEntry == NULL)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    u4AddrType = pTnlIfEntry->u4AddrType;

    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        TnlIfSrcAddr.pu1_OctetList = &TnlSrcAddr[0];
        TnlIfSrcAddr.i4_Length = 4;
        TnlIfDestAddr.pu1_OctetList = &TnlDestAddr[0];
        TnlIfDestAddr.i4_Length = 4;

        PTR_ASSIGN4 (TnlIfSrcAddr.pu1_OctetList,
                     pTnlIfEntry->LocalAddr.Ip4TnlAddr);
        PTR_ASSIGN4 (TnlIfDestAddr.pu1_OctetList,
                     pTnlIfEntry->RemoteAddr.Ip4TnlAddr);
    }

    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        TnlIfSrcAddr.pu1_OctetList = &TnlSrcAddr[0];
        TnlIfSrcAddr.i4_Length = 16;
        TnlIfDestAddr.pu1_OctetList = &TnlDestAddr[0];
        TnlIfDestAddr.i4_Length = 16;

        MEMCPY (TnlIfSrcAddr.pu1_OctetList,
                pTnlIfEntry->LocalAddr.Ip6TnlAddr, TnlIfSrcAddr.i4_Length);
        MEMCPY (TnlIfDestAddr.pu1_OctetList,
                pTnlIfEntry->RemoteAddr.Ip6TnlAddr, TnlIfDestAddr.i4_Length);
    }

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4IfCxtId);
    if (CfaUtilSetTnlCxt (u4IfCxtId) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    u4EncapsMethod = pTnlIfEntry->u4EncapsMethod;
    u4ConfigId = pTnlIfEntry->u4ConfigId;
    if (nmhTestv2FsTunlIfCksumFlag
        (&u4ErrCode, (INT4) u4AddrType, &TnlIfSrcAddr, &TnlIfDestAddr,
         (INT4) u4EncapsMethod, (INT4) u4ConfigId,
         i4ChkSumFlag) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot enable checksum for this type of tunnel\r\n");
        CfaUtilResetTnlCxt ();
        return CLI_FAILURE;
    }

    if (nmhSetFsTunlIfCksumFlag
        ((INT4) u4AddrType, &TnlIfSrcAddr, &TnlIfDestAddr,
         (INT4) u4EncapsMethod, (INT4) u4ConfigId,
         i4ChkSumFlag) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Cannot enable checksum\r\n");
        CfaUtilResetTnlCxt ();
        return CLI_FAILURE;
    }
    CfaUtilResetTnlCxt ();
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetTnlPmtuFlag                                  */
/*                                                                           */
/*     DESCRIPTION      : This function configures the PAth MTU Flag         */
/*                                                                           */
/*     INPUT            :                                                    */
/*                        CliHandle - CLI Handler                            */
/*                        u4IfIndex - index of the interface                 */
/*                        i4AgeTime - Age Timer                              */
/*                        i4PmtuFlag - Path MTU Flag                         */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetTnlPmtuFlag (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4AgeTime,
                   INT4 i4PmtuFlag)
{
    UINT4               u4AddrType;
    UINT4               u4IfCxtId = 0;
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;
    UINT4               u4ErrCode;
    tTnlIfEntry         TnlIfEntry;
    tTnlIfEntry        *pTnlIfEntry = NULL;
    tSNMP_OCTET_STRING_TYPE TnlIfSrcAddr;
    tSNMP_OCTET_STRING_TYPE TnlIfDestAddr;
    UINT1               TnlSrcAddr[MAX_MAC_LENGTH];
    UINT1               TnlDestAddr[MAX_MAC_LENGTH];

    pTnlIfEntry = &TnlIfEntry;
    if (CfaFindTnlEntryFromIfIndex (u4IfIndex, pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }

    if (pTnlIfEntry == NULL)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    u4AddrType = pTnlIfEntry->u4AddrType;

    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        TnlIfSrcAddr.pu1_OctetList = &TnlSrcAddr[0];
        TnlIfSrcAddr.i4_Length = 4;
        TnlIfDestAddr.pu1_OctetList = &TnlDestAddr[0];
        TnlIfDestAddr.i4_Length = 4;

        PTR_ASSIGN4 (TnlIfSrcAddr.pu1_OctetList,
                     pTnlIfEntry->LocalAddr.Ip4TnlAddr);
        PTR_ASSIGN4 (TnlIfDestAddr.pu1_OctetList,
                     pTnlIfEntry->RemoteAddr.Ip4TnlAddr);
    }

    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        TnlIfSrcAddr.pu1_OctetList = &TnlSrcAddr[0];
        TnlIfSrcAddr.i4_Length = 16;
        TnlIfDestAddr.pu1_OctetList = &TnlDestAddr[0];
        TnlIfDestAddr.i4_Length = 16;

        MEMCPY (TnlIfSrcAddr.pu1_OctetList,
                pTnlIfEntry->LocalAddr.Ip6TnlAddr, TnlIfSrcAddr.i4_Length);
        MEMCPY (TnlIfDestAddr.pu1_OctetList,
                pTnlIfEntry->RemoteAddr.Ip6TnlAddr, TnlIfDestAddr.i4_Length);
    }

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4IfCxtId);
    if (CfaUtilSetTnlCxt (u4IfCxtId) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    u4EncapsMethod = pTnlIfEntry->u4EncapsMethod;
    u4ConfigId = pTnlIfEntry->u4ConfigId;
    if (nmhTestv2FsTunlIfPmtuFlag (&u4ErrCode, (INT4) u4AddrType, &TnlIfSrcAddr,
                                   &TnlIfDestAddr, (INT4) u4EncapsMethod,
                                   (INT4) u4ConfigId,
                                   i4PmtuFlag) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot enable PMTU Flag for this type of tunnel\r\n");
        CfaUtilResetTnlCxt ();
        return CLI_FAILURE;
    }

    if (nmhSetFsTunlIfPmtuFlag
        ((INT4) u4AddrType, &TnlIfSrcAddr, &TnlIfDestAddr,
         (INT4) u4EncapsMethod, (INT4) u4ConfigId, i4PmtuFlag) == SNMP_FAILURE)
    {

        CliPrintf (CliHandle, "\r%% Cannot enable PMTU Flag \r\n");
        CfaUtilResetTnlCxt ();
        return CLI_FAILURE;
    }

    if (i4AgeTime != 0)
    {
#ifdef IP_WANTED
        if (nmhTestv2FsIpPmtuEntryAge (&u4ErrCode, i4AgeTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Cannot Configure Pmtu Entry Age \r\n");
            CfaUtilResetTnlCxt ();
            return CLI_FAILURE;
        }

        if (nmhSetFsIpPmtuEntryAge (i4AgeTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Cannot Configure Pmtu Entry Age \r\n");
            CfaUtilResetTnlCxt ();
            return CLI_FAILURE;
        }
#endif /* IP_WANTED */
    }
    CfaUtilResetTnlCxt ();
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetTnlDir                                       */
/*                                                                           */
/*     DESCRIPTION      : This function configures the tunnel Direction      */
/*                                                                           */
/*     INPUT            :                                                    */
/*                        CliHandle - CLI Handler                            */
/*                        u4IfIndex - index of the interface                 */
/*                        u1DirFlag - Direction Flag                         */
/*                        u1Direction - Direction                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetTnlDir (tCliHandle CliHandle, UINT4 u4IfIndex,
              UINT1 u1DirFlag, UINT1 u1Direction)
{
    UINT4               u4AddrType;
    UINT4               u4IfCxtId = 0;
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;
    UINT4               u4ErrCode;
    tTnlIfEntry         TnlIfEntry;
    tTnlIfEntry        *pTnlIfEntry = NULL;
    tSNMP_OCTET_STRING_TYPE TnlIfSrcAddr;
    tSNMP_OCTET_STRING_TYPE TnlIfDestAddr;
    UINT1               TnlSrcAddr[MAX_MAC_LENGTH];
    UINT1               TnlDestAddr[MAX_MAC_LENGTH];

    pTnlIfEntry = &TnlIfEntry;
    if (CfaFindTnlEntryFromIfIndex (u4IfIndex, pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }
    if (pTnlIfEntry == NULL)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    u4AddrType = pTnlIfEntry->u4AddrType;

    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        TnlIfSrcAddr.pu1_OctetList = &TnlSrcAddr[0];
        TnlIfSrcAddr.i4_Length = 4;
        TnlIfDestAddr.pu1_OctetList = &TnlDestAddr[0];
        TnlIfDestAddr.i4_Length = 4;

        PTR_ASSIGN4 (TnlIfSrcAddr.pu1_OctetList,
                     pTnlIfEntry->LocalAddr.Ip4TnlAddr);
        PTR_ASSIGN4 (TnlIfDestAddr.pu1_OctetList,
                     pTnlIfEntry->RemoteAddr.Ip4TnlAddr);
    }

    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        TnlIfSrcAddr.pu1_OctetList = &TnlSrcAddr[0];
        TnlIfSrcAddr.i4_Length = 16;
        TnlIfDestAddr.pu1_OctetList = &TnlDestAddr[0];
        TnlIfDestAddr.i4_Length = 16;

        MEMCPY (TnlIfSrcAddr.pu1_OctetList,
                pTnlIfEntry->LocalAddr.Ip6TnlAddr, TnlIfSrcAddr.i4_Length);
        MEMCPY (TnlIfDestAddr.pu1_OctetList,
                pTnlIfEntry->RemoteAddr.Ip6TnlAddr, TnlIfDestAddr.i4_Length);
    }

    u4EncapsMethod = pTnlIfEntry->u4EncapsMethod;
    u4ConfigId = pTnlIfEntry->u4ConfigId;

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4IfCxtId);
    if (CfaUtilSetTnlCxt (u4IfCxtId) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2FsTunlIfDirFlag (&u4ErrCode, (INT4) u4AddrType, &TnlIfSrcAddr,
                                  &TnlIfDestAddr, (INT4) u4EncapsMethod,
                                  (INT4) u4ConfigId,
                                  (INT4) u1DirFlag) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Cannot configure tunnel dir flag\r\n");
        CfaUtilResetTnlCxt ();
        return CLI_FAILURE;
    }

    if (nmhSetFsTunlIfDirFlag ((INT4) u4AddrType, &TnlIfSrcAddr, &TnlIfDestAddr,
                               (INT4) u4EncapsMethod, (INT4) u4ConfigId,
                               u1DirFlag) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Cannot configure tunnel dir flag\r\n");
        CfaUtilResetTnlCxt ();
        return CLI_FAILURE;
    }

    if (u1Direction != 0)
    {
        if (nmhTestv2FsTunlIfDirection
            (&u4ErrCode, (INT4) u4AddrType, &TnlIfSrcAddr, &TnlIfDestAddr,
             (INT4) u4EncapsMethod, (INT4) u4ConfigId,
             (INT4) u1Direction) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Cannot configure tunnel direction \r\n");
            CfaUtilResetTnlCxt ();
            return CLI_FAILURE;
        }

        if (nmhSetFsTunlIfDirection
            ((INT4) u4AddrType, &TnlIfSrcAddr, &TnlIfDestAddr,
             (INT4) u4EncapsMethod, (INT4) u4ConfigId,
             u1Direction) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Cannot configure tunnel direction \r\n");
            CfaUtilResetTnlCxt ();
            return CLI_FAILURE;
        }
    }
    CfaUtilResetTnlCxt ();
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetTnlHopLimit                                  */
/*                                                                           */
/*     DESCRIPTION      : This function configures the tunnel Hop Limit      */
/*                                                                           */
/*     INPUT            :                                                    */
/*                        CliHandle - CLI Handler                            */
/*                        u4IfIndex - index of the interface                 */
/*                        i4HopLimit - Hop Limit                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetTnlHopLimit (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4HopLimit)
{
    UINT1               TnlSrcAddr[IPVX_IPV6_ADDR_LEN];
    UINT1               TnlDestAddr[IPVX_IPV6_ADDR_LEN];
    tTnlIfEntry         TnlIfEntry;
    tSNMP_OCTET_STRING_TYPE TnlIfSrcAddr;
    tSNMP_OCTET_STRING_TYPE TnlIfDestAddr;
    tTnlIfEntry        *pTnlIfEntry = NULL;
    UINT4               u4AddrType;
    UINT4               u4IfCxtId = 0;
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;
    UINT4               u4ErrCode;

    pTnlIfEntry = &TnlIfEntry;
    if (CfaFindTnlEntryFromIfIndex (u4IfIndex, pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }
    if (pTnlIfEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%% Error: Tunnel mode is not configured \r\n");
        return (CLI_FAILURE);
    }

    u4AddrType = pTnlIfEntry->u4AddrType;

    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        TnlIfSrcAddr.pu1_OctetList = &TnlSrcAddr[0];
        TnlIfSrcAddr.i4_Length = 4;
        TnlIfDestAddr.pu1_OctetList = &TnlDestAddr[0];
        TnlIfDestAddr.i4_Length = 4;

        PTR_ASSIGN4 (TnlIfSrcAddr.pu1_OctetList,
                     pTnlIfEntry->LocalAddr.Ip4TnlAddr);
        PTR_ASSIGN4 (TnlIfDestAddr.pu1_OctetList,
                     pTnlIfEntry->RemoteAddr.Ip4TnlAddr);
    }

    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        TnlIfSrcAddr.pu1_OctetList = &TnlSrcAddr[0];
        TnlIfSrcAddr.i4_Length = 16;
        TnlIfDestAddr.pu1_OctetList = &TnlDestAddr[0];
        TnlIfDestAddr.i4_Length = 16;

        MEMCPY (TnlIfSrcAddr.pu1_OctetList,
                pTnlIfEntry->LocalAddr.Ip6TnlAddr, TnlIfSrcAddr.i4_Length);
        MEMCPY (TnlIfDestAddr.pu1_OctetList,
                pTnlIfEntry->RemoteAddr.Ip6TnlAddr, TnlIfDestAddr.i4_Length);
    }

    u4EncapsMethod = pTnlIfEntry->u4EncapsMethod;
    u4ConfigId = pTnlIfEntry->u4ConfigId;

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4IfCxtId);
    if (CfaUtilSetTnlCxt (u4IfCxtId) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhTestv2FsTunlIfHopLimit (&u4ErrCode, (INT4) u4AddrType, &TnlIfSrcAddr,
                                   &TnlIfDestAddr, (INT4) u4EncapsMethod,
                                   (INT4) u4ConfigId,
                                   i4HopLimit) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Cannot configure tunnel hop limit\r\n");
        CfaUtilResetTnlCxt ();
        return CLI_FAILURE;
    }

    if (nmhSetFsTunlIfHopLimit
        ((INT4) u4AddrType, &TnlIfSrcAddr, &TnlIfDestAddr,
         (INT4) u4EncapsMethod, (INT4) u4ConfigId, i4HopLimit) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Cannot configure tunnel dir flag\r\n");
        CfaUtilResetTnlCxt ();
        return CLI_FAILURE;
    }

    CfaUtilResetTnlCxt ();
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaIvrSetSecondaryIpAddress                        */
/*                                                                           */
/*     DESCRIPTION      : This function sets the secondary IP address of an  */
/*                        IVR interface                                      */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
/*                        u4IpAddr, u4SubnetMask - IPAddress, Subnet Mask    */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SUCCESS/FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIvrSetSecondaryIpAddress (tCliHandle CliHandle, UINT4 u4IfIndex,
                             UINT4 u4IpAddr, UINT4 u4IpSubnetMask)
{
    INT4                i4RowStatus = 0;
    UINT4               u4PrevNetMask = 0;
    UINT4               u4PrevBcastAddr;
    UINT4               u4BcastAddr;
    UINT4               u4ErrCode;
    tIpAddrInfo         SecondaryInfo;

    MEMSET (&SecondaryInfo, 0, sizeof (tIpAddrInfo));

    if (nmhGetIfSecondaryIpRowStatus ((INT4) u4IfIndex, u4IpAddr,
                                      &i4RowStatus) == SNMP_SUCCESS)
    {
        if (CfaIpIfGetSecondaryAddressInfo (u4IfIndex, u4IpAddr,
                                            &SecondaryInfo) == CFA_FAILURE)
        {
            CliPrintf (CliHandle, "%% Fetching Secondary Address failed\r\n");
            return CLI_FAILURE;
        }

        if (SecondaryInfo.u1AllocMethod == CFA_IP_IF_VIRTUAL)
        {
            CliPrintf (CliHandle,
                       "%% IP Address already present as Virtual IP\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetIfSecondaryIpRowStatus (u4IfIndex, u4IpAddr,
                                          CFA_RS_NOTINSERVICE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2IfSecondaryIpRowStatus
            (&u4ErrCode, (INT4) u4IfIndex, u4IpAddr,
             CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetIfSecondaryIpRowStatus (u4IfIndex, u4IpAddr,
                                          CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2IfSecondaryIpSubnetMask
        (&u4ErrCode, (INT4) u4IfIndex, u4IpAddr,
         u4IpSubnetMask) == SNMP_FAILURE)
    {
        /* We have created the new row.So Delete it  */
        if (i4RowStatus == 0)
        {
            nmhSetIfSecondaryIpRowStatus (u4IfIndex, u4IpAddr, CFA_RS_DESTROY);
        }
        return CLI_FAILURE;
    }

    nmhGetIfSecondaryIpSubnetMask ((INT4) u4IfIndex, u4IpAddr, &u4PrevNetMask);
    nmhGetIfSecondaryIpBroadcastAddr ((INT4) u4IfIndex, u4IpAddr,
                                      &u4PrevBcastAddr);
    if (nmhSetIfSecondaryIpSubnetMask (u4IfIndex, u4IpAddr, u4IpSubnetMask) ==
        SNMP_FAILURE)
    {
        /* If the row is newly created ,Destroy it.Else, restore the
         * original values */
        if (i4RowStatus == 0)
        {
            nmhSetIfSecondaryIpRowStatus (u4IfIndex, u4IpAddr, CFA_RS_DESTROY);
        }
        else
        {
            nmhSetIfSecondaryIpSubnetMask (u4IfIndex, u4IpAddr, u4PrevNetMask);
        }
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    u4BcastAddr = u4IpAddr | (~(u4IpSubnetMask));
    if (nmhTestv2IfSecondaryIpBroadcastAddr
        (&u4ErrCode, (INT4) u4IfIndex, u4IpAddr, u4BcastAddr) == SNMP_FAILURE)
    {
        /* If the row is newly created ,Destroy it.Else, restore the
         * original values */
        if (i4RowStatus == 0)
        {
            nmhSetIfSecondaryIpRowStatus (u4IfIndex, u4IpAddr, CFA_RS_DESTROY);
        }
        else
        {
            nmhSetIfSecondaryIpSubnetMask (u4IfIndex, u4IpAddr, u4PrevNetMask);
        }
        return CLI_FAILURE;
    }

    if (nmhSetIfSecondaryIpBroadcastAddr (u4IfIndex, u4IpAddr,
                                          u4BcastAddr) == SNMP_FAILURE)
    {
        if (i4RowStatus == 0)
        {
            nmhSetIfSecondaryIpRowStatus (u4IfIndex, u4IpAddr, CFA_RS_DESTROY);
        }
        else
        {
            nmhSetIfSecondaryIpSubnetMask (u4IfIndex, u4IpAddr, u4PrevNetMask);
            nmhSetIfSecondaryIpBroadcastAddr (u4IfIndex, u4IpAddr,
                                              u4PrevBcastAddr);
        }
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2IfSecondaryIpRowStatus (&u4ErrCode, (INT4) u4IfIndex, u4IpAddr,
                                         CFA_RS_ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIfSecondaryIpRowStatus (u4IfIndex, u4IpAddr,
                                      CFA_RS_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaIvrDeleteIpAddress                              */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the secondary IP address of  */
/*                        an IVR interface                                   */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
/*                        u4IpAddr  - IPAddress                              */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIvrDeleteIpAddress (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4IpAddr)
{
    UINT4               u4ErrCode;
    UINT4               u4PrimaryAddr = 0;
    INT4                i4NextIfIndex = 0;
    UINT4               u4SecondaryAddr = 0;
    tIpAddrInfo         SecondaryInfo;
#ifdef VXLAN_WANTED
    INT4                i4IfaceType = 0;
#endif

    MEMSET (&SecondaryInfo, 0, sizeof (tIpAddrInfo));

#ifdef VXLAN_WANTED
    if (nmhGetIfMainType ((INT4) u4IfIndex, &i4IfaceType) == SNMP_SUCCESS)
    {
        if (i4IfaceType == CFA_LOOPBACK)
        {
            if (VxlanIsLoopbackUsed (u4IfIndex) == VXLAN_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r\n%% Deletion/Modification of loopback "
                           "interface is not allowed since it is used by VXLAN module\r\n");
                return CLI_FAILURE;

            }
        }
    }
#endif

    nmhGetIfIpAddr ((INT4) u4IfIndex, &u4PrimaryAddr);

    if (u4IpAddr == u4PrimaryAddr)
    {
        /* Primary address is coming for deletion.If there is any secondary
         * address availble on the interface, Dont allow deletion of 
         * primary address */
        if ((nmhGetNextIndexIfSecondaryIpAddressTable ((INT4) u4IfIndex,
                                                       &i4NextIfIndex, 0,
                                                       &u4SecondaryAddr) ==
             SNMP_SUCCESS) && ((INT4) u4IfIndex == i4NextIfIndex))
        {
            CLI_SET_ERR (CFA_CLI_PRIMARY_IP_DEL_ERR);
            return CLI_FAILURE;
        }
#ifdef RIP_WANTED
        RipDeleteIfRecord (u4IfIndex, u4IpAddr, CFA_DEL);
#endif

        return (CfaIvrSetIpAddress (CliHandle, u4IfIndex, 0, 0));
    }
    else if (CfaIpIfGetSecondaryAddressInfo (u4IfIndex, u4IpAddr,
                                             &SecondaryInfo) == CFA_FAILURE)
    {
        CLI_SET_ERR (CLI_CFA_UNASSIGNED_IPADDR_ERR);
        return CLI_FAILURE;
    }

    if (CfaIpIfGetSecondaryAddressInfo (u4IfIndex, u4IpAddr,
                                        &SecondaryInfo) == CFA_FAILURE)
    {
        CliPrintf (CliHandle, "%% Fetching Secondary Address failed\r\n");
        return CLI_FAILURE;
    }

    if (SecondaryInfo.u1AllocMethod == CFA_IP_IF_VIRTUAL)
    {
        CliPrintf (CliHandle,
                   "%% Cannot Delete IP Address present as Virtual IP\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2IfSecondaryIpRowStatus (&u4ErrCode, (INT4) u4IfIndex, u4IpAddr,
                                         CFA_RS_DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

#ifdef RIP_WANTED
    RipDeleteIfRecord (u4IfIndex, u4IpAddr, CFA_DEL);
#endif

    if (nmhSetIfSecondaryIpRowStatus (u4IfIndex, u4IpAddr,
                                      CFA_RS_DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliSetDot1QEncapForL3SubIf                      */
/*                                                                           */
/*     DESCRIPTION      : This function sets Dot1Q VLANID for the            */
/*                        L3SubInterface                                     */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4IfIndex - L3Sub Interface index                  */
/*                        CliHandle - CLI Handler                            */
/*                        u4VlanId  - VLAN ID to be encapsulated             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCliSetDot1QEncapForL3SubIf (tCliHandle CliHandle, UINT4 u4IfIndex,
                               UINT4 u4VlanId)
{
    UINT4               u4ErrCode;
    UINT4               u4ContextId;

#ifdef VCM_WANTED
    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
#endif

    if (CfaGetVlanInterfaceL3IndexInCxt
        (u4ContextId, (tVlanIfaceVlanId) u4VlanId) != CFA_INVALID_INDEX)
    {
        CliPrintf (CliHandle, "\r%% Cannot Set Encapsulation! As VLAN %d is "
                   "associated with L3Interface VLAN \r\n", u4VlanId);
        return SNMP_FAILURE;
    }

    if (nmhTestv2IfMainEncapDot1qVlanId
        (&u4ErrCode, (INT4) u4IfIndex, (INT4) u4VlanId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIfMainEncapDot1qVlanId (u4IfIndex, (INT4) u4VlanId) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliDelDot1QEncapForL3SubIf                      */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the Dot1Q encapsulation for  */
/*                        the L3SubInterface                                 */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4IfIndex - L3Sub Interface index                  */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCliDelDot1QEncapForL3SubIf (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    tIpConfigInfo       IpIfInfo;

    MEMSET (&IpIfInfo, 0, sizeof (tIpConfigInfo));

    if (CfaIpIfGetIfInfo (u4IfIndex, &IpIfInfo) == CFA_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Check if IP address is configured for L3Subinterface */
    /* If configured , do not allow user to remove the encapsulation */
    if (IpIfInfo.u4Addr != 0)
    {
        CliPrintf (CliHandle,
                   "\r%% Unconfigure IP address to unset Encapsulation\n\r");
        return CLI_FAILURE;
    }

    if (nmhSetIfMainEncapDot1qVlanId (u4IfIndex, 0) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetCustTLV                                      */
/*                                                                           */
/*     DESCRIPTION      : This function creates/delete the                   */
/*                        TLV(Type/Length/Value) for a given port.           */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
/*                        Length  - Length of the value.                     */
/*                        Vlaue   - Value to be configured.                  */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetCustTLV (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4TLVType,
               UINT4 u4TLVLength, UINT1 *pu1TLVValue, UINT4 u4Flag)
{
    tSNMP_OCTET_STRING_TYPE tCustTLVValue;
    UINT4               u4ErrCode;

    if (u4Flag == CFA_TLV_CREATE)
    {
        tCustTLVValue.pu1_OctetList = pu1TLVValue;
        tCustTLVValue.i4_Length = (INT4) STRLEN (pu1TLVValue);

        if (nmhTestv2IfCustTLVRowStatus (&u4ErrCode,
                                         (INT4) u4IfIndex,
                                         u4TLVType,
                                         CFA_RS_CREATEANDWAIT) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetIfCustTLVRowStatus (u4IfIndex,
                                      u4TLVType,
                                      CFA_RS_CREATEANDWAIT) == SNMP_SUCCESS)
        {
            if (nmhTestv2IfCustTLVLength (&u4ErrCode,
                                          (INT4) u4IfIndex,
                                          u4TLVType,
                                          u4TLVLength) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Invalid length given\n\r");

                nmhSetIfCustTLVRowStatus (u4IfIndex, u4TLVType, CFA_RS_DESTROY);
                return CLI_FAILURE;
            }

            if (nmhSetIfCustTLVLength (u4IfIndex, u4TLVType,
                                       u4TLVLength) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);

                nmhSetIfCustTLVRowStatus (u4IfIndex, u4TLVType, CFA_RS_DESTROY);
                return CLI_FAILURE;
            }

            if (nmhTestv2IfCustTLVValue (&u4ErrCode,
                                         (INT4) u4IfIndex,
                                         u4TLVType,
                                         &tCustTLVValue) != SNMP_SUCCESS)
            {
                nmhSetIfCustTLVRowStatus (u4IfIndex, u4TLVType, CFA_RS_DESTROY);
                return CLI_FAILURE;
            }

            if (nmhSetIfCustTLVValue (u4IfIndex, u4TLVType,
                                      &tCustTLVValue) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Failed to set the Value\n\r");

                nmhSetIfCustTLVRowStatus (u4IfIndex, u4TLVType, CFA_RS_DESTROY);
                return CLI_FAILURE;
            }

            if (nmhSetIfCustTLVRowStatus (u4IfIndex, u4TLVType,
                                          CFA_RS_ACTIVE) != SNMP_SUCCESS)
            {

                nmhSetIfCustTLVRowStatus (u4IfIndex, u4TLVType, CFA_RS_DESTROY);
                return CLI_FAILURE;
            }
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if (u4Flag == CFA_TLV_DELETE)
    {
        if (u4TLVType != 0)
        {

            if (nmhTestv2IfCustTLVRowStatus (&u4ErrCode,
                                             (INT4) u4IfIndex,
                                             u4TLVType,
                                             (INT4) CFA_RS_DESTROY) !=
                SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Invalid TLV Type\n\r");
                return CLI_FAILURE;
            }
            if (nmhSetIfCustTLVRowStatus (u4IfIndex,
                                          u4TLVType,
                                          CFA_RS_DESTROY) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliSetSysSpecificPortID                         */
/*                                                                           */
/*     DESCRIPTION      : This function configures an opaque attribute       */
/*                        on a particular interface                          */
/*                                                                           */
/*     INPUT            : i4IfaceIndex - Index of the interface on which     */
/*                                       opaque attribute needs to configured*/
/*                        u4PortID  - The system specific PortID that is to  */
/*                                    be configured on this interface        */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCliSetSysSpecificPortID (tCliHandle CliHandle, INT4 i4IfaceIndex,
                            UINT4 u4PortID)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2IfMainExtSysSpecificPortID (&u4ErrCode, i4IfaceIndex, u4PortID)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIfMainExtSysSpecificPortID (i4IfaceIndex, u4PortID) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliSetOpqArribute                               */
/*                                                                           */
/*     DESCRIPTION      : This function configures an opaque attribute       */
/*                        on a particular interface                          */
/*                                                                           */
/*     INPUT            : i4IfaceIndex - Index of the interface on which     */
/*                                    opaque attribute needs to configured   */
/*                        i4OpqAttrID - The id of the opaque attribute which */
/*                                      needs to be configured               */
/*                        u4OpqAttrValue - The opaque attribbute value for   */
/*                                         this i4OpqAttrID                  */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCliSetOpqArribute (tCliHandle CliHandle, INT4 i4IfaceIndex, INT4 i4OpqAttrID,
                      UINT4 u4OpqAttrValue)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4ReturnVal = 0;

    if (nmhGetIfCustOpaqueRowStatus (i4IfaceIndex, i4OpqAttrID, &i4ReturnVal)
        == SNMP_FAILURE)
    {
        if (nmhTestv2IfCustOpaqueRowStatus (&u4ErrorCode, i4IfaceIndex,
                                            i4OpqAttrID, CFA_RS_CREATEANDWAIT)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetIfCustOpaqueRowStatus
            (i4IfaceIndex, i4OpqAttrID, CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2IfCustOpaqueAttribute (&u4ErrorCode, i4IfaceIndex,
                                        i4OpqAttrID, u4OpqAttrValue)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIfCustOpaqueAttribute (i4IfaceIndex, i4OpqAttrID, u4OpqAttrValue)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (i4ReturnVal == CFA_RS_ACTIVE)
    {
        return CLI_SUCCESS;
    }

    if (nmhTestv2IfCustOpaqueRowStatus (&u4ErrorCode, i4IfaceIndex,
                                        i4OpqAttrID, CFA_RS_ACTIVE)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIfCustOpaqueRowStatus (i4IfaceIndex, i4OpqAttrID, CFA_RS_ACTIVE)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliDeleteOpqArribute                            */
/*                                                                           */
/*     DESCRIPTION      : This function deletes an opaque attribute          */
/*                        on a particular interface                          */
/*                                                                           */
/*     INPUT            : i4IfaceIndex - Index of the interface on which     */
/*                                    opaque attribute needs to deleted      */
/*                        i4OpqAttrID - The id of the opaque attribute which */
/*                                      needs to be deleted                  */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCliDeleteOpqArribute (tCliHandle CliHandle, INT4 i4IfaceIndex,
                         INT4 i4OpqAttrID)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IfCustOpaqueRowStatus (&u4ErrorCode, i4IfaceIndex,
                                        i4OpqAttrID, CFA_RS_DESTROY)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIfCustOpaqueRowStatus (i4IfaceIndex, i4OpqAttrID, CFA_RS_DESTROY)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliDelAllCustomParams                           */
/*                                                                           */
/*     DESCRIPTION      : This function deletes all the opaque attributes    */
/*                        on an interface                                    */
/*                                                                           */
/*     INPUT            : i4IfaceIndex-Index of the interface on which opaque*/
/*                                    attribute needs to deleted             */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCliDelAllCustomParams (tCliHandle CliHandle, INT4 i4IfaceIndex)
{
    INT4                i4OpqAttrID = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4NodeCount = 0;
    UINT4               u4IfIndex;
    tTMO_SLL           *pTlvList = NULL;
    tIfTlvInfoStruct   *pIfLastTlvNode = NULL;

    for (i4OpqAttrID = 1; i4OpqAttrID <= CFA_MAX_OPAQUE_ATTRS; i4OpqAttrID++)
    {
        if (CFA_IF_OPQ_CONFIGURED ((UINT4) i4IfaceIndex, i4OpqAttrID))
        {
            if (nmhTestv2IfCustOpaqueRowStatus (&u4ErrorCode, i4IfaceIndex,
                                                i4OpqAttrID, CFA_RS_DESTROY)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetIfCustOpaqueRowStatus (i4IfaceIndex, i4OpqAttrID,
                                             CFA_RS_DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }

    u4IfIndex = (UINT4) i4IfaceIndex;

    pTlvList = &CFA_IF_TLV_INFO (u4IfIndex);

    u4NodeCount = TMO_SLL_Count (pTlvList);

    while (u4NodeCount != 0)
    {
        pIfLastTlvNode = (tIfTlvInfoStruct *) TMO_SLL_Last (pTlvList);

        if (nmhTestv2IfCustTLVRowStatus (&u4ErrorCode,
                                         (INT4) u4IfIndex,
                                         pIfLastTlvNode->u4TlvType,
                                         (INT4) CFA_RS_DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetIfCustTLVRowStatus (u4IfIndex,
                                      pIfLastTlvNode->u4TlvType,
                                      CFA_RS_DESTROY) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        u4NodeCount--;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowSysSpecificPortID                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays PortIDs on all interfaces   */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowSysSpecificPortID (tCliHandle CliHandle)
{
    INT4                i4IfIndex = 0;
    INT4                i4PrevIfIndex = 0;
    UINT4               u4PortID = 0;
    UINT1              *pu1Alias = NULL;
    UINT1               au1Tmp[CFA_MAX_PORT_NAME_LENGTH];

    pu1Alias = &au1Tmp[0];

    if (nmhGetFirstIndexIfMainExtTable (&i4IfIndex) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle, "\r\n%-10s%-10s\r\n", "Interface", "PortID");
    CliPrintf (CliHandle, "\r\n%-10s%-10s\r\n", "--------", "--------");

    do
    {
        if ((CfaIsILanInterface ((UINT4) i4IfIndex) == CFA_FALSE) &&
            (CfaIsIfEntryProgrammingAllowed ((UINT4) i4IfIndex) == CFA_TRUE))
        {
            nmhGetIfMainExtSysSpecificPortID (i4IfIndex, &u4PortID);

            if (u4PortID != 0)
            {
                CfaGetInterfaceNameFromIndex ((UINT4) i4IfIndex, pu1Alias);
                CliPrintf (CliHandle, "\r\n%-10s%-10u\r\n", pu1Alias, u4PortID);
            }
        }
        i4PrevIfIndex = i4IfIndex;
    }
    while (nmhGetNextIndexIfMainExtTable (i4PrevIfIndex, &i4IfIndex) ==
           SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowCustomParam                                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays all the opaque attributes   */
/*                        and TLV configured on all the interfaces           */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowCustomParam (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE CustTLVValue;
    tTMO_SLL           *pTlvList = NULL;
    tIfTlvInfoStruct   *pIfTlvNode = NULL;
    UINT4               u4TlvLength = 0;
    UINT4               u4OpqAttrValue = 0;
    UINT4               u4Index;
    UINT1              *pu1Alias = NULL;
    UINT1               au1Tmp[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1TLVValue[CFA_TLV_MAX_LENGTH + 1];
    UINT1               u1IfaceExists = 0;
    UINT1               u1TLVExists = 0;
    INT4                i4IfIndex = 1;
    INT4                i4OpqAttrID = 0;
    INT4                i4ReturnVal = 0;

    pu1Alias = &au1Tmp[0];

    CFA_CDB_SCAN_WITH_LOCK (i4IfIndex, BRG_MAX_PHY_PLUS_LOG_PORTS,
                            CFA_ALL_IFTYPE)
    {
        /* Flexible ifIndex changes: for lexicographic display
         */
        CFA_DS_LOCK ();
        if ((CFA_CDB_IF_TYPE ((UINT4) i4IfIndex) != CFA_ENET) &&
            (CFA_CDB_IF_TYPE ((UINT4) i4IfIndex) != CFA_LAGG))
        {
            CFA_DS_UNLOCK ();
            continue;
        }
        CFA_DS_UNLOCK ();

        for (i4OpqAttrID = 1; i4OpqAttrID <= CFA_MAX_OPAQUE_ATTRS;
             i4OpqAttrID++)
        {
            if (nmhValidateIndexInstanceIfCustOpaqueAttrTable
                (i4IfIndex, i4OpqAttrID) == SNMP_SUCCESS)
            {
                nmhGetIfCustOpaqueRowStatus (i4IfIndex, i4OpqAttrID,
                                             &i4ReturnVal);
                if (i4ReturnVal == CFA_RS_ACTIVE)
                {
                    if (u1IfaceExists == 0)
                    {
                        CfaGetInterfaceNameFromIndex ((UINT4) i4IfIndex,
                                                      pu1Alias);
                        CliPrintf (CliHandle, "\r\n%s\r\n", pu1Alias);
                        CliPrintf (CliHandle,
                                   "\r\n%-10s%-10s\r\n", "AttrID", "AttrValue");
                        CliPrintf (CliHandle,
                                   "\r\n%-10s%-10s\r\n", "--------",
                                   "--------");
                        u1IfaceExists = 1;
                    }
                    nmhGetIfCustOpaqueAttribute (i4IfIndex, i4OpqAttrID,
                                                 &u4OpqAttrValue);
                    CliPrintf (CliHandle, "%-10d%-10u\r\n", i4OpqAttrID,
                               u4OpqAttrValue);
                }
            }

        }

        u4Index = (UINT4) i4IfIndex;

        if (CFA_IF_ENTRY_USED (u4Index))
        {
            if (CfaIfIsBridgedInterface (i4IfIndex) != CFA_TRUE)
            {
                continue;
            }

            MEMSET (au1TLVValue, 0, CFA_TLV_MAX_LENGTH + 1);

            CustTLVValue.pu1_OctetList = au1TLVValue;
            CustTLVValue.i4_Length = CFA_TLV_MAX_LENGTH + 1;

            pTlvList = &CFA_IF_TLV_INFO (u4Index);

            TMO_SLL_Scan (pTlvList, pIfTlvNode, tIfTlvInfoStruct *)
            {
                if (pIfTlvNode->i4TlvRowStatus == CFA_RS_ACTIVE)
                {
                    if (u1IfaceExists == 0)
                    {
                        CfaGetInterfaceNameFromIndex ((UINT4) i4IfIndex,
                                                      pu1Alias);
                        CliPrintf (CliHandle, "\r\n%s\r\n", pu1Alias);
                        u1IfaceExists = 1;
                    }
                    if (u1TLVExists == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n%-10s%-10s%-10s\r\n", "Type", "Length",
                                   "Value");
                        CliPrintf (CliHandle, "\r\n%-10s%-10s%-10s\r\n",
                                   "--------", "--------", "--------");
                        u1TLVExists = 1;
                    }
                    nmhGetIfCustTLVLength (i4IfIndex, pIfTlvNode->u4TlvType,
                                           &u4TlvLength);
                    nmhGetIfCustTLVValue (i4IfIndex, pIfTlvNode->u4TlvType,
                                          &CustTLVValue);
                    CliPrintf (CliHandle, "%-10u%-10u%-10s\r\n",
                               pIfTlvNode->u4TlvType, u4TlvLength,
                               CustTLVValue.pu1_OctetList);
                }
            }
        }
        u1IfaceExists = 0;
        u1TLVExists = 0;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowConnectParam                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays the internal ports of a     */
/*                        context connected to an another internal port      */
/*                        within another context.                            */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaShowConnectParam (tCliHandle CliHandle, tPortList IfPortList)
{
    INT4                i4RowStatus = 0;
    UINT4               u4IfIndex;
    UINT4               u4StackedIfIndex;
    INT4                i4RetVal;
    INT1               *pIfName = NULL;
    tTMO_SLL           *pHighStack = NULL;
    tStackInfoStruct   *pHighStackListScan = NULL;
    UINT2               u2ByteIndex;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT2               u2LocalPortId = 0;
    UINT4               u4ContextId = 0;
    UINT1               au1IfBrgPortType[CFA_MAX_BRG_PORT_TYPE_LEN];
    UINT1               au1ContextName[CFA_SWITCH_ALIAS_LEN];

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pIfName = (INT1 *) (&au1IfName[0]);

    CliPrintf (CliHandle, "\r\nIntra Bridge Connections \r\n");
    CliPrintf (CliHandle, "-------------------------------- \r\n");

    for (u2ByteIndex = 0; u2ByteIndex < BRG_PORT_ILAN_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = IfPortList[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < INTERNAL_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {

            if ((u1PortFlag & INTERNAL_BIT8) != 0)
            {
                u4IfIndex = (UINT4) ((u2ByteIndex * INTERNAL_PORTS_PER_BYTE) +
                                     u2BitIndex + 1);
                nmhGetIfBridgeILanIfStatus ((INT4) u4IfIndex, &i4RowStatus);
                if (i4RowStatus == CFA_RS_ACTIVE)
                {
                    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
                    CfaCliGetIfName (u4IfIndex, pIfName);
                    CliPrintf (CliHandle, "I-LAN : %s \r\n", pIfName);
                    pHighStack = &CFA_IF_STACK_HIGH (u4IfIndex);
                    pHighStackListScan =
                        (tStackInfoStruct *)
                        CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
                    TMO_SLL_Scan (pHighStack, pHighStackListScan,
                                  tStackInfoStruct *)
                    {
                        if (pHighStackListScan->u4IfIndex != 0)
                        {
                            u4ContextId = 0;
                            u4StackedIfIndex =
                                (UINT4) pHighStackListScan->u4IfIndex;
                            i4RetVal =
                                VcmGetContextInfoFromIfIndex (u4StackedIfIndex,
                                                              &u4ContextId,
                                                              &u2LocalPortId);
                            if (i4RetVal == VCM_FAILURE)
                            {
                                CliPrintf (CliHandle, "Switch : %5s", "");
                            }
                            else
                            {
                                VcmGetAliasName (u4ContextId, au1ContextName);
                                CliPrintf (CliHandle, "Switch : %5s",
                                           au1ContextName);
                            }

                            MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
                            CfaCliGetIfName (u4StackedIfIndex, pIfName);
                            CliPrintf (CliHandle, "\tPort : %5s ", pIfName);
                            MEMSET (au1IfBrgPortType, 0,
                                    CFA_MAX_BRG_PORT_TYPE_LEN);
                            CfaGetInterfaceBrgPortTypeString ((INT4)
                                                              u4StackedIfIndex,
                                                              au1IfBrgPortType);
                            CliPrintf (CliHandle,
                                       "\tBridge Port Type: %20s\r\n",
                                       au1IfBrgPortType);
                        }
                    }
                    CliPrintf (CliHandle, "\r\n");
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetOpenflowInterface                            */
/*                                                                           */
/*     DESCRIPTION      : This function will be used to create/delete the    */
/*                        openflow interface in CFA. This will set the       */
/*                        subtype to openflow and applicable to only ethernet*/
/*                        IfType                                             */
/*                                                                           */
/*     INPUT            : CliHandle - Cli Handler.                           */
/*                      : u4IfIndex - Index of the interface to be created   */
/*                        pu1IfName - Name of the interface                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetOpenflowInterface (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4Action)
{
    UINT4               u4ErrCode = 0;
    INT4                i4AdminStatus = CFA_IF_DOWN;
    INT4                i4SubType = CFA_SUBTYPE_SISP_INTERFACE;

    nmhGetIfMainAdminStatus ((INT4) u4IfIndex, &i4AdminStatus);
    if (i4AdminStatus == (INT4) CFA_IF_UP)
    {
        CliPrintf (CliHandle, "\r%% Make admin status down for setting");
        CliPrintf (CliHandle, " the Bridged Interface Status\r\n");
        return CLI_FAILURE;
    }

    if (nmhGetIfMainExtSubType ((INT4) u4IfIndex, &i4SubType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to get If Sub Type\r\n");
        return CLI_FAILURE;
    }

    /* There is no change in Sub Type . So Do nothing */
    if ((UINT4) i4SubType == u4Action)
    {
        return CLI_SUCCESS;
    }

    if (nmhTestv2IfMainExtSubType
        (&u4ErrCode, (INT4) u4IfIndex, (INT4) u4Action) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIfMainExtSubType ((INT4) u4IfIndex, (INT4) u4Action) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCreateSispInterface                             */
/*                                                                           */
/*     DESCRIPTION      : This function will be used to create a logical     */
/*                        interface in CFA. This will configure the following*/
/*                        parameters for the interface. IfAlias & IfMainType */
/*                        This port will not be created in gapIfTable.       */
/*                                                                           */
/*     INPUT            : CliHandle - Cli Handler.                           */
/*                      : u4IfIndex - Index of the interface to be created   */
/*                        pu1IfName - Name of the interface                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCreateSispInterface (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 *pu1IfName)
{
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE Alias;

    MEMSET (&Alias, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    Alias.i4_Length = (INT4) STRLEN (pu1IfName);
    Alias.pu1_OctetList = pu1IfName;

    /* we can directly call set routine, because validation is done already
     * in the function CfaCreateInterface() */
    nmhSetIfAlias (u4IfIndex, &Alias);

    /* Set the Type */
    if (nmhTestv2IfMainType (&u4ErrCode, (INT4) u4IfIndex,
                             (INT4) CFA_PROP_VIRTUAL_INTERFACE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return (CLI_FAILURE);
    }

    if (nmhSetIfMainType (u4IfIndex, CFA_PROP_VIRTUAL_INTERFACE) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return (CLI_FAILURE);
    }

    if (nmhTestv2IfMainExtSubType (&u4ErrCode, (INT4) u4IfIndex,
                                   (INT4) CFA_SUBTYPE_SISP_INTERFACE) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return (CLI_FAILURE);
    }
    if (nmhSetIfMainExtSubType (u4IfIndex, CFA_SUBTYPE_SISP_INTERFACE) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return (CLI_FAILURE);
    }

    /* All the mandatory parameters are set. 
     * So make the row active */
    nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetBackPlaneStatus                              */
/*                                                                           */
/*     DESCRIPTION      : This function configures the backplane status of an*/
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
/*                        i4Status - Status                                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE.                           */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetBackPlaneStatus (UINT4 u4IfIndex, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IfMainExtInterfaceType (&u4ErrorCode, (INT4) u4IfIndex,
                                         i4Status) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIfMainExtInterfaceType ((INT4) u4IfIndex, i4Status)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaAddIfTypeDenyProtocolEntry                      */
/*                                                                           */
/*     DESCRIPTION      : This function configures the IfType Deny protocol  */
/*                        entry.                                             */
/*                                                                           */
/*     INPUT            : i4ContextId - Context Id                           */
/*                        i4ifType      - Interface type of port             */
/*                        i4BrgPortType - Bridge Port Type of Port           */
/*                        i4Protocol    - Protocol Id                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE.                           */
/*                                                                           */
/*****************************************************************************/
INT4
CfaAddIfTypeDenyProtocolEntry (tCliHandle CliHandle,
                               INT4 i4ContextId, INT4 i4ifType,
                               INT4 i4BrgPortType, INT4 i4Protocol)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IfTypeProtoDenyRowStatus (&u4ErrorCode, (UINT4) i4ContextId,
                                           i4ifType, i4BrgPortType,
                                           i4Protocol,
                                           (INT4) CFA_RS_CREATEANDGO) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIfTypeProtoDenyRowStatus (i4ContextId, i4ifType, i4BrgPortType,
                                        i4Protocol, CFA_RS_CREATEANDGO) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaRemIfTypeDenyProtocolEntry                      */
/*                                                                           */
/*     DESCRIPTION      : This function removes the IfType Deny protocol     */
/*                        entry.                                             */
/*                                                                           */
/*     INPUT            : i4ContextId - Context Id                           */
/*                        i4ifType      - Interface type of port             */
/*                        i4BrgPortType - Bridge Port Type of Port           */
/*                        i4Protocol    - Protocol Id                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE.                           */
/*                                                                           */
/*****************************************************************************/
INT4
CfaRemIfTypeDenyProtocolEntry (tCliHandle CliHandle,
                               INT4 i4ContextId, INT4 i4ifType,
                               INT4 i4BrgPortType, INT4 i4Protocol)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IfTypeProtoDenyRowStatus (&u4ErrorCode, (UINT4) i4ContextId,
                                           i4ifType, i4BrgPortType,
                                           i4Protocol, (INT4) CFA_RS_DESTROY) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIfTypeProtoDenyRowStatus (i4ContextId, i4ifType, i4BrgPortType,
                                        i4Protocol, CFA_RS_DESTROY) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliShowIfTypeDenyProtoTable                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays the If Deny Protocol Table  */
/*                        for Context                                        */
/*                                                                           */
/*     INPUT            : i4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE.                           */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCliShowIfTypeDenyProtoTable (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    UINT4               u4NextContextId = 0;
    INT4                i4IfType = 0;
    INT4                i4NextIfType = 0;
    INT4                i4BrgPortType = 0;
    INT4                i4NextBrgPortType = 0;
    INT4                i4ProtocolId = 0;
    INT4                i4NextProtocolId = 0;
    UINT1               u1ShowAllCtx = CFA_FALSE;

    if (u4ContextId == SYS_DEF_MAX_NUM_CONTEXTS)
    {
        if (nmhGetFirstIndexIfTypeProtoDenyTable (&u4ContextId, &i4IfType,
                                                  &i4BrgPortType, &i4ProtocolId)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        u4NextContextId = u4ContextId;
        i4NextIfType = i4IfType;
        i4NextBrgPortType = i4BrgPortType;
        i4NextProtocolId = i4ProtocolId;
        u1ShowAllCtx = CFA_TRUE;
    }
    else
    {
        if (nmhGetNextIndexIfTypeProtoDenyTable
            (u4ContextId, &u4NextContextId, i4IfType, &i4NextIfType,
             i4BrgPortType, &i4NextBrgPortType, i4ProtocolId, &i4NextProtocolId)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if ((u4ContextId == u4NextContextId))
    {
        MEMSET (au1ContextName, 0, VCM_ALIAS_MAX_LEN);
        VcmGetAliasName (u4ContextId, au1ContextName);
        CliPrintf (CliHandle, "\r Switch %s\r\n", au1ContextName);
        CliPrintf (CliHandle, "\r\n %-20s %-20s %-20s\r\n",
                   "IfType", "BridgePortType", "Protocol");
        CliPrintf (CliHandle, "----------------------------------------");
        CliPrintf (CliHandle, "--------------------\r\n");
    }

    do
    {
        if ((u4ContextId != u4NextContextId) && (u1ShowAllCtx == CFA_FALSE))
        {
            break;
        }

        if ((u4ContextId != u4NextContextId))
        {
            MEMSET (au1ContextName, 0, VCM_ALIAS_MAX_LEN);
            VcmGetAliasName (u4NextContextId, au1ContextName);
            CliPrintf (CliHandle, "\r Switch %s\r\n", au1ContextName);
            CliPrintf (CliHandle, "\r\n %-20s %-20s %-20s\r\n",
                       "IfType", "BridgePortType", "Protocol");
            CliPrintf (CliHandle, "----------------------------------------");
            CliPrintf (CliHandle, "--------------------\r\n");
        }

        u4ContextId = u4NextContextId;
        i4IfType = i4NextIfType;
        i4BrgPortType = i4NextBrgPortType;
        i4ProtocolId = i4NextProtocolId;

        switch (i4IfType)
        {
            case CFA_ENET:

                CliPrintf (CliHandle, "%-20s", "Ethernet");
                break;

            case CFA_PROP_VIRTUAL_INTERFACE:

                CliPrintf (CliHandle, "%-20s", "Sisp");
                break;

            case CFA_LAGG:

                CliPrintf (CliHandle, "%-20s", "Port-Channel");
                break;

            case CFA_PIP:

                CliPrintf (CliHandle, "%-20s", "Pip");
                break;

            case CFA_BRIDGED_INTERFACE:

                CliPrintf (CliHandle, "%-20s", "Internal Bridge Ports");
                break;

            default:
                break;

        }

        switch (i4BrgPortType)
        {
            case CFA_PROVIDER_NETWORK_PORT:

                CliPrintf (CliHandle, "%-20s", "ProviderNetworkPort");
                break;

            case CFA_CNP_PORTBASED_PORT:

                CliPrintf (CliHandle, "%-20s", "CustomerNetworkPort");
                break;

            case CFA_CNP_STAGGED_PORT:

                CliPrintf (CliHandle, "%-20s", "CustomerNetworkPort");
                break;

            case CFA_CUSTOMER_EDGE_PORT:

                CliPrintf (CliHandle, "%-20s", "CustomerEdgePort");
                break;

            case CFA_PROP_CUSTOMER_EDGE_PORT:

                CliPrintf (CliHandle, "%-20s", "PropCustomerEdgePort");
                break;

            case CFA_PROP_PROVIDER_NETWORK_PORT:

                CliPrintf (CliHandle, "%-23s", "PropProviderNetworkPort");
                break;

            case CFA_PROP_CUSTOMER_NETWORK_PORT:

                CliPrintf (CliHandle, "-%23s", "PropCustomerNetworkPort");
                break;
            case CFA_CUSTOMER_BRIDGE_PORT:

                CliPrintf (CliHandle, "%-20s", "CustomerBridgePort");
                break;
            case CFA_CNP_CTAGGED_PORT:

                CliPrintf (CliHandle, "%-20s", "CustomerNetworkPort");
                break;
            case CFA_VIRTUAL_INSTANCE_PORT:

                CliPrintf (CliHandle, "%-20s", "VirtualInstancePort");
                break;
            case CFA_PROVIDER_INSTANCE_PORT:

                CliPrintf (CliHandle, "%-20s", "ProviderInstancePort");
                break;
            case CFA_CUSTOMER_BACKBONE_PORT:

                CliPrintf (CliHandle, "%-20s", "CustomerBackbonePort");
                break;
            default:
                break;

        }

        switch (i4ProtocolId)
        {
            case L2IWF_PROTOCOL_ID_PNAC:

                CliPrintf (CliHandle, "%-20s", "pnac");
                break;

            case L2IWF_PROTOCOL_ID_LA:

                CliPrintf (CliHandle, "%-20s", "la");
                break;

            case L2IWF_PROTOCOL_ID_XSTP:

                CliPrintf (CliHandle, "%-20s", "xstp");
                break;

            case L2IWF_PROTOCOL_ID_VLAN:

                CliPrintf (CliHandle, "%-20s", "vlan");
                break;

            case L2IWF_PROTOCOL_ID_GARP:

                CliPrintf (CliHandle, "%-20s", "garp");
                break;

            case L2IWF_PROTOCOL_ID_MRP:

                CliPrintf (CliHandle, "%-23s", "mrp");
                break;

            case L2IWF_PROTOCOL_ID_PBB:

                CliPrintf (CliHandle, "%-23s", "pbb");
                break;
            case L2IWF_PROTOCOL_ID_ECFM:

                CliPrintf (CliHandle, "%-20s", "ecfm");
                break;
            case L2IWF_PROTOCOL_ID_ELMI:

                CliPrintf (CliHandle, "%-20s", "elmi");
                break;
            case L2IWF_PROTOCOL_ID_SNOOP:

                CliPrintf (CliHandle, "%-20s", "snooping");
                break;
            case L2IWF_PROTOCOL_ID_LLDP:

                CliPrintf (CliHandle, "%-20s", "lldp");
                break;
            case L2IWF_PROTOCOL_ID_BRIDGE:

                CliPrintf (CliHandle, "%-20s", "bridge");
                break;
            case L2IWF_PROTOCOL_ID_QOS:

                CliPrintf (CliHandle, "%-20s", "qos");
                break;
            default:
                break;

        }

        switch (i4BrgPortType)
        {
            case CFA_CNP_PORTBASED_PORT:

                CliPrintf (CliHandle, "\r\n%-20s" "%-20s\r\n", " ",
                           "portbased");
                break;

            case CFA_CNP_STAGGED_PORT:
                CliPrintf (CliHandle, "\r\n%-20s" "%-20s\r\n", " ", "s-tagged");
                break;

            case CFA_CNP_CTAGGED_PORT:
                CliPrintf (CliHandle, "\r\n%-20s" "%-20s\r\n", " ", "c-tagged");
                break;

            default:
                CliPrintf (CliHandle, "\r\n");
                break;
        }

    }
    while (nmhGetNextIndexIfTypeProtoDenyTable
           (u4ContextId, &u4NextContextId, i4IfType, &i4NextIfType,
            i4BrgPortType, &i4NextBrgPortType, i4ProtocolId, &i4NextProtocolId)
           != SNMP_FAILURE);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowBridgePortType                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays the port type of the gien   */
/*                        interface.                                         */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4IfIndex - InterfaceIndex                         */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
CfaShowBridgePortType (tCliHandle CliHandle, INT4 i4IfIndex)
{
    INT4                i4BridgePortType = CFA_INVALID_BRIDGE_PORT;
    UINT4               u4BridgeMode = L2IWF_INVALID_BRIDGE_MODE;
    UINT4               u4ContextId = L2IWF_DEFAULT_CONTEXT;
    UINT2               u2LocalPortId = 0;

#ifdef VCM_WANTED
    VcmGetContextInfoFromIfIndex ((UINT4) i4IfIndex, &u4ContextId,
                                  &u2LocalPortId);
#endif
    L2IwfGetBridgeMode (u4ContextId, &u4BridgeMode);

    /*Bridge Port Type */
    if (u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE ||
        u4BridgeMode == L2IWF_PBB_ICOMPONENT_BRIDGE_MODE ||
        u4BridgeMode == L2IWF_PBB_BCOMPONENT_BRIDGE_MODE ||
        u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE)
    {
        nmhGetIfMainBrgPortType (i4IfIndex, &i4BridgePortType);
        switch (i4BridgePortType)
        {
            case CFA_PROVIDER_NETWORK_PORT:
                /* Provide Network port is the default value. */
                break;
            case CFA_CNP_PORTBASED_PORT:
                CliPrintf (CliHandle,
                           "bridge port-type customerNetworkPort port-based\r\n");
                break;
            case CFA_CNP_STAGGED_PORT:
                CliPrintf (CliHandle,
                           "bridge port-type customerNetworkPort s-tagged\r\n");
                break;
            case CFA_CUSTOMER_EDGE_PORT:
                CliPrintf (CliHandle, "bridge port-type customerEdgePort\r\n");
                break;
            case CFA_PROP_PROVIDER_NETWORK_PORT:
                CliPrintf (CliHandle,
                           "bridge port-type propProviderNetworkPort\r\n");
                break;
            case CFA_PROP_CUSTOMER_NETWORK_PORT:
                CliPrintf (CliHandle,
                           "bridge port-type propCustomerNetworkPort\r\n");
                break;
            case CFA_PROP_CUSTOMER_EDGE_PORT:
                CliPrintf (CliHandle,
                           "bridge port-type propCustomerEdgePort\r\n");
                break;
            case CFA_CNP_CTAGGED_PORT:
                CliPrintf (CliHandle,
                           "bridge port-type customerNetworkPort c-tagged\r\n");
                break;
            case CFA_CUSTOMER_BACKBONE_PORT:
                CliPrintf (CliHandle,
                           "bridge port-type customerBackbonePort \r\n");
                break;
            case CFA_PROVIDER_INSTANCE_PORT:
                CliPrintf (CliHandle, "port-type providerInstancePort \r\n");
                break;
            default:
                break;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShowILanConfig                                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays the internal ports of a     */
/*                        context connected to an another internal port      */
/*                        within another context.                            */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        u4CurrIfIndex - I-LAN interface index              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
CfaShowILanConfig (tCliHandle CliHandle, UINT4 u4CurrIfIndex)
{
    INT4                i4RowStatus = 0;
    INT4                i4AdminStatus = CFA_IF_DOWN;
    INT4                i4PagingStatus = 0;
    UINT4               u4StackedIfIndex = 0;
    INT1               *pIfName = NULL;
    tTMO_SLL           *pHighStack = NULL;
    tStackInfoStruct   *pHighStackListScan = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1ILanName[CFA_MAX_PORT_NAME_LENGTH];
    tPortList          *pVirtualPortList = NULL;
    tSNMP_OCTET_STRING_TYPE SnmpVirtualPorts;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1ILanName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pIfName = (INT1 *) (&au1IfName[0]);
    nmhGetIfBridgeILanIfStatus ((INT4) u4CurrIfIndex, &i4RowStatus);
    if (i4RowStatus == CFA_RS_ACTIVE)
    {
        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        CfaCliGetIfName (u4CurrIfIndex, (INT1 *) au1ILanName);
        pVirtualPortList =
            (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
        if (pVirtualPortList == NULL)
        {
            CliPrintf (CliHandle, "\r%% Error in Allocating memory "
                       "for bitlist\r\n");
            return CLI_SUCCESS;
        }
        CLI_MEMSET ((*pVirtualPortList), 0, sizeof (tPortList));
        SnmpVirtualPorts.pu1_OctetList = (UINT1 *) pVirtualPortList;
        SnmpVirtualPorts.i4_Length = sizeof (tPortList);

        pHighStack = &CFA_IF_STACK_HIGH (u4CurrIfIndex);
        pHighStackListScan =
            (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4CurrIfIndex);
        TMO_SLL_Scan (pHighStack, pHighStackListScan, tStackInfoStruct *)
        {
            if (pHighStackListScan->u4IfIndex != 0)
            {
                u4StackedIfIndex = (UINT4) pHighStackListScan->u4IfIndex;
                OSIX_BITLIST_SET_BIT ((*pVirtualPortList), u4StackedIfIndex,
                                      sizeof (tPortList));
                nmhGetIfAdminStatus ((INT4) u4StackedIfIndex, &i4AdminStatus);
                if (i4AdminStatus == CFA_IF_UP)
                {
                    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                    CfaCliConfGetIfName (u4StackedIfIndex, pIfName);
                    CliPrintf (CliHandle, "interface %s\r\n", pIfName);
                    CliPrintf (CliHandle, "shutdown\r\n");
                    CliPrintf (CliHandle, "!\r\n");
                }
            }
        }
        CliPrintf (CliHandle, "internal-lan %s add ",
                   (au1ILanName + CFA_ILAN_NAME_OFFSET));

        CliConfOctetToIfName (CliHandle, "interface ", NULL,
                              &SnmpVirtualPorts, (UINT4 *) (&i4PagingStatus));
        CliPrintf (CliHandle, "\r\n");
        FsUtilReleaseBitList ((UINT1 *) pVirtualPortList);
        pHighStack = &CFA_IF_STACK_HIGH (u4CurrIfIndex);
        pHighStackListScan =
            (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4CurrIfIndex);
        TMO_SLL_Scan (pHighStack, pHighStackListScan, tStackInfoStruct *)
        {
            if (pHighStackListScan->u4IfIndex != 0)
            {
                u4StackedIfIndex = (UINT4) pHighStackListScan->u4IfIndex;
                nmhGetIfAdminStatus ((INT4) u4StackedIfIndex, &i4AdminStatus);
                if (i4AdminStatus == CFA_IF_UP)
                {
                    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                    CfaCliConfGetIfName (u4StackedIfIndex, pIfName);
                    CliPrintf (CliHandle, "interface %s\r\n", pIfName);
                    CliPrintf (CliHandle, "no shutdown\r\n");
                    CliPrintf (CliHandle, "!\r\n");
                }
            }
        }
    }
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : CfaSetDebug
 *
 *     DESCRIPTION      : This function updates(sets/resets) the trace int
 *
 *     INPUT            : CliHandle       - CliContext ID
 *                        u4TraceInput   - Pointer to trace input strig
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/

PUBLIC INT4
CfaSetDebug (tCliHandle CliHandle, UINT4 u4TraceInput, UINT1 u1Action)
{
    UINT4               u4DebugTrace = 0;
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IfmDebug (&u4ErrorCode, u4DebugTrace) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    nmhGetIfmDebug (&u4DebugTrace);
    switch (u1Action)
    {
        case CFA_ENABLED:
            if (u4DebugTrace == u4TraceInput)
            {
                return CLI_SUCCESS;
            }
            u4DebugTrace = u4DebugTrace | u4TraceInput;
            break;
        case CFA_DISABLED:
            u4DebugTrace = u4DebugTrace & (~u4TraceInput);
            break;
        default:
            return CLI_FAILURE;
            break;
    }

    if (nmhSetIfmDebug (u4DebugTrace) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "Entering CfaInit.\n");
    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_MAIN, "CFA_TRC_ALL_TRACK.\n");
    CFA_DBG (CFA_TRC_ALL_PKT_DUMP, CFA_MAIN, "CFA_TRC_ALL_PKT_DUMP.\n");
    CFA_DBG (CFA_TRC_ENET_PKT_DUMP, CFA_MAIN, "CFA_TRC_ENET_PKT_DUMP.\n");
    CFA_DBG (CFA_TRC_IP_PKT_DUMP, CFA_MAIN, "CFA_TRC_IP_PKT_DUMP.\n");
    CFA_DBG (CFA_TRC_ARP_PKT_DUMP, CFA_MAIN, "CFA_TRC_ARP_PKT_DUMP.\n");
    CFA_DBG (CFA_TRC_ERROR, CFA_MAIN, "CFA_TRC_ERROR.\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetPortSecState                                 */
/*                                                                           */
/*     DESCRIPTION      : This function set the port state of the interface  */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the port to be set            */
/*                        CliHandle - CLI Handler                            */
/*                        u4PortSecState - Port state                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
CfaSetPortSecState (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4PortSecState)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2IfMainExtPortSecState (&u4ErrCode, (INT4) u4IfIndex,
                                        (INT4) u4PortSecState) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIfMainExtPortSecState ((INT4) u4IfIndex,
                                     (INT4) u4PortSecState) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliShowPortSecState                             */
/*                                                                           */
/*     DESCRIPTION      : This function displays trust enabled status of all */
/*                        the interfaces.                                    */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4IfIndex - Interface Index                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
CfaCliShowPortSecState (tCliHandle CliHandle, INT4 i4IfIndex)
{
    INT4                i4Status = 0;
    INT1               *piIfName = NULL;
    UINT1               u1IfType = 0;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN] = { 0 };

    piIfName = (INT1 *) &au1IfName[0];

    nmhGetIfMainExtPortSecState (i4IfIndex, &i4Status);
    CfaCliGetIfName ((UINT4) i4IfIndex, piIfName);

    CfaGetIfType ((UINT4) i4IfIndex, &u1IfType);
    if ((u1IfType == CFA_ENET) || (u1IfType == CFA_LAGG))
    {
        if (i4Status == CFA_PORT_STATE_TRUSTED)
        {
            CliPrintf (CliHandle, "%-10s   Trusted \r\n", piIfName);
        }
        else if (i4Status == CFA_PORT_STATE_UNTRUSTED)
        {
            CliPrintf (CliHandle, "%-10s  Untrusted \r\n", piIfName);
        }
        else
        {
            CliPrintf (CliHandle, "%-10s   Invalid \r\n", piIfName);
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaPppSetAddressAlloc                              */
/*                                                                           */
/*     DESCRIPTION      : This function is used to set IP address allocation */
/*                        method                                             */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface Index                        */
/*                        u1AddrAllocMethod - Manual or Dynamic              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SUccess/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
CfaPppSetAddressAlloc (tCliHandle CliHandle, UINT4 u4IfIndex,
                       UINT1 u1AddrAllocMethod)
{
    INT4                i4RetValue;
    INT4                i4Result;

    if (CFA_IF_ADMIN (u4IfIndex) == CFA_IF_UP)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Interface must first be made administratively down\r\n");
        return CLI_FAILURE;
    }

    /* Get the interface Address allocation method,
     * if the address allocation method is not dynamic then
     * only proceed otherwise return
     */
    i4Result = (INT4) nmhGetIfIpAddrAllocMethod ((INT4) u4IfIndex, &i4RetValue);

    if (i4Result == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if ((u1AddrAllocMethod == CFA_IP_ALLOC_NEGO) &&
        (i4RetValue == CFA_IP_ALLOC_MAN))
    {
        i4Result = nmhSetIfIpAddrAllocMethod (u4IfIndex, CFA_IP_ALLOC_NEGO);

        if (i4Result == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if ((i4RetValue == CFA_IP_ALLOC_NEGO) &&
             (u1AddrAllocMethod == CFA_IP_ALLOC_MAN))
    {
        /* Negotiated -> Static */

        i4Result = nmhSetIfIpAddrAllocMethod (u4IfIndex, CFA_IP_ALLOC_MAN);

        if (i4Result == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME : CfaCliSetWanType                                        *
 * DESCRIPTION   : Sets the WAN type as Private or Public for the          *
 *                 specified interface.                                    *
 * RETURNS       : returns 0 on CFA_SUCCESS                                *
 *                 returns -1 on CFA_FAILURE                               *
 **************************************************************************/
INT4
CfaCliSetWanType (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4WanType)
{
    UINT4               u4ErrCode;

    if (CfaValidateIfIndex (u4IfIndex) == CFA_FAILURE)
    {
        /* Only PPP interfaces can be in the Inactive state
         * when this command is executed */
        CliPrintf (CliHandle,
                   "\r\n%% Attach the PPP interface to an underlying"
                   " physical interface first\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2IfMainWanType (&u4ErrCode, (INT4) u4IfIndex,
                                (INT4) u4WanType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIfMainWanType ((INT4) u4IfIndex, u4WanType) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

#ifdef PPP_WANTED
/***************************************************************************
 * FUNCTION NAME : CfaCliCreatePppInterface
 * DESCRIPTION   : Creates a ppp interface.
 * INPUT         : CliHandle -  CLI context id to be used by CliPrintf
 *                 u4IfIndex - Index of the interface to be created   *
 *                 pu1IfName - Name of the interface                  *
 * RETURNS       : returns CLI_SUCCESS/ CLI_FAILURE
 **************************************************************************/
INT1
CfaCliCreatePppInterface (tCliHandle CliHandle, UINT4 u4IfIndex,
                          UINT1 *pu1IfName)
{
    tSNMP_OCTET_STRING_TYPE tAlias;
    INT4                i4PppIfaceNo = (INT4) u4IfIndex;

    if (nmhSetIfMainRowStatus (i4PppIfaceNo, CFA_RS_CREATEANDWAIT)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    tAlias.i4_Length = STRLEN (pu1IfName);
    tAlias.pu1_OctetList = MEM_MALLOC (tAlias.i4_Length, UINT1);
    MEMCPY (tAlias.pu1_OctetList, pu1IfName, tAlias.i4_Length);

    nmhSetIfAlias (i4PppIfaceNo, &tAlias);

    MEM_FREE (tAlias.pu1_OctetList);

    if (nmhSetIfMainType (i4PppIfaceNo, CFA_PPP) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (i4PppIfaceNo, CFA_RS_DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetIfIpAddrAllocMethod (i4PppIfaceNo, CFA_IP_ALLOC_NEGO)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (i4PppIfaceNo, CFA_RS_DESTROY);
        return CLI_FAILURE;
    }

    return (CLI_SUCCESS);
}

/***************************************************************************
 * FUNCTION NAME : CfaCliStackPpp                                          *
 * DESCRIPTION   : Stacks the PPP interface over the HDLC interface        *
 * INPUT         : CliHandle    - CLI context id to be used by CliPrintf   *
 *                 i4PppIfaceNo - Index of the ppp interface               *
 *                 i4LlIfaceNo  - Index of the underlying interface        *
 *                 u1IfType     - Type of the underlying interface         *
 * RETURNS       : returns 0 on CFA_SUCCESS                                *
 *                 returns -1 on CFA_FAILURE                               *
 ***************************************************************************/
INT4
CfaCliStackPpp (tCliHandle CliHandle, INT4 i4PppIfaceNo,
                INT4 i4LlIfaceNo, UINT1 u1IfType)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (u1IfType);

    if (nmhTestv2IfStackStatus (&u4ErrorCode, i4PppIfaceNo, i4LlIfaceNo,
                                CFA_RS_CREATEANDGO) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% PPP virtual link is already layered "
                   "over another physical interface \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetIfStackStatus (i4PppIfaceNo, i4LlIfaceNo,
                             CFA_RS_CREATEANDGO) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% PPP interface creation failed \r\n");
        return CLI_FAILURE;
    }
    if (nmhSetIfMainRowStatus (i4PppIfaceNo, CFA_RS_ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% PPP interface activation failed \r\n");
        return CLI_FAILURE;
    }

    if ((u1IfType == CFA_ENET))
    {
        /* If PPP is to be run over ethernet enable PPPoE */
        if (nmhSetPPPoEBindingsEnabled (i4LlIfaceNo, PPPOE_ENABLE) ==
            SNMP_FAILURE)
        {
            nmhSetIfMainRowStatus (i4PppIfaceNo, CFA_RS_NOTINSERVICE);
            return CLI_FAILURE;
        }

        if (nmhSetPPPoEHostUniqueEnabled (PPPOE_ENABLE) == SNMP_FAILURE)
        {
            nmhSetIfMainRowStatus (i4PppIfaceNo, CFA_RS_NOTINSERVICE);
            return CLI_FAILURE;
        }

        if (nmhSetPppExtLinkConfigLowerIfType (i4PppIfaceNo, CFA_ENET)
            == SNMP_FAILURE)
        {
            nmhSetIfMainRowStatus (i4PppIfaceNo, CFA_RS_NOTINSERVICE);
            return CLI_FAILURE;
        }
        if (nmhSetIfMainSubType (i4PppIfaceNo, CFA_GI_ENET) == SNMP_FAILURE)
        {
            nmhSetIfMainRowStatus (i4PppIfaceNo, CFA_RS_NOTINSERVICE);
            return CLI_FAILURE;
        }
    }
#ifdef HDLC_WANTED
    else if (u1IfType == CFA_HDLC)
    {
        /* Change to SerialIfType in PPP code and change
           the below hardcoded value along with it */
        if (nmhSetPppExtLinkConfigLowerIfType (i4PppIfaceNo, CFA_ASYNC)
            == SNMP_FAILURE)
        {
            nmhSetIfMainRowStatus (i4PppIfaceNo, CFA_RS_NOTINSERVICE);
            return CLI_FAILURE;
        }
        if (nmhSetIfMainSubType (i4PppIfaceNo, CFA_HDLC) == SNMP_FAILURE)
        {
            nmhSetIfMainRowStatus (i4PppIfaceNo, CFA_RS_NOTINSERVICE);
            return CLI_FAILURE;
        }
    }
#endif

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME : CfaCliUnStackPpp                                        *
 * DESCRIPTION   : Remove the PPP interface from the lower layer interface *
 * INPUT         : CliHandle    - CLI context id to be used by CliPrintf   *
 *                 i4PppIfaceNo - Index of the ppp interface               *
 * RETURNS       : returns 0 on CFA_SUCCESS                                *
 *                 returns -1 on CFA_FAILURE                               *
 ***************************************************************************/
INT4
CfaCliUnStackPpp (tCliHandle CliHandle, INT4 i4PppIfaceNo)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IfMainRowStatus
        (&u4ErrorCode, i4PppIfaceNo, CFA_RS_NOTINSERVICE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIfMainRowStatus (i4PppIfaceNo, CFA_RS_NOTINSERVICE) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Failed to inactivate PPP \r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*                                                                        */
/* Function Name   : CfaCliPppShowInterface                               */
/*                                                                        */
/* Description     : This function dispalys details of the ppp interface  */
/*                                                                        */
/* Input(s)        : CliHandle, i4IfIndex                                  */
/*                                                                        */
/* Output(s)       : NONE                                                 */
/*                                                                        */
/* Returns         : CLI_SUCCESS                                          */
/*                                                                        */
/**************************************************************************/

INT4
CfaCliPppShowInterface (tCliHandle CliHandle, INT4 i4IfIndex)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4HighSpeed = 0;
    UINT4               u4Speed = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4SubMask = 0;
    UINT4               u4PeerIpAddr = 0;
    UINT4               u4DnsPriIpAddr = 0;
    UINT4               u4DnsSecIpAddr = 0;
    INT4                i4MTU = 0;
    INT4                i4KeepAlive = 0;
    CHR1               *pi1IpAddStr = NULL;
    CHR1               *pi1PeerIpAddStr = NULL;
    CHR1               *pi1DnsPriStr = NULL;
    CHR1               *pi1DnsSecStr = NULL;
    INT4                i4AuthPro = 0;
    INT4                i4PeerAuthPro = 0;
    INT4                i4LcpUp = 0;
    INT4                i4IPCPUp = 0;
    INT4                i4BCPUp = 0;
    UINT4               u4MaskBits;

    nmhGetIfIpAddr (i4IfIndex, &u4IpAddr);
    nmhGetIfIpSubnetMask (i4IfIndex, &u4SubMask);
    nmhGetIfMainMtu ((INT4) i4IfIndex, &i4MTU);
    u4PeerIpAddr = CFA_IF_NEGO_PEER_IPADDR (i4IfIndex);
    u4DnsPriIpAddr = CFA_IF_DNS_PRI_IPADDR (i4IfIndex);
    u4DnsSecIpAddr = CFA_IF_DNS_SEC_IPADDR (i4IfIndex);

    CFA_UNLOCK ();

    PppLock ();
    i4LcpUp = PppIsIfaceLCPStatusOpen (i4IfIndex);
    i4IPCPUp = PppIsIfaceIPCPStatusOpen (i4IfIndex);
    i4BCPUp = PppIsIfaceBCPStatusOpen (i4IfIndex);

    nmhGetPppExtKeepAliveTimeOut (i4IfIndex, &i4KeepAlive);
    PppGetNegAuthProtocol (i4IfIndex, &i4AuthPro, &i4PeerAuthPro);
    PppUnlock ();

    CFA_LOCK ();

    if (CfaGetIfInfo (i4IfIndex, &IfInfo) == CFA_FAILURE)
    {
        CliPrintf (CliHandle, "Invalid interface\r\n");
        return CFA_FAILURE;
    }

    if (i4LcpUp == TRUE)
    {
        CliPrintf (CliHandle, "\r\nLCP open, ");

        if (i4IPCPUp == TRUE)
        {
            CliPrintf (CliHandle, "IPCP open \r\n");
        }
        if (i4BCPUp == TRUE)
        {
            CliPrintf (CliHandle, "BCP open \r\n");
        }

        nmhGetIfHighSpeed (i4IfIndex, &u4HighSpeed);
        nmhGetIfSpeed (i4IfIndex, &u4Speed);

        if (u4HighSpeed == 0)
        {
            u4Speed = u4Speed / 1000;
            CliPrintf (CliHandle, "MTU %d bytes, BW %d Kbits\r\n", i4MTU,
                       u4Speed);
        }
        else
        {
            CliPrintf (CliHandle, "MTU %d bytes, BW %d Mbits\r\n", i4MTU,
                       u4HighSpeed);
        }

        if (i4KeepAlive != 0)
        {
            CliPrintf (CliHandle, "Keepalive set (%d sec)\r\n", i4KeepAlive);
        }

        if (i4AuthPro != 0)
        {
            CliPrintf (CliHandle, "Authentication protocol ");
            switch (i4AuthPro)
            {
                case CHAP_PROTOCOL:
                    CliPrintf (CliHandle, "CHAP\r\n");
                    break;
                case PAP_PROTOCOL:
                    CliPrintf (CliHandle, "PAP\r\n");
                    break;
                case MSCHAP_PROTOCOL:
                    CliPrintf (CliHandle, "MSCHAP\r\n");
                    break;
                case EAP_PROTOCOL:
                    CliPrintf (CliHandle, "EAP\r\n");
                    break;
                default:
                    CliPrintf (CliHandle, "Unknown\r\n");
                    break;
            }
        }
        if (i4PeerAuthPro != 0)
        {
            CliPrintf (CliHandle,
                       "Authentication protocol requested by peer is ");
            switch (i4PeerAuthPro)
            {
                case CHAP_PROTOCOL:
                    CliPrintf (CliHandle, "CHAP\r\n");
                    break;
                case PAP_PROTOCOL:
                    CliPrintf (CliHandle, "PAP\r\n");
                    break;
                case MSCHAP_PROTOCOL:
                    CliPrintf (CliHandle, "MSCHAP\r\n");
                    break;
                case EAP_PROTOCOL:
                    CliPrintf (CliHandle, "EAP\r\n");
                    break;
                default:
                    CliPrintf (CliHandle, "Unknown\r\n");
                    break;
            }
        }

    }
    else
    {
        CliPrintf (CliHandle, "\r\nLCP Down ");
    }

    if (i4IPCPUp == TRUE)
    {
        CLI_CONVERT_IPADDR_TO_STR (pi1IpAddStr, u4IpAddr)
            CliPrintf (CliHandle, "Internet address is %s/", pi1IpAddStr);

        u4MaskBits = CliGetMaskBits (u4SubMask);
        CliPrintf (CliHandle, "%d\r\n", u4MaskBits);

        if (u4DnsPriIpAddr != 0)
        {
            CLI_CONVERT_IPADDR_TO_STR (pi1DnsPriStr, u4DnsPriIpAddr);
            CliPrintf (CliHandle, "Primary DNS address is %s \r\n",
                       pi1DnsPriStr);
        }
        if (u4DnsSecIpAddr != 0)
        {
            CLI_CONVERT_IPADDR_TO_STR (pi1DnsSecStr, u4DnsSecIpAddr);
            CliPrintf (CliHandle, "Secondary DNS address is %s \r\n",
                       pi1DnsSecStr);
        }

        if (u4PeerIpAddr != 0)
        {
            CLI_CONVERT_IPADDR_TO_STR (pi1PeerIpAddStr, u4PeerIpAddr);

            CliPrintf (CliHandle, "Peer address is %s ", pi1PeerIpAddStr);
        }
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME : CfaCliSetPppIpAddr                                      *
 * DESCRIPTION   : Sets Allocation method and IP address for PPP interface *
 * RETURNS       : returns 0 on CFA_SUCCESS                                *
 *                 returns -1 on CFA_FAILURE                               *
 **************************************************************************/
INT4
CfaCliSetPppIpAddr (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4IpAddr,
                    UINT4 u4IpMask)
{
    if (CfaPppSetAddressAlloc (CliHandle, u4IfIndex,
                               CFA_IP_ALLOC_MAN) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    return (CfaIvrSetIpAddress (CliHandle, u4IfIndex, u4IpAddr, u4IpMask));
}

INT4
CfaCliSetPppNoIpAddr (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    if (CfaPppSetAddressAlloc (CliHandle, u4IfIndex,
                               CFA_IP_ALLOC_MAN) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    return (CfaIvrSetIpAddress (CliHandle, u4IfIndex, 0, 0));
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaPppUnnumIntSetAssociatedIPInterface             */
/*                                                                           */
/*     DESCRIPTION      : This function Resets the alias IP interface of an  */
/*                        unnumbered interface                               */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface Index                        */
/*                        u1AddrAllocMethod - Manual or Dynamic              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SUccess/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
CfaPppUnnumIntSetAssociatedIPInterface (tCliHandle CliHandle, UINT4 u4IfIndex,
                                        UINT4 u4AliasIfIndex)
{
    UINT4               u4ErrorCode = CFA_NONE;

    if (CfaValidateIfIndex (u4IfIndex) == CFA_FAILURE)
    {
        CLI_SET_ERR (CLI_CFA_PPP_NOT_STACKED_ERR);
        return CLI_FAILURE;
    }
    if (CfaPppSetAddressAlloc (CliHandle, u4IfIndex,
                               CFA_IP_ALLOC_MAN) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2IfIpUnnumAssocIPIf
        (&u4ErrorCode, (INT4) u4IfIndex, u4AliasIfIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIfIpUnnumAssocIPIf (u4IfIndex, u4AliasIfIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaPppUnnumIntResetAssociatedIPInterface           */
/*                                                                           */
/*     DESCRIPTION      : This function Resets the alias IP interface of an  */
/*                        unnumbered interface                               */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface Index                        */
/*                        u1AddrAllocMethod - Manual or Dynamic              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SUccess/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
CfaPppUnnumIntResetAssociatedIPInterface (tCliHandle CliHandle, UINT4 u4IfIndex,
                                          UINT4 u4AssocAddr)
{
    UNUSED_PARAM (CliHandle);

    UINT4               u4ErrorCode = CFA_NONE;

    if (CFA_IF_ADMIN (u4IfIndex) == CFA_IF_UP)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Interface must first be made administratively down\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2IfIpUnnumAssocIPIf
        (&u4ErrorCode, (INT4) u4IfIndex, u4AssocAddr) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetIfIpUnnumAssocIPIf (u4IfIndex, 0);
    return (CLI_SUCCESS);

}

INT4
CfaCliPppShowInterfaceConfig (tCliHandle CliHandle, INT4 i4IfIndex)
{

    UINT2               u2LLIfIndex = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4PeerIpAddr = 0;
    UINT4               u4PeerDnsAddr = 0;
    UINT4               i4RetVal = 0;
    INT4                i4SecretIndex = 0;
    INT4                i4PrevSecretIndex = 0;
    INT4                i4PppIfIndex = 0;
    INT4                i4Direction = 0;
    INT4                i4BridgeType = 0;
    INT4                i4WanType = 0;
    INT4                i4MTU = 0;
    CHR1               *pi1IpAddStr = NULL;
    CHR1               *pi1PeerIpAddStr = NULL;
    CHR1               *pi1PeerDnsAddStr = NULL;

    UINT1               au1IfName[PPP_MAX_NAME_LEN];
    UINT1               au1Hostname[PPP_MAX_NAME_LEN];
    UINT1               au1UserName[PPP_MAX_NAME_LEN];
    UINT1               au1PeerUserName[PPP_MAX_NAME_LEN];
    tSNMP_OCTET_STRING_TYPE Hostname;
    tSNMP_OCTET_STRING_TYPE UserName;
    tSNMP_OCTET_STRING_TYPE PeerUserName;

    Hostname.pu1_OctetList = &au1Hostname[0];
    UserName.pu1_OctetList = &au1UserName[0];
    PeerUserName.pu1_OctetList = &au1PeerUserName[0];

    Hostname.i4_Length = 0;
    UserName.i4_Length = 0;
    PeerUserName.i4_Length = 0;
    MEMSET (au1Hostname, 0, PPP_MAX_NAME_LEN);
    MEMSET (au1UserName, 0, PPP_MAX_NAME_LEN);
    MEMSET (au1PeerUserName, 0, PPP_MAX_NAME_LEN);

    u2LLIfIndex =
        CFA_IF_STACK_IFINDEX (CFA_IF_STACK_LOW_ENTRY ((UINT2) i4IfIndex));
    nmhGetIfMainWanType (i4IfIndex, &i4WanType);    /* Only if Private */
    nmhGetIfIvrBridgedIface (i4IfIndex, &i4BridgeType);    /* Only if CFA_ENABLED */
    CFA_UNLOCK ();

    PppLock ();
    nmhGetPppExtIpLocToRemoteAddress (i4IfIndex, &u4IpAddr);
    nmhGetPppExtIpRemoteToLocAddress (i4IfIndex, &u4PeerIpAddr);
    nmhGetPppExtIpRemoteToLocPrimaryDNSAddress (i4IfIndex, &u4PeerDnsAddr);
    nmhGetPppExtLinkConfigHostName (i4IfIndex, &Hostname);
    nmhGetPppTestConfigMTU (i4IfIndex, &i4MTU);

    i4RetVal = nmhGetNextIndexPppSecuritySecretsTable (i4IfIndex,
                                                       &i4PppIfIndex, 0,
                                                       &i4SecretIndex);

    while ((i4RetVal == SNMP_SUCCESS) && (i4IfIndex == i4PppIfIndex))
    {
        nmhGetPppSecuritySecretsDirection (i4IfIndex, i4SecretIndex,
                                           &i4Direction);

        if (i4Direction == LOC_TO_REMOTE_DIRECTION)
        {
            nmhGetPppSecuritySecretsIdentity (i4IfIndex, i4SecretIndex,
                                              &UserName);
            break;
        }

        i4PrevSecretIndex = i4SecretIndex;
        i4RetVal = nmhGetNextIndexPppSecuritySecretsTable (i4IfIndex,
                                                           &i4PppIfIndex,
                                                           i4PrevSecretIndex,
                                                           &i4SecretIndex);
    }

    i4RetVal = nmhGetNextIndexPppSecuritySecretsTable (i4IfIndex,
                                                       &i4PppIfIndex, 0,
                                                       &i4SecretIndex);

    while ((i4RetVal == SNMP_SUCCESS) && (i4IfIndex == i4PppIfIndex))
    {
        nmhGetPppSecuritySecretsDirection (i4IfIndex, i4SecretIndex,
                                           &i4Direction);

        if (i4Direction == REMOTE_TO_LOC_DIRECTION)
        {
            nmhGetPppSecuritySecretsIdentity (i4IfIndex, i4SecretIndex,
                                              &PeerUserName);
            break;
        }

        i4PrevSecretIndex = i4SecretIndex;
        i4RetVal = nmhGetNextIndexPppSecuritySecretsTable (i4IfIndex,
                                                           &i4PppIfIndex,
                                                           i4PrevSecretIndex,
                                                           &i4SecretIndex);
    }
    PppUnlock ();

    CFA_LOCK ();
    CliPrintf (CliHandle, "\r\n");
    if (u2LLIfIndex != CFA_NONE)
    {
        CfaCliGetIfName (u2LLIfIndex, (INT1 *) au1IfName);
        CliPrintf (CliHandle, "Physical interface is %s\r\n", au1IfName);
    }
    if (u4IpAddr != 0)
    {
        CLI_CONVERT_IPADDR_TO_STR (pi1IpAddStr, u4IpAddr)
            CliPrintf (CliHandle, "Configured internet address is %s\r\n",
                       pi1IpAddStr);
    }
    if (i4MTU != 0)
    {
        CliPrintf (CliHandle, "Configured MTU %d\r\n", i4MTU);
    }
    if (i4BridgeType == CFA_ENABLED)
    {
        CliPrintf (CliHandle, "No Switchport \r\n");
    }
    if (i4WanType == CFA_WAN_TYPE_PRIVATE)
    {
        CliPrintf (CliHandle, "Private link \r\n");
    }
    if (Hostname.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Hostname is %s \r\n", au1Hostname);
    }
    if (u4PeerIpAddr != 0)
    {
        CLI_CONVERT_IPADDR_TO_STR (pi1PeerIpAddStr, u4PeerIpAddr);
        CliPrintf (CliHandle, "Configured Peer address is %s\r\n",
                   pi1PeerIpAddStr);
    }

    if (u4PeerDnsAddr != 0)
    {
        CLI_CONVERT_IPADDR_TO_STR (pi1PeerDnsAddStr, u4PeerDnsAddr);
        CliPrintf (CliHandle, "Configured Peer DNS address is %s\r\n",
                   pi1PeerDnsAddStr);
    }
    if (UserName.i4_Length != 0)
    {
        CliPrintf (CliHandle, "PPP Username %s \r\n", au1UserName);
    }
    if (PeerUserName.i4_Length != 0)
    {
        CliPrintf (CliHandle, "PPP Authenticate Username %s \r\n",
                   au1PeerUserName);
    }
    return CLI_SUCCESS;

}

#endif /* PPP_WANTED */

/*************************************************************************
 *  FUNCTION NAME : CfaCliCreatePswInterface                             *
 *  DESCRIPTION   : Creates Psw Interface                                *
 *  INPUT         : CliHandle -  CLI context id to be used by CliPrintf  *
 *                  u4IfIndex -  Index of the interface to be created    *
 *                  pu1IfName -  Name of the interface                   *
 *  RETURNS       : returns 0 on CFA_SUCCESS                             *
 *                  returns -1 on CFA_FAILURE                            *
 *************************************************************************/
INT4
CfaCliCreatePswInterface (tCliHandle CliHandle, UINT4 u4IfIndex,
                          UINT1 *pu1IfName)
{
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE Alias;
    INT4                i4PswIfaceNo = (INT4) u4IfIndex;

    MEMSET (&Alias, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhSetIfMainRowStatus (i4PswIfaceNo, CFA_RS_CREATEANDWAIT)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    Alias.i4_Length = (INT4) STRLEN (pu1IfName);
    Alias.pu1_OctetList = pu1IfName;

    nmhSetIfAlias (i4PswIfaceNo, &Alias);

    if (nmhTestv2IfMainType (&u4ErrCode, (INT4) u4IfIndex,
                             (INT4) CFA_PSEUDO_WIRE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return (CLI_FAILURE);
    }

    if (nmhSetIfMainType (i4PswIfaceNo, CFA_PSEUDO_WIRE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (i4PswIfaceNo, CFA_RS_DESTROY);
        return CLI_FAILURE;
    }

    nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE);
    return (CLI_SUCCESS);

}

#ifdef HDLC_WANTED
/***************************************************************************
 * FUNCTION NAME : CfaCliCreateHdlcInterface
 * DESCRIPTION   : Creates a ppp interface.
 * INPUT         : CliHandle -  CLI context id to be used by CliPrintf
 *                 u4IfIndex - Index of the interface to be created   *
 *                 pu1IfName - Name of the interface                  *
 * RETURNS       : returns CLI_SUCCESS/ CLI_FAILURE
 **************************************************************************/
INT4
CfaCliCreateHdlcInterface (tCliHandle CliHandle, UINT4 u4IfIndex,
                           UINT1 *pu1IfName)
{
    tSNMP_OCTET_STRING_TYPE tAlias;
    INT4                i4HdlcIfaceNo = (INT4) u4IfIndex;

    if (nmhSetIfMainRowStatus (i4HdlcIfaceNo, CFA_RS_CREATEANDWAIT)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    tAlias.i4_Length = (INT4) STRLEN (pu1IfName);
    tAlias.pu1_OctetList = pu1IfName;

    nmhSetIfAlias (i4HdlcIfaceNo, &tAlias);

    if (nmhSetIfMainType (i4HdlcIfaceNo, CFA_HDLC) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (i4HdlcIfaceNo, CFA_RS_DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetIfMainRowStatus (i4HdlcIfaceNo, CFA_RS_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return (CLI_SUCCESS);
}
#endif

/*************************************************************************
 * FUNCTION NAME : CfaCliCreateACInterface                              *
 * DESCRIPTION   : Creates AC Interface                                 *
 * INPUT         : CliHandle -  CLI context id to be used by CliPrintf  *
 *                 u4IfIndex -  Index of the interface to be created    *
 *                 pu1IfName -  Name of the interface                   *
 * RETURNS       : returns 0 on CFA_SUCCESS                             *
 *                 returns -1 on CFA_FAILURE                            *
 *************************************************************************/

INT4
CfaCliCreateACInterface (tCliHandle CliHandle, UINT4 u4IfIndex,
                         UINT1 *pu1IfName)
{
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE Alias;
    INT4                i4ACIfaceNo = (INT4) u4IfIndex;

    MEMSET (&Alias, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhSetIfMainRowStatus (i4ACIfaceNo, CFA_RS_CREATEANDWAIT)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    Alias.i4_Length = (INT4) STRLEN (pu1IfName);
    Alias.pu1_OctetList = pu1IfName;

    nmhSetIfAlias (i4ACIfaceNo, &Alias);

    if (nmhTestv2IfMainType (&u4ErrCode, (INT4) u4IfIndex,
                             CFA_PROP_VIRTUAL_INTERFACE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return (CLI_FAILURE);
    }
    if (nmhSetIfMainType (i4ACIfaceNo, CFA_PROP_VIRTUAL_INTERFACE) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (i4ACIfaceNo, CFA_RS_DESTROY);
        return CLI_FAILURE;
    }

    if (nmhTestv2IfMainExtSubType (&u4ErrCode, (INT4) u4IfIndex,
                                   CFA_SUBTYPE_AC_INTERFACE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return (CLI_FAILURE);
    }
    if (nmhSetIfMainExtSubType (u4IfIndex, CFA_SUBTYPE_AC_INTERFACE) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return (CLI_FAILURE);
    }
    nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);
    return (CLI_SUCCESS);

}

#ifdef VXLAN_WANTED
/*************************************************************************
 *  FUNCTION NAME : CfaCliCreateNveInterface                             *
 *  DESCRIPTION   : Creates VXLAN-NVE Interface                          *
 *  INPUT         : CliHandle -  CLI context id to be used by CliPrintf  *
 *                  u4IfIndex -  Index of the interface to be created    *
 *                  pu1IfName -  Name of the interface                   *
 *  RETURNS       : returns 0 on CFA_SUCCESS                             *
 *                  returns -1 on CFA_FAILURE                            *
 *************************************************************************/
INT4
CfaCliCreateNveInterface (tCliHandle CliHandle, UINT4 u4IfIndex,
                          UINT1 *pu1IfName)
{
    UINT4               u4ErrCode = 0;
    tSNMP_OCTET_STRING_TYPE Alias;
    INT4                i4NveIfaceNo = (INT4) u4IfIndex;

    MEMSET (&Alias, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhSetIfMainRowStatus (i4NveIfaceNo, CFA_RS_CREATEANDWAIT)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    Alias.i4_Length = (INT4) STRLEN (pu1IfName);
    Alias.pu1_OctetList = pu1IfName;

    nmhSetIfAlias (i4NveIfaceNo, &Alias);

    if (nmhTestv2IfMainType (&u4ErrCode, (INT4) u4IfIndex,
                             (INT4) CFA_VXLAN_NVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return (CLI_FAILURE);
    }

    if (nmhSetIfMainType (i4NveIfaceNo, CFA_VXLAN_NVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (i4NveIfaceNo, CFA_RS_DESTROY);
        return CLI_FAILURE;
    }

    nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE);
    return (CLI_SUCCESS);

}
#endif
#ifdef VCPEMGR_WANTED
/*************************************************************************
 *  FUNCTION NAME : CfaCliCreateTapInterface                             *
 *  DESCRIPTION   : Creates TAP  Interface                               *
 *  INPUT         : CliHandle -  CLI context id to be used by CliPrintf  *
 *                  u4IfIndex -  Index of the interface to be created    *
 *                  pu1IfName -  Name of the interface                   *
 *  RETURNS       : returns 0 on CLI_SUCCESS                             *
 *                  returns -1 on CLI_FAILURE                            *
 *************************************************************************/
INT4
CfaCliCreateTapInterface (tCliHandle CliHandle, UINT4 u4IfIndex,
                          UINT1 *pu1IfName)
{
    UINT4               u4ErrCode = 0;
    tSNMP_OCTET_STRING_TYPE Alias;
    INT4                i4TapIfNo = (INT4) u4IfIndex;

    MEMSET (&Alias, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhSetIfMainRowStatus (i4TapIfNo, CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    Alias.i4_Length = (INT4) STRLEN (pu1IfName);
    Alias.pu1_OctetList = pu1IfName;

    nmhSetIfAlias (i4TapIfNo, &Alias);

    if (nmhTestv2IfMainType (&u4ErrCode, (INT4) u4IfIndex,
                             (INT4) CFA_TAP) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return (CLI_FAILURE);
    }

    if (nmhSetIfMainType (i4TapIfNo, CFA_TAP) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        nmhSetIfMainRowStatus (i4TapIfNo, CFA_RS_DESTROY);
        return CLI_FAILURE;
    }

    nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE);

    CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "CfaCliCreateTapInterface Success.\n");
    return (CLI_SUCCESS);

}
#endif
/***************************************************************************
 * FUNCTION NAME : CfaCliSetNetworkType                                    *
 * DESCRIPTION   : Sets the WAN type as Private or Public for the          *
 *                 specified interface.                                    *
 * RETURNS       : returns 0 on CFA_SUCCESS                                *
 *                 returns -1 on CFA_FAILURE                               *
 **************************************************************************/
INT4
CfaCliSetNetworkType (tCliHandle CliHandle, UINT4 u4IfIndex,
                      UINT4 u4NetworkType)
{
    UINT4               u4ErrCode;

    if (CfaValidateIfIndex (u4IfIndex) == CFA_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Invalid interface\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2IfMainNetworkType
        (&u4ErrCode, (INT4) u4IfIndex, (INT4) u4NetworkType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIfMainNetworkType (u4IfIndex, u4NetworkType) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME : CfaCliSetPktRxValue
 *
 * DESCRIPTION   : Sets the packet pattern and pattern index to analyse the 
 *                 packlets reveived on all ports of CFA.
 *
 * INPUTS        : CliHandle - CLI handle
 *                 u4Index - packet pattern index
 *                 pu1PktRxVal - Packet pattern to be analysed.
 *
 * RETURNS       : returns CFA_SUCCESS, when the packet pattern is set 
 *                 successfully
 *                 else returns CFA_FAILURE
 **************************************************************************/
INT4
CfaCliSetPktRxValue (tCliHandle CliHandle, UINT4 u4Index, UINT1 *pu1PktRxVal)
{
    UINT1              *pi1Ptr = pu1PktRxVal;
    tSNMP_OCTET_STRING_TYPE PktRxVal;
    UINT4               u4ErrCode = 0;
    UINT4               u4ByteValue = 0;
    INT4                i4Index1 = 0;
    INT4                i4PktRxStatus = 0;

    MEMSET (&PktRxVal, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhGetFsPacketAnalyserStatus (u4Index, &i4PktRxStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2FsPacketAnalyserStatus (&u4ErrCode, u4Index,
                                             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Packet Analyser: Invalid Index \r\n");
            return CLI_FAILURE;
        }
        nmhSetFsPacketAnalyserStatus (u4Index, CREATE_AND_WAIT);
    }
    if (i4PktRxStatus == ACTIVE)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Pakcet Analyser: Index already filled \r\n");
        return CLI_FAILURE;
    }

    PktRxVal.i4_Length = (INT4) STRLEN (pu1PktRxVal) / 2;

    for (i4Index1 = 0; i4Index1 < PktRxVal.i4_Length; ++i4Index1)
    {
        if (SSCANF ((char *) pi1Ptr, "%2x", &u4ByteValue) != 1)
        {
            break;
        }
        pu1PktRxVal[i4Index1] = (UINT1) u4ByteValue;
        pi1Ptr += 2;
    }

    PktRxVal.pu1_OctetList = pu1PktRxVal;

    if (nmhTestv2FsPacketAnalyserWatchValue (&u4ErrCode, u4Index, &PktRxVal)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Packet Analyser: Wrong Value \r\n");
        return CLI_FAILURE;
    }
    nmhSetFsPacketAnalyserWatchValue (u4Index, &PktRxVal);

    if (nmhTestv2FsPacketAnalyserStatus (&u4ErrCode, u4Index, ACTIVE)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Index Already Filled \r\n");
        return CLI_FAILURE;
    }
    nmhSetFsPacketAnalyserStatus (u4Index, ACTIVE);
    return CLI_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME : CfaCliSetPktRxMask
 *
 * INPUTS        : CliHandle - CLI handle
 *                 u4Index - packet pattern index
 *                 pu1PktRxMask - Packet pattern Mask to be analysed.
 *
 * DESCRIPTION   : Sets the i/f Mask value for the index specified.
 *
 * RETURNS       : returns 0 on CFA_SUCCESS
 *                 returns -1 on CFA_FAILURE
 **************************************************************************/
INT4
CfaCliSetPktRxMask (tCliHandle CliHandle, UINT4 u4Index, UINT1 *pu1PktRxMask)
{
    UINT1              *pi1Ptr = pu1PktRxMask;
    tSNMP_OCTET_STRING_TYPE PktRxMask;
    UINT4               u4ErrCode = 0;
    UINT4               u4ByteValue = 0;
    INT4                i4Index1 = 0;

    MEMSET (&PktRxMask, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    PktRxMask.i4_Length = (INT4) STRLEN (pu1PktRxMask) / 2;

    for (i4Index1 = 0; i4Index1 < PktRxMask.i4_Length; ++i4Index1)
    {
        if (SSCANF ((char *) pi1Ptr, "%2x", &u4ByteValue) != 1)
        {
            break;
        }
        pu1PktRxMask[i4Index1] = (UINT1) u4ByteValue;
        pi1Ptr += 2;
    }

    PktRxMask.pu1_OctetList = pu1PktRxMask;

    if (nmhTestv2FsPacketAnalyserWatchMask (&u4ErrCode, u4Index, &PktRxMask)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Packet analyser: Wrong Input values \r\n");

        return CLI_FAILURE;
    }
    nmhSetFsPacketAnalyserWatchMask (u4Index, &PktRxMask);
    return CLI_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME : CfaCliSetPortForPktRx
 *
 * DESCRIPTION   : Sets the ports on which the packet analyser has to work on.
 *
 * INPUTS        : pu1Ports - Port list of the interfaces to watch for the packet.
 *                 u4Index - packet pattern index
 *
 * RETURNS       : returns CFA_SUCCESS or CFA_FAILURE
 **************************************************************************/
INT4
CfaCliSetPortForPktRx (UINT4 u4Index, UINT1 *pu1Ports)
{
    UINT1              *pau1PortList = NULL;
    tSNMP_OCTET_STRING_TYPE PortListVal;
    UINT4               u4ErrCode = 0;
    pau1PortList = FsUtilAllocBitList (BRG_PORT_LIST_SIZE);

    if (pau1PortList == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaCliSetPortForPktRx: "
                 "Error in allocating memory for pau1PortList\r\n");
        return CFA_FAILURE;
    }
    MEMSET (pau1PortList, 0, BRG_PORT_LIST_SIZE);

    MEMSET (&PortListVal, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    PortListVal.pu1_OctetList = pau1PortList;
    PortListVal.i4_Length = BRG_PORT_LIST_SIZE;

    MEMCPY (PortListVal.pu1_OctetList, pu1Ports, BRG_PORT_LIST_SIZE);

    if (nmhTestv2FsPacketAnalyserWatchPorts (&u4ErrCode, u4Index, &PortListVal)
        == SNMP_FAILURE)
    {
        FsUtilReleaseBitList (pau1PortList);
        return CLI_FAILURE;
    }
    nmhSetFsPacketAnalyserWatchPorts (u4Index, &PortListVal);
    FsUtilReleaseBitList (pau1PortList);
    return CLI_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME : CfaCliPktRxDisable
 *
 * INPUTS        : CliHandle - CLI handle
 *                 u4Index - packet pattern index
 *
 * DESCRIPTION   : Disable Pattern Match for the Index Specified.
 *
 * RETURNS       : returns 0 on CFA_SUCCESS
 *                 returns -1 on CFA_FAILURE
 **************************************************************************/
INT4
CfaCliPktRxDisable (tCliHandle CliHandle, UINT4 u4Index)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPacketAnalyserStatus (&u4ErrCode, u4Index, DESTROY)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Packet Analyser: Invalid Index \r\n");
        return CLI_FAILURE;
    }
    nmhSetFsPacketAnalyserStatus (u4Index, DESTROY);

    return CLI_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME : CfaCliShowPktRxEntry 
 *
 * DESCRIPTION   : Displays the statistics of the Packet Analyser for the 
 *                 given index.
 *
 * INPUTS        : CliHandle - CLI handle
 *                 u4Index = Index of the Packet Analyser Table
 *
 * RETURNS       : returns 0 on CFA_SUCCESS
 *                 returns -1 on CFA_FAILURE
 **************************************************************************/
INT4
CfaCliShowPktRxEntry (tCliHandle CliHandle, UINT4 u4Index)
{
    tSNMP_OCTET_STRING_TYPE PktRxValue;
    tSNMP_OCTET_STRING_TYPE PktRxMask;
    tSNMP_OCTET_STRING_TYPE MatchPorts;
    tSNMP_OCTET_STRING_TYPE WatchPorts;
    UINT2               u2Result = OSIX_TRUE;
    UINT4               u4IfIndex = 0;
    UINT4               u4PacketCount = 0;
    UINT4               u4RcvdTime = 0;
    UINT4               u4CreateTime = 0;
    UINT4               u4PagingStatus = 0;
    INT4                i4PktRxRowStatus = 0;

    MEMSET (&PktRxValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PktRxMask, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&MatchPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WatchPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    if (nmhGetFsPacketAnalyserStatus (u4Index, &i4PktRxRowStatus)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Packet analyser Index not exists\r\n");
        return CLI_FAILURE;
    }

    nmhGetFsPacketAnalyserWatchValue (u4Index, &PktRxValue);
    nmhGetFsPacketAnalyserWatchMask (u4Index, &PktRxMask);
    nmhGetFsPacketAnalyserMatchPorts (u4Index, &MatchPorts);
    nmhGetFsPacketAnalyserWatchPorts (u4Index, &WatchPorts);
    nmhGetFsPacketAnalyserCounter (u4Index, &u4PacketCount);
    nmhGetFsPacketAnalyserCreateTime (u4Index, &u4CreateTime);
    nmhGetFsPacketAnalyserTime (u4Index, &u4RcvdTime);

    CliPrintf (CliHandle, "  Index       : %d\r\n", u4Index);
    CliPrintf (CliHandle, "  Value       : ");
    CfaCliPrintHex (CliHandle, &PktRxValue);
    CliPrintf (CliHandle, "  Mask        : ");
    CfaCliPrintHex (CliHandle, &PktRxMask);
    if (CliOctetToIfName (CliHandle, "  Match Ports :", &MatchPorts,
                          BRG_PORT_LIST_SIZE, BRG_PORT_LIST_SIZE,
                          0, &u4PagingStatus,
                          CFA_MAX_PKT_GEN_ENTRIES) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    for (u4IfIndex = 1;
         u4IfIndex < SYS_DEF_MAX_INTERFACES && u2Result == OSIX_TRUE;
         u4IfIndex++)
    {

        OSIX_BITLIST_IS_BIT_SET (WatchPorts.pu1_OctetList, u4IfIndex,
                                 BRG_PORT_LIST_SIZE, u2Result);
    }

    if (u2Result != OSIX_FALSE)
    {
        CliPrintf (CliHandle, "  Watch Ports : All Ports \n ");
    }
    else
    {
        if (CliOctetToIfName (CliHandle, "  Watch Ports :", &WatchPorts,
                              BRG_PORT_LIST_SIZE, BRG_PORT_LIST_SIZE,
                              0, &u4PagingStatus,
                              CFA_MAX_PKT_GEN_ENTRIES) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    CliPrintf (CliHandle, "  No of Pkts  : %d\r\n", u4PacketCount);
    CliPrintf (CliHandle, "  Last Rcvd   : %d\r\n", u4RcvdTime);

    return CLI_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME : CfaCliShowPktRxTable 
 *
 * INPUTS        : CliHandle - CLI handle
 *
 * DESCRIPTION   : Displays the statistics of the Packet Analyser for  
 *                 all index.
 *
 * RETURNS       : returns 0 on CFA_SUCCESS
 *                 returns -1 on CFA_FAILURE
 **************************************************************************/
INT4
CfaCliShowPktRxTable (tCliHandle CliHandle)
{
    UINT4               u4Index = 0;
    UINT4               u4NextIndex = 0;
    UINT4               u4RetStat = 0;

    u4RetStat = (UINT4) nmhGetFirstIndexFsPacketAnalyserTable (&u4Index);

    while (u4RetStat != SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n");
        if (CfaCliShowPktRxEntry (CliHandle, u4Index) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
        u4RetStat = (UINT4) nmhGetNextIndexFsPacketAnalyserTable (u4Index,
                                                                  &u4NextIndex);
        u4Index = u4NextIndex;
    }
    return CLI_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME : CfaCliSetPktTxValue 
 *                 
 * DESCRIPTION   : This function sets the given value for the Transmitter Packet
 *                 In order to send the packet, both the Packet value and the
 *                 port should be set.
 *
 * INPUTS        : CliHandle - CLI handle
 *                 u4Index - packet pattern index
 *                 pu1PktTxVal - Packet pattern to be Sent.
 *
 * RETURNS       : returns 0 on CFA_SUCCESS
 *                 returns -1 on CFA_FAILURE
 **************************************************************************/
INT4
CfaCliSetPktTxValue (tCliHandle CliHandle, UINT4 u4Index, UINT1 *pu1PktTxVal)
{
    tSNMP_OCTET_STRING_TYPE PktTxVal;
    UINT1              *pi1Ptr = pu1PktTxVal;
    UINT4               u4ErrCode = 0;
    UINT4               u4ByteValue = 0;
    INT4                i4Index1 = 0;
    INT4                i4PktgenStatus = 0;
    UINT1               u1SetActive = 0;

    MEMSET (&PktTxVal, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhGetFsPacketTransmitterStatus (u4Index, &i4PktgenStatus)
        == SNMP_FAILURE)
    {
        if (nmhTestv2FsPacketTransmitterStatus (&u4ErrCode, u4Index,
                                                CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%% Packet Transmitter: Invalid Index \r\n");
            return CLI_FAILURE;
        }
        nmhSetFsPacketTransmitterStatus (u4Index, CREATE_AND_WAIT);
        u1SetActive = 1;
    }
    if (i4PktgenStatus == ACTIVE)
    {
        CliPrintf (CliHandle,
                   "\r\n %% Packet Transmitter: Interface Already exists \r\n");
        return CLI_FAILURE;
    }

    PktTxVal.i4_Length = (INT4) STRLEN (pu1PktTxVal) / 2;

    for (i4Index1 = 0; i4Index1 < PktTxVal.i4_Length; ++i4Index1)
    {
        if (SSCANF ((char *) pi1Ptr, "%2x", &u4ByteValue) != 1)
        {
            break;
        }
        pu1PktTxVal[i4Index1] = (UINT1) u4ByteValue;
        pi1Ptr += 2;
    }

    PktTxVal.pu1_OctetList = pu1PktTxVal;

    if (nmhTestv2FsPacketTransmitterValue (&u4ErrCode, u4Index, &PktTxVal)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Packet Transmitter: Wrong value \r\n");
        return CLI_FAILURE;
    }

    nmhSetFsPacketTransmitterValue (u4Index, &PktTxVal);

    if (u1SetActive == 1)
    {
        i4PktgenStatus = NOT_READY;
    }
    else
    {
        i4PktgenStatus = ACTIVE;
    }

    if (nmhTestv2FsPacketTransmitterStatus (&u4ErrCode, u4Index,
                                            i4PktgenStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Please Enter the Port to transmit the "
                   "packet \r\n");
        return CLI_FAILURE;
    }
    nmhSetFsPacketTransmitterStatus (u4Index, i4PktgenStatus);
    return CLI_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME : CfaCliSetPktTxPort 
 *
 * DESCRIPTION   : This function sets the given port for Transmittinf the 
 *                 packet. In order to send the packet, both the Packet value 
 *                 and the port should be set.
 *
 * INPUTS        : CliHandle - CLI handle
 *                 u4Index - packet pattern index
 *                 pu1Ports - Port List to send the packets
 *                 
 * RETURNS       : returns 0 on CFA_SUCCESS
 *                 returns -1 on CFA_FAILURE
 **************************************************************************/
INT4
CfaCliSetPktTxPort (tCliHandle CliHandle, UINT4 u4Index, UINT1 *pu1Ports)
{
    UINT4               u4ErrCode = 0;
    INT4                i4PktgenStatus = 0;
    UINT1               u1SetActive = 0;

    if (nmhGetFsPacketTransmitterStatus (u4Index, &i4PktgenStatus)
        == SNMP_FAILURE)
    {
        if (nmhTestv2FsPacketTransmitterStatus (&u4ErrCode, u4Index,
                                                CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Invalid Index \r\n");
            return CLI_FAILURE;
        }
        nmhSetFsPacketTransmitterStatus (u4Index, CREATE_AND_WAIT);
        u1SetActive = 1;
    }
    if (i4PktgenStatus == ACTIVE)
    {
        CliPrintf (CliHandle, "\r\n %% Interface Already Filled \r\n");
        return CLI_FAILURE;
    }
    CfaCliSetPortForPktTx (u4Index, pu1Ports);
    if (u1SetActive == 1)
    {
        i4PktgenStatus = NOT_READY;
    }
    else
    {
        i4PktgenStatus = ACTIVE;
    }

    if (nmhTestv2FsPacketTransmitterStatus (&u4ErrCode, u4Index,
                                            i4PktgenStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Please Enter the Value of the "
                   "packet    \r\n");
        return CLI_FAILURE;
    }
    nmhSetFsPacketTransmitterStatus (u4Index, i4PktgenStatus);
    return CLI_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME : CfaCliSetPktTxPortCount
 *
 * DESCRIPTION   : This function sets the given port and the number of packets
 *                 for Transmitting. In order to send the packet, 
 *                 both the Packet value and the port should be set.
 *                 
 * INPUTS        : CliHandle - CLI handle
 *                 u4Index - packet pattern index
 *                 pu1Ports - Port List to send the packets
 *                 u4Count - Number of packets to send.
 *                 
 * RETURNS       : returns 0 on CFA_SUCCESS
 *                 returns -1 on CFA_FAILURE
 **************************************************************************/
INT4
CfaCliSetPktTxPortCount (tCliHandle CliHandle, UINT4 u4Index, UINT1 *pu1Ports,
                         UINT4 u4PktCount)
{
    UINT4               u4ErrCode = 0;
    INT4                i4PktgenStatus = 0;
    UINT1               u1SetActive = 0;

    if (nmhGetFsPacketTransmitterStatus (u4Index, &i4PktgenStatus)
        == SNMP_FAILURE)
    {
        if (nmhTestv2FsPacketTransmitterStatus (&u4ErrCode, u4Index,
                                                CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Invalid Index \r\n");
            return CLI_FAILURE;
        }
        u1SetActive = 1;
        nmhSetFsPacketTransmitterStatus (u4Index, CREATE_AND_WAIT);
    }

    CfaCliSetPortForPktTx (u4Index, pu1Ports);
    CfaCliSetCountForPktTx (u4Index, u4PktCount);

    if (i4PktgenStatus == ACTIVE)
    {
        CliPrintf (CliHandle, "\r\n %% Interface Already Filled \r\n");
        return CLI_FAILURE;
    }
    if (u1SetActive == 1)
    {
        i4PktgenStatus = NOT_READY;
    }
    else
    {
        i4PktgenStatus = ACTIVE;
    }

    if (nmhTestv2FsPacketTransmitterStatus (&u4ErrCode, u4Index,
                                            i4PktgenStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Please Enter the Value of the "
                   "packet    \r\n");
        return CLI_FAILURE;
    }
    nmhSetFsPacketTransmitterStatus (u4Index, i4PktgenStatus);
    return CLI_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME : CfaCliSetPktTxAll
 *
 * DESCRIPTION   : This function sets the given port and the number of packets
 *                 and interval for Transmitting. In order to send the packet, 
 *                 both the Packet value and the port should be set.
 *
 * INPUTS        : CliHandle - CLI handle
 *                 u4Index - packet pattern index
 *                 pu1Ports - Port List to send the packets
 *                 u4interval - Time interval to send the packets
 *                 u4Count - Number of packets to send.
 *                 
 * RETURNS       : returns 0 on CFA_SUCCESS
 *                 returns -1 on CFA_FAILURE
 **************************************************************************/
INT4
CfaCliSetPktTxAll (tCliHandle CliHandle, UINT4 u4Index, UINT1 *pu1Ports,
                   UINT4 u4Interval, UINT4 u4Count)
{
    UINT4               u4ErrCode = 0;
    INT4                i4PktgenStatus = 0;
    UINT1               u1SetActive = 0;

    if (nmhGetFsPacketTransmitterStatus (u4Index, &i4PktgenStatus)
        == SNMP_FAILURE)
    {
        if (nmhTestv2FsPacketTransmitterStatus (&u4ErrCode, u4Index,
                                                CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Invalid Packet Gen Index \r\n");
            return CLI_FAILURE;
        }
        nmhSetFsPacketTransmitterStatus (u4Index, CREATE_AND_WAIT);
        u1SetActive = 1;
    }
    if (i4PktgenStatus == ACTIVE)
    {
        CliPrintf (CliHandle, "\r\n %% Pacget Gen Index already present \r\n");
        return CLI_FAILURE;
    }
    CfaCliSetPortForPktTx (u4Index, pu1Ports);
    CfaCliSetCountForPktTx (u4Index, u4Count);
    CfaCliSetIntervalForPktTx (u4Index, u4Interval);
    if (u1SetActive == 1)
    {
        i4PktgenStatus = NOT_READY;
    }
    else
    {
        i4PktgenStatus = ACTIVE;
    }

    if (nmhTestv2FsPacketTransmitterStatus (&u4ErrCode, u4Index,
                                            i4PktgenStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Please Enter the Value of the "
                   "packet    \r\n");
        return CLI_FAILURE;
    }
    nmhSetFsPacketTransmitterStatus (u4Index, i4PktgenStatus);
    return CLI_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME : CfaCliSetPortForPktTx
 *
 * DESCRIPTION   : Sets the packet analyser ports for the given index.
 *
 * INPUTS        : pu1Ports - Port list of the interfaces to send the packet.
 *                 u4Index - packet pattern index
 *
 * RETURNS       : returns CFA_SUCCESS, when the packet ports are set 
 *                 successfully
 *                 else returns CFA_FAILURE
 **************************************************************************/
INT4
CfaCliSetPortForPktTx (UINT4 u4Index, UINT1 *pu1Ports)
{
    tSNMP_OCTET_STRING_TYPE PortListVal;
    UINT4               u4ErrCode = 0;
    UINT1              *pau1PortList = NULL;
    pau1PortList = FsUtilAllocBitList (BRG_PORT_LIST_SIZE);

    if (pau1PortList == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaCliSetPortForPktTx: "
                 "Error in allocating memory for pau1PortList\r\n");
        return CFA_FAILURE;
    }
    MEMSET (pau1PortList, 0, BRG_PORT_LIST_SIZE);
    MEMSET (&PortListVal, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    PortListVal.pu1_OctetList = pau1PortList;
    PortListVal.i4_Length = BRG_PORT_LIST_SIZE;

    MEMCPY (PortListVal.pu1_OctetList, pu1Ports, BRG_PORT_LIST_SIZE);

    if (nmhTestv2FsPacketTransmitterPort (&u4ErrCode, u4Index, &PortListVal)
        == SNMP_FAILURE)
    {
        FsUtilReleaseBitList (pau1PortList);
        return CLI_FAILURE;
    }
    nmhSetFsPacketTransmitterPort (u4Index, &PortListVal);

    FsUtilReleaseBitList (pau1PortList);
    return CLI_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME : CfaCliSetCountForPktTx
 *
 * DESCRIPTION   : Sets the packet analyser count for the given index.
 *
 * INPUTS        : u4Count - Number of packets to send. 0 = infinite.
 *                 u4Index - packet pattern index
 *
 * RETURNS       : returns CFA_SUCCESS, when the packet count is set 
 *                 successfully
 *                 else returns CFA_FAILURE
 **************************************************************************/
INT4
CfaCliSetCountForPktTx (UINT4 u4Index, UINT4 u4Count)
{
    UINT4               u4ErrCode = 0;
    if (nmhTestv2FsPacketTransmitterCount (&u4ErrCode, u4Index, u4Count)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsPacketTransmitterCount (u4Index, u4Count);

    return CLI_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME : CfaCliSetIntervalForPktTx
 *
 * DESCRIPTION   : Sets the packet analyser interval for the given index. 
 *
 * INPUTS        : u4Interval - Interval Value.
 *                 u4Index - packet pattern index
 *
 * RETURNS       : returns CFA_SUCCESS, when the packet interval is set 
 *                 successfully
 *                 else returns CFA_FAILURE
 **************************************************************************/
INT4
CfaCliSetIntervalForPktTx (UINT4 u4Index, UINT4 u4Interval)
{
    UINT4               u4ErrCode = 0;
    if (nmhTestv2FsPacketTransmitterInterval (&u4ErrCode, u4Index, u4Interval)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsPacketTransmitterInterval (u4Index, u4Interval);

    return CLI_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME : CfaCliPktTxDisable
 *
 * DESCRIPTION   : Sets the packet Transmitter Row Status to destroy.
 *                 This stops the packet generator and deletes that row
 *
 * INPUTS        : CliHandle - CLI handle
 *                 u4Index - packet pattern index
 *
 * RETURNS       : returns CFA_SUCCESS, when the packet row is deleted 
 *                 successfully
 *                 else returns CFA_FAILURE
 **************************************************************************/
INT4
CfaCliPktTxDisable (tCliHandle CliHandle, UINT4 u4Index)
{
    UINT4               u4ErrCode;
    if (nmhTestv2FsPacketTransmitterStatus (&u4ErrCode, u4Index,
                                            DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Packet Transmitter: Index not found \r\n");
        return CLI_FAILURE;
    }
    nmhSetFsPacketTransmitterStatus (u4Index, DESTROY);
    return CLI_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME : CfaCliClearPktRxMask
 *
 * DESCRIPTION   : Sets the packet Mask to defaukt Value for the given index.
 *
 * INPUTS        : CliHandle - CLI handle
 *                 u4Index - packet pattern index
 *
 * RETURNS       : returns CFA_SUCCESS, when the packet mask is set 
 *                 successfully
 *                 else returns CFA_FAILURE
 **************************************************************************/
INT4
CfaCliClearPktRxMask (tCliHandle CliHandle, UINT4 u4Index)
{
    tSNMP_OCTET_STRING_TYPE *pPktRxMask = NULL;
    UINT4               u4ErrCode = 0;
    INT4                i4PatternRowStatus = 0;

    if (nmhGetFsPacketAnalyserStatus (u4Index, &i4PatternRowStatus)
        == SNMP_FAILURE)
    {

        CliPrintf (CliHandle, "\r\n%% Invalid Index \r\n");
        return CLI_FAILURE;
    }

    pPktRxMask = allocmem_octetstring (CFA_MAX_PKT_GEN_PACKET_VAL);
    if (pPktRxMask == NULL)
    {
        CliPrintf (CliHandle, "\r%% cfa : Memory Allocation failure\r\n");
        return CLI_FAILURE;
    }
    MEMSET (pPktRxMask, 0, CFA_MAX_PKT_GEN_PACKET_VAL);

    if (nmhTestv2FsPacketAnalyserWatchMask (&u4ErrCode, u4Index, pPktRxMask)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Invalid Index \r\n");
        free_octetstring (pPktRxMask);
        return CLI_FAILURE;
    }
    nmhSetFsPacketAnalyserWatchMask (u4Index, pPktRxMask);
    free_octetstring (pPktRxMask);
    return CLI_SUCCESS;

}

/**************************************************************************
 * FUNCTION NAME : CfaCliShowPktTxEntry
 *
 * DESCRIPTION   : Shows the Packet Transmotter Values for the given Index
 *
 * INPUTS        : CliHandle - CLI handle
 *                 u4Index - packet pattern index
 *
 * RETURNS       : returns CFA_SUCCESS/CFA_FAILURE
 **************************************************************************/
INT4
CfaCliShowPktTxEntry (tCliHandle CliHandle, UINT4 u4Index)
{
    tSNMP_OCTET_STRING_TYPE PktgenValue;
    tSNMP_OCTET_STRING_TYPE PktgenPorts;
    UINT4               u4PacketCount = 0;
    UINT4               u4RcvdTime = 0;
    UINT4               u4PagingStatus = 0;
    INT4                i4PktgenRowStatus = 0;

    MEMSET (&PktgenValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PktgenPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhGetFsPacketTransmitterStatus (u4Index, &i4PktgenRowStatus)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Invalid Index \r\n");
        return CLI_FAILURE;
    }
    nmhGetFsPacketTransmitterValue (u4Index, &PktgenValue);
    nmhGetFsPacketTransmitterPort (u4Index, &PktgenPorts);
    nmhGetFsPacketTransmitterCount (u4Index, &u4PacketCount);
    nmhGetFsPacketTransmitterInterval (u4Index, &u4RcvdTime);

    CliPrintf (CliHandle, "  Index              : %d\r\n", u4Index);
    CliPrintf (CliHandle, "  Value of the Pkt   : ");
    CfaCliPrintHex (CliHandle, &PktgenValue);
    if (CliOctetToIfName (CliHandle, "  Ports to send Pkt  :", &PktgenPorts,
                          BRG_PORT_LIST_SIZE, BRG_PORT_LIST_SIZE,
                          0, &u4PagingStatus,
                          CFA_MAX_PKT_GEN_ENTRIES) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "  No of Pkts to send : ");
    if (u4PacketCount == 0)
    {
        CliPrintf (CliHandle, "Infinite\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "%d\r\n", u4PacketCount);
    }
    CliPrintf (CliHandle, "  Time Interval      : %d\r\n", u4RcvdTime);

    return CLI_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME : CfaCliShowPktTxTable
 *
 * DESCRIPTION   : Shows the contents in the Packet Transmitter Table.
 *
 * INPUTS        : CliHandle - CLI handle
 *                 u4Index - packet pattern index
 *                 pu1PktRxVal - Packet pattern to be analysed.
 *
 * RETURNS       : returns CFA_SUCCESS/CFA_FAILURE
 **************************************************************************/
INT4
CfaCliShowPktTxTable (tCliHandle CliHandle)
{
    UINT4               u4Index = 0;
    UINT4               u4NextIndex = 0;
    UINT4               u4RetStat = 0;

    u4RetStat = (UINT4) nmhGetFirstIndexFsPacketTransmitterTable (&u4Index);

    while (u4RetStat != SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n");
        if (CfaCliShowPktTxEntry (CliHandle, u4Index) == CLI_FAILURE)
            return CLI_FAILURE;
        u4RetStat = (UINT4) nmhGetNextIndexFsPacketTransmitterTable (u4Index,
                                                                     &u4NextIndex);
        u4Index = u4NextIndex;
    }
    return CLI_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME : CfaCliPrintHex
 *
 * DESCRIPTION   : This function prints the Hex to String
 *
 * INPUTS        : CliHandle - CLI handle
 *                 pValue - Packet pattern to be printed.
 *
 * RETURNS       : none
 *
 **************************************************************************/
static VOID
CfaCliPrintHex (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE * pValue)
{
    INT4                i4Index1 = 0;

    for (i4Index1 = 0; i4Index1 < pValue->i4_Length; i4Index1++)
    {
        CliPrintf (CliHandle, "%2x ", pValue->pu1_OctetList[i4Index1]);
    }
    CliPrintf (CliHandle, "\r\n");
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliSetACIfInfo                                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the AC information for a given  */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : u4IfIndex - AC interface index                     */
/*                        u4PhyIndex - Physical interface index              */
/*                        CliHandle - CLI Handler                            */
/*                        u4VlanId - Customer VLAN Idendifier               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCliSetACIfInfo (tCliHandle CliHandle, UINT4 u4ACIndex, UINT4 u4PhyIndex,
                   UINT4 u4VlanId)
{

    UINT4               u4ErrCode = 0;

    if (nmhTestv2IfMainRowStatus
        (&u4ErrCode, (INT4) u4ACIndex, CFA_RS_NOTINSERVICE) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIfMainRowStatus ((INT4) u4ACIndex, CFA_RS_NOTINSERVICE)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhTestv2IfACPortIdentifier (&u4ErrCode, (INT4) u4ACIndex,
                                     (INT4) u4PhyIndex) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Failure in testing physical interface that will be mapped to attachment circuit interface\n");
        return (CLI_FAILURE);
    }
    if (nmhSetIfACPortIdentifier ((INT4) u4ACIndex, (INT4) u4PhyIndex)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set physical interface for  attachment circuit interface\n");
        return (CLI_FAILURE);
    }
    if (nmhTestv2IfACCustomerVlan (&u4ErrCode, (INT4) u4ACIndex,
                                   (INT4) u4VlanId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Failure in testing vlan to attachment circuit interface\n");
        return (CLI_FAILURE);
    }
    if (nmhSetIfACCustomerVlan ((INT4) u4ACIndex, (INT4) u4VlanId)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set vlan for attachment circuit interface\n");
        return (CLI_FAILURE);
    }
    if (nmhTestv2IfMainRowStatus (&u4ErrCode, (INT4) u4ACIndex, CFA_RS_ACTIVE)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIfMainRowStatus ((INT4) u4ACIndex, CFA_RS_ACTIVE) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

#ifdef HDLC_WANTED
/*****************************************************************************
 * Function Name :  CfaCliSetHdlcControllerEnable                            *
 *                                                                           *
 * Description   :  This function  is used to get HDLC Configuration         *
 *                  prompt and enable the controller HDLC.                   *
 *                                                                           *
 * Input (s)     :  CliHandle    - Handle to CLI session                     *
 *                  u4HdlcIndex  - HDLCInterface Index                       *
 *                                                                           *
 * Output (s)    :  None.                                                    *
 *                                                                           *
 * Returns       :  CLI_SUCCESS/CLI_FAILURE                                  *
 *****************************************************************************/
INT4
CfaCliSetHdlcControllerEnable (tCliHandle CliHandle, UINT4 u4HdlcIndex)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    SPRINTF ((CHR1 *) au1Cmd, "%s%u", CFA_HDLC_MODE, u4HdlcIndex);

    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "/r%% Unable to enter into HDLC configuration mode.\r\n");
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}
#endif
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliValidateIpInterfaceName                      */
/*                                                                           */
/*     DESCRIPTION      : This function validates Ip interface name          */
/*                                                                           */
/*     INPUT            : pi1IfName                                          */
/*                                                                           */
/*     OUTPUT           : pi1RetIfName                                       */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCliValidateIpInterfaceName (INT1 *pi1IfName, INT1 *pi1RetIfName,
                               UINT4 *pu4RetIfType)
{
    INT4                i4Index;
    INT4                i4Len = 0;
    INT4                i4IfTypeLen = 0;
#define CFA_CLI_MAX_IPIFTYPES   1
    tCfaIfaceInfo       ai1CfaIfTypes[CFA_CLI_MAX_IPIFTYPES] = {
        {CFA_PSEUDO_WIRE, "pw", "0"}
    };

    if (!(pi1IfName) || !(*pi1IfName))
    {
        return CLI_FAILURE;
    }

    i4Len = (INT4) STRLEN (pi1IfName);

    /* Check for longest match with  any interface type's available */

    for (i4Index = 0; i4Index < CFA_CLI_MAX_IPIFTYPES; i4Index++)
    {
        i4IfTypeLen = (INT4) STRLEN (ai1CfaIfTypes[i4Index].au1IfName);

        i4Len = (i4Len < i4IfTypeLen) ? (i4Len) : (i4IfTypeLen);

        if (STRNCASECMP (ai1CfaIfTypes[i4Index].au1IfName, pi1IfName, i4Len) ==
            0)
        {
            break;
        }
    }
    if (i4Index == CFA_CLI_MAX_IPIFTYPES)
    {
        return CLI_FAILURE;
    }
    if (pi1RetIfName != NULL)
    {
        STRNCPY (pi1RetIfName, ai1CfaIfTypes[i4Index].au1IfName,
                 STRLEN (ai1CfaIfTypes[i4Index].au1IfName));
        pi1RetIfName[STRLEN (ai1CfaIfTypes[i4Index].au1IfName)] = '\0';
    }
    if (pu4RetIfType != NULL)
    {
        *pu4RetIfType = ai1CfaIfTypes[i4Index].u4IfType;
    }

    return CLI_SUCCESS;
}

#ifdef MPLS_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliActOnLock                                    */
/*                                                                           */
/*     DESCRIPTION      : This function takes or releases lock for           */
/*                        modules MPLS or ALL                                */
/*                                                                           */
/*     INPUT            : CliHandle  - CliHandle                             */
/*                        bLockFlag  - If true takes CFA_LOCK, If false,     */
/*                                     releases CFA_LOCK                     */
/*                        u4Module   - Module for which SRC is displayed     */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
CfaCliActOnLock (tCliHandle CliHandle, BOOL1 bLockFlag, UINT4 u4Module)
{
    if ((u4Module != ISS_MPLS_SHOW_RUNNING_CONFIG) &&
        (u4Module != ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        return;
    }

    if (bLockFlag == TRUE)
    {
        CliRegisterLock (CliHandle, CfaLock, CfaUnlock);
        CFA_LOCK ();
    }
    else
    {
        CFA_UNLOCK ();
        CliUnRegisterLock (CliHandle);
    }

    return;
}
#endif /* MPLS_WANTED */

/*****************************************************************************/
/*     FUNCTION NAME    : IssInterfaceShowDebugging                          */
/*                                                                           */
/*     DESCRIPTION      : This function prints interface Debug level         */
/*                        for interfaces                                     */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
IssInterfaceShowDebugging (tCliHandle CliHandle)
{
    UINT4               u4DebugTrace = 0;

    nmhGetIfmDebug (&u4DebugTrace);

    if (u4DebugTrace == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rINTERFACE :");

    if (u4DebugTrace & CFA_TRC_ALL_TRACK)
    {
        CliPrintf (CliHandle, "\r\n  Interface all track debugging is on");
    }
    if (u4DebugTrace & CFA_TRC_ENET_PKT_DUMP)
    {
        CliPrintf (CliHandle,
                   "\r\n  Interface ENET packet dump debugging is on");
    }
    if (u4DebugTrace & CFA_TRC_IP_PKT_DUMP)
    {
        CliPrintf (CliHandle, "\r\n  Interface IP packet dump debugging is on");
    }
    if (u4DebugTrace & CFA_TRC_ARP_PKT_DUMP)
    {
        CliPrintf (CliHandle,
                   "\r\n  Interface ARP packet dump debugging is on");
    }
    if (u4DebugTrace & CFA_TRC_ERROR)
    {
        CliPrintf (CliHandle, "\r\n  Interface error debugging is on");
    }
    if (u4DebugTrace & OS_RESOURCE_TRC)
    {
        CliPrintf (CliHandle, "\r\n  Interface OS Resource debugging is on");
    }
    if (u4DebugTrace & ALL_FAILURE_TRC)
    {
        CliPrintf (CliHandle, "\r\n  Interface all failures including packet"
                   " validation debugging is on");
    }
    if (u4DebugTrace & BUFFER_TRC)
    {
        CliPrintf (CliHandle, "\r\n  Interface buffer debugging is on");
    }
    CliPrintf (CliHandle, "\r\n");

    return;
}

/*****************************************************************************/
/* Function Name      : CfaCliGetShowCmdOutputToFile                         */
/*                                                                           */
/* Description        : This function handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1FileName - The output of the show cmd is          */
/*                      redirected to this file                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaCliGetShowCmdOutputToFile (UINT1 *pu1FileName)
{
    if (FileStat ((const CHR1 *) pu1FileName) == OSIX_SUCCESS)
    {
        if (0 != FileDelete (pu1FileName))
        {
            return CFA_FAILURE;
        }
    }
    if (CliGetShowCmdOutputToFile (pu1FileName, (UINT1 *) CFA_AUDIT_SHOW_CMD)
        == CLI_FAILURE)
    {
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaCliCalcSwAudCheckSum                              */
/*                                                                           */
/* Description        : This function handles the Calculation of checksum    */
/*                      for the data in the given input file.                */
/*                                                                           */
/* Input(s)           : pu1FileName - The checksum is calculated for the data*/
/*                      in this file                                         */
/*                                                                           */
/* Output(s)          : pu2ChkSum - The calculated checksum is stored here   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaCliCalcSwAudCheckSum (UINT1 *pu1FileName, UINT2 *pu2ChkSum)
{
    INT1                ai1Buf[CFA_CLI_MAX_GROUPS_LINE_LEN + 1];
    INT4                i4Fd;
    INT2                i2ReadLen;
    UINT4               u4Sum = 0;
    UINT4               u4Last = 0;
    UINT2               u2CkSum = 0;

    i4Fd = FileOpen ((CONST UINT1 *) pu1FileName, CFA_CLI_RDONLY);
    if (i4Fd == -1)
    {
        return CFA_FAILURE;
    }
    MEMSET (ai1Buf, 0, CFA_CLI_MAX_GROUPS_LINE_LEN + 1);
    while (CfaCliReadLineFromFile (i4Fd, ai1Buf, CFA_CLI_MAX_GROUPS_LINE_LEN,
                                   &i2ReadLen) != CFA_CLI_EOF)

    {
        if ((i2ReadLen > 0) &&
            (NULL == STRSTR (ai1Buf, "Packets")) &&
            (NULL == STRSTR (ai1Buf, "Octets")) &&
            (NULL == STRSTR (ai1Buf, "Counters")))
        {
            UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                              &u2CkSum, (UINT4) i2ReadLen, CKSUM_DATA);

            MEMSET (ai1Buf, '\0', CFA_CLI_MAX_GROUPS_LINE_LEN + 1);
        }
    }
    /*CheckSum for last line */
    if ((u4Sum != 0) || (i2ReadLen != 0))
    {
        UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                          &u2CkSum, (UINT4) i2ReadLen, CKSUM_LASTDATA);

    }
    *pu2ChkSum = u2CkSum;
    if (FileClose (i4Fd) < 0)
    {
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaCliReadLineFromFile                               */
/*                                                                           */
/* Description        : It is a utility to read a line from the given file   */
/*                      descriptor.                                          */
/*                                                                           */
/* Input(s)           : i4Fd - File Descriptor for the file                  */
/*                      i2MaxLen - Maximum length that can be read and store */
/*                                                                           */
/* Output(s)          : pi1Buf - Buffer to store the line read from the file */
/*                      pi2ReadLen - the total length of the data read       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT1
CfaCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen,
                        INT2 *pi2ReadLen)
{
    INT1                i1Char;

    *pi2ReadLen = 0;

    while (FileRead (i4Fd, (CHR1 *) & i1Char, 1) == 1)
    {
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (i1Char == '\n')
        {
            return (CFA_CLI_NO_EOF);
        }
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = 0;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (CFA_CLI_EOF);
}

/*****************************************************************************/
/*     FUNCTION NAME    : CfaCliSetCounters                                  */
/*                                                                           */
/*     DESCRIPTION      : This function enables counters statisctics         */
/*                        collection                                         */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface index                        */
/*                        u4IpIntfStats - enabled/disabled status            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCliSetCounters (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4IpIntfStats)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4ErrCode;

    i4RetVal =
        nmhTestv2IfIpIntfStatsEnable (&u4ErrCode, (INT4) u4IfIndex,
                                      (INT4) u4IpIntfStats);
    if (i4RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Invalid value for interface stats \n");
        return CLI_FAILURE;
    }

    i4RetVal =
        nmhSetIfIpIntfStatsEnable ((INT4) u4IfIndex, (INT4) u4IpIntfStats);
    if (i4RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Error in setting the interface stats status \n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : CfaCliSetUfdSystemCtrl 
 *
 *     DESCRIPTION      : This function will start/shut Ufd module
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Status   - Ufd System Control Status
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT4
CfaCliSetUfdSystemCtrl (tCliHandle CliHandle, INT4 i4Status)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IfUfdSystemControl (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIfUfdSystemControl (i4Status) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to shut/no shut the UFD\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : CfaCliSetUfdModuleStatus
 *
 *     DESCRIPTION      : This function will enable/disable UFD module
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Status   - Ufd Module status
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT4
CfaCliSetUfdModuleStatus (tCliHandle CliHandle, INT4 i4Status)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IfUfdModuleStatus (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIfUfdModuleStatus (i4Status) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% UFD is shut down. "
                   "Can't enable/disable UFD\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliSetPortRole                                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the port role for the           */
/*                        given port.                                        */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        u4IfIndex - Index of the interface                 */
/*                        i4PortRole- port role to be set.                   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
CfaCliSetPortRole (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4PortRole,
                   UINT4 u4DesigUplinkStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IfMainPortRole (&u4ErrorCode, (INT4) u4IfIndex,
                                 i4PortRole) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIfMainPortRole ((INT4) u4IfIndex, i4PortRole) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set the port role\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2IfMainDesigUplinkStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                          (INT4) u4DesigUplinkStatus) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIfMainDesigUplinkStatus
        ((INT4) u4IfIndex, (INT4) u4DesigUplinkStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set the port role as designated uplink\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliCreateUfdGroup                               */
/*                                                                           */
/*     DESCRIPTION      : This function will enter into UFD Group database   */
/*                        config mode. New UFD Group will be created when    */
/*                        identifier is not present in the database.         */
/*                                                                           */
/*     INPUT            : u4UfdGroupId  - UFD Group Identifier to create     */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
CfaCliCreateUfdGroup (tCliHandle CliHandle, UINT4 u4UfdGroupId,
                      UINT1 *pau1UfdGroupName)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4ErrCode = 0;
    INT4                i4RetValIfUfdGroupRowStatus = 0;
    tSNMP_OCTET_STRING_TYPE Name;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    MEMSET (&Name, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    /* Check if the UFD Group is already present */
    if ((nmhGetIfUfdGroupRowStatus ((INT4) u4UfdGroupId,
                                    &i4RetValIfUfdGroupRowStatus)) !=
        SNMP_SUCCESS)
    {

        /* UFD Group NOT present - CREATE */
        if (nmhTestv2IfUfdGroupRowStatus (&u4ErrCode, (INT4) u4UfdGroupId,
                                          CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Group row status validation failed\r\n");
            return CLI_FAILURE;
        }

        /* CREATE UFD Group */
        if (nmhSetIfUfdGroupRowStatus ((INT4) u4UfdGroupId, CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Group entry creation failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (pau1UfdGroupName != NULL)
    {
        Name.i4_Length = (INT4) STRLEN (pau1UfdGroupName);
        Name.pu1_OctetList = pau1UfdGroupName;

        if (nmhTestv2IfUfdGroupName (&u4ErrCode, (INT4) u4UfdGroupId,
                                     &Name) == SNMP_SUCCESS)
        {

            if (nmhTestv2IfUfdGroupRowStatus (&u4ErrCode, (INT4) u4UfdGroupId,
                                              NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Group row status validation failed\r\n");
                return CLI_FAILURE;
            }

            /* Set NOT_IN_SERVICE before setting the group name */
            if (nmhSetIfUfdGroupRowStatus ((INT4) u4UfdGroupId, NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to set the Row Status as NOT_IN_SERVICE\r\n");
                return CLI_FAILURE;
            }

            if (nmhSetIfUfdGroupName ((INT4) u4UfdGroupId, &Name)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to set Group name\r\n");
                return CLI_FAILURE;
            }
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r%% Group name validation failed, Groupname not set\r\n");
        }
    }
    if (nmhTestv2IfUfdGroupRowStatus (&u4ErrCode, (INT4) u4UfdGroupId,
                                      ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Group row status validation failed\r\n");
        return CLI_FAILURE;
    }
    /* Set NOT_IN_SERVICE before setting the group name */
    if (nmhSetIfUfdGroupRowStatus ((INT4) u4UfdGroupId, ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set the Row Status as ACTIVE\r\n");
        return CLI_FAILURE;
    }

    /* ENTER UFD Mode */
    SPRINTF ((CHR1 *) au1Cmd, "%s%u", CFA_UFD_MODE, (INT4) u4UfdGroupId);
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to enter into UFD configuration mode\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliDeleteUfdGroup                               */
/*                                                                           */
/*     DESCRIPTION      : This function deletes existing UFD group           */
/*                        configuration                                      */
/*                                                                           */
/*     INPUT            : i4UfdGroupId   - UFD Group Identifier to delete    */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
CfaCliDeleteUfdGroup (tCliHandle CliHandle, INT4 i4UfdGroupId)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2IfUfdGroupRowStatus (&u4ErrCode, i4UfdGroupId, DESTROY)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Group entry delete validation failed\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetIfUfdGroupRowStatus (i4UfdGroupId, DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Group entry deletion failed\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliUfdGroupSetPorts                             */
/*                                                                           */
/*     DESCRIPTION      : This function maps the ports to the group          */
/*                                                                           */
/*                                                                           */
/*     INPUT            : IfPortList - Contains Uplink ports                 */
/*                        u4Flag     - Contains forbidden ports              */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCliUfdGroupSetPorts (tCliHandle CliHandle, tPortList IfPortList,
                        UINT4 u4Flag)
{
    UINT4               u4UfdGroupId = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;
    UINT1               u1BrgPortType = 0;
    UINT4               u4IfIndex = 0;

    u4UfdGroupId = (UINT4) CLI_GET_UFD_GROUPID ();
    if (u4UfdGroupId == (UINT4) CLI_ERROR)
    {
        return CLI_FAILURE;
    }

    for (u2ByteIndex = 0; u2ByteIndex < BRG_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = IfPortList[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < BITS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & INTERNAL_BIT8) != 0)
            {
                u4IfIndex =
                    (UINT4) ((u2ByteIndex * BITS_PER_BYTE) + u2BitIndex + 1);

                CfaGetIfBrgPortType (u4IfIndex, &u1BrgPortType);

                if (u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
                {
                    /* S-Channel Interfaces cannot be added as a member 
                     * of UFD group */
                    CLI_SET_ERR (CLI_CFA_SBP_ERR);
                    return CLI_FAILURE;
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }

    }
    if (CfaCliUfdSetPortList (CliHandle, IfPortList, u4UfdGroupId, u4Flag) !=
        CFA_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%% Port list can not be added/deleted in UFD group\r\n");
        return CLI_FAILURE;
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliSetDesigUplinkPort                           */
/*                                                                           */
/*     DESCRIPTION      : Set the Designated uplink port in the UFD group    */
/*     NOTE             : Designated-uplink functionality will not work     */
/*                        since GroupDesigUplinkPort is deprecated.         */
/*     INPUT            : CliHandle - CLI Handler,                           */
/*                        i4IfIndex - Interface Index                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCliSetDesigUplinkPort (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4UfdGroupId = 0;

    u4UfdGroupId = (UINT4) CLI_GET_UFD_GROUPID ();

    if (u4UfdGroupId == (UINT4) CLI_ERROR)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2IfUfdGroupDesigUplinkPort (&u4ErrorCode, (INT4) u4UfdGroupId,
                                            i4IfIndex) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Group Designated uplink port validation failed\r\n");
        return CLI_FAILURE;
    }
    if (nmhTestv2IfUfdGroupRowStatus (&u4ErrorCode, (INT4) u4UfdGroupId,
                                      NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Group row status validation failed\r\n");
        return CLI_FAILURE;
    }
    /* Set NOT_IN_SERVICE before setting the designated uplink port */
    if (nmhSetIfUfdGroupRowStatus ((INT4) u4UfdGroupId, NOT_IN_SERVICE)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set the Row Status as NOT_IN_SERVICE\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetIfUfdGroupDesigUplinkPort ((INT4) u4UfdGroupId, i4IfIndex)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set the Port as designated uplink\r\n");
        return CLI_FAILURE;
    }
    if (nmhTestv2IfUfdGroupRowStatus (&u4ErrorCode, (INT4) u4UfdGroupId,
                                      ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Group row status validation failed\r\n");
        return CLI_FAILURE;
    }
    /* Set ACTIVE after setting the designated uplink port */
    if (nmhSetIfUfdGroupRowStatus ((INT4) u4UfdGroupId, ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set the Row Status as ACTIVE\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliShowPortRole                                 */
/*                                                                           */
/*     DESCRIPTION      : Displays UFD entry related information             */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler,                           */
/*                        i4NextIndex - Interface Index                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
CfaCliShowPortRole (tCliHandle CliHandle, INT4 i4NextIndex)
{
    INT4                i4PortRole = 0;
    INT4                i4PrevIndex = 0;
    INT4                i4Quit = 0;
    INT4                i4DesigUplink = 0;
    INT4                u4Continue = CFA_FALSE;
    INT1                ai1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    if (i4NextIndex == 0)
    {
        if (nmhGetFirstIndexIfTable (&i4NextIndex) == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
        u4Continue = CFA_TRUE;
    }
    if (nmhValidateIndexInstanceIfTable (i4NextIndex) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    while (u4Continue == CFA_TRUE)
    {
        MEMSET (ai1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
        CfaCliGetIfName ((UINT4) i4NextIndex, ai1IfName);
        nmhGetIfMainPortRole (i4NextIndex, &i4PortRole);
        nmhGetIfMainDesigUplinkStatus (i4NextIndex, &i4DesigUplink);
        CliPrintf (CliHandle, "%-12s", ai1IfName);
        if (i4PortRole == CFA_PORT_ROLE_UPLINK)
        {
            if (i4DesigUplink == CFA_ENABLED)
            {
                i4Quit = CliPrintf (CliHandle, "%-17s", "Desig-Uplink");
            }
            else
            {
                i4Quit = CliPrintf (CliHandle, "%-17s", "Uplink");
            }
        }
        else if (i4PortRole == CFA_PORT_ROLE_DOWNLINK)
        {
            i4Quit = CliPrintf (CliHandle, "%-17s", "Downlink");
        }
        if (i4Quit == CLI_FAILURE)
        {
            u4Continue = CFA_FALSE;
        }
        CliPrintf (CliHandle, "\r\n");
        i4PrevIndex = i4NextIndex;

        if (nmhGetNextIndexIfTable (i4PrevIndex, &i4NextIndex) == SNMP_FAILURE)
        {
            u4Continue = CFA_FALSE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliShowPortUfdStats                             */
/*                                                                           */
/*     DESCRIPTION      : Displays UFD entry related stats                   */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler,                           */
/*                        i4NextIndex - Interface Index                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
CfaCliShowPortUfdStats (tCliHandle CliHandle, INT4 i4NextIndex)
{
    UINT4               u4UfdDownlinkDisabledCount = 0;
    UINT4               u4UfdDownlinkEnabledCount = 0;
    INT4                i4PrevIndex = 0;
    INT4                i4Quit = 0;
    INT4                u4Continue = CFA_FALSE;
    INT1                ai1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               u1BrgPortType = 0;

    if (i4NextIndex == 0)
    {
        if (nmhGetFirstIndexIfTable (&i4NextIndex) == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
        u4Continue = CFA_TRUE;
    }
    if (nmhValidateIndexInstanceIfTable (i4NextIndex) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle, "\r\n%-12s %-20s %s\r\n",
               "Interface", "DownlinkEnabledCount", "DownlinkDisabledCount");

    while (u4Continue == CFA_TRUE)
    {
        CfaGetIfBrgPortType ((UINT4) i4NextIndex, &u1BrgPortType);
        /* UFD Statistics are anot applicable for S-Channel Interface */
        if (u1BrgPortType != CFA_STATION_FACING_BRIDGE_PORT)
        {
            MEMSET (ai1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
            CfaCliGetIfName ((UINT4) i4NextIndex, ai1IfName);
            nmhGetIfMainUfdDownlinkDisabledCount (i4NextIndex,
                                                  &u4UfdDownlinkDisabledCount);
            nmhGetIfMainUfdDownlinkEnabledCount (i4NextIndex,
                                                 &u4UfdDownlinkEnabledCount);
            CliPrintf (CliHandle, "%-30s", ai1IfName);
            i4Quit = CliPrintf (CliHandle, "%-20d", u4UfdDownlinkEnabledCount);
            i4Quit = CliPrintf (CliHandle, "%-20d", u4UfdDownlinkDisabledCount);
            if (i4Quit == CLI_FAILURE)
            {
                u4Continue = CFA_FALSE;
            }
            CliPrintf (CliHandle, "\r\n");
        }
        i4PrevIndex = i4NextIndex;
        if (nmhGetNextIndexIfTable (i4PrevIndex, &i4NextIndex) == SNMP_FAILURE)
        {
            u4Continue = CFA_FALSE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliShowUfdGroupDatabase                         */
/*                                                                           */
/*     DESCRIPTION      : Displays UFD information for a specific group id   */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler,                           */
/*                        i4NextGroupId - Group Index                        */
/*                        u4command - show command                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
CfaCliShowUfdGroupDatabase (tCliHandle CliHandle, INT4 i4NextIndex,
                            INT4 i4NextGroupId)
{

    INT1                ai1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    INT4                i4Quit = CLI_FAILURE;
    INT4                i4PortRole = CFA_PORT_ROLE_DOWNLINK;
    INT4                i4UfdOperStatus = CFA_IF_DOWN;
    INT4                i4PrevIndex = 0;
    INT4                i4GroupId = 0;
    UINT4               u4Continue = CFA_TRUE;

    while (u4Continue == CFA_TRUE)
    {
        nmhGetIfMainUfdGroupId (i4NextIndex, &i4GroupId);
        if (i4GroupId == i4NextGroupId)
        {
            MEMSET (ai1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
            CfaCliGetIfName ((UINT4) i4NextIndex, ai1IfName);
            i4Quit = CliPrintf (CliHandle, "%-12s", ai1IfName);
            nmhGetIfMainPortRole (i4NextIndex, &i4PortRole);
            if (i4PortRole == CFA_PORT_ROLE_UPLINK)
            {
                i4Quit = CliPrintf (CliHandle, "%-20s", "Uplink");
            }
            else
            {
                i4Quit = CliPrintf (CliHandle, "%-20s", "Downlink");
            }
            if (i4Quit == CLI_FAILURE)
            {
                u4Continue = CFA_FALSE;
            }
            nmhGetIfMainUfdOperStatus (i4NextIndex, (INT4 *) &i4UfdOperStatus);
            if (i4UfdOperStatus == CFA_IF_UP)
            {
                CliPrintf (CliHandle, "%-30s", "Up");
            }
            else if (i4UfdOperStatus == CFA_IF_DOWN)
            {
                CliPrintf (CliHandle, "%-30s", "Down");
            }
            else if (i4UfdOperStatus == CFA_IF_UFD_ERR_DISABLED)
            {
                CliPrintf (CliHandle, "%-30s", "Error Disabled");
            }
            CliPrintf (CliHandle, "\r\n");
        }
        i4PrevIndex = i4NextIndex;

        if (nmhGetNextIndexIfTable (i4PrevIndex, &i4NextIndex) == SNMP_FAILURE)
        {
            u4Continue = CFA_FALSE;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCliShowUfdDatabase                              */
/*                                                                           */
/*     DESCRIPTION      : Displays UFD global information                    */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler,                           */
/*                        i4NextGroupId - Group Index                        */
/*                        u4command - show command                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
CfaCliShowUfdDatabase (tCliHandle CliHandle, INT4 i4NextGroupId,
                       UINT4 u4Command)
{
    INT4                i4NextIndex = 0;
    INT4                i4PrevGroupId = 0;
    INT4                i4UfdGroupStatus = 0;
    INT4                i4UfdModuleStatus = 0;
    INT4                i4GroupCount = 0;
    INT4                i4GroupDesigUplink = 0;
    INT4                u4Continue1 = CFA_FALSE;
    UINT1               au1GroupName[CFA_UFD_GROUP_MAX_NAME_LEN + 1];
    tSNMP_OCTET_STRING_TYPE Name;

    Name.pu1_OctetList = au1GroupName;
    nmhGetIfUfdModuleStatus (&i4UfdModuleStatus);

    CliPrintf (CliHandle, "\r\nUFD Configurations\r\n");

    CliPrintf (CliHandle, "------------------\r\n");

    if (i4UfdModuleStatus == CFA_UFD_ENABLED)
    {
        CliPrintf (CliHandle, "%-s : Enabled\r\n", "UFD Status");
    }
    else if (i4UfdModuleStatus == CFA_UFD_DISABLED)
    {
        CliPrintf (CliHandle, "%-s : Disabled\r\n", "UFD Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-s : Unknown\r\n", "UFD Status");
    }

    if (i4NextGroupId == 0)
    {
        if (nmhGetFirstIndexIfUfdGroupTable (&i4NextGroupId) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\rNo UFD Group exist\n\n");
            return CLI_SUCCESS;
        }
        u4Continue1 = CFA_TRUE;
    }
    if (nmhValidateIndexInstanceIfUfdGroupTable (i4NextGroupId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\rUFD Group with Group Id %d doesn't exist\n\n",
                   i4NextGroupId);
        return CLI_FAILURE;
    }

    if (u4Command == CLI_CFA_SHOW_UFD)
    {
        while (u4Continue1 == CFA_TRUE)
        {
            i4GroupCount++;
            CliPrintf (CliHandle, "\rGroup Id  : %d  \n", i4NextGroupId);
            i4PrevGroupId = i4NextGroupId;
            if (nmhGetNextIndexIfUfdGroupTable (i4PrevGroupId, &i4NextGroupId)
                == SNMP_FAILURE)
            {
                u4Continue1 = CFA_FALSE;
            }
        }
        CliPrintf (CliHandle, "\r\n");
    }

    if (u4Command == CLI_CFA_SHOW_UFD_BRIEF)
    {
        while (u4Continue1 == CFA_TRUE)
        {
            nmhGetIfUfdGroupStatus (i4NextGroupId, &i4UfdGroupStatus);
            nmhGetIfUfdGroupDesigUplinkPort (i4NextGroupId,
                                             &i4GroupDesigUplink);
            i4GroupCount++;
            CliPrintf (CliHandle, "\rGroup Id  : %d\n", i4NextGroupId);
            MEMSET (au1GroupName, 0, (CFA_UFD_GROUP_MAX_NAME_LEN + 1));
            nmhGetIfUfdGroupName (i4NextGroupId, &Name);
            if (Name.i4_Length == 0)
            {
                CliPrintf (CliHandle, "\rGroup Name: None\n");

            }
            else
            {

                CliPrintf (CliHandle, "\rGroup Name: %s  \n",
                           Name.pu1_OctetList);
            }

            if (i4UfdGroupStatus == CFA_UFD_GROUP_UP)
            {
                CliPrintf (CliHandle, "%-s : UP\r", "Group Status");
            }
            else
            {
                CliPrintf (CliHandle, "%-s : DOWN\r", "Group Status");
            }

            if (nmhGetFirstIndexIfTable (&i4NextIndex) == SNMP_FAILURE)
            {
                return CLI_SUCCESS;
            }

            CliPrintf (CliHandle, "\r\n%-12s%-20s%s\r\n",
                       "Interface", "Role", "UFD Status");
            CliPrintf (CliHandle, "%-12s%-20s%s\r\n",
                       "---------", "----", "----------");
            CfaCliShowUfdGroupDatabase (CliHandle, i4NextIndex, i4NextGroupId);
            CliPrintf (CliHandle, "\r\n");
            i4PrevGroupId = i4NextGroupId;
            if (nmhGetNextIndexIfUfdGroupTable (i4PrevGroupId, &i4NextGroupId)
                == SNMP_FAILURE)
            {
                u4Continue1 = CFA_FALSE;
            }
        }
        CliPrintf (CliHandle, "Total UFD Groups present: %d\n", i4GroupCount);
        CliPrintf (CliHandle, "\r\n");
    }
    if (u4Command == CLI_CFA_SHOW_UFD_GROUP)
    {

        CliPrintf (CliHandle, "\rGroup Id: %d\n", i4NextGroupId);

        MEMSET (au1GroupName, 0, (CFA_UFD_GROUP_MAX_NAME_LEN + 1));
        nmhGetIfUfdGroupName (i4NextGroupId, &Name);
        if (Name.i4_Length == 0)
        {
            CliPrintf (CliHandle, "\rGroup Name: None\n");

        }
        else
        {

            CliPrintf (CliHandle, "\rGroup Name: %s  \n", Name.pu1_OctetList);
        }
        nmhGetIfUfdGroupStatus (i4NextGroupId, &i4UfdGroupStatus);
        if (i4UfdGroupStatus == CFA_UFD_GROUP_UP)
        {
            CliPrintf (CliHandle, "%-s : UP\r", "Group Status");
        }
        else
        {
            CliPrintf (CliHandle, "%-s : DOWN\r", "Group Status");
        }

        if (nmhGetFirstIndexIfTable (&i4NextIndex) == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }

        nmhGetIfUfdGroupDesigUplinkPort (i4NextGroupId, &i4GroupDesigUplink);

        CliPrintf (CliHandle, "\r\n%-12s%-20s%s\r\n",
                   "Interface", "Role", "UFD Status");
        CliPrintf (CliHandle, "%-12s%-20s%s\r\n",
                   "---------", "----", "----------");
        CfaCliShowUfdGroupDatabase (CliHandle, i4NextIndex, i4NextGroupId);
        CliPrintf (CliHandle, "\r\n");
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdSetPortList
 *
 *    Description        : This function is a utility to set the portlist
 *                         to be added from a ufd group.
 *
 *    Input(s)           : Portlist, GroupId , Flag
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if changes succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaCliUfdSetPortList (tCliHandle CliHandle, tPortList IfPortList,
                      UINT4 u4UfdGroupId, UINT4 u4Flag)
{
    tUfdGroupInfo      *pUfdGroupInfo = NULL;
    tPortList          *pUplinkPorts = NULL;
    tPortList          *pDownlinkPorts = NULL;
    tSNMP_OCTET_STRING_TYPE IfUfdGroupUpLinkPorts;
    tSNMP_OCTET_STRING_TYPE IfUfdGroupDownLinkPorts;
    UINT4               u4ErrorCode = 0;
    UINT4               u4Index = 0;
    UINT1               u1BitToAdd = 0;
    UINT1               u1BitToRemove = 0;
    UINT1               u1OriginalBit = 0;
    UINT1               u1ExtraUplink = 0;
    UINT1               u1ExtraDownlink = 0;
    /* Seperate downlink and uplink ports */
    pUplinkPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
    if (pUplinkPorts == NULL)
    {
        return CFA_FAILURE;
    }
    pDownlinkPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
    if (pDownlinkPorts == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
        return CFA_FAILURE;
    }
    MEMSET (pUplinkPorts, 0, sizeof (tPortList));
    MEMSET (pDownlinkPorts, 0, sizeof (tPortList));

    pUfdGroupInfo = CfaIfUfdGroupInfoDbGet (u4UfdGroupId);
    if (pUfdGroupInfo == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdSetPortList - Group  %d does not exist\n",
                  u4UfdGroupId);
        FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
        FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
        return CFA_FAILURE;
    }

    CfaUfdSeperatePortlistOnPortRole (IfPortList, pUplinkPorts, pDownlinkPorts);
    /* Validation in CLI , in case of deletion of ports, check if 
     * ports are actually present */
    if (u4Flag == CFA_UFD_GROUP_PORTS_DELETE)
    {
        for (u4Index = 0; u4Index < BRG_PORT_LIST_SIZE; u4Index++)
        {
            u1BitToRemove = (*pUplinkPorts)[u4Index];
            u1OriginalBit = pUfdGroupInfo->UfdGroupUplinkPortList[u4Index];
            u1ExtraUplink = (UINT1) ((~u1OriginalBit) & (u1BitToRemove));

            u1BitToRemove = (*pDownlinkPorts)[u4Index];
            u1OriginalBit = pUfdGroupInfo->UfdGroupDownlinkPortList[u4Index];
            u1ExtraDownlink = (UINT1) ((~u1OriginalBit) & (u1BitToRemove));

            if ((u1ExtraUplink != 0) || (u1ExtraDownlink != 0))
            {
                CLI_SET_ERR (CLI_CFA_UFD_PORT_NOT_PRESENT_ERR);
                FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
                FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
                return CFA_FAILURE;
            }
        }

    }

    switch (u4Flag)
    {
        case CFA_UFD_GROUP_PORTS_ADD:

            /* Find cumulative portlist, for add ports OR to already existing list */
            for (u4Index = 0; u4Index < BRG_PORT_LIST_SIZE; u4Index++)
            {
                u1BitToAdd = (*pUplinkPorts)[u4Index];
                u1OriginalBit = pUfdGroupInfo->UfdGroupUplinkPortList[u4Index];
                (*pUplinkPorts)[u4Index] =
                    (UINT1) ((u1OriginalBit) | (u1BitToAdd));

                u1BitToAdd = (*pDownlinkPorts)[u4Index];
                u1OriginalBit =
                    pUfdGroupInfo->UfdGroupDownlinkPortList[u4Index];
                (*pDownlinkPorts)[u4Index] =
                    (UINT1) ((u1OriginalBit) | (u1BitToAdd));

            }

            break;
        case CFA_UFD_GROUP_PORTS_DELETE:
            /* For delete ports, perform existing list AND negation of list to remove */

            for (u4Index = 0; u4Index < BRG_PORT_LIST_SIZE; u4Index++)
            {
                u1BitToRemove = (*pUplinkPorts)[u4Index];
                u1OriginalBit = pUfdGroupInfo->UfdGroupUplinkPortList[u4Index];
                (*pUplinkPorts)[u4Index] =
                    (UINT1) ((u1OriginalBit) & (~u1BitToRemove));

                u1BitToRemove = (*pDownlinkPorts)[u4Index];
                u1OriginalBit =
                    pUfdGroupInfo->UfdGroupDownlinkPortList[u4Index];
                (*pDownlinkPorts)[u4Index] =
                    (UINT1) ((u1OriginalBit) & (~u1BitToRemove));
            }

            break;

        default:
            break;

    }
    IfUfdGroupUpLinkPorts.pu1_OctetList = *pUplinkPorts;
    IfUfdGroupUpLinkPorts.i4_Length = sizeof (tPortList);

    IfUfdGroupDownLinkPorts.pu1_OctetList = *pDownlinkPorts;
    IfUfdGroupDownLinkPorts.i4_Length = sizeof (tPortList);
    if (nmhTestv2IfUfdGroupRowStatus (&u4ErrorCode, (INT4) u4UfdGroupId,
                                      NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Group row status validation(NOT_IN_SERVICE) failed\r\n");
        FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
        FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
        return CFA_FAILURE;

    }

    if (nmhSetIfUfdGroupRowStatus ((INT4) u4UfdGroupId, NOT_IN_SERVICE)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set the Row Status as NOT_IN_SERVICE\r\n");
        FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
        FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
        return CFA_FAILURE;

    }

    if (u4Flag == CFA_UFD_GROUP_PORTS_ADD)
    {
        if (nmhTestv2IfUfdGroupDownlinkPorts (&u4ErrorCode, (INT4) u4UfdGroupId,
                                              &IfUfdGroupDownLinkPorts) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r%% Validation of downlink ports to be set failed\r\n");
            FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
            FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
            return CFA_FAILURE;

        }
        if (nmhSetIfUfdGroupDownlinkPorts ((INT4) u4UfdGroupId,
                                           &IfUfdGroupDownLinkPorts) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Setting of downlink ports failed\r\n");
            FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
            FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
            return CFA_FAILURE;

        }

        if (nmhTestv2IfUfdGroupUplinkPorts (&u4ErrorCode, (INT4) u4UfdGroupId,
                                            &IfUfdGroupUpLinkPorts) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r%% Validation of uplink ports to be set failed\r\n");
            FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
            FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
            return CFA_FAILURE;

        }

        if (nmhSetIfUfdGroupUplinkPorts ((INT4) u4UfdGroupId,
                                         &IfUfdGroupUpLinkPorts) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Setting of uplink ports failed\r\n");
            FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
            FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
            return CFA_FAILURE;

        }
    }
    else if (u4Flag == CFA_UFD_GROUP_PORTS_DELETE)
    {

        if (nmhTestv2IfUfdGroupUplinkPorts (&u4ErrorCode, (INT4) u4UfdGroupId,
                                            &IfUfdGroupUpLinkPorts) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r%% Validation of uplink ports to be set failed\r\n");
            FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
            FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
            return CFA_FAILURE;

        }

        if (nmhSetIfUfdGroupUplinkPorts ((INT4) u4UfdGroupId,
                                         &IfUfdGroupUpLinkPorts) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Setting of uplink ports failed\r\n");
            FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
            FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
            return CFA_FAILURE;

        }
        if (nmhTestv2IfUfdGroupDownlinkPorts (&u4ErrorCode, (INT4) u4UfdGroupId,
                                              &IfUfdGroupDownLinkPorts) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r%% Validation of downlink ports to be set failed\r\n");
            FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
            FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
            return CFA_FAILURE;

        }
        if (nmhSetIfUfdGroupDownlinkPorts ((INT4) u4UfdGroupId,
                                           &IfUfdGroupDownLinkPorts) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Setting of downlink ports failed\r\n");
            FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
            FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
            return CFA_FAILURE;

        }

    }
    if (nmhTestv2IfUfdGroupRowStatus (&u4ErrorCode, (INT4) u4UfdGroupId,
                                      ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Group row status validation(ACTIVE) failed\r\n");
        FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
        FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
        return CFA_FAILURE;
    }
    /* Set ACTIVE after setting the designated uplink port */
    if (nmhSetIfUfdGroupRowStatus ((INT4) u4UfdGroupId, ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set the Row Status as ACTIVE\r\n");
        FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
        FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
        return CFA_FAILURE;
    }

    FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
    FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaUfdShowRunningConfig                            */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the  UFD running           */
/*                        Configuration                                      */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
CfaUfdShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    INT4                i4NextIndex = 0;
    INT4                i4PrevGroupId = 0;
    INT4                i4NextGroupId = 0;
    INT4                i4UfdModuleStatus = 0;
    INT4                i4UfdSysControl = 0;
    INT4                u4Continue1 = CFA_TRUE;
    INT4                i4GroupDesigUplink = 0;
    UINT1               u1GroupExist = CFA_TRUE;
    UINT1               au1GroupName[CFA_UFD_GROUP_MAX_NAME_LEN];
    tSNMP_OCTET_STRING_TYPE Name;

    tSNMP_OCTET_STRING_TYPE IfUfdGroupUpLinkPorts;
    tSNMP_OCTET_STRING_TYPE IfUfdGroupDownLinkPorts;
    tPortList          *pUpLinkPortList = NULL;
    tPortList          *pDownLinkPortList = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    CliRegisterLock (CliHandle, CfaLock, CfaUnlock);
    CFA_LOCK ();

    MEMSET (&Name, 0, sizeof (Name));
    Name.pu1_OctetList = au1GroupName;
    UNUSED_PARAM (u4Module);
    nmhGetIfUfdSystemControl (&i4UfdSysControl);
    if (i4UfdSysControl == CFA_UFD_START)
    {
        CliPrintf (CliHandle, "!\r\n");
        CliPrintf (CliHandle, "no shutdown ufd\r\n");
    }
    else if (i4UfdSysControl == CFA_UFD_SHUTDOWN)
    {
        CFA_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }

    nmhGetIfUfdModuleStatus (&i4UfdModuleStatus);
    if (i4UfdModuleStatus == CFA_UFD_ENABLED)
    {
        CliPrintf (CliHandle, "set ufd enable\r\n");
    }
    else if (i4UfdModuleStatus == CFA_UFD_DISABLED)
    {
        CFA_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }
    if (nmhGetFirstIndexIfUfdGroupTable (&i4NextGroupId) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }
    while (u4Continue1 == CFA_TRUE)
    {
        MEMSET (au1GroupName, 0, CFA_UFD_GROUP_MAX_NAME_LEN);
        nmhGetIfUfdGroupName (i4NextGroupId, &Name);
        if (Name.i4_Length != 0)
        {
            CliPrintf (CliHandle, "ufd group %d groupname %s\r\n",
                       i4NextGroupId, Name.pu1_OctetList);
        }
        else
        {
            CliPrintf (CliHandle, "ufd group %d\r\n", i4NextGroupId);
        }
        if (nmhGetFirstIndexIfTable (&i4NextIndex) == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_SUCCESS;
        }
        u1GroupExist = CFA_TRUE;

        pUpLinkPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (pUpLinkPortList == NULL)
        {
            CliPrintf (CliHandle,
                       "\r%% Error in Allocating memory for UpLink bitlist \r\n");
            return CLI_FAILURE;
        }
        pDownLinkPortList =
            (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
        if (pDownLinkPortList == NULL)
        {
            CliPrintf (CliHandle,
                       "\r%% Error in Allocating memory for DownLink bitlist \r\n");
            FsUtilReleaseBitList ((UINT1 *) pUpLinkPortList);
            return CLI_FAILURE;
        }

        MEMSET (*pDownLinkPortList, 0, sizeof (tPortList));
        MEMSET (*pUpLinkPortList, 0, sizeof (tPortList));
        IfUfdGroupUpLinkPorts.pu1_OctetList = *pUpLinkPortList;
        IfUfdGroupUpLinkPorts.i4_Length = sizeof (tPortList);

        IfUfdGroupDownLinkPorts.pu1_OctetList = *pDownLinkPortList;
        IfUfdGroupDownLinkPorts.i4_Length = sizeof (tPortList);

        nmhGetIfUfdGroupDownlinkPorts (i4NextGroupId, &IfUfdGroupDownLinkPorts);

        nmhGetIfUfdGroupUplinkPorts (i4NextGroupId, &IfUfdGroupUpLinkPorts);

        OSIX_ADD_PORT_LIST (IfUfdGroupUpLinkPorts.pu1_OctetList,
                            IfUfdGroupDownLinkPorts.pu1_OctetList,
                            sizeof (tPortList), sizeof (tPortList));
        CliConfOctetToIfName (CliHandle, "ports add ", NULL,
                              &IfUfdGroupUpLinkPorts, &u4PagingStatus);

        CliPrintf (CliHandle, "\r\n");
        FsUtilReleaseBitList ((UINT1 *) pUpLinkPortList);
        FsUtilReleaseBitList ((UINT1 *) pDownLinkPortList);
        nmhGetIfUfdGroupDesigUplinkPort (i4NextGroupId, &i4GroupDesigUplink);
        i4PrevGroupId = i4NextGroupId;
        if (nmhGetNextIndexIfUfdGroupTable (i4PrevGroupId, &i4NextGroupId)
            == SNMP_FAILURE)
        {
            u4Continue1 = CFA_FALSE;
        }
        CliPrintf (CliHandle, "! \r\n");
    }
    if (u1GroupExist == CFA_TRUE)
    {
        CliPrintf (CliHandle, "! \r\n");
    }
    CFA_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : CfaCliSetShModuleStatus
 *
 *     DESCRIPTION      : This function will enable/disable SH module
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Status   - Split-horizon System Control Status
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT4
CfaCliSetShModuleStatus (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (CFA_SH_SYSTEM_CONTROL != CFA_SH_START)
    {
        CliPrintf (CliHandle,
                   "\r%% SH is not started on the system; SH Can't be enabled/ disabled\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2IfSplitHorizonModStatus (&u4ErrorCode, i4Status) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Split Horizon module status validation failed\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetIfSplitHorizonModStatus (i4Status) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set Split Horizon module status\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : CfaCliSetShSystemCtrl
 *
 *     DESCRIPTION      : This function will start/shut Ufd module
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Status   - Split-horizon System Control Status
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT4
CfaCliSetShSystemCtrl (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IfSplitHorizonSysControl (&u4ErrorCode, i4Status) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Split Horizon system status validation failed\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetIfSplitHorizonSysControl (i4Status) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set Split Horizon system status\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaShShowRunningConfig                             */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the  SH  running           */
/*                        Configuration                                      */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4Module - Module Name                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
CfaShShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    INT4                i4ShModuleStatus = 0;
    INT4                i4ShSysControl = 0;
    UNUSED_PARAM (u4Module);

    CliRegisterLock (CliHandle, CfaLock, CfaUnlock);
    CFA_LOCK ();

    nmhGetIfSplitHorizonSysControl (&i4ShSysControl);
    nmhGetIfSplitHorizonModStatus (&i4ShModuleStatus);

    CliPrintf (CliHandle, "! \r\n");
    if (i4ShSysControl == CFA_SH_START)
    {
        CliPrintf (CliHandle, "no shut split-horizon\r\n");
    }
    else if (i4ShSysControl == CFA_SH_SHUTDOWN)
    {
        CFA_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }

    if (i4ShModuleStatus == CFA_SH_ENABLED)
    {
        CliPrintf (CliHandle, "set split-horizon enable\r\n");
    }
    CliPrintf (CliHandle, "! \r\n");
    CFA_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssShowSplitHorizon                                */
/*                                                                           */
/*     DESCRIPTION      :This function displays the configured Split horizn  */
/*                       rules for all the ports                             */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        i4IfIndex - Port Index                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/
VOID
IssShowSplitHorizon (tCliHandle CliHandle, INT4 i4IfIndex)
{
    INT4                i4ShModuleStatus = 0;
    INT4                i4RetVal = 0;

    nmhGetIfSplitHorizonModStatus (&i4ShModuleStatus);

    if (i4ShModuleStatus == CFA_SH_ENABLED)
    {
        CliPrintf (CliHandle, "%-s : Enabled\r\n", "SH Status");
    }
    else if (i4ShModuleStatus == CFA_SH_DISABLED)
    {
        CliPrintf (CliHandle, "%-s : Disabled\r\n", "SH Status");
        return;
    }
    if (i4IfIndex != 0)
    {
        i4RetVal = IssPICliShowPortIsolation (CliHandle, i4IfIndex);
    }
    else
    {
        i4RetVal = IssPICliShowPortIsolation (CliHandle, 0);
    }
    UNUSED_PARAM (i4RetVal);
    return;
}

#endif /*_CFACLI_C */
