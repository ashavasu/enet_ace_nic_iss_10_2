/***************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: stdethlw.c,v 1.31 2016/06/18 11:27:14 siva Exp $
 *
 * Description: This file contains the low level routines for the
 *                          etherMIB (RFC 2665).
 ****************************************************************/

#include "cfainc.h"

#ifdef SNMP_2_WANTED
extern UINT4        Dot3PauseAdminMode[11];
#endif

/* LOW LEVEL Routines for Table : Dot3StatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot3StatsTable
 Input       :  The Indices
                Dot3StatsIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhValidateIndexInstanceDot3StatsTable (INT4 i4dot3StatsIndex)
{
    UINT2               u2IfIndex;
    UINT1               u1IfType;

    u2IfIndex = (UINT2) i4dot3StatsIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0))
    {
        return (SNMP_FAILURE);
    }

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot3StatsAlignmentErrors
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3StatsAlignmentErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3StatsAlignmentErrors (INT4 i4dot3StatsIndex, UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

    *pElement = 0;
#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_STAT_DOT3_ALIGNMENT_ERRORS;
    if (CfaFsEtherHwGetStats ((UINT4) i4dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    *pElement = EthStats.u4AlignErrors;
#else
    UNUSED_PARAM (i4dot3StatsIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetDot3StatsFCSErrors
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3StatsFCSErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3StatsFCSErrors (INT4 i4dot3StatsIndex, UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

    *pElement = 0;
#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_STAT_DOT3_FCS_ERRORS;
    if (CfaFsEtherHwGetStats ((UINT4) i4dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    *pElement = EthStats.u4FCSErrors;
#else
    UNUSED_PARAM (i4dot3StatsIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetDot3StatsSingleCollisionFrames
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3StatsSingleCollisionFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3StatsSingleCollisionFrames (INT4 i4dot3StatsIndex, UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;
#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

    *pElement = 0;
#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_STAT_DOT3_SINGLE_COLLISION_ERRORS;
    if (CfaFsEtherHwGetStats ((UINT4) i4dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    *pElement = EthStats.u4SingleColFrames;
#else
    UNUSED_PARAM (i4dot3StatsIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetDot3StatsMultipleCollisionFrames
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3StatsMultipleCollisionFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3StatsMultipleCollisionFrames (INT4 i4dot3StatsIndex, UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

    *pElement = 0;
#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_STAT_DOT3_MULTIPLE_COLLISION_ERRORS;
    if (CfaFsEtherHwGetStats ((UINT4) i4dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    *pElement = EthStats.u4MultipleColFrames;
#else
    UNUSED_PARAM (i4dot3StatsIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetDot3StatsSQETestErrors
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3StatsSQETestErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3StatsSQETestErrors (INT4 i4dot3StatsIndex, UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

    *pElement = 0;
#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_STAT_DOT3_SQET_TEST_ERRORS;
    if (CfaFsEtherHwGetStats ((UINT4) i4dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    *pElement = EthStats.u4SQETestErrors;
#else
    UNUSED_PARAM (i4dot3StatsIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetDot3StatsDeferredTransmissions
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3StatsDeferredTransmissions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3StatsDeferredTransmissions (INT4 i4dot3StatsIndex, UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

    *pElement = 0;
#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_STAT_DOT3_DEFERRED_TRANSMISSIONS;
    if (CfaFsEtherHwGetStats ((UINT4) i4dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    *pElement = EthStats.u4DeferredTrans;
#else
    UNUSED_PARAM (i4dot3StatsIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetDot3StatsLateCollisions
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3StatsLateCollisions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3StatsLateCollisions (INT4 i4dot3StatsIndex, UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

    *pElement = 0;
#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_STAT_DOT3_LATE_COLLISIONS;
    if (CfaFsEtherHwGetStats ((UINT4) i4dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    *pElement = EthStats.u4LateCollisions;
#else
    UNUSED_PARAM (i4dot3StatsIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetDot3StatsExcessiveCollisions
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3StatsExcessiveCollisions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3StatsExcessiveCollisions (INT4 i4dot3StatsIndex, UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

    *pElement = 0;
#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_STAT_DOT3_EXCESSIVE_COLLISIONS;
    if (CfaFsEtherHwGetStats ((UINT4) i4dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    *pElement = EthStats.u4ExcessiveCols;
#else
    UNUSED_PARAM (i4dot3StatsIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetDot3StatsInternalMacTransmitErrors
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3StatsInternalMacTransmitErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3StatsInternalMacTransmitErrors (INT4 i4dot3StatsIndex,
                                          UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

    *pElement = 0;
#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_STAT_DOT3_INTERNAL_MAC_TX_ERRORS;
    if (CfaFsEtherHwGetStats ((UINT4) i4dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    *pElement = EthStats.u4IntMacTxErrors;
#else
    UNUSED_PARAM (i4dot3StatsIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetDot3StatsCarrierSenseErrors
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3StatsCarrierSenseErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3StatsCarrierSenseErrors (INT4 i4dot3StatsIndex, UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

    *pElement = 0;
#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_STAT_DOT3_CARRIER_SENSE_ERRORS;
    if (CfaFsEtherHwGetStats ((UINT4) i4dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    *pElement = EthStats.u4CarrierSenseErrors;
#else
    UNUSED_PARAM (i4dot3StatsIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetDot3StatsFrameTooLongs
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3StatsFrameTooLongs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3StatsFrameTooLongs (INT4 i4dot3StatsIndex, UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

    *pElement = 0;
#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
     EthStats.u4ReqType = NP_STAT_DOT3_FRAME_TOO_LONGS;
    if (CfaFsEtherHwGetStats ((UINT4) i4dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    *pElement = EthStats.u4FrameTooLongs;
#else
    UNUSED_PARAM (i4dot3StatsIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetDot3StatsInternalMacReceiveErrors
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3StatsInternalMacReceiveErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3StatsInternalMacReceiveErrors (INT4 i4dot3StatsIndex, UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

    *pElement = 0;
#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_STAT_DOT3_INTERNAL_MAC_RX_ERRORS;
    if (CfaFsEtherHwGetStats ((UINT4) i4dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    *pElement = EthStats.u4IntMacRxErrors;
#else
    UNUSED_PARAM (i4dot3StatsIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetDot3StatsEtherChipSet
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3StatsEtherChipSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE

 NOTE        :  THIS ELEMENT IS DEPRECATED IN RFC 2665.
****************************************************************************/

INT4
nmhGetDot3StatsEtherChipSet (INT4 i4dot3StatsIndex, tSNMP_OID_TYPE * pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (i4dot3StatsIndex);
    if (pElement != NULL)
    {
        /* For successful walk fill dummy value to this Deprecated mib object 
         * This fix is temporary (Rfc 7611). Midgen tool should not 
         * generate low levels for deprecated object. 
         */

        pElement->u4_Length = 1;
        pElement->pu4_OidList[0] = 1;
    }
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetDot3StatsSymbolErrors
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3StatsSymbolErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3StatsSymbolErrors (INT4 i4dot3StatsIndex, UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

    *pElement = 0;
#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_STAT_DOT3_SYMBOL_ERRORS;
    if (CfaFsEtherHwGetStats ((UINT4) i4dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    *pElement = EthStats.u4SymbolErrors;
#else
    UNUSED_PARAM (i4dot3StatsIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetDot3StatsDuplexStatus
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3StatsDuplexStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3StatsDuplexStatus (INT4 i4dot3StatsIndex, INT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

    *pElement = 1;
#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_STAT_PORT_DUPLEX_STATUS;
    if (CfaFsEtherHwGetStats ((UINT4) i4dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    *pElement = (INT4) EthStats.u4DuplexStatus;
#else
    UNUSED_PARAM (i4dot3StatsIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 *  Function    :  nmhGetDot3StatsRateControlAbility
 *  Input       :  The Indices
 *                 Dot3StatsIndex
 *                 The Object
 *                 retValDot3StatsRateControlAbility
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/

INT1
nmhGetDot3StatsRateControlAbility (INT4 i4Dot3StatsIndex, INT4 *pElement)
{
#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

    *pElement = CFA_FALSE;
#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4Dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_ETH_STATS_BRATE_CONTROLABILITY;
    if (CfaFsEtherHwGetStats ((UINT4) i4Dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pElement = (INT1) EthStats.bRateControlAbility;
#else
    UNUSED_PARAM (i4Dot3StatsIndex);
#endif
    return SNMP_SUCCESS;
}

/*RateControlStatus coding */
/****************************************************************************
 *  Function    :  nmhGetDot3StatsRateControlStatus
 *  Input       :  The Indices
 *                 Dot3StatsIndex
 *                 The Object
 *                 retValDot3StatsRateControlStatus
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/

INT4
nmhGetDot3StatsRateControlStatus (INT4 i4Dot3StatsIndex, INT4 *pElement)
{
#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

    *pElement = CFA_ETHER_RATE_CONTROL_UNKNOWN;
#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4Dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_ETH_STATS_RATE_CONTROL_STATUS;
    if (CfaFsEtherHwGetStats ((UINT4) i4Dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pElement = (INT4) EthStats.u2RateControlStatus;
#else
    UNUSED_PARAM (i4Dot3StatsIndex);
#endif
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot3CollTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot3CollTable
 Input       :  The Indices
                IfIndex
                Dot3CollCount
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT4
nmhValidateIndexInstanceDot3CollTable (INT4 i4IfIndex, INT4 i4dot3CollCount)
{
    UINT2               u2IfIndex;
    UINT1               u1IfType;

    u2IfIndex = (UINT2) i4IfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0))
    {
        return (SNMP_FAILURE);
    }

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET))
    {
        if ((i4dot3CollCount >= MIN_COLLISION_COUNT) &&
            (i4dot3CollCount <= MAX_COLLISION_COUNT))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot3CollFrequencies
 Input       :  The Indices
                IfIndex
                Dot3CollCount

                The Object
                retValDot3CollFrequencies
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3CollFrequencies (INT4 i4IfIndex,
                           INT4 i4dot3CollCount, UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    *pElement = 0;
#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4IfIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

    if (CfaFsEtherHwGetCollFreq ((UINT4) i4IfIndex, i4dot3CollCount, pElement)
        != FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4dot3CollCount);
    UNUSED_PARAM (i4IfIndex);
#endif
    return (i4RetVal);
}

/* LOW LEVEL Routines for Table : Dot3ControlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot3ControlTable
 Input       :  The Indices
                Dot3StatsIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT4
nmhValidateIndexInstanceDot3ControlTable (INT4 i4dot3StatsIndex)
{
    UINT2               u2IfIndex;
    UINT1               u1IfType;

    u2IfIndex = (UINT2) i4dot3StatsIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0))
    {
        return (SNMP_FAILURE);
    }

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot3ControlFunctionsSupported
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3ControlFunctionsSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3ControlFunctionsSupported (INT4 i4dot3StatsIndex,
                                     tSNMP_OCTET_STRING_TYPE * pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4Result = 0;

    pElement->i4_Length = sizeof (UINT1);

#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE)
    {
        pElement->pu1_OctetList[0] = (UINT1) i4Result;
        return SNMP_SUCCESS;
    }

    if (CfaFsEtherHwGetCntrl ((UINT4) i4dot3StatsIndex, &i4Result,
                              ETH_CNTRL_FNS_SUPPORTED) != FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4dot3StatsIndex);
#endif
    pElement->pu1_OctetList[0] = (UINT1) i4Result;
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetDot3ControlInUnknownOpcodes
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3ControlInUnknownOpcodes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3ControlInUnknownOpcodes (INT4 i4dot3StatsIndex, UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    *pElement = 0;
#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

    if (CfaFsEtherHwGetCntrl ((UINT4) i4dot3StatsIndex, (INT4 *) pElement,
                              ETH_CNTRL_IN_UNK_OPCODES) != FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4dot3StatsIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 *  Function    :  nmhGetDot3HCControlInUnknownOpcodes
 *  Input       :  The Indices
 *                 Dot3StatsIndex
 *                 The Object
 *                 retValDot3HCControlInUnknownOpcodes
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT4
nmhGetDot3HCControlInUnknownOpcodes (INT4 i4Dot3StatsIndex,
                                     tSNMP_COUNTER64_TYPE * pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;
#ifdef NPAPI_WANTED
    INT4                i4Val = 0;
    if (CfaIsPhysicalInterface ((UINT4) i4Dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    if (CfaFsEtherHwGetCntrl ((UINT4) i4Dot3StatsIndex, &i4Val,
                              ETH_CNTRL_HC_IN_UNK_OPCODES) != FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    else
    {
        pElement->lsn = i4Val;
    }
#else
    UNUSED_PARAM (i4Dot3StatsIndex);
    UNUSED_PARAM (pElement);
#endif
    return (i4RetVal);

}

/* Dot3HCStats Table Objects Low level Routines*/
/****************************************************************************
 *  Function    :  nmhValidateIndexInstanceDot3HCStatsTable
 *  Input       :  The Indices
 *                 Dot3StatsIndex
 *  Output      :  The Routines Validates the Given Indices.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT4
nmhValidateIndexInstanceDot3HCStatsTable (INT4 i4Dot3StatsIndex)
{
    UINT2               u2IfIndex;
    UINT1               u1IfType;

    u2IfIndex = (UINT2) i4Dot3StatsIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0))
    {
        return (SNMP_FAILURE);
    }

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 *  Function    :  nmhGetFirstIndexDot3HCStatsTable
 *  Input       :  The Indices
 *                 Dot3StatsIndex
 *  Output      :  The Get First Routines gets the Lexicographicaly
 *                 First Entry from the Table.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
/* GET_FIRST Routine. */
INT4
nmhGetFirstIndexDot3HCStatsTable (INT4 *pi4Dot3StatsIndex)
{
    UINT2               u2Index = 1;
    UINT1               u1IfType = CFA_NONE;

    /* Get the First Used Index */
    CFA_CDB_SCAN (u2Index, BRG_MAX_PHY_PLUS_LOG_PORTS, CFA_ENET)
    {
        CfaGetIfType ((UINT4) u2Index, &u1IfType);

        if ((CFA_IF_ENTRY_USED (u2Index)) && (u1IfType == CFA_ENET))
        {
            *pi4Dot3StatsIndex = (INT4) u2Index;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 *  Function    :  nmhGetNextIndexDot3HCStatsTable
 *  Input       :  The Indices
 *                 Dot3StatsIndex
 *                 nextDot3StatsIndex
 *  Output      :  The Get Next function gets the Next Index for
 *                 the Index Value given in the Index Values. The
 *                 Indices are stored in the next_varname variables.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
/* GET_NEXT Routine.  */
INT4
nmhGetNextIndexDot3HCStatsTable (INT4 i4Dot3StatsIndex,
                                 INT4 *pi4NextDot3StatsIndex)
{
    UINT4               u4IfIndex;
    UINT4               u4Counter;
    UINT1               u1IfType;

    if (i4Dot3StatsIndex < 0)
    {
        return SNMP_FAILURE;
    }

    u4IfIndex = (UINT4) i4Dot3StatsIndex;

    /* Next Index should always return the next used Index */
    u4Counter = u4IfIndex + 1;
    CFA_CDB_SCAN (u4Counter, BRG_MAX_PHY_PLUS_LOG_PORTS, CFA_ENET)
    {
        CfaGetIfType (u4Counter, &u1IfType);

        if ((CFA_IF_ENTRY_USED ((UINT2) u4Counter)) && (u1IfType == CFA_ENET))
        {
            *pi4NextDot3StatsIndex = (INT4) u4Counter;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 *  Function    :  nmhGetDot3HCStatsAlignmentErrors
 *  Input       :  The Indices
 *                 Dot3StatsIndex
 *                 The Object
 *                 retValDot3HCStatsAlignmentErrors
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *            store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT4
nmhGetDot3HCStatsAlignmentErrors (INT4 i4Dot3StatsIndex,
                                  tSNMP_COUNTER64_TYPE * pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;
#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4Dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_STAT_DOT3_HC_ALIGNMENT_ERRORS;
    if (CfaFsEtherHwGetStats ((UINT4) i4Dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    pElement->msn = EthStats.EthHCStats.u8HCAlignErrors.u4Hi;
    pElement->lsn = EthStats.EthHCStats.u8HCAlignErrors.u4Lo;
#else
    UNUSED_PARAM (i4Dot3StatsIndex);
    UNUSED_PARAM (pElement);
#endif
    return (i4RetVal);
}

/****************************************************************************
 *  Function    :  nmhGetDot3HCStatsFCSErrors
 *  Input       :  The Indices
 *                 Dot3StatsIndex
 *                 The Object
 *                 retValDot3HCStatsFCSErrors
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT4
nmhGetDot3HCStatsFCSErrors (INT4 i4Dot3StatsIndex,
                            tSNMP_COUNTER64_TYPE * pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;
#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4Dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_STAT_DOT3_HC_FCS_ERRORS;
    if (CfaFsEtherHwGetStats ((UINT4) i4Dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    pElement->msn = EthStats.EthHCStats.u8HCFCSErrors.u4Hi;
    pElement->lsn = EthStats.EthHCStats.u8HCFCSErrors.u4Lo;
#else
    UNUSED_PARAM (i4Dot3StatsIndex);
    UNUSED_PARAM (pElement);
#endif
    return (i4RetVal);
}

/****************************************************************************
 *  Function    :  nmhGetDot3HCStatsInternalMacTransmitErrors
 *  Input       :  The Indices
 *                 Dot3StatsIndex
 *                 The Object
 *                 retValDot3HCStatsInternalMacTransmitErrors
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT4
nmhGetDot3HCStatsInternalMacTransmitErrors (INT4 i4Dot3StatsIndex,
                                            tSNMP_COUNTER64_TYPE * pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;
#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4Dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_STAT_DOT3_HC_INTERNAL_MAC_TX_ERRORS;
    if (CfaFsEtherHwGetStats ((UINT4) i4Dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    pElement->msn = EthStats.EthHCStats.u8HCIntMacTxErrors.u4Hi;
    pElement->lsn = EthStats.EthHCStats.u8HCIntMacTxErrors.u4Lo;
#else
    UNUSED_PARAM (i4Dot3StatsIndex);
    UNUSED_PARAM (pElement);
#endif
    return (i4RetVal);
}

/****************************************************************************
 *  Function    :  nmhGetDot3HCStatsFrameTooLongs
 *  Input       :  The Indices
 *                 Dot3StatsIndex
 *                 The Object
 *                 retValDot3HCStatsFrameTooLongs
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT4
nmhGetDot3HCStatsFrameTooLongs (INT4 i4Dot3StatsIndex,
                                tSNMP_COUNTER64_TYPE * pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;
#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4Dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
     EthStats.u4ReqType = NP_STAT_DOT3_HC_FRAME_TOO_LONGS;
    if (CfaFsEtherHwGetStats ((UINT4) i4Dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    pElement->msn = EthStats.EthHCStats.u8HCFrameTooLongs.u4Hi;
    pElement->lsn = EthStats.EthHCStats.u8HCFrameTooLongs.u4Lo;
#else
    UNUSED_PARAM (i4Dot3StatsIndex);
    UNUSED_PARAM (pElement);
#endif
    return (i4RetVal);
}

/****************************************************************************
 *  Function    :  nmhGetDot3HCStatsInternalMacReceiveErrors
 *  Input       :  The Indices
 *                 Dot3StatsIndex
 *                 The Object
 *                 retValDot3HCStatsInternalMacReceiveErrors
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/

INT4
nmhGetDot3HCStatsInternalMacReceiveErrors (INT4 i4Dot3StatsIndex,
                                           tSNMP_COUNTER64_TYPE * pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;
#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    if (CfaIsPhysicalInterface ((UINT4) i4Dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_STAT_DOT3_HC_INTERNAL_MAC_RX_ERRORS;
    if (CfaFsEtherHwGetStats ((UINT4) i4Dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    pElement->msn = EthStats.EthHCStats.u8HCIntMacRxErrors.u4Hi;
    pElement->lsn = EthStats.EthHCStats.u8HCIntMacRxErrors.u4Lo;
#else
    UNUSED_PARAM (pElement);
    UNUSED_PARAM (i4Dot3StatsIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 *  Function    :  nmhGetDot3HCStatsSymbolErrors
 *  Input       :  The Indices
 *                 Dot3StatsIndex
 *                 The Object
 *                 retValDot3HCStatsSymbolErrors
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/

INT4
nmhGetDot3HCStatsSymbolErrors (INT4 i4Dot3StatsIndex,
                               tSNMP_COUNTER64_TYPE * pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;
#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    MEMSET (&EthStats.EthHCStats, 0, sizeof (tEthHCStats));
    if (CfaIsPhysicalInterface ((UINT4) i4Dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }
    EthStats.u4ReqType = NP_STAT_DOT3_HC_SYMBOL_ERRORS;
    if (CfaFsEtherHwGetStats ((UINT4) i4Dot3StatsIndex, &EthStats) !=
        FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
    pElement->msn = EthStats.EthHCStats.u8HCSymbolErrors.u4Hi;
    pElement->lsn = EthStats.EthHCStats.u8HCSymbolErrors.u4Lo;
#else
    UNUSED_PARAM (i4Dot3StatsIndex);
    UNUSED_PARAM (pElement);
#endif
    return (i4RetVal);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot3PauseAdminMode
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                testValDot3PauseAdminMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhTestv2Dot3PauseAdminMode (UINT4 *pu4Error,
                             INT4 i4dot3StatsIndex, INT4 i4Element)
{

    UINT1   u1BrgPortType = 0;

    CfaGetIfBrgPortType ((UINT4)i4dot3StatsIndex, &u1BrgPortType);
    if(u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        /* Flow Control is not supported for SChannel Interface */
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_SBP_ERR);
        return SNMP_FAILURE;
    }

#ifdef MRVLLS
    if ((i4Element == PAUSE_DISABLED) || (i4Element == PAUSE_ENABLED_BOTH))
    {
        return SNMP_SUCCESS;
    }
#else
    if ((i4Element >= PAUSE_DISABLED) && (i4Element <= PAUSE_ENABLED_BOTH))
    {
        return SNMP_SUCCESS;
    }
#endif

    *pu4Error = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot3PauseAdminMode
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                setValDot3PauseAdminMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhSetDot3PauseAdminMode (INT4 i4dot3StatsIndex, INT4 i4Element)
{
    UINT1               u1ExistingAdminMode;
    INT4                i4RetVal = SNMP_SUCCESS;
    UINT4               u4SeqNum = 0;
    UINT2               au2ConfPorts[L2IWF_MAX_PORTS_PER_CONTEXT];
    UINT2               u2NumPorts = 0;
    UINT2               u2Index = 0;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT1               u1IfType;

    CfaGetIfType ((UINT4) i4dot3StatsIndex, &u1IfType);

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    MEMSET (au2ConfPorts, 0, sizeof (UINT2));

    /* Get the existing pause admin mode. */
    u1ExistingAdminMode =
        CFA_CDB_IF_PAUSE_ADMIN_MODE ((UINT4) i4dot3StatsIndex);

    /* Change the pause admin mode. */
    CFA_CDB_IF_PAUSE_ADMIN_MODE ((UINT4) i4dot3StatsIndex) = (UINT1) i4Element;

    if (u1IfType == CFA_ENET)
    {
        LaUpdatePauseAdminMode ((UINT2) i4dot3StatsIndex, (UINT4) i4Element);
    }

    if (u1IfType == CFA_LAGG)
    {
        LaUpdateAggPauseAdminMode ((UINT2) i4dot3StatsIndex, (UINT4) i4Element);
    }

    if ((i4dot3StatsIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES)
        && (i4dot3StatsIndex <= (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG)))
    {
        L2IwfGetConfiguredPortsForPortChannel ((UINT2) i4dot3StatsIndex,
                                               au2ConfPorts, &u2NumPorts);

        for (u2Index = 0; u2Index < u2NumPorts; u2Index++)
        {
            /* Change the pause admin mode. */
            CFA_CDB_IF_PAUSE_ADMIN_MODE (au2ConfPorts[u2Index]) =
                (UINT1) i4Element;

            RM_GET_SEQ_NUM (&u4SeqNum);
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot3PauseAdminMode,
                                  u4SeqNum, FALSE, NULL, NULL, 1, SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              au2ConfPorts[u2Index], i4Element));
        }
    }

#ifdef NPAPI_WANTED
    if ((CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE) &&
        (u1IfType != CFA_LAGG))
    {
        /* Since hardware set failed, revert back the set pause admin mode
         * to the older value. */
        CFA_CDB_IF_PAUSE_ADMIN_MODE (i4dot3StatsIndex) = u1ExistingAdminMode;
        return SNMP_FAILURE;
    }

    if (CfaFsEtherHwSetPauseAdminMode ((UINT4) i4dot3StatsIndex,
                                       &i4Element) != FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
        /* Since hardware set failed, revert back the set pause admin mode
         * to the older value. */
        CFA_CDB_IF_PAUSE_ADMIN_MODE (i4dot3StatsIndex) = u1ExistingAdminMode;
    }
#else
    UNUSED_PARAM (u1ExistingAdminMode);
#endif

    return (i4RetVal);

}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot3PauseTable
 Input       :  The Indices
                Dot3StatsIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhValidateIndexInstanceDot3PauseTable (INT4 i4dot3StatsIndex)
{
    UINT2               u2IfIndex;
    UINT1               u1IfType;

    u2IfIndex = (UINT2) i4dot3StatsIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0))
    {
        return (SNMP_FAILURE);
    }

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && ((u1IfType == CFA_ENET) ||
                                            (u1IfType == CFA_LAGG)))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot3PauseAdminMode
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3PauseAdminMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3PauseAdminMode (INT4 i4dot3StatsIndex, INT4 *pElement)
{

    *pElement = CFA_CDB_IF_PAUSE_ADMIN_MODE ((UINT4) i4dot3StatsIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3PauseOperMode
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3PauseOperMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3PauseOperMode (INT4 i4dot3StatsIndex, INT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    *pElement = ETH_PAUSE_DISABLED;

#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

    if (CfaFsEtherHwGetPauseMode ((UINT4) i4dot3StatsIndex, pElement,
                                  ETH_PAUSE_OPER_MODE) != FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4dot3StatsIndex);
    UNUSED_PARAM (pElement);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetDot3InPauseFrames
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3InPauseFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3InPauseFrames (INT4 i4dot3StatsIndex, UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    *pElement = 0;
#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

    if (CfaFsEtherHwGetPauseFrames ((UINT4) i4dot3StatsIndex, pElement,
                                    ETH_PAUSE_IN_FRAMES) != FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4dot3StatsIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetDot3OutPauseFrames
 Input       :  The Indices
                Dot3StatsIndex

                The Object
                retValDot3OutPauseFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetDot3OutPauseFrames (INT4 i4dot3StatsIndex, UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    *pElement = 0;
#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

    if (CfaFsEtherHwGetPauseFrames ((UINT4) i4dot3StatsIndex, pElement,
                                    ETH_PAUSE_OUT_FRAMES) != FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4dot3StatsIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 *  Function    :  nmhGetDot3HCInPauseFrames
 *  Input       :  The Indices
 *                 Dot3StatsIndex
 *                 The Object
 *                 retValDot3HCInPauseFrames
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT4
nmhGetDot3HCInPauseFrames (INT4 i4Dot3StatsIndex,
                           tSNMP_COUNTER64_TYPE * pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    MEMSET (pElement, 0, sizeof (tSNMP_COUNTER64_TYPE));

#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4Dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

    if (CfaFsEtherHwGetPauseFrames ((UINT4) i4Dot3StatsIndex, &(pElement->lsn),
                                    ETH_HC_PAUSE_IN_FRAMES) != FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4Dot3StatsIndex);
    UNUSED_PARAM (pElement);
#endif
    return (i4RetVal);

}

/****************************************************************************
 *  Function    :  nmhGetDot3HCOutPauseFrames
 *  Input       :  The Indices
 *                 Dot3StatsIndex
 *                 The Object
 *                 retValDot3HCOutPauseFrames
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT4
nmhGetDot3HCOutPauseFrames (INT4 i4Dot3StatsIndex,
                            tSNMP_COUNTER64_TYPE * pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    MEMSET (pElement, 0, sizeof (tSNMP_COUNTER64_TYPE));

#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4Dot3StatsIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

    if (CfaFsEtherHwGetPauseFrames ((UINT4) i4Dot3StatsIndex, &(pElement->lsn),
                                    ETH_HC_PAUSE_OUT_FRAMES) != FNP_SUCCESS)
    {
        i4RetVal = SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4Dot3StatsIndex);
    UNUSED_PARAM (pElement);
#endif
    return (i4RetVal);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot3PauseTable
 Input       :  The Indices
                Dot3StatsIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT4
nmhGetFirstIndexDot3PauseTable (INT4 *pi4dot3StatsIndex)
{
    UINT2               u2Index = 1;
    UINT1               u1IfType = CFA_NONE;

    /* Get the First Used Index */
    CFA_CDB_SCAN_WITH_LOCK (u2Index, BRG_MAX_PHY_PLUS_LOG_PORTS, CFA_ALL_IFTYPE)
    {
        CfaGetIfType ((UINT4) u2Index, &u1IfType);

        if ((CFA_IF_ENTRY_USED (u2Index)) && ((u1IfType == CFA_ENET) ||
                                              (u1IfType == CFA_LAGG)))
        {
            *pi4dot3StatsIndex = (INT4) u2Index;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot3PauseTable
 Input       :  The Indices
                Dot3StatsIndex
                nextDot3StatsIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT4
nmhGetNextIndexDot3PauseTable (INT4 i4dot3StatsIndex,
                               INT4 *pi4dot3StatsIndexOUT)
{
    UINT4               u4IfIndex;
    UINT4               u4Counter;
    UINT1               u1IfType;

    if (i4dot3StatsIndex < 0)
    {
        return SNMP_FAILURE;
    }
    u4IfIndex = (UINT4) i4dot3StatsIndex;

    /* Next Index should always return the next used Index */
    u4Counter = u4IfIndex + 1;
    CFA_CDB_SCAN_WITH_LOCK (u4Counter, BRG_MAX_PHY_PLUS_LOG_PORTS,
                            CFA_ALL_IFTYPE)
    {
        CfaGetIfType (u4Counter, &u1IfType);

        if ((CFA_IF_ENTRY_USED ((UINT2) u4Counter))
            && ((u1IfType == CFA_ENET) || (u1IfType == CFA_LAGG)))
        {
            *pi4dot3StatsIndexOUT = (INT4) u4Counter;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot3ControlTable
 Input       :  The Indices
                Dot3StatsIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT4
nmhGetFirstIndexDot3ControlTable (INT4 *pi4dot3StatsIndex)
{
    UINT2               u2Index = 1;
    UINT1               u1IfType = CFA_NONE;

    /* Get the First Used Index */
    CFA_CDB_SCAN_WITH_LOCK (u2Index, BRG_MAX_PHY_PLUS_LOG_PORTS, CFA_ENET)
    {
        CfaGetIfType ((UINT4) u2Index, &u1IfType);

        if ((CFA_IF_ENTRY_USED (u2Index)) && (u1IfType == CFA_ENET))
        {
            *pi4dot3StatsIndex = (INT4) u2Index;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot3ControlTable
 Input       :  The Indices
                Dot3StatsIndex
                nextDot3StatsIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT4
nmhGetNextIndexDot3ControlTable (INT4 i4dot3StatsIndex,
                                 INT4 *pi4dot3StatsIndexOUT)
{
    UINT4               u4IfIndex;
    UINT4               u4Counter;
    UINT1               u1IfType;

    if (i4dot3StatsIndex < 0)
    {
        return SNMP_FAILURE;
    }
    u4IfIndex = (UINT4) i4dot3StatsIndex;

    /* Next Index should always return the next used Index */
    u4Counter = u4IfIndex + 1;
    CFA_CDB_SCAN_WITH_LOCK (u4Counter, BRG_MAX_PHY_PLUS_LOG_PORTS, CFA_ENET)
    {
        CfaGetIfType (u4Counter, &u1IfType);

        if ((CFA_IF_ENTRY_USED ((UINT2) u4Counter)) && (u1IfType == CFA_ENET))
        {
            *pi4dot3StatsIndexOUT = (INT4) u4Counter;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot3CollTable
 Input       :  The Indices
                IfIndex
                Dot3CollCount
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT4
nmhGetFirstIndexDot3CollTable (INT4 *pi4ifIndex, INT4 *pi4dot3CollCount)
{
    UINT2               u2Index = 1;
    UINT1               u1IfType = CFA_NONE;

/* Get the First Used Index */
    CFA_CDB_SCAN_WITH_LOCK (u2Index, BRG_MAX_PHY_PLUS_LOG_PORTS, CFA_ENET)
    {
        CfaGetIfType ((UINT4) u2Index, &u1IfType);

        if ((CFA_IF_ENTRY_USED (u2Index)) && (u1IfType == CFA_ENET))
        {
            *pi4ifIndex = (INT4) u2Index;
            *pi4dot3CollCount = MIN_COLLISION_COUNT;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot3CollTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                Dot3CollCount
                nextDot3CollCount
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT4
nmhGetNextIndexDot3CollTable (INT4 i4IfIndex,
                              INT4 *pi4ifIndexOUT,
                              INT4 i4dot3CollCount, INT4 *pi4dot3CollCountOUT)
{
    UINT4               u4IfIndex;
    UINT4               u4Counter;
    UINT1               u1IfType;

    if ((i4IfIndex < 0) || (i4dot3CollCount < 0) ||
        (i4IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return SNMP_FAILURE;
    }
    u4IfIndex = (UINT4) i4IfIndex;

    if (i4dot3CollCount < MAX_COLLISION_COUNT)
    {
        *pi4ifIndexOUT = (INT4) u4IfIndex;
        *pi4dot3CollCountOUT = i4dot3CollCount + 1;
        return SNMP_SUCCESS;
    }

    /* Next Index should always return the next used Index */
    u4Counter = u4IfIndex + 1;
    CFA_CDB_SCAN_WITH_LOCK (u4Counter, BRG_MAX_PHY_PLUS_LOG_PORTS, CFA_ENET)
    {
        CfaGetIfType (u4Counter, &u1IfType);

        if ((CFA_IF_ENTRY_USED (u4Counter)) && (u1IfType == CFA_ENET))
        {
            *pi4ifIndexOUT = (INT4) u4Counter;
            *pi4dot3CollCountOUT = MIN_COLLISION_COUNT;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot3StatsTable
 Input       :  The Indices
                Dot3StatsIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT4
nmhGetFirstIndexDot3StatsTable (INT4 *pi4dot3StatsIndex)
{
    UINT2               u2Index = 1;
    UINT1               u1IfType = CFA_NONE;

    /* Get the First Used Index */
    CFA_CDB_SCAN_WITH_LOCK (u2Index, BRG_MAX_PHY_PLUS_LOG_PORTS, CFA_ENET)
    {
        CfaGetIfType ((UINT4) u2Index, &u1IfType);

        if ((CFA_IF_ENTRY_USED (u2Index)) && (u1IfType == CFA_ENET))
        {
            *pi4dot3StatsIndex = (INT4) u2Index;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot3StatsTable
 Input       :  The Indices
                Dot3StatsIndex
                nextDot3StatsIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT4
nmhGetNextIndexDot3StatsTable (INT4 i4dot3StatsIndex,
                               INT4 *pi4dot3StatsIndexOUT)
{
    UINT4               u4IfIndex;
    UINT4               u4Counter;
    UINT1               u1IfType;

    if (i4dot3StatsIndex < 0)
    {
        return SNMP_FAILURE;
    }
    u4IfIndex = (UINT4) i4dot3StatsIndex;

    /* Next Index should always return the next used Index */
    u4Counter = u4IfIndex + 1;
    CFA_CDB_SCAN_WITH_LOCK (u4Counter, BRG_MAX_PHY_PLUS_LOG_PORTS, CFA_ENET)
    {
        CfaGetIfType (u4Counter, &u1IfType);

        if ((CFA_IF_ENTRY_USED ((UINT2) u4Counter)) && (u1IfType == CFA_ENET))
        {
            *pi4dot3StatsIndexOUT = (INT4) u4Counter;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot3PauseTable
 Input       :  The Indices
                Dot3StatsIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot3PauseTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
