/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfared.c,v 1.53 2016/04/08 09:58:21 siva Exp $
 *
 * Description:This file contains utility routines common to all
 *             the modules. 
 *
 *******************************************************************/
#include "cfainc.h"

extern tTimerListId CfaGddTimerListId;
#ifndef CFA_INTERRUPT_MODE
/* CFA's timer list which has GDD poll timer */
extern tTmrAppTimer GddPollTimer;    /* GDD poll timer */
#endif /* CFA_INTERRUPT_MODE */

UINT4               gu4CfaBulkUptIfIndex;
UINT1               gu1CfaNoOfPeers;
BOOL1               bCfaBulkReqRecvd = CFA_FALSE;
UINT1               gu1CfaAuditFlag;
tOsixTaskId         gCfaAuditTaskId;

/* SLL for the Red Buffer Entries */
tTMO_SLL            CfaRedBufferSll;
/* MemPool for Red Buffer Entries */
tMemPoolId          CfaRedBuffMemPoolId;
UINT1               gCFANpSyncBlk = RM_FALSE;

static const UINT2  au2DaysInMonth[2][13] = {
    /* Normal Year */
    {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365},
    /* Leap Year   */
    {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366}
};

/******************************************************************************/
/*                                                                            */
/*  Function Name   : CfaRedInitBufferPool                                    */
/*                                                                            */
/*  Description     : This function initilizes the mempool for the buffer     */
/*                    entries                                                 */
/*                                                                            */
/*  Input(s)        : None                                                    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : CFA_SUCCESS / CFA_FAILURE                               */
/******************************************************************************/
INT4
CfaRedInitBufferPool (VOID)
{
    /* Create SLL for CfaRedBuffer Table */
    TMO_SLL_Init (&CfaRedBufferSll);

    CFA_NPSYNC_BLK () = OSIX_FALSE;

    CFA_RED_BUF_MEMPOOL () = CFAMemPoolIds[MAX_CFA_RED_BUF_ENTRIES_SIZING_ID];

    return (CFA_SUCCESS);
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : CfaRedDeInitBufferPool                                  */
/*                                                                            */
/*  Description     : This function Deinitilizes the mempool for the buffer   */
/*                    entries                                                 */
/*                                                                            */
/*  Input(s)        : None                                                    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : CFA_SUCCESS / CFA_FAILURE                               */
/******************************************************************************/
INT4
CfaRedDeInitBufferPool (VOID)
{
#ifdef NPAPI_WANTED
    tCfaBufferEntry    *pBuf = NULL;
    tCfaBufferEntry    *pTempBuf = NULL;

    if (CFA_RED_BUF_MEMPOOL () != 0)
    {
        /* Delete the Buffer entries */
        TMO_DYN_SLL_Scan (&CfaRedBufferSll, pBuf, pTempBuf, tCfaBufferEntry *)
        {
            TMO_SLL_Delete (&CfaRedBufferSll, &pBuf->Node);
            MemReleaseMemBlock (CFA_RED_BUF_MEMPOOL (), (UINT1 *) pBuf);
        }

    }
#endif

    CFA_NPSYNC_BLK () = OSIX_FALSE;
    CFA_RED_BUF_MEMPOOL () = 0;
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaHandleRmEvents                                    */
/*                                                                           */
/* Description        : This function is invoked to process the following    */
/*                      from RM module:-                                     */
/*                           - RM events and                                 */
/*                           - update messages.                              */
/*                      This function interprets the RM events and calls the */
/*                      corresponding module functions to process those      */
/*                      events.                                              */
/*                                                                           */
/* Input(s)           : CfaRmMsg - Rm Message which contains event and buffer*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS / CFA_FAILURE                            */
/*****************************************************************************/
INT4
CfaHandleRmEvents (tCfaRmMsg CfaRmMsg)
{
    tRmNodeInfo        *pData = NULL;
    tCfaNodeStatus      CfaPrevNodeState = CFA_NODE_IDLE;
    tRmProtoEvt         ProtoEvt;
    tRmProtoAck         ProtoAck;
    UINT4               u4SeqNum = 0;
#ifndef CFA_INTERRUPT_MODE
    UINT4               u4GddTmrRemTime = 0;
#endif
#if defined(CLI_LNXIP_WANTED) || defined(SNMP_LNXIP_WANTED)
    tIpConfigInfo       IpConfigInfo;
    UINT4               u4Flag = 0;
#endif

    ProtoEvt.u4AppId = RM_CFA_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    switch (CfaRmMsg.u1Event)
    {
        case GO_ACTIVE:

            CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "In CfaHandleRmEvents "
                     "GO_ACTIVE message received from RM \r\n");

            if (CFA_NODE_STATUS () == CFA_NODE_ACTIVE)
            {
                break;
            }

            CfaPrevNodeState = CFA_NODE_STATUS ();

            if (CfaPrevNodeState == CFA_NODE_IDLE)
            {
                /* CFA_IP_UP_EVENT was received by CFA but didn't Initialize IP 
                 * since Cfa Node Status is in IDLE State. So Initalize IP once
                 * GO_ACTIVE trigger is fot from RM.*/
                if (CFA_RED_IPINIT_COMPLETE_FLAG () == CFA_TRUE)
                {
                    CFA_NODE_STATUS () = CFA_NODE_ACTIVE;

                    if ((IssGetRestoreFlagFromNvRam () != ISS_CONFIG_RESTORE) ||
                        CFA_MGMT_PORT == CFA_TRUE)
                    {
                        CfaProcessIpInitComplete ();
                    }
                }

                /* CFA_IP_UP_EVENT was not yet received by CFA during 
                 * ISS initialization. So just set this flag & change Node 
                 * Status to Active. When this event is received, CFA will 
                 * initailze IP */
                if (CFA_RED_IPINIT_COMPLETE_FLAG () == CFA_FALSE)
                {
                    CFA_RED_IPINIT_COMPLETE_FLAG () = CFA_TRUE;
                }

                CFA_RED_AUDIT_FLAG () = CFA_RED_AUDIT_STOP;
            }

            CFA_NODE_STATUS () = CFA_NODE_ACTIVE;

            CFA_NUM_STANDBY_NODES () = RmGetStandbyNodeCount ();
            if (CFA_IS_BULK_REQ_RECVD () == CFA_TRUE)
            {
                CFA_IS_BULK_REQ_RECVD () = CFA_FALSE;

                gu4CfaBulkUptIfIndex = 1;

                CfaHandleBulkReqEvent (NULL);
            }

#ifndef CFA_INTERRUPT_MODE
            /* CFA Gdd Poll timer was disabled in STANDBY node. Now the 
             * STANDBY node has become ACTIVE. Therefore, start CFA Gdd 
             * Poll timer by sending an event to CFA task as if the poll
             * timer is expired. Poll timer will be used only in case of
             * non interrupt mode
             * */
            if (CfaPrevNodeState == CFA_NODE_STANDBY)
            {
                if (CfaSendEventToCfaTask (CFA_GDD_POLL_TMR_EXP_EVENT) !=
                    CFA_SUCCESS)
                {
                    CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                             "Send Event to CFA Failed!!!!.\n");
                }
            }
#endif
            if (CfaPrevNodeState == CFA_NODE_IDLE)
            {
                ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
            }
            else
            {
#if defined(CLI_LNXIP_WANTED) || defined(SNMP_LNXIP_WANTED)
                /* Linux programming of OOB interface is done on GO_ACTIVE event
                 * during switchover. */
                if ((CFA_MGMT_PORT == TRUE) &&
                    (CFA_CDB_IF_ADMIN_STATUS (CFA_DEFAULT_ROUTER_IFINDEX) == CFA_IF_UP))
                {
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "In CfaHandleRmEvents "
                         " GO_ACTIVE event received from RM, "
                         "programming OOB interface in linux\r\n");
                MEMSET (&IpConfigInfo, 0, sizeof(tIpConfigInfo));
                CfaIpIfGetIfInfo (CFA_DEFAULT_ROUTER_IFINDEX, &IpConfigInfo);
               /* Set all the IP interface params */
                u4Flag = CFA_IP_IF_ALLOC_PROTO | CFA_IP_IF_ALLOC_METHOD |
                CFA_IP_IF_PRIMARY_ADDR | CFA_IP_IF_NETMASK |
                CFA_IP_IF_FORWARD | CFA_IP_IF_BCASTADDR;

                CfaIpUpdateInterfaceParams(CFA_CDB_IF_ALIAS 
                           (CFA_DEFAULT_ROUTER_IFINDEX), u4Flag, &IpConfigInfo);
#ifdef IP6_WANTED
                CfaGetIfIp6Addr (CFA_DEFAULT_ROUTER_IFINDEX, &IpConfigInfo);
                CfaLinuxIpUpdateIpv6Addr (CFA_DEFAULT_ROUTER_IFINDEX, &IpConfigInfo.
                       Ip6Addr, (INT4) IpConfigInfo.u1PrefixLen, OSIX_TRUE);
#endif
                }
#endif
                ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
            }

            RmApiHandleProtocolEvent (&ProtoEvt);

            break;

        case GO_STANDBY:

            if (CFA_NODE_STATUS () == CFA_NODE_STANDBY)
            {
                break;
            }

            if (CFA_NODE_STATUS () == CFA_NODE_IDLE)
            {
                /* Stop Sync Up events in Static Bulk Update */
                CFA_NPSYNC_BLK () = OSIX_TRUE;

                /* IP_UP_EVENT was received by CFA but didn't Initialize IP 
                 * since Cfa Node Status is in IDLE State. So Initalize IP once
                 * GO_ACTIVE trigger is fot from RM.*/
                if (CFA_RED_IPINIT_COMPLETE_FLAG () == CFA_TRUE)
                {
                    if ((IssGetRestoreFlagFromNvRam () != ISS_CONFIG_RESTORE) ||
                            (CFA_MGMT_PORT == CFA_TRUE))
                    {
                        CfaProcessIpInitComplete ();
                    }
                }
                /* IP_UP_EVENT was not yet received by CFA during 
                 * ISS initialization. So just set this flag & change Node 
                 * Status to Active. When this event is received, CFA will 
                 * initailze IP */

                if (CFA_RED_IPINIT_COMPLETE_FLAG () == CFA_FALSE)
                {
                    CFA_RED_IPINIT_COMPLETE_FLAG () = CFA_RED_STADNBY_RCVD;
                }

                CFA_RED_AUDIT_FLAG () = CFA_RED_AUDIT_STOP;
            }
            else if (CFA_NODE_STATUS () == CFA_NODE_ACTIVE)
            {
#if defined(CLI_LNXIP_WANTED) || defined(SNMP_LNXIP_WANTED)
                /* When node becomes standby from active state, ip addr 
                 * of OOB interface is clear in linux. */
                if (CFA_MGMT_PORT == TRUE)
                {
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "In CfaHandleRmEvents "
                         " GO_STANDBY event received from RM, "
                         "clearing the programmed OOB interface in linux\r\n");
                MEMSET (&IpConfigInfo, 0, sizeof(tIpConfigInfo));
                CfaGetIfIp6Addr (CFA_DEFAULT_ROUTER_IFINDEX, &IpConfigInfo);
#ifdef IP6_WANTED
                CfaLinuxIpUpdateIpv6Addr (CFA_DEFAULT_ROUTER_IFINDEX, &IpConfigInfo
                           .Ip6Addr, (INT4) IpConfigInfo.u1PrefixLen, OSIX_FALSE);
#endif                
                CfaIpUpdateInterfaceParams(CFA_CDB_IF_ALIAS 
                           (CFA_DEFAULT_ROUTER_IFINDEX), CFA_IP_IF_NOFORWARD, NULL);
                }
#endif
                CFA_NODE_STATUS () = CFA_NODE_STANDBY;

                CFA_NUM_STANDBY_NODES () = 0;
#ifndef CFA_INTERRUPT_MODE
                /* Stop the poll timer running. This node is going to
                 * transit to STANDBY
                 * */
                if (CFA_GDD_TMR_LIST != 0)
                {
                    TmrGetRemainingTime (CFA_GDD_TMR_LIST, &GddPollTimer,
                                         &u4GddTmrRemTime);
                    if (u4GddTmrRemTime != 0)
                    {
                        TmrStopTimer (CFA_GDD_TMR_LIST, &GddPollTimer);
                    }
                }
#endif

                /* Stop auditing task, if it is running. */
                if (CFA_AUDIT_TASK_ID () != 0)
                {
                    CFA_RED_AUDIT_FLAG () = CFA_RED_AUDIT_CLEAN_AND_STOP;

                    CFA_SEND_EVENT (CFA_AUDIT_TASK_ID (),
                                    CFA_RED_AUDIT_TSK_DEL_EVENT);
                }

                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                RmApiHandleProtocolEvent (&ProtoEvt);
            }

            CFA_IS_BULK_REQ_RECVD () = CFA_FALSE;
            break;

        case RM_INIT_HW_AUDIT:
            /* Hw Audit is not implemented for CFA */
            if (CFA_NODE_STATUS () == CFA_NODE_ACTIVE)
            {
                /* Start Audit Task. */
                CfaRedCreateAuditTask ();
            }
            break;

            break;

        case RM_STANDBY_UP:

            pData = (tRmNodeInfo *) CfaRmMsg.pFrame;
            CFA_NUM_STANDBY_NODES () = pData->u1NumStandby;
            RmReleaseMemoryForMsg ((UINT1 *) pData);
            if (CFA_IS_BULK_REQ_RECVD () == CFA_TRUE)
            {
                CFA_IS_BULK_REQ_RECVD () = CFA_FALSE;

                gu4CfaBulkUptIfIndex = 1;

                CfaHandleBulkReqEvent (NULL);
            }
            break;

        case RM_STANDBY_DOWN:
            pData = (tRmNodeInfo *) CfaRmMsg.pFrame;
            CFA_NUM_STANDBY_NODES () = pData->u1NumStandby;
            RmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            if ((CFA_NODE_STATUS () == CFA_NODE_IDLE)
                && (RmGetNodeState () == RM_STANDBY))
            {
                CFA_NPSYNC_BLK () = OSIX_FALSE;
                /* Handle GO_STANDBY */
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "In CfaHandleRmEvents "
                         "CONFIG_RESTORE_COMPLETE message received from RM, "
                         "Moving Node to Standby\r\n");

                CFA_NODE_STATUS () = CFA_NODE_STANDBY;

                CFA_NUM_STANDBY_NODES () = 0;

                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                RmApiHandleProtocolEvent (&ProtoEvt);
            }
            break;

        case RM_MESSAGE:
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "In CfaHandleRmEvents "
                     "RM_MESSAGE event received from RM\n");

            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM ((tRmMsg *) CfaRmMsg.pFrame, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR ((tRmMsg *) CfaRmMsg.pFrame, CfaRmMsg.u2Length);

            ProtoAck.u4AppId = RM_CFA_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            CfaHandleMessageFromRm ((tRmMsg *) CfaRmMsg.pFrame);

            /* Processing of message is over, hence free the RM message. */
            RM_FREE ((tRmMsg *) (CfaRmMsg.pFrame));

            /* Sending ACK to RM */
            RmApiSendProtoAckToRM (&ProtoAck);

            break;

        case RM_COMPLETE_SYNC_UP:

            CFA_DBG (CFA_TRC_ALL, CFA_MAIN, " In CfaHandleMessageFromRm "
                     "Received RM Complete Syncup  Message.\r\n");
            CfaTriggerHigherLayer (L2_COMPLETE_SYNC_UP);
            break;

        case L2_INITIATE_BULK_UPDATES:

            CFA_DBG (CFA_TRC_ALL, CFA_MAIN, " In CfaHandleMessageFromRm "
                     "Received RM L2Initiate BulkUpdate  Message.\r\n");
            CfaSendBulkMessage (CFA_BULK_REQ_MSG);
            break;

        case RM_DYNAMIC_SYNCH_AUDIT:
            CfaRedHandleDynSyncAudit ();
            break;

        default:
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "!!ERROR: In CfaHandleRmEvents "
                     "Invalid Event received from RM\n");

            break;
    }
    return CFA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : CfaSendBulkRequest.                                  */
/*                                                                           */
/* Description        : This function sends a bulk request to the active     */
/*                      node in the system.                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS / CFA_FAILURE.                           */
/*****************************************************************************/
INT4
CfaSendBulkMessage (UINT1 u1MsgType)
{

    tRmMsg             *pMsg;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = 0;
    UINT2               u2BufSize;

    ProtoEvt.u4AppId = RM_CFA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    CFA_DBG (CFA_TRC_ALL, CFA_MAIN, " In CfaSendBulkMessage "
             "Sending Bulk Message.\r\n");

    u2BufSize = CFA_RM_BULK_MSG_SIZE;
    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN, "Error in CfaSendBulkMessage - "
                 "Memory Allocation failed \r\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return (CFA_FAILURE);
    }

    /* Fill the message type. */
    CFA_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);

    /* This length field will not be used. It is just to keep the
     * this message structure similar to that of other sync msgs. */
    CFA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2BufSize);

    if (CfaSendMsgToRm (pMsg, u2BufSize) == CFA_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaSendMsgToRm                                       */
/*                                                                           */
/* Description        : This function sends given RM message to              */
/*                      Redundancy Manager.                                  */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to the RM input buffer.               */
/*                      u2BufSize  - Size of the given input buffer.         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS / CFA_FAILURE                            */
/*****************************************************************************/
INT4
CfaSendMsgToRm (tRmMsg * pMsg, UINT2 u2BufSize)
{

    /* Call the API provided by RM to send the data to RM */
    if (RmEnqMsgToRmFromAppl (pMsg, u2BufSize, RM_CFA_APP_ID, RM_CFA_APP_ID)
        == RM_FAILURE)
    {

        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN, "Error in CfaSendMsgToRm - "
                 "Message Enqueue Failed \r\n");
        RM_FREE (pMsg);
        return (CFA_FAILURE);
    }

    return (CFA_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : CfaHandleMessageFromRm.                              */
/*                                                                           */
/* Description        : This function handles the messages from RM.          */
/*                                                                           */
/* Input(s)           : pMesg    - Pointer to the sync up message.           */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
CfaHandleMessageFromRm (tRmMsg * pMesg)
{
    UINT4               u4Offset = 0;
    UINT2               u2RedBufOffset = 0;
    UINT2               u2Length = 0;
    UINT2               u2PortCount = 0;
    UINT1               u1MsgType;
    UINT1               u1CfaMsgType = 0;
    tRmProtoEvt         ProtoEvt;
    tUtlTm              CurTime;
    UINT4               u4Months = 0;
    UINT4               u4Days = 0;
    UINT4               u4Leap = 0;

    ProtoEvt.u4AppId = RM_CFA_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    MEMSET (&CurTime, 0, sizeof (tUtlTm));

    CFA_RM_GET_1_BYTE (pMesg, &u4Offset, u1MsgType);
    CFA_RM_GET_2_BYTE (pMesg, &u4Offset, u2Length);
    switch (u1MsgType)
    {
        case CFA_BULK_REQ_MSG:

            CFA_DBG (CFA_TRC_ALL, CFA_MAIN, " In CfaHandleMessageFromRm "
                     "Received Bulk Request Message.\r\n");

            if (CFA_IS_STANDBY_UP () == CFA_FALSE)
            {
                CFA_DBG (CFA_TRC_ERROR, CFA_MAIN, "CfaHandleBulkReqEvent "
                         "Peer Node Not Ready \r\n");

                CFA_IS_BULK_REQ_RECVD () = CFA_TRUE;
                return;
            }

            CFA_IS_BULK_REQ_RECVD () = CFA_FALSE;

            gu4CfaBulkUptIfIndex = 1;

            CfaHandleBulkReqEvent (NULL);

            break;

        case CFA_BULK_UPD_TAIL_MSG:

            CFA_DBG (CFA_TRC_ALL, CFA_MAIN, " In CfaHandleMessageFromRm "
                     "Received Bulk Update Tail Message.\r\n");

            ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
            RmApiHandleProtocolEvent (&ProtoEvt);

            break;

        case CFA_SYNC_PORT_INFO_MSG:

            if (CFA_NODE_STATUS () != CFA_NODE_STANDBY)
            {
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN, " In CfaHandleMessageFromRm "
                         "Ignoring Sync Port Oper Status Message Received "
                         "Not in Standby state .\r\n");
                /* Not a Standby node, hence don't process the received 
                 * sync message. */
                return;

            }

            CFA_DBG (CFA_TRC_ALL, CFA_MAIN, " In CfaHandleMessageFromRm "
                     "Received Sync Port Oper Status Message.\r\n");
            CFA_RM_GET_2_BYTE (pMesg, &u4Offset, u2PortCount);

            CfaProcessPortInformation (pMesg, u4Offset, u2PortCount);
            break;

        case CFA_BULK_RESP_MSG:

            if (CFA_NODE_STATUS () != CFA_NODE_STANDBY)
            {
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN, " In CfaHandleMessageFromRm "
                         "Ignoring Bulk Response  Message Received "
                         "Not in Standby state .\r\n");
                /* Not a Standby node, hence don't process the received 
                 * sync message. */
                ProtoEvt.u4Error = RM_PROCESS_FAIL;
                ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
                RmApiHandleProtocolEvent (&ProtoEvt);
                return;

            }

            CFA_DBG (CFA_TRC_ALL, CFA_MAIN, " In CfaHandleMessageFromRm "
                     "Received Bulk Response Message.\r\n");

            CFA_RM_GET_1_BYTE (pMesg, &u4Offset, u1CfaMsgType);
            if (u1CfaMsgType == CFA_PORT_INFO_MSG)
            {
                CFA_RM_GET_2_BYTE (pMesg, &u4Offset, u2PortCount);

                CfaProcessBulkResponse (pMesg, u4Offset, u2PortCount);
            }
            else if (u1CfaMsgType == CFA_CLOCK_INFO_MSG)
            {
                CFA_RM_GET_N_BYTE (pMesg, &CurTime, &u4Offset, sizeof (tUtlTm));
                u4Months = CurTime.tm_mon;
                u4Days = CurTime.tm_mday - 1;

                u4Leap = (IS_LEAP (CurTime.tm_year) ? 1 : 0);
                if (u4Months <= 11)
                {
                    CurTime.tm_yday =
                        (UINT4) (au2DaysInMonth[u4Leap][u4Months]);
                }
                CurTime.tm_yday += u4Days;

                UtlSetTime (&CurTime);
            }
            break;

        case NPSYNC_MESSAGE:
        {
            if (CFA_NODE_STATUS () != CFA_NODE_STANDBY)
            {
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN, " In CfaHandleMessageFromRm "
                         "Ignoring NPSYC message Received "
                         "Not in Standby state .\r\n");
                /* Not a Standby node, hence don't process the received 
                 * sync message. */
                ProtoEvt.u4Error = RM_PROCESS_FAIL;
                ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
                RmApiHandleProtocolEvent (&ProtoEvt);
                return;
            }

            u2RedBufOffset = (UINT2) u4Offset;

#ifdef NPAPI_WANTED
            /* Process the NPAPI Sync up message */
            CfaNpSyncProcessSyncMsg (pMesg, &u2RedBufOffset);
#else
            UNUSED_PARAM (u2RedBufOffset);
#endif

        }

        default:
            break;
    }

}

/*****************************************************************************/
/* Function Name      : CfaHandleBulkReqEvent.                               */
/*                                                                           */
/* Description        : This function gets the Port Oper Status from all     */
/*                      ports Sync with standby node.                        */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS / CFA_FAILURE                            */
/*****************************************************************************/
INT4
CfaHandleBulkReqEvent (VOID *pvPeerId)
{
    /* Send Bulk Update messages to the standby. */
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4IfIndex;
    INT4                i4Retval;
    UINT2               u2PortCount = 0;
    UINT4               u4Value = 0;
    UINT1               u1OperStatus = 0;
    UINT1               u1IfUfdOperStatus = 0;
    UINT1               u1EtherType = 0;
    tRmProtoEvt         ProtoEvt;
    INT4                i4Support = 0;
    INT4                i4Status = 0;
    UINT4               u4OperMauType = 0;
    UINT2               u2AdvtCapability = 0;
    tUtlTm              tm;

    UNUSED_PARAM (pvPeerId);

    ProtoEvt.u4AppId = RM_CFA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    MEMSET (&tm, 0, sizeof (tUtlTm));

    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN, "CfaHandleBulkReqEvent "
                 "CFA is not initialised \r\n");
        gu4CfaBulkUptIfIndex = 1;

        /* CFA completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        RmSetBulkUpdatesStatus (RM_CFA_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        CfaSendBulkMessage (CFA_BULK_UPD_TAIL_MSG);

        return CFA_SUCCESS;
    }
    if (CFA_NODE_STATUS () != CFA_NODE_ACTIVE)
    {
        /* Only Active node can process bulk request and can
         * send the bulk update.*/
        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN, "CfaHandleBulkReqEvent "
                 "Node Status not Active \r\n");

        CFA_IS_BULK_REQ_RECVD () = CFA_TRUE;

        return CFA_SUCCESS;
    }

    if (CFA_IS_STANDBY_UP () == CFA_FALSE)
    {

        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN, "CfaHandleBulkReqEvent "
                 "Peer Node Not Ready \r\n");

        return CFA_SUCCESS;
    }

    if ((pMsg = RM_ALLOC_TX_BUF (CFA_RM_BULK_RESP_MSG_SIZE)) == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN, "Error in CfaHandleBulkReqEvent "
                 "Memory Allocation failed \r\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return CFA_FAILURE;
    }

    if (gu4CfaBulkUptIfIndex < SYS_MAX_INTERFACES)
    {
        /* Fill the message type. */
        CFA_RM_PUT_1_BYTE (pMsg, &u4Offset, CFA_BULK_RESP_MSG);

        /* Fill the Buffer Size. */
        CFA_RM_PUT_2_BYTE (pMsg, &u4Offset, CFA_RM_BULK_RESP_MSG_SIZE);

        /* Fill the message type. */
        CFA_RM_PUT_1_BYTE (pMsg, &u4Offset, CFA_PORT_INFO_MSG);

        /* Fill the No of Ports - Temp just to incr the offset */
        u4Offset = u4Offset + 2;

        for (u4IfIndex = gu4CfaBulkUptIfIndex;
             u4IfIndex <= SYS_MAX_INTERFACES
             && u2PortCount < CFA_PORT_COUNT_PER_BULK_MSG; u4IfIndex++)
        {
            i4Retval = CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);

            if (i4Retval == CFA_FAILURE)
            {
                continue;
            }

            /* Fill the IfIndex */
            CFA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4IfIndex);

            /* Fill the OperStatus */
            CFA_RM_PUT_1_BYTE (pMsg, &u4Offset, u1OperStatus);

            CfaGetIfSpeed (u4IfIndex, &u4Value);

            /* Fill Speed */
            CFA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4Value);

            CfaGetIfHighSpeed (u4IfIndex, &u4Value);

            /* Fill High Speed */
            CFA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4Value);

            CfaGetIfDuplexStatus (u4IfIndex, (INT4 *) &u4Value);

            /* Fill Duplex Status */
            CFA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4Value);

            CfaGetEthernetType (u4IfIndex, &u1EtherType);

            /* Fill Ethernet type */
            CFA_RM_PUT_1_BYTE (pMsg, &u4Offset, u1EtherType);

            /*
             * For MAU low level routines, where REPEATER connection is not
             * supported, the i4MauIndex for the MAU Tables is always "1".
             * Hence second argument is 1.
             */
            /* Fill Auto negotiation support */
            i4Support = CFA_CDB_IF_AUTONEGSUPPORT (u4IfIndex);
            CFA_RM_PUT_4_BYTE (pMsg, &u4Offset, i4Support);

            /* Fill Auto negotiation status */
            i4Status = CFA_CDB_IF_AUTONEGSTATUS (u4IfIndex);
            CFA_RM_PUT_4_BYTE (pMsg, &u4Offset, i4Status);

            /* Fill advertising capabilities */
            u2AdvtCapability = CFA_CDB_IF_ADVT_CAPABILITY (u4IfIndex);
            CFA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2AdvtCapability);

            /* Fill oper mau type */
            u4OperMauType = CFA_CDB_IF_OPER_MAU_TYPE (u4IfIndex);
            CFA_RM_PUT_2_BYTE (pMsg, &u4Offset, u4OperMauType);

            /* Fill UFD Oper Status */
            u1IfUfdOperStatus = CFA_CDB_IF_UFD_OPER (u4IfIndex);
            CFA_RM_PUT_1_BYTE (pMsg, &u4Offset, u1IfUfdOperStatus);

            u2PortCount++;
        }

        u4Offset = CFA_BULK_MSG_PORT_COUNT_OFFSET;
        /* Fill  the actual Port count */
        CFA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2PortCount);

        /* Don't use the u4Offset any more */
        if (CfaSendMsgToRm (pMsg, (UINT2) ((u2PortCount * CFA_RM_INTF_MSG_SIZE)
                                           + CFA_RM_GLB_MSG_SIZE)) ==
            CFA_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }

        gu4CfaBulkUptIfIndex = u4IfIndex;
        /* Send an event to start the next sub bulk update.
         */

        /* send an explicit event to the task */
        OsixEvtSend (CFA_TASK_ID, CFA_SUB_BULK_UPDT_EVENT);
    }
    else
    {
        gu4CfaBulkUptIfIndex = 1;

        /* Send the Clock Information */
        /* Fill the message type. */
        CFA_RM_PUT_1_BYTE (pMsg, &u4Offset, CFA_BULK_RESP_MSG);

        /* Fill the Buffer Size. */
        CFA_RM_PUT_2_BYTE (pMsg, &u4Offset,
                           (CFA_RM_GLB_CLOCK_MSG_SIZE + sizeof (tUtlTm)));

        /* Fill the message type. */
        CFA_RM_PUT_1_BYTE (pMsg, &u4Offset, CFA_CLOCK_INFO_MSG);

        UtlGetTime (&tm);

        tm.tm_wday++;

        CFA_RM_PUT_N_BYTE (pMsg, &tm, &u4Offset, sizeof (tUtlTm));

        if (CfaSendMsgToRm (pMsg, (UINT2) (CFA_RM_GLB_CLOCK_MSG_SIZE +
                                           sizeof (tUtlTm))) == CFA_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }

        /* CFA completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        RmSetBulkUpdatesStatus (RM_CFA_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        CfaSendBulkMessage (CFA_BULK_UPD_TAIL_MSG);
    }

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaTrigHigherLayer                                   */
/*                                                                           */
/* Description        : This function will send the given event to the next  */
/*                      higher layer (PNCA/LA/STP/VLAN).                     */
/*                                                                           */
/* Input(s)           : u1TrigType - Trigger Type. L2_INITIATE_BULK_UPDATES  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
CfaTriggerHigherLayer (UINT1 u1TrigType)
{

#ifdef EOAM_WANTED
    EoamRedHandleUpdateEvent (u1TrigType, NULL, 0);
#elif PNAC_WANTED
    PnacRedRcvPktFromRm (u1TrigType, NULL, 0);
#elif LA_WANTED
    LaRedRcvPktFromRm (u1TrigType, NULL, 0);
#elif defined(RSTP_WANTED) || defined(MSTP_WANTED)
    AstHandleUpdateEvents (u1TrigType, NULL, 0);
#elif VLAN_WANTED
    VlanRedHandleUpdateEvents (u1TrigType, NULL, 0);
#elif LLDP_WANTED
    LldpRedRcvPktFromRm (u1TrigType, NULL, 0);
#else

    if ((CFA_NODE_STATUS () == CFA_NODE_ACTIVE) &&
        (u1TrigType == L2_COMPLETE_SYNC_UP))
    {
        /* Send an event to RM to notify that sync up is completed */
        if (RmSendEventToRmTask (L2_SYNC_UP_COMPLETED) != RM_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                     "Error In CfaTriggerHigherLayer - "
                     "Event sending failed\r\n");
        }
    }

#endif
}

/*****************************************************************************/
/* Function Name      : CfaProcessBulkResponse.                              */
/*                                                                           */
/* Description        : This function process the received Port Bulk Response*/
/*                      Message.                                             */
/*                                                                           */
/* Input(s)           : pMesg    - Pointer to the sync up message.           */
/*                      u4Offset - current Offset in the msg.                */
/*                      MsgCount - Message Count.                            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
CfaProcessBulkResponse (tRmMsg * pMesg, UINT4 u4Offset, UINT2 u2MsgCount)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4Value = 0;
    INT4                i4Support = 0;
    INT4                i4Status = 0;
    UINT2               u2AdvtCapability = 0;
    UINT2               u2OperMauType = 0;
    UINT1               u1OperStatus = 0;
    UINT1               u1EtherType = 0;
    UINT1               u1IfUfdOperStatus = 0;

    for (; u2MsgCount > 0; u2MsgCount--)
    {
        /* Retrive IfIndex */
        CFA_RM_GET_4_BYTE (pMesg, &u4Offset, u4IfIndex);

        /* Retrive OperStatus */
        CFA_RM_GET_1_BYTE (pMesg, &u4Offset, u1OperStatus);

        CfaRedOperStatusUpdate (u4IfIndex, (UINT4) u1OperStatus);

        /* Retrive Speed */
        CFA_RM_GET_4_BYTE (pMesg, &u4Offset, u4Value);

        CfaSetIfSpeed (u4IfIndex, u4Value);

        CFA_RM_GET_4_BYTE (pMesg, &u4Offset, u4Value);

        /* Retrive High Speed */
        CfaSetIfHighSpeed (u4IfIndex, u4Value);

        CFA_RM_GET_4_BYTE (pMesg, &u4Offset, u4Value);

        /* Retrive duplex Status Speed */
        CfaSetIfDuplexStatus (u4IfIndex, (INT4) u4Value);

        CFA_RM_GET_1_BYTE (pMesg, &u4Offset, u1EtherType);

        /* Retrive ethernet type */
        CfaSetEthernetType (u4IfIndex, u1EtherType);

        /* Retrieve auto negotiation support */
        CFA_RM_GET_4_BYTE (pMesg, &u4Offset, *(UINT4 *) &i4Support);
        CfaSetIfAutoNegSupport (u4IfIndex, i4Support);

        /* Retrieve auto negotiation status */
        CFA_RM_GET_4_BYTE (pMesg, &u4Offset, *(UINT4 *) &i4Status);
        CfaSetIfAutoNegStatus (u4IfIndex, i4Status);

        /* Retrieve advertising capability */
        CFA_RM_GET_2_BYTE (pMesg, &u4Offset, u2AdvtCapability);
        CfaSetIfAdvtCapability (u4IfIndex, u2AdvtCapability);

        /* Retrieve oper mau type */
        CFA_RM_GET_2_BYTE (pMesg, &u4Offset, u2OperMauType);
        CfaSetIfOperMauType (u4IfIndex, (UINT2) u2OperMauType);

        /* Retrieve UFD Oper Status */
        CFA_RM_GET_1_BYTE (pMesg, &u4Offset, u1IfUfdOperStatus);
        CFA_CDB_IF_UFD_OPER (u4IfIndex) = u1IfUfdOperStatus;
    }
}

/*****************************************************************************/
/* Function Name      : CfaProcessPortInformation.                           */
/*                                                                           */
/* Description        : This function process the received Port information  */
/*                      SyncUp  msg.                                         */
/*                                                                           */
/* Input(s)           : pMesg    - Pointer to the sync up message.           */
/*                      u4Offset - current Offset in the msg.                */
/*                      MsgCount - Message Count.                            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
CfaProcessPortInformation (tRmMsg * pMesg, UINT4 u4Offset, UINT2 u2MsgCount)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4Value = 0;
    UINT4               u4MsgType = 0;

    for (; u2MsgCount > 0; u2MsgCount--)
    {
        CFA_RM_GET_4_BYTE (pMesg, &u4Offset, u4MsgType);
        CFA_RM_GET_4_BYTE (pMesg, &u4Offset, u4IfIndex);
        CFA_RM_GET_4_BYTE (pMesg, &u4Offset, u4Value);

        switch (u4MsgType)
        {
            case CFA_SYNC_PORT_OPER_STATUS:

                /* TODO: Problem - Port Oper SyncUp Msg comes before Interface 
                 * Creation ( CliSyncUp).
                 * Need to solve the same */

                CfaRedOperStatusUpdate (u4IfIndex, u4Value);

                break;

            case CFA_SYNC_PORT_SPEED:

                CfaSetIfSpeed (u4IfIndex, u4Value);
                break;

            case CFA_SYNC_PORT_HIGH_SPEED:

                CfaSetIfHighSpeed (u4IfIndex, u4Value);
                break;

            case CFA_SYNC_PORT_DUPLEX_STATUS:

                CfaSetIfDuplexStatus (u4IfIndex, (INT4) u4Value);
                break;

            case CFA_SYNC_PORT_ETHER_TYPE:

                CfaSetEthernetType (u4IfIndex, (UINT1) u4Value);
                break;

            case CFA_SYNC_PORT_AUTO_NEG_SUPPORT:

                CfaSetIfAutoNegSupport (u4IfIndex, (INT4) u4Value);
                break;

            case CFA_SYNC_PORT_AUTO_NEG_STATUS:

                CfaSetIfAutoNegStatus (u4IfIndex, (INT4) u4Value);
                break;

            case CFA_SYNC_PORT_ADV_CAP:

                CfaSetIfAdvtCapability (u4IfIndex, (UINT2) u4Value);
                break;

            case CFA_SYNC_PORT_OPER_MAU_TYPE:

                CfaSetIfOperMauType (u4IfIndex, (UINT2) u4Value);
                break;

            default:
                break;

        }
    }
}

/*****************************************************************************/
/* Function Name      : CfaSyncPortInformation.                              */
/*                                                                           */
/* Description        : This function synchornize the Port Oper Status       */
/*                      with the standby node.                               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index.                         */
/*                      u1OperStatus - OperStatus of the Port.               */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS / CFA_FAILURE                            */
/*****************************************************************************/
INT4
CfaSyncPortInformation (UINT4 u4IfIndex, UINT4 u4MsgType, UINT4 u4Value)
{
    tRmMsg             *pMsg;
    UINT4               u4Offset = 0;

    if (CFA_NODE_STATUS () != CFA_NODE_ACTIVE)
    {
        /* Only Active node can send sync messages           
         * send the bulk update.*/
        return CFA_SUCCESS;
    }

    if (CFA_IS_STANDBY_UP () == CFA_FALSE)
    {
        return CFA_SUCCESS;
    }

    if ((pMsg = RM_ALLOC_TX_BUF (CFA_RM_SYNC_MSG_SIZE)) == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN, "Error in CfaSyncPortInformation"
                 "Memory Allocation failed \r\n");
        return CFA_FAILURE;
    }

    /* Fill the message type. */
    CFA_RM_PUT_1_BYTE (pMsg, &u4Offset, CFA_SYNC_PORT_INFO_MSG);

    /* Fill the Buffer Size. */
    CFA_RM_PUT_2_BYTE (pMsg, &u4Offset, CFA_RM_SYNC_MSG_SIZE);

    /* Fill the Port Count. */
    CFA_RM_PUT_2_BYTE (pMsg, &u4Offset, 1);

    /* Fill the Sub Message Type */
    CFA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4MsgType);
    /* Fill the IfIndex */
    CFA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4IfIndex);

    /* Fill the OperStatus */
    CFA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4Value);

    CfaSendMsgToRm (pMsg, CFA_RM_SYNC_MSG_SIZE);

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaRedOperStatusUpdate                               */
/*                                                                           */
/* Description        : This function updates the Standby Node with          */
/*                      received Port Oper Status                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index.                         */
/*                      u4Value - OperStatus of the Port.                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
CfaRedOperStatusUpdate (UINT4 u4IfIndex, UINT4 u4Value)
{

    UINT1               u1OperStatus = 0;
    UINT1               u1OldOperStatus = 0;
    UINT1               u1IfType = 0;
    UINT1               u1IsFromMib = 0;
    UINT1               u1VipOperStatusFlag = CFA_FALSE;
    UINT1               u1AdminStatus = CFA_IF_DOWN;

    /* Check if interface present or not */
    if (CfaIsVipInterface (u4IfIndex) == CFA_TRUE)
    {
        CfaGetCdbPortAdminStatus (u4IfIndex, &u1AdminStatus);

        if ((u1AdminStatus == CFA_IF_UP) && (u4Value == CFA_IF_UP))
        {
            u1OperStatus = CFA_IF_UP;
        }
        else if (u4Value == CFA_IF_NP)
        {
            u1OperStatus = CFA_IF_NP;
        }
        else
        {
            u1OperStatus = CFA_IF_DOWN;
        }
        if (CfaGetIfOperStatus (u4IfIndex, &u1OldOperStatus) == CFA_SUCCESS)
        {
            if (u1OperStatus != u1OldOperStatus)
            {
                L2IwfGetVipOperStatusFlag (u4IfIndex, &u1VipOperStatusFlag);

                if (u1VipOperStatusFlag == CFA_TRUE)
                {
                    CfaSetVipOperStatus (u4IfIndex, u1OperStatus);
                }
            }
        }
    }
    else
    {
        if ((CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE) &&
            (CFA_IF_ENTRY (u4IfIndex) == NULL))
        {
            return;
        }
        else if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) != CFA_TRUE)
        {
            return;
        }

        if ((CFA_IF_ADMIN (u4IfIndex) == CFA_IF_UP) && (u4Value == CFA_IF_UP))
        {
            u1OperStatus = CFA_IF_UP;
        }
        else if (u4Value == CFA_IF_NP)
        {
            u1OperStatus = CFA_IF_NP;
        }
        else
        {
            u1OperStatus = CFA_IF_DOWN;
        }

        if (CfaGetIfOperStatus (u4IfIndex, &u1OldOperStatus) == CFA_SUCCESS)
        {
            CfaGetIfType (u4IfIndex, &u1IfType);

            if (u1OperStatus != u1OldOperStatus)
            {
                if (u1IfType == CFA_ENET)
                {
                    /*
                     * u1IsFromMib can take 3 values -
                     *    CFA_TRUE  - Status change is from Mgmt 
                     *    CFA_IF_LINK_STATUS_CHANGE - 
                     *          Status change is from Ethernet
                     *          Link Layer.
                     *    CFA_FALSE - Otherwise      
                     */
                    u1IsFromMib = CFA_IF_LINK_STATUS_CHANGE;
                }
                else
                {
                    u1IsFromMib = CFA_FALSE;
                }

                CfaIfmHandleInterfaceStatusChange
                    ((UINT2) u4IfIndex, CFA_IF_ADMIN
                     (u4IfIndex), u1OperStatus, u1IsFromMib);
            }
        }
    }

    L2CfaRedSetBridgePortOperStatus (u4IfIndex, (UINT1) u4Value);
    /* Copy physical port properties to the SISP ports created over it */
    CfaCopyBridgePortPropToSispPorts (u4IfIndex,
                                      CFA_PORT_PROP_OPER_STATUS,
                                      (UINT4) u1OperStatus);
}

/*****************************************************************************/
/* Function Name      : CfaRedCreateAuditTask                                */
/*                                                                           */
/* Description        : This function will invoke the audit                  */
/*                      start function to start the audit process.           */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
CfaRedCreateAuditTask (VOID)
{
    UINT4               u4RetValue = 0;

    UNUSED_PARAM (u4RetValue);
    /* Start performing the audit */
    CFA_RED_AUDIT_FLAG () = CFA_RED_AUDIT_START;

    if (CFA_AUDIT_TASK_ID () == 0)
    {
        /* Doing audit for the first time */
        u4RetValue = CFA_SPAWN_TASK (CFA_AUDIT_TASK,
                                     CFA_AUDIT_TASK_PRIORITY,
                                     OSIX_DEFAULT_STACK_SIZE,
                                     (OsixTskEntry) CfaRedAuditMain,
                                     0, &(CFA_AUDIT_TASK_ID ()));
    }

    if (CFA_AUDIT_TASK_ID () != 0)
    {
        CFA_SEND_EVENT (CFA_AUDIT_TASK_ID (), CFA_RED_AUDIT_START_EVENT);
    }

}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : CfaRedAuditMain                                         */
/*                                                                            */
/*  Description     : This the main routine for the CFA Audit submodule.      */
/*                                                                            */
/*  Input(s)        : pi1Param                                                */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
CfaRedAuditMain (INT1 *pi1Param)
{
    UINT4               u4Events;

    UNUSED_PARAM (pi1Param);

    while (1)
    {
        if ((CFA_RECEIVE_EVENT (CFA_AUDIT_TASK_ID (),
                                (CFA_RED_AUDIT_START_EVENT |
                                 CFA_RED_AUDIT_TSK_DEL_EVENT),
                                OSIX_WAIT, &u4Events)) == OSIX_SUCCESS)
        {
            if (u4Events & CFA_RED_AUDIT_TSK_DEL_EVENT)
            {
                /* Delete audit task, in suicide manner */
#ifdef NPAPI_WANTED
                CfaRedDeleteAuditTask ();
#endif
            }
            if (u4Events & CFA_RED_AUDIT_START_EVENT)
            {
#ifdef NPAPI_WANTED
                CfaRedStartAudit ();
#endif
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : CfaRedHandleDynSyncAudit                             */
/*                                                                           */
/* Description        : This function Handles the Dynamic Sync-up Audit      */
/*                      event. This event is used to start the audit         */
/*                      between the active and standby units for the         */
/*                      dynamic sync-up happened                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
CfaRedHandleDynSyncAudit ()
{
    CfaExecuteCmdAndCalculateChkSum ();
    return;
}

/*****************************************************************************/
/* Function Name      : CfaExecuteCmdAndCalculateChkSum                      */
/*                                                                           */
/* Description        : This function Handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
CfaExecuteCmdAndCalculateChkSum (VOID)
{
    /*Execute CLI commands and calculate checksum */
#ifdef RM_WANTED
    UINT2               u2AppId = RM_CFA_APP_ID;
#endif
    UINT2               u2ChkSum = 0;

    CFA_UNLOCK ();
    if (CfaGetShowCmdOutputAndCalcChkSum (&u2ChkSum) == CFA_FAILURE)
    {
        CFA_LOCK ();
        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                 "Checksum of calculation failed for VCM\n");
        return;
    }
#ifdef RM_WANTED

    if (CfaRmEnqChkSumMsgToRm (u2AppId, u2ChkSum) == CFA_FAILURE)
    {
        CFA_LOCK ();
        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN, "Sending checkum to RM failed\n");
        return;
    }
#endif
    CFA_LOCK ();
    return;
}

#ifdef NPAPI_WANTED
/******************************************************************************/
/*                                                                            */
/*  Function Name   : CfaRedStartAudit                                        */
/*                                                                            */
/*  Description     : This function performs audit of the CFA Virtual ports   */
/*                    and ILAN between the hardware and the software.         */
/*                    This function will be invoked whenever the              */
/*                    AUDIT_START_EVENT is received. This function            */
/*                    synchronizes the CFA virtual ports and ILAN created     */
/*                    between the hardware and the software.                  */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
CfaRedStartAudit (VOID)
{
    tCfaBufferEntry    *pBuf = NULL;
    tCfaBufferEntry    *pTempBuf = NULL;

    if (CFA_RED_AUDIT_FLAG () != CFA_RED_AUDIT_START)
    {
        return;
    }

    /* Scan the Buffer entries and delete it */
    TMO_DYN_SLL_Scan (&CfaRedBufferSll, pBuf, pTempBuf, tCfaBufferEntry *)
    {
        if (CFA_RED_AUDIT_FLAG () != CFA_RED_AUDIT_START)
        {
            return;
        }

        CFA_NPSYNC_BLK () = OSIX_FALSE;
        /* First process all Delete port entries, so that the ports
           are created in the hardware before programming their 
           properties */
        if (pBuf->u4NpApiId == NPSYNC_FS_CFA_HW_DELETE_INTERNAL_PORT)
        {
            CfaRedAuditBufferEntry (pBuf);
            TMO_SLL_Delete (&CfaRedBufferSll, &pBuf->Node);
            MemReleaseMemBlock (CFA_RED_BUF_MEMPOOL (), (UINT1 *) pBuf);
            pBuf = NULL;
        }
    }

    pBuf = NULL;
    pTempBuf = NULL;

    /* Scan the rest of the Buffer entries and delete it */
    TMO_DYN_SLL_Scan (&CfaRedBufferSll, pBuf, pTempBuf, tCfaBufferEntry *)
    {
        if (CFA_RED_AUDIT_FLAG () != CFA_RED_AUDIT_START)
        {
            return;
        }

        CFA_NPSYNC_BLK () = OSIX_FALSE;
        CfaRedAuditBufferEntry (pBuf);
        TMO_SLL_Delete (&CfaRedBufferSll, &pBuf->Node);
        MemReleaseMemBlock (CFA_RED_BUF_MEMPOOL (), (UINT1 *) pBuf);
        pBuf = NULL;
    }

}

/*****************************************************************************/
/* Function Name      : CfaRedDeleteAuditTask                                */
/*                                                                           */
/* Description        : This function will delete the Audit Task if running  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
CfaRedDeleteAuditTask (VOID)
{
    tOsixTaskId         TempTaskId;
    tCfaBufferEntry    *pBuf = NULL;
    tCfaBufferEntry    *pTempBuf = NULL;

    if (CFA_RED_AUDIT_FLAG () == CFA_RED_AUDIT_CLEAN_AND_STOP)
    {
        /* This scenario would come in the following scenario,
         *   -- Audit task spawned, in STANDBY->ACTIVE scenario
         *   -- Audit is going on
         *   -- Before Audit completes, ACTIVE->STANDBY event comes,
         *
         *   Then, delete the remaining audit entries in SLL */

        TMO_DYN_SLL_Scan (&CfaRedBufferSll, pBuf, pTempBuf, tCfaBufferEntry *)
        {
            TMO_SLL_Delete (&CfaRedBufferSll, &pBuf->Node);
            MemReleaseMemBlock (CFA_RED_BUF_MEMPOOL (), (UINT1 *) pBuf);
        }
    }

    /* Store PBB AUDIT Task ID in temporary variable, before deleting the
     * task */
    TempTaskId = CFA_AUDIT_TASK_ID ();

    /* Make the Audit task id to zero */
    CFA_AUDIT_TASK_ID () = 0;

    /* Delete the task in suicidal manner */
    CFA_DELETE_TASK (TempTaskId);

    /* Task would have terminated in the previous step. No step 
     * should be done after this */
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : CfaRedAuditCreateInternalPort                           */
/*                                                                            */
/*  Description     : This function performs audit of the Interal Port        */
/*                    If not there in Software then delete in from Hardware   */
/*                                                                            */
/*  Input(s)        : pBuf - Buffer Entry                                     */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
CfaRedAuditCreateInternalPort (tCfaBufferEntry * pBuf)
{
    unNpSync            NpSync;
    UINT4               u4IfIndex;

    NpSync = pBuf->unNpData;
    u4IfIndex = NpSync.FsCfaHwCreateInternalPort.u4IfIndex;

    if (CfaIsVirtualInterface (u4IfIndex) == CFA_FALSE)
    {
        if (CfaIsVipInterface (u4IfIndex) == CFA_FALSE)
        {
            /* If neither Internal nor VIP then delete it */
            CFA_NPSYNC_BLK () = OSIX_TRUE;
            FsCfaHwDeleteInternalPort (u4IfIndex);
            return;
        }
        else                    /* If VIP Interface */
        {
            if (CfaValidateCfaIfIndex (u4IfIndex) != CFA_SUCCESS)
            {
                CFA_NPSYNC_BLK () = OSIX_TRUE;
                FsCfaHwDeleteInternalPort (u4IfIndex);
                return;
            }
        }
    }
    else                        /* If Virtual Port */
    {
        if (!CFA_IF_ENTRY_USED (u4IfIndex))
        {
            CFA_NPSYNC_BLK () = OSIX_TRUE;
            FsCfaHwDeleteInternalPort (u4IfIndex);
            return;
        }
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : CfaRedAuditDelInternalPort                              */
/*                                                                            */
/*  Description     : This function performs audit of the Interal Port        */
/*                    If there in Software then create in Hardware also       */
/*                                                                            */
/*  Input(s)        : pBuf - Buffer Entry                                     */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
CfaRedAuditDelInternalPort (tCfaBufferEntry * pBuf)
{
    tCfaIfInfo          IfInfo;
    unNpSync            NpSync;
    UINT4               u4IfIndex;
    UINT4               u4ContextId;
    UINT2               u2Port;
    UINT1               u1CreatePort = CFA_FALSE;
    UINT1               u1AdminStatus = CFA_IF_DOWN;

    NpSync = pBuf->unNpData;
    u4IfIndex = NpSync.FsCfaHwDeleteInternalPort.u4IfIndex;

    if (CfaIsVirtualInterface (u4IfIndex) == CFA_FALSE)
    {
        if (CfaIsVipInterface (u4IfIndex) == CFA_TRUE)
        {
            if (CfaValidateCfaIfIndex (u4IfIndex) == CFA_SUCCESS)
            {
                u1CreatePort = CFA_TRUE;
            }
        }
    }
    else                        /* Virtual Interface */
    {
        if (CFA_IF_ENTRY_USED (u4IfIndex))
        {
            u1CreatePort = CFA_TRUE;
        }
    }

    if (u1CreatePort == CFA_TRUE)
    {
        CFA_NPSYNC_BLK () = OSIX_TRUE;
        FsCfaHwCreateInternalPort (u4IfIndex);

        if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
        {
            return;
        }

        if (VcmGetContextInfoFromIfIndex (u4IfIndex,
                                          &u4ContextId, &u2Port) != VCM_SUCCESS)
        {
            return;
        }

        if (IfInfo.u1IfType != CFA_LAGG)
        {
            VcmMapPortToContextInHw (u4ContextId, u4IfIndex);
        }

        /* Only Set the the internal CBP, PIP */
        if (CfaIsVipInterface (u4IfIndex) == CFA_FALSE)
        {
            CfaGetCdbPortAdminStatus (u4IfIndex, &u1AdminStatus);
            if (CfaIsMgmtPort (u4IfIndex) == FALSE)
            {
                if (u1AdminStatus == CFA_IF_UP)
                {
                    if (CfaFsHwUpdateAdminStatusChange (u4IfIndex,
                                                        FNP_TRUE) !=
                        FNP_SUCCESS)
                    {
                        return;
                    }
                }
            }
        }
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : CfaRedAuditCreateIlan                                   */
/*                                                                            */
/*  Description     : This function performs audit of the ILAN Port           */
/*                    If not there in Software then delete it from Hardware   */
/*                                                                            */
/*  Input(s)        : pBuf - Buffer Entry                                     */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
CfaRedAuditCreateIlan (tCfaBufferEntry * pBuf)
{
    unNpSync            NpSync;
    UINT4               u4IfIndex;
    UINT1               u1IfType = CFA_INVALID_TYPE;

    NpSync = pBuf->unNpData;
    u4IfIndex = NpSync.FsCfaHwCreateILan.u4ILanIndex;

    CfaGetIfType (u4IfIndex, &u1IfType);

    if ((CfaIsILanInterface (u4IfIndex) == CFA_FALSE) || (u1IfType != CFA_ILAN))
    {
        CFA_NPSYNC_BLK () = OSIX_TRUE;
        FsCfaHwDeleteILan (u4IfIndex);
        return;
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : CfaRedAuditDeleteIlan                                   */
/*                                                                            */
/*  Description     : This function performs audit of the Deleted ILAN Port   */
/*                    If there in Software then create it again in Hardware   */
/*                                                                            */
/*  Input(s)        : pBuf - Buffer Entry                                     */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
CfaRedAuditDeleteIlan (tCfaBufferEntry * pBuf)
{
    unNpSync            NpSync;
    tHwPortArray        HwPortArray;
    UINT4               u4IfIndex;
    UINT1               u1IfType = CFA_INVALID_TYPE;

    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));
    NpSync = pBuf->unNpData;
    u4IfIndex = NpSync.FsCfaHwDeleteILan.u4ILanIndex;

    CfaGetIfType (u4IfIndex, &u1IfType);

    if ((CfaIsILanInterface (u4IfIndex) == CFA_TRUE) && (u1IfType == CFA_ILAN))
    {
        CFA_NPSYNC_BLK () = OSIX_TRUE;
        FsCfaHwCreateILan (u4IfIndex, HwPortArray);
        return;
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : CfaRedAuditAddPortIlan                                  */
/*                                                                            */
/*  Description     : This function performs audit of the Port added in ILAN. */
/*                    If not there in Software then delete in from Hardware   */
/*                                                                            */
/*  Input(s)        : pBuf - Buffer Entry                                     */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
CfaRedAuditAddPortIlan (tCfaBufferEntry * pBuf)
{
    tTMO_SLL           *pIfStack = NULL;
    tStackInfoStruct   *pStackListScan;
    unNpSync            NpSync;
    UINT4               u4ILanIndex;
    UINT4               u4IfIndex;
    UINT1               u1StackFound = CFA_FALSE;

    NpSync = pBuf->unNpData;
    u4ILanIndex = NpSync.FsCfaHwAddPortToILan.u4ILanIndex;
    u4IfIndex = NpSync.FsCfaHwAddPortToILan.u4PortIndex;

    /* If any entry is not present then remove the port
       from the ILAN */
    if ((CfaIsILanInterface (u4ILanIndex) == CFA_FALSE) ||
        (CfaIsVirtualInterface (u4IfIndex) == CFA_FALSE) ||
        (!CFA_IF_ENTRY_USED (u4IfIndex)) || (!CFA_IF_ENTRY_USED (u4ILanIndex)))
    {
        CFA_NPSYNC_BLK () = OSIX_TRUE;
        FsCfaHwRemovePortFromILan (u4ILanIndex, u4IfIndex);
        return;
    }

    /* Get the Lower stack for the Port */
    pIfStack = &CFA_IF_STACK_LOW (u4IfIndex);

    if (pIfStack == NULL)
    {
        CFA_NPSYNC_BLK () = OSIX_TRUE;
        FsCfaHwRemovePortFromILan (u4ILanIndex, u4IfIndex);
        return;
    }
    else
    {
        /* Find if the ILAN port is there or not */
        TMO_SLL_Scan (pIfStack, pStackListScan, tStackInfoStruct *)
        {
            if (pStackListScan->u4IfIndex == u4ILanIndex)
            {
                /* Stack entry exist with the given ILAN port */
                u1StackFound = CFA_TRUE;
                break;
            }
        }
    }

    /* No Stack entry exist in software so remove it from
       the hardware also */
    if (u1StackFound == CFA_FALSE)
    {
        CFA_NPSYNC_BLK () = OSIX_TRUE;
        FsCfaHwRemovePortFromILan (u4ILanIndex, u4IfIndex);
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : CfaRedAuditDelPortIlan                                  */
/*                                                                            */
/*  Description     : This function performs audit of the Port deleted in     */
/*                    ILAN. If found in Software then create in from Hardware */
/*                                                                            */
/*  Input(s)        : pBuf - Buffer Entry                                     */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
CfaRedAuditDelPortIlan (tCfaBufferEntry * pBuf)
{
    unNpSync            NpSync;
    tTMO_SLL           *pIfStack = NULL;
    tStackInfoStruct   *pStackListScan;
    UINT4               u4ILanIndex;
    UINT4               u4IfIndex;

    NpSync = pBuf->unNpData;
    u4ILanIndex = NpSync.FsCfaHwRemovePortFromILan.u4ILanIndex;
    u4IfIndex = NpSync.FsCfaHwRemovePortFromILan.u4PortIndex;

    /* If any entry is not present then just do not do anything */
    if ((CfaIsILanInterface (u4ILanIndex) == CFA_FALSE) ||
        (CfaIsVirtualInterface (u4IfIndex) == CFA_FALSE) ||
        (!CFA_IF_ENTRY_USED (u4IfIndex)) || (!CFA_IF_ENTRY_USED (u4ILanIndex)))
    {
        return;
    }

    /* Get the Lower stack for the Port */
    pIfStack = &CFA_IF_STACK_LOW (u4IfIndex);

    if (pIfStack != NULL)
    {
        /* Find if the ILAN port is there or not */
        TMO_SLL_Scan (pIfStack, pStackListScan, tStackInfoStruct *)
        {
            if (pStackListScan->u4IfIndex == u4ILanIndex)
            {
                /* Stack entry exist with the given ILAN port */
                CFA_NPSYNC_BLK () = OSIX_TRUE;
                FsCfaHwAddPortToILan (u4ILanIndex, u4IfIndex);
                break;
            }
        }
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : CfaRedAuditBufferEntry                                  */
/*                                                                            */
/*  Description     : This function performs audit for the buffer entry       */
/*                    present in the Red Buffer SLL                           */
/*                                                                            */
/*  Input(s)        : pBuf - Buffer Entry                                     */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
CfaRedAuditBufferEntry (tCfaBufferEntry * pBuf)
{
    INT4                i4NpapiId = 0;

    i4NpapiId = (INT4) (pBuf->u4NpApiId);

    switch (i4NpapiId)
    {
        case NPSYNC_FS_CFA_HW_CREATE_INTERNAL_PORT:
        {
            CfaRedAuditCreateInternalPort (pBuf);
        }
            break;

        case NPSYNC_FS_CFA_HW_DELETE_INTERNAL_PORT:
        {
            CfaRedAuditDelInternalPort (pBuf);
        }
            break;

        case NPSYNC_FS_CFA_HW_ADD_PORT_TO_I_LAN:
        {
            CfaRedAuditAddPortIlan (pBuf);
        }
            break;

        case NPSYNC_FS_CFA_HW_REMOVE_PORT_FROM_I_LAN:
        {
            CfaRedAuditDelPortIlan (pBuf);
        }
            break;

        case NPSYNC_FS_CFA_HW_CREATE_I_LAN:
        {
            CfaRedAuditCreateIlan (pBuf);
        }
            break;

        case NPSYNC_FS_CFA_HW_DELETE_I_LAN:
        {
            CfaRedAuditDeleteIlan (pBuf);
        }
            break;

        default:
            break;
    }

    CFA_NPSYNC_BLK () = OSIX_FALSE;

}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : CfaHwAuditCreateOrFlushBuffer                           */
/*                                                                            */
/*  Description     : This function performs cehcks whether a buffer entry    */
/*                    is already present or not. If not present then create it*/
/*                                                                            */
/*  Input(s)        : pBuf - Buffer Entry                                     */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/

VOID
CfaHwAuditCreateOrFlushBuffer (unNpSync * pNpSync, UINT4 u4NpApiId,
                               UINT4 u4EventId)
{
    tCfaBufferEntry    *pBuf = NULL;
    tCfaBufferEntry    *pTempBuf = NULL;
    tCfaBufferEntry    *pPrevNode = NULL;
    UINT1               u1Match = CFA_FALSE;

    UNUSED_PARAM (u4EventId);

    if (u4NpApiId == 0)
    {
        /* Currently we are not having any Event sync up is CFA */
        return;
    }

    /* Scan the Buffer entries and delete it */
    TMO_DYN_SLL_Scan (&CfaRedBufferSll, pBuf, pTempBuf, tCfaBufferEntry *)
    {
        if (u4NpApiId == pBuf->u4NpApiId)
        {
            if (MEMCMP (&(pBuf->unNpData), pNpSync, sizeof (unNpSync)) == 0)
            {
                TMO_SLL_Delete (&CfaRedBufferSll, &pBuf->Node);
                MemReleaseMemBlock (CFA_RED_BUF_MEMPOOL (), (UINT1 *) pBuf);
                pBuf = NULL;
                u1Match = CFA_TRUE;
                break;
            }
        }

        pPrevNode = pBuf;
    }

    if (u1Match == CFA_FALSE)
    {
        pTempBuf = NULL;
        /* allocate for the tunnel table entry */
        pTempBuf = (tCfaBufferEntry *) MemAllocMemBlk (CFA_RED_BUF_MEMPOOL ());

        if (pTempBuf == NULL)
        {
            return;
        }

        MEMSET (pTempBuf, 0, sizeof (tCfaBufferEntry));

        TMO_SLL_Init_Node (&pTempBuf->Node);

        MEMCPY (&pTempBuf->unNpData, pNpSync, sizeof (unNpSync));
        pTempBuf->u4NpApiId = u4NpApiId;

        if (pPrevNode == NULL)
        {
            /* Node to be inserted in first position */
            TMO_SLL_Insert (&CfaRedBufferSll, NULL,
                            (tTMO_SLL_NODE *) & (pTempBuf->Node));
        }
        else
        {
            /* Node to be inserted in last position */
            TMO_SLL_Insert (&CfaRedBufferSll,
                            (tTMO_SLL_NODE *) & (pPrevNode->Node),
                            (tTMO_SLL_NODE *) & (pTempBuf->Node));
        }
    }
}
#endif
