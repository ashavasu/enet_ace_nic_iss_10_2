/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddwasp.c,v 1.9 2017/11/24 10:36:58 siva Exp $
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/
#include "cfainc.h"
#include "fsvlan.h"
#include "vlangarp/garp.h"
#ifdef NPAPI_WANTED
#ifndef CFA_INTERRUPT_MODE
#error "Enable INTERRUPT_MODE in CFA"
#endif
#include "cfanp.h"
#endif
#include "gddwaspapi.h"
#include "cfaport.h"

#include <fcntl.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>

static INT4         gi4PortDesc;

PRIVATE INT4        NpCfaIsVlanTagRequest (tCRU_BUF_CHAIN_HEADER * pBuf);
UINT1              *CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
VOID                CfaGddSetLnxIntfnameForPort (UINT4 u4Index,
                                                 UINT1 *pu1IfName);

PUBLIC INT4         CfaGddConfigIp (UINT4 u4IpAddr);

extern INT4         CustNpSend80211Frame (UINT4 u4WlanIfIndex, UINT1 *u1PktData,
                                          UINT1 type, UINT1 subtype,
                                          INT4 i4Len);
#ifdef LNXIP4_WANTED
static UINT1
     
     
     
     au1IntMapTable[SYS_DEF_MAX_PHYSICAL_INTERFACES][2]
    [CFA_MAX_PORT_NAME_LENGTH];
#else
        /* Array containing the Mapping between Slot and Eth Interfaces
         *  * In case of LinuxIP, ports are referred based on the Linux Eth name
         *   * to which it is mapped */
static UINT1
     
     
     
     au1IntMapTable[SYS_DEF_MAX_PHYSICAL_INTERFACES][2]
    [CFA_MAX_PORT_NAME_LENGTH] = { {"Slot0/1", "eth0"},
{"Slot0/2", "eth1"},
{"Slot0/3", "eth2"},
{"Slot0/4", "eth3"},
{"Slot0/5", "eth4"},
{"Slot0/6", "eth5"},
{"Slot0/7", "eth6"},
{"Slot0/8", "eth7"},
{"Slot0/9", "eth8"},
{"Slot0/10", "eth9"},
{"Slot0/11", "eth10"},
{"Slot0/12", "eth11"},
{"Slot0/13", "eth12"},
{"Slot0/14", "eth13"},
{"Slot0/15", "eth14"},
{"Slot0/16", "eth15"},
{"Slot0/17", "eth16"},
{"Slot0/18", "eth17"},
{"Slot0/19", "eth18"},
{"Slot0/20", "eth19"},
{"Slot0/21", "eth20"},
{"Slot0/22", "eth21"},
{"Slot0/23", "eth22"},
{"Slot0/24", "eth23"}
};
#endif

/*****************************************************************************
 *    Function Name      : CfaGddProcessRecvInterruptEvent ()
 *    Description        : This function is invoked when an driver 
 *                         event occurs. This routine invokes the packet
 *                         processing function if a packet is received 
 *                         else it invokes the function which process the
 *                         driver status if status indication message is
 *                         received
 *    Input(s)           : VOID
 *    Output(s)          : None.
 
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Returns                  : None.
 *****************************************************************************/

PUBLIC INT4
CfaGddProcessRecvInterruptEvent (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pCruBuf;
    UINT4               u4IfIndex;
    UINT4               u4PktSize;
    UINT1               au1EnetHdr[VLAN_TAG_OFFSET];
    UINT1               u1IfType;

    while (OsixReceiveFromQ
           (SELF, (UINT1 *) CFA_PACKET_QUEUE_MUX, OSIX_NO_WAIT, 0,
            (tCRU_BUF_CHAIN_HEADER **) & pCruBuf) == 0)
    {
        CFA_LOCK ();

        /* extract the interface index and packet size from buffer */
        if (NpCfaIsVlanTagRequest (pCruBuf) == CFA_FALSE)
        {
            CRU_BUF_Copy_FromBufChain (pCruBuf, au1EnetHdr, 0, VLAN_TAG_OFFSET);
            CRU_BUF_Move_ValidOffset (pCruBuf,
                                      VLAN_TAG_OFFSET + VLAN_TAG_PID_LEN);
            CRU_BUF_Prepend_BufChain (pCruBuf, au1EnetHdr, VLAN_TAG_OFFSET);
        }

        u4IfIndex = pCruBuf->ModuleData.InterfaceId.u4IfIndex;
        u4PktSize = pCruBuf->pFirstValidDataDesc->u4_ValidByteCount;

        if (CfaGetIfType (u4IfIndex, &u1IfType) == CFA_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                     "In CfaMain Pkt recvd in interrupt mode failed to deliver to HL.\n");
        }
        else
        {
            /* Handover the Packet to Receive module */
            if ((CFA_GDD_HL_RX_FNPTR (u4IfIndex))
                (pCruBuf, u4IfIndex, u4PktSize, u1IfType,
                 CFA_ENCAP_NONE) != CFA_SUCCESS)
            {
                /* release buffer which were not successfully sent to higher layer
                 */
                CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "In CfaMain Pkt recvd in interrupt mode failed to deliver to HL.\n");
            }
            else
            {
                /* increment after successful handling of packet */
                CFA_IF_SET_IN_OCTETS (u4IfIndex, u4PktSize);
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "In CfaMain Pkt recvd in interrupt mode delivered to HL.\n");
            }
        }
        CFA_UNLOCK ();
    }

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *    Function Name       : CfaGddInit
 *    Description         : This function performs the initialisation of
 *                          the  Device Driver Module of CFA. This routine
 *                          1. creates the Queue for reception of packets from
 *                             driver
 *                          2. creates the memory pool for the driver messages
 *                          3. initializes the driver to cfa mapping tables
 *    Input(s)            : None.
 *    Output(s)           : None.
 *
 *    Global Variables Referred : None 
 *    Global Variables Modified : gaCfaNpIndexMap 
 *                               
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if initialisation succeeds,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
CfaGddInit (VOID)
{
    tMacAddr            au1SwitchMac;    /* Base MAC address */

    MEMSET (au1SwitchMac, 0, CFA_ENET_ADDR_LEN);

    /* Getting the Switch MAC address */
    //   CfaGetSysMacAddress (au1SwitchMac);

    /* Updating MAC address for all ports */
    //CfaNpUpdatePortMac (au1SwitchMac); 

    /* Initialize the Board and UnitInfo structures */
    /*if (CfaNpInit () != FNP_SUCCESS)
       {
       PRINTF ("ERROR[NP] - CfaNpInit returned failure\n\r");
       return (CFA_FAILURE);
       } */
    return (CFA_SUCCESS);
}

#ifdef MBSM_WANTED
/*****************************************************************************
 *    Function Name       : CfaMbsmGddInit
 *    Description         : This function performs the initialisation of
 *                          the  Device Driver Module of CFA. This routine
 *                          1. creates the Queue for reception of packets from
 *                             driver
 *                          2. creates the memory pool for the driver messages
 *                          3. initializes the driver to cfa mapping tables
 *    Input(s)            : None.
 *    Output(s)           : None.
 *
 *    Global Variables Referred : _devices, _ndevices
 *    Global Variables Modified : gFsDrvMemPoolId, gaCfaNpIndexMap
 *                                gaCfaNpDevMap
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if initialisation succeeds,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

PUBLIC INT4
CfaMbsmGddInit (tMbsmSlotInfo * pSlotInfo)
{
    /* Update the Driver to cfa mapping tables for the given slot.
     */
    UNUSED_PARAM (pSlotInfo);

    return CFA_SUCCESS;
}

#endif /* MBSM_WANTED */

/*****************************************************************************
 *    Function Name        : CfaGddShutdown
 *    Description         : This function performs the initialisation of
 *                the Generic Device Driver Module of CFA. This
 *                initialization routine should be called
 *                from the init of IFM after we have obtained
 *                the number of physical ports after parsing of
 *                the Config file. This function initializes the FD list
 *                and ifIndex array of the polling table.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : FdTable
 *
 *    Global Variables Modified : FdTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

PUBLIC VOID
CfaGddShutdown (VOID)
{
    return;
}

#ifdef MBSM_WANTED
/*****************************************************************************
 *    Function Name        : CfaMbsmGddDeInit
 *    Description         : This function performs the shutdown of
 *                the Generic Device Driver Module of CFA. This
 *                shutdown routine should be called
 *                before the protocols are informed about the shutdown.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : FdTable
 *
 *    Global Variables Modified : FdTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
CfaMbsmGddDeInit (tMbsmSlotInfo * pSlotInfo)
{
    /* Re-initialize the Slot info paramaters for the given slot */
    UNUSED_PARAM (pSlotInfo);

    return CFA_SUCCESS;
}

#endif /* MBSM_WANTED */

/*****************************************************************************
 *
 *    Function Name        : CfaGddOsRemoveIfFromList
 *
 *    Description        : Function is to remove the Ifindex from the port list
 *
 *    Input(s)           : UINT4 u4IfIndex.
 *
 *    Output(s)          : None
 *
 *    Returns            : None
 *
 *****************************************************************************/
VOID
CfaGddOsRemoveIfFromList (UINT4 u4IfIndex)
{
    (VOID) u4IfIndex;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddOsProcessRecvEvent
 *
 *    Description        : Function is to process the Recv Event from os
 *
 *    Input(s)           : None
 *
 *    Output(s)          : None
 *
 *    Returns            : CFA_SUCCESS
 *
 *****************************************************************************/
INT4
CfaGddOsProcessRecvEvent (VOID)
{
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddGetOsHwAddr
 *
 *    Description        : Function is called for getting the hardware
 *                         address of the Enet ports at Interface initialisation.
 *                         This function can be called only at port creation time.
 *                         The MAC address is obtained in the same way as it is used
 *                         for IndexMap table initialisation at GddInit.
 *
 *    Input(s)           : UINT4 u4IfIndex.
 *
 *    Output(s)          : UINT1 *au1HwAddr.
 *
 *    Returns            : CFA_SUCCESS if address is obtained,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaGddGetOsHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
    if (CfaIsL3IpVlanInterface (u4IfIndex) == CFA_SUCCESS)
    {
        /* For IVR interfaces, Get the Switch Mac Address from CDB table */
        CfaGetIfHwAddr (u4IfIndex, au1HwAddr);
        return CFA_SUCCESS;
    }

    /* Copying the MAC address from the Global variable */
    //CfaNpGetHwAddr ((UINT2) u4IfIndex, au1HwAddr); 

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddConfigPort
 *
 *    Description        : Function is called for configuration of the
 *                Enet ports. No such support is available for
 *                Wanic at present. Only the Promiscuous mode
 *                and multicast addresses can be configured at
 *                present for Enet ports. This is called
 *                directly whenever the Manager configures the
 *                ifTable for Ethernet ports. It can also be
 *                called indirectly by Bridge or RIP, etc.
 *                through the CfaIfmEnetConfigMcastAddr API.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *                UINT1 u1ConfigOption,
 *                VOID *pConfigParam.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if configuration succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption, VOID *pConfigParam)
{
    struct sockaddr_ll  Enet;
    struct ifreq        ifr;
    UINT1              *pu1PortName = (UINT1 *) "eth0";
    INT4                i4IfFlags = 0;
    INT4                i4CommandStatus = 0;

    MEMSET (&Enet, 0, sizeof (Enet));
    MEMSET (&ifr, 0, sizeof (ifr));

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pConfigParam);
    UNUSED_PARAM (u1ConfigOption);

    /* Open a Socket */
    if (gi4PortDesc == 0)
    {
        gi4PortDesc = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_ALL));
        if (gi4PortDesc < 0)
        {
            perror (" Socket Creation Failed\n");
        }
        else
        {
            sprintf (ifr.ifr_name, "%s", pu1PortName);
            if (ioctl (gi4PortDesc, SIOCGIFINDEX, (char *) &ifr) < 0)
            {
                close (gi4PortDesc);
                return CFA_FAILURE;
            }

            Enet.sll_family = AF_PACKET;
            Enet.sll_ifindex = ifr.ifr_ifindex;

            if (bind (gi4PortDesc, (struct sockaddr *) &Enet, sizeof (Enet)) <
                0)
            {
                perror ("Bind error\n");
                close (gi4PortDesc);
                return CFA_FAILURE;
            }

            /* set options for non-blocking mode */
            if (fcntl (gi4PortDesc, F_SETFL, O_NONBLOCK) < 0)
            {
                perror ("Failure in setting ETH SOCK option\n");
                close (gi4PortDesc);
            }

            if ((i4CommandStatus = ioctl (gi4PortDesc, SIOCGIFFLAGS, &ifr)) < 0)
            {
                perror ("Error in getting interface flags\n");
            }
            i4IfFlags = ifr.ifr_flags;
            ifr.ifr_flags = ifr.ifr_flags | IFF_PROMISC;
            if ((i4CommandStatus = ioctl (gi4PortDesc, SIOCSIFFLAGS, &ifr)) < 0)
            {
                perror ("Error in setting promisc mode\n");
            }
        }
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddSetGddType
 *
 *    Description        : Function is to get the GDD type set.
 *
 *    Input(s)           : UINT4 u4IfIndex.
 *
 *    Output(s)          : None
 *
 *    Returns            : CFA_SUCCESS.
 *
 *****************************************************************************/
INT4
CfaGddSetGddType (UINT4 u4IfIndex)
{
    UINT1               u1IfType;

    CfaGetIfType (u4IfIndex, &u1IfType);

    CFA_GDD_TYPE (u4IfIndex) = u1IfType;

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthSockOpen
 *
 *    Description        : Function is to open the GDD Ethernet Socket.
 *
 *    Input(s)           : UINT4 u4IfIndex.
 *
 *    Output(s)          : None
 *
 *    Returns            : CFA_SUCCESS.
 *
 *****************************************************************************/
PUBLIC INT4
CfaGddEthSockOpen (UINT4 u4IfIndex)
{
    INT4                i4RetVal = CFA_SUCCESS;
    UNUSED_PARAM (u4IfIndex);
    return i4RetVal;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthSockClose
 *
 *    Description        : Function is to close the GDD Ethernet Socket.
 *
 *    Input(s)           : UINT4 u4IfIndex.
 *
 *    Output(s)          : None
 *
 *    Returns            : CFA_SUCCESS.
 *
 *****************************************************************************/
PUBLIC INT4
CfaGddEthSockClose (UINT4 u4IfIndex)
{
    INT4                i4RetVal = CFA_SUCCESS;
    UNUSED_PARAM (u4IfIndex);
    return i4RetVal;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthSockRead
 *
 *    Description        : Function is to read the Ethernet Socket
 *
 *    Input(s)           : UINT4 u4IfIndex.
 *               pu1DataBuf - Buffer.
 *
 *    Output(s)          : pu4PktSize - Packet size
 *
 *    Returns            : CFA_SUCCESS.
 *
 *****************************************************************************/
PUBLIC INT4
CfaGddEthSockRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    INT4                i4RetVal = CFA_FAILURE;
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (pu4PktSize);
    return i4RetVal;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddOsAddIfToList
 *
 *    Description        : Function is to add the interface to the list.
 *
 *    Input(s)           : UINT4 u4IfIndex.
 *
 *    Output(s)          : None
 *
 *    Returns            : CFA_SUCCESS.
 *
 *****************************************************************************/
PUBLIC VOID
CfaGddOsAddIfToList (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
}

/* Link up/down changes start */
/*****************************************************************************
 *    Function Name        : CfaGddGetLinkStatus
 *    Description         : This function  retrives the status of the Link
 *    Input(s)            : None.
 *    Output(s)            : None.
 *    Global Variables Referred : FdTable
 *    Global Variables Modified : FdTable
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Use of Recursion        : None.
 *    Returns            : CFA_IF_DOWN if the Link is Down
 *                         else, CFA_IF_UP.
 *
 *****************************************************************************/
PUBLIC UINT1
CfaGddGetLinkStatus (UINT4 u4IfIndex)
{
    return CfaNpGetLinkStatus (u4IfIndex);
}

/*****************************************************************************
 *    Function Name        : CfaGddGetPhyAndLinkStatus
 *    Description         : This function  retrives the status of the Link and
 *                          PHY
 *    Input(s)            : None.
 *    Output(s)            : None.
 *    Global Variables Referred : FdTable
 *    Global Variables Modified : FdTable
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Use of Recursion        : None.
 *    Returns            : CFA_IF_DOWN if the Link  or PHY is Down
 *                         else, CFA_IF_UP both LINK and PHY is up.
 *
 *****************************************************************************/

PUBLIC UINT1
CfaGddGetPhyAndLinkStatus (UINT2 u2IfIndex)
{
    return CfaNpGetPhyAndLinkStatus (u2IfIndex);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthOpen
 *
 *    Description        : This function opens the Ethernet port and stores the
                           descriptor in the interface table. Since the port is 
                           opened already during the registration time, this 
                           function is dummy.
 *
 *    Input(s)            : u4IfIndex - Interface Index
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable (Interface table)
 *                                structure
 *
 *    Global Variables Modified :  gapIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if open succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthOpen (UINT4 u4IfIndex)
{

    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthClose
 *
 *    Description        : First disables the promiscuous mode of 
 *                         the port, if enabled. The multicast address
 *                         list is lost when the port is closed.i
 *                         We dont need to check if the call is a 
 *                         success or not.
 *
 *    Input(s)            : u4IfIndex - Interface index.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable
 *
 *    Global Variables Modified : gapIfTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if close succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthClose (UINT4 u4IfIndex)
{

    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthWrite
 *
 *    Description        : Writes the data to the ethernet driver.
 *
 *    Input(s)            : pu1DataBuf - Pointer to the linear buffer.
 *                          u4IfIndex - MIB-2 interface index
 *                          u4PktSize - Size of the buffer.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable (Interface table)
 *                                structure,
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if write succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
/*    if (CfaNpPortWrite (pu1DataBuf, u4IfIndex, u4PktSize) != FNP_SUCCESS)
    {
        return CFA_FAILURE;
    }*/

    if (pu1DataBuf[0] == 0x08)
    {
        if (CustNpSend80211Frame (u4IfIndex, pu1DataBuf, 0, 14, u4PktSize) !=
            FNP_SUCCESS)
        {
            return CFA_FAILURE;
        }
    }
    else
    {
        if (CustNpSend80211Frame (u4IfIndex, pu1DataBuf, 0, 15, u4PktSize) !=
            FNP_SUCCESS)
        {
            return CFA_FAILURE;
        }
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *    Function Name             : CfaGddEthWriteWithPri
 *    Description               : Writes the data to the ethernet driver.
 *    Input(s)                  : pu1DataBuf - Pointer to the linear buffer.
 *                                u4IfIndex  - MIB-2 interface index
 *                                u4PktSize  - Size of the buffer.
 *                                u1Priority - Traffic class on which Pkt to
 *                            be TXed
 *    Output(s)                 : None.
 *    Global Variables Referred : None
 *    Global Variables Modified : None.
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : CFA_SUCCESS if write succeeds,
 *                                otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGddEthWriteWithPri (UINT1 *pData, UINT4 u4IfIndex, UINT4 u4PktLen,
                       UINT1 u1Priority)
{
    if (CfaGddEthWrite (pData, u4IfIndex, u4PktLen) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }
    UNUSED_PARAM (u1Priority);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthRead
 *
 *    Description        : This function reads the data from the ethernet port.
 *
 *    Input(s)            : pu1DataBuf - Pointer to the linear buffer.
 *                          u4IfIndex - IfIndex of the interface
 *                          pu4PktSize - Pointer to the packet size
 *
 *    Output(s)            : pu1DataBuf, pu1PktSize
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (pu4PktSize);

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : NpCfaIsVlanTagRequest
 *
 *    Description        : This function will check, whether VLAN tag is
 *                         required for the incoming packet.
 *
 *    Input(s)           : pBuf - Pointer to the linear buffer.
 *
 *    Output(s)          : None. 
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            : CFA_TRUE or CFA_FALSE.
 *
 *****************************************************************************/
PRIVATE INT4
NpCfaIsVlanTagRequest (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tEnetV2Header      *pEthHdr = NULL;
    UINT4               u4VlanOffset = CFA_VLAN_TAG_OFFSET;
    UINT4               u4ContextId = VLAN_DEF_CONTEXT_ID;
    UINT4               u4IfIndex;
    INT4                i4RetVal;
    UINT4               u4TempIfIndex = 0;
    UINT2               u2AggId = 0;
    UINT2               u2LenOrType = 0;
    UINT2               u2VlanTag = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2Pvid = 0;
    UINT1               au1GmrpAddr[6] = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x20 };
    UINT1               u1PortAccpFrmType;
    tMacAddr            ProviderGmrpAddr;
    tMacAddr            ProviderLacpAddr;
    tMacAddr            ProviderDot1xAddr;
    tMacAddr            ProviderGvrpAddr;
    tMacAddr            ProviderStpAddr;
    BOOL1               bIsTunnelPort;
    UINT1               u1IfEtherType;

    /* CFA_GET_IFINDEX should not be used here, because in the packet
     * processing thread, Cfa Interface index is filled in
     * "ModuleData.InterfaceId" data structure.
     */
    u4IfIndex = pBuf->ModuleData.InterfaceId.u4IfIndex;
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        CfaGetEthernetType (u4IfIndex, &u1IfEtherType);
        if (u1IfEtherType == CFA_STACK_ENET)
        {
            /* If Packets received for Stack interface,
             * then the vlan tag should not be removed */
            return CFA_TRUE;
        }
    }
    /* If the port belongs to port-channel, then get the port-channel id.
     * If the port is not part of any port-channel, then u2AggId will be
     * same as the given u4IfIndex. */
    if (L2IwfGetPortChannelForPort (u4IfIndex, &u2AggId) == L2IWF_SUCCESS)
    {
        u4TempIfIndex = u2AggId;
    }
    else
    {
        u4TempIfIndex = u4IfIndex;
    }

    pEthHdr = (tEnetV2Header *) CRU_BMC_Get_DataPointer (pBuf);

    L2IwfGetPortVlanTunnelStatus (u4TempIfIndex, &bIsTunnelPort);

    if (bIsTunnelPort == OSIX_TRUE)
    {
        /* 
         * In SDK:5.2.4 bpdus and gvrp packets were received as untagged on 
         * tunnel external ports (tunnel ports). On tunnel internal ports 
         * bpdus and gvrp pkts were received 8100 tagged.
         * In SDK:5.4.3, bpdus and gvrp pkts were received tagged on both
         * tunnel internal and external ports.
         * Hence in case of tunnel external ports, if tagged packets are 
         * received with pvid vlan, then untag them.
         */
        u2LenOrType = OSIX_NTOHS (pEthHdr->u2LenOrType);

        if (u2LenOrType == VLAN_PROTOCOL_ID)
        {

            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2VlanTag,
                                       VLAN_TAG_VLANID_OFFSET, VLAN_TAG_SIZE);

            u2VlanTag = OSIX_NTOHS (u2VlanTag);

            u2VlanId = (tVlanId) (u2VlanTag & VLAN_ID_MASK);

            L2IwfGetVlanPortPvid (u4TempIfIndex, &u2Pvid);

            if (u2VlanId != u2Pvid)
            {
                return CFA_TRUE;
            }
        }
        else
        {
            return CFA_TRUE;
        }
    }

    MEMSET (ProviderGmrpAddr, 0, sizeof (tMacAddr));

    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_GMRP_PROTO_ID,
                              ProviderGmrpAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_DOT1X_PROTO_ID,
                              ProviderDot1xAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_LACP_PROTO_ID,
                              ProviderLacpAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_GVRP_PROTO_ID,
                              ProviderGvrpAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_STP_PROTO_ID,
                              ProviderStpAddr);

    if ((MEMCMP (pEthHdr->au1DstAddr, au1GmrpAddr, CFA_ENET_ADDR_LEN) == 0) ||
        (MEMCMP (pEthHdr->au1DstAddr, ProviderGvrpAddr, CFA_ENET_ADDR_LEN) == 0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderStpAddr, CFA_ENET_ADDR_LEN) ==
            0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderLacpAddr, CFA_ENET_ADDR_LEN) ==
            0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderDot1xAddr, CFA_ENET_ADDR_LEN)
            == 0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderGmrpAddr, CFA_ENET_ADDR_LEN) ==
            0))
    {
        /* GMRP packet - retain VLAN tag */
        return CFA_TRUE;
    }

    if (VlanGetTagLenInFrame (pBuf, u4IfIndex, &u4VlanOffset) == VLAN_SUCCESS)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2LenOrType,
                                   u4VlanOffset, CFA_ENET_TYPE_OR_LEN);
        u2LenOrType = OSIX_NTOHS (u2LenOrType);
    }

    VlanNpRetainTag (u4IfIndex, pEthHdr->au1DstAddr, u2LenOrType, &i4RetVal);
    if (i4RetVal == VLAN_TRUE)
    {
        return CFA_TRUE;
    }

    /* Retain tag for all the L3 protocols  and Dot1x packets as well */
    if ((u2LenOrType == CFA_ENET_IPV4) || (u2LenOrType == CFA_ENET_ARP) ||
        (u2LenOrType == CFA_ENET_RARP) || (u2LenOrType == CFA_ENET_IPV6))
    {
        /* Most hardwares add a vlan tag to the received untagged frames before sending to
           the CPU. For the port that accepts only untagged & priority tagged frame, the
           Vlan ID in the tags needs to be removed */
        L2IwfGetVlanPortAccpFrmType (u4IfIndex, &u1PortAccpFrmType);
        if (u1PortAccpFrmType ==
            VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES)
        {
            return CFA_FALSE;
        }
        return CFA_TRUE;
    }

    return CFA_FALSE;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddGetLnxIntfnameForPort
 *
 *    Description          : This function Returns LinuxEth Name Based on Slot
 *                           Referred.
 *    Input(s)            :  u4IfIndex - Interface index
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure
 *
 *    Global Variables Modified : gaIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : EthName if Slot is found Else NULL
 *
 *****************************************************************************/
UINT1              *
CfaGddGetLnxIntfnameForPort (UINT2 u2Index)
{
    UNUSED_PARAM (u2Index);
    return NULL;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddSetLnxIntfnameForPort
 *
 *    Description          : This function sets Interface Name for the interface
 *                           index.
 *    Input(s)            :  u4IfIndex - Interface index
 *                           pu1IfName - Interface name
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure
 *
 *    Global Variables Modified : gaIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : NULL
 *
 *****************************************************************************/
VOID
CfaGddSetLnxIntfnameForPort (UINT4 u4Index, UINT1 *pu1IfName)
{
    UNUSED_PARAM (u4Index);
    UNUSED_PARAM (pu1IfName);
    return;
}

/*****************************************************************************
 *
 *    Function Name       :CfaGddTxPktOnVlanMemberPortsInCxt
 *
 *    Description         :This routine transmits the given Frame on the member
 *                         ports of the given Vlan.
 *
 *    Input(s)            : u4L2ContextId - L2 Context Id to which VlanId is
 *                                          associated
 *                        : pu1DataBuf  - Pointer to the frame buffer
 *                          u4PktSize   - Size of the frame
 *                          VlanId      - Vlan on which the frame is to be
 *                                        transmitted
 *                          bBcast      - Whether this frame is to be
 *                                        broadcasted
 *
 *    Output(s)           : None
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/ CFA_FAILURE.
 *****************************************************************************/

INT4
CfaGddTxPktOnVlanMemberPortsInCxt (UINT4 u4L2ContextId,
                                   UINT1 *pu1DataBuf, tVlanId VlanId,
                                   BOOL1 bBcast, UINT4 u4PktSize)
{
    tPortList          *pTagPorts = NULL;
    tPortList          *pUnTagPorts = NULL;
    tMacAddr            DestAddr;
    tCfaVlanInfo        VlanInfo;
    UINT4               u4BridgeMode;
    UINT4               u4OutPort;
    BOOL1               bIsTag;
    INT4                i4RetVal;
    UINT1              *pu1TempBuf = NULL;
    UINT1               TempDataBuf[CFA_ENET_MIN_UNTAGGED_FRAME_SIZE];
    UINT4               u4MinFrameMtu = 0;
    INT4                i4RetStat = CFA_SUCCESS;
    UINT1               u1PaddingDone = CFA_FALSE;
    MEMCPY (DestAddr, pu1DataBuf, CFA_ENET_ADDR_LEN);

    MEMSET (TempDataBuf, 0, sizeof (TempDataBuf));
    VlanInfo.u2VlanId = VlanId;
#ifdef NPAPI_WANTED
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
#ifdef MBSM_WANTED
        /* If the packet belongs to STACK IVR, then send the
         * packet via stack port using ATP*/
        if (VlanId == CFA_DEFAULT_STACK_VLAN_ID)
        {
            if (CfaMbsmNpTxOnStackInterface (pu1DataBuf, u4PktSize) ==
                FNP_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddTxPktOnVlanMemberPorts - "
                         "Unable to tx on stack interface.\n");
                return CFA_FAILURE;
            }
            return CFA_SUCCESS;
        }
#endif
    }
#endif

    pTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        return CFA_FAILURE;
    }

    pUnTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pUnTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        return CFA_FAILURE;
    }

    MEMSET (*pTagPorts, 0, sizeof (tPortList));
    MEMSET (*pUnTagPorts, 0, sizeof (tPortList));
    i4RetVal = VlanIvrGetTxPortOrPortListInCxt (u4L2ContextId,
                                                DestAddr, VlanId,
                                                (UINT1) bBcast, &u4OutPort,
                                                &bIsTag, *pTagPorts,
                                                *pUnTagPorts);

    if (i4RetVal == VLAN_FORWARD)
    {
        /*ASSUMPTION: The Packet reached here will be untagged always.
         * Since bcoz this thread is called only for the outgoing packet
         * from IP.*/

        u4MinFrameMtu = CFA_ENET_MIN_UNTAGGED_FRAME_SIZE;

        if (u4PktSize < u4MinFrameMtu)
        {
            pu1TempBuf = pu1DataBuf;
            MEMCPY (TempDataBuf, pu1DataBuf, u4PktSize);
            pu1DataBuf = TempDataBuf;
            u4PktSize = u4MinFrameMtu;
            u1PaddingDone = CFA_TRUE;
        }

        CfaTxFillVlanInfo (DestAddr, u4OutPort,
                           bIsTag, *pTagPorts, *pUnTagPorts, &VlanInfo);
        if (L2IwfGetBridgeMode (L2IWF_DEFAULT_CONTEXT, &u4BridgeMode) ==
            L2IWF_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaGddWrite - "
                     "L2IWF-GetBridgeMode -  FAILURE.\n");
            i4RetStat = CFA_FAILURE;
        }
        else
        {
            if (CfaL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo) !=
                CFA_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddWrite - "
                         "Unsuccessful Driver Write -  FAILURE.\n");
                i4RetStat = CFA_FAILURE;

            }
        }
        if (u1PaddingDone == CFA_TRUE)
        {
            pu1DataBuf = pu1TempBuf;
        }
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pUnTagPorts));
        return i4RetStat;
    }

    FsUtilReleaseBitList ((UINT1 *) pTagPorts);
    FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pTagPorts));
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pUnTagPorts));
    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddConfigIp
 *
 *    Description        : Function is called for assigning IP to Eth Interface
 *
 *    Input(s)            : UINT4 u4IpAddr,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if configuration succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
CfaGddConfigIp (UINT4 u4IpAddr)
{
    struct ifreq        if_req;
    struct sockaddr_in *sinptr;
    INT4                i4Err = -1;

    MEMSET (&if_req, 0, sizeof (if_req));
    STRCPY (if_req.ifr_ifrn.ifrn_name, "eth0");

    if_req.ifr_ifru.ifru_addr.sa_family = AF_INET;

    sinptr = (struct sockaddr_in *) (VOID *) &if_req.ifr_addr;
    sinptr->sin_family = AF_INET;
    sinptr->sin_addr.s_addr = OSIX_HTONL (u4IpAddr);
    sinptr->sin_port = 0;

    i4Err = ioctl (gi4PortDesc, SIOCSIFADDR, &if_req);
    if (i4Err < 0)
    {
        perror ("IF ADDR Set");
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddGetIfIndexforInterface
 *
 *    Description        : Function is called for getting IfIndex from Interface
 *    Name
 *    
 *    Input(s)            : None
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if configuration succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
UINT1
CfaGddGetIfIndexforInterface (UINT1 *u1InterfaceName, INT4 *i4Ifindex)
{
    INT4                i4Index;
    if (STRLEN (u1InterfaceName) == 0)
    {
        return CFA_FAILURE;
    }
    for (i4Index = 0; i4Index < SYS_DEF_MAX_PHYSICAL_INTERFACES; i4Index++)
    {
        if (STRCMP (au1IntMapTable[i4Index][1], u1InterfaceName) == 0)
        {
            *i4Ifindex = i4Index + 1;
        }
    }
    return CFA_SUCCESS;
}
