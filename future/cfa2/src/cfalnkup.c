
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: cfalnkup.c,v 1.3 2015/11/20 10:45:19 siva Exp $
*
* Description: Link Up Delay Routine
*********************************************************************/

#include "cfainc.h"
#include "fscfacli.h"
#include "ifmibcli.h"
#include "rtm.h"
#ifdef NPSIM_WANTED
#include "gddnpsim.h"
#endif
#ifdef OPENFLOW_WANTED
#include "ofcl.h"
#endif /* OPENFLOW_WANTED */
#ifdef LNXIP4_WANTED
#ifdef VRF_WANTED
#include "fssocket.h"
#endif
#endif

extern INT4                gu4CfaLinkUpStatus;

/*****************************************************************************/
/* Function Name      : CfaLinkUpDelayTmrInit                                */
/*                                                                           */
/* Description        : This function Create timer list id for Link Up delay */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS / CFA_FAILURE                            */
/*****************************************************************************/
INT4
CfaLinkUpDelayTmrInit (VOID)
{
    /* create the timer lists for LINK UP DELAY */
     if (TmrCreateTimerList ((const UINT1 *) CFA_TASK_NAME,
                            CFA_LINKUP_DELAY_TMR_EXP_EVENT,
                            NULL, &CFA_LINKUP_DELAY_TMR_LIST) != TMR_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                 "Exiting CfaLinkUpDelayTmrInit - create timer list for "
                 "link up delay  timer fail\n");
        return CFA_FAILURE;
    }
    CfaTmrInitTmrDesc();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaLinkUpDelayTmrDeInit                              */
/*                                                                           */
/* Description        : This function Delete timer list id for Link Up delay */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS / CFA_FAILURE                            */
/*****************************************************************************/
INT4
CfaLinkUpDelayTmrDeInit (VOID)
{
    /* Delete  the timer lists for LINK UP DELAY */
    if(CFA_LINKUP_DELAY_TMR_LIST != CFA_LINKUP_ZERO)
    {

        if (TmrDeleteTimerList ((CFA_LINKUP_DELAY_TMR_LIST)) != TMR_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                     "Exiting CfaLinkUpDelayTmrDeInit - Delete timer list for "
                     "link up delay  timer fail\n");
            return CFA_FAILURE;
        }

        CFA_LINKUP_DELAY_TMR_LIST = CFA_LINKUP_ZERO;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaTmrInitTmrDesc                                    */
/*                                                                           */
/* Description        : This function set the timer ID to call on expiry     */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID CfaTmrInitTmrDesc (VOID)
{
    gGlobalTimerDesc.aTmrDesc[CFA_LINKUP_DELAY_TIMER].i2Offset =
                    (INT2) FSAP_OFFSETOF (tCfaIfInfo,IfLinkUpDelay);

    gGlobalTimerDesc.aTmrDesc[CFA_LINKUP_DELAY_TIMER].TmrExpFn =
        CfaLinkUpDelayTmrExpiry;


    gGlobalTimerDesc.aTmrDesc[CFA_LINKUP_STARTUP_TIMER].i2Offset =
                    (INT2) FSAP_OFFSETOF (tCfaIfInfo,IfLinkUpDelay);

    gGlobalTimerDesc.aTmrDesc[CFA_LINKUP_STARTUP_TIMER].TmrExpFn =
        CfaLinkUpDelayTmrExpiry;
    return;
}
/*****************************************************************************/
/* Function Name      : CfaLinkUpDelayProcessTimerExpiryEvent                */
/*                                                                           */
/* Description        : This function call timer expiry for link up delay    */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CfaLinkUpDelayProcessTimerExpiryEvent (VOID)
{
    tLinkUpTimer        *pExpiredTmr = NULL;
    INT2                i2Offset = CFA_LINKUP_ZERO;
    UINT1               u1TimerId = CFA_LINKUP_ZERO;

   /* Get the expired timer from the list */
    while ((pExpiredTmr = (tLinkUpTimer *)
            TmrGetNextExpiredTimer (CFA_LINKUP_DELAY_TMR_LIST)) != NULL)
    {
        /* Get the timer id and Offset */
        u1TimerId = ((tLinkUpTimer *) pExpiredTmr)->TmrBlk.u1TimerId;

        i2Offset = gGlobalTimerDesc.aTmrDesc[u1TimerId].i2Offset;

        /* Call the corresponding timer expiry handler to the
         * expiry of the timer */

      (*(gGlobalTimerDesc.aTmrDesc[u1TimerId].TmrExpFn))
          ((UINT1 *) pExpiredTmr - i2Offset);

      }
}
/*****************************************************************************/
/* Function Name      : CfaLinkUpDelayTmrExpiry                              */
/*                                                                           */
/* Description        : This function process the timer expiry               */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID CfaLinkUpDelayTmrExpiry (VOID *pArg)
{

    tCfaIfInfo     *pCfaPort = pArg;
    UINT4           u4IfIndex = CFA_LINKUP_ZERO;

    u4IfIndex = pCfaPort->u4CfaIfIndex;
#ifdef NPAPI_WANTED
        if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
        {

            if (CfaFsHwUpdateAdminStatusChange (u4IfIndex,FNP_LINKUP_STP_FORWARD) !=
                FNP_SUCCESS)
            {
                return;
            }
        }
#endif
    CfaIfmHandleInterfaceStatusChange (u4IfIndex,
                                       CFA_IF_ADMIN(u4IfIndex),
                                       CFA_IF_UP,CFA_IF_LINK_STATUS_CHANGE);
    pCfaPort->IfLinkUpDelay.PeriodicTmrNode.u1TmrStatus = LINKUPDELAY_TMR_RESET;

    return;
}
/*****************************************************************************/
/* Function Name      : CfaLinkUpDelayTmrDeInit                              */
/*                                                                           */
/* Description        : This function Delete timer list id for Link Up delay */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS / CFA_FAILURE                            */
/*****************************************************************************/
INT4
CfaLinkUpDelayTmrStart (UINT4 u4IfIndex)
{
    
    tCfaIfInfo         *pCfaIfInfo = NULL;
          
    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
      
    CFA_DS_UNLOCK ();
    if (TmrStart (CFA_LINKUP_DELAY_TMR_LIST,
                &(pCfaIfInfo->IfLinkUpDelay.PeriodicTmrNode.TmrBlk),
                 CFA_LINKUP_DELAY_TIMER,
                 pCfaIfInfo->IfLinkUpDelay.u4ConfLinkUpTime,
                 CFA_LINKUP_ZERO) != TMR_SUCCESS)
    {
            pCfaIfInfo->IfLinkUpDelay.PeriodicTmrNode.u1TmrStatus = LINKUPDELAY_TMR_RESET;
            CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                     "Exiting CfaLinkUpDelayTmrStart - Start timer list for "
                     "link up delay  timer fail\n");
            return CFA_FAILURE;
    }
    pCfaIfInfo->IfLinkUpDelay.PeriodicTmrNode.u1TmrStatus = LINKUPDELAY_TMR_SET;
    return CFA_SUCCESS;

}
/*****************************************************************************/
/* Function Name      : CfaLinkUpDelayTmrProcessRemainingTime                */
/*                                                                           */
/* Description        : This function get the remaining time                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                      u4SetValIfMainExtLinkUpDelayTimer                    */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS / CFA_FAILURE                            */
/*****************************************************************************/
INT4 
CfaLinkUpDelayTmrProcessRemainingTime(UINT4 u4IfIndex,
                                      UINT4 u4SetValIfMainExtLinkUpDelayTimer)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    UINT4               u4ExistSetTime = CFA_LINKUP_ZERO;
    UINT4               u4CompletedTime = CFA_LINKUP_ZERO;
    UINT4               u4NewTime = CFA_LINKUP_ZERO;
    UINT4               u4RemainingTime = CFA_LINKUP_ZERO;
    UINT1               u1TmrStatus = CFA_LINKUP_ZERO;

    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry(u4IfIndex);
    CFA_DS_UNLOCK ();

    u4ExistSetTime = pCfaIfInfo->IfLinkUpDelay.u4ConfLinkUpTime;
    u1TmrStatus = pCfaIfInfo->IfLinkUpDelay.PeriodicTmrNode.u1TmrStatus;
    if(u1TmrStatus == LINKUPDELAY_TMR_SET)
    {
        if(TmrGetRemainingTime (CFA_LINKUP_DELAY_TMR_LIST,
           &(pCfaIfInfo->IfLinkUpDelay.PeriodicTmrNode.TmrBlk.TimerNode),
           &u4RemainingTime) != TMR_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                     "Exiting CfaLinkUpDelayTmrProcessRemainingTime-" 
                     " Remaining timer list for "
                     "link up delay timer fail\n");
            return CFA_FAILURE;
        }
        /*convert the time from milliseconds to seconds*/
       u4RemainingTime = (u4RemainingTime/100);
       pCfaIfInfo->IfLinkUpDelay.u4LinkUpRemainingTime = u4RemainingTime;
       u4CompletedTime = u4ExistSetTime - u4RemainingTime;
       if(u4SetValIfMainExtLinkUpDelayTimer < u4CompletedTime)
       {
          if(CfaLinkUpDelayTmrStop(u4IfIndex) != TMR_SUCCESS)
          {
             CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                     "Exiting CfaLinkUpDelayTmrProcessRemainingTime-"
                     "Timer stop list for"
                     "link up delay timer fail\n");
              return CFA_FAILURE;
          }
#ifdef NPAPI_WANTED
        if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
        {
            if (CfaFsHwUpdateAdminStatusChange (u4IfIndex,FNP_LINKUP_STP_FORWARD) !=
                FNP_SUCCESS)
            {
                return CFA_FAILURE;
            }
        }
#endif
          CfaIfmHandleInterfaceStatusChange (u4IfIndex,
                                        CFA_IF_ADMIN(u4IfIndex),
                                        CFA_IF_UP,CFA_IF_LINK_STATUS_CHANGE);
          pCfaIfInfo->IfLinkUpDelay.u4LinkUpRemainingTime = CFA_LINKUP_ZERO;
       }
       else
       {
          u4NewTime = u4SetValIfMainExtLinkUpDelayTimer - u4CompletedTime;
          if(TmrRestart (CFA_LINKUP_DELAY_TMR_LIST,
                       &(pCfaIfInfo->IfLinkUpDelay.PeriodicTmrNode.TmrBlk),
                        CFA_LINKUP_DELAY_TIMER,u4NewTime,CFA_LINKUP_ZERO) != TMR_SUCCESS)
          {
             CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                     "Exiting CfaLinkUpDelayTmrProcessRemainingTime- Timer restart  for"
                     "link up delay timer fail\n");
              return CFA_FAILURE;
          }

       } 

    }
    return CFA_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : CfaLinkUpDelayTmrStop                                */
/*                                                                           */
/* Description        : This function Delete timer list id for Link Up delay */
/*                                                                           */
/* Input(s)           : u4IfIndex  : interface index                         */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS / CFA_FAILURE                            */
/*****************************************************************************/
INT4 
CfaLinkUpDelayTmrStop(UINT4 u4IfIndex)
{

    tCfaIfInfo         *pCfaIfInfo = NULL;

    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    CFA_DS_UNLOCK ();

    if(TmrStop (CFA_LINKUP_DELAY_TMR_LIST,
               &(pCfaIfInfo->IfLinkUpDelay.PeriodicTmrNode.TmrBlk))!= TMR_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                    "Failed to Stop Timer for Linkup-Delay "
                    "on the interface %d\n",
                    u4IfIndex);
        return CFA_FAILURE;
    } 
    pCfaIfInfo->IfLinkUpDelay.PeriodicTmrNode.u1TmrStatus = LINKUPDELAY_TMR_RESET;

return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaProcessPortOperStatus                             */
/*                                                                           */
/* Description        : This function ge the operation staus and timer status*/
/*                                                                           */
/* Input(s)           : u4IfIndex :-  Interface index.                       */
/*                      u1AdminStatus :- Interface Admin Status              */
/*                      u1OperStatus  :- Interface Oper Status               */
/*                      u1IsFromMibi  :- type of interface port              */
/*                      u1OldOperStatus :- Previous Oper Status              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS / CFA_FAILURE                            */
/*****************************************************************************/
INT4 
CfaProcessPortOperStatus(UINT4 u4IfIndex, UINT1 u1AdminStatus,
                         UINT1 u1OperStatus, UINT1 u1IsFromMib,
                         UINT1 u1OldOperStatus)
{
    UINT2               u2EnablePortStatus = CFA_LINKUP_ZERO;
    /* this function check the oper status when linkup delay is enabled */
    CfaGetIfLinkUpDelayStatus(u4IfIndex,&(u2EnablePortStatus));
    if((u2EnablePortStatus == CFA_LINKUP_DELAY_ENABLED))
    {
       /*to process the linkup delay when OPER STATUS is down/up*/

        CfaInterfaceLinkUpDelay(u4IfIndex,u1OperStatus,u1OldOperStatus);
    }
    else
    {
        /* when linkup delay is disabled , we will enable oper status Up be Default
         * and CfaIfmHandleInterfaceStatusChange
         * be used to indicate the higher layer about the port oper status*/
        CfaIfmHandleInterfaceStatusChange(u4IfIndex, u1AdminStatus, u1OperStatus, u1IsFromMib);
    }
    return CFA_SUCCESS;
}


/*****************************************************************************/
/* Function Name      : CfaInterfaceLinkUpDelay                              */
/*                                                                           */
/* Description        : This function start/stop the timer wrto Oper Status  */
/*                                                                           */
/* Input(s)           : u4IfIndex :-  Interface index.                       */
/*                      u1OperStatus  :- Interface Oper Status               */
/*                      u1OldOperStatus :- Previous Oper Status              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS / CFA_FAILURE                            */
/*****************************************************************************/



INT4 CfaInterfaceLinkUpDelay(UINT4 u4IfIndex,UINT1 u1OperStatus,UINT1 u1OldOperStatus)
{

    tCfaIfInfo         *pCfaIfInfo = NULL;
    INT4               i4RetVal = CFA_LINKUP_ZERO;
    INT4               i4TmrStatus = CFA_LINKUP_ZERO;


    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
    CFA_DS_UNLOCK ();

    i4TmrStatus = pCfaIfInfo->IfLinkUpDelay.PeriodicTmrNode.u1TmrStatus;
    if((u1OldOperStatus  == CFA_IF_DOWN) &&
       (u1OperStatus == CFA_IF_UP))
    {
        i4RetVal = CfaLinkUpDelayTmrStart(u4IfIndex);
#ifdef NPAPI_WANTED
        if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
        {
            if (CfaFsHwUpdateAdminStatusChange (u4IfIndex,FNP_LINKUP_STP_BLOCK) !=
                FNP_SUCCESS)
            {
                i4RetVal = CFA_FAILURE;
            }
        }
#endif
    }
    if(((u1OldOperStatus  == CFA_IF_UP) &&
        (u1OperStatus == CFA_IF_DOWN)))
    {

        if((i4TmrStatus == LINKUPDELAY_TMR_SET))
        {
            i4RetVal = CfaLinkUpDelayTmrStop (u4IfIndex);
        }
#ifdef NPAPI_WANTED
        if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
        {

            if (CfaFsHwUpdateAdminStatusChange (u4IfIndex,FNP_LINKUP_STP_FORWARD) !=
                FNP_SUCCESS)
            {
                i4RetVal = CFA_FAILURE;
            }
        }
#endif
        CfaIfmHandleInterfaceStatusChange (u4IfIndex,
                                           CFA_IF_ADMIN(u4IfIndex),
                                           CFA_IF_DOWN,CFA_IF_LINK_STATUS_CHANGE);



    }
    if(i4RetVal == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                  "Failed to Start/Stop Timer for Linkup-Delay "
                  "on the interface %d\n",
                  u4IfIndex);

        return CFA_FAILURE;
    }
    return CFA_SUCCESS;

}



/*****************************************************************************/
/* Function Name      : CfaCheckForLinkUpDelay                                */
/*                                                                           */
/* Description        : This function check for the timer status             */
/*                                                                           */
/* Input(s)           : u4IfIndex :-  Interface index.                       */
/*                      u1OperStatus  :- Interface Oper Status               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS / CFA_FAILURE                            */
/*****************************************************************************/
VOID
CfaCheckForLinkUpDelay (INT4 u4IfIndex,UINT1 u1OperStatus)
{

    tCfaIfInfo         *pCfaIfInfo = NULL;
    INT4               i4TmrStatus = CFA_LINKUP_ZERO;
    UINT2               u2EnablePortStatus = CFA_LINKUP_ZERO;

    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry ((UINT4) u4IfIndex);

    CFA_DS_UNLOCK ();

    i4TmrStatus = pCfaIfInfo->IfLinkUpDelay.PeriodicTmrNode.u1TmrStatus;
    CfaGetIfLinkUpDelayStatus((UINT4)u4IfIndex,&(u2EnablePortStatus));
    if((u2EnablePortStatus == CFA_LINKUP_DELAY_ENABLED))
    {
      if((i4TmrStatus == LINKUPDELAY_TMR_SET) && (u1OperStatus == CFA_IF_DOWN ))
      {
         CfaLinkUpDelayTmrStop((UINT4)u4IfIndex);  
         pCfaIfInfo->IfLinkUpDelay.u4LinkUpRemainingTime = CFA_LINKUP_ZERO;

      }

    }
   return;

}


/*****************************************************************************/
/* Function Name      : CfaCleanUpLinkUpModuleStatus                         */
/*                                                                           */
/* Description        : This function reset the linkup delay                 */
/*                                                                           */
/* Input(s)           : u4IfIndex :-  Interface index.                       */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/

VOID CfaCleanUpLinkUpModuleStatus(INT4  i4LinkUpSystemStatus)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    INT4                i4Flag = CFA_FALSE;
    INT4                i4Type = CFA_FAILURE;
    INT4               i4TmrStatus = CFA_LINKUP_ZERO;
    UINT4               u4IfIndex;
    UINT1               u1BridgedIfaceStatus;

    if (CFA_LINKUP_DELAY_SHUTDOWN == i4LinkUpSystemStatus)
    {

        for (u4IfIndex = 1; u4IfIndex <=
             (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF); u4IfIndex++)
        {
            i4Flag = CFA_FALSE;
            CFA_DS_LOCK ();
            pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
            CFA_DS_UNLOCK ();
            if (pCfaIfInfo == NULL)
            {
                continue;
            }

            CfaGetIfBridgedIfaceStatus ((UINT4)u4IfIndex,
                                        &u1BridgedIfaceStatus);
            i4Type = CfaIsL3IpVlanInterface ((UINT4)u4IfIndex);
            if ((u1BridgedIfaceStatus == CFA_DISABLED) ||
                (i4Type == CFA_SUCCESS))
            {
                i4Flag = CFA_TRUE;
            }
            if(i4Flag == CFA_FALSE)
            {

                i4TmrStatus = pCfaIfInfo->IfLinkUpDelay.PeriodicTmrNode.u1TmrStatus;
                if((i4TmrStatus == LINKUPDELAY_TMR_SET))
                {
                    CfaLinkUpDelayTmrStop(u4IfIndex);
                    if(CFA_IF_ADMIN(u4IfIndex) == CFA_IF_UP)
                    {

#ifdef NPAPI_WANTED
                        if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
                        {

                            if (CfaFsHwUpdateAdminStatusChange (u4IfIndex,FNP_LINKUP_STP_FORWARD) !=
                                FNP_SUCCESS)
                            {
                                return;
                            }
                        }
#endif
                        CfaIfmHandleInterfaceStatusChange (u4IfIndex,
                                                           CFA_IF_ADMIN(u4IfIndex),
                                                           CFA_IF_UP,CFA_IF_LINK_STATUS_CHANGE);

                    }
                    else
                    {
#ifdef NPAPI_WANTED
                        if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
                        {

                            if (CfaFsHwUpdateAdminStatusChange (u4IfIndex,FNP_LINKUP_STP_FORWARD) !=
                                FNP_SUCCESS)
                            {
                                return;
                            }
                        }
#endif
                        CfaIfmHandleInterfaceStatusChange (u4IfIndex,
                                                           CFA_IF_ADMIN(u4IfIndex),
                                                           CFA_IF_DOWN,CFA_IF_LINK_STATUS_CHANGE);
                    }


                }
                CfaLinkUpDelayTmrProcessRemainingTime(u4IfIndex,CFA_LINKUP_ZERO);
                CfaSetIfLinkUpDelayStatus (u4IfIndex,CFA_LINKUP_DELAY_DISABLED);
                CfaSetIfLinkUpDelayTimer (u4IfIndex,CFA_LINKUP_ZERO);
                pCfaIfInfo->IfLinkUpDelay.u4LinkUpRemainingTime = CFA_LINKUP_ZERO;

            }
        }
    }
    return;

}



/*****************************************************************************/
/* Function Name      : CfaCleanUpLinkUpPortStatus                           */
/*                                                                           */
/* Description        : This function reset the linkup delay  on port        */
/*                                                                           */
/* Input(s)           : u4IfIndex :-  Interface index.                       */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/

VOID CfaCleanUpLinkUpPortStatus(UINT4 i4IfMainIndex)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    INT4               i4TmrStatus = CFA_LINKUP_ZERO;

    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (i4IfMainIndex);
    CFA_DS_UNLOCK ();
    i4TmrStatus = pCfaIfInfo->IfLinkUpDelay.PeriodicTmrNode.u1TmrStatus;
    if((i4TmrStatus == LINKUPDELAY_TMR_SET))
    {
        CfaLinkUpDelayTmrStop(i4IfMainIndex);

#ifdef NPAPI_WANTED
        if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
        {

            if (CfaFsHwUpdateAdminStatusChange (i4IfMainIndex,FNP_LINKUP_STP_FORWARD) !=
                FNP_SUCCESS)
            {
                return;
            }
        }
#endif
        CfaIfmHandleInterfaceStatusChange (i4IfMainIndex,
                                           CFA_IF_ADMIN(i4IfMainIndex),
                                           CFA_IF_UP,CFA_IF_LINK_STATUS_CHANGE);

    }
    CfaSetIfLinkUpDelayStatus (i4IfMainIndex,CFA_LINKUP_DELAY_DISABLED);
    CfaSetIfLinkUpDelayTimer ( i4IfMainIndex,CFA_LINKUP_ZERO);
    pCfaIfInfo->IfLinkUpDelay.u4LinkUpRemainingTime = CFA_LINKUP_ZERO;
   return;
}



/*****************************************************************************/
/* Function Name      : CfaProcessLinkUpDelayStartUp                         */
/*                                                                           */
/* Description        : This function is used to start the time for defaul  t*/
/*                       interface index                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

VOID 
CfaProcessLinkUpDelayStartUp(VOID)
{

    tCfaIfInfo         *pCfaIfInfo = NULL;

    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (CFA_DEFAULT_ROUTER_IFINDEX);

    CFA_DS_UNLOCK ();


    if (TmrStart (CFA_LINKUP_DELAY_TMR_LIST,
                &(pCfaIfInfo->IfLinkUpDelay.PeriodicTmrNode.TmrBlk),
                 CFA_LINKUP_STARTUP_TIMER,
                 CFA_LINKUP_DELAY_STARTUP_TIMER,
                 CFA_LINKUP_ZERO) != TMR_SUCCESS)
    {
            CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                     "Exiting CfaLinkUpDelayTmrStart - Start timer list for "
                     "link up delay  timer fail\n");
            return;
    }
    gu4CfaLinkUpStatus = CFA_DEFAULT_DISABLE;
    return;
}

