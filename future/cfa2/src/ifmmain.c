/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ifmmain.c,v 1.270 2018/01/11 11:17:49 siva Exp $
 *
 * Description:This file contains the routines for the     
 *             Interface Management Module of the CFA. These   
 *             routines are mostly used in the control path    
 *             for creation/deletion/configuration of      
 *             interfaces. Most of the routines are common to  
 *             all interfaces or to Enet/HDLC interfaces.    
 *
 *******************************************************************/

#include "cfainc.h"
#include "mbsnp.h"
#include "ifmibcli.h"
#include "fscfacli.h"
#include "iss.h"

#ifdef MBSM_WANTED
#include "mbsm.h"
#endif

extern UINT1        gu1CfaDefIpv6Initialised;

/* globals of interface table */
tIfGlobalStruct     gIfGlobal;

/* the interface table */
tIfInfoStruct      *gapIfTable[SYS_DEF_MAX_INTERFACES];

/* IpInterface information table */
tIpIfGlobalInfo     gIpIfInfo;

tCfaNodeStatus      gCfaNodeStatus = CFA_NODE_IDLE;

UINT1               gu1CfaIpInitComplete = CFA_FALSE;    /* Flag to ensure that 
                                                           IpInitComplete needs to 
                                                           be done only after call
                                                           from ISS */
tMemPoolId          ExtTriggerMsgPoolId;
tMemPoolId          TxPktOnPortListMsgPoolId;
tMemPoolId          IfTlvEntryMemPoolId;
tMemPoolId          EnetIwfMemPoolId;
INT4                i4IfTlvBuddyMemPoolId;

/* Table of default values */
tCfaIfDefaultVal   *ppCfaIfDefaultVal[CFA_MAX_IF_TYPES + 1];
tCfaIfDefaultVal    asCfaIfDefaultVal[CFA_IF_TYPES] = {
    {CFA_ENET, CFA_ENET_MTU, CFA_ENET_SPEED, 1, CFA_ENET_NAME_PREFIX,
     CFA_ENET_DESCR, "ENET"}
    ,
    {CFA_TUNNEL, CFA_TNL_MTU, CFA_TNL_SPEED, 0, CFA_TNL_NAME_PREFIX,
     CFA_TNL_DESCR, "TUNNEL"}
    ,
    {CFA_L2VLAN, 0, 0, 0, "", CFA_L2VLAN_DESCR, "L2VLAN"}
    ,
    {CFA_L3IPVLAN, 0, 0, 0, CFA_L3IPVLAN_NAME_PREFIX, CFA_L3IPVLAN_DESCR,
     "L3IPVLAN"}
    ,
    {CFA_LAGG, 0, 0, 0, CFA_LAGG_NAME_PREFIX, CFA_LAGG_DESCR, "LAGG"}
    ,
    {CFA_BRIDGED_INTERFACE, 0, 0, 0, CFA_BRIDGED_INTERFACE_NAME_PREFIX,
     CFA_BRIDGED_INTERFACE_DESCR, "INTERNAL"}
    ,
    {CFA_ILAN, 0, 0, 0, CFA_ILAN_NAME_PREFIX, CFA_ILAN_DESCR, "ILAN"}
#if defined (WLC_WANTED) || defined (WTP_WANTED)
    ,
    {CFA_RADIO, 0, 0, 0, CFA_RADIO_NAME_PREFIX, CFA_RADIO_DESCR, "RADIO"}
#endif
};

/* Mempool ID for the interface table structure */
tMemPoolId          IfEntryMemPoolId;
tMemPoolId          GddEntryMemPoolId;
tMemPoolId          WanEntryMemPoolId;
tMemPoolId          IpEntryMemPoolId;

UINT1               gu1NumofVlanInterface = 0;

/* singly linked list used for propagating If status up the stack */
tTMO_SLL            IfStatusUpdate;

/* table for mapping CIDR numbers to IP subnet masks - from RFC 1878*/
UINT4               u4CidrSubnetMask[CFA_MAX_CIDR + 1] = {
    0x00000000, 0x80000000, 0xC0000000, 0xE0000000, 0xF0000000, 0xF8000000,
    0xFC000000, 0xFE000000, 0xFF000000, 0xFF800000, 0xFFC00000, 0xFFE00000,
    0xFFF00000, 0xFFF80000, 0xFFFC0000, 0xFFFE0000, 0xFFFF0000, 0xFFFF8000,
    0xFFFFC000, 0xFFFFE000, 0xFFFFF000, 0xFFFFF800, 0xFFFFFC00, 0xFFFFFE00,
    0xFFFFFF00, 0xFFFFFF80, 0xFFFFFFC0, 0xFFFFFFE0, 0xFFFFFFF0, 0xFFFFFFF8,
    0xFFFFFFFC, 0xFFFFFFFE, 0xFFFFFFFF
};

#if defined (LNXIP4_WANTED) && defined (NPAPI_WANTED)
extern VOID         CfaGddSetLnxIntfnameForPort (UINT4 u4Index,
                                                 UINT1 *au1IfName);
#endif
#if defined (WLC_WANTED) || defined (WTP_WANTED)
#ifdef LNXIP4_WANTED
extern UINT1       *CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
#endif
#endif
/*****************************************************************************
 *
 *    Function Name        : CfaIfmInit
 *
 *    Description        : This function initialises the iftable of IFM.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gIfGlobal (Interface's global struct),
 *                gaPhysIfTable (GDD reg table) structure,
 *                IfTable (gapIfTable) structure,
 *                gaConfigStruct (configuration file) struct.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *                gaConfigStruct (configuration file) struct.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInit (VOID)
{

    UINT4               u4IfIndex;
    UINT4               u4MinBlkAdjusted;
    UINT4               u4MaxBlkAdjusted;
    UINT1               u1IfType = CFA_ENET_UNKNOWN;

    CFA_DBG (CFA_TRC_ALL, CFA_IFM, "Entering CfaIfmInit \n");

/* initialize the MemPoolIds */
    CFA_IF_MEMPOOL = 0;
    CFA_GDD_MEMPOOL = 0;
    CFA_IPIF_MEMPOOL = 0;
    CFA_WAN_MEMPOOL = 0;

    if (CfaIpIfCreateIpIfTable () == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                 "Fatal Error in CfaIfmInit - Error in Malloc for IpifTable.\n");
        return CFA_FAILURE;
    }

    if (CfaIfmInitDefaultVal () == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                 "Fatal Error in CfaIfmInit - CfaIfmInitDefaultVal failed\n");
        return CFA_FAILURE;
    }

    /* Initialize the GapIfTable's array of pointers to NULL */
    CFA_GAP_IF_INIT ();

    CFA_IF_MEMPOOL = CFAMemPoolIds[MAX_CFA_IFINFO_INTF_IN_SYS_SIZING_ID];
    CFA_GDD_MEMPOOL = CFAMemPoolIds[MAX_CFA_GDD_REG_BLOCKS_SIZING_ID];
    CFA_TLV_MEMPOOL = CFAMemPoolIds[MAX_CFA_TLV_INFO_IN_SYSTEM_SIZING_ID];
    CFA_ENET_IWF_MEMPOOL = CFAMemPoolIds[MAX_CFA_ENET_IWF_BLOCKS_SIZING_ID];
    CFA_IPIF_MEMPOOL = CFAMemPoolIds[MAX_CFA_IPIF_BLOCKS_SIZING_ID];
    CFA_WAN_MEMPOOL = CFAMemPoolIds[MAX_CFA_WAN_BLOCKS_SIZING_ID];

    /* Call  Util Routine to calculate Adjustment values for minimum
       and Max Block size before creating Buddy Memory Pool   */
    UtilCalculateBuddyMemPoolAdjust (CFA_TLV_MIN_LENGTH,
                                     CFA_TLV_MAX_LENGTH,
                                     &u4MinBlkAdjusted, &u4MaxBlkAdjusted);

    if ((CFA_TLV_BUDDY_MEMPOOL =
         MemBuddyCreate (u4MaxBlkAdjusted,
                         u4MinBlkAdjusted,
                         CFA_MAX_TLV_INFO_BUDDY_IN_SYSTEM,
                         BUDDY_HEAP_EXTN)) == BUDDY_FAILURE)
    {
        return CFA_FAILURE;
    }

/* initialise the global structure of IFM */
    CFA_IFNUMBER () = 0;
    CFA_IFTAB_CHNG () = 0;
    CFA_IFSTK_CHNG () = 0;
    CFA_AVAIL_IFINDEX () = 1;
    CfaIfUtlCreateIfDb (CFA_GAP_IFDB_REQ);

    /* Initialise the default Router interface - viz. eth0 */
    if (CfaIfmDefaultInterfaceInit () != CFA_SUCCESS)
    {

        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                 "Fatal Error in CfaIfmInit - Error in creating "
                 "default interface.\n");
        return (CFA_FAILURE);
    }
    if (VcmGetSystemModeExt (CFA_PROTOCOL_ID) == VCM_MI_MODE)
    {
        /* Find the ethernet type of all the ports */
        for (u4IfIndex = 1; u4IfIndex <= ISS_MAX_PORTS; u4IfIndex++)
        {
#ifdef NPAPI_WANTED
            if (CfaFsHwGetEthernetType (u4IfIndex, &u1IfType) == FNP_FAILURE)
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM, "Error in CfaIfmInit - "
                          "Getting Ethernet type for interface %d\n",
                          u4IfIndex);
            }
            CfaSetEthernetType (u4IfIndex, u1IfType);
#else
            u1IfType = (UINT1) IssCustGetEthernetType (u4IfIndex);
            CfaSetEthernetType (u4IfIndex, u1IfType);
#endif
        }
    }
    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_IFM,
             "Exiting CfaIfmInit - successful Init.\n");

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmShutdown
 *
 *    Description        : This function performs the shutdown of
 *                the Interface Management Module of CFA. This
 *                routine releases the Interface Table
 *                and should be called after the shutdown of
 *                FFM and IWF modules of CFA.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gIfGlobal (Interface's global struct),
 *                IfTable (gapIfTable) structure.
 *                gaConfigStruct (configuration file) struct.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *                gaConfigStruct (configuration file) struct.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
VOID
CfaIfmShutdown (VOID)
{
    UINT4               u4IfmShutdownCounter = 1;
    UINT1               u1IfType = CFA_NONE;

    CFA_DBG (CFA_TRC_ALL, CFA_IFM, "Entering CfaIfmShutdown \n");

    /* de-allocate the dynamic memory allocated during interface creation */
    CFA_CDB_SCAN_WITH_LOCK (u4IfmShutdownCounter, SYS_DEF_MAX_INTERFACES,
                            CFA_ALL_IFTYPE)
    {
        /* delete if the IfEntry is used */
        if (CFA_IF_ENTRY_USED (u4IfmShutdownCounter))
        {
            CfaGetIfType (u4IfmShutdownCounter, &u1IfType);

            if (CfaIfmDeleteIfEntry (u4IfmShutdownCounter,
                                     u1IfType) != CFA_SUCCESS)
            {

                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                          "Error in CfaIfmShutdown - "
                          "Error in deleting interface %d.\n",
                          u4IfmShutdownCounter);

            }                    /* end of delete ifEntry */
        }                        /* end of used ifEntry */
    }                            /* end of for loop */

    CfaIpIfDestroyIpIfTable ();
    CfaIfUtlDestroyIfDb (CFA_GAP_IFDB_REQ);

    if (CFA_TLV_BUDDY_MEMPOOL != 0)
    {
        MemBuddyDestroy ((UINT1) CFA_TLV_BUDDY_MEMPOOL);
        CFA_TLV_BUDDY_MEMPOOL = 0;
    }
    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_IFM, "Exiting CfaIfmShutdown.\n");

}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmCreateAndInitIfEntry
 *
 *    Description        : This function allocates for an entry in the
 *                interface table and initialises it to
 *                default values.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *                UINT1 u1IfType.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *                gIfGlobal (Interface's global struct)
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *                gIfGlobal (Interface's global struct)
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmCreateAndInitIfEntry (UINT4 u4IfIndex, UINT1 u1IfType, UINT1 *pu1PortName)
{
    INT4                i4RetVal = CFA_SUCCESS;

    CFA_DBG2 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmCreateAndInitIfEntry "
              "for interface %d of type %d.\n", u4IfIndex, u1IfType);

    /* check if the IfIndex is valid */
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmCreateAndInitIfEntry - "
                  "invalid interface %d.\n", u4IfIndex);
        return (CFA_FAILURE);
    }
#ifndef MBSM_WANTED
    if (((u1IfType == CFA_ENET) &&
         (u4IfIndex > gIssSysGroupInfo.u4FrontPanelPortCount)) &&
        (u4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmCreateAndInitIfEntry - "
                  "interface %d exceeds front panel port count.\n", u4IfIndex);
        return (CFA_FAILURE);
    }
#endif
    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        /* check if the IfEntry is available */
        if (CFA_IF_ENTRY_USED (u4IfIndex))
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmCreateAndInitIfEntry - "
                      "interface %d in use.\n", u4IfIndex);
            return (CFA_FAILURE);
        }

        /* Add node to CFA GapIfTable */
        if (CFA_SUCCESS != CfaIfUtlIfDbAdd (u4IfIndex, CFA_GAP_IFDB_REQ))
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmCreateAndInitIfEntry - "
                      "Error in adding interface to GapIf DB %d.\n", u4IfIndex);

            return (CFA_FAILURE);
        }
    }

    /* Add node to CFA DB table */
    if (CFA_SUCCESS != CfaIfUtlIfDbAdd (u4IfIndex, CFA_CDB_IFDB_REQ))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmCreateAndInitIfEntry - "
                  "Error in adding node to Cdb db %d.\n", u4IfIndex);

        return (CFA_FAILURE);
    }

    CfaInitCdbInfo (u4IfIndex);

    /* Add to the DLL */
    CfaIfUtlIfTypeDbNodeInit (u4IfIndex, CFA_CDB_IFTYPE_REQ);

    /* Add to the DLL - When an interface entry is initially created it is first
     * added to unknown interface type. Later when ifType is provided it will be
     * added to the respective IfType DLL location */

    CfaIfUtlIfTypeDbAddNode (u4IfIndex, CFA_CDB_IFTYPE_REQ);

    /* This interface is now used */
    CfaSetIfValidStatus (u4IfIndex, CFA_TRUE);

    /* give name to the interface */
    CfaSetIfName (u4IfIndex, pu1PortName);

    CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
    CfaSetIfSubType (u4IfIndex, CFA_ENET_UNKNOWN);

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        /* when created, there are no interfaces above and below this interface. */
        TMO_SLL_Init (&CFA_IF_STACK_LOW (u4IfIndex));
        if (CfaIfmAddStackEntry (u4IfIndex, CFA_LOW, CFA_NONE,
                                 CFA_RS_NOTREADY) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmCreateAndInitIfEntry - "
                      "No LowStack for interface %d.\n", u4IfIndex);
        }

        /* we do not set the RowStatus to active till we register to IP */
        TMO_SLL_Init (&CFA_IF_STACK_HIGH (u4IfIndex));
        if (CfaIfmAddStackEntry (u4IfIndex, CFA_HIGH, CFA_NONE,
                                 CFA_RS_NOTREADY) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmCreateAndInitIfEntry - "
                      "No HighStack for interface %d.\n", u4IfIndex);
        }

        TMO_SLL_Init (&CFA_IF_TLV_INFO (u4IfIndex));

        CFA_IF_IWF (u4IfIndex) = NULL;
        CFA_GDD_ENTRY (u4IfIndex) = NULL;

        CFA_TNL_ENTRY (u4IfIndex) = NULL;
        /* assign default values to variables  */
        CFA_IF_ADMIN (u4IfIndex) = CFA_IF_DOWN;
        CFA_IF_STORAGE_TYPE (u4IfIndex) = CFA_STORAGE_TYPE_NONVOLATILE;
    }

    CfaSetCdbPortAdminStatus (u4IfIndex, CFA_IF_DOWN);
    CfaSetIfOperStatus (u4IfIndex, CFA_IF_DOWN);

    CfaSetIfUfdOperStatus (u4IfIndex, CFA_IF_DOWN);
    /*Below code has been added to reduce coverity issue */
    if (CfaSetIfPortRole (u4IfIndex, CFA_PORT_ROLE_DOWNLINK) != CFA_FAILURE)
    {
        /*do nothing */
    }
    if (CfaSetIfUfdGroupId (u4IfIndex, 0) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Warning in CfaIfmCreateAndInitIfEntry - "
                  "UFD group id setting failed for interface %d.\n", u4IfIndex);
    }

    CFA_CDB_IF_UFD_DOWNLINKENABLED_COUNT (u4IfIndex) = 0;
    CFA_CDB_IF_UFD_DOWNLINKDISABLED_COUNT (u4IfIndex) = 0;

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        CFA_IF_TRAP_EN (u4IfIndex) = CFA_DISABLED;
        CFA_IF_PROMISC (u4IfIndex) = CFA_DISABLED;
        CFA_IF_IS_BACKPLANE (u4IfIndex) = CFA_FRONTPANEL_INTF;

        CfaSetBrgPortFromBrgMode (u4IfIndex);
        /* assign default values based on the ifType */
        switch (u1IfType)
        {
            case CFA_ENET:
                i4RetVal = CfaIfmInitEnetIfEntry (u4IfIndex, pu1PortName);
                break;
#ifdef WGS_WANTED
            case CFA_L2VLAN:
#endif /* WGS_WANTED */
            case CFA_L3IPVLAN:
                if (gu4IsIvrEnabled == CFA_ENABLED)
                {
                    i4RetVal = CfaIfmInitVlanIfEntry (u4IfIndex, pu1PortName);
                }
                break;

            case CFA_L3SUB_INTF:
                if (gu4IsIvrEnabled == CFA_ENABLED)
                {
                    i4RetVal = CfaIfmInitL3SubIfEntry (u4IfIndex, pu1PortName);
                }
                break;

#ifdef PPP_WANTED
            case CFA_PPP:
                i4RetVal = CfaIfmInitPppIfEntry (u4IfIndex, pu1PortName);
                break;
#endif /* PPP_WANTED */

#ifdef MPLS_WANTED
            case CFA_MPLS:
                i4RetVal = CfaIfmInitMplsIfEntry (u4IfIndex, pu1PortName);
                break;

            case CFA_MPLS_TUNNEL:
                i4RetVal = CfaIfmInitMplsTunnelIfEntry (u4IfIndex, pu1PortName);
                break;
#endif /* MPLS_WANTED */

            case CFA_LAGG:
                /* Create port channel only in cfa and call higher layers to
                 * update the port.
                 */
                i4RetVal =
                    CfaIfmInitPortChannelIfEntry (u4IfIndex, pu1PortName);
                break;

            case CFA_PIP:
            case CFA_BRIDGED_INTERFACE:
                i4RetVal = CfaIfmInitInternalIfEntry (u4IfIndex, pu1PortName);
                break;
#ifdef VPN_WANTED
#ifdef IKE_WANTED
            case CFA_VPNC:
                i4RetVal = CfaIfmInitVpncIfEntry (u4IfIndex, pu1PortName);
                break;
#endif /* VPN_WANTED */
#endif /* IKE_WANTED */

#ifdef TLM_WANTED
            case CFA_TELINK:
                i4RetVal = CfaIfmInitTeLinkIfEntry (u4IfIndex, pu1PortName);
                break;
#endif
            case CFA_OTHER:
                i4RetVal = CfaIfmInitOtherIfEntry (u4IfIndex, pu1PortName);
                break;
#ifdef HDLC_WANTED
            case CFA_HDLC:
                i4RetVal = CfaIfmInitHdlcIfEntry (u4IfIndex, pu1PortName);
                break;
#endif
#if defined (WLC_WANTED) || defined (WTP_WANTED)
            case CFA_RADIO:
                i4RetVal = CfaIfmInitRadioIfEntry (u4IfIndex, pu1PortName);
                break;

            case CFA_WLAN_RADIO:
                i4RetVal = CfaIfmInitWlanRadioIfEntry (u4IfIndex, pu1PortName);
                break;

            case CFA_CAPWAP_VIRT_RADIO:
                i4RetVal =
                    CfaIfmInitVirtualRadioIfEntry (u4IfIndex, pu1PortName);
                break;

            case CFA_CAPWAP_DOT11_PROFILE:
                i4RetVal =
                    CfaIfmInitWlanProfileIfEntry (u4IfIndex, pu1PortName);
                break;

            case CFA_CAPWAP_DOT11_BSS:
                i4RetVal = CfaIfmInitWlanBssIfEntry (u4IfIndex, pu1PortName);
                break;
#endif

            default:
                CFA_IF_RS (u4IfIndex) = CFA_RS_NOTREADY;
                CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTREADY);
                i4RetVal = CFA_SUCCESS;
                break;            /* this case occurs when the interface
                                   is created through the CFA MIB when
                                   the type is not specified while creating
                                   the entry. */
        }                        /* end of switch case */

        /* proceed further only if there was no problem in malloc for IWF struct */
        if (i4RetVal == CFA_SUCCESS)
        {
            OsixGetSysTime (&CFA_IF_CHNG (u4IfIndex));

            /* update the globals of the interface table */
            ++CFA_IFNUMBER ();
            CFA_IFTAB_CHNG () = CFA_IFSTK_CHNG () = CFA_IF_CHNG (u4IfIndex);

            /* assign the next available ifIndex to IfAvailableIndex, if required. We
               try not to reuse the IfIndex as far as possible */

            /* no need to change the available ifIndex if we created the entry with some
               other ifIndex */
            if (u4IfIndex == CFA_AVAIL_IFINDEX ())
            {
                ++CFA_AVAIL_IFINDEX ();

                /* Start from the newly created ifIndex and see if there is an unused index */
                for (; ((CFA_AVAIL_IFINDEX () <= CFA_MAX_INTERFACES ()) &&
                        (CFA_IF_ENTRY_USED (CFA_AVAIL_IFINDEX ())));
                     ++CFA_AVAIL_IFINDEX ())
                {
                    /* null body with no initialisation of the for loop */
                }

                /* Check for any deleted ifIndex and point to that */
                if (CFA_AVAIL_IFINDEX () > CFA_MAX_INTERFACES ())
                {
                    for (CFA_AVAIL_IFINDEX () = 1;
                         (CFA_IF_ENTRY_USED (CFA_AVAIL_IFINDEX ()) &&
                          (CFA_AVAIL_IFINDEX () < u4IfIndex));
                         ++CFA_AVAIL_IFINDEX ())
                    {
                        /* null body of the for loop */
                    }
                }

                /* the whole interface table is free then assign an invalid ifIndex - 0 */
                if (CFA_AVAIL_IFINDEX () == u4IfIndex)
                {
                    CFA_AVAIL_IFINDEX () = 0;
                }
            }                    /* end of Available ifIndex updation */

            CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                      "In CfaIfmCreateAndInitIfEntry - "
                      "Creation for interface %d - SUCCESS\n", u4IfIndex);
        }                        /* end of interface Initing (in Enet, PPP, etc.) success check */
        else
        {

            /* de-allocate memory taken from IF Mempool */
            if (CFA_SUCCESS != CfaIfUtlIfDbRem (u4IfIndex, CFA_GAP_IFDB_REQ))
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                          "Error in CfaIfmCreateAndInitIfEntry - "
                          "Error in deleting interface from GapIf DB %d\n",
                          u4IfIndex);
            }

            /* This interface is now unused */
            CfaSetIfValidStatus (u4IfIndex, CFA_FALSE);

            CFA_DBG1 ((CFA_TRC_ERROR | CFA_TRC_ALL_TRACK), CFA_IFM,
                      "Error in CfaIfmCreateAndInitIfEntry - "
                      "Creation fail for interface %d\n", u4IfIndex);
        }
    }

    return (i4RetVal);
}

/*****************************************************************************
 *
 *    Function Name             : CfaIfmInitOtherIfEntry
 *
 *    Description               : This function creates Interface of type 
 *                                CFA_OTHER in the interface table and 
 *                                initialises it to default values.
 *
 *    Input(s)                  :  u4IfIndex - Interface IfIndex
 *                                 au1PortName - Interface Name
 *
 *    Output(s)                 : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                  : CFA_SUCCESS on Successful creation of Interface 
 *                               CFA_FAILURE - Failure of creation of Interface
 *
 *****************************************************************************/
INT4
CfaIfmInitOtherIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{

    /* CFA_OTHER : These Interfaces are similar to CFA_ENET Interfaces.
       Used as Connecting Interfaces in Dual Unit Stacking.
       Ports are visible only to CFA. 
       Port Create Indications are not given to L2IWF and IP
       By default they are up. Can not be controlled by Administrator */
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;
    UINT1               au1HwAddr[CFA_MAX_MEDIA_ADDR_LEN];

    UINT4               u4IfMtu = CFA_ENET_MTU;
    tCfaIfInfo          CfaIfInfo;
    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitOtherIfEntry for Connecting interface %d .\n",
              u4IfIndex);
    CfaSetIfType (u4IfIndex, CFA_OTHER);
    CfaSetIfName (u4IfIndex, au1PortName);

    if (CfaIfUtlMauMemAlloc (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitOtherIfEntry - "
                  "Mau struct malloc fail for interface %d\n", u4IfIndex);
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return CFA_FAILURE;
    }
    /* create the entry in the GDD table for this interface */
    if (CfaGddInitPort (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitOtherIfEntry - "
                  "GDD entry creation fail for interface %d\n", u4IfIndex);
        CfaIfUtlMauMemRelease (u4IfIndex);
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    /* we first register this interface with the GDD module immediately */
    /* Gdd Registration is same as CFA_ENET . Purposefully passing type
       as CFA_ENET */
    if (CfaGddRegisterPort (u4IfIndex, CFA_ENET) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitOtherIfEntry - "
                  "Registration fail for interface %d\n", u4IfIndex);
        /* restore the GDD entry */
        CfaGddDeInitPort (u4IfIndex);
        CfaIfUtlMauMemRelease (u4IfIndex);
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }
    if (CfaGddGetOsHwAddr (u4IfIndex, au1HwAddr) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitOtherIfEntry - "
                  "unable to get the socket or MAC address for interface %d\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }
    /* allocate memory for Enet IWF struct - if failure then interface cant be
       created */
    if ((CFA_IF_IWF (u4IfIndex) =
         MemAllocMemBlk (CFA_ENET_IWF_MEMPOOL)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitOtherIfEntry - "
                  "IWF struct malloc fail for interface %d\n", u4IfIndex);
        /* restore the GDD entry */
        CfaGddDeInitPort (u4IfIndex);
        CfaIfUtlMauMemRelease (u4IfIndex);
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    MEMSET (CFA_IF_IWF (u4IfIndex), 0, sizeof (tEnetIwfStruct));

    CfaSetIfHwAddr (u4IfIndex, au1HwAddr, CFA_ENET_ADDR_LEN);

    /* initialise the IWF structures for Enet */
    CfaIwfEnetInit (u4IfIndex);
    /* To ensure that this Interface is not visible to L2IWF */
    CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_DISABLED);
    /* To ensure that this Interface is not visible to IP */
    CfaIfInfo.i4IpPort = CFA_INVALID_INDEX;
    CfaSetIfInfo (IF_IP_PORTNUM, u4IfIndex, &CfaIfInfo);
    CfaSetIfMtu (u4IfIndex, u4IfMtu);
#ifdef NPAPI_WANTED
    CfaFsCfaHwSetMtu (u4IfIndex, u4IfMtu);
#endif
    CfaSetIfNwType (u4IfIndex, CFA_NETWORK_TYPE_LAN);
    STRNCPY (CFA_IF_DESCR (u4IfIndex), CFA_OTHER_DESCR,
             STRLEN (CFA_OTHER_DESCR));
    CFA_IF_DESCR (u4IfIndex)[STRLEN (CFA_OTHER_DESCR)] = '\0';
    CfaSetIfInfo (IF_DESC, u4IfIndex, &CfaIfInfo);

    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    /* get the HW address from the driver - if failure then dont show it */
    if (CfaIfmAddRcvAddrEntry (u4IfIndex, au1HwAddr, CFA_RCVADDR_NVOL)
        != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitOtherIfEntry - "
                  "RcvAddrTab UCAST add fail on interface %d\n", u4IfIndex);
    }

    /* Add Enet Bcast addr also */
    MEMSET (au1HwAddr, 0, CFA_MAX_MEDIA_ADDR_LEN);
    MEMSET (au1HwAddr, 0xff, CFA_ENET_ADDR_LEN);
    if (CfaIfmAddRcvAddrEntry (u4IfIndex, au1HwAddr, CFA_RCVADDR_NVOL)
        != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitOtherIfEntry - "
                  "RcvAddrTab BCAST add fail on interface %d\n", u4IfIndex);
    }

    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_ENETV2;
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);

    pHighStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
    pLowStackListScan = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);

    CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
    CFA_IF_STACK_STATUS (pLowStackListScan) = CFA_RS_ACTIVE;

    CfaSetIfName (u4IfIndex, CFA_GDD_PORT_NAME (u4IfIndex));
    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmInitOtherIfEntry - "
              "Initialized Enet interface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmInitEnetIfEntry
 *
 *    Description        : This function allocates for Enet IWF in the
 *                interface table and initialises it to
 *                default values.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitEnetIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{

    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;
    UINT1               au1HwAddr[CFA_MAX_MEDIA_ADDR_LEN];
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1TmpIfType = CFA_ENET_UNKNOWN;

    UINT4               u4IfMtu = CFA_ENET_MTU;
    UINT4               u4IfSpeed = 0;
    UINT4               u4IfHighSpeed = 0;
#ifdef KERNEL_WANTED
#ifdef NETFILTER_WANTED
    tOobIfName          OobIfName;
    INT4                i4CommandStatus;
#endif
#endif
    UINT1               u1ForwardStatus = CFA_DISABLED;
#ifdef LNXIP4_WANTED
    UINT1               u1Status = IP_FORW_DISABLE;
#endif
    tCfaIfInfo          CfaIfInfo;
    tIpConfigInfo       IpCfgInfo;
    UINT4               u4Flag;
#ifdef NPAPI_WANTED
    UINT1               u1IfType = CFA_ENET_UNKNOWN;
#ifndef MBSM_WANTED
    tEthStats           EthStats;
    INT4                i4Status = 0;
    INT4                i4MauIndex = 1;
#endif
#ifdef MBSM_WANTED
    UINT1               u1IfEtherType;
    UINT1               u1HgPortType;
#endif
#endif

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitEnetIfEntry for Enet interface %d .\n",
              u4IfIndex);
    MEMSET (au1HwAddr, 0, CFA_MAX_MEDIA_ADDR_LEN);
#ifdef NPAPI_WANTED
    if (CfaIsMgmtPort (u4IfIndex) == FALSE)
    {
        if (CfaFsHwGetEthernetType (u4IfIndex, &u1IfType) == FNP_FAILURE)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmInitEnetIfEntry - "
                      "Getting Ethernet type for interface %d\n", u4IfIndex);
            return CFA_FAILURE;
        }

#ifdef MBSM_WANTED
        if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
        {
            if (u1IfType == CFA_ENET_UNKNOWN)
            {
                /* During preconfiguration, to set Higig ports */
                CfaSetHgPortsEtherType (u4IfIndex, &u1HgPortType);
                u1IfType = u1HgPortType;
            }
            /* If it is stack port means, set Admin UP
             * disable bridge status */
            if (u1IfType == CFA_ENET_UNKNOWN)
            {
                if ((u4IfIndex >= (UINT4) CFA_STACK_PORT_START_INDEX)
                    && (u4IfIndex <=
                        (UINT4) (MBSM_MAX_PORTS_PER_SLOT + ISS_MAX_STK_PORTS)))
                {
                    /* When MBSM Init to Standby state chage occurs,
                     * it could not get LPort from BCMX and also 
                     * it could not get Ethernt Type and also
                     * could not get speed.
                     * So if it is StackIfIndex, means it will be created
                     * as STACKENET type*/
                    u1IfType = CFA_STACK_ENET;
                    CfaSetEthernetType (u4IfIndex, CFA_STACK_ENET);
                    CFA_IF_ADMIN (u4IfIndex) = CFA_IF_UP;
                    CfaSetCdbPortAdminStatus (u4IfIndex, CFA_IF_UP);
                    CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_DISABLED);
                }
            }
            else
            {
                CfaGetEthernetType (u4IfIndex, &u1IfEtherType);
                if (u1IfEtherType == CFA_STACK_ENET)
                {
                    CFA_IF_ADMIN (u4IfIndex) = CFA_IF_UP;
                    CfaSetCdbPortAdminStatus (u4IfIndex, CFA_IF_UP);
                    CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_DISABLED);
                }
            }
        }
        if (u1IfType == CFA_ENET_UNKNOWN)
        {
            /* As In MI with  MBSM, in pre-config case, the FsHwGetEthernetType () 
             * This H/w Api will return the interface EtherType as 
             * CFA_ENET_UNKNOWN. So we need to call preconfig database here
             * to get the expected ethertype for this interface
             */
            MbsmGetPortSpeedTypeFromIfIndex (u4IfIndex, &u1IfType);

            switch (u1IfType)
            {
                case MBSM_SPEED_FAST_ETHER:
                    u1IfType = CFA_FA_ENET;
                    break;
                case MBSM_SPEED_ETHER:
                case MBSM_SPEED_GIG_ETHER:
                    u1IfType = CFA_GI_ENET;
                    break;
                case MBSM_SPEED_10GIG_ETHER:
                    u1IfType = CFA_XE_ENET;
                    break;
                case MBSM_SPEED_40GIG_ETHER:
                    u1IfType = CFA_XL_ENET;
                    break;
                case MBSM_SPEED_56GIG_ETHER:
                    u1IfType = CFA_LVI_ENET;
                    break;
                case MBSM_SPEED_NULL:
                case MBSM_SPEED_INVALID:
                default:
                    u1IfType = CFA_ENET_UNKNOWN;
            }
        }
#endif
        u1TmpIfType = u1IfType;
        CfaSetEthernetType (u4IfIndex, u1TmpIfType);

    }
    else
    {
        u1TmpIfType = u1IfType;
        CfaSetEthernetType (u4IfIndex, u1TmpIfType);
    }
#else
    u1TmpIfType = (UINT1) IssCustGetEthernetType (u4IfIndex);
    CfaSetEthernetType (u4IfIndex, u1TmpIfType);
#endif

    CfaSetIfType (u4IfIndex, CFA_ENET);
    CfaSetIfSubType (u4IfIndex, u1TmpIfType);
    CfaSetIfName (u4IfIndex, au1PortName);
    CfaSetIfAlias (u4IfIndex, au1PortName);

    if (CfaIfUtlMauMemAlloc (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitEnetIfEntry - "
                  "Mau struct malloc fail for interface %d\n", u4IfIndex);
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return CFA_FAILURE;
    }

    if (VcmGetSystemModeExt (CFA_PROTOCOL_ID) == VCM_MI_MODE)
    {
        /* check if port already inited */
        if (CFA_GDD_ENTRY_USED (u4IfIndex))
        {
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                      "Error in CfaGddInitPort - Interface %d already "
                      "Initialized - FAILURE.\n", u4IfIndex);
            return (CFA_SUCCESS);
        }
    }
    /* create the entry in the GDD table for this interface */
    if (CfaGddInitPort (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitEnetIfEntry - "
                  "GDD entry creation fail for interface %d\n", u4IfIndex);
        CfaIfUtlMauMemRelease (u4IfIndex);
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    /* we first register this interface with the GDD module immediately */
    if (CfaGddRegisterPort (u4IfIndex, CFA_ENET) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitEnetIfEntry - "
                  "Registration fail for interface %d\n", u4IfIndex);
        /* restore the GDD entry */
        CfaGddDeInitPort (u4IfIndex);
        CfaIfUtlMauMemRelease (u4IfIndex);
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    if (CfaIsMgmtPort (u4IfIndex) == TRUE)
    {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
        if (CfaGddOobGetOsHwAddr (u4IfIndex, au1HwAddr) == CFA_FAILURE)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaGddOobGetOsHwAddr - "
                      "Oob get HW addr fail for interface %d\n", u4IfIndex);
            /* restore the GDD entry */
            CfaGddDeInitPort (u4IfIndex);
            CfaIfUtlMauMemRelease (u4IfIndex);
            CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
            return (CFA_FAILURE);
        }
#ifdef KERNEL_WANTED
#ifdef NETFILTER_WANTED
        OobIfName.i4Cmd = CFA_OOB_SET_IFNAME;
        /*Since Ifname size is 16 in kernel */
        STRNCPY (OobIfName.au1OobIfName, au1PortName, STRLEN (au1PortName));
        OobIfName.au1OobIfName[STRLEN (au1PortName)] = '\0';

        i4CommandStatus = KAPIUpdateInfo (GDD_OOB_IOCTL,
                                          (tKernCmdParam *) & OobIfName);

        if (i4CommandStatus == OSIX_FAILURE)
        {
            return (CFA_FAILURE);
        }
#endif
#endif
#endif

    }
    else if (CfaIsLinuxVlanIntf (u4IfIndex) == TRUE)
    {
        if (CfaGddOobGetOsHwAddr (CFA_DEFAULT_ROUTER_IFINDEX, au1HwAddr) ==
            CFA_FAILURE)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaGddOobGetOsHwAddr - "
                      "Oob get HW address fail for interface %d\n", u4IfIndex);
            /* restore the GDD entry */
            CfaGddDeInitPort (u4IfIndex);
            CfaIfUtlMauMemRelease (u4IfIndex);
            CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
            return (CFA_FAILURE);
        }
    }
    else
    {
        if (CfaGddGetOsHwAddr (u4IfIndex, au1HwAddr) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmInitEnetIfEntry - "
                      "unable to get the socket or MAC address for interface %d\n",
                      u4IfIndex);
            return (CFA_FAILURE);
        }
    }

    /* allocate memory for Enet IWF struct - if failure then interface cant be
       created */
    if ((CFA_IF_IWF (u4IfIndex) =
         MemAllocMemBlk (CFA_ENET_IWF_MEMPOOL)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitEnetIfEntry - "
                  "IWF struct malloc fail for interface %d\n", u4IfIndex);
        /* restore the GDD entry */
        CfaGddDeInitPort (u4IfIndex);
        CfaIfUtlMauMemRelease (u4IfIndex);
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    MEMSET (CFA_IF_IWF (u4IfIndex), 0, sizeof (tEnetIwfStruct));

    CfaSetIfHwAddr (u4IfIndex, au1HwAddr, CFA_ENET_ADDR_LEN);

    /* initialise the IWF structures for Enet */
    CfaIwfEnetInit (u4IfIndex);

    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        if (CfaIsMgmtPort (u4IfIndex) == TRUE)
        {
            CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_DISABLED);
#ifdef LNXIP4_WANTED
            if (CFA_OOB_MGMT_INTF_ROUTING == TRUE)
            {
                /* Enable the IP Forwarding for OOB Interface 
                   when routing over the oob is wanted */
                u1ForwardStatus = CFA_ENABLED;
                u1Status = IP_FORW_ENABLE;
            }

            if (LnxIpSetIfForwarding (CFA_GDD_PORT_NAME (u4IfIndex),
                                      u1Status) == IP_FAILURE)
            {
                return CFA_FAILURE;
            }
#endif /* LNXIP4_WANTED */
            /* Create a entry in IP interface table .Ip can be enable in
             * fast ethernet interface only if BridgeStatus is disabled.*/
            if (CfaIpIfCreateIpInterface (u4IfIndex) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                          "Error in CfaIfmInitEnetIfEntry - "
                          "IP Entry Mempool alloc fail interface %d.\n",
                          u4IfIndex);
                /* free the IWF struct */
                MemReleaseMemBlock (CFA_ENET_IWF_MEMPOOL, (UINT1 *)
                                    (CFA_IF_IWF (u4IfIndex)));
                /* de-register the GDD port */
                if (CfaGddDeRegisterPort (u4IfIndex) != CFA_SUCCESS)
                {
                    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                              "Error in CfaIfmInitEnetIfEntry - "
                              "GDD De-Registration fail interface %d\n",
                              u4IfIndex);
                }
                /* restore the GDD entry */
                CfaGddDeInitPort (u4IfIndex);
                CfaIfUtlMauMemRelease (u4IfIndex);
                CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
                return (CFA_FAILURE);
            }
            u4Flag =
                CFA_IP_IF_FORWARD | CFA_IP_IF_ALLOC_PROTO |
                CFA_IP_IF_ALLOC_METHOD;
            IpCfgInfo.u1IpForwardEnable = u1ForwardStatus;
            IpCfgInfo.u1AddrAllocMethod = CFA_IP_ALLOC_NONE;
            IpCfgInfo.u1AddrAllocProto = CFA_PROTO_DHCP;
            if (CfaIpIfSetIfInfo (u4IfIndex, u4Flag, &IpCfgInfo) == CFA_FAILURE)
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                          "Setting IP Param for Interface %d failed\n",
                          u4IfIndex);
                return CFA_FAILURE;
            }
        }
        else
        {
            CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_ENABLED);
        }
    }
    else
    {
        if ((CfaIsMgmtPort (u4IfIndex) == TRUE) &&
            (CFA_OOB_MGMT_INTF_ROUTING == FALSE))
        {
            /* Disable the IP Forwarding for OOB Interface 
               when routing over the oob is not wanted */
            u1ForwardStatus = CFA_DISABLED;
        }
        else
        {
            u1ForwardStatus = CFA_ENABLED;
        }
    }

    CfaIfInfo.i4IpPort = CFA_INVALID_INDEX;
    CfaSetIfInfo (IF_IP_PORTNUM, u4IfIndex, &CfaIfInfo);
    CFA_IF_TRAP_EN (u4IfIndex) = CFA_ENABLED;    /* as it is a physical if */
    if (CfaIsMgmtPort (u4IfIndex) == TRUE)
    {
        CfaSetIfMtu (u4IfIndex, u4IfMtu);
        CfaSetIfSpeed (u4IfIndex, CFA_ENET_SPEED);
        CfaSetIfHighSpeed (u4IfIndex, 0);
    }
    else
    {
#ifdef NPAPI_WANTED
        /* Port Speed should in Sync with H/W */
        CfaFsCfaHwSetMtu (u4IfIndex, u4IfMtu);
#endif
#ifndef MBSM_WANTED
        CfaSetIfMtu (u4IfIndex, u4IfMtu);
        CfaDetermineSpeed (u4IfIndex, &u4IfSpeed, &u4IfHighSpeed);
        CfaSetIfSpeed (u4IfIndex, u4IfSpeed);
        CfaSetIfHighSpeed (u4IfIndex, u4IfHighSpeed);
#ifdef NPAPI_WANTED
        CfaGetIfAutoNegStatus (u4IfIndex, i4MauIndex, &i4Status);
        CfaSetIfAutoNegStatus (u4IfIndex, i4Status);
        MEMSET (&EthStats, 0, sizeof (tEthStats));
        EthStats.u4ReqType = NP_STAT_PORT_DUPLEX_STATUS;
        CfaFsEtherHwGetStats (u4IfIndex, &EthStats);
        CfaSetIfDuplexStatus (u4IfIndex, (INT4) EthStats.u4DuplexStatus);
#endif /* NPAPI_WANTED */
#else
        /* Update MTU only if it is not OOB port */
        MbsmGetPortMtu (u4IfIndex, &u4IfMtu);
        CfaSetIfMtu (u4IfIndex, u4IfMtu);

        /* Initialize the invalid value for the interface
         * parameters in CFA */
        CfaSetIfSpeed (u4IfIndex, u4IfSpeed);
        CfaSetIfHighSpeed (u4IfIndex, u4IfHighSpeed);
        CfaSetIfAutoNegStatus (u4IfIndex, 0);
        CfaSetIfDuplexStatus (u4IfIndex, 0);
#endif /* MBSM_WANTED */
    }
    CfaSetIfNwType (u4IfIndex, CFA_NETWORK_TYPE_LAN);

    CfaGetIfDescr (u4IfIndex, CFA_IF_DESCR (u4IfIndex));

    MEMSET (CfaIfInfo.au1Descr, 0, CFA_MAX_DESCR_LENGTH);
    MEMCPY (CfaIfInfo.au1Descr, CFA_IF_DESCR (u4IfIndex), CFA_MAX_DESCR_LENGTH);
    CfaSetIfInfo (IF_DESC, u4IfIndex, &CfaIfInfo);

    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    /* get the HW address from the driver - if failure then dont show it */
    if (CfaIfmAddRcvAddrEntry (u4IfIndex, au1HwAddr, CFA_RCVADDR_NVOL)
        != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitEnetIfEntry - "
                  "RcvAddrTab UCAST add fail on interface %d\n", u4IfIndex);
    }

    /* Add Enet Bcast addr also */
    MEMSET (au1HwAddr, 0, CFA_MAX_MEDIA_ADDR_LEN);
    MEMSET (au1HwAddr, 0xff, CFA_ENET_ADDR_LEN);
    if (CfaIfmAddRcvAddrEntry (u4IfIndex, au1HwAddr, CFA_RCVADDR_NVOL)
        != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitEnetIfEntry - "
                  "RcvAddrTab BCAST add fail on interface %d\n", u4IfIndex);
    }

    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_ENETV2;
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);

    pHighStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
    pLowStackListScan = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);

    CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
    CFA_IF_STACK_STATUS (pLowStackListScan) = CFA_RS_ACTIVE;

    CfaSetIfName (u4IfIndex, CFA_GDD_PORT_NAME (u4IfIndex));

    MEMSET (au1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);

    if (CfaIsMgmtPort (u4IfIndex) == FALSE)
    {
        switch (u1TmpIfType)
        {
            case CFA_GI_ENET:
                STRNCPY (au1InterfaceName, "Gi", STRLEN ("Gi"));
                au1InterfaceName[STRLEN ("Gi")] = '\0';
                break;
#ifdef NPAPI_WANTED
            case CFA_FA_ENET:
                STRNCPY (au1InterfaceName, "Fe", STRLEN ("Fe"));
                au1InterfaceName[STRLEN ("Fe")] = '\0';
                break;
            case CFA_XE_ENET:
                STRNCPY (au1InterfaceName, "Ex", STRLEN ("Ex"));
                au1InterfaceName[STRLEN ("Ex")] = '\0';
                break;
            case CFA_XL_ENET:
                STRNCPY (au1InterfaceName, "Xl", STRLEN ("Xl"));
                au1InterfaceName[STRLEN ("Xl")] = '\0';
                break;
            case CFA_LVI_ENET:
                STRNCPY (au1InterfaceName, "Lvi", STRLEN ("Lvi"));
                au1InterfaceName[STRLEN ("Lvi")] = '\0';
                break;
            case CFA_ENET_UNKNOWN:
                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                          "Error in CfaIfmInitEnetIfEntry - "
                          "Getting Ethernet type for interface %d\n",
                          u4IfIndex);
                return CFA_FAILURE;
#endif
            default:
                return CFA_FAILURE;
                break;
        }
    }
    CfaSetPortName (u4IfIndex, au1InterfaceName);

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmInitEnetIfEntry - "
              "Initialized Enet interface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmInitOfcVlanIfEntry
 *
 *    Description          : This function allocates for Openflow vlan  in the
 *                           interface table and initialises it to
 *                           default values.
 *
 *    Input(s)             : UINT4 u4IfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitOfcVlanIfEntry (UINT4 u4IfIndex, UINT1 *au1IfName)
{
    INT4                i4RetVal = CFA_SUCCESS;
    i4RetVal = CfaSetIfType (u4IfIndex, CFA_L2VLAN);
    i4RetVal = CfaSetIfName (u4IfIndex, au1IfName);
    CFA_DS_LOCK ();
    STRNCPY (CFA_CDB_IF_DESC (u4IfIndex), au1IfName, CFA_MAX_DESCR_LENGTH - 1);
    CFA_DS_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmInitPswIfEntry
 *
 *    Description          : This function allocates for Psw IWF in the
 *                           interface table and initialises it to
 *                           default values.
 *
 *    Input(s)             : UINT4 u4IfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitPswIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{

    UINT4               u4IfSpeed = 0;
    UINT4               u4IfHighSpeed = 0;
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;
    UINT1               au1HwAddr[CFA_MAX_MEDIA_ADDR_LEN];
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    tCfaIfInfo          CfaIfInfo;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitPswIfEntry for Psw interface %d .\n",
              u4IfIndex);

    MEMSET (au1HwAddr, 0, CFA_MAX_MEDIA_ADDR_LEN);
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    /* allocate memory for PSW IWF struct - if failure then interface cant be
       created */
    if ((CFA_IF_IWF (u4IfIndex) =
         MemAllocMemBlk (CFA_ENET_IWF_MEMPOOL)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitPswIfEntry - "
                  "IWF struct malloc fail for interface %d\n", u4IfIndex);
        /* restore the GDD entry */
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    MEMSET (CFA_IF_IWF (u4IfIndex), 0, sizeof (tEnetIwfStruct));

    CfaSetIfType (u4IfIndex, CFA_PSEUDO_WIRE);
    CfaSetIfName (u4IfIndex, au1PortName);
    CfaSetPortName (u4IfIndex, au1PortName);
    CfaSetIfMtu (u4IfIndex, CFA_DEF_PSEUDO_WIRE_MTU);
    CfaDetermineSpeed (u4IfIndex, &u4IfSpeed, &u4IfHighSpeed);
    CfaSetIfSpeed (u4IfIndex, u4IfSpeed);
    CfaSetIfNwType (u4IfIndex, CFA_NETWORK_TYPE_LAN);

    /*This will change the port-type according to Bridge Mode for pseudowire in SI mode */
    if (VcmGetL2ModeExt () == VCM_SI_MODE)
    {

        if (L2IwfPbSetDefBrgPortTypeInCfa (0, u4IfIndex,
                                           CFA_TRUE,
                                           CFA_FALSE) == L2IWF_FAILURE)
        {
            return (CFA_FAILURE);
        }
    }
    if (CfaGddGetOsHwAddr (u4IfIndex, au1HwAddr) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitEnetIfEntry - "
                  "unable to get the socket or MAC addr for interface %d\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

    CfaSetIfHwAddr (u4IfIndex, au1HwAddr, CFA_ENET_ADDR_LEN);

    /* initialise the IWF structures for PSW */
    CfaIwfPswInit (u4IfIndex);

    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        if (CfaIsMgmtPort (u4IfIndex) == TRUE)
        {
            return CFA_FAILURE;
        }
        else
        {
            CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_ENABLED);
        }
    }

    CfaGetIfDescr (u4IfIndex, CFA_IF_DESCR (u4IfIndex));

    MEMSET (CfaIfInfo.au1Descr, 0, CFA_MAX_DESCR_LENGTH);
    MEMCPY (CfaIfInfo.au1Descr, CFA_IF_DESCR (u4IfIndex), CFA_MAX_DESCR_LENGTH);
    CfaSetIfInfo (IF_DESC, u4IfIndex, &CfaIfInfo);

    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    /* get the HW address from the driver - if failure then dont show it */
    if (CfaIfmAddRcvAddrEntry (u4IfIndex, au1HwAddr, CFA_RCVADDR_NVOL)
        != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitPswIfEntry - "
                  "RcvAddrTab UCAST add fail on interface %d\n", u4IfIndex);
    }

    /* Add Psw Bcast addr also */
    MEMSET (au1HwAddr, 0, CFA_MAX_MEDIA_ADDR_LEN);
    MEMSET (au1HwAddr, 0xff, CFA_ENET_ADDR_LEN);
    if (CfaIfmAddRcvAddrEntry (u4IfIndex, au1HwAddr, CFA_RCVADDR_NVOL)
        != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitPswIfEntry - "
                  "RcvAddrTab BCAST add fail on interface %d\n", u4IfIndex);
    }

    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_ENETV2;
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);

    pHighStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
    pLowStackListScan = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);

    CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
    CFA_IF_STACK_STATUS (pLowStackListScan) = CFA_RS_ACTIVE;

    MEMSET (au1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmInitPswIfEntry - "
              "Initialized Enet interface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *  Function Name        : CfaIfmInitACIfEntry
 *
 *  Description          : This function allocates for AC IWF in the
 *                          interface table and initialises it to
 *                           default values.
 *
 *  Input(s)             : UINT4 u4IfIndex,
 *
 *  Output(s)            : None.
 *
 *  Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *  Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *  Exceptions or Operating
 *  System Error Handling    : None.
 *
 *  Use of Recursion        : None.
 *
 *  Returns            : CFA_SUCCESS if create and init succeeds,
 *                       otherwise CFA_FAILURE.
 *
 *
 *****************************************************************************/
INT4
CfaIfmInitACIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{

    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;
    UINT1               au1HwAddr[CFA_MAX_MEDIA_ADDR_LEN];
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4BridgeMode = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4IfMtu = CFA_ENET_MTU;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitPswIfEntry for Psw interface %d .\n",
              u4IfIndex);

    MEMSET (au1HwAddr, 0, CFA_MAX_MEDIA_ADDR_LEN);
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    /* allocate memory for PSW IWF struct - if failure then interface cant be
     * created */
    if ((CFA_IF_IWF (u4IfIndex) =
         MemAllocMemBlk (CFA_ENET_IWF_MEMPOOL)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitPswIfEntry - "
                  "IWF struct malloc fail for interface %d\n", u4IfIndex);
        /* restore the GDD entry */
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    MEMSET (CFA_IF_IWF (u4IfIndex), 0, sizeof (tEnetIwfStruct));
    /*CFA_OTHER-- need to check - PROP_VIRTUAL_INTERFACE */
    CfaSetIfType (u4IfIndex, CFA_PROP_VIRTUAL_INTERFACE);
    CfaSetIfName (u4IfIndex, au1PortName);
    CfaSetPortName (u4IfIndex, au1PortName);
    /*At PB the bridge port of AC should be by default CEP */
    L2IwfGetBridgeMode (u4ContextId, &u4BridgeMode);

    if (u4BridgeMode == VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        CfaSetIfBrgPortType (u4IfIndex, CFA_CUSTOMER_EDGE_PORT);
    }
    /* initialise the IWF structures for PSW */
    CfaIwfACInit (u4IfIndex);

    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        if (CfaIsMgmtPort (u4IfIndex) == TRUE)
        {
            return CFA_FAILURE;
        }
        else
        {
            CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_ENABLED);
        }
    }

    CfaGetIfDescr (u4IfIndex, CFA_IF_DESCR (u4IfIndex));

    MEMSET (CfaIfInfo.au1Descr, 0, CFA_MAX_DESCR_LENGTH);
    MEMCPY (CfaIfInfo.au1Descr, CFA_IF_DESCR (u4IfIndex), CFA_MAX_DESCR_LENGTH);
    CfaSetIfInfo (IF_DESC, u4IfIndex, &CfaIfInfo);

    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    /* get the HW address from the driver - if failure then dont show it */
    if (CfaIfmAddRcvAddrEntry (u4IfIndex, au1HwAddr, CFA_RCVADDR_NVOL)
        != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitPswIfEntry - "
                  "RcvAddrTab UCAST add fail on interface %d\n", u4IfIndex);
    }
    /* Add Psw Bcast addr also */
    MEMSET (au1HwAddr, 0, CFA_MAX_MEDIA_ADDR_LEN);
    MEMSET (au1HwAddr, 0xff, CFA_ENET_ADDR_LEN);
    if (CfaIfmAddRcvAddrEntry (u4IfIndex, au1HwAddr, CFA_RCVADDR_NVOL)
        != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitPswIfEntry - "
                  "RcvAddrTab BCAST add fail on interface %d\n", u4IfIndex);
    }

    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_ENETV2;
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);

    CfaSetIfMtu (u4IfIndex, u4IfMtu);
    CfaSetIfSpeed (u4IfIndex, CFA_ENET_SPEED);
    CfaSetIfHighSpeed (u4IfIndex, 0);

    pHighStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
    pLowStackListScan = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);

    CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
    CFA_IF_STACK_STATUS (pLowStackListScan) = CFA_RS_ACTIVE;

    MEMSET (au1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmInitPswIfEntry - "
              "Initialized Enet interface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}

#ifdef VXLAN_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaIfmInitNveIfEntry
 *
 *    Description          : This function allocates for NVE IWF in the
 *                           interface table and initialises it to
 *                           default values.
 *
 *    Input(s)             : UINT4 u4IfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitNveIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{

    UINT4               u4IfSpeed = 0;
    UINT4               u4IfHighSpeed = 0;
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;
    tCfaIfInfo          CfaIfInfo;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitNveIfEntry for NVE interface %d .\n",
              u4IfIndex);

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    /* allocate memory for NVE IWF struct - if failure then interface cant be
       created */
    if ((CFA_IF_IWF (u4IfIndex) =
         MemAllocMemBlk (CFA_ENET_IWF_MEMPOOL)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitNveIfEntry - "
                  "IWF struct malloc fail for interface %d\n", u4IfIndex);
        /* restore the GDD entry */
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    MEMSET (CFA_IF_IWF (u4IfIndex), 0, sizeof (tEnetIwfStruct));

    CfaSetIfType (u4IfIndex, CFA_VXLAN_NVE);
    CfaSetIfName (u4IfIndex, au1PortName);
    CfaSetPortName (u4IfIndex, au1PortName);
    CfaSetIfMtu (u4IfIndex, CFA_DEF_VXLAN_NVE_MTU);
    CfaDetermineSpeed (u4IfIndex, &u4IfSpeed, &u4IfHighSpeed);
    CfaSetIfSpeed (u4IfIndex, u4IfSpeed);
    CfaSetIfNwType (u4IfIndex, CFA_NETWORK_TYPE_LAN);

    /*This will change the port-type according to Bridge Mode for NVE in SI mode */
    if (VcmGetL2ModeExt () == VCM_SI_MODE)
    {

        if (L2IwfPbSetDefBrgPortTypeInCfa (0, u4IfIndex,
                                           CFA_TRUE,
                                           CFA_FALSE) == L2IWF_FAILURE)
        {
            return (CFA_FAILURE);
        }
    }
    /* initialise the IWF structures for PSW */
    CfaIwfPswInit (u4IfIndex);

    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        if (CfaIsMgmtPort (u4IfIndex) == TRUE)
        {
            return CFA_FAILURE;
        }
        else
        {
            CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_ENABLED);
        }
    }

    CfaGetIfDescr (u4IfIndex, CFA_IF_DESCR (u4IfIndex));

    MEMSET (CfaIfInfo.au1Descr, 0, CFA_MAX_DESCR_LENGTH);
    MEMCPY (CfaIfInfo.au1Descr, CFA_IF_DESCR (u4IfIndex), CFA_MAX_DESCR_LENGTH);
    CfaSetIfInfo (IF_DESC, u4IfIndex, &CfaIfInfo);

    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_ENETV2;
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);

    pHighStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
    pLowStackListScan = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);

    CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
    CFA_IF_STACK_STATUS (pLowStackListScan) = CFA_RS_ACTIVE;

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmInitNveIfEntry - "
              "Initialized Enet interface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}

#endif
#ifdef VCPEMGR_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaIfmInitTapIfEntry
 *
 *    Description          : This function allocates for TAP IWF in the
 *                           interface table and initialises it to
 *                           default values.
 *
 *    Input(s)             : UINT4 u4IfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitTapIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{

    UINT4               u4IfSpeed = 0;
    UINT4               u4IfHighSpeed = 0;
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;
    tCfaIfInfo          CfaIfInfo;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitTapIfEntry for TAP interface %d .\n",
              u4IfIndex);

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    /* allocate memory for NVE IWF struct - if failure then interface cant be
       created */
    if ((CFA_IF_IWF (u4IfIndex) =
         MemAllocMemBlk (CFA_ENET_IWF_MEMPOOL)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitTapIfEntry - "
                  "IWF struct malloc fail for interface %d\n", u4IfIndex);
        /* restore the GDD entry */
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    MEMSET (CFA_IF_IWF (u4IfIndex), 0, sizeof (tEnetIwfStruct));

    CfaSetIfType (u4IfIndex, CFA_TAP);
    CfaSetIfName (u4IfIndex, au1PortName);
    CfaSetPortName (u4IfIndex, au1PortName);
    CfaSetIfMtu (u4IfIndex, CFA_MAX_FAST_ENET_MTU);
    CfaDetermineSpeed (u4IfIndex, &u4IfSpeed, &u4IfHighSpeed);
    CfaSetIfSpeed (u4IfIndex, u4IfSpeed);
    CfaSetIfNwType (u4IfIndex, CFA_NETWORK_TYPE_LAN);

    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        if (CfaIsMgmtPort (u4IfIndex) == TRUE)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                     "Error in CfaIfmInitTapIfEntry - CfaIsMgmtPort failed\n");
            return CFA_FAILURE;
        }
        else
        {
            CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_ENABLED);
        }
    }

    CfaGetIfDescr (u4IfIndex, CFA_IF_DESCR (u4IfIndex));

    MEMSET (CfaIfInfo.au1Descr, 0, CFA_MAX_DESCR_LENGTH);
    MEMCPY (CfaIfInfo.au1Descr, CFA_IF_DESCR (u4IfIndex), CFA_MAX_DESCR_LENGTH);
    CfaSetIfInfo (IF_DESC, u4IfIndex, &CfaIfInfo);

    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_ENETV2;
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);

    pHighStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
    pLowStackListScan = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);

    CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
    CFA_IF_STACK_STATUS (pLowStackListScan) = CFA_RS_ACTIVE;

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmInitTapIfEntry - "
              "Initialized TAP interface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}
#endif
/*****************************************************************************
 *
 *    Function Name        : CfaIfmInitVlanIfEntry
 *
 *    Description        : This function allocates an entry for VLAN interface
 *                         in the interface table and initialises it to
 *                         default values.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitVlanIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{
#ifndef WGS_WANTED
    UINT2               u2IfIvrVlanId = 0;
#endif /*WGS_WANTED */

    UINT1               au1HwAddr[CFA_ENET_ADDR_LEN];
    UINT4               u4IfSpeed;
    UINT4               u4IfHighSpeed;
    tCfaIfInfo          CfaIfInfo;
    tIpConfigInfo       IpCfgInfo;
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;
    UINT4               u4Flag;
    UINT1               u1ForwardStatus;
    INT1               *pu1VlanId;
#if (defined HB_WANTED && ICCH_WANTED)
    UINT2               u2HbVlanId = 0;
#endif
#if (defined  RM_WANTED) && (defined MBSM_WANTED)
    UINT2               u2VlanId;
    UINT4               u4SlotId;
#endif
    UINT4               u4IfMaxVlanId;

    CfaSetIfName (u4IfIndex, au1PortName);

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitVlanIfEntry for VLAN interface %d .\n",
              u4IfIndex);
    MEMSET (au1HwAddr, 0, CFA_ENET_ADDR_LEN);
    /* allocate for the IP config entry in the ifTable since IP can run
       over VLAN */
    if (CfaIpIfCreateIpInterface (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitVlanIfEntry - "
                  "IP Entry Mempool alloc fail interface %d.\n", u4IfIndex);
        /* free the IWF struct */
    }

    /* allocate memory for VLAN IWF struct - if failure then interface cant be
       created */
    if ((CFA_IF_IWF (u4IfIndex) =
         MemAllocMemBlk (CFA_ENET_IWF_MEMPOOL)) == NULL)

    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitVlanIfEntry - "
                  "IWF struct malloc fail for interface %d\n", u4IfIndex);
        /* call the FM fault handler */
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    MEMSET (CFA_IF_IWF (u4IfIndex), 0, sizeof (tEnetIwfStruct));

#ifdef WGS_WANTED
    CfaSetIfType (u4IfIndex, CFA_L2VLAN);
    u1ForwardStatus = CFA_DISABLED;
    STRNCPY (CFA_IF_DESCR (u4IfIndex), CFA_L2VLAN_DESCR,
             STRLEN (CFA_L2VLAN_DESCR));
    CFA_IF_DESCR (u4IfIndex)[STRLEN (CFA_L2VLAN_DESCR)] = '\0';

    /* when  the interface is  linux vlan interface 
       do not set VLAN list for the management L2 VLAN interface */
    if (CfaIsLinuxVlanIntf (u4IfIndex) == FALSE)
    {
        CfaSetVlanListForMgmtIface ();
    }
    UNUSED_PARAM (u4IfMaxVlanId);
    UNUSED_PARAM (pu1VlanId);
#else
    CfaSetIfType (u4IfIndex, CFA_L3IPVLAN);
    CfaSetIfWanType (u4IfIndex, CFA_WAN_TYPE_OTHER);
    u1ForwardStatus = CFA_ENABLED;
    STRNCPY (CFA_IF_DESCR (u4IfIndex), CFA_L3IPVLAN_DESCR,
             STRLEN (CFA_L3IPVLAN_DESCR));
    CFA_IF_DESCR (u4IfIndex)[STRLEN (CFA_L3IPVLAN_DESCR)] = '\0';
    if (STRNCASECMP (au1PortName, CFA_LINUX_L3IPVLAN_NAME_PREFIX,
                     STRLEN (CFA_LINUX_L3IPVLAN_NAME_PREFIX)) == 0)
    {
        /* Assumption linux vlan interface will have name as eth0.100 */
        pu1VlanId = (INT1 *) STRCHR (au1PortName, '.');
        if (pu1VlanId != NULL)
        {
            pu1VlanId++;
            u2IfIvrVlanId = (UINT2) ATOI (pu1VlanId);
        }
    }
    else
    {
        u2IfIvrVlanId = (UINT2) ATOI (&au1PortName[4]);
    }
    /* vlan 4094 is reserved for MBSM.In Coldstandby case, 
     * stack interface will be created for vlan 4094 */

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        u4IfMaxVlanId = CFA_DEFAULT_STACK_VLAN_ID;
    }
    else
    {
        u4IfMaxVlanId = VLAN_MAX_VLAN_ID;
    }
    if ((u2IfIvrVlanId < 1) || (u2IfIvrVlanId > u4IfMaxVlanId))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitVlanIfEntry - "
                  "Invalid VLAN ID %d\n", u4IfIndex);
        return CFA_FAILURE;
    }

    CfaSetIfIvrVlanId (u4IfIndex, u2IfIvrVlanId);
    /* for linux vlan interface  do not set gIntfVlanBitList */
    if (CfaIsLinuxVlanIntf (u4IfIndex) == FALSE)
    {
        /* Set this vlan as an interface Vlan */
        CfaCdbAddToIntfVlanList (u2IfIvrVlanId);
    }
#endif /* WGS_WANTED */

    /* For Linux Vlan Interface  get the mac address from the linux */
    if (CfaIsLinuxVlanIntf (u4IfIndex) == TRUE)
    {
        if (CfaGddOobGetOsHwAddr (CFA_DEFAULT_ROUTER_IFINDEX, au1HwAddr) !=
            CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmInitVlanIfEntry - "
                      "unable to get the MAC addr "
                      "for linux vlan interface %d\n", u4IfIndex);
            return (CFA_FAILURE);
        }
    }
    else
    {
        /* 
         * All the ISS VLAN interfaces can have the same MAC address. The VLAN 
         * interfaces are assigned the MAC address of the default bridged  
         * Ethernet interface by assuming that default Ethernet interface 
         * will be there in the system always.
         */
#ifdef UNIQUE_VLAN_MAC_WANTED
        /* If the compilation switch UNIQUE_VLAN_MAC_WANTED is defined, each vlan 
           can take different MAC address. Here the unique vlan MAC address is derived 
           from switch MAC address (ie) VLAN MAC = SWITCH MAC+ vlan If Index.
         */
        if (CfaGddGetVlanHwAddr (u4IfIndex, au1HwAddr) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmInitVlanIfEntry - "
                      "unable to get the MAC addr for interface %d\n",
                      u4IfIndex);
            return (CFA_FAILURE);
        }
#else
        if (VcmGetVRMacAddr (VCM_DEFAULT_CONTEXT, au1HwAddr) != VCM_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmInitVlanIfEntry - "
                      "unable to get the MAC address for interface %d\n",
                      u4IfIndex);
            return (CFA_FAILURE);
        }

#endif

    }
#if (defined  RM_WANTED) && (defined MBSM_WANTED)
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        CfaGetIfIvrVlanId (u4IfIndex, &u2VlanId);
        /* Incase of StackIvr StackMac should be taken. */
        if (u2VlanId == CFA_DEFAULT_STACK_VLAN_ID)
        {
            RmGetNodeSwitchId (&u4SlotId);
            CfaMbsmNpGetStackMac (u4SlotId, au1HwAddr);
        }
    }
#endif

    CfaSetIfHwAddr (u4IfIndex, au1HwAddr, CFA_ENET_ADDR_LEN);

    /* initialise the IWF structures for VLAN */
    CfaIwfEnetInit (u4IfIndex);
    CfaIfInfo.i4IpPort = CFA_INVALID_INDEX;
    CfaSetIfInfo (IF_IP_PORTNUM, u4IfIndex, &CfaIfInfo);

    CfaIfInfo.u4IpIntfStats = CFA_DISABLED;
    CfaSetIfInfo (IF_L3VLAN_INTF_STATS, u4IfIndex, &CfaIfInfo);

    IpCfgInfo.u1IpForwardEnable = u1ForwardStatus;
    IpCfgInfo.u1AddrAllocMethod = CFA_IP_ALLOC_MAN;
    IpCfgInfo.u1AddrAllocProto = CFA_PROTO_DHCP;
    u4Flag = CFA_IP_IF_FORWARD | CFA_IP_IF_ALLOC_PROTO | CFA_IP_IF_ALLOC_METHOD;

    if (CfaIpIfSetIfInfo (u4IfIndex, u4Flag, &IpCfgInfo) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Setting IP Param for Interface %d failed\n", u4IfIndex);
        return CFA_FAILURE;
    }

    CFA_IF_TRAP_EN (u4IfIndex) = CFA_DISABLED;    /* as it isn't a physical if */

    CfaSetIfMtu (u4IfIndex, CFA_ENET_MTU);
    CfaDetermineSpeed (u4IfIndex, &u4IfSpeed, &u4IfHighSpeed);
    CfaSetIfSpeed (u4IfIndex, u4IfSpeed);
    CfaSetIfHighSpeed (u4IfIndex, u4IfHighSpeed);
    CfaSetIfNwType (u4IfIndex, CFA_NETWORK_TYPE_LAN);

    CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_DISABLED);

    /*There is no Recv Addr table for VLAN interface */
    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_ENETV2;
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);

    pHighStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
    pLowStackListScan = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);

    CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
    CFA_IF_STACK_STATUS (pLowStackListScan) = CFA_RS_ACTIVE;

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmInitVlanIfEntry - "
              "Initialized VLAN interface %d \n", u4IfIndex);

#if (defined HB_WANTED && ICCH_WANTED)
    CfaGetIfIvrVlanId (u4IfIndex, &u2HbVlanId);

    if (LaGetMCLAGSystemStatus () == LA_MCLAG_ENABLED)
    {
        /* Create FDB entry with ICCL index and HB peer MAC for this new VLAN */
        HbUpdateIcclFdbEntryForVlan (u2HbVlanId, HB_FDB_ADD);
    }
#endif

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmInitL3SubIfEntry
 *
 *    Description        : This function allocates an entry for L3Sub interface
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitL3SubIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{
    UINT1               au1HwAddr[CFA_ENET_ADDR_LEN];
    UINT4               u4IfSpeed;
    UINT4               u4IfHighSpeed;
    tCfaIfInfo          CfaIfInfo;
    tIpConfigInfo       IpCfgInfo;
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;
    UINT4               u4Flag;
    UINT1               u1ForwardStatus;
    UINT1               u1TmpIfType = CFA_ENET_UNKNOWN;
#if (defined  RM_WANTED) && (defined MBSM_WANTED)
    UINT2               u2VlanId;
    UINT4               u4SlotId;
#endif
#if defined (WLC_WANTED) || defined (WTP_WANTED)
#ifdef LNXIP4_WANTED
    INT4                i4PhyPort = 0;
    INT4                i4LPortNum = 0;
    INT4                i4SlotNum = 0;
    UINT1              *pu1IfPortName;
#endif
#endif
    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitL3SubIfEntry for L3subinterface index %d .\n",
              u4IfIndex);

    /* Set Iface Name as Enet interface in Linux with vconf */
#ifndef LNXIP4_WANTED
    CfaSetIfName (u4IfIndex, au1PortName);
#else
#if defined (WLC_WANTED) || defined (WTP_WANTED)
    CfaSetPortName (u4IfIndex, au1PortName);
    /*Below change is to be parsed to get the interface for the corressponding
       num slot0 */
    pu1IfPortName = (au1PortName + CFA_SLOT_NUM_OFFSET);

    if ((CfaCliGetPhysicalAndLogicalPortNum ((INT1 *) pu1IfPortName,
                                             &i4PhyPort, &i4LPortNum,
                                             &i4SlotNum)) == CFA_SUCCESS)
    {
        SNPRINTF ((CHR1 *) au1PortName, CFA_MAX_PORT_NAME_LENGTH, "%s.%d",
                  CfaGddGetLnxIntfnameForPort (i4PhyPort), i4LPortNum);
        CfaSetIfName (u4IfIndex, au1PortName);
    }
#else
    UNUSED_PARAM (au1PortName);
#endif
#endif
    MEMSET (au1HwAddr, 0, CFA_ENET_ADDR_LEN);
    /* allocate for the IP config entry in the ifTable since IP can run
       over VLAN */
    if (CfaIpIfCreateIpInterface (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitL3SubIfEntry- "
                  "IP Entry Mempool alloc fail interface %d.\n", u4IfIndex);
        /* free the IWF struct */
    }

    /* allocate memory for VLAN IWF struct - if failure then interface cant be
       created */
    if ((CFA_IF_IWF (u4IfIndex) =
         MemAllocMemBlk (CFA_ENET_IWF_MEMPOOL)) == NULL)

    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitL3SubIfEntry- "
                  "IWF struct malloc fail for interface %d\n", u4IfIndex);
        /* call the FM fault handler */
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    MEMSET (CFA_IF_IWF (u4IfIndex), 0, sizeof (tEnetIwfStruct));

    u1TmpIfType = (UINT1) IssCustGetEthernetType (u4IfIndex);
    CfaSetEthernetType (u4IfIndex, u1TmpIfType);
    CfaSetIfType (u4IfIndex, CFA_L3SUB_INTF);
    CfaSetIfSubType (u4IfIndex, u1TmpIfType);
    CfaSetIfWanType (u4IfIndex, CFA_WAN_TYPE_OTHER);
    CFA_CDB_SET_ENCAP_STATUS (u4IfIndex, CFA_FALSE);
    u1ForwardStatus = CFA_ENABLED;

    /* For Linux Vlan Interface  get the mac address from the linux */
    if (CfaIsLinuxVlanIntf (u4IfIndex) == TRUE)
    {
        if (CfaGddOobGetOsHwAddr (CFA_DEFAULT_ROUTER_IFINDEX, au1HwAddr) !=
            CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmInitVlanIfEntry - "
                      "unable to get the MAC addr "
                      "for linux vlan interface %d\n", u4IfIndex);
            return (CFA_FAILURE);
        }
    }
    else
    {
        /* 
         * All the ISS VLAN interfaces can have the same MAC address. The VLAN 
         * interfaces are assigned the MAC address of the default bridged  
         * Ethernet interface by assuming that default Ethernet interface 
         * will be there in the system always.
         */
#ifdef UNIQUE_VLAN_MAC_WANTED
        /* If the compilation switch UNIQUE_VLAN_MAC_WANTED is defined, each vlan 
           can take different MAC address. Here the unique vlan MAC address is derived 
           from switch MAC address (ie) VLAN MAC = SWITCH MAC+ vlan If Index.
         */
        if (CfaGddGetVlanHwAddr (u4IfIndex, au1HwAddr) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmInitVlanIfEntry - "
                      "unable to get the MAC addr for interface %d\n",
                      u4IfIndex);
            return (CFA_FAILURE);
        }
#else
        if (VcmGetVRMacAddr (VCM_DEFAULT_CONTEXT, au1HwAddr) != VCM_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmInitVlanIfEntry - "
                      "unable to get the MAC address for interface %d\n",
                      u4IfIndex);
            return (CFA_FAILURE);
        }

#endif

    }
#if (defined  RM_WANTED) && (defined MBSM_WANTED)
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        CfaGetIfIvrVlanId (u4IfIndex, &u2VlanId);
        /* Incase of StackIvr StackMac should be taken. */
        if (u2VlanId == CFA_DEFAULT_STACK_VLAN_ID)
        {
            RmGetNodeSwitchId (&u4SlotId);
            CfaMbsmNpGetStackMac (u4SlotId, au1HwAddr);
        }
    }
#endif

    CfaSetIfHwAddr (u4IfIndex, au1HwAddr, CFA_ENET_ADDR_LEN);

    /* initialise the IWF structures for VLAN */
    CfaIwfEnetInit (u4IfIndex);
    CfaIfInfo.i4IpPort = CFA_INVALID_INDEX;
    CfaSetIfInfo (IF_IP_PORTNUM, u4IfIndex, &CfaIfInfo);

    CfaIfInfo.u4IpIntfStats = CFA_DISABLED;
    CfaSetIfInfo (IF_L3VLAN_INTF_STATS, u4IfIndex, &CfaIfInfo);

    IpCfgInfo.u1IpForwardEnable = u1ForwardStatus;
    IpCfgInfo.u1AddrAllocMethod = CFA_IP_ALLOC_MAN;
    IpCfgInfo.u1AddrAllocProto = CFA_PROTO_DHCP;
    u4Flag = CFA_IP_IF_FORWARD | CFA_IP_IF_ALLOC_PROTO | CFA_IP_IF_ALLOC_METHOD;

    if (CfaIpIfSetIfInfo (u4IfIndex, u4Flag, &IpCfgInfo) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Setting IP Param for Interface %d failed\n", u4IfIndex);
        return CFA_FAILURE;
    }

    CFA_IF_TRAP_EN (u4IfIndex) = CFA_DISABLED;    /* as it isn't a physical if */

    CfaSetIfMtu (u4IfIndex, CFA_ENET_MTU);
    CfaDetermineSpeed (u4IfIndex, &u4IfSpeed, &u4IfHighSpeed);
    CfaSetIfSpeed (u4IfIndex, u4IfSpeed);
    CfaSetIfHighSpeed (u4IfIndex, u4IfHighSpeed);
    CfaSetIfNwType (u4IfIndex, CFA_NETWORK_TYPE_LAN);

    CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_DISABLED);

    /*There is no Recv Addr table for VLAN interface */
    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_ENETV2;
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);

    pHighStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
    pLowStackListScan = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);

    CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
    CFA_IF_STACK_STATUS (pLowStackListScan) = CFA_RS_ACTIVE;

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmInitL3SubIfEntry- "
              "Initialized L3Subinterface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmInitLoopbackIfEntry
 *
 *    Description        : This function allocates an entry for loopback interface
 *                         in the interface table and initialises it to
 *                         default values.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitLoopbackIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{
    tIpConfigInfo       IpCfgInfo;
    tCfaIfInfo          CfaIfInfo;
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;
    UINT4               u4Flag;
    UINT2               u2IfLoopbackId;
    UINT1               u1ForwardStatus;
    UINT4               u4IfSpeed = CFA_ENET_SPEED_10M;
    UINT4               u4IfHighSpeed = CFA_ENET_MAX_SPEED_VALUE;
    UINT1               au1HwAddr[CFA_ENET_ADDR_LEN];

    MEMSET (au1HwAddr, 0, CFA_ENET_ADDR_LEN);

    CfaSetIfName (u4IfIndex, au1PortName);

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitLoopbackIfEntry for Loopback  interface %d .\n",
              u4IfIndex);

    /* allocate for the IP config entry in the ifTable since IP can run
       over Loopback */
    if (CfaIpIfCreateIpInterface (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitLoopbackIfEntry - "
                  "IP Entry Mempool alloc fail interface %d.\n", u4IfIndex);
    }

    u1ForwardStatus = CFA_ENABLED;
    STRNCPY (CFA_IF_DESCR (u4IfIndex), CFA_LOOPBACK_DESCR,
             STRLEN (CFA_LOOPBACK_DESCR));
    CFA_IF_DESCR (u4IfIndex)[STRLEN (CFA_LOOPBACK_DESCR)] = '\0';

    u2IfLoopbackId = (UINT2) ATOI (&au1PortName[8]);
    CfaSetIfLoopbackId (u4IfIndex, u2IfLoopbackId);

    CfaIfInfo.i4IpPort = CFA_INVALID_INDEX;
    CfaSetIfInfo (IF_IP_PORTNUM, u4IfIndex, &CfaIfInfo);

    IpCfgInfo.u1IpForwardEnable = u1ForwardStatus;
    IpCfgInfo.u1AddrAllocMethod = CFA_IP_ALLOC_MAN;
    IpCfgInfo.u1AddrAllocProto = CFA_PROTO_DHCP;
    u4Flag = CFA_IP_IF_FORWARD | CFA_IP_IF_ALLOC_PROTO | CFA_IP_IF_ALLOC_METHOD;

    if (CfaIpIfSetIfInfo (u4IfIndex, u4Flag, &IpCfgInfo) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Setting IP Param for Interface %d failed\n", u4IfIndex);
        return CFA_FAILURE;
    }

    CFA_IF_TRAP_EN (u4IfIndex) = CFA_DISABLED;    /* as it isn't a physical if */

    if (VcmGetVRMacAddr (VCM_DEFAULT_CONTEXT, au1HwAddr) != VCM_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitLoopbackIfEntry - "
                  "unable to get the MAC address for interface %d\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

    CfaSetIfHwAddr (u4IfIndex, au1HwAddr, CFA_ENET_ADDR_LEN);

    CfaSetIfMtu (u4IfIndex, CFA_ENET_MTU);
    CfaDetermineSpeed (u4IfIndex, &u4IfSpeed, &u4IfHighSpeed);
    CfaSetIfSpeed (u4IfIndex, u4IfSpeed);
    CfaSetIfHighSpeed (u4IfIndex, u4IfHighSpeed);
    CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_DISABLED);

    /*There is no Recv Addr table for Loopback interface */
    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_ENETV2;
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);
    pHighStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
    pLowStackListScan = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);

    CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
    CFA_IF_STACK_STATUS (pLowStackListScan) = CFA_RS_ACTIVE;

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmInitVlanIfEntry - "
              "Initialized VLAN interface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmInitPortChannelIfEntry
 *
 *    Description        : This function allocates an entry for VLAN interface
 *                         in the interface table and initialises it to
 *                         default values.
 *
 *    Input(s)            : UINT4 u4IfIndex, UINT1 *au1PortName
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitPortChannelIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{
    UINT1               au1IfHwAddr[CFA_ENET_ADDR_LEN];
    UINT4               u4IfSpeed;
    UINT4               u4IfHighSpeed;
    UINT4               u4IfMtu = CFA_ENET_MTU;
    tCfaIfInfo          CfaIfInfo;
    UINT1               u1BrgPortType = CFA_CUSTOMER_BRIDGE_PORT;
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;

    if (CfaIfUtlMauMemAlloc (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitEnetIfEntry - "
                  "Mau struct malloc fail for interface %d\n", u4IfIndex);
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return CFA_FAILURE;
    }

    CfaSetIfName (u4IfIndex, au1PortName);

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitPortChannelIfEntry for PortChannel interface %d .\n",
              u4IfIndex);

    /* allocate memory for PortChannel IWF struct - if failure then interface cant be
       created */
    if ((CFA_IF_IWF (u4IfIndex) =
         MemAllocMemBlk (CFA_ENET_IWF_MEMPOOL)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitPortChannelIfEntry - "
                  "IWF struct malloc fail for interface %d\n", u4IfIndex);
        CfaIfUtlMauMemRelease (u4IfIndex);
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    MEMSET (CFA_IF_IWF (u4IfIndex), 0, sizeof (tEnetIwfStruct));

    CfaSetIfType (u4IfIndex, CFA_LAGG);
    STRNCPY (CFA_IF_DESCR (u4IfIndex), CFA_LAGG_DESCR, STRLEN (CFA_LAGG_DESCR));
    CFA_IF_DESCR (u4IfIndex)[STRLEN (CFA_LAGG_DESCR)] = '\0';

    MEMSET (au1IfHwAddr, 0, CFA_ENET_ADDR_LEN);

    IssGetAggregatorMac ((UINT2) u4IfIndex, au1IfHwAddr);
    CfaSetIfHwAddr (u4IfIndex, au1IfHwAddr, CFA_ENET_ADDR_LEN);

    /* initialise the IWF structures for VLAN */
    CfaIwfEnetInit (u4IfIndex);
    CfaIfInfo.i4IpPort = CFA_INVALID_INDEX;
    CfaSetIfInfo (IF_IP_PORTNUM, u4IfIndex, &CfaIfInfo);

    CFA_IF_TRAP_EN (u4IfIndex) = CFA_ENABLED;    /* as it is a physical if */

    CfaSetIfMtu (u4IfIndex, u4IfMtu);
    CfaDetermineSpeed (u4IfIndex, &u4IfSpeed, &u4IfHighSpeed);
    CfaSetIfSpeed (u4IfIndex, u4IfSpeed);
    CfaSetIfHighSpeed (u4IfIndex, u4IfHighSpeed);
    CfaSetIfNwType (u4IfIndex, CFA_NETWORK_TYPE_LAN);

    CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_ENABLED);

    /* Since restoration happens only after all modules are started, 
       port-channel will be created only after the modules are created. */
    u1BrgPortType = VlanGetDefPortType (u4IfIndex);
    CfaSetIfBrgPortType (u4IfIndex, u1BrgPortType);

    /*There is no Recv Addr table for PortChannel interface */
    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_ENETV2;
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);
    pHighStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
    pLowStackListScan = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);
    CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
    CFA_IF_STACK_STATUS (pLowStackListScan) = CFA_RS_ACTIVE;

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmInitPortChannelIfEntry - "
              "Initialised PortChannel interface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}

#ifdef HDLC_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaIfmInitHdlcIfEntry
 *
 *    Description        : This function allocates an entry for VLAN interface
 *                         in the interface table and initialises it to
 *                         default values.
 *
 *    Input(s)            : UINT4 u4IfIndex, UINT1 *au1PortName
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitHdlcIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{
    CfaSetIfType (u4IfIndex, (UINT1) CFA_HDLC);
    CfaSetIfName (u4IfIndex, au1PortName);

    /* create the entry in the GDD table for this interface */
    if (CfaGddInitPort (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitHdlcIfEntry - "
                  "GDD entry creation fail for interface %d\n", u4IfIndex);
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    /* we first register this interface with the GDD module immediately */
    if (CfaGddRegisterPort (u4IfIndex, CFA_PPP) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitPvcIfEntry - "
                  "Registration fail for interface %d\n", u4IfIndex);
        /* restore the GDD entry */
        CfaGddDeInitPort (u4IfIndex);
        CfaSetIfType ((UINT4) u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    STRNCPY (CFA_IF_DESCR (u4IfIndex), CFA_HDLC_DESCR, STRLEN (CFA_HDLC_DESCR));
    CFA_IF_DESCR (u4IfIndex)[STRLEN (CFA_HDLC_DESCR)] = '\0';

    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_OTHER;
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);
    CfaSetIfSpeed (u4IfIndex, CFA_HDLC_SPEED);
    CfaSetIfNwType (u4IfIndex, CFA_NETWORK_TYPE_WAN);
    /* Setting the MTU as that of ethernet so that WAN to LAN traffic
     * need not be fragmented */
    CfaSetIfMtu (u4IfIndex, CFA_MAX_FAST_ENET_MTU);

    return (CFA_SUCCESS);
}
#endif /* HDLC_WANTED */

/*****************************************************************************
 *
 *    Function Name        : CfaIfmInitInternalIfEntry
 *
 *    Description        : This function allocates an entry for Internal 
 *                         interface in the interface table and initialises
 *                         it to default values.
 *
 *    Input(s)            : UINT4 u4IfIndex, UINT1 *au1PortName
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitInternalIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{
    tCfaIfInfo          CfaIfInfo;
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitInternalIfEntry for Internal interface %d .\n",
              u4IfIndex);

    CfaSetIfName (u4IfIndex, au1PortName);

    CfaSetIfType (u4IfIndex, CFA_BRIDGED_INTERFACE);

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        STRNCPY (CFA_IF_DESCR (u4IfIndex), CFA_BRIDGED_INTERFACE_DESCR,
                 STRLEN (CFA_BRIDGED_INTERFACE_DESCR));
        CFA_IF_DESCR (u4IfIndex)[STRLEN (CFA_BRIDGED_INTERFACE_DESCR)] = '\0';
    }
    CfaIfInfo.i4IpPort = CFA_INVALID_INDEX;
    CfaSetIfInfo (IF_IP_PORTNUM, u4IfIndex, &CfaIfInfo);

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        CFA_IF_TRAP_EN (u4IfIndex) = CFA_DISABLED;    /* as it is not a physical if */
    }
    CfaSetIfSpeed (u4IfIndex,
                   ppCfaIfDefaultVal[CFA_BRIDGED_INTERFACE]->u4IfSpeed);

    CfaSetIfMtu (u4IfIndex, ppCfaIfDefaultVal[CFA_BRIDGED_INTERFACE]->u4IfMtu);

    CfaSetIfHighSpeed (u4IfIndex, 0);

    CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_ENABLED);
    CfaSetIfInternalStatus (u4IfIndex, CFA_TRUE);

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        /*There is no Recv Addr table for Internal interface */
        TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

        CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
        pHighStackListScan =
            (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
        CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
        pLowStackListScan =
            (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
        CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);
        CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
        CFA_IF_STACK_STATUS (pLowStackListScan) = CFA_RS_ACTIVE;
        ++CFA_IFNUMBER ();
    }
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);

#ifdef NPAPI_WANTED
    /* Hardware Call for Creating Internal Port */
    if (FsCfaHwCreateInternalPort (u4IfIndex) == FNP_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Creating Internal Interface %d in Hardware failed\n",
                  u4IfIndex);
        return CFA_FAILURE;
    }
#endif
    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmInitInternalIfEntry - "
              "Initialized Internal interface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *    Function Name      : CfaIfmInitILanIfEntry
 *    Description        : This function creates & initializes the entries 
 *                         in the ifTable for ILAN interfaces 
 *    Input(s)           : UINT4 u4IfIndex,
 *                         UINT1 *au1PortName      
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitILanIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{
    tCfaIfInfo          CfaIfInfo;
    UINT1               u1BrgPortType = CFA_INVALID_BRIDGE_PORT;
#ifdef NPAPI_WANTED
    tHwPortArray        HwPortArray;
#endif
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitILanIfEntry for ILAN interface %d .\n",
              u4IfIndex);

#ifdef NPAPI_WANTED
    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));
#endif
    CfaSetIfName (u4IfIndex, au1PortName);

    CfaSetIfType (u4IfIndex, CFA_ILAN);
    STRNCPY (CFA_IF_DESCR (u4IfIndex), CFA_ILAN_DESCR, STRLEN (CFA_ILAN_DESCR));
    CFA_IF_DESCR (u4IfIndex)[STRLEN (CFA_ILAN_DESCR)] = '\0';

    CfaIfInfo.i4IpPort = CFA_INVALID_INDEX;
    CfaSetIfInfo (IF_IP_PORTNUM, u4IfIndex, &CfaIfInfo);

    CFA_IF_TRAP_EN (u4IfIndex) = CFA_DISABLED;    /* as it is not a physical if */

    CfaSetIfSpeed (u4IfIndex, ppCfaIfDefaultVal[CFA_ILAN]->u4IfSpeed);

    CfaSetIfMtu (u4IfIndex, ppCfaIfDefaultVal[CFA_ILAN]->u4IfMtu);

    CfaSetIfHighSpeed (u4IfIndex, 0);

    CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_DISABLED);

    CfaSetIfBrgPortType (u4IfIndex, u1BrgPortType);

    /* No RcvAddrTable for logical interface */
    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);
    pHighStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
    pLowStackListScan = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);

    CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
    CFA_IF_STACK_STATUS (pLowStackListScan) = CFA_RS_ACTIVE;

    /* Call NPAPI to create ILAN interface */
#ifdef NPAPI_WANTED
    FsCfaHwCreateILan (u4IfIndex, HwPortArray);
#endif

    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
              "Initialized ILAN interface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmDeleteIfEntry
 *
 *    Description        : This function de-allocates the entry from
 *                the interface table and releases its IWF
 *                structure too. The calling function should
 *                de-register this interface with the higher
 *                and lower layers - this functions simply
 *                de-allocates the memory.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *                UINT1 u1IfType
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *                gIfGlobal (Interface's global struct)
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *                gIfGlobal (Interface's global struct)
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if deletion succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmDeleteIfEntry (UINT4 u4IfIndex, UINT1 u1IfType)
{

    CFA_DBG2 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmDeleteIfEntry for interface %d of type %d.\n",
              u4IfIndex, u1IfType);

    /* check if the IfEntry is valid */
    if (CfaValidateCfaIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteIfEntry - invalid interface %d.\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

    if ((u1IfType == CFA_ENET) || (u1IfType == CFA_LAGG))
    {
        CfaIfUtlMauMemRelease (u4IfIndex);
    }

    /* Changes for flexible interface index
     * Remove from DLL */
    CfaIfUtlIfTypeDbRem (u4IfIndex, CFA_CDB_IFTYPE_REQ);

    CfaInitCdbInfo (u4IfIndex);

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        /* update the ifTable globals */
        OsixGetSysTime (&CFA_IFSTK_CHNG ());
        --CFA_IFNUMBER ();
        CFA_IFTAB_CHNG () = CFA_IFSTK_CHNG ();

        /* delete the linked lists */
        if (TMO_SLL_Count (&CFA_IF_STACK_LOW (u4IfIndex)) > 0)
        {
            TMO_SLL_FreeNodes ((&CFA_IF_STACK_LOW (u4IfIndex)),
                               gCfaStackPoolId);
        }
        if (TMO_SLL_Count (&CFA_IF_STACK_HIGH (u4IfIndex)) > 0)
        {
            TMO_SLL_FreeNodes ((&CFA_IF_STACK_HIGH (u4IfIndex)),
                               gCfaStackPoolId);
        }
        if (TMO_SLL_Count (&CFA_IF_RCVADDRTAB (u4IfIndex)) > 0)
        {
            TMO_SLL_FreeNodes ((&CFA_IF_RCVADDRTAB (u4IfIndex)),
                               gCfaRcvAddrPoolId);
        }
        /* free the IWF structures */
        if (CFA_IF_IWF (u4IfIndex) != NULL)
        {
#ifdef PPP_WANTED
            if (u1IfType == CFA_PPP)
            {
                MEM_FREE (CFA_IF_IWF (u4IfIndex));
            }
            else
#endif
            {
                MemReleaseMemBlock (CFA_ENET_IWF_MEMPOOL, (UINT1 *)
                                    (CFA_IF_IWF (u4IfIndex)));
            }
        }

        /* make pointers to global strings NULL */
        STRNCPY (CFA_IF_DESCR (u4IfIndex), "", STRLEN (""));
        CFA_IF_DESCR (u4IfIndex)[STRLEN ("")] = '\0';

        /* clear the name of device - except for PPP, all other names point to the
           GDD structure and should not be cleared here */

        /* de-allocate memory taken from GDD entry Mempool, if allocated 
           This check is necessary during shutdown - for other deletions
           the respective protocol specific routines do the release */
        if (CFA_GDD_ENTRY (u4IfIndex) != NULL)
        {
            if (MemReleaseMemBlock (CFA_GDD_MEMPOOL,
                                    (UINT1 *) CFA_GDD_ENTRY (u4IfIndex)) !=
                MEM_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                         "Error in CfaIfmDeleteIfEntry - "
                         "Cant releasing ifEntrye MemBlock.\n");

            }
            else
            {
                CFA_GDD_ENTRY (u4IfIndex) = NULL;
            }
        }

        /* If the IP Interface is avaiable over the interface,delete it */
        CfaIpIfDeleteIpInterface (u4IfIndex);
        /* de-allocate memory taken from IF Mempool */
        if (CFA_SUCCESS != (CfaIfUtlIfDbRem (u4IfIndex, CFA_GAP_IFDB_REQ)))
        {
            CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                      "Error in CfaIfmDeleteIfEntry"
                      "Error in deleting interface from GapIfDb %d \n",
                      u4IfIndex);
            return (CFA_FAILURE);
        }
    }

    /* Check if the deleted interface is a logical interface and if there */
    /* are no available interfaces assign the deleted interface to the */
    /* available ifIndex */

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        if (CFA_AVAIL_IFINDEX () == 0)
            CFA_AVAIL_IFINDEX () = u4IfIndex;
    }
    /* Changes for flexible interface index
     * Remove from IfDB */

    CfaIfUtlIfDbRem (u4IfIndex, CFA_CDB_IFDB_REQ);

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmDeleteIfEntry - Deleted interface %d \n",
              u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmDeleteEnetInterface
 *
 *    Description        : This function is for for de-registration of
 *                interface with PPP/MP. Used for both PPP and
 *                MP interfaces.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if deletion succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmDeleteEnetInterface (UINT4 u4IfIndex, UINT1 u1DelIfEntry)
{

    UINT1               u1IfType = CFA_NONE;
    UINT1               u1Code;
    UINT1               u1BridgedIfStatus;
    UINT4               u4IfUfdGroupId = 0;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmDeleteEnetInterface "
              "for Enet interface %d\n", u4IfIndex);

/* first we de-register the interface from IP or higher layer if it was
registered earlier */

    if (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX)
    {
        /* this interface is registered with IP */
        u1Code = (u1DelIfEntry == CFA_FALSE) ? CFA_NET_IF_NIS : CFA_NET_IF_DEL;

        if (CfaIfmConfigNetworkInterface
            (u1Code, u4IfIndex, 0, NULL) != CFA_SUCCESS)
        {
/* unsuccessful in de-registering from IP - we cant delete the interface.
we dont expect this to fail - just for handling unforeseen exceptions. */
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                      "Error in CfaIfmDeleteEnetInterface - "
                      "IP De-reg fail for interface %d\n", u4IfIndex);
            return (CFA_FAILURE);
        }
    }                            /* end of interface was registered with IP */

/* currently there is no other interface possible over Enet */

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteEnetInterface - "
              "Un-reg with HL success - interface %d\n", u4IfIndex);
    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfStatus);
    CfaGetIfType (u4IfIndex, &u1IfType);
    if (u1BridgedIfStatus == CFA_ENABLED)
    {
        /* If the Port is OOB,we Should not update that to L2IWF */
        if (CfaIsMgmtPort (u4IfIndex) == FALSE)
        {
            L2IwfPortUnmapIndication (u4IfIndex);
            EoamApiNotifyIfDelete (u4IfIndex);
#ifdef RMON_WANTED
            RmonDeletePort (u4IfIndex);
#endif /* RMON_WANTED */
#ifdef ISS_WANTED
            IssDeletePort ((UINT2) u4IfIndex, ISS_ALL_TABLES);
#ifdef QOSX_WANTED
            tCfaIfInfo          IfInfo;
            CfaGetIfInfo (u4IfIndex, &IfInfo);
            if (!((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
                  (IfInfo.u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
            {
                QosDeletePort (u4IfIndex);
            }
#endif
#endif
        }

        if (!(CFA_ENET_CREATE_DELETE_ALLOWED))
        {
            /* Unmap Port from its context */
            VcmUnMapPortFromContext (u4IfIndex);
        }
    }
    else
    {
        L2IwfDeletePort (u4IfIndex, u1IfType, L2IWF_FALSE);
    }
#ifdef LA_WANTED
    LaUnmapPort ((UINT2) u4IfIndex);
#endif /* LA_WANTED */

    /* check whether the entry is to be deleted from IfTable itself */
    if (u1DelIfEntry == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "In CfaIfmDeletePppInterface - "
                  "disabling success - interface %d\n", u4IfIndex);

        return (CFA_SUCCESS);
    }

    CfaGddDeRegisterPort (u4IfIndex);
    CfaGddDeInitPort (u4IfIndex);

    CfaGetIfType (u4IfIndex, &u1IfType);

    if (u1IfType == CFA_ENET || u1IfType == CFA_LAGG)
    {
        if (CfaGetIfUfdGroupId (u4IfIndex, &u4IfUfdGroupId) != CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                     "CfaIfmDeleteEnetInterface - Failure in getting UFD group Id\n");
        }
        /* Before removing the port/port-channel
         * it has to be removed from the UFD group
         * if the port/port channel is in the group*/

        if (u4IfUfdGroupId != 0)
        {
            if (CfaUfdCheckLastDownlinkInGroup (u4IfIndex) == OSIX_FALSE)
            {
                if (CfaUfdRemovePortFromGroup (u4IfIndex,
                                               u4IfUfdGroupId) != CFA_SUCCESS)
                {
                    CLI_SET_ERR (CLI_CFA_UFD_PORT_DEL_ERR);
                    CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                             "CfaIfmDeleteEnetInterface - Failure in deleting the port from the UFD group\n");
                }
            }
        }
    }

/* delete the interface from the IfTable */
    if (CfaIfmDeleteIfEntry (u4IfIndex, u1IfType) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteEnetInterface - "
                  "ifEntry del fail for interface %d\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteEnetInterface - "
              "Full Delete success - interface %d\n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*******************************************************************
 *  Function Name                 : CfaIfmDeletePswInterface
 *   
 *  Description                   : This function is for deleting the 
 *                                  Pseudo wire Interface.
 *   
 *  Input(s)                      : u4IfIndex,
 *  
 *  Output(s)                     : None.
 *  
 *  Global Variables Referred     : IfTable (gapIfTable) structure,
 *  
 *  Global Variables Modified     : IfTable (gapIfTable) structure.
 *                                  Exceptions or Operating
 *  System Error Handling         : None.
 *  
 *  Use of Recursion              : None.
 *                   
 *  Returns                       :  CFA_SUCCESS if deletion succeeds,
 *                                  otherwise CFA_FAILURE.
 *
 *
 * *********************************************************************/
INT4
CfaIfmDeletePswInterface (UINT4 u4IfIndex, UINT1 u1DelIfEntry)
{

    UINT1               u1IfType = 0;
    UINT1               u1BridgedIfStatus = 0;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmDeletePswInterface "
              "for Enet interface %d\n", u4IfIndex);

/* currently there is no other interface possible over Psw */

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeletePswInterface - "
              "Un-reg with HL success - interface %d\n", u4IfIndex);
    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfStatus);
    if (u1BridgedIfStatus == CFA_ENABLED)
    {
        /* If the Port is OOB,we Should not update that to L2IWF */
        if (CfaIsMgmtPort (u4IfIndex) == FALSE)
        {
            L2IwfPortUnmapIndication (u4IfIndex);
            EoamApiNotifyIfDelete (u4IfIndex);
#ifdef RMON_WANTED
            RmonDeletePort (u4IfIndex);
#endif /* RMON_WANTED */
#ifdef ISS_WANTED
            IssDeletePort ((UINT2) u4IfIndex, ISS_ALL_TABLES);
#ifdef QOSX_WANTED
            tCfaIfInfo          IfInfo;
            CfaGetIfInfo (u4IfIndex, &IfInfo);
            if (!((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
                  (IfInfo.u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
            {
                QosDeletePort (u4IfIndex);
            }
#endif
#endif
        }

        if (!(CFA_PSW_CREATE_DELETE_ALLOWED))
        {
            /* Unmap Port from its context */
            VcmUnMapPortFromContext (u4IfIndex);
        }
    }

    /* check whether the entry is to be deleted from IfTable itself */
    if (u1DelIfEntry == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "In CfaIfmDeletePppInterface - "
                  "disabling success - interface %d\n", u4IfIndex);

        return (CFA_SUCCESS);
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

/* delete the interface from the IfTable */
    if (CfaIfmDeleteIfEntry (u4IfIndex, u1IfType) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeletePswInterface - "
                  "ifEntry del fail for interface %d\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeletePswInterface - "
              "Full Delete success - interface %d\n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*******************************************************************
 *  Function Name                 : CfaIfmDeleteACInterface
 *
 *Description                   : This function is for deleting the
 *                                 Pseudo wire Interface.
 *
 *Input(s)                      : u4IfIndex,
 *
 *Output(s)                     : None.
 *
 *Global Variables Referred     : IfTable (gapIfTable) structure,
 *
 *Global Variables Modified     : IfTable (gapIfTable) structure.
 *                               Exceptions or Operating
 *System Error Handling         : None.
 *
 *Use of Recursion              : None.
 *
 *Returns                       :  CFA_SUCCESS if deletion succeeds,
 *                                 otherwise CFA_FAILURE.
 *
 *********************************************************************/
INT4
CfaIfmDeleteACInterface (UINT4 u4IfIndex, UINT1 u1DelIfEntry)
{

    UINT1               u1IfType = 0;
    UINT1               u1BridgedIfStatus = 0;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmDeleteACInterface "
              "for Enet interface %d\n", u4IfIndex);

/* currently there is no other interface possible over AC */

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteACInterface - "
              "Un-reg with HL success - interface %d\n", u4IfIndex);
    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfStatus);
    if (u1BridgedIfStatus == CFA_ENABLED)
    {
        /* If the Port is OOB,we Should not update that to L2IWF */
        if (CfaIsMgmtPort (u4IfIndex) == FALSE)
        {
            L2IwfPortUnmapIndication (u4IfIndex);
            EoamApiNotifyIfDelete (u4IfIndex);
#ifdef RMON_WANTED
            RmonDeletePort (u4IfIndex);
#endif /* RMON_WANTED */
#ifdef ISS_WANTED
            IssDeletePort ((UINT2) u4IfIndex, ISS_ALL_TABLES);
#ifdef QOSX_WANTED
            tCfaIfInfo          IfInfo;
            CfaGetIfInfo (u4IfIndex, &IfInfo);
            if (!((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
                  (IfInfo.u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
            {
                QosDeletePort (u4IfIndex);
            }
#endif
#endif
        }

        if (!(CFA_AC_CREATE_DELETE_ALLOWED))
        {
            /* Unmap Port from its context */
            VcmUnMapPortFromContext (u4IfIndex);
        }
    }
    /* check whether the entry is to be deleted from IfTable itself */
    if (u1DelIfEntry == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "In CfaIfmDeleteACInterface - "
                  "disabling success - interface %d\n", u4IfIndex);

        return (CFA_SUCCESS);
    }

    CfaGetIfType (u4IfIndex, &u1IfType);
/* delete the interface from the IfTable */
    if (CfaIfmDeleteIfEntry (u4IfIndex, u1IfType) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteACInterface - "
                  "ifEntry delete fail for interface %d\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteACInterface - "
              "Full Delete success - interface %d\n", u4IfIndex);

    return (CFA_SUCCESS);
}

#ifdef VXLAN_WANTED
/*******************************************************************
 *  Function Name                 : CfaIfmDeleteNveInterface
 *   
 *  Description                   : This function is for deleting the 
 *                                  NVE Interface.
 *   
 *  Input(s)                      : u4IfIndex,
 *  
 *  Output(s)                     : None.
 *  
 *  Global Variables Referred     : IfTable (gapIfTable) structure,
 *  
 *  Global Variables Modified     : IfTable (gapIfTable) structure.
 *                                  Exceptions or Operating
 *  System Error Handling         : None.
 *  
 *  Use of Recursion              : None.
 *                   
 *  Returns                       :  CFA_SUCCESS if deletion succeeds,
 *                                  otherwise CFA_FAILURE.
 *
 *
 * *********************************************************************/
INT4
CfaIfmDeleteNveInterface (UINT4 u4IfIndex, UINT1 u1DelIfEntry)
{

    UINT1               u1IfType = 0;
    UINT1               u1BridgedIfStatus = 0;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmDeleteNveInterface "
              "for Enet interface %d\n", u4IfIndex);

/* currently there is no other interface possible over Psw */

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteNveInterface - "
              "Un-reg with HL success - interface %d\n", u4IfIndex);
    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfStatus);
    if (u1BridgedIfStatus == CFA_ENABLED)
    {
        /* If the Port is OOB,we Should not update that to L2IWF */
        if (CfaIsMgmtPort (u4IfIndex) == FALSE)
        {
            L2IwfPortUnmapIndication (u4IfIndex);
            EoamApiNotifyIfDelete (u4IfIndex);
#ifdef RMON_WANTED
            RmonDeletePort (u4IfIndex);
#endif /* RMON_WANTED */
#ifdef ISS_WANTED
            IssDeletePort ((UINT2) u4IfIndex, ISS_ALL_TABLES);
#endif
        }
        /* Unmap Port from its context */
        VcmUnMapPortFromContext (u4IfIndex);
    }

    VxlanPortDelNveInterface (u4IfIndex);
    /* check whether the entry is to be deleted from IfTable itself */
    if (u1DelIfEntry == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "In CfaIfmDeleteNveInterface - "
                  "disabling success - interface %d\n", u4IfIndex);

        return (CFA_SUCCESS);
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

/* delete the interface from the IfTable */
    if (CfaIfmDeleteIfEntry (u4IfIndex, u1IfType) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteNveInterface - "
                  "ifEntry del fail for interface %d\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteNveInterface - "
              "Full Delete success - interface %d\n", u4IfIndex);

    return (CFA_SUCCESS);
}

#endif
#ifdef VCPEMGR_WANTED
/*******************************************************************
 *  Function Name                 : CfaIfmDeleteTapInterface
 *   
 *  Description                   : This function is for deleting the 
 *                                  TAP Interface.
 *   
 *  Input(s)                      : u4IfIndex,
 *  
 *  Output(s)                     : None.
 *  
 *  Global Variables Referred     : IfTable (gapIfTable) structure,
 *  
 *  Global Variables Modified     : IfTable (gapIfTable) structure.
 *                                  Exceptions or Operating
 *  System Error Handling         : None.
 *  
 *  Use of Recursion              : None.
 *                   
 *  Returns                       :  CFA_SUCCESS if deletion succeeds,
 *                                  otherwise CFA_FAILURE.
 *
 *
 * *********************************************************************/
INT4
CfaIfmDeleteTapInterface (UINT4 u4IfIndex, UINT1 u1DelIfEntry)
{

    UINT1               u1IfType = 0;
    UINT1               u1BridgedIfStatus = 0;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmDeleteTapInterface "
              "for Enet interface %d\n", u4IfIndex);
/* currently there is no other interface possible over Psw */

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteTapInterface - "
              "Un-reg with HL success - interface %d\n", u4IfIndex);
    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfStatus);
    if (u1BridgedIfStatus == CFA_ENABLED)
    {
        /* If the Port is OOB,we Should not update that to L2IWF */
        if (CfaIsMgmtPort (u4IfIndex) == FALSE)
        {
            L2IwfPortUnmapIndication (u4IfIndex);
            EoamApiNotifyIfDelete (u4IfIndex);
#ifdef RMON_WANTED
            RmonDeletePort (u4IfIndex);
#endif /* RMON_WANTED */
#ifdef ISS_WANTED
            IssDeletePort ((UINT2) u4IfIndex, ISS_ALL_TABLES);
#endif
        }
        /* Unmap Port from its context */
        VcmUnMapPortFromContext (u4IfIndex);
    }

    if (CfaGddTapIfDelete (u4IfIndex) == OSIX_FAILURE)
    {
        CFA_DBG (CFA_TRC_ALL_TRACK, CFA_IFM,
                 "Error in CfaIfmDeleteTapInterface - "
                 "CfaGddTapIfDelete failed\n");
        return (CFA_FAILURE);
    }
    /* check whether the entry is to be deleted from IfTable itself */
    if (u1DelIfEntry == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "In CfaIfmDeleteTapInterface - "
                  "disabling success - interface %d\n", u4IfIndex);

        return (CFA_SUCCESS);
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

/* delete the interface from the IfTable */
    if (CfaIfmDeleteIfEntry (u4IfIndex, u1IfType) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteTapInterface - "
                  "ifEntry del fail for interface %d\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteTapInterface - "
              "Full Delete success - interface %d\n", u4IfIndex);

    return (CFA_SUCCESS);
}
#endif
/*****************************************************************************
 *
 *    Function Name        : CfaIfmDeletePortChannelInterface
 *
 *    Description        : This function is for for deleting the PortChannel
 *                         Interface.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if deletion succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmDeletePortChannelInterface (UINT4 u4IfIndex, UINT1 u1DelIfEntry,
                                  UINT1 u1IndicateHl)
{
    UINT1               u1IfType = CFA_NONE;
    UINT1               u1IfSubType;
    UINT1               u1Code;
    UINT1               u1BridgeStatus = CFA_ENABLED;
    UINT4               u4IfUfdGroupId = 0;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmDeletePortChannelInterface "
              "for PortChannel interface %d\n", u4IfIndex);

/* currently there is no other interface possible over Enet */

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeletePortChannelInterface - "
              "Un-reg with HL success - interface %d\n", u4IfIndex);

    if (CfaValidateCfaIfIndex (u4IfIndex) != CFA_SUCCESS)
    {

        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteIfEntry - invalid interface %d.\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }
    if (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX)
    {
        /* this interface is registered with IP */
        u1Code = (u1DelIfEntry == CFA_FALSE) ? CFA_NET_IF_NIS : CFA_NET_IF_DEL;

        if (CfaIfmConfigNetworkInterface
            (u1Code, u4IfIndex, 0, NULL) != CFA_SUCCESS)
        {
            /* unsuccessful in de-registering from IP - we cant delete the interface.
             * we dont expect this to fail - just for handling unforeseen exceptions. */
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                      "Error in CfaIfmDeleteEnetInterface - "
                      "IP De-reg fail for interface %d\n", u4IfIndex);
            return (CFA_FAILURE);
        }
    }

    /* update the ifTable globals */
    OsixGetSysTime (&CFA_IFSTK_CHNG ());

    CFA_IFTAB_CHNG () = CFA_IFSTK_CHNG ();

    CfaGetIfType (u4IfIndex, &u1IfType);
    CfaGetIfSubType (u4IfIndex, &u1IfSubType);
    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgeStatus);
    if (u1IfType == CFA_ENET || u1IfType == CFA_LAGG)
    {
        if (CfaGetIfUfdGroupId (u4IfIndex, &u4IfUfdGroupId) != CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                     "CfaIfmDeleteEnetInterface - Failure in getting UFD group Id\n");
        }
        /* Before removing the port/port-channel
         * it has to be removed from the UFD group
         * if the port/port channel is in the group*/

        if (u4IfUfdGroupId != 0)
        {
            if (CfaUfdCheckLastDownlinkInGroup (u4IfIndex) == OSIX_FALSE)
            {
                if (CfaUfdRemovePortFromGroup (u4IfIndex,
                                               u4IfUfdGroupId) != CFA_SUCCESS)
                {
                    CLI_SET_ERR (CLI_CFA_UFD_PORT_DEL_ERR);
                    CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                             "CfaIfmDeletePortChannelInterface - Failure in deleting the port from the UFD group\n");
                }
            }
        }
    }

    /* remove the entry  when it is a lag */
    if ((u1IfType == CFA_LAGG) && (u1BridgeStatus == CFA_DISABLED))
    {
#ifdef LA_WANTED
        L2IwfSetPortChannelValid (u4IfIndex, OSIX_FALSE);
        L2IwfSetPortChannelConfigured (u4IfIndex, OSIX_FALSE);
#endif
    }

    if (u1IndicateHl == CFA_TRUE)
    {
        L2IwfPortUnmapIndication (u4IfIndex);
    }
    if ((u1IfType == CFA_LAGG) && (u1BridgeStatus != CFA_ENABLED))
    {
        L2IwfDeleteAllPortsFromTrunk ((UINT2) u4IfIndex);
        L2IwfDeletePort (u4IfIndex, u1IfType, L2IWF_FALSE);
    }
#ifdef LA_WANTED
    LaDeletePort ((UINT2) u4IfIndex);
    LaUnmapPort ((UINT2) u4IfIndex);
#endif /* LA_WANTED */

    /* when port channel is deleted, then the port channel
     * attributes also needs to be deleted */
#ifdef ISS_WANTED
    IssDeletePort ((UINT2) u4IfIndex, ISS_RATECTRL_TABLE);
#endif

#ifdef RMON_WANTED
    RmonDeletePort (u4IfIndex);
#endif /* RMON_WANTED */
    /* Unmap Port from its context */
    VcmUnMapPortFromContext (u4IfIndex);

    /* check whether the entry is to be deleted from IfTable itself */
    if (u1DelIfEntry == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "In CfaIfmDeletePortChannelInterface - "
                  "disabling success - interface %d\n", u4IfIndex);

        return (CFA_SUCCESS);
    }

/* delete the interface from the IfTable */
    if (CfaIfmDeleteIfEntry (u4IfIndex, u1IfType) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeletePortChannelInterface - "
                  "ifEntry delete fail for interface %d\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeletePortChannelInterface - "
              "Full Delete success - interface %d\n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmDeleteInternalInterface
 *
 *    Description        : This function is for for deleting the internal
 *                         Interface.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if deletion succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmDeleteInternalInterface (UINT4 u4IfIndex,
                               UINT1 u1DelIfEntry, UINT1 u1IndicateHl)
{
    tStackInfoStruct   *pHighStackListScan = NULL;
    tTMO_SLL           *pHighStack = NULL;
    UINT1               u1IfType = CFA_NONE;
    tStackInfoStruct   *pLowStackListScan = NULL;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmDeleteInternalInterface "
              "for Internal interface %d\n", u4IfIndex);

    if (CfaValidateCfaIfIndex (u4IfIndex) != CFA_SUCCESS)
    {

        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteIfEntry - invalid interface %d.\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        /* update the ifTable globals */
        OsixGetSysTime (&CFA_IFSTK_CHNG ());

        --CFA_IFNUMBER ();
        CFA_IFTAB_CHNG () = CFA_IFSTK_CHNG ();
    }

    if (u1IndicateHl == CFA_TRUE)
    {
        L2IwfPortDeleteIndication (u4IfIndex);
    }

    /* Unmap Port from its context */
    VcmUnMapPortFromContext (u4IfIndex);

    /* check whether the entry is to be deleted from IfTable itself */
    if (u1DelIfEntry == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "In CfaIfmDeleteInternalInterface - "
                  "disabling success - interface %d\n", u4IfIndex);

        return (CFA_SUCCESS);
    }

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        pHighStackListScan =
            (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
        pHighStack = &CFA_IF_STACK_LOW (u4IfIndex);
        /* If any ILAN entry for port exists in stack,
         * delete that entry. */
        TMO_SLL_Scan (pHighStack, pHighStackListScan, tStackInfoStruct *)
        {
            if (pHighStackListScan->u4IfIndex != CFA_NONE)
            {
                if ((nmhSetIfStackStatus ((INT4) u4IfIndex,
                                          (INT4) pHighStackListScan->u4IfIndex,
                                          CFA_RS_DESTROY)) == SNMP_FAILURE)
                {
                    CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                             "Error in CfaIfmDeleteInternalInterface. \n");
                    return (CFA_FAILURE);
                }
                break;
            }
        }

        while (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex))
        {
            pHighStackListScan = (tStackInfoStruct *)
                CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);

            CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
            if (CFA_IF_STACK_IFINDEX (pHighStackListScan) != CFA_NONE)
            {
                /* First Delete the Lower Layer Stack entry for this interface
                 * from the Higher Layer Stack entry */
                CfaIfmDeleteStackEntry (u4IfIndex, CFA_HIGH,
                                        CFA_IF_STACK_IFINDEX
                                        (pHighStackListScan));
            }
            else
            {

                CfaIfmUpdateStackForDeletion (&CFA_IF_STACK_HIGH (u4IfIndex),
                                              CFA_FALSE, CFA_NONE);
            }
        }

        while (CFA_IF_STACK_LOW_ENTRY (u4IfIndex))
        {
            pLowStackListScan =
                (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);

            CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);
            if (CFA_IF_STACK_IFINDEX (pLowStackListScan) != CFA_NONE)
            {
                /* First Delete this entry from the lower layer's stack value. */
                CfaIfmDeleteStackEntry (CFA_IF_STACK_IFINDEX
                                        (pLowStackListScan), CFA_HIGH,
                                        u4IfIndex);

            }
            else
            {
                CfaIfmUpdateStackForDeletion (&CFA_IF_STACK_LOW (u4IfIndex),
                                              CFA_FALSE, CFA_NONE);
            }
        }
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

    /* delete the interface from the IfTable */
    if (CfaIfmDeleteIfEntry (u4IfIndex, u1IfType) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteInternalInterface - "
                  "ifEntry delete fail for interface %d\n", u4IfIndex);
        return (CFA_FAILURE);
    }

#ifdef NPAPI_WANTED
    if (FsCfaHwDeleteInternalPort (u4IfIndex) == FNP_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteInternalInterface - "
                  "Hardware delete fail for interface %d\n", u4IfIndex);
        return (CFA_FAILURE);
    }
#endif
    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteInternalInterface - "
              "Full Delete success - interface %d\n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *    Function Name        : CfaIfmDeleteILanInterface
 *    Description          : This function is for deleting the ILAN interface
 *    Input(s)             : UINT4 u4IfIndex,
 *    Output(s)            : None.
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *    Returns              : CFA_SUCCESS if deletion succeeds,
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaIfmDeleteILanInterface (UINT4 u4IfIndex, UINT1 u1DelIfEntry)
{
    UINT1               u1IfType = CFA_NONE;
    tStackInfoStruct   *pHighStackListScan = NULL;

    CFA_IF_ADMIN (u4IfIndex) = CFA_IF_DOWN;
    CfaSetCdbPortAdminStatus (u4IfIndex, CFA_IF_DOWN);

    while (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex))
    {
        pHighStackListScan =
            (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
        CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);

        if (CFA_IF_STACK_IFINDEX (pHighStackListScan) != CFA_NONE)
        {
            /* First Delete the Lower Layer Stack entry for this interface
             * from the Higher Layer Stack entry */
            CfaIfmDeleteStackEntry (u4IfIndex, CFA_HIGH,
                                    CFA_IF_STACK_IFINDEX (pHighStackListScan));
        }
        else
        {

            CfaIfmUpdateStackForDeletion (&CFA_IF_STACK_HIGH (u4IfIndex),
                                          CFA_FALSE, CFA_NONE);
        }
    }

    /* Also Delete the lower layer stack entry. */
    CfaIfmDeleteStackEntry (u4IfIndex, CFA_HIGH, CFA_NONE);

    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
              "In CfaIfmDeleteILanInterface - "
              "Un-reg with HL success - interface %d\n", u4IfIndex);

    /* check whether the entry is to be deleted from IfTable itself */
    if (u1DelIfEntry == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "In CfaIfmDeleteILanInterface - "
                  "disabling success - interface %d\n", u4IfIndex);

        return (CFA_SUCCESS);
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

    /* delete the interface from the IfTable */
    if (CfaIfmDeleteIfEntry (u4IfIndex, u1IfType) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteILanInterface - "
                  "ifEntry delete fail for interface %d\n", u4IfIndex);
        return (CFA_FAILURE);
    }

#ifdef NPAPI_WANTED
    /* Send ACTIVE indication to NPAPI */
    FsCfaHwDeleteILan (u4IfIndex);
#endif

    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
              "In CfaIfmDeleteILanInterface - "
              "Full Delete success - interface %d\n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmDeleteVlanInterface
 *
 *    Description        : This function is for for de-registration of
 *                         interface with IP. 
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if deletion succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmDeleteVlanInterface (UINT4 u4IfIndex, UINT1 u1DelIfEntry)
{
    UINT1               u1IfType = CFA_NONE;
    UINT2               u2IfIvrVlanId = 0;
    tStackInfoStruct   *pHighStackListScan = NULL;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmDeleteVlanInterface "
              "for VLAN interface %d\n", u4IfIndex);

/* first we de-register the interface from IP or higher layer if it was
registered earlier */
    if (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX)
    {
        if (u1DelIfEntry == CFA_FALSE)
        {
            /*While making the interface as NotInService the function CfaIfmConfigNetworkInterface
             * is called with CFA_NET_IF_NIS instead of CFA_NET_IF_DEL*/
            if (CfaIfmConfigNetworkInterface
                (CFA_NET_IF_NIS, u4IfIndex, 0, NULL) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                          "Error in CfaIfmDeleteVlanInterface - "
                          "IP De-reg fail for interface %d\n", u4IfIndex);
                return (CFA_FAILURE);
            }
            CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                      "In CfaIfmDeleteVlanInterface - "
                      "disabling success - interface %d\n", u4IfIndex);

            return (CFA_SUCCESS);
        }
/* this interface is registered with IP */
        if (CfaIfmConfigNetworkInterface
            (CFA_NET_IF_DEL, u4IfIndex, 0, NULL) != CFA_SUCCESS)
        {
/* unsuccessful in de-registering from IP - we cant delete the interface.
we dont expect this to fail - just for handling unforeseen exceptions. */
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                      "Error in CfaIfmDeleteVlanInterface - "
                      "IP De-reg fail for interface %d\n", u4IfIndex);
            return (CFA_FAILURE);
        }
    }                            /* end of interface was registered with IP */

    CfaGetIfIvrVlanId (u4IfIndex, &u2IfIvrVlanId);

#if (defined HB_WANTED && ICCH_WANTED)
    /* Delete statically created FDB entry for this VLAN on ICCL */
    HbUpdateIcclFdbEntryForVlan (u2IfIvrVlanId, HB_FDB_DEL);
#endif

    /* check whether the entry is to be deleted from IfTable itself */
    if (u1DelIfEntry == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "In CfaIfmDeleteVlanInterface - "
                  "disabling success - interface %d\n", u4IfIndex);

        return (CFA_SUCCESS);
    }
    while (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex))
    {
        pHighStackListScan = (tStackInfoStruct *)
            CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
        CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);

        if (CFA_IF_STACK_IFINDEX (pHighStackListScan) != CFA_NONE)
        {
            /* First Delete the Lower Layer Stack entry for this interface
             * from the Higher Layer Stack entry */
            CfaIfmDeleteStackEntry (CFA_IF_STACK_IFINDEX
                                    (pHighStackListScan), CFA_LOW, u4IfIndex);
            CfaIfmDeleteStackEntry (u4IfIndex, CFA_HIGH,
                                    CFA_IF_STACK_IFINDEX (pHighStackListScan));
        }
        else
        {

            CfaIfmUpdateStackForDeletion (&CFA_IF_STACK_HIGH (u4IfIndex),
                                          CFA_FALSE, CFA_NONE);
        }
    }

    /* Delete the lower layer stack entry. */
    CfaIfmUpdateStackForDeletion (&CFA_IF_STACK_LOW (u4IfIndex),
                                  CFA_FALSE, CFA_NONE);
/* currently there is no other interface possible over VLAN */

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteVlanInterface - "
              "Un-reg with HL success - interface %d\n", u4IfIndex);

    CfaGetIfType (u4IfIndex, &u1IfType);

    /* This vlan is no longer an Interface Vlan */
    CfaCdbRemoveFromIntfVlanList (u2IfIvrVlanId);

    /* delete the interface from the IfTable */
    if (CfaIfmDeleteIfEntry (u4IfIndex, u1IfType) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteVlanInterface - "
                  "ifEntry delete fail for interface %d\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteVlanInterface - "
              "Full Delete success - interface %d\n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmDeleteL3SubInterface
 *
 *    Description          : This function is for de-registration of
 *                           interface with IP. 
 *
 *    Input(s)             : UINT4 u4IfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if deletion succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmDeleteL3SubInterface (UINT4 u4IfIndex, UINT1 u1DelIfEntry)
{
    UINT1               u1IfType = CFA_NONE;
    UINT2               u2VlanId = 0;
#ifdef NPAPI_WANTED
    UINT4               u4ParentIfIndex = 0;
    INT4                i4RetVal = FNP_FAILURE;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
#endif
    tStackInfoStruct   *pHighStackListScan = NULL;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmDeleteL3SubInterface"
              " for interface index %d\n", u4IfIndex);

/* first we de-register the interface from IP or higher layer if it was
registered earlier */
    if (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX)
    {
        if (u1DelIfEntry == CFA_FALSE)
        {
            /*While making the interface as NotInService the function CfaIfmConfigNetworkInterface
             * is called with CFA_NET_IF_NIS instead of CFA_NET_IF_DEL*/
            if (CfaIfmConfigNetworkInterface
                (CFA_NET_IF_NIS, u4IfIndex, 0, NULL) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                          "Error in CfaIfmDeleteL3SubInterface- "
                          "IP De-reg fail for interface %d\n", u4IfIndex);
                return (CFA_FAILURE);
            }
            CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                      "In CfaIfmDeleteL3SubInterface - "
                      "disabling success - interface %d\n", u4IfIndex);

            return (CFA_SUCCESS);
        }
/* this interface is registered with IP */
        if (CfaIfmConfigNetworkInterface
            (CFA_NET_IF_DEL, u4IfIndex, 0, NULL) != CFA_SUCCESS)
        {
/* unsuccessful in de-registering from IP - we cant delete the interface.
we dont expect this to fail - just for handling unforeseen exceptions. */
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                      "Error in CfaIfmDeleteL3SubInterface- "
                      "IP De-reg fail for interface %d\n", u4IfIndex);
            return (CFA_FAILURE);
        }
    }                            /* end of interface was registered with IP */

    /* check whether the entry is to be deleted from IfTable itself */
    if (u1DelIfEntry == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "In CfaIfmDeleteL3SubInterface- "
                  "disabling success - interface %d\n", u4IfIndex);

        return (CFA_SUCCESS);
    }
    while (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex))
    {
        pHighStackListScan = (tStackInfoStruct *)
            CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
        CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);

        if (CFA_IF_STACK_IFINDEX (pHighStackListScan) != CFA_NONE)
        {
            /* First Delete the Lower Layer Stack entry for this interface
             * from the Higher Layer Stack entry */
            CfaIfmDeleteStackEntry (CFA_IF_STACK_IFINDEX
                                    (pHighStackListScan), CFA_LOW, u4IfIndex);
            CfaIfmDeleteStackEntry (u4IfIndex, CFA_HIGH,
                                    CFA_IF_STACK_IFINDEX (pHighStackListScan));
        }
        else
        {

            CfaIfmUpdateStackForDeletion (&CFA_IF_STACK_HIGH (u4IfIndex),
                                          CFA_FALSE, CFA_NONE);
        }
    }

    /* Delete the lower layer stack entry. */
    CfaIfmUpdateStackForDeletion (&CFA_IF_STACK_LOW (u4IfIndex),
                                  CFA_FALSE, CFA_NONE);
/* currently there is no other interface possible over VLAN */

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteL3SubInterface- "
              "Un-reg with HL success - interface %d\n", u4IfIndex);

    CfaGetIfType (u4IfIndex, &u1IfType);

    /* Get the VLAN Id configured for the Subinterface */
    CfaGetIfIvrVlanId (u4IfIndex, &u2VlanId);

    /* Remove the VLAND Id from the IntfVlan List */
    CfaCdbRemoveFromIntfVlanList (u2VlanId);

    CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex) = 0;

    /* Set the encap status to be CFA_FALSE */
    CFA_CDB_SET_ENCAP_STATUS (u4IfIndex, CFA_FALSE);

#ifdef NPAPI_WANTED
#ifdef RM_WANTED
    if (RmGetNodeState () == RM_ACTIVE)
    {
#endif
        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        CfaGetIfName (u4IfIndex, au1IfName);

        CfaGetL3SubIfParentIndex (u4IfIndex, &u4ParentIfIndex);

        i4RetVal = CfaFsNpIpv4UpdateIpInterfaceStatus (0, au1IfName,
                                                       u4IfIndex, 0,
                                                       0, u2VlanId,
                                                       0, CFA_IF_DOWN);
        if (FNP_FAILURE == i4RetVal)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmDeleteL3SubInterface- "
                      "Delete fail for interface %d\n", u4IfIndex);

            return CFA_FAILURE;
        }

        i4RetVal = (INT4) (CfaFsNpIpv4DeleteL3SubInterface (u4IfIndex));
        if (FNP_FAILURE == i4RetVal)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmDeleteL3SubInterface- "
                      "Delete fail for interface %d\n", u4IfIndex);

            return CFA_FAILURE;
        }
#ifdef RM_WANTED
    }
#endif
#endif
    /* delete the interface from the IfTable */
    if (CfaIfmDeleteIfEntry (u4IfIndex, u1IfType) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteL3SubInterface- "
                  "ifEntry delete fail for interface %d\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteL3SubInterface- "
              "Full Delete success - interface %d\n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmDeleteLoopbackInterface
 *
 *    Description        : This function is for deleting loopback of
 *                         interface with IP. 
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if deletion succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmDeleteLoopbackInterface (UINT4 u4IfIndex, UINT1 u1DelIfEntry)
{
    UINT1               u1IfType = CFA_NONE;
    tStackInfoStruct   *pHighStackListScan = NULL;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmDeleteVlanInterface "
              "for VLAN interface %d\n", u4IfIndex);

/* first we de-register the interface from IP or higher layer if it was
registered earlier */
    if (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX)
    {
/* this interface is registered with IP */
        if (CfaIfmConfigNetworkInterface
            (CFA_NET_IF_DEL, u4IfIndex, 0, NULL) != CFA_SUCCESS)
        {
/* unsuccessful in de-registering from IP - we cant delete the interface.
we dont expect this to fail - just for handling unforeseen exceptions. */
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                      "Error in CfaIfmDeleteVlanInterface - "
                      "IP De-reg fail for interface %d\n", u4IfIndex);
            return (CFA_FAILURE);
        }
    }                            /* end of interface was registered with IP */

    /* check whether the entry is to be deleted from IfTable itself */
    if (u1DelIfEntry == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "In CfaIfmDeleteVlanInterface - "
                  "disabling success - interface %d\n", u4IfIndex);

        return (CFA_SUCCESS);
    }
    while (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex))
    {
        pHighStackListScan = (tStackInfoStruct *)
            CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
        CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
        if (CFA_IF_STACK_IFINDEX (pHighStackListScan) != CFA_NONE)
        {
            /* First Delete the Lower Layer Stack entry for this interface
             * from the Higher Layer Stack entry */
            CfaIfmDeleteStackEntry (CFA_IF_STACK_IFINDEX
                                    (pHighStackListScan), CFA_LOW, u4IfIndex);
            CfaIfmDeleteStackEntry (u4IfIndex, CFA_HIGH,
                                    CFA_IF_STACK_IFINDEX (pHighStackListScan));
        }
        else
        {
            CfaIfmUpdateStackForDeletion (&CFA_IF_STACK_HIGH (u4IfIndex),
                                          CFA_FALSE, CFA_NONE);
        }
    }

    /* Also Delete the lower layer stack entry. */
    CfaIfmDeleteStackEntry (u4IfIndex, CFA_LOW, CFA_NONE);

/* currently there is no other interface possible over VLAN */

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteVlanInterface - "
              "Un-reg with HL success - interface %d\n", u4IfIndex);

    CfaGetIfType (u4IfIndex, &u1IfType);

/* delete the interface from the IfTable */
    if (CfaIfmDeleteIfEntry (u4IfIndex, u1IfType) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteVlanInterface - "
                  "ifEntry delete fail for interface %d\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteVlanInterface - "
              "Full Delete success - interface %d\n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmConfigNetworkInterface
 *
 *    Description        : This function is for registration of
 *                interface with IP and updation of
 *                registration information in the ifTable.
 *
 *    Input(s)            : UINT1 u1OperCode,
 *                UINT4 u4IfIndex,
 *                u4Flag  - Parameters to be configured for the interface
 *                pIpConfigInfo -IP information for the interface
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if configuration succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmConfigNetworkInterface (UINT1 u1OperCode, UINT4 u4IfIndex,
                              UINT4 u4Flag, tIpConfigInfo * pIpConfigInfo)
{
    tStackInfoStruct   *pHighStackListScan = NULL;
#ifdef NPAPI_WANTED
#ifdef RM_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
#endif
#ifdef LNXIP4_WANTED
    UINT2               KnetIfVlanId = 0;
#endif
    tFsNpL3IfInfo       FsNpL3IfInfo;
    UINT4               u4PhyIndex = 0;
    INT4                i4PortVlanId = 0;
#endif
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    UINT4               u4PeerIpAddr = 0;
    UINT1               u1Persistence = CFA_PERS_OTHER;
    UINT1               u1OperStatus = CFA_IF_DOWN;
    UINT4               u4IfMtu = 0;
    UINT4               u4IfSpeed = 0;
    UINT1               u1IfType = CFA_NONE;
    UINT1               au1MacAddress[MAC_ADDR_LEN];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    t_IFACE_PARAMS      IfParams;
    tIpConfigInfo       IpConfigInfo;
#endif
#if defined (LNXIP4_WANTED)
    UINT4               u4Port = 0;
    INT4                i4ReturnVal = 0;
    tCfaIfInfo          CfaIfInfo;
#endif

    INT4                i4RetVal = CFA_FAILURE;
    UINT2               u2VlanId;
#if defined (IP6_WANTED) || defined (SMOD_WANTED)
    tCfaRegInfo         CfaInfo;
#endif
#if (defined MBSM_WANTED) && (defined RM_WANTED)
    UINT4               u4SlotId;
#endif
    UINT4               u4ContextId = 0;
#if defined (WLC_WANTED) || defined (WTP_WANTED)
#ifdef LNXIP4_WANTED
    INT4                i4PhyPort = 0;
    INT4                i4LPortNum = 0;
    INT4                i4SlotNum = 0;
    UINT1              *pu1IfPortName = NULL;
#endif
#endif
#ifdef NPAPI_WANTED
#ifdef RM_WANTED
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
#endif
    MEMSET (&FsNpL3IfInfo, 0, sizeof (tFsNpL3IfInfo));
#endif
#ifdef LNXIP4_WANTED
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
#endif

    CFA_DBG2 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmConfigNetworkInterface "
              "for interface %d for Oper %d.\n", u4IfIndex, u1OperCode);

#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    CfaGetIfName (u4IfIndex, au1IfName);
    MEMCPY (IfParams.au1IfName, au1IfName, CFA_MAX_PORT_NAME_LENGTH);
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pIpConfigInfo);
    UNUSED_PARAM (u4Flag);
#endif
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    CfaGetIfType (u4IfIndex, &u1IfType);
    if (u1IfType == CFA_LOOPBACK)
    {
        u2VlanId = FNP_ZERO;
    }
    else
    {
        CfaGetIfIvrVlanId (u4IfIndex, &u2VlanId);
    }

#if (defined MBSM_WANTED) && (defined RM_WANTED)
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* Incase of StackIvr StackMac should be taken RMobj */
        if (u2VlanId == CFA_DEFAULT_STACK_VLAN_ID)
        {
            RmGetNodeSwitchId (&u4SlotId);
            CfaMbsmNpGetStackMac (u4SlotId, au1MacAddress);
        }
        else
        {
            CfaGetIfHwAddr (u4IfIndex, au1MacAddress);
        }
    }
    else
#endif
    {
        CfaGetIfHwAddr (u4IfIndex, au1MacAddress);
    }
    if (u4Flag == 0)
    {
        pIpConfigInfo = &IpConfigInfo;
        if (CfaIpIfGetIfInfo (u4IfIndex, pIpConfigInfo) != CFA_SUCCESS)
        {
            return CFA_FAILURE;
        }
    }
#endif /* IP_WANTED || LNXIP4_WANTED */

    switch (u1OperCode)
    {

        case CFA_NET_IF_NEW:
            /* register the new interface with IP  - the interface MUST be 
             * created before registering it with IP.The Status of the 
             * interface is always down at the time of creation.It is
             * explicitly made up by CFA. at a later point of time. */
            /* Verify if the interface is already present else create 
             * the interface in VCM */
            if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId)
                == VCM_FAILURE)
            {
                if (VcmCfaCreateIPIfaceMapping (VCM_DEFAULT_CONTEXT, u4IfIndex)
                    == VCM_FAILURE)
                {
                    /* Creating the interface is unsuccessful */
                    return CFA_FAILURE;
                }
                u4ContextId = VCM_DEFAULT_CONTEXT;
            }
            if (VcmCfaMakeIPIfaceMappingActive
                (u4ContextId, u4IfIndex) == VCM_FAILURE)
            {
                /* Unsuccessful in making  the interface
                 * mapping in VCM active */
                return CFA_FAILURE;
            }

#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
#ifdef NPAPI_WANTED
            if ((CfaIsMgmtPort (u4IfIndex) == FALSE) &&
                (CfaIsLinuxVlanIntf (u4IfIndex) == FALSE))
            {
                if ((u1IfType == CFA_L3IPVLAN) || (u1IfType == CFA_LOOPBACK))
                {
                    if (CFA_NODE_STATUS () == CFA_NODE_ACTIVE)
                    {
                        i4RetVal =
                            CfaFsNpIpv4CreateIpInterface (u4ContextId,
                                                          au1IfName, u4IfIndex,
                                                          pIpConfigInfo->u4Addr,
                                                          pIpConfigInfo->
                                                          u4NetMask, u2VlanId,
                                                          au1MacAddress);
                    }
                }
                else if ((u1IfType == CFA_ENET) || (u1IfType == CFA_PPP) ||
                         (u1IfType == CFA_OTHER)
                         || (u1IfType == CFA_PSEUDO_WIRE)
                         || (u1IfType == CFA_LAGG))
                {
                    FsNpL3IfInfo.u4VrId = u4ContextId;
                    FsNpL3IfInfo.u4IfIndex = u4IfIndex;
                    FsNpL3IfInfo.u4IpAddr = pIpConfigInfo->u4Addr;
                    FsNpL3IfInfo.u4IpSubnet = pIpConfigInfo->u4NetMask;
                    FsNpL3IfInfo.u2PortVlanId = pIpConfigInfo->u2PortVlanId;
                    MEMCPY (FsNpL3IfInfo.au1IfName, au1IfName,
                            CFA_MAX_PORT_NAME_LENGTH);
                    MEMCPY (FsNpL3IfInfo.au1MacAddr, au1MacAddress,
                            MAC_ADDR_LEN);
                    if (u1IfType == CFA_PPP)
                    {
                        CfaGetLLIfIndex (u4IfIndex, &u4PhyIndex);
                        CfaGetIfName (u4PhyIndex, au1IfName);
                        FsNpL3IfInfo.u4IfIndex = u4PhyIndex;
                        MEMCPY (FsNpL3IfInfo.au1IfName, au1IfName,
                                CFA_MAX_PORT_NAME_LENGTH);
                    }
                    i4RetVal = CfaFsNpIpv4L3IpInterface (L3_HW_CREATE_IF_INFO,
                                                         &FsNpL3IfInfo);
                }
                if (i4RetVal == FNP_FAILURE)
                {
                    return CFA_FAILURE;
                }
                else if ((u1IfType == CFA_ENET) && (i4RetVal == FNP_SUCCESS))
                {
#ifdef LNXIP4_WANTED
                    if ((u1IfType == CFA_ENET) &&
                        (ISS_HW_SUPPORTED ==
                         IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT)))
                    {
                        /* Knet interface name is assigned dynamically based on 
                         * the created dummy Vlan Id and the name is set 
                         * to the correspondig interface index */
                        KnetIfVlanId = FsNpL3IfInfo.u2PortVlanId;
                        SNPRINTF ((CHR1 *) FsNpL3IfInfo.au1IfName,
                                  CFA_MAX_PORT_NAME_LENGTH, "vlan%d",
                                  KnetIfVlanId);
                        CfaGddSetLnxIntfnameForPort (u4IfIndex,
                                                     FsNpL3IfInfo.au1IfName);

                        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                        CfaGetIfName (u4IfIndex, au1IfName);
                        MEMCPY (IfParams.au1IfName, au1IfName,
                                CFA_MAX_PORT_NAME_LENGTH);
                    }
#endif /* LNXIP4_WANTED */
                    pIpConfigInfo->u2PortVlanId = FsNpL3IfInfo.u2PortVlanId;
                    if (CFA_SUCCESS ==
                        CfaIpIfSetIfInfo (u4IfIndex, CFA_IP_IF_PORTVID,
                                          pIpConfigInfo))
                    {
#ifdef RM_WANTED
                        RM_GET_SEQ_NUM (&u4SeqNum);
                        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, IfIpPortVlanId,
                                              u4SeqNum, TRUE, CfaLock,
                                              CfaUnlock, 1, SNMP_SUCCESS);
                        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                          FsNpL3IfInfo.u4IfIndex,
                                          FsNpL3IfInfo.u2PortVlanId));
#endif
                    }
                }
            }
#endif /* NPAPI_WANTED */

            MEMSET (IfParams.MacAddress, 0, MAC_ADDR_LEN);
            MEMCPY (IfParams.MacAddress, au1MacAddress, MAC_ADDR_LEN);
            if (u1IfType != CFA_LOOPBACK)
            {
                CfaGetIfMtu (u4IfIndex, &u4IfMtu);
                CfaGetIfSpeed (u4IfIndex, &u4IfSpeed);
            }

            IfParams.u4IpAddr = pIpConfigInfo->u4Addr;
            IfParams.u4SubnetMask = pIpConfigInfo->u4NetMask;
            IfParams.u4BcastAddr = pIpConfigInfo->u4BcastAddr;
            IfParams.u4Mtu = u4IfMtu;
            IfParams.u4IfSpeed = u4IfSpeed;
            IfParams.u4PeerIpAddr = u4PeerIpAddr;
            IfParams.u2IfIndex = (UINT2) u4IfIndex;
            IfParams.u1IfType = u1IfType;
            IfParams.u1EncapType = CFA_IF_ENCAP (u4IfIndex);
            IfParams.u1Persistence = u1Persistence;
#ifdef IP_WANTED
            IfParams.u2Port = IpGetFreePort (u4IfIndex);
            if (IfParams.u2Port == IPIF_INVALID_INDEX)
            {
                return CFA_FAILURE;
            }
            CfaSetIfIpPort (u4IfIndex, (INT4) IfParams.u2Port);
            VcmUpdateIpPortNumber (u4IfIndex, IfParams.u2Port);
#endif
            if (u4Flag != 0)
            {
                if (CfaIpIfSetIfInfo (u4IfIndex,
                                      u4Flag, pIpConfigInfo) == CFA_FAILURE)
                {
                    return CFA_FAILURE;
                }
            }
            if (IpHandleInterfaceMsgFromCfa (&IfParams, IPIF_CREATE_INTERFACE)
                == IP_SUCCESS)
            {

                pHighStackListScan =
                    (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
                CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
/* update the interface stack - it is assumed that the stack is sorted */
                CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_ACTIVE;
                i4RetVal = CFA_SUCCESS;

                CFA_DBG2 (CFA_TRC_ALL_TRACK, CFA_IFM,
                          "In CfaIfmConfigNetworkInterface - "
                          "New interface %d IP address %d \n",
                          u4IfIndex, pIpConfigInfo->u4Addr);

            }
            /* Act on the secondary addresses configured on the interface */
            CfaIpIfHandleSecondaryAddress (u4IfIndex, CFA_NET_IF_NEW);
#ifdef LNXIP4_WANTED
            i4ReturnVal = NetIpv4GetPortFromIfIndex (u4IfIndex, &u4Port);
            CfaSetIfIpPort (u4IfIndex, (INT4) u4Port);
            VcmUpdateIpPortNumber (u4IfIndex, u4Port);
            UNUSED_PARAM (i4ReturnVal);
#endif

#endif /* IP_WANTED */
#ifdef IP6_WANTED
            /* Notify interface creation to all HLs */
            CfaInfo.u4IfIndex = u4IfIndex;
            CfaGetIfType (u4IfIndex, &(CfaInfo.CfaIntfInfo.u1IfType));
            CfaGetIfMtu (u4IfIndex, &(CfaInfo.CfaIntfInfo.u4IfMtu));
            CfaGetIfOperStatus (u4IfIndex,
                                &(CfaInfo.CfaIntfInfo.u1IfOperStatus));
            CfaGetIfSpeed (u4IfIndex, &(CfaInfo.CfaIntfInfo.u4IfSpeed));
            CfaGetIfHighSpeed (u4IfIndex, &(CfaInfo.CfaIntfInfo.u4IfHighSpeed));
            CfaGetIfName (u4IfIndex, CfaInfo.CfaIntfInfo.au1IfName);
            CfaGetIfIvrVlanId (u4IfIndex, &CfaInfo.CfaIntfInfo.u2VlanId);

            if ((CfaInfo.CfaIntfInfo.u1IfType == CFA_ENET)
                || (CfaInfo.CfaIntfInfo.u1IfType == CFA_LAGG) ||
                (CfaInfo.CfaIntfInfo.u1IfType == CFA_OTHER) ||
                (CfaInfo.CfaIntfInfo.u1IfType == CFA_L3SUB_INTF))
            {
                CfaGetIfHwAddr (u4IfIndex, CfaInfo.CfaIntfInfo.au1MacAddr);
            }
            else if ((CfaInfo.CfaIntfInfo.u1IfType == CFA_L3IPVLAN)
#ifdef WGS_WANTED
                     || (CfaInfo.CfaIntfInfo.u1IfType == CFA_L2VLAN)
#endif /* WGS_WANTED */
                )
            {
                CfaGetIfHwAddr (u4IfIndex, CfaInfo.CfaIntfInfo.au1MacAddr);
            }
            /* if we are in the process of configuring default ipv6 address then enter the if statement */
            if (gu1CfaDefIpv6Initialised == FALSE)
            {
                CfaInfo.CfaIntfInfo.IfIp6Addr = pIpConfigInfo->Ip6Addr;
                CfaInfo.CfaIntfInfo.u1PrefixLen = pIpConfigInfo->u1PrefixLen;
                gu1CfaDefIpv6Initialised = TRUE;    /*reset the flag */
            }
            else
            {
                CfaInfo.CfaIntfInfo.u1PrefixLen = 0;
            }
            CfaNotifyIfCreate (&CfaInfo);
#endif /* IP6_WANTED */
            break;

        case CFA_NET_IF_UPD:
/* notify IP of the update of configuration */
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
#ifdef NPAPI_WANTED
            if ((CfaIsMgmtPort (u4IfIndex) == FALSE) &&
                (CfaIsLinuxVlanIntf (u4IfIndex) == FALSE))
            {
                if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId)
                    == VCM_FAILURE)
                {
                    return CFA_FAILURE;
                }
                if ((u1IfType == CFA_L3IPVLAN) || (u1IfType == CFA_LOOPBACK))
                {

                    if (CFA_NODE_STATUS () == CFA_NODE_ACTIVE)
                    {
                        i4RetVal =
                            CfaFsNpIpv4ModifyIpInterface (u4ContextId,
                                                          au1IfName, u4IfIndex,
                                                          pIpConfigInfo->u4Addr,
                                                          pIpConfigInfo->
                                                          u4NetMask, u2VlanId,
                                                          au1MacAddress);
                    }
                }
                else if ((u1IfType == CFA_ENET) || (u1IfType == CFA_PPP) ||
                         (u1IfType == CFA_OTHER) || (u1IfType == CFA_LAGG)
                         || (u1IfType == CFA_PSEUDO_WIRE))
                {
                    FsNpL3IfInfo.u4VrId = u4ContextId;
                    FsNpL3IfInfo.u4IfIndex = u4IfIndex;
                    FsNpL3IfInfo.u4IpAddr = pIpConfigInfo->u4Addr;
                    FsNpL3IfInfo.u4IpSubnet = pIpConfigInfo->u4NetMask;
                    FsNpL3IfInfo.u2PortVlanId = pIpConfigInfo->u2PortVlanId;
                    MEMCPY (FsNpL3IfInfo.au1IfName, au1IfName,
                            CFA_MAX_PORT_NAME_LENGTH);
                    MEMCPY (FsNpL3IfInfo.au1MacAddr, au1MacAddress,
                            MAC_ADDR_LEN);
                    if (u1IfType == CFA_PPP)
                    {
                        CfaGetLLIfIndex (u4IfIndex, &u4PhyIndex);
                        CfaGetIfName (u4PhyIndex, au1IfName);
                        FsNpL3IfInfo.u4IfIndex = u4PhyIndex;
                        MEMCPY (FsNpL3IfInfo.au1IfName, au1IfName,
                                CFA_MAX_PORT_NAME_LENGTH);
                    }
                    if (CfaGetIfIpPortVlanId (u4IfIndex, &i4PortVlanId) ==
                        CFA_SUCCESS)
                    {
                        FsNpL3IfInfo.u2PortVlanId = (UINT2) i4PortVlanId;
                    }
                    i4RetVal = CfaFsNpIpv4L3IpInterface (L3_HW_MODIFY_IF_INFO,
                                                         &FsNpL3IfInfo);

                    if ((u1IfType == CFA_ENET) && (i4RetVal == FNP_SUCCESS))
                    {
#ifdef LNXIP4_WANTED
                        if ((u1IfType == CFA_ENET) &&
                            (ISS_HW_SUPPORTED ==
                             IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT)))
                        {
                            /* Knet interface name is assigned dynamically based on 
                             * the created dummy Vlan Id and the name is set 
                             * to the correspondig interface index */
                            KnetIfVlanId = FsNpL3IfInfo.u2PortVlanId;
                            SNPRINTF ((CHR1 *) FsNpL3IfInfo.au1IfName,
                                      CFA_MAX_PORT_NAME_LENGTH, "vlan%d",
                                      KnetIfVlanId);
                            CfaGddSetLnxIntfnameForPort (u4IfIndex,
                                                         FsNpL3IfInfo.
                                                         au1IfName);

                            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                            CfaGetIfName (u4IfIndex, au1IfName);
                            MEMCPY (IfParams.au1IfName, au1IfName,
                                    CFA_MAX_PORT_NAME_LENGTH);
                        }
#endif /* LNXIP4_WANTED */
                        pIpConfigInfo->u2PortVlanId = FsNpL3IfInfo.u2PortVlanId;
                        if (CFA_SUCCESS ==
                            CfaIpIfSetIfInfo (u4IfIndex, CFA_IP_IF_PORTVID,
                                              pIpConfigInfo))
                        {
#ifdef RM_WANTED
                            RM_GET_SEQ_NUM (&u4SeqNum);
                            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                                  IfIpPortVlanId, u4SeqNum,
                                                  TRUE, CfaLock, CfaUnlock, 1,
                                                  SNMP_SUCCESS);
                            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                              FsNpL3IfInfo.u4IfIndex,
                                              FsNpL3IfInfo.u2PortVlanId));
#endif
                        }
                    }

                }
                if (i4RetVal == FNP_FAILURE)
                {
                    return CFA_FAILURE;
                }
            }
#endif /* NPAPI_WANTED */
#if defined (WLC_WANTED) || defined (WTP_WANTED)
#ifdef LNXIP4_WANTED
            if (u1IfType == CFA_L3SUB_INTF)
            {
                /*Below change is to be parsed to get the interface for the corressponding
                   num slot0 */
                pu1IfPortName = au1IfName + CFA_SLOT_NUM_OFFSET;
                if ((CfaCliGetPhysicalAndLogicalPortNum ((INT1 *) pu1IfPortName,
                                                         &i4PhyPort,
                                                         &i4LPortNum,
                                                         &i4SlotNum)) ==
                    CFA_SUCCESS)
                {
                    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                    SNPRINTF ((CHR1 *) au1IfName, CFA_MAX_PORT_NAME_LENGTH,
                              "%s.%d", CfaGddGetLnxIntfnameForPort (i4PhyPort),
                              i4LPortNum);
                    MEMCPY (IfParams.au1IfName, au1IfName,
                            CFA_MAX_PORT_NAME_LENGTH);
                }
            }
#endif /* LNXIP4_WANTED */
#endif

            CfaGetIfMtu (u4IfIndex, &u4IfMtu);
            CfaGetIfSpeed (u4IfIndex, &u4IfSpeed);
            IfParams.u4IpAddr = pIpConfigInfo->u4Addr;
            IfParams.u4SubnetMask = pIpConfigInfo->u4NetMask;
            IfParams.u4BcastAddr = pIpConfigInfo->u4BcastAddr;
            IfParams.u4Mtu = u4IfMtu;
            IfParams.u4IfSpeed = u4IfSpeed;
            IfParams.u2Port = (UINT2) CFA_IF_IPPORT (u4IfIndex);
            IfParams.u2IfIndex = (UINT2) u4IfIndex;
            IfParams.u1IfType = u1IfType;

            if (u4Flag != 0)
            {
                if (CfaIpIfSetIfInfo (u4IfIndex,
                                      u4Flag, pIpConfigInfo) == CFA_FAILURE)
                {
                    return CFA_FAILURE;
                }
            }
            if (IpHandleInterfaceMsgFromCfa (&IfParams, IPIF_UPDATE_INTERFACE)
                == IP_SUCCESS)
            {
                i4RetVal = CFA_SUCCESS;

                CFA_DBG2 (CFA_TRC_ALL_TRACK, CFA_IFM,
                          "In CfaIfmConfigNetworkInterface - "
                          "updated interface %d IP address %d \n",
                          u4IfIndex, pIpConfigInfo->u4Addr);

            }
#endif
#if defined (IP6_WANTED) || defined (SMOD_WANTED)
            /* Notify interface update info to all HLs */
            CfaInfo.u4IfIndex = u4IfIndex;
            CfaGetIfType (u4IfIndex, &(CfaInfo.CfaIntfInfo.u1IfType));
            CfaGetIfMtu (u4IfIndex, &(CfaInfo.CfaIntfInfo.u4IfMtu));
            CfaGetIfOperStatus (u4IfIndex,
                                &(CfaInfo.CfaIntfInfo.u1IfOperStatus));
            CfaGetIfSpeed (u4IfIndex, &(CfaInfo.CfaIntfInfo.u4IfSpeed));
            CfaGetIfHighSpeed (u4IfIndex, &(CfaInfo.CfaIntfInfo.u4IfHighSpeed));
            CfaGetIfName (u4IfIndex, CfaInfo.CfaIntfInfo.au1IfName);
            CfaGetIfIvrVlanId (u4IfIndex, &CfaInfo.CfaIntfInfo.u2VlanId);

            if ((CfaInfo.CfaIntfInfo.u1IfType == CFA_ENET)
                || (CfaInfo.CfaIntfInfo.u1IfType == CFA_LAGG) ||
                (CfaInfo.CfaIntfInfo.u1IfType == CFA_L3SUB_INTF))
            {
                CfaGetIfHwAddr (u4IfIndex, CfaInfo.CfaIntfInfo.au1MacAddr);
            }
            else if ((CfaInfo.CfaIntfInfo.u1IfType == CFA_L3IPVLAN)
#ifdef WGS_WANTED
                     || (CfaInfo.CfaIntfInfo.u1IfType == CFA_L2VLAN)
#endif /* WGS_WANTED */
                )
            {
                CfaGetIfHwAddr (u4IfIndex, CfaInfo.CfaIntfInfo.au1MacAddr);
            }
            CfaNotifyIfUpdate (&CfaInfo);
#endif /* IP6_WANTED || SMOD_WANTED */

            break;

        case CFA_NET_IF_NIS:
        case CFA_NET_IF_DEL:
/* de-register the new interface from IP */
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
            IfParams.u2Port = (UINT2) CFA_IF_IPPORT (u4IfIndex);
            IfParams.u2IfIndex = (UINT2) u4IfIndex;
            IfParams.u1IfType = u1IfType;
#ifdef VRRP_WANTED

/* delete the vrrp interface from NP. This is done here because, vrrp module 
 * will not be able to get information about the interface since cfa will
 * clean up those informations before vrrp gets notification*/

            if (CfaDeleteVrrpInterface (u4IfIndex) == CFA_FAILURE)
            {
                return CFA_FAILURE;
            }
#endif

            CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
            if (u1OperStatus == CFA_IF_UP)
            {

                i4RetVal = CfaUpdtIvrInterface (u4IfIndex, CFA_IF_DELETE);
            }
            /*Below code has been added to resolve coverity
               unused_value error */
            if (i4RetVal)
            {
                /*Do nothing */
            }
#ifdef NPAPI_WANTED
            if ((CfaIsMgmtPort (u4IfIndex) == FALSE) &&
                (CfaIsLinuxVlanIntf (u4IfIndex) == FALSE))
            {
                if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId)
                    == VCM_FAILURE)
                {
                    return CFA_FAILURE;
                }
                if ((u1IfType == CFA_L3IPVLAN) || (u1IfType == CFA_LOOPBACK))
                {

                    if (CFA_NODE_STATUS () == CFA_NODE_ACTIVE)
                    {
                        if (CfaFsNpIpv4DeleteIpInterface
                            (u4ContextId, au1IfName, u4IfIndex,
                             u2VlanId) == FNP_FAILURE)
                        {
                            i4RetVal = CFA_FAILURE;
                        }
                    }
                }
                else if ((u1IfType == CFA_ENET) || (u1IfType == CFA_PPP)
                         || (u1IfType == CFA_PSEUDO_WIRE)
                         || (u1IfType == CFA_LAGG))
                {
                    FsNpL3IfInfo.u4VrId = u4ContextId;
                    FsNpL3IfInfo.u4IfIndex = u4IfIndex;
                    FsNpL3IfInfo.u4IpAddr = pIpConfigInfo->u4Addr;
                    FsNpL3IfInfo.u2PortVlanId = pIpConfigInfo->u2PortVlanId;
                    MEMCPY (FsNpL3IfInfo.au1IfName, au1IfName,
                            CFA_MAX_PORT_NAME_LENGTH);
                    MEMCPY (FsNpL3IfInfo.au1MacAddr, au1MacAddress,
                            MAC_ADDR_LEN);
                    if (u1IfType == CFA_PPP)
                    {
                        CfaGetLLIfIndex (u4IfIndex, &u4PhyIndex);
                        CfaGetIfName (u4PhyIndex, au1IfName);
                        FsNpL3IfInfo.u4IfIndex = u4PhyIndex;
                        MEMCPY (FsNpL3IfInfo.au1IfName, au1IfName,
                                CFA_MAX_PORT_NAME_LENGTH);
                    }
                    if (CfaFsNpIpv4L3IpInterface (L3_HW_DELETE_IF_INFO,
                                                  &FsNpL3IfInfo) == FNP_FAILURE)
                    {
                        i4RetVal = CFA_FAILURE;
                    }
                }
            }
#endif /* NPAPI_WANTED */

            /* Act on the secondary addresses configured on the interface */
            CfaIpIfHandleSecondaryAddress (u4IfIndex, CFA_NET_IF_DEL);
#ifdef LNXIP4_WANTED
            IfParams.u4IdxMgrPort = CFA_IF_IDXMGRPORT (u4IfIndex);
#endif

            i4RetVal =
                IpHandleInterfaceMsgFromCfa (&IfParams, IPIF_DELETE_INTERFACE);
            if (i4RetVal != IP_SUCCESS)
            {
                i4RetVal = CFA_FAILURE;
            }
            else
            {
                /* invalidate the IP port obtained during registration */
                i4RetVal = CFA_SUCCESS;

                pHighStackListScan =
                    (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
                CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
                /* update the interface stack - it is assumed that the stack is sorted */
                CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
                CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                          "In CfaIfmConfigNetworkInterface - "
                          "deleted interface %d \n", u4IfIndex);

            }
#endif
            CfaRegNotifyHigherLayer (u4IfIndex, CFA_IF_DELETE);
            if (u1OperCode == CFA_NET_IF_DEL)
            {

                i4RetVal = VcmCfaDeleteIPIfaceMapping (u4IfIndex, CFA_TRUE);

                if (i4RetVal == VCM_FAILURE)
                {
                    /* Unsuccessful in deleting the interface
                     * mapping from VCM */
                    i4RetVal = CFA_FAILURE;
                }

            }
            else if (u1OperCode == CFA_NET_IF_NIS)
            {
                /*When making the interface as NOT_IN_SERVICE the Mapping in VCM is made
                 * as NOT_READY instead of Deleting the Mapping*/
                if (VcmCfaDeleteIPIfaceMapping (u4IfIndex, CFA_FALSE) ==
                    VCM_FAILURE)
                {
                    i4RetVal = CFA_FAILURE;
                }

            }
#ifdef LNXIP4_WANTED
            CfaIfInfo.i4IpPort = CFA_INVALID_INDEX;
            CfaSetIfInfo (IF_IP_PORTNUM, u4IfIndex, &CfaIfInfo);

            CfaIfInfo.u4IndexMgrPort = CFA_ZERO;
            CfaSetIfInfo (IF_IP_IDX_MGR_NUM, u4IfIndex, &CfaIfInfo);
#endif

            break;

        default:                /* invalid command - i4RetVal = CFA_FAILURE */
            break;

    }                            /* end of switch */

    if (i4RetVal != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Exiting CfaIfmCreateAndInitIfEntry - "
                  "Config fail for interface %d\n", u4IfIndex);
    }
    else
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Exiting CfaIfmCreateAndInitIfEntry - "
                  "Config for interface %d - SUCCESS\n", u4IfIndex);
    }
    return (i4RetVal);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmHandleInterfaceStatusChange
 *
 *    Description        : This function is for propagating change in
 *                the admin and oper status through the
 *                interface stack. When called from the MIB
 *                - u1IsFromMib should be CFA_TRUE else it
 *                should be CFA_FALSE. This function does
 *                pre-order traversal of the tree formed with
 *                the root at the node whose status has
 *                changed.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *                UINT1 u1AdminStatus,
 *                UINT1 u1OperStatus,
 *                UINT1 u1IsFromMib
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if updation succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmHandleInterfaceStatusChange (UINT4 u4IfIndex, UINT1 u1AdminStatus,
                                   UINT1 u1OperStatus, UINT1 u1IsFromMib)
{

    tStackInfoStruct   *pScanNode;
    tStackInfoStruct   *pTempScanNode;
    tTMO_SLL           *pIfStack;
    tIfStatusPropagate *pListEntry = NULL;
    UINT4               u4TempIfIndex;
    UINT1               u1IsToBeAddedToList = CFA_FALSE;
    UINT1               u1IfAdminStatus;
    UINT1               u1IfType = CFA_NONE;
    UINT1               u1TmpOperStatus;
    tStackInfoStruct   *pHighStackListScan = NULL;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmHandleInterfaceStatusChange for interface %d.\n",
              u4IfIndex);
    CFA_DBG3 (CFA_TRC_ALL, CFA_IFM,
              "In CfaIfmHandleInterfaceStatusChange from "
              "MIB? %d, Admin %d, Oper %d.\n",
              u1IsFromMib, u1AdminStatus, u1OperStatus);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        if (u1OperStatus == CFA_IF_NP)
        {
            if (CfaCheckIsHgIndex ((UINT2) u4IfIndex) == CFA_SUCCESS)
                return CFA_SUCCESS;
        }
    }
/* first we update the status for the concerned interface and send indication
to the core data-link layer module */
    if (CfaIfmChangeInterfaceStatus (u4IfIndex, u1AdminStatus, &u1OperStatus,
                                     u1IsFromMib) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmHandleInterfaceStatusChange -"
                  "Update fail for interface %d\n", u4IfIndex);
        if (u1IsFromMib == CFA_TRUE)
            return (CFA_FAILURE);
    }

    pScanNode = (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pScanNode, CFA_FAILURE);
    /* if this is the higher most interface then our job is done - exit */
    if (CFA_IF_STACK_IFINDEX (pScanNode) == CFA_NONE)
        return (CFA_SUCCESS);

/* if the Oper is UNK - then we cant propagate the status till it becomes
either UP or DOWN */
    if (u1OperStatus == CFA_IF_UNK)
        return CFA_SUCCESS;

/* initialise the List which we use for traversing up the stack layers */
    TMO_SLL_Init (&IfStatusUpdate);

/* travel all the paths up the stack from the layer whose status has changed.
the terminating condition along each path is when we reach a node which is the
topmost layer or when the node has more than one interfaces below it. We
expect that a node which has multiple interfaces under it (like MP) has a
mechanism by which it decides its own status based on the status of the
interfaces under it - we dont need to tell it - we just change the status of
the interfaces under it. */

/* initialise the pScanNode with the first interface above current interface
and pIfStack with the Higher Stack Linked list.*/
    pIfStack = &CFA_IF_STACK_HIGH (u4IfIndex);

/* if the current interface is Oper down then all the higher interfaces should
have oper as lower-layer-down. similarly for dormant.
for all other Oper status we keep them as is.
We dont change the adminstatus for any of the higher interfaces. */

    CfaGetIfType (pScanNode->u4IfIndex, &u1IfType);
    CfaGetIfOperStatus (u4IfIndex, &u1TmpOperStatus);

    if (u1OperStatus == CFA_IF_DOWN)
        u1OperStatus = CFA_IF_LLDOWN;

    while (CFA_ALWAYS)
    {

/* if the interface is the highest or the interface above it has more than 1
lower interfaces, then update its interface status. */
        CFA_CHECK_IF_NULL (pScanNode, CFA_FAILURE);

        if (CfaValidateCfaIfIndex (pScanNode->u4IfIndex) == CFA_FAILURE)
        {
            return CFA_FAILURE;
        }
        pHighStackListScan = CFA_IF_STACK_HIGH_ENTRY (pScanNode->u4IfIndex);
        CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
        u4TempIfIndex = CFA_IF_STACK_IFINDEX (pHighStackListScan);
        if (u4TempIfIndex == CFA_NONE)
        {
/* we check this first to avoid invalid index into the ifTable */
            u1IsToBeAddedToList = CFA_FALSE;
        }
        else
        {
            CfaGetIfType (u4TempIfIndex, &u1IfType);

            /* Oper status indication for all higher layers of CFA_TELINK
             * should be done. */
            if (u1IfType == CFA_TELINK)
            {
                u1IsToBeAddedToList = CFA_TRUE;
            }
            else if ((CFA_IF_ENTRY (u4TempIfIndex) != NULL)
                     && (TMO_SLL_Count (&CFA_IF_STACK_LOW (u4TempIfIndex)) > 1))
            {
                u1IsToBeAddedToList = CFA_FALSE;
            }
            else
            {
/* now this means that we need to add this node to the list and proceed our
traversal up the higher layers. */
                u1IsToBeAddedToList = CFA_TRUE;
            }
        }

/* before proceeding we update this interface's status */
/* For AtmVc the admin status that has to be passed is the admin status of the
lower interface */

        CfaGetIfType (pScanNode->u4IfIndex, &u1IfType);

        u1IfAdminStatus = CFA_IF_ADMIN (pScanNode->u4IfIndex);

        if (CfaIfmChangeInterfaceStatus (pScanNode->u4IfIndex,
                                         u1IfAdminStatus,
                                         &u1OperStatus,
                                         u1IsFromMib) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                      "Error in CfaIfmHandleInterfaceStatusChange - "
                      "propgt fail for interface %d\n", u4IfIndex);

/* if we have to propagate the interface UP event through the stack and
fail, then we cannot proceed further along this path */
            if ((u1OperStatus == CFA_IF_UP) || (u1OperStatus == CFA_IF_DORM))
                u1IsToBeAddedToList = CFA_FALSE;

        }

/* if the operstatus is going UP - i.e. UNK - then we cannot proceed 
further - we can go further only when the status is UP or DOWN */
        if (u1OperStatus == CFA_IF_UNK)
            u1IsToBeAddedToList = CFA_FALSE;

/* now we proceed with our traversal */
        if (u1IsToBeAddedToList == CFA_FALSE)
        {

/* now we check and see if there are any other interfaces in the higher
stack entry list */
            if ((pScanNode = (tStackInfoStruct *)
                 TMO_SLL_Next (pIfStack, &pScanNode->NextEntry)) != NULL)
            {
/* there are still other interfaces left in the same stack table list */
                continue;
            }
            else
            {
/* this stack list is exhausted - so we take a node from the list which we
use for traversal up all the higher layers */
                if ((pListEntry = (tIfStatusPropagate *)
                     TMO_SLL_Get (&IfStatusUpdate)) == NULL)
                {
/* this is our terminating condition - there are no more nodes in the list */
                    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                              "In CfaIfmHandleInterfaceStatusChange - "
                              "status propgt over for interface %d\n",
                              u4IfIndex);
                    break;
                }
                else
                {
/* initialise the pScanNode and pIfStack with the values from the node and
then release the node */
                    pIfStack = pListEntry->pStack;
                    pScanNode = pListEntry->pNode;
                    MemReleaseMemBlock (gCfaIntfStatusPoolId,
                                        (UINT1 *) pListEntry);
                    continue;
                }
            }                    /* end of taking a new node and stack list from our traversal
                                   list */
        }                        /* end of not to be added to our traversal list */

        else
        {
/* we now have to add the current stack table list and the current node to
our traversal list and proceed onto a higher interface layer */

/* before adding the next node at this layer into our traversal list we
first verify whether such a node exists in the first place - no need to
add if it doesnt exist at all. */
            if ((pTempScanNode = (tStackInfoStruct *)
                 TMO_SLL_Next (pIfStack, &pScanNode->NextEntry)) != NULL)
            {
/* there does exist a node at this level */

/* we allocate for a node in our traversal list */
                if ((pListEntry = MemAllocMemBlk (gCfaIntfStatusPoolId))
                    == NULL)
                {
/* Memory Error    - but we dont return failure - we stop the
traversal up this path and continue after taking a fresh node from the
traversal list or the stack list as the case maybe. Code is duplicated
from above. */

                    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                              "Error in CfaIfmHandleInterfaceStatusChange - "
                              "No Memory for interface %d\n", u4IfIndex);

/* call the FM fault handler and release buffer */

                    if ((pScanNode =
                         (tStackInfoStruct *) TMO_SLL_Next (pIfStack,
                                                            &pScanNode->
                                                            NextEntry)) != NULL)
                    {
                        continue;
                    }
                    else
                    {
                        if ((pListEntry = (tIfStatusPropagate *)
                             TMO_SLL_Get (&IfStatusUpdate)) == NULL)
                        {
                            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                                      CFA_IFM,
                                      "In CfaIfmHandleInterfaceStatusChange - status propagate term for interface %d\n",
                                      u4IfIndex);
                            break;
                        }
                        else
                        {
                            pIfStack = pListEntry->pStack;
                            pScanNode = pListEntry->pNode;
                            MemReleaseMemBlock (gCfaIntfStatusPoolId,
                                                (UINT1 *) pListEntry);
                            continue;
                        }
                    }
/* end of code for resuming traversal along a different path following
memory allocation failure */
                }
                else
                {
/* first we save our current position in the traversal list before proceeding
higher in the interface stack */
                    pListEntry->pStack = pIfStack;
                    pListEntry->pNode = pTempScanNode;
                    TMO_SLL_Insert (&IfStatusUpdate, NULL,
                                    &pListEntry->NextEntry);

/* now we initialise pScanNode and pIfStack with the values of the higher
layer */
                    pIfStack =
                        (tTMO_SLL *) & CFA_IF_STACK_HIGH (pScanNode->u4IfIndex);
                    pScanNode =
                        (tStackInfoStruct *)
                        CFA_IF_STACK_HIGH_ENTRY (pScanNode->u4IfIndex);
                    continue;

                }
/* end of need for allocating a node and adding it to the traversal list */
            }
            else
            {
/* there is no node at this stage - so no need to allocate and add to the
traversal list - simply initialise the pScanNode and pIfStack with the
new values */
                pIfStack =
                    (tTMO_SLL *) & CFA_IF_STACK_HIGH (pScanNode->u4IfIndex);
                pScanNode =
                    (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (pScanNode->
                                                                  u4IfIndex);
                continue;

            }
        }                        /* end of the need/possibility of adding to the traversal list */

    }                            /* end of while(1) loop */

    return (CFA_SUCCESS);

}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmEnetConfigMcastAddr
 *
 *    Description        : This function configures the multicast
 *                address on the Enet interfaces.
 *
 *    Input(s)            : UINT1 u1OperCode,
 *                UINT4 u4IfIndex,
 *                UINT4 u4IpAddr,
 *                UINT1 *au1HwAddr
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if configuration succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmEnetConfigMcastAddr (UINT1 u1OperCode, UINT4 u4IfIndex,
                           UINT4 u4IpAddr, UINT1 *au1HwAddr)
{

    INT4                i4RetVal = CFA_FAILURE;
    UINT1               au1MediaAddr[CFA_MAX_MEDIA_ADDR_LEN];

    MEMSET (au1MediaAddr, 0, sizeof (au1MediaAddr));

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmEnetConfigMcastAddr for interface %d.\n",
              u4IfIndex);

    /* If the Port is OOB Port ,Don't allow Configuration of 
       McastAddr Info */
    if (CfaIsMgmtPort (u4IfIndex) == TRUE)
    {
        return (CFA_FAILURE);
    }

/* ifIndex need not be verified as this function is called only by
CFA's modules */

    switch (u1OperCode)
    {

        case CFA_ADD_IP:
/* we allocate memory for the HW addr - we expect that the calling
function has not allocated any memory in this case */
            au1HwAddr = au1MediaAddr;
            CfaIwfEnetFormEnetMcastAddr (u4IpAddr, au1HwAddr);

/* no need for putting break - having obtained the Enet mcast addr, perform
the steps for adding enet address. */

        case CFA_ADD_ENET:
/* add the address to the Enet port's multicast address list */
            if (CfaGddConfigPort (u4IfIndex, CFA_ENET_ADD_MCAST,
                                  (VOID *) au1HwAddr) == CFA_SUCCESS)
            {
                i4RetVal = CFA_SUCCESS;

/* add the address in the rcv addr table */
                if (CfaIfmAddRcvAddrEntry (u4IfIndex, au1HwAddr,
                                           CFA_RCVADDR_VOL) != CFA_SUCCESS)
                {

                    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                              "Error in CfaIfmEnetConfigMcastAddr - "
                              "Add in RcvAddrTab fail interface %d\n",
                              u4IfIndex);
                }
            }

            break;

        case CFA_DEL_IP:
/* we allocate memory for the HW addr - we expect that the calling
function has not allocated any memory in this case */

            au1HwAddr = au1MediaAddr;
            CfaIwfEnetFormEnetMcastAddr (u4IpAddr, au1HwAddr);

/* no need for putting break - having obtained the Enet mcast addr, perform
the steps for deleting enet address. */

        case CFA_DEL_ENET:
/* no need to verify as config will fail if address is not found */
/* delete the address from the Enet port's multicast address list */
/* add the address to the Enet port's multicast address list */
            if (CfaGddConfigPort (u4IfIndex, CFA_ENET_DEL_MCAST,
                                  (VOID *) au1HwAddr) == CFA_SUCCESS)
            {
                i4RetVal = CFA_SUCCESS;

/* del the address in the rcv addr table */
                CfaIfmDeleteRcvAddrEntry (u4IfIndex, au1HwAddr);
            }
            break;

        case CFA_ADD_ALL_MULTI:
        case CFA_DEL_ALL_MULTI:
            if (CfaGddConfigPort (u4IfIndex, u1OperCode, NULL) == CFA_SUCCESS)
            {
                i4RetVal = CFA_SUCCESS;
            }
            break;

        default:
            break;                /* CFA_FAILURE */

    }

    if (i4RetVal != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Exiting CfaIfmEnetConfigMcastAddr - "
                  "Config fail for interface %d\n", u4IfIndex);
    }
    else
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Exiting CfaIfmEnetConfigMcastAddr - "
                  "Config for interface %d - SUCCESS\n", u4IfIndex);
    }

    return (i4RetVal);

}

/*****************************************************************************
 *    Function Name      : CfaIfmInitDefaultVal
 *    Description        : This function initializes the default values 
 *                         for all the interfaces in ppCfaIfDefaultVal table  
 *    Input(s)           : UINT4 u4IfIndex,
 *                         tCfaIfCreateParams - Interface info       
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT1
CfaIfmInitDefaultVal (VOID)
{
    UINT2               u2IfType;
    UINT2               u2Index;

    for (u2IfType = 0; u2IfType <= CFA_MAX_IF_TYPES; u2IfType++)
    {
        for (u2Index = 0; u2Index < CFA_IF_TYPES; u2Index++)
        {
            if (asCfaIfDefaultVal[u2Index].u4IfType == u2IfType)
            {
                ppCfaIfDefaultVal[u2IfType] = &asCfaIfDefaultVal[u2Index];
                break;
            }
        }
        if (u2Index == CFA_IF_TYPES)
        {
            ppCfaIfDefaultVal[u2IfType] = NULL;
        }
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *    Function Name      : CfaIfmInitTunlIfEntry
 *    Description        : This function creates & initializes the entries 
 *                         in the ifTable for logical interfaces 
 *    Input(s)           : UINT4 u4IfIndex,
 *                         tCfaIfCreateParams - Interface info       
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitTunlIfEntry (UINT4 u4IfIndex, tCfaIfCreateParams * pIfaceInfo)
{
    UINT4               u4IfType;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tIpConfigInfo       IpCfgInfo;
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4Flag;
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;

    u4IfType = pIfaceInfo->u4IfType;

    if (STRLEN (pIfaceInfo->au1AliasName) != 0)
    {
        CfaSetIfName (u4IfIndex, pIfaceInfo->au1AliasName);
    }
    else
    {
        SNPRINTF ((CHR1 *) au1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s%d",
                  ppCfaIfDefaultVal[u4IfType]->au1AliasPrefix,
                  ppCfaIfDefaultVal[u4IfType]->u4IfCount);

        CfaSetIfName (u4IfIndex, au1IfName);
    }

    CfaSetIfType (u4IfIndex, (UINT1) pIfaceInfo->u4IfType);

    /* allocate for the IP config entry in the ifTable since IP can run
       over this interface */
    if (CfaIpIfCreateIpInterface (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitTunlIfEntry - "
                  "IP Entry Mempool alloc fail interface %d.\n", u4IfIndex);
    }

    /* Create mem for pIvrEntry */
    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_DISABLED);
    }

    u4Flag = CFA_IP_IF_ALLOC_PROTO | CFA_IP_IF_ALLOC_METHOD;

    IpCfgInfo.u1AddrAllocMethod = CFA_IP_ALLOC_MAN;
    IpCfgInfo.u1AddrAllocProto = CFA_PROTO_DHCP;
    if (CfaIpIfSetIfInfo (u4IfIndex, u4Flag, &IpCfgInfo) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Setting IP Param for Interface %d failed\n", u4IfIndex);
        return CFA_FAILURE;
    }

    CfaIfInfo.i4IpPort = CFA_INVALID_INDEX;
    CfaSetIfInfo (IF_IP_PORTNUM, u4IfIndex, &CfaIfInfo);

    CFA_IF_TRAP_EN (u4IfIndex) = CFA_DISABLED;    /* as it's not physical if */
    STRNCPY (CFA_IF_DESCR (u4IfIndex),
             ppCfaIfDefaultVal[u4IfType]->au1AliasDescr,
             STRLEN (ppCfaIfDefaultVal[u4IfType]->au1AliasDescr));
    CFA_IF_DESCR (u4IfIndex)[STRLEN
                             (ppCfaIfDefaultVal[u4IfType]->au1AliasDescr)] =
        '\0';
    if (!(pIfaceInfo->u4IfSpeed))
    {
        CfaSetIfSpeed (u4IfIndex, ppCfaIfDefaultVal[u4IfType]->u4IfSpeed);
    }
    else
    {
        CfaSetIfSpeed (u4IfIndex, pIfaceInfo->u4IfSpeed);
    }

    if (!(pIfaceInfo->u4IfMtu))
    {
        CfaSetIfMtu (u4IfIndex, ppCfaIfDefaultVal[u4IfType]->u4IfMtu);
    }
    else
    {
        CfaSetIfMtu (u4IfIndex, pIfaceInfo->u4IfMtu);
    }

    CfaSetIfHighSpeed (u4IfIndex, 0);
    /* No RcvAddrTable for logical interface */
    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_ENETV2;
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);
    pHighStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
    pLowStackListScan = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);
    CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
    CFA_IF_STACK_STATUS (pLowStackListScan) = CFA_RS_ACTIVE;

    /* update the interface count */
    ++(ppCfaIfDefaultVal[u4IfType]->u4IfCount);

    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
              "Initialized Logical interface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *    Function Name        : CfaIfmDeInitTunlInterface
 *    Description          : This function is for for de-registration of
 *                           interface with IP and higher layers. 
 *    Input(s)             : UINT4 u4IfIndex,
 *    Output(s)            : None.
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *    Returns              : CFA_SUCCESS if deletion succeeds,
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaIfmDeInitTunlInterface (UINT4 u4IfIndex, UINT1 u1DelIfEntry)
{
    UINT1               u1IfType = CFA_NONE;
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;
    tCfaTnlCxt         *pTunlCxt = NULL;
    UINT4               u4CxtId = 0;

    CFA_IF_ADMIN (u4IfIndex) = CFA_IF_DOWN;
    CfaSetCdbPortAdminStatus (u4IfIndex, CFA_IF_DOWN);

    /* check if it is registered with IP */
    if (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX)
    {
        if (CfaIfmConfigNetworkInterface
            (CFA_NET_IF_DEL, u4IfIndex, 0, NULL) != CFA_SUCCESS)
        {
            /* unsuccessful in de-registering from IP - we cant delete 
             * the interface. we dont expect this to fail - just for 
             * handling unforeseen exceptions. */
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmDeInitTunlInterface - "
                      "IP De-reg fail for interface %d\n", u4IfIndex);
            return (CFA_FAILURE);
        }
    }

    while (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex))
    {
        pHighStackListScan =
            (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
        CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
        if (CFA_IF_STACK_IFINDEX (pHighStackListScan) != CFA_NONE)
        {
            /* First Delete this entry from Higher layer's stack value. */
            CfaIfmDeleteStackEntry
                (CFA_IF_STACK_IFINDEX (pHighStackListScan), CFA_LOW, u4IfIndex);

            CfaIfmDeleteStackEntry (u4IfIndex, CFA_HIGH,
                                    CFA_IF_STACK_IFINDEX (pHighStackListScan));
        }
        else
        {
            CfaIfmUpdateStackForDeletion (&CFA_IF_STACK_HIGH (u4IfIndex),
                                          CFA_FALSE, CFA_NONE);
        }
    }

    while (CFA_IF_STACK_LOW_ENTRY (u4IfIndex))
    {
        pLowStackListScan =
            (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
        CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);

        if (CFA_IF_STACK_IFINDEX (pLowStackListScan) != CFA_NONE)
        {
            /* First Delete this entry from the lower layer's stack value. */
            CfaIfmDeleteStackEntry
                (CFA_IF_STACK_IFINDEX (pLowStackListScan), CFA_HIGH, u4IfIndex);

            CfaIfmDeleteStackEntry (u4IfIndex, CFA_LOW,
                                    CFA_IF_STACK_IFINDEX (pLowStackListScan));
        }
        else
        {
            CfaIfmUpdateStackForDeletion (&CFA_IF_STACK_LOW (u4IfIndex),
                                          CFA_FALSE, CFA_NONE);
        }
    }

    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
              "In CfaIfmDeInitTunlInterface - "
              "Un-reg with HL success - interface %d\n", u4IfIndex);

    /* check whether the entry is to be deleted from IfTable itself */
    if (u1DelIfEntry == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "In CfaIfmDeInitTunlInterface - "
                  "disabling success - interface %d\n", u4IfIndex);

        return (CFA_SUCCESS);
    }

    CfaGetIfType (u4IfIndex, &u1IfType);
    /* update the interface count */
    --(ppCfaIfDefaultVal[u1IfType]->u4IfCount);

    /* Intimate higher layers */

    if (u1IfType == CFA_TUNNEL)
    {
        VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4CxtId);
        pTunlCxt = CfaTnlGetCxt (u4CxtId);
        /* delete the entry from TnlIfTbl */
        if (CfaDeleteTnlIfEntryInCxt (pTunlCxt, u4IfIndex) == CFA_FAILURE)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmDeInitTunlInterface - "
                      "TnlEntry delete fail for interface %d\n", u4IfIndex);
        }
    }

    /* delete the interface from the IfTable */
    if (CfaIfmDeleteIfEntry (u4IfIndex, u1IfType) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeInitTunlInterface - "
                  "ifEntry delete fail for interface %d\n", u4IfIndex);
        return (CFA_FAILURE);
    }
    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
              "In CfaIfmDeInitTunlInterface - "
              "Full Delete success - interface %d\n", u4IfIndex);

    return (CFA_SUCCESS);
}

#ifdef VRRP_WANTED
/*****************************************************************************
 *    Function Name      : CfaDeleteVrrpInterface
 *    Description        : This function deletes a vrrp interface from NP 
 *    Input(s)           : UINT4 u4IfIndex,
 *    Returns            : CFA_SUCCESS or  CFA_FAILURE.
 *
*****************************************************************************/
INT4
CfaDeleteVrrpInterface (UINT4 u4IfIndex)
{
    if (VrrpEnabledOnInterface ((INT4) u4IfIndex) == VRRP_OK)
    {
        /* Delete all VRID information in this interface in which
         * it is master. */
        if (VrrpHandleIfDelete (u4IfIndex) == VRRP_NOT_OK)
        {
            return CFA_FAILURE;
        }
    }

    return CFA_SUCCESS;
}
#endif
/*****************************************************************************
 *    Function Name      : CfaSetHgPortsEtherType
 *    Description        : This function to set higig port as XE ports
 *    Input(s)           : u2IfIndex - Ifindex
 *    Output             : pu1IfType - ethertype
 *    Returns            : None.
 *
*****************************************************************************/
VOID
CfaSetHgPortsEtherType (UINT2 u2IfIndex, UINT1 *pu1IfType)
{
#ifdef MBSM_WANTED
    UINT4               u4EndIfIndex = 0;
    INT4                i4Slot = 0;
    INT1                i1MaxHgPortCount = 0;

    *pu1IfType = CFA_ENET_UNKNOWN;

    i1MaxHgPortCount = (ISS_MAX_POSSIBLE_STK_PORTS -
                        IssGetStackPortCountFromNvRam ());

    /* If u2IfIndex is Higig ports index and which is not stack port 
     * index means set it as XE_ENET*/
    for (i4Slot = MBSM_SLOT_INDEX_START;
         i4Slot < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START); i4Slot++)
    {
        /* Particular slot's final ifindex of physical ports
         * all higig ports except stack ports*/
        u4EndIfIndex = ((i4Slot * MBSM_MAX_POSSIBLE_PORTS_PER_SLOT) +
                        MBSM_MAX_PORTS_PER_SLOT);

        if (((UINT4) u2IfIndex >= u4EndIfIndex - i1MaxHgPortCount) &&
            ((UINT4) u2IfIndex <= u4EndIfIndex))
        {
            *pu1IfType = CFA_XE_ENET;
            break;
        }
    }
#else
    UNUSED_PARAM (u2IfIndex);
    UNUSED_PARAM (pu1IfType);
#endif
}

/*****************************************************************************
 *    Function Name      : CfaCheckIsHgIndex
 *    Description        : This function to check u2IfIndex is higig port 
                           or not   
 *    Input(s)           : u2IfIndex - IfIndex
 *    Output             : None
 *    Returns            : CFA_SUCCESS/CFA_FAILURE
 *
*****************************************************************************/
INT4
CfaCheckIsHgIndex (UINT2 u2IfIndex)
{
#ifdef MBSM_WANTED
    INT4                i4Slot = 0;
    INT4                i4EndIfIndex = 0;
    for (i4Slot = MBSM_SLOT_INDEX_START;
         i4Slot < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START); i4Slot++)
    {
        /* Particular slot's final ifindex of physical ports including 
         * all higig  ports*/
        i4EndIfIndex = ((i4Slot + 1) * MBSM_MAX_POSSIBLE_PORTS_PER_SLOT);

        if ((u2IfIndex > (i4EndIfIndex - ISS_MAX_POSSIBLE_STK_PORTS))
            && (u2IfIndex <= i4EndIfIndex))
        {
            return CFA_SUCCESS;
        }
    }
#else
    UNUSED_PARAM (u2IfIndex);
#endif
    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name      : CfaIfmCheckAndSetL3VlanIfWanType 
 *
 *    Description        : This function is to set the Network type for the
 *                       : L3 interface ,if there is any physical port of WAN
 *                       : type added to the L2 VLAN. 
 *                       : This function is applicable for IVR interfaces .
 *
 *    Input(s)           : UINT4 u4IfIndex : IVR Ifindex
 *
 *    Output(s)          : None.
 *
 *****************************************************************************/
INT4
CfaIfmCheckAndSetL3VlanIfWanType (UINT4 u4IfIndex, UINT1 u1Status)
{
    tPortList          *pEgressPortList = NULL;
    tPortList          *pUntagPortList = NULL;
    UINT4               u4PhyIfIndex = 0;
    UINT2               u2IfIvrVlanId = 0;
    UINT1               u1NwType = 0;
    BOOL1               bResult = OSIX_FALSE;
    BOOL1               bWanPresentFlag = OSIX_FALSE;

    if (u4IfIndex < CFA_MIN_IVR_IF_INDEX || u4IfIndex > CFA_MAX_IVR_IF_INDEX)
    {
        return CFA_SUCCESS;
    }

    if (CFA_FAILURE == CfaGetIfIvrVlanId (u4IfIndex, &u2IfIvrVlanId))
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "CfaIfmCheckAndSetL3VlanIfWanType:Getting the IVR VLAN ID for the IfIndex\n");
        return CFA_FAILURE;

    }
    if (u1Status == CFA_IF_DELETE)
    {
        CfaSetIfNwType (u4IfIndex, CFA_NETWORK_TYPE_LAN);
        CfaSetIfWanType (u4IfIndex, CFA_WAN_TYPE_PRIVATE);
        return CFA_SUCCESS;;
    }

    pEgressPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pEgressPortList == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "CfaIfmCheckAndSetL3VlanIfWanType:Error in Allocating memory for EgressportList\r\n");
        return CFA_FAILURE;
    }
    pUntagPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pUntagPortList == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pEgressPortList);
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "CfaIfmCheckAndSetL3VlanIfWanType:Error in Allocating memory for UntagPortList\r\n");
        return CFA_FAILURE;
    }
    MEMSET (pEgressPortList, 0, sizeof (tPortList));
    MEMSET (pUntagPortList, 0, sizeof (tPortList));

    if (VLAN_FAILURE ==
        VlanGetVlanMemberPorts (u2IfIvrVlanId, *pEgressPortList,
                                *pUntagPortList))
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "CfaIfmCheckAndSetL3VlanIfWanType:Getting the member ports of the VLAN failed\r\n");
        FsUtilReleaseBitList ((UINT1 *) pEgressPortList);
        FsUtilReleaseBitList ((UINT1 *) pUntagPortList);
        /*  When VLAN is not yet created ,but we configure the IVR interface,
         * then there will be no member ports.Therefore return SUCCESS */

        return CFA_SUCCESS;
    }

    for (u4PhyIfIndex = 1; u4PhyIfIndex <= BRG_NUM_PHY_PLUS_LOG_PORTS;
         u4PhyIfIndex++)
    {
        OSIX_BITLIST_IS_BIT_SET ((*pEgressPortList), u4PhyIfIndex,
                                 BRG_PORT_LIST_SIZE, bResult);

        if (OSIX_TRUE == bResult)
        {
            CfaGetIfNwType (u4PhyIfIndex, &u1NwType);
            if (u1NwType == CFA_NETWORK_TYPE_WAN)
            {
                CfaSetIfNwType (u4IfIndex, CFA_NETWORK_TYPE_WAN);
                CfaSetIfWanType (u4IfIndex, CFA_WAN_TYPE_PUBLIC);
                bWanPresentFlag = CFA_TRUE;
                break;
            }

        }
    }

    if (bWanPresentFlag != OSIX_TRUE)
    {
        CfaSetIfNwType (u4IfIndex, CFA_NETWORK_TYPE_LAN);
        CfaSetIfWanType (u4IfIndex, CFA_WAN_TYPE_PRIVATE);
    }
    FsUtilReleaseBitList ((UINT1 *) pEgressPortList);
    FsUtilReleaseBitList ((UINT1 *) pUntagPortList);
    return CFA_SUCCESS;
}

#if defined (WLC_WANTED) || defined (WTP_WANTED)
/*****************************************************************************
 *
 *    Function Name        : CfaIfmInitRadioIfEntry
 *
 *    Description        : This function allocates for Radio IWF in the
 *                interface table and initialises it to
 *                default values.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitRadioIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;
    UINT1               au1HwAddr[CFA_MAX_MEDIA_ADDR_LEN];
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    tCfaIfInfo          CfaIfInfo;
    UINT1               au1Tmp[CFA_MAX_PORT_NAME_LENGTH];
    UINT1              *pu1Alias = NULL;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitRadioIfEntry for Radio interface %d .\n",
              u4IfIndex);

    MEMSET (au1Tmp, 0, CFA_MAX_PORT_NAME_LENGTH);
    pu1Alias = &au1Tmp[0];
    MEMSET (au1HwAddr, 0, CFA_MAX_MEDIA_ADDR_LEN);

    /* check if the IfIndex is valid */
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitRadioIfEntry - "
                  "invalid interface %d.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    if ((CFA_IF_IWF (u4IfIndex) =
         MemAllocMemBlk (CFA_ENET_IWF_MEMPOOL)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitRadioIfEntry - "
                  "IWF struct malloc fail for interface %d\n", u4IfIndex);

        /* restore the GDD entry */
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    MEMSET (CFA_IF_IWF (u4IfIndex), 0, sizeof (tEnetIwfStruct));
    CfaSetIfType (u4IfIndex, CFA_RADIO);
    CfaSetIfName (u4IfIndex, au1PortName);
    CfaSetPortName (u4IfIndex, au1PortName);
    CfaSetIfNwType (u4IfIndex, CFA_NETWORK_TYPE_LAN);

    STRNCPY (CFA_IF_DESCR (u4IfIndex), CFA_RADIO_DESCR,
             STRLEN (CFA_RADIO_DESCR));
    CFA_IF_DESCR (u4IfIndex)[STRLEN (CFA_RADIO_DESCR)] = '\0';
    CfaGetIfDescr (u4IfIndex, CFA_IF_DESCR (u4IfIndex));

    MEMSET (CfaIfInfo.au1Descr, 0, CFA_MAX_DESCR_LENGTH);
    MEMCPY (CfaIfInfo.au1Descr, CFA_IF_DESCR (u4IfIndex), CFA_MAX_DESCR_LENGTH);
    CfaSetIfInfo (IF_DESC, u4IfIndex, &CfaIfInfo);

    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    /* create the entry in the GDD table for this interface */
    if (CfaGddInitPort (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitRadioIfEntry - "
                  "GDD entry creation fail for interface %d\n", u4IfIndex);
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    /* we first register this interface with the GDD module immediately */
    /* Gdd Registration is same as CFA_ENET . Purposefully passing type
       as CFA_ENET */
    if (CfaGddRegisterPort (u4IfIndex, CFA_ENET) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitRadioIfEntry - "
                  "Registration fail for interface %d\n", u4IfIndex);
        /* restore the GDD entry */
        CfaGddDeInitPort (u4IfIndex);
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    /* get the HW address from the driver - if failure then dont show it */
    if (CfaIfmAddRcvAddrEntry (u4IfIndex, au1HwAddr, CFA_RCVADDR_NVOL)
        != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitRadioIfEntry - "
                  "RcvAddrTab UCAST add fail on interface %d\n", u4IfIndex);
    }

    /* Add Enet Bcast addr also */
    MEMSET (au1HwAddr, 0, CFA_MAX_MEDIA_ADDR_LEN);
    MEMSET (au1HwAddr, 0xff, CFA_ENET_ADDR_LEN);
    if (CfaIfmAddRcvAddrEntry (u4IfIndex, au1HwAddr, CFA_RCVADDR_NVOL)
        != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitRadioIfEntry - "
                  "RcvAddrTab BCAST add fail on interface %d\n", u4IfIndex);
    }

    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_OTHER;
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);

    pHighStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
    pLowStackListScan = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);

    CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
    CFA_IF_STACK_STATUS (pLowStackListScan) = CFA_RS_ACTIVE;

    MEMSET (au1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);

    SPRINTF ((CHR1 *) pu1Alias, "%s%d", CFA_RADIO_NAME_PREFIX,
             u4IfIndex - SYS_DEF_MAX_ENET_INTERFACES);
    CfaSetPortName (u4IfIndex, pu1Alias);
    CfaSetIfName (u4IfIndex, pu1Alias);

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmInitRadioIfEntry - "
              "Initialised Radio interface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmInitWlanRadioIfEntry
 *
 *    Description        : This function allocates for Radio IWF in the
 *                interface table and initialises it to
 *                default values.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitWlanRadioIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;
    UINT1               au1HwAddr[CFA_MAX_MEDIA_ADDR_LEN];
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    tCfaIfInfo          CfaIfInfo;
    UINT1               au1Tmp[CFA_MAX_PORT_NAME_LENGTH];
    UINT1              *pu1Alias = NULL;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitWlanRadioIfEntry for Radio interface %d .\n",
              u4IfIndex);

    MEMSET (au1Tmp, 0, CFA_MAX_PORT_NAME_LENGTH);
    pu1Alias = &au1Tmp[0];
    MEMSET (au1HwAddr, 0, CFA_MAX_MEDIA_ADDR_LEN);

    /* check if the IfIndex is valid */
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitWlanRadioIfEntry - "
                  "invalid interface %d.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    if ((CFA_IF_IWF (u4IfIndex) =
         MemAllocMemBlk (CFA_ENET_IWF_MEMPOOL)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitWlanRadioIfEntry - "
                  "IWF struct malloc fail for interface %d\n", u4IfIndex);

        /* restore the GDD entry */
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    MEMSET (CFA_IF_IWF (u4IfIndex), 0, sizeof (tEnetIwfStruct));
    CfaSetIfType (u4IfIndex, CFA_WLAN_RADIO);
    CfaSetIfName (u4IfIndex, au1PortName);
    CfaSetPortName (u4IfIndex, au1PortName);
    CfaSetIfNwType (u4IfIndex, CFA_NETWORK_TYPE_LAN);

    STRNCPY (CFA_IF_DESCR (u4IfIndex), CFA_WLAN_RADIO_DESCR,
             STRLEN (CFA_WLAN_RADIO_DESCR));
    CFA_IF_DESCR (u4IfIndex)[STRLEN (CFA_WLAN_RADIO_DESCR)] = '\0';
    CfaGetIfDescr (u4IfIndex, CFA_IF_DESCR (u4IfIndex));

    MEMSET (CfaIfInfo.au1Descr, 0, CFA_MAX_DESCR_LENGTH);
    MEMCPY (CfaIfInfo.au1Descr, CFA_IF_DESCR (u4IfIndex), CFA_MAX_DESCR_LENGTH);
    CfaSetIfInfo (IF_DESC, u4IfIndex, &CfaIfInfo);

    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    /* create the entry in the GDD table for this interface */
    if (CfaGddInitPort (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitWlanRadioIfEntry - "
                  "GDD entry creation fail for interface %d\n", u4IfIndex);
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    /* we first register this interface with the GDD module immediately */
    /* Gdd Registration is same as CFA_ENET . Purposefully passing type
       as CFA_ENET */
    if (CfaGddRegisterPort (u4IfIndex, CFA_ENET) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitWlanRadioIfEntry - "
                  "Registration fail for interface %d\n", u4IfIndex);
        /* restore the GDD entry */
        CfaGddDeInitPort (u4IfIndex);
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        return (CFA_FAILURE);
    }

    /* get the HW address from the driver - if failure then dont show it */
    if (CfaIfmAddRcvAddrEntry (u4IfIndex, au1HwAddr, CFA_RCVADDR_NVOL)
        != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitWlanRadioIfEntry - "
                  "RcvAddrTab UCAST add fail on interface %d\n", u4IfIndex);
    }

    /* Add Enet Bcast addr also */
    MEMSET (au1HwAddr, 0, CFA_MAX_MEDIA_ADDR_LEN);
    MEMSET (au1HwAddr, 0xff, CFA_ENET_ADDR_LEN);
    if (CfaIfmAddRcvAddrEntry (u4IfIndex, au1HwAddr, CFA_RCVADDR_NVOL)
        != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitWlanRadioIfEntry - "
                  "RcvAddrTab BCAST add fail on interface %d\n", u4IfIndex);
    }

    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_OTHER;
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);

    pHighStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
    pLowStackListScan = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);

    CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
    CFA_IF_STACK_STATUS (pLowStackListScan) = CFA_RS_ACTIVE;

    MEMSET (au1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);

    SPRINTF ((CHR1 *) pu1Alias, "%s%d", CFA_WLAN_RADIO_NAME_PREFIX,
             u4IfIndex - CFA_MIN_WSS_IF_INDEX + 1);
    CfaSetPortName (u4IfIndex, pu1Alias);
    CfaSetIfName (u4IfIndex, pu1Alias);

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmInitWlanRadioIfEntry - "
              "Initialized Radio interface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmInitVirtualRadioIfEntry
 *
 *    Description        : This function allocates an entry for virtual 
 *                         radio interface in the interface table and 
 *                         initialises it to default values.
 *
 *    Input(s)            : UINT4 u4IfIndex, UINT1 *au1PortName
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitVirtualRadioIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{
    UINT1               au1HwAddr[CFA_ENET_ADDR_LEN];
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT1               au1VirtPortName[CFA_MAX_PORT_NAME_LENGTH];

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitVirtualRadioIfEntry for Radio interface %d .\n",
              u4IfIndex);

    MEMSET (au1VirtPortName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1HwAddr, 0, CFA_ENET_ADDR_LEN);
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    /* check if the IfIndex is valid */
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitVirtualRadioIfEntry - "
                  "invalid interface %d.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CfaSetIfType (u4IfIndex, CFA_CAPWAP_VIRT_RADIO);
    CfaSetIfName (u4IfIndex, au1PortName);
    CfaSetPortName (u4IfIndex, au1PortName);
    CfaSetIfNwType (u4IfIndex, CFA_NETWORK_TYPE_LAN);

    STRNCPY (CFA_IF_DESCR (u4IfIndex), CFA_CAPWAP_VIRT_RADIO_DESCR,
             STRLEN (CFA_CAPWAP_VIRT_RADIO_DESCR));
    CFA_IF_DESCR (u4IfIndex)[STRLEN (CFA_CAPWAP_VIRT_RADIO_DESCR)] = '\0';
    CfaGetIfDescr (u4IfIndex, CFA_IF_DESCR (u4IfIndex));

    MEMSET (CfaIfInfo.au1Descr, 0, CFA_MAX_DESCR_LENGTH);
    MEMCPY (CfaIfInfo.au1Descr, CFA_IF_DESCR (u4IfIndex), CFA_MAX_DESCR_LENGTH);
    CfaSetIfInfo (IF_DESC, u4IfIndex, &CfaIfInfo);

    MEMSET (au1HwAddr, 0, CFA_ENET_ADDR_LEN);

    CfaSetIfHwAddr (u4IfIndex, au1HwAddr, CFA_ENET_ADDR_LEN);

    CfaSetIfName (u4IfIndex, au1PortName);
    CfaSetIfType (u4IfIndex, CFA_CAPWAP_VIRT_RADIO);
    STRNCPY (CFA_IF_DESCR (u4IfIndex), CFA_CAPWAP_VIRT_RADIO_DESCR,
             STRLEN (CFA_CAPWAP_VIRT_RADIO_DESCR));
    CFA_IF_DESCR (u4IfIndex)[STRLEN (CFA_CAPWAP_VIRT_RADIO_DESCR)] = '\0';

    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_OTHER;
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);

    pHighStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
    pLowStackListScan = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);
    CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
    CFA_IF_STACK_STATUS (pLowStackListScan) = CFA_RS_ACTIVE;
    ++CFA_IFNUMBER ();

    SPRINTF ((char *) au1VirtPortName, "%s", CFA_CAPWAP_VIRT_RADIO_NAME_PREFIX);
    CfaSetPortName (u4IfIndex, au1VirtPortName);
    CfaSetIfName (u4IfIndex, au1VirtPortName);

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmInitVirtualRadioIfEntry - "
              "Initialized virtual radio interface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmInitWlanProfileIfEntry
 *
 *    Description        : This function allocates an entry for WLAN Profile
 *                         interface in the interface table and initialises 
 *                         it to default values.
 *
 *    Input(s)            : UINT4 u4IfIndex, UINT1 *au1PortName
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitWlanProfileIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{
    UINT1               au1HwAddr[CFA_ENET_ADDR_LEN];
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT1               au1WlanPortName[CFA_MAX_PORT_NAME_LENGTH];

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitWlanProfileIfEntry for WLAN Profile interface %d .\n",
              u4IfIndex);

    MEMSET (au1WlanPortName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1HwAddr, 0, CFA_ENET_ADDR_LEN);
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    /* check if the IfIndex is valid */
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitWlanProfileIfEntry - "
                  "invalid interface %d.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CfaSetIfType (u4IfIndex, CFA_CAPWAP_DOT11_PROFILE);
    CfaSetIfName (u4IfIndex, au1PortName);
    CfaSetPortName (u4IfIndex, au1PortName);
    CfaSetIfNwType (u4IfIndex, CFA_NETWORK_TYPE_LAN);

    STRNCPY (CFA_IF_DESCR (u4IfIndex), CFA_CAPWAP_DOT11_PROFILE_DESCR,
             STRLEN (CFA_CAPWAP_DOT11_PROFILE_DESCR));
    CFA_IF_DESCR (u4IfIndex)[STRLEN (CFA_CAPWAP_DOT11_PROFILE_DESCR)] = '\0';
    CfaGetIfDescr (u4IfIndex, CFA_IF_DESCR (u4IfIndex));

    MEMSET (CfaIfInfo.au1Descr, 0, CFA_MAX_DESCR_LENGTH);
    MEMCPY (CfaIfInfo.au1Descr, CFA_IF_DESCR (u4IfIndex), CFA_MAX_DESCR_LENGTH);
    CfaSetIfInfo (IF_DESC, u4IfIndex, &CfaIfInfo);

    MEMSET (au1HwAddr, 0, CFA_ENET_ADDR_LEN);

    CfaSetIfHwAddr (u4IfIndex, au1HwAddr, CFA_ENET_ADDR_LEN);

    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_OTHER;
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);
    pHighStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
    pLowStackListScan = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);
    CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
    CFA_IF_STACK_STATUS (pLowStackListScan) = CFA_RS_ACTIVE;
    ++CFA_IFNUMBER ();

    SPRINTF ((char *) au1WlanPortName, "%s",
             CFA_CAPWAP_DOT11_PROFILE_NAME_PREFIX);
    CfaSetPortName (u4IfIndex, au1WlanPortName);
    CfaSetIfName (u4IfIndex, au1WlanPortName);
    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmInitWlanProfileIfEntry - "
              "Initialized WLAN Profile interface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmInitWlanBssIfEntry
 *
 *    Description        : This function allocates an entry for WLAN BSS
 *                         interface in the interface table and 
 *                         initialises it to default values.
 *
 *    Input(s)            : UINT4 u4IfIndex, UINT1 *au1PortName
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitWlanBssIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{
    UINT1               au1HwAddr[CFA_ENET_ADDR_LEN];
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT1               au1BssPortName[CFA_MAX_PORT_NAME_LENGTH];

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitWlanBssIfEntry for WLAN BSS interface %d .\n",
              u4IfIndex);

    MEMSET (au1BssPortName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1HwAddr, 0, CFA_ENET_ADDR_LEN);
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    /* check if the IfIndex is valid */
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitWlanProfileIfEntry - "
                  "invalid interface %d.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CfaSetIfType (u4IfIndex, CFA_CAPWAP_DOT11_BSS);
    CfaSetIfName (u4IfIndex, au1PortName);
    /*  Enabling default bridging capability for BSS/VAP interface */
    CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_ENABLED);

    CfaSetPortName (u4IfIndex, au1PortName);
    CfaSetIfNwType (u4IfIndex, CFA_NETWORK_TYPE_LAN);

    STRNCPY (CFA_IF_DESCR (u4IfIndex), CFA_CAPWAP_DOT11_BSS_DESCR,
             STRLEN (CFA_CAPWAP_DOT11_BSS_DESCR));
    CFA_IF_DESCR (u4IfIndex)[STRLEN (CFA_CAPWAP_DOT11_BSS_DESCR)] = '\0';
    CfaGetIfDescr (u4IfIndex, CFA_IF_DESCR (u4IfIndex));
    MEMCPY (CfaIfInfo.au1Descr, CFA_IF_DESCR (u4IfIndex), CFA_MAX_DESCR_LENGTH);
    CfaSetIfInfo (IF_DESC, u4IfIndex, &CfaIfInfo);

    MEMSET (au1HwAddr, 0, CFA_ENET_ADDR_LEN);

    CfaSetIfHwAddr (u4IfIndex, au1HwAddr, CFA_ENET_ADDR_LEN);

    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    if (CfaGddInitPort (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "BSSIFINDEX INIT FAILED for Index :" "%d \n", u4IfIndex);
    }

    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);
    pHighStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
    pLowStackListScan = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);
    CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
    CFA_IF_STACK_STATUS (pLowStackListScan) = CFA_RS_ACTIVE;
    ++CFA_IFNUMBER ();

    SPRINTF ((char *) au1BssPortName, "%s", CFA_CAPWAP_DOT11_BSS_NAME_PREFIX);
    CfaSetPortName (u4IfIndex, au1BssPortName);
    CfaSetIfName (u4IfIndex, au1BssPortName);

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmInitWlanBssIfEntry - "
              "Initialized WLAN BSS interface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmDeleteRadioInterface
 *
 *    Description          : This function is to de-registration of physical
 *                           radio interface
 *
 *    Input(s)            : UINT4 u4IfIndex
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if deletion succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmDeleteRadioInterface (UINT4 u4IfIndex, UINT1 u1DelIfEntry)
{

    UINT1               u1IfType = CFA_NONE;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmDeleteRadioInterface "
              "for Radio interface %d\n", u4IfIndex);

    /* check if the IfIndex is valid */
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitWlanBssIfEntry - "
                  "invalid interface %d.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    /* check whether the entry is to be deleted from IfTable itself */
    if (u1DelIfEntry == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "In CfaIfmDeleteRadioInterface - "
                  "disabling success - interface %d\n", u4IfIndex);

        return (CFA_SUCCESS);
    }

    CfaGddDeRegisterPort (u4IfIndex);
    CfaGddDeInitPort (u4IfIndex);

    CfaGetIfType (u4IfIndex, &u1IfType);

/* delete the interface from the IfTable */
    if (CfaIfmDeleteIfEntry (u4IfIndex, u1IfType) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteRadioInterface - "
                  "ifEntry delete fail for interface %d\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteRadioInterface - "
              "Full Delete success - interface %d\n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmDeleteWssInterface
 *
 *    Description        : This function is to delete the interface for the
 *                         WSS (Virtual Radio, WLAN Profile or BSS) Interface 
 *
 *    Input(s)            : UINT4 u4IfIndex
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if deletion succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmDeleteWssInterface (UINT4 u4IfIndex, UINT1 u1DelIfEntry)
{

    UINT1               u1IfType = CFA_NONE;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmDeleteWssInterface "
              "for Radio interface %d\n", u4IfIndex);

    /* check if the IfIndex is valid */
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitWlanBssIfEntry - "
                  "invalid interface %d.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    /* check whether the entry is to be deleted from IfTable itself */
    if (u1DelIfEntry == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "In CfaIfmDeleteWssInterface - "
                  "disabling success - interface %d\n", u4IfIndex);

        return (CFA_SUCCESS);
    }

    CfaGetIfType (u4IfIndex, &u1IfType);
#if defined (WLC_WANTED) || defined (WTP_WANTED)
    if (u1IfType == CFA_CAPWAP_DOT11_BSS)
    {
        L2IwfPortDeleteIndication (u4IfIndex);
    }
#endif
/* delete the interface from the IfTable */
    if (CfaIfmDeleteIfEntry (u4IfIndex, u1IfType) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteWssInterface - "
                  "ifEntry delete fail for interface %d\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteWssInterface - "
              "Full Delete success - interface %d\n", u4IfIndex);

    return (CFA_SUCCESS);
}

#endif
