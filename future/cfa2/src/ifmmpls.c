/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ifmmpls.c,v 1.58 2016/04/09 09:56:22 siva Exp $
 *
 * Description:This file contains the routines for the     
 *             Interface Management of MPLS interfaces   
 *             of the CFA.                     
 *
 *******************************************************************/

#if defined(MPLS_WANTED) || defined(TLM_WANTED)
#include "cfainc.h"
#endif
#ifdef MPLS_WANTED
#ifdef NPAPI_WANTED
#include "mplsnp.h"
#include "mplsnpwr.h"
#include "ipnp.h"
#endif
/**************************** GLOBAL DEFINITIONS *****************************/
/* the count of the MPLS interfaces in the system */
UINT4               gu4NumofMplsInterface = 0;
UINT4               gu4NumofMplsTnlInterface = 0;

/*****************************************************************************
 *
 *    Function Name        : CfaIfmInitMplsTunnelIfEntry
 *
 *    Description          : This function sets the default values for the 
 *                           MPLS Tunnel Interface
 *
 *    Input(s)             : UINT4 u4IfIndex, UINT1 *au1PortName
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                         CFA_FAILURE otherwise.
 *
 *****************************************************************************/
INT4
CfaIfmInitMplsTunnelIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1              *pu1IfName = NULL;
    UINT4               u4CfaMplsCurrentIndex = 0;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitMplsTunnelIfEntry for Enet interface %d .\n",
              u4IfIndex);

    if (STRCMP (au1PortName, "") != 0)
    {
        pu1IfName = au1PortName;
    }
    else
    {
        pu1IfName = &au1IfName[0];
        MPLS_TNL_IF_COUNT (u4IfIndex, u4CfaMplsCurrentIndex);
        SNPRINTF ((CHR1 *) pu1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s%d",
                  CFA_MPLS_TNL_NAME_PREFIX, u4CfaMplsCurrentIndex);
    }

    CfaSetIfType (u4IfIndex, CFA_MPLS_TUNNEL);

    CfaSetIfName (u4IfIndex, pu1IfName);

    STRNCPY (CFA_IF_DESCR (u4IfIndex), CFA_MPLS_TNL_DESCR,
             sizeof (CFA_MPLS_TNL_DESCR));
    (CFA_IF_DESCR (u4IfIndex))[strlen (CFA_MPLS_TNL_DESCR)] = '\0';

    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_OTHER;
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmInitMplsTunnelIfEntry - "
              "Initialized MPLS Tunnel interface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name      : CfaIfmCreateStackMplsTunnelInterface 
 *
 *    Description        : This function creates the MPLS Tunnel Interface and
 *                         in the ifTable and creates a stack entry for mpls and
 *                         l3ipvlan interface in the stack table and layers 
 *                         mpls interface on the vlan interface
 *
 *    Input(s)           : UINT4 u4StackLowIf - L3IPVLAN or MPLS-Tunnel If
 *
 *    Output(s)          : pu4MplsTnlIfIndex   - IfIndex of created MPLS-TUNNEL.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if interface creation and stacking 
 *                         succeeds 
 *                         CFA_FAILURE otherwise.
 *
 *****************************************************************************/
INT4
CfaIfmCreateStackMplsTunnelInterface (UINT4 u4L3IpIntf,
                                      UINT4 *pu4MplsTnlIfIndex)
{
    INT4                i4RetVal;
    UINT4               u4MplsIfIndex = CFA_NONE;
    UINT4               u4IfIndex = 0;
    tTMO_SLL           *pHighStack = NULL;
    tTMO_SLL           *pLowStack = NULL;
    tTMO_SLL_NODE      *pNode = NULL;

    /*Notes: u4L3IpIntf -> Type Vlan
     *       u4MplsIfIndex -> Type 166 (MPLS)
     *       *pu4MplsTnlIfIndex -> Type 150 (TUNNEL te/non-te)
     */
    CFA_LOCK ();
    if (CfaValidateCfaIfIndex (u4L3IpIntf) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmCreateStackMplsTunnelInterface- "
                  "Invalid Interface Index - FAIL\n", u4L3IpIntf);
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }
    if (CFA_IF_ENTRY (u4L3IpIntf) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmCreateStackMplsTunnelInterface- "
                  "Invalid Interface Index %d - \n", u4L3IpIntf);
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }

    if (CfaGetMplsIfFromIfIndex (u4L3IpIntf, &u4MplsIfIndex) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmCreateStackMplsTunnelInterface- "
                  "Unable to get MPLS If from IfIndex %d\n", u4L3IpIntf);
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }

    if (*pu4MplsTnlIfIndex == 0)
    {
        /* Get the Free Interface Index from the MPLS Tunnel's Pool */
        if (CfaGetFreeInterfaceIndex (&u4IfIndex, CFA_MPLS_TUNNEL) ==
            OSIX_FAILURE)
        {
            CFA_UNLOCK ();
            return (CFA_FAILURE);
        }

        /* Set the default alias name for mpls tunnel i/f */

        if ((i4RetVal =
             CfaIfmCreateDynamicMplsTunnelInterface (u4IfIndex,
                                                     NULL)) == CFA_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                     "Error in CfaIfmCreateDynamicMplsTunnelInterface - "
                     "interface creation - FAILURE.\n");
            CFA_UNLOCK ();
            return (CFA_FAILURE);
        }

    }
    else
    {
        u4IfIndex = *pu4MplsTnlIfIndex;
        CfaSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_DOWN);
    }

    /* Stack MPLS tunnel(150) over MPLSIf(166)  */

    CFA_IF_ENTRY (u4IfIndex)->u4HighIfIndex = u4MplsIfIndex;

    if (CfaIfmAddStackEntry (u4MplsIfIndex, CFA_HIGH, u4IfIndex,
                             CFA_RS_NOTINSERVICE) == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmAddDynamicStackEntry - "
                 "Stack creation - FAILURE.\n");
        if (*pu4MplsTnlIfIndex == 0)
        {
            CfaIfmDeleteDynamicMplsTunnelInterface (u4IfIndex, NULL, FALSE);
        }
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }

    pHighStack = &CFA_IF_STACK_LOW (u4IfIndex);

    pNode = TMO_SLL_First (pHighStack);

    if (pNode != NULL)
    {
        CFA_IF_STACK_STATUS (pNode) = (UINT1) CFA_RS_ACTIVE;
    }

    pLowStack = &CFA_IF_STACK_HIGH (u4IfIndex);

    pNode = TMO_SLL_First (pLowStack);

    if (pNode != NULL)
    {
        CFA_IF_STACK_STATUS (pNode) = (UINT1) CFA_RS_ACTIVE;
    }

    /* now make the row status of mplstunnel as active and make the admin
     * status up */
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_ACTIVE);

    /* In the ifTable of MPLS, store u4LowerIfIndex as the u1HighType */
    /* Added for the refernce of mpls purpose */
    CFA_IF_ENTRY (u4IfIndex)->u4HighIfIndex = u4L3IpIntf;

    if (CfaSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_UP) == CFA_FAILURE)
    {
        CfaIfmDeleteDynamicStackEntry (u4IfIndex, u4MplsIfIndex);
        if (*pu4MplsTnlIfIndex == 0)
        {
            CfaIfmDeleteDynamicMplsTunnelInterface (u4IfIndex, NULL, FALSE);
        }
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }

    *pu4MplsTnlIfIndex = u4IfIndex;
    CFA_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaIfmCreateStackMplsTnlIntfFromTnlIfIndex
 *
 *    Description        : This function creates the MPLS Tunnel Interface and
 *                         in the ifTable and creates a stack entry for mpls and
 *                         l3ipvlan interface in the stack table and layers
 *                         mpls interface on the vlan interface for the given
 *                         MPLS TNL if Index. No new TNL If Index is fetched
 *                         from the CFA Index manager.
 *
 *    Input(s)           : UINT4 u4StackLowIf - L3IPVLAN or MPLS-Tunnel If
                         : UINT4 u4TnlIfIndex - MPLS Tnl If Index
 *
 *    Output(s)          : NONE
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if interface creation and stacking
 *                         succeeds
 *                         CFA_FAILURE otherwise.
 *
 *****************************************************************************/
INT4
CfaIfmCreateStackMplsTnlIntfFromTnlIfIndex (UINT4 u4L3IpIntf,
                                            UINT4 u4TnlIfIndex)
{

    INT4                i4RetVal;
    UINT4               u4MplsIfIndex = CFA_NONE;
    tTMO_SLL           *pHighStack = NULL;
    tTMO_SLL           *pLowStack = NULL;
    tTMO_SLL_NODE      *pNode = NULL;

    /*Notes: u4L3IpIntf -> Type Vlan
     *       u4MplsIfIndex -> Type 166 (MPLS)
     *       *pu4MplsTnlIfIndex -> Type 150 (TUNNEL te/non-te)
     */
    CFA_LOCK ();
    if (CfaValidateCfaIfIndex (u4L3IpIntf) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmCreateStackMplsTnlIntfFromTnlIfIndex- "
                  "Invalid Interface Index - FAIL\n", u4L3IpIntf);
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }
    if (CFA_IF_ENTRY (u4L3IpIntf) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmCreateStackMplsTnlIntfFromTnlIfIndex- "
                  "Invalid Interface Index %d - \n", u4L3IpIntf);
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }

    if (CfaGetMplsIfFromIfIndex (u4L3IpIntf, &u4MplsIfIndex) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmCreateStackMplsTnlIntfFromTnlIfIndex- "
                  "Unable to get MPLS If from IfIndex %d\n", u4L3IpIntf);
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }

    if ((i4RetVal =
         CfaIfmCreateDynamicMplsTunnelInterface (u4TnlIfIndex,
                                                 NULL)) == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmCreateStackMplsTnlIntfFromTnlIfIndex- "
                 "interface creation - FAILURE.\n");
        CFA_UNLOCK ();
        return (CFA_FAILURE);
    }

    /* Stack MPLS tunnel(150) over MPLSIf(166)  */

    CFA_IF_ENTRY (u4TnlIfIndex)->u4HighIfIndex = u4MplsIfIndex;

    if (CfaIfmAddStackEntry (u4MplsIfIndex, CFA_HIGH, u4TnlIfIndex,
                             CFA_RS_NOTINSERVICE) == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmAddDynamicStackEntry - "
                 "Stack creation - FAILURE.\n");
        CfaIfmDeleteDynamicMplsTunnelInterface (u4TnlIfIndex, NULL, FALSE);
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }

    pHighStack = &CFA_IF_STACK_LOW (u4TnlIfIndex);

    pNode = TMO_SLL_First (pHighStack);

    if (pNode != NULL)
    {
        CFA_IF_STACK_STATUS (pNode) = (UINT1) CFA_RS_ACTIVE;
    }

    pLowStack = &CFA_IF_STACK_HIGH (u4TnlIfIndex);

    pNode = TMO_SLL_First (pLowStack);

    if (pNode != NULL)
    {
        CFA_IF_STACK_STATUS (pNode) = (UINT1) CFA_RS_ACTIVE;
    }
    /* now make the row status of mplstunnel as active and make the admin
     * status up */
    CfaSetCdbRowStatus (u4TnlIfIndex, CFA_RS_ACTIVE);

    /* In the ifTable of MPLS, store u4LowerIfIndex as the u1HighType */
    /* Added for the refernce of mpls purpose */
    CFA_IF_ENTRY (u4TnlIfIndex)->u4HighIfIndex = u4L3IpIntf;

    if (CfaSetIfMainAdminStatus ((INT4) u4TnlIfIndex, CFA_IF_UP) == CFA_FAILURE)
    {
        CfaIfmDeleteDynamicStackEntry (u4TnlIfIndex, u4MplsIfIndex);
        CfaIfmDeleteDynamicMplsTunnelInterface (u4TnlIfIndex, NULL, FALSE);
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }

    CFA_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaIfmDeleteStackMplsTunnelInterface 
 *
 *    Description        : This function deletes the MPLS Tunnel Interface 
 *
 *    Input(s)           : UINT4 u4StackLowIf - L3IPVLAN or MPLS-Tunnel If
 *
 *                       : u4MplsTnlIfIndex   - IfIndex of created MPLS-TUNNEL.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : CFA_SUCCESS if interface stacking is removed
 *                              and the interface is deleted
 *                              succeeds 
 *                              CFA_FAILURE otherwise.
 *
 *****************************************************************************/
INT4
CfaIfmDeleteStackMplsTunnelInterface (UINT4 u4L3IpIntf, UINT4 u4MplsTnlIfIndex)
{
    INT4                i4IfMainStorageType;
    UINT4               u4MplsIfIndex = CFA_NONE;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    /*Notes: u4L3IpIntf -> Type Vlan
     *       u4MplsIfIndex -> Type 166 (MPLS)
     *       *pu4MplsTnlIfIndex -> Type 150 (TUNNEL te/non-te)
     */

    CFA_LOCK ();
    if (CfaValidateCfaIfIndex (u4L3IpIntf) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmCreateStackMplsTunnelInterface- "
                  "Invalid Interface Index - FAIL\n", u4L3IpIntf);
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }
    if (CFA_IF_ENTRY (u4L3IpIntf) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteStackMplsTunnelInterface- "
                  "Invalid Interface Index %d - \n", u4L3IpIntf);
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }

    if (CfaGetMplsIfFromIfIndex (u4L3IpIntf, &u4MplsIfIndex) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteStackMplsTunnelInterface- "
                  "Unable to get MPLS If from IfIndex %d\n", u4L3IpIntf);
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }

    /* Remove Stacking MPLS tunnel(150) over MPLSIf(166)  */
    if (CfaIfmDeleteDynamicStackEntry (u4MplsTnlIfIndex, u4MplsIfIndex)
        == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmDeleteDynamicStackEntry -"
                 "Stack deletion - FAILURE.\n");
        CFA_UNLOCK ();
        return (CFA_FAILURE);
    }

    CfaGetIfMainStorageType (u4MplsTnlIfIndex, &i4IfMainStorageType);
    if (i4IfMainStorageType == CFA_STORAGE_TYPE_VOLATILE)
    {
        /* Remove the Tunnel interface over MPLSIf(166) */
        if (CfaIfmDeleteDynamicMplsTunnelInterface
            (u4MplsTnlIfIndex, au1IfName, FALSE) == CFA_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                     "Error in CfaIfmCreateDynamicMplsTunnelInterface - "
                     "interface creation - FAILURE.\n");
            CFA_UNLOCK ();
            return (CFA_FAILURE);
        }
    }
    else
    {
        CFA_IF_RS (u4MplsTnlIfIndex) = CFA_RS_ACTIVE;
        CfaSetCdbRowStatus (u4MplsTnlIfIndex, CFA_RS_ACTIVE);
        CfaSetIfActiveStatus (u4MplsTnlIfIndex, CFA_TRUE);
    }

    /* After deleting the Mpls Stack Tunnel Interface ,the HighIfindex is not cleared *
     * and therefore while creation of the MPLS Stack interface in dynamic scenario , *
     * it is assumed that the stacking is already present as both the existing *
     * HighIfIndex and the current L3index would be the same.This is incorrect as the stacking is not actually present *
     * Therefore ,the stack is not created in shut /no shut scenario and hence the tunnel and PW above this is down */

    if (CFA_IF_ENTRY (u4MplsTnlIfIndex) != NULL)
    {
        CFA_IF_ENTRY (u4MplsTnlIfIndex)->u4HighIfIndex = 0;
    }

    CFA_UNLOCK ();
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmCreateDynamicMplsTunnelInterface
 *
 *    Description          : This function creates the MPLS Tunnel IfEntry.
 *
 *    Input(s)             : UINT4 u4IfIndex
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmCreateDynamicMplsTunnelInterface (UINT4 u4IfIndex, UINT1 *pu1IfName)
{
    UINT1               au1NullString[2] = "";
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    UNUSED_PARAM (pu1IfName);

    if (CfaIfmCreateAndInitIfEntry (u4IfIndex, CFA_INVALID_TYPE,
                                    au1NullString) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }

    CfaGetIfAlias (u4IfIndex, au1IfName);

    CfaIfmInitMplsTunnelIfEntry (u4IfIndex, au1IfName);
    /* Here set the storage type as VOLATILE */
    CfaSetIfMainStorageType (u4IfIndex, CFA_STORAGE_TYPE_VOLATILE);

    CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
    CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmDeleteDynamicMplsTunnelInterface
 *
 *    Description          : This function  deletes MPLS Tunnel IfEntry.
 *
 *    Input(s)             : UINT4 u4IfIndex
 *                           UINT1 pu1IfName - Interface Name
 *                           bLockReq - Lock requested If 
 *                                      TRUE - Needs to take CFA_LOCK ()
 *                                      FALSE - Need not take CFA_LOCK ()
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if creation succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmDeleteDynamicMplsTunnelInterface (UINT4 u4IfIndex, UINT1 *pu1IfName,
                                        BOOL1 bLockReq)
{
    UNUSED_PARAM (pu1IfName);

    CFA_FLAG_LOCK (bLockReq);

    if (CfaIfmDeleteMplsInterface (u4IfIndex, CFA_TRUE) == CFA_FAILURE)
    {
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    CFA_FLAG_UNLOCK (bLockReq);
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmDeleteMplsInterface
 *
 *    Description        : This function is for for deleting the MPLS
 *                         interface in the ifTable
 *
 *    Input(s)            : UINT4 u4IfIndex,UINT1 u1DelIfEntry
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if deletion succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmDeleteMplsInterface (UINT4 u4IfIndex, UINT1 u1DelIfEntry)
{

    UINT1               u1IfType = CFA_NONE;
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmDeleteMplsInterface for IPOA interface %d\n",
              u4IfIndex);

    if (CfaValidateCfaIfIndex (u4IfIndex) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteMplsInterface- "
                  "Invalid Interface Index - FAIL\n", u4IfIndex);

        return CFA_FAILURE;
    }

    if (CFA_IF_ENTRY (u4IfIndex) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteMplsInterface- "
                  "Invalid Interface Index %d - \n", u4IfIndex);
        return CFA_FAILURE;
    }
    /* we de-register all the lower interfaces and delete the stack entries
       for lower layers */
    /* check whether the entry is to be deleted from IfTable itself */

    CFA_IF_ADMIN (u4IfIndex) = CFA_IF_DOWN;

    pHighStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY ((UINT2) u4IfIndex);
    CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);

    pLowStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY ((UINT2) u4IfIndex);
    CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);

    if (CFA_IF_STACK_STATUS (pHighStackListScan) == CFA_RS_ACTIVE)
    {
        while (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex))
        {
            pHighStackListScan =
                (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY ((UINT2)
                                                              u4IfIndex);

            CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
            if (CFA_IF_STACK_IFINDEX (pHighStackListScan) != CFA_NONE)
            {
                /* First Delete this entry from Higher layer's stack value. */
                CfaIfmDeleteStackEntry
                    (CFA_IF_STACK_IFINDEX (pHighStackListScan),
                     CFA_LOW, u4IfIndex);
                CfaIfmDeleteStackEntry (u4IfIndex, CFA_HIGH,
                                        CFA_IF_STACK_IFINDEX
                                        (pHighStackListScan));
            }
            else
            {
                CfaIfmUpdateStackForDeletion (&CFA_IF_STACK_HIGH (u4IfIndex),
                                              CFA_FALSE, CFA_NONE);
            }
        }
    }

    while (CFA_IF_STACK_LOW_ENTRY (u4IfIndex))
    {
        pLowStackListScan =
            (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY ((UINT2) u4IfIndex);

        CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);
        if (CFA_IF_STACK_IFINDEX (pLowStackListScan) != CFA_NONE)
        {
            /* First Delete this entry from the lower layer's stack value. */
            CfaIfmDeleteStackEntry
                (u4IfIndex, CFA_LOW, CFA_IF_STACK_IFINDEX (pLowStackListScan));
        }
        else
        {
            CfaIfmUpdateStackForDeletion (&CFA_IF_STACK_LOW (u4IfIndex),
                                          CFA_FALSE, CFA_NONE);
        }
    }

    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
              "In CfaIfmDeInitTunlInterface - "
              "Un-reg with HL success - interface %d\n", u4IfIndex);

    /* check whether the entry is to be deleted from IfTable itself */
    if (u1DelIfEntry == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "In CfaIfmDeInitTunlInterface - "
                  "disabling success - interface %d\n", u4IfIndex);

        return (CFA_SUCCESS);
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

    /* Intimate higher layers */
    if (u1IfType == CFA_MPLS)
    {
        /* TODO intimate to Mpls Tunnel */
    }

    if (u1DelIfEntry != CFA_TRUE)
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "In CfaIfmDeleteMplsInterface - "
                  "disabling success - interface %d\n", u4IfIndex);

        return (CFA_SUCCESS);
    }

    /* delete the interface from the IfTable */
    if (CfaIfmDeleteIfEntry (u4IfIndex, CFA_MPLS) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteMplsInterface - "
                  "ifEntry delete fail for interface %d\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteMplsInterface - "
              "Full Delete success - interface %d\n", u4IfIndex);

    return (CFA_SUCCESS);
}
#ifdef MPLS_WANTED
#ifdef NPAPI_WANTED
INT4
CfaIfmConfigL3MplsTunnelInterface (UINT4 u4IfIndex, INT4 ifStatus,
                                   BOOL1 bLockReq)
{
    UINT2               u2VlanId = 0;
    UINT4               u4L3VlanIf = 0;
    UINT1               au1MacAddress[MAC_ADDR_LEN];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4RetVal;
    tMplsNpWrNpMplsCreateMplsInterface MplsCreateMplsInt;
    tMplsHwMplsIntInfo   MplsHwMplsIntInfo;
    MEMSET (&MplsCreateMplsInt, 0, sizeof(tMplsNpWrNpMplsCreateMplsInterface));
    MEMSET (&MplsHwMplsIntInfo,0,sizeof(tMplsHwMplsIntInfo));
   
    CFA_FLAG_LOCK (bLockReq);
    /* Get L3Vlan interface from Mpls Tunnel Interface */
    if (CfaUtilGetIfIndexFromMplsTnlIf (u4IfIndex, &u4L3VlanIf, FALSE) ==
        CFA_FAILURE)
    {
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }
    /* Lower layer is not present do not create / delete MPLS Tunnel interface
     * in Hardware. */
    if (u4L3VlanIf == 0)
    {
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_SUCCESS;
    }

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    CfaGetIfName (u4IfIndex, au1IfName);

    CfaGetIfIvrVlanId (u4L3VlanIf, &u2VlanId);
    CfaGetIfHwAddr (u4L3VlanIf, au1MacAddress);

    MplsHwMplsIntInfo.u4CfaIfIndex = u4IfIndex;
    MplsHwMplsIntInfo.u2VlanId = u2VlanId;
    MplsHwMplsIntInfo.u4L3Intf = u4L3VlanIf;
    MEMCPY(MplsHwMplsIntInfo.au1MacAddress,au1MacAddress,MAC_ADDR_LEN);

    MplsCreateMplsInt.pMplsHwMplsIntInfo=&MplsHwMplsIntInfo;


    if (ifStatus != CFA_IF_DOWN)
    {
        i4RetVal = MplsNpMplsCreateMplsInterface (MplsCreateMplsInt.pMplsHwMplsIntInfo);
    }
    else
    {
        i4RetVal = CfaFsNpIpv4DeleteIpInterface (IP_VRID, au1IfName,
                                                 u4IfIndex, u2VlanId);
    }

    if (i4RetVal == FNP_FAILURE)
    {
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }
    CFA_FLAG_UNLOCK (bLockReq);
    return CFA_SUCCESS;
}
#endif /* NPAPI_WANTED */
#endif
/*****************************************************************************
 *
 *    Function Name        : CfaUtilGetIfIndexFromMplsTnlIf 
 *
 *    Description          : Geting L3IPVLAN on which MPLS TUNNEL 
 *                           interface is stacked over
 *
 *    Input(s)             : u2TnlIfIndex - MPLS Tunnel Interface(150) 
 *                           bLockReq - Lock requested - 
 *                           TRUE - Needs to take CFA_LOCK ()
 *                           FALSE - Need not take CFA_LOCK ()
 *
 *    Output(s)            : pu4L3VlanIf - L3IPVLAN Interface 
 *
 *    Returns              : CFA_SUCCESS if succeeds, 
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaUtilGetIfIndexFromMplsTnlIf (UINT4 u4TnlIfIndex, UINT4 *pu4L3VlanIf,
                                BOOL1 bLockReq)
{
    tCfaIfInfo          IfInfo;

    *pu4L3VlanIf = CFA_NONE;

    CFA_FLAG_LOCK (bLockReq);
    if (CfaValidateCfaIfIndex (u4TnlIfIndex) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaUtilGetIfIndexFromMplsTnlIf- "
                  "Invalid Interface Index - FAIL\n", u4TnlIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }
    if (CFA_IF_ENTRY (u4TnlIfIndex) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaUtilGetIfIndexFromMplsTnlIf- "
                  "Invalid Interface Index %d - \n", u4TnlIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CfaGetIfInfo (u4TnlIfIndex, &IfInfo) == CFA_SUCCESS)
    {
        if (IfInfo.u1IfType == CFA_MPLS_TUNNEL)
        {
            *pu4L3VlanIf = CFA_IF_ENTRY (u4TnlIfIndex)->u4HighIfIndex;
            CFA_FLAG_UNLOCK (bLockReq);
            return CFA_SUCCESS;
        }
    }
    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
              "Error in CfaUtilGetIfIndexFromMplsTnlIf- "
              "Invalid Interface Type - FAIL\n", u4TnlIfIndex);
    CFA_FLAG_UNLOCK (bLockReq);
    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name        : CfaUtilGetMplsIfFromIfIndex 
 *
 *    Description          : Geting MPLS interface which is stacked over
 *                           L3IPVLAN interface
 *
 *    Input(s)             : u4L3IpIntf - L3IPVLAN Interface
 *                           bLockReq - Lock requested - 
 *                           TRUE - Needs to take CFA_LOCK ()
 *                           FALSE - Need not take CFA_LOCK ()
 *
 *    Output(s)            : pu4MplsIfIndex - MPLS Interface (166) 
 *
 *    Returns              : CFA_SUCCESS if succeeds, 
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaUtilGetMplsIfFromIfIndex (UINT4 u4L3IpIntf, UINT4 *pu4MplsIfIndex,
                             BOOL1 bLockReq)
{
    *pu4MplsIfIndex = CFA_NONE;

    CFA_FLAG_LOCK (bLockReq);
    if (CfaValidateCfaIfIndex (u4L3IpIntf) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaUtilGetMplsIfFromIfIndex- "
                  "Invalid Interface Index - FAIL\n", u4L3IpIntf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    /* Although Virtual Instance Ports are IfIndices, no entries are allocated  
     * in gapIfTable for them, hence if the port is VIP or SISP logical 
     * interfaces then return FAILURE 
     */

    if (CfaIsIfEntryProgrammingAllowed (u4L3IpIntf) == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "\r\nError in CfaUtilGetMplsIfFromIfIndex- "
                  "No MPLS is layered over Virtual Instance Port and SISP "
                  "Logical Interface Indices - FAIL\r\n", u4L3IpIntf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }
    if (CFA_IF_ENTRY (u4L3IpIntf) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaUtilGetMplsIfFromIfIndex- "
                  "Invalid Interface Index - FAIL\n", u4L3IpIntf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CfaGetMplsIfFromIfIndex (u4L3IpIntf, pu4MplsIfIndex) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaUtilGetMplsIfFromIfIndex- "
                  "Invalid Interface Index - FAIL\n", u4L3IpIntf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }
    CFA_FLAG_UNLOCK (bLockReq);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *  
 *    Function Name        : CfaUtilGetMplsTnlIfFromMplsIfIndex
 *    
 *    Description          : Geting MPLS interface which is stacked over
 *                           L3IPVLAN interface
 *     
 *    Input(s)             : u4MplsL3IpIntf - MplsL3IP Interface
 *                           bLockReq - Lock requested -
 *                           TRUE - Needs to take CFA_LOCK ()
 *                           FALSE - Need not take CFA_LOCK ()
 *        
 *    Output(s)            : pu4MplsIfIndex - MPLS Interface (166)
 *   
 *    Returns              : CFA_SUCCESS if succeeds,
 *                           otherwise CFA_FAILURE.
 ******************************************************************************/
INT4
CfaUtilGetMplsTnlIfFromMplsIfIndex (UINT4 u4MplsL3IpIntf, UINT4 *pu4MplsIfIndex,
                                    BOOL1 bLockReq)
{
    *pu4MplsIfIndex = CFA_NONE;

    CFA_FLAG_LOCK (bLockReq);
    if (CfaValidateCfaIfIndex (u4MplsL3IpIntf) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaUtilGetMplsTnlIfFromMplsIfIndex- "
                  "Invalid Interface Index - FAIL\n", u4MplsL3IpIntf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    /* Although Virtual Instance Ports are IfIndices, no entries are allocated
     *      * in gapIfTable for them, hence if the port is VIP or SISP logical
     *           * interfaces then return FAILURE
     *                */

    if (CfaIsIfEntryProgrammingAllowed (u4MplsL3IpIntf) == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "\r\nError in CfaUtilGetMplsTnlIfFromMplsIfIndex- "
                  "No MPLS is layered over Virtual Instance Port and SISP "
                  "Logical Interface Indices - FAIL\r\n", u4MplsL3IpIntf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }
    if (CFA_IF_ENTRY (u4MplsL3IpIntf) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaUtilGetMplsTnlIfFromMplsIfIndex- "
                  "Invalid Interface Index - FAIL\n", u4MplsL3IpIntf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }
    if (CfaGetMplsTnlIfFromMplsIfIndex (u4MplsL3IpIntf, pu4MplsIfIndex) ==
        CFA_FAILURE)

    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaUtilGetMplsTnlIfFromMplsIfIndex- "
                  "Invalid Interface Index - FAIL\n", u4MplsL3IpIntf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }
    CFA_FLAG_UNLOCK (bLockReq);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaUtilL3IfStatusHandleForMplsIf
 *
 *    Description          : Geting L3IPVLAN on which MPLS interface is
 *                           stacked over
 *
 *    Input(s)             : u4MplsIfIndex - MPLS Interface (166)
 *                           u1OperStatus - Operational status
 *    Output(s)            : 
 *
 *    Returns              : CFA_SUCCESS if succeeds,
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaUtilL3IfStatusHandleForMplsIf (UINT4 u4MplsIfIndex, UINT1 u1OperStatus)
{
    UINT1               u1IfType = CFA_NONE;
    tTMO_SLL           *pIfStack = NULL;
    tStackInfoStruct   *pScanNode = NULL;
    UINT1               u1IsFromMib = CFA_FALSE;

    if (CFA_IF_ENTRY (u4MplsIfIndex) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaUtilL3IfStatusHandleForMplsIf "
                  "Invalid Interface Index %d - \n", u4MplsIfIndex);
        return CFA_FAILURE;
    }
    pIfStack = &CFA_IF_STACK_HIGH (u4MplsIfIndex);
    TMO_SLL_Scan (pIfStack, pScanNode, tStackInfoStruct *)
    {
        if ((CfaValidateCfaIfIndex (pScanNode->u4IfIndex) == CFA_FAILURE) ||
            ((pScanNode->u4IfIndex) > SYS_DEF_MAX_INTERFACES))
        {
            continue;
        }
        CfaGetIfType (pScanNode->u4IfIndex, &u1IfType);

        /* Fetch the Higher Layer type of MPLS_TUNNEL */
        if (u1IfType != CFA_MPLS_TUNNEL)
        {
            continue;
        }

        /* Check if the Interface Entry is present in gapIfTable */
        if (CFA_IF_ENTRY (pScanNode->u4IfIndex) == NULL)
        {
            continue;
        }
        CfaIfmHandleInterfaceStatusChange (pScanNode->u4IfIndex,
                                           CFA_IF_ADMIN (pScanNode->u4IfIndex),
                                           u1OperStatus, u1IsFromMib);
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGetMplsIfFromIfIndex
 *
 *    Description          : Getting MPLS interface on which the given L3
 *                           interface is stacked over.
 *                           This is not an API and it should not be invoked
 *                           directly from the external modules.
 *
 *    Input(s)             : u4L3If - L3 Interface
 *
 *    Output(s)            : pu4MplsIf - MPLS interface
 *
 *    Returns              : CFA_SUCCESS if succeeds,
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGetMplsIfFromIfIndex (UINT4 u4L3If, UINT4 *pu4MplsIf)
{
    UINT1               u1IfType = CFA_ENET_UNKNOWN;
    UINT4               u4TempIf = CFA_NONE;

    *pu4MplsIf = CFA_NONE;

    /* MPLS Interface can be stacked over 
     * 1. L3 Interface
     * 2. One level of TE Link Interface
     * 3. Two levels of TE Link Interface
     */

    /* Get Higher Layer of L3 Interface */
    if (CfaGetHLIfFromLLIf (u4L3If, &u4TempIf, &u1IfType) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }

    /* If Higher Layer is CFA_MPLS, return */
    if (u1IfType == CFA_MPLS)
    {
        *pu4MplsIf = u4TempIf;
        return CFA_SUCCESS;
    }

    /* If it is CFA_TELINK, go one more layer higher */
    if (u1IfType == CFA_TELINK)
    {
        if (CfaGetHLIfFromLLIf (u4TempIf, &u4TempIf, &u1IfType) == CFA_FAILURE)
        {
            return CFA_FAILURE;
        }

        /* If Higher Layer is CFA_MPLS, return */
        if (u1IfType == CFA_MPLS)
        {
            *pu4MplsIf = u4TempIf;
            return CFA_SUCCESS;
        }

        /* If it is CFA_TELINK again, go one more layer higher */
        if (u1IfType == CFA_TELINK)
        {
            if (CfaGetHLIfFromLLIf (u4TempIf, &u4TempIf, &u1IfType)
                == CFA_FAILURE)
            {
                return CFA_FAILURE;
            }

            /* If Higher Layer is CFA_MPLS, return. */
            if (u1IfType == CFA_MPLS)
            {
                *pu4MplsIf = u4TempIf;
                return CFA_SUCCESS;
            }
        }
    }

    return CFA_FAILURE;
}

/*****************************************************************************
 *  
 *  Function Name        : CfaGetMplsTnlIfFromMplsIfIndex
 *
 *
 *  Description          : Getting MPLS Tunnel interface on which the given L3
 *                         interface is stacked over.
 *                         This is not an API and it should not be invoked
 *                         directly from the external modules.
 *      
 *  Input(s)             :  u4MplsL3If - L3 Interface
 *      
 *  Output(s)            : pu4MplsInterIf - MPLS interface
 *            
 *  Returns              : CFA_SUCCESS if succeeds,
 *                          otherwise CFA_FAILURE.
  *****************************************************************************/
INT4
CfaGetMplsTnlIfFromMplsIfIndex (UINT4 u4MplsL3If, UINT4 *pu4MplsInterIf)
{
    UINT1               u1MplsIfType = CFA_ENET_UNKNOWN;
    UINT4               u4MplsTempIf = CFA_NONE;

    *pu4MplsInterIf = CFA_NONE;

    /*   MPLS Interface can be stacked over
     *   1. L3 Interface     */

    /* Get Higher Layer of L3 Mpls Interface */
    if (CfaGetHLIfFromLLIf (u4MplsL3If, &u4MplsTempIf, &u1MplsIfType) ==
        CFA_FAILURE)
    {
        return CFA_FAILURE;
    }

    /* If Higher Layer is CFA_MPLS_TUNNEL, return */
    if (u1MplsIfType == CFA_MPLS_TUNNEL)
    {
        *pu4MplsInterIf = u4MplsTempIf;
        return CFA_SUCCESS;
    }

    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGetTeLinkIfFromMplsIf
 *
 *    Description          : Getting TE Link interface on 
 *                           which the given MPLS interface is stacked over.
 *                           This is not an API and it should not be invoked
 *                           directly from the external modules
 *
 *                           This function only returns the topmost (bundled)
 *                           TE Link Interface. It does not return the
 *                           underlying individual component TE Links.
 *
 *    Input(s)             : u4MplsIfIndex - MPLS Interface (166)
 *                           TRUE - Needs to take CFA_LOCK ()
 *                           FALSE - Need not take CFA_LOCK ()
 *
 *    Output(s)            : pu4TeLinkIf - TE Link interface
 *
 *    Returns              : CFA_SUCCESS if succeeds,
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGetTeLinkIfFromMplsIf (UINT4 u4MplsIfIndex, UINT4 *pu4TeLinkIf)
{
    UINT1               u1IfType = CFA_ENET_UNKNOWN;
    tTMO_SLL           *pIfStack = NULL;
    tStackInfoStruct   *pScanNode = NULL;

    *pu4TeLinkIf = CFA_NONE;

    pIfStack = &CFA_IF_STACK_LOW (u4MplsIfIndex);

    TMO_SLL_Scan (pIfStack, pScanNode, tStackInfoStruct *)
    {
        if (CfaValidateCfaIfIndex (pScanNode->u4IfIndex) == CFA_SUCCESS)
        {
            CfaGetIfType (pScanNode->u4IfIndex, &u1IfType);

            /* Fetch the Lower Layer type of TE Link */
            if (u1IfType == CFA_TELINK)
            {
                *pu4TeLinkIf = pScanNode->u4IfIndex;
                break;
            }
        }
    }

    if (*pu4TeLinkIf == CFA_NONE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaGetTeLinkIfFromMplsIf - "
                  "No TE Link interface is layered"
                  "below this MPLS interface" "Index - %d\r\n", u4MplsIfIndex);
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGetLLIfOperStatusFromMplsIf
 *
 *    Description          : This function scans the underlying interfaces.
 *                           If underlying interface is L3 If, or TE Link,
 *                           returns its Oper Status.
 *
 *    Input(s)             : u4MplsIf - MPLS Interface (166)
 *
 *    Output(s)            : None
 *
 *    Returns              : CFA_IF_UP or CFA_IF_DOWN
 *****************************************************************************/
UINT1
CfaGetLLIfOperStatusFromMplsIf (UINT4 u4MplsIf)
{
    UINT1               u1IfType = CFA_ENET_UNKNOWN;
    UINT1               u1BridgedIfaceStatus = 0;
    tTMO_SLL           *pIfStack = NULL;
    tStackInfoStruct   *pScanNode = NULL;
    UINT1               u1OperStatus = CFA_IF_DOWN;

    pIfStack = &CFA_IF_STACK_LOW (u4MplsIf);

    TMO_SLL_Scan (pIfStack, pScanNode, tStackInfoStruct *)
    {
        if (CfaValidateCfaIfIndex (pScanNode->u4IfIndex) == CFA_SUCCESS)
        {
            CfaGetIfType (pScanNode->u4IfIndex, &u1IfType);
            CfaGetIfBridgedIfaceStatus (pScanNode->u4IfIndex,
                                        &u1BridgedIfaceStatus);

            if ((u1IfType == CFA_L3IPVLAN) ||
                ((u1IfType == CFA_ENET) &&
                 (u1BridgedIfaceStatus == CFA_DISABLED)) ||
                (u1IfType == CFA_TELINK))
            {
                CfaGetIfOperStatus (pScanNode->u4IfIndex, &u1OperStatus);
                break;
            }
        }
    }

    return u1OperStatus;
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmSetIfSpeed
 *
 *    Description        : This function sets the If Speed for Mpls Tunnel
 *                         interface
 *
 *    Input(s)            : UINT4 u4IfIndex - Interface Index, 
 *                          UINT4 u4Speed - Intercace Speed
 *                          UINT1 u1Flag - Determines ifSpeed or ifHighSpeed to
 *                          be filled
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS / CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmSetIfSpeed (UINT4 u4IfIndex, UINT4 u4Speed, UINT1 u1Flag)
{
    CFA_LOCK ();

    if (u1Flag == CFA_TRUE)
    {
        if (CfaSetIfSpeed (u4IfIndex, u4Speed) == CFA_FAILURE)
        {
            CFA_UNLOCK ();
            return CFA_FAILURE;
        }
    }
    else
    {
        if (CfaSetIfSpeed (u4IfIndex, CFA_ENET_MAX_SPEED_VALUE) == CFA_FAILURE)
        {
            CFA_UNLOCK ();
            return CFA_FAILURE;
        }
        if (CfaSetIfHighSpeed (u4IfIndex, u4Speed) == CFA_FAILURE)
        {
            CFA_UNLOCK ();
            return CFA_FAILURE;
        }
    }

    CFA_UNLOCK ();

    return CFA_SUCCESS;
}

#endif /* MPLS_WANTED */

#if defined(MPLS_WANTED) || defined(TLM_WANTED)

/*****************************************************************************
 *
 *    Function Name        : CfaIfmInitMplsIfEntry
 *
 *    Description        : This function sets the default values for the 
 *                         MPLS interface
 *
 *    Input(s)            : UINT4 u4IfIndex, UINT1 *au1PortName
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitMplsIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4CfaMplsCurrentIndex = 0;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitMplsIfEntry for Mpls interface %d .\n",
              u4IfIndex);

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    CfaSetIfType (u4IfIndex, CFA_MPLS);

    /* Set the default alias name only if the alias is not set */
    if (*au1PortName == 0)
    {
        MPLS_IF_COUNT (u4IfIndex, u4CfaMplsCurrentIndex);
        SNPRINTF ((CHR1 *) au1PortName, CFA_MAX_PORT_NAME_LENGTH, "%s%d",
                  CFA_MPLS_NAME_PREFIX, u4CfaMplsCurrentIndex);
    }
    CfaSetIfName (u4IfIndex, au1PortName);

    STRNCPY (CFA_IF_DESCR (u4IfIndex), CFA_MPLS_DESCR, sizeof (CFA_MPLS_DESCR));
    (CFA_IF_DESCR (u4IfIndex))[strlen (CFA_MPLS_DESCR)] = '\0';

    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_OTHER;

    /* Assign the RS are NotReady because for this mpls interface to operate
     * successfully we need this to be stacked over an L3(IPVLAN) interface.
     * This interface would be made NotInService once the stacking is 
     * successful
     */
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmInitMplsIfEntry - "
              "Initialised MPLS interface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}

INT4
CfaIfmDeleteDynamicStackEntry (UINT4 u4IfStackHigherLayer,
                               UINT4 u4IfStackLowerLayer)
{
    /* To delete an interface the admin status has to be down */
    /* low level routine check for interface destroy */
    if (CfaSetIfMainAdminStatus ((INT4) u4IfStackHigherLayer,
                                 CFA_IF_DOWN) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }

    CfaIfmDeleteStackEntry (u4IfStackLowerLayer, CFA_HIGH,
                            u4IfStackHigherLayer);

    CFA_IF_RS (u4IfStackHigherLayer) = CFA_RS_NOTREADY;
    CfaSetCdbRowStatus (u4IfStackHigherLayer, CFA_RS_NOTREADY);
    CfaSetIfActiveStatus (u4IfStackHigherLayer, CFA_FALSE);

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaUtilGetMplsIfFromMplsTnlIf 
 *
 *    Description          : Getting MPLS If on which MPLS Tnl interface is
 *                           stacked over
 *
 *    Input(s)             : u4MplsTnlIfIndex - MPLS Tnl Interface (150) 
 *                           bLockReq - Lock requested - 
 *                           TRUE - Needs to take CFA_LOCK ()
 *                           FALSE - Need not take CFA_LOCK ()
 *
 *    Output(s)            : pu4MplsIfIndex - MPLS Interface (166) 
 *
 *    Returns              : CFA_SUCCESS if succeeds, 
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaUtilGetMplsIfFromMplsTnlIf (UINT4 u4MplsTnlIfIndex, UINT4 *pu4MplsIfIndex,
                               BOOL1 bLockReq)
{
    tCfaIfInfo          IfInfo;

    *pu4MplsIfIndex = CFA_NONE;
    MEMSET (&IfInfo, CFA_NONE, sizeof (tCfaIfInfo));

    CFA_FLAG_LOCK (bLockReq);
    if (CfaValidateCfaIfIndex (u4MplsTnlIfIndex) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaUtilGetMplsIfFromMplsTnlIf "
                  "Invalid Interface Index - %d\n", u4MplsTnlIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }
    if (CfaGetIfInfo (u4MplsTnlIfIndex, &IfInfo) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaUtilGetMplsIfFromMplsTnlIf "
                  "Fetching IfInfo failed for IfIndex - %d\n",
                  u4MplsTnlIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;

    }
    if (IfInfo.u1IfType != CFA_MPLS_TUNNEL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaUtilGetMplsIfFromMplsTnlIf "
                  "Invalid interface type for IfIndex - %d\n",
                  u4MplsTnlIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;

    }
    if (CFA_IF_ENTRY (u4MplsTnlIfIndex) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaUtilGetMplsIfFromMplsTnlIf "
                  "Invalid Interface Index %d - \n", u4MplsTnlIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CfaGetMplsIfFromMplsTnlIf (u4MplsTnlIfIndex, pu4MplsIfIndex)
        == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaUtilGetMplsIfFromMplsTnlIf- "
                  "No MPLS If is layered below this MPLS Tnl interface"
                  "Index - FAIL\r\n", u4MplsTnlIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }
    CFA_FLAG_UNLOCK (bLockReq);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaUtilGetL3IfFromMplsIf 
 *
 *    Description          : Getting L3IPVLAN or Router port on which MPLS interface is
 *                           stacked over
 *
 *    Input(s)             : u4MplsIfIndex - MPLS Interface (166) 
 *                           bLockReq - Lock requested - 
 *                           TRUE - Needs to take CFA_LOCK ()
 *                           FALSE - Need not take CFA_LOCK ()
 *
 *    Output(s)            : pu4L3IfIndex - L3IPVLAN Interface or Router port 
 *
 *    Returns              : CFA_SUCCESS if succeeds, 
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaUtilGetL3IfFromMplsIf (UINT4 u4MplsIfIndex, UINT4 *pu4L3IfIndex,
                          BOOL1 bLockReq)
{

    tCfaIfInfo          IfInfo;

    *pu4L3IfIndex = CFA_NONE;

    CFA_FLAG_LOCK (bLockReq);
    if (CfaValidateCfaIfIndex (u4MplsIfIndex) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaUtilGetL3IfFromMplsIf- "
                  "Invalid Interface Index - %d\n", u4MplsIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }
    if (CfaGetIfInfo (u4MplsIfIndex, &IfInfo) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaUtilGetL3IfFromMplsIf "
                  "Fetching IfInfo failed for IfIndex - %d\n", u4MplsIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;

    }
    if (IfInfo.u1IfType != CFA_MPLS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaUtilGetL3IfFromMplsIf "
                  "Invalid interface type for IfIndex - %d\n", u4MplsIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;

    }

    if (CFA_IF_ENTRY (u4MplsIfIndex) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaUtilGetL3IfFromMplsIf "
                  "Invalid Interface Index %d - \n", u4MplsIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CfaGetL3IfFromMplsIf (u4MplsIfIndex, pu4L3IfIndex) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaUtilGetL3IfFromMplsIf "
                  "Fetching L3 interface failed for IfIndex - %d \n",
                  u4MplsIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;

    }

    CFA_FLAG_UNLOCK (bLockReq);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGetMplsIfFromMplsTnlIf
 *
 *    Description          : Getting MPLS interface on which the given MPLS
 *                           Tunnel interface is stacked over.
 *                           This is not an API and it should not be invoked
 *                           directly from the external modules
 *
 *    Input(s)             : u4MplsTnlIfIndex - MPLS Tunnel Interface (150)
 *                           TRUE - Needs to take CFA_LOCK ()
 *                           FALSE - Need not take CFA_LOCK ()
 *
 *    Output(s)            : pu4MplsIfIndex - MPLS interface
 *
 *    Returns              : CFA_SUCCESS if succeeds,
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGetMplsIfFromMplsTnlIf (UINT4 u4MplsTnlIfIndex, UINT4 *pu4MplsIfIndex)
{
    UINT1               u1IfType = CFA_ENET_UNKNOWN;
    tTMO_SLL           *pIfStack = NULL;
    tStackInfoStruct   *pScanNode = NULL;

    *pu4MplsIfIndex = CFA_NONE;

    pIfStack = &CFA_IF_STACK_LOW (u4MplsTnlIfIndex);

    TMO_SLL_Scan (pIfStack, pScanNode, tStackInfoStruct *)
    {
        if (CfaValidateCfaIfIndex (pScanNode->u4IfIndex) == CFA_SUCCESS)
        {
            CfaGetIfType (pScanNode->u4IfIndex, &u1IfType);

            if ((u1IfType == CFA_MPLS) || (u1IfType == CFA_MPLS_TUNNEL))
            {
                *pu4MplsIfIndex = pScanNode->u4IfIndex;
                break;
            }
        }
    }

    if (*pu4MplsIfIndex == CFA_NONE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "\r\nError in CfaGetMplsIfFromMplsTnlIf - "
                  "No MPLS interface is layered"
                  "below this MPLS Tunnel interface - %d\r\n",
                  u4MplsTnlIfIndex);
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGetL3IfFromMplsIf
 *
 *    Description          : Getting L3 (IVR or Router port) interface on 
 *                           which the given MPLS interface is stacked over.
 *                           This is not an API and it should not be invoked
 *                           directly from the external modules
 *
 *    Input(s)             : u4MplsIfIndex - MPLS Interface (166)
 *                           TRUE - Needs to take CFA_LOCK ()
 *                           FALSE - Need not take CFA_LOCK ()
 *
 *    Output(s)            : pu4L3IfIndex - L3 (VLAN or Router port) interface
 *
 *    Returns              : CFA_SUCCESS if succeeds,
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGetL3IfFromMplsIf (UINT4 u4MplsIfIndex, UINT4 *pu4L3IfIndex)
{
    UINT1               u1IfType = CFA_ENET_UNKNOWN;
    UINT1               u1BridgedIfaceStatus = 0;
    tTMO_SLL           *pIfStack = NULL;
    tStackInfoStruct   *pScanNode = NULL;

    *pu4L3IfIndex = CFA_NONE;

    pIfStack = &CFA_IF_STACK_LOW (u4MplsIfIndex);

    TMO_SLL_Scan (pIfStack, pScanNode, tStackInfoStruct *)
    {
        if (CfaValidateCfaIfIndex (pScanNode->u4IfIndex) == CFA_SUCCESS)
        {
            CfaGetIfType (pScanNode->u4IfIndex, &u1IfType);
            CfaGetIfBridgedIfaceStatus (pScanNode->u4IfIndex,
                                        &u1BridgedIfaceStatus);
            /* Fetch the Lower Layer type of L3IPVLAN */
            if (u1IfType == CFA_L3IPVLAN)
            {
                *pu4L3IfIndex = pScanNode->u4IfIndex;
                break;
            }
            else if ((u1IfType == CFA_ENET)
                     && (u1BridgedIfaceStatus == CFA_DISABLED))
            {
                *pu4L3IfIndex = pScanNode->u4IfIndex;
                break;
            }
#ifdef TLM_WANTED
            else if (u1IfType == CFA_TELINK)
            {
                /* The below function will return L3 Interface Index from
                 * TE Link If if there is just one layer of L3 Interface
                 * present below the TE Link.
                 *
                 * If there are some more TE Links available, then below
                 * function will fail to fetch L3 Interface Index.
                 */
                CfaGetL3IfFromTeLinkIf (pScanNode->u4IfIndex, pu4L3IfIndex);
                break;
            }
#endif
        }
    }

    if (*pu4L3IfIndex == CFA_NONE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaGetL3IfFromMplsIf- "
                  "No L3 (IVR or Router port) interface is layered"
                  "below this MPLS interface" "Index - %d\r\n", u4MplsIfIndex);
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

#endif
