/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfamux.c,v 1.6 2016/06/18 11:26:30 siva Exp $
 *
 * Description: This file contains the routines required for an module
 *              to register with the Packet Handler so as to receive 
 *              them directly (bypassing CFA module) for acheiving better
 *              performance
 *              
 *******************************************************************/
#ifndef _CFA_MUX_C_
#define _CFA_MUX_C_

#include "cfainc.h"
#include "mux.h"
#include "cfamux.h"

#if !defined(KERNEL_WANTED) || defined (NP_KERNEL_WANTED)
PRIVATE INT4 CfaMuxRegTupleTreeCmpFun PROTO ((tRBElem * prbTuple1,
                                              tRBElem * prbTuple2));
PRIVATE VOID CfaMuxRegExtractRegnTuple PROTO ((UINT1 *pu1Data, tProtRegTuple *
                                               pIncomingTuple));
PRIVATE tCfaMuxRegTbl *CfaMuxRegFindMatchingTblEntry
PROTO ((tProtRegTuple * pSearchTuple));
#endif

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CfaMuxIwfEnetProcessRxFrame
 *                                                                          
 *    DESCRIPTION      : This function gets called when a packet is received
 *                       from the lower layer. Incoming packet is parsed to
 *                       extract the packet tuple's using which the Registration 
 *                       table lookup is done for finding out the interested 
 *                       module. If no module has registered, then default 
 *                       packet rx flow is maintained.
 *
 *    INPUT            : pBuf      - Packet buffer
 *                       u4IfIndex - Interface on which packet was received.
 *                       u4PktSize - Size of the packet. 
 *                                                                               
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : CFA_SUCCESS/CFA_FAILURE 
 *                                                                          
 ****************************************************************************/
INT4
CfaMuxIwfEnetProcessRxFrame (tCRU_BUF_CHAIN_HEADER * pBuf,
                             UINT4 u4IfIndex, UINT4 u4PktSize,
                             UINT2 u2IfType, UINT1 u1EnetEncapType)
{
    UINT4               u4ModuleId = 0;

    /* For Desktop and BCM platform's (if KERNEL = NO or BCM (NP_KERNEL_WANTED))
     * then do the lookup in the Packet Handler database to find the module 
     * that is interested to process this packet. Else simply follow the 
     * previous (old) reception logic */
#if !defined(KERNEL_WANTED) || defined (NP_KERNEL_WANTED)
    tProtRegTuple       IncomingTuple;
    UINT1              *pu1Data = NULL;
    tCfaMuxRegTbl      *pProtRegEntry = NULL;

    MEMSET (&IncomingTuple, 0, sizeof (tProtRegTuple));

    pu1Data = CRU_BUF_GetDataPtr (pBuf->pFirstValidDataDesc);

    /* Extract the Tuple Information from the Incoming packet buffer */
    CfaMuxRegExtractRegnTuple (pu1Data, &IncomingTuple);

    if ((pProtRegEntry =
         CfaMuxRegFindMatchingTblEntry (&IncomingTuple)) != NULL)
    {
        u4ModuleId = (UINT4) CFA_MUX_MODULE_ID (pProtRegEntry);
    }
    else
    {
        /* Packets destined to all other modules who have not registered with 
         * Packet Handler Module will be simply posted to CFA for further
         * processings */
        u4ModuleId = 0;
    }

#else
    /* If packet is received from MUX, then Module ID would have been sent from
     * kernel */
    u4ModuleId = CFA_GET_PROTOCOL (pBuf);
#endif
    return (CfaMuxHandleIncomingFrame (pBuf, u4ModuleId, u4IfIndex, u4PktSize,
                                       u2IfType, u1EnetEncapType));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CfaMuxHandleIncomingFrame
 *                                                                          
 *    DESCRIPTION      : The function checks whether the input packet is 
 *                       destined to any registered module or not. Based on
 *                       that, the input packet gets posted to CFA or to their
 *                       respective modules (using the registered callback
 *                       routines).
 *
 *    INPUT            : pBuf       - Packet buffer
 *                       u4ModuleId - Module Id for which the packet is destined
 *                       u4IfIndex  - Interface on which packet was received
 *                       u4PktLen   - Size of the packet
 *    *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : CFA_SUCCESS/CFA_FAILURE 
 *                                                                          
 ****************************************************************************/
INT4
CfaMuxHandleIncomingFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ModuleId,
                           UINT4 u4IfIndex, UINT4 u4PktLen, UINT2 u2IfType,
                           UINT1 u1EnetEncapType)
{
    tCfaMuxRegTbl      *pCfaMuxRegEntry = NULL;

    if (u4ModuleId >= PACKET_HDLR_PROTO_MAX)
    {
        /* Invalid Module Identifier. Return Failure */
        return CFA_FAILURE;
    }

    /* Set the Module ID to which this packet is destined */
    CFA_SET_PROTOCOL (pBuf, (u4ModuleId));

    CFA_DS_LOCK ();

    pCfaMuxRegEntry = &gaCfaMuxRegTbl[u4ModuleId];
    if (CFA_MUX_PROT_FN_PTR (pCfaMuxRegEntry) != NULL)
    {
        /* The protocol has registered a function.
         * Give the packet to the function */
        CFA_MUX_PROT_FN_PTR (pCfaMuxRegEntry) (pBuf, u4IfIndex, u4PktLen);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();

    /* Incoming packet did not match with any registered protocol callback 
     * routine. Send the packet to CFA for further processing's */
#ifdef WTP_WANTED
    if ((u2IfType == CFA_RADIO) || (u2IfType == CFA_WLAN_RADIO))
    {
        return (CfaIwfWssProcessRxFrame (pBuf, u4IfIndex, u4PktLen));
    }
#endif
    return (CfaIwfEnetProcessRxFrame (pBuf, u4IfIndex, u4PktLen,
                                      u2IfType, u1EnetEncapType));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CfaMuxRegisterProtocolCallBack
 *                                                                          
 *    DESCRIPTION      : This function is called by the protocols for 
 *                       registering packet reception. The protocols register
 *                       with a set of qualifiers and a call back function. 
 *                       This function registers with the MUX and when the MUX
 *                       sends packet, it sends the packet to the corresponding
 *                       module.
 *                                                                          
 *    INPUT            : i1ProtocolId - The ID of the registering/deregistering
 *                                       protocol
 *                       pProtocolInfo - Structure containing the qualifiers for
 *                       the protocol
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : CFA_SUCCESS/CFA_FAILURE 
 *                                                                          
 ****************************************************************************/

INT4
CfaMuxRegisterProtocolCallBack (INT1 i1ProtocolId,
                                tProtocolInfo * pProtocolInfo)
{
    tCfaMuxRegTbl      *pCfaMuxRegEntry = NULL;
#if defined (KERNEL_WANTED) && !defined (NP_KERNEL_WANTED)
    tKernCmdParam       CmdParams;
#endif

    /* If MUX module is present [KERNEL = YES & BCM (NP_KERNEL_WANTED) is NO
     * (Marvell 6095 as of now)], a registration indication is sent to the 
     * MUX module present in Kernel Space via ioctl () which in turn process
     * it and updates its database */
    CFA_DS_LOCK ();

    pCfaMuxRegEntry = &gaCfaMuxRegTbl[i1ProtocolId];
    ++CFA_MUX_REG_COUNT (pCfaMuxRegEntry);
    if (CFA_MUX_REG_COUNT (pCfaMuxRegEntry) > 1)
    {
        /* If registeration is already done with the same Module Identifier
         * earlier(GMRP-MMRP, GVRP-MVRP etc. have common module ID's), do 
         * nothing and return Success */
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    /* Copy the details into the Global array */
    MEMCPY (CFA_MUX_PROT_INFO (pCfaMuxRegEntry), pProtocolInfo,
            sizeof (tProtocolInfo));
    CFA_MUX_REG_STATUS (pCfaMuxRegEntry) = CFA_TRUE;
    /* Set the counter to 1 as if it hits this part, the module
     * is registered only once*/
    CFA_MUX_REG_COUNT (pCfaMuxRegEntry) = 1;
    CFA_MUX_MODULE_ID (pCfaMuxRegEntry) = i1ProtocolId;

#if defined (KERNEL_WANTED) && !defined (NP_KERNEL_WANTED)
    CFA_DS_UNLOCK ();

    MEMCPY (&(CmdParams.MuxProtRegParams.ProtRegTuple),
            &(pProtocolInfo->ProtRegTuple), sizeof (tProtRegTuple));

    CmdParams.MuxProtRegParams.i4Command = MUX_PROT_REG_HDLR;
    CmdParams.MuxProtRegParams.u4ModuleId = (UINT4) i1ProtocolId;

    CmdParams.MuxProtRegParams.u1SendToFlag = pProtocolInfo->u1SendToFlag;

    /* Register the info with MUX */
    if (KAPIUpdateInfo (MUX_PROT_REG_IOCTL, &CmdParams) != OSIX_SUCCESS)
    {
        return CFA_FAILURE;
    }
#else
    /* Register the Incoming Module Information with the Registration Table in 
     * ISS if KERNEL = NO or in case of BCM (NP_KERNEL_WANTED) */
    if (RBTreeAdd (gpIssRegnTree, pCfaMuxRegEntry) == RB_FAILURE)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    CFA_DS_UNLOCK ();

#endif

    return CFA_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CfaMuxUnRegisterProtocolCallBack
 *                                                                          
 *    DESCRIPTION      : This function is called by the protocols for 
 *                       unregistering packet reception usually when the module 
 *                       is shutdown
 *                       
 *    INPUT            : i1ProtocolId - The ID of the registering/deregistering
 *                                       protocol
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : CFA_SUCCESS/CFA_FAILURE 
 *                                                                          
 ****************************************************************************/
INT4
CfaMuxUnRegisterProtocolCallBack (INT1 i1ProtocolId)
{
    tCfaMuxRegTbl      *pCfaMuxRegEntry = NULL;
#if defined (KERNEL_WANTED) && !defined (NP_KERNEL_WANTED)
    tKernCmdParam       CmdParams;
#endif

    CFA_DS_LOCK ();

    pCfaMuxRegEntry = &gaCfaMuxRegTbl[i1ProtocolId];
    if (CFA_MUX_REG_STATUS (pCfaMuxRegEntry) == CFA_FALSE)
    {
        /* Not registered but calling deregister */
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    /* Do not deregister with MUX unless all modules using the same 
     * module ID unregisters*/
    --CFA_MUX_REG_COUNT (pCfaMuxRegEntry);
    if (CFA_MUX_REG_COUNT (pCfaMuxRegEntry) != 0)
    {
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    /* We do not delete the information, we just reset the enabled flag. */
    CFA_MUX_REG_STATUS (pCfaMuxRegEntry) = CFA_FALSE;

#if defined (KERNEL_WANTED) && !defined (NP_KERNEL_WANTED)
    MEMCPY (&(CmdParams.MuxProtRegParams.ProtRegTuple),
            &CFA_MUX_PROT_REG_TUPLE (pCfaMuxRegEntry), sizeof (tProtRegTuple));

    CFA_DS_UNLOCK ();

    CmdParams.MuxProtRegParams.i4Command = MUX_PROT_DEREG_HDLR;
    CmdParams.MuxProtRegParams.u4ModuleId = (UINT4) i1ProtocolId;
    CmdParams.MuxProtRegParams.u1SendToFlag = MUX_REG_SEND_TO_ISS;

    /* Deregister the protocol with MUX */
    if (KAPIUpdateInfo (MUX_PROT_REG_IOCTL, &CmdParams) != OSIX_SUCCESS)
    {
        return CFA_FAILURE;
    }
#else
    /* Deregister the Incoming Module Information from the Registration Table
     * in ISS if KERNEL = NO or in case of BCM (NP_KERNEL_WANTED) */
    if (RBTreeRemove (gpIssRegnTree, pCfaMuxRegEntry) == RB_FAILURE)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    CFA_DS_UNLOCK ();

#endif

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaMuxInitProtRegnTable                              */
/*                                                                           */
/* Description        : This function gets called during CFA initialization  */
/*                      If MUX Module is not present in Kernel, then the     */
/*                      Registration Table is maintained in ISS and its      */
/*                      initialization happens in this routine               */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
CfaMuxInitProtRegnTable ()
{
    /* Create the RB tree for maintaining the Registration Table Desktop or BCM
     * platform [i.e. if KERNEL = NO or in case of BCM (NP_KERNEL_WANTED). 
     * Else this Registration table is maintained by the MUX module in Kernel 
     * Space (Marvell 6095 & Linux IP Combination) */
#if !defined(KERNEL_WANTED) || defined (NP_KERNEL_WANTED)
    gpIssRegnTree =
        RBTreeCreate (PACKET_HDLR_PROTO_MAX, CfaMuxRegTupleTreeCmpFun);

    if (NULL == gpIssRegnTree)
    {
        return CFA_FAILURE;
    }
#endif
    /* Do nothing if MUX module is present (Marvell 6095 & Linux IP Combination)
     * since this entire table gets maintained in the kernel module itself */
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaMuxRegisterArpModule                              */
/*                                                                           */
/* Description        : This function registers with the packet handler for  */
/*                      packets to be given to IP and ARP module for         */
/*                      processing. This gets called during initialization   */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
CfaMuxRegisterArpModule ()
{
    /* If MUX module is present (Marvell 6095 & Linux IP Combination), 
     * register ARP/RARP module with the Protocol Registration Table in MUX 
     * kernel module since Linux IP Stack takes care of processing the ARP
     * packets
     * If MUX is not present, this registration is not explicitly required and
     * they packets would follow the old packet reception logic itself since 
     * these packet are trivial (with Module ID = 0)*/
#if defined (KERNEL_WANTED) && !defined (NP_KERNEL_WANTED)
    tProtocolInfo       ProtocolInfo;

    MEMSET (&ProtocolInfo, 0, sizeof (tProtocolInfo));
    ProtocolInfo.ProtocolFnPtr = NULL;

    ProtocolInfo.i4Command = MUX_PROT_REG_HDLR;
    ProtocolInfo.u1SendToFlag = MUX_REG_SEND_TO_IP;

#ifdef ARP_WANTED
    ProtocolInfo.ProtRegTuple.EthInfo.u2Protocol = CFA_ENET_RARP;
    ProtocolInfo.ProtRegTuple.EthInfo.u2Mask = 0xFFFF;

    if (CfaMuxRegisterProtocolCallBack (PACKET_HDLR_PROTO_RARP, &ProtocolInfo)
        != CFA_SUCCESS)
    {
        return CFA_FAILURE;
    }

    ProtocolInfo.ProtRegTuple.EthInfo.u2Protocol = CFA_ENET_ARP;
    ProtocolInfo.ProtRegTuple.EthInfo.u2Mask = 0xFFFF;

    if (CfaMuxRegisterProtocolCallBack (PACKET_HDLR_PROTO_ARP, &ProtocolInfo) !=
        CFA_SUCCESS)
    {
        return CFA_FAILURE;
    }
#endif
#endif
    return CFA_SUCCESS;
}

#if !defined(KERNEL_WANTED) || defined (NP_KERNEL_WANTED)
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CfaMuxRegTupleTreeCmpFun
 *                                                                          
 *    DESCRIPTION      : This Function gets a matching 3 tuple entry from the
 *                       Protocol Registration table. This is used while 
 *                       processing the incoming packets and also while 
 *                       registering/deregistering an entry from the table
 *                       from ISS using ioctl ()
 *                                               
 *    INPUT            : prbTuple1 & prbTuple2 Pointer to the Tuple entries 
 *                       to be matched.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Returns 0 if Success. -1 or 1 in case of failure
 *                       scenarios (i.e no matching entry)
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
CfaMuxRegTupleTreeCmpFun (tRBElem * prbElem1, tRBElem * prbElem2)
{
    tCfaMuxRegTbl      *pRegTbl1 = (tCfaMuxRegTbl *) prbElem1;
    tCfaMuxRegTbl      *pRegTbl2 = (tCfaMuxRegTbl *) prbElem2;
    tProtRegTuple      *pTuple1 = &pRegTbl1->ProtocolInfo.ProtRegTuple;
    tProtRegTuple      *pTuple2 = &pRegTbl2->ProtocolInfo.ProtRegTuple;
    tProtRegTuple       IncomingTuple;
    tProtRegTuple       RegnTblInfo;
    INT4                i4ByteCount = 0;
    INT4                i4CmpVal = 0;

    MEMSET (&IncomingTuple, 0, sizeof (tProtRegTuple));
    MEMSET (&RegnTblInfo, 0, sizeof (tProtRegTuple));

    /* Incoming packets will not have Mask Information and the
     * Modules registering from ISS WILL set valid Mask if Qualifier 
     * needs to be considered as a Valid Key or else it sets an invalid mask
     * say 0 to indicate that this Qualifier is a "don't care" key */

    if ((CfaIsMacAllZeros (pTuple1->MacInfo.MacMask) == OSIX_TRUE) &&
        (pTuple1->EthInfo.u2Mask == 0) && (pTuple1->IPInfo.u2Mask == 0))

    {
        IncomingTuple.EthInfo.u2Protocol =
            pTuple1->EthInfo.u2Protocol & pTuple2->EthInfo.u2Mask;
        IncomingTuple.IPInfo.u2Protocol =
            pTuple1->IPInfo.u2Protocol & pTuple2->IPInfo.u2Mask;

        /* Masking the Incoming Packet MAC Information with the
         * Registration Table entry's Mask */
        for (; i4ByteCount < MAC_ADDR_LEN; i4ByteCount++)
        {
            IncomingTuple.MacInfo.MacAddr[i4ByteCount] =
                pTuple1->MacInfo.MacAddr[i4ByteCount] & pTuple2->MacInfo.
                MacMask[i4ByteCount];
            RegnTblInfo.MacInfo.MacAddr[i4ByteCount] =
                pTuple2->MacInfo.MacAddr[i4ByteCount] & pTuple2->MacInfo.
                MacMask[i4ByteCount];
        }
    }
    else
    {
        IncomingTuple.EthInfo.u2Protocol =
            pTuple1->EthInfo.u2Protocol & pTuple1->EthInfo.u2Mask;
        IncomingTuple.IPInfo.u2Protocol =
            pTuple1->IPInfo.u2Protocol & pTuple1->IPInfo.u2Mask;

        /* Masking the Incoming Registration Tuple's MAC Information */
        for (; i4ByteCount < MAC_ADDR_LEN; i4ByteCount++)
        {
            IncomingTuple.MacInfo.MacAddr[i4ByteCount] =
                pTuple1->MacInfo.MacAddr[i4ByteCount] & pTuple1->MacInfo.
                MacMask[i4ByteCount];
            RegnTblInfo.MacInfo.MacAddr[i4ByteCount] =
                pTuple2->MacInfo.MacAddr[i4ByteCount] & pTuple2->MacInfo.
                MacMask[i4ByteCount];
        }
    }

    RegnTblInfo.EthInfo.u2Protocol =
        pTuple2->EthInfo.u2Protocol & pTuple2->EthInfo.u2Mask;

    RegnTblInfo.IPInfo.u2Protocol =
        pTuple2->IPInfo.u2Protocol & pTuple2->IPInfo.u2Mask;

    /* If MAC Information matches, return the current entry */
    i4CmpVal = MEMCMP (IncomingTuple.MacInfo.MacAddr,
                       RegnTblInfo.MacInfo.MacAddr, sizeof (tMacAddr));
    if (i4CmpVal == 0)
    {

        /* Check for matching Ethernet Protocol number */

        if (IncomingTuple.EthInfo.u2Protocol > RegnTblInfo.EthInfo.u2Protocol)
        {
            return 1;
        }
        else if (IncomingTuple.EthInfo.u2Protocol <
                 RegnTblInfo.EthInfo.u2Protocol)
        {
            return -1;
        }

        /* Check for matching IP Protocol number */

        if (IncomingTuple.IPInfo.u2Protocol > RegnTblInfo.IPInfo.u2Protocol)
        {
            return 1;
        }
        else if (IncomingTuple.IPInfo.u2Protocol <
                 RegnTblInfo.IPInfo.u2Protocol)
        {
            return -1;
        }

        /* Incoming Tuple matches with the Registration Table Entry */
        return 0;
    }
    else
    {
        return i4CmpVal;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CfaMuxRegFindMatchingTblEntry
 *                                                                          
 *    DESCRIPTION      : This Function scans the given RB Tree with the incoming
 *                       search tuple extracted from the packet and returns
 *                       the matching entry from the tree ng packet buffer 
 *                                               
 *    INPUT            : pSearchTuple - Incoming Tuple entries
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : Pointer to tCfaMuxRegTbl entry if Success else NULL is
 *                       returned
 *                                                                          
 ****************************************************************************/
PRIVATE tCfaMuxRegTbl *
CfaMuxRegFindMatchingTblEntry (tProtRegTuple * pSearchTuple)
{
    tProtRegTuple       IncomingTuple;
    tProtRegTuple      *pRegnTblTuple = NULL;
    tCfaMuxRegTbl      *pRegnNode = NULL;
    tCfaMuxRegTbl      *pNextRegnNode = NULL;
    INT4                i4ByteCount = 0;

    MUX_SCAN_TREE (gpIssRegnTree, pRegnNode, pNextRegnNode, tCfaMuxRegTbl *)
    {
        pRegnTblTuple = &(pRegnNode->ProtocolInfo.ProtRegTuple);

        IncomingTuple.EthInfo.u2Protocol =
            pSearchTuple->EthInfo.u2Protocol & pRegnTblTuple->EthInfo.u2Mask;
        IncomingTuple.IPInfo.u2Protocol =
            pSearchTuple->IPInfo.u2Protocol & pRegnTblTuple->IPInfo.u2Mask;

        /* Masking the Incoming Registration Tuple's MAC Information */
        for (i4ByteCount = 0; i4ByteCount < MAC_ADDR_LEN; i4ByteCount++)
        {
            IncomingTuple.MacInfo.MacAddr[i4ByteCount] =
                pSearchTuple->MacInfo.MacAddr[i4ByteCount] &
                pRegnTblTuple->MacInfo.MacMask[i4ByteCount];
        }

        if (MUX_REG_ETH_PROTO_CMP (&IncomingTuple, pRegnTblTuple) &&
            MUX_REG_IP_PROTO_CMP (&IncomingTuple, pRegnTblTuple))
        {
            if (MEMCMP (&IncomingTuple.MacInfo.MacAddr,
                        pRegnTblTuple->MacInfo.MacAddr, sizeof (tMacAddr)) == 0)
            {
                return pRegnNode;
            }
        }
    }

    return NULL;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CfaMuxRegExtractRegnTuple
 *                                                                          
 *    DESCRIPTION      : This Function extracts the Protocol Registration 
 *                       Tuple information from the incoming packet buffer 
 *                                               
 *    INPUT            : pu1Data - Incoming packet's data buffer
 *                                                                          
 *    OUTPUT           : pIncomingTuple - Filled up Tuple entries
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
CfaMuxRegExtractRegnTuple (UINT1 *pu1Data, tProtRegTuple * pIncomingTuple)
{
    tEnetV2Header      *pEthHdr = NULL;
    t_IP_HEADER        *pIPHdr = NULL;

    pEthHdr = (tEnetV2Header *) (VOID *) pu1Data;

    /* Get the 3 Tuple <D-MAC, Ethernet Protocol, IP Protocol number> from the
     * incoming packet */

    /* Copy the Destination MAC from the incoming packet (first 6 bytes) */

    MEMCPY (MUX_REG_MAC_ADDR (pIncomingTuple), pu1Data, CFA_ENET_ADDR_LEN);

    /* Copy the Other 2 Tuples based on the Ethernet Protocol Type */

    MUX_REG_ETH_PROTO (pIncomingTuple) = OSIX_NTOHS (pEthHdr->u2LenOrType);

    switch (OSIX_NTOHS (pEthHdr->u2LenOrType))
    {
        case VLAN_PROTOCOL_ID:

            /* Skip the VLAN Header and copy the Ethernet and IP Protocol 
             * numbers */
            MEMCPY (&MUX_REG_ETH_PROTO (pIncomingTuple),
                    (pu1Data + CFA_VLAN_TAGGED_HEADER_SIZE),
                    CFA_VLAN_PROTOCOL_SIZE);
            MUX_REG_ETH_PROTO (pIncomingTuple) =
                OSIX_NTOHS (MUX_REG_ETH_PROTO (pIncomingTuple));
            if (MUX_REG_ETH_PROTO (pIncomingTuple) == CFA_ENET_IPV4)
            {
            pIPHdr =
                    (t_IP_HEADER *) (VOID *) (pu1Data + CFA_VLAN_TAGGED_HEADER_SIZE + CFA_ENET_TYPE_OR_LEN);
                MUX_REG_IP_PROTO (pIncomingTuple) = (UINT1) pIPHdr->u1Proto;
            }
            else
            {
                MUX_REG_IP_PROTO (pIncomingTuple) =
                    (UINT1) pu1Data[CFA_VLAN_TAGGED_HEADER_SIZE + CFA_ENET_TYPE_OR_LEN];
            }
            break;

        case CFA_ENET_IPV4:

            pIPHdr =
                (t_IP_HEADER *) (VOID *) (pu1Data + CFA_ENET_V2_HEADER_SIZE);
            MUX_REG_IP_PROTO (pIncomingTuple) = pIPHdr->u1Proto;
            break;

#ifdef IP6_WANTED
        case CFA_ENET_IPV6:
            /* For MLDS packets.
             * Check Whether it is an ICMPv6 packet in the Next header. 
             * If so, the ICMP type should be Added in the IP Proto field. */
            if (pu1Data[CFA_ENET_V2_HEADER_SIZE + IPV6_HEADER_LEN] ==
                ICMPV6_PROTOCOL_ID)
            {
                /* Assign the Icmpv6 Type from the ICMPv6 header */
                MUX_REG_IP_PROTO (pIncomingTuple) =
                    (UINT1) pu1Data[CFA_ENET_V2_HEADER_SIZE +
                                    IPV6_HEADER_LEN +
                                    IPV6_HOP_BY_HOP_HEADER_LEN];
            }
            break;
#endif
        case CFA_ENET_SLOW_PROTOCOL:

            /* Copy the Subtype (For EOAM & LA packets) as a Qualifier since 
             * they both have same MAC and Ethertype [01-80-C2-00-00-02 and
             * 0x8809].
             * Subtype is a Single byte field and is present in the 14th byte
             * (just after Ethernet Protocol) */
            MUX_REG_IP_PROTO (pIncomingTuple) =
                (UINT1) pu1Data[CFA_ENET_V2_HEADER_SIZE];

            break;

        default:

            /* Just return the MAC as Key for Table Lookup [Case for all L2
             * Packets] */

            break;
    }

    return;
}
#endif

#endif /* _CFA_MUX_C_ */
/* END OF FILE */
