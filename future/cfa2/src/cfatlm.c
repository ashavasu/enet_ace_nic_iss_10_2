/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfatlm.c,v 1.2 2012/05/28 13:40:37 siva Exp $
 *
 * Description: This file contains the routines for the Interface Management of 
 *              TLM interfaces in CFA.
 *
 *******************************************************************/

#ifdef TLM_WANTED

#include "cfainc.h"

/*****************************************************************************
 *
 *    Function Name        : CfaIfmInitTeLinkIfEntry
 *
 *    Description        : This function sets the default values for the 
 *                         TLM interface.
 *
 *    Input(s)            : UINT4 u4IfIndex, UINT1 *au1PortName
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if create and init succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmInitTeLinkIfEntry (UINT4 u4IfIndex, UINT1 *au1PortName)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1              *pu1IfName = NULL;
    UINT4               u4CfaTeLinkCurrentIndex = 0;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmInitTeLinkIfEntry for TeLink interface %d.\n",
              u4IfIndex);

    pu1IfName = &au1IfName[0];
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    CfaSetIfType (u4IfIndex, CFA_TELINK);

    /* Set the default alias name only if the alias is not set */
    if (*au1PortName == 0)
    {
        TELINK_IF_COUNT (u4IfIndex, u4CfaTeLinkCurrentIndex);
        SPRINTF ((CHR1 *) pu1IfName, "%s%d", CFA_TELINK_NAME_PREFIX,
                 u4CfaTeLinkCurrentIndex);
        CfaSetIfName (u4IfIndex, pu1IfName);
    }

    STRCPY (CFA_IF_DESCR (u4IfIndex), CFA_TELINK_DESCR);
    CfaSetIfMtu (u4IfIndex, CFA_TELINK_DEF_MTU);
    CfaSetIfSpeed (u4IfIndex, CFA_MAX_PORT_SPEED);
    CfaSetIfHighSpeed (u4IfIndex, (CFA_MAX_PORT_SPEED / CFA_1MB));

    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));
    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_OTHER;

    /* Assign the RS are NotReady because for this mpls interface to operate
     * successfully we need this to be stacked over an L3(IPVLAN) interface.
     * This interface would be made Active once the stacking is 
     * successful
     */
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Exiting CfaIfmInitTeLinkIfEntry - "
              "Initialised TeLink interface %d \n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmDeleteTeLinkIf
 *
 *    Description          : This function is for for deleting the TE Link
 *                           interface in the ifTable
 *
 *    Input(s)            : UINT4 u4IfIndex,UINT1 u1DelIfEntry
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if deletion succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmDeleteTeLinkIf (UINT4 u4IfIndex, UINT1 u1DelIfEntry)
{
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;
    UINT4               u4MplsIf = 0;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmDeleteTeLinkIf for TE Link interface %d\n",
              u4IfIndex);

    /* Get the MPLS Interface stacked over the TE Link */
    CfaGetMplsIfFromTeLinkIf (u4IfIndex, &u4MplsIf);

    CFA_IF_ADMIN (u4IfIndex) = CFA_IF_DOWN;

    pHighStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY ((UINT2) u4IfIndex);
    CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);

    pLowStackListScan =
        (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY ((UINT2) u4IfIndex);
    CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);

    while (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex))
    {
        pHighStackListScan =
            (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY ((UINT2) u4IfIndex);

        CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
        if (CFA_IF_STACK_IFINDEX (pHighStackListScan) != CFA_NONE)
        {
            /* Disassociate the Lower Stack Entry from Higher Layer and
             * Higher Stack Entry from Lower Layer. */
            CfaIfmDeleteStackEntry (CFA_IF_STACK_IFINDEX (pHighStackListScan),
                                    CFA_LOW, u4IfIndex);
        }
        else
        {
            CfaIfmUpdateStackForDeletion (&CFA_IF_STACK_HIGH (u4IfIndex),
                                          CFA_FALSE, CFA_NONE);
        }
    }

    while (CFA_IF_STACK_LOW_ENTRY (u4IfIndex))
    {
        pLowStackListScan =
            (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY ((UINT2) u4IfIndex);

        CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);
        if (CFA_IF_STACK_IFINDEX (pLowStackListScan) != CFA_NONE)
        {
            /* Disassociate the Lower Stack Entry from Higher Layer and
             * Higher Stack Entry from Lower Layer. */
            CfaIfmDeleteStackEntry (u4IfIndex, CFA_LOW,
                                    CFA_IF_STACK_IFINDEX (pLowStackListScan));
        }
        else
        {
            CfaIfmUpdateStackForDeletion (&CFA_IF_STACK_LOW (u4IfIndex),
                                          CFA_FALSE, CFA_NONE);
        }
    }

    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
              "In CfaIfmDeleteTeLinkIf - "
              "Un-reg with HL success - interface %d\n", u4IfIndex);

    /* check whether the entry is to be deleted from IfTable itself */
    if (u1DelIfEntry == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "In CfaIfmDeleteTeLinkIf - "
                  "disabling success - interface %d\n", u4IfIndex);

        return (CFA_SUCCESS);
    }

    /* Delete the MPLS Interface */
    CfaIfmDeleteIfEntry (u4MplsIf, CFA_MPLS);
    /* delete the interface from the IfTable */
    if (CfaIfmDeleteIfEntry (u4IfIndex, CFA_TELINK) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteTeLinkIf - "
                  "ifEntry del fail for interface %d\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteTeLinkIf - "
              "Full Delete success - interface %d\n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGetMplsIfFromTeLinkIf
 *
 *    Description          : Getting MPLS interface on which the given TE Link
 *                           interface is stacked over.
 *                           This is not an API and it should not be invoked
 *                           directly from the external modules.
 *
 *    Input(s)             : u4TeLinkIf - TE Link Interface (200)
 *
 *    Output(s)            : pu4MplsIf - MPLS interface
 *
 *    Returns              : CFA_SUCCESS if succeeds,
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGetMplsIfFromTeLinkIf (UINT4 u4TeLinkIf, UINT4 *pu4MplsIf)
{
    UINT1               u1IfType = CFA_ENET_UNKNOWN;
    UINT4               u4TempIf = CFA_NONE;

    *pu4MplsIf = CFA_NONE;

    /* MPLS Interface can be stacked over
     *
     * 1. One layer of TE Link Interface
     * 2. Two layers of TE Link Interface
     */

    /* Get Higher Layer of TE Link Interface */
    if (CfaGetHLIfFromLLIf (u4TeLinkIf, &u4TempIf, &u1IfType) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }

    /* If Higher Layer is CFA_MPLS, return */
    if (u1IfType == CFA_MPLS)
    {
        *pu4MplsIf = u4TempIf;
        return CFA_SUCCESS;
    }

    /* If Higher layer is CFA_TELINK, go one layer higher */
    if (u1IfType == CFA_TELINK)
    {
        if (CfaGetHLIfFromLLIf (u4TempIf, &u4TempIf, &u1IfType) == CFA_FAILURE)
        {
            return CFA_FAILURE;
        }

        /* If Higher Layer is CFA_MPLS, return */
        if (u1IfType == CFA_MPLS)
        {
            *pu4MplsIf = u4TempIf;
            return CFA_SUCCESS;
        }
    }

    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGetTeLinkIfFromL3If
 *
 *    Description          : Getting TE Link interface on which the given L3
 *                           interface is stacked over.
 *                           This is not an API and it should not be invoked
 *                           directly from the external modules.
 *
 *    Input(s)             : u4L3If  - L3 Interface (200)
 *                           u1Level - Level at which TE Link interface is
 *                                     required
 *
 *    Output(s)            : pu4TeLinkIf - pu4TeLinkIf
 *
 *    Returns              : CFA_SUCCESS if succeeds,
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGetTeLinkIfFromL3If (UINT4 u4TeLinkIf, UINT1 u1Level, UINT4 *pu4MplsIf)
{
    UINT1               u1IfType = CFA_ENET_UNKNOWN;
    UINT4               u4TempIf = CFA_NONE;
    UINT1               u1TestLevel = u1Level;
    BOOL1               bFlag = FALSE;

    *pu4MplsIf = CFA_NONE;

    /* TE Link Interface can be stacked over
     *
     * 1. L3 Interface
     * 2. One layer of TE Link Interface
     */

    while (u1TestLevel > CFA_NONE)
    {
        /* Get Higher Layer of TE Link Interface */
        if (CfaGetHLIfFromLLIf (u4TeLinkIf, &u4TempIf, &u1IfType)
            == CFA_FAILURE)
        {
            break;
        }

        /* If Higher layer is CFA_TELINK check level */
        if (u1IfType == CFA_TELINK)
        {
            u1TestLevel--;

            /* If required level is reached return */
            if (u1TestLevel == CFA_NONE)
            {
                *pu4MplsIf = u4TempIf;
                bFlag = TRUE;
                break;
            }

            /* Required level is not reached so go one higher layer */
            u4TeLinkIf = u4TempIf;
        }
        else
        {
            /* Invalid interface type is received, so break. */
            break;
        }
    }

    if (bFlag == FALSE)
    {
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGetL3IfFromTeLinkIf
 *
 *    Description          : Getting L3 (IVR or Router port) interface on 
 *                           which the given TE Link interface is stacked over.
 *                           This is not an API and it should not be invoked
 *                           directly from the external modules.
 *                           
 *                           This function will return L3 Interface Index from
 *                           TE Link If if there is just one layer of L3 Interface
 *                           present below the TE Link.
 *                           
 *                           If there are some more TE Links available, then below
 *                           function will fail to fetch L3 Interface Index.
 *
 *    Input(s)             : u4TeLinkIf - TE Link Interface (200)
 *
 *    Output(s)            : pu4L3IfIndex - L3 (VLAN or Router port) interface
 *
 *    Returns              : CFA_SUCCESS if succeeds,
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGetL3IfFromTeLinkIf (UINT4 u4TeLinkIf, UINT4 *pu4L3IfIndex)
{
    UINT1               u1IfType = CFA_ENET_UNKNOWN;
    UINT1               u1BridgedIfaceStatus = 0;
    tTMO_SLL           *pIfStack = NULL;
    tStackInfoStruct   *pScanNode = NULL;

    *pu4L3IfIndex = CFA_NONE;

    pIfStack = &CFA_IF_STACK_LOW (u4TeLinkIf);

    TMO_SLL_Scan (pIfStack, pScanNode, tStackInfoStruct *)
    {
        if (CfaValidateCfaIfIndex (pScanNode->u4IfIndex) == CFA_SUCCESS)
        {
            CfaGetIfType (pScanNode->u4IfIndex, &u1IfType);
            CfaGetIfBridgedIfaceStatus (pScanNode->u4IfIndex,
                                        &u1BridgedIfaceStatus);

            /* Fetch the Lower Layer type of L3IPVLAN */
            if (u1IfType == CFA_L3IPVLAN)
            {
                *pu4L3IfIndex = pScanNode->u4IfIndex;
                break;
            }
            else if ((u1IfType == CFA_ENET)
                     && (u1BridgedIfaceStatus == CFA_DISABLED))
            {
                *pu4L3IfIndex = pScanNode->u4IfIndex;
                break;
            }
            else if (u1IfType == CFA_MPLS_TUNNEL)
            {
                *pu4L3IfIndex = pScanNode->u4IfIndex;
                break;
            }
        }
    }

    if (*pu4L3IfIndex == CFA_NONE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaGetL3IfFromTeLinkIf- "
                  "No L3 (IVR or Router port) interface is layered"
                  "below this TE Link interface" "Index - %d\r\n", u4TeLinkIf);
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}
/*****************************************************************************
 *
 *    Function Name        : CfaIfmCreateDynamicTeLinkIf
 *
 *    Description          : This function creates the TE-Link If Entry
 *
 *    Input(s)             : UINT4 u4IfIndex
 *                           u4CompIfIndex  - ComponentLink IfIndex
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if create and init succeeds,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmCreateDynamicTeLinkIf (UINT4 u4IfIndex, UINT4 u4CompIfIndex)
{
    UINT1               au1NullString[2] = "";
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (&au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    /* Validate the Presence of Component Link in CFA */
    if (CfaValidateCfaIfIndex (u4CompIfIndex) == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmCreateDynamicTeLinkIf- "
                 "Invalid Interface Index - FAIL\n");
        return CFA_FAILURE;
    }

    /* Validate the Presence of Component Link in CFA */
    if (CFA_IF_ENTRY (u4CompIfIndex) == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmCreateDynamicTeLinkIf- "
                 "Invalid Interface Index - FAIL\n");
        return CFA_FAILURE;
    }

    /* Create the TE Link Entry in CFA */
    if (CfaIfmCreateAndInitIfEntry (u4IfIndex, CFA_INVALID_TYPE,
                                    au1NullString) == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmCreateDynamicTeLinkIf- "
                 "creation of TELink - FAIL\n");
        return CFA_FAILURE;
    }

    CfaGetIfAlias (u4IfIndex, au1IfName);

    /* Populate TE Link specific properties */
    CfaIfmInitTeLinkIfEntry (u4IfIndex, au1IfName);
    CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
    CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);
    /* Here set the storage type as VOLATILE */
    CfaSetIfMainStorageType (u4IfIndex, CFA_STORAGE_TYPE_VOLATILE);
   
    /* Call the function to stack the TELink over compLink and
     * MplsIf over TELink */
    if ((CfaIfmStackDynamicTeLink (u4IfIndex, u4CompIfIndex)) == 
        CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmCreateDynamicTeLinkIf- "
                 "creation of TELink - FAIL\n");
        CfaIfmDeleteIfEntry (u4IfIndex, CFA_TELINK);
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}
/*****************************************************************************
 *
 *    Function Name        : CfaIfmCreateDynamicMplsIf
 *
 *    Description          : This function creates the TE-Link If Entry
 *
 *    Input(s)             : u4IfIndex - Mpls Interface Index
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if create and init succeeds,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaIfmCreateDynamicMplsIf (UINT4 u4IfIndex)
{
    UINT1               au1NullString[2] = "";
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (&au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    if (CfaIfmCreateAndInitIfEntry (u4IfIndex, CFA_INVALID_TYPE,
                                    au1NullString) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }

    CfaGetIfAlias (u4IfIndex, au1IfName);

    CfaIfmInitMplsIfEntry (u4IfIndex, au1IfName);
    CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
    CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);

    /* Here set the storage type as VOLATILE */
    CfaSetIfMainStorageType (u4IfIndex, CFA_STORAGE_TYPE_VOLATILE);
    
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmStackDynamicTeLink
 *
 *    Description          : This function does the stacking of TE-Link over
 *                           component Link and Mpls Interface over TE-Link
 *
 *    Input(s)             : u4TeIfIndex    - TELink IfIndex
 *                           u4CompIfIndex  - ComponentLink IfIndex
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if stacking is success
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaIfmStackDynamicTeLink (UINT4 u4TeIfIndex, UINT4 u4CompIfIndex)
{
    UINT4           u4MplsIfIndex = CFA_NONE;
    tTMO_SLL        *pHighStack = NULL;
    tTMO_SLL        *pLowStack = NULL;
    tTMO_SLL_NODE   *pNode = NULL;


    /* Get the Free MPLS Interface Index from the Mpls Interface Index's Pool */
    if (CfaGetFreeInterfaceIndex (&u4MplsIfIndex, CFA_MPLS) ==
        OSIX_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmStackDynamicTeLink- "
                 "Unable to Get Free Interface Index - FAIL\n");
        return CFA_FAILURE;
    }

    /* Do stacking of TE-Link Interface (200) over Component Link Interface */
    if (CfaIfmAddStackEntry (u4TeIfIndex, CFA_LOW, u4CompIfIndex,
                             CFA_RS_NOTINSERVICE) == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmStackDynamicTeLink- "
                 "TE-Link over Cpt Link Stack creation- FAIL\n");
        return CFA_FAILURE;
    }

    /* Activate stack of componentIfIndex and its lower 
     * layer - MplsInterfaceIndex */
    pHighStack = &CFA_IF_STACK_LOW (u4CompIfIndex);
    pNode = TMO_SLL_First (pHighStack);

    if (pNode != NULL)
    {
        CFA_IF_STACK_STATUS (pNode) = (UINT1) CFA_RS_ACTIVE;
    }

    /* Activate stack of componentIfIndex and its higher
     * layer - TE Link Interface */
    pLowStack = &CFA_IF_STACK_HIGH (u4CompIfIndex);
    pNode = TMO_SLL_First (pLowStack);

    if (pNode != NULL)
    {
        CFA_IF_STACK_STATUS (pNode) = (UINT1) CFA_RS_ACTIVE;
    }

    /* TeLink is the higher layer for the compIfIndex */
    CFA_IF_ENTRY (u4TeIfIndex)->u4HighIfIndex = u4CompIfIndex;

    /* Create MPLS Interface in the CFA Level */
    if (CfaIfmCreateDynamicMplsIf (u4MplsIfIndex) == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmStackDynamicTeLink- "
                 "Unable to create Dynamic MplsInterface - FAIL\n");
        /* Delete the stacking Entries */
        CfaIfmDeleteDynamicStackEntry (u4TeIfIndex, u4CompIfIndex);
        return CFA_FAILURE;
    }
    /* check todo */
    /* Now Do stacking of MPLS Interface (166) over TE Link Interface */
    if (CfaIfmAddStackEntry (u4MplsIfIndex, CFA_LOW, u4TeIfIndex,
                             CFA_RS_NOTINSERVICE) == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmStackDynamicTeLink- "
                 "MplsIf over TE-Link Stack creation- FAIL\n");
        /* Delete the stacking Entries */
        CfaIfmDeleteDynamicStackEntry (u4TeIfIndex, u4CompIfIndex);
        /* Delete the created MplsIfIndex */
        CfaIfmDeleteIfEntry (u4MplsIfIndex, CFA_MPLS);
        return CFA_FAILURE;
    }

    /* Activate stack of MplsInterface and its lower layer
     * TE Link Interface */
    pHighStack = &CFA_IF_STACK_LOW (u4MplsIfIndex);
    pNode = TMO_SLL_First (pHighStack);

    if (pNode != NULL)
    {
        CFA_IF_STACK_STATUS (pNode) = (UINT1) CFA_RS_ACTIVE;
    }

    /* Activate stack of TE Link Interface and its Lower layer
     * Component Link Interface */
    pHighStack = &CFA_IF_STACK_LOW (u4TeIfIndex);
    pNode = TMO_SLL_First (pHighStack);

    if (pNode != NULL)
    {
        CFA_IF_STACK_STATUS (pNode) = (UINT1) CFA_RS_ACTIVE;
    }
    /* MplsIfIndex is the highest layer for the compIfIndex */
    CFA_IF_ENTRY (u4MplsIfIndex)->u4HighIfIndex = u4CompIfIndex;
    /* Now make the rowstatus of TE-Link as Active */
    CFA_IF_RS (u4TeIfIndex) = CFA_RS_ACTIVE;
    CfaSetCdbRowStatus (u4TeIfIndex, CFA_RS_ACTIVE);

    if (CfaSetIfMainAdminStatus ((INT4) u4TeIfIndex, CFA_IF_UP) == CFA_FAILURE)
    {
        /* Delete the stacking Entries */
        CfaIfmDeleteDynamicStackEntry (u4TeIfIndex, u4CompIfIndex);
        CfaIfmDeleteDynamicStackEntry (u4MplsIfIndex, u4TeIfIndex);
        CfaIfmDeleteIfEntry (u4MplsIfIndex, CFA_MPLS);
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmStackDynamicTeLink- "
                 "Setting AdminStatus of TeLink - FAIL\n");
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaApiDeleteDynamicTeLink
 *
 *    Description          : This function deletes the TE-Link and their    
 *                           associated stacking Entries
 *
 *    Input(s)             : u4IfIndex     - TELink IfIndex
 *                           u1DelIfEntry  - To delete the Entry in ifTable
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS / CFA_FAILURE.
 *
 *****************************************************************************/


INT4 
CfaApiDeleteDynamicTeLink (UINT4 u4IfIndex, UINT1 u1DelIfEntry) 
{
    CFA_LOCK();
    if ((CfaIfmDeleteTeLinkIf (u4IfIndex, u1DelIfEntry)) ==
        CFA_FAILURE)
    {
        CFA_UNLOCK();
        return CFA_FAILURE;
    }

    CFA_UNLOCK();
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaApiCreateDynamicTeLinkInterface
 *
 *    Description          : This function creates the TE-Link If Entry
 *
 *    Input(s)             : UINT4 u4IfIndex
 *                           u4CompIfIndex  - ComponentLink IfIndex
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if create and init succeeds,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaApiCreateDynamicTeLinkInterface (UINT4 u4IfIndex, UINT4 u4CompIfIndex)
{

    CFA_LOCK();

    if ((CfaIfmCreateDynamicTeLinkIf (u4IfIndex, u4CompIfIndex)) == 
        CFA_FAILURE)
    {
        CFA_UNLOCK();
        return CFA_FAILURE;
    }

    CFA_UNLOCK();
    return CFA_SUCCESS;
}


/*****************************************************************************
 *
 *    Function Name        : CfaGetL3IfOperStatusFromTeLinkIf
 *
 *    Description          : This function scans the underlying interfaces.
 *                           If underlying interface is L3 If, returns its 
 *                           Oper Status.
 *
 *                           Else if underlying interface is TE Link If, further 
 *                           scan of TE Link If is done. If atleast one TE Link 
 *                           If is Oper UP, Oper Up is returned, else Oper Down
 *                           is returned. 
 *                           
 *    Input(s)             : u4TeLinkIf - TE Link Interface (200)
 *
 *    Output(s)            : None
 *
 *    Returns              : CFA_IF_UP or CFA_IF_DOWN
 *****************************************************************************/
UINT1
CfaGetL3IfOperStatusFromTeLinkIf (UINT4 u4TeLinkIf)
{
    UINT1               u1IfType = CFA_ENET_UNKNOWN;
    UINT1               u1BridgedIfaceStatus = 0;
    tTMO_SLL           *pIfStack = NULL;
    tStackInfoStruct   *pScanNode = NULL;
    UINT1               u1OperStatus = CFA_IF_DOWN;

    pIfStack = &CFA_IF_STACK_LOW (u4TeLinkIf);

    TMO_SLL_Scan (pIfStack, pScanNode, tStackInfoStruct *)
    {
        if (CfaValidateCfaIfIndex (pScanNode->u4IfIndex) == CFA_SUCCESS)
        {
            CfaGetIfType (pScanNode->u4IfIndex, &u1IfType);
            CfaGetIfBridgedIfaceStatus (pScanNode->u4IfIndex,
                                        &u1BridgedIfaceStatus);

            if ((u1IfType == CFA_L3IPVLAN) || (u1IfType == CFA_MPLS_TUNNEL) ||
                ((u1IfType == CFA_ENET) &&
                 (u1BridgedIfaceStatus == CFA_DISABLED)))
            {
                CfaGetIfOperStatus (pScanNode->u4IfIndex, &u1OperStatus);
                break;
            }
            else if (u1IfType == CFA_TELINK)
            {
                CfaGetIfOperStatus (pScanNode->u4IfIndex, &u1OperStatus);

                if (u1OperStatus == CFA_IF_UP)
                {
                    break;
                }
            }
        }
    }

    return u1OperStatus;
}

#endif
