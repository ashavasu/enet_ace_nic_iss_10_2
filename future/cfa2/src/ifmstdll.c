/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ifmstdll.c,v 1.128 2018/01/08 12:28:33 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#include  "cfainc.h"
#include  "fscfacli.h"

/* LOW LEVEL Routines for Table : IfTable. */

/****************************************************************************
 Function    :    nmhValidateIndexInstanceIfTable
 Input    :    The Indices
        IfIndex
 Output    :    The Routines Validates the Given Indices.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIfTable (INT4 i4IfIndex)
{
    UINT4               u4IfIndex;

    if (((UINT4) i4IfIndex > CFA_MAX_INTERFACES ()) || (i4IfIndex <= 0))
    {
        return (SNMP_FAILURE);
    }

    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        if (CFA_IF_ENTRY_USED (u4IfIndex))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }

}

/****************************************************************************
 Function    :    nmhGetFirstIndexIfTable
 Input    :    The Indices
        IfIndex
 Output    :    The Get First Routines gets the Lexicographicaly
        First Entry from the Table.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIfTable (INT4 *pi4IfIndex)
{
    UINT4               u4Index = 1;
#ifdef MBSM_WANTED
    UINT1               u1IfType;
#endif

/* Get the First Used Index */
    CFA_CDB_SCAN_WITH_LOCK (u4Index, (CFA_MAX_INTERFACES ()), CFA_ALL_IFTYPE)
    {
        if (CfaIsIfEntryProgrammingAllowed (u4Index) == CFA_FALSE)
        {
            if (CFA_CDB_IS_INTF_VALID (u4Index) == CFA_TRUE)
            {
                *pi4IfIndex = (INT4) u4Index;
                return SNMP_SUCCESS;
            }
        }
        else
        {
#ifdef MBSM_WANTED
            if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
            {
                /* To skip the stack ports */
                CfaGetEthernetType ((UINT4) u4Index, &u1IfType);
                if (u1IfType == CFA_STACK_ENET)
                {
                    u4Index = u4Index + ISS_MAX_STK_PORTS;
                }
            }
#endif
            if (CFA_IF_ENTRY_USED (u4Index))
            {
                *pi4IfIndex = (INT4) u4Index;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :    nmhGetNextIndexIfTable
 Input    :    The Indices
        IfIndex
        nextIfIndex
 Output    :    The Get Next function gets the Next Index for
        the Index Value given in the Index Values. The
        Indices are stored in the next_varname variables.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIfTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{

    UINT4               u4IfIndex;
    UINT4               u4Index;
#ifdef MBSM_WANTED
    UINT1               u1IfType;
#endif

    /* To comply with the SNMP Agt operations,for Values greater 
     * than the supported index(here -ve value meaning u4 value is given),
     * return ERROR,so that the next  element's first index would be 
     * given by the agent for the manager.
     */
    if (i4IfIndex <= 0 || (UINT4) i4IfIndex > CFA_MAX_INTERFACES ())
    {
        return SNMP_FAILURE;
    }

    u4IfIndex = (UINT4) i4IfIndex;

    /* Next Index should always return the next used Index */
    u4Index = u4IfIndex + 1;
    CFA_CDB_SCAN_WITH_LOCK (u4Index, SYS_DEF_MAX_INTERFACES, CFA_ALL_IFTYPE)
    {
        if (CfaIsIfEntryProgrammingAllowed (u4Index) == CFA_FALSE)
        {
            if (CFA_CDB_IS_INTF_VALID (u4Index) == CFA_TRUE)
            {
                *pi4NextIfIndex = (INT4) u4Index;
                return SNMP_SUCCESS;
            }
        }
        else
        {
#ifdef MBSM_WANTED
            if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
            {
                /* To skip the stack ports */
                CfaGetEthernetType (u4Index, &u1IfType);
                if (u1IfType == CFA_STACK_ENET)
                {
                    u4Index = u4Index + ISS_MAX_STK_PORTS;
                }
            }
#endif
            if (CFA_IF_ENTRY_USED (u4Index))
            {
                *pi4NextIfIndex = (INT4) u4Index;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :    nmhGetIfDescr
 Input    :    The Indices
        IfIndex

        The Object
        retValIfDescr
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfDescr (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pRetValIfDescr)
{
    UINT4               u4IfIndex;

    u4IfIndex = (UINT4) i4IfIndex;
    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        pRetValIfDescr->i4_Length = (INT4) STRLEN (CFA_IF_DESCR (u4IfIndex));
        MEMCPY (pRetValIfDescr->pu1_OctetList, CFA_IF_DESCR (u4IfIndex),
                pRetValIfDescr->i4_Length);
    }
    else
    {
        pRetValIfDescr->i4_Length = (INT4) STRLEN (CFA_CDB_IF_DESC (u4IfIndex));
        MEMCPY (pRetValIfDescr->pu1_OctetList, CFA_CDB_IF_DESC (u4IfIndex),
                pRetValIfDescr->i4_Length);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfType
 Input    :    The Indices
        IfIndex

        The Object
        retValIfType
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfType (INT4 i4IfIndex, INT4 *pi4RetValIfType)
{
    UINT1               u1IfType;

    if (CfaGetIfType ((UINT4) i4IfIndex, &u1IfType) == CFA_SUCCESS)
    {
        *pi4RetValIfType = (INT4) u1IfType;
        return SNMP_SUCCESS;
    }
    else
    {
        *pi4RetValIfType = (INT4) CFA_INVALID_TYPE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :    nmhGetIfMtu
 Input    :    The Indices
        IfIndex

        The Object
        retValIfMtu
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMtu (INT4 i4IfIndex, INT4 *pi4RetValIfMtu)
{
    UINT4               u4IfMtu = 0;

    UINT4               u4IfIndex;

    u4IfIndex = (UINT4) i4IfIndex;

    CfaGetIfMtu (u4IfIndex, &u4IfMtu);
    *pi4RetValIfMtu = (INT4) u4IfMtu;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfSpeed
 Input    :    The Indices
        IfIndex

        The Object
        retValIfSpeed
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfSpeed (INT4 i4IfIndex, UINT4 *pu4RetValIfSpeed)
{

    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif

    u4IfIndex = (UINT4) i4IfIndex;

#ifdef NPAPI_WANTED

    CfaGetIfType (u4IfIndex, &u1IfType);

    if ((u1IfType == CFA_ENET) && (CfaIsMgmtPort (u4IfIndex) == FALSE))
    {
        if (CfaIssHwGetPortSpeed (u4IfIndex, (INT4 *) pu4RetValIfSpeed) ==
            FNP_FAILURE)
        {
            return (SNMP_FAILURE);
        }
        switch (*pu4RetValIfSpeed)
        {
            case ISS_10MBPS:
                *pu4RetValIfSpeed = CFA_ENET_SPEED_10M;
                break;
            case ISS_100MBPS:
                *pu4RetValIfSpeed = CFA_ENET_SPEED_100M;
                break;
            case ISS_1GB:
                *pu4RetValIfSpeed = CFA_ENET_SPEED_1G;
                break;
            case ISS_2500MBPS:
                *pu4RetValIfSpeed = CFA_ENET_SPEED_2500M;
                break;
            case ISS_10GB:
                *pu4RetValIfSpeed = CFA_ENET_MAX_SPEED_VALUE;
                break;
            default:
                *pu4RetValIfSpeed = CFA_ENET_SPEED_1G;
        }
    }
    else
    {
        CfaGetIfSpeed (u4IfIndex, pu4RetValIfSpeed);
    }
#else
    CfaGetIfSpeed (u4IfIndex, pu4RetValIfSpeed);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfPhysAddress
 Input    :    The Indices
        IfIndex

        The Object
        retValIfPhysAddress
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfPhysAddress (INT4 i4IfIndex,
                     tSNMP_OCTET_STRING_TYPE * pRetValIfPhysAddress)
{

    UINT4               u4IfIndex;
    UINT1               u1SubType = 0;
    UINT1               u1IfType = CFA_IF_DOWN;
    tRcvAddressInfo    *pRcvAddrNode;
    UINT1               au1IfHwAddr[CFA_ENET_ADDR_LEN];

    tMacAddr            SwitchMac;

    u4IfIndex = (UINT4) i4IfIndex;

    MEMSET (au1IfHwAddr, 0, CFA_ENET_ADDR_LEN);

    CfaGetIfType (u4IfIndex, &u1IfType);
    CfaGetIfSubType (u4IfIndex, &u1SubType);

    if (u1IfType == CFA_ENET)
    {
        if ((pRcvAddrNode =
             (tRcvAddressInfo *) CFA_IF_RCVADDRTAB_ENTRY (u4IfIndex)) == NULL)
        {
            return SNMP_FAILURE;
        }
        MEMCPY (pRetValIfPhysAddress->pu1_OctetList, pRcvAddrNode->au1Address,
                CFA_ENET_ADDR_LEN);
        pRetValIfPhysAddress->i4_Length = CFA_ENET_ADDR_LEN;
        return (SNMP_SUCCESS);
    }
    else if (u1IfType == CFA_LAGG)
    {
        CfaGetIfHwAddr (u4IfIndex, au1IfHwAddr);
        MEMCPY (pRetValIfPhysAddress->pu1_OctetList,
                au1IfHwAddr, CFA_ENET_ADDR_LEN);

        pRetValIfPhysAddress->i4_Length = CFA_ENET_ADDR_LEN;
        return (SNMP_SUCCESS);
    }

    else if (u1IfType == CFA_L3IPVLAN)
    {
        if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
        {
            /* The stack interface uses the stack mac address
             * which is different from the system mac address*/

            if (i4IfIndex == CFA_DEFAULT_STACK_IFINDEX)
            {
                CfaGetIfHwAddr (CFA_DEFAULT_STACK_IFINDEX, SwitchMac);
            }
            else
            {
                CfaGetSysMacAddress (SwitchMac);
            }
        }
        else
        {
            CfaGetSysMacAddress (SwitchMac);
        }
        MEMCPY (pRetValIfPhysAddress->pu1_OctetList, SwitchMac,
                CFA_ENET_ADDR_LEN);

        pRetValIfPhysAddress->i4_Length = CFA_ENET_ADDR_LEN;
        return (SNMP_SUCCESS);
    }
    else if ((u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
             (u1SubType == CFA_SUBTYPE_SISP_INTERFACE))
    {
        CFA_DS_LOCK ();

        if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
        {
            MEMCPY (pRetValIfPhysAddress->pu1_OctetList,
                    &(CFA_CDB_IF_ENTRY (u4IfIndex)->au1MacAddr),
                    CFA_ENET_ADDR_LEN);

            pRetValIfPhysAddress->i4_Length = CFA_ENET_ADDR_LEN;
        }

        CFA_DS_UNLOCK ();

        return (SNMP_SUCCESS);
    }
    else
    {
        MEMSET (pRetValIfPhysAddress->pu1_OctetList, 0, 1);
        pRetValIfPhysAddress->i4_Length = CFA_GEN_ADDR_LEN;
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :    nmhGetIfAdminStatus
 Input    :    The Indices
        IfIndex

        The Object
        retValIfAdminStatus
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfAdminStatus (INT4 i4IfIndex, INT4 *pi4RetValIfAdminStatus)
{

    UINT4               u4IfIndex;

    u4IfIndex = (UINT4) i4IfIndex;
    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        *pi4RetValIfAdminStatus = CFA_CDB_IF_ADMIN_STATUS (u4IfIndex);
    }
    else
    {
        *pi4RetValIfAdminStatus = CFA_IF_ADMIN (u4IfIndex);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfOperStatus
 Input    :    The Indices
        IfIndex

        The Object
        retValIfOperStatus
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfOperStatus (INT4 i4IfIndex, INT4 *pi4RetValIfOperStatus)
{
    UINT1               u1OperStatus = 0;
    UINT1               u1BrgPortType = 0;
    UINT4               u4UapIfIndex = 0;
    UINT2               u2Svid = 0;

    CfaGetIfOperStatus ((UINT4) i4IfIndex, &u1OperStatus);

    CfaGetIfBrgPortType ((UINT4) i4IfIndex, &u1BrgPortType);
    if (u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        if (u1OperStatus == CFA_IF_UP)
        {
            *pi4RetValIfOperStatus = (INT4) CFA_IF_UP;
        }
        else                    /* OPER STATUS IS EITHER DOWN or NOT PRESENT FOR THIS S-CHANNEL */
        {
            /* get the UAP ID for this s-channel */
            VlanApiGetSChInfoFromSChIfIndex ((UINT4) i4IfIndex, &u4UapIfIndex,
                                             &u2Svid);
            /* Retreiving the Oper Status of the UAP */
            CfaGetIfOperStatus (u4UapIfIndex, &u1OperStatus);
            if (u1OperStatus == CFA_IF_NP)
            {
                *pi4RetValIfOperStatus = (INT4) CFA_IF_NP;
            }
            else
            {
                *pi4RetValIfOperStatus = (INT4) CFA_IF_DOWN;
            }

        }
    }
    else
    {
        *pi4RetValIfOperStatus = (INT4) u1OperStatus;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfLastChange
 Input    :    The Indices
        IfIndex

        The Object
        retValIfLastChange
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfLastChange (INT4 i4IfIndex, UINT4 *pu4RetValIfLastChange)
{

    UINT4               u4IfIndex;

    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        *pu4RetValIfLastChange = CFA_IF_CHNG (u4IfIndex);
    }
    else
    {
        *pu4RetValIfLastChange = 0;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfInOctets
 Input    :    The Indices
        IfIndex

        The Object
        retValIfInOctets
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfInOctets (INT4 i4IfIndex, UINT4 *pu4RetValIfInOctets)
{

    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
    tCfaIfInfo         *pCfaIfInfo = NULL;
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (i4IfIndex);

    if (pCfaIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCfaIfInfo->u4IpIntfStats == CFA_DISABLED)
    {
        *pu4RetValIfInOctets = 0;
        return SNMP_SUCCESS;
    }
#endif

    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        *pu4RetValIfInOctets = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    /* If L3 switching is disabled, there will not be any 
     * hardware routing and hence we can rely on the software
     * to give us the correct values of l3 vlan counters*/
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        *pu4RetValIfInOctets = CFA_IF_GET_IN_OCTETS (u4IfIndex);
        return SNMP_SUCCESS;
    }
#endif
    if (u4IfIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG)
    {
        CfaFsHwGetVlanIntfStats (u4IfIndex, NP_STAT_IF_IN_OCTETS,
                                 pu4RetValIfInOctets);
    }
    else
    {
        CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_IN_OCTETS, pu4RetValIfInOctets);
    }
#else
    *pu4RetValIfInOctets = CFA_IF_GET_IN_OCTETS (u4IfIndex);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfInUcastPkts
 Input    :    The Indices
        IfIndex

        The Object
        retValIfInUcastPkts
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfInUcastPkts (INT4 i4IfIndex, UINT4 *pu4RetValIfInUcastPkts)
{

    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
    tCfaIfInfo         *pCfaIfInfo = NULL;
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (i4IfIndex);

    if (pCfaIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCfaIfInfo->u4IpIntfStats == CFA_DISABLED)
    {
        *pu4RetValIfInUcastPkts = 0;
        return SNMP_SUCCESS;
    }
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        *pu4RetValIfInUcastPkts = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        *pu4RetValIfInUcastPkts = CFA_IF_GET_IN_UCAST (u4IfIndex);
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_IN_UCAST_PKTS,
                    pu4RetValIfInUcastPkts);
#else
    *pu4RetValIfInUcastPkts = CFA_IF_GET_IN_UCAST (u4IfIndex);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfInNUcastPkts
 Input    :    The Indices
        IfIndex

        The Object
        retValIfInNUcastPkts
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfInNUcastPkts (INT4 i4IfIndex, UINT4 *pu4RetValIfInNUcastPkts)
{

    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
    tCfaIfInfo         *pCfaIfInfo = NULL;

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (i4IfIndex);

    if (pCfaIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCfaIfInfo->u4IpIntfStats == CFA_DISABLED)
    {
        *pu4RetValIfInNUcastPkts = 0;
        return SNMP_SUCCESS;
    }
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        *pu4RetValIfInNUcastPkts = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        *pu4RetValIfInNUcastPkts = CFA_IF_GET_IN_NUCAST (u4IfIndex);
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_IN_NUCAST_PKTS,
                    pu4RetValIfInNUcastPkts);
#else
    *pu4RetValIfInNUcastPkts = CFA_IF_GET_IN_NUCAST (u4IfIndex);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfInDiscards
 Input    :    The Indices
        IfIndex

        The Object
        retValIfInDiscards
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfInDiscards (INT4 i4IfIndex, UINT4 *pu4RetValIfInDiscards)
{

    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
    tCfaIfInfo         *pCfaIfInfo = NULL;

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (i4IfIndex);

    if (pCfaIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCfaIfInfo->u4IpIntfStats == CFA_DISABLED)
    {
        *pu4RetValIfInDiscards = 0;
        return SNMP_SUCCESS;
    }
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        *pu4RetValIfInDiscards = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        *pu4RetValIfInDiscards = CFA_IF_GET_IN_DISCARD (u4IfIndex);
        return SNMP_SUCCESS;
    }
#endif
    if (u4IfIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG)
    {
        CfaFsHwGetVlanIntfStats (u4IfIndex, NP_STAT_IF_IN_DISCARDS,
                                 pu4RetValIfInDiscards);
    }
    else
    {
        CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_IN_DISCARDS,
                        pu4RetValIfInDiscards);
    }
#else
    *pu4RetValIfInDiscards = CFA_IF_GET_IN_DISCARD (u4IfIndex);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfInErrors
 Input    :    The Indices
        IfIndex

        The Object
        retValIfInErrors
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfInErrors (INT4 i4IfIndex, UINT4 *pu4RetValIfInErrors)
{

    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
    tCfaIfInfo         *pCfaIfInfo = NULL;

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (i4IfIndex);

    if (pCfaIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCfaIfInfo->u4IpIntfStats == CFA_DISABLED)
    {
        *pu4RetValIfInErrors = 0;
        return SNMP_SUCCESS;
    }
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        *pu4RetValIfInErrors = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        *pu4RetValIfInErrors = CFA_IF_GET_IN_ERR (u4IfIndex);
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_IN_ERRORS, pu4RetValIfInErrors);
#else
    *pu4RetValIfInErrors = CFA_IF_GET_IN_ERR (u4IfIndex);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfInUnknownProtos
 Input    :    The Indices
        IfIndex

        The Object
        retValIfInUnknownProtos
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfInUnknownProtos (INT4 i4IfIndex, UINT4 *pu4RetValIfInUnknownProtos)
{

    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
    tCfaIfInfo         *pCfaIfInfo = NULL;

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (i4IfIndex);

    if (pCfaIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCfaIfInfo->u4IpIntfStats == CFA_DISABLED)
    {
        *pu4RetValIfInUnknownProtos = 0;
        return SNMP_SUCCESS;
    }
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        *pu4RetValIfInUnknownProtos = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        *pu4RetValIfInUnknownProtos = CFA_IF_GET_IN_UNK (u4IfIndex);
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_IN_UNKNOWN_PROTOS,
                    pu4RetValIfInUnknownProtos);
#else
    *pu4RetValIfInUnknownProtos = CFA_IF_GET_IN_UNK (u4IfIndex);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfOutOctets
 Input    :    The Indices
        IfIndex

        The Object
        retValIfOutOctets
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfOutOctets (INT4 i4IfIndex, UINT4 *pu4RetValIfOutOctets)
{

    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
    tCfaIfInfo         *pCfaIfInfo = NULL;
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (i4IfIndex);

    if (pCfaIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCfaIfInfo->u4IpIntfStats == CFA_DISABLED)
    {
        *pu4RetValIfOutOctets = 0;
        return SNMP_SUCCESS;
    }
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        *pu4RetValIfOutOctets = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        *pu4RetValIfOutOctets = CFA_IF_GET_OUT_OCTETS (u4IfIndex);
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_OUT_OCTETS, pu4RetValIfOutOctets);
#else
    *pu4RetValIfOutOctets = CFA_IF_GET_OUT_OCTETS (u4IfIndex);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfOutUcastPkts
 Input    :    The Indices
        IfIndex

        The Object
        retValIfOutUcastPkts
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfOutUcastPkts (INT4 i4IfIndex, UINT4 *pu4RetValIfOutUcastPkts)
{

    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
    tCfaIfInfo         *pCfaIfInfo = NULL;

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (i4IfIndex);

    if (pCfaIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCfaIfInfo->u4IpIntfStats == CFA_DISABLED)
    {
        *pu4RetValIfOutUcastPkts = 0;
        return SNMP_SUCCESS;
    }
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        *pu4RetValIfOutUcastPkts = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        *pu4RetValIfOutUcastPkts = CFA_IF_GET_OUT_UCAST (u4IfIndex);
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_OUT_UCAST_PKTS,
                    pu4RetValIfOutUcastPkts);
#else
    *pu4RetValIfOutUcastPkts = CFA_IF_GET_OUT_UCAST (u4IfIndex);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfOutNUcastPkts
 Input    :    The Indices
        IfIndex

        The Object
        retValIfOutNUcastPkts
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfOutNUcastPkts (INT4 i4IfIndex, UINT4 *pu4RetValIfOutNUcastPkts)
{

    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
    tCfaIfInfo         *pCfaIfInfo = NULL;
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (i4IfIndex);

    if (pCfaIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCfaIfInfo->u4IpIntfStats == CFA_DISABLED)
    {
        *pu4RetValIfOutNUcastPkts = 0;
        return SNMP_SUCCESS;
    }
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        *pu4RetValIfOutNUcastPkts = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        *pu4RetValIfOutNUcastPkts = CFA_IF_GET_OUT_NUCAST (u4IfIndex);
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_OUT_NUCAST_PKTS,
                    pu4RetValIfOutNUcastPkts);
#else
    *pu4RetValIfOutNUcastPkts = CFA_IF_GET_OUT_NUCAST (u4IfIndex);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfOutDiscards
 Input    :    The Indices
        IfIndex

        The Object
        retValIfOutDiscards
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfOutDiscards (INT4 i4IfIndex, UINT4 *pu4RetValIfOutDiscards)
{

    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
    tCfaIfInfo         *pCfaIfInfo = NULL;
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (i4IfIndex);

    if (pCfaIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCfaIfInfo->u4IpIntfStats == CFA_DISABLED)
    {
        *pu4RetValIfOutDiscards = 0;
        return SNMP_SUCCESS;
    }
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        *pu4RetValIfOutDiscards = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        *pu4RetValIfOutDiscards = CFA_IF_GET_OUT_DISCARD (u4IfIndex);
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_OUT_DISCARDS, pu4RetValIfOutDiscards);
#else
    *pu4RetValIfOutDiscards = CFA_IF_GET_OUT_DISCARD (u4IfIndex);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfOutErrors
 Input    :    The Indices
        IfIndex

        The Object
        retValIfOutErrors
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfOutErrors (INT4 i4IfIndex, UINT4 *pu4RetValIfOutErrors)
{

    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
    tCfaIfInfo         *pCfaIfInfo = NULL;
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (i4IfIndex);

    if (pCfaIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCfaIfInfo->u4IpIntfStats == CFA_DISABLED)
    {
        *pu4RetValIfOutErrors = 0;
        return SNMP_SUCCESS;
    }
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        *pu4RetValIfOutErrors = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        *pu4RetValIfOutErrors = CFA_IF_GET_OUT_ERR (u4IfIndex);
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_OUT_ERRORS, pu4RetValIfOutErrors);
#else
    *pu4RetValIfOutErrors = CFA_IF_GET_OUT_ERR (u4IfIndex);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfOutQLen
 Input    :    The Indices
        IfIndex

        The Object
        retValIfOutQLen
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfOutQLen (INT4 i4IfIndex, UINT4 *pu4RetValIfOutQLen)
{

    UINT4               u4IfIndex;
    u4IfIndex = (UINT4) i4IfIndex;

    *pu4RetValIfOutQLen = 0;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface (u4IfIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_OUT_QLEN, pu4RetValIfOutQLen);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfSpecific
 Input    :    The Indices
        IfIndex

        The Object
        retValIfSpecific
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfSpecific (INT4 i4IfIndex, tSNMP_OID_TYPE * pRetValIfSpecific)
{

    UINT4               u4IfIndex;
    UINT4               au4PppLcpOid[14] =
        { 1, 3, 6, 1, 2, 1, 10, 23, 1, 1, 1, 1, 1 };

    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

    au4PppLcpOid[13] = u4IfIndex;

    MEMCPY (pRetValIfSpecific->pu4_OidList, au4PppLcpOid,
            sizeof (au4PppLcpOid));
    pRetValIfSpecific->u4_Length = sizeof (au4PppLcpOid) / sizeof (UINT4);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :    nmhSetExIfAdminStatus
 Input    :    The Indices
        IfIndex

        The Object
        setValIfAdminStatus
 Output    :    The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfAdminStatus (INT4 i4IfIndex, INT4 i4SetValIfAdminStatus)
{

    return (nmhSetIfMainAdminStatus (i4IfIndex, i4SetValIfAdminStatus));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :    nmhTestv2IfAdminStatus
 Input    :    The Indices
        IfIndex

        The Object
        testValIfAdminStatus
 Output    :    The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
 Error Codes :    The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfAdminStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                        INT4 i4TestValIfAdminStatus)
{

    return (nmhTestv2IfMainAdminStatus (pu4ErrorCode, i4IfIndex,
                                        i4TestValIfAdminStatus));

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IfTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IfXTable. */

/****************************************************************************
 Function    :    nmhValidateIndexInstanceIfXTable
 Input    :    The Indices
        IfIndex
 Output    :    The Routines Validates the Given Indices.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIfXTable (INT4 i4IfIndex)
{

    UINT4               u4IfIndex = 0;

    if (((UINT4) i4IfIndex > CFA_MAX_INTERFACES ()) || (i4IfIndex <= 0))
    {
        return (SNMP_FAILURE);
    }

    u4IfIndex = (UINT4) i4IfIndex;

    if (((CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE) &&
         (CFA_IF_ENTRY_USED (u4IfIndex))) ||
        ((CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE) &&
         (CfaValidateCfaIfIndex (u4IfIndex) == CFA_SUCCESS)))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :    nmhGetFirstIndexIfXTable
 Input    :    The Indices
        IfIndex
 Output    :    The Get First Routines gets the Lexicographicaly
        First Entry from the Table.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIfXTable (INT4 *pi4IfIndex)
{

    UINT4               u4Index = 1;

    CFA_CDB_SCAN_WITH_LOCK (u4Index, (CFA_MAX_INTERFACES ()), CFA_ALL_IFTYPE)
    {
        if (((CfaIsIfEntryProgrammingAllowed (u4Index) == CFA_TRUE) &&
             (CFA_IF_ENTRY_USED (u4Index))) ||
            ((CfaIsIfEntryProgrammingAllowed (u4Index) == CFA_FALSE) &&
             (CfaValidateCfaIfIndex (u4Index) == CFA_SUCCESS)))
        {
            *pi4IfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :    nmhGetNextIndexIfXTable
 Input    :    The Indices
        IfIndex
        nextIfIndex
 Output    :    The Get Next function gets the Next Index for
        the Index Value given in the Index Values. The
        Indices are stored in the next_varname variables.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexIfXTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{

    UINT4               u4IfIndex;
    UINT4               u4Index;

    if (i4IfIndex < 0 || (UINT4) i4IfIndex > CFA_MAX_INTERFACES ())
    {
        return SNMP_FAILURE;
    }
    u4IfIndex = (UINT4) i4IfIndex;
    u4Index = u4IfIndex + 1;

    CFA_CDB_SCAN_WITH_LOCK (u4Index, (CFA_MAX_INTERFACES ()), CFA_ALL_IFTYPE)
    {
        if (((CfaIsIfEntryProgrammingAllowed (u4Index) == CFA_TRUE) &&
             (CFA_IF_ENTRY_USED (u4Index))) ||
            ((CfaIsIfEntryProgrammingAllowed (u4Index) == CFA_FALSE) &&
             (CfaValidateCfaIfIndex (u4Index) == CFA_SUCCESS)))
        {
            *pi4NextIfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :    nmhGetIfName
 Input    :    The Indices
        IfIndex

        The Object
        retValIfName
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfName (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pRetValIfName)
{
    UINT1               au1IfName[CFA_MAX_IFALIAS_LENGTH];
    UINT4               u4IfIndex;

    MEMSET (au1IfName, 0, CFA_MAX_IFALIAS_LENGTH);

    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsMgmtPort (u4IfIndex) == TRUE)
    {
        STRNCPY (au1IfName, OOB_ALIAS_NAME,
                 sizeof (au1IfName) - CFA_STR_DELIM_SIZE);
        au1IfName[STRLEN (au1IfName)] = '\0';
        pRetValIfName->i4_Length = STRLEN (au1IfName);
    }
    else
    {
        CfaGetIfName (u4IfIndex, au1IfName);
        pRetValIfName->i4_Length = (INT4) STRLEN (au1IfName);
    }

    MEMCPY (pRetValIfName->pu1_OctetList, au1IfName, pRetValIfName->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfInMulticastPkts
 Input    :    The Indices
        IfIndex

        The Object
        retValIfInMulticastPkts
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfInMulticastPkts (INT4 i4IfIndex, UINT4 *pu4RetValIfInMulticastPkts)
{

    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif

    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        *pu4RetValIfInMulticastPkts = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        *pu4RetValIfInMulticastPkts = CFA_IF_GET_IN_MCAST (u4IfIndex);
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_IN_MCAST_PKTS,
                    pu4RetValIfInMulticastPkts);
#else
    *pu4RetValIfInMulticastPkts = CFA_IF_GET_IN_MCAST (u4IfIndex);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfInBroadcastPkts
 Input    :    The Indices
        IfIndex

        The Object
        retValIfInBroadcastPkts
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfInBroadcastPkts (INT4 i4IfIndex, UINT4 *pu4RetValIfInBroadcastPkts)
{

    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        *pu4RetValIfInBroadcastPkts = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        *pu4RetValIfInBroadcastPkts = CFA_IF_GET_IN_BCAST (u4IfIndex);
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_IN_BCAST_PKTS,
                    pu4RetValIfInBroadcastPkts);
#else
    *pu4RetValIfInBroadcastPkts = CFA_IF_GET_IN_BCAST (u4IfIndex);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfOutMulticastPkts
 Input    :    The Indices
        IfIndex

        The Object
        retValIfOutMulticastPkts
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfOutMulticastPkts (INT4 i4IfIndex, UINT4 *pu4RetValIfOutMulticastPkts)
{

    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        *pu4RetValIfOutMulticastPkts = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        *pu4RetValIfOutMulticastPkts = CFA_IF_GET_OUT_MCAST (u4IfIndex);
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_OUT_MCAST_PKTS,
                    pu4RetValIfOutMulticastPkts);
#else
    *pu4RetValIfOutMulticastPkts = CFA_IF_GET_OUT_MCAST (u4IfIndex);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfOutBroadcastPkts
 Input    :    The Indices
        IfIndex

        The Object
        retValIfOutBroadcastPkts
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfOutBroadcastPkts (INT4 i4IfIndex, UINT4 *pu4RetValIfOutBroadcastPkts)
{

    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        *pu4RetValIfOutBroadcastPkts = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        *pu4RetValIfOutBroadcastPkts = CFA_IF_GET_OUT_BCAST (u4IfIndex);
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_OUT_BCAST_PKTS,
                    pu4RetValIfOutBroadcastPkts);
#else
    *pu4RetValIfOutBroadcastPkts = CFA_IF_GET_OUT_BCAST (u4IfIndex);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfHCInOctets
 Input    :    The Indices
        IfIndex

        The Object
        retValIfHCInOctets
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfHCInOctets (INT4 i4IfIndex,
                    tSNMP_COUNTER64_TYPE * pu8RetValIfHCInOctets)
{

    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        pu8RetValIfHCInOctets->msn = 0;
        pu8RetValIfHCInOctets->lsn = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        pu8RetValIfHCInOctets->msn = CFA_IF_GET_IN_HC_OCTETS (u4IfIndex);
        pu8RetValIfHCInOctets->lsn = CFA_IF_GET_IN_OCTETS (u4IfIndex);
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat64 (u4IfIndex, NP_STAT_IF_HC_IN_OCTETS,
                      pu8RetValIfHCInOctets);
#else
    pu8RetValIfHCInOctets->msn = CFA_IF_GET_IN_HC_OCTETS (u4IfIndex);
    pu8RetValIfHCInOctets->lsn = CFA_IF_GET_IN_OCTETS (u4IfIndex);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfHCInUcastPkts
 Input    :    The Indices
        IfIndex

        The Object
        retValIfHCInUcastPkts
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfHCInUcastPkts (INT4 i4IfIndex,
                       tSNMP_COUNTER64_TYPE * pu8RetValIfHCInUcastPkts)
{
    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        pu8RetValIfHCInUcastPkts->msn = 0;
        pu8RetValIfHCInUcastPkts->lsn = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        pu8RetValIfHCInUcastPkts->msn = 0;
        pu8RetValIfHCInUcastPkts->lsn = 0;
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat64 (u4IfIndex, NP_STAT_IF_HC_IN_UCAST_PKTS,
                      pu8RetValIfHCInUcastPkts);
#else
    pu8RetValIfHCInUcastPkts->msn = 0;
    pu8RetValIfHCInUcastPkts->lsn = 0;
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfHCInMulticastPkts
 Input    :    The Indices
        IfIndex

        The Object
        retValIfHCInMulticastPkts
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfHCInMulticastPkts (INT4 i4IfIndex,
                           tSNMP_COUNTER64_TYPE * pu8RetValIfHCInMulticastPkts)
{
    UINT4               u4IfIndex;

#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        pu8RetValIfHCInMulticastPkts->msn = 0;
        pu8RetValIfHCInMulticastPkts->lsn = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        pu8RetValIfHCInMulticastPkts->msn = 0;
        pu8RetValIfHCInMulticastPkts->lsn = 0;
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat64 (u4IfIndex, NP_STAT_IF_HC_IN_MCAST_PKTS,
                      pu8RetValIfHCInMulticastPkts);
#else
    pu8RetValIfHCInMulticastPkts->msn = 0;
    pu8RetValIfHCInMulticastPkts->lsn = 0;
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfHCInBroadcastPkts
 Input    :    The Indices
        IfIndex

        The Object
        retValIfHCInBroadcastPkts
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfHCInBroadcastPkts (INT4 i4IfIndex,
                           tSNMP_COUNTER64_TYPE * pu8RetValIfHCInBroadcastPkts)
{

    UINT4               u4IfIndex;

#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        pu8RetValIfHCInBroadcastPkts->msn = 0;
        pu8RetValIfHCInBroadcastPkts->lsn = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        pu8RetValIfHCInBroadcastPkts->msn = 0;
        pu8RetValIfHCInBroadcastPkts->lsn = 0;
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat64 (u4IfIndex, NP_STAT_IF_HC_IN_BCAST_PKTS,
                      pu8RetValIfHCInBroadcastPkts);
#else
    pu8RetValIfHCInBroadcastPkts->msn = 0;
    pu8RetValIfHCInBroadcastPkts->lsn = 0;
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfHCOutOctets
 Input    :    The Indices
        IfIndex

        The Object
        retValIfHCOutOctets
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfHCOutOctets (INT4 i4IfIndex,
                     tSNMP_COUNTER64_TYPE * pu8RetValIfHCOutOctets)
{

    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        pu8RetValIfHCOutOctets->msn = 0;
        pu8RetValIfHCOutOctets->lsn = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        pu8RetValIfHCOutOctets->msn = CFA_IF_GET_OUT_HC_OCTETS (u4IfIndex);
        pu8RetValIfHCOutOctets->lsn = CFA_IF_GET_OUT_OCTETS (u4IfIndex);
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat64 (u4IfIndex, NP_STAT_IF_HC_OUT_OCTETS,
                      pu8RetValIfHCOutOctets);
#else
    pu8RetValIfHCOutOctets->msn = CFA_IF_GET_OUT_HC_OCTETS (u4IfIndex);
    pu8RetValIfHCOutOctets->lsn = CFA_IF_GET_OUT_OCTETS (u4IfIndex);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfHCOutUcastPkts
 Input    :    The Indices
        IfIndex

        The Object
        retValIfHCOutUcastPkts
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfHCOutUcastPkts (INT4 i4IfIndex,
                        tSNMP_COUNTER64_TYPE * pu8RetValIfHCOutUcastPkts)
{
    UINT4               u4IfIndex;

#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        pu8RetValIfHCOutUcastPkts->msn = 0;
        pu8RetValIfHCOutUcastPkts->lsn = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        pu8RetValIfHCOutUcastPkts->msn = 0;
        pu8RetValIfHCOutUcastPkts->lsn = 0;
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat64 (u4IfIndex, NP_STAT_IF_HC_OUT_UCAST_PKTS,
                      pu8RetValIfHCOutUcastPkts);
#else
    pu8RetValIfHCOutUcastPkts->msn = 0;
    pu8RetValIfHCOutUcastPkts->lsn = 0;
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfHCOutMulticastPkts
 Input    :    The Indices
        IfIndex

        The Object
        retValIfHCOutMulticastPkts
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfHCOutMulticastPkts (INT4 i4IfIndex,
                            tSNMP_COUNTER64_TYPE *
                            pu8RetValIfHCOutMulticastPkts)
{
    UINT4               u4IfIndex;

#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        pu8RetValIfHCOutMulticastPkts->msn = 0;
        pu8RetValIfHCOutMulticastPkts->lsn = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        pu8RetValIfHCOutMulticastPkts->msn = 0;
        pu8RetValIfHCOutMulticastPkts->lsn = 0;
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat64 (u4IfIndex, NP_STAT_IF_HC_OUT_MCAST_PKTS,
                      pu8RetValIfHCOutMulticastPkts);
#else
    pu8RetValIfHCOutMulticastPkts->msn = 0;
    pu8RetValIfHCOutMulticastPkts->lsn = 0;
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfHCOutBroadcastPkts
 Input    :    The Indices
        IfIndex

        The Object
        retValIfHCOutBroadcastPkts
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfHCOutBroadcastPkts (INT4 i4IfIndex,
                            tSNMP_COUNTER64_TYPE *
                            pu8RetValIfHCOutBroadcastPkts)
{
    UINT4               u4IfIndex;

#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        pu8RetValIfHCOutBroadcastPkts->msn = 0;
        pu8RetValIfHCOutBroadcastPkts->lsn = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        pu8RetValIfHCOutBroadcastPkts->msn = 0;
        pu8RetValIfHCOutBroadcastPkts->lsn = 0;
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat64 (u4IfIndex, NP_STAT_IF_HC_OUT_BCAST_PKTS,
                      pu8RetValIfHCOutBroadcastPkts);
#else
    pu8RetValIfHCOutBroadcastPkts->msn = 0;
    pu8RetValIfHCOutBroadcastPkts->lsn = 0;
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfLinkUpDownTrapEnable
 Input    :    The Indices
        IfIndex

        The Object
        retValIfLinkUpDownTrapEnable
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfLinkUpDownTrapEnable (INT4 i4IfIndex,
                              INT4 *pi4RetValIfLinkUpDownTrapEnable)
{

    UINT4               u4IfIndex;

    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        *pi4RetValIfLinkUpDownTrapEnable = (INT4) CFA_IF_TRAP_EN (u4IfIndex);
        return SNMP_SUCCESS;
    }
    else
    {
        *pi4RetValIfLinkUpDownTrapEnable = CFA_DISABLED;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfHighSpeed
 Input    :    The Indices
        IfIndex

        The Object
        retValIfHighSpeed
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfHighSpeed (INT4 i4IfIndex, UINT4 *pu4RetValIfHighSpeed)
{
    UINT4               u4IfIndex;
    UINT4               u4IfSpeed;

    u4IfIndex = (UINT4) i4IfIndex;
#ifdef NPAPI_WANTED
    CfaDetermineSpeed (u4IfIndex, &u4IfSpeed, pu4RetValIfHighSpeed);
#else
    CfaGetIfHighSpeed (u4IfIndex, pu4RetValIfHighSpeed);
    UNUSED_PARAM (u4IfSpeed);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfPromiscuousMode
 Input    :    The Indices
        IfIndex

        The Object
        retValIfPromiscuousMode
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfPromiscuousMode (INT4 i4IfIndex, INT4 *pi4RetValIfPromiscuousMode)
{

    UINT4               u4IfIndex;

    u4IfIndex = (UINT4) i4IfIndex;
    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        *pi4RetValIfPromiscuousMode = CFA_FALSE;
        return SNMP_SUCCESS;
    }
    else
    {
        *pi4RetValIfPromiscuousMode = (INT4) CFA_IF_PROMISC (u4IfIndex);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfConnectorPresent
 Input    :    The Indices
        IfIndex

        The Object
        retValIfConnectorPresent
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIfConnectorPresent ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIfConnectorPresent (INT4 i4IfIndex, INT4 *pi4RetValIfConnectorPresent)
#else
INT1
nmhGetIfConnectorPresent (i4IfIndex, pi4RetValIfConnectorPresent)
     INT4                i4IfIndex;
     INT4               *pi4RetValIfConnectorPresent;
#endif
{
    UINT1               u1IfType = CFA_IF_DOWN;

    UINT4               u4IfIndex;

    u4IfIndex = (UINT4) i4IfIndex;

    CfaGetIfType (u4IfIndex, &u1IfType);
    if (u1IfType == CFA_ENET)
    {
        *pi4RetValIfConnectorPresent = CFA_TRUE;
    }
    else
    {
        *pi4RetValIfConnectorPresent = CFA_FALSE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfAlias
 Input    :    The Indices
        IfIndex

        The Object
        retValIfAlias
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIfAlias ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIfAlias (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pRetValIfAlias)
#else
INT1
nmhGetIfAlias (i4IfIndex, pRetValIfAlias)
     INT4                i4IfIndex;
     tSNMP_OCTET_STRING_TYPE *pRetValIfAlias;
#endif
{
    UINT1               au1IfAlias[CFA_MAX_IFALIAS_LENGTH];
    UINT4               u4IfIndex;

    u4IfIndex = (UINT4) i4IfIndex;

    MEMSET (au1IfAlias, 0, CFA_MAX_IFALIAS_LENGTH);
    CfaGetIfAlias (u4IfIndex, au1IfAlias);

    pRetValIfAlias->i4_Length = (INT4) STRLEN (au1IfAlias);

    MEMCPY (pRetValIfAlias->pu1_OctetList, au1IfAlias,
            pRetValIfAlias->i4_Length);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfCounterDiscontinuityTime
 Input    :    The Indices
        IfIndex

        The Object
        retValIfCounterDiscontinuityTime
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIfCounterDiscontinuityTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIfCounterDiscontinuityTime (INT4 i4IfIndex,
                                  UINT4 *pu4RetValIfCounterDiscontinuityTime)
#else
INT1
nmhGetIfCounterDiscontinuityTime (i4IfIndex,
                                  pu4RetValIfCounterDiscontinuityTime)
     INT4                i4IfIndex;
     UINT4              *pu4RetValIfCounterDiscontinuityTime;
#endif
{
    UNUSED_PARAM (i4IfIndex);
    *pu4RetValIfCounterDiscontinuityTime = 0;
    UNUSED_PARAM (pu4RetValIfCounterDiscontinuityTime);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :    nmhSetExIfLinkUpDownTrapEnable
 Input    :    The Indices
        IfIndex

        The Object
        setValIfLinkUpDownTrapEnable
 Output    :    The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIfLinkUpDownTrapEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetIfLinkUpDownTrapEnable (INT4 i4IfIndex,
                              INT4 i4SetValIfLinkUpDownTrapEnable)
#else
INT1
nmhSetIfLinkUpDownTrapEnable (i4IfIndex, i4SetValIfLinkUpDownTrapEnable)
     INT4                i4IfIndex;
     INT4                i4SetValIfLinkUpDownTrapEnable;

#endif
{

    UINT4               u4IfIndex;

    u4IfIndex = (UINT4) i4IfIndex;
    /* Null check has to be provided here to avoid crash as MSR does 
     * not callTest routine and verify the existence of the interface*/
    if (CFA_IF_ENTRY (u4IfIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        CFA_IF_TRAP_EN (u4IfIndex) = (UINT1) i4SetValIfLinkUpDownTrapEnable;
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :    nmhSetExIfPromiscuousMode
 Input    :    The Indices
        IfIndex

        The Object
        setValIfPromiscuousMode
 Output    :    The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIfPromiscuousMode ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetIfPromiscuousMode (INT4 i4IfIndex, INT4 i4SetValIfPromiscuousMode)
#else
INT1
nmhSetIfPromiscuousMode (i4IfIndex, i4SetValIfPromiscuousMode)
     INT4                i4IfIndex;
     INT4                i4SetValIfPromiscuousMode;

#endif
{

    UINT4               u4IfIndex;
    UINT1               u1Command;
    INT4                i4RetVal = CFA_FAILURE;

    u4IfIndex = (UINT4) i4IfIndex;
    /* Null check has to be provided here to avoid crash as MSR does 
     * not callTest routine and verify the existence of the interface*/
    if (CFA_IF_ENTRY (u4IfIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }
    if (CFA_IF_PROMISC (u4IfIndex) == (UINT1) i4SetValIfPromiscuousMode)
        return SNMP_SUCCESS;

    if (i4SetValIfPromiscuousMode == CFA_ENABLED)
        u1Command = CFA_ENET_EN_PROMISC;
    else
        u1Command = CFA_ENET_DIS_PROMIS;

    if (CfaIsMgmtPort (u4IfIndex) == TRUE)
    {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
        /*Set promiscous mode forOOB Interface */
        i4RetVal = CfaGddOobConfigPort (u4IfIndex, u1Command);
#endif
    }
    else
    {
        /*Set promiscous mode for the port */
        i4RetVal = CfaGddConfigPort (u4IfIndex, u1Command, NULL);
    }
    if (i4RetVal != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfAlias
 Input       :  The Indices
                IfIndex

                The Object 
                setValIfAlias
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIfAlias ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetIfAlias (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pSetValIfAlias)
#else
INT1
nmhSetIfAlias (i4IfIndex, pSetValIfAlias)
     INT4                i4IfIndex;
     tSNMP_OCTET_STRING_TYPE *pSetValIfAlias;

#endif
{
   /*** $$TRACE_LOG (ENTRY, "IfIndex = %d\n", i4IfIndex); ***/
#if defined (WLC_WANTED) || defined (WTP_WANTED)
    UINT1               u1IfType = 0;
#endif
    UINT4               u4IfIndex = (UINT4) i4IfIndex;
    UINT1               au1IfAlias[CFA_MAX_IFALIAS_LENGTH];

    if (pSetValIfAlias->i4_Length == 0)
    {
        /*Skip if the incoming value is NULL */
        return SNMP_FAILURE;
    }

    MEMSET (au1IfAlias, 0, CFA_MAX_IFALIAS_LENGTH);
    MEMCPY (au1IfAlias, pSetValIfAlias->pu1_OctetList,
            pSetValIfAlias->i4_Length);
    au1IfAlias[pSetValIfAlias->i4_Length] = '\0';
    CfaSetIfAlias (u4IfIndex, au1IfAlias);

#if defined (WLC_WANTED) || defined (WTP_WANTED)
    /* Notify the change to LLDP for interfaces other than CAPWAP 
     * and physical radio interfaces */
    CfaGetIfType (u4IfIndex, &u1IfType);

    if (u1IfType != CFA_CAPWAP_DOT11_BSS)
    {
#ifdef WTP_WANTED
        if ((CfaIsWssIntf (u4IfIndex) != CFA_FALSE) ||
            (CfaIsRadioIntf (u4IfIndex) != CFA_FALSE))
#else
        if (CfaIsWssIntf (u4IfIndex) != CFA_FALSE)
#endif
        {
            return SNMP_SUCCESS;
        }
    }
#endif
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :    nmhTestv2IfLinkUpDownTrapEnable
 Input    :    The Indices
        IfIndex

        The Object
        testValIfLinkUpDownTrapEnable
 Output    :    The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
 Error Codes :    The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IfLinkUpDownTrapEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2IfLinkUpDownTrapEnable (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                 INT4 i4TestValIfLinkUpDownTrapEnable)
#else
INT1
nmhTestv2IfLinkUpDownTrapEnable (*pu4ErrorCode, i4IfIndex,
                                 i4TestValIfLinkUpDownTrapEnable)
     UINT4              *pu4ErrorCode;
     INT4                i4IfIndex;
     INT4                i4TestValIfLinkUpDownTrapEnable;
#endif
{
    UINT4               u4IfIndex;
    UINT1               u1BrgPortType = 0;

    u4IfIndex = (UINT4) i4IfIndex;

    /* Validate the interface Index. User might have deleted the interface */

    if ((nmhValidateIndexInstanceIfTable (i4IfIndex) == SNMP_FAILURE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    CfaGetIfBrgPortType (u4IfIndex, &u1BrgPortType);

    if (u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        /* Setting LinkUpDownTrap is not applicable for
         * S-Channel Interface */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_SBP_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValIfLinkUpDownTrapEnable == CFA_ENABLED) ||
        (i4TestValIfLinkUpDownTrapEnable == CFA_DISABLED))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :    nmhTestv2IfPromiscuousMode
 Input    :    The Indices
        IfIndex

        The Object
        testValIfPromiscuousMode
 Output    :    The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
 Error Codes :    The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IfPromiscuousMode ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2IfPromiscuousMode (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                            INT4 i4TestValIfPromiscuousMode)
#else
INT1
nmhTestv2IfPromiscuousMode (*pu4ErrorCode, i4IfIndex,
                            i4TestValIfPromiscuousMode)
     UINT4              *pu4ErrorCode;
     INT4                i4IfIndex;
     INT4                i4TestValIfPromiscuousMode;
#endif
{

    UINT4               u4IfIndex;
    UINT1               u1IfType = CFA_IF_DOWN;

    u4IfIndex = (UINT4) i4IfIndex;

    /* Validate the interface Index. User might have deleted the interface */

    if ((nmhValidateIndexInstanceIfTable (i4IfIndex) == SNMP_FAILURE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* cannot change the PromiscuousMode for the default router interface */
    if (u4IfIndex == CFA_DEFAULT_ROUTER_IFINDEX)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    CfaGetIfType (u4IfIndex, &u1IfType);
    if (u1IfType == CFA_ENET)
    {
        if ((i4TestValIfPromiscuousMode == CFA_ENABLED) ||
            (i4TestValIfPromiscuousMode == CFA_DISABLED))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfAlias
 Input       :  The Indices
                IfIndex

                The Object
                testValIfAlias
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IfAlias ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2IfAlias (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                  tSNMP_OCTET_STRING_TYPE * pTestValIfAlias)
#else
INT1
nmhTestv2IfAlias (*pu4ErrorCode, i4IfIndex, pTestValIfAlias)
     UINT4              *pu4ErrorCode;
     INT4                i4IfIndex;
     tSNMP_OCTET_STRING_TYPE *pTestValIfAlias;
#endif
{

   /*** $$TRACE_LOG (ENTRY, "IfIndex = %d\n", i4IfIndex); ***/

    tL2PvlanMappingInfo L2PvlanMappingInfo;
    UINT4               u4IfIndex = (UINT4) i4IfIndex;
    UINT2               u2VlanId = 0;
    UINT1               u1IfType = 0;
    UINT4               u4TempIndex = 1;
    UINT1               au1Str[CFA_MAX_IFALIAS_LENGTH];
    UINT1              *pu1KeyStr = NULL;
    UINT4               u4KeyStrLen = 0;
    UINT4               u4TmpL2CxtId = 0;
    UINT4               u4L2CxtId = 0;
    INT4                i4Key = -1;
    INT4                i4IfName = 0;
    INT4                i4RetValue = 0;
    UINT1               au1IfName[CFA_MAX_IFALIAS_LENGTH];
    UINT1               u1Found = CFA_FALSE;

    UNUSED_PARAM (i4RetValue);

    MEMSET (au1IfName, 0, CFA_MAX_IFALIAS_LENGTH);
    MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));
    MEMSET (au1Str, 0, CFA_MAX_IFALIAS_LENGTH);
    /* Validate the interface Index. User might have deleted the interface */

    if ((nmhValidateIndexInstanceIfTable (i4IfIndex) == SNMP_FAILURE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

    switch (u1IfType)
    {
        case CFA_ENET:
#ifndef NPAPI_WANTED
/* for physical ports, the alias cannot be changed once the registration
takes place with the GDD as alias serves as an handle. */
            if (!(CFA_ENET_CREATE_DELETE_ALLOWED))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
#endif /* NPAPI_WANTED */
            /* Alias name is allowed to be modified if NPAPI is defined */
            /* Intentional fallthrough - to check whether 
             * the Alias name is already used */

        default:
            if (pTestValIfAlias->i4_Length > CFA_MAX_IFALIAS_LENGTH)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return SNMP_FAILURE;
            }

#ifdef SNMP_2_WANTED
            if (SNMPCheckForNVTChars (pTestValIfAlias->pu1_OctetList,
                                      pTestValIfAlias->i4_Length)
                == OSIX_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
#endif

            /* scan all interfaces and see if the same alias is being used
               by some other interface */

            CFA_CDB_SCAN_WITH_LOCK (u4TempIndex, SYS_DEF_MAX_INTERFACES,
                                    CFA_ALL_IFTYPE)
            {
                /* Flexible ifIndex Change */
                CFA_DS_LOCK ();

                if ((CFA_CDB_IF_BRIDGE_PORT_TYPE (u4TempIndex)
                     == CFA_VIRTUAL_INSTANCE_PORT)
                    || (CFA_CDB_IF_TYPE (u4TempIndex)
                        == CFA_PROP_VIRTUAL_INTERFACE))
                {
                    CFA_DS_UNLOCK ();
                    continue;
                }
                CFA_DS_UNLOCK ();

                if (CFA_IF_ENTRY_USED (u4TempIndex))
                {
                    CfaGetIfAlias (u4TempIndex, au1IfName);

                    if (STRCMP (au1IfName, pTestValIfAlias->pu1_OctetList) == 0)
                    {
                        u1Found = CFA_TRUE;
                        break;
                    }
                }
            }
            if ((u1Found == CFA_TRUE) && (u4IfIndex != u4TempIndex))
            {
                if ((u4IfIndex >= CFA_MIN_IVR_IF_INDEX)
                    && (u4IfIndex < CFA_MAX_IVR_IF_INDEX))
                {
                    i4RetValue =
                        VcmGetL2CxtIdForIpIface (u4TempIndex, &u4TmpL2CxtId);
                    i4RetValue =
                        VcmGetL2CxtIdForIpIface (u4IfIndex, &u4L2CxtId);
                    if (u4TmpL2CxtId == u4L2CxtId)
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return SNMP_FAILURE;
                    }
                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }

            break;
    }
    /* If the alias is for port-channel interface (CFA_LAGG), the string
     * should be of the form "po<KEY>" where KEY is the port-channel id
     * used as ActorAdminKey for the Aggregator in Link Aggregation module.
     * Validate the key for acceptable range.
     */
    MEMSET (au1Str, 0, sizeof (au1Str));
    MEMCPY (au1Str, pTestValIfAlias->pu1_OctetList,
            MEM_MAX_BYTES (pTestValIfAlias->i4_Length,
                           CFA_MAX_PORT_NAME_LENGTH));
    switch (u1IfType)
    {
        case CFA_LAGG:
            if (STRNCMP (au1Str, "po", 2) == 0)
            {
                u4KeyStrLen = STRNLEN (au1Str, CFA_MAX_PORT_NAME_LENGTH);
                while (u4KeyStrLen != 0)
                {
                    pu1KeyStr = &au1Str[2];
                    if (!ISDIGIT (*pu1KeyStr))
                    {
                        CLI_SET_ERR (CLI_CFA_ALIAS_ERR);
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return SNMP_FAILURE;
                    }
                    u4KeyStrLen--;
                }
                if (NULL != pu1KeyStr)
                {
                    i4Key = ATOL (pu1KeyStr);
                }
                pu1KeyStr = &au1Str[2];
                /* If the value is not within size of 16 bits, return */
                if ((i4Key <= 0) || (i4Key > 65535))
                {
                    CLI_SET_ERR (CLI_CFA_ALIAS_ERR);
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }

            }
            else
            {
                CLI_SET_ERR (CLI_CFA_ALIAS_ERR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        case CFA_L3IPVLAN:

            /* Secondary VLANs do not have L3 interfaces. If IVR interface
             * is created for a secondary VLAN return failure.
             */

            if (VcmGetL2CxtIdForIpIface (u4IfIndex, &u4L2CxtId) == VCM_FAILURE)
            {
                return SNMP_FAILURE;
            }

            L2PvlanMappingInfo.u4ContextId = u4L2CxtId;
            L2PvlanMappingInfo.u1RequestType = L2IWF_VLAN_TYPE;
            L2PvlanMappingInfo.InVlanId = u2VlanId;

            L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

            if ((L2PvlanMappingInfo.u1VlanType == L2IWF_ISOLATED_VLAN) ||
                (L2PvlanMappingInfo.u1VlanType == L2IWF_COMMUNITY_VLAN))
            {
                CLI_SET_ERR (CLI_CFA_IVR_VLAN_SEC_VLAN_ERR);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;
        case CFA_PROP_VIRTUAL_INTERFACE:
            if (STRNCMP (au1Str, "sisp", 4) == 0)
            {
                /* For SISP logical interfaces, the IfAlias should always be 
                 * formed using the convention string "sisp" followed by interface
                 * index. This needs to be checked
                 * */
                i4IfName = ATOL (&(au1Str[CFA_MAX_SISP_NAME_LEN]));

                if (!((i4IfName > 0) && (i4IfName < 65535)))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            else if (STRNCMP (au1Str, "virtual", 7) == 0)
            {
                /* Internal ports PIP & CBP can be created with alias name 
                 * as virtualX. X should be in the range 1 to MAX_INTERNAL_IFACES.
                 * VIP Interfaces can be created with alias name as virtualY.
                 * Y can be in the range MIN_VIP_IFINDEX to MAX_VIP_IFINDEX 
                 *
                 * Whenever a new Internal interface (PIP or CBP) is tried to 
                 * create with alias name greater than SYS_MAX_INTERNAL_IFACEs,
                 * error will be thrown*/

                i4IfName = ATOL (&(au1Str[7]));

                if (i4IfName == 0)
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }

                if (CfaIsVipInterface (u4IfIndex) == CFA_TRUE)
                {
                    if (!((i4IfName >= CFA_MIN_VIP_IF_INDEX) &&
                          (i4IfName <= CFA_MAX_VIP_IF_INDEX)))
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return SNMP_FAILURE;
                    }
                }
                else
                {
                    if (i4IfName > SYS_DEF_MAX_INTERNAL_IFACES)
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return SNMP_FAILURE;
                    }
                }
                break;
            }
        default:
            break;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IfXTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfXTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IfStackTable. */

/****************************************************************************
 Function    :    nmhValidateIndexInstanceIfStackTable
 Input    :    The Indices
        IfStackHigherLayer
        IfStackLowerLayer
 Output    :    The Routines Validates the Given Indices.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceIfStackTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceIfStackTable (INT4 i4IfStackHigherLayer,
                                      INT4 i4IfStackLowerLayer)
#else
INT1
nmhValidateIndexInstanceIfStackTable (i4IfStackHigherLayer, i4IfStackLowerLayer)
     INT4                i4IfStackHigherLayer;
     INT4                i4IfStackLowerLayer;
#endif
{

    UINT4               u4HighIfIndex;
    UINT4               u4LowIfIndex;
    tTMO_SLL           *pStack = NULL;
    tStackInfoStruct   *pStackNode = NULL;

    u4HighIfIndex = (UINT4) i4IfStackHigherLayer;
    u4LowIfIndex = (UINT4) i4IfStackLowerLayer;

    if ((CfaIsSispInterface (u4HighIfIndex) == CFA_TRUE) ||
        (CfaIsSispInterface (u4LowIfIndex) == CFA_TRUE))
    {
        return SNMP_SUCCESS;
    }

    if (i4IfStackHigherLayer > 0)
    {
        if (((UINT4) i4IfStackHigherLayer > SYS_DEF_MAX_INTERFACES) ||
            (!CFA_IF_ENTRY_USED (u4HighIfIndex)))
        {
            return (SNMP_FAILURE);
        }

        pStack = &CFA_IF_STACK_LOW (u4HighIfIndex);
        TMO_SLL_Scan (pStack, pStackNode, tStackInfoStruct *)
        {
            if (u4LowIfIndex == pStackNode->u4IfIndex)
            {
                /* relation exists between the two interfces */
                return SNMP_SUCCESS;
            }
        }
    }
    else
    {
        if (i4IfStackLowerLayer <= 0)
        {
            return SNMP_FAILURE;
        }

        if (((UINT4) i4IfStackLowerLayer > SYS_DEF_MAX_INTERFACES) ||
            (!CFA_IF_ENTRY_USED (u4LowIfIndex)))
        {
            return (SNMP_FAILURE);
        }

        if (CFA_IF_ENTRY_USED (u4LowIfIndex))
        {
            pStack = &CFA_IF_STACK_HIGH (u4LowIfIndex);
            pStackNode = (tStackInfoStruct *) TMO_SLL_First (pStack);

            if (pStackNode == NULL)
            {
                return SNMP_FAILURE;
            }

            if (pStackNode->u4IfIndex == CFA_NONE)
            {
                return SNMP_SUCCESS;
            }
        }
    }

    /* No relation exits between the two interfaces */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :    nmhGetFirstIndexIfStackTable
 Input    :    The Indices
        IfStackHigherLayer
        IfStackLowerLayer
 Output    :    The Get First Routines gets the Lexicographicaly
        First Entry from the Table.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIfStackTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexIfStackTable (INT4 *pi4IfStackHigherLayer,
                              INT4 *pi4IfStackLowerLayer)
#else
INT1
nmhGetFirstIndexIfStackTable (pi4IfStackHigherLayer, pi4IfStackLowerLayer)
     INT4               *pi4IfStackHigherLayer;
     INT4               *pi4IfStackLowerLayer;
#endif
{

    UINT4               u4Index = 0;
    tTMO_SLL           *pStack = NULL;
    tStackInfoStruct   *pStackNode = NULL;

    /* Get the first used index */
    u4Index = 1;
    CFA_GAP_IF_SCAN (u4Index, SYS_DEF_MAX_INTERFACES)
    {

        if (!CFA_IF_ENTRY_USED (u4Index))
        {
            continue;
        }

        pStack = &CFA_IF_STACK_HIGH (u4Index);
        pStackNode = (tStackInfoStruct *) TMO_SLL_First (pStack);

        if (pStackNode == NULL)
        {
            continue;
        }

        if (pStackNode->u4IfIndex == CFA_NONE)
        {
            *pi4IfStackHigherLayer = CFA_NONE;
            *pi4IfStackLowerLayer = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }

/* there has to be atleast one entry in the stacktable with high as 0 */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :    nmhGetNextIndexIfStackTable
 Input    :    The Indices
        IfStackHigherLayer
        nextIfStackHigherLayer
        IfStackLowerLayer
        nextIfStackLowerLayer
 Output    :    The Get Next function gets the Next Index for
        the Index Value given in the Index Values. The
        Indices are stored in the next_varname variables.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIfStackTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexIfStackTable (INT4 i4IfStackHigherLayer,
                             INT4 *pi4NextIfStackHigherLayer,
                             INT4 i4IfStackLowerLayer,
                             INT4 *pi4NextIfStackLowerLayer)
#else
INT1
nmhGetNextIndexIfStackTable (i4IfStackHigherLayer, pi4NextIfStackHigherLayer,
                             i4IfStackLowerLayer, pi4NextIfStackLowerLayer)
     INT4                i4IfStackHigherLayer;
     INT4               *pi4NextIfStackHigherLayer;
     INT4                i4IfStackLowerLayer;
     INT4               *pi4NextIfStackLowerLayer;
#endif
{
    UINT4               u4Index;
    UINT4               u4HighIfIndex;
    UINT4               u4LowIfIndex;
    tTMO_SLL           *pStack = NULL;
    tStackInfoStruct   *pStackNode = NULL;
    tStackInfoStruct   *pTempStackNode = NULL;
    BOOL1               bNextHighIfFound = CFA_FALSE;
    BOOL1               bNextLowIfFound = CFA_FALSE;

    if ((i4IfStackHigherLayer < 0) || (i4IfStackLowerLayer < 0))
    {
        return SNMP_FAILURE;
    }

    if (i4IfStackHigherLayer > SYS_DEF_MAX_INTERFACES)
    {
        return SNMP_FAILURE;
    }

    if (i4IfStackLowerLayer > SYS_DEF_MAX_INTERFACES)
    {
        i4IfStackLowerLayer = SYS_DEF_MAX_INTERFACES;
    }

    u4HighIfIndex = (UINT4) i4IfStackHigherLayer;
    u4LowIfIndex = (UINT4) i4IfStackLowerLayer;

    if (u4HighIfIndex != CFA_NONE)
    {
        if (!CFA_IF_ENTRY_USED (u4HighIfIndex))
        {
            u4Index = u4HighIfIndex + 1;
            /* Next Index should always return the next used Index */
            CFA_GAP_IF_SCAN (u4Index, SYS_DEF_MAX_INTERFACES)
            {
                if (CFA_IF_ENTRY_USED (u4Index))
                {
                    u4HighIfIndex = u4Index;
                    break;
                }
            }
            if (u4Index > SYS_DEF_MAX_INTERFACES)
                return SNMP_FAILURE;
        }

        pStack = &CFA_IF_STACK_LOW (u4HighIfIndex);

        TMO_SLL_Scan (pStack, pStackNode, tStackInfoStruct *)
        {
            if (u4LowIfIndex == pStackNode->u4IfIndex)
            {
                if ((pTempStackNode = (tStackInfoStruct *)
                     TMO_SLL_Next (pStack, &pStackNode->NextEntry)) != NULL)
                {
                    *pi4NextIfStackHigherLayer = (INT4) u4HighIfIndex;
                    *pi4NextIfStackLowerLayer =
                        (INT4) pTempStackNode->u4IfIndex;
                    bNextLowIfFound = CFA_TRUE;
                    return SNMP_SUCCESS;
                }
            }
        }                        /* end of SLL Scan */

        if (bNextLowIfFound == CFA_FALSE)
        {
            u4HighIfIndex = (UINT4) i4IfStackHigherLayer;
            u4Index = u4HighIfIndex + 1;
            /* Next Index should always return the next used Index */
            CFA_GAP_IF_SCAN (u4Index, SYS_DEF_MAX_INTERFACES)
            {
                if (!CFA_IF_ENTRY_USED (u4Index))
                {
                    continue;
                }

                pStack = &CFA_IF_STACK_LOW (u4Index);
                pTempStackNode = (tStackInfoStruct *) TMO_SLL_First (pStack);
                if (pTempStackNode != NULL)
                {
                    *pi4NextIfStackHigherLayer = (INT4) u4Index;
                    *pi4NextIfStackLowerLayer =
                        (INT4) pTempStackNode->u4IfIndex;
                    bNextHighIfFound = CFA_TRUE;
                    break;
                }
            }
            if (u4Index > SYS_DEF_MAX_INTERFACES)
            {
                return SNMP_FAILURE;
            }
        }

        if (bNextHighIfFound == CFA_TRUE)
        {
            return SNMP_SUCCESS;
        }
    }
    else
    {
        u4Index = u4LowIfIndex + 1;
        CFA_GAP_IF_SCAN (u4Index, SYS_DEF_MAX_INTERFACES)
        {
            if (!CFA_IF_ENTRY_USED (u4Index))
            {
                continue;
            }

            pStack = &CFA_IF_STACK_HIGH (u4Index);
            pStackNode = (tStackInfoStruct *) TMO_SLL_First (pStack);

            if (pStackNode == NULL)
            {
                continue;
            }

            if (pStackNode->u4IfIndex == CFA_NONE)
            {
                *pi4NextIfStackHigherLayer = CFA_NONE;
                *pi4NextIfStackLowerLayer = (INT4) u4Index;
                return SNMP_SUCCESS;
            }
        }
        u4Index = 1;
        CFA_GAP_IF_SCAN (u4Index, SYS_DEF_MAX_INTERFACES)
        {
            if (!CFA_IF_ENTRY_USED (u4Index))
            {
                continue;
            }

            pStack = &CFA_IF_STACK_LOW (u4Index);
            pStackNode = (tStackInfoStruct *) TMO_SLL_First (pStack);

            if (pStackNode == NULL)
            {
                continue;
            }

            *pi4NextIfStackHigherLayer = (INT4) u4Index;
            *pi4NextIfStackLowerLayer = (INT4) pStackNode->u4IfIndex;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :    nmhGetIfStackStatus
 Input    :    The Indices
        IfStackHigherLayer
        IfStackLowerLayer

        The Object
        retValIfStackStatus
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIfStackStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIfStackStatus (INT4 i4IfStackHigherLayer, INT4 i4IfStackLowerLayer,
                     INT4 *pi4RetValIfStackStatus)
#else
INT1
nmhGetIfStackStatus (i4IfStackHigherLayer, i4IfStackLowerLayer,
                     pi4RetValIfStackStatus)
     INT4                i4IfStackHigherLayer;
     INT4                i4IfStackLowerLayer;
     INT4               *pi4RetValIfStackStatus;
#endif
{

    UINT4               u4HighIfIndex;
    UINT4               u4LowIfIndex;
    tTMO_SLL           *pStack = NULL;
    tStackInfoStruct   *pStackNode = NULL;

    u4HighIfIndex = (UINT4) i4IfStackHigherLayer;
    u4LowIfIndex = (UINT4) i4IfStackLowerLayer;

    /* get the SLL List */
    /* Scan for the node */
    if (u4HighIfIndex != 0)
    {
        pStack = &CFA_IF_STACK_LOW (u4HighIfIndex);
        TMO_SLL_Scan (pStack, pStackNode, tStackInfoStruct *)
        {
            if (u4LowIfIndex == pStackNode->u4IfIndex)
            {
                *pi4RetValIfStackStatus = pStackNode->u1StackStatus;
                return SNMP_SUCCESS;
            }
        }
    }
    else
    {
        pStack = &CFA_IF_STACK_HIGH (u4LowIfIndex);
        TMO_SLL_Scan (pStack, pStackNode, tStackInfoStruct *)
        {
            if (u4HighIfIndex == pStackNode->u4IfIndex)
            {
                *pi4RetValIfStackStatus = pStackNode->u1StackStatus;
                return SNMP_SUCCESS;
            }
        }
    }
    /* Node not found */
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIfStackStatus
 Input       :  The Indices
                IfStackHigherLayer
                IfStackLowerLayer

                The Object 
                setValIfStackStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfStackStatus (INT4 i4IfStackHigherLayer, INT4 i4IfStackLowerLayer,
                     INT4 i4SetValIfStackStatus)
{

    UINT4               u4HighIfIndex = (UINT4) i4IfStackHigherLayer;
    UINT4               u4LowIfIndex = (UINT4) i4IfStackLowerLayer;
    UINT1               u1HighIfType;
    tStackInfoStruct   *pHighStackListScan;
    tStackInfoStruct   *pTemp = NULL;
    tTMO_SLL           *pHighStack;
    UINT1               u1LowIfType;
    tStackInfoStruct   *pLowStackListScan;
    tTMO_SLL           *pLowStack;
    tTMO_SLL           *pIfStack = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
#ifdef PPP_WANTED
    tIfLayerInfo        LowInterface;
    tIfLayerInfo        HighInterface;
    tPppIpInfo          IpInfo;
    UINT4               u4IdleTimer;
    UINT4               u4CurMtu;
    tIpConfigInfo      *pIpConfigInfo = NULL;

#endif /* PPP_WANTED */
    UINT1               u1LayeringActive = CFA_FALSE;
    UINT1               u1LayeringExists = CFA_FALSE;
    UINT1               u1Delete = CFA_FALSE;
    tStackInfoStruct   *pNodeList = NULL;
/* we need to examine the low stack of the higher interface */
    if (u4HighIfIndex != 0)
    {
        CfaGetIfType ((UINT4) u4HighIfIndex, &u1HighIfType);
        pHighStackListScan =
            (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4HighIfIndex);
        pHighStack = &CFA_IF_STACK_LOW (u4HighIfIndex);
    }
    else
    {                            /* lower layer is none */
        u1HighIfType = CFA_NONE;
        pHighStackListScan = NULL;
        pHighStack = NULL;
    }

/* we need to examine the high stack of the lower interface */
    if (u4LowIfIndex != 0)
    {
        CfaGetIfType ((UINT4) u4LowIfIndex, &u1LowIfType);
        pLowStackListScan =
            (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4LowIfIndex);
        pLowStack = &CFA_IF_STACK_HIGH (u4LowIfIndex);
    }
    else
    {                            /* higher layer is none or IP */
        u1LowIfType = CFA_NONE;
        pLowStackListScan = NULL;
        pLowStack = NULL;
    }

/* we see if the stack relation already exists first */
    if (u4HighIfIndex != 0)
    {
        TMO_SLL_Scan (pHighStack, pHighStackListScan, tStackInfoStruct *)
        {
            if (pHighStackListScan->u4IfIndex == u4LowIfIndex)
            {
                if (pHighStackListScan->u1StackStatus == CFA_RS_ACTIVE)
                    u1LayeringActive = CFA_TRUE;
                else
                {
                    u1LayeringExists = CFA_TRUE;
                    pIfStack = pHighStack;
                }
                break;
            }
        }
    }
    else
    {                            /* since higher layer is NONE, we see the lower interface's stack */
        if (u4LowIfIndex != 0)
        {
            TMO_SLL_Scan (pLowStack, pLowStackListScan, tStackInfoStruct *)
            {
                if (pLowStackListScan->u4IfIndex == u4HighIfIndex)
                {
                    if (pLowStackListScan->u1StackStatus == CFA_RS_ACTIVE)
                        u1LayeringActive = CFA_TRUE;
                    else
                    {
                        u1LayeringExists = CFA_TRUE;
                        pIfStack = pLowStack;
                    }
                    break;
                }
            }
        }
    }

    pNodeList = (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4LowIfIndex);
    if (pNodeList == NULL)
    {
        return (CFA_FAILURE);
    }
/* currently the interfaces which can be layered onto other interfaces
are only PPP, MP, IPOA and ATMVC */
    switch (i4SetValIfStackStatus)
    {

        case CFA_RS_CREATEANDGO:
        case CFA_RS_CREATEANDWAIT:
/* We create the entry in the interface stack, but we dont send this 
notification to the protocol module */

            switch (u1HighIfType)
            {

#ifdef PPP_WANTED
                case CFA_PPP:
                    if (CFA_IF_ENTRY_REG_WITH_IP (u4LowIfIndex, pNodeList))
                    {
                        /* Physical link being stacked under PPP is already
                         * registered with IP     or
                         * Member PPP link being stacvked under MP is already
                         * registered with IP     - deregister now */
                        if (CfaIfmConfigNetworkInterface (CFA_NET_IF_DEL,
                                                          u4LowIfIndex,
                                                          0,
                                                          pIpConfigInfo) !=
                            CFA_SUCCESS)
                        {
                            /* unsuccessful in updating to IP - but we
                             * dont fail the interface creation */
                            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                                      CFA_IFM,
                                      "Error in nmhSetIfStackStatus - "
                                      "Deregistering from IP failed %d\n",
                                      u4LowIfIndex);
                            return SNMP_FAILURE;
                        }

                        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                                  "In nmhSetIfStackStatus - "
                                  "Deregister with IP success - interface %d\n",
                                  u4LowIfIndex);
                    }

                    if (CfaIfmAddStackEntry (u4HighIfIndex, CFA_LOW,
                                             u4LowIfIndex,
                                             CFA_RS_NOTINSERVICE) !=
                        CFA_SUCCESS)
                    {
                        return (SNMP_FAILURE);
                    }
                    break;

#endif /* PPP_WANTED */

#ifdef MPLS_WANTED
                case CFA_MPLS_TUNNEL:

                    CFA_IF_ENTRY (u4HighIfIndex)->u4HighIfIndex = u4LowIfIndex;
                    /* just create the stack entry */
                    if (CfaIfmAddStackEntry (u4LowIfIndex, CFA_HIGH,
                                             u4HighIfIndex,
                                             CFA_RS_NOTINSERVICE) !=
                        CFA_SUCCESS)
                    {
                        return (SNMP_FAILURE);
                    }
                    break;
                case CFA_MPLS:
#endif
                case CFA_TELINK:
                    CFA_IF_ENTRY (u4HighIfIndex)->u4HighIfIndex = u4LowIfIndex;
                    /* just create the stack entry */
                case CFA_LAGG:
                    if (CfaIfmAddStackEntry (u4HighIfIndex, CFA_LOW,
                                             u4LowIfIndex,
                                             CFA_RS_NOTINSERVICE) !=
                        CFA_SUCCESS)
                    {
                        return (SNMP_FAILURE);
                    }
                    break;

                case CFA_PIP:
                case CFA_BRIDGED_INTERFACE:
                    /* If any ILAN entry for port exists in stack,
                     * delete that entry and add new entry. */
                    TMO_SLL_Scan (pHighStack, pHighStackListScan,
                                  tStackInfoStruct *)
                {
                    if (pHighStackListScan->u4IfIndex != CFA_NONE)
                    {
                        u1Delete = CFA_TRUE;
                        break;
                    }
                }
                    if (u1Delete == CFA_TRUE)
                    {
                        CfaIfmDeleteStackEntry (pHighStackListScan->u4IfIndex,
                                                CFA_HIGH, u4HighIfIndex);
                        pHighStackListScan = NULL;
                    }

                    /*Fall Through */

                case CFA_NONE:
/* just create the stack entry in the lower layer's stack - it will be made 
active when the interface is registered with IP. */
                    if (CfaIfmAddStackEntry (u4LowIfIndex, CFA_HIGH,
                                             u4HighIfIndex,
                                             CFA_RS_NOTINSERVICE) !=
                        CFA_SUCCESS)
                    {
                        return (SNMP_FAILURE);
                    }
                    break;

                default:
/* not expected */
                    return (SNMP_FAILURE);
            }

/* go on to make the interface active if the command was createAndGo */
            if (i4SetValIfStackStatus == CFA_RS_CREATEANDWAIT)
            {
                break;
            }

        case CFA_RS_ACTIVE:
/* We send the notification about this layering to the protocol module if
the status was not already active */
            if (u1LayeringActive == CFA_FALSE && u1LayeringExists == CFA_TRUE)
            {
                if ((u1HighIfType == CFA_PIP) ||
                    (u1HighIfType == CFA_BRIDGED_INTERFACE))
                {
                    pTempNode = (tTMO_SLL_NODE *) TMO_SLL_First (pIfStack);
                    CFA_CHECK_IF_NULL (pTempNode, SNMP_FAILURE);
                    pNode = TMO_SLL_Next (pIfStack, pTempNode);
                    if (pNode != NULL)
                    {
                        CFA_IF_STACK_STATUS (pNode) =
                            (UINT1) i4SetValIfStackStatus;
                    }
                }
                else if ((u1HighIfType == CFA_MPLS) ||
                         (u1HighIfType == CFA_TELINK) ||
                         (u1HighIfType == CFA_MPLS_TUNNEL))
                {
                    if (u1LowIfType == CFA_TELINK)
                    {
                        TMO_SLL_Scan (pIfStack, pTemp, tStackInfoStruct *)
                        {
                            if (pTemp->u4IfIndex == u4LowIfIndex)
                            {
                                pTemp->u1StackStatus
                                    = (UINT1) i4SetValIfStackStatus;
                                break;
                            }
                        }
                    }
                    else
                    {
                        pTempNode = (tTMO_SLL_NODE *) TMO_SLL_First (pIfStack);
                        CFA_CHECK_IF_NULL (pTempNode, SNMP_FAILURE);
                        CFA_IF_STACK_STATUS (pTempNode) =
                            (UINT1) i4SetValIfStackStatus;
                    }

                    pIfStack = &CFA_IF_STACK_HIGH (u4HighIfIndex);
                    pTempNode = (tTMO_SLL_NODE *) TMO_SLL_First (pIfStack);
                    CFA_CHECK_IF_NULL (pTempNode, SNMP_FAILURE);
                    CFA_IF_STACK_STATUS (pTempNode) =
                        (UINT1) i4SetValIfStackStatus;

                    if (pLowStack != NULL)
                    {
                        pTempNode = (tTMO_SLL_NODE *) TMO_SLL_First (pLowStack);
                        CFA_CHECK_IF_NULL (pTempNode, SNMP_FAILURE);
                        CFA_IF_STACK_STATUS (pTempNode) =
                            (UINT1) i4SetValIfStackStatus;
                    }

                    pIfStack = &CFA_IF_STACK_LOW (u4LowIfIndex);
                    pTempNode = (tTMO_SLL_NODE *) TMO_SLL_First (pIfStack);
                    CFA_CHECK_IF_NULL (pTempNode, SNMP_FAILURE);
                    CFA_IF_STACK_STATUS (pTempNode) =
                        (UINT1) i4SetValIfStackStatus;
                }
                else
                {
                    pTempNode = (tTMO_SLL_NODE *) TMO_SLL_First (pIfStack);
                    CFA_CHECK_IF_NULL (pTempNode, SNMP_FAILURE);
                    CFA_IF_STACK_STATUS (pTempNode) =
                        (UINT1) i4SetValIfStackStatus;
                }
            }
            switch (u1HighIfType)
            {

#ifdef PPP_WANTED
                case CFA_PPP:
                    if (u1LayeringActive == CFA_FALSE)
                    {
/* have to update the PPP interface and make it NOTINSERVICE */
                        CFA_IF_RS (u4HighIfIndex) = CFA_RS_NOTINSERVICE;
                    }
                    break;

#endif /* PPP_WANTED */

                case CFA_TELINK:
                case CFA_MPLS:
                case CFA_MPLS_TUNNEL:
                    CfaIfmHandleInterfaceStatusChange
                        (u4HighIfIndex, CFA_IF_ADMIN (u4HighIfIndex),
                         CFA_IF_UP, (UINT1) CFA_TRUE);
                    break;

                case CFA_NONE:
#ifdef HDLC_WANTED
                case CFA_HDLC:
#endif
/* the reg with IP will be automatically made active after the interface comes
up */
                    break;
                case CFA_PIP:
                case CFA_BRIDGED_INTERFACE:
#ifdef NPAPI_WANTED
                    FsCfaHwAddPortToILan (u4LowIfIndex, u4HighIfIndex);
#endif
                    break;

                default:
/* not expected */
                    return (SNMP_FAILURE);

            }                    /* end of RS ACTIVE switch */
            break;

        case CFA_RS_DESTROY:
/* We send the notification about the deletion of this layering to the 
protocol module and delete the entries in the interface stack */

            switch (u1HighIfType)
            {
#ifdef PPP_WANTED
                case CFA_PPP:
                    if (u1LayeringActive == CFA_FALSE)
                    {
/* just delete the stack entry and make the RS of PPP NOTREADY */
                        CfaIfmDeleteStackEntry (u4HighIfIndex, CFA_LOW,
                                                u4LowIfIndex);
                        CFA_IF_RS (u4HighIfIndex) = CFA_RS_NOTREADY;
                    }
                    else
                    {
/* have to update the PPP interface and make it NOTREADY */
                        HighInterface.u1Command = CFA_LAYER_IGNORE;
                        LowInterface.u1Command = CFA_LAYER_DEL;
                        LowInterface.u4IfIndex = u4LowIfIndex;
                        CfaGetIfType (u4LowIfIndex, &(LowInterface.u1IfType));
                        IpInfo.u4LocalIpAddr = CFA_IF_IPADDR (u4HighIfIndex);
                        IpInfo.u4PeerIpAddr =
                            CFA_IF_PEER_IPADDR (u4HighIfIndex);
                        IpInfo.u1SubnetMask = CFA_IF_IPSUBNET (u4HighIfIndex);
                        u4IdleTimer = CFA_IF_IDLE_TIMEOUT (u4HighIfIndex);

                        CfaGetIfMtu (u4HighIfIndex, &u4CurMtu);
                        if (CfaIfmUpdatePppInterface
                            (u4HighIfIndex, &LowInterface, &HighInterface,
                             &IpInfo, u4IdleTimer, u4CurMtu) != CFA_SUCCESS)
                        {

                            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                                      CFA_IFM,
                                      "Error in nmhSetIfStackStatus - "
                                      "LL De-reg fail for interface %d\n",
                                      u4HighIfIndex);
                            return (SNMP_FAILURE);
                        }
                        else
                        {
                            CfaIfmDeleteStackEntry (u4HighIfIndex, CFA_LOW,
                                                    u4LowIfIndex);
                            CFA_IF_RS (u4HighIfIndex) = CFA_RS_NOTREADY;
                        }
                    }
                    break;

#endif /* PPP_WANTED */

                case CFA_TELINK:
                    /* Intentional Fall through */

                case CFA_MPLS:
                    if (u1LayeringActive == CFA_TRUE)
                    {
                        CfaIfmDeleteStackEntry (u4HighIfIndex, CFA_LOW,
                                                u4LowIfIndex);
                        CfaIfmHandleInterfaceStatusChange
                            (u4HighIfIndex, CFA_IF_ADMIN (u4HighIfIndex),
                             CFA_IF_DOWN, (UINT1) CFA_TRUE);
                    }
                    break;
                case CFA_MPLS_TUNNEL:
                    if (u1LayeringActive == CFA_TRUE)
                    {
/* just delete the stack entry and make the RS of MPLS NOTREADY */
                        CfaIfmDeleteStackEntry (u4LowIfIndex, CFA_HIGH,
                                                u4HighIfIndex);
                        CfaIfmHandleInterfaceStatusChange
                            (u4HighIfIndex, CFA_IF_ADMIN (u4HighIfIndex),
                             CFA_IF_DOWN, (UINT1) CFA_TRUE);
                    }
                    break;

                case CFA_PIP:
                case CFA_BRIDGED_INTERFACE:
                    if (u1LayeringActive == CFA_TRUE)
                    {
                        /* just delete the stack entry */
                        CfaIfmDeleteStackEntry (u4LowIfIndex, CFA_HIGH,
                                                u4HighIfIndex);
                    }
                    break;

                case CFA_NONE:
                    if (u1LayeringActive == CFA_FALSE)
                    {
/* just delete the stack entry in the lower layer's stack */
                        CfaIfmDeleteStackEntry (u4LowIfIndex, CFA_HIGH,
                                                u4HighIfIndex);
                    }
                    else
                    {
/* this interface is registered with IP */
                        if (CfaIfmConfigNetworkInterface (CFA_NET_IF_DEL,
                                                          u4LowIfIndex, 0,
                                                          0) != CFA_SUCCESS)
                        {
/* unsuccessful in de-registering from IP - we cant delete the interface.
we dont expect this to fail - just for handling unforeseen exceptions. */
                            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                                      CFA_IFM,
                                      "Error in nmhSetExIfStackStatus - "
                                      "IP De-reg fail for interface %d\n",
                                      u4LowIfIndex);
                            return (SNMP_FAILURE);
                        }
                    }            /* end of de-reg with IP */
                    break;

                default:
/* not expected */
                    return (SNMP_FAILURE);

            }                    /* end of Destroy RS iftype switch */
            break;                /* end of dest RS */

        default:
            return SNMP_FAILURE;    /* not expected */

    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IfStackStatus
 Input       :  The Indices
                IfStackHigherLayer
                IfStackLowerLayer

                The Object 
                testValIfStackStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IfStackStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2IfStackStatus (UINT4 *pu4ErrorCode, INT4 i4IfStackHigherLayer,
                        INT4 i4IfStackLowerLayer, INT4 i4TestValIfStackStatus)
#else
INT1
nmhTestv2IfStackStatus (*pu4ErrorCode, i4IfStackHigherLayer,
                        i4IfStackLowerLayer, i4TestValIfStackStatus)
     UINT4              *pu4ErrorCode;
     INT4                i4IfStackHigherLayer;
     INT4                i4IfStackLowerLayer;
     INT4                i4TestValIfStackStatus;
#endif
{
   /*** $$TRACE_LOG (ENTRY, "IfStackHigherLayer = %d\n", i4IfStackHigherLayer); ***/
   /*** $$TRACE_LOG (ENTRY, "IfStackLowerLayer = %d\n", i4IfStackLowerLayer); ***/
   /*** $$TRACE_LOG (ENTRY, "IfStackStatus = %d\n", i4TestValIfStackStatus); ***/

    UINT4               u4HighIfIndex = (UINT4) i4IfStackHigherLayer;
    UINT4               u4LowIfIndex = (UINT4) i4IfStackLowerLayer;
    UINT1               u1HighIfType = CFA_NONE;
    UINT1               u1OperStatus = CFA_IF_DOWN;
    tStackInfoStruct   *pHighStackListScan;
    tTMO_SLL           *pHighStack = NULL;
    UINT1               u1LowIfType = CFA_NONE;
    tStackInfoStruct   *pLowStackListScan = NULL;
    tTMO_SLL           *pLowStack = NULL;

    UINT1               u1CreationAttempt = CFA_FALSE;
    UINT1               u1LayeringExists = CFA_FALSE;
    UINT1               u1BridgedIfaceStatus;
    tTMO_SLL_NODE      *pTempNode = NULL;
    UINT4               u4TempIfIndex;
/* currently we do not support NOTINSERVICE - we just delete the layering. 
But dont accept SETs with this value in the first case so that the Manager 
does not get a wrong impression */
    if (i4TestValIfStackStatus == CFA_RS_NOTINSERVICE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

/* check the validity of ifIndex of high and low. */
    if (((u4HighIfIndex == 0) && (u4LowIfIndex == 0)) ||
        (CfaIsIfEntryProgrammingAllowed (u4HighIfIndex) == CFA_FALSE) ||
        (CfaIsIfEntryProgrammingAllowed (u4LowIfIndex) == CFA_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceIfMainTable ((INT4) u4HighIfIndex)
        != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceIfMainTable ((INT4) u4LowIfIndex)
        != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

/* we need to examine the low stack of the higher interface */
    if (u4HighIfIndex != 0)
    {
        CfaGetIfType ((UINT4) u4HighIfIndex, &u1HighIfType);
        pHighStackListScan =
            (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4HighIfIndex);
        pHighStack = &CFA_IF_STACK_LOW (u4HighIfIndex);
    }

    /* we need to examine the high stack of the lower interface */
    if (u4LowIfIndex != 0)
    {
        CfaGetIfType ((UINT4) u4LowIfIndex, &u1LowIfType);
        pLowStackListScan =
            (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4LowIfIndex);
        pLowStack = &CFA_IF_STACK_HIGH (u4LowIfIndex);
    }

/* if the type is not specified then set is not allowed. */
    if ((u1HighIfType == CFA_INVALID) || (u1LowIfType == CFA_INVALID))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (u4HighIfIndex != CFA_NONE)
    {
        CfaGetIfOperStatus ((UINT4) u4HighIfIndex, &u1OperStatus);

        if ((u1OperStatus == CFA_IF_UP) &&
            ((u1HighIfType != CFA_MPLS) && (u1HighIfType != CFA_TELINK) &&
             (u1HighIfType != CFA_MPLS_TUNNEL)))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }

/* we see if the stack relation already exists first */
    if (u4HighIfIndex != 0)
    {
        TMO_SLL_Scan (pHighStack, pHighStackListScan, tStackInfoStruct *)
        {
            if (pHighStackListScan->u4IfIndex == u4LowIfIndex)
            {
                u1LayeringExists = CFA_TRUE;
                break;
            }
        }
    }

/* find out what operation is being attempted */
    if ((i4TestValIfStackStatus == CFA_RS_CREATEANDGO) ||
        (i4TestValIfStackStatus == CFA_RS_CREATEANDWAIT))
    {
        if (u1LayeringExists == CFA_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        u1CreationAttempt = CFA_TRUE;
    }

    if ((i4TestValIfStackStatus == CFA_RS_DESTROY) ||
        (i4TestValIfStackStatus == CFA_RS_NOTINSERVICE))
    {
        if (u1LayeringExists == CFA_FALSE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if ((u1LayeringExists == CFA_FALSE) &&
        (i4TestValIfStackStatus == CFA_RS_ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

/* first we check the interface type compatability */
    switch (u1HighIfType)
    {
/* for Enet currently no layering is possible - all sets should fail */
        case CFA_L3IPVLAN:
        case CFA_ENET:
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
            break;

        case CFA_PPP:
            if (u1CreationAttempt == CFA_TRUE)
            {
                if ((u1LowIfType != CFA_ENET)
#ifdef HDLC_WANTED
                    && (u1LowIfType != CFA_HDLC)
#endif
                    )
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    return SNMP_FAILURE;
                }
                pHighStackListScan =
                    (tStackInfoStruct *) TMO_SLL_First (pHighStack);

                /* check if there is any other interface already below PPP */
                if (pHighStackListScan != NULL &&
                    (CFA_IF_STACK_IFINDEX (pHighStackListScan) !=
                     CFA_NONE)
                    && (CFA_IF_STACK_IFINDEX (pHighStackListScan) !=
                        u4LowIfIndex))
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    CLI_SET_ERR (CLI_CFA_PPP_ALREADY_STACKED_ERR);
                    return SNMP_FAILURE;
                }

                /* check if there is any other interface already above specificed lower layer */
                pLowStackListScan = TMO_SLL_First (pLowStack);

                if (pLowStackListScan == NULL)
                {
                    return SNMP_FAILURE;
                }

                u4TempIfIndex = CFA_IF_STACK_IFINDEX (pLowStackListScan);
                if (u4TempIfIndex != CFA_NONE)
                {

                    /* multiplexing is possible only for VCs - like ATMVC */
                    if (u4TempIfIndex != u4HighIfIndex)
                    {
                        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                        CLI_SET_ERR (CLI_CFA_PPP_LL_ALREADY_STACKED_ERR);
                        return SNMP_FAILURE;
                    }
                }

                if (u1LowIfType == CFA_ENET)
                {
                    CfaGetIfBridgedIfaceStatus (u4LowIfIndex,
                                                &u1BridgedIfaceStatus);

                    if (u1BridgedIfaceStatus == CFA_ENABLED)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                        CLI_SET_ERR (CLI_CFA_PPP_LL_SWITCH_PORT_ERR);
                        return SNMP_FAILURE;
                    }

                    if (CFA_IF_ADMIN (u4LowIfIndex) == CFA_IF_UP)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                        CLI_SET_ERR (CLI_CFA_PPP_LL_UP_ERR);
                        return SNMP_FAILURE;
                    }
                }
            }
            break;

        case CFA_TELINK:
            /* Currently TE Link Interfaces can be stacked over
             * 1. Another TE Link Interface (CFA_TELINK) 
             *    in case of TE Link Bundling.
             * 2. L3 Interfaces (CFA_L3IPVLAN or (CFA_ENET with 
             *    bridging disabled). 
             * 3. Mpls Tunnel Interface (CFA_MPLS_TUNNEL) in 
             *    case of Forwarding Adjacency creation.
             */

            /* This is an intentional fall through */
        case CFA_MPLS:
            if (u1CreationAttempt == CFA_TRUE)
            {
                /* Currently MPLS Interfaces can be stacked over
                 *
                 * 1. TE Link Interface (CFA_TELINK).
                 * 2. L3 Interfaces (CFA_L3IPVLAN or (CFA_ENET with 
                 *    bridging disabled).
                 */

                CfaGetIfBridgedIfaceStatus (u4LowIfIndex,
                                            &u1BridgedIfaceStatus);
                pLowStackListScan = TMO_SLL_First (pLowStack);

                if (pLowStackListScan == NULL)
                {
                    return SNMP_FAILURE;
                }
                if ((u1LowIfType == CFA_TELINK) ||
                    (u1LowIfType == CFA_L3IPVLAN) ||
                    (u1LowIfType == CFA_MPLS_TUNNEL) ||
                    ((u1LowIfType == CFA_ENET) &&
                     u1BridgedIfaceStatus == CFA_DISABLED))
                {
                    u4TempIfIndex = CFA_IF_STACK_IFINDEX (pLowStackListScan);

                    if ((u4TempIfIndex == CFA_NONE) ||
                        (u4TempIfIndex == u4HighIfIndex))
                    {
                        break;
                    }
                    else
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }
                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            break;

        case CFA_MPLS_TUNNEL:
            if (u1CreationAttempt == CFA_TRUE)
            {
                /* Tunnel can be layered on top of MPLS interface(166) or HLSP tunnel interface */
                if ((u1LowIfType != CFA_MPLS) && (u1LowIfType != CFA_MPLS_TUNNEL))    /* STATIC_HLSP */
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    return SNMP_FAILURE;
                }
            }
            break;

        case CFA_PIP:
        case CFA_BRIDGED_INTERFACE:
            if (u1CreationAttempt == CFA_TRUE)
            {
                /* As of now INTERNAL port can be layered only on ILAN Port */
                if (u1LowIfType != CFA_ILAN)
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    return SNMP_FAILURE;
                }
            }
            break;

        case CFA_NONE:
/* currently high type is CFA_NONE implies this interface is registered to
IP if the status is active. If not-ready then it just indicates that there
is no interface above. if not-in-service then it indicates that this interface
may be registered to IP when it is made up. */
            if (u1CreationAttempt == CFA_TRUE)
            {
/* check if there is any other interface already above the lower layer
specified */
                if (pLowStack == NULL)
                {
                    return SNMP_FAILURE;
                }
                pTempNode = (tTMO_SLL_NODE *) TMO_SLL_First (pLowStack);
                CFA_CHECK_IF_NULL (pTempNode, SNMP_FAILURE);
                if (CFA_IF_STACK_IFINDEX (pTempNode) != CFA_NONE)
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    return SNMP_FAILURE;
                }
            }
/* no checks necessary for deletion or activation attempt - already have been
verified earlier in the function - no protocol specific checks */
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;

    }                            /* end of switch case */

    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IfStackTable
 Input       :  The Indices
                IfStackHigherLayer
                IfStackLowerLayer
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfStackTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IfRcvAddressTable. */

/****************************************************************************
 Function    :    nmhValidateIndexInstanceIfRcvAddressTable
 Input    :    The Indices
        IfIndex
        IfRcvAddressAddress
 Output    :    The Routines Validates the Given Indices.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceIfRcvAddressTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceIfRcvAddressTable (INT4 i4IfIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pIfRcvAddressAddress)
#else
INT1
nmhValidateIndexInstanceIfRcvAddressTable (i4IfIndex, pIfRcvAddressAddress)
     INT4                i4IfIndex;
     tSNMP_OCTET_STRING_TYPE *pIfRcvAddressAddress;
#endif
{

    UINT4               u4IfIndex;

    tTMO_SLL           *pIfRcvAddrTab = NULL;
    tRcvAddressInfo    *pScanNode = NULL;

    if (((UINT4) i4IfIndex > CFA_MAX_INTERFACES ()) || (i4IfIndex <= 0))
    {
        return (SNMP_FAILURE);
    }

    u4IfIndex = (UINT4) i4IfIndex;

    if (((CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE) &&
         (!CFA_IF_ENTRY_USED (u4IfIndex))) ||
        (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE))
    {
        return SNMP_FAILURE;
    }

    pIfRcvAddrTab = &CFA_IF_RCVADDRTAB (u4IfIndex);
    /* scan the list the find the node */
    TMO_SLL_Scan (pIfRcvAddrTab, pScanNode, tRcvAddressInfo *)
    {
        if (MEMCMP (pScanNode->au1Address, pIfRcvAddressAddress->pu1_OctetList,
                    pIfRcvAddressAddress->i4_Length) == 0)
            /* found an entry, return Success */
            return SNMP_SUCCESS;
    }
    /* No entry found, return Failure */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :    nmhGetFirstIndexIfRcvAddressTable
 Input    :    The Indices
        IfIndex
        IfRcvAddressAddress
 Output    :    The Get First Routines gets the Lexicographicaly
        First Entry from the Table.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIfRcvAddressTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexIfRcvAddressTable (INT4 *pi4IfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pIfRcvAddressAddress)
#else
INT1
nmhGetFirstIndexIfRcvAddressTable (pi4IfIndex, pIfRcvAddressAddress)
     INT4               *pi4IfIndex;
     tSNMP_OCTET_STRING_TYPE *pIfRcvAddressAddress;
#endif
{

    UINT1               u1IfType = CFA_IF_DOWN;
    UINT4               u4Index = 0;
    tTMO_SLL           *pIfRcvAddrTab = NULL;
    tRcvAddressInfo    *pScanNode = NULL;
    INT4                length;

    u4Index = 1;

    /* get the first used index */
    CFA_GAP_IF_SCAN (u4Index, SYS_DEF_MAX_INTERFACES)
    {
        if (!CFA_IF_ENTRY_USED (u4Index))
            continue;

        /* check Recv. Addr. table entry is there or not  */
        *pi4IfIndex = (INT4) u4Index;

        CfaGetIfType (u4Index, &u1IfType);
        if (u1IfType == CFA_ENET)
            length = CFA_ENET_ADDR_LEN;
        else
            length = CFA_MAX_MEDIA_ADDR_LEN;

        if ((pIfRcvAddrTab = &CFA_IF_RCVADDRTAB (u4Index)) != NULL)
        {
            if ((pScanNode = (tRcvAddressInfo *) TMO_SLL_First (pIfRcvAddrTab))
                != NULL)
            {
                MEMCPY (pIfRcvAddressAddress->pu1_OctetList,
                        pScanNode->au1Address, length);
                pIfRcvAddressAddress->i4_Length = length;
                return SNMP_SUCCESS;
            }
        }

    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :    nmhGetNextIndexIfRcvAddressTable
 Input    :    The Indices
        IfIndex
        nextIfIndex
        IfRcvAddressAddress
        nextIfRcvAddressAddress
 Output    :    The Get Next function gets the Next Index for
        the Index Value given in the Index Values. The
        Indices are stored in the next_varname variables.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIfRcvAddressTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexIfRcvAddressTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pIfRcvAddressAddress,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pNextIfRcvAddressAddress)
#else
INT1
nmhGetNextIndexIfRcvAddressTable (i4IfIndex, pi4NextIfIndex,
                                  pIfRcvAddressAddress,
                                  pNextIfRcvAddressAddress)
     INT4                i4IfIndex;
     INT4               *pi4NextIfIndex;
     tSNMP_OCTET_STRING_TYPE *pIfRcvAddressAddress;
     tSNMP_OCTET_STRING_TYPE *pNextIfRcvAddressAddress;
#endif
{
    UINT4               u4Index;    /* for-loop index */
    UINT4               u4IfIndex;
    UINT1               u1IfType = CFA_IF_DOWN;
    tTMO_SLL           *pIfRcvAddrTab = NULL;
    tRcvAddressInfo    *pScanNode = NULL;
    tRcvAddressInfo    *pScanNodeNext = NULL;
    UINT1               au1EthMACAddr[CFA_MAX_MEDIA_ADDR_LEN];
    INT4                i4length;

    if ((i4IfIndex < 0) || ((UINT4) i4IfIndex > CFA_MAX_INTERFACES ()))
    {
        return SNMP_FAILURE;
    }
    u4IfIndex = (UINT4) i4IfIndex;

    if (((CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE) &&
         (!CFA_IF_ENTRY_USED (u4IfIndex))))
    {
        u4Index = u4IfIndex + 1;
        /* Next Index should always return the next used Index */
        CFA_GAP_IF_SCAN (u4Index, SYS_DEF_MAX_INTERFACES)
        {
            if (CFA_IF_ENTRY_USED (u4Index))
            {
                u4IfIndex = u4Index;
                break;
            }
        }
        if (u4Index > SYS_DEF_MAX_INTERFACES)
            return SNMP_FAILURE;
    }

    pIfRcvAddrTab = &CFA_IF_RCVADDRTAB (u4IfIndex);

    MEMSET (au1EthMACAddr, 0, CFA_MAX_MEDIA_ADDR_LEN);

    if ((pIfRcvAddressAddress->i4_Length <= 0) ||
        (pIfRcvAddressAddress->i4_Length > CFA_MAX_MEDIA_ADDR_LEN))
    {
        return SNMP_FAILURE;
    }

    MEMCPY (au1EthMACAddr, pIfRcvAddressAddress->pu1_OctetList,
            pIfRcvAddressAddress->i4_Length);

    /* we have to scan the stack to find the node */
    TMO_SLL_Scan (pIfRcvAddrTab, pScanNode, tRcvAddressInfo *)
    {
        if ((pIfRcvAddressAddress->i4_Length != 0) &&
            (MEMCMP (pScanNode->au1Address, au1EthMACAddr,
                     pIfRcvAddressAddress->i4_Length) == 0))
        {
            break;
        }
    }

    CfaGetIfType (u4IfIndex, &u1IfType);
    if (u1IfType == CFA_ENET)
    {
        i4length = CFA_ENET_ADDR_LEN;
    }
    else
    {
        i4length = CFA_MAX_MEDIA_ADDR_LEN;
    }

    /* get the next node in the list and check if it is NULL */
    if ((pScanNode == NULL) &&
        ((pScanNodeNext = (tRcvAddressInfo *)
          TMO_SLL_First (pIfRcvAddrTab)) != NULL))
    {
        MEMCPY (pNextIfRcvAddressAddress->pu1_OctetList,
                pScanNodeNext->au1Address, pIfRcvAddressAddress->i4_Length);
        pNextIfRcvAddressAddress->i4_Length = pIfRcvAddressAddress->i4_Length;
        *pi4NextIfIndex = (INT4) u4IfIndex;
        return SNMP_SUCCESS;
    }
    else if ((pScanNode != NULL) &&
             ((pScanNodeNext = (tRcvAddressInfo *)
               TMO_SLL_Next (pIfRcvAddrTab, &pScanNode->NextEntry)) != NULL))
    {
        MEMCPY (pNextIfRcvAddressAddress->pu1_OctetList,
                pScanNodeNext->au1Address, pIfRcvAddressAddress->i4_Length);
        pNextIfRcvAddressAddress->i4_Length = pIfRcvAddressAddress->i4_Length;
        *pi4NextIfIndex = (INT4) u4IfIndex;
        return SNMP_SUCCESS;
    }
    else
    {
        /* find out the next used ifIndex */
        u4Index = u4IfIndex + 1;
        CFA_GAP_IF_SCAN (u4Index, SYS_DEF_MAX_INTERFACES)
        {
            if (!CFA_IF_ENTRY_USED ((UINT4) u4Index))
                continue;

            pIfRcvAddrTab = &CFA_IF_RCVADDRTAB (u4Index);

            if ((pScanNode = (tRcvAddressInfo *) TMO_SLL_First (pIfRcvAddrTab))
                == NULL)
                continue;

            CfaGetIfType (u4Index, &u1IfType);

            if (u1IfType == CFA_ENET)
                i4length = CFA_ENET_ADDR_LEN;
            else
                i4length = CFA_MAX_MEDIA_ADDR_LEN;

            MEMCPY (pNextIfRcvAddressAddress->pu1_OctetList,
                    pScanNode->au1Address, i4length);
            pNextIfRcvAddressAddress->i4_Length = i4length;
            *pi4NextIfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }

    /*  all the above conditions fails */

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :    nmhGetIfRcvAddressStatus
 Input    :    The Indices
        IfIndex
        IfRcvAddressAddress

        The Object
        retValIfRcvAddressStatus
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIfRcvAddressStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIfRcvAddressStatus (INT4 i4IfIndex,
                          tSNMP_OCTET_STRING_TYPE * pIfRcvAddressAddress,
                          INT4 *pi4RetValIfRcvAddressStatus)
#else
INT1
nmhGetIfRcvAddressStatus (i4IfIndex, pIfRcvAddressAddress,
                          pi4RetValIfRcvAddressStatus)
     INT4                i4IfIndex;
     tSNMP_OCTET_STRING_TYPE *pIfRcvAddressAddress;
     INT4               *pi4RetValIfRcvAddressStatus;
#endif
{

    UINT4               u4IfIndex;
    tTMO_SLL           *pIfRcvAddrTab = NULL;
    tRcvAddressInfo    *pScanNode = NULL;
    UINT1               au1EthMACAddr[CFA_MAX_MEDIA_ADDR_LEN];

    u4IfIndex = (UINT4) i4IfIndex;
    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }

    pIfRcvAddrTab = &CFA_IF_RCVADDRTAB (u4IfIndex);

    MEMSET (au1EthMACAddr, 0, CFA_MAX_MEDIA_ADDR_LEN);
    MEMCPY (au1EthMACAddr, pIfRcvAddressAddress->pu1_OctetList,
            pIfRcvAddressAddress->i4_Length);
    /* we have to scan the stack to find the node */
    TMO_SLL_Scan (pIfRcvAddrTab, pScanNode, tRcvAddressInfo *)
    {
        if (MEMCMP (pScanNode->au1Address, au1EthMACAddr,
                    (UINT4) pIfRcvAddressAddress->i4_Length) == 0)
        {
            *pi4RetValIfRcvAddressStatus = (INT4) (pScanNode->u1AddressStatus);
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :    nmhGetIfRcvAddressType
 Input    :    The Indices
        IfIndex
        IfRcvAddressAddress

        The Object
        retValIfRcvAddressType
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIfRcvAddressType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIfRcvAddressType (INT4 i4IfIndex,
                        tSNMP_OCTET_STRING_TYPE * pIfRcvAddressAddress,
                        INT4 *pi4RetValIfRcvAddressType)
#else
INT1
nmhGetIfRcvAddressType (i4IfIndex, pIfRcvAddressAddress,
                        pi4RetValIfRcvAddressType)
     INT4                i4IfIndex;
     tSNMP_OCTET_STRING_TYPE *pIfRcvAddressAddress;
     INT4               *pi4RetValIfRcvAddressType;
#endif
{

    UINT4               u4IfIndex;
    tTMO_SLL           *pIfRcvAddrTab = NULL;
    tRcvAddressInfo    *pScanNode = NULL;
    UINT1               au1EthMACAddr[CFA_MAX_MEDIA_ADDR_LEN];

    u4IfIndex = (UINT4) i4IfIndex;
    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }

    pIfRcvAddrTab = &CFA_IF_RCVADDRTAB (u4IfIndex);

    MEMSET (au1EthMACAddr, 0, CFA_MAX_MEDIA_ADDR_LEN);
    MEMCPY (au1EthMACAddr, pIfRcvAddressAddress->pu1_OctetList,
            pIfRcvAddressAddress->i4_Length);
    /* we have to scan the stack to find the node */
    TMO_SLL_Scan (pIfRcvAddrTab, pScanNode, tRcvAddressInfo *)
    {
        if (MEMCMP (pScanNode->au1Address, au1EthMACAddr,
                    pIfRcvAddressAddress->i4_Length) == 0)
        {
            *pi4RetValIfRcvAddressType = (INT4) (pScanNode->u1AddressType);
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :    nmhSetIfRcvAddressStatus
 Input    :    The Indices
        IfIndex
        IfRcvAddressAddress

        The Object
        setValIfRcvAddressStatus
 Output    :    The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIfRcvAddressStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetIfRcvAddressStatus (INT4 i4IfIndex,
                          tSNMP_OCTET_STRING_TYPE * pIfRcvAddressAddress,
                          INT4 i4SetValIfRcvAddressStatus)
#else
INT1
nmhSetIfRcvAddressStatus (i4IfIndex, pIfRcvAddressAddress,
                          i4SetValIfRcvAddressStatus)
     INT4                i4IfIndex;
     tSNMP_OCTET_STRING_TYPE *pIfRcvAddressAddress;
     INT4                i4SetValIfRcvAddressStatus;

#endif
{

    UINT4               u4IfIndex;
    UINT1               au1EthMACAddr[CFA_MAX_MEDIA_ADDR_LEN];
    tTMO_SLL           *pIfRcvAddrTab = NULL;
    UNUSED_PARAM (pIfRcvAddrTab);

    u4IfIndex = (UINT4) i4IfIndex;
    /* Null check has to be provided here to avoid crash as MSR does 
     * not callTest routine and verify the existence of the interface*/
    if (CFA_IF_ENTRY (u4IfIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }

    MEMSET (au1EthMACAddr, 0, CFA_MAX_MEDIA_ADDR_LEN);
    MEMCPY (au1EthMACAddr, pIfRcvAddressAddress->pu1_OctetList,
            pIfRcvAddressAddress->i4_Length);

    pIfRcvAddrTab = &CFA_IF_RCVADDRTAB (u4IfIndex);

    switch (i4SetValIfRcvAddressStatus)
    {
        case CFA_RS_ACTIVE:
            return SNMP_SUCCESS;    /* just return success */
        case CFA_RS_CREATEANDGO:
        case CFA_RS_CREATEANDWAIT:

            if (CfaIfmEnetConfigMcastAddr
                (CFA_ADD_ENET, u4IfIndex, 0, au1EthMACAddr) == CFA_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            else
            {
                return SNMP_FAILURE;
            }

        case CFA_RS_DESTROY:

            if (CfaIfmEnetConfigMcastAddr
                (CFA_DEL_ENET, u4IfIndex, 0, au1EthMACAddr) == CFA_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            else
            {
                return SNMP_FAILURE;
            }

        default:
            return SNMP_FAILURE;    /* others not supported */

    }
}

/****************************************************************************
 Function    :    nmhSetIfRcvAddressType
 Input    :    The Indices
        IfIndex
        IfRcvAddressAddress

        The Object
        setValIfRcvAddressType
 Output    :    The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIfRcvAddressType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetIfRcvAddressType (INT4 i4IfIndex,
                        tSNMP_OCTET_STRING_TYPE * pIfRcvAddressAddress,
                        INT4 i4SetValIfRcvAddressType)
#else
INT1
nmhSetIfRcvAddressType (i4IfIndex, pIfRcvAddressAddress,
                        i4SetValIfRcvAddressType)
     INT4                i4IfIndex;
     tSNMP_OCTET_STRING_TYPE *pIfRcvAddressAddress;
     INT4                i4SetValIfRcvAddressType;

#endif
{

    UINT4               u4IfIndex;
    tTMO_SLL           *pIfRcvAddrTab = NULL;
    tRcvAddressInfo    *pScanNode = NULL;
    UINT1               au1EthMACAddr[CFA_MAX_MEDIA_ADDR_LEN];

    u4IfIndex = (UINT4) i4IfIndex;
    /* Null check has to be provided here to avoid crash as MSR does 
     * not callTest routine and verify the existence of the interface*/
    if (CFA_IF_ENTRY (u4IfIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }

    pIfRcvAddrTab = &CFA_IF_RCVADDRTAB (u4IfIndex);
    MEMSET (au1EthMACAddr, 0, CFA_MAX_MEDIA_ADDR_LEN);
    MEMCPY (au1EthMACAddr, pIfRcvAddressAddress->pu1_OctetList,
            pIfRcvAddressAddress->i4_Length);
    /* we have to scan the stack to find the node */
    TMO_SLL_Scan (pIfRcvAddrTab, pScanNode, tRcvAddressInfo *)
    {
        if (MEMCMP (pScanNode->au1Address, au1EthMACAddr,
                    pIfRcvAddressAddress->i4_Length) == 0)
        {
            pScanNode->u1AddressType = (UINT1) i4SetValIfRcvAddressType;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :    nmhTestv2IfRcvAddressStatus
 Input    :    The Indices
        IfIndex
        IfRcvAddressAddress

        The Object
        testValIfRcvAddressStatus
 Output    :    The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
 Error Codes :    The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IfRcvAddressStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2IfRcvAddressStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                             tSNMP_OCTET_STRING_TYPE * pIfRcvAddressAddress,
                             INT4 i4TestValIfRcvAddressStatus)
#else
INT1
nmhTestv2IfRcvAddressStatus (*pu4ErrorCode, i4IfIndex, pIfRcvAddressAddress,
                             i4TestValIfRcvAddressStatus)
     UINT4              *pu4ErrorCode;
     INT4                i4IfIndex;
     tSNMP_OCTET_STRING_TYPE *pIfRcvAddressAddress;
     INT4                i4TestValIfRcvAddressStatus;
#endif
{
    UINT4               u4IfIndex;
    UINT1               u1RowStatus;
    UINT1               u1IfType;
    UINT1               au1EthMACAddr[CFA_MAX_MEDIA_ADDR_LEN];
    tTMO_SLL           *pIfRcvAddrTab = NULL;
    tRcvAddressInfo    *pScanNode = NULL;
    UINT1               u1EntryExists = CFA_TRUE;

    u4IfIndex = (UINT4) i4IfIndex;
    u1RowStatus = (UINT1) i4TestValIfRcvAddressStatus;

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if ((CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE) &&
        (!CFA_IF_ENTRY_USED (u4IfIndex)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

    if (u1IfType != CFA_ENET)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Do not allow the set operation on IfRcvAddrTable, if
     * promiscuous mode is enabled for that interface */
    if (CFA_IF_PROMISC (u4IfIndex) == CFA_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIfRcvAddressAddress->pu1_OctetList == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check the Mac Addr Len matches in Chars for dotted decimal notation */
    if (pIfRcvAddressAddress->i4_Length > CFA_MAX_MEDIA_ADDR_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    MEMSET (au1EthMACAddr, 0, CFA_MAX_MEDIA_ADDR_LEN);
    MEMCPY (au1EthMACAddr, pIfRcvAddressAddress->pu1_OctetList,
            pIfRcvAddressAddress->i4_Length);

/* check if entry exists or not */
    pIfRcvAddrTab = &CFA_IF_RCVADDRTAB (u4IfIndex);

    /* scan the list the find the node */
    TMO_SLL_Scan (pIfRcvAddrTab, pScanNode, tRcvAddressInfo *)
    {
        if (MEMCMP (pScanNode->au1Address, au1EthMACAddr,
                    pIfRcvAddressAddress->i4_Length) == 0)
            /* found an entry, return Success */
            break;
    }

    if (pScanNode == NULL)
        u1EntryExists = CFA_FALSE;

    /* check for the RowStatus Validity */
    switch (u1RowStatus)
    {

        case CFA_RS_CREATEANDWAIT:
        case CFA_RS_CREATEANDGO:
            if (u1EntryExists == CFA_FALSE)
                return SNMP_SUCCESS;
            break;

        case CFA_RS_ACTIVE:
            if (u1EntryExists == CFA_TRUE)
                return SNMP_SUCCESS;
            break;

        case CFA_RS_DESTROY:
            if (u1EntryExists == CFA_TRUE)
                return SNMP_SUCCESS;
            break;

        default:
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :    nmhTestv2IfRcvAddressType
 Input    :    The Indices
        IfIndex
        IfRcvAddressAddress

        The Object
        testValIfRcvAddressType
 Output    :    The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
 Error Codes :    The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IfRcvAddressType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2IfRcvAddressType (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                           tSNMP_OCTET_STRING_TYPE * pIfRcvAddressAddress,
                           INT4 i4TestValIfRcvAddressType)
#else
INT1
nmhTestv2IfRcvAddressType (*pu4ErrorCode, i4IfIndex, pIfRcvAddressAddress,
                           i4TestValIfRcvAddressType)
     UINT4              *pu4ErrorCode;
     INT4                i4IfIndex;
     tSNMP_OCTET_STRING_TYPE *pIfRcvAddressAddress;
     INT4                i4TestValIfRcvAddressType;
#endif
{

    UINT1               u1RcvAddrType;
    UINT4               u4IfIndex;

/* for index validation - because validation is commented in mid-level */
    if (nmhValidateIndexInstanceIfRcvAddressTable (i4IfIndex,
                                                   pIfRcvAddressAddress) !=
        SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u4IfIndex = (UINT4) i4IfIndex;
    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }

    /* Do not allow the set operation on IfRcvAddrTable, if
     * promiscuous mode is enabled for that interface */
    if (CFA_IF_PROMISC (u4IfIndex) == CFA_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* to remove unused warning */
    pIfRcvAddressAddress = pIfRcvAddressAddress;
    u1RcvAddrType = (UINT1) i4TestValIfRcvAddressType;

    if ((u1RcvAddrType == CFA_RCVADDR_VOL) ||
        (u1RcvAddrType == CFA_RCVADDR_NVOL) ||
        (u1RcvAddrType == CFA_RCVADDR_OTHER))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :    nmhGetIfNumber
 Input    :    The Indices

        The Object
        retValIfNumber
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIfNumber ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIfNumber (INT4 *pi4RetValIfNumber)
#else
INT1
nmhGetIfNumber (pi4RetValIfNumber)
     INT4               *pi4RetValIfNumber;
#endif
{

    *pi4RetValIfNumber = (INT4) CFA_IFNUMBER ();

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :    nmhGetIfTableLastChange
 Input    :    The Indices

        The Object
        retValIfTableLastChange
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIfTableLastChange ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIfTableLastChange (UINT4 *pu4RetValIfTableLastChange)
#else
INT1
nmhGetIfTableLastChange (pu4RetValIfTableLastChange)
     UINT4              *pu4RetValIfTableLastChange;
#endif
{

    *pu4RetValIfTableLastChange = CFA_IFTAB_CHNG ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetIfStackLastChange
 Input    :    The Indices

        The Object
        retValIfStackLastChange
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIfStackLastChange ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIfStackLastChange (UINT4 *pu4RetValIfStackLastChange)
#else
INT1
nmhGetIfStackLastChange (pu4RetValIfStackLastChange)
     UINT4              *pu4RetValIfStackLastChange;
#endif
{

    *pu4RetValIfStackLastChange = CFA_IFSTK_CHNG ();

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IfRcvAddressTable
 Input       :  The Indices
                IfIndex
                IfRcvAddressAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfRcvAddressTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
