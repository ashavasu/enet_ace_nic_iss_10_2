#include "cfainc.h"

/* -------------------------------------------------------------
 *
 * Function: NpSyncProcessSyncMsg
 *
 * -------------------------------------------------------------
 */
PUBLIC VOID
CfaNpSyncProcessSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    INT4                NpApiId = 0;
    unNpSync            NpSync;

    MEMSET (&NpSync, 0, sizeof (unNpSync));

    NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpApiId);

    switch (NpApiId)
    {
        case NPSYNC_FS_CFA_HW_CREATE_INTERNAL_PORT:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsCfaHwCreateInternalPort.u4IfIndex);
            break;

        case NPSYNC_FS_CFA_HW_DELETE_INTERNAL_PORT:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsCfaHwDeleteInternalPort.u4IfIndex);
            break;

        case NPSYNC_FS_CFA_HW_CREATE_I_LAN:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsCfaHwCreateILan.u4ILanIndex);
            break;

        case NPSYNC_FS_CFA_HW_DELETE_I_LAN:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsCfaHwDeleteILan.u4ILanIndex);
            break;

        case NPSYNC_FS_CFA_HW_REMOVE_PORT_FROM_I_LAN:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsCfaHwRemovePortFromILan.u4ILanIndex);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsCfaHwRemovePortFromILan.u4PortIndex);
            break;

        case NPSYNC_FS_CFA_HW_ADD_PORT_TO_I_LAN:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsCfaHwAddPortToILan.u4ILanIndex);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsCfaHwAddPortToILan.u4PortIndex);
            break;

        default:
            break;
    }

    /* Create entry or delete the existing entry */
    CfaHwAuditCreateOrFlushBuffer (&NpSync, NpApiId, 0);

}
