/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddqcax.c,v 1.3 2018/01/18 09:56:47 siva Exp $
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/
#include "cfainc.h"
#include "fsvlan.h"
#include "vlangarp/garp.h"
#ifdef NPAPI_WANTED
#include "cfanp.h"
#endif
#include "gddwaspapi.h"
#include "cfaport.h"
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include "gddapi.h"

static INT4         gi4PortDesc;

tFdListStruct       FdTable;    /* list of all the file descriptors given to the select
                                   call during polling */

PRIVATE INT4        NpCfaIsVlanTagRequest (tCRU_BUF_CHAIN_HEADER * pBuf);
UINT1              *CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
UINT1              *CfaGddGetLnxIntfnameForFcapwap (UINT2 u2Index);
VOID                CfaGddSetLnxIntfnameForPort (UINT4 u4Index,
                                                 UINT1 *pu1IfName);
UINT1               CfaGddGetWlanIfName (UINT2 u2WlanIfIndex,
                                         UINT1 *pu1WlanIfName);

PUBLIC INT4         CfaGddConfigIp (UINT4 u4IpAddr);

extern INT4         CustNpSend80211Frame (UINT4 u4WlanIfIndex, UINT1 *u1PktData,
                                          UINT1 type, UINT1 subtype,
                                          INT4 i4Len);
pollfdstruct        aGddFdList[SYS_DEF_MAX_INTERFACES];
UINT4               au4GddIfIndexList[SYS_DEF_MAX_INTERFACES];

/* Array containing the Mapping between Slot and Eth Interfaces */
#ifdef T1023_WANTED
static UINT1
     
     
     
     au1IntMapTable[SYS_DEF_MAX_PHYSICAL_INTERFACES][2]
    [CFA_MAX_PORT_NAME_LENGTH] = { {"Slot0/1", "br-lan"},
{"Slot0/2", "fm1-mac1"},
{"Slot0/3", "fm1-mac4"},
{"Slot0/4", "ath1"},
{"Slot0/5", "ath2"},
{"Slot0/6", "ath3"},
{"Slot0/7", "ath16"},
{"Slot0/8", "ath17"},
{"Slot0/9", "ath18"},
{"Slot0/10", "ath19"},
};
#elif AP200_WANTED
static UINT1
     
     
     
     au1IntMapTable[SYS_DEF_MAX_PHYSICAL_INTERFACES][2]
    [CFA_MAX_PORT_NAME_LENGTH] = { {"Slot0/1", "br0"},
{"Slot0/2", "eth0"},
};
#else
static UINT1
     
     
     
     au1IntMapTable[SYS_DEF_MAX_PHYSICAL_INTERFACES][2]
    [CFA_MAX_PORT_NAME_LENGTH] = {
    {"Slot0/1", "br-lan"},
    {"Slot0/2", "eth1"},
    {"Slot0/3", "eth0"},
    {"Slot0/4", "br0"},
    {"Slot0/5", "br1"},
    {"Slot0/6", "br2"},
    {"Slot0/7", "br3"},
    {"Slot0/8", "br4"},
    {"Slot0/9", "br5"},
    {"Slot0/10", "br6"},
    {"Slot0/11", "br7"},
    {"Slot0/12", "br16"},
    {"Slot0/13", "br17"},
    {"Slot0/14", "br18"},
    {"Slot0/15", "br19"},
    {"Slot0/16", "br20"},
    {"Slot0/17", "br21"},
    {"Slot0/18", "br22"},
    {"Slot0/19", "br23"},
};
#endif

/*****************************************************************************
 *    Function Name      : CfaGddProcessRecvInterruptEvent ()
 *    Description        : This function is invoked when an driver 
 *                         event occurs. This routine invokes the packet
 *                         processing function if a packet is received 
 *                         else it invokes the function which process the
 *                         driver status if status indication message is
 *                         received
 *    Input(s)           : VOID
 *    Output(s)          : None.
 
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Returns                  : None.
 *****************************************************************************/

PUBLIC INT4
CfaGddProcessRecvInterruptEvent (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pCruBuf;
    UINT4               u4IfIndex;
    UINT4               u4PktSize;
    UINT1               au1EnetHdr[VLAN_TAG_OFFSET];
    UINT1               u1IfType;

    while (OsixReceiveFromQ
           (SELF, (UINT1 *) CFA_PACKET_QUEUE_MUX, OSIX_NO_WAIT, 0,
            (tCRU_BUF_CHAIN_HEADER **) & pCruBuf) == 0)
    {
        CFA_LOCK ();

        /* extract the interface index and packet size from buffer */
        if (NpCfaIsVlanTagRequest (pCruBuf) == CFA_FALSE)
        {
            CRU_BUF_Copy_FromBufChain (pCruBuf, au1EnetHdr, 0, VLAN_TAG_OFFSET);
            CRU_BUF_Move_ValidOffset (pCruBuf,
                                      VLAN_TAG_OFFSET + VLAN_TAG_PID_LEN);
            CRU_BUF_Prepend_BufChain (pCruBuf, au1EnetHdr, VLAN_TAG_OFFSET);
        }

        u4IfIndex = pCruBuf->ModuleData.InterfaceId.u4IfIndex;
        u4PktSize = pCruBuf->pFirstValidDataDesc->u4_ValidByteCount;

        if (CfaGetIfType (u4IfIndex, &u1IfType) == CFA_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                     "In CfaMain Pkt recvd in interrupt mode failed to deliver to HL.\n");
        }
        else
        {
            /* Handover the Packet to Receive module */
            if ((CFA_GDD_HL_RX_FNPTR (u4IfIndex))
                (pCruBuf, u4IfIndex, u4PktSize, u1IfType,
                 CFA_ENCAP_NONE) != CFA_SUCCESS)
            {
                /* release buffer which were not successfully sent to higher layer
                 */
                CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "In CfaMain Pkt recvd in interrupt mode failed to deliver to HL.\n");
            }
            else
            {
                /* increment after successful handling of packet */
                CFA_IF_SET_IN_OCTETS (u4IfIndex, u4PktSize);
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "In CfaMain Pkt recvd in interrupt mode delivered to HL.\n");
            }
        }
        CFA_UNLOCK ();
    }

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *    Function Name       : CfaGddInit
 *    Description         : This function performs the initialisation of
 *                          the  Device Driver Module of CFA. This routine
 *                          1. creates the Queue for reception of packets from
 *                             driver
 *                          2. creates the memory pool for the driver messages
 *                          3. initializes the driver to cfa mapping tables
 *    Input(s)            : None.
 *    Output(s)           : None.
 *
 *    Global Variables Referred : None 
 *    Global Variables Modified : gaCfaNpIndexMap 
 *                               
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if initialisation succeeds,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
CfaGddInit (VOID)
{
    tMacAddr            au1SwitchMac;    /* Base MAC address */

    MEMSET (au1SwitchMac, 0, CFA_ENET_ADDR_LEN);
    /* allocate memory for the GDD File Descriptor - polling table */
    CFA_GDD_FDLIST () = &aGddFdList[0];

    /* allocate memory for the ifIndex array - polling table */
    CFA_GDD_IFINDEX_LIST () = &au4GddIfIndexList[0];

    CFA_GDD_FDLIST_LAST_INDEX () = 0;
    /* Getting the Switch MAC address */
    //   CfaGetSysMacAddress (au1SwitchMac);

    /* Updating MAC address for all ports */
    //CfaNpUpdatePortMac (au1SwitchMac); 

    /* Initialize the Board and UnitInfo structures */
    /*if (CfaNpInit () != FNP_SUCCESS)
       {
       PRINTF ("ERROR[NP] - CfaNpInit returned failure\n\r");
       return (CFA_FAILURE);
       } */
    return (CFA_SUCCESS);
}

#ifdef MBSM_WANTED
/*****************************************************************************
 *    Function Name       : CfaMbsmGddInit
 *    Description         : This function performs the initialisation of
 *                          the  Device Driver Module of CFA. This routine
 *                          1. creates the Queue for reception of packets from
 *                             driver
 *                          2. creates the memory pool for the driver messages
 *                          3. initializes the driver to cfa mapping tables
 *    Input(s)            : None.
 *    Output(s)           : None.
 *
 *    Global Variables Referred : _devices, _ndevices
 *    Global Variables Modified : gFsDrvMemPoolId, gaCfaNpIndexMap
 *                                gaCfaNpDevMap
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if initialisation succeeds,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

PUBLIC INT4
CfaMbsmGddInit (tMbsmSlotInfo * pSlotInfo)
{
    /* Update the Driver to cfa mapping tables for the given slot.
     */
    UNUSED_PARAM (pSlotInfo);

    return CFA_SUCCESS;
}

#endif /* MBSM_WANTED */

/*****************************************************************************
 *    Function Name        : CfaGddShutdown
 *    Description         : This function performs the initialisation of
 *                the Generic Device Driver Module of CFA. This
 *                initialization routine should be called
 *                from the init of IFM after we have obtained
 *                the number of physical ports after parsing of
 *                the Config file. This function initializes the FD list
 *                and ifIndex array of the polling table.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : FdTable
 *
 *    Global Variables Modified : FdTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

PUBLIC VOID
CfaGddShutdown (VOID)
{
    return;
}

#ifdef MBSM_WANTED
/*****************************************************************************
 *    Function Name        : CfaMbsmGddDeInit
 *    Description         : This function performs the shutdown of
 *                the Generic Device Driver Module of CFA. This
 *                shutdown routine should be called
 *                before the protocols are informed about the shutdown.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : FdTable
 *
 *    Global Variables Modified : FdTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
CfaMbsmGddDeInit (tMbsmSlotInfo * pSlotInfo)
{
    /* Re-initialize the Slot info paramaters for the given slot */
    UNUSED_PARAM (pSlotInfo);

    return CFA_SUCCESS;
}

#endif /* MBSM_WANTED */

/*****************************************************************************
 *
 *    Function Name        : CfaGddOsRemoveIfFromList
 *
 *    Description        : Function is to remove the Ifindex from the port list
 *
 *    Input(s)           : UINT4 u4IfIndex.
 *
 *    Output(s)          : None
 *
 *    Returns            : None
 *
 *****************************************************************************/
VOID
CfaGddOsRemoveIfFromList (UINT4 u4IfIndex)
{
    UINT2               u2PollTableIndex = 0;
/* remove the port from the polling table */
    for (; u2PollTableIndex < CFA_GDD_FDLIST_LAST_INDEX (); ++u2PollTableIndex)
    {
        if (u4IfIndex == CFA_GDD_FDLIST_IFINDEX (u2PollTableIndex))
        {
            CFA_GDD_FDLIST_DEINIT (u2PollTableIndex);
            break;
        }
    }
    return;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddOsProcessRecvEvent
 *
 *    Description        : Function is to process the Recv Event from os
 *
 *    Input(s)           : None
 *
 *    Output(s)          : None
 *
 *    Returns            : CFA_SUCCESS
 *
 *****************************************************************************/
INT4
CfaGddOsProcessRecvEvent (VOID)
{
    UINT4               u4IfIndex = 0;
    INT4                i4ReadPortNum = 0;
    UINT2               u2PollTableIndex = 0;
    UINT1              *pu1DataBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4PktSize = 0;
    UINT2               u2PortIfIndex = 0;
    UINT1               u1IfType = 0;
    UINT1               u1SubType = CFA_SUBTYPE_SISP_INTERFACE;
#ifdef OPENFLOW_WANTED
    UINT4               u4VlanIndex = 0;
#endif
    /* MAX_DRIVER_MTU * 2 in order to allow for the worst case of byte stuffing
     * for async interfaces where the packet size doubles over the MTU */
    if ((pu1DataBuf = MemAllocMemBlk (gCfaDriverMtuPoolId)) == NULL)
    {

        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaGddProcessRecvEvent - " "No Memory - FAILURE.\n");
        return (CFA_FAILURE);
    }
    if ((i4ReadPortNum = poll (CFA_GDD_FDLIST (), CFA_GDD_FDLIST_LAST_INDEX (),
                               0)) < 0)
    {

        MemReleaseMemBlock (gCfaDriverMtuPoolId, (UINT1 *) pu1DataBuf);
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaGddProcessRecvEvent - Poll Call - FAILURE.\n");
        return (CFA_FAILURE);
    }
    while ((i4ReadPortNum > 0) &&
           (u2PollTableIndex < CFA_GDD_FDLIST_LAST_INDEX ()))
    {
        u4IfIndex = CFA_GDD_FDLIST_IFINDEX (u2PollTableIndex);
        /* Check the validity of the index */
        if (u4IfIndex == 0 || u4IfIndex > SYS_DEF_MAX_INTERFACES)
        {
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                      "Error in CfaGddProcessRecvEvent\n"
                      "Value at index %d in FdTable is 0.\n", u2PollTableIndex);

            MemReleaseMemBlock (gCfaDriverMtuPoolId, (UINT1 *) pu1DataBuf);

            return (CFA_FAILURE);
        }

        CfaGetIfType (u4IfIndex, &u1IfType);

        u2PortIfIndex = (UINT2) u4IfIndex;
        /* if data is not available then dont do read operation */
        if (!CFA_GDD_IS_DATA_AVAIL (u2PollTableIndex))
        {

            ++u2PollTableIndex;
            continue;
        }
        --i4ReadPortNum;

        /* now read all the data from the interface */
        while ((CFA_GDD_READ_FNPTR (u2PortIfIndex))
               (pu1DataBuf, u4IfIndex, &u4PktSize) != CFA_FAILURE)
        {
            CFA_LOCK ();

            CFA_DBG1 (CFA_TRC_ALL, CFA_GDD,
                      "In CfaGddProcessRecvEvent - got data from interface %d.\n",
                      u4IfIndex);
            CFA_DBG1 (CFA_TRC_ALL, CFA_GDD,
                      "In CfaGddProcessRecvEvent - got %d bytes.\n", u4PktSize);

#ifdef RMON_WANTED
            /* Forward the frames to RMON module only when SW_FWD is enabled */
#ifdef SW_FWD
            /* send packet to RMON before converting the buffer to CRU */
            if (CFA_GDD_TYPE (u2PortIfIndex) == CFA_ETHERNET)
            {
                /*invoke rmon update tables routine */
                if (CfaIwfRmonUpdateTables (pu1DataBuf, u4IfIndex, u4PktSize)
                    != CFA_FAILURE)
                {
                    CFA_DBG (CFA_TRC_ALL, CFA_GDD,
                             "In CfaGddProcessRecvEvent - update RMON tables by invoking CfaIwfRmonUpdateTables \n");
                }
                else
                {
                    CFA_DBG (CFA_TRC_ERROR, CFA_GDD,
                             "In CfaGddProcessRecvEvent - invoke CfaIwfRmonUpdateTables function failed \n");
                }
            }
#endif /* SW_FWD */
#endif /* RMON_WANTED */
            /* allocate CRU Buf */
            if ((pBuf = CRU_BUF_Allocate_MsgBufChain ((u4PktSize +
                                                       CFA_MAX_FRAME_HEADER_SIZE),
                                                      CFA_MAX_FRAME_HEADER_SIZE))
                == NULL)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddProcessRecvEvent - "
                         "No CRU Buf Available - FAILURE.\n");

                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                MemReleaseMemBlock (gCfaDriverMtuPoolId, (UINT1 *) pu1DataBuf);
                CFA_UNLOCK ();
                return CFA_FAILURE;
            }

            /* copy data from linear buffer into CRU buffer - it is expected that the
               higher layers will fill the CRU interface struct if required */
            if (CRU_BUF_Copy_OverBufChain (pBuf, pu1DataBuf, 0, u4PktSize) ==
                CRU_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddProcessRecvEvent - "
                         "unable to copy to CRU Buf- FAILURE.\n");

                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                CFA_UNLOCK ();
                continue;
            }

#ifdef OPENFLOW_WANTED
            if (OfcHandleVlanPkt (pBuf, &u4VlanIndex) == OSIX_SUCCESS)
            {
                if (OpenflowNotifyPktRecv (u4VlanIndex, pBuf) == OFC_FAILURE)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                }
                CFA_UNLOCK ();
                continue;
            }
#endif /* OPENFLOW_WANTED */

            CfaGetIfSubType ((UINT4) u4IfIndex, &u1SubType);
            if (u1SubType == CFA_SUBTYPE_OPENFLOW_INTERFACE)
            {
                CFA_IF_SET_IN_OCTETS (u4IfIndex, u4PktSize);
                CFA_IF_SET_IN_UCAST (u4IfIndex);
#ifdef OPENFLOW_WANTED
                /* 
                 * Make the openflow ports visble to the ISS context when the Client 
                 * context associated with the IfIndex is in fail stand alone mode 
                 * and also process the packets via CFA.Else process the 
                 * packets through the Openflow Pipeline Process.
                 */
                if (OfcHandleOpenflowSwModeProcess (&u4IfIndex, pBuf) !=
                    OFC_SUCCESS)
                {
                    if (OpenflowNotifyPktRecv (u4IfIndex, pBuf) == OFC_FAILURE)
                    {
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    }
                    CFA_UNLOCK ();
                    continue;
                }
#else
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CFA_UNLOCK ();
                continue;
#endif /* OPENFLOW_WANTED */
            }

            /* send the buffer to the higher layer - the port is not opened unless
               some layer is defined abover this */

            if (CFA_GDD_HL_RX_FNPTR (u2PortIfIndex) == NULL)
            {
                /* Higher layer is not found. Release buffer */
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                CFA_UNLOCK ();
                continue;
            }

            if ((CFA_GDD_HL_RX_FNPTR (u2PortIfIndex))
                (pBuf, u4IfIndex, u4PktSize, u1IfType,
                 CFA_ENCAP_NONE) != CFA_SUCCESS)
            {
                /* release buffer which were not successfully sent to higher layer */
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            }

            CFA_UNLOCK ();
        }                        /* end of while loop - read while data is available */

        ++u2PollTableIndex;

    }                            /* end of while loop - polling */

    /* free the linear buffer allocated */
    MemReleaseMemBlock (gCfaDriverMtuPoolId, (UINT1 *) pu1DataBuf);
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddGetOsHwAddr
 *
 *    Description        : Function is called for getting the hardware
 *                         address of the Enet ports at Interface initialisation.
 *                         This function can be called only at port creation time.
 *                         The MAC address is obtained in the same way as it is used
 *                         for IndexMap table initialisation at GddInit.
 *
 *    Input(s)           : UINT4 u4IfIndex.
 *
 *    Output(s)          : UINT1 *au1HwAddr.
 *
 *    Returns            : CFA_SUCCESS if address is obtained,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaGddGetOsHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
    /* For ETH_SOCK */
    struct ifreq        ifr;    /* struct which contains status of interface */
    INT4                i4CommandStatus;
    UINT1               u1IfType;
    /* end of for ETH_SOCK */
    UINT1               au1IfHwAddr[CFA_ENET_ADDR_LEN];
    UINT1              *pu1PortName = NULL;

    CfaGetIfaceType (u4IfIndex, &u1IfType);
    if (u1IfType == CFA_PSEUDO_WIRE || u1IfType == CFA_PPP)
    {
        if (CfaGetPswHwAddr (u4IfIndex, au1HwAddr) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmInitEnetIfEntry - "
                      "unable to get the socket or MAC address for interface %d\n",
                      u4IfIndex);
            return CFA_FAILURE;
        }
        return CFA_SUCCESS;
    }

    if (CfaGddLinuxEthSockOpen (u4IfIndex) != CFA_SUCCESS)
    {
        return (CFA_FAILURE);
    }

    if ((u4IfIndex > SYS_DEF_MAX_INTERFACES) || (u4IfIndex < 1))
    {

        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddGetOsHwAddr - "
                  "Interface %u not Valid - FAILURE.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CfaGetIfType (u4IfIndex, &u1IfType);
    /* 
     * When the request comes from ARP for the hardware address 
     * of VLAN interface, this function should not return FAILURE. Instead, 
     * copy the MAC address of the default bridged Ethernet interface into 
     * the MAC address of VLAN interface and ARP/RARP will update the same 
     * for VLAN interfaces.
     */
    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        if ((u1IfType == CFA_L3IPVLAN)
#ifdef WGS_WANTED
            || (u1IfType == CFA_L2VLAN)
#endif /* WGS_WANTED */
            )
        {
            CfaGetIfHwAddr (CFA_DEFAULT_ROUTER_IFINDEX, au1IfHwAddr);
            MEMCPY (au1HwAddr, au1IfHwAddr, CFA_ENET_ADDR_LEN);

            return CFA_SUCCESS;
        }
    }

    /* cant get HW address for ports which are not registered - we dont have
       socket/file descriptors for them and only ETH_SOCK is supported currently */
    if ((CFA_GDD_REGSTAT (u4IfIndex) == CFA_INVALID) ||
        (CFA_GDD_TYPE (u4IfIndex) != CFA_ETHERNET))
    {

        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddGetOsHwAddr - "
                  "Interface %u not Registered/Enet - FAILURE.\n", u4IfIndex);

        return (CFA_FAILURE);
    }

    /* invoke the IOCTL call on the socket */
    pu1PortName = CfaGddGetLnxIntfnameForPort ((UINT2) u4IfIndex);

    /*If No Mapping Exists,No Such Physical Interface can Exists.So Just
       return FAILURE */
    if (pu1PortName == NULL)
    {
        return (CFA_FAILURE);
    }

    STRNCPY (ifr.ifr_name, pu1PortName, STRLEN (pu1PortName));
    ifr.ifr_name[STRLEN (pu1PortName)] = '\0';

    if ((i4CommandStatus = ioctl (CFA_GDD_PORT_DESC (u4IfIndex), SIOCGIFHWADDR,
                                  &ifr)) == 0)
    {
        MEMCPY (au1HwAddr, ifr.ifr_hwaddr.sa_data, 6);
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_GDD,
                  "In CfaGddGetOsHwAddr - "
                  "Got Address of interface %u - SUCCESS.\n", u4IfIndex);
    }
    else
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddGetOsHwAddr - "
                  "IOCTL on Interface %u failed - FAILURE.\n", u4IfIndex);
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddConfigPort
 *
 *    Description        : Function is called for configuration of the
 *                Enet ports. No such support is available for
 *                Wanic at present. Only the Promiscuous mode
 *                and multicast addresses can be configured at
 *                present for Enet ports. This is called
 *                directly whenever the Manager configures the
 *                ifTable for Ethernet ports. It can also be
 *                called indirectly by Bridge or RIP, etc.
 *                through the CfaIfmEnetConfigMcastAddr API.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *                UINT1 u1ConfigOption,
 *                VOID *pConfigParam.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if configuration succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption, VOID *pConfigParam)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1ConfigOption);
    UNUSED_PARAM (pConfigParam);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddLinuxEthSockOpen
 *
 *    Description        : Function is to open an linux ethernet socket
 *
 *    Input(s)           : Interface index
 *
 *    Output(s)          : None
 *
 *    Returns            : CFA_SUCCESS
 *
 *****************************************************************************/

INT4
CfaGddLinuxEthSockOpen (UINT4 u4IfIndex)
{
    struct sockaddr_ll  Enet;
    struct ifreq        ifr;
    UINT1              *pu1PortName = NULL;
    INT4                i4CommandStatus = 0;
    INT4                i4PortDesc = 0;
    INT4                i4AuxVal = 0;

    MEMSET (&Enet, 0, sizeof (Enet));
    MEMSET (&ifr, 0, sizeof (ifr));

    pu1PortName = CfaGddGetLnxIntfnameForPort ((UINT2) u4IfIndex);

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddSetGddType
 *
 *    Description        : Function is to get the GDD type set.
 *
 *    Input(s)           : UINT4 u4IfIndex.
 *
 *    Output(s)          : None
 *
 *    Returns            : CFA_SUCCESS.
 *
 *****************************************************************************/
INT4
CfaGddSetGddType (UINT4 u4IfIndex)
{
    UINT1               u1IfType;

    CfaGetIfType (u4IfIndex, &u1IfType);

    CFA_GDD_TYPE (u4IfIndex) = u1IfType;

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthSockOpen
 *
 *    Description        : Function is to open the GDD Ethernet Socket.
 *
 *    Input(s)           : UINT4 u4IfIndex.
 *
 *    Output(s)          : None
 *
 *    Returns            : CFA_SUCCESS.
 *
 *****************************************************************************/
PUBLIC INT4
CfaGddEthSockOpen (UINT4 u4IfIndex)
{
    INT4                i4RetVal = CFA_SUCCESS;
    UNUSED_PARAM (u4IfIndex);
    return i4RetVal;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthSockClose
 *
 *    Description        : Function is to close the GDD Ethernet Socket.
 *
 *    Input(s)           : UINT4 u4IfIndex.
 *
 *    Output(s)          : None
 *
 *    Returns            : CFA_SUCCESS.
 *
 *****************************************************************************/
PUBLIC INT4
CfaGddEthSockClose (UINT4 u4IfIndex)
{
    INT4                i4RetVal = CFA_SUCCESS;
    UNUSED_PARAM (u4IfIndex);
    return i4RetVal;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthSockRead
 *
 *    Description        : Function is to read the Ethernet Socket
 *
 *    Input(s)           : UINT4 u4IfIndex.
 *               pu1DataBuf - Buffer.
 *
 *    Output(s)          : pu4PktSize - Packet size
 *
 *    Returns            : CFA_SUCCESS.
 *
 *****************************************************************************/
PUBLIC INT4
CfaGddEthSockRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    INT4                i4RetVal = CFA_FAILURE;
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (pu4PktSize);
    return i4RetVal;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddOsAddIfToList
 *
 *    Description        : Function is to add the interface to the list.
 *
 *    Input(s)           : UINT4 u4IfIndex.
 *
 *    Output(s)          : None
 *
 *    Returns            : None.
 *
 *****************************************************************************/
PUBLIC VOID
CfaGddOsAddIfToList (UINT4 u4IfIndex)
{
    UINT1               u1IfType = 0;

    CfaGetIfType (u4IfIndex, &u1IfType);
    CFA_GDD_FDLIST_INIT (u4IfIndex, CFA_GDD_PORT_DESC (u4IfIndex));
}

/* Link up/down changes start */
/*****************************************************************************
 *    Function Name        : CfaGddGetLinkStatus
 *    Description         : This function  retrives the status of the Link
 *    Input(s)            : None.
 *    Output(s)            : None.
 *    Global Variables Referred : FdTable
 *    Global Variables Modified : FdTable
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Use of Recursion        : None.
 *    Returns            : CFA_IF_DOWN if the Link is Down
 *                         else, CFA_IF_UP.
 *
 *****************************************************************************/
PUBLIC UINT1
CfaGddGetLinkStatus (UINT4 u4IfIndex)
{
    return CfaNpGetLinkStatus (u4IfIndex);
}

/*****************************************************************************
 *    Function Name        : CfaGddGetPhyAndLinkStatus
 *    Description         : This function  retrives the status of the Link and
 *                          PHY
 *    Input(s)            : None.
 *    Output(s)            : None.
 *    Global Variables Referred : FdTable
 *    Global Variables Modified : FdTable
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Use of Recursion        : None.
 *    Returns            : CFA_IF_DOWN if the Link  or PHY is Down
 *                         else, CFA_IF_UP both LINK and PHY is up.
 *
 *****************************************************************************/

PUBLIC UINT1
CfaGddGetPhyAndLinkStatus (UINT2 u2IfIndex)
{
    return CfaNpGetPhyAndLinkStatus (u2IfIndex);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthOpen
 *
 *    Description        : This function opens the Ethernet port and stores the
                           descriptor in the interface table. Since the port is 
                           opened already during the registration time, this 
                           function is dummy.
 *
 *    Input(s)            : u4IfIndex - Interface Index
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable (Interface table)
 *                                structure
 *
 *    Global Variables Modified :  gapIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if open succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthOpen (UINT4 u4IfIndex)
{

    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthClose
 *
 *    Description        : First disables the promiscuous mode of 
 *                         the port, if enabled. The multicast address
 *                         list is lost when the port is closed.i
 *                         We dont need to check if the call is a 
 *                         success or not.
 *
 *    Input(s)            : u4IfIndex - Interface index.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable
 *
 *    Global Variables Modified : gapIfTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if close succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthClose (UINT4 u4IfIndex)
{

    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthWrite
 *
 *    Description        : Writes the data to the ethernet driver.
 *
 *    Input(s)            : pu1DataBuf - Pointer to the linear buffer.
 *                          u4IfIndex - MIB-2 interface index
 *                          u4PktSize - Size of the buffer.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable (Interface table)
 *                                structure,
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if write succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    struct sockaddr_ll  Enet;
    UINT4               u4ToAddrLen = 0;
    INT4                i4WrittenBytes = 0;
    struct ifreq        ifr;
    UINT1              *pu1PortName = NULL;
    UINT1               u1IfType = 0;

    CfaGetIfType (u4IfIndex, &u1IfType);
    if (u1IfType == CFA_WLAN_RADIO)
    {
        /* For wireless interface */
        if (pu1DataBuf[0] == CFA_TYPE_WIRELESS)
        {
            if (CustNpSend80211Frame (u4IfIndex, pu1DataBuf, 0,
                                      (CFA_OFFSET_10 + CFA_OFFSET_4),
                                      u4PktSize) != FNP_SUCCESS)
            {
                return CFA_FAILURE;
            }
        }
        else
        {
            if (CustNpSend80211Frame (u4IfIndex, pu1DataBuf, 0,
                                      (CFA_OFFSET_10 + CFA_OFFSET_4 +
                                       CFA_OFFSET_1), u4PktSize) != FNP_SUCCESS)
            {
                return CFA_FAILURE;
            }
        }
    }
    else
    {
        if (u4IfIndex > SYS_DEF_MAX_INTERFACES)
        {
            return CFA_FAILURE;
        }
        pu1PortName = CfaGddGetLnxIntfnameForPort ((UINT2) u4IfIndex);

        if (pu1PortName == NULL)
        {
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                      "Error in CfaGddEthWrite - "
                      "Not able to get Interface name for %d - FAILURE.\n",
                      u4IfIndex);
            return CFA_FAILURE;
        }

        /* For ENET Interfaces */
        MEMSET (&Enet, 0, sizeof (Enet));
        MEMSET (&ifr, 0, sizeof (ifr));

        sprintf (ifr.ifr_name, "%s", pu1PortName);
        if (ioctl (CFA_GDD_PORT_DESC (u4IfIndex), SIOCGIFINDEX, (char *) &ifr) <
            0)
        {
            perror ("Interface Index Get Failed");
            close (CFA_GDD_PORT_DESC (u4IfIndex));
            return CFA_FAILURE;
        }

        Enet.sll_family = AF_PACKET;
        Enet.sll_ifindex = ifr.ifr_ifindex;

        u4ToAddrLen = sizeof (Enet);

        if ((i4WrittenBytes =
             sendto (CFA_GDD_PORT_DESC (u4IfIndex), (VOID *) pu1DataBuf,
                     u4PktSize, 0, (struct sockaddr *) &Enet,
                     (socklen_t) u4ToAddrLen)) < 0)
        {
            return CFA_FAILURE;
        }

        if (i4WrittenBytes != (INT4) u4PktSize)
        {
            return CFA_FAILURE;
        }
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *    Function Name             : CfaGddEthWriteWithPri
 *    Description               : Writes the data to the ethernet driver.
 *    Input(s)                  : pu1DataBuf - Pointer to the linear buffer.
 *                                u4IfIndex  - MIB-2 interface index
 *                                u4PktSize  - Size of the buffer.
 *                                u1Priority - Traffic class on which Pkt to
 *                            be TXed
 *    Output(s)                 : None.
 *    Global Variables Referred : None
 *    Global Variables Modified : None.
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : CFA_SUCCESS if write succeeds,
 *                                otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGddEthWriteWithPri (UINT1 *pData, UINT4 u4IfIndex, UINT4 u4PktLen,
                       UINT1 u1Priority)
{
    if (CfaGddEthWrite (pData, u4IfIndex, u4PktLen) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }
    UNUSED_PARAM (u1Priority);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthRead
 *
 *    Description        : This function reads the data from the ethernet port.
 *
 *    Input(s)            : pu1DataBuf - Pointer to the linear buffer.
 *                          u4IfIndex - IfIndex of the interface
 *                          pu4PktSize - Pointer to the packet size
 *
 *    Output(s)            : pu1DataBuf, pu1PktSize
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
#ifndef DAK_WANTED
    struct sockaddr_ll  Enet;
    struct sockaddr    *pPort = NULL;
    int                 i4FromAddrLen = 0;
    INT4                i4ReadBytes = 0;
    tEnetV2Header      *pEthHdr = NULL;
    tEnetSnapHeader    *pSnapEthHdr = NULL;
    INT2                i2IpTotalLen = 0;
    UINT2               u2Protocol = 0xFF;
    UINT2               u2LenOrType = 0;
    UINT4               u4ExtraBytes = 0;
    UINT1               u1EnetHeaderSize = 0;
    UINT1              *pu1PortName = NULL;
    struct ifreq        ifr;
    char                bp[CFA_MAX_DRIVER_MTU];
    struct msghdr       msg;
    struct cmsghdr     *cmsg;
    union
    {
        struct cmsghdr      cmsg;
        char                buf[CMSG_SPACE (sizeof (struct tpacket_auxdata))];
    } cmsg_buf;
    struct iovec        iov;

    MEMSET (&Enet, 0, sizeof (Enet));
    MEMSET (&ifr, 0, sizeof (ifr));

    if (u4IfIndex > SYS_DEF_MAX_INTERFACES)
    {
        return CFA_FAILURE;
    }

    pu1PortName = CfaGddGetLnxIntfnameForPort ((UINT2) u4IfIndex);

    if (pu1PortName == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddEthRead - "
                  "Not able to get Interface name for %d - FAILURE.\n",
                  u4IfIndex);
        return CFA_FAILURE;
    }
    sprintf (ifr.ifr_name, "%s", pu1PortName);

    Enet.sll_family = PF_PACKET;
    Enet.sll_ifindex = ifr.ifr_ifindex;

    pPort = (struct sockaddr *) &Enet;
    msg.msg_name = &Enet;
    msg.msg_namelen = sizeof (Enet);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    msg.msg_control = &cmsg_buf;
    msg.msg_controllen = sizeof (cmsg_buf);
    msg.msg_flags = 0;

    iov.iov_len = CFA_MAX_DRIVER_MTU;
    iov.iov_base = bp;

    /* Invoke RecvMsg to read packets from socket. Vlan header 
     * is stripped for tagged packets by kernel before passing 
     * it to userspace. Read Vlan Information from AUXDATA and 
     * reconstruct the packet header */
    if ((i4ReadBytes = recvmsg (gi4PortDesc, &msg, MSG_TRUNC)) <= 0)
    {
        return CFA_FAILURE;
    }

    for (cmsg = CMSG_FIRSTHDR (&msg); cmsg; cmsg = CMSG_NXTHDR (&msg, cmsg))
    {
        struct tpacket_auxdata *aux;

        if (cmsg->cmsg_len < CMSG_LEN (sizeof (struct tpacket_auxdata)) ||
            cmsg->cmsg_level != SOL_PACKET || cmsg->cmsg_type != PACKET_AUXDATA)
        {
            continue;
        }

        aux = (struct tpacket_auxdata *) CMSG_DATA (cmsg);

        if (aux->tp_vlan_tci == 0)
        {
            /* Packet is untagged. Copy the buffer into pu1DataBuf */
            memcpy (pu1DataBuf, bp, i4ReadBytes);
        }
        else
        {
            /* Packet is Tagged. Reconstruct VLAN Header using AUX Data */
            memcpy (pu1DataBuf, bp, 2 * ETH_ALEN);
            pu1DataBuf[12] = 0x81;
            pu1DataBuf[13] = 0x00;
            pu1DataBuf[14] = (aux->tp_vlan_tci & 0xFF00) >> 8;
            pu1DataBuf[15] = (aux->tp_vlan_tci & 0x00FF);

            memcpy (pu1DataBuf + ((2 * ETH_ALEN) + VLAN_TAG_LEN),
                    bp + (2 * ETH_ALEN), (i4ReadBytes - (2 * ETH_ALEN)));
            i4ReadBytes += VLAN_TAG_LEN;
        }
    }

    CFA_IF_SET_IN_OCTETS (u4IfIndex, (UINT4) i4ReadBytes);

    pEthHdr = (tEnetV2Header *) (VOID *) pu1DataBuf;
    u2LenOrType = OSIX_NTOHS (pEthHdr->u2LenOrType);

    if (CFA_ENET_IS_TYPE (u2LenOrType))
    {
        u2Protocol = u2LenOrType;
        u1EnetHeaderSize = CFA_ENET_V2_HEADER_SIZE;
    }
    else
    {
        pSnapEthHdr = (tEnetSnapHeader *) (VOID *) pu1DataBuf;
        u1EnetHeaderSize = CFA_ENET_SNAP_HEADER_SIZE;

        /* check for LLC control frame first - we expect only LLC in SNAP now */
        if (pSnapEthHdr->u1Control == CFA_LLC_CONTROL_UI)
        {
            /*check for presence of SNAP which may carry IP/ARP/RARP after LLC */
            if ((pSnapEthHdr->u1DstLSap == CFA_LLC_SNAP_SAP) ||
                (pSnapEthHdr->u1SrcLSap == CFA_LLC_SNAP_SAP) ||
                (pSnapEthHdr->u1Oui1 == 0x00) ||
                (pSnapEthHdr->u1Oui2 == 0x00) || (pSnapEthHdr->u1Oui3 == 0x00))
            {
                /* determine which protocol is being carried */
                u2Protocol = OSIX_NTOHS (pSnapEthHdr->u2ProtocolType);
            }                    /* end of SNAP framing */

        }                        /* end of LLC framing */

    }

    if (u2Protocol == CFA_ENET_IPV4)
    {
        MEMCPY (&i2IpTotalLen, (pu1DataBuf + u1EnetHeaderSize
                                + IP_PKT_OFF_LEN), sizeof (INT2));
        i2IpTotalLen = (INT2) OSIX_NTOHS (i2IpTotalLen);

        if ((i2IpTotalLen + u1EnetHeaderSize) > i4ReadBytes)
        {
            return CFA_FAILURE;
        }

        u4ExtraBytes =
            (UINT4) i4ReadBytes - (UINT4) (i2IpTotalLen + u1EnetHeaderSize);

    }
#ifdef IP6_WANTED
    else if (u2Protocol == CFA_ENET_IPV6)
    {
        MEMCPY (&i2IpTotalLen, (pu1DataBuf + u1EnetHeaderSize +
                                IPV6_OFF_PAYLOAD_LEN), sizeof (INT2));
        i2IpTotalLen = (INT2) OSIX_NTOHS (i2IpTotalLen);

        if ((i2IpTotalLen + u1EnetHeaderSize + IPV6_HEADER_LEN) > i4ReadBytes)
        {
            return CFA_FAILURE;
        }

        u4ExtraBytes =
            (UINT4) i4ReadBytes - ((UINT4) i2IpTotalLen +
                                   (UINT4) u1EnetHeaderSize +
                                   (UINT4) IPV6_HEADER_LEN);
    }
#endif /*IP6_WANTED */

    (*pu4PktSize) = (UINT4) i4ReadBytes - u4ExtraBytes;
#endif

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : NpCfaIsVlanTagRequest
 *
 *    Description        : This function will check, whether VLAN tag is
 *                         required for the incoming packet.
 *
 *    Input(s)           : pBuf - Pointer to the linear buffer.
 *
 *    Output(s)          : None. 
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            : CFA_TRUE or CFA_FALSE.
 *
 *****************************************************************************/
PRIVATE INT4
NpCfaIsVlanTagRequest (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tEnetV2Header      *pEthHdr = NULL;
    UINT4               u4VlanOffset = CFA_VLAN_TAG_OFFSET;
    UINT4               u4ContextId = VLAN_DEF_CONTEXT_ID;
    UINT4               u4IfIndex;
    INT4                i4RetVal;
    UINT4               u4TempIfIndex = 0;
    UINT2               u2AggId = 0;
    UINT2               u2LenOrType = 0;
    UINT2               u2VlanTag = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2Pvid = 0;
    UINT1               au1GmrpAddr[6] = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x20 };
    UINT1               u1PortAccpFrmType;
    tMacAddr            ProviderGmrpAddr;
    tMacAddr            ProviderLacpAddr;
    tMacAddr            ProviderDot1xAddr;
    tMacAddr            ProviderGvrpAddr;
    tMacAddr            ProviderStpAddr;
    BOOL1               bIsTunnelPort;
    UINT1               u1IfEtherType;

    /* CFA_GET_IFINDEX should not be used here, because in the packet
     * processing thread, Cfa Interface index is filled in
     * "ModuleData.InterfaceId" data structure.
     */
    u4IfIndex = pBuf->ModuleData.InterfaceId.u4IfIndex;
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        CfaGetEthernetType (u4IfIndex, &u1IfEtherType);
        if (u1IfEtherType == CFA_STACK_ENET)
        {
            /* If Packets received for Stack interface,
             * then the vlan tag should not be removed */
            return CFA_TRUE;
        }
    }
    /* If the port belongs to port-channel, then get the port-channel id.
     * If the port is not part of any port-channel, then u2AggId will be
     * same as the given u4IfIndex. */
    if (L2IwfGetPortChannelForPort (u4IfIndex, &u2AggId) == L2IWF_SUCCESS)
    {
        u4TempIfIndex = u2AggId;
    }
    else
    {
        u4TempIfIndex = u4IfIndex;
    }

    pEthHdr = (tEnetV2Header *) CRU_BMC_Get_DataPointer (pBuf);

    L2IwfGetPortVlanTunnelStatus (u4TempIfIndex, &bIsTunnelPort);

    if (bIsTunnelPort == OSIX_TRUE)
    {
        /* 
         * In SDK:5.2.4 bpdus and gvrp packets were received as untagged on 
         * tunnel external ports (tunnel ports). On tunnel internal ports 
         * bpdus and gvrp pkts were received 8100 tagged.
         * In SDK:5.4.3, bpdus and gvrp pkts were received tagged on both
         * tunnel internal and external ports.
         * Hence in case of tunnel external ports, if tagged packets are 
         * received with pvid vlan, then untag them.
         */
        u2LenOrType = OSIX_NTOHS (pEthHdr->u2LenOrType);

        if (u2LenOrType == VLAN_PROTOCOL_ID)
        {

            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2VlanTag,
                                       VLAN_TAG_VLANID_OFFSET, VLAN_TAG_SIZE);

            u2VlanTag = OSIX_NTOHS (u2VlanTag);

            u2VlanId = (tVlanId) (u2VlanTag & VLAN_ID_MASK);

            L2IwfGetVlanPortPvid (u4TempIfIndex, &u2Pvid);

            if (u2VlanId != u2Pvid)
            {
                return CFA_TRUE;
            }
        }
        else
        {
            return CFA_TRUE;
        }
    }

    MEMSET (ProviderGmrpAddr, 0, sizeof (tMacAddr));

    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_GMRP_PROTO_ID,
                              ProviderGmrpAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_DOT1X_PROTO_ID,
                              ProviderDot1xAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_LACP_PROTO_ID,
                              ProviderLacpAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_GVRP_PROTO_ID,
                              ProviderGvrpAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_STP_PROTO_ID,
                              ProviderStpAddr);

    if ((MEMCMP (pEthHdr->au1DstAddr, au1GmrpAddr, CFA_ENET_ADDR_LEN) == 0) ||
        (MEMCMP (pEthHdr->au1DstAddr, ProviderGvrpAddr, CFA_ENET_ADDR_LEN) == 0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderStpAddr, CFA_ENET_ADDR_LEN) ==
            0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderLacpAddr, CFA_ENET_ADDR_LEN) ==
            0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderDot1xAddr, CFA_ENET_ADDR_LEN)
            == 0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderGmrpAddr, CFA_ENET_ADDR_LEN) ==
            0))
    {
        /* GMRP packet - retain VLAN tag */
        return CFA_TRUE;
    }

    if (VlanGetTagLenInFrame (pBuf, u4IfIndex, &u4VlanOffset) == VLAN_SUCCESS)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2LenOrType,
                                   u4VlanOffset, CFA_ENET_TYPE_OR_LEN);
        u2LenOrType = OSIX_NTOHS (u2LenOrType);
    }

    VlanNpRetainTag (u4IfIndex, pEthHdr->au1DstAddr, u2LenOrType, &i4RetVal);
    if (i4RetVal == VLAN_TRUE)
    {
        return CFA_TRUE;
    }

    /* Retain tag for all the L3 protocols  and Dot1x packets as well */
    if ((u2LenOrType == CFA_ENET_IPV4) || (u2LenOrType == CFA_ENET_ARP) ||
        (u2LenOrType == CFA_ENET_RARP) || (u2LenOrType == CFA_ENET_IPV6))
    {
        /* Most hardwares add a vlan tag to the received untagged frames before sending to
           the CPU. For the port that accepts only untagged & priority tagged frame, the
           Vlan ID in the tags needs to be removed */
        L2IwfGetVlanPortAccpFrmType (u4IfIndex, &u1PortAccpFrmType);
        if (u1PortAccpFrmType ==
            VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES)
        {
            return CFA_FALSE;
        }
        return CFA_TRUE;
    }

    return CFA_FALSE;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddGetLnxIntfnameForPort
 *
 *    Description          : This function Returns LinuxEth Name Based on Slot
 *                           Referred.
 *    Input(s)            :  u4IfIndex - Interface index
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure
 *
 *    Global Variables Modified : gaIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : EthName if Slot is found Else NULL
 *
 *****************************************************************************/
UINT1              *
CfaGddGetLnxIntfnameForPort (UINT2 u2Index)
{
    INT4                i4Index;
    UINT1               au1Temp[CFA_MAX_PORT_NAME_LENGTH];

    if (u2Index == 0)
    {
        return NULL;
    }

    for (i4Index = 0; i4Index < SYS_DEF_MAX_PHYSICAL_INTERFACES; i4Index++)
    {
        MEMSET (au1Temp, 0, CFA_MAX_PORT_NAME_LENGTH);

        STRCPY (au1Temp, au1IntMapTable[i4Index]);

        if (MEMCMP (CFA_GDD_PORT_NAME (u2Index), au1Temp,
                    STRLEN (CFA_GDD_PORT_NAME (u2Index))) == 0)
        {
            return au1IntMapTable[i4Index][1];
        }
    }
    return NULL;

}

/*****************************************************************************
 *
 *    Function Name        : CfaGddGetLnxIntfnameforFcapwap
 *
 *    Description          : This function Returns LinuxEth Name Based on Slot
 *                           Referred.
 *    Input(s)            :  u4IfIndex - Interface index
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure
 *
 *    Global Variables Modified : gaIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : EthName if Slot is found Else NULL
 *
 *****************************************************************************/
UINT1              *
CfaGddGetLnxIntfnameForFcapwap (UINT2 u2Index)
{
    INT4                i4Index;
    UINT1               au1Temp[CFA_MAX_PORT_NAME_LENGTH];

    if (u2Index == 0)
    {
        return NULL;
    }
    MEMSET (au1Temp, 0, CFA_MAX_PORT_NAME_LENGTH);

    STRCPY (au1Temp, au1IntMapTable[u2Index - 1]);

    if (MEMCMP (CFA_GDD_PORT_NAME (u2Index), au1Temp,
                STRLEN (CFA_GDD_PORT_NAME (u2Index))) == 0)
    {
        return au1IntMapTable[u2Index - 1][1];
    }
    return NULL;

}

/*****************************************************************************
 *
 *    Function Name        : CfaGddSetLnxIntfnameForPort
 *
 *    Description          : This function sets Interface Name for the interface
 *                           index.
 *    Input(s)            :  u4IfIndex - Interface index
 *                           pu1IfName - Interface name
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure
 *
 *    Global Variables Modified : gaIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : NULL
 *
 *****************************************************************************/
VOID
CfaGddSetLnxIntfnameForPort (UINT4 u4Index, UINT1 *pu1IfName)
{
    if (u4Index == 0)
    {
        return;
    }

    STRNCPY (au1IntMapTable[u4Index], CFA_GDD_PORT_NAME (u4Index),
             CFA_MAX_PORT_NAME_LENGTH);

    STRNCPY (au1IntMapTable[u4Index][1], pu1IfName, CFA_MAX_PORT_NAME_LENGTH);
    CfaSetIfName (u4Index, pu1IfName);
    return;

}

/*****************************************************************************
 *
 *    Function Name       :CfaGddTxPktOnVlanMemberPortsInCxt
 *
 *    Description         :This routine transmits the given Frame on the member
 *                         ports of the given Vlan.
 *
 *    Input(s)            : u4L2ContextId - L2 Context Id to which VlanId is
 *                                          associated
 *                        : pu1DataBuf  - Pointer to the frame buffer
 *                          u4PktSize   - Size of the frame
 *                          VlanId      - Vlan on which the frame is to be
 *                                        transmitted
 *                          bBcast      - Whether this frame is to be
 *                                        broadcasted
 *
 *    Output(s)           : None
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/ CFA_FAILURE.
 *****************************************************************************/

INT4
CfaGddTxPktOnVlanMemberPortsInCxt (UINT4 u4L2ContextId,
                                   UINT1 *pu1DataBuf, tVlanId VlanId,
                                   BOOL1 bBcast, UINT4 u4PktSize)
{
    tPortList          *pTagPorts = NULL;
    tPortList          *pUnTagPorts = NULL;
    tMacAddr            DestAddr;
    tCfaVlanInfo        VlanInfo;
    UINT4               u4BridgeMode;
    UINT4               u4OutPort;
    BOOL1               bIsTag;
    INT4                i4RetVal;
    UINT1              *pu1TempBuf = NULL;
    UINT1               TempDataBuf[CFA_ENET_MIN_UNTAGGED_FRAME_SIZE];
    UINT4               u4MinFrameMtu = 0;
    INT4                i4RetStat = CFA_SUCCESS;
    UINT1               u1PaddingDone = CFA_FALSE;
    MEMCPY (DestAddr, pu1DataBuf, CFA_ENET_ADDR_LEN);

    MEMSET (TempDataBuf, 0, sizeof (TempDataBuf));
    VlanInfo.u2VlanId = VlanId;
#ifdef NPAPI_WANTED
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
#ifdef MBSM_WANTED
        /* If the packet belongs to STACK IVR, then send the
         * packet via stack port using ATP*/
        if (VlanId == CFA_DEFAULT_STACK_VLAN_ID)
        {
            if (CfaMbsmNpTxOnStackInterface (pu1DataBuf, u4PktSize) ==
                FNP_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddTxPktOnVlanMemberPorts - "
                         "Unable to tx on stack interface.\n");
                return CFA_FAILURE;
            }
            return CFA_SUCCESS;
        }
#endif
    }
#endif

    pTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        return CFA_FAILURE;
    }

    pUnTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pUnTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        return CFA_FAILURE;
    }

    MEMSET (*pTagPorts, 0, sizeof (tPortList));
    MEMSET (*pUnTagPorts, 0, sizeof (tPortList));
    i4RetVal = VlanIvrGetTxPortOrPortListInCxt (u4L2ContextId,
                                                DestAddr, VlanId,
                                                (UINT1) bBcast, &u4OutPort,
                                                &bIsTag, *pTagPorts,
                                                *pUnTagPorts);

    if (i4RetVal == VLAN_FORWARD)
    {
        /*ASSUMPTION: The Packet reached here will be untagged always.
         * Since bcoz this thread is called only for the outgoing packet
         * from IP.*/

        u4MinFrameMtu = CFA_ENET_MIN_UNTAGGED_FRAME_SIZE;

        if (u4PktSize < u4MinFrameMtu)
        {
            pu1TempBuf = pu1DataBuf;
            MEMCPY (TempDataBuf, pu1DataBuf, u4PktSize);
            pu1DataBuf = TempDataBuf;
            u4PktSize = u4MinFrameMtu;
            u1PaddingDone = CFA_TRUE;
        }

        CfaTxFillVlanInfo (DestAddr, u4OutPort,
                           bIsTag, *pTagPorts, *pUnTagPorts, &VlanInfo);
        if (L2IwfGetBridgeMode (L2IWF_DEFAULT_CONTEXT, &u4BridgeMode) ==
            L2IWF_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaGddWrite - "
                     "L2IWF-GetBridgeMode -  FAILURE.\n");
            i4RetStat = CFA_FAILURE;
        }
        else
        {
            if (CfaL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo) !=
                CFA_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddWrite - "
                         "Unsuccessful Driver Write -  FAILURE.\n");
                i4RetStat = CFA_FAILURE;

            }
        }
        if (u1PaddingDone == CFA_TRUE)
        {
            pu1DataBuf = pu1TempBuf;
        }
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pUnTagPorts));
        return i4RetStat;
    }

    FsUtilReleaseBitList ((UINT1 *) pTagPorts);
    FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pTagPorts));
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pUnTagPorts));
    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddConfigIp
 *
 *    Description        : Function is called for assigning IP to Eth Interface
 *
 *    Input(s)            : UINT4 u4IpAddr,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if configuration succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
CfaGddConfigIp (UINT4 u4IpAddr)
{
    struct ifreq        if_req;
    struct sockaddr_in *sinptr;
    INT4                i4Err = -1;

    MEMSET (&if_req, 0, sizeof (if_req));
    STRCPY (if_req.ifr_ifrn.ifrn_name, "eth0");

    if_req.ifr_ifru.ifru_addr.sa_family = AF_INET;

    sinptr = (struct sockaddr_in *) (VOID *) &if_req.ifr_addr;
    sinptr->sin_family = AF_INET;
    sinptr->sin_addr.s_addr = OSIX_HTONL (u4IpAddr);
    sinptr->sin_port = 0;

    i4Err = ioctl (gi4PortDesc, SIOCSIFADDR, &if_req);
    if (i4Err < 0)
    {
        perror ("IF ADDR Set");
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
*
*    Function Name        : CfaGddGetIfIndexforInterface
*
*    Description          : Function is called to get index for interface
*
*    Input(s)             : UINT1 *pu1InterfaceName, INT4 *pi4Ifindex
*
*    Output(s)            : None.
*
*    Global Variables Referred : None
*
*    Global Variables Modified : None.
*
*    Exceptions or Operating
*    System Error Handling     : None.
*
*    Use of Recursion          : None.
*
*    Returns            : CFA_SUCCESS if configuration succeeds,
*                         otherwise CFA_FAILURE.
*
*****************************************************************************/
UINT1
CfaGddGetIfIndexforInterface (UINT1 *pu1InterfaceName, INT4 *pi4Ifindex)
{
    INT4                i4Index = 0;
    if (STRLEN (pu1InterfaceName) == 0)
    {
        return CFA_FAILURE;
    }
    for (i4Index = 0; i4Index < SYS_DEF_MAX_PHYSICAL_INTERFACES; i4Index++)
    {
        if (STRCMP (au1IntMapTable[i4Index][1], pu1InterfaceName) == 0)
        {
            *pi4Ifindex = i4Index + 1;
        }
    }
    return CFA_SUCCESS;
}

/***********************************************************************/
/* Function Name      : CfaGddGetWlanIfName                            */
/*                                                                     */
/* Description        : This function is will return the WLAN Binding  */
/*                      interface name for the given interface index   */
/*                                                                     */
/* Input(s)           : u2WlanIfIndex - WLAN Interface index           */
/*                                                                     */
/* Output(s)          : pu1WlanIfName - Interface name                 */
/*                                                                     */
/* Global Variables                                                    */
/* Referred           : None.                                          */
/*                                                                     */
/* Global Variables                                                    */
/* Modified           : None.                                          */
/*                                                                     */
/* Return Value(s)    : None                                           */
/***********************************************************************/
UINT1
CfaGddGetWlanIfName (UINT2 u2WlanIfIndex, UINT1 *pu1WlanIfName)
{
    SPRINTF ((char *) pu1WlanIfName, "ath%d",
             (u2WlanIfIndex - CFA_MIN_WSS_BSSID));

    return OSIX_SUCCESS;
}

/****************************************************************************************
FUNCTION    : CfaGddClearInterface
DESCRIPTION : To Destroy the Dhcp Pools and the Set the Existing Interface IP's to 0.0.0.0
          The Function is written to fix the issue in MSR
RETURN      : CFA_SUCCESS
******************************************************************************************/
UINT1
CfaGddClearInterface (UINT1 u1RadioId, UINT1 u1WlanId, UINT1 *pu1InterfaceName)
{
    INT4                i4Ifindex = 0;
    INT4                i4BridgedIfStat = CFA_ENABLED;
    UINT4               u4IpAddr = 0x0;
    UINT4               u4SubnetMask = 0x0;
    INT4                i4DhcpPool = 0;
    INT4                i4NextDhcpPool = 0;
    if (CfaGddGetIfIndexforInterface (pu1InterfaceName, &i4Ifindex) ==
        CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "CfaGddGetIfIndexforInterface failed\r\n");
        return OSIX_SUCCESS;
    }
    /*To Set the ip interface to 0.0.0.0 */
    if (nmhSetIfIpAddr (i4Ifindex, u4IpAddr) == SNMP_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM, "nmhSetIfIvrBridgedIface failed\r\n");
    }
    if (nmhSetIfIpSubnetMask (i4Ifindex, u4SubnetMask) == SNMP_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM, "nmhSetIfIpSubnetMask failed\r\n");
    }
    /*Destroying Dhcp Pools */
    if (nmhGetFirstIndexDhcpSrvSubnetPoolConfigTable (&i4NextDhcpPool) ==
        SNMP_FAILURE)
    {
        return OSIX_SUCCESS;
    }
    do
    {
        i4DhcpPool = i4NextDhcpPool;
        if (nmhSetDhcpSrvSubnetPoolRowStatus (i4DhcpPool, DESTROY) ==
            SNMP_FAILURE)
        {
            continue;
        }
    }
    while (nmhGetNextIndexDhcpSrvSubnetPoolConfigTable
           (i4DhcpPool, &i4NextDhcpPool) == SNMP_SUCCESS);
    return CFA_SUCCESS;
}
