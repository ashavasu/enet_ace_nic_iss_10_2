/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: cfapktreg.c,v 1.1
 *
 * Description:This file contains the routines for protocols to
 *             register with cfa for periodic packet tx
 *             
 *
 *******************************************************************/
#include "cfainc.h"
INT1                gi1TskReg = 0;
tOsixSemId          gCfaPktTxSemId = 0;
tOsixSemId          gCfaAstPktTxSemId = 0;
tOsixTaskId         gCfaPktTaskId = 0;
tOsixTaskId         gCfaTxPktTaskId = 0;
/* Cfa protocol registration tbl for periodic pkt tx*/
tRegPeriodicTxInfo  gCfaRegPeriodicTxInfo[CFA_MAX_APPLNS];
/******************************************************************************
 * Function           : CfaInitPeriodicPktTxRegnTbl
 * Input(s)           : None. 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : Routine to initialize the CFA Periodic Pkt Tx registration table 
 *                      used to store the HL functin pointers.       
 ******************************************************************************/
INT4
CfaInitPeriodicPktTxRegnTbl (VOID)
{
    UINT2               u2RegTblIndx;

    for (u2RegTblIndx = 0; u2RegTblIndx < CFA_MAX_APPLNS; u2RegTblIndx++)
    {
        gCfaRegPeriodicTxInfo[u2RegTblIndx].SendToApplication = NULL;
    }

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaDeInitPeriodicPktTxRegnTbl
 * Input(s)           : None. 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : Routine to de-initialize the CFA Periodic Pkt Tx registration table. 
 ******************************************************************************/
INT4
CfaDeInitPeriodicPktTxRegnTbl (VOID)
{
    UINT2               u2RegTblIndx;

    for (u2RegTblIndx = 0; u2RegTblIndx < CFA_MAX_APPLNS; u2RegTblIndx++)
    {
        if (gCfaRegPeriodicTxInfo[u2RegTblIndx].SendToApplication != NULL)
        {
            gCfaRegPeriodicTxInfo[u2RegTblIndx].SendToApplication = NULL;
        }
    }

    if (gCfaPktTxSemId != 0)
    {
        OsixSemDel (gCfaPktTxSemId);
        gCfaPktTxSemId = 0;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function     : CfaPeriodicPktTxTsk                                        */
/*                                                                           */
/* Description  : This function invokes the call back function periodically  */
/*                for all the applications registered                        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
CfaPeriodicPktTxTsk (INT1 *pArg)
{
    INT4                i4RetVal = 0;
    UINT4               u4AppId = 0;
    UINT4               u4EventMask = 0;
    UINT4               u4RetValue = 0;
    UNUSED_PARAM (pArg);

    /* initialise cfa registration table for periodic packet */
    if ((i4RetVal = CfaInitPeriodicPktTxRegnTbl ()) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaInitPeriodicPktTxRegnTbl - Init Fail - FAILURE.\n");
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    if (OsixCreateSem (CFA_PKTTSK_LOCK, 1, 0, &gCfaPktTxSemId) != OSIX_SUCCESS)
    {
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    CFA_INIT_COMPLETE (OSIX_SUCCESS);

    if (OsixTskIdSelf (&CFA_PKT_TASK_ID) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                 "Exiting CfaPeriodicPktTxTsk - OsixTskIdSelf Failed\n");
        /* Indicate the status of initialization to the main routine */
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Wait for CFA_PACKET_PROCESS_START_EVENT from CFA to start sending packets */
    u4RetValue = OsixEvtRecv (CFA_PKT_TASK_ID, CFA_PACKET_START_EVENT,
                              CFA_EVENT_WAIT_FLAGS, &u4EventMask);

    /*post a self event incase some protocol has registered before */
    OsixEvtSend (CFA_PKT_TASK_ID, CFA_PROTOCOL_REG_EVENT);

    while (1)
    {
        /* Wait for atleast one protocol to register before starting periodic packet TX. */
        u4RetValue = OsixEvtRecv (CFA_PKT_TASK_ID, CFA_PROTOCOL_REG_EVENT,
                                  CFA_EVENT_WAIT_FLAGS, &u4EventMask);
        /* Start the Periodic Pkt Tx Task */
        if (u4EventMask & CFA_PROTOCOL_REG_EVENT)
        {
            /*Cross check if some protocol has registered as it can be even a self event
               to check missed Protocol registrations */

            if (gi1TskReg == 1)
            {
                while (1)
                {
                    for (u4AppId = 0; u4AppId < CFA_MAX_APPLNS; u4AppId++)
                    {
                        if (gCfaRegPeriodicTxInfo[u4AppId].SendToApplication !=
                            NULL)
                        {
                            gCfaRegPeriodicTxInfo[u4AppId].SendToApplication ();
                        }
                    }

                    /* No protocols have registered , so stop Periodic Pkt Tx Task */
                    if (gi1TskReg == 0)
                    {
                        break;
                    }
                    /*nanosleep for 10 msecs. One tick is 10 ms */
                    OsixTskDelay (1);
                }
            }
        }
    }
    UNUSED_PARAM (u4RetValue);
}

/*****************************************************************************/
/* Function Name      : CfaPktTxLock                                         */
/*                                                                           */
/* Description        : This function is used to take the CFA Pkt Tsk        */
/*                      SEMaphore to avoid simultaneous access to            */
/*                      CFA PktTx database                                   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
CfaPktTxLock (VOID)
{
    if (OsixSemTake (gCfaPktTxSemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaPktTxUnLock                                       */
/*                                                                           */
/* Description        : This function is used to give the CFA Pkt Tsk        */
/*                      SEMaphore to avoid simultaneous access to            */
/*                      CFA PktTx database                                   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS or CFA_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
CfaPktTxUnlock (VOID)
{
    OsixSemGive (gCfaPktTxSemId);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : CfaRegisterPeriodicPktTx                                   */
/*                                                                           */
/* Description  : This function used to register the applications            */
/*                with CFA module for periodic Pkt Tx.                       */
/*                CFA module invokes the call back function                  */
/*                Registered by the appl at specified interval to tx packets */
/*                                                                           */
/* Input        : u4AppId - Application Id                                   */
/*                SendToApplication - Callback function                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
CfaRegisterPeriodicPktTx (UINT4 u4AppId, tRegPeriodicTxInfo * pRegnInfo)
{
    CFA_PKTTX_LOCK ();
    gCfaRegPeriodicTxInfo[u4AppId].SendToApplication =
        pRegnInfo->SendToApplication;
    /* Set the global flag even if one protocol has registered */
    gi1TskReg = 1;
    CFA_PKTTX_UNLOCK ();

    if (CFA_PKT_TASK_ID == 0)
    {
        if (OsixGetTaskId (SELF, (const UINT1 *) CFA_PKT_TASK_NAME,
                           &CFA_PKT_TASK_ID) != OSIX_SUCCESS)
        {
            CFA_DBG (ALL_FAILURE_TRC, CFA_MAIN,
                     "Event send to CFA PKT TASK failed\n");
            return;
        }

    }

    OsixEvtSend (CFA_PKT_TASK_ID, CFA_PROTOCOL_REG_EVENT);
}

/*****************************************************************************/
/* Function     : CfaDeRegisterPeriodicPktTx                                 */
/*                                                                           */
/* Description  : This function used to deregister the applications          */
/*                with CFA module                                            */
/*                                                                           */
/* Input        : u4AppId - Application Id                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
CfaDeRegisterPeriodicPktTx (UINT4 u4AppId)
{
    INT1                i1Flag = 0;

    CFA_PKTTX_LOCK ();
    gCfaRegPeriodicTxInfo[u4AppId].SendToApplication = NULL;
    CFA_PKTTX_UNLOCK ();

    /* Update the global flag to check out if this the last protocol registered */
    for (u4AppId = 0; u4AppId < CFA_MAX_APPLNS; u4AppId++)
    {
        if (gCfaRegPeriodicTxInfo[u4AppId].SendToApplication != NULL)
        {
            i1Flag = 1;
        }
    }

    /* If there is a change , update the global flag */
    if (i1Flag == 0)
    {
        CFA_PKTTX_LOCK ();
        gi1TskReg = 0;
        CFA_PKTTX_UNLOCK ();

    }
}

/*****************************************************************************/
/* Function     : CfaPacketTxTask                                         */
/*                                                                           */
/* Description  : This function used to transmit STP packets to NP           */
/*                                                                           */
/* Input        : None                                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
CfaPacketTxTask (INT1 *pArg)
{
    tCRU_BUF_CHAIN_HEADER *pBuf;
    UINT1              *pu1DataBuf = NULL;
#ifdef NPAPI_WANTED
    UINT4               u4IfIndex = 0;
#endif
    UINT4               u4PktSize = 0;
    UINT4               u4Event = 0;
    UINT4               u4MinFrameMtu = CFA_ENET_MIN_UNTAGGED_FRAME_SIZE;
    UINT4               u4Loop = 0;
    UINT4               u4RetValue = 0;

    UNUSED_PARAM (pArg);
    if (OsixCreateSem (CFA_AST_PKTTSK_LOCK, 1, 0, &gCfaAstPktTxSemId) !=
        OSIX_SUCCESS)
    {
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (OsixGetTaskId (SELF, (const UINT1 *) CFA_AST_PKTTX_TASK_NAME,
                       &CFA_AST_PKTTX_TASK_ID) != OSIX_SUCCESS)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_MAIN,
                 "Unable to get Task Id for AstPktTx\n");
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (OsixQueCrt ((UINT1 *) CFA_AST_PACKET_QUEUE, OSIX_MAX_Q_MSG_LEN,
                    CFA_PACKET_QUEUE_DEPTH,
                    &(CFA_AST_PACKET_QID)) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaPeriodicPktTxTsk - Creation Fail -"
                 "FAILURE.\n");
        /* Indicate the status of initialization to the main routine */
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    CFA_INIT_COMPLETE (OSIX_SUCCESS);
    while (1)
    {
        u4RetValue =
            OsixEvtRecv (CFA_AST_PKTTX_TASK_ID, CFA_AST_BPDU_EVENT,
                         (UINT4) OSIX_WAIT, &u4Event);
        {
            if (u4Event & CFA_AST_BPDU_EVENT)
            {
                while (OsixQueRecv (CFA_AST_PACKET_QID, (UINT1 *) &pBuf,
                                    OSIX_DEF_MSG_LEN,
                                    OSIX_NO_WAIT) == OSIX_SUCCESS)
                {
                    CFA_AST_PKT_LOCK ();
                    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
                    if ((pu1DataBuf =
                         CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                                       u4PktSize)) == NULL)
                    {
                        CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                                 "Error in CfaEnetTxLoopbackTestFrame - "
                                 "CRU Buf pointer not linear -  FAILURE.\n");
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        return;
                    }
                    if (u4PktSize < u4MinFrameMtu)
                    {
                        for (u4Loop = u4PktSize; u4Loop < u4MinFrameMtu;
                             u4Loop++)
                        {
                            pu1DataBuf[u4Loop] = 0;
                        }
                        u4PktSize = u4MinFrameMtu;
                    }
#ifdef NPAPI_WANTED
                    u4IfIndex = CFA_GET_IFINDEX (pBuf);
                    CfaGddEthWrite (pu1DataBuf, u4IfIndex, u4PktSize);
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
#else
                    UNUSED_PARAM (u4PktSize);
#endif
                    CFA_AST_PKT_UNLOCK ();

                }
            }
        }
    }
    UNUSED_PARAM (u4RetValue);
}

/********************************************************************************/
/* Function Name      : CfaPacketTxLock                                         */
/*                                                                             */
/* Description        : This function is used to take the CFA AST Pkt Tsk       */
/*                      SEMaphore to avoid simultaneous access to              */
/*                      CFA AST PktTx database                                  */
/*                                                                             */
/* Input(s)           : None                                                   */
/*                                                                             */
/* Output(s)          : None                                                   */
/*                                                                             */
/* Global Variables                                                            */
/* Referred           : None                                                   */
/*                                                                             */
/* Global Variables                                                            */
/* Modified           : None                                                   */
/*                                                                             */
/* Return Value(s)    : CFA_SUCCESS or CFA_FAILURE                             */
/*                                                                             */
/********************************************************************************/
INT4
CfaPacketTxLock (VOID)
{
    if (OsixSemTake (gCfaAstPktTxSemId) != OSIX_SUCCESS)
    {
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

/********************************************************************************/
/* Function Name      : CfaPacketTxUnLock                                       */
/*                                                                             */
/* Description        : This function is used to give the CFA AST Pkt Tsk       */
/*                      SEMaphore to avoid simultaneous access to              */
/*                      CFA PktTx database                                     */
/*                                                                             */
/* Input(s)           : None                                                   */
/*                                                                             */
/* Output(s)          : None                                                   */
/*                                                                             */
/* Global Variables                                                            */
/* Referred           : None                                                   */
/*                                                                             */
/* Global Variables                                                            */
/* Modified           : None                                                   */
/*                                                                             */
/* Return Value(s)    : CFA_SUCCESS or CFA_FAILURE                             */
/*                                                                             */
/********************************************************************************/
INT4
CfaPacketTxUnLock (VOID)
{
    OsixSemGive (gCfaAstPktTxSemId);
    return CFA_SUCCESS;
}
