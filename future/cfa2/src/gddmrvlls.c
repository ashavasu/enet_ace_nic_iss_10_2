/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddmrvlls.c,v 1.13 2016/07/23 11:41:25 siva Exp $
 *
 * Description:This file contains the routines for the     
 *             Gemeric Device Driver Module of the CFA.    
 *             These routines are called at various places     
 *             in the CFA modules for interfacing with the    
 *             device drivers in the system. These routines   
 *             have to be modified when moving onto different  
 *             drivers. 
 *               Drivers currently supported are                  
 *                  SOCK_PACKET for Ethernet                      
 *                  WANIC HDLC driver                             
 *                  SANGOMA wanpipe driver                        
 *                  ETINC driver                                  
 *                  ATMVC's support                               
 *                              
 *******************************************************************/

#include "cfainc.h"
#include "cfaport.h"
#include "gddmrvlls.h"
#include "issnvram.h"

#ifdef LNXIP4_WANTED
/************************** PRIVATE DECLARATIONS ******************************/

PRIVATE INT4 CfaGddKernBridgeInit PROTO ((VOID));
PRIVATE VOID CfaGddKernBridgeDeInit PROTO ((VOID));
PRIVATE INT4 CfaGddKernBridgeUpdate PROTO ((UINT1 u1Command));
PRIVATE INT4 CfaGddKernBridgeUpdatePortInfo PROTO ((UINT4 u4PortId,
                                                    UINT1 u1Command));
PRIVATE INT4 CfaGddKernGetIfIndexFromPort PROTO ((UINT4 u4PortId,
                                                  INT4 *pi4IfIndex));

/************************** GLOBALS DECLARATIONS ******************************/

INT4                gi4BrSock;
#endif
/*****************************************************************************
 *
 *    Function Name        : CfaGddInit
 *
 *    Description        : This function performs the initialisation of
 *                the Generic Device Driver Module of CFA. This
 *                initialization routine should be called
 *                from the init of IFM after we have obtained
 *                the number of physical ports after parsing of
 *                the Config file. This function initializes the FD list
 *                and ifIndex array of the polling table.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : FdTable
 *
 *    Global Variables Modified : FdTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddInit (VOID)
{
    tMacAddr            au1SwitchMac;

    CfaGetSysMacAddress (au1SwitchMac);

    CfaNpUpdateSwitchMac (au1SwitchMac);

    if (FsHwNpInitDataBuffers () != FNP_SUCCESS)
    {
        return CFA_FAILURE;
    }

#ifdef LNXIP4_WANTED
    if (CfaGddKernBridgeInit () != CFA_SUCCESS)
    {
        return CFA_FAILURE;
    }
#endif

    if (CfaNpUpdatePortMacAddr () != FNP_SUCCESS)
    {
        return CFA_FAILURE;
    }

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddShutdown
 *
 *    Description        : This function performs the shutdown of
 *                the Generic Device Driver Module of CFA. This
 *                routine frees the polling table structure.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : FdTable.
 *
 *    Global Variables Modified : FdTable.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
VOID
CfaGddShutdown (VOID)
{
#ifdef LNXIP4_WANTED
    CfaGddKernBridgeDeInit ();
#endif
    return;
}

/**************************************************************************/
INT4
CfaGddSetGddType (UINT4 u4IfIndex)
{
    UINT1               u1IfType;

    CfaGetIfType (u4IfIndex, &u1IfType);
    CFA_GDD_TYPE (u4IfIndex) = u1IfType;

    return (CFA_SUCCESS);
}

/**************************************************************************/
VOID
CfaGddOsRemoveIfFromList (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return;
}

/**************************************************************************/
VOID
CfaGddOsAddIfToList (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return;
}

PUBLIC INT4
CfaGddProcessRecvInterruptEvent (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pBuf;
    tEnetV2Header      *pEthHdr;
    tVlanTag            VlanTag;
    UINT4               u4IfIndex;
    UINT4               u4PktSize;
    UINT4               u4TagOffSet;
    UINT4               u4ContextId;
    UINT2               u2LocalPort;
    UINT1               au1Buf[CFA_ENET_V2_HEADER_SIZE];
    UINT1               u1IfType;
    UINT1               u1IngressAction;

    while (OsixQueRecv (CFA_PACKET_MUX_QID, (UINT1 *) &pBuf,
                        OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        CFA_LOCK ();
        u4IfIndex = pBuf->ModuleData.InterfaceId.u4IfIndex;

        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
        CfaGetIfType ((UINT4) u4IfIndex, &u1IfType);

        MEMSET (au1Buf, 0, CFA_ENET_V2_HEADER_SIZE);
        CRU_BUF_Copy_FromBufChain (pBuf, au1Buf, 0, CFA_VLAN_TAG_OFFSET);
        pEthHdr = (tEnetV2Header *) au1Buf;

        if (CfaCheckIfLocalMac (pEthHdr, (UINT2) u4IfIndex) == CFA_SUCCESS)
        {
            VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPort);
            VlanGetVlanInfoFromFrame (u4ContextId, u2LocalPort, pBuf, &VlanTag,
                                      &u1IngressAction, &u4TagOffSet);
            if (AclCheckDenyRule (VlanTag.OuterVlanTag.u2VlanId,
                                  pEthHdr->au1SrcAddr, SRC_MAC_BASED_ACL)
                == ISS_DROP)
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
            }
            else if ((CFA_GDD_HL_RX_FNPTR (u4IfIndex))
                     (pBuf, u4IfIndex, u4PktSize, u1IfType,
                      CFA_ENCAP_NONE) != CFA_SUCCESS)
            {
                /* release buffer which were not successfully sent to higher layer
                 */
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
            }
        }
        else if ((CFA_GDD_HL_RX_FNPTR (u4IfIndex))
                 (pBuf, u4IfIndex, u4PktSize, u1IfType,
                  CFA_ENCAP_NONE) != CFA_SUCCESS)
        {
            /* release buffer which were not successfully sent to higher layer
             */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CFA_IF_SET_IN_DISCARD (u4IfIndex);
        }

        CFA_UNLOCK ();
    }
    return (CFA_SUCCESS);
}

/**************************************************************************/
INT4
CfaGddOsProcessRecvEvent (VOID)
{
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddConfigPort
 *
 *    Description        : Function is called for configuration of the
 *                Enet ports. No such support is available for
 *                Wanic at present. Only the Promiscuous mode
 *                and multicast addresses can be configured at
 *                present for Enet ports. This is called
 *                directly whenever the Manager configures the
 *                ifTable for Ethernet ports. It can also be
 *                called indirectly by Bridge or RIP, etc.
 *                through the CfaIfmEnetConfigMcastAddr API.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *                UINT1 u1ConfigOption,
 *                VOID *pConfigParam.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaPhysIfTable (GDD Reg Table) structure.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if configuration succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption, VOID *pConfigParam)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1ConfigOption);
    UNUSED_PARAM (pConfigParam);
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddGetHwAddr
 *
 *    Description        : Function is called for getting the hardware
 *                address of the Enet ports. This function can
 *                be called only after the opening of the ports
 *                - the ports should be open but Admin/Oper
 *                Status can be down.
 *
 *    Input(s)            : UINT4 u4IfIndex.
 *
 *    Output(s)            : UINT1 *au1HwAddr.
 *
 *    Global Variables Referred : gIfGlobal (Interface's global struct),
 *                gaPhysIfTable (GDD Reg Table) structure.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if address is obtained,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddGetOsHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{

    if (CfaIsL3IpVlanInterface (u4IfIndex) == CFA_SUCCESS)
    {
        CfaGetIfHwAddr (u4IfIndex, au1HwAddr);
        return (CFA_SUCCESS);
    }

    /* Get the port mac address from NP for each port
     * at port creation & store it in CFA CDB table.
     * Here onwards protocol should refer CDB table
     * for port mac retrieval.
     */
    CfaNpGetHwAddr (u4IfIndex, au1HwAddr);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthOpen
 *
 *    Description        : This function opens the Ethernet port and stores the
                           descriptor in the interface table. Since the port is 
                           opened already during the registration time, this 
                           function is dummy.
 *
 *    Input(s)            : u4IfIndex - Interface Index
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable (Interface table)
 *                                structure
 *
 *    Global Variables Modified :  gapIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if open succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthOpen (UINT4 u4IfIndex)
{

    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthClose
 *
 *    Description        : First disables the promiscuous mode of 
 *                         the port, if enabled. The multicast address
 *                         list is lost when the port is closed.i
 *                         We dont need to check if the call is a 
 *                         success or not.
 *
 *    Input(s)            : u4IfIndex - Interface index.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable
 *
 *    Global Variables Modified : gapIfTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if close succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthClose (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthWrite
 *
 *    Description        : Writes the data to the ethernet driver.
 *
 *    Input(s)            : pu1DataBuf - Pointer to the linear buffer.
 *                          u4IfIndex - MIB-2 interface index
 *                          u4PktSize - Size of the buffer.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable (Interface table)
 *                                structure,
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if write succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    if (FNP_SUCCESS != CfaHwL2VlanIntfWrite (pu1DataBuf, u4IfIndex, u4PktSize))
    {
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *    Function Name             : CfaGddEthWriteWithPri
 *    Description               : Writes the data to the ethernet driver.
 *    Input(s)                  : pu1DataBuf - Pointer to the linear buffer.
 *                                u4IfIndex  - MIB-2 interface index
 *                                u4PktSize  - Size of the buffer.
 *                                u1Priority - Traffic class on which Pkt to
 *					        be TXed
 *    Output(s)                 : None.
 *    Global Variables Referred : None
 *    Global Variables Modified : None.
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : CFA_SUCCESS if write succeeds,
 *                                otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGddEthWriteWithPri (UINT1 *pData, UINT4 u4IfIndex, UINT4 u4PktLen, 
		       UINT1 u1Priority)
{
    if(CfaGddEthWrite(pData,u4IfIndex,u4PktLen) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }
    UNUSED_PARAM (u1Priority);
    return CFA_SUCCESS;
}
/*****************************************************************************
 *
 *    Function Name        : CfaGddEthRead
 *
 *    Description        : This function reads the data from the ethernet port.
 *
 *    Input(s)            : pu1DataBuf - Pointer to the linear buffer.
 *                          u4IfIndex - IfIndex of the interface
 *                          pu4PktSize - Pointer to the packet size
 *
 *    Output(s)            : pu1DataBuf, pu1PktSize
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4PktSize);
    return (CFA_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      :  CfaGddGetLinkStatus                             */
/*                                                                          */
/*    Description        : Gets the Physical Link Status of the device      */
/*    Input(s)           : Logical Port ,Vlan and the Data Buffer           */
/*                                                                          */
/*    Output(s)          : Link Status for the specific IfIndex             */
/*                                                                          */
/****************************************************************************/

UINT1
CfaGddGetLinkStatus (UINT4 u4IfIndex)
{
    UINT1               u1IfType;
    UINT1               u1OperStatus;

    CfaGetIfType (u4IfIndex, &u1IfType);
    if (u1IfType == CFA_ENET)
    {
        return CfaCfaNpGetLinkStatus (u4IfIndex);
    }
    else
    {
        CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
        return (u1OperStatus);
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : CfaGddSendIntrEvent                              */
/*                                                                          */
/*    Description        :  Sends an event to CFA to indicate packet arrival*/
/*                          at the driver                                   */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/****************************************************************************/

UINT4
CfaGddSendIntrEvent (VOID)
{
    if (CFA_INITIALISED != TRUE)
    {
        return (CFA_FAILURE);
    }
    OsixEvtSend (CFA_TASK_ID, CFA_GDD_INTERRUPT_EVENT);
    return (CFA_SUCCESS);
}

#ifdef LNXIP4_WANTED
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CfaGddKernBridgeInit
 *                                                                          
 *    DESCRIPTION      : Function to Create Bridge Device and associate the 
 *                       physical interfaces present in the System 
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : CFA_FAILURE on Socket Creation failure or Bridge
 *                       Device creation failure else returns CFA_SUCCESS
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
CfaGddKernBridgeInit (VOID)
{
    UINT4               u4PortNo = 0;

    gi4BrSock = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_ALL));

    if (gi4BrSock < 0)
    {
        perror (" CfaGddKernBridgeInit : Socket Creation Failure");
        return (CFA_FAILURE);
    }

    if ((CfaGddKernBridgeUpdate (CFA_GDD_ADD_BRIDGE)) == CFA_FAILURE)
    {
        return (CFA_FAILURE);
    }

    for (u4PortNo = CFA_GDD_MIN_PHY_PORTS; u4PortNo <= CFA_GDD_MAX_PHY_PORTS;
         u4PortNo++)
    {
        CfaGddKernBridgeUpdatePortInfo (u4PortNo, CFA_GDD_ADD_BRIDGE_IF);
    }

    return (CFA_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CfaGddKernBridgeDeInit
 *                                                                          
 *    DESCRIPTION      : Function to Delete the Bridge Device
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
CfaGddKernBridgeDeInit (VOID)
{
    CfaGddKernBridgeUpdate (CFA_GDD_DEL_BRIDGE);
    close (gi4BrSock);
    gi4BrSock = -1;

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CfaGddKernBridgeUpdate
 *                                                                          
 *    DESCRIPTION      : Function to Add/Delete a Bridge Interface to the System
 *                       to which the physical interfaces would be associated
 *                       later in order to receive packets arriving in those 
 *                       interfaces
 *
 *    INPUT            : u1Command - Add or Delete Bridge Device
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : CFA_FAILURE on Bridge device addition failure while
 *                       calling the ioctl () or CFA_SUCCESS on succesful
 *                       addition or deletion
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
CfaGddKernBridgeUpdate (UINT1 u1Command)
{
    struct ifreq        if_req;
    UINT1               au1DeviceName[CFA_GDD_DEV_NAME_SIZE];
    INT4                i4RetVal = CFA_SUCCESS;

    MEMSET (&if_req, 0, sizeof (if_req));
    MEMSET (au1DeviceName, 0, CFA_GDD_DEV_NAME_SIZE);

    if_req.ifr_addr.sa_family = AF_INET;

    SPRINTF ((CHR1 *) & au1DeviceName, "%s", CFA_GDD_BRIDGE_INTF_NAME);
    STRCPY (if_req.ifr_name, au1DeviceName);

    /* Create/Delete a Bridge Interface "bridge" using ioctl ()'s to which 
     * physical interfaces will be added for receiving packets arriving in 
     * those interfaces */

    switch (u1Command)
    {
        case CFA_GDD_ADD_BRIDGE:

            if (ioctl (gi4BrSock, SIOCBRADDBR, &au1DeviceName) < 0)
            {
                perror ("SIOCBRADDBR Failed");
                i4RetVal = CFA_FAILURE;
            }
            else
            {
                if_req.ifr_flags = IFF_UP | IFF_RUNNING;
                /* Make the Administrative Status of Interface to UP */
                if (ioctl (gi4BrSock, SIOCSIFFLAGS, &if_req) < 0)
                {
                    perror ("IF FLAGS Set");
                    i4RetVal = CFA_FAILURE;
                }
            }
            break;

        case CFA_GDD_DEL_BRIDGE:

            if_req.ifr_flags = IFF_RUNNING;
            /* Make the Administrative Status of Interface to UP */
            if (ioctl (gi4BrSock, SIOCSIFFLAGS, &if_req) < 0)
            {
                perror ("IF FLAGS Set");
                i4RetVal = CFA_FAILURE;
            }
            else
            {
                if (ioctl (gi4BrSock, SIOCBRDELBR, &au1DeviceName) < 0)
                {
                    perror ("SIOCBRADDBR Failed");
                    i4RetVal = CFA_FAILURE;
                }
            }
            break;

        default:
            i4RetVal = CFA_FAILURE;
            break;
    }

    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CfaGddKernBridgeUpdatePortInfo
 *                                                                          
 *    DESCRIPTION      : Function to update the Physical Port's with the
 *                       Bridge Device
 *
 *    INPUT            : u4PortNo   - Physical Port Number 
 *                       u1Command  - Add or Delete a Port from a Bridge Device
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : CFA_FAILURE on Port Association failure or 
 *                       retrieval of Port Number to IF Index failure
 *                       returns CFA_SUCCESS on successful updation
 *                       (Port add/delete)
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
CfaGddKernBridgeUpdatePortInfo (UINT4 u4PortId, UINT1 u1Command)
{
    struct ifreq        if_req;
    UINT1               au1DeviceName[CFA_GDD_DEV_NAME_SIZE];
    INT4                i4RetVal = CFA_SUCCESS;

    MEMSET (&if_req, 0, sizeof (if_req));
    MEMSET (au1DeviceName, 0, CFA_GDD_DEV_NAME_SIZE);

    SPRINTF ((CHR1 *) & au1DeviceName, "%s", CFA_GDD_BRIDGE_INTF_NAME);

    MEMCPY (&if_req.ifr_name, au1DeviceName, CFA_GDD_DEV_NAME_SIZE);

    /* Add Physical Interfaces present in the system to the Bridge Interface
     * (Logical Device) created earlier so as to receive the packets using 
     * NetFilter Bridge Hook */

    if (CfaGddKernGetIfIndexFromPort (u4PortId,
                                      &if_req.ifr_ifindex) == CFA_FAILURE)
    {
        i4RetVal = CFA_FAILURE;
    }
    else
    {
        switch (u1Command)
        {
            case CFA_GDD_ADD_BRIDGE_IF:

                if (ioctl (gi4BrSock, SIOCBRADDIF, &if_req) < 0)
                {
                    i4RetVal = CFA_FAILURE;
                }
                break;

            case CFA_GDD_DEL_BRIDGE_IF:

                ioctl (gi4BrSock, SIOCBRDELIF, &if_req);
                break;

            default:
                /* Invalid Option */
                i4RetVal = CFA_FAILURE;
                break;
        }
    }

    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CfaGddKernGetIfIndexFromPort
 *                                                                          
 *    DESCRIPTION      : Function to retrieve the IF Index from the physical
 *                       Port number
 *
 *    INPUT            : u4PortId - inter to the INode Structure 
 *                                                                          
 *    OUTPUT           : pi4IfIndex - Physical Port's IF Index (IP Port Number)
 *                                                                          
 *    RETURNS          : CFA_FAILURE if ioctl () fails else returns
 *                       CFA_SUCCESS
 *
 ****************************************************************************/
PRIVATE INT4
CfaGddKernGetIfIndexFromPort (UINT4 u4PortId, INT4 *pi4IfIndex)
{
    struct ifreq        if_req;
    UINT1               au1DeviceName[CFA_GDD_DEV_NAME_SIZE];

    MEMSET (&if_req, 0, sizeof (if_req));
    MEMSET (au1DeviceName, 0, CFA_GDD_DEV_NAME_SIZE);

    SNPRINTF ((CHR1 *) & au1DeviceName, CFA_GDD_DEV_NAME_SIZE,
              "%s%d", CFA_GDD_ETHERNET_PREFIX, u4PortId);

    STRCPY (if_req.ifr_ifrn.ifrn_name, au1DeviceName);

    if (ioctl (gi4BrSock, SIOCGIFINDEX, &if_req) < 0)
    {
        return CFA_FAILURE;
    }
    else
    {
        *pi4IfIndex = if_req.ifr_ifindex;
        return CFA_SUCCESS;
    }
}

#endif /* LNXIP4_WANTED */

/*****************************************************************************
 *
 *    Function Name       :CfaGddTxPktOnVlanMemberPortsInCxt 
 *
 *    Description         :This routine transmits the given Frame on the member
 *                         ports of the given Vlan.
 *
 *    Input(s)            : u4L2ContextId - L2 Context Id to which VlanId is 
 *                                          associated
 *                        : pu1DataBuf  - Pointer to the frame buffer 
 *                          u4PktSize   - Size of the frame 
 *                          VlanId      - Vlan on which the frame is to be 
 *                                        transmitted
 *                          bBcast      - Whether this frame is to be 
 *                                        broadcasted  
 *
 *    Output(s)           : None
 *
 *    Global Variables Referred : None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/ CFA_FAILURE.
 *****************************************************************************/

INT4
CfaGddTxPktOnVlanMemberPortsInCxt (UINT4 u4L2ContextId,
                                   UINT1 *pu1DataBuf, tVlanId VlanId,
                                   BOOL1 bBcast, UINT4 u4PktSize)
{
    tCfaVlanInfo        VlanInfo;
    UINT4               u4BridgeMode;
    UINT1              *pu1TempBuf = NULL;
    UINT1               TempDataBuf[CFA_ENET_MIN_UNTAGGED_FRAME_SIZE];
    UINT4               u4MinFrameMtu = 0;
    INT4                i4RetStat = CFA_SUCCESS;
    UINT1               u1PaddingDone = CFA_FALSE;
    UNUSED_PARAM (u4L2ContextId);
    UNUSED_PARAM (bBcast);

    MEMSET (TempDataBuf, 0, sizeof (TempDataBuf));
    VlanInfo.u2VlanId = VlanId;
    VlanInfo.unPortInfo.TxUcastPort.u2TxPort = VLAN_INVALID_PORT;
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
#ifdef MBSM_WANTED
        /* If the packet belongs to STACK IVR, then send the
         * packet via stack port using ATP*/
        if (VlanId == CFA_DEFAULT_STACK_VLAN_ID)
        {
            if (CfaMbsmNpTxOnStackInterface (pu1DataBuf, u4PktSize) ==
                FNP_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddTxPktOnVlanMemberPortsInCxt - "
                         "Unable to tx on stack interface.\n");
                return CFA_FAILURE;
            }
            return CFA_SUCCESS;
        }
#endif
    }

    /*ASSUMPTION: The Packet reached here will be untagged always.
     * Since bcoz this thread is called only for the outgoing packet
     * from IP.*/

    u4MinFrameMtu = CFA_ENET_MIN_UNTAGGED_FRAME_SIZE;

    if (u4PktSize < u4MinFrameMtu)
    {
        pu1TempBuf = pu1DataBuf;
        MEMCPY (TempDataBuf, pu1DataBuf, u4PktSize);
        pu1DataBuf = TempDataBuf;
        u4PktSize = u4MinFrameMtu;
        u1PaddingDone = CFA_TRUE;
    }

    if (L2IwfGetBridgeMode (L2IWF_DEFAULT_CONTEXT, &u4BridgeMode) ==
        L2IWF_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaGddWrite - " "L2IWF-GetBridgeMode -  FAILURE.\n");
        i4RetStat = CFA_FAILURE;
    }
    else
    {
        if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
        {
            if ((u4BridgeMode == L2IWF_CUSTOMER_BRIDGE_MODE) ||
                (u4BridgeMode == L2IWF_PROVIDER_BRIDGE_MODE) ||
                (u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE) ||
                (u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE))
            {
                if (CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo)
                    != FNP_SUCCESS)
                {
                    CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                             "Error in CfaGddWrite - "
                             "Unsuccessful Driver Write -  FAILURE.\n");
                    i4RetStat = CFA_FAILURE;
                }
            }
        }
    }
    if (u1PaddingDone == CFA_TRUE)
    {
        pu1DataBuf = pu1TempBuf;
    }
    return i4RetStat;
}
