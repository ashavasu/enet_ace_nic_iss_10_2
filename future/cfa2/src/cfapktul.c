/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: cfapktul.c,v 1.1 
 *
 * Description:This file contains utility routines common to 
 *             Packet Analyser and Packet Transmitter. This is used for
 *             testing purpose only.
 *
 *******************************************************************/
#include "cfainc.h"

tCfaPktRxInfo      *gpCfaPktRxInfoList = NULL;
tCfaPktTxInfo      *gpCfaPktTxInfoList = NULL;
tTimerListId        CfaPktgenTmrListId;
tOsixQId            gCfaPktgenQId;
UINT1               gu1CfaPktgenQStatus = CFA_DISABLED;

/*****************************************************************************
 *
 *    Function Name      : CfaPktGenInit
 *
 *    Description        : This function performs the initialisation of
 *                         the CFA PktGen. It creates the mempool and the queue
 *                         for transfering data. And this function creates the
 *                         Timer for sending the packet after a certain
 *                         interval.
 *
 *    Input(s)           : None.
 *
 *    Output(s)          : None.
 *
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                            otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaPktGenInit ()
{
    gpCfaPktRxInfoList = MemAllocMemBlk (CFA_PKT_GEN_RX_POOL_ID);

    if (gpCfaPktRxInfoList == NULL)
    {
        return CFA_FAILURE;
    }

    gpCfaPktTxInfoList = MemAllocMemBlk (CFA_PKT_GEN_TX_POOL_ID);

    if (gpCfaPktTxInfoList == NULL)
    {
        return CFA_FAILURE;
    }

    MEMSET (gpCfaPktRxInfoList, 0,
            FsCFASizingParams[MAX_CFA_PKTGEN_RX_INFO_SIZING_ID].u4StructSize);
    MEMSET (gpCfaPktTxInfoList, 0,
            FsCFASizingParams[MAX_CFA_PKTGEN_TX_INFO_SIZING_ID].u4StructSize);

    if (OsixQueCrt ((UINT1 *) CFA_PKTGEN_QUEUE, OSIX_MAX_Q_MSG_LEN,
                    CFA_PKTGEN_Q_DEPTH, &CFA_PKT_GEN_TX_QID) != OSIX_SUCCESS)
    {
        return CFA_FAILURE;
    }
    gu1CfaPktgenQStatus = CFA_ENABLED;

    if (TmrCreateTimerList ((const UINT1 *) CFA_TASK_NAME,
                            CFA_PKT_GEN_TMR_EVENT,
                            NULL, &CfaPktgenTmrListId) != TMR_SUCCESS)
    {
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaPktGenShutdown
 *
 *    Description        : This function performs the shutdown of
 *                         the CFA PktGen. It deletes the mempool and the queue 
 *                         and the timer list.
 *
 *    Input(s)           : None.
 *
 *    Output(s)          : None.
 *
 *
 *    Returns            : CFA_SUCCESS 
 *
 *****************************************************************************/
INT4
CfaPktGenShutdown ()
{
    tCRU_BUF_CHAIN_HEADER *pTransmitPkt = NULL;
    UINT4               u4PktSendRemTime = 0;
    UINT4               u4Index = 0;
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;

    if (gu1CfaPktgenQStatus == CFA_ENABLED)
    {
        while (OsixQueRecv (CFA_PKT_GEN_TX_QID,
                            (UINT1 *) &pTransmitPkt,
                            OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pTransmitPkt, FALSE);
        }
    }
    if (CFA_PKT_GEN_TX_QID != 0)
    {
        OsixQueDel (CFA_PKT_GEN_TX_QID);
        CFA_PKT_GEN_TX_QID = 0;
    }
    if (CfaPktgenTmrListId != 0)
    {
        for (u4Index = 0; u4Index < CFA_MAX_PKT_GEN_ENTRIES; u4Index++)
        {
            pCfaPktTxInfo = &gpCfaPktTxInfoList[u4Index];
            TmrGetRemainingTime (CfaPktgenTmrListId,
                                 &(pCfaPktTxInfo->PktgenTmrBlk.TimerNode),
                                 &u4PktSendRemTime);
            if (u4PktSendRemTime != 0)
            {
                TmrStopTimer (CfaPktgenTmrListId,
                              &(pCfaPktTxInfo->PktgenTmrBlk.TimerNode));
            }
        }
        TmrDeleteTimerList (CfaPktgenTmrListId);
        CfaPktgenTmrListId = 0;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaPktGenTxPkt
 *
 *    Description        : This function starts the timer and sends a packet
 *                         when the u4Action is Send. It will stop the timer 
 *                         when the u4Action is Stop.
 *
 *    Input(s)           : pu1TransmitInfo.
 *
 *    Output(s)          : None.
 *
 *
 *    Returns            : None.
 *
 *****************************************************************************/
VOID
CfaPktGenTxPkt (UINT1 *pu1TransmitInfo)
{
    tPktgenQueInfo     *pPktgenQueInfo = NULL;
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;
    UINT4               u4Index = 0;
    UINT4               u4RetValue = 0;

    UNUSED_PARAM (u4RetValue);

    pPktgenQueInfo = (tPktgenQueInfo *) (VOID *) pu1TransmitInfo;

    u4Index = pPktgenQueInfo->u4Index;
    pCfaPktTxInfo = &gpCfaPktTxInfoList[u4Index];

    if (pPktgenQueInfo->u4Action == CFA_PKTGEN_START)
    {
        CfaPktgenPacketSend (pCfaPktTxInfo);
        u4RetValue =
            TmrStart (CfaPktgenTmrListId, &(pCfaPktTxInfo->PktgenTmrBlk),
                      (UINT1) u4Index, pCfaPktTxInfo->u4Interval, 0);
    }
    else
    {
        if (pCfaPktTxInfo->u4RowStatus == 0)
        {
            return;
        }
        TmrStop (CfaPktgenTmrListId, &(pCfaPktTxInfo->PktgenTmrBlk));
        MEMSET (pCfaPktTxInfo, 0, sizeof (tCfaPktTxInfo));
    }
}

/*****************************************************************************
 *
 *    Function Name      : CfaPktGenTmrExpHdlr
 *
 *    Description        : This function is called when the timer expires. 
 *                         When the count is zero, the pacekt is send again 
 *                         and the timer is restarted. When the count is 1,
 *                         the timer is stopepd. Otherwise, the count is
 *                         decremented by one and the timer is restarted after
 *                         sending a packet.
 *
 *    Input(s)           : None.
 *
 *    Output(s)          : None.
 *
 *
 *    Returns            : None.
 *
 *****************************************************************************/

VOID
CfaPktGenTmrExpHdlr (VOID)
{
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;
    UINT4               u4Index = 0;
    UINT4               u4RetValue = 0;

    UNUSED_PARAM (u4RetValue);

    while ((pCfaPktTxInfo = (tCfaPktTxInfo *) TmrGetNextExpiredTimer
            (CfaPktgenTmrListId)) != NULL)
    {
        if (pCfaPktTxInfo->u4RowStatus == 0)
        {
            return;
        }

        if (pCfaPktTxInfo->u4PktCount == 1)
        {
            TmrStop (CfaPktgenTmrListId, &(pCfaPktTxInfo->PktgenTmrBlk));
            MEMSET (pCfaPktTxInfo, 0, sizeof (tCfaPktTxInfo));
            return;
        }
        if (pCfaPktTxInfo->u4PktCount != 0)
        {
            pCfaPktTxInfo->u4PktCount--;
        }

        u4Index = pCfaPktTxInfo->u4Index;
        CfaPktgenPacketSend (pCfaPktTxInfo);
        u4RetValue =
            TmrStart (CfaPktgenTmrListId, &(pCfaPktTxInfo->PktgenTmrBlk),
                      (UINT1) u4Index, pCfaPktTxInfo->u4Interval, 0);
    }

}

/*****************************************************************************
 *
 *    Function Name      : CfaPktgenPacketSend
 *
 *    Description        : This function copies the data from the tCfaPktTxInfo
 *                         and and checks for the ports. It copies the value
 *                         into a CRU_BUF and calls CfaGddWrite to send the 
 *                         packet.
 *
 *    Input(s)           : pCfaPktTxInfo.
 *
 *    Output(s)          : None.
 *
 *
 *    Returns            : None.
 *
 *****************************************************************************/

VOID
CfaPktgenPacketSend (tCfaPktTxInfo * pCfaPktTxInfo)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tCfaIfInfo          IfInfo;
    UINT4               u4Index = 0;
    UINT4               u4PktLen = 0;
    UINT2               bResult = 0;
    UINT1               u1VlanTagCheck = CFA_TRUE;

    u4PktLen = pCfaPktTxInfo->u4Length;

    for (u4Index = 0; u4Index < BRG_PORT_LIST_SIZE * 8; u4Index++)
    {
        OSIX_BITLIST_IS_BIT_SET (pCfaPktTxInfo->au1Ports, u4Index,
                                 BRG_PORT_LIST_SIZE, bResult);

        if (bResult == OSIX_FALSE)
        {
            continue;
        }

        pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktLen, 0);

        if (pBuf == NULL)
        {
            return;
        }

        CRU_BUF_Copy_OverBufChain (pBuf, pCfaPktTxInfo->au1PktGenVal, 0,
                                   u4PktLen);

        CfaGetIfInfo (u4Index, &IfInfo);

        if (CfaGddWrite (pBuf, u4Index, u4PktLen, u1VlanTagCheck,
                         &IfInfo) != CFA_SUCCESS)
        {
            return;
        }
    }
}

/*****************************************************************************
 *
 *    Function Name      : CfaPktUtlAnalysePkt
 *
 *    Description        : This function checks the received packet for the 
 *                         given pattern & given mask. When the pattern matches
 *                         it increments the count by for that index, and 
 *                         stores the time the packet received.
 *
 *    Input(s)           : pBuf - CRU_BUF Data
 *                         u4IfIndex - The index on which the packet arrives
 *                         u4PktSize - The size of the packet.
 *
 *    Output(s)          : None.
 *
 *
 *    Returns            : CFA_SUCCESS / CFA_FAILURE.
 *
 *****************************************************************************/
VOID
CfaPktUtlAnalysePkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                     UINT4 u4PktSize)
{
    tCfaPktRxInfo      *pCfaPktRxInfo = NULL;
    tOsixSysTime        SysTime;
    UINT1              *pu1CharBuf = NULL;
    UINT1              *pu1PktRxMask = NULL;
    UINT1              *pu1PktRxValue = NULL;
    UINT2               u2PatternFound = 0;
    UINT2               u2Result = 0;
    UINT4               u4Index = 0;
    UINT4               u4Number = 0;
    UINT4               u4ByteNumber = 0;

    for (u4Index = 0; u4Index < CFA_MAX_PKT_GEN_ENTRIES; u4Index++)
    {
        u2PatternFound = 0;
        pCfaPktRxInfo = &gpCfaPktRxInfoList[u4Index];
        if (pCfaPktRxInfo->u4RowStatus != ACTIVE)
        {
            continue;
        }

        pu1CharBuf = CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0, u4PktSize);

        if (pu1CharBuf == NULL)
        {
            return;
        }

        pu1PktRxValue = pCfaPktRxInfo->au1Val;
        pu1PktRxMask = pCfaPktRxInfo->au1Mask;

        u4Number = 0;
        for (u4ByteNumber = 0; u4ByteNumber < u4PktSize; u4ByteNumber++)
        {
            if ((pu1CharBuf[u4ByteNumber] & pu1PktRxMask[u4Number])
                != (pu1PktRxValue[u4Number] & pu1PktRxMask[u4Number]))
            {
                u4Number = 0;
                continue;
            }

            u4Number++;
            if (u4Number == pCfaPktRxInfo->u4Len)
            {
                u2PatternFound = 1;
                break;
            }
        }
        if (u2PatternFound == 1)
        {
            OSIX_BITLIST_SET_BIT (pCfaPktRxInfo->au1MatchPorts, u4IfIndex,
                                  BRG_PORT_LIST_SIZE);

            OSIX_BITLIST_IS_BIT_SET (pCfaPktRxInfo->au1RcvPorts, u4IfIndex,
                                     BRG_PORT_LIST_SIZE, u2Result);
            if (u2Result == OSIX_TRUE)
            {
                pCfaPktRxInfo->u4Cnt = pCfaPktRxInfo->u4Cnt + 1;
                OsixGetSysTime (&SysTime);
                pCfaPktRxInfo->u4LastPktRcvTime = (UINT4) SysTime;
            }
        }
    }

    return;

}
