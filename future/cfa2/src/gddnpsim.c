/*
 *  $Id: gddnpsim.c,v 1.81 2016/07/23 11:41:25 siva Exp $
 */

#include "ms.h"
#include "cfainc.h"
#include <sys/select.h>

#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

#ifdef TCPDUMP_WANTED
#include "cfacli.h"
typedef unsigned char u_char;
typedef unsigned short u_short;
typedef unsigned int u_int;
#include <pcap.h>
#endif

#include "fsvlan.h"

#include "rmgr.h"
#include "hb.h"

#include "gddnpsim.h"

#ifdef NPSIM_ASYNC_WANTED
#include "asnpsim.h"
#endif /* NPSIM_ASYNC_WANTED */

#ifdef ECFM_WANTED
#include "ecfm.h"
#endif
#include "cnnp.h"

#ifdef BFD_WANTED
#include "bfd.h"
#include "bfdnp.h"
#endif
#ifdef PTP_WANTED
#include "ptp.h"
#include "ptpnp.h"
#endif
#include "nputil.h"
#include "icch.h"
extern VOID         DummyTrace (char *, ...);
#ifdef SYSLOG_WANTED
#ifdef RM_WANTED
extern INT1         SendLogMsgToSyslog (tCRU_BUF_CHAIN_HEADER *, UINT4);
#endif
#endif

#if defined PIM_NP_HELLO_WANTED 
extern tOsixTaskId         gu4PimHelloTaskId;
extern tOsixQId            gu4PimHelloQId;
#define  PIM_NP_HELLO_PKT_ARRIVAL_EVENT   (0x100000)
#define  PIMV6_NP_HELLO_PKT_ARRIVAL_EVENT   (0x200000)
#define  PIMSM_IP_NEXTHOP_MTU  9216 
#define  PIM_PROTOID		103
#define  PIM_HELLO_MSG		32
#define  PIMV4_DEST_IP1		224
#define  PIMV4_DEST_IP2		13
#define  PIMV6_DEST_IP1		255
#define  PIMV6_DEST_IP2		2
#define  PIMV6_DEST_IP3		13
#define  PIMV4_DEST_IP1_POS	16
#define  PIMV4_DEST_IP2_POS	19
#define  PIMV4_PROTOID_POS	9
#define  PIMV4_HELLO_MSG_POS	20
#define  PIMV6_DEST_IP1_POS	24
#define  PIMV6_DEST_IP2_POS	25
#define  PIMV6_DEST_IP3_POS	39
#define  PIMV6_PROTOID_POS	6
#define  PIMV6_HELLO_MSG_POS	40
#endif

#define NSL_MAX_PORTS   SYS_DEF_MAX_PHYSICAL_INTERFACES    /* max number of ports
                                                           for switch */
tMemPoolId          gPktBufPoolId = 0;

#define NSL_ADDR_LEN    6        /* we support only ethernet MAC addresses */
#define NSL_DOWN        0        /* flag to indicate port state            */
#define NSL_UP          1        /* flag to indicate port state            */

#define DUMMY_VARIABLE(x) ((x)=(x))
#define TTRACE1           DummyTrace    /* printf */
#define CRTTRACE1           DummyTrace    /* printf */

UINT1              *CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
VOID               CfaGddSetLnxIntfnameForPort (UINT4 u4Index, UINT1 * pu1IfName);
/* Array containing the Mapping between Slot and Eth Interfaces */
static UINT1        au1IntMapTable[SYS_DEF_MAX_PHYSICAL_INTERFACES][2][CFA_MAX_PORT_NAME_LENGTH] =
    { {"Slot0/1", "eth0"},
{"Slot0/2", "eth1"},
{"Slot0/3", "eth2"},
{"Slot0/4", "eth3"},
{"Slot0/5", "eth4"},
{"Slot0/6", "eth5"},
{"Slot0/7", "eth6"},
{"Slot0/8", "eth7"},
{"Slot0/9", "eth8"},
{"Slot0/10", "eth9"},
{"Slot0/11", "eth10"},
{"Slot0/12", "eth11"},
{"Slot0/13", "eth12"},
{"Slot0/14", "eth13"},
{"Slot0/15", "eth14"},
{"Slot0/16", "eth15"},
{"Slot0/17", "eth16"},
{"Slot0/18", "eth17"},
{"Slot0/19", "eth18"},
{"Slot0/20", "eth19"},
{"Slot0/21", "eth20"},
{"Slot0/22", "eth21"},
{"Slot0/23", "eth22"},
{"Slot0/24", "eth23"}
};

typedef struct _NslPort
{
    UINT4               u4IfIndex;
    CHR1                PortName[20];    /* Assume no more than 999 ports per switch */
    UINT1               u1PortState;    /* connected to network or not connected   */
    UINT1               au1Rsvd[3];    /* to align on 4-byte word boundary         */
}
tNslPort;
tNslPort            gaNslPort[NSL_MAX_PORTS];

static INT4         NslReadConf (CHR1 * pConfFile);
static INT4         isSwitch (CHR1 * pLine);
static INT4         isMac (CHR1 * pLine);
static INT4         isPktSrv (CHR1 * pLine);
static VOID         NslHandlePktSrvCtrlMsg (UINT1 *pData);
static VOID         NslBufDump (UINT1 *pBuf, UINT1 *pTitle, INT4 u4Len);
static VOID         NpCfaVlanTagFrame (UINT1 *pBuf, tVlanId VlanId,
                                       UINT1 u1Priority);
INT4                NslGetPortFromIfIndex (UINT4 u4IfIndex);
INT4                gNslSock;    /* Socket for all packets                   */
                                  /* Initialize with invalid value            */
INT4                gRpcSock;
INT4                gMaxFd;        /* Largest FD value, used in select call  */
INT4                gi4SingleMacAddr = -1;    /* 0: the whole switch has one MAC */
                                  /* otherwise: each port has a MAC of its    */
                                  /* own                                      */
UINT1               gau1HwAddr[NSL_MAX_PORTS][NSL_ADDR_LEN];
                                  /* Switch ethernet address. For simulation  */
                                  /* Each switch has one and only one MAC     */
                                  /* Address used by all ports of the switch  */
UINT2               gu2NslSw = 0;    /* Switch number for simulation. Every   */
                                  /* switch is assigned a unique number. The  */
                                  /* number is carried in packets between     */
                                  /* switch and packet server.                */

UINT2               gu2NslSlot = 0;
UINT2               gu2NslBaseNpSimPort = 9000;
UINT2               gu2NslBaseUdpPort = 8000;
UINT2               gu2NslBaseIssPort = 7000;
UINT2               gu2NslBaseRpcPort = 6000;
UINT4               gu4SelfIpAddr;
UINT2               gu2NslUdpPort = 0;    /* UDP port no. for server connection.  */
                                  /* This is derived from the switch number   */
                                  /* and base port number. Switch 1 uses base */
                                  /* port number + 1, switch 2 uses base port */
                                  /* number + 2, .... and so on.              */
                                  /* The server uses the base port itself.    */

UINT4               gu4NslPktServerIpAddr = 0;    /* IP address for server messages          */
UINT4               gu4NpSimProgNo;

/* Buffer to send frames - global to avoid allocating for each packet. */
UINT1               gau1TxBuf[CFA_MAX_DRIVER_MTU + CFA_MAX_FRAME_HEADER_SIZE];
INT4                CfaProcessPacket (tCRU_BUF_CHAIN_HEADER *, UINT4, UINT4);

/* defines for debugging and traces at physical level */
UINT4               gu4NslDebug = 3;
#ifdef TCPDUMP_WANTED
UINT4               gu4DoTcpDump = 0;
tCfaTcpDumpParams   TDArgs;
#endif
extern int          eflag, xflag, Xflag;
#define NSL_DEBUG_CTRL_MSGS        (0x00000001)
#define NSL_DEBUG_PKT_EVENT        (0x00000002)
#define NSL_DEBUG_PKT_DUMP         (0x00000004)

#define IS_NSL_DEBUG_CTRL_MSGS(x)  ((x) & NSL_DEBUG_CTRL_MSGS)
#define IS_NSL_DEBUG_PKT_EVENT(x)  ((x) & NSL_DEBUG_PKT_EVENT)
#define IS_NSL_DEBUG_PKT_DUMP(x)   ((x) & NSL_DEBUG_PKT_DUMP)

#ifdef CFA_INTERRUPT_MODE
tOsixTaskId         gu4NpSimPktHdlrTaskId;
tTmrBlk             gNpSimPktHdlrTmrBlk;
tTimerListId        gNpSimPktHdlrTimerLst;
#define CFA_PKT_HDLR_TIMER_EVENT        0x00000100
UINT1               gau1PktBuffer[CFA_MAX_DRIVER_MTU +
                                  CFA_MAX_FRAME_HEADER_SIZE];
VOID                CfaPktHdlrTask (VOID);
VOID                CfaNpSimPktHdlrTask (VOID);
VOID                CfaNpSimPktHdlrUtilTmrRestart (tTimerListId TimerLst,
                                                   tTmrBlk * pTmrBlk,
                                                   UINT4 u4Time);
INT4                CfaNpSimProcessPkt (VOID);
INT4                CfaNpSimProcessActiveIPCMsg
PROTO ((UINT1 *pu1Buf, tHwInfo * pHwInfo, INT4 i4Offset));
INT4                CfaNpSimProcessStandbyIPCMsg
PROTO ((UINT1 *pu1Pkt, tHwInfo * pHwInfo, INT4 i4Offset));
INT4 CfaNpSimProcessTxOnStandby PROTO ((tHwInfo * pHwInfo));
#endif
extern INT4         ICCHProcessRxMessage (tHwInfo * pHwInfo);
extern INT4         FsCfaHwSendIPCMsg (tHwInfo * pHwInfo);
extern INT4         MbsmIsConnectingPort (UINT4 u4IfIndex);
extern INT4         MbsmGetSlotIdFromRemotePort (UINT4 u4RemotePort,
                                                 UINT4 *pu4SlotId);
extern INT4         MbsmGetConnectingPortFromSlotId (UINT4 u4SlotId,
                                                     UINT4 *pu4ConnectingPort);

static int
is_blankline (char *line)
{
    while (*line && isspace (*line))
        line++;
    if (*line == 0)
        return 1;

    return 0;
}
static int
is_comment (char *line)
{
    while (isspace (*line))
        line++;
    if (*line == '#')
        return 1;
    return 0;
}
static INT4
isSwitch (CHR1 * pLine)
{
    return ((PTR_TO_I4 (strstr (pLine, "switch"))));
}
static INT4
isSlot (CHR1 * pLine)
{
    return ((PTR_TO_I4 (strstr (pLine, "slot"))));
}

static INT4
isMac (CHR1 * pLine)
{
    return ((PTR_TO_I4 (strstr (pLine, "mac"))));
}

static INT4
isInterfaceMac (CHR1 * pLine)
{
    return ((PTR_TO_I4 (strstr (pLine, "mac port"))));
}

static INT4
isPktSrv (CHR1 * pLine)
{
    return ((PTR_TO_I4 (strstr (pLine, "packet server ip"))));
}

static INT4
NslReadConf (CHR1 * pConfFile)
{
    FILE               *fp;
    UINT4               au4HwAddr[NSL_ADDR_LEN], u4Tmp;
    CHR1                au1PktServerIp[20];
    CHR1                au1Line[80];

    if ((fp = FOPEN (pConfFile, "r")) == NULL)
    {
        printf ("ERROR: Cannot open config file %s for reading\n", pConfFile);
        return (CFA_FAILURE);
    }

    MEMSET (gau1HwAddr, 0, NSL_ADDR_LEN * NSL_MAX_PORTS);
    gu4NslPktServerIpAddr = 0;
    gu2NslSw = 0;

    MEMSET (au1Line, '\0', 80);
    while (fgets (au1Line, 80, fp))
    {
        if (is_blankline (au1Line))
        {
            continue;
        }
        if (is_comment (au1Line))
        {
            continue;
        }

        if (isSwitch (au1Line))
        {
            sscanf (au1Line, "switch %d\n", (int *) &u4Tmp);
            gu2NslSw = (UINT2) u4Tmp;
            continue;
        }

        if (isSlot (au1Line))
        {
            sscanf (au1Line, "slot %d\n", (int *) &u4Tmp);
            gu2NslSlot = (UINT2) u4Tmp;
            continue;
        }

        if (isInterfaceMac (au1Line))
        {
            INT4                portId;

            if (gi4SingleMacAddr == 0)
            {
                /* if we reach here it means that the switch MAC has
                 * already been specified. We cannot have both the per port
                 * MAC configuration and MAC Switch configuration appearing
                 * together.
                 */
                printf ("NSL INIT: Cannot specify per port MAC when "
                        "switch MAC is already configured\n");
                return (CFA_FAILURE);
            }
            sscanf (au1Line, "mac port %d %2x:%2x:%2x:%2x:%2x:%2x",
                    (int *) &portId,
                    (unsigned int *) &au4HwAddr[0],
                    (unsigned int *) &au4HwAddr[1],
                    (unsigned int *) &au4HwAddr[2],
                    (unsigned int *) &au4HwAddr[3],
                    (unsigned int *) &au4HwAddr[4],
                    (unsigned int *) &au4HwAddr[5]);

            if ((portId < 1) || (portId > NSL_MAX_PORTS))
            {
                printf ("NSL INIT: Port ID must be between 1 and %d\n",
                        NSL_MAX_PORTS);
                return (CFA_FAILURE);
            }

            portId--;

            gau1HwAddr[portId][0] = (UINT1) au4HwAddr[0];
            gau1HwAddr[portId][1] = (UINT1) au4HwAddr[1];
            gau1HwAddr[portId][2] = (UINT1) au4HwAddr[2];
            gau1HwAddr[portId][3] = (UINT1) au4HwAddr[3];
            gau1HwAddr[portId][4] = (UINT1) au4HwAddr[4];
            gau1HwAddr[portId][5] = (UINT1) au4HwAddr[5];

            if (gi4SingleMacAddr == -1)
            {
                gi4SingleMacAddr = 1;
            }
            else
            {
                gi4SingleMacAddr++;
            }
            continue;
        }

        if (isMac (au1Line))
        {
            if ((gi4SingleMacAddr != 0) && (gi4SingleMacAddr != -1))
            {
                /* if we reach here it means that the mac addresses for
                 * ports have been specified on a per port basis. We cannot
                 * have both the per port MAC configuration and MAC Switch
                 * configuration appearing together.
                 */
                printf ("NSL INIT: Cannot specify switch MAC, when per "
                        "port MAC is already configured\n");
                return (CFA_FAILURE);
            }
            sscanf (au1Line, "mac %2x:%2x:%2x:%2x:%2x:%2x",
                    (unsigned int *) &au4HwAddr[0],
                    (unsigned int *) &au4HwAddr[1],
                    (unsigned int *) &au4HwAddr[2],
                    (unsigned int *) &au4HwAddr[3],
                    (unsigned int *) &au4HwAddr[4],
                    (unsigned int *) &au4HwAddr[5]);
            gau1HwAddr[0][0] = (UINT1) au4HwAddr[0];
            gau1HwAddr[0][1] = (UINT1) au4HwAddr[1];
            gau1HwAddr[0][2] = (UINT1) au4HwAddr[2];
            gau1HwAddr[0][3] = (UINT1) au4HwAddr[3];
            gau1HwAddr[0][4] = (UINT1) au4HwAddr[4];
            gau1HwAddr[0][5] = (UINT1) au4HwAddr[5];

            gi4SingleMacAddr = 0;    /* only single MAC for the whole switch */

            continue;
        }

        if (isPktSrv (au1Line))
        {
            sscanf (au1Line, "packet server ip %s\n", au1PktServerIp);
            gu4NslPktServerIpAddr = (UINT4) inet_addr (au1PktServerIp);
            continue;
        }
    }
    fclose (fp);

    if ((gu2NslSw == 0) || (gu4NslPktServerIpAddr == 0))
    {
        printf ("ERROR: %s file does not specify either switch number or "
                "packet server IP address\n", pConfFile);
        return (CFA_FAILURE);
    }

    return (CFA_SUCCESS);
}

static CHR1         ConfigFile[] = "nsl.conf";

/*
 * NslOpenNpSimSocket:
 *  Open a UDP port to send and receive pkts from NPSIM.
 */
static INT4
NslOpenNpSimSocket (void)
{
    struct sockaddr_in  NslSockAddr;

    if ((gNslSock = socket (AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror ("ISS<->NPSIM Error in opening port\n");
        return (CFA_FAILURE);
    }

    NslSockAddr.sin_family = AF_INET;
    NslSockAddr.sin_port = htons ((UINT2) (gu2NslBaseIssPort +
                                           gu2NslSw * 100 + gu2NslSlot));

    /* TODO */
    gu4SelfIpAddr = gu4NslPktServerIpAddr;
    MEMCPY ((char *) &NslSockAddr.sin_addr.s_addr, (char *) &gu4SelfIpAddr, 4);
    if (bind (gNslSock, (struct sockaddr *) &NslSockAddr,
              sizeof (NslSockAddr)) < 0)
    {
        perror ("ISS<->NPSIM UDP socket bind failure");
        return (CFA_FAILURE);
    }

    /* set options for non-blocking mode */
    if (fcntl (gNslSock, F_SETFL, O_NONBLOCK) < 0)
    {
        perror ("Failure in setting socket nonblocking option\n");
        close (gNslSock);
        return (CFA_FAILURE);
    }
    return (CFA_SUCCESS);
}

#include "npsimiss.h"

static void
NslDoRpc (void)
{
    struct sockaddr_in  FromAddr;
    INT4                i4AddrLen;
    INT2                i2PktLen;
    struct Np2IssNotif  NotifyMsg;
#ifdef PTP_WANTED
    tPtpHwPtpCbParams   PtpHwPtpCbParams;
#endif
    MEMSET (&FromAddr, 0, sizeof (FromAddr));
    i4AddrLen = sizeof (FromAddr);

    if ((i2PktLen = recvfrom (gRpcSock, (VOID *) &NotifyMsg, sizeof (NotifyMsg),
                              0, (struct sockaddr *) &FromAddr,
                              (socklen_t *) & i4AddrLen)) <= 0)
    {
        return;
    }

    /*
       printf ("Inside NslDoRpc msgtype %d............\n", NotifyMsg.u2MsgType);
       fflush (stdout);
       printf ("Msgsize is %d\n", sizeof(NotifyMsg));
     */
    switch (NotifyMsg.u4MsgType)
    {
        case NPSIM_FWD_TBL_UPDATE:

            /*
               printf ("FWD tbl update msg, action = %d\n",
               NotifyMsg.u.MacInfo.Action);
             */
            if (NotifyMsg.u.MacInfo.Action == NPSIM_FDB_ADD)
            {
#ifdef SW_LEARNING
                VlanFdbTableAddEx (NotifyMsg.u.MacInfo.u4SrcPort,
                                   NotifyMsg.u.MacInfo.MacAddr,
                                   NotifyMsg.u.MacInfo.VlanId,
                                   NotifyMsg.u.MacInfo.Type,
                                   NotifyMsg.u.MacInfo.ConnectionId);
#endif
                /*
                   memcpy (mac, NotifyMsg.u.MacInfo.MacAddr, 6);
                   printf ("Adding mac %2x:%2x:%2x:%2x:%2x:%2x\n",
                   mac[0],mac[1],mac[2],
                   mac[3],mac[4],mac[5]
                   );
                 */
            }
            else
            {
#ifdef SW_LEARNING
                VlanRemoveFdbEntry (L2IWF_DEFAULT_CONTEXT,
                                    NotifyMsg.u.MacInfo.VlanId,
                                    NotifyMsg.u.MacInfo.MacAddr);
#endif
                /*
                   memcpy (mac, NotifyMsg.u.MacInfo.MacAddr, 6);
                   printf ("removing mac %2x:%2x:%2x:%2x:%2x:%2x\n",
                   mac[0],mac[1],mac[2],
                   mac[3],mac[4],mac[5]
                   );
                 */
            }
            break;

        case NPSIM_ARP_L2_MOVEMENT:

            /*Trigger the L2 movement indication to ARP Module  */

            ArpEventSend (ARP_L2_MOVEMENT_EVENT);
            break;

#ifdef MBSM_WANTED
        case NPSIM_CARD_ATTACH:
        case NPSIM_PORT_ATTACH:

            NpSimInsertNotify (NotifyMsg.u.CardInfo);
            break;

        case NPSIM_CARD_DETACH:
        case NPSIM_PORT_DETACH:

            NpSimRemoveNotify (NotifyMsg.u.CardInfo);
            break;
#endif /* ISS_FIX */
#ifdef CN_WANTED
        case NPSIM_CNM_GENERATED:

            FsMiCnHwCallBkFunc (NotifyMsg.u.CnmInfo.au1CpId,
                                NotifyMsg.u.CnmInfo.i4CnmQOffset,
                                NotifyMsg.u.CnmInfo.i4CnmQDelta);
            break;
#endif /* CN_WANTED */
#ifdef PTP_WANTED
        case NPSIM_PTP_SYNC_PKT_SENT:
            if (PtpApiIsTwoStepClock (NotifyMsg.u.PtpPktLog.u4ContextId,
                                      NotifyMsg.u.PtpPktLog.u4PortNo,
                                      NotifyMsg.u.PtpPktLog.u1DomainId)
                == OSIX_TRUE)
            {
                PtpHwPtpCbParams.u4ContextId =
                    NotifyMsg.u.PtpPktLog.u4ContextId;
                PtpHwPtpCbParams.u4IfIndex = NotifyMsg.u.PtpPktLog.u4PortNo;
                PtpHwPtpCbParams.u1DomainId = NotifyMsg.u.PtpPktLog.u1DomainId;
                PtpHwPtpCbParams.u1MsgType = NotifyMsg.u.PtpPktLog.u1MsgType;
                MEMCPY (&PtpHwPtpCbParams.FsClkTimeVal,
                        &NotifyMsg.u.PtpPktLog.FsTimeVal,
                        sizeof (tFsClkTimeVal));

                if (FsNpPtpHwTimeStampCallBack (&PtpHwPtpCbParams) ==
                    FNP_FAILURE)
                {
                    return;
                }
            }
            break;
#endif

    }

    return;
}

static INT4
NslOpenRpcSocket (void)
{
    struct sockaddr_in  SockAddr;

    if ((gRpcSock = socket (AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror ("ISS RPC, Error in opening port\n");
        return (CFA_FAILURE);
    }

    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = htons ((UINT2) (gu2NslBaseRpcPort +
                                        gu2NslSw * 100 + gu2NslSlot));

    gu4SelfIpAddr = gu4NslPktServerIpAddr;
    MEMCPY ((char *) &SockAddr.sin_addr.s_addr, (char *) &gu4SelfIpAddr, 4);
    if (bind (gRpcSock, (struct sockaddr *) &SockAddr, sizeof (SockAddr)) < 0)
    {
        perror ("ISS<->NPSIM UDP socket bind failure");
        return (CFA_FAILURE);
    }

    /* set options for non-blocking mode */
    if (fcntl (gRpcSock, F_SETFL, O_NONBLOCK) < 0)
    {
        perror ("Failure in setting socket nonblocking option\n");
        close (gRpcSock);
        return (CFA_FAILURE);
    }
    return (CFA_SUCCESS);
}

INT4
CfaGddInit (VOID)
{
    UINT4               i;
#ifdef MBSM_WANTED
    INT4                i4Slot = 0;
    INT4                i4Port = 0;
    INT4                i4PortsPerSlot = 0;
    INT4                i4SlaveConnPorts = 0;
    INT4                i4MasterConnPorts = 0;
#endif
    UINT4               u4MaxFrontPanelPorts;
    UINT4               u4StackingModel = ISS_STACKING_MODEL_NONE;

    if (NslReadConf (ConfigFile) != CFA_SUCCESS)
    {
        return (CFA_FAILURE);
    }

    gu4NpSimProgNo = 0x31000000 + gu2NslSw;

    if (MemCreateMemPool ((CFA_MAX_DRIVER_MTU + CFA_MAX_FRAME_HEADER_SIZE),
                          (SYS_DEF_MAX_PHYSICAL_INTERFACES * 2),
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(gPktBufPoolId)) == MEM_FAILURE)
    {
        return CFA_FAILURE;
    }

#ifdef CFA_INTERRUPT_MODE
/* Create a task for handling the packets received from NPSIM and do the 
 * initial processing before posting the packet to the CFA Queue
 * This task is needed in case of dual data plane redundancy model
 * This is needed for the following purpose.
 * - If this is node is in the standby state, then the packet received should not 
 *   be processed by this node instead the packet should place in the IPC message
 *   and sent across to the active unit.
 * - For IPC message processing.
 *   Only if the packet is intended for CFA then the packet will be posted to 
 *   CFA Queue. 
 */
    if (OsixTskCrt ((UINT1 *) "tNpPkt", (10 | OSIX_SCHED_RR),
                    OSIX_DEFAULT_STACK_SIZE, (OsixTskEntry) CfaPktHdlrTask, 0,
                    &gu4NpSimPktHdlrTaskId) != OSIX_SUCCESS)
    {
        return CFA_FAILURE;
    }
#endif
    /* Create NPAPI Task. 
     * When a NPAPI is invoked by a protocol, this task will receive a 
     * message containing NPAPI Call and its parameters. This Task will
     * invoke NPSIM Clnt Call. Once the NPAPI Call is completed, this 
     * task will post the Operational status and return value to the
     * protocol. */
#ifdef NPSIM_ASYNC_WANTED
    if (AsyncNPInit () == CFA_FAILURE)
    {
        return (CFA_FAILURE);
    }
#endif /* NPSIM_ASYNC_WANTED */

    /* Get SelfIpAddr */
    /*
     * TODO remove cc error.
     gethostname(hostname, 100);
     h = gethostbyname(hostname);
     gu4SelfIpAddr = (UINT4) *(unsigned long *)(h->h_addr_list[0]);
     */

    /* Open a UDP port to send/recv. pkts from NPSIM */
    if (NslOpenNpSimSocket ())
    {
        return (CFA_FAILURE);
    }

    if (NslOpenRpcSocket ())
    {
        return (CFA_FAILURE);
    }
#ifdef FIREWALL_WANTED
/*   Currently commented the character device socket code
 
   if( NpSimOpenSecuritySocket())
    {
        return (CFA_FAILURE);
    } 
 
*/
#endif /* FIREWALL_WANTED */

    /* Open the UDP port for the message server        */
    /* When using MS, makes RPC call NpSimOpenMSSocket */
    /* The rpc call triggers npsim to connect to MS    */
    /* In STANDALONE mode, do nothing.                 */
#ifdef USING_MS
    NpSimOpenMSSocket (gu2NslSw, gu2NslSlot,
                       (gu2NslBaseUdpPort + gu2NslSw), gu4NslPktServerIpAddr);
#endif

    for (i = 0; i < NSL_MAX_PORTS; i++)
    {
        u4StackingModel = ISS_GET_STACKING_MODEL ();
        if ((u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL) ||
            (u4StackingModel == ISS_DISS_STACKING_MODEL))
        {
            gaNslPort[i].u1PortState = NSL_UP;
            gaNslPort[i].u4IfIndex = i + 1;
#ifdef MBSM_WANTED
            /* Max Ports per slot includes the physical ports in the slot 
             * and the inter connecting stacking port 
             */
            if (u4StackingModel == ISS_DISS_STACKING_MODEL)
            {
                i4MasterConnPorts = (MBSM_MAX_SLOTS - 1);
                if (i4Slot == 0)
                {
                    i4PortsPerSlot =
                        ((MBSM_MAX_PORTS_PER_SLOT * (i4Slot + 1)) +
                         i4MasterConnPorts + i4SlaveConnPorts);
                }
                if ((i + 1) >
                    (UINT4) ((MBSM_MAX_PORTS_PER_SLOT * (i4Slot + 1)) +
                             i4MasterConnPorts + i4SlaveConnPorts))
                {
                    i4Slot++;
                    i4PortsPerSlot =
                        ((MBSM_MAX_PORTS_PER_SLOT * i4Slot) +
                         i4MasterConnPorts + i4SlaveConnPorts);
                    i4SlaveConnPorts++;
                }
                i4Port = (i % i4PortsPerSlot) + 1;
            }
            else
            {
                i4PortsPerSlot = (MBSM_MAX_PORTS_PER_SLOT + 1);
                i4Slot = (i / i4PortsPerSlot);
                i4Port = (i % i4PortsPerSlot) + 1;
            }
            SPRINTF ((CHR1 *) (gaNslPort[i].PortName), "Slot%d/%d", i4Slot,
                     i4Port);
#else
            SPRINTF ((CHR1 *) (gaNslPort[i].PortName), "Slot0/%d", (int) i + 1);
#endif
        }
        else
        {
            gaNslPort[i].u4IfIndex = -1;
            gaNslPort[i].u1PortState = NSL_DOWN;
#ifndef MBSM_WANTED
            SPRINTF ((CHR1 *) (gaNslPort[i].PortName), "Slot0/%d", (int) i + 1);
#else
            /* In case of MBSM, IfIndex name will be based on the slot 
             * id and the port identifier.
             * Slot 2/1 <==> IfIndex = 1;
             * Slot 4/1 <==> IfIndex = 97
             * */
            i4Slot = ((i / MBSM_MAX_PORTS_PER_SLOT) + 2);
            i4Port = (i % MBSM_MAX_PORTS_PER_SLOT) + 1;
            SPRINTF ((CHR1 *) (gaNslPort[i].PortName), "Slot%d/%d", i4Slot,
                     i4Port);
#endif
        }
    }

    if (CfaNpInit () != FNP_SUCCESS)
    {
        PRINTF ("ERROR[NP] - CfaNpInit returned failure\n\r");
        return (CFA_FAILURE);
    }

#ifdef ISS_WANTED
    u4MaxFrontPanelPorts = IssGetFrontPanelPortCountFromNvRam ();
#endif

#ifdef NPAPI_WANTED
#ifdef MBSM_WANTED
    /*In case of MBSM, Dynamic Fron Panel Ports in invalid */
    CfaNpCfaFrontPanelPorts (SYS_DEF_MAX_PHYSICAL_INTERFACES);
#else
    CfaNpCfaFrontPanelPorts (u4MaxFrontPanelPorts);
#endif
#endif
    return (CFA_SUCCESS);
}

static VOID
NslHandlePktSrvCtrlMsg (UINT1 *pData)
{
    struct ctrlMsg     *pCtrlmsg = (struct ctrlMsg *) (VOID *) pData;

    /* control messages from packet server */
#define PKTSRV_CTRL_SET_DEBUG   (10)

    switch (pCtrlmsg->code)
    {
        case PKTSRV_CTRL_SET_DEBUG:
            MEMCPY (&gu4NslDebug, pData, 4);
            printf ("NSL: CTRL: NSL debug now is 0x%8X\n", (int) gu4NslDebug);
            break;

        case CTRL_MSG_LINK_STATUS:
        {
            struct linkStatus  *linkStatus;
            int                 u2RxSw, u2RxPort, status;
            int                 i4IfIndex = 0;

            linkStatus = &pCtrlmsg->data.linkStatus;

            u2RxSw = linkStatus->s;
            u2RxPort = linkStatus->p;
            status = linkStatus->status;

            if (IS_NSL_DEBUG_CTRL_MSGS (gu4NslDebug))
            {
                printf ("NSL: CTRL: port %s; sw: %d, port: %d\n",
                        (status == NSL_DOWN ? "DOWN" : "UP"), u2RxSw, u2RxPort);
            }
            gaNslPort[u2RxPort - 1].u1PortState = status;

            if ((i4IfIndex = (int) gaNslPort[u2RxPort - 1].u4IfIndex) < 0)
            {
                break;
            }

            if (CFA_IF_ADMIN (i4IfIndex) == CFA_IF_UP)
            {
                CfaIfmHandleInterfaceStatusChange (i4IfIndex, CFA_IF_UP,
                                                   (status ? CFA_IP_UP :
                                                    CFA_IF_DOWN),
                                                   CFA_IF_LINK_STATUS_CHANGE);
            }

            break;
        }

        default:
            if (IS_NSL_DEBUG_CTRL_MSGS (gu4NslDebug))
            {
                printf ("NSL: CTRL: INVALID control message (%d)\n",
                        pCtrlmsg->code);
            }
            break;

    }
}

VOID
NslBufDump (UINT1 *pBuf, UINT1 *pTitle, INT4 i4Len)
{
    INT4                i4Tmp;

    if (!pBuf)
        return;

    printf ("\n%s\n", pTitle);
    for (i4Tmp = 0; i4Tmp < i4Len; i4Tmp++)
    {
        if (i4Tmp)
        {
            if (!(i4Tmp % 24))
            {
                printf ("\n");
            }
            else
            {
                if (!(i4Tmp % 8))
                {
                    printf ("  ");
                }
                else
                {
                    if (i4Tmp)
                    {
                        printf (" ");
                    }
                }
            }
        }
        printf ("%02X", pBuf[i4Tmp]);
    }
    printf ("\n");
}

VOID
CfaGddShutdown (VOID)
{
    INT4                i;

#ifdef USING_MS
    NpSimCloseMSSocket ();
#endif

#ifdef FIREWALL_WANTED
/* Currently commented the character device socket code
 *
    NpSimCloseSecuritySocket();
*/
#endif /* FIREWALL_WANTED */
    close (gNslSock);
    close (gRpcSock);
    gNslSock = -1;
    gRpcSock = -1;
    for (i = 0; i < NSL_MAX_PORTS; i++)
    {
        gaNslPort[i].u4IfIndex = i + 1;
        gaNslPort[i].u1PortState = NSL_DOWN;
        gaNslPort[i].PortName[0] = 0;
    }
}

INT4
CfaGddSetGddType (UINT4 u4IfIndex)
{
    UINT1               u1IfType;

    CfaGetIfType (u4IfIndex, &u1IfType);

    CFA_GDD_TYPE (u4IfIndex) = u1IfType;
    return (CFA_SUCCESS);
}

INT4
CfaGddLinuxFixForWanPipe (UINT4 u4IfIndex)
{
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    return (CFA_SUCCESS);
}

VOID
CfaGddOsRemoveIfFromList (UINT4 u4IfIndex)
{
    /* unused function, we don't maintain any polling */
    /* table because we have only one real socket.    */
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
}

VOID
CfaGddOsAddIfToList (UINT4 u4IfIndex)
{
    /* unused function, we don't maintain any polling */
    /* table because we have only one real socket.    */
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
}

INT4
CfaGddOsProcessRecvEvent (VOID)
{
    struct sockaddr_in  FromAddr;
    UINT4               u4IfIndex, u4PktLen;
    INT4                i4RetVal, i4AddrLen;
    UINT1              *pData;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tEnetV2Header      *pHdr;
    tEnetSnapHeader    *pSnapHdr;
    fd_set              readFds;
    UINT2               u2Protocol = 0, u2LenOrType, u2RxPort, u2RxSw;
    INT2                i2PktLen, i2HdrLen, i2IpPktLen = 0;
    struct timeval      tv;
#ifdef BFD_WANTED
    UINT1               au1CallBackStrn[] = "CallBackBfd";
    UINT4               u4ContextId;
    tBfdSessionHandle   BfdSessHandle;
    tBfdHwhandle        BfdHwSessHandle;
    tBfdEventType       eBfdEventType;
    BOOL1               bActionFlag;
    tBfdParams          BfdParams;
    tBfdOffPktBuf       BfdPkt;
    UINT4               u4BfdPktLen = 0;
    UINT1               au1Buffer[BFD_MAX_CALL_BACK_BUF];
#endif

#ifdef TCPDUMP_WANTED
    typedef             u_int (*if_printer) (const struct pcap_pkthdr *,
                                             const u_char *);
    struct print_info
    {
        if_printer          printer;
    };
    struct pcap_pkthdr  h;
    struct print_info   user;
    u_int               ether_if_print (const struct pcap_pkthdr *,
                                        const u_char *);
#endif

    /* - Select to check if socket has any data to read
       - If select fails, stop loop
       - Check if our socket descriptor shows data present
       - If no data present stop loop
       - If buffer pointer null, allocate a buffer; if not use
       allocated buffer
       - Call function to read in the packet
       - If read fails stop loop
       - Call function to validate packet and get ifIndex, source
       and destination mac-address
       - If packet valid, call function to process packet further
       - If packet invalid, loop further to check for more packets
       - When loop is finished, if buffer pointer not null, free
       it and return
     */
    while (1)
    {
        FD_ZERO (&readFds);
        FD_SET (gNslSock, &readFds);
        FD_SET (gRpcSock, &readFds);

        gMaxFd = gNslSock;
        if (gRpcSock > gMaxFd)
            gMaxFd = gRpcSock;

        tv.tv_sec = 0;
        tv.tv_usec = 0;
        i4RetVal = select ((gMaxFd + 1), &readFds, NULL, NULL, &tv);
        if ((i4RetVal < 0) ||
            (!(FD_ISSET (gNslSock, &readFds))
             && !(FD_ISSET (gRpcSock, &readFds))))
        {
            break;                /* No data to read or some error */
        }

        if (FD_ISSET (gRpcSock, &readFds))
        {
            NslDoRpc ();
            continue;
        }
        /* Socket has data. Get buffer if needed to read packet in to. */
        if (pBuf == NULL)
        {
            u4PktLen = 1520;
            if ((pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktLen, 0)) == NULL)
            {
                break;
            }
        }

        /* We don't check return value here. We assume buffer is linear. */
        pData = CRU_BUF_GetDataPtr (pBuf->pFirstValidDataDesc);

        MEMSET (&FromAddr, 0, sizeof (FromAddr));
        i4AddrLen = sizeof (FromAddr);

        if ((i2PktLen = recvfrom (gNslSock, (VOID *) pData, CFA_MAX_DRIVER_MTU +
                                  CFA_MAX_FRAME_HEADER_SIZE + 4, 0,
                                  (struct sockaddr *) &FromAddr,
                                  (socklen_t *) & i4AddrLen)) <= 0)
        {
            break;
        }

#ifdef TCPDUMP_WANTED
        if (gu4DoTcpDump)
        {
            if (((TDArgs.u4Flags & CFA_TCPDUMP_c) == 0) || TDArgs.u4Count--)
            {
                gettimeofday (&h.ts, NULL);
                h.caplen = h.len = i2PktLen - 4;
                user.printer = ether_if_print;
                Xflag = xflag = eflag = 1;
                print_packet ((u_char *) & user, &h, pData + 4);
            }
            if ((TDArgs.u4Flags & CFA_TCPDUMP_c) && TDArgs.u4Count == 0)
                gu4DoTcpDump = 0;
        }
#endif

        if (i2PktLen > 1518)
        {
            printf ("GDDNSL: pktsize %d\n", i2PktLen);
        }
        pBuf->pFirstValidDataDesc->u4_ValidByteCount += i2PktLen;
        pBuf->pFirstValidDataDesc->u4_FreeByteCount -= i2PktLen;

        MEMCPY (&u2RxSw, pData, 2);
        MEMCPY (&u2RxPort, (pData + 2), 2);

        i2PktLen -= 4;
        CRU_BUF_Move_ValidOffset (pBuf, 4);

        if (IS_NSL_DEBUG_PKT_EVENT (gu4NslDebug) &&
            (!IS_NSL_DEBUG_PKT_DUMP (gu4NslDebug)))
        {
            u4IfIndex = (gaNslPort[u2RxPort - 1].u4IfIndex);
            TTRACE1 ("NSL: PktEv: Rx sw: %d, port: %d, ifIndex: %d, Size: %d\n",
                     (int) u2RxSw, (int) (u2RxPort - 1), (int) u4IfIndex,
                     (int) i2PktLen);
        }

        if ((u2RxPort == 0) && (u2RxSw == 0))
        {
            /* this is error; control messages are handled only at the
             * packet server
             */
#ifdef BFD_WANTED
            UINT4               u4Len = 0;
            if (MEMCMP (pData + 4, au1CallBackStrn,
                        sizeof (au1CallBackStrn)) == 0)
            {
                MEMSET (au1Buffer, 0, BFD_MAX_CALL_BACK_BUF);
                MEMSET (&BfdSessHandle, 0, sizeof (tBfdSessionHandle));
                MEMSET (&BfdHwSessHandle, 0, sizeof (tBfdHwhandle));
                MEMSET (&BfdParams, 0, sizeof (tBfdParams));
                MEMSET (&BfdPkt, 0, sizeof (tBfdOffPktBuf));

                BfdPkt.pau1PktBuf = au1Buffer;
                /* MS header skip */
                u4Len += 4;
                u4Len += sizeof (au1CallBackStrn);

                PTR_FETCH4 (u4ContextId, pData + u4Len);
                u4Len += 4;

                /*Fetch Session Handle */
                PTR_FETCH4 (BfdSessHandle.u4BfdSessId, pData + u4Len);
                u4Len += 4;

                PTR_FETCH4 (BfdSessHandle.u4BfdSessLocalDiscr, pData + u4Len);
                u4Len += 4;

                PTR_FETCH4 (BfdSessHandle.u4BfdSlotId, pData + u4Len);
                u4Len += 4;

                PTR_FETCH4 (BfdSessHandle.u4BfdCardNum, pData + u4Len);
                u4Len += 4;

                /*Fetch Hardware Handle */
                PTR_FETCH4 (BfdHwSessHandle.u4BfdSessHwHandle, pData + u4Len);
                u4Len += 4;

                PTR_FETCH4 (BfdHwSessHandle.u4BfdOffErrNum, pData + u4Len);
                u4Len += 4;

                /*Fetch Event Type */
                PTR_FETCH4 (eBfdEventType, pData + u4Len);
                u4Len += 4;

                /*Fetch Action Flag */
                bActionFlag = *(pData + u4Len);
                u4Len += 1;

                /*Fetch BFD params */
                MEMCPY ((UINT1 *) &BfdParams, pData + u4Len,
                        sizeof (BfdParams));
                u4Len += sizeof (BfdParams);

                /*Fetch Bfd pkt length */
                PTR_FETCH4 (u4BfdPktLen, pData + u4Len);
                u4Len += 4;

                MEMCPY (BfdPkt.pau1PktBuf, pData + u4Len, u4BfdPktLen);
                BfdPkt.u4Len = u4BfdPktLen;

                /*CALL Bfd CallBack */
                FsMiBfdHwSessionCallBack (u4ContextId,
                                          &BfdSessHandle,
                                          &BfdHwSessHandle,
                                          eBfdEventType,
                                          bActionFlag, &BfdParams, &BfdPkt);

                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                pBuf = NULL;
                continue;
            }
#endif

#ifdef ECFM_WANTED
            /* Check if Ccm Offload Control message */
            if (MEMCMP (pData + 4, "CallBackPdu", 11) == 0)
            {
                UINT4               u4NpContextId = 0;
                UINT2               u2TxFilterId = 0;
                UINT2               u2RxFilterId;
                UINT1               au1HwHandler[ECFM_HW_MEP_HANDLER_SIZE];
                if (MEMCMP (pData + 15, "Rx", 2) == 0)
                {
                    u2RxFilterId = *(pData + 17);
                    u2RxFilterId = u2RxFilterId << 8;
                    u2RxFilterId |= *(pData + 18);

                    u4NpContextId = *(pData + 19);
                    u4NpContextId = u4NpContextId << 8;
                    u4NpContextId |= *(pData + 20);
                    u4NpContextId = u4NpContextId << 8;
                    u4NpContextId |= *(pData + 21);
                    u4NpContextId = u4NpContextId << 8;
                    u4NpContextId |= *(pData + 22);

                    u4IfIndex = *(pData + 23);
                    u4IfIndex = u4IfIndex << 8;
                    u4IfIndex |= *(pData + 24);
                    u4IfIndex = u4IfIndex << 8;
                    u4IfIndex |= *(pData + 25);
                    u4IfIndex = u4IfIndex << 8;
                    u4IfIndex |= *(pData + 26);

                    /* Here 2 APIs can be used
                       1.EcfmCcmOffHandleRxCallBack
                       2.EcfmCcmHwHandleRxCallBack
                       EcfmCcmOffHandleRxCallBack API should be used when the 
                       H/W able to retun the FilterId given be control plane 
                       EcfmCcmHwHandleRxCallBack API should be used when the
                       H/W able to retun the H/W handle retunrd by HW. But 
                       To test Second API EcfmCcmOffHandleRxCallBack is replced
                       with EcfmCcmHwHandleRxCallBack */

                    MEMCPY (au1HwHandler, &u2RxFilterId,
                            ECFM_HW_MEP_HANDLER_SIZE);
                    EcfmCcmHwHandleRxCallBack (u4NpContextId, au1HwHandler,
                                               u4IfIndex);
                }
                else
                {
                    u2TxFilterId = *(pData + 17);
                    u2TxFilterId = u2TxFilterId << 8;
                    u2TxFilterId |= *(pData + 18);

                    u4NpContextId = *(pData + 19);
                    u4NpContextId = u4NpContextId << 8;
                    u4NpContextId |= *(pData + 20);
                    u4NpContextId = u4NpContextId << 8;
                    u4NpContextId |= *(pData + 21);
                    u4NpContextId = u4NpContextId << 8;
                    u4NpContextId |= *(pData + 22);

                    /* Here 2 APIs Can be used
                       1.EcfmCcmOffHandleTxFailureCallBack
                       2.EcfmCcmHwHandleTxFailureCallBack
                       EcfmCcmOffHandleTxFailureCallBack API should be used when
                       the H/W able to retun the FilterId given be control plane
                       EcfmCcmHwHandleTxFailureCallBack API should be used when
                       the H/W able to retun the H/W handle retunrd by HW. But
                       To test Second API EcfmCcmOffHandleRxCallBack is replaced
                       with EcfmCcmHwHandleTxFailureCallBack */

                    MEMCPY (au1HwHandler, &u2TxFilterId,
                            ECFM_HW_MEP_HANDLER_SIZE);
                    EcfmCcmHwHandleTxFailureCallBack (u4NpContextId,
                                                      au1HwHandler);
                }
            }
            else
#endif
            {
                NslHandlePktSrvCtrlMsg (pData);
            }
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            pBuf = NULL;
            continue;

        }

        if ((u2RxPort > NSL_MAX_PORTS) || (u2RxSw != gu2NslSw))
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            pBuf = NULL;
            continue;
        }

        pData += 4;

        u2RxPort--;
        u4IfIndex = (gaNslPort[u2RxPort].u4IfIndex);

        if (gaNslPort[u2RxPort].u1PortState == NSL_DOWN)
        {
            if (IS_NSL_DEBUG_PKT_EVENT (gu4NslDebug))
            {
                printf ("    i/f down; discarding packet\n");
            }
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            pBuf = NULL;
            continue;
        }
        /* check if the interface exists */
        if ((CFA_IF_ENTRY (u4IfIndex) == NULL) ||
            (CFA_IF_RS (u4IfIndex) != CFA_RS_ACTIVE))
        {
            if (IS_NSL_DEBUG_PKT_EVENT (gu4NslDebug))
            {
                printf ("    no i/f; discarding packet\n");
            }
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            pBuf = NULL;
            continue;
        }

        pHdr = (tEnetV2Header *) (pData);
        u2LenOrType = OSIX_NTOHS (pHdr->u2LenOrType);
        if (CFA_ENET_IS_TYPE (u2LenOrType))
        {
            u2Protocol = u2LenOrType;
            i2HdrLen = CFA_ENET_V2_HEADER_SIZE;
        }
        else
        {
            pSnapHdr = (tEnetSnapHeader *) pData;
            i2HdrLen = CFA_ENET_SNAP_HEADER_SIZE;

            /* check for LLC control frame first - we expect only LLC in SNAP now */
            if (pSnapHdr->u1Control == CFA_LLC_CONTROL_UI)
            {
                /*check for presence of SNAP which may carry IP/ARP/RARP after LLC */
                if ((pSnapHdr->u1DstLSap == CFA_LLC_SNAP_SAP) ||
                    (pSnapHdr->u1SrcLSap == CFA_LLC_SNAP_SAP) ||
                    (pSnapHdr->u1Oui1 == 0x00) ||
                    (pSnapHdr->u1Oui2 == 0x00) || (pSnapHdr->u1Oui3 == 0x00))
                {
                    /* determine which protocol is being carried */
                    u2Protocol = OSIX_NTOHS (pSnapHdr->u2ProtocolType);
                }                /* end of SNAP framing */
            }                    /* end of LLC framing */
        }

        /* u2Protocol = 0xFF; */
        if (u2Protocol == CFA_ENET_IPV4)
        {
            MEMCPY (&i2IpPktLen, (pData + i2HdrLen + IP_PKT_OFF_LEN),
                    sizeof (INT2));
            i2IpPktLen = OSIX_NTOHS (i2IpPktLen);
            u4PktLen = (UINT4) (i2IpPktLen + i2HdrLen);
        }
#ifdef IP6_WANTED
        else if (u2Protocol == CFA_ENET_IPV6)
        {
            MEMCPY (&i2IpPktLen, (pData + i2HdrLen + IPV6_OFF_PAYLOAD_LEN),
                    sizeof (INT2));
            i2IpPktLen = OSIX_NTOHS (i2IpPktLen);
            u4PktLen = (UINT4) (i2IpPktLen + i2HdrLen + IPV6_HEADER_LEN);
        }
#endif /* IP6_WANTED */
        else
        {
            u4PktLen = i2PktLen;    /* leaving out the simulation header */
        }

        CFA_IF_SET_IN_OCTETS (u4IfIndex, u4PktLen);
#ifdef RMON_WANTED
        /* RMON code should process only if SW_FWD enabled.         */
        /* Update rmon tables. Return value doesn't matter.         */
        /* Routine should be called only for ethernet interfaces.   */
        /* We avoid checking because we support only ethernet.      */
        CfaIwfRmonUpdateTables (pData, u4IfIndex, u4PktLen);
#endif /* RMON_WANTED */
        if (IS_NSL_DEBUG_PKT_DUMP (gu4NslDebug))
        {
            CHR1                Tmp[80];

            sprintf (Tmp, "NSL: PktDump: Rx Sw: %d, port: %d, ifIndex: %d\n",
                     (int) u2RxSw, (int) u2RxPort, (int) u4IfIndex);
            NslBufDump (pData, (UINT1 *) Tmp, u4PktLen);
        }

        if (CfaProcessPacket (pBuf, u4IfIndex, u4PktLen) != CFA_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        }
        pBuf = NULL;
    }

    if (pBuf != NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }
    return (CFA_SUCCESS);
}

INT4
CfaGddConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption, VOID *pConfigParam)
{
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    DUMMY_VARIABLE (pConfigParam);    /* avoid compiler warning */
    DUMMY_VARIABLE (u1ConfigOption);    /* avoid compiler warning */

    if ((u1ConfigOption == CFA_ENET_EN_PROMISC) ||
        (u1ConfigOption == CFA_ENET_DIS_PROMIS))
        return (CFA_SUCCESS);
    else
        return (CFA_FAILURE);
}

INT4
CfaGddGetHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
    UINT2               u2Port;
    UINT1               u1IfType;

    CfaGetIfType (u4IfIndex, &u1IfType);
    /* 
     * When the request comes from ARP for the hardware address 
     * of VLAN interface, this function should not return FAILURE. Instead, 
     * copy the MAC address of the default bridged Ethernet interface into 
     * the MAC address of VLAN interface and ARP/RARP will update the same 
     * for VLAN interfaces.
     */
    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        if (u1IfType == CFA_L3IPVLAN)
        {
            CfaGetSysMacAddress (au1HwAddr);
            return CFA_SUCCESS;
        }
    }

    if (u1IfType != CFA_ENET)
    {
        MEMSET (au1HwAddr, 0, CFA_ENET_ADDR_LEN);
        return CFA_SUCCESS;
    }

    if (gi4SingleMacAddr == 0)
    {
        /* single MAC for whole switch */
        MEMCPY (au1HwAddr, gau1HwAddr[0], NSL_ADDR_LEN);
    }

    /* new interface being created by CFA */
    /* check if NSL is configured with an interface by that name */
    /* NSL interfaces MUST have names like eth0, eth1, eth2,...  */
    for (u2Port = 0; u2Port < NSL_MAX_PORTS; u2Port++)
    {
        if (STRCMP (CFA_GDD_PORT_NAME (u4IfIndex),
                    gaNslPort[u2Port].PortName) == 0)
        {
            /* Found the NSL interface. Register CFA on interface */
            /* and also store array index as GDD port descriptor. */
            gaNslPort[u2Port].u1PortState = NSL_UP;
            gaNslPort[u2Port].u4IfIndex = u4IfIndex;
            CFA_GDD_PORT_DESC (u4IfIndex) = u2Port;

            if (gi4SingleMacAddr != 0)
            {
                /* mac address per port */
                MEMCPY (au1HwAddr, gau1HwAddr[u2Port], NSL_ADDR_LEN);

            }

            return (CFA_SUCCESS);
        }
    }
    return (CFA_SUCCESS);
}

INT4
CfaGddGetOsHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
    UINT2               u2Port;
    INT4                i4rc;
    UINT1               u1IfType = 0;

    /* The name of the function is misleading. For ethernet */
    /* this function actually opens the port and registers. */
    /* If interface already open/registered return address */
    if (gi4SingleMacAddr == 0)
    {
        /* single MAC for whole switch */
        MEMCPY (au1HwAddr, gau1HwAddr[0], NSL_ADDR_LEN);
    }

    CfaGetIfaceType (u4IfIndex, &u1IfType);
    if ((u1IfType == CFA_PSEUDO_WIRE) || (u1IfType == CFA_PPP))
    {
        if (CfaGetPswHwAddr (u4IfIndex, au1HwAddr) != CFA_SUCCESS)
        {
            return CFA_FAILURE;
        }
        return CFA_SUCCESS;
    }
    /* new interface being created by CFA */
    /* check if NSL is configured with an interface by that name */
    /* NSL interfaces MUST have names like eth0, eth1, eth2,...  */
    for (u2Port = 0; u2Port < NSL_MAX_PORTS; u2Port++)
    {
        if (STRCMP (CFA_GDD_PORT_NAME (u4IfIndex),
                    gaNslPort[u2Port].PortName) == 0)
        {
            /* Found the NSL interface.                       */
            /* RPC call to NPSIM to open physical interface   */
            /* Applicable only in STANDALONE mode             */
            /* In MS mode, the rpc server stub zimbly returns */
            /* To avoid recompiling ISS we keep the dummy RPC */
            i4rc = NpSimOpenEnetSocket (gaNslPort[u2Port].PortName, u2Port);
            if (i4rc < 0)
            {
                printf ("ERROR: Eth open failed for %s\n",
                        gaNslPort[u2Port].PortName);
            }

            /* Register CFA on interface */
            /* and also store array index as GDD port descriptor. */
            gaNslPort[u2Port].u1PortState = NSL_UP;
            gaNslPort[u2Port].u4IfIndex = u4IfIndex;
            CFA_GDD_PORT_DESC (u4IfIndex) = u2Port;

            if (gi4SingleMacAddr != 0)
            {
                /* mac address per port */
                MEMCPY (au1HwAddr, gau1HwAddr[u2Port], NSL_ADDR_LEN);
            }

            return (CFA_SUCCESS);
        }
    }
    return (CFA_SUCCESS);
}

/* prototype added for warning suppression */
INT4                CfaGddLinuxEthSockOpen (UINT4 u4IfIndex);

INT4
CfaGddLinuxEthSockOpen (UINT4 u4IfIndex)
{
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    return (CFA_SUCCESS);
}

INT4
CfaGddEthOpen (UINT4 u4IfIndex)
{
    UINT2               u2Port;

    /* Nothing to do; port opened in CfaGddGetOsHwAddr() call */
    for (u2Port = 0; u2Port < NSL_MAX_PORTS; u2Port++)
    {
        if (u4IfIndex == gaNslPort[u2Port].u4IfIndex)
        {
            /* Found the NSL interface. Register CFA on interface */
            /* and also store array index as GDD port descriptor. */
            gaNslPort[u2Port].u4IfIndex = u4IfIndex;
            CFA_GDD_PORT_DESC (u4IfIndex) = u2Port;
            return (CFA_SUCCESS);
        }
    }
    return (CFA_SUCCESS);
}

INT4
CfaGddEthClose (UINT4 u4IfIndex)
{
    UINT2               u2Port;

    /* Scan NSL port array, match input ifIndex and close */
    for (u2Port = 0; u2Port < NSL_MAX_PORTS; u2Port++)
    {
        if ((gaNslPort[u2Port].u4IfIndex) == u4IfIndex)
        {
            CFA_GDD_PORT_DESC (u4IfIndex) = CFA_INVALID_DESC;    /* invalid port */
            return (CFA_SUCCESS);
        }
    }
    return (CFA_FAILURE);        /* interface does not exist */
}

INT4
CfaGddEthWrite (UINT1 *pu1Buf, UINT4 u4IfIndex, UINT4 u4Size)
{
    struct sockaddr_in  ToAddr;
    tHwPortInfo         HwPortInfo;
    UINT4               u4StackingModel = ISS_STACKING_MODEL_NONE;
#ifdef RM_WANTED
    tHwInfo             HwInfo;
#endif
    UINT1              *pu1TxBuf = NULL;
    INT4                i4Bytes;
    INT2                i2Port;
    UINT2               u2Protocol;
    UINT2               u2EtherType = 0;
#define VLAN_PROTO_OFFSET 12
#define VLAN_PROTO_ID     0x8100

#ifdef TCPDUMP_WANTED
    typedef             u_int (*if_printer) (const struct pcap_pkthdr *,
                                             const u_char *);
    struct print_info
    {
        if_printer          printer;
    };
    struct pcap_pkthdr  h;
    struct print_info   user;
    u_int               ether_if_print (const struct pcap_pkthdr *,
                                        const u_char *);
#endif

    u4StackingModel = ISS_GET_STACKING_MODEL ();
    MEMCPY (&u2EtherType, pu1Buf + CFA_NPSIM_IPC_ETHERTYPE_OFFSET,
            sizeof (UINT2));
    u2EtherType = OSIX_NTOHS (u2EtherType);
    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);
    if ((u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL) ||
        (u4StackingModel == ISS_DISS_STACKING_MODEL))
    {

        if (CfaGetRetrieveNodeState () != RM_ACTIVE)
        {
            /* Allow only IPC message , drop all other traffic */
            /* In Distributed Model, distributed protocols will run in slave node */
            if (!((HwPortInfo.au4ConnectingPortIfIndex[0] == u4IfIndex) ||
                  (u4StackingModel == ISS_DISS_STACKING_MODEL)))
            {
                TTRACE1 ("Trying to write when the node is not active \n");
                /* only IPC message Tx is allowed in standby ports */
                return CFA_FAILURE;
            }
        }
        else
        {
            if (u4IfIndex != HwPortInfo.au4ConnectingPortIfIndex[0])
            {
                /* Check if the outgoing port is a remote unit port */
                if ((!((u4IfIndex >= HwPortInfo.u4StartIfIndex) &&
                       (u4IfIndex <= HwPortInfo.u4EndIfIndex))) &&
                    (u4IfIndex <= (INT2) SYS_DEF_MAX_PHYSICAL_INTERFACES))
                {
                    /* u4IfIndex is present in the remote unit. so send the packet to remote unit. */
                    if (CfaNpSimPostPktToStandby (pu1Buf, u4IfIndex, u4Size) ==
                        CFA_FAILURE)
                    {
                        CRTTRACE1 ("CfaNpSimPostPktToStandby send failed \n");
                        return CFA_FAILURE;
                    }
                    return CFA_SUCCESS;
                }
            }
        }
    }

    if (((u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL) ||
         (u4StackingModel == ISS_DISS_STACKING_MODEL)) &&
        (HwPortInfo.au4ConnectingPortIfIndex[0] == u4IfIndex) &&
        (!(u2EtherType == CFA_NPSIM_IPC_MSG_ETHERTYPE)))
    {
#ifdef RM_WANTED
        MEMSET (&HwInfo, 0, sizeof (tHwInfo));
        HwInfo.u4MessageType = RM_SYNC_PKT;
        HwInfo.u4IfIndex = u4IfIndex;
        HwInfo.u4PktSize = u4Size;
        HwInfo.uHwMsgInfo.HwGenMsgInfo.pu1Data = pu1Buf;
        if (FsCfaHwSendIPCMsg (&HwInfo) == FNP_FAILURE)
        {
            return CFA_FAILURE;
        }
#endif
        return CFA_SUCCESS;
    }
    /* 
     * Theory:
     * ======
     * VLAN untagged Frame:
     * Min Frame size =  14 (EthHdr)
     *                 + 46 (Data)
     *                 +  4 (FCS)
     *                 ----------
     *                 = 64
     *                 ----------
     *
     * VLAN Tagged frame:
     * Min frame size =   12 (Dest & Src MAC)
     *                  +  2 (VlanProtId)
     *                  +  2 (VlanCtrlInfo)
     *                  +  2 (Len/Type)
     *                  + 46 (Data)
     *                  +  4 (FCS)
     *                  ----------
     *                  = 68 bytes.
     *                  ----------
     *
     * FCS bytes are added by the device driver. So in dealing with the
     * problem of allowed min/max frame sizes, we drop the 4 bytes
     * from our calculation.
     *
     * So the above numbers are 60 and 64 respectively.
     * The spec. says that the min. frame size is implementation defined.
     * Linux sends 60 byte *tagged* packets. It does not pad to 64 bytes.
     * We conform to this and allow 60 byte packets on both tagged and
     * untagged frames. (A similar argument doesn't seem to be true for
     * max frame sizes).
     *
     * Thus, u4Size >= 60 bytes. (u4Size is a fn. param. See above).
     *
     * CfaGddWrite() pads its received buffer to 60 bytes (if it is lesser). 
     * and then calls CfaGddEthWrite (this function).
     * CFA will ensure that u4Size >= 60. So we do not check for it.
     */

    /* Get the protocol id (12th and 13 byte) from the frame */
    MEMCPY (&u2Protocol, &pu1Buf[VLAN_PROTO_OFFSET], sizeof (UINT2));
    u2Protocol = OSIX_NTOHS (u2Protocol);
    /* Dont do the MTU check for IPC messages */
    if (!(((u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL) ||
           (u4StackingModel == ISS_DISS_STACKING_MODEL)) &&
          (u2EtherType == CFA_NPSIM_IPC_MSG_ETHERTYPE)))
    {
        /* check if it is a VLAN tagged frame */
        if (u2Protocol == VLAN_PROTO_ID)
        {
            /* 
             * Theory:
             * ======
             * VLAN Tagged Frame: 
             * Max frame size =     12 (Dest & Src MAC)
             *                  +    2 (VlanProtId)
             *                  +    2 (VlanCtrlInfo)
             *                  +    2 (LenOrType)
             *                  + 1500 (Data)
             *                  +    4 (FCS)
             *                  ------------
             *                  = 1522
             *                  ------------
             * Discard the frame if the length exceeds 1518 (excluding FCS)
             *
             * Untagged Frame: 
             * Max frame size =      12 (Dest & Src MAC)
             *                   +    2 (LenOrType)
             *                   + 1500 (Data)
             *                   +    4 (FCS)
             *                   -------
             *                   = 1518
             *                   -------
             * Discard the frame if the length exceeds 1514 (excluding FCS)
             *
             * As given above, our calculations drop the 4 bytes of FCS.
             */
            if (u4Size > 1518)
            {
                return (CFA_FAILURE);
            }
        }
        else
        {
            /*
             * Untagged Frame:
             */
            if (u4Size > 1514)
            {
                return (CFA_FAILURE);
            }
        }
    }

    i2Port = NslGetPortFromIfIndex (u4IfIndex);

    if (i2Port < 0)
    {
        return (CFA_FAILURE);
    }

    if (gaNslPort[i2Port].u1PortState != NSL_UP)
    {
        return (CFA_FAILURE);
    }

    if (IS_NSL_DEBUG_PKT_DUMP (gu4NslDebug))
    {
        CHR1                Tmp[80];

        sprintf (Tmp, "NSL: PktDump: Tx sw: %d, port: %d, ifIndex: %d"
                 "Size: %d", (int) gu2NslSw, (int) i2Port,
                 (int) u4IfIndex, (int) u4Size);
        NslBufDump (pu1Buf, (UINT1 *) Tmp, u4Size);
    }
    else
    {
        if (IS_NSL_DEBUG_PKT_EVENT (gu4NslDebug))
        {
            TTRACE1 ("NSL: PktEv: Tx sw: %d, port: %d, ifIndex: %d, "
                     "Size: %d\n", (int) gu2NslSw, (int) i2Port,
                     (int) u4IfIndex, (int) u4Size);
        }
    }

    pu1TxBuf = MemAllocMemBlk (gPktBufPoolId);
    if (pu1TxBuf == NULL)
    {
        CRTTRACE1 ("CfaGddEthWrite: Memory allocation for Tx Buf failed\n");
        return CFA_FAILURE;
    }
    MEMSET (pu1TxBuf, 0, (CFA_MAX_DRIVER_MTU + CFA_MAX_FRAME_HEADER_SIZE));
    MEMCPY (&(pu1TxBuf[0]), &gu2NslSw, 2);
    i2Port++;
    MEMCPY (&(pu1TxBuf[2]), &i2Port, 2);
    MEMCPY (&(pu1TxBuf[4]), pu1Buf, u4Size);

    ToAddr.sin_family = AF_INET;
    ToAddr.sin_port = htons (gu2NslBaseNpSimPort + gu2NslSw);
    MEMCPY ((CHR1 *) (&(ToAddr.sin_addr.s_addr)),
            (CHR1 *) & (gu4NslPktServerIpAddr), 4);

    i4Bytes = sendto (gNslSock, (void *) pu1TxBuf, (u4Size + 4),
                      0, (struct sockaddr *) &ToAddr, sizeof (ToAddr));
    if (i4Bytes == (INT4) (u4Size + 4))
    {
#ifdef TCPDUMP_WANTED
        if (gu4DoTcpDump)
        {
            if (((TDArgs.u4Flags & CFA_TCPDUMP_c) == 0) || TDArgs.u4Count--)
            {
                gettimeofday (&h.ts, NULL);
                h.caplen = h.len = u4Size;
                user.printer = ether_if_print;
                Xflag = xflag = eflag = 1;
                print_packet ((u_char *) & user, &h, pu1TxBuf + 4);
            }
            if ((TDArgs.u4Flags & CFA_TCPDUMP_c) && TDArgs.u4Count == 0)
                gu4DoTcpDump = 0;
        }
#endif
        MemReleaseMemBlock (gPktBufPoolId, pu1TxBuf);
        return (CFA_SUCCESS);
    }

    MemReleaseMemBlock (gPktBufPoolId, pu1TxBuf);
    return (CFA_FAILURE);
}

INT4
CfaGddEthWriteWithPri (UINT1 *pData, UINT4 u4IfIndex, UINT4 u4PktLen,
		        UINT1 u1Priority)
{
    if(CfaGddEthWrite(pData,u4IfIndex,u4PktLen) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }
    DUMMY_VARIABLE (u1Priority);
    return CFA_SUCCESS;
}

INT4
CfaGddEthRead (UINT1 *pData, UINT4 u4IfIndex, UINT4 *pu4PktLen)
{
    /* Read and process is done in CfaGddOsProcsssRecvEvent. */
    /* Nothing to do here. Just avoid compiler warnings.     */
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    DUMMY_VARIABLE (pData);        /* avoid compiler warning */
    DUMMY_VARIABLE (pu4PktLen);    /* avoid compiler warning */
    return CFA_FAILURE;
}

INT4
CfaProcessPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex, UINT4 u4PktLen)
{
    UINT2               u2EtherType = 0;
    UINT2               u2Protocol = 0;
    UINT2               u2Offset = 0;
    UINT1               u1IfType = 0;

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Protocol,
                               CFA_VLAN_TAG_OFFSET,CFA_VLAN_PROTOCOL_SIZE);
    u2Protocol = OSIX_NTOHS (u2Protocol);

    if (VLAN_PROVIDER_PROTOCOL_ID == u2Protocol)
    {
        u2Offset = CFA_ENET_TYPE_OR_LEN + CFA_ENET_TYPE_OR_LEN;
    }

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2EtherType,
                               CFA_NPSIM_HB_MSG_ETHERTYPE_OFFSET + u2Offset,
                               CFA_NPSIM_HB_MSG_ETHERTYPE_LEN);
    u2EtherType = OSIX_NTOHS(u2EtherType);
    if (u2EtherType == CFA_NPSIM_HB_MSG_ETHERTYPE)
    {
        CfaNpSimPostHBPktToICCHQ (pBuf, u4IfIndex, u4PktLen);
    }
    else
    {
        if (CfaGetRetrieveNodeState () != RM_ACTIVE)
        {
            TTRACE1 ("Trying to write when the node is not active \n");
            return CFA_FAILURE;
        }

        /* Check if higher layer registered. */
        if (CFA_GDD_HL_RX_FNPTR (u4IfIndex) == NULL)
        {
            CFA_IF_SET_IN_DISCARD (u4IfIndex);    /* No higher layer */
            return (CFA_FAILURE);
        }

        CfaGetIfType (u4IfIndex, &u1IfType);

        /* If higher layer registered, give packet to it. */
        if ((CFA_GDD_HL_RX_FNPTR (u4IfIndex))
            (pBuf, u4IfIndex, u4PktLen, u1IfType, CFA_ENCAP_NONE) != CFA_SUCCESS)
        {
            return (CFA_FAILURE);
        }
    }
    return (CFA_SUCCESS);
}

UINT1
CfaGddGetLinkStatus (UINT4 u4IfIndex)
{
    return CfaCfaNpGetLinkStatus (u4IfIndex);
}

INT4
NslGetPortFromIfIndex (UINT4 u4IfIndex)
{
    INT4                i4Port;

    /* Scan NSL interface array, match input ifIndex and validate */
    for (i4Port = 0; i4Port < NSL_MAX_PORTS; i4Port++)
    {
        if (gaNslPort[i4Port].u4IfIndex == u4IfIndex)
        {
            return i4Port;
        }
    }

    return -1;
}

/* prototype added for warning suppression */
VOID                CfaGddScanLinkStatus (VOID);

VOID
CfaGddScanLinkStatus (VOID)
{
    /* Scans the status of all links.   
     * Dummy function in the case of NS   
     */
    return;
}

INT4
NpCfaSendPktToHw (INT2 i2Port, UINT4 u4Size)
{
    struct sockaddr_in  ToAddr;
    INT4                i4Bytes;
    tHwPortInfo         HwPortInfo;
    tHwInfo             HwInfo;
    UINT1              *pu1TxBuf = NULL;
    UINT4               u4StackingModel = ISS_STACKING_MODEL_NONE;

    u4StackingModel = ISS_GET_STACKING_MODEL ();
    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);
    MEMSET (&HwInfo, 0, sizeof (tHwInfo));
    /* In Dual Unit Stacking Model,Packet Tx can happen over Ports of
       both Self and Peer Nodes */
    if (u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL)
    {
        /* If Port is part of Peer Unit,Send the pkt to Peer CPU
           through IPC Mechanism */
#ifdef RM_WANTED
        /* Packets tranmitted on the connecting port from CPU are
           RM Sync up Messages . They have to be sent to Peer CPU */
        if (HwPortInfo.au4ConnectingPortIfIndex[0] == (UINT4) i2Port)
        {
            HwInfo.u4MessageType = RM_SYNC_PKT;
            HwInfo.u4IfIndex = (UINT4) i2Port;
            HwInfo.u4PktSize = u4Size;

            pu1TxBuf = MemAllocMemBlk (gPktBufPoolId);
            if (pu1TxBuf == NULL)
            {
                CRTTRACE1
                    ("NpCfaSendPktToHw: Memory allocation for Tx Buf failed\n");
                return CFA_FAILURE;
            }
            MEMSET (pu1TxBuf, 0,
                    (CFA_MAX_DRIVER_MTU + CFA_MAX_FRAME_HEADER_SIZE));
            MEMCPY (pu1TxBuf, &gau1TxBuf[4], u4Size);

            HwInfo.uHwMsgInfo.HwGenMsgInfo.pu1Data = pu1TxBuf;
            if (FsCfaHwSendIPCMsg (&HwInfo) == FNP_SUCCESS)
            {
                MemReleaseMemBlock (gPktBufPoolId, pu1TxBuf);
                return CFA_SUCCESS;
            }
            MemReleaseMemBlock (gPktBufPoolId, pu1TxBuf);
            return CFA_FAILURE;
        }
#endif
    }
    /* check for remote unit port should be done if the 
     * stacking model is ISS_CTRL_PLANE_STACKING_MODEL/ISS_DISS_STACKING_MODEL
     * and if the out going port is not a stacking port
     */

    if (((u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL) ||
         (u4StackingModel == ISS_DISS_STACKING_MODEL)) &&
        (i2Port != (INT2) HwPortInfo.au4ConnectingPortIfIndex[0]))
    {
        /* Check if the outgoing port is a remote unit port */
        if ((!((i2Port >= (INT2) HwPortInfo.u4StartIfIndex) &&
               (i2Port <= (INT2) HwPortInfo.u4EndIfIndex))) &&
            (i2Port <= (INT2) SYS_DEF_MAX_PHYSICAL_INTERFACES))
        {
            pu1TxBuf = MemAllocMemBlk (gPktBufPoolId);
            if (pu1TxBuf == NULL)
            {
                CRTTRACE1
                    ("NpCfaSendPktToHw: Memory allocation for Tx Buf failed\n");
                return CFA_FAILURE;
            }
            MEMSET (pu1TxBuf, 0,
                    (CFA_MAX_DRIVER_MTU + CFA_MAX_FRAME_HEADER_SIZE));
            MEMCPY (pu1TxBuf, &gau1TxBuf[4], u4Size);

            /* i2Port is present in the remote unit. so send the packet to remote unit. */
            if (CfaNpSimPostPktToStandby (pu1TxBuf, i2Port, u4Size) ==
                CFA_FAILURE)
            {
                CRTTRACE1
                    ("NpCfaSendPktToHw:CfaNpSimPostPktToStandby send failed \n");
                MemReleaseMemBlock (gPktBufPoolId, pu1TxBuf);
                return CFA_FAILURE;
            }
            MemReleaseMemBlock (gPktBufPoolId, pu1TxBuf);
            return CFA_SUCCESS;
        }
    }

    MEMCPY ((CHR1 *) & gau1TxBuf[0], &gu2NslSw, 2);
    /*i2Port++; */
    MEMCPY ((CHR1 *) & gau1TxBuf[2], &i2Port, 2);

    /* Send to hardware */
    ToAddr.sin_family = AF_INET;
    ToAddr.sin_port = htons (gu2NslBaseNpSimPort + gu2NslSw);
    MEMCPY ((CHR1 *) (&(ToAddr.sin_addr.s_addr)),
            (CHR1 *) & (gu4NslPktServerIpAddr), 4);

    i4Bytes = sendto (gNslSock, (void *) gau1TxBuf, (u4Size + 4),
                      0, (struct sockaddr *) &ToAddr, sizeof (ToAddr));
    if (i4Bytes != (INT4) (u4Size + 4))
    {
        return (CFA_FAILURE);
    }
    return CFA_SUCCESS;
}

/*
CfaGddEthWrite (UINT1 *pu1Buf, UINT4 u4IfIndex, UINT4 u4Size)
*/
#define VLAN_PROTO_OFFSET 12
#define VLAN_PROTO_ID     0x8100
INT4
CfaHwL3VlanIntfWrite (UINT1 *pu1Buf, UINT4 u4Size, tCfaVlanInfo VlanInfo)
{
    INT2                i2Port;
    UINT2               u2Protocol;
    UINT1               bResult = OSIX_FALSE;
    UINT1               u1IfType = 0;
    UINT2               u2PhyPort = 0;
    UINT4               u4Total = 0;

    /* Get the protocol id (12th and 13 byte) from the frame */
    MEMCPY (&u2Protocol, &pu1Buf[VLAN_PROTO_OFFSET], sizeof (UINT2));
    u2Protocol = OSIX_NTOHS (u2Protocol);

    if (VlanInfo.u2PktType == CFA_NP_KNOWN_UCAST_PKT)
    {
        /* Output port is known, hence tx on that port. */
        i2Port = (INT2) VlanInfo.unPortInfo.TxUcastPort.u2TxPort;

        if (u2Protocol != VLAN_PROTO_ID)
        {
            /* Untagged frame. */
            if (VlanInfo.unPortInfo.TxUcastPort.u1Tag == CFA_NP_TAGGED)
            {
                /* It is a tagged port hence tag it. */
                MEMCPY (gau1TxBuf + 4, pu1Buf, 12);
                NpCfaVlanTagFrame (gau1TxBuf + 16, VlanInfo.u2VlanId,    /*u1Priority */
                                   7);
                MEMCPY (gau1TxBuf + 20, pu1Buf + 12, u4Size - 12);
                u4Size += 4;
            }
            else
            {
                MEMCPY ((UINT1 *) (&(gau1TxBuf[4])), pu1Buf, u4Size);
            }
        }
        else
        {
            /* Tagged frame. */
            if (VlanInfo.unPortInfo.TxUcastPort.u1Tag == CFA_NP_UNTAGGED)
            {
                /* It is a untagged port hence untag it. */
                MEMCPY (gau1TxBuf + 4, pu1Buf, 12);
                MEMCPY (gau1TxBuf + 16, pu1Buf + 16, u4Size - 16);
                u4Size -= 4;
            }
            else
            {
                MEMCPY ((UINT1 *) (&(gau1TxBuf[4])), pu1Buf, u4Size);
            }
        }
        if ((i2Port >= CFA_MIN_PSW_IF_INDEX)
            && (i2Port <= CFA_MAX_PSW_IF_INDEX))
        {
            if (CfaHwL3PwVlanIntfWrite (u4Size, i2Port) == CFA_SUCCESS)
            {
                return FNP_SUCCESS;
            }
            else
            {
                return FNP_FAILURE;
            }
        }

        /* send the packet to the hw. */
        if (NpCfaSendPktToHw (i2Port, u4Size) == CFA_FAILURE)
        {
            return FNP_FAILURE;
        }

        return (FNP_SUCCESS);
    }

    if (u2Protocol != VLAN_PROTO_ID)
    {
        /* Untagged packet. */
        if (u4Size > 1514)
        {
            return (FNP_FAILURE);
        }

        /* Flood the packet on all vlan member ports. */
        u4Total = u4Size;
        for (i2Port = 1; i2Port <= CFA_MAX_PSW_IF_INDEX; i2Port++)
        {
            u4Size = u4Total;
            OSIX_BITLIST_IS_BIT_SET ((*VlanInfo.unPortInfo.TxPorts.pTagPorts),
                                     i2Port, sizeof (tPortList), bResult);
            if (bResult == OSIX_TRUE)
            {
                /* It is a tagged port hence tag it. */
                /*changes regarding pw as an outgoing port over npsim */

                MEMCPY (gau1TxBuf + 4, pu1Buf, 12);
                NpCfaVlanTagFrame (gau1TxBuf + 16, VlanInfo.u2VlanId,    /*u1Priority */
                                   7);
                MEMCPY (gau1TxBuf + 20, pu1Buf + 12, u4Size - 12);
                u4Size += 4;

            }
            else
            {
                OSIX_BITLIST_IS_BIT_SET ((*VlanInfo.unPortInfo.TxPorts.
                                          pUnTagPorts), i2Port,
                                         sizeof (tPortList), bResult);
                if (bResult == OSIX_TRUE)
                {
                    MEMCPY ((UINT1 *) (&(gau1TxBuf[4])), pu1Buf, u4Size);

                }
                else
                {
                    /* Neither a tagged port nor an untagged member port
                     * of vlan. */
                    continue;
                }
            }

            u2PhyPort = i2Port;
            CfaGetIfType (i2Port, &u1IfType);
            if (u1IfType == CFA_LAGG)
            {
                /* Get the physical port corresponding to the port-channel */
                if (L2IwfGetPotentialTxPortForAgg (i2Port, &u2PhyPort)
                    == L2IWF_FAILURE)
                {
                    return FNP_FAILURE;
                }
            }

            /*If it is a pw member port then it will give it to MPLS for
               adding the MPLS Lable / MPLS Header and outer ethernet */

            if ((i2Port >= CFA_MIN_PSW_IF_INDEX)
                && (i2Port <= CFA_MAX_PSW_IF_INDEX))
            {
                if (CfaHwL3PwVlanIntfWrite (u4Size, i2Port) == CFA_SUCCESS)
                {
                    return FNP_SUCCESS;
                }
                else
                {
                    return FNP_FAILURE;
                }

            }
            if (NpCfaSendPktToHw (u2PhyPort, u4Size) == CFA_FAILURE)
            {
                return FNP_FAILURE;
            }

            /* Aviod looping for other logical interfaces present
             * after (Physical + port chanel) > && < (Min pseudo wire index) */

            if (i2Port == BRG_MAX_PHY_PLUS_LOG_PORTS)
            {
                i2Port = (CFA_MIN_PSW_IF_INDEX) - 1;
            }

        }
    }
    else
    {
        /* Tagged packet. */
        if (u4Size > 1518)
        {
            return (FNP_FAILURE);
        }

        /* Flood the packet on all vlan member ports. */
        u4Total = u4Size;
        /*changes regarding pw as an outgoing port over npsim */

        for (i2Port = 1; i2Port < CFA_MAX_PSW_IF_INDEX; i2Port++)
        {
            u4Size = u4Total;
            OSIX_BITLIST_IS_BIT_SET ((*VlanInfo.unPortInfo.TxPorts.pUnTagPorts),
                                     i2Port, sizeof (tPortList), bResult);
            if (bResult == OSIX_TRUE)
            {
                /* It is a untagged port hence untag it. */
                MEMCPY (gau1TxBuf + 4, pu1Buf, 12);
                MEMCPY (gau1TxBuf + 16, pu1Buf + 16, u4Size - 16);
                u4Size -= 4;

            }
            else
            {
                OSIX_BITLIST_IS_BIT_SET ((*VlanInfo.unPortInfo.TxPorts.
                                          pTagPorts), i2Port,
                                         sizeof (tPortList), bResult);
                if (bResult == OSIX_TRUE)
                {
                    MEMCPY ((UINT1 *) (&(gau1TxBuf[4])), pu1Buf, u4Size);

                }
                else
                {
                    /* Neither a tagged port nor an untagged member port
                     * of vlan. Hence go for next port.*/
                    continue;
                }
            }

            u2PhyPort = i2Port;
            CfaGetIfType (i2Port, &u1IfType);
            if (u1IfType == CFA_LAGG)
            {
                /* Get the physical port corresponding to the port-channel */
                if (L2IwfGetPotentialTxPortForAgg (i2Port, &u2PhyPort)
                    == L2IWF_FAILURE)
                {
                    return FNP_FAILURE;
                }
            }

            /*If it is a pw member port then it will give it to MPLS for
               adding the MPLS Lable / MPLS Header and outer ethernet */

            if ((i2Port >= CFA_MIN_PSW_IF_INDEX)
                && (i2Port <= CFA_MAX_PSW_IF_INDEX))
            {
                if (CfaHwL3PwVlanIntfWrite (u4Size, i2Port) == CFA_SUCCESS)
                {
                    return FNP_SUCCESS;
                }
                else
                {
                    return FNP_FAILURE;
                }

            }

            if (NpCfaSendPktToHw (u2PhyPort, u4Size) == CFA_FAILURE)
            {
                return FNP_FAILURE;
            }
            /* Aviod looping for other logical interfaces present
             * after (Physical + port chanel) > && < (Min pseudo wire index) */

            if (i2Port == BRG_MAX_PHY_PLUS_LOG_PORTS)
            {
                i2Port = (CFA_MIN_PSW_IF_INDEX) - 1;
            }

        }
    }

    return (FNP_SUCCESS);
}

/* yanked from bcmx/cfanp.c */
#define VLAN_TAG_PRIORITY_SHIFT 13
#define VLAN_ID_MASK            0x0FFF
#define VLAN_TYPE_OR_LEN_SIZE   2
#define VLAN_TAG_SIZE           2
PRIVATE VOID
NpCfaVlanTagFrame (UINT1 *pBuf, tVlanId VlanId, UINT1 u1Priority)
{
    UINT2               u2Tag = 0;
    UINT2               u2Protocol = 0;

    u2Protocol = OSIX_HTONS (VLAN_PROTO_ID);
    u2Tag = (UINT2) u1Priority;
    u2Tag = (UINT2) (u2Tag << VLAN_TAG_PRIORITY_SHIFT);
    u2Tag = (UINT2) (u2Tag | (VlanId & VLAN_ID_MASK));
    u2Tag = (UINT2) (OSIX_HTONS (u2Tag));
    MEMCPY (&pBuf[0], (UINT1 *) &u2Protocol, VLAN_TYPE_OR_LEN_SIZE);
    MEMCPY (&pBuf[VLAN_TYPE_OR_LEN_SIZE], (UINT1 *) &u2Tag, VLAN_TAG_SIZE);
    /* TODO: change to PTR_FETCH */
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddGetLnxIntfnameForPort
 *
 *    Description          : This function Returns LinuxEth Name Based on Slot
 *                           Referred.
 *    Input(s)            :  u4IfIndex - Interface index
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure
 *
 *    Global Variables Modified : gaIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : EthName if Slot is found Else NULL
 *
 *****************************************************************************/
UINT1              *
CfaGddGetLnxIntfnameForPort (UINT2 u2Index)
{
    INT4                i4Index;
    UINT1               au1Temp[CFA_MAX_PORT_NAME_LENGTH];

    if (u2Index == 0)
    {
        return NULL;
    }

    for (i4Index = 0; i4Index < SYS_DEF_MAX_PHYSICAL_INTERFACES; i4Index++)
    {
        MEMSET (au1Temp, 0, CFA_MAX_PORT_NAME_LENGTH);

        STRCPY (au1Temp, au1IntMapTable[i4Index]);

        if (MEMCMP (CFA_GDD_PORT_NAME (u2Index), au1Temp,
                    STRLEN (CFA_GDD_PORT_NAME (u2Index))) == 0)
        {
            return au1IntMapTable[i4Index][1];
        }
    }
    return NULL;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddSetLnxIntfnameForPort
 *
 *    Description          : This function sets Interface Name for the interface
 *                           index.
 *    Input(s)            :  u4IfIndex - Interface index
 *                           pu1IfName - Interface name
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure
 *
 *    Global Variables Modified : gaIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : NULL
 *
 *****************************************************************************/
VOID        
CfaGddSetLnxIntfnameForPort (UINT4 u4Index, UINT1 * pu1IfName)
{

    if (u4Index == 0)
    {
        return;
    }

    STRNCPY (au1IntMapTable[u4Index], CFA_GDD_PORT_NAME (u4Index),
            CFA_MAX_PORT_NAME_LENGTH); 

    STRNCPY (au1IntMapTable[u4Index][1], pu1IfName, CFA_MAX_PORT_NAME_LENGTH);
    CfaSetIfName (u4Index, pu1IfName);
    return;
}
#ifdef MBSM_WANTED
/*****************************************************************************
 *    Function Name        : CfaMbsmGddDeInit
 *    Description         : This function performs the shutdown of
 *                the Generic Device Driver Module of CFA. This
 *                shutdown routine should be called
 *                before the protocols are informed about the shutdown.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : FdTable
 *
 *    Global Variables Modified : FdTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
CfaMbsmGddDeInit (tMbsmSlotInfo * pSlotInfo)
{
    /* Re-initialize the Slot info paramaters for the given slot */
    CfaCfaMbsmNpSlotDeInit (pSlotInfo);

    return CFA_SUCCESS;
}

/*****************************************************************************
 *    Function Name       : CfaMbsmGddInit
 *    Description         : This function performs the initialisation of
 *                          the  Device Driver Module of CFA. This routine
 *                          1. creates the Queue for reception of packets from
 *                             driver
 *                          2. creates the memory pool for the driver messages
 *                          3. initializes the driver to cfa mapping tables
 *    Input(s)            : None.
 *    Output(s)           : None.
 *
 *    Global Variables Referred : _devices, _ndevices
 *    Global Variables Modified : gFsDrvMemPoolId, gaCfaNpIndexMap
 *                                gaCfaNpDevMap
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if initialisation succeeds,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

PUBLIC INT4
CfaMbsmGddInit (tMbsmSlotInfo * pSlotInfo)
{
    /* Update the Driver to cfa mapping tables for the given slot.
     */
    CfaCfaMbsmNpSlotInit (pSlotInfo);

    return CFA_SUCCESS;
}

#endif /* MBSM_WANTED */

/*****************************************************************************
 *
 *    Function Name       :CfaGddTxPktOnVlanMemberPorts 
 *
 *    Description         :This routine transmits the given Frame on the member
 *                         ports of the given Vlan.
 *
 *    Input(s)            : u4L2ContextId - L2 Context Id to which VlanId is 
 *                                          associated
 *                        : pu1DataBuf  - Pointer to the frame buffer 
 *                          u4PktSize   - Size of the frame 
 *                          VlanId      - Vlan on which the frame is to be 
 *                                        transmitted
 *                          bBcast      - Whether this frame is to be 
 *                                        broadcasted  
 *
 *    Output(s)           : None
 *
 *    Global Variables Referred : None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/ CFA_FAILURE.
 *****************************************************************************/

INT4
CfaGddTxPktOnVlanMemberPortsInCxt (UINT4 u4L2ContextId,
                                   UINT1 *pu1DataBuf, tVlanId VlanId,
                                   BOOL1 bBcast, UINT4 u4PktSize)
{
    tPortList          *pTagPorts = NULL;
    tPortList          *pUnTagPorts = NULL;
    tMacAddr            DestAddr;
    tCfaVlanInfo        VlanInfo;
    UINT4               u4BridgeMode;
#ifdef NPAPI_WANTED
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2EtherType = 0;
    UINT1               u1PortFlag;
#endif
    UINT4               u4OutPort;
    BOOL1               bIsTag;
    INT4                i4RetVal;
    UINT1              *pu1TempBuf = NULL;
    UINT1               TempDataBuf[CFA_ENET_MIN_UNTAGGED_FRAME_SIZE];
    UINT4               u4MinFrameMtu = 0;
    INT4                i4RetStat = CFA_SUCCESS;
    UINT1               u1PaddingDone = CFA_FALSE;
    MEMCPY (DestAddr, pu1DataBuf, CFA_ENET_ADDR_LEN);

    MEMSET (TempDataBuf, 0, sizeof (TempDataBuf));
    VlanInfo.u2VlanId = VlanId;
#ifdef NPAPI_WANTED
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
#ifdef MBSM_WANTED
        /* If the packet belongs to STACK IVR, then send the
         * packet via stack port using ATP*/
        if (VlanId == CFA_DEFAULT_STACK_VLAN_ID)
        {
            if (CfaMbsmNpTxOnStackInterface (pu1DataBuf, u4PktSize) ==
                FNP_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaUtilTxPktOnVlanMemberPorts - "
                         "Unable to tx on stack interface.\n");
                return CFA_FAILURE;
            }
            return CFA_SUCCESS;
        }
#endif
    }
#endif

    pTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        return CFA_FAILURE;
    }

    pUnTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pUnTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        return CFA_FAILURE;
    }

    MEMSET (*pTagPorts, 0, sizeof (tPortList));
    MEMSET (*pUnTagPorts, 0, sizeof (tPortList));
    i4RetVal = VlanIvrGetTxPortOrPortListInCxt (u4L2ContextId,
                                                DestAddr, VlanId, bBcast,
                                                &u4OutPort, &bIsTag, *pTagPorts,
                                                *pUnTagPorts);

    if (i4RetVal == VLAN_FORWARD)
    {
        /*ASSUMPTION: The Packet reached here will be untagged always.
         * Since bcoz this thread is called only for the outgoing packet
         * from IP.*/

        u4MinFrameMtu = CFA_ENET_MIN_UNTAGGED_FRAME_SIZE;

        if (u4PktSize < u4MinFrameMtu)
        {
            pu1TempBuf = pu1DataBuf;
            MEMCPY (TempDataBuf, pu1DataBuf, u4PktSize);
            pu1DataBuf = TempDataBuf;
            u4PktSize = u4MinFrameMtu;
            u1PaddingDone = CFA_TRUE;
        }

        CfaTxFillVlanInfo (DestAddr, u4OutPort,
                           bIsTag, *pTagPorts, *pUnTagPorts, &VlanInfo);
        if (L2IwfGetBridgeMode (L2IWF_DEFAULT_CONTEXT, &u4BridgeMode) ==
            L2IWF_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaGddWrite - "
                     "L2IWF-GetBridgeMode -  FAILURE.\n");
            i4RetStat = CFA_FAILURE;
        }
        else
        {
#ifdef NPAPI_WANTED
            if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
            {
                if ((u4BridgeMode == L2IWF_CUSTOMER_BRIDGE_MODE) ||
                    (u4BridgeMode == L2IWF_PROVIDER_BRIDGE_MODE))
                {
                    if (CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo)
                        != FNP_SUCCESS)
                    {
                        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                 "Error in CfaGddWrite - "
                                 "Unsuccessful Driver Write -  FAILURE.\n");
                        i4RetStat = CFA_FAILURE;
                    }
                }
                else if ((u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE) ||
                         (u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE))
                {
                    if (u4OutPort != VLAN_INVALID_PORT)
                    {
                        if (CfaHwL3VlanIntfWrite
                            (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                        {
                            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                     "Error in CfaGddWrite - "
                                     "Unsuccessful Driver Write -  FAILURE.\n");
                            i4RetStat = CFA_FAILURE;
                        }
                    }
                    else
                    {
                        for (u2ByteInd = 0; u2ByteInd < BRG_PORT_LIST_SIZE;
                             u2ByteInd++)
                        {
                            if ((*pTagPorts)[u2ByteInd] == 0)
                            {
                                continue;
                            }

                            u1PortFlag = (*pTagPorts)[u2ByteInd];

                            for (u2BitIndex = 0;
                                 ((u2BitIndex < BITS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
                            {
                                if ((u1PortFlag & 0x80) != 0)
                                {
                                    VlanInfo.u2PktType = CFA_NP_KNOWN_UCAST_PKT;

                                    VlanInfo.unPortInfo.TxUcastPort.u2TxPort =
                                        (UINT2) ((u2ByteInd * BITS_PER_BYTE) +
                                                 u2BitIndex + 1);

                                    u4OutPort =
                                        VlanInfo.unPortInfo.TxUcastPort.
                                        u2TxPort;

#ifdef VLAN_WANTED
                                    VlanGetPortEtherType (u4OutPort,
                                                          &u2EtherType);
#endif
                                    VlanInfo.unPortInfo.TxUcastPort.
                                        u2EtherType = u2EtherType;

                                    VlanInfo.unPortInfo.TxUcastPort.u1Tag =
                                        CFA_NP_TAGGED;

                                    CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize,
                                                          VlanInfo);
                                }
                                u1PortFlag = (UINT1) (u1PortFlag << 1);
                            }
                        }
                        MEMSET (VlanInfo.unPortInfo.TxPorts.pTagPorts, 0,
                                sizeof (tPortList));
                        VlanInfo.u2PktType = CFA_NP_UNKNOWN_UCAST_PKT;
                        /* 
                         * In 802.1ad Bridges, for untagged ports, the tag ethertype 
                         * need not be filled in based on the port ethertype. 
                         * Hence calling the API for packet transmission on L3 
                         * interfaces directly.
                         */
                        if (CfaHwL3VlanIntfWrite
                            (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                        {
                            i4RetStat = CFA_FAILURE;
                        }
                    }
                }
            }
#else
            if (CfaL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo) !=
                CFA_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddWrite - "
                         "Unsuccessful Driver Write -  FAILURE.\n");
                i4RetStat = CFA_FAILURE;

            }
#endif
        }
        if (u1PaddingDone == CFA_TRUE)
        {
            pu1DataBuf = pu1TempBuf;
        }
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pUnTagPorts));
        return i4RetStat;
    }

    FsUtilReleaseBitList ((UINT1 *) pTagPorts);
    FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pTagPorts));
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pUnTagPorts));
    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name        : CfaNpSimPostHBPktToICCHQ
 *
 *    Description          : This function posts the HB packet to ICCH Queue
 *
 *    Input(s)             : pBuf - Cru buffer which contains the HB packet 
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.                                 
 *
 *    Global Variables Modified : None.                                
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : None.
 *
 *****************************************************************************/
VOID
CfaNpSimPostHBPktToICCHQ (tCRU_BUF_CHAIN_HEADER * pBuf, 
                                    UINT4 u4IfIndex, UINT4 u4PktLen)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PktLen);
    /* Post the HB packet to ICCH */
    CfaHbIcchPostHBPktToIcch (pBuf);
    UNUSED_PARAM (pBuf);
    return;
}

#ifdef CFA_INTERRUPT_MODE
/*****************************************************************************
 *
 *    Function Name       :CfaPktHdlrTask
 *
 *    Description         :This routine creates the packet handler task for NPSIm
 *
 *    Input(s)            : NONE
 *    Output(s)           : None
 *
 *    Returns            : NONE
 *****************************************************************************/
VOID
CfaPktHdlrTask (VOID)
{
    UINT4               u4Event;

    /* Create the timer for polling the Driver for packet arrival */
    if (TmrCreateTimerList
        ((CONST UINT1 *) "tNpPkt", CFA_PKT_HDLR_TIMER_EVENT, NULL,
         (tTimerListId *) & (gNpSimPktHdlrTimerLst)) != TMR_SUCCESS)
    {
        return;
    }
    if (OsixTskIdSelf (&gu4NpSimPktHdlrTaskId) == OSIX_FAILURE)
    {
        return;
    }
    /* start the timer with 10 millsec */
    CfaNpSimPktHdlrUtilTmrRestart (gNpSimPktHdlrTimerLst, &gNpSimPktHdlrTmrBlk,
                                   10);

    while (1)
    {
        OsixEvtRecv (gu4NpSimPktHdlrTaskId,
                     CFA_PKT_HDLR_TIMER_EVENT,
                     (OSIX_WAIT | OSIX_EV_ANY), (UINT4 *) &(u4Event));
        if (u4Event & CFA_PKT_HDLR_TIMER_EVENT)
        {

            if (gu1CfaInitialised == TRUE)
            {
                CfaNpSimProcessPkt ();
            }
            /* Restart the timer with 10 milliseconds */
            CfaNpSimPktHdlrUtilTmrRestart (gNpSimPktHdlrTimerLst,
                                           &gNpSimPktHdlrTmrBlk, 10);
        }
    }

    return;
}

/*****************************************************************************
 *
 *    Function Name       :CfaNpSimPktHdlrUtilTmrRestart
 *
 *    Description         :This routine starts the 10 ms polling timer for packet 
 *                         handling packet reception in the NPSIM pkt handler task
 *
 *    Input(s)            : TimerLst - Timer List
 *                        : pTmrBlk - Timer block
 *                        : u4Time - Time (10 milli seconds)
 *    Output(s)           : None
 *
 *    Returns            : NONE
 *****************************************************************************/
VOID
CfaNpSimPktHdlrUtilTmrRestart (tTimerListId TimerLst, tTmrBlk * pTmrBlk,
                               UINT4 u4Time)
{
    /* Restart the timer with 10 milliseconds */
    if (TmrStart (TimerLst, pTmrBlk, 1, 0, u4Time) == TMR_FAILURE)
    {
        return;
    }
}

/*****************************************************************************
 *
 *    Function Name       :CfaNpSimProcessPkt
 *
 *    Description         :This routine handles the packet arrival in the NPSIM socket
 *                        :If the packet received is an IPC message from the peer, then 
 *                         the packet will be consumed in the context of the current task.
 *                         Or else the packet will be posted to the CFA Queue and an event 
 *                         will be posted to the CFA Task.
 *
 *    Input(s)            : None                    
 *    Output(s)           : None
 *
 *    Returns            : NONE
 *****************************************************************************/

INT4
CfaNpSimProcessPkt (VOID)
{
    struct sockaddr_in  FromAddr;
    UINT4               u4IfIndex;
    UINT4               u4StackingModel = ISS_STACKING_MODEL_NONE;
    INT4                i4RetVal, i4AddrLen;
    UINT1              *pData;
    tMacAddr            DestAddr;
    fd_set              readFds;
    UINT2               u2RxPort, u2RxSw;
    INT2                i2PktLen = 0;
    UINT1               u1IsPktHandled = OSIX_FALSE;
    struct timeval      tv;

#ifdef TCPDUMP_WANTED
    typedef             u_int (*if_printer) (const struct pcap_pkthdr *,
                                             const u_char *);
    struct print_info
    {
        if_printer          printer;
    };
    struct pcap_pkthdr  h;
    struct print_info   user;
    u_int               ether_if_print (const struct pcap_pkthdr *,
                                        const u_char *);
#endif

    while (1)
    {
        FD_ZERO (&readFds);
        FD_SET (gNslSock, &readFds);
        FD_SET (gRpcSock, &readFds);

        gMaxFd = gNslSock;
        if (gRpcSock > gMaxFd)
            gMaxFd = gRpcSock;

        tv.tv_sec = 0;
        tv.tv_usec = 0;
        i4RetVal = select ((gMaxFd + 1), &readFds, NULL, NULL, &tv);
        if ((i4RetVal < 0) ||
            (!(FD_ISSET (gNslSock, &readFds))
             && !(FD_ISSET (gRpcSock, &readFds))))
        {
            break;                /* No data to read or some error */
        }

        if (FD_ISSET (gRpcSock, &readFds))
        {
            NslDoRpc ();
            continue;
        }
        /* Socket has data. Get buffer if needed to read packet in to. */

        /* We don't check return value here. We assume buffer is linear. */

        MEMSET (&FromAddr, 0, sizeof (FromAddr));
        i4AddrLen = sizeof (FromAddr);

        pData = &gau1PktBuffer[0];
        MEMSET (pData, 0, (CFA_MAX_DRIVER_MTU + CFA_MAX_FRAME_HEADER_SIZE));
        if ((i2PktLen = recvfrom (gNslSock, (VOID *) pData, CFA_MAX_DRIVER_MTU +
                                  CFA_MAX_FRAME_HEADER_SIZE + 4, 0,
                                  (struct sockaddr *) &FromAddr,
                                  (socklen_t *) & i4AddrLen)) <= 0)
        {
            break;
        }

#ifdef TCPDUMP_WANTED
        if (gu4DoTcpDump)
        {
            if (((TDArgs.u4Flags & CFA_TCPDUMP_c) == 0) || TDArgs.u4Count--)
            {
                gettimeofday (&h.ts, NULL);
                h.caplen = h.len = i2PktLen - 4;
                user.printer = ether_if_print;
                Xflag = xflag = eflag = 1;
                print_packet ((u_char *) & user, &h, pData + 4);
            }
            if ((TDArgs.u4Flags & CFA_TCPDUMP_c) && TDArgs.u4Count == 0)
                gu4DoTcpDump = 0;
        }
#endif

        if (i2PktLen == 64)
        {
            TTRACE1 ("ping pkt\n");
        }
        if (i2PktLen > 1518)
        {
            TTRACE1 ("GDDNSL: pktsize %d\n", i2PktLen);
        }

        MEMCPY (&u2RxSw, pData, 2);
        MEMCPY (&u2RxPort, (pData + 2), 2);

        i2PktLen -= 4;

        if (IS_NSL_DEBUG_PKT_EVENT (gu4NslDebug) &&
            (!IS_NSL_DEBUG_PKT_DUMP (gu4NslDebug)))
        {
            u4IfIndex = (UINT4) (gaNslPort[u2RxPort - 1].u4IfIndex);
            TTRACE1 ("NSL: PktEv: Rx sw: %d, port: %d, ifIndex: %d, Size: %d\n",
                     (int) u2RxSw, (int) (u2RxPort - 1), (int) u4IfIndex,
                     (int) i2PktLen);
        }

        if ((u2RxPort == 0) && (u2RxSw == 0))
        {
            /* this is error; control messages are handled only at the
             * packet server
             */
            NslHandlePktSrvCtrlMsg (pData);
            continue;

        }

        if ((u2RxPort > NSL_MAX_PORTS) || (u2RxSw != gu2NslSw))
        {
            continue;
        }

        pData += 4;

        u2RxPort--;
        u4IfIndex = (UINT4) (gaNslPort[u2RxPort].u4IfIndex);

        if (gaNslPort[u2RxPort].u1PortState == NSL_DOWN)
        {
            if (IS_NSL_DEBUG_PKT_EVENT (gu4NslDebug))
            {
                TTRACE1 ("    i/f down; discarding packet\n");
            }
            continue;
        }

        u4StackingModel = ISS_GET_STACKING_MODEL ();

        /* To  Check whether Packet is arrived from peer CPU
         * with customized Header.If so extract the
         * header and invoke the common Notification function to
         * process the Packet */
        if ((u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL) ||
            (u4StackingModel == ISS_DISS_STACKING_MODEL))
        {

            MEMCPY (DestAddr, pData, sizeof (tMacAddr));
            if (CfaNpSimProcessIPCMsgFromRemoteCPU (pData, &u1IsPktHandled) ==
                FNP_FAILURE)
            {
                TTRACE1 ("Failure in processing the IPC message\n");
            }
            if (u1IsPktHandled == OSIX_TRUE)
            {
                /* This is custom IPC message and not intended for CFA
                 * hence continue
                 */
                continue;
            }
            /* In DISS stack model, centralised protocol packets, ARP and packets to switch Mac
             * are sent to to active for processing
             * Other packets are processed in standby (Slave)  Node.
             * For NON=DISS stack model, Packet processing should not be done in the standby
             * so send the packet to the Active
             * Exception to this is RM packets received on the stacking interface */
            else if ((CfaGetRetrieveNodeState () == RM_STANDBY) &&
                     (CfaCheckCentralizedProto (DestAddr) == CFA_TRUE))
            {
                CfaNpSimPostPktToActive (pData, u4IfIndex, i2PktLen);
            }
            else
            {
                CfaNpSimProcessAndPostPktToCfaQ (pData, u4IfIndex, i2PktLen);
            }
        }
        /*  Process and post Remote packet if stacking model is None */
        else if (u4StackingModel == ISS_STACKING_MODEL_NONE)
        {
            CfaNpSimProcessAndPostPktToCfaQ (pData, u4IfIndex, i2PktLen);
        }
    }

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name       :CfaNpSimProcessAndPostPktToCfaQ
 *
 *    Description         :This routine handles posting of the packet to the CFA Queue 
 *                         and sending the packet arrival event to the CFA Task.
 *
 *    Input(s)            : pu1Buf - packet buffer
 *                        : u4IfIndex - interface on which packet is rx
 *                        : u4PktLen - packet length
 *    Output(s)           : None
 *
 *    Returns            : NONE
 *****************************************************************************/
VOID
CfaNpSimProcessAndPostPktToCfaQ (UINT1 *pu1Buf, UINT4 u4IfIndex, UINT4 u4PktLen)
{
    tEnetV2Header      *pHdr;
    tEnetSnapHeader    *pSnapHdr;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
#ifdef IP6_WANTED
    tMacAddr            DestAddr;
    UINT1               au1DstMac[CFA_ENET_ADDR_LEN] =
        { 0x33, 0x33, 0xff, 0x00, 0x00, 0x01 };
#endif
    UINT2               u2Protocol = 0, u2LenOrType;
    INT2                i2PktLen, i2HdrLen, i2IpPktLen = 0;
#if defined PIM_NP_HELLO_WANTED 
    UINT2               u2VlanProt = 0;
    UINT2               u2OffSet = 0;
    UINT1		*pu1Data = NULL;
    UINT1		au1Data[PIMSM_IP_NEXTHOP_MTU];

    MEMSET (au1Data, 0, PIMSM_IP_NEXTHOP_MTU);
    pu1Data = au1Data;
#endif
#ifdef IP6_WANTED
    /*To Avoid All CruBuffer is allocated with the neighbor
     * Solicitation message sent by the Peer Node*/
    MEMCPY (DestAddr, pu1Buf, CFA_ENET_ADDR_LEN);
    if ((MEMCMP (DestAddr, au1DstMac, CFA_ENET_ADDR_LEN) == 0) &&
        (MEM_FREE_POOL_UNIT_COUNT (1)) < 200)
    {
        TTRACE1 ("Rate Limiting For IPV6 Solicitation\n");
        return;
    }
#endif
    i2PktLen = u4PktLen;
    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktLen, 0)) == NULL)
    {
        TTRACE1 ("Unable to allocate memory for packet reception\n");
        return;
    }
    CRU_BUF_Copy_OverBufChain (pBuf, pu1Buf, 0, u4PktLen);
    /* check if the interface exists */

    pHdr = (tEnetV2Header *) (pu1Buf);
    u2LenOrType = OSIX_NTOHS (pHdr->u2LenOrType);
    if (CFA_ENET_IS_TYPE (u2LenOrType))
    {
        u2Protocol = u2LenOrType;
        i2HdrLen = CFA_ENET_V2_HEADER_SIZE;
    }
    else
    {
        pSnapHdr = (tEnetSnapHeader *) pu1Buf;
        i2HdrLen = CFA_ENET_SNAP_HEADER_SIZE;

        /* check for LLC control frame first - we expect only LLC in SNAP now */
        if (pSnapHdr->u1Control == CFA_LLC_CONTROL_UI)
        {
            /*check for presence of SNAP which may carry IP/ARP/RARP after LLC */
            if ((pSnapHdr->u1DstLSap == CFA_LLC_SNAP_SAP) ||
                (pSnapHdr->u1SrcLSap == CFA_LLC_SNAP_SAP) ||
                (pSnapHdr->u1Oui1 == 0x00) ||
                (pSnapHdr->u1Oui2 == 0x00) || (pSnapHdr->u1Oui3 == 0x00))
            {
                /* determine which protocol is being carried */
                u2Protocol = OSIX_NTOHS (pSnapHdr->u2ProtocolType);
            }                    /* end of SNAP framing */
        }                        /* end of LLC framing */
    }

    /* u2Protocol = 0xFF; */
    if (u2Protocol == CFA_ENET_IPV4)
    {
        i2IpPktLen = (INT2) *(INT2 *) (pu1Buf + i2HdrLen + IP_PKT_OFF_LEN);
        i2IpPktLen = OSIX_NTOHS (i2IpPktLen);
        u4PktLen = (UINT4) (i2IpPktLen + i2HdrLen);
    }
#ifdef IP6_WANTED
    else if (u2Protocol == CFA_ENET_IPV6)
    {
        i2IpPktLen = (INT2) *(INT2 *) (pu1Buf + i2HdrLen
                                       + IPV6_OFF_PAYLOAD_LEN);
        i2IpPktLen = OSIX_NTOHS (i2IpPktLen);
        u4PktLen = (UINT4) (i2IpPktLen + i2HdrLen + IPV6_HEADER_LEN);
    }
#endif /* IP6_WANTED */
    else
    {
        u4PktLen = i2PktLen;    /* leaving out the simulation header */
    }

    CFA_IF_SET_IN_OCTETS ((UINT2) u4IfIndex, u4PktLen);
#ifdef RMON_WANTED
    /* RMON code should process only if SW_FWD enabled.         */
    /* Update rmon tables. Return value doesn't matter.         */
    /* Routine should be called only for ethernet interfaces.   */
    /* We avoid checking because we support only ethernet.      */
    CfaIwfRmonUpdateTables (pu1Buf, (UINT2) u4IfIndex, u4PktLen);
#endif /* RMON_WANTED */

    /* insert received interface index into module data */
    pBuf->ModuleData.InterfaceId.u4IfIndex = u4IfIndex;

#if defined PIM_NP_HELLO_WANTED 
    /* Check for the presence of VLAN Tag */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2VlanProt,
                               CFA_VLAN_TAG_OFFSET, CFA_VLAN_PROTOCOL_SIZE);
    if (u2VlanProt == OSIX_HTONS (CFA_VLAN_PROTOCOL_ID))
    {
	u2OffSet = CFA_VLAN_TAGGED_HEADER_SIZE + CFA_ENET_TYPE_OR_LEN;
    }
    else /* router port */
    {
	u2OffSet = CFA_VLAN_TAG_OFFSET + CFA_ENET_TYPE_OR_LEN;
    }
    /* copy the received packet from IP header */
    CRU_BUF_Copy_FromBufChain (pBuf, pu1Data, u2OffSet, (u4PktLen - u2OffSet));

    /* PIM Hello Packets needs to be sent directly to PIM Hello task.
     * Below fields in the packet are compared to identify PIM Hello packet.
     * PIM Protocol Id - 103, PIM Hello Msg type - 32
     * PIM Hello Packet Destination Address
     * PIMv4 - 224.0.0.13, PIMv6 - ff02::d */
    if ((pu1Data[PIMV4_DEST_IP1_POS] == PIMV4_DEST_IP1) && 
	(pu1Data[PIMV4_DEST_IP2_POS] == PIMV4_DEST_IP2) && 
	(pu1Data[PIMV4_PROTOID_POS] == PIM_PROTOID) && 
	(pu1Data[PIMV4_HELLO_MSG_POS] == PIM_HELLO_MSG)) 
    {
    	/* post the buffer to PIM Hello queue */
    	if (OsixQueSend (gu4PimHelloQId, (UINT1 *) &pBuf,
        	             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    	{
            TTRACE1 ("Queue Send failed for posting PIM hello buffer from NP to PIM\n");
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return;
    	}
    	/* trigger the interuppt event for PIM */
    	OsixEvtSend (gu4PimHelloTaskId, PIM_NP_HELLO_PKT_ARRIVAL_EVENT);
        TTRACE1 ("Posted PIMv4 hello buffer from NP to PIM\n");
    }
    else if ((pu1Data[PIMV6_DEST_IP1_POS] == PIMV6_DEST_IP1) && 
		(pu1Data[PIMV6_DEST_IP2_POS] == PIMV6_DEST_IP2) && 
		(pu1Data[PIMV6_DEST_IP3_POS] == PIMV6_DEST_IP3) && 
		(pu1Data[PIMV6_PROTOID_POS] == PIM_PROTOID) && 
		(pu1Data[PIMV6_HELLO_MSG_POS] == PIM_HELLO_MSG))
    {
    	/* post the buffer to PIM Hello queue */
    	if (OsixQueSend (gu4PimHelloQId, (UINT1 *) &pBuf,
        	             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    	{
            TTRACE1 ("Queue Send failed for posting PIM hello buffer from NP to PIM\n");
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return;
    	}
    	/* trigger the interuppt event for PIM */
    	OsixEvtSend (gu4PimHelloTaskId, PIMV6_NP_HELLO_PKT_ARRIVAL_EVENT);
        TTRACE1 ("Posted PIMv6 hello buffer from NP to PIM\n");
    }
    else
    {
    	/* post the buffer to CFA queue */
    	if (OsixQueSend (CFA_PACKET_MUX_QID, (UINT1 *) &pBuf,
        	             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    	{
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return;
    	}
    	/* trigger the interuppt event for CFA */
    	OsixEvtSend (CFA_TASK_ID, CFA_GDD_INTERRUPT_EVENT);
    }
#else
    /* post the buffer to CFA queue */
    if (OsixQueSend (CFA_PACKET_MUX_QID, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }
    /* trigger the interuppt event for CFA */
    OsixEvtSend (CFA_TASK_ID, CFA_GDD_INTERRUPT_EVENT);
#endif
}

VOID
CfaNpSimProcessAndPostHBPktToRMQ (UINT1 *pu1Buf, UINT4 u4IfIndex,
                                  UINT4 u4PktLen)
{
    UNUSED_PARAM (u4IfIndex);
#ifdef RM_WANTED
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktLen, 0)) == NULL)
    {
        TTRACE1 ("Unable to allocate memory for packet reception\n");
        return;
    }
    CRU_BUF_Copy_OverBufChain (pBuf, pu1Buf, 0, u4PktLen);
    /* check if the interface exists */
    RmHbCallBackFn (pBuf);
#else
    UNUSED_PARAM (pu1Buf);
    UNUSED_PARAM (u4PktLen);
#endif
    return;
}

VOID
CfaNpSimProcessAndPostStdbyToSyslog (UINT1 *pu1Buf, UINT4 u4PktLen)
{
#ifdef RM_WANTED
#ifdef SYSLOG_WANTED
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktLen, 0)) == NULL)
    {
        PRINTF ("Unable to Allocate Memory For Peer Message Packets \n");
        return;
    }
    /* copy data from linear buffer into CRU buffer */
    CRU_BUF_Copy_OverBufChain (pBuf, pu1Buf, 0, u4PktLen);

    if (SendLogMsgToSyslog (pBuf, u4PktLen) != SYSLOG_SUCCESS)
    {
        return;
    }
    UNUSED_PARAM (u4PktLen);
#else
    UNUSED_PARAM (pu1Buf);
    UNUSED_PARAM (u4PktLen);
#endif
#else
    UNUSED_PARAM (pu1Buf);
    UNUSED_PARAM (u4PktLen);
#endif
    return;
}

/*****************************************************************************
 *    Function Name             : CfaNpSimProcessIPCMsgFromRemoteCPU
 *    Description               : This is invoked when a packet arrives to the
 *                                CPU. Packets received from Peer CPU with
 *                                customized Ether-type are Processed in this
 *                                function.Other Packets are not handled.
 *                                Customized Header is stripped and
 *                                Message Structure is filled based on the
 *                                Message Type .common API is invokded to process
 *                                the Notification
 *
 *    Input(s)                  : pkt             - Pointer to packet
 *
 *    Output(s)                 : pu1IsPktHandled - Indicates whether Pkt is processed
 *                                                 or Not.
 *    Globals Referred          : None
 *
 *    Globals Modified          : None
 *
 *    Returns                   : FNP_SUCCESS
 *
 *****************************************************************************/
INT4
CfaNpSimProcessIPCMsgFromRemoteCPU (UINT1 *pu1Pkt, UINT1 *pu1IsPktHandled)
{
    INT4                i4Offset = 0;
    UINT2               u2EtherType = 0;

    MEMCPY (&u2EtherType, pu1Pkt + CFA_NPSIM_IPC_ETHERTYPE_OFFSET,
            sizeof (UINT2));
    u2EtherType = OSIX_NTOHS (u2EtherType);

    if (u2EtherType == CFA_NPSIM_IPC_MSG_ETHERTYPE)
    {
        *pu1IsPktHandled = OSIX_TRUE;
        return CfaNpSimCustProcessIPCMsg (pu1Pkt, i4Offset);
    }
    else
    {
        *pu1IsPktHandled = OSIX_FALSE;
        return FNP_SUCCESS;
    }

}

/*****************************************************************************
 *    Function Name            : CfaNpSimCustProcessIPCMsg
 *    Description              : Processes the IPC Message received from Peer
 *                               CPU and Invoke the Corresponding function based
 *                               on RM State
 *
 *
 *    Inputs                     pu1Pkt   - Pointer to packet
 *
 *                               i4Offset - Offset of Customized Header
 *    Output(s)                : None
 *    Globals Referred         : None
 *    Globals Modified         : None
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Returns                  : FNP_SUCCESS/FNP_FAILURE
 *****************************************************************************/
INT4
CfaNpSimCustProcessIPCMsg (UINT1 *pu1Pkt, INT4 i4Offset)
{
    UINT4               u4MsgType = 0;
    INT4                i4RetVal = FNP_FAILURE;
    tHwInfo             HwInfo;
    MEMSET (&HwInfo, 0, sizeof (tHwInfo));
    u4MsgType = CfaNpSimGetIPCMsgType (pu1Pkt);

    i4Offset = CFA_NPSIM_IPC_BUF_OFFSET;

    HwInfo.u4MessageType = u4MsgType;
    if ((u4MsgType != NPWRAP_CALL_FROM_ACT_TO_STBY) &&
        (u4MsgType != NPWRAP_CALL_OUTPUT_FROM_STBY_TO_ACT) &&
        (u4MsgType != DISS_MSG))
    {
        MEMCPY (&HwInfo, pu1Pkt + i4Offset, sizeof (tHwInfo));
    }
    /* Any message from the peer CPU (active/standby) will be treated as 
     * an equivalent to HB message*/
#ifdef HB_WANTED
    HbApiSetKeepAliveFlag (OSIX_TRUE);
#endif
    if (CfaGetRetrieveNodeState () == RM_ACTIVE)
    {
        i4RetVal = CfaNpSimProcessActiveIPCMsg (pu1Pkt, &HwInfo, i4Offset);
    }
    else if (CfaGetRetrieveNodeState () == RM_STANDBY)
    {
        i4RetVal = CfaNpSimProcessStandbyIPCMsg (pu1Pkt, &HwInfo, i4Offset);
    }
    else
    {
        /* Only RM IPC msg will be handled here */
        i4RetVal = CfaNpSimProcessIPCMsgInInit (pu1Pkt, &HwInfo, i4Offset);
    }
    return i4RetVal;
}

/*****************************************************************************
 *    Function Name            : CfaNpSimProcessActiveIPCMsg
 *    Description              : Processes the IPC Message received from Standby
 *                               CPU and Invoke the Common Notification function.
 *                               
 *                                       
 *    Inputs                     pu1Buf   - Pointer to packet buffer
 *
 *                               HwInfo - Hardware Message Info .
 *
 *                               i4Offset - Offset of Customized Header
 *    Output(s)                : None 
 *    Globals Referred         : None
 *    Globals Modified         : None
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Returns                  : FNP_SUCCESS/FNP_FAILURE
 *****************************************************************************/
INT4
CfaNpSimProcessActiveIPCMsg (UINT1 *pu1Buf, tHwInfo * pHwInfo, INT4 i4Offset)
{
    UINT4               u4SeqNumber = 0;
    INT4                i4RetVal = FNP_FAILURE;
    tHwPortInfo         HwPortInfo;
    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    if ((pHwInfo->u4MessageType != NPWRAP_CALL_FROM_ACT_TO_STBY) &&
        (pHwInfo->u4MessageType != NPWRAP_CALL_OUTPUT_FROM_STBY_TO_ACT) &&
        (pHwInfo->u4MessageType != DISS_MSG))
    {
        if (pHwInfo->u4IfIndex == 0)
        {
            return FNP_SUCCESS;
        }
    }

    switch (pHwInfo->u4MessageType)
    {
            /* Strip the Msg Header added at the peer CPU */
        case NPWRAP_CALL_OUTPUT_FROM_STBY_TO_ACT:
            MEMCPY (&(pHwInfo->u4PktSize),
                    pu1Buf + CFA_NPSIM_IPC_MSGLENGTH_OFFSET, sizeof (UINT4));
            pHwInfo->u4PktSize = OSIX_NTOHL (pHwInfo->u4PktSize);
            MEMCPY (&u4SeqNumber, pu1Buf + CFA_NPSIM_NPAPI_IPC_SEQID_OFFSET,
                    sizeof (UINT4));
            pHwInfo->uHwMsgInfo.HwRemClntMsgInfo.u4SeqNum =
                OSIX_NTOHL (u4SeqNumber);
            MEMCPY (&i4RetVal, pu1Buf + CFA_NPSIM_NPAPI_IPC_RETVAL_OFFSET,
                    sizeof (UINT4));
            pHwInfo->uHwMsgInfo.HwRemClntMsgInfo.u4Status =
                OSIX_NTOHL (i4RetVal);
            pHwInfo->uHwMsgInfo.HwRemClntMsgInfo.pu1Data = pu1Buf + i4Offset;
            /* Reply for the NPAPI programming in standby */
            i4RetVal = NpUtilProcessActiveNpIPCMsg (pHwInfo);
            break;
        case PKT_RX_TO_CPU:
            i4Offset += sizeof (tHwInfo);
            /* offset of the buffer is moved to the start of the 
             * the orginial packet.
             */
            pu1Buf = pu1Buf + i4Offset;
            /* Packet is received in the remote CPU.
             * Remote CPU is in standby state
             * Hence the remote CPU has sent the packet to the local CPU
             * Local CPU is in active state.
             * Hence validate and post the packet to CFA in the local CPU
             */
            CfaNpSimProcessAndPostPktToCfaQ (pu1Buf, pHwInfo->u4IfIndex,
                                             pHwInfo->u4PktSize);
            i4RetVal = FNP_SUCCESS;
            break;

#ifdef RM_WANTED
        case RM_HB_PKT:
            i4Offset += sizeof (tHwInfo);
            /* offset of the buffer is moved to the start of the 
             * the orginial packet.
             */
            pu1Buf = pu1Buf + i4Offset;
            /* Packet is received in the remote CPU.
             * Remote CPU is in standby state
             * Hence the remote CPU has sent the packet to the local CPU
             * Local CPU is in active state.
             * Hence validate and post the packet to CFA in the local CPU
             */
            MEMCPY (&u4SeqNumber, pu1Buf + CFA_NPSIM_NPAPI_IPC_SEQID_OFFSET,
                    sizeof (UINT4));
            CfaNpSimProcessAndPostHBPktToRMQ (pu1Buf,
                                              HwPortInfo.
                                              au4ConnectingPortIfIndex[0],
                                              pHwInfo->u4PktSize);
            i4RetVal = FNP_SUCCESS;
            break;
        case RM_STDBY_MSG:
            i4Offset += sizeof (tHwInfo);
            pu1Buf = pu1Buf + i4Offset;
            CfaNpSimProcessAndPostStdbyToSyslog (pu1Buf, pHwInfo->u4PktSize);
            i4RetVal = FNP_SUCCESS;
            break;

        case RM_SYNC_PKT:
            i4Offset += sizeof (tHwInfo);
            /* offset of the buffer is moved to the start of the 
             * the orginial packet.
             */
            pu1Buf = pu1Buf + i4Offset;
            /* Packet is received in the remote CPU.
             * Remote CPU is in standby state
             * Hence the remote CPU has sent the packet to the local CPU
             * Local CPU is in active state.
             * Hence validate and post the packet to CFA in the local CPU
             */
            CfaNpSimProcessAndPostPktToCfaQ (pu1Buf,
                                             HwPortInfo.
                                             au4ConnectingPortIfIndex[0],
                                             pHwInfo->u4PktSize);
            i4RetVal = FNP_SUCCESS;
            break;
#endif

        case OPER_STATUS_INDICATION:
            i4Offset += sizeof (tHwInfo);
            pu1Buf = pu1Buf + i4Offset;
            pHwInfo->uHwMsgInfo.HwGenMsgInfo.pu1Data = pu1Buf;
            i4RetVal = FNP_SUCCESS;
            break;

        case MAC_LEARNING_INDICATION:
            i4Offset += sizeof (tHwInfo);
            pu1Buf = pu1Buf + i4Offset;
            pHwInfo->uHwMsgInfo.HwMacLearnMsgInfo.pu1Data = pu1Buf;
            i4RetVal = FNP_SUCCESS;
            break;

        case DOS_ATTACK_PKT_TO_CPU:
            i4Offset += sizeof (tHwInfo);
            pu1Buf = pu1Buf + i4Offset;
            pHwInfo->uHwMsgInfo.HwGenMsgInfo.pu1Data = pu1Buf;
            i4RetVal = FNP_SUCCESS;
            break;

        case DISS_MSG:
            MEMCPY (&(pHwInfo->u4PktSize),
                    pu1Buf + CFA_NPSIM_IPC_MSGLENGTH_OFFSET, sizeof (UINT4));
            pHwInfo->u4PktSize = OSIX_NTOHL (pHwInfo->u4PktSize);
            MEMCPY (&(pHwInfo->uHwMsgInfo.HwDissMsgInfo.u4AppId),
                    pu1Buf + i4Offset, sizeof (UINT4));

            MEMCPY (&(pHwInfo->uHwMsgInfo.HwDissMsgInfo.i4SrcSlot),
                    pu1Buf + i4Offset + ICCH_SLOTID_SIZE, sizeof (UINT4));

            pHwInfo->uHwMsgInfo.HwDissMsgInfo.pu1Data =
                pu1Buf + i4Offset + ICCH_APPID_SIZE + ICCH_SLOTID_SIZE;

            i4RetVal = ICCHProcessRxMessage (pHwInfo);
            break;

        default:
            TTRACE1 ("Invalid Message Type. Dropping the Packets \r\n");
            break;

    }
    return i4RetVal;
}

/*****************************************************************************
 *    Function Name            : CfaNpSimProcessIPCMsgInInit
 *    Description              : Processes the IPC Message received when the node 
 *                               is in INIT state
 *                                       
 *    Inputs                     pu1Buf   - Pointer to packet buffer
 *
 *                               HwInfo - Hardware Message Info .
 *
 *                               i4Offset - Offset of Customized Header
 *    Output(s)                : None 
 *    Globals Referred         : None
 *    Globals Modified         : None
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Returns                  : FNP_SUCCESS/FNP_FAILURE
 *****************************************************************************/

INT4
CfaNpSimProcessIPCMsgInInit (UINT1 *pu1Buf, tHwInfo * pHwInfo, INT4 i4Offset)
{
#ifdef RM_WANTED
    UINT4               u4SeqNumber = 0;
#else
    UNUSED_PARAM (pu1Buf);
    UNUSED_PARAM (i4Offset);
#endif
    tHwPortInfo         HwPortInfo;
    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    switch (pHwInfo->u4MessageType)
    {

#ifdef RM_WANTED
        case RM_HB_PKT:
            i4Offset += sizeof (tHwInfo);
            /* offset of the buffer is moved to the start of the 
             * the orginial packet.
             */
            pu1Buf = pu1Buf + i4Offset;
            /* Packet is received in the remote CPU.
             * Remote CPU is in standby state
             * Hence the remote CPU has sent the packet to the local CPU
             * Local CPU is in active state.
             * Hence validate and post the packet to CFA in the local CPU
             */
            MEMCPY (&u4SeqNumber, pu1Buf + CFA_NPSIM_NPAPI_IPC_SEQID_OFFSET,
                    sizeof (UINT4));
            CfaNpSimProcessAndPostHBPktToRMQ (pu1Buf,
                                              HwPortInfo.
                                              au4ConnectingPortIfIndex[0],
                                              pHwInfo->u4PktSize);
            break;
        case RM_SYNC_PKT:
            i4Offset += sizeof (tHwInfo);
            /* offset of the buffer is moved to the start of the 
             * the orginial packet.
             */
            pu1Buf = pu1Buf + i4Offset;
            /* Packet is received in the remote CPU.
             * Remote CPU is in standby state
             * Hence the remote CPU has sent the packet to the local CPU
             * Local CPU is in active state.
             * Hence validate and post the packet to CFA in the local CPU
             */
            CfaNpSimProcessAndPostPktToCfaQ (pu1Buf,
                                             HwPortInfo.
                                             au4ConnectingPortIfIndex[0],
                                             pHwInfo->u4PktSize);
            break;
#endif
        default:
            TTRACE1 ("Invalid Message Type. Dropping the Packets \r\n");
            break;

    }
    return FNP_SUCCESS;
}

/*****************************************************************************
 *    Function Name            : CfaNpSimProcessStandbyIPCMsg
 *    Description              : Processes the IPC Message received from Standby
 *                               CPU and Invoke the Common Notification function.
 *                               
 *                                       
 *    Inputs                     pkt   - Pointer to packet
 *
 *                               HwInfo - Hardware Message Info .
 *
 *                               i4Offset - Offset of Customized Header
 *    Output(s)                : None 
 *    Globals Referred         : None
 *    Globals Modified         : None
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Returns                  : FNP_SUCCESS/FNP_FAILURE
 *****************************************************************************/
INT4
CfaNpSimProcessStandbyIPCMsg (UINT1 *pu1Pkt, tHwInfo * pHwInfo, INT4 i4Offset)
{
    UINT4               u4SeqNumber = 0;
    UINT4               u4NpRetStatus = 0;
    INT4                i4RetVal = FNP_FAILURE;

    /* These Packets  are received from Active CPU and 
       to be Tx on ports of Stand-By node. */
    tHwPortInfo         HwPortInfo;
    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    i4Offset = CFA_NPSIM_IPC_BUF_OFFSET;
    switch (pHwInfo->u4MessageType)
    {
            /* Strip the Msg Header added at the peer CPU */
        case NPWRAP_CALL_FROM_ACT_TO_STBY:
            pHwInfo->u4MessageType = NPWRAP_CALL_FROM_ACT_TO_STBY;
            MEMCPY (&pHwInfo->u4PktSize,
                    pu1Pkt + CFA_NPSIM_IPC_MSGLENGTH_OFFSET, sizeof (UINT4));
            pHwInfo->u4PktSize = OSIX_NTOHL (pHwInfo->u4PktSize);
            MEMCPY (&u4SeqNumber, pu1Pkt + CFA_NPSIM_NPAPI_IPC_SEQID_OFFSET,
                    sizeof (UINT4));
            MEMCPY (&u4NpRetStatus, pu1Pkt + CFA_NPSIM_NPAPI_IPC_RETVAL_OFFSET,
                    sizeof (UINT4));
            pHwInfo->uHwMsgInfo.HwRemClntMsgInfo.u4SeqNum =
                OSIX_NTOHL (u4SeqNumber);
            pHwInfo->uHwMsgInfo.HwRemClntMsgInfo.u4Status =
                OSIX_NTOHL (u4NpRetStatus);
            pHwInfo->uHwMsgInfo.HwRemClntMsgInfo.pu1Data = pu1Pkt + i4Offset;
            i4RetVal = NpUtilProcessWrapperFn (pHwInfo);
            break;

        case PKT_TX_FROM_CPU:
            i4Offset += sizeof (tHwInfo);
            pu1Pkt = pu1Pkt + i4Offset;
            pHwInfo->uHwMsgInfo.HwGenMsgInfo.pu1Data = pu1Pkt;
            if (CfaNpSimProcessTxOnStandby (pHwInfo) == CFA_SUCCESS)
            {
                i4RetVal = FNP_SUCCESS;
            }
            break;

        case PKT_L3_TX_FROM_CPU:
            i4RetVal = FNP_SUCCESS;
            break;

#ifdef RM_WANTED
        case RM_HB_PKT:
            i4Offset += sizeof (tHwInfo);
            /* offset of the buffer is moved to the start of the 
             * the orginial packet.
             */
            pu1Pkt = pu1Pkt + i4Offset;
            /* Packet is received in the remote CPU.
             * Remote CPU is in standby state
             * Hence the remote CPU has sent the packet to the local CPU
             * Local CPU is in active state.
             * Hence validate and post the packet to CFA in the local CPU
             */
            CfaNpSimProcessAndPostHBPktToRMQ (pu1Pkt,
                                              HwPortInfo.
                                              au4ConnectingPortIfIndex[0],
                                              pHwInfo->u4PktSize);
            i4RetVal = FNP_SUCCESS;
            break;
        case RM_SYNC_PKT:
            i4Offset += sizeof (tHwInfo);
            /* offset of the buffer is moved to the start of the 
             * the orginial packet.
             */
            pu1Pkt = pu1Pkt + i4Offset;
            /* Packet is received in the remote CPU.
             * Remote CPU is in standby state
             * Hence the remote CPU has sent the packet to the local CPU
             * Local CPU is in active state.
             * Hence validate and post the packet to CFA in the local CPU
             */
            CfaNpSimProcessAndPostPktToCfaQ (pu1Pkt,
                                             HwPortInfo.
                                             au4ConnectingPortIfIndex[0],
                                             pHwInfo->u4PktSize);
            i4RetVal = FNP_SUCCESS;
            break;
        case RM_STDBY_MSG:
            i4Offset += sizeof (tHwInfo);
            pu1Pkt = pu1Pkt + i4Offset;
            CfaNpSimProcessAndPostStdbyToSyslog (pu1Pkt, pHwInfo->u4PktSize);
            i4RetVal = FNP_SUCCESS;
            break;
#endif
        case DISS_MSG:
            MEMCPY (&(pHwInfo->u4PktSize),
                    pu1Pkt + CFA_NPSIM_IPC_MSGLENGTH_OFFSET, sizeof (UINT4));
            pHwInfo->u4PktSize = OSIX_NTOHL (pHwInfo->u4PktSize);
            MEMCPY (&(pHwInfo->uHwMsgInfo.HwDissMsgInfo.u4AppId),
                    pu1Pkt + i4Offset, sizeof (UINT4));

            MEMCPY (&(pHwInfo->uHwMsgInfo.HwDissMsgInfo.i4SrcSlot),
                    pu1Pkt + i4Offset + ICCH_SLOTID_SIZE, sizeof (UINT4));

            pHwInfo->uHwMsgInfo.HwDissMsgInfo.pu1Data =
                pu1Pkt + i4Offset + ICCH_APPID_SIZE + ICCH_SLOTID_SIZE;

            i4RetVal = ICCHProcessRxMessage (pHwInfo);
            break;

        default:
            break;
    }
    return i4RetVal;
}

/****************************************************************************/
/*    Function Name      : CfaNpSimGetIPCMsgType                            */
/*                                                                          */
/*    Description        : This function take the message type from the     */
/*                         buffer and returns the same.                     */
/*                                                                          */
/*    Input(s)           : pu1Buf - pointer to in packet.                   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : u4MsgType                                        */
/****************************************************************************/
UINT4
CfaNpSimGetIPCMsgType (UINT1 *pu1Buf)
{
    UINT4               u4MsgType;

    MEMCPY (&u4MsgType, pu1Buf + CFA_NPSIM_IPC_MSGTYPE_OFFSET, sizeof (UINT4));
    u4MsgType = OSIX_NTOHL (u4MsgType);

    return u4MsgType;
}

/*****************************************************************************
 *
 *    Function Name       :CfaNpSimPostPktToActive
 *
 *    Description         :This routine takes of sending the packets received in  
 *                         standby unit to the active unit. Original packet will 
 *                         be encapsulated in the IPC message and sent to the active 
 *                         unit
 *
 *    Input(s)            : pu1Buf - packet buffer
 *                        : u4IfIndex - interface on which packet is rx
 *                        : u4PktLen - packet length
 *    Output(s)           : None
 *
 *    Returns            : NONE
 *****************************************************************************/
VOID
CfaNpSimPostPktToActive (UINT1 *pu1Buf, UINT4 u4IfIndex, UINT4 u4PktLen)
{
    tHwInfo             HwInfo;
    MEMSET (&HwInfo, 0, sizeof (tHwInfo));
    /* Packets Received Rx from HW,  in standby needs to be posted
     * to Active unit 
     */
    HwInfo.u4IfIndex = u4IfIndex;
    HwInfo.u4PktSize = u4PktLen;
    HwInfo.u4MessageType = PKT_RX_TO_CPU;
    HwInfo.uHwMsgInfo.HwRxMsgInfo.pu1Data = pu1Buf;
    if (FsCfaHwSendIPCMsg (&HwInfo) == FNP_FAILURE)
    {
        TTRACE1 ("Packet Send to remote CPU failed\n");
    }
}

/*****************************************************************************
 *    Function Name            : CfaNpSimProcessTxOnStandby
 *    Description              : This function transmits the packet on the standby port.
 *    Input(s)                 : pHwInfo - Pointer to Hardware Info
 *                                         It contains all required Information  
 *                                         Message Type,If-Index and other        
 *                                        parameters based on Mesage Type       
 *                         
 *    Output(s)                : None 
 *    Globals Referred         : None
 *    Globals Modified         : None
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Returns                  : CFA_SUCCESS/CFA_FAILURE
 *****************************************************************************/
INT4
CfaNpSimProcessTxOnStandby (tHwInfo * pHwInfo)
{

    /* invoke the driver call based on the type of port - use of function pointer */
    /* check whether the socket is opened for the standby port */
    struct sockaddr_in  ToAddr;
    INT4                i4Bytes;
    INT2                i2Port;
    UINT1              *pu1Buf = NULL;
    UINT1              *pu1TxBuf = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4Size = 0;

    u4IfIndex = pHwInfo->u4IfIndex;
    pu1Buf = pHwInfo->uHwMsgInfo.HwGenMsgInfo.pu1Data;
    u4Size = pHwInfo->u4PktSize;

    if (u4Size > CFA_MAX_DRIVER_MTU)
    {
        return (CFA_FAILURE);
    }
    i2Port = NslGetPortFromIfIndex (u4IfIndex);

    if (i2Port < 0)
    {
        return (CFA_FAILURE);
    }

    if (gaNslPort[i2Port].u1PortState != NSL_UP)
    {
        return (CFA_FAILURE);
    }

    if (IS_NSL_DEBUG_PKT_DUMP (gu4NslDebug))
    {
        CHR1                Tmp[80];

        sprintf (Tmp, "NSL: PktDump: Tx sw: %d, port: %d, ifIndex: %d"
                 "Size: %d", (int) gu2NslSw, (int) i2Port,
                 (int) u4IfIndex, (int) u4Size);
        NslBufDump (pu1Buf, (UINT1 *) Tmp, u4Size);
    }
    else
    {
        if (IS_NSL_DEBUG_PKT_EVENT (gu4NslDebug))
        {
            TTRACE1 ("NSL: PktEv: Tx sw: %d, port: %d, ifIndex: %d, "
                     "Size: %d\n", (int) gu2NslSw, (int) i2Port,
                     (int) u4IfIndex, (int) u4Size);
        }
    }
    pu1TxBuf = MemAllocMemBlk (gPktBufPoolId);
    if (pu1TxBuf == NULL)
    {
        CRTTRACE1
            ("CfaNpSimProcessTxOnStandby: Memory allocation for Tx Buf failed\n");
        return CFA_FAILURE;
    }
    MEMSET (pu1TxBuf, 0, (CFA_MAX_DRIVER_MTU + CFA_MAX_FRAME_HEADER_SIZE));

    MEMCPY (&(pu1TxBuf[0]), &gu2NslSw, 2);
    i2Port++;
    MEMCPY (&(pu1TxBuf[2]), &i2Port, 2);
    MEMCPY (&(pu1TxBuf[4]), pu1Buf, u4Size);

    ToAddr.sin_family = AF_INET;
    ToAddr.sin_port = htons (gu2NslBaseNpSimPort + gu2NslSw);
    MEMCPY ((CHR1 *) (&(ToAddr.sin_addr.s_addr)),
            (CHR1 *) & (gu4NslPktServerIpAddr), 4);

    i4Bytes = sendto (gNslSock, (void *) pu1TxBuf, (u4Size + 4),
                      0, (struct sockaddr *) &ToAddr, sizeof (ToAddr));
    MemReleaseMemBlock (gPktBufPoolId, pu1TxBuf);
    if (i4Bytes == (INT4) (u4Size + 4))
    {
        return (CFA_SUCCESS);
    }

    return (CFA_FAILURE);
}

/*****************************************************************************
 *    Function Name      : CfaGddProcessRecvInterruptEvent ()
 *    Description        : This function is invoked when an driver 
 *                         event occurs. This routine invokes the packet
 *                         processing function if a packet is received 
 *                         else it invokes the function which process the
 *                         driver status if status indication message is
 *                         received
 *    Input(s)           : VOID
 *    Output(s)          : None.
 
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Returns                  : None.
 *****************************************************************************/

PUBLIC INT4
CfaGddProcessRecvInterruptEvent (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pCruBuf;
    UINT4               u4IfIndex;
    UINT4               u4PktSize;
    UINT1               u1IfType;

    while (OsixQueRecv (CFA_PACKET_MUX_QID, (UINT1 *) &pCruBuf,
                        OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        CFA_LOCK ();
        u4IfIndex = pCruBuf->ModuleData.InterfaceId.u4IfIndex;
        u4PktSize = pCruBuf->pFirstValidDataDesc->u4_ValidByteCount;

        if (CfaGetIfType (u4IfIndex, &u1IfType) == CFA_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                     "In CfaMain Pkt recvd in interrupt mode failed to deliver to HL.\n");
        }
        else
        {
            /* Handover the Packet to Receive module */
            if (NULL != CFA_GDD_HL_RX_FNPTR (u4IfIndex))
            {
                if ((CFA_GDD_HL_RX_FNPTR (u4IfIndex))
                    (pCruBuf, u4IfIndex, u4PktSize, u1IfType,
                     CFA_ENCAP_NONE) != CFA_SUCCESS)
                {
                    /* release buffer which were not successfully sent to higher layer
                     */
                    CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
                    CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                             "In CfaMain Pkt recvd in interrupt mode failed to deliver to HL.\n");
                }
                else
                {
                    /* increment after successful handling of packet */
                    CFA_IF_SET_IN_OCTETS (u4IfIndex, u4PktSize);
                    CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                             "In CfaMain Pkt recvd in interrupt mode delivered to HL.\n");
                }
            }
            else
            {
                CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "In CfaMain Pkt recvd in interrupt mode. "
                         "Rx Fn not registered\n");
            }

        }
        CFA_UNLOCK ();
    }

    return (CFA_SUCCESS);
}
#endif
/*****************************************************************************
 *    Function Name            : CfaNpSimPostPktToStandby
 *    Description              : This function will be invoked to post the packet 
 *                               to remote CPU, since the outgoing port is present
 *                               in the remote CPU. Org Packet will be encapuslated
 *                               in a IPC message with additional information, like
 *                               the port in which packet needs to be transmitted
 *                               Length of the packet.
 *    Input(s)                 : pu1Buf - Packet buffer
 *                             : u4IfIndex - Interface index
 *                               u4PktLen - u4Packet Length  
 *    Returns                  : CFA_SUCCESS/CFA_FAILURE
 *****************************************************************************/
INT4
CfaNpSimPostPktToStandby (UINT1 *pu1Buf, UINT4 u4IfIndex, UINT4 u4PktLen)
{
    tHwInfo             HwInfo;
#ifdef MBSM_WANTED
    UINT4               u4SlotId = 0;
#endif
    MEMSET (&HwInfo, 0, sizeof (tHwInfo));

    /* For the packets to be transmitted in the standby ports.
     * send the packet in the IPC message to the standby unit. 
     * Standby on receiving this message will take of tx on the respective port
     */
    HwInfo.u4IfIndex = u4IfIndex;
    HwInfo.u4PktSize = u4PktLen;
    HwInfo.u4MessageType = PKT_TX_FROM_CPU;
#ifdef MBSM_WANTED
    MbsmGetSlotIdFromRemotePort (u4IfIndex, &u4SlotId);
    HwInfo.u4DestSlotId = u4SlotId;
#endif
    HwInfo.uHwMsgInfo.HwRxMsgInfo.pu1Data = pu1Buf;
    if (FsCfaHwSendIPCMsg (&HwInfo) == FNP_FAILURE)
    {
        TTRACE1 ("Packet Send to remote CPU failed\n");
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *    Function Name            : FsCfaHwSendIPCMsg
 *    Description              : Transmits packet to the peer CPU through the
 *                               connecting Interface. 
 *    Input(s)                 : pHwInfo - Pointer to Hardware Info
 *                                         It contains all required Information  
 *                                         Message Type,If-Index and other        
 *                                        parameters based on Mesage Type       
 *                         
 *    Output(s)                : None 
 *    Globals Referred         : None
 *    Globals Modified         : None
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Returns                  : FNP_SUCCESS/FNP_FAILURE
 *****************************************************************************/
INT4
FsCfaHwSendIPCMsg (tHwInfo * pHwInfo)
{
    tHwPortInfo         HwPortInfo;
    UINT1              *pu1DataBuf = NULL;
    UINT1              *pu1LinearBuf = NULL;
    UINT1              *pu1IPCMsgTxBuf = NULL;
    UINT1               au1DstMac[CFA_ENET_ADDR_LEN] =
        { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };
    tMacAddr            au1SrcMac;
    UINT4               u4StackingIfIndex = 0;
    UINT4               u4CurrentPktOffset = 0;
    UINT4               u4Pktsize = pHwInfo->u4PktSize;
    UINT4               u4MsgType = OSIX_HTONL (pHwInfo->u4MessageType);
    UINT4               u4MsgLength = OSIX_HTONL (pHwInfo->u4PktSize);
    UINT4               u4SeqNumber = 0;
    UINT4               u4NPRetVal = 0;
    UINT2               u2EtherType = OSIX_HTONS (CFA_NPSIM_IPC_MSG_ETHERTYPE);

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    pu1IPCMsgTxBuf = MemAllocMemBlk (gPktBufPoolId);
    if (pu1IPCMsgTxBuf == NULL)
    {
        CRTTRACE1 ("FsCfaHwSendIPCMsg: Memory allocation for Tx Buf failed\n");
        return FNP_FAILURE;
    }
    MEMSET (pu1IPCMsgTxBuf, 0,
            (CFA_MAX_DRIVER_MTU + CFA_MAX_FRAME_HEADER_SIZE));
    pu1LinearBuf = pu1IPCMsgTxBuf;

    CfaGetLocalUnitPortInformation (&HwPortInfo);
    u4StackingIfIndex = HwPortInfo.au4ConnectingPortIfIndex[0];

    u4Pktsize = u4Pktsize + CFA_NPSIM_IPC_BUF_OFFSET;
    if ((pHwInfo->u4MessageType != NPWRAP_CALL_FROM_ACT_TO_STBY) &&
        (pHwInfo->u4MessageType != NPWRAP_CALL_OUTPUT_FROM_STBY_TO_ACT))
    {
        u4Pktsize += sizeof (tHwInfo);
    }
    CfaGetSysMacAddress (au1SrcMac);
    if ((pHwInfo->u4MessageType == NPWRAP_CALL_FROM_ACT_TO_STBY) ||
        (pHwInfo->u4MessageType == NPWRAP_CALL_OUTPUT_FROM_STBY_TO_ACT))
    {
        u4SeqNumber =
            OSIX_HTONL (pHwInfo->uHwMsgInfo.HwRemClntMsgInfo.u4SeqNum);
        u4NPRetVal = OSIX_HTONL (pHwInfo->uHwMsgInfo.HwRemClntMsgInfo.u4Status);
        pu1DataBuf = pHwInfo->uHwMsgInfo.HwRemClntMsgInfo.pu1Data;
    }
    else if (pHwInfo->u4MessageType == DISS_MSG)
    {
        pu1DataBuf = pHwInfo->uHwMsgInfo.HwDissMsgInfo.pu1Data;
    }
    else
    {
        if (CfaGetRetrieveNodeState () == RM_ACTIVE)
        {
            switch (pHwInfo->u4MessageType)
            {
                case PKT_L3_TX_FROM_CPU:
                    pu1DataBuf = pHwInfo->uHwMsgInfo.HwL3TxMsgInfo.pu1Data;
                    break;
                case PKT_TX_FROM_CPU:
                    pu1DataBuf = pHwInfo->uHwMsgInfo.HwGenMsgInfo.pu1Data;
                    break;
#ifdef RM_WANTED
                case RM_HB_PKT:
                case RM_SYNC_PKT:
                    pHwInfo->u4IfIndex = u4StackingIfIndex;
                    pu1DataBuf = pHwInfo->uHwMsgInfo.HwGenMsgInfo.pu1Data;
                    break;
#endif
                default:
                    TTRACE1 ("Invalid Message Type. Dropping the Packets \r\n");
                    MemReleaseMemBlock (gPktBufPoolId, pu1IPCMsgTxBuf);
                    return FNP_FAILURE;
            }
        }
        else if (CfaGetRetrieveNodeState () == RM_STANDBY)
        {
            switch (pHwInfo->u4MessageType)
            {
                case PKT_RX_TO_CPU:
                    pu1DataBuf = pHwInfo->uHwMsgInfo.HwRxMsgInfo.pu1Data;
                    break;

                case OPER_STATUS_INDICATION:
#ifdef RM_WANTED
                case RM_HB_PKT:
                case RM_SYNC_PKT:
                    pHwInfo->u4IfIndex = u4StackingIfIndex;
                    pu1DataBuf = pHwInfo->uHwMsgInfo.HwGenMsgInfo.pu1Data;
                    break;
#endif
                case MAC_LEARNING_INDICATION:
                    pu1DataBuf = pHwInfo->uHwMsgInfo.HwMacLearnMsgInfo.pu1Data;
                    break;

                case DOS_ATTACK_PKT_TO_CPU:
                    TTRACE1 ("Not Supported for NPSIM \r\n");
                    break;
                case RM_STDBY_MSG:
                    pHwInfo->u4IfIndex = u4StackingIfIndex;
                    pu1DataBuf = pHwInfo->uHwMsgInfo.HwGenMsgInfo.pu1Data;
                    break;

                default:
                    TTRACE1 ("Invalid Message Type. Dropping the Packets \r\n");
                    MemReleaseMemBlock (gPktBufPoolId, pu1IPCMsgTxBuf);
                    return FNP_FAILURE;
            }
        }
        else
        {
            switch (pHwInfo->u4MessageType)
            {
#ifdef RM_WANTED
                case RM_HB_PKT:
                case RM_SYNC_PKT:
                    pHwInfo->u4IfIndex = u4StackingIfIndex;
                    pu1DataBuf = pHwInfo->uHwMsgInfo.HwGenMsgInfo.pu1Data;
                    break;
#endif
                default:
                    TTRACE1 ("Invalid Message Type. Dropping the Packets \r\n");
                    MemReleaseMemBlock (gPktBufPoolId, pu1IPCMsgTxBuf);
                    return FNP_FAILURE;
            }
        }
    }
    /* allocate CRU buffer */

    u4CurrentPktOffset = 0;
    /* Copy the Destination Mac */
    MEMCPY (pu1LinearBuf, au1DstMac, CFA_ENET_ADDR_LEN);
    u4CurrentPktOffset += CFA_ENET_ADDR_LEN;
    pu1LinearBuf += CFA_ENET_ADDR_LEN;

    /* Copy the Source Mac */
    MEMCPY (pu1LinearBuf, au1SrcMac, CFA_ENET_ADDR_LEN);
    u4CurrentPktOffset += CFA_ENET_ADDR_LEN;
    pu1LinearBuf += CFA_ENET_ADDR_LEN;

    /* Copy the Ether Type */
    MEMCPY (pu1LinearBuf, (UINT1 *) &u2EtherType,
            CFA_NPSIM_IPC_MSG_ETHERTYPE_LEN);
    u4CurrentPktOffset += CFA_NPSIM_IPC_MSG_ETHERTYPE_LEN;
    pu1LinearBuf += CFA_NPSIM_IPC_MSG_ETHERTYPE_LEN;

    /* Copy the IPC Message Type */
    MEMCPY (pu1LinearBuf, (UINT1 *) &u4MsgType, sizeof (UINT4));
    u4CurrentPktOffset += sizeof (UINT4);
    pu1LinearBuf += sizeof (UINT4);

    /* Copy the IPC Message Length */
    MEMCPY (pu1LinearBuf, (UINT1 *) &u4MsgLength, sizeof (UINT4));
    u4CurrentPktOffset += sizeof (UINT4);
    pu1LinearBuf += sizeof (UINT4);

    /* Copy the IPC Message Sequence Number 
     * Valid for NPAPI IPC messages, for other message this field will be 0
     */
    MEMCPY (pu1LinearBuf, (UINT1 *) &u4SeqNumber, sizeof (UINT4));
    u4CurrentPktOffset += sizeof (UINT4);
    pu1LinearBuf += sizeof (UINT4);
    /* Copy the IPC Message Return Value 
     * Valid for NPAPI IPC messages, for other message this field will be 0
     */
    MEMCPY (pu1LinearBuf, (UINT1 *) &u4NPRetVal, sizeof (UINT4));
    u4CurrentPktOffset += sizeof (UINT4);
    pu1LinearBuf += sizeof (UINT4);

    if ((pHwInfo->u4MessageType != NPWRAP_CALL_FROM_ACT_TO_STBY) &&
        (pHwInfo->u4MessageType != NPWRAP_CALL_OUTPUT_FROM_STBY_TO_ACT) &&
        (pHwInfo->u4MessageType != DISS_MSG))
    {
        /* pHwInfo will be copied in the message, 
         * For All the IPC message, Excluding the NPAPI IPC messages.
         */
        MEMCPY (pu1LinearBuf, (UINT1 *) pHwInfo, sizeof (tHwInfo));
        u4CurrentPktOffset += sizeof (tHwInfo);
        pu1LinearBuf += sizeof (tHwInfo);
    }

    if (pHwInfo->u4MessageType == DISS_MSG)
    {
        MEMCPY (pu1LinearBuf,
                (UINT1 *) &pHwInfo->uHwMsgInfo.HwDissMsgInfo.u4AppId,
                sizeof (pHwInfo->uHwMsgInfo.HwDissMsgInfo.u4AppId));
        u4CurrentPktOffset +=
            sizeof (pHwInfo->uHwMsgInfo.HwDissMsgInfo.u4AppId);
        pu1LinearBuf += sizeof (pHwInfo->uHwMsgInfo.HwDissMsgInfo.u4AppId);

        MEMCPY (pu1LinearBuf,
                (UINT1 *) &pHwInfo->uHwMsgInfo.HwDissMsgInfo.i4SrcSlot,
                sizeof (pHwInfo->uHwMsgInfo.HwDissMsgInfo.i4SrcSlot));
        u4CurrentPktOffset +=
            sizeof (pHwInfo->uHwMsgInfo.HwDissMsgInfo.i4SrcSlot);
        pu1LinearBuf += sizeof (pHwInfo->uHwMsgInfo.HwDissMsgInfo.i4SrcSlot);
    }

    /* Copy the IPC Message  
     */
    if (pu1DataBuf == NULL)
    {
        MemReleaseMemBlock (gPktBufPoolId, pu1IPCMsgTxBuf);
        return FNP_FAILURE;
    }
    MEMCPY (pu1LinearBuf, pu1DataBuf, pHwInfo->u4PktSize);
    u4CurrentPktOffset += pHwInfo->u4PktSize;
    pu1LinearBuf += pHwInfo->u4PktSize;
    if (CfaIfUtlGapIfGet (u4StackingIfIndex) == NULL)
    {
        MemReleaseMemBlock (gPktBufPoolId, pu1IPCMsgTxBuf);
        return FNP_FAILURE;
    }
    if (CFA_GDD_ENTRY (u4StackingIfIndex) == NULL)
    {
        MemReleaseMemBlock (gPktBufPoolId, pu1IPCMsgTxBuf);
        return FNP_FAILURE;
    }
    /* invoke the driver call based on the type of port - use of function pointer */
    if (CFA_GDD_WRITE_FNPTR (u4StackingIfIndex) == NULL)
    {
        MemReleaseMemBlock (gPktBufPoolId, pu1IPCMsgTxBuf);
        return FNP_FAILURE;
    }
    if ((CFA_GDD_WRITE_FNPTR (u4StackingIfIndex))
        (pu1IPCMsgTxBuf, u4StackingIfIndex, u4Pktsize) != CFA_SUCCESS)
    {
        MemReleaseMemBlock (gPktBufPoolId, pu1IPCMsgTxBuf);
        return FNP_FAILURE;
    }

    MemReleaseMemBlock (gPktBufPoolId, pu1IPCMsgTxBuf);
    return FNP_SUCCESS;
}

/*****************************************************************************
 *    Function Name            : CfaGddPortWrite
 *    Description              : Transmits packet to the peer CPU through the
 *                               connecting Interface.
 *    Input(s)                 : pu1DataBuf - Pointer to data buffer which 
 *                                     contains all required Information
 *                                     to be written on the interface
 *                             : u2IfIndex - Interface index on which the data
 *                                     to be written
 *                             : u4PktSize - size of the data buffer
 *    Output(s)                : None
 *    Globals Referred         : None
 *    Globals Modified         : None
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Returns                  : FNP_SUCCESS/FNP_FAILURE
 *****************************************************************************/
INT4
CfaGddPortWrite (UINT1 *pu1DataBuf, UINT2 u2IfIndex, UINT4 u4PktSize)
{
    if ((CFA_GDD_WRITE_FNPTR (u2IfIndex)) == NULL)
    {
        return FNP_FAILURE;
    }

    if ((CFA_GDD_WRITE_FNPTR (u2IfIndex))
        (pu1DataBuf, u2IfIndex, u4PktSize) != CFA_SUCCESS)
    {
        /*MemReleaseMemBlock (gPktBufPoolId, pu1HbMsgTxBuf);*/
        return FNP_FAILURE;
    }
    return FNP_SUCCESS;
}
/*****************************************************************************
 *
 *    Function Name        : CfaHwL3PwVlanIntfWrite
 *
 *    Description          : This function process the incoming packet
 *                           over the pseudowire interface send the packet
 *                           for MPLS Processing.
 *
 *    Input(s)            :  i2Port - Interface index
 *
 *    Returns             :  CFA_SUCCESS/CFA_FAILURE
 *
 *****************************************************************************/

INT4
CfaHwL3PwVlanIntfWrite (UINT4 u4Size, INT2 i2Port)
{

#ifdef MPLS_WANTED
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    pBuf = CRU_BUF_Allocate_MsgBufChain (u4Size, 0);
    if (pBuf == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CRU buffer allocation is failed while sending the"
                 "packet on Pseudo wire interface\r\n");
        return CFA_FAILURE;
    }
    CRU_BUF_Copy_OverBufChain (pBuf, gau1TxBuf + 4, 0, u4Size);

    MEMSET (gau1TxBuf, 0, u4Size);
    if (CfaIwfMplsProcessOutGoingL2Pkt (pBuf, i2Port, 0,
                                        CFA_LINK_UCAST) == MPLS_SUCCESS)
    {
        return CFA_SUCCESS;
    }
#else
    UNUSED_PARAM (u4Size);
    UNUSED_PARAM (i2Port);
#endif
    /* No need to free the CRU buff MPLS will take care of Releasing it */

    return CFA_FAILURE;
}
