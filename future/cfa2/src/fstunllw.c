/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstunllw.c,v 1.29 2014/06/18 11:02:34 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "cfainc.h"
#include "fsmptucli.h"

#define  CFA_ADDR_ANY       0x00000000
#define  CFA_ADDR_LOOPBACK  0x7f000000
#define  CFA_ADDR_BROADCAST 0xffffffff
#define  CFA_ADDR_MCAST1    0xe0000001
#define  CFA_ADDR_MCAST2    0xefffffff

/* LOW LEVEL Routines for Table : FsTunlIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsTunlIfTable
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsTunlIfTable (INT4 i4FsTunlIfAddressType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsTunlIfLocalInetAddress,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsTunlIfRemoteInetAddress,
                                       INT4 i4FsTunlIfEncapsMethod,
                                       INT4 i4FsTunlIfConfigID)
{
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;

    if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }

    if (pTnlIfEntry == NULL)
    {
        /* Entry not present */
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsTunlIfTable
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsTunlIfTable (INT4 *pi4FsTunlIfAddressType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsTunlIfLocalInetAddress,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsTunlIfRemoteInetAddress,
                               INT4 *pi4FsTunlIfEncapsMethod,
                               INT4 *pi4FsTunlIfConfigID)
{
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (CfaTnlGetFirstEntryInCxt (pTunlCxt,
                                  pi4FsTunlIfAddressType,
                                  pFsTunlIfLocalInetAddress,
                                  pFsTunlIfRemoteInetAddress,
                                  pi4FsTunlIfEncapsMethod,
                                  pi4FsTunlIfConfigID) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsTunlIfTable
 Input       :  The Indices
                FsTunlIfAddressType
                nextFsTunlIfAddressType
                FsTunlIfLocalInetAddress
                nextFsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                nextFsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                nextFsTunlIfEncapsMethod
                FsTunlIfConfigID
                nextFsTunlIfConfigID
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsTunlIfTable (INT4 i4FsTunlIfAddressType,
                              INT4 *pi4NextFsTunlIfAddressType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsTunlIfLocalInetAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pNextFsTunlIfLocalInetAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsTunlIfRemoteInetAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pNextFsTunlIfRemoteInetAddress,
                              INT4 i4FsTunlIfEncapsMethod,
                              INT4 *pi4NextFsTunlIfEncapsMethod,
                              INT4 i4FsTunlIfConfigID,
                              INT4 *pi4NextFsTunlIfConfigID)
{
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (CfaTnlGetNextEntryInCxt (pTunlCxt,
                                 i4FsTunlIfAddressType,
                                 pi4NextFsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pNextFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 pNextFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 pi4NextFsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 pi4NextFsTunlIfConfigID) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTunlIfHopLimit
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                retValFsTunlIfHopLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunlIfHopLimit (INT4 i4FsTunlIfAddressType,
                        tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                        tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                        INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                        INT4 *pi4RetValFsTunlIfHopLimit)
{
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;
    if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }

    if (pTnlIfEntry == NULL)
    {
        /* Entry not present */
        return SNMP_FAILURE;
    }

    *pi4RetValFsTunlIfHopLimit = pTnlIfEntry->u2HopLimit;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTunlIfSecurity
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                retValFsTunlIfSecurity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunlIfSecurity (INT4 i4FsTunlIfAddressType,
                        tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                        tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                        INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                        INT4 *pi4RetValFsTunlIfSecurity)
{
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;
    if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }

    if (pTnlIfEntry == NULL)
    {
        /* Entry not present */
        return SNMP_FAILURE;
    }

    *pi4RetValFsTunlIfSecurity = pTnlIfEntry->u1Security;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTunlIfTOS
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                retValFsTunlIfTOS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunlIfTOS (INT4 i4FsTunlIfAddressType,
                   tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                   tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                   INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                   INT4 *pi4RetValFsTunlIfTOS)
{
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;
    if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }

    if (pTnlIfEntry == NULL)
    {
        /* Entry not present */
        return SNMP_FAILURE;
    }
    *pi4RetValFsTunlIfTOS = pTnlIfEntry->i2TOS;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTunlIfFlowLabel
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                retValFsTunlIfFlowLabel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunlIfFlowLabel (INT4 i4FsTunlIfAddressType,
                         tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                         tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                         INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                         INT4 *pi4RetValFsTunlIfFlowLabel)
{
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;
    if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }

    if (pTnlIfEntry == NULL)
    {
        /* Entry not present */
        return SNMP_FAILURE;
    }
    *pi4RetValFsTunlIfFlowLabel = pTnlIfEntry->i4FlowLabel;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTunlIfMTU
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                retValFsTunlIfMTU
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunlIfMTU (INT4 i4FsTunlIfAddressType,
                   tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                   tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                   INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                   INT4 *pi4RetValFsTunlIfMTU)
{
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;
    if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }

    if (pTnlIfEntry == NULL)
    {
        /* Entry not present */
        return SNMP_FAILURE;
    }

    *pi4RetValFsTunlIfMTU = (INT4) pTnlIfEntry->u4Mtu;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTunlIfDirFlag
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                retValFsTunlIfDirFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunlIfDirFlag (INT4 i4FsTunlIfAddressType,
                       tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                       tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                       INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                       INT4 *pi4RetValFsTunlIfDirFlag)
{
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;
    if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }

    if (pTnlIfEntry == NULL)
    {
        /* Entry not present */
        return SNMP_FAILURE;
    }

    *pi4RetValFsTunlIfDirFlag = pTnlIfEntry->u1DirFlag;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTunlIfDirection
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                retValFsTunlIfDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunlIfDirection (INT4 i4FsTunlIfAddressType,
                         tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                         tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                         INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                         INT4 *pi4RetValFsTunlIfDirection)
{
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;
    if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }

    if (pTnlIfEntry == NULL)
    {
        /* Entry not present */
        return SNMP_FAILURE;
    }

    *pi4RetValFsTunlIfDirection = pTnlIfEntry->u1Direction;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTunlIfEncaplmt
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                retValFsTunlIfEncaplmt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunlIfEncaplmt (INT4 i4FsTunlIfAddressType,
                        tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                        tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                        INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                        UINT4 *pu4RetValFsTunlIfEncaplmt)
{
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;
    if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }

    if (pTnlIfEntry == NULL)
    {
        /* Entry not present */
        return SNMP_FAILURE;
    }

    *pu4RetValFsTunlIfEncaplmt = pTnlIfEntry->u4EncapLmt;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTunlIfEncapOption
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                retValFsTunlIfEncapOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunlIfEncapOption (INT4 i4FsTunlIfAddressType,
                           tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                           tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                           INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                           INT4 *pi4RetValFsTunlIfEncapOption)
{
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;
    if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }

    if (pTnlIfEntry == NULL)
    {
        /* Entry not present */
        return SNMP_FAILURE;
    }

    *pi4RetValFsTunlIfEncapOption = pTnlIfEntry->u1EncapOption;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTunlIfIndex
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                retValFsTunlIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunlIfIndex (INT4 i4FsTunlIfAddressType,
                     tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                     tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                     INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                     INT4 *pi4RetValFsTunlIfIndex)
{
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;
    if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }

    if (pTnlIfEntry == NULL)
    {
        /* Entry not present */
        return SNMP_FAILURE;
    }

    *pi4RetValFsTunlIfIndex = (INT4) pTnlIfEntry->u4IfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTunlIfAlias
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                retValFsTunlIfAlias
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunlIfAlias (INT4 i4FsTunlIfAddressType,
                     tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                     tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                     INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                     tSNMP_OCTET_STRING_TYPE * pRetValFsTunlIfAlias)
{
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;
    if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }

    if (pTnlIfEntry == NULL)
    {
        /* Entry not present in TnlIfTbl */
        return SNMP_FAILURE;
    }

    pRetValFsTunlIfAlias->i4_Length = (INT4) STRLEN (pTnlIfEntry->au1IfAlias);
    MEMCPY (pRetValFsTunlIfAlias->pu1_OctetList, pTnlIfEntry->au1IfAlias,
            pRetValFsTunlIfAlias->i4_Length);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTunlIfCksumFlag
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                retValFsTunlIfCksumFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunlIfCksumFlag (INT4 i4FsTunlIfAddressType,
                         tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                         tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                         INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                         INT4 *pi4RetValFsTunlIfCksumFlag)
{
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;
    if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }

    if (pTnlIfEntry == NULL)
    {
        /* Entry not present */
        return SNMP_FAILURE;
    }

    *pi4RetValFsTunlIfCksumFlag = pTnlIfEntry->bCksumFlag;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTunlIfPmtuFlag
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                retValFsTunlIfPmtuFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunlIfPmtuFlag (INT4 i4FsTunlIfAddressType,
                        tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                        tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                        INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                        INT4 *pi4RetValFsTunlIfPmtuFlag)
{
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;
    if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }

    if (pTnlIfEntry == NULL)
    {
        /* Entry not present */
        return SNMP_FAILURE;
    }

    *pi4RetValFsTunlIfPmtuFlag = pTnlIfEntry->bPmtuFlag;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsTunlIfStatus
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                retValFsTunlIfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunlIfStatus (INT4 i4FsTunlIfAddressType,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                      INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                      INT4 *pi4RetValFsTunlIfStatus)
{
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;
    if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }

    if (pTnlIfEntry == NULL)
    {
        /* Entry not present */
        return SNMP_FAILURE;
    }

    *pi4RetValFsTunlIfStatus = pTnlIfEntry->u1RowStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsTunlIfHopLimit
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                setValFsTunlIfHopLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunlIfHopLimit (INT4 i4FsTunlIfAddressType,
                        tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                        tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                        INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                        INT4 i4SetValFsTunlIfHopLimit)
{
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (CfaSetTnlIfHopLimitInCxt (pTunlCxt,
                                  i4FsTunlIfAddressType,
                                  pFsTunlIfLocalInetAddress,
                                  pFsTunlIfRemoteInetAddress,
                                  i4FsTunlIfEncapsMethod,
                                  i4FsTunlIfConfigID,
                                  i4SetValFsTunlIfHopLimit) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    CfaIncMsrForTunlIfTable (gCfaTnlGblInfo.pTunlGblCxt->u4ContextId,
                             i4FsTunlIfAddressType,
                             pFsTunlIfLocalInetAddress,
                             pFsTunlIfRemoteInetAddress,
                             i4FsTunlIfEncapsMethod,
                             i4FsTunlIfConfigID, 'i',
                             &i4SetValFsTunlIfHopLimit,
                             FsMITunlIfHopLimit,
                             sizeof (FsMITunlIfHopLimit) / sizeof (UINT4),
                             FALSE);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsTunlIfTOS
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                setValFsTunlIfTOS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunlIfTOS (INT4 i4FsTunlIfAddressType,
                   tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                   tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                   INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                   INT4 i4SetValFsTunlIfTOS)
{
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (CfaSetTnlIfTOSInCxt (pTunlCxt, i4FsTunlIfAddressType,
                             pFsTunlIfLocalInetAddress,
                             pFsTunlIfRemoteInetAddress,
                             i4FsTunlIfEncapsMethod,
                             i4FsTunlIfConfigID,
                             i4SetValFsTunlIfTOS) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    CfaIncMsrForTunlIfTable (gCfaTnlGblInfo.pTunlGblCxt->u4ContextId,
                             i4FsTunlIfAddressType,
                             pFsTunlIfLocalInetAddress,
                             pFsTunlIfRemoteInetAddress,
                             i4FsTunlIfEncapsMethod,
                             i4FsTunlIfConfigID, 'i',
                             &i4SetValFsTunlIfTOS,
                             FsMITunlIfTOS,
                             sizeof (FsMITunlIfTOS) / sizeof (UINT4), FALSE);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsTunlIfFlowLabel
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                setValFsTunlIfFlowLabel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunlIfFlowLabel (INT4 i4FsTunlIfAddressType,
                         tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                         tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                         INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                         INT4 i4SetValFsTunlIfFlowLabel)
{
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (CfaSetTnlIfFlowLabelInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                   pFsTunlIfLocalInetAddress,
                                   pFsTunlIfRemoteInetAddress,
                                   i4FsTunlIfEncapsMethod,
                                   i4FsTunlIfConfigID,
                                   i4SetValFsTunlIfFlowLabel) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    CfaIncMsrForTunlIfTable (gCfaTnlGblInfo.pTunlGblCxt->u4ContextId,
                             i4FsTunlIfAddressType,
                             pFsTunlIfLocalInetAddress,
                             pFsTunlIfRemoteInetAddress,
                             i4FsTunlIfEncapsMethod,
                             i4FsTunlIfConfigID, 'i',
                             &i4SetValFsTunlIfFlowLabel,
                             FsMITunlIfFlowLabel,
                             sizeof (FsMITunlIfFlowLabel) / sizeof (UINT4),
                             FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsTunlIfDirFlag
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                setValFsTunlIfDirFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunlIfDirFlag (INT4 i4FsTunlIfAddressType,
                       tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                       tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                       INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                       INT4 i4SetValFsTunlIfDirFlag)
{
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (CfaSetTnlIfDirFlagInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 i4SetValFsTunlIfDirFlag) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    CfaIncMsrForTunlIfTable (gCfaTnlGblInfo.pTunlGblCxt->u4ContextId,
                             i4FsTunlIfAddressType,
                             pFsTunlIfLocalInetAddress,
                             pFsTunlIfRemoteInetAddress,
                             i4FsTunlIfEncapsMethod,
                             i4FsTunlIfConfigID, 'i',
                             &i4SetValFsTunlIfDirFlag,
                             FsMITunlIfDirFlag,
                             sizeof (FsMITunlIfDirFlag) / sizeof (UINT4),
                             FALSE);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsTunlIfDirection
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                setValFsTunlIfDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunlIfDirection (INT4 i4FsTunlIfAddressType,
                         tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                         tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                         INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                         INT4 i4SetValFsTunlIfDirection)
{
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (CfaSetTnlIfDirectionInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                   pFsTunlIfLocalInetAddress,
                                   pFsTunlIfRemoteInetAddress,
                                   i4FsTunlIfEncapsMethod,
                                   i4FsTunlIfConfigID,
                                   i4SetValFsTunlIfDirection) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }

    CfaIncMsrForTunlIfTable (gCfaTnlGblInfo.pTunlGblCxt->u4ContextId,
                             i4FsTunlIfAddressType,
                             pFsTunlIfLocalInetAddress,
                             pFsTunlIfRemoteInetAddress,
                             i4FsTunlIfEncapsMethod,
                             i4FsTunlIfConfigID, 'i',
                             &i4SetValFsTunlIfDirection,
                             FsMITunlIfDirection,
                             sizeof (FsMITunlIfDirection) / sizeof (UINT4),
                             FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsTunlIfEncaplmt
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                setValFsTunlIfEncaplmt
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunlIfEncaplmt (INT4 i4FsTunlIfAddressType,
                        tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                        tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                        INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                        UINT4 u4SetValFsTunlIfEncaplmt)
{
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (CfaSetTnlIfEncapLimitInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                    pFsTunlIfLocalInetAddress,
                                    pFsTunlIfRemoteInetAddress,
                                    i4FsTunlIfEncapsMethod,
                                    i4FsTunlIfConfigID,
                                    u4SetValFsTunlIfEncaplmt) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    CfaIncMsrForTunlIfTable (gCfaTnlGblInfo.pTunlGblCxt->u4ContextId,
                             i4FsTunlIfAddressType,
                             pFsTunlIfLocalInetAddress,
                             pFsTunlIfRemoteInetAddress,
                             i4FsTunlIfEncapsMethod,
                             i4FsTunlIfConfigID, 'u',
                             &u4SetValFsTunlIfEncaplmt,
                             FsMITunlIfEncaplmt,
                             sizeof (FsMITunlIfEncaplmt) / sizeof (UINT4),
                             FALSE);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsTunlIfEncapOption
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                setValFsTunlIfEncapOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunlIfEncapOption (INT4 i4FsTunlIfAddressType,
                           tSNMP_OCTET_STRING_TYPE *
                           pFsTunlIfLocalInetAddress,
                           tSNMP_OCTET_STRING_TYPE *
                           pFsTunlIfRemoteInetAddress,
                           INT4 i4FsTunlIfEncapsMethod,
                           INT4 i4FsTunlIfConfigID,
                           INT4 i4SetValFsTunlIfEncapOption)
{
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (CfaSetTnlIfEncapOptionInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                     pFsTunlIfLocalInetAddress,
                                     pFsTunlIfRemoteInetAddress,
                                     i4FsTunlIfEncapsMethod,
                                     i4FsTunlIfConfigID,
                                     i4SetValFsTunlIfEncapOption) ==
        CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    CfaIncMsrForTunlIfTable (gCfaTnlGblInfo.pTunlGblCxt->u4ContextId,
                             i4FsTunlIfAddressType,
                             pFsTunlIfLocalInetAddress,
                             pFsTunlIfRemoteInetAddress,
                             i4FsTunlIfEncapsMethod,
                             i4FsTunlIfConfigID, 'i',
                             &i4SetValFsTunlIfEncapOption,
                             FsMITunlIfEncapOption,
                             sizeof (FsMITunlIfEncapOption) / sizeof (UINT4),
                             FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsTunlIfAlias
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                setValFsTunlIfAlias
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunlIfAlias (INT4 i4FsTunlIfAddressType,
                     tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                     tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                     INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                     tSNMP_OCTET_STRING_TYPE * pSetValFsTunlIfAlias)
{
    UINT4               u4Index;
    UINT1               au1AliasName[CFA_MAX_PORT_NAME_LENGTH];
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (au1AliasName, 0, CFA_MAX_PORT_NAME_LENGTH);

    /* Store IfAlias in TnlIfTbl */
    if (CfaSetTnlIfAliasInCxt (pTunlCxt, i4FsTunlIfAddressType,
                               pFsTunlIfLocalInetAddress,
                               pFsTunlIfRemoteInetAddress,
                               i4FsTunlIfEncapsMethod,
                               i4FsTunlIfConfigID,
                               pSetValFsTunlIfAlias) == CFA_FAILURE)
    {

        return SNMP_FAILURE;
    }

    /* Copy the alias name from octet string to au1AliasName. This is
     * because, the octet string may not have NULL terminated character
     * and CfaGetInterfaceIndexFromName is expecting a NULL terminated
     * string
     */
    MEMCPY (au1AliasName, pSetValFsTunlIfAlias->pu1_OctetList,
            pSetValFsTunlIfAlias->i4_Length);

    /* Get the IfIndex from IfAlias */
    if (CfaGetInterfaceIndexFromName (au1AliasName, &u4Index) == OSIX_FAILURE)
    {
        /* No such AliasName in gapIfTable */
        return SNMP_FAILURE;
    }

    if (CfaGetIfTnlEntry (u4Index) != NULL)
    {
        /* valid entry already exists - delete the newly
         * create entry and return failure */
        CfaSnmpDeleteTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                    pFsTunlIfLocalInetAddress,
                                    pFsTunlIfRemoteInetAddress,
                                    i4FsTunlIfEncapsMethod, i4FsTunlIfConfigID);
        return SNMP_FAILURE;
    }

    /* Store the corresponding Logical IfIndex in TnlIfTbl */
    CfaSetTnlIfIndexInCxt (pTunlCxt, i4FsTunlIfAddressType,
                           pFsTunlIfLocalInetAddress,
                           pFsTunlIfRemoteInetAddress,
                           i4FsTunlIfEncapsMethod, i4FsTunlIfConfigID, u4Index);

    CfaIncMsrForTunlIfTable (gCfaTnlGblInfo.pTunlGblCxt->u4ContextId,
                             i4FsTunlIfAddressType,
                             pFsTunlIfLocalInetAddress,
                             pFsTunlIfRemoteInetAddress,
                             i4FsTunlIfEncapsMethod,
                             i4FsTunlIfConfigID, 's',
                             pSetValFsTunlIfAlias,
                             FsMITunlIfAlias,
                             sizeof (FsMITunlIfAlias) / sizeof (UINT4), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsTunlIfCksumFlag
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                setValFsTunlIfCksumFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunlIfCksumFlag (INT4 i4FsTunlIfAddressType,
                         tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                         tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                         INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                         INT4 i4SetValFsTunlIfCksumFlag)
{
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (CfaSetTnlIfCksumFlagInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                   pFsTunlIfLocalInetAddress,
                                   pFsTunlIfRemoteInetAddress,
                                   i4FsTunlIfEncapsMethod,
                                   i4FsTunlIfConfigID,
                                   i4SetValFsTunlIfCksumFlag) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    CfaIncMsrForTunlIfTable (gCfaTnlGblInfo.pTunlGblCxt->u4ContextId,
                             i4FsTunlIfAddressType,
                             pFsTunlIfLocalInetAddress,
                             pFsTunlIfRemoteInetAddress,
                             i4FsTunlIfEncapsMethod,
                             i4FsTunlIfConfigID, 'i',
                             &i4SetValFsTunlIfCksumFlag,
                             FsMITunlIfCksumFlag,
                             sizeof (FsMITunlIfCksumFlag) / sizeof (UINT4),
                             FALSE);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsTunlIfPmtuFlag
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                setValFsTunlIfPmtuFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunlIfPmtuFlag (INT4 i4FsTunlIfAddressType,
                        tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                        tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                        INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                        INT4 i4SetValFsTunlIfPmtuFlag)
{
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (CfaSetTnlIfPmtuFlagInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                  pFsTunlIfLocalInetAddress,
                                  pFsTunlIfRemoteInetAddress,
                                  i4FsTunlIfEncapsMethod,
                                  i4FsTunlIfConfigID,
                                  i4SetValFsTunlIfPmtuFlag) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    CfaIncMsrForTunlIfTable (gCfaTnlGblInfo.pTunlGblCxt->u4ContextId,
                             i4FsTunlIfAddressType,
                             pFsTunlIfLocalInetAddress,
                             pFsTunlIfRemoteInetAddress,
                             i4FsTunlIfEncapsMethod,
                             i4FsTunlIfConfigID, 'i',
                             &i4SetValFsTunlIfPmtuFlag,
                             FsMITunlIfPmtuFlag,
                             sizeof (FsMITunlIfPmtuFlag) / sizeof (UINT4),
                             FALSE);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsTunlIfStatus
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                setValFsTunlIfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunlIfStatus (INT4 i4FsTunlIfAddressType,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                      INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                      INT4 i4SetValFsTunlIfStatus)
{
    UINT1               u1RowStatus;
    tTnlIfEntry         TnlIfEntry;
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    tCfaRegInfo         CfaInfo;
    tStackInfoStruct   *pLowStackListScan = NULL;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;
    u1RowStatus = (UINT1) i4SetValFsTunlIfStatus;

    MEMSET (&TnlIfEntry, 0, sizeof (tTnlIfEntry));

    TnlIfEntry.u4AddrType = (UINT4) i4FsTunlIfAddressType;
    TnlIfEntry.u4ContextId = pTunlCxt->u4ContextId;

    if (i4FsTunlIfAddressType == ADDR_TYPE_IPV4)
    {
        PTR_FETCH4 (TnlIfEntry.LocalAddr.Ip4TnlAddr,
                    pFsTunlIfLocalInetAddress->pu1_OctetList);
        PTR_FETCH4 (TnlIfEntry.RemoteAddr.Ip4TnlAddr,
                    pFsTunlIfRemoteInetAddress->pu1_OctetList);
    }
    else if (i4FsTunlIfAddressType == ADDR_TYPE_IPV6)
    {
        MEMCPY (TnlIfEntry.LocalAddr.Ip6TnlAddr,
                pFsTunlIfLocalInetAddress->pu1_OctetList,
                pFsTunlIfLocalInetAddress->i4_Length);
        MEMCPY (TnlIfEntry.RemoteAddr.Ip6TnlAddr,
                pFsTunlIfRemoteInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
    }
    if ((u1RowStatus != CFA_RS_CREATEANDGO) &&
        (u1RowStatus != CFA_RS_CREATEANDWAIT))
    {
        if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                     pFsTunlIfLocalInetAddress,
                                     pFsTunlIfRemoteInetAddress,
                                     i4FsTunlIfEncapsMethod,
                                     i4FsTunlIfConfigID,
                                     pTnlIfEntry) == CFA_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    TnlIfEntry.u4EncapsMethod = (UINT4) i4FsTunlIfEncapsMethod;
    TnlIfEntry.u4ConfigId = (UINT4) i4FsTunlIfConfigID;

    switch (u1RowStatus)
    {
        case CFA_RS_CREATEANDWAIT:
        case CFA_RS_CREATEANDGO:
            TnlIfEntry.u1RowStatus = u1RowStatus;
            if (CfaCreateAndInitTnlIfEntryInCxt (pTunlCxt,
                                                 &TnlIfEntry) == CFA_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        case CFA_RS_ACTIVE:
            /* Validate if row exists */
            if (CfaSetTnlIfActiveStatusInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                              pFsTunlIfLocalInetAddress,
                                              pFsTunlIfRemoteInetAddress,
                                              i4FsTunlIfEncapsMethod,
                                              i4FsTunlIfConfigID) ==
                CFA_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        case CFA_RS_DESTROY:

            /* Notify HL of the deletion in Tunnel parameters */
            MEMSET (&CfaInfo, 0, sizeof (tCfaRegInfo));
            CfaInfo.u4IfIndex = pTnlIfEntry->u4IfIndex;
            CfaInfo.CfaTnlIfInfo.u4TnlMask =
                (TNL_DIR_FLAG_CHG
                 | TNL_DIRECTION_CHG | TNL_ENCAP_OPT_CHG
                 | TNL_ENCAP_LIMIT_CHG | TNL_ENCAP_ADDR_CHG);

            CfaInfo.CfaTnlIfInfo.u4TnlDirFlag = 0;
            CfaInfo.CfaTnlIfInfo.u4TnlDir = 0;
            CfaInfo.CfaTnlIfInfo.u4TnlEncapLmt = 0;
            CfaInfo.CfaTnlIfInfo.u4TnlEncapOpt = 0;
            CfaInfo.CfaTnlIfInfo.u4LocalAddr = 0;
            CfaInfo.CfaTnlIfInfo.u4RemoteAddr = 0;
            CfaInfo.CfaTnlIfInfo.u4TnlType = 0;
            CfaInfo.CfaTnlIfInfo.u4TnlHopLimit = 0;

            CfaNotifyTnlIfUpdate (&CfaInfo);
            CfaSetIfTnlEntry (pTnlIfEntry->u4IfIndex, NULL);
            CfaDeleteTnlIfEntryInCxt (pTunlCxt, pTnlIfEntry->u4IfIndex);

            while (CFA_IF_STACK_LOW_ENTRY (pTnlIfEntry->u4IfIndex))
            {
                pLowStackListScan = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY
                    (pTnlIfEntry->u4IfIndex);

                CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);
                if (CFA_IF_STACK_IFINDEX (pLowStackListScan) != CFA_NONE)
                {
                    /* First Delete this entry from the lower layer's stack */
                    CfaIfmDeleteStackEntry
                        (CFA_IF_STACK_IFINDEX
                         (pLowStackListScan),
                         CFA_HIGH, (UINT2) pTnlIfEntry->u4IfIndex);

                    CfaIfmDeleteStackEntry ((UINT2) pTnlIfEntry->u4IfIndex,
                                            CFA_LOW, CFA_IF_STACK_IFINDEX
                                            (pLowStackListScan));
                }
                else
                {
                    CfaIfmUpdateStackForDeletion (&CFA_IF_STACK_LOW
                                                  (pTnlIfEntry->u4IfIndex),
                                                  CFA_FALSE, CFA_NONE);
                }
            }
            break;

        case CFA_RS_NOTINSERVICE:
            if (CfaSetTnlIfNotInServiceStatusInCxt
                (pTunlCxt, i4FsTunlIfAddressType, pFsTunlIfLocalInetAddress,
                 pFsTunlIfRemoteInetAddress, i4FsTunlIfEncapsMethod,
                 i4FsTunlIfConfigID) == CFA_FAILURE)
            {
                return SNMP_FAILURE;
            }

            /* OperStatus is made down and notification is given to HL */
            CfaSetIfOperStatus (pTnlIfEntry->u4IfIndex, CFA_IF_DOWN);
            /* TODO : Change According to Context of execution */
            if (CfaInterfaceStatusChangeIndication ((UINT2)
                                                    pTnlIfEntry->u4IfIndex,
                                                    CFA_IF_DOWN) == CFA_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        default:
            break;
    }

    CfaIncMsrForTunlIfTable (gCfaTnlGblInfo.pTunlGblCxt->u4ContextId,
                             i4FsTunlIfAddressType,
                             pFsTunlIfLocalInetAddress,
                             pFsTunlIfRemoteInetAddress,
                             i4FsTunlIfEncapsMethod,
                             i4FsTunlIfConfigID, 'i',
                             &i4SetValFsTunlIfStatus,
                             FsMITunlIfStatus,
                             sizeof (FsMITunlIfStatus) / sizeof (UINT4), TRUE);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsTunlIfHopLimit
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                testValFsTunlIfHopLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunlIfHopLimit (UINT4 *pu4ErrorCode, INT4 i4FsTunlIfAddressType,
                           tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                           tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                           INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                           INT4 i4TestValFsTunlIfHopLimit)
{
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsTunlIfTable (i4FsTunlIfAddressType,
                                               pFsTunlIfLocalInetAddress,
                                               pFsTunlIfRemoteInetAddress,
                                               i4FsTunlIfEncapsMethod,
                                               i4FsTunlIfConfigID) !=
        SNMP_SUCCESS)
    {
        /* Entry not present */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValFsTunlIfHopLimit < MIN_TUNL_IF_HOP_LIMIT ||
        i4TestValFsTunlIfHopLimit > MAX_TUNL_IF_HOP_LIMIT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4TestValFsTunlIfHopLimit);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunlIfTOS
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                testValFsTunlIfTOS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunlIfTOS (UINT4 *pu4ErrorCode, INT4 i4FsTunlIfAddressType,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                      INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                      INT4 i4TestValFsTunlIfTOS)
{
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    UNUSED_PARAM (i4TestValFsTunlIfTOS);

    if (pTunlCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceFsTunlIfTable (i4FsTunlIfAddressType,
                                               pFsTunlIfLocalInetAddress,
                                               pFsTunlIfRemoteInetAddress,
                                               i4FsTunlIfEncapsMethod,
                                               i4FsTunlIfConfigID) !=
        SNMP_SUCCESS)
    {
        /* Entry not present */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValFsTunlIfTOS < MIN_TUNL_IF_TOS ||
        i4TestValFsTunlIfTOS > MAX_TUNL_IF_TOS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunlIfFlowLabel
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                testValFsTunlIfFlowLabel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunlIfFlowLabel (UINT4 *pu4ErrorCode, INT4 i4FsTunlIfAddressType,
                            tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                            tSNMP_OCTET_STRING_TYPE *
                            pFsTunlIfRemoteInetAddress,
                            INT4 i4FsTunlIfEncapsMethod,
                            INT4 i4FsTunlIfConfigID,
                            INT4 i4TestValFsTunlIfFlowLabel)
{
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsTunlIfTable (i4FsTunlIfAddressType,
                                               pFsTunlIfLocalInetAddress,
                                               pFsTunlIfRemoteInetAddress,
                                               i4FsTunlIfEncapsMethod,
                                               i4FsTunlIfConfigID) !=
        SNMP_SUCCESS)
    {
        /* Entry not present */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValFsTunlIfFlowLabel < MIN_TUNL_IF_FLOWLABEL ||
        i4TestValFsTunlIfFlowLabel > MAX_TUNL_IF_FLOWLABEL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4TestValFsTunlIfFlowLabel);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunlIfDirFlag
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                testValFsTunlIfDirFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunlIfDirFlag (UINT4 *pu4ErrorCode, INT4 i4FsTunlIfAddressType,
                          tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                          tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                          INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                          INT4 i4TestValFsTunlIfDirFlag)
{
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsTunlIfTable (i4FsTunlIfAddressType,
                                               pFsTunlIfLocalInetAddress,
                                               pFsTunlIfRemoteInetAddress,
                                               i4FsTunlIfEncapsMethod,
                                               i4FsTunlIfConfigID) !=
        SNMP_SUCCESS)
    {
        /* Entry not present */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsTunlIfDirFlag != TNL_UNIDIRECTIONAL) &&
        (i4TestValFsTunlIfDirFlag != TNL_BIDIRECTIONAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunlIfDirection
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                testValFsTunlIfDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunlIfDirection (UINT4 *pu4ErrorCode, INT4 i4FsTunlIfAddressType,
                            tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                            tSNMP_OCTET_STRING_TYPE *
                            pFsTunlIfRemoteInetAddress,
                            INT4 i4FsTunlIfEncapsMethod,
                            INT4 i4FsTunlIfConfigID,
                            INT4 i4TestValFsTunlIfDirection)
{
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsTunlIfTable (i4FsTunlIfAddressType,
                                               pFsTunlIfLocalInetAddress,
                                               pFsTunlIfRemoteInetAddress,
                                               i4FsTunlIfEncapsMethod,
                                               i4FsTunlIfConfigID) !=
        SNMP_SUCCESS)
    {
        /* Entry not present */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsTunlIfDirection != TNL_INCOMING) &&
        (i4TestValFsTunlIfDirection != TNL_OUTGOING))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunlIfEncaplmt
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                testValFsTunlIfEncaplmt
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunlIfEncaplmt (UINT4 *pu4ErrorCode, INT4 i4FsTunlIfAddressType,
                           tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                           tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                           INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                           UINT4 u4TestValFsTunlIfEncaplmt)
{
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;
    UNUSED_PARAM (u4TestValFsTunlIfEncaplmt);

    if (pTunlCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceFsTunlIfTable (i4FsTunlIfAddressType,
                                               pFsTunlIfLocalInetAddress,
                                               pFsTunlIfRemoteInetAddress,
                                               i4FsTunlIfEncapsMethod,
                                               i4FsTunlIfConfigID) !=
        SNMP_SUCCESS)
    {
        /* Entry not present */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunlIfEncapOption
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                testValFsTunlIfEncapOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunlIfEncapOption (UINT4 *pu4ErrorCode, INT4 i4FsTunlIfAddressType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsTunlIfLocalInetAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsTunlIfRemoteInetAddress,
                              INT4 i4FsTunlIfEncapsMethod,
                              INT4 i4FsTunlIfConfigID,
                              INT4 i4TestValFsTunlIfEncapOption)
{
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsTunlIfTable (i4FsTunlIfAddressType,
                                               pFsTunlIfLocalInetAddress,
                                               pFsTunlIfRemoteInetAddress,
                                               i4FsTunlIfEncapsMethod,
                                               i4FsTunlIfConfigID) !=
        SNMP_SUCCESS)
    {
        /* Entry not present */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsTunlIfEncapOption != TNL_ENCAP_ENABLE) &&
        (i4TestValFsTunlIfEncapOption != TNL_ENCAP_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunlIfAlias
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                testValFsTunlIfAlias
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunlIfAlias (UINT4 *pu4ErrorCode, INT4 i4FsTunlIfAddressType,
                        tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                        tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                        INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                        tSNMP_OCTET_STRING_TYPE * pTestValFsTunlIfAlias)
{
    UINT4               u4Index;
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;
    if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }

    if (pTnlIfEntry == NULL)
    {
        /* Entry not present in TnlIfTbl */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* IfAlias is a mandatory argument before setting the RowStatus to ACTIVE */
    if (pTnlIfEntry->u1RowStatus == CFA_RS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pTestValFsTunlIfAlias->i4_Length < MIN_TUNL_IF_ALIAS_LEN ||
        pTestValFsTunlIfAlias->i4_Length > MAX_TUNL_IF_ALIAS_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    /* NVT Check */
#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValFsTunlIfAlias->pu1_OctetList,
                              pTestValFsTunlIfAlias->i4_Length) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    /* Validate the existance AliasName in gapIfTable */
    if (CfaGetInterfaceIndexFromName (pTestValFsTunlIfAlias->pu1_OctetList,
                                      &u4Index) == OSIX_FAILURE)
    {
        /* No such Alias Name in gapIfTable */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunlIfCksumFlag
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                testValFsTunlIfCksumFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunlIfCksumFlag (UINT4 *pu4ErrorCode, INT4 i4FsTunlIfAddressType,
                            tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                            tSNMP_OCTET_STRING_TYPE *
                            pFsTunlIfRemoteInetAddress,
                            INT4 i4FsTunlIfEncapsMethod,
                            INT4 i4FsTunlIfConfigID,
                            INT4 i4TestValFsTunlIfCksumFlag)
{
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;
    if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }
    if (pTnlIfEntry == NULL)
    {
        /* Entry not present */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* checksum enabling/disabling is allowed only for GRE tunnels */
    if (pTnlIfEntry->u4EncapsMethod != TNL_TYPE_GRE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsTunlIfCksumFlag != TNL_CKSUM_ENABLE) &&
        (i4TestValFsTunlIfCksumFlag != TNL_CKSUM_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pTnlIfEntry->bCksumFlag == i4TestValFsTunlIfCksumFlag)
    {
        /* Value already set. No change required */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunlIfPmtuFlag
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                testValFsTunlIfPmtuFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunlIfPmtuFlag (UINT4 *pu4ErrorCode, INT4 i4FsTunlIfAddressType,
                           tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                           tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                           INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                           INT4 i4TestValFsTunlIfPmtuFlag)
{
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;
    if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                 pFsTunlIfLocalInetAddress,
                                 pFsTunlIfRemoteInetAddress,
                                 i4FsTunlIfEncapsMethod,
                                 i4FsTunlIfConfigID,
                                 pTnlIfEntry) == CFA_FAILURE)
    {
        pTnlIfEntry = NULL;
    }

    if (pTnlIfEntry == NULL)
    {
        /* Entry not present */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* path-mtu-discovery enabling/disabling is allowed only for configured 
     * tunnels */
    if ((pTnlIfEntry->u4EncapsMethod != TNL_TYPE_IPV6IP) &&
        (pTnlIfEntry->u4EncapsMethod != TNL_TYPE_GRE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsTunlIfPmtuFlag != TNL_PMTU_ENABLE) &&
        (i4TestValFsTunlIfPmtuFlag != TNL_PMTU_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pTnlIfEntry->bPmtuFlag == i4TestValFsTunlIfPmtuFlag)
    {
        /* Value already set. No change in bPmtuFlag */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunlIfStatus
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID

                The Object 
                testValFsTunlIfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunlIfStatus (UINT4 *pu4ErrorCode, INT4 i4FsTunlIfAddressType,
                         tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                         tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                         INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                         INT4 i4TestValFsTunlIfStatus)
{
    tTnlIfNode          TnlIfNode;
    tTnlIfNode         *pTnlIfEntry = NULL;
    UINT4               u4SrcIpAddr;
    UINT4               u4DestIpAddr;
    UINT4               u4IfIndex;
    tCfaTnlCxt         *pTunlCxt = gCfaTnlGblInfo.pTunlGblCxt;

    if (pTunlCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pTnlIfEntry = &TnlIfNode;
    switch (i4TestValFsTunlIfStatus)
    {
        case CFA_RS_CREATEANDWAIT:
        case CFA_RS_CREATEANDGO:
        {
            /* check if the given src addr is a valid interface ip addr */
            PTR_FETCH4 (u4SrcIpAddr, pFsTunlIfLocalInetAddress->pu1_OctetList);

            if (CfaIpIfGetIfIndexFromIpAddressInCxt (pTunlCxt->u4ContextId,
                                                     u4SrcIpAddr, &u4IfIndex)
                == CFA_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_CFA_INVALID_TUNNEL_ENTRY);
                return SNMP_FAILURE;
            }

            /* check if the given dest addr is a valid unicast ip addr */
            PTR_FETCH4 (u4DestIpAddr,
                        pFsTunlIfRemoteInetAddress->pu1_OctetList);

            /* Destination Address validation */
            if (((u4DestIpAddr & CFA_ADDR_LOOPBACK) == CFA_ADDR_LOOPBACK) ||
                (u4DestIpAddr == CFA_ADDR_BROADCAST) ||
                (u4DestIpAddr >= CFA_ADDR_MCAST1))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_CFA_INVALID_TUNNEL_ENTRY);
                return SNMP_FAILURE;
            }

            /* Destination address should not belong to our interface. */
            if ((u4DestIpAddr != 0) &&
                (CfaIpIfGetIfIndexFromIpAddressInCxt (pTunlCxt->u4ContextId,
                                                      u4DestIpAddr,
                                                      &u4IfIndex) ==
                 CFA_SUCCESS))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_CFA_INVALID_TUNNEL_ENTRY);
                return SNMP_FAILURE;
            }

            /* validate the config-id - currently supports one/two 
             * tunnel per set of endpoint addresses. 
             * 2 tunnels are allowed for a set of end-point addresses. 
             * i.e., both uni-directional 
             * (1st set: src-incoming, dest-outgoing,
             *  2nd set: src-outgoing, dest-incoming) */
            if ((i4FsTunlIfConfigID != 1) && (i4FsTunlIfConfigID != 2))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_CFA_INVALID_TUNNEL_CONF);
                return SNMP_FAILURE;
            }

            switch (i4FsTunlIfEncapsMethod)
            {
                case TNL_TYPE_ISATAP:
#ifdef OPENFLOW_WANTED
                case TNL_TYPE_OPENFLOW:
#endif /* OPENFLOW_WANTED */
                case TNL_TYPE_COMPAT:
                case TNL_TYPE_SIXTOFOUR:
                    if (CfaSnmpGetMatchingEntryInCxt
                        (pTunlCxt, (UINT4) i4FsTunlIfEncapsMethod,
                         pFsTunlIfLocalInetAddress, pTnlIfEntry) == CFA_SUCCESS)
                    {
                        /* Entry already present */
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (CLI_CFA_INVALID_TUNNEL_ENTRY);
                        return SNMP_FAILURE;
                    }
                    break;

                case TNL_TYPE_IPV6IP:
                case TNL_TYPE_GRE:
                    /* DestIpAddr should not be configured as 0 for 
                     * these tunnel types */
                    if (u4DestIpAddr == CFA_ADDR_ANY)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (CLI_CFA_INVALID_TUNNEL_ENTRY);
                        return SNMP_FAILURE;
                    }

                    /* cannot have multiple entries for same src & dest. 
                     * can have more than one entry for same src and 
                     * different dest */
                    if (CfaSnmpGetTnlEntryInCxt
                        (pTunlCxt, i4FsTunlIfAddressType,
                         pFsTunlIfLocalInetAddress, pFsTunlIfRemoteInetAddress,
                         i4FsTunlIfEncapsMethod, i4FsTunlIfConfigID,
                         pTnlIfEntry) == CFA_FAILURE)
                    {
                        pTnlIfEntry = NULL;
                    }
                    if (pTnlIfEntry != NULL)
                    {
                        /* Entry present */
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (CLI_CFA_INVALID_TUNNEL_ENTRY);
                        return SNMP_FAILURE;
                    }

                    /* Configured/Gre Tunnel's Source addres should not
                     * conflict with automatic tunnel source address. */
                    pTnlIfEntry =
                        CfaTnlCheckSourceAddrInCxt (pTunlCxt,
                                                    pFsTunlIfLocalInetAddress);

                    if (pTnlIfEntry != NULL)
                    {
                        /* Entry present */
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (CLI_CFA_INVALID_TUNNEL_ENTRY);
                        return SNMP_FAILURE;
                    }
                    break;

                default:
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_CFA_INVALID_TUNNEL_ENTRY);
                    return SNMP_FAILURE;
            }
            break;
        }
        case CFA_RS_ACTIVE:
        case CFA_RS_DESTROY:
        case CFA_RS_NOTINSERVICE:
        {
            if (CfaSnmpGetTnlEntryInCxt (pTunlCxt, i4FsTunlIfAddressType,
                                         pFsTunlIfLocalInetAddress,
                                         pFsTunlIfRemoteInetAddress,
                                         i4FsTunlIfEncapsMethod,
                                         i4FsTunlIfConfigID,
                                         pTnlIfEntry) == CFA_FAILURE)
            {
                pTnlIfEntry = NULL;
            }
            if (pTnlIfEntry == NULL)
            {
                /* Entry not present */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_CFA_INVALID_TUNNEL_ENTRY);
                return SNMP_FAILURE;
            }

            /*if active is tried, but the tunnel interface index is not yet created,
             * reject the input */
            if ((i4TestValFsTunlIfStatus == CFA_RS_ACTIVE) &&
                (pTnlIfEntry->u4IfIndex == 0))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_CFA_INVALID_TUNNEL_ENTRY);
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Exiting nmhTestv2FsTunlIfStatus - "
                          "Invalid Tunnel Interface index (%d) - FAILED\n",
                          pTnlIfEntry->u4IfIndex);
                return SNMP_FAILURE;
            }

            break;
        }
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_CFA_INVALID_TUNNEL_ENTRY);
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsTunlIfTable
 Input       :  The Indices
                FsTunlIfAddressType
                FsTunlIfLocalInetAddress
                FsTunlIfRemoteInetAddress
                FsTunlIfEncapsMethod
                FsTunlIfConfigID
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTunlIfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

INT4
CfaSnmpGetTnlEntryInCxt (tCfaTnlCxt * pTunlCxt, INT4 i4FsTunlIfAddressType,
                         tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                         tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                         INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                         tTnlIfNode * pTnlIfEntry)
{
    UINT4               u4AddrType;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4DestIpAddr = 0;
    UINT1               au1SrcIp6Addr[16];
    UINT1               au1DestIp6Addr[16];
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;

    u4AddrType = (UINT4) i4FsTunlIfAddressType;
    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        PTR_FETCH4 (u4SrcIpAddr, pFsTunlIfLocalInetAddress->pu1_OctetList);
        PTR_FETCH4 (u4DestIpAddr, pFsTunlIfRemoteInetAddress->pu1_OctetList);
    }
    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        MEMCPY (au1SrcIp6Addr, pFsTunlIfLocalInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
        MEMCPY (au1DestIp6Addr, pFsTunlIfRemoteInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
    }
    u4EncapsMethod = (UINT4) i4FsTunlIfEncapsMethod;
    u4ConfigId = (UINT4) i4FsTunlIfConfigID;

    /* Check if entry is already present */
    if ((CfaGetTnlInCxt (pTunlCxt, u4AddrType, u4SrcIpAddr, u4DestIpAddr,
                         au1SrcIp6Addr, au1DestIp6Addr, u4EncapsMethod,
                         u4ConfigId, pTnlIfEntry)) == CFA_SUCCESS)
    {
        /* Entry present */
        return CFA_SUCCESS;
    }

    /* Entry not present */
    pTnlIfEntry = NULL;
    return CFA_FAILURE;
}

INT4
CfaSnmpDeleteTnlEntryInCxt (tCfaTnlCxt * pTunlCxt, INT4 i4FsTunlIfAddressType,
                            tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                            tSNMP_OCTET_STRING_TYPE *
                            pFsTunlIfRemoteInetAddress,
                            INT4 i4FsTunlIfEncapsMethod,
                            INT4 i4FsTunlIfConfigID)
{
    UINT4               u4AddrType;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4DestIpAddr = 0;
    UINT1               au1SrcIp6Addr[16];
    UINT1               au1DestIp6Addr[16];
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;
    tTnlIfNode          TnlNode;
    tTnlIfNode         *pNode = NULL;

    pNode = &TnlNode;
    u4AddrType = (UINT4) i4FsTunlIfAddressType;
    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        PTR_FETCH4 (u4SrcIpAddr, pFsTunlIfLocalInetAddress->pu1_OctetList);
        PTR_FETCH4 (u4DestIpAddr, pFsTunlIfRemoteInetAddress->pu1_OctetList);
    }
    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        MEMCPY (au1SrcIp6Addr, pFsTunlIfLocalInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
        MEMCPY (au1DestIp6Addr, pFsTunlIfRemoteInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
    }
    u4EncapsMethod = (UINT4) i4FsTunlIfEncapsMethod;
    u4ConfigId = (UINT4) i4FsTunlIfConfigID;

    if ((CfaGetTnlInCxt (pTunlCxt, u4AddrType, u4SrcIpAddr, u4DestIpAddr,
                         au1SrcIp6Addr, au1DestIp6Addr, u4EncapsMethod,
                         u4ConfigId, pNode)) == CFA_SUCCESS)
    {
        if (CfaDeleteTnlIfEntryInCxt (pTunlCxt, pNode->u4IfIndex) ==
            CFA_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_FAILURE;
}

INT4
CfaSnmpGetMatchingEntryInCxt (tCfaTnlCxt * pTunlCxt, UINT4 u4EncapsMethod,
                              tSNMP_OCTET_STRING_TYPE * pTunlIfLocalAddress,
                              tTnlIfNode * pNode)
{
    if ((CfaSnmpGetMatchingAutoTnlEntryInCxt (pTunlCxt, u4EncapsMethod,
                                              pTunlIfLocalAddress,
                                              pNode)) == CFA_SUCCESS)
    {
        return CFA_SUCCESS;
    }
    pNode = NULL;
    return CFA_FAILURE;
}
