
/* $Id: ifmibnc.c,v 1.1 2016/06/16 12:09:43 siva Exp $
    ISS Wrapper module
    module IF-MIB

 */

#include "lr.h"
#include "cfa.h"
#include "fssnmp.h"
/*#include "ifmibwr.h"*/
#include "ifmiblow.h"
/*#include "ifmibdb.h"*/
#include "ifmibnc.h"
#include "cli.h"

/********************************************************************
* FUNCTION NcIfNumberGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfNumberGet (
                INT4 *pi4IfNumber )
{

    INT1 i1RetVal;

    i1RetVal = nmhGetIfNumber(
                 pi4IfNumber );

    return i1RetVal;


} /* NcIfNumberGet */

/********************************************************************
* FUNCTION NcIfTableLastChangeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfTableLastChangeGet (
                UINT4 *pu4IfTableLastChange )
{

    INT1 i1RetVal;

    i1RetVal = nmhGetIfTableLastChange(
                 pu4IfTableLastChange );

    return i1RetVal;


} /* NcIfTableLastChangeGet */

/********************************************************************
* FUNCTION NcIfStackLastChangeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfStackLastChangeGet (
                UINT4 *pu4IfStackLastChange )
{

    INT1 i1RetVal;

    i1RetVal = nmhGetIfStackLastChange(
                 pu4IfStackLastChange );

    return i1RetVal;


} /* NcIfStackLastChangeGet */

/********************************************************************
* FUNCTION NcIfDescrGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfDescrGet (
                INT4 i4IfIndex,
                UINT1 *pIfDescr )
{

    INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE	FsIfDescr;
	MEMSET (pIfDescr, '\0', CFA_MAX_DESCR_LENGTH);
	MEMSET (&FsIfDescr, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

	FsIfDescr.pu1_OctetList = pIfDescr;
    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfDescr(
                 i4IfIndex,
                 &FsIfDescr );

    return i1RetVal;


} /* NcIfDescrGet */

/********************************************************************
* FUNCTION NcIfTypeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfTypeGet (
                INT4 i4IfIndex,
                INT4 *pi4IfType )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfType(
                 i4IfIndex,
                 pi4IfType );

    return i1RetVal;


} /* NcIfTypeGet */

/********************************************************************
* FUNCTION NcIfMtuGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfMtuGet (
                INT4 i4IfIndex,
                INT4 *pi4IfMtu )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfMtu(
                 i4IfIndex,
                 pi4IfMtu );

    return i1RetVal;


} /* NcIfMtuGet */

/********************************************************************
* FUNCTION NcIfSpeedGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfSpeedGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfSpeed )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfSpeed(
                 i4IfIndex,
                 pu4IfSpeed );

    return i1RetVal;


} /* NcIfSpeedGet */

/********************************************************************
* FUNCTION NcIfPhysAddressGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfPhysAddressGet (
                INT4 i4IfIndex,
                UINT1 *pIfPhysAddress )
{

    INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE	FsIfPhysAddress;
	UINT1	au1Address[CFA_ENET_ADDR_LEN];
	

	MEMSET (au1Address, '\0', CFA_ENET_ADDR_LEN);
	/*MEMSET (pIfPhysAddress, '\0', 25); */   /*MAX_MAC_LENGTH is macro for 25 */
	MEMSET (&FsIfPhysAddress, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

	FsIfPhysAddress.pu1_OctetList = au1Address;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfPhysAddress(
                 i4IfIndex,
                 &FsIfPhysAddress );

	CliMacToStr (au1Address, pIfPhysAddress);

    return i1RetVal;


} /* NcIfPhysAddressGet */

/********************************************************************
* FUNCTION NcIfAdminStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfAdminStatusSet (
                INT4 i4IfIndex,
                INT4 i4IfAdminStatus )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetIfAdminStatus(
                 i4IfIndex,
                i4IfAdminStatus);

    return i1RetVal;


} /* NcIfAdminStatusSet */

/********************************************************************
* FUNCTION NcIfAdminStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfAdminStatusTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                INT4 i4IfAdminStatus )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2IfAdminStatus(pu4Error,
                 i4IfIndex,
                i4IfAdminStatus);

    return i1RetVal;


} /* NcIfAdminStatusTest */

/********************************************************************
* FUNCTION NcIfOperStatusGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfOperStatusGet (
                INT4 i4IfIndex,
                INT4 *pi4IfOperStatus )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfOperStatus(
                 i4IfIndex,
                 pi4IfOperStatus );

    return i1RetVal;


} /* NcIfOperStatusGet */

/********************************************************************
* FUNCTION NcIfLastChangeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfLastChangeGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfLastChange )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfLastChange(
                 i4IfIndex,
                 pu4IfLastChange );

    return i1RetVal;


} /* NcIfLastChangeGet */

/********************************************************************
* FUNCTION NcIfInOctetsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfInOctetsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfInOctets )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfInOctets(
                 i4IfIndex,
                 pu4IfInOctets );

    return i1RetVal;


} /* NcIfInOctetsGet */

/********************************************************************
* FUNCTION NcIfInUcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfInUcastPktsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfInUcastPkts )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfInUcastPkts(
                 i4IfIndex,
                 pu4IfInUcastPkts );

    return i1RetVal;


} /* NcIfInUcastPktsGet */

/********************************************************************
* FUNCTION NcIfInNUcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfInNUcastPktsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfInNUcastPkts )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfInNUcastPkts(
                 i4IfIndex,
                 pu4IfInNUcastPkts );

    return i1RetVal;


} /* NcIfInNUcastPktsGet */

/********************************************************************
* FUNCTION NcIfInDiscardsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfInDiscardsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfInDiscards )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfInDiscards(
                 i4IfIndex,
                 pu4IfInDiscards );

    return i1RetVal;


} /* NcIfInDiscardsGet */

/********************************************************************
* FUNCTION NcIfInErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfInErrorsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfInErrors )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfInErrors(
                 i4IfIndex,
                 pu4IfInErrors );

    return i1RetVal;


} /* NcIfInErrorsGet */

/********************************************************************
* FUNCTION NcIfInUnknownProtosGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfInUnknownProtosGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfInUnknownProtos )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfInUnknownProtos(
                 i4IfIndex,
                 pu4IfInUnknownProtos );

    return i1RetVal;


} /* NcIfInUnknownProtosGet */

/********************************************************************
* FUNCTION NcIfOutOctetsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfOutOctetsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfOutOctets )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfOutOctets(
                 i4IfIndex,
                 pu4IfOutOctets );

    return i1RetVal;


} /* NcIfOutOctetsGet */

/********************************************************************
* FUNCTION NcIfOutUcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfOutUcastPktsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfOutUcastPkts )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfOutUcastPkts(
                 i4IfIndex,
                 pu4IfOutUcastPkts );

    return i1RetVal;


} /* NcIfOutUcastPktsGet */

/********************************************************************
* FUNCTION NcIfOutNUcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfOutNUcastPktsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfOutNUcastPkts )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfOutNUcastPkts(
                 i4IfIndex,
                 pu4IfOutNUcastPkts );

    return i1RetVal;


} /* NcIfOutNUcastPktsGet */

/********************************************************************
* FUNCTION NcIfOutDiscardsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfOutDiscardsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfOutDiscards )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfOutDiscards(
                 i4IfIndex,
                 pu4IfOutDiscards );

    return i1RetVal;


} /* NcIfOutDiscardsGet */

/********************************************************************
* FUNCTION NcIfOutErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfOutErrorsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfOutErrors )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfOutErrors(
                 i4IfIndex,
                 pu4IfOutErrors );

    return i1RetVal;


} /* NcIfOutErrorsGet */

/********************************************************************
* FUNCTION NcIfOutQLenGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfOutQLenGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfOutQLen )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfOutQLen(
                 i4IfIndex,
                 pu4IfOutQLen );

    return i1RetVal;


} /* NcIfOutQLenGet */

/********************************************************************
* FUNCTION NcIfSpecificGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfSpecificGet (
                INT4 i4IfIndex,
                UINT1 *pIfSpecific )
{

    INT1 i1RetVal = SNMP_SUCCESS;
	UNUSED_PARAM (i4IfIndex);
	UNUSED_PARAM (pIfSpecific);
    /*if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfSpecific(
                 i4IfIndex,
                 pIfSpecific );*/

    return i1RetVal;


} /* NcIfSpecificGet */

/********************************************************************
* FUNCTION NcIfNameGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfNameGet (
                INT4 i4IfIndex,
                UINT1 *pIfName )
{

    INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE	IfName;

	MEMSET (&IfName, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
	MEMSET (pIfName, '\0', CFA_MAX_IFALIAS_LENGTH);

	IfName.pu1_OctetList = pIfName;
    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfName(
                 i4IfIndex,
                 &IfName );

    return i1RetVal;


} /* NcIfNameGet */

/********************************************************************
* FUNCTION NcIfInMulticastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfInMulticastPktsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfInMulticastPkts )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfInMulticastPkts(
                 i4IfIndex,
                 pu4IfInMulticastPkts );

    return i1RetVal;


} /* NcIfInMulticastPktsGet */

/********************************************************************
* FUNCTION NcIfInBroadcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfInBroadcastPktsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfInBroadcastPkts )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfInBroadcastPkts(
                 i4IfIndex,
                 pu4IfInBroadcastPkts );

    return i1RetVal;


} /* NcIfInBroadcastPktsGet */

/********************************************************************
* FUNCTION NcIfOutMulticastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfOutMulticastPktsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfOutMulticastPkts )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfOutMulticastPkts(
                 i4IfIndex,
                 pu4IfOutMulticastPkts );

    return i1RetVal;


} /* NcIfOutMulticastPktsGet */

/********************************************************************
* FUNCTION NcIfOutBroadcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfOutBroadcastPktsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfOutBroadcastPkts )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfOutBroadcastPkts(
                 i4IfIndex,
                 pu4IfOutBroadcastPkts );

    return i1RetVal;


} /* NcIfOutBroadcastPktsGet */

/********************************************************************
* FUNCTION NcIfHCInOctetsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfHCInOctetsGet (
                INT4 i4IfIndex,
                unsigned long long *pu8IfHCInOctets )
{

    INT1 i1RetVal;
	tSNMP_COUNTER64_TYPE	IfHCInOctets;

	MEMSET (&IfHCInOctets, 0, sizeof(tSNMP_COUNTER64_TYPE));

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfHCInOctets(
                 i4IfIndex,
                 &IfHCInOctets);

	*pu8IfHCInOctets = ((*pu8IfHCInOctets | IfHCInOctets.msn) << 32) | IfHCInOctets.lsn;

    return i1RetVal;


} /* NcIfHCInOctetsGet */

/********************************************************************
* FUNCTION NcIfHCInUcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfHCInUcastPktsGet (
                INT4 i4IfIndex,
                unsigned long long *pu8IfHCInUcastPkts )
{

    INT1 i1RetVal;
	tSNMP_COUNTER64_TYPE	IfHCInUcastPkts;

	MEMSET (&IfHCInUcastPkts, 0, sizeof(tSNMP_COUNTER64_TYPE));

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfHCInUcastPkts(
                 i4IfIndex,
                 &IfHCInUcastPkts);
	*pu8IfHCInUcastPkts = ((*pu8IfHCInUcastPkts | IfHCInUcastPkts.msn) << 32) | IfHCInUcastPkts.lsn;

    return i1RetVal;


} /* NcIfHCInUcastPktsGet */

/********************************************************************
* FUNCTION NcIfHCInMulticastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfHCInMulticastPktsGet (
                INT4 i4IfIndex,
                unsigned long long *pu8IfHCInMulticastPkts )
{

    INT1 i1RetVal;
	tSNMP_COUNTER64_TYPE	IfHCInMulticastPkts;

	MEMSET (&IfHCInMulticastPkts, 0, sizeof(tSNMP_COUNTER64_TYPE));

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfHCInMulticastPkts(
                 i4IfIndex,
                 &IfHCInMulticastPkts );
	
	*pu8IfHCInMulticastPkts = ((*pu8IfHCInMulticastPkts | IfHCInMulticastPkts.msn) << 32)| IfHCInMulticastPkts.lsn;
    return i1RetVal;


} /* NcIfHCInMulticastPktsGet */

/********************************************************************
* FUNCTION NcIfHCInBroadcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfHCInBroadcastPktsGet (
                INT4 i4IfIndex,
                unsigned long long *pu8IfHCInBroadcastPkts )
{

    INT1 i1RetVal;
	tSNMP_COUNTER64_TYPE	IfHCInBroadcastPkts;

	MEMSET (&IfHCInBroadcastPkts, 0, sizeof(tSNMP_COUNTER64_TYPE));

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfHCInBroadcastPkts(
                 i4IfIndex,
                 &IfHCInBroadcastPkts);

	*pu8IfHCInBroadcastPkts = ((*pu8IfHCInBroadcastPkts | IfHCInBroadcastPkts.msn) << 32)| IfHCInBroadcastPkts.lsn;

    return i1RetVal;


} /* NcIfHCInBroadcastPktsGet */

/********************************************************************
* FUNCTION NcIfHCOutOctetsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfHCOutOctetsGet (
                INT4 i4IfIndex,
                unsigned long long *pu8IfHCOutOctets )
{

    INT1 i1RetVal;
	tSNMP_COUNTER64_TYPE	IfHCOutOctets;

	MEMSET (&IfHCOutOctets, 0, sizeof(tSNMP_COUNTER64_TYPE));

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfHCOutOctets(
                 i4IfIndex,
                 &IfHCOutOctets);

	*pu8IfHCOutOctets = ((*pu8IfHCOutOctets | IfHCOutOctets.msn) << 32)| IfHCOutOctets.lsn;

    return i1RetVal;


} /* NcIfHCOutOctetsGet */

/********************************************************************
* FUNCTION NcIfHCOutUcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfHCOutUcastPktsGet (
                INT4 i4IfIndex,
                unsigned long long *pu8IfHCOutUcastPkts )
{

    INT1 i1RetVal;
	tSNMP_COUNTER64_TYPE	IfHCOutUcastPkts;

	MEMSET (&IfHCOutUcastPkts, 0, sizeof(tSNMP_COUNTER64_TYPE));

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfHCOutUcastPkts(
                 i4IfIndex,
                 &IfHCOutUcastPkts);

	*pu8IfHCOutUcastPkts= ((*pu8IfHCOutUcastPkts | IfHCOutUcastPkts.msn) << 32)| IfHCOutUcastPkts.lsn;

    return i1RetVal;


} /* NcIfHCOutUcastPktsGet */

/********************************************************************
* FUNCTION NcIfHCOutMulticastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfHCOutMulticastPktsGet (
                INT4 i4IfIndex,
                unsigned long long *pu8IfHCOutMulticastPkts )
{

    INT1 i1RetVal;
	tSNMP_COUNTER64_TYPE	IfHCOutMulticastPkts;

	MEMSET (&IfHCOutMulticastPkts, 0, sizeof(tSNMP_COUNTER64_TYPE));

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfHCOutMulticastPkts(
                 i4IfIndex,
                 &IfHCOutMulticastPkts);
	
	*pu8IfHCOutMulticastPkts = ((*pu8IfHCOutMulticastPkts | IfHCOutMulticastPkts.msn) << 32)| IfHCOutMulticastPkts.lsn;

    return i1RetVal;


} /* NcIfHCOutMulticastPktsGet */

/********************************************************************
* FUNCTION NcIfHCOutBroadcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfHCOutBroadcastPktsGet (
                INT4 i4IfIndex,
                unsigned long long *pu8IfHCOutBroadcastPkts )
{

    INT1 i1RetVal;
	tSNMP_COUNTER64_TYPE	IfHCOutBroadcastPkts;

	MEMSET (&IfHCOutBroadcastPkts, 0, sizeof(tSNMP_COUNTER64_TYPE));

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfHCOutBroadcastPkts(
                 i4IfIndex,
                 &IfHCOutBroadcastPkts);

	*pu8IfHCOutBroadcastPkts = ((*pu8IfHCOutBroadcastPkts | IfHCOutBroadcastPkts.msn) << 32)| IfHCOutBroadcastPkts.lsn;

    return i1RetVal;


} /* NcIfHCOutBroadcastPktsGet */

/********************************************************************
* FUNCTION NcIfLinkUpDownTrapEnableSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfLinkUpDownTrapEnableSet (
                INT4 i4IfIndex,
                INT4 i4IfLinkUpDownTrapEnable )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetIfLinkUpDownTrapEnable(
                 i4IfIndex,
                i4IfLinkUpDownTrapEnable);

    return i1RetVal;


} /* NcIfLinkUpDownTrapEnableSet */

/********************************************************************
* FUNCTION NcIfLinkUpDownTrapEnableTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfLinkUpDownTrapEnableTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                INT4 i4IfLinkUpDownTrapEnable )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2IfLinkUpDownTrapEnable(pu4Error,
                 i4IfIndex,
                i4IfLinkUpDownTrapEnable);

    return i1RetVal;


} /* NcIfLinkUpDownTrapEnableTest */

/********************************************************************
* FUNCTION NcIfHighSpeedGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfHighSpeedGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfHighSpeed )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfHighSpeed(
                 i4IfIndex,
                 pu4IfHighSpeed );

    return i1RetVal;


} /* NcIfHighSpeedGet */

/********************************************************************
* FUNCTION NcIfPromiscuousModeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfPromiscuousModeSet (
                INT4 i4IfIndex,
                INT4 i4IfPromiscuousMode )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetIfPromiscuousMode(
                 i4IfIndex,
                i4IfPromiscuousMode);

    return i1RetVal;


} /* NcIfPromiscuousModeSet */

/********************************************************************
* FUNCTION NcIfPromiscuousModeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfPromiscuousModeTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                INT4 i4IfPromiscuousMode )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2IfPromiscuousMode(pu4Error,
                 i4IfIndex,
                i4IfPromiscuousMode);

    return i1RetVal;


} /* NcIfPromiscuousModeTest */

/********************************************************************
* FUNCTION NcIfConnectorPresentGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfConnectorPresentGet (
                INT4 i4IfIndex,
                INT4 *pi4IfConnectorPresent )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfConnectorPresent(
                 i4IfIndex,
                 pi4IfConnectorPresent );

    return i1RetVal;


} /* NcIfConnectorPresentGet */

/********************************************************************
* FUNCTION NcIfAliasSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfAliasSet (
                INT4 i4IfIndex,
                UINT1 *pIfAlias )
{

    INT1 i1RetVal;
    tSNMP_OCTET_STRING_TYPE IfAlias;
    MEMSET (&IfAlias, 0,  sizeof (IfAlias));

    IfAlias.i4_Length = (INT4) STRLEN (pIfAlias);
    IfAlias.pu1_OctetList = pIfAlias;

    i1RetVal = nmhSetIfAlias(
                 i4IfIndex,
                &IfAlias);

    return i1RetVal;


} /* NcIfAliasSet */

/********************************************************************
* FUNCTION NcIfAliasTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfAliasTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                UINT1 *pIfAlias )
{

    INT1 i1RetVal;
    tSNMP_OCTET_STRING_TYPE IfAlias;
    MEMSET (&IfAlias, 0,  sizeof (IfAlias));

    IfAlias.i4_Length = (INT4) STRLEN (pIfAlias);
    IfAlias.pu1_OctetList = pIfAlias;

    i1RetVal = nmhTestv2IfAlias(pu4Error,
                 i4IfIndex,
                &IfAlias);

    return i1RetVal;


} /* NcIfAliasTest */

/********************************************************************
* FUNCTION NcIfCounterDiscontinuityTimeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfCounterDiscontinuityTimeGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfCounterDiscontinuityTime )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfCounterDiscontinuityTime(
                 i4IfIndex,
                 pu4IfCounterDiscontinuityTime );

    return i1RetVal;


} /* NcIfCounterDiscontinuityTimeGet */

/********************************************************************
* FUNCTION NcIfTestIdSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfTestIdSet (
                INT4 i4IfIndex,
                INT4 i4IfTestId )
{

    INT1 i1RetVal = SNMP_SUCCESS;
	UNUSED_PARAM (i4IfIndex);
	UNUSED_PARAM (i4IfTestId);
    /*i1RetVal = nmhSetIfTestId(
                 i4IfIndex,
                i4IfTestId);*/

    return i1RetVal;


} /* NcIfTestIdSet */

/********************************************************************
* FUNCTION NcIfTestIdTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfTestIdTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                INT4 i4IfTestId )
{

    INT1 i1RetVal = SNMP_SUCCESS;

	UNUSED_PARAM (pu4Error);
	UNUSED_PARAM (i4IfIndex);
	UNUSED_PARAM (i4IfTestId);
    /*i1RetVal = nmhTestv2IfTestId(pu4Error,
                 i4IfIndex,
                i4IfTestId);*/

    return i1RetVal;


} /* NcIfTestIdTest */

/********************************************************************
* FUNCTION NcIfTestStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfTestStatusSet (
                INT4 i4IfIndex,
                INT4 i4IfTestStatus )
{

    INT1 i1RetVal = SNMP_SUCCESS;

	UNUSED_PARAM (i4IfIndex);
	UNUSED_PARAM (i4IfTestStatus);
    /*i1RetVal = nmhSetIfTestStatus(
                 i4IfIndex,
                i4IfTestStatus);*/

    return i1RetVal;


} /* NcIfTestStatusSet */

/********************************************************************
* FUNCTION NcIfTestStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfTestStatusTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                INT4 i4IfTestStatus )
{

    INT1 i1RetVal = SNMP_SUCCESS;
	UNUSED_PARAM (i4IfIndex);
	UNUSED_PARAM (i4IfTestStatus);
	UNUSED_PARAM (pu4Error);

    /*i1RetVal = nmhTestv2IfTestStatus(pu4Error,
                 i4IfIndex,
                i4IfTestStatus);*/

    return i1RetVal;


} /* NcIfTestStatusTest */

/********************************************************************
* FUNCTION NcIfTestTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfTestTypeSet (
                INT4 i4IfIndex,
                const UINT1 *pIfTestType )
{

    INT1 i1RetVal = SNMP_SUCCESS;

	UNUSED_PARAM (i4IfIndex);
	UNUSED_PARAM (pIfTestType);
    /*i1RetVal = nmhSetIfTestType(
                 i4IfIndex,
                &IfTestType);*/

    return i1RetVal;


} /* NcIfTestTypeSet */

/********************************************************************
* FUNCTION NcIfTestTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfTestTypeTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                const UINT1 *pIfTestType )
{

    INT1 i1RetVal = SNMP_SUCCESS;

	UNUSED_PARAM (i4IfIndex);
	UNUSED_PARAM (pIfTestType);
	UNUSED_PARAM (pu4Error);

    /*i1RetVal = nmhTestv2IfTestType(pu4Error,
                 i4IfIndex,
                &IfTestType);*/

    return i1RetVal;


} /* NcIfTestTypeTest */

/********************************************************************
* FUNCTION NcIfTestResultGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfTestResultGet (
                INT4 i4IfIndex,
                INT4 *pi4IfTestResult )
{

    INT1 i1RetVal = SNMP_SUCCESS;

	UNUSED_PARAM (i4IfIndex);
	UNUSED_PARAM (pi4IfTestResult);
    /*if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfTestResult(
                 i4IfIndex,
                 pi4IfTestResult );*/

    return i1RetVal;


} /* NcIfTestResultGet */

/********************************************************************
* FUNCTION NcIfTestCodeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfTestCodeGet (
                INT4 i4IfIndex,
                UINT1 *pIfTestCode )
{

    INT1 i1RetVal = SNMP_SUCCESS;

	UNUSED_PARAM (i4IfIndex);
	UNUSED_PARAM (pIfTestCode);
    /*if (nmhValidateIndexInstanceIfTable(
                 i4IfIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetIfTestCode(
                 i4IfIndex,
                 pIfTestCode );*/

    return i1RetVal;


} /* NcIfTestCodeGet */

/********************************************************************
* FUNCTION NcIfTestOwnerSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfTestOwnerSet (
                INT4 i4IfIndex,
                UINT1 *pIfTestOwner )
{

    INT1 i1RetVal = SNMP_SUCCESS;

	UNUSED_PARAM (i4IfIndex);
	UNUSED_PARAM (pIfTestOwner);
    /*tSNMP_OCTET_STRING_TYPE IfTestOwner;
    MEMSET (&IfTestOwner, 0,  sizeof (IfTestOwner));

    IfTestOwner.i4_Length = (INT4) STRLEN (pIfTestOwner);
    IfTestOwner.pu1_OctetList = pIfTestOwner;

    i1RetVal = nmhSetIfTestOwner(
                 i4IfIndex,
                &IfTestOwner);*/

    return i1RetVal;


} /* NcIfTestOwnerSet */

/********************************************************************
* FUNCTION NcIfTestOwnerTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfTestOwnerTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                UINT1 *pIfTestOwner )
{

    INT1 i1RetVal = SNMP_SUCCESS;

	UNUSED_PARAM (pu4Error);
	UNUSED_PARAM (i4IfIndex);
	UNUSED_PARAM (pIfTestOwner);

    /*tSNMP_OCTET_STRING_TYPE IfTestOwner;
    MEMSET (&IfTestOwner, 0,  sizeof (IfTestOwner));

    IfTestOwner.i4_Length = (INT4) STRLEN (pIfTestOwner);
    IfTestOwner.pu1_OctetList = pIfTestOwner;

    i1RetVal = nmhTestv2IfTestOwner(pu4Error,
                 i4IfIndex,
                &IfTestOwner);*/

    return i1RetVal;


} /* NcIfTestOwnerTest */

/********************************************************************
* FUNCTION NcIfStackStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfStackStatusSet (
                INT4 i4IfStackHigherLayer,
                INT4 i4IfStackLowerLayer,
                INT4 i4IfStackStatus )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetIfStackStatus(
                 i4IfStackHigherLayer,
                 i4IfStackLowerLayer,
                i4IfStackStatus);

    return i1RetVal;


} /* NcIfStackStatusSet */

/********************************************************************
* FUNCTION NcIfStackStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfStackStatusTest (UINT4 *pu4Error,
                INT4 i4IfStackHigherLayer,
                INT4 i4IfStackLowerLayer,
                INT4 i4IfStackStatus )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2IfStackStatus(pu4Error,
                 i4IfStackHigherLayer,
                 i4IfStackLowerLayer,
                i4IfStackStatus);

    return i1RetVal;


} /* NcIfStackStatusTest */

/********************************************************************
* FUNCTION NcIfRcvAddressStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfRcvAddressStatusSet (
                INT4 i4IfIndex,
                UINT1 *pIfRcvAddressAddress,
                INT4 i4IfRcvAddressStatus )
{

    INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE	IfRcvAddressAddress;
	UINT1	au1IfRcvAddressAddress[CFA_ENET_ADDR_LEN];

	MEMSET (&IfRcvAddressAddress, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
	MEMSET (au1IfRcvAddressAddress, '\0', CFA_ENET_ADDR_LEN);

	CliDotStrToMac (pIfRcvAddressAddress, au1IfRcvAddressAddress);

	IfRcvAddressAddress.pu1_OctetList = au1IfRcvAddressAddress;
	IfRcvAddressAddress.i4_Length = CFA_ENET_ADDR_LEN;

    i1RetVal = nmhSetIfRcvAddressStatus(
                 i4IfIndex,
                 &IfRcvAddressAddress,
                i4IfRcvAddressStatus);

    return i1RetVal;


} /* NcIfRcvAddressStatusSet */

/********************************************************************
* FUNCTION NcIfRcvAddressStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfRcvAddressStatusTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                UINT1 *pIfRcvAddressAddress,
                INT4 i4IfRcvAddressStatus )
{

    INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE	IfRcvAddressAddress;
	UINT1	au1IfRcvAddressAddress[CFA_ENET_ADDR_LEN];

	MEMSET (&IfRcvAddressAddress, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
	MEMSET (au1IfRcvAddressAddress, '\0', CFA_ENET_ADDR_LEN);

	CliDotStrToMac (pIfRcvAddressAddress, au1IfRcvAddressAddress);
	IfRcvAddressAddress.pu1_OctetList = au1IfRcvAddressAddress;
	IfRcvAddressAddress.i4_Length = CFA_ENET_ADDR_LEN;

    i1RetVal = nmhTestv2IfRcvAddressStatus(pu4Error,
                 i4IfIndex,
                 &IfRcvAddressAddress,
                i4IfRcvAddressStatus);

    return i1RetVal;


} /* NcIfRcvAddressStatusTest */

/********************************************************************
* FUNCTION NcIfRcvAddressTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfRcvAddressTypeSet (
                INT4 i4IfIndex,
                UINT1 *pIfRcvAddressAddress,
                INT4 i4IfRcvAddressType )
{

    INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE	IfRcvAddressAddress;
	UINT1	au1IfRcvAddressAddress[CFA_ENET_ADDR_LEN];

	MEMSET (&IfRcvAddressAddress, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
	MEMSET (au1IfRcvAddressAddress, '\0', CFA_ENET_ADDR_LEN);

	CliDotStrToMac (pIfRcvAddressAddress, au1IfRcvAddressAddress);
	IfRcvAddressAddress.pu1_OctetList = au1IfRcvAddressAddress;
	IfRcvAddressAddress.i4_Length = CFA_ENET_ADDR_LEN;

    i1RetVal = nmhSetIfRcvAddressType(
                 i4IfIndex,
                 &IfRcvAddressAddress,
                i4IfRcvAddressType);

    return i1RetVal;


} /* NcIfRcvAddressTypeSet */

/********************************************************************
* FUNCTION NcIfRcvAddressTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcIfRcvAddressTypeTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                UINT1 *pIfRcvAddressAddress,
                INT4 i4IfRcvAddressType )
{

    INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE	IfRcvAddressAddress;
	UINT1	au1IfRcvAddressAddress[CFA_ENET_ADDR_LEN];

	MEMSET (&IfRcvAddressAddress, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
	MEMSET (au1IfRcvAddressAddress, '\0', CFA_ENET_ADDR_LEN);

	CliDotStrToMac (pIfRcvAddressAddress, au1IfRcvAddressAddress);
	IfRcvAddressAddress.pu1_OctetList = au1IfRcvAddressAddress;
	IfRcvAddressAddress.i4_Length = CFA_ENET_ADDR_LEN;

    i1RetVal = nmhTestv2IfRcvAddressType(pu4Error,
                 i4IfIndex,
                 &IfRcvAddressAddress,
                i4IfRcvAddressType);

    return i1RetVal;


} /* NcIfRcvAddressTypeTest */

/* END i_IF_MIB.c */
