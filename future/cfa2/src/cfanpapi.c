/* SOURCE FILE HEADER :
 * $Id: cfanpapi.c,v 1.22 2017/11/14 07:31:10 siva Exp $
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : cfanpapi.c                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Aricent Inc.                                     |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : CFA                                              |
 * |                                                                           |
 * |  MODULE NAME           : CFA NP Wrapper Configurations                    |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : All  Network Processor API Function              |
 * |                          calls are done here                              |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#ifndef __CFA_NPAPI_C__
#define __CFA_NPAPI_C__

#include "cfainc.h"
#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaCfaNpGetLinkStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes CfaNpGetLinkStatus
 *                                                                          
 *    Input(s)            : Arguments of CfaNpGetLinkStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaCfaNpGetLinkStatus (UINT4 u4IfIndex)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrCfaNpGetLinkStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         CFA_NP_GET_LINK_STATUS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpCfaNpGetLinkStatus;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1PortLinkStatus = CFA_IF_NP;
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (CFA_IF_NP);
    }
    return pEntry->u1PortLinkStatus;

}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaCfaRegisterWithNpDrv                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes CfaRegisterWithNpDrv
 *                                                                          
 *    Input(s)            : Arguments of CfaRegisterWithNpDrv
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaCfaRegisterWithNpDrv (VOID)
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         CFA_REGISTER_WITH_NP_DRV,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsCfaHwClearStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsCfaHwClearStats
 *                                                                          
 *    Input(s)            : Arguments of FsCfaHwClearStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsCfaHwClearStats (UINT4 u4Port)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsCfaHwClearStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_CFA_HW_CLEAR_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsCfaHwClearStats;

    pEntry->u4Port = u4Port;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsCfaHwGetIfFlowControl                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsCfaHwGetIfFlowControl
 *                                                                          
 *    Input(s)            : Arguments of FsCfaHwGetIfFlowControl
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsCfaHwGetIfFlowControl (UINT4 u4IfIndex, UINT4 *pu4PortFlowControl)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsCfaHwGetIfFlowControl *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_CFA_HW_GET_IF_FLOW_CONTROL,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsCfaHwGetIfFlowControl;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu4PortFlowControl = pu4PortFlowControl;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsCfaHwGetMtu                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsCfaHwGetMtu
 *                                                                          
 *    Input(s)            : Arguments of FsCfaHwGetMtu
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsCfaHwGetMtu (UINT2 u2IfIndex, UINT4 *pu4MtuSize)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsCfaHwGetMtu *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_CFA_HW_GET_MTU,    /* Function/OpCode */
                         u2IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsCfaHwGetMtu;

    pEntry->u4IfIndex = u2IfIndex;
    pEntry->pu4MtuSize = pu4MtuSize;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsCfaHwSetMacAddr                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsCfaHwSetMacAddr
 *                                                                          
 *    Input(s)            : Arguments of FsCfaHwSetMacAddr
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsCfaHwSetMacAddr (UINT4 u4IfIndex, tMacAddr PortMac)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsCfaHwSetMacAddr *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_CFA_HW_SET_MAC_ADDR,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsCfaHwSetMacAddr;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->PortMac = PortMac;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsCfaHwSetMtu                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsCfaHwSetMtu
 *                                                                          
 *    Input(s)            : Arguments of FsCfaHwSetMtu
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsCfaHwSetMtu (UINT4 u4IfIndex, UINT4 u4MtuSize)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsCfaHwSetMtu *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_CFA_HW_SET_MTU,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsCfaHwSetMtu;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4MtuSize = u4MtuSize;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsCfaHwL3SetMtu                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsCfaHwSetMtu
 *                                                                          
 *    Input(s)            : Arguments of FsCfaHwL3SetMtu
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
UINT1
CfaFsCfaHwL3SetMtu (tL3MtuInfo L3Mtu)
{

    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsCfaHwL3SetMtu *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_CFA_HW_L3_SET_MTU,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsCfaHwL3SetMtu;

    pEntry->L3Mtu.u4IfIndex = L3Mtu.u4IfIndex;
    pEntry->L3Mtu.u4Mtu = L3Mtu.u4Mtu;
    pEntry->L3Mtu.u4VrfId = L3Mtu.u4VrfId;

    return (NpUtilHwProgram (&FsHwNp));

}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsCfaHwSetWanTye                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsCfaHwSetWanTye
 *                                                                          
 *    Input(s)            : Arguments of FsCfaHwSetWanTye
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsCfaHwSetWanTye (tIfWanInfo * pCfaWanInfo)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsCfaHwSetWanTye *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_CFA_HW_SET_WAN_TYE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsCfaHwSetWanTye;

    pEntry->pCfaWanInfo = pCfaWanInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsEtherHwGetCntrl                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsEtherHwGetCntrl
 *                                                                          
 *    Input(s)            : Arguments of FsEtherHwGetCntrl
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsEtherHwGetCntrl (UINT4 u4IfIndex, INT4 *pElement, INT4 i4Code)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsEtherHwGetCntrl *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_ETHER_HW_GET_CNTRL,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsEtherHwGetCntrl;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pElement = pElement;
    pEntry->i4Code = i4Code;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsEtherHwGetCollFreq                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsEtherHwGetCollFreq
 *                                                                          
 *    Input(s)            : Arguments of FsEtherHwGetCollFreq
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsEtherHwGetCollFreq (UINT4 u4IfIndex, INT4 i4dot3CollCount, UINT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsEtherHwGetCollFreq *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_ETHER_HW_GET_COLL_FREQ,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsEtherHwGetCollFreq;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4dot3CollCount = i4dot3CollCount;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsEtherHwGetPauseFrames                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsEtherHwGetPauseFrames
 *                                                                          
 *    Input(s)            : Arguments of FsEtherHwGetPauseFrames
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsEtherHwGetPauseFrames (UINT4 u4IfIndex, UINT4 *pElement, INT4 i4Code)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsEtherHwGetPauseFrames *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_ETHER_HW_GET_PAUSE_FRAMES,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsEtherHwGetPauseFrames;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pElement = pElement;
    pEntry->i4Code = i4Code;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsEtherHwGetPauseMode                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsEtherHwGetPauseMode
 *                                                                          
 *    Input(s)            : Arguments of FsEtherHwGetPauseMode
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsEtherHwGetPauseMode (UINT4 u4IfIndex, INT4 *pElement, INT4 i4Code)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsEtherHwGetPauseMode *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_ETHER_HW_GET_PAUSE_MODE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsEtherHwGetPauseMode;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pElement = pElement;
    pEntry->i4Code = i4Code;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsEtherHwGetStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsEtherHwGetStats
 *                                                                          
 *    Input(s)            : Arguments of FsEtherHwGetStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsEtherHwGetStats (UINT4 u4IfIndex, tEthStats * pEthStats)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsEtherHwGetStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_ETHER_HW_GET_STATS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsEtherHwGetStats;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pEthStats = pEthStats;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsEtherHwSetPauseAdminMode                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsEtherHwSetPauseAdminMode
 *                                                                          
 *    Input(s)            : Arguments of FsEtherHwSetPauseAdminMode
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsEtherHwSetPauseAdminMode (UINT4 u4IfIndex, INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsEtherHwSetPauseAdminMode *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_ETHER_HW_SET_PAUSE_ADMIN_MODE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsEtherHwSetPauseAdminMode;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsHwGetEthernetType                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsHwGetEthernetType
 *                                                                          
 *    Input(s)            : Arguments of FsHwGetEthernetType
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsHwGetEthernetType (UINT4 u4IfIndex, UINT1 *pu1EtherType)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsHwGetEthernetType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_HW_GET_ETHERNET_TYPE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsHwGetEthernetType;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu1EtherType = pu1EtherType;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsHwGetStat                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsHwGetStat
 *                                                                          
 *    Input(s)            : Arguments of FsHwGetStat
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsHwGetStat (UINT4 u4IfIndex, INT1 i1StatType, UINT4 *pu4Value)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsHwGetStat *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_HW_GET_STAT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsHwGetStat;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i1StatType = i1StatType;
    pEntry->pu4Value = pu4Value;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsHwGetStat64                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsHwGetStat64
 *                                                                          
 *    Input(s)            : Arguments of FsHwGetStat64
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsHwGetStat64 (UINT4 u4IfIndex, INT1 i1StatType,
                  tSNMP_COUNTER64_TYPE * pValue)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsHwGetStat64 *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_HW_GET_STAT64,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsHwGetStat64;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i1StatType = i1StatType;
    pEntry->pValue = pValue;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsHwSetCustIfParams                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsHwSetCustIfParams
 *                                                                          
 *    Input(s)            : Arguments of FsHwSetCustIfParams
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsHwSetCustIfParams (UINT4 u4IfIndex, tHwCustIfParamType HwCustParamType,
                        tHwCustIfParamVal CustIfParamVal,
                        tNpEntryAction EntryAction)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsHwSetCustIfParams *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_HW_SET_CUST_IF_PARAMS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsHwSetCustIfParams;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->HwCustParamType = HwCustParamType;
    pEntry->CustIfParamVal = CustIfParamVal;
    pEntry->EntryAction = EntryAction;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsHwUpdateAdminStatusChange                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsHwUpdateAdminStatusChange
 *                                                                          
 *    Input(s)            : Arguments of FsHwUpdateAdminStatusChange
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsHwUpdateAdminStatusChange (UINT4 u4IfIndex, UINT1 u1AdminEnable)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsHwUpdateAdminStatusChange *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_HW_UPDATE_ADMIN_STATUS_CHANGE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsHwUpdateAdminStatusChange;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1AdminEnable = u1AdminEnable;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetAutoNegAdminConfig                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetAutoNegAdminConfig
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetAutoNegAdminConfig
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetAutoNegAdminConfig (UINT4 u4IfIndex, INT4 i4IfMauId,
                                 INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetAutoNegAdminConfig *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_AUTO_NEG_ADMIN_CONFIG,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegAdminConfig;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetAutoNegAdminStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetAutoNegAdminStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetAutoNegAdminStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetAutoNegAdminStatus (UINT4 u4IfIndex, INT4 i4IfMauId,
                                 INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetAutoNegAdminStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_AUTO_NEG_ADMIN_STATUS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegAdminStatus;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetAutoNegCapAdvtBits                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetAutoNegCapAdvtBits
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetAutoNegCapAdvtBits
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetAutoNegCapAdvtBits (UINT4 u4IfIndex, INT4 i4IfMauId,
                                 INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetAutoNegCapAdvtBits *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_AUTO_NEG_CAP_ADVT_BITS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegCapAdvtBits;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetAutoNegCapBits                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetAutoNegCapBits
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetAutoNegCapBits
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetAutoNegCapBits (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetAutoNegCapBits *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_AUTO_NEG_CAP_BITS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegCapBits;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetAutoNegCapRcvdBits                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetAutoNegCapRcvdBits
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetAutoNegCapRcvdBits
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetAutoNegCapRcvdBits (UINT4 u4IfIndex, INT4 i4IfMauId,
                                 INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetAutoNegCapRcvdBits *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_AUTO_NEG_CAP_RCVD_BITS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegCapRcvdBits;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetAutoNegRemFltAdvt                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetAutoNegRemFltAdvt
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetAutoNegRemFltAdvt
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetAutoNegRemFltAdvt (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetAutoNegRemFltAdvt *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_AUTO_NEG_REM_FLT_ADVT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegRemFltAdvt;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetAutoNegRemFltRcvd                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetAutoNegRemFltRcvd
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetAutoNegRemFltRcvd
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetAutoNegRemFltRcvd (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetAutoNegRemFltRcvd *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_AUTO_NEG_REM_FLT_RCVD,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegRemFltRcvd;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetAutoNegRemoteSignaling                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetAutoNegRemoteSignaling
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetAutoNegRemoteSignaling
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetAutoNegRemoteSignaling (UINT4 u4IfIndex, INT4 i4IfMauId,
                                     INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetAutoNegRemoteSignaling *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_AUTO_NEG_REMOTE_SIGNALING,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegRemoteSignaling;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetAutoNegRestart                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetAutoNegRestart
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetAutoNegRestart
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetAutoNegRestart (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetAutoNegRestart *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_AUTO_NEG_RESTART,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegRestart;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetAutoNegSupported                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetAutoNegSupported
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetAutoNegSupported
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetAutoNegSupported (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetAutoNegSupported *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_AUTO_NEG_SUPPORTED,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegSupported;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetFalseCarriers                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetFalseCarriers
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetFalseCarriers
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetFalseCarriers (UINT4 u4IfIndex, INT4 i4IfMauId, UINT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetFalseCarriers *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_FALSE_CARRIERS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetFalseCarriers;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetJabberState                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetJabberState
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetJabberState
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetJabberState (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetJabberState *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_JABBER_STATE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetJabberState;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetJabberingStateEnters                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetJabberingStateEnters
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetJabberingStateEnters
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetJabberingStateEnters (UINT4 u4IfIndex, INT4 i4IfMauId,
                                   UINT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetJabberingStateEnters *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_JABBERING_STATE_ENTERS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetJabberingStateEnters;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetJackType                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetJackType
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetJackType
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetJackType (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 i4ifJackIndex,
                       INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetJackType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_JACK_TYPE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetJackType;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->i4ifJackIndex = i4ifJackIndex;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetMauType                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetMauType
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetMauType
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetMauType (UINT4 u4IfIndex, INT4 i4IfMauId, UINT4 *pu4Val)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetMauType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_MAU_TYPE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetMauType;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pu4Val = pu4Val;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetMediaAvailStateExits                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetMediaAvailStateExits
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetMediaAvailStateExits
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetMediaAvailStateExits (UINT4 u4IfIndex, INT4 i4IfMauId,
                                   UINT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetMediaAvailStateExits *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_MEDIA_AVAIL_STATE_EXITS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetMediaAvailStateExits;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetMediaAvailable                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetMediaAvailable
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetMediaAvailable
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetMediaAvailable (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetMediaAvailable *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_MEDIA_AVAILABLE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetMediaAvailable;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetTypeListBits                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetTypeListBits
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetTypeListBits
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetTypeListBits (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetTypeListBits *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_TYPE_LIST_BITS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetTypeListBits;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwSetAutoNegAdminStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwSetAutoNegAdminStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwSetAutoNegAdminStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwSetAutoNegAdminStatus (UINT4 u4IfIndex, INT4 i4IfMauId,
                                 INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwSetAutoNegAdminStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_SET_AUTO_NEG_ADMIN_STATUS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwSetAutoNegAdminStatus;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwSetAutoNegCapAdvtBits                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwSetAutoNegCapAdvtBits
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwSetAutoNegCapAdvtBits
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwSetAutoNegCapAdvtBits (UINT4 u4IfIndex, INT4 i4IfMauId,
                                 INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwSetAutoNegCapAdvtBits *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_SET_AUTO_NEG_CAP_ADVT_BITS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwSetAutoNegCapAdvtBits;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwSetAutoNegRemFltAdvt                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwSetAutoNegRemFltAdvt
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwSetAutoNegRemFltAdvt
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwSetAutoNegRemFltAdvt (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwSetAutoNegRemFltAdvt *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_SET_AUTO_NEG_REM_FLT_ADVT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwSetAutoNegRemFltAdvt;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwSetAutoNegRestart                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwSetAutoNegRestart
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwSetAutoNegRestart
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwSetAutoNegRestart (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwSetAutoNegRestart *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_SET_AUTO_NEG_RESTART,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwSetAutoNegRestart;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwSetMauStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwSetMauStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwSetMauStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwSetMauStatus (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwSetMauStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_SET_MAU_STATUS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwSetMauStatus;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetStatus (UINT4 u4IfIndex, INT4 i4IfMauId, INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_STATUS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetStatus;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pElement = pElement;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwSetDefaultType                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwSetDefaultType
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwSetDefaultType
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwSetDefaultType (UINT4 u4IfIndex, INT4 i4IfMauId, UINT4 u4Val)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwSetDefaultType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_SET_DEFAULT_TYPE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwSetDefaultType;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->u4Val = u4Val;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMauHwGetDefaultType                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMauHwGetDefaultType
 *                                                                          
 *    Input(s)            : Arguments of FsMauHwGetDefaultType
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMauHwGetDefaultType (UINT4 u4IfIndex, INT4 i4IfMauId, UINT4 *pu4Val)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsMauHwGetDefaultType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_MAU_HW_GET_DEFAULT_TYPE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetDefaultType;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IfMauId = i4IfMauId;
    pEntry->pu4Val = pu4Val;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaNpCfaFrontPanelPorts                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes NpCfaFrontPanelPorts
 *                                                                          
 *    Input(s)            : Arguments of NpCfaFrontPanelPorts
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaNpCfaFrontPanelPorts (UINT4 u4MaxFrontPanelPorts)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrNpCfaFrontPanelPorts *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         NP_CFA_FRONT_PANEL_PORTS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpNpCfaFrontPanelPorts;

    pEntry->u4MaxFrontPanelPorts = u4MaxFrontPanelPorts;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsCfaHwMacLookup                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsCfaHwMacLookup
 *                                                                          
 *    Input(s)            : Arguments of FsCfaHwMacLookup
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsCfaHwMacLookup (UINT1 *pu1MacAddr, tVlanIfaceVlanId VlanId, UINT2 *pu2Port)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsCfaHwMacLookup *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_CFA_HW_MAC_LOOKUP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsCfaHwMacLookup;

    pEntry->pu1MacAddr = pu1MacAddr;
    pEntry->VlanId = VlanId;
    pEntry->pu2Port = pu2Port;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaCfaNpSetHwPortInfo                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes CfaNpSetHwPortInfo
 *                                                                          
 *    Input(s)            : Arguments of CfaNpSetHwPortInfo
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaCfaNpSetHwPortInfo (tHwPortInfo HwPortInfo)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrCfaNpSetHwPortInfo *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         CFA_NP_SET_HW_PORT_INFO,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpCfaNpSetHwPortInfo;

    pEntry->HwPortInfo = HwPortInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaCfaNpGetHwPortInfo                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes CfaNpGetHwPortInfo
 *                                                                          
 *    Input(s)            : Arguments of CfaNpGetHwPortInfo
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaCfaNpGetHwPortInfo (tHwPortInfo * pHwPortInfo)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrCfaNpGetHwPortInfo *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         CFA_NP_GET_HW_PORT_INFO,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpCfaNpGetHwPortInfo;

    pEntry->pHwPortInfo = pHwPortInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsCfaHwDeletePktFilter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsCfaHwDeletePktFilter
 *                                                                          
 *    Input(s)            : Arguments of FsCfaHwDeletePktFilter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsCfaHwDeletePktFilter (UINT4 u4FilterId)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsCfaHwDeletePktFilter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_CFA_HW_DELETE_PKT_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsCfaHwDeletePktFilter;

    pEntry->u4FilterId = u4FilterId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsCfaHwCreatePktFilter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsCfaHwCreatePktFilter
 *                                                                          
 *    Input(s)            : Arguments of FsCfaHwCreatePktFilter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsCfaHwCreatePktFilter (tHwCfaFilterInfo * pFilterInfo, UINT4 *pu4FilterId)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsCfaHwCreatePktFilter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_CFA_HW_CREATE_PKT_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsCfaHwCreatePktFilter;

    pEntry->pFilterInfo = pFilterInfo;
    pEntry->pu4FilterId = pu4FilterId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaCfaNpSetStackingModel                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes CfaNpSetStackingModel
 *                                                                          
 *    Input(s)            : Arguments of CfaNpSetStackingModel
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaCfaNpSetStackingModel (UINT4 u4StackingModel)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrCfaNpSetStackingModel *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         CFA_NP_SET_STACKING_MODEL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpCfaNpSetStackingModel;

    pEntry->u4StackingModel = u4StackingModel;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaCfaMbsmNpSlotDeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes CfaMbsmNpSlotDeInit
 *                                                                          
 *    Input(s)            : Arguments of CfaMbsmNpSlotDeInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaCfaMbsmNpSlotDeInit (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrCfaMbsmNpSlotDeInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         CFA_MBSM_NP_SLOT_DE_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpCfaMbsmNpSlotDeInit;

    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaCfaMbsmNpSlotInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes CfaMbsmNpSlotInit
 *                                                                          
 *    Input(s)            : Arguments of CfaMbsmNpSlotInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaCfaMbsmNpSlotInit (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrCfaMbsmNpSlotInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         CFA_MBSM_NP_SLOT_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpCfaMbsmNpSlotInit;

    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaCfaMbsmRegisterWithNpDrv                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes CfaMbsmRegisterWithNpDrv
 *                                                                          
 *    Input(s)            : Arguments of CfaMbsmRegisterWithNpDrv
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaCfaMbsmRegisterWithNpDrv (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrCfaMbsmRegisterWithNpDrv *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         CFA_MBSM_REGISTER_WITH_NP_DRV,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpCfaMbsmRegisterWithNpDrv;

    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaCfaMbsmUpdatePortMacAddress                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes CfaMbsmUpdatePortMacAddress
 *                                                                          
 *    Input(s)            : Arguments of CfaMbsmUpdatePortMacAddress
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaCfaMbsmUpdatePortMacAddress (UINT1 u1SlotId, UINT4 u4PortIndex,
                                UINT4 u4IfIndex, UINT1 *pu1SwitchMac)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrCfaMbsmUpdatePortMacAddress *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         CFA_MBSM_UPDATE_PORT_MAC_ADDRESS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpCfaMbsmUpdatePortMacAddress;

    pEntry->u1SlotId = u1SlotId;
    pEntry->u4PortIndex = u4PortIndex;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu1SwitchMac = pu1SwitchMac;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsCfaMbsmSetMacAddr                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsCfaMbsmSetMacAddr
 *                                                                          
 *    Input(s)            : Arguments of FsCfaMbsmSetMacAddr
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsCfaMbsmSetMacAddr (UINT4 u4IfIndex, tMacAddr PortMac,
                        tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsCfaMbsmSetMacAddr *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_CFA_MBSM_SET_MAC_ADDR,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsCfaMbsmSetMacAddr;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->PortMac = PortMac;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsHwMbsmSetCustIfParams                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsHwMbsmSetCustIfParams
 *                                                                          
 *    Input(s)            : Arguments of FsHwMbsmSetCustIfParams
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsHwMbsmSetCustIfParams (UINT4 u4IfIndex, tHwCustIfParamType HwCustParamType,
                            tHwCustIfParamVal CustIfParamVal,
                            tNpEntryAction EntryAction,
                            tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsHwMbsmSetCustIfParams *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_HW_MBSM_SET_CUST_IF_PARAMS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsHwMbsmSetCustIfParams;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->HwCustParamType = HwCustParamType;
    pEntry->CustIfParamVal = CustIfParamVal;
    pEntry->EntryAction = EntryAction;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsNpMbsmIpv4CreateIpInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv4CreateIpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv4CreateIpInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsNpMbsmIpv4CreateIpInterface (UINT4 u4VrId, UINT4 u4CfaIfIndex,
                                  UINT4 u4IpAddr, UINT4 u4IpSubNetMask,
                                  UINT2 u2VlanId, UINT1 *au1MacAddr,
                                  tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmIpv4CreateIpInterface *pEntry = NULL;
    tIpNpWrFsNpIpv4UpdateIpInterfaceStatus *pUpdtEntry = NULL;
    tCfaIfInfo          CfaIfInfo;

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV4_CREATE_IP_INTERFACE,    /* Function/OpCode */
                         u4CfaIfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4CreateIpInterface;

    pEntry->u4VrId = u4VrId;
    pEntry->u4CfaIfIndex = u4CfaIfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->u4IpSubNetMask = u4IpSubNetMask;
    pEntry->u2VlanId = u2VlanId;
    pEntry->au1MacAddr = au1MacAddr;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo);

    /*Give an indication to Other node, if the status is UP */
    if (CfaIfInfo.u1IfOperStatus == CFA_IF_UP)
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_IP_MOD,    /* Module ID */
                             FS_NP_MBSM_IPV4_UPDATE_IP_INTERFACE_STATUS,    /* Function/OpCode */
                             u4CfaIfIndex,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        pIpNpModInfo = &(FsHwNp.IpNpModInfo);
        pUpdtEntry = &pIpNpModInfo->IpNpFsNpIpv4UpdateIpInterfaceStatus;

        pUpdtEntry->u4VrId = u4VrId;
        pUpdtEntry->pu1IfName = CfaIfInfo.au1IfName;
        pUpdtEntry->u4CfaIfIndex = u4CfaIfIndex;
        pUpdtEntry->u4IpAddr = u4IpAddr;
        pUpdtEntry->u4IpSubNetMask = u4IpSubNetMask;
        pUpdtEntry->u2VlanId = u2VlanId;
        pUpdtEntry->au1MacAddr = au1MacAddr;
        pUpdtEntry->u4Status = CFA_IF_UP;

        NpUtilHwProgram (&FsHwNp);
    }

    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsNpMbsmIpv4MapVlansToIpInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv4MapVlansToIpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv4MapVlansToIpInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsNpMbsmIpv4MapVlansToIpInterface (tNpIpVlanMappingInfo * pPvlanMappingInfo,
                                      tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmIpv4MapVlansToIpInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV4_MAP_VLANS_TO_IP_INTERFACE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4MapVlansToIpInterface;

    pEntry->pPvlanMappingInfo = pPvlanMappingInfo;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* MBSM_WANTED */

/***************************************************************************
 *
 *    Function Name       : CfaIssHwGetPortSpeed
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwGetPortSpeed
 *
 *    Input(s)            : Arguments of IssHwGetPortSpeed
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
CfaIssHwGetPortSpeed (UINT4 u4IfIndex, INT4 *pi4PortSpeed)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwGetPortSpeed *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_GET_PORT_SPEED,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetPortSpeed;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pi4PortSpeed = pi4PortSpeed;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *
 *    Function Name       : CfaIssHwSetPortSpeed
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetPortSpeed
 *
 *    Input(s)            : Arguments of IssHwSetPortSpeed
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
CfaIssHwSetPortSpeed (UINT4 u4IfIndex, INT4 i4Value)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetPortSpeed *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_PORT_SPEED,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortSpeed;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4Value = i4Value;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *
 *    Function Name       : CfaIssHwSetPortAutoNegAdvtCapBits
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetPortAutoNegAdvtCapBits
 *
 *    Input(s)            : Arguments of IssHwSetPortAutoNegAdvtCapBits
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
CfaIssHwSetPortAutoNegAdvtCapBits (UINT4 u4IfIndex, INT4 *pi4MauCapBits,
                                   INT4 *pi4IssCapBits)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetPortAutoNegAdvtCapBits *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_PORT_AUTO_NEG_ADVT_CAP_BITS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortAutoNegAdvtCapBits;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pi4MauCapBits = pi4MauCapBits;
    pEntry->pi4IssCapBits = pi4IssCapBits;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *
 *    Function Name       : CfaIssHwInitFilter
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram                                                                                  *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwInitFilter                                                                                                *
 *    Input(s)            : Arguments of IssHwInitFilter                                                                                                      *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *                                                                                                                                                            *    Global Var Modified : None.
 *                                                                                                                                                            *    Use of Recursion    : None.
 *                                                                                                                                                            *    Exceptions or Operating
 *    System Error Handling    : None.
 *                                                                                                                                                            *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *                                                                                                                                                            *****************************************************************************/
UINT1
CfaIssHwInitFilter (VOID)
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_INIT_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *
 *    Function Name       : CfaCfaNpGetHwInfo
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes CfaNpGetHwInfo
 *
 *    Input(s)            : Arguments of CfaNpGetHwInfo
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
CfaCfaNpGetHwInfo (tHwIdInfo * pHwIdInfo)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrCfaNpGetHwInfo *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         CFA_NP_GET_HW_INFO,    /* Function/OpCode */
                         pHwIdInfo->u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpCfaNpGetHwInfo;

    pEntry->pHwIdInfo = pHwIdInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaCfaNpRemoteSetHwInfo
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes CfaNpRemoteSetHwInfo
 *                                                                          
 *    Input(s)            : Arguments of CfaNpRemoteSetHwInfo
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaCfaNpRemoteSetHwInfo (tHwIdInfo * pHwIdInfo)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrCfaNpRemoteSetHwInfo *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         CFA_NP_REMOTE_SET_HW_INFO,    /* Function/OpCode */
                         pHwIdInfo->u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpCfaNpRemoteSetHwInfo;

    pEntry->pHwIdInfo = pHwIdInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef MBSM_WANTED
/***************************************************************************
 *                                                                          
 *    Function Name       : CfaMbsmNpTxOnStackInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MbsmNpTxOnStackInterface
 *                                                                          
 *    Input(s)            : Arguments of MbsmNpTxOnStackInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaMbsmNpTxOnStackInterface (UINT1 *pu1Pkt, INT4 i4PktSize)
{
    tFsHwNp             FsHwNp;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmNpWrMbsmNpTxOnStackInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MBSM_MOD,    /* Module ID */
                         MBSM_NP_TX_ON_STACK_INTERFACE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMbsmNpModInfo = &(FsHwNp.MbsmNpModInfo);
    pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpTxOnStackInterface;

    pEntry->pu1Pkt = pu1Pkt;
    pEntry->i4PktSize = i4PktSize;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaMbsmNpGetStackMac                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MbsmNpGetStackMac
 *                                                                          
 *    Input(s)            : Arguments of MbsmNpGetStackMac
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaMbsmNpGetStackMac (UINT4 u4SlotId, UINT1 *pu1MacAddr)
{
    tFsHwNp             FsHwNp;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmNpWrMbsmNpGetStackMac *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MBSM_MOD,    /* Module ID */
                         MBSM_NP_GET_STACK_MAC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMbsmNpModInfo = &(FsHwNp.MbsmNpModInfo);
    pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpGetStackMac;

    pEntry->u4SlotId = u4SlotId;
    pEntry->pu1MacAddr = pu1MacAddr;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* MBSM_WANTED */

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsNpIpv4DeleteIpInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4DeleteIpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4DeleteIpInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsNpIpv4DeleteIpInterface (UINT4 u4VrId, UINT1 *pu1IfName, UINT4 u4IfIndex,
                              UINT2 u2VlanId)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4DeleteIpInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_DELETE_IP_INTERFACE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4DeleteIpInterface;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1IfName = pu1IfName;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2VlanId = u2VlanId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsNpIpv4DeleteL3SubInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4DeleteIpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4DeleteIpInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsNpIpv4DeleteL3SubInterface (UINT4 u4IfIndex)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4DeleteL3SubInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_DELETE_L3SUB_INTERFACE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4DeleteL3SubInterface;
    pEntry->u4IfIndex = u4IfIndex;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *
 *    Function Name       : CfaFsNpIpv4DeleteSecIpInterface
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4DeleteIpInterface
 *
 *    Input(s)            : Arguments of FsNpIpv4DeleteIpInterface
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
CfaFsNpIpv4DeleteSecIpInterface (UINT4 u4VrId, UINT1 *pu1IfName,
                                 UINT4 u4IfIndex, UINT4 u4IpAddr,
                                 UINT2 u2VlanId)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4DeleteSecIpInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_DELETE_SEC_IP_INTERFACE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4DeleteSecIpInterface;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1IfName = pu1IfName;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IpAddr = u4IpAddr;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsNpIpv4MapVlansToIpInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4MapVlansToIpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4MapVlansToIpInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsNpIpv4MapVlansToIpInterface (tNpIpVlanMappingInfo * pPvlanMappingInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4MapVlansToIpInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_MAP_VLANS_TO_IP_INTERFACE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4MapVlansToIpInterface;

    pEntry->pPvlanMappingInfo = pPvlanMappingInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsNpIpv4IntfStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4IntfStatus
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4IntfStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsNpIpv4IntfStatus (UINT4 u4VrId, UINT4 u4IfStatus,
                       tFsNpIp4IntInfo * pIpIntInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4IntfStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_INTF_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4IntfStatus;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IfStatus = u4IfStatus;
    pEntry->pIpIntInfo = pIpIntInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsNpIpv4UpdateIpInterfaceStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4UpdateIpInterfaceStatus
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4UpdateIpInterfaceStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsNpIpv4UpdateIpInterfaceStatus (UINT4 u4VrId, UINT1 *pu1IfName,
                                    UINT4 u4CfaIfIndex, UINT4 u4IpAddr,
                                    UINT4 u4IpSubNetMask, UINT2 u2VlanId,
                                    UINT1 *au1MacAddr, UINT4 u4Status)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4UpdateIpInterfaceStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_UPDATE_IP_INTERFACE_STATUS,    /* Function/OpCode */
                         u4CfaIfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4UpdateIpInterfaceStatus;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1IfName = pu1IfName;
    pEntry->u4CfaIfIndex = u4CfaIfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->u4IpSubNetMask = u4IpSubNetMask;
    pEntry->u2VlanId = u2VlanId;
    pEntry->au1MacAddr = au1MacAddr;
    pEntry->u4Status = u4Status;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsNpIpv4CreateIpInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4CreateIpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4CreateIpInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsNpIpv4CreateIpInterface (UINT4 u4VrId, UINT1 *pu1IfName,
                              UINT4 u4CfaIfIndex, UINT4 u4IpAddr,
                              UINT4 u4IpSubNetMask, UINT2 u2VlanId,
                              UINT1 *au1MacAddr)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4CreateIpInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_CREATE_IP_INTERFACE,    /* Function/OpCode */
                         u4CfaIfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4CreateIpInterface;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1IfName = pu1IfName;
    pEntry->u4CfaIfIndex = u4CfaIfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->u4IpSubNetMask = u4IpSubNetMask;
    pEntry->u2VlanId = u2VlanId;
    pEntry->au1MacAddr = au1MacAddr;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsNpIpv4CreateL3SubInterface 
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4CreateIpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4CreateL3SubInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsNpIpv4CreateL3SubInterface (UINT4 u4VrId, UINT1 *pu1IfName,
                                 UINT4 u4CfaIfIndex, UINT4 u4IpAddr,
                                 UINT4 u4IpSubNetMask, UINT2 u2VlanId,
                                 UINT1 *au1MacAddr, UINT4 u4ParentIfIndex)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4CreateL3SubInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_CREATE_L3SUB_INTERFACE,    /* Function/OpCode */
                         u4CfaIfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4CreateL3SubInterface;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1IfName = pu1IfName;
    pEntry->u4CfaIfIndex = u4CfaIfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->u4IpSubNetMask = u4IpSubNetMask;
    pEntry->u2VlanId = u2VlanId;
    pEntry->au1MacAddr = au1MacAddr;
    pEntry->u4ParentIfIndex = u4ParentIfIndex;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsNpIpv4L3IpInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4L3IpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4L3IpInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsNpIpv4L3IpInterface (tL3Action L3Action, tFsNpL3IfInfo * pFsNpL3IfInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4L3IpInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_L3_IP_INTERFACE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4L3IpInterface;

    pEntry->L3Action = L3Action;
    pEntry->pFsNpL3IfInfo = pFsNpL3IfInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        if (pFsNpL3IfInfo->u2ErrCode == FNP_RESERVED_VLAN_EXHAUST_ERR)
        {
            CLI_SET_ERR (CLI_CFA_RSVD_VLAN_EXHAUST_ERR);
        }
        return (FNP_FAILURE);
    }
    else
    {
        return (FNP_SUCCESS);
    }

}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsNpIpv4ModifyIpInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4ModifyIpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4ModifyIpInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsNpIpv4ModifyIpInterface (UINT4 u4VrId, UINT1 *pu1IfName,
                              UINT4 u4CfaIfIndex, UINT4 u4IpAddr,
                              UINT4 u4IpSubNetMask, UINT2 u2VlanId,
                              UINT1 *au1MacAddr)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4ModifyIpInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_MODIFY_IP_INTERFACE,    /* Function/OpCode */
                         u4CfaIfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4ModifyIpInterface;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1IfName = pu1IfName;
    pEntry->u4CfaIfIndex = u4CfaIfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->u4IpSubNetMask = u4IpSubNetMask;
    pEntry->u2VlanId = u2VlanId;
    pEntry->au1MacAddr = au1MacAddr;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef VRRP_WANTED
/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsNpIpv4VrrpIntfDeleteWr                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrrpIntfDeleteWr
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrrpIntfDeleteWr
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsNpIpv4VrrpIntfDeleteWr (tNpVlanId u2VlanId, UINT4 u4IfIndex,
                             UINT4 u4IpAddr, UINT1 *au1MacAddr)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4VrrpIntfDeleteWr *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRRP_INTF_DELETE_WR,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrrpIntfDeleteWr;

    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->au1MacAddr = au1MacAddr;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* VRRP_WANTED */
#ifdef RSTP_WANTED
/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsMiRstpNpSetPortState                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiRstpNpSetPortState
 *                                                                          
 *    Input(s)            : Arguments of FsMiRstpNpSetPortState
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsMiRstpNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1Status)
{
    tFsHwNp             FsHwNp;
    tRstpNpModInfo     *pRstpNpModInfo = NULL;
    tRstpNpWrFsMiRstpNpSetPortState *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RSTP_MOD,    /* Module ID */
                         FS_MI_RSTP_NP_SET_PORT_STATE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRstpNpModInfo = &(FsHwNp.RstpNpModInfo);
    pEntry = &pRstpNpModInfo->RstpNpFsMiRstpNpSetPortState;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1Status = u1Status;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* RSTP_WANTED */
#ifdef IP6_WANTED
/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsNpIpv6IntfStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6IntfStatus
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6IntfStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsNpIpv6IntfStatus (UINT4 u4VrId, UINT4 u4IfStatus, tFsNpIntInfo * pIntfInfo)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpIpv6IntfStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_IPV6_INTF_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpIpv6IntfStatus;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IfStatus = u4IfStatus;
    pEntry->pIntfInfo = pIntfInfo;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* IP6_WANTED */

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsHwGetVlanIntfStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsHwGetStat
 *                                                                          
 *    Input(s)            : Arguments of FsHwGetStat
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsHwGetVlanIntfStats (UINT4 u4IfIndex, INT1 i1StatType, UINT4 *pu4Value)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsHwGetVlanIntfStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_HW_GET_VLAN_INTF_STAT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsHwGetVlanIntfStats;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i1StatType = i1StatType;
    pEntry->pu4Value = pu4Value;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsCfaHwRemoveIpNetRcvdDlfInHash                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsCfaHwRemoveIpNetRcvdDlfInHash
 *                                                                          
 *    Input(s)            : Arguments of FsCfaHwRemoveIpNetRcvdDlfInHash
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsCfaHwRemoveIpNetRcvdDlfInHash (UINT4 u4ContextId, UINT4 u4IpNet,
                                    UINT4 u4IpMask)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsCfaHwRemoveIpNetRcvdDlfInHash *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_CFA_HW_REMOVE_IP_NET_RCVD_DLF_IN_HASH,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsCfaHwRemoveIpNetRcvdDlfInHash;

    pEntry->u4IpNet = u4IpNet;
    pEntry->u4IpMask = u4IpMask;
    pEntry->u4ContextId = u4ContextId;
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#ifdef IP6_WANTED
/***************************************************************************
 *                                                                          
 *    Function Name       : CfaFsCfaHwRemoveIp6NetRcvdDlfInHash
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsCfaHwRemoveIp6NetRcvdDlfInHash
 *                                                                          
 *    Input(s)            : Arguments of FsCfaHwRemoveIp6NetRcvdDlfInHash
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
CfaFsCfaHwRemoveIp6NetRcvdDlfInHash (UINT4 u4ContextId, tIp6Addr Ip6Addr,
                                     UINT4 u4Prefix)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsCfaHwRemoveIp6NetRcvdDlfInHash *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_CFA_HW_REMOVE_IP6_NET_RCVD_DLF_IN_HASH,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsCfaHwRemoveIp6NetRcvdDlfInHash;

    pEntry->Ip6Addr = Ip6Addr;
    pEntry->u4Prefix = u4Prefix;
    pEntry->u4ContextId = u4ContextId;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif

#endif /* __CFA_NPAPI_C__ */
