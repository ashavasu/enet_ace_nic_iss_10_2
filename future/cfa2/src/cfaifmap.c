/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfaifmap.c,v 1.6 2013/12/07 11:00:31 siva Exp $
 *
 * Description:This file has the functions related to
 *             Private VLAN L3 support in CFA.
 *
 *******************************************************************/
#include "cfainc.h"

/******************************************************************************
 * Function           : CfaIvrMapInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE
 * Action             : Routine to initialize the PVLAN IVR mapping table
 ******************************************************************************/
INT4
CfaIvrMapInit (VOID)
{
    CFA_IVR_MAPPING_MEMPOOL () =
        CFAMemPoolIds[MAX_CFA_IVR_MAPPING_ENTRIES_SIZING_ID];

    /* Create and Initialize the RBTree root node for the gCfaIvrMapTable */

    gCfaIvrMapTable = RBTreeCreateEmbedded (0,
                                            (tRBCompareFn) CfaIvrMapInfoRBCmp);

    if (gCfaIvrMapTable == NULL)
    {
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaIvrMapDeInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : Routine to Deinitialize the PVLAN IVR Mapping table
 ******************************************************************************/
INT4
CfaIvrMapDeInit (VOID)
{
    /* Deleting the RBTree node for the PvlanIvrTable */
    if (gCfaIvrMapTable != NULL)
    {
        RBTreeDestroy (gCfaIvrMapTable, CfaIvrMapInfoRBFree, 0);
        gCfaIvrMapTable = NULL;
    }

    /* Delete the mempool for secondary vlan */
    if (MemDeleteMemPool (CFA_IVR_MAPPING_MEMPOOL ()) != MEM_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                 "Releasing CFA PVLAN Mempool buffer failed.\n");

        return CFA_FAILURE;
    }

    CFA_IVR_MAPPING_MEMPOOL () = 0;

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaIvrMapInfoRBFree                                  */
/*                                                                           */
/* Description        : This routine releases the memory allocated to each   */
/*                      node of PVlan RBTree                                 */
/*                                                                           */
/* Input(s)           : pRBElem - pointer to the node to be freed            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS / CFA_FAILURE                            */
/*****************************************************************************/
INT4
CfaIvrMapInfoRBFree (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (CFA_IVR_MAPPING_MEMPOOL (), (UINT1 *) pRBElem);
        pRBElem = NULL;
        return CFA_SUCCESS;
    }

    return CFA_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : CfaIvrMapInfoRBCmp                               */
/*                                                                           */
/*    Description         : This function is used to compare the indices of  */
/*                          two pvlan entries. The indices are context       */
/*                          id and vlan id.                                  */
/*                                                                           */
/*    Input(s)            : pNodeA,pNodeB - PVlan Entries.                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - pNodeA > pNodeB                              */
/*                          -1 - pNodeA < pNodeB                             */
/*                          0 - pNodeA == PNodeB                             */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIvrMapInfoRBCmp (tRBElem * pNodeA, tRBElem * pNodeB)
{
    tCfaVlanToIvrMapInfo *pPvlanEntryA = (tCfaVlanToIvrMapInfo *) pNodeA;
    tCfaVlanToIvrMapInfo *pPvlanEntryB = (tCfaVlanToIvrMapInfo *) pNodeB;

    if (pPvlanEntryA->u4ContextId != pPvlanEntryB->u4ContextId)
    {
        if (pPvlanEntryA->u4ContextId > pPvlanEntryB->u4ContextId)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }

    if (pPvlanEntryA->VlanId != pPvlanEntryB->VlanId)
    {
        if (pPvlanEntryA->VlanId > pPvlanEntryB->VlanId)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }

    return 0;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : CfaIvrMapGetVlanIfIndex                          */
/*                                                                           */
/*    Description         : This function is used to compare the indices of  */
/*                          two pvlan entries. The indices are context       */
/*                          id and vlan id.                                  */
/*                                                                           */
/*    Input(s)            : u4IfIndex     - Interface Index                  */
/*                          VlanId        - VlanId                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CFA_SUCCESS / CFA_INVALID_INDEX                  */
/*                                                                           */
/*****************************************************************************/

UINT4
CfaIvrMapGetVlanIfIndex (UINT4 u4L2CxtId, tVlanId VlanId)
{
    tCfaVlanToIvrMapInfo PvlanInfo;
    tCfaVlanToIvrMapInfo PvlanEntry;
    tCfaVlanToIvrMapInfo *pPvlanEntry = NULL;

    MEMSET (&PvlanEntry, 0, sizeof (tCfaVlanToIvrMapInfo));

    PvlanEntry.u4ContextId = u4L2CxtId;
    PvlanEntry.VlanId = VlanId;

    CFA_DS_LOCK ();
    pPvlanEntry = (tCfaVlanToIvrMapInfo *) RBTreeGet (gCfaIvrMapTable,
                                                      (tRBElem *) & PvlanEntry);

    if (pPvlanEntry == NULL)
    {
        CFA_DS_UNLOCK ();
        return CFA_INVALID_INDEX;
    }

    MEMCPY (&PvlanInfo, pPvlanEntry, sizeof (tCfaVlanToIvrMapInfo));

    CFA_DS_UNLOCK ();

    return PvlanInfo.u4IvrIfIndex;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : CfaIvrMapGetInfo                                 */
/*                                                                           */
/*    Description         : This function is used to get the the IVR mapping */
/*                          entries from the IVR Map table                   */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                        : VlanId    - Vlan Id                              */
/*                                                                           */
/*    Output(s)           : pPvlanInfo - IVR Map Entry                       */
/*                                                                           */
/*    Returns             : CFA_FAILURE / CFA_SUCCESS                        */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIvrMapGetInfo (UINT4 u4IfIndex, tVlanId VlanId,
                  tCfaVlanToIvrMapInfo * pPvlanInfo)
{
    tCfaVlanToIvrMapInfo PvlanEntry;
    tCfaVlanToIvrMapInfo *pPvlanEntry = NULL;
    UINT4               u4L2CxtId = 0;

    MEMSET (&PvlanEntry, 0, sizeof (tCfaVlanToIvrMapInfo));

    if (u4IfIndex != 0)
    {
        if (VcmGetL2CxtIdForIpIface (u4IfIndex, &u4L2CxtId) == VCM_FAILURE)
        {
            return CFA_FAILURE;
        }
    }

    PvlanEntry.u4ContextId = u4L2CxtId;
    PvlanEntry.VlanId = VlanId;
    PvlanEntry.u4IvrIfIndex = u4IfIndex;

    CFA_DS_LOCK ();
    pPvlanEntry = (tCfaVlanToIvrMapInfo *) RBTreeGet (gCfaIvrMapTable,
                                                      (tRBElem *) & PvlanEntry);

    if (pPvlanEntry == NULL)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    if (pPvlanInfo != NULL)
    {
        MEMCPY (pPvlanInfo, pPvlanEntry, sizeof (tCfaVlanToIvrMapInfo));
    }

    CFA_DS_UNLOCK ();

    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : CfaIvrMapGetNextInfo                             */
/*                                                                           */
/*    Description         : This function is used to get the next entry from */
/*                          IVR Mapping table.                               */
/*                                                                           */
/*    Input(s)            : u4IfIndex, VlanId                                */
/*                                                                           */
/*    Output(s)           : pi4NextIfMainIndex, pi4NextIfIvrMappingVlan      */
/*                                                                           */
/*    Returns             : CFA_SUCCESS / CFA_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIvrMapGetNextInfo (UINT4 u4IfIndex, tVlanId VlanId,
                      INT4 *pi4NextIfMainIndex, INT4 *pi4NextIfIvrMappingVlan)
{
    tCfaVlanToIvrMapInfo PvlanEntry;
    tCfaVlanToIvrMapInfo *pNextPvlanEntry = NULL;
    UINT4               u4L2CxtId = 0;
    UINT4               u4TmpIfIndex = 0;

    MEMSET (&PvlanEntry, 0, sizeof (tCfaVlanToIvrMapInfo));

    if (u4IfIndex == 0)
    {
        u4IfIndex = CFA_MIN_IVR_IF_INDEX;
        VlanId = 0;
    }

    while (1)
    {
        if (VcmGetL2CxtIdForIpIface (u4IfIndex, &u4L2CxtId) == VCM_FAILURE)
        {
            return CFA_FAILURE;
        }

        PvlanEntry.u4ContextId = u4L2CxtId;
        PvlanEntry.VlanId = VlanId;

        CFA_DS_LOCK ();
        pNextPvlanEntry = RBTreeGetNext (gCfaIvrMapTable,
                                         &PvlanEntry, CfaIvrMapInfoRBCmp);

        if (pNextPvlanEntry != NULL)
        {
            if (pNextPvlanEntry->u4IvrIfIndex != u4IfIndex)
            {
                VlanId = pNextPvlanEntry->VlanId;
                CFA_DS_UNLOCK ();
                continue;
            }

            if (pNextPvlanEntry->u4ContextId == u4L2CxtId)
            {
                *pi4NextIfMainIndex = (INT4) pNextPvlanEntry->u4IvrIfIndex;
                *pi4NextIfIvrMappingVlan = pNextPvlanEntry->VlanId;
                CFA_DS_UNLOCK ();
                break;
            }
        }

        CFA_DS_UNLOCK ();

        if (CfaGetNextIvrIfIndex (u4IfIndex, &u4TmpIfIndex) == CFA_FAILURE)
        {
            return CFA_FAILURE;
        }

        u4IfIndex = u4TmpIfIndex;
        VlanId = 0;

    }                            /* end of while (1) */

    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : CfaIvrMapSetInfo                                 */
/*                                                                           */
/*    Description         : This function is used create new node in the IVR */
/*                          mapping table                                    */
/*                                                                           */
/*    Input(s)            : VlanId, u4IfIndex                                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CFA_SUCCESS / CFA_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIvrMapSetInfo (UINT4 u4IfIndex, tVlanId VlanId)
{
    tCfaVlanToIvrMapInfo PvlanEntry;
    tCfaVlanToIvrMapInfo *pPvlanEntry = NULL;
    UINT4               u4L2CxtId = 0;
#ifdef NPAPI_WANTED
    UINT4               u4VrId = 0;
#endif

    MEMSET (&PvlanEntry, 0, sizeof (tCfaVlanToIvrMapInfo));

    if (VcmGetL2CxtIdForIpIface (u4IfIndex, &u4L2CxtId) == VCM_FAILURE)
    {
        return CFA_FAILURE;
    }

    PvlanEntry.u4ContextId = u4L2CxtId;
    PvlanEntry.VlanId = VlanId;
    PvlanEntry.u4IvrIfIndex = u4IfIndex;

    CFA_DS_LOCK ();

    /* Check if the VLAN is already mapped to the IVR interface */

    pPvlanEntry = (tCfaVlanToIvrMapInfo *) RBTreeGet (gCfaIvrMapTable,
                                                      (tRBElem *) & PvlanEntry);

    if (pPvlanEntry != NULL)
    {
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    pPvlanEntry = MemAllocMemBlk (CFA_IVR_MAPPING_MEMPOOL ());

    if (pPvlanEntry == NULL)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    /* Add secondary vlan for the primary vlan interface index */

    pPvlanEntry->VlanId = VlanId;
    pPvlanEntry->u4IvrIfIndex = u4IfIndex;
    pPvlanEntry->u4ContextId = u4L2CxtId;

    if (RBTreeAdd (gCfaIvrMapTable, (tRBElem *) pPvlanEntry) == RB_FAILURE)
    {
        MemReleaseMemBlock (CFA_IVR_MAPPING_MEMPOOL (), (UINT1 *) pPvlanEntry);
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    if (CFA_CDB_IF_OPER (u4IfIndex) != CFA_IF_UP)
    {
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    /* Program mapping of ivr interface to secondary vlan mapping
     * into NP
     */

#ifdef NPAPI_WANTED
    MEMSET (&gIpVlanMappingInfo, 0, sizeof (tNpIpVlanMappingInfo));
    /* Going to take the Lock of Other Module. (VCM_LOCK).
     * So releasing my Lock here..
     * To Avoid Dead Lock Scenario
     */
    CFA_DS_UNLOCK ();
    VcmGetIfMapVcId (u4IfIndex, &u4VrId);
    /* Taking My lock again */
    CFA_DS_LOCK ();
    gIpVlanMappingInfo.u4IvrIfIndex = u4IfIndex;
    gIpVlanMappingInfo.IvrVlanId = CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex);
    gIpVlanMappingInfo.MappedVlanId = VlanId;
    gIpVlanMappingInfo.u4VrtId = u4VrId;
    gIpVlanMappingInfo.u1Action = CFA_ADD;

    MEMCPY (gIpVlanMappingInfo.IvrMac,
            CFA_CDB_IF_ENTRY (u4IfIndex)->au1MacAddr, CFA_ENET_ADDR_LEN);
#endif

    CFA_DS_UNLOCK ();

#ifdef NPAPI_WANTED
    if (CfaFsNpIpv4MapVlansToIpInterface (&gIpVlanMappingInfo) == FNP_FAILURE)
    {
        return CFA_FAILURE;
    }
#endif
    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : CfaIvrMapDeleteInfo                              */
/*                                                                           */
/*    Description         : This function is used to delete node from the IVR*/
/*                          Mapping table                                    */
/*                                                                           */
/*    Input(s)            : u4IfMainIndex, u4IfIvrMappingVlan                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CFA_SUCCESS / CFA_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT4
CfaIvrMapDeleteInfo (UINT4 u4IfMainIndex, UINT4 u4IfIvrMappingVlan)
{
    tCfaVlanToIvrMapInfo PvlanEntry;
    tCfaVlanToIvrMapInfo *pPvlanEntry = NULL;
#ifdef NPAPI_WANTED
    UINT4               u4VrId;
#endif
    UINT4               u4L2CxtId;

    if (VcmGetL2CxtIdForIpIface (u4IfMainIndex, &u4L2CxtId) == VCM_FAILURE)
    {
        return CFA_FAILURE;
    }

    PvlanEntry.u4ContextId = u4L2CxtId;
    PvlanEntry.VlanId = (tVlanId) u4IfIvrMappingVlan;
    PvlanEntry.u4IvrIfIndex = u4IfMainIndex;

    /* Check if the secondary VLAN is mapped to the Primary VLAN
     * interface.
     */

    CFA_DS_LOCK ();
    pPvlanEntry = (tCfaVlanToIvrMapInfo *) RBTreeGet (gCfaIvrMapTable,
                                                      (tRBElem *) & PvlanEntry);

    if (pPvlanEntry == NULL)
    {
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    /* secondary VLAN is mapped to Primary VLAN interface.
     * so delete the secondary vlan entry
     */

    if (RBTreeRem (gCfaIvrMapTable, (tRBElem *) pPvlanEntry) == NULL)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    MemReleaseMemBlock (CFA_IVR_MAPPING_MEMPOOL (), (UINT1 *) pPvlanEntry);

    if (CFA_CDB_IF_OPER (u4IfMainIndex) != CFA_IF_UP)
    {
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    /* Remove mapping of ivr interface to secondary vlan from NP
     */

#ifdef NPAPI_WANTED

    /* Going to take the Lock of Other Module. (VCM_LOCK).
     * So releasing my Lock here..
     * To Avoid Dead Lock Scenario
     */
    CFA_DS_UNLOCK ();
    VcmGetIfMapVcId (u4IfMainIndex, &u4VrId);
    CFA_DS_LOCK ();
    MEMSET (&gIpVlanMappingInfo, 0, sizeof (tNpIpVlanMappingInfo));
    gIpVlanMappingInfo.u4IvrIfIndex = u4IfMainIndex;
    gIpVlanMappingInfo.IvrVlanId = CFA_CDB_IF_VLANID_OF_IVR (u4IfMainIndex);
    gIpVlanMappingInfo.MappedVlanId = u4IfIvrMappingVlan;
    gIpVlanMappingInfo.u4VrtId = u4VrId;
    gIpVlanMappingInfo.u1Action = CFA_DEL;

    MEMCPY (gIpVlanMappingInfo.IvrMac,
            CFA_CDB_IF_ENTRY (u4IfMainIndex)->au1MacAddr, CFA_ENET_ADDR_LEN);
#endif

    CFA_DS_UNLOCK ();

#ifdef NPAPI_WANTED
    if (CfaFsNpIpv4MapVlansToIpInterface (&gIpVlanMappingInfo) == FNP_FAILURE)
    {
        return CFA_FAILURE;
    }
#endif

    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : CfaIvrMapDeleteMappedVlans                       */
/*                                                                           */
/*    Description         : This function returns the next valid IVR interfac*/
/*                          index                                            */
/*                                                                           */
/*    Input(s)            : u4IfIndex                                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CFA_SUCCESS / CFA_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT4
CfaIvrMapDeleteMappedVlans (UINT4 u4IfIndex)
{
    INT4                i4NextVlan = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4PrevVlan = 0;
    INT4                i4RetVal = 0;
    UINT4               u4ErrorCode = 0;

    /* Test if all the secondary VLAN mapped to the IVR interface
     * can be deleted.
     */

    i4PrevIfIndex = (INT4) u4IfIndex;
    i4PrevVlan = 0;

    i4RetVal = nmhGetNextIndexIfIvrMappingTable (i4PrevIfIndex,
                                                 &i4NextIfIndex, i4PrevVlan,
                                                 &i4NextVlan);

    while (i4RetVal != SNMP_FAILURE)
    {
        if (u4IfIndex != (UINT4) i4NextIfIndex)
        {
            break;
        }

        if (nmhTestv2IfIvrMappingRowStatus (&u4ErrorCode, i4NextIfIndex,
                                            i4NextVlan,
                                            DESTROY) == SNMP_FAILURE)
        {
            return CFA_FAILURE;
        }

        i4PrevIfIndex = i4NextIfIndex;
        i4PrevVlan = i4NextVlan;

        i4RetVal = nmhGetNextIndexIfIvrMappingTable (i4PrevIfIndex,
                                                     &i4NextIfIndex, i4PrevVlan,
                                                     &i4NextVlan);
    }

    /* Delete all the secondary VLAN to IVR mapping for the
     * VLAN intercace 
     */

    i4PrevIfIndex = (INT4) u4IfIndex;
    i4PrevVlan = 0;

    i4RetVal = nmhGetNextIndexIfIvrMappingTable (i4PrevIfIndex,
                                                 &i4NextIfIndex, i4PrevVlan,
                                                 &i4NextVlan);

    while (i4RetVal != SNMP_FAILURE)
    {
        if (u4IfIndex != (UINT4) i4NextIfIndex)
        {
            break;
        }

        nmhSetIfIvrMappingRowStatus (i4NextIfIndex, i4NextVlan, DESTROY);

        i4PrevIfIndex = i4NextIfIndex;
        i4PrevVlan = i4NextVlan;

        i4RetVal = nmhGetNextIndexIfIvrMappingTable (i4PrevIfIndex,
                                                     &i4NextIfIndex, i4PrevVlan,
                                                     &i4NextVlan);
    }

    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : CfaIvrMapGetMappedVlans                          */
/*                                                                           */
/*    Description         : This function returns secondary VLANs mapped to  */
/*                          the primary VLAN IVR interface.                  */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Ivr IfIndex                          */
/*                                                                           */
/*    Output(s)           : MappedVlanList - List of secondary vlans mapped  */
/*                                           to this given ivr interface     */
/*                                                                           */
/*    Returns             : Number of vlans mapped to this IVR interface.    */
/*                                                                           */
/*****************************************************************************/

INT4
CfaIvrMapGetMappedVlans (UINT4 u4IfIndex, tVlanList MappedVlanList)
{
    tCfaVlanToIvrMapInfo IvrMapInfo;
    tCfaVlanToIvrMapInfo *pNextIvrMapInfo = NULL;
    UINT4               u4L2CxtId = 0;
    INT4                i4VlanCount = 0;

    if (VcmGetL2CxtIdForIpIface (u4IfIndex, &u4L2CxtId) == VCM_FAILURE)
    {
        return i4VlanCount;
    }

    IvrMapInfo.u4ContextId = u4L2CxtId;
    IvrMapInfo.VlanId = 0;

    CFA_DS_LOCK ();

    do
    {
        pNextIvrMapInfo = RBTreeGetNext (gCfaIvrMapTable,
                                         &IvrMapInfo, CfaIvrMapInfoRBCmp);

        if (pNextIvrMapInfo != NULL)
        {
            IvrMapInfo.VlanId = pNextIvrMapInfo->VlanId;
            if (pNextIvrMapInfo->u4IvrIfIndex != u4IfIndex)
            {
                continue;
            }

            /* For a primary vlan, only the secondary vlans available in that
             * L2context, can be mapped. Hence when the L2ContextId changes,
             * we can break from the loop.
             */
            if (pNextIvrMapInfo->u4ContextId != u4L2CxtId)
            {
                break;
            }
            OSIX_BITLIST_SET_BIT (MappedVlanList, pNextIvrMapInfo->VlanId,
                                  VLAN_LIST_SIZE);
            i4VlanCount++;
        }
    }
    while (pNextIvrMapInfo != NULL);

    CFA_DS_UNLOCK ();

    return i4VlanCount;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : CfaIvrMapUpdtMappedVlans                         */
/*                                                                           */
/*    Description         : If the given vlan is a primary vlan, then this   */
/*                          function maps or unmaps the list of  secondary   */
/*                          vlans from this IVR interface based on u1Status. */
/*                                                                           */
/*    Input(s)            : u4VrId -    Virtual Router ID                    */
/*                          u4IfIndex - Ivr IfIndex                          */
/*                          u2VlanId  - primary vlan id                      */
/*                          u1Status  - flag indication add/del              */
/*                                                                           */
/*    Output(s)           : NONE                                             */
/*                                                                           */
/*    Returns             : Number of vlans mapped to this IVR interface.    */
/*                                                                           */
/*****************************************************************************/

INT4
CfaIvrMapUpdtMappedVlans (UINT4 u4VrId, UINT4 u4IfIndex, tVlanId u2VlanId,
                          UINT1 u1Status)
{
#ifdef NPAPI_WANTED
    tL2PvlanMappingInfo L2PvlanMappingInfo;

    MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    /* Get the L2 ContextId associated with this IVR interface. */
    VcmGetL2CxtIdForIpIface (u4IfIndex, &(L2PvlanMappingInfo.u4ContextId));

    L2PvlanMappingInfo.u1RequestType = L2IWF_VLAN_TYPE;
    L2PvlanMappingInfo.InVlanId = u2VlanId;

    /* Get the VLAN type */
    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    if (L2PvlanMappingInfo.u1VlanType == L2IWF_PRIMARY_VLAN)
    {
        MEMSET (&gIpVlanMappingInfo, 0, sizeof (tNpIpVlanMappingInfo));

        gIpVlanMappingInfo.u4IvrIfIndex = u4IfIndex;
        gIpVlanMappingInfo.IvrVlanId = u2VlanId;
        gIpVlanMappingInfo.u4VrtId = u4VrId;

        /* Fetch list of secondary VLAN mapped to primary VLAN
         * IVR interface
         */
        if ((CfaIvrMapGetMappedVlans (u4IfIndex,
                                      gIpVlanMappingInfo.MappedVlanList)) > 0)
        {
            CfaGetIfHwAddr (u4IfIndex, gIpVlanMappingInfo.IvrMac);

            if (u1Status == CFA_IF_CREATE)
            {
                gIpVlanMappingInfo.u1Action = CFA_ADD;
            }
            else if (u1Status == CFA_IF_DELETE)
            {
                gIpVlanMappingInfo.u1Action = CFA_DEL;
            }

            if (CfaFsNpIpv4MapVlansToIpInterface (&gIpVlanMappingInfo) ==
                FNP_FAILURE)
            {
                return CFA_FAILURE;
            }
        }
    }
#else
    UNUSED_PARAM (u4VrId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (u1Status);
#endif
    return CFA_SUCCESS;
}
