/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfautils.c,v 1.276 2018/01/08 12:28:33 siva Exp $
 *
 * Description:This file contains utility routines common to all
 *             the modules. 
 *
 *******************************************************************/
#include "cfainc.h"
#include "fscfacli.h"
#include "ifmibcli.h"

extern UINT4        gRmonEnableStatusFlag;
#ifdef ISS_TEST_WANTED
extern INT4         gi4CentralizedIPSupport;
#endif
unCfaUtilCallBackEntry CFA_UTIL_CALL_BACK[CFA_CUST_MAX_CALL_BACK_EVENTS];

#ifdef MBSM_WANTED
INT4
 
 
 
 
 
 
 
 CfaGetSlotLocalPortNum (UINT4 u4IfType, INT4 i4PortNum, INT4 i4SlotNum,
                         UINT4 *pu4RetValIfIndex);
#endif
/*GSN change */
INT4
 
 
 
 
 
 
 
 CfaIPVXTestIpAddrpSubnetMask (INT4 i4IfMainIndex,
                               UINT4 u4TestValIfIpAddr,
                               UINT4 u4TestValIfIpSubnetMask);

UINT1               gau1IvrPortBitMaskMap[CFA_MGMT_VLANS_PER_BYTE] =
    { 0x01, 0x80, 0x40, 0x20, 0x10,
    0x08, 0x04, 0x02
};

/* SUBTYPE changes */
tCfaIfaceInfo       asCfaIfaces[CLI_MAX_IFTYPES][CLI_MAX_IFSUBTYPES] = {
#ifdef CFA_UNIQUE_INTF_NAME
    {
     {CFA_GI_ENET, "Et", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
#else
    {
     {CFA_GI_ENET, "Gi", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
#endif
    {
     {CFA_FA_ENET, "Fa", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_XE_ENET, "Ex", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_XL_ENET, "Xl", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_LVI_ENET, "Lvi", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
#ifdef CFA_UNIQUE_INTF_NAME
    {
     {CFA_ENET, "eeth", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
#else
    {
     {CFA_ENET, "eth", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
#endif
    {
     {CFA_ENET, "znb", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_TUNNEL, "tunnel", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_RFC1483, "rfc1483", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_L3IPVLAN, "vlan", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_LAGG, "po", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_ENET, "Slot", "0"}
#ifdef WGS_WANTED
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_L2VLAN, "vlanMgmt", "0"}
#endif
#ifdef MBSM_WANTED
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_ENET, "cpu", "0"}
#endif
#ifdef PPP_WANTED
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_PPP, "ppp", "0"}
#endif /* PPP_WANTED */
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_L3IPVLAN, "linuxvlan", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_LOOPBACK, "loopback", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_BRIDGED_INTERFACE, "virtual", "0"}
     ,
     {0, "\0", "0"}
     }
#ifdef PBB_WANTED
    ,
    {
     {CFA_ILAN, "internal-lan", "0"}
     ,
     {0, "\0", "0"}
     }
#endif
    ,
    {
     {CFA_PROP_VIRTUAL_INTERFACE, "sisp", "0"}
     ,
     {CFA_PROP_VIRTUAL_INTERFACE, "ac", "0"}
     }
    ,
    {
     {CFA_PSEUDO_WIRE, "pw", "0"}
     ,
     {0, "\0", "0"}
     }
#ifdef VXLAN_WANTED
    ,
    {
     {CFA_VXLAN_NVE, "nve", "0"}
     ,
     {0, "\0", "0"}
     }
#endif
#ifdef MPLS_WANTED
    ,
    {
     {CFA_MPLS_TUNNEL, "mplstunnel", "0"}
     ,
     {0, "\0", "0"}
     }
#endif
#ifdef MBSM_WANTED
    ,
    {
     {CFA_STACK_ENET, "Stack", "0"}
     ,
     {0, "\0", "0"}
     }
#endif
#if defined (WLC_WANTED) || defined (WTP_WANTED)
    ,
    {
     {CFA_RADIO, "wifi", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_WLAN_RADIO, "ath", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_CAPWAP_VIRT_RADIO, "virt-radio", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_CAPWAP_DOT11_PROFILE, "dot11-profile", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_CAPWAP_DOT11_BSS, "dot11-bss", "0"}
     ,
     {0, "\0", "0"}
     }
#endif
#ifdef HDLC_WANTED
    ,
    {
     {CFA_HDLC, "serial", "0"}
     ,
     {0, "\0", "0"}
     }
#endif
    ,
    {
     {CFA_UPLINK_ACCESS_PORT, "uap", "0"}
     ,
     {0, "\0", "0"}
     }
    ,
    {
     {CFA_STATION_FACING_BRIDGE_PORT, "s-channel", "0"}
     ,
     {0, "\0", "0"}
     }
#ifdef VCPEMGR_WANTED
    ,
    {
     {CFA_TAP, "tap", "0"}
     ,
     {0, "\0", "0"}
     }
#endif
};

/*****************************************************************************
 *
 *    Function Name       : CfaCliIsDefaultInterface
 *
 *    Description         : This function retrieves the interface name 
 *                          for the given interface index.
 *
 *    Input(s)            : i4IfaceIndex - Interface Index
 *
 *    Output(s)           : None.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : OSIX_SUCCESS if interface index is default 
 *                         otherwise OSIX_FAILURE.
 *
 *****************************************************************************/
UINT4
CfaCliIsDefaultInterface (INT4 i4IfaceIndex)
{
    if ((CFA_DEFAULT_ROUTER_IFINDEX == i4IfaceIndex)
#ifdef WGS_WANTED
        || (CFA_MGMT_IF_INDEX == i4IfaceIndex)
#endif /* WGS_WANTED */
        )
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetDefaultVlanInterfaceIndex
 *
 *    Description         : This function returns the router's 
 *                          default interface index or default vlan index 
 *
 *    Input(s)            : None. 
 *
 *    Output(s)           : None.
 *
 *    Global Variables Referred : gu4IsIvrEnabled.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : Router's default interface index  or
 *                         default vlan index
 *
 *****************************************************************************/

UINT4
CfaGetDefaultVlanInterfaceIndex (VOID)
{
    return (gu4IsIvrEnabled == CFA_ENABLED) ?
        (CFA_DEFAULT_ROUTER_VLAN_IFINDEX) : (CFA_DEFAULT_ROUTER_IFINDEX);
}

/*****************************************************************************
 *
 *    Function Name       : CfaDetermineSpeed
 *
 *    Description         : This function returns the interface speed.   
 *
 *    Input(s)            : u4IfIndex - interface index of the interface whose
 *                                      speed needs to be set.
 *
 *    Output(s)           : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : interface's speed.  
 *                         
 *
 *****************************************************************************/
INT4
CfaDetermineSpeed (UINT4 u4IfIndex, UINT4 *pu4IfSpeed, UINT4 *pu4IfHighSpeed)
{
#ifdef NPAPI_WANTED
    UINT4               u4Val = 0;
#else
    UINT4               u4IfSpeed;
    UINT4               u4IfHighSpeed;
#endif
    UINT1               u1IfType = CFA_NONE;

    if (!CFA_IF_ENTRY (u4IfIndex))
        return CFA_FAILURE;

    CfaGetIfType (u4IfIndex, &u1IfType);

    switch (u1IfType)
    {
        case CFA_ENET:
#ifdef NPAPI_WANTED
            if (CfaFsMauHwGetMauType
                (u4IfIndex, (INT4) MAU_DEF_INDEX, &u4Val) != FNP_SUCCESS)
            {
                *pu4IfSpeed = CFA_ENET_SPEED;
                *pu4IfHighSpeed = (CFA_ENET_SPEED / CFA_1MB);
                return CFA_SUCCESS;
            }
            if (u4Val <= MAU_TYPE_10_BASE_FLFD)
            {
                *pu4IfSpeed = CFA_ENET_SPEED_10M;
                *pu4IfHighSpeed = (CFA_ENET_SPEED_10M / CFA_1MB);
                return CFA_SUCCESS;
            }
            if (u4Val <= MAU_TYPE_100_BASE_T2FD)
            {
                *pu4IfSpeed = CFA_ENET_SPEED_100M;
                *pu4IfHighSpeed = (CFA_ENET_SPEED_100M / CFA_1MB);
                return CFA_SUCCESS;
            }
            if (u4Val <= MAU_TYPE_1000_BASE_TFD)
            {
                *pu4IfSpeed = CFA_ENET_SPEED_1G;
                *pu4IfHighSpeed = (CFA_ENET_SPEED_1G / CFA_1MB);
                return CFA_SUCCESS;
            }
            if (u4Val <= MAU_TYPE_10GIG_BASE_X)
            {
                *pu4IfSpeed = CFA_ENET_MAX_SPEED_VALUE;
                *pu4IfHighSpeed = CFA_ENET_HISPEED_10G;
                return CFA_SUCCESS;
            }
            if (u4Val <= MAU_TYPE_40GIG_BASE_X)
            {
                /* if any of the above case is matched, then the speed is 40G */
                *pu4IfSpeed = CFA_ENET_MAX_SPEED_VALUE;
                *pu4IfHighSpeed = CFA_ENET_HISPEED_40G;
                return CFA_SUCCESS;
            }
            /* 2.5 GBPS has to be handled as a special case. The constants for
             * 10G and 40G had already been introduced. 2.5GBPS has been
             * introduced later. To keep backward compatability the earlier 
             * constants are not changed. So the check for 2.5 GBPS is done
             * separately.
             */
            if (u4Val <= MAU_TYPE_2500_BASE_TFD)
            {
                *pu4IfSpeed = CFA_ENET_SPEED_2500M;
                *pu4IfHighSpeed = (CFA_ENET_SPEED_2500M / CFA_1MB);
                return CFA_SUCCESS;
            }
            if (u4Val <= MAU_TYPE_56GIG_BASE_X)
            {
                /* if any of the above case is matched, then the speed is 56G */
                *pu4IfSpeed = CFA_ENET_MAX_SPEED_VALUE;
                *pu4IfHighSpeed = CFA_ENET_HISPEED_56G;
                return CFA_SUCCESS;
            }
            if (u4Val <= MAU_TYPE_25GIG_BASE_X)
            {
                *pu4IfSpeed = CFA_ENET_MAX_SPEED_VALUE;
                *pu4IfHighSpeed = CFA_ENET_HISPEED_25G;
                return CFA_SUCCESS;
            }
            if (u4Val <= MAU_TYPE_100GIG_BASE_X)
            {
                *pu4IfSpeed = CFA_ENET_MAX_SPEED_VALUE;
                *pu4IfHighSpeed = CFA_ENET_HISPEED_100G;
                return CFA_SUCCESS;
            }

#else
            if (IssCustDetermineSpeed (u4IfIndex, &u4IfSpeed, &u4IfHighSpeed)
                == ISS_SUCCESS)
            {
                *pu4IfSpeed = u4IfSpeed;
                *pu4IfHighSpeed = u4IfHighSpeed;
                return CFA_SUCCESS;
            }
#endif /* NPAPI_WANTED */
            return CFA_SUCCESS;

        case CFA_LAGG:
            *pu4IfSpeed = 0;
            CfaGetIfHighSpeed (u4IfIndex, pu4IfHighSpeed);
            return CFA_SUCCESS;

        case CFA_L3IPVLAN:
        case CFA_PSEUDO_WIRE:
        case CFA_L3SUB_INTF:
            *pu4IfSpeed = CFA_MAX_PORT_SPEED;
            *pu4IfHighSpeed = (CFA_MAX_PORT_SPEED / CFA_1MB);
            return CFA_SUCCESS;
#ifdef WGS_WANTED
        case CFA_L2VLAN:
            *pu4IfSpeed = CFA_MAX_PORT_SPEED;
            *pu4IfHighSpeed = (CFA_MAX_PORT_SPEED / CFA_1MB);
            return CFA_SUCCESS;
#endif /* WGS_WANTED */
        case CFA_LOOPBACK:
            *pu4IfSpeed = CFA_MAX_PORT_SPEED;
            *pu4IfHighSpeed = (CFA_MAX_PORT_SPEED / CFA_1MB);
            return CFA_SUCCESS;
#ifdef VXLAN_WANTED
        case CFA_VXLAN_NVE:
            *pu4IfSpeed = CFA_MAX_PORT_SPEED;
            *pu4IfHighSpeed = (CFA_MAX_PORT_SPEED / CFA_1MB);
            return CFA_SUCCESS;
#endif
#ifdef VCPEMGR_WANTED
        case CFA_TAP:
            *pu4IfSpeed = CFA_MAX_PORT_SPEED;
            *pu4IfHighSpeed = (CFA_MAX_PORT_SPEED / CFA_1MB);
            return CFA_SUCCESS;
#endif
        default:
            return CFA_FAILURE;
    }
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetVlanIfIndexAndUntagFrame
 *
 *    Description         : This function gives the packet to VLAN module for
 *                          getting the VLAN ID and untag the frame if it is 
 *                          tagged.It returns the VLAN interface index also. 
 *
 *    Input(s)            : u4IfIndex - Incoming interface index
 *                          pBuf      - Incoming data buffer.
 *                          pVlanId   - Pointer to Vlan Id. 
 *
 *    Output(s)           : Untagged frame if the frame is a tagged one.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :  VLAN interface index if success. Else, returns the 
 *                          invalid interface index.
 *****************************************************************************/

UINT4
CfaGetVlanIfIndexAndUntagFrame (UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER * pBuf,
                                tVlanIfaceVlanId * pVlanId)
{
    tVlanIfaceVlanId    VlanId = 0;
    UINT2               u2AggId;
    INT4                i4RetVal;
    UINT4               u4VlanIfIndex = CFA_INVALID_INDEX;
    UINT4               u4L2CxtId = 0;
    BOOL1               bResult = OSIX_FALSE;

    if (L2IwfGetPortChannelForPort (u4IfIndex, &u2AggId) == L2IWF_SUCCESS)
    {
        u4IfIndex = u2AggId;
    }

    if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4L2CxtId) == VCM_FAILURE)
    {
        return CFA_INVALID_INDEX;
    }

    i4RetVal = VlanIdentifyVlanIdAndUntagFrame (IVR_MODULE,
                                                pBuf, u4IfIndex, &VlanId);

    *pVlanId = VlanId;

    if (i4RetVal == VLAN_FORWARD)
    {
        if (CfaIsParentPort (u4IfIndex) == CFA_TRUE)
        {
            u4VlanIfIndex =
                CfaGetL3XSubIfVlanIndexInCxt (u4L2CxtId, VlanId, u4IfIndex);
        }

        if (CFA_INVALID_INDEX == u4VlanIfIndex)
        {
            /* If Given VLAN ID does not match in L3subifIndex range,then
               scan if VLAN ID matches in IVR Index range */
            u4VlanIfIndex = CfaGetVlanInterfaceIndexInCxt (u4L2CxtId, VlanId);
        }

        OSIX_BITLIST_IS_BIT_SET (gSecVlanList, VlanId,
                                 sizeof (tSecVlanList), bResult);
        if (bResult == OSIX_TRUE)
        {
            u4VlanIfIndex = gu4SecIvrIfIndex;
        }

        if (u4VlanIfIndex != CFA_INVALID_INDEX)
        {
            /* Update the stats for the VLAN interface */
            if (CFA_IS_ENET_MAC_BCAST (pBuf))
            {
                CFA_IF_SET_IN_BCAST (u4VlanIfIndex);
            }
            else if (CFA_IS_ENET_MAC_MCAST (pBuf))
            {
                CFA_IF_SET_IN_MCAST (u4VlanIfIndex);
            }
            else
            {
                CFA_IF_SET_IN_UCAST (u4VlanIfIndex);
            }
        }
    }

    return u4VlanIfIndex;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetLinkTrapEnabledStatus
 *
 *    Description         : This function returns the Link Trap status      
 *                          for given port                               
 *
 *    Input(s)            : u4IfIndex. 
 *
 *    Output(s)           : pu1TrapStatus
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :  CFA_SUCCESS/CFA_FAILURE
 *****************************************************************************/

INT1
CfaGetLinkTrapEnabledStatus (UINT4 u4IfIndex, UINT1 *pu1TrapStatus)
{
    if (CfaValidateIfIndex (u4IfIndex) == CFA_SUCCESS)
    {
        *pu1TrapStatus = CFA_IF_TRAP_EN (u4IfIndex);

        return CFA_SUCCESS;
    }
    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaIsL3IpVlanInterface 
 *
 *    Description         : This function returns checks whether the interface
 *                          is a L3VLAN Interface type.
 *
 *    Input(s)            : Interface index. 
 *
 *    Output(s)           : None. 
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :    CFA_SUCCESS when it is L3VLAN interface. Else
 *                            CFA_FAILURE.
 *        
 *****************************************************************************/

INT4                CfaIsL3IpVlanInterface
PROTO ((UINT4 u4IfIndex))
{
    UINT1               u1IfType = CFA_NONE;

    CfaGetIfType (u4IfIndex, &u1IfType);

    if (u1IfType == CFA_L3IPVLAN)
        return CFA_SUCCESS;
    else
        return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name       :CfaIsTunnelInterface 
 *
 *    Description         : This function checks whether  the interface 
 *                          is tunnel interface or not.
 *
 *    Input(s)            : u4Index - interface index
 *
 *    Output(s)           : NONE
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_TRUE-If interface is a tunnel interface
 *****************************************************************************/
UINT1
CfaIsTunnelInterface (UINT4 u4IfIndex)
{
    UINT1               u1RetVal = CFA_FALSE;
    UINT1               u1IfType = CFA_NONE;

    CfaGetIfType (u4IfIndex, &u1IfType);

    if (u1IfType == CFA_TUNNEL)
    {
        u1RetVal = CFA_TRUE;
    }
    return u1RetVal;
}

/*****************************************************************************
 *
 *    Function Name       :CfaIsLaggInterface 
 *
 *    Description         : This function checks whether  the interface 
 *                          is PortChannel interface or not.
 *
 *    Input(s)            : u4Index - interface index
 *
 *    Output(s)           : NONE
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_TRUE-If interface is a PortChannel interface
 *****************************************************************************/

UINT1
CfaIsLaggInterface (UINT4 u4IfIndex)
{
    UINT1               u1RetVal = CFA_FALSE;
    UINT1               u1IfType = CFA_NONE;

    CfaGetIfType (u4IfIndex, &u1IfType);

    if (u1IfType == CFA_LAGG)
    {
        u1RetVal = CFA_TRUE;
    }
    return u1RetVal;
}

/*****************************************************************************
 *
 *    Function Name       :CfaIsVirtualInterface 
 *
 *    Description         : This function checks whether  the interface 
 *                          is virtual/internal interface or not.
 *
 *    Input(s)            : u4Index - interface index
 *
 *    Output(s)           : NONE
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_TRUE-If interface is a virtual interface
 *****************************************************************************/

UINT1
CfaIsVirtualInterface (UINT4 u4IfIndex)
{
    UINT1               u1RetVal = CFA_FALSE;

    if ((u4IfIndex >= CFA_MIN_INTERNAL_IF_INDEX) &&
        (u4IfIndex <= CFA_MAX_INTERNAL_IF_INDEX))
    {
        u1RetVal = CFA_TRUE;
    }
    return u1RetVal;
}

/*****************************************************************************
 *
 *    Function Name       :CfaIsILanInterface 
 *
 *    Description         : This function checks whether  the interface 
 *                          is ILAN interface or not.
 *
 *    Input(s)            : u4Index - interface index
 *
 *    Output(s)           : NONE
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_TRUE-If interface is a ILAN interface
 *****************************************************************************/

UINT1
CfaIsILanInterface (UINT4 u4IfIndex)
{
    UINT1               u1RetVal = CFA_FALSE;

    if ((u4IfIndex >= CFA_MIN_ILAN_IF_INDEX) &&
        (u4IfIndex <= CFA_MAX_ILAN_IF_INDEX))
    {
        u1RetVal = CFA_TRUE;
    }
    return u1RetVal;
}

/*****************************************************************************
 *
 *    Function Name       :CfaIsVipInterface 
 *
 *    Description         : This function checks whether the interface 
 *                          is VIP interface or not.
 *
 *    Input(s)            : u4Index - interface index
 *
 *    Output(s)           : NONE
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_TRUE-If interface is a VIP interface
 *****************************************************************************/

UINT1
CfaIsVipInterface (UINT4 u4IfIndex)
{
    UINT1               u1RetVal = CFA_FALSE;

    if ((u4IfIndex >= CFA_MIN_VIP_IF_INDEX)
        && (u4IfIndex <= CFA_MAX_VIP_IF_INDEX))
    {
        u1RetVal = CFA_TRUE;
    }
    return u1RetVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name       : CfaIsSispInterface                              */
/*                                                                          */
/*    Description         : This function checks whether the interface      */
/*                          is SISP interface or not.                       */
/*                                                                          */
/*    Input(s)            : u4Index - interface index                       */
/*                                                                          */
/*    Output(s)           : NONE                                            */
/*                                                                          */
/*    Global Variables Referred : None.                                     */
/*                                                                          */
/*    Global Variables Modified : None.                                     */
/*                                                                          */
/*    Exceptions or Operating                                               */
/*    System Error Handling    : None.                                      */
/*                                                                          */
/*    Use of Recursion        : None.                                       */
/*                                                                          */
/*    Returns            : CFA_TRUE-If interface is a SISP interface        */
/****************************************************************************/
UINT1
CfaIsSispInterface (UINT4 u4IfIndex)
{
    UINT1               u1RetVal = CFA_FALSE;

    if ((u4IfIndex >= CFA_MIN_SISP_IF_INDEX)
        && (u4IfIndex <= CFA_MAX_SISP_IF_INDEX))
    {
        u1RetVal = CFA_TRUE;
    }

    return u1RetVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name       : CfaIsSbpInterface                               */
/*                                                                          */
/*    Description         : This function checks whether the interface      */
/*                          is SBP  interface or not.                       */
/*                                                                          */
/*    Input(s)            : u4Index - interface index                       */
/*                                                                          */
/*    Output(s)           : NONE                                            */
/*                                                                          */
/*    Global Variables Referred : None.                                     */
/*                                                                          */
/*    Global Variables Modified : None.                                     */
/*                                                                          */
/*    Exceptions or Operating                                               */
/*    System Error Handling    : None.                                      */
/*                                                                          */
/*    Use of Recursion        : None.                                       */
/*                                                                          */
/*    Returns            : CFA_TRUE-If interface is a SBP interface        */
/****************************************************************************/

UINT1
CfaIsSbpInterface (UINT4 u4IfIndex)
{
    UINT1               u1RetVal = CFA_FALSE;

    if ((u4IfIndex >= CFA_MIN_EVB_SBP_INDEX)
        && (u4IfIndex <= CFA_MAX_EVB_SBP_INDEX))
    {
        u1RetVal = CFA_TRUE;
    }

    return u1RetVal;
}

#ifdef CLI_WANTED
/*****************************************************************************
 *
 *    Function Name       :CfaGetIfPrompt
 *
 *    Description         : This function validates the given pi1ModeName  
 *                          and returns the prompt in pi1DispStr if valid.
 *    Input(s)            : pi1ModeName is NULL to display the mode tree with
 *                          mode name and prompt string.
 *
 *    Output(s)           : pi1DispStr:Pointer to a string. 
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : TRUE if given pi1ModeName is valid.
 *                         FALSE if the given pi1ModeName is not valid.
 *****************************************************************************/
INT1
CfaGetIfPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index;
    tCfaIfaceInfo       sIfaceInfo;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "eth" as the prompt 
     * for the mode ETH */
    if (pi1ModeName == NULL)
    {
        STRNCPY (pi1DispStr, "eth", STRLEN ("eth"));
        pi1DispStr[STRLEN ("eth")] = '\0';
        return TRUE;
    }

    /* Get the interface type */
    if (CfaGetIfaceInfo ((UINT1 *) pi1ModeName, &sIfaceInfo) == FALSE)
    {
        return FALSE;
    }

    /* if the type is other than ETH, return FALSE */
    if ((sIfaceInfo.u4IfType != CFA_ENET) &&
        (sIfaceInfo.u4IfType != CFA_TUNNEL))
        return FALSE;

    if (CfaGetInterfaceIndexFromName ((UINT1 *) pi1ModeName, &u4Index)
        == OSIX_FAILURE)
    {
        return FALSE;
    }

    STRNCPY (pi1DispStr, pi1ModeName, STRLEN (pi1ModeName));
    pi1DispStr[STRLEN (pi1ModeName)] = '\0';
    return TRUE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetLinuxIvrPrompt
 *
 *    Description         : This function validates the given pi1ModeName  
 *                          and returns the prompt in pi1DispStr if valid.
 *    Input(s)            : pi1ModeName is NULL to display the mode tree with
 *                          mode name and prompt string.
 *
 *    Output(s)           : pi1DispStr:Pointer to a string. 
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : TRUE if given pi1ModeName is valid.
 *                         FALSE if the given pi1ModeName is not valid.
 *****************************************************************************/
/* This function is used for IVR mode */
INT1
CfaGetLinuxIvrPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index;
    UINT1               u1IfType;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    if (pi1ModeName == NULL)
    {
        STRNCPY (pi1DispStr, "linuxvlan", STRLEN ("linuxvlan"));
        pi1DispStr[STRLEN ("linuxvlan")] = '\0';
        return TRUE;
    }

    if (CfaGetInterfaceIndexFromName ((UINT1 *) pi1ModeName, &u4Index)
        == OSIX_FAILURE)
    {
        return FALSE;
    }
    if (CfaGetIfType (u4Index, &u1IfType) == CFA_FAILURE)
    {
        return FALSE;
    }

    /* if the type is other than L3IPVLAN, return FALSE */
    if (u1IfType != CFA_L3IPVLAN)
        return FALSE;

    if (((UINT4) CLI_SET_VLANID ((INT4) u4Index)) != u4Index)
        return FALSE;

    STRNCPY (pi1DispStr, "(config-if)#", STRLEN ("(config-if)#"));
    pi1DispStr[STRLEN ("(config-if)#")] = '\0';
    return TRUE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetIvrPrompt
 *
 *    Description         : This function validates the given pi1ModeName  
 *                          and returns the prompt in pi1DispStr if valid.
 *    Input(s)            : pi1ModeName is NULL to display the mode tree with
 *                          mode name and prompt string.
 *
 *    Output(s)           : pi1DispStr:Pointer to a string. 
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : TRUE if given pi1ModeName is valid.
 *                         FALSE if the given pi1ModeName is not valid.
 *****************************************************************************/
/* This function is used for IVR mode */
INT1
CfaGetIvrPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index;
    UINT4               u4L2CxtId = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1L2CxtName[VCM_ALIAS_MAX_LEN];
    UINT1               u1Var1 = 0;
    UINT1               u1Var2 = 0;
    tCfaIfaceInfo       sIfaceInfo;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1L2CxtName, 0, VCM_ALIAS_MAX_LEN);
    if (!pi1DispStr)
    {
        return FALSE;
    }

    if (pi1ModeName == NULL)
    {
        STRNCPY (pi1DispStr, "vlan", STRLEN ("vlan"));
        pi1DispStr[STRLEN ("vlan")] = '\0';
        return TRUE;
    }
    /* Get the interface type */
    if (CfaGetIfaceInfo ((UINT1 *) pi1ModeName, &sIfaceInfo) == FALSE)
    {
        return FALSE;
    }

    /* if the type is other than L3IPVLAN, return FALSE */
    if (sIfaceInfo.u4IfType != CFA_L3IPVLAN)
        return FALSE;

    while (pi1ModeName[u1Var1] != ' ')
    {
        if (pi1ModeName[u1Var1] == '\0')
        {
            return FALSE;
        }
        au1IfName[u1Var1] = (UINT1) pi1ModeName[u1Var1];
        u1Var1++;
    }

    au1IfName[u1Var1] = '\0';
    u1Var1++;

    while (pi1ModeName[u1Var1] != '\0')
    {
        au1L2CxtName[u1Var2] = (UINT1) pi1ModeName[u1Var1];
        u1Var2++;
        u1Var1++;
    }

    au1L2CxtName[u1Var2] = (UINT1) pi1ModeName[u1Var1];
    /* Get the interface type */

    if (VcmIsSwitchExist (au1L2CxtName, &u4L2CxtId) == VCM_FALSE)
    {
        return FALSE;
    }

    if (CfaGetInterfaceIndexFromNameInCxt (u4L2CxtId, (UINT1 *) au1IfName,
                                           &u4Index) == OSIX_FAILURE)
    {
        return FALSE;
    }

    if (((UINT4) CLI_SET_VLANID ((INT4) u4Index)) != u4Index)
        return FALSE;

    STRNCPY (pi1DispStr, "(config-if)#", STRLEN ("(config-if)#"));
    pi1DispStr[STRLEN ("(config-if)#")] = '\0';
    return TRUE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetLoopbackPrompt
 *
 *    Description         : This function validates the given pi1ModeName  
 *                          and returns the prompt in pi1DispStr if valid.
 *    Input(s)            : pi1ModeName is NULL to display the mode tree with
 *                          mode name and prompt string.
 *
 *    Output(s)           : pi1DispStr:Pointer to a string. 
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : TRUE if given pi1ModeName is valid.
 *                         FALSE if the given pi1ModeName is not valid.
 *****************************************************************************/
/* This function is used for IVR mode */
INT1
CfaGetLoopbackPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index;
    tCfaIfaceInfo       sIfaceInfo;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    if (pi1ModeName == NULL)
    {
        STRNCPY (pi1DispStr, "loopback", STRLEN ("loopback"));
        pi1DispStr[STRLEN ("loopback")] = '\0';
        return TRUE;
    }
    /* Get the interface type */
    if (CfaGetIfaceInfo ((UINT1 *) pi1ModeName, &sIfaceInfo) == FALSE)
    {
        return FALSE;
    }

    /* if the type is other than LOOPBACK, return FALSE */
    if (sIfaceInfo.u4IfType != CFA_LOOPBACK)
        return FALSE;

    if (CfaGetInterfaceIndexFromName ((UINT1 *) pi1ModeName, &u4Index)
        == OSIX_FAILURE)
    {
        return FALSE;
    }

    if (((UINT4) CLI_SET_LOOPBACKID ((INT4) u4Index)) != u4Index)
        return FALSE;

    STRNCPY (pi1DispStr, "(config-if)#", STRLEN ("(config-if)#"));
    pi1DispStr[STRLEN ("(config-if)#")] = '\0';
    return TRUE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaConfigOOBInterface
 *
 *    Description         : This function validates the given pi1ModeName  
 *                          and returns the prompt in pi1DispStr if valid.
 *    Input(s)            : pi1ModeName is NULL to display the mode tree with
 *                          mode name and prompt string.
 *
 *    Output(s)           : pi1DispStr:Pointer to a string. 
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : TRUE if given pi1ModeName is valid.
 *                         FALSE if the given pi1ModeName is not valid.
 *****************************************************************************/
INT1
CfaConfigOOBInterface (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;

    if (!(pi1ModeName) || !(pi1DispStr))
    {
        return FALSE;
    }
    u4Len = STRLEN (CLI_OOB_CONFIG);
    if (STRNCMP (pi1ModeName, CLI_OOB_CONFIG, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;
    STRNCPY (pi1DispStr, CLI_OOB_CONFIG, STRLEN (CLI_OOB_CONFIG));
    pi1DispStr[STRLEN (CLI_OOB_CONFIG)] = '\0';
    return TRUE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetPortChannelPrompt 
 *
 *    Description         : This function validates the given pi1ModeName  
 *                          and returns the prompt in pi1DispStr if valid.
 *    Input(s)            : pi1ModeName is NULL to display the mode tree with
 *                          mode name and prompt string.
 *
 *    Output(s)           : pi1DispStr:Pointer to a string. 
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : TRUE if given pi1ModeName is valid.
 *                         FALSE if the given pi1ModeName is not valid.
 *****************************************************************************/
INT1
CfaGetPortChannelPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index;
    tCfaIfaceInfo       sIfaceInfo;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    if (pi1ModeName == NULL)
    {
        STRNCPY (pi1DispStr, "po", STRLEN ("po"));
        pi1DispStr[STRLEN ("po")] = '\0';
        return TRUE;
    }

    if (CfaGetIfaceInfo ((UINT1 *) pi1ModeName, &sIfaceInfo) == FALSE)
    {
        return FALSE /* CLI_ERROR */ ;
    }

    if (sIfaceInfo.u4IfType != CFA_LAGG)
        return FALSE /* CLI_ERROR */ ;

    if (CfaGetInterfaceIndexFromName ((UINT1 *) pi1ModeName, &u4Index)
        == OSIX_FAILURE)
    {
        return FALSE;
    }
    if (((UINT4) CLI_SET_PO_INDEX ((INT4) u4Index)) != u4Index)
        return FALSE;

    STRNCPY (pi1DispStr, "(config-if)#", STRLEN ("(config-if)#"));
    pi1DispStr[STRLEN ("(config-if)#")] = '\0';
    return TRUE;
}

/*****************************************************************************
 *  *
 * Function Name       : CfaGetEvbSbpPrompt
 *
 * Description         : This function validates the given pi1ModeName
 *                       and returns the prompt in pi1DispStr if valid.
 * Input(s)            : pi1ModeName is NULL to display the mode tree with
 *                       mode name and prompt string.
 *
 * Output(s)           : pi1DispStr:Pointer to a string.
 *
 * Global Variables Referred : None.
 *
 * Global Variables Modified : None.
 *
 * Exceptions or Operating
 * System Error Handling    : None.
 *
 * Use of Recursion        : None.
 *
 * Returns                 : TRUE if given pi1ModeName is valid.
 *                         FALSE if the given pi1ModeName is not valid.
 ****************************************************************************/
INT1
CfaGetEvbSbpPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index = 0;
    tCfaIfaceInfo       sIfaceInfo;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    MEMSET (&sIfaceInfo, 0, sizeof (tCfaIfaceInfo));

    if (pi1ModeName == NULL)
    {
        STRNCPY (pi1DispStr, "s-channel", STRLEN ("s-channel"));
        pi1DispStr[STRLEN ("s-channel")] = '\0';
        return TRUE;
    }

    if (CfaGetIfaceInfo ((UINT1 *) pi1ModeName, &sIfaceInfo) == FALSE)
    {
        return FALSE /* CLI_ERROR */ ;
    }

    if (sIfaceInfo.u4IfType != CFA_STATION_FACING_BRIDGE_PORT)
    {
        return FALSE /* CLI_ERROR */ ;
    }

    if (CfaGetInterfaceIndexFromName ((UINT1 *) pi1ModeName, &u4Index)
        == OSIX_FAILURE)
    {
        return FALSE;
    }
    if (((UINT4) CLI_SET_IFINDEX ((INT4) u4Index)) != u4Index)
    {
        return FALSE;
    }

    STRNCPY (pi1DispStr, "(config-if)#", STRLEN ("(config-if)#"));
    pi1DispStr[STRLEN ("(config-if)#")] = '\0';
    return TRUE;
}

/*****************************************************************************
 *  *
 * Function Name       : CfaGetEvbUapPrompt
 *
 * Description         : This function validates the given pi1ModeName
 *                       and returns the prompt in pi1DispStr if valid.
 * Input(s)            : pi1ModeName is NULL to display the mode tree with
 *                       mode name and prompt string.
 *
 * Output(s)           : pi1DispStr:Pointer to a string.
 *
 * Global Variables Referred : None.
 *
 * Global Variables Modified : None.
 *
 * Exceptions or Operating
 * System Error Handling    : None.
 *
 * Use of Recursion        : None.
 *
 * Returns                 : TRUE if given pi1ModeName is valid.
 *                         FALSE if the given pi1ModeName is not valid.
 ****************************************************************************/
INT1
CfaGetEvbUapPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index = 0;
    UINT4               u4Len = 0;
    tCfaIfaceInfo       sIfaceInfo;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    MEMSET (&sIfaceInfo, 0, sizeof (tCfaIfaceInfo));

    if (pi1ModeName == NULL)
    {
        STRNCPY (pi1DispStr, "uap", STRLEN ("uap"));
        pi1DispStr[STRLEN ("uap")] = '\0';
        return TRUE;
    }

    if (CfaGetIfaceInfo ((UINT1 *) pi1ModeName, &sIfaceInfo) == FALSE)
    {
        return FALSE /* CLI_ERROR */ ;
    }

    if (sIfaceInfo.u4IfType != CFA_UPLINK_ACCESS_PORT)
    {
        return FALSE /* CLI_ERROR */ ;
    }

    u4Len = STRLEN (CLI_UAP_MODE);

    if (STRNCMP (pi1ModeName, CLI_UAP_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    u4Index = (UINT4) CLI_ATOI (pi1ModeName + u4Len);

    if (CfaValidateCfaIfIndex ((UINT2) u4Index) == CFA_FAILURE)
    {
        return FALSE;
    }

    if (((UINT4) CLI_SET_IFINDEX ((INT4) u4Index)) != u4Index)
    {
        return FALSE;
    }

    STRNCPY (pi1DispStr, "(config-if)#", STRLEN ("(config-if)#"));
    pi1DispStr[STRLEN ("(config-if)#")] = '\0';
    return TRUE;
}

/*****************************************************************************
 *  *
 * Function Name       : CfaGetPswPrompt
 *    
 * Description         : This function validates the given pi1ModeName
 *                       and returns the prompt in pi1DispStr if valid.
 * Input(s)            : pi1ModeName is NULL to display the mode tree with
 *                       mode name and prompt string.
 *         
 * Output(s)           : pi1DispStr:Pointer to a string.
 *           
 * Global Variables Referred : None.
 *             
 * Global Variables Modified : None.
 *               
 * Exceptions or Operating
 * System Error Handling    : None.
 *                  
 * Use of Recursion        : None.
 *                    
 * Returns                 : TRUE if given pi1ModeName is valid.
 *                         FALSE if the given pi1ModeName is not valid.
 ** *****************************************************************************/

INT1
CfaGetPswPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index;
    tCfaIfaceInfo       sIfaceInfo;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    if (pi1ModeName == NULL)
    {
        STRNCPY (pi1DispStr, "pw", STRLEN ("pw"));
        pi1DispStr[STRLEN ("pw")] = '\0';
        return TRUE;
    }

    if (CfaGetIfaceInfo ((UINT1 *) pi1ModeName, &sIfaceInfo) == FALSE)
    {
        return FALSE /* CLI_ERROR */ ;
    }

    if (sIfaceInfo.u4IfType != CFA_PSEUDO_WIRE)
        return FALSE /* CLI_ERROR */ ;

    if (CfaGetInterfaceIndexFromName ((UINT1 *) pi1ModeName, &u4Index)
        == OSIX_FAILURE)
    {
        return FALSE;
    }
    if (((UINT4) CLI_SET_IFINDEX ((INT4) u4Index)) != u4Index)
        return FALSE;

    CLI_SET_CXT_ID (0xFFFFFFFF);
    STRNCPY (pi1DispStr, "(config-if)#", STRLEN ("(config-if)#"));
    pi1DispStr[STRLEN ("(config-if)#")] = '\0';
    return TRUE;
}

/*****************************************************************************
 *  *
 * Function Name       : CfaGetACPrompt
 *    
 * Description         : This function validates the given pi1ModeName
 *                       and returns the prompt in pi1DispStr if valid.
 * Input(s)            : pi1ModeName is NULL to display the mode tree with
 *                       mode name and prompt string.
 *         
 * Output(s)           : pi1DispStr:Pointer to a string.
 *           
 * Global Variables Referred : None.
 *             
 * Global Variables Modified : None.
 *               
 * Exceptions or Operating
 * System Error Handling    : None.
 *                  
 * Use of Recursion        : None.
 *                    
 * Returns                 : TRUE if given pi1ModeName is valid.
 *                         FALSE if the given pi1ModeName is not valid.
 ** *****************************************************************************/

INT1
CfaGetACPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    tCfaIfaceInfo       sIfaceInfo;
    INT4                i4SubType = -1;
    UINT4               u4Index = 0;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    if (pi1ModeName == NULL)
    {
        STRNCPY (pi1DispStr, "ac", STRLEN ("ac"));
        pi1DispStr[STRLEN ("ac")] = '\0';
        return TRUE;
    }

    MEMSET (&sIfaceInfo, 0, sizeof (tCfaIfaceInfo));

    if (CfaGetIfaceInfo ((UINT1 *) pi1ModeName, &sIfaceInfo) == FALSE)
    {
        return FALSE /* CLI_ERROR */ ;
    }

    if (sIfaceInfo.u4IfType != CFA_PROP_VIRTUAL_INTERFACE)
    {
        return FALSE /* CLI_ERROR */ ;
    }

    if (CfaUtilGetSubTypeFromNameAndType ((UINT1 *) pi1ModeName,
                                          CFA_PROP_VIRTUAL_INTERFACE,
                                          &i4SubType) == CFA_FAILURE)
    {
        return FALSE;
    }

    if (i4SubType != CFA_SUBTYPE_AC_INTERFACE)
    {
        return FALSE;
    }

    if (CfaGetInterfaceIndexFromName ((UINT1 *) pi1ModeName, &u4Index)
        == OSIX_FAILURE)
    {
        return FALSE;
    }

    if (((UINT4) CLI_SET_IFINDEX ((INT4) u4Index)) != u4Index)
    {
        return FALSE;
    }
    /* CLI_SET_CXT_ID (0xFFFFFFFF); */
    STRNCPY (pi1DispStr, "(config-if)#", STRLEN ("(config-if)#"));
    pi1DispStr[STRLEN ("(config-if)#")] = '\0';
    return TRUE;
}

#ifdef VXLAN_WANTED
/*****************************************************************************
 *  *
 * Function Name       : CfaGetNvePrompt
 *
 * Description         : This function validates the given pi1ModeName
 *                       and returns the prompt in pi1DispStr if valid.
 * Input(s)            : pi1ModeName is NULL to display the mode tree with
 *                       mode name and prompt string.
 *
 * Output(s)           : pi1DispStr:Pointer to a string.
 *
 * Global Variables Referred : None.
 *
 * Global Variables Modified : None.
 *
 * Exceptions or Operating
 * System Error Handling    : None.
 *
 * Use of Recursion        : None.
 *
 * Returns                 : TRUE if given pi1ModeName is valid.
 *                         FALSE if the given pi1ModeName is not valid.
 ** *****************************************************************************/

INT1
CfaGetNvePrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index = 0;
    tCfaIfaceInfo       sIfaceInfo;

    MEMSET (&sIfaceInfo, 0, sizeof (tCfaIfaceInfo));
    if (!pi1DispStr)
    {
        return FALSE;
    }

    if (pi1ModeName == NULL)
    {
        STRNCPY (pi1DispStr, "nve", STRLEN ("nve"));
        pi1DispStr[STRLEN ("nve")] = '\0';
        return TRUE;
    }

    if (CfaGetIfaceInfo ((UINT1 *) pi1ModeName, &sIfaceInfo) == FALSE)
    {
        return FALSE /* CLI_ERROR */ ;
    }

    if (sIfaceInfo.u4IfType != CFA_VXLAN_NVE)
        return FALSE /* CLI_ERROR */ ;

    if (CfaGetInterfaceIndexFromName ((UINT1 *) pi1ModeName, &u4Index)
        == OSIX_FAILURE)
    {
        return FALSE;
    }
    if (((UINT4) CLI_SET_IFINDEX ((INT4) u4Index)) != u4Index)
        return FALSE;

    CLI_SET_CXT_ID (0xFFFFFFFF);
    STRNCPY (pi1DispStr, "(config-if)#", STRLEN ("(config-if)#"));
    pi1DispStr[STRLEN ("(config-if)#")] = '\0';
    return TRUE;
}
#endif
#ifdef VCPEMGR_WANTED
/*****************************************************************************
 *  *
 * Function Name       : CfaGetTapPrompt
 *
 * Description         : This function validates the given pi1ModeName
 *                       and returns the prompt in pi1DispStr if valid.
 * Input(s)            : pi1ModeName is NULL to display the mode tree with
 *                       mode name and prompt string.
 *
 * Output(s)           : pi1DispStr:Pointer to a string.
 *
 * Global Variables Referred : None.
 *
 * Global Variables Modified : None.
 *
 * Exceptions or Operating
 * System Error Handling    : None.
 *
 * Use of Recursion        : None.
 *
 * Returns                 : TRUE if given pi1ModeName is valid.
 *                         FALSE if the given pi1ModeName is not valid.
 ** *****************************************************************************/

INT1
CfaGetTapPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index = 0;
    tCfaIfaceInfo       sIfaceInfo;

    MEMSET (&sIfaceInfo, 0, sizeof (tCfaIfaceInfo));
    if (!pi1DispStr)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaGetTapPrompt - " "pi1DispStr - FAILURE.\n");
        return FALSE;
    }

    if (pi1ModeName == NULL)
    {
        STRNCPY (pi1DispStr, "tap", STRLEN ("tap"));
        pi1DispStr[STRLEN ("tap")] = '\0';
        return TRUE;
    }

    if (CfaGetIfaceInfo ((UINT1 *) pi1ModeName, &sIfaceInfo) == FALSE)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaGetTapPrompt - " "CfaGetIfaceInfo - FAILURE.\n");
        return FALSE /* CLI_ERROR */ ;
    }

    if (sIfaceInfo.u4IfType != CFA_TAP)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaGetTapPrompt - IF not TAP.\n");
        return FALSE /* CLI_ERROR */ ;
    }

    if (CfaGetInterfaceIndexFromName ((UINT1 *) pi1ModeName, &u4Index)
        == OSIX_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaGetTapPrompt - "
                 "CfaGetInterfaceIndexFromName - FAILURE.\n");
        return FALSE;
    }
    if (((UINT4) CLI_SET_IFINDEX ((INT4) u4Index)) != u4Index)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaGetTapPrompt - " "CLI_SET_IFINDEX - FAILURE.\n");
        return FALSE;
    }
    CLI_SET_CXT_ID (0xFFFFFFFF);
    STRNCPY (pi1DispStr, "(config-if)#", STRLEN ("(config-if)#"));
    pi1DispStr[STRLEN ("(config-if)#")] = '\0';
    return TRUE;
}
#endif
/*****************************************************************************
 *
 *    Function Name       : CfaGetVirtualPrompt 
 *
 *    Description         : This function validates the given pi1ModeName  
 *                          and returns the prompt in pi1DispStr if valid.
 *    Input(s)            : pi1ModeName is NULL to display the mode tree with
 *                          mode name and prompt string.
 *
 *    Output(s)           : pi1DispStr:Pointer to a string. 
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : TRUE if given pi1ModeName is valid.
 *                         FALSE if the given pi1ModeName is not valid.
 *****************************************************************************/
INT1
CfaGetVirtualPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index;
    tCfaIfaceInfo       sIfaceInfo;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    if (pi1ModeName == NULL)
    {
        STRNCPY (pi1DispStr, "vr", STRLEN ("vr"));
        pi1DispStr[STRLEN ("vr")] = '\0';
        return TRUE;
    }

    if (CfaGetIfaceInfo ((UINT1 *) pi1ModeName, &sIfaceInfo) == FALSE)
    {
        return FALSE /* CLI_ERROR */ ;
    }

    if (sIfaceInfo.u4IfType != CFA_BRIDGED_INTERFACE)
        return FALSE /* CLI_ERROR */ ;

    if (CfaGetInterfaceIndexFromName ((UINT1 *) pi1ModeName, &u4Index)
        == OSIX_FAILURE)
    {
        return FALSE;
    }
    if (((UINT4) CLI_SET_VR_INDEX ((INT4) u4Index)) != u4Index)
        return FALSE;

    STRNCPY (pi1DispStr, "(config-if)#", STRLEN ("(config-if)#"));
    pi1DispStr[STRLEN ("(config-if)#")] = '\0';

    return TRUE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetSispPrompt 
 *
 *    Description         : This function validates the given pi1ModeName  
 *                          and returns the prompt in pi1DispStr if valid.
 *
 *    Input(s)            : pi1ModeName is NULL to display the mode tree with
 *                          mode name and prompt string.
 *
 *    Output(s)           : pi1DispStr:Pointer to a string. 
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : TRUE if given pi1ModeName is valid.
 *                         FALSE if the given pi1ModeName is not valid.
 *****************************************************************************/
INT1
CfaGetSispPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index;
    INT4                i4SubType = -1;
    tCfaIfaceInfo       sIfaceInfo;

    if (pi1DispStr == NULL)
    {
        return FALSE;
    }

    if (pi1ModeName == NULL)
    {
        STRNCPY (pi1DispStr, "sisp", STRLEN ("sisp"));
        pi1DispStr[STRLEN ("sisp")] = '\0';
        return TRUE;
    }

    if (CfaGetIfaceInfo ((UINT1 *) pi1ModeName, &sIfaceInfo) == FALSE)
    {
        return FALSE;            /* CLI_ERROR */
    }

    if (sIfaceInfo.u4IfType != CFA_PROP_VIRTUAL_INTERFACE)
    {
        return FALSE;            /* CLI_ERROR */
    }

    if (CfaUtilGetSubTypeFromNameAndType ((UINT1 *) pi1ModeName,
                                          CFA_PROP_VIRTUAL_INTERFACE,
                                          &i4SubType) == CFA_FAILURE)
    {
        return FALSE;
    }

    if (i4SubType != CFA_SUBTYPE_SISP_INTERFACE)
    {
        return FALSE;            /* CLI_ERROR */
    }

    if (CfaGetInterfaceIndexFromName ((UINT1 *) pi1ModeName, &u4Index)
        == OSIX_FAILURE)
    {
        return FALSE;
    }
    if (((UINT4) CLI_SET_SISP_INDEX ((INT4) u4Index)) != u4Index)
    {
        return FALSE;
    }

    STRNCPY (pi1DispStr, "(config-if)#", STRLEN ("(config-if)#"));
    pi1DispStr[STRLEN ("(config-if)#")] = '\0';

    return TRUE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetTnlPrompt 
 *
 *    Description         : This function validates the given pi1ModeName  
 *                          and returns the prompt in pi1DispStr if valid.
 *    Input(s)            : pi1ModeName is NULL to display the mode tree with
 *                          mode name and prompt string.
 *
 *    Output(s)           : pi1DispStr:Pointer to a string. 
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : TRUE if given pi1ModeName is valid.
 *                         FALSE if the given pi1ModeName is not valid.
 *****************************************************************************/

INT1
CfaGetTnlPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index;
    tCfaIfaceInfo       sIfaceInfo;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    if (pi1ModeName == NULL)
    {
        STRNCPY (pi1DispStr, "Tnl", STRLEN ("Tnl"));
        pi1DispStr[STRLEN ("Tnl")] = '\0';
        return TRUE;
    }

    if (CfaGetIfaceInfo ((UINT1 *) pi1ModeName, &sIfaceInfo) == FALSE)
    {
        return FALSE /* CLI_ERROR */ ;
    }

    if (sIfaceInfo.u4IfType != CFA_TUNNEL)
        return FALSE /* CLI_ERROR */ ;

    if (CfaGetInterfaceIndexFromName ((UINT1 *) pi1ModeName, &u4Index)
        == OSIX_FAILURE)
    {
        return FALSE;
    }
    if (((UINT4) CLI_SET_IFINDEX ((INT4) u4Index)) != u4Index)
        return FALSE;

    STRNCPY (pi1DispStr, "(config-if)#", STRLEN ("(config-if)#"));
    pi1DispStr[STRLEN ("(config-if)#")] = '\0';
    return TRUE;
}

#ifdef PPP_WANTED
/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : CfaGetPppPrompt                                         */
/*                                                                            */
/* DESCRIPTION      : This function validates the given pi1ModeName           */
/*                    and returns the prompt in pi1DispStr if valid.          */
/*                    Returns TRUE if given pi1ModeName is valid.             */
/*                    Returns FALSE if the given pi1ModeName is not valid     */
/*                    pi1ModeName is NULL to display the mode tree with       */
/*                    mode name and prompt string.                            */
/*                                                                            */
/* INPUT            : pi1ModeName- Mode Name                                  */
/*                                                                            */
/* OUTPUT           : pi1DispStr- DIsplay string                              */
/*                                                                            */
/* RETURNS          : TRUE/FALSE                                              */
/*                                                                            */
/******************************************************************************/

INT1
CfaGetPppPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    UINT4               u4Index;

    if ((pi1DispStr == NULL) || (pi1ModeName == NULL))
    {
        return FALSE;
    }

    u4Len = STRLEN (CFA_PPP_MODE);

    if (STRNCMP (pi1ModeName, CFA_PPP_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    if (CfaGetInterfaceIndexFromName ((UINT1 *) pi1ModeName,
                                      &u4Index) == OSIX_FAILURE)
    {
        return FALSE;
    }

    if (((UINT4) CLI_SET_IFINDEX ((INT4) u4Index)) != (UINT4) u4Index)
    {
        return FALSE;
    }

    CLI_SET_CXT_ID (0xFFFFFFFF);
    STRNCPY (pi1DispStr, "(config-ppp)#", STRLEN ("(config-ppp)#"));
    pi1DispStr[STRLEN ("(config-ppp)#")] = '\0';
    return TRUE;
}

#endif /* PPP_WANTED */
#ifdef HDLC_WANTED
/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : CfaGetHdlcPrompt                                        */
/*                                                                            */
/* DESCRIPTION      : This function validates the given pi1ModeName           */
/*                    and returns the prompt in pi1DispStr if valid.          */
/*                    Returns TRUE if given pi1ModeName is valid.             */
/*                    Returns FALSE if the given pi1ModeName is not valid     */
/*                    pi1ModeName is NULL to display the mode tree with       */
/*                    mode name and prompt string.                            */
/*                                                                            */
/* INPUT            : pi1ModeName- Mode Name                                  */
/*                                                                            */
/* OUTPUT           : pi1DispStr- DIsplay string                              */
/*                                                                            */
/* RETURNS          : TRUE/FALSE                                              */
/*                                                                            */
/******************************************************************************/

INT1
CfaGetHdlcPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = 0;
    UINT4               u4Index = 0;

    if ((pi1DispStr == NULL) || (pi1ModeName == NULL))
    {
        return FALSE;
    }

    u4Len = STRLEN (CFA_HDLC_MODE);

    if (STRNCMP (pi1ModeName, CFA_HDLC_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    u4Index = *(pi1ModeName + u4Len) - '0';

    if (((UINT4) CLI_SET_IFINDEX ((INT4) u4Index)) != (UINT4) u4Index)
    {
        return FALSE;
    }

    STRNCPY (pi1DispStr, "(config-controller)#",
             STRLEN ("(config-controller)#"));
    pi1DispStr[STRLEN ("(config-controller)#")] = '\0';
    return TRUE;
}
#endif /* HDLC_WANTED */

#endif /*CLI_WANTED */
/*****************************************************************************
 *
 *    Function Name       : CfaGetIfFlowControl
 *
 *    Description         : This function returns the interface FlowControl status.   
 *
 *    Input(s)            : u4IfIndex - interface index of the interface whose
 *                                      Flowcontrol status is needed.
 *
 *    Output(s)           : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : interface's FlowControl Status.  
 *                         
 *
 *****************************************************************************/
UINT4
CfaGetIfFlowControl (UINT4 u4IfIndex)
{
#ifdef NPAPI_WANTED
    UINT4               u4Val = ENET_FLOW_UNKNOWN;
#endif
    UINT1               u1IfType = CFA_NONE;

    CfaGetIfType (u4IfIndex, &u1IfType);

    switch (u1IfType)
    {
        case CFA_ENET:

#ifdef NPAPI_WANTED
            if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
            {
                if (CfaFsCfaHwGetIfFlowControl (u4IfIndex, &u4Val) !=
                    FNP_SUCCESS)
                {
                    return ENET_FLOW_UNKNOWN;
                }
            }
            /* u4Val can be ENET_FLOW_ON or ENET_FLOW_OFF */
            return u4Val;
#else
            return ENET_FLOW_UNKNOWN;

#endif /* NPAPI_WANTED */

        default:
            return ENET_FLOW_UNKNOWN;
    }
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetIfAutoNegSupport
 *
 *    Description         : This function updates the interface Auto-negotiation               
 *                          support
 *
 *    Input(s)            : u4IfMauIfIndex - interface index whose AutoNeg 
 *                                           support is needed.
 *                        : i4IfMauIndex - MAU index of the Interface.
 *
 *    Output(s)           : Auto-Neg Support is updated.
 *
 *    Global Variables Referred : None. 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE  
 *                         
 *
 *****************************************************************************/
INT4
CfaGetIfAutoNegSupport (UINT4 u4IfMauIfIndex, INT4 i4IfMauIndex,
                        INT4 *pi4RetSupport)
{
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif

    if ((u4IfMauIfIndex > CFA_MAX_INTERFACES ()) || (u4IfMauIfIndex == 0) ||
        (i4IfMauIndex > MAU_MAX_IF_COUNT))
    {
        return CFA_FAILURE;
    }

    *pi4RetSupport = ENET_AUTO_SUPPORT;

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfMauIfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u4IfMauIfIndex)) && (u1IfType == CFA_ENET))
    {
        if (CfaFsMauHwGetAutoNegSupported (u4IfMauIfIndex, i4IfMauIndex,
                                           pi4RetSupport) != FNP_SUCCESS)
        {
            return CFA_FAILURE;
        }
    }
#endif
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetIfAutoNegStatus
 *
 *    Description         : This function updates the interface 
 *                          Auto-negotiation status.   
 *
 *    Input(s)            : i4IfMauIfIndex - interface index whose
 *                                      AutoNeg status is needed.
 *                        : i4IfMauIndex - MAU index of the Interface.
 *
 *    Output(s)           : Auto-Neg Status is updated.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE  
 *                         
 *
 *****************************************************************************/
INT4
CfaGetIfAutoNegStatus (INT4 i4IfMauIfIndex, INT4 i4IfMauIndex, INT4 *pRetStatus)
{
    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif

    u4IfIndex = (UINT4) i4IfMauIfIndex;

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0) ||
        (i4IfMauIndex > MAU_MAX_IF_COUNT))
    {
        return CFA_FAILURE;
    }

    *pRetStatus = CFA_DISABLED;

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u4IfIndex)) && (u1IfType == CFA_ENET))
    {
        if (CfaFsMauHwGetAutoNegAdminStatus (i4IfMauIfIndex,
                                             i4IfMauIndex, pRetStatus)
            != FNP_SUCCESS)
        {
            return CFA_FAILURE;
        }
        else
        {
            /* Update the AdminStatus in the MAU table */
            CfaCdbSetIfMauAutoNegAdminStatus (u4IfIndex, *pRetStatus);
        }
    }
#endif
    return CFA_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name       : CfaGetIfAdvtCapability
 *
 *    Description         : This function updates the capabilities advertised
 *                          by the interface.              
 *
 *    Input(s)            : u4IfMauIfIndex - interface index whose
 *                                      AutoNeg status is needed.
 *                        : i4MauIndex - MAU index of the Interface.
 *
 *    Output(s)           : Advertised capability is updated.
 *
 *    Global Variables Referred : None. 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE  
 *                         
 *
 *****************************************************************************/
INT4
CfaGetIfAdvtCapability (UINT4 u4IfMauIfIndex, INT4 i4MauIndex,
                        UINT2 *pu2RetAdvtCapability)
{
#ifdef NPAPI_WANTED
    INT4                i4RetAdvtCapability = 0;
    UINT1               u1IfType;
#endif

    if ((u4IfMauIfIndex > CFA_MAX_INTERFACES ()) || (u4IfMauIfIndex == 0) ||
        (i4MauIndex > MAU_MAX_IF_COUNT))
    {
        return CFA_FAILURE;
    }

    *pu2RetAdvtCapability = MAU_AUTO_B_OTHER;

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfMauIfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u4IfMauIfIndex)) && (u1IfType == CFA_ENET))
    {
        if (CfaFsMauHwGetAutoNegCapAdvtBits (u4IfMauIfIndex, i4MauIndex,
                                             &i4RetAdvtCapability) !=
            FNP_SUCCESS)
        {
            return CFA_FAILURE;
        }
    }
    *pu2RetAdvtCapability = (UINT2) (i4RetAdvtCapability & 0x0000ffff);
#endif
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetIfOperMauType
 *
 *    Description         : This function updates the oper MAU type of the
 *                          interface.              
 *
 *    Input(s)            : u4IfIndex - interface index whose
 *                                      Oper Mau Type is needed.
 *                        : i4IfMauIndex - MAU index of the Interface.
 *
 *    Output(s)           : Oper MAU Type is updated.
 *
 *    Global Variables Referred : None. 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE  
 *                         
 *
 *****************************************************************************/
INT4
CfaGetIfOperMauType (UINT4 u4IfIndex, INT4 i4MauIndex, UINT4 *pu4RetOperMauType)
{
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0) ||
        (i4MauIndex > MAU_MAX_IF_COUNT))
    {
        return CFA_FAILURE;
    }

    *pu4RetOperMauType = 0;

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u4IfIndex)) && (u1IfType == CFA_ENET))
    {
        if (CfaFsMauHwGetMauType (u4IfIndex, i4MauIndex, pu4RetOperMauType)
            != FNP_SUCCESS)
        {
            return CFA_FAILURE;
        }
    }
#endif
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetMgmtVlanList
 *
 *    Description         : This function returns the VLAN List for the 
 *                          management interface.
 *
 *    Input(s)            : None. 
 *
 *    Output(s)           : VLAN List.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :  CFA_SUCCESS or CFA_FAILURE.
 *    
 *****************************************************************************/

INT1
CfaGetMgmtVlanList (tMgmtVlanList MgmtVlanList)
{
    if (CfaValidateCfaIfIndex (CFA_MGMT_IF_INDEX) == CFA_SUCCESS)
    {
        CFA_DS_LOCK ();
        MEMCPY (MgmtVlanList, CFA_MGMT_VLANLIST (), sizeof (tMgmtVlanList));
        CFA_DS_UNLOCK ();

        return CFA_SUCCESS;
    }
    return CFA_FAILURE;
}

#ifdef CLI_WANTED
/*****************************************************************************
 *
 *    Function Name       : CfaPrintMgmtVlanList 
 *
 *    Description         : This function prints the VLAN list associated with 
 *                          the L3 VLAN interface.
 *
 *    Input(s)            : None 
 *
 *    Output(s)           : pu1Temp - VLAN list associated with the VLAN
 *                                    interface. 
 *
 *    Global Variables Referred :None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :   None 
 *        
 *****************************************************************************/

INT4
CfaPrintMgmtVlanList (tCliHandle CliHandle)
{
    UINT1               u1VlanFlag = 0;
    UINT1               u1Count = 0;
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    UINT2               u2VlanId;
    UINT1              *pMgmtVlanList = NULL;
    INT4                i4Quit = CLI_SUCCESS;

    /* Allocating memory for Vlan List Size */
    pMgmtVlanList = UtilVlanAllocVlanListSize (sizeof (tMgmtVlanList));
    if (pMgmtVlanList == NULL)
    {
        return CFA_FAILURE;
    }

    MEMSET (pMgmtVlanList, 0, sizeof (tMgmtVlanList));

    if (CfaGetMgmtVlanList (pMgmtVlanList) == CFA_FAILURE)
    {
        /* Releasing memory for Vlan List Size */
        UtilVlanReleaseVlanListSize (pMgmtVlanList);
        return (CLI_FAILURE);
    }
    for (u2ByteInd = 0; u2ByteInd < CFA_MGMT_VLAN_LIST_SIZE; u2ByteInd++)
    {
        if (pMgmtVlanList[u2ByteInd] != 0)
        {
            u1VlanFlag = pMgmtVlanList[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < CFA_MGMT_VLANS_PER_BYTE) && (u1VlanFlag != 0));
                 u2BitIndex++)
            {
                if ((u1VlanFlag & CFA_MGMT_BIT8) != 0)
                {
                    u2VlanId =
                        (UINT2) ((u2ByteInd * CFA_MGMT_VLANS_PER_BYTE) +
                                 u2BitIndex + 1);
                    u1Count++;
                    if (!(u1Count % 15))
                    {
                        i4Quit = CliPrintf (CliHandle, "\r\n%d,", u2VlanId);
                    }
                    else
                    {
                        i4Quit = CliPrintf (CliHandle, "%d,", u2VlanId);
                    }
                }
                u1VlanFlag = (UINT1) (u1VlanFlag << 1);
            }
        }
        if (i4Quit == CLI_FAILURE)
        {
            break;
        }
    }
    UtilVlanReleaseVlanListSize (pMgmtVlanList);

    return (i4Quit);
}
#endif /*CLI_WANTED */
#ifdef WGS_WANTED
/*****************************************************************************
 *
 *    Function Name       : CfaSetVlanListForMgmtIface 
 *
 *    Description         : This function sets the VLAN list for the management 
 *                          L2 VLAN interface.
 *
 *    Input(s)            : None 
 *
 *    Output(s)           : None 
 *
 *    Global Variables Referred :None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :   None 
 *        
 *****************************************************************************/

VOID
CfaSetVlanListForMgmtIface ()
{
#ifdef KERNEL_WANTED
    tKernMgmtVlanParams KernMgmtVlanParams;
#endif

    CFA_DS_LOCK ();
    CFA_MGMT_SET_MEMBER_VLAN (CFA_MGMT_VLANLIST (), CFA_DEFAULT_VLAN_ID);
#ifdef KERNEL_WANTED
    MEMCPY (KernMgmtVlanParams.MgmtVlanList, CFA_MGMT_VLANLIST (),
            sizeof (tMgmtVlanList));
#endif
    CFA_DS_UNLOCK ();

#ifdef KERNEL_WANTED
    KernMgmtVlanParams.u4Action = KERN_SET_MGMT_VLAN_LIST;

    KAPIUpdateInfo (MGMT_VLAN_IOCTL, (tKernCmdParam *) & KernMgmtVlanParams);
#endif
}
#endif
/*****************************************************************************
 *
 *    Function Name       :CfaIvrConvertStrToVlanList 
 *
 *    Description         : This function converts the string to array of ports 
 *
 *    Input(s)            : pu1String:Pointer to the String
 *
 *    Output(s)           : au1PortArray:Pointer to array of ports  
 *
 *    Global Variables Referred :None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE 
 *****************************************************************************/
INT4
CfaIvrConvertStrToVlanList (UINT1 *pu1String, UINT1 *au1PortArray)
{
    UINT1              *pu1Temp = pu1String;
    UINT1               u1Index = 0;
    UINT1               u1TempIndex = 0;
    UINT4               u4Temp;
    UINT4               u4Tmp;
    UINT1               u1Temp;
    UINT4               u4Hold;
    UINT4               u4First;
    UINT4               u4Second;

    if ((*pu1Temp == ',') || (*pu1Temp == '-'))
    {
        return CFA_FAILURE;
    }

    if ((*(pu1Temp + STRLEN (pu1String) - 1) == ',') ||
        (*(pu1Temp + STRLEN (pu1String) - 1) == '-'))
    {
        return CFA_FAILURE;
    }

    for (u1Index = 0; u1Index < STRLEN (pu1String);
         u1Index = (UINT1) (u1Index + u1TempIndex))
    {
      /***patterns  (-- ,, -, ,-)  must be called as invalid ****/

        if (((*pu1Temp == ',') && (*(pu1Temp + 1) == ',')) ||
            ((*pu1Temp == '-') && (*(pu1Temp + 1) == '-')) ||
            ((*pu1Temp == ',') && (*(pu1Temp + 1) == '-')) ||
            ((*pu1Temp == '-') && (*(pu1Temp + 1) == ',')))
        {
            return CFA_FAILURE;
        }

      /*** checking (digit or '-' or ',') ************/

        if ((*pu1Temp != ',') && (*pu1Temp != '-') && (ISDIGIT (*pu1Temp) == 0))
        {
            return CFA_FAILURE;
        }

      /***** checking any 5 digit or more is given as portlist *******/

        if (ISDIGIT (*pu1Temp) &&
            ISDIGIT (*(pu1Temp + 1)) &&
            ISDIGIT (*(pu1Temp + 2)) &&
            ISDIGIT (*(pu1Temp + 3)) && ISDIGIT (*(pu1Temp + 4)))
        {
            return CFA_FAILURE;
        }

        /*To check if VLAN ID = 0 */
        if ((ISDIGIT (*pu1Temp)) && (*pu1Temp == '0'))
        {
            return CFA_FAILURE;
        }
        u1TempIndex = 1;

        if ((ISDIGIT (*pu1Temp)))
        {
            for (; ISDIGIT (*(pu1Temp + u1TempIndex)); u1TempIndex++);
        }

        pu1Temp += u1TempIndex;
    }

    pu1Temp = pu1String;

    for (u1Index = 0; u1Index < STRLEN (pu1String); u1Index++)
    {
        /*****patterns of 2 & 2,*******/

        if ((*pu1Temp != ',') && (*pu1Temp != '\0') &&
            (ISDIGIT (*pu1Temp) != 0))
        {
            if (*(pu1Temp + 1) == '\0')
            {
                CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, ((*pu1Temp) - '0'));
                return CFA_SUCCESS;
            }
            else if (*(pu1Temp + 1) == ',')
            {
                CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, ((*pu1Temp) - '0'));
                pu1Temp += 2;
            }
        }
        /*****patterns of 22 & 22,*******/
        if ((*pu1Temp != ',') && (*pu1Temp != '\0') &&
            (ISDIGIT (*pu1Temp) != 0) && (ISDIGIT (*(pu1Temp + 1)) != 0))
        {
            if (*(pu1Temp + 2) == '\0')
            {
                u4Temp =
                    (UINT4) ((((*pu1Temp) - '0') * 10) +
                             (*(pu1Temp + 1) - '0'));

                CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                return CFA_SUCCESS;
            }
            else if (*(pu1Temp + 2) == ',')
            {
                u4Temp =
                    (UINT4) ((((*pu1Temp) - '0') * 10) +
                             (*(pu1Temp + 1) - '0'));

                CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                pu1Temp += 3;
            }
        }
        /*****patterns of 222 & 222,*******/
        if ((*pu1Temp != ',') && (*pu1Temp != '\0') &&
            (ISDIGIT (*pu1Temp) != 0) &&
            (ISDIGIT (*(pu1Temp + 1)) != 0) && (ISDIGIT (*(pu1Temp + 2)) != 0))
        {
            if (*(pu1Temp + 3) == '\0')
            {
                u4Temp = (UINT4) ((((*pu1Temp) - '0') * 100) +
                                  (*(pu1Temp + 1) - '0') * 10 +
                                  (*(pu1Temp + 2) - '0'));

                CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                return CFA_SUCCESS;
            }
            else if ((*(pu1Temp + 3) == ','))
            {
                u4Temp = (UINT4) ((((*pu1Temp) - '0') * 100) +
                                  (*(pu1Temp + 1) - '0') * 10 +
                                  (*(pu1Temp + 2) - '0'));

                CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                pu1Temp += 4;
            }
        }

        /*****patterns of 2222 & 2222,*******/
        if ((*pu1Temp != ',') && (*pu1Temp != '\0') &&
            (ISDIGIT (*pu1Temp) != 0) &&
            (ISDIGIT (*(pu1Temp + 1)) != 0) &&
            (ISDIGIT (*(pu1Temp + 2)) != 0) && (ISDIGIT (*(pu1Temp + 3)) != 0))
        {
            if (*(pu1Temp + 4) == '\0')
            {
                u4Temp = (UINT4) ((((*pu1Temp) - '0') * 1000) +
                                  (*(pu1Temp + 1) - '0') * 100 +
                                  (*(pu1Temp + 2) - '0') * 10 +
                                  (*(pu1Temp + 3) - '0'));

                CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                return CFA_SUCCESS;
            }
            else if (*(pu1Temp + 4) == ',')
            {
                u4Temp = (UINT4) ((((*pu1Temp) - '0') * 1000) +
                                  (*(pu1Temp + 1) - '0') * 100 +
                                  (*(pu1Temp + 2) - '0') * 10 +
                                  (*(pu1Temp + 3) - '0'));

                CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                pu1Temp += 5;
            }
        }
        /*****patterns of 1-3 & 1-3,*******/
        else if ((*pu1Temp != ',') && (*pu1Temp != '\0') &&
                 (ISDIGIT (*pu1Temp) != 0) &&
                 (*(pu1Temp + 1) == '-') && (ISDIGIT (*(pu1Temp + 2)) != 0))

        {
            if (*(pu1Temp + 3) == '\0')
            {
                u4Temp = (UINT4) (*pu1Temp) - '0';

                if (((*(pu1Temp + 2) - '0')) < ((*pu1Temp - '0')))
                {
                    return CFA_FAILURE;
                }

                for (u1Temp = 0; u1Temp <= (((*(pu1Temp + 2) - '0')) -
                                            ((*pu1Temp - '0'))); u1Temp++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                    u4Temp++;
                }
                return CFA_SUCCESS;
            }
            else if (*(pu1Temp + 3) == ',')
            {
                u4Temp = (UINT4) (*pu1Temp) - '0';

                if (((*(pu1Temp + 2) - '0')) < ((*pu1Temp - '0')))
                {
                    return CFA_FAILURE;
                }

                for (u1Temp = 0; u1Temp <= (((*(pu1Temp + 2) - '0')) -
                                            ((*pu1Temp - '0'))); u1Temp++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                    u4Temp++;
                }
                pu1Temp += 4;
            }
        }
        /******* patterns of 2-34 & 2-34,**************/

        if ((*pu1Temp != ',') && (*pu1Temp != '\0') &&
            (ISDIGIT (*pu1Temp) != 0) &&
            (*(pu1Temp + 1) == '-') &&
            (ISDIGIT (*(pu1Temp + 2)) != 0) && (ISDIGIT (*(pu1Temp + 3)) != 0))
        {
            if (*(pu1Temp + 4) == '\0')
            {
                u4Temp =
                    (UINT4) ((*(pu1Temp + 2) - '0') * 10 +
                             (*(pu1Temp + 3) - '0'));
                u4Tmp = (UINT4) (*pu1Temp - '0');

                if (u4Temp < u4Tmp)
                {
                    return CFA_FAILURE;
                }

                for (u4Hold = 0; u4Hold <= (u4Temp - (UINT4) (*pu1Temp - '0'));
                     u4Hold++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Tmp);
                    u4Tmp++;
                }
                return CFA_SUCCESS;
            }
            else if (*(pu1Temp + 4) == ',')
            {
                u4Temp =
                    (UINT4) ((*(pu1Temp + 2) - '0') * 10 +
                             (*(pu1Temp + 3) - '0'));
                u4Tmp = (UINT4) (*pu1Temp - '0');

                if (u4Temp < u4Tmp)
                {
                    return CFA_FAILURE;
                }

                for (u4Hold = 0; u4Hold <= (u4Temp - (UINT4) (*pu1Temp - '0'));
                     u4Hold++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Tmp);
                    u4Tmp++;
                }
                pu1Temp += 5;
            }
        }
        /******* patterns of 2-345 & 2-345,**************/

        if ((*pu1Temp != ',') && (*pu1Temp != '\0') &&
            (ISDIGIT (*pu1Temp) != 0) &&
            (*(pu1Temp + 1) == '-') &&
            (ISDIGIT (*(pu1Temp + 2)) != 0) &&
            (ISDIGIT (*(pu1Temp + 3)) != 0) && (ISDIGIT (*(pu1Temp + 4)) != 0))
        {
            if (*(pu1Temp + 5) == '\0')
            {
                u4Temp = (UINT4) ((*(pu1Temp + 2) - '0') * 100 +
                                  (*(pu1Temp + 3) - '0') * 10 +
                                  (*(pu1Temp + 4) - '0'));

                u4Tmp = (UINT4) (*pu1Temp - '0');

                if (u4Temp < u4Tmp)
                {
                    return CFA_FAILURE;
                }

                for (u4Hold = 0; u4Hold <= (u4Temp - (UINT4) (*pu1Temp - '0'));
                     u4Hold++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Tmp);
                    u4Tmp++;
                }
                return CFA_SUCCESS;
            }
            else if (*(pu1Temp + 5) == ',')
            {
                u4Temp = (UINT4) ((*(pu1Temp + 2) - '0') * 100 +
                                  (*(pu1Temp + 3) - '0') * 10 +
                                  (*(pu1Temp + 4) - '0'));

                u4Tmp = (UINT4) (*pu1Temp - '0');

                if (u4Temp < u4Tmp)
                {
                    return CFA_FAILURE;
                }

                for (u4Hold = 0; u4Hold <= (u4Temp - (UINT4) (*pu1Temp - '0'));
                     u4Hold++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Tmp);
                    u4Tmp++;
                }
                pu1Temp += 6;
            }
        }
        /******* patterns of 2-3456 & 2-3456,**************/

        if ((*pu1Temp != ',') && (*pu1Temp != '\0') &&
            (ISDIGIT (*pu1Temp) != 0) &&
            (*(pu1Temp + 1) == '-') &&
            (ISDIGIT (*(pu1Temp + 2)) != 0) &&
            (ISDIGIT (*(pu1Temp + 3)) != 0) &&
            (ISDIGIT (*(pu1Temp + 4)) != 0) && (ISDIGIT (*(pu1Temp + 5)) != 0))
        {
            if (*(pu1Temp + 6) == '\0')
            {
                u4Temp = (UINT4) ((*(pu1Temp + 2) - '0') * 1000 +
                                  (*(pu1Temp + 3) - '0') * 100 +
                                  (*(pu1Temp + 4) - '0') * 10 +
                                  (*(pu1Temp + 5) - '0'));

                u4Tmp = (UINT4) (*pu1Temp - '0');

                if (u4Temp < u4Tmp)
                {
                    return CFA_FAILURE;
                }

                for (u4Hold = 0; u4Hold <= (u4Temp - (UINT4) (*pu1Temp - '0'));
                     u4Hold++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Tmp);
                    u4Tmp++;
                }
                return CFA_SUCCESS;
            }
            else if (*(pu1Temp + 6) == ',')
            {
                u4Temp = (UINT4) ((*(pu1Temp + 2) - '0') * 1000 +
                                  (*(pu1Temp + 3) - '0') * 100 +
                                  (*(pu1Temp + 4) - '0') * 10 +
                                  (*(pu1Temp + 5) - '0'));

                u4Tmp = (UINT4) (*pu1Temp - '0');

                if (u4Temp < u4Tmp)
                {
                    return CFA_FAILURE;
                }

                for (u4Hold = 0; u4Hold <= (u4Temp - (UINT4) (*pu1Temp - '0'));
                     u4Hold++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Tmp);
                    u4Tmp++;
                }
                pu1Temp += 7;
            }
        }
        /******* patterns of 22-34 & 22-34,**************/

        if ((*pu1Temp != ',') && (*pu1Temp != '\0') &&
            (ISDIGIT (*pu1Temp) != 0) &&
            (ISDIGIT (*(pu1Temp + 1)) != 0) &&
            (*(pu1Temp + 2) == '-') &&
            (ISDIGIT (*(pu1Temp + 3)) != 0) && (ISDIGIT (*(pu1Temp + 4)) != 0))
        {
            if (*(pu1Temp + 5) == '\0')
            {
                u4Second =
                    (UINT4) (((*(pu1Temp + 3) - '0') * 10) +
                             ((*(pu1Temp + 4) - '0')));
                u4First =
                    (UINT4) (((*(pu1Temp) - '0') * 10) +
                             ((*(pu1Temp + 1) - '0')));

                if (u4Second < u4First)
                {
                    return CFA_FAILURE;
                }

                u4Temp = u4First;

                for (u4Hold = 0; u4Hold <= (u4Second - u4First); u4Hold++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                    u4Temp++;
                }
                return CFA_SUCCESS;

            }
            else if (*(pu1Temp + 5) == ',')
            {
                u4Second =
                    (UINT4) (((*(pu1Temp + 3) - '0') * 10) +
                             ((*(pu1Temp + 4) - '0')));
                u4First =
                    (UINT4) (((*(pu1Temp) - '0') * 10) +
                             ((*(pu1Temp + 1) - '0')));

                if (u4Second < u4First)
                {
                    return CFA_FAILURE;
                }

                u4Temp = u4First;

                for (u4Hold = 0; u4Hold <= (u4Second - u4First); u4Hold++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                    u4Temp++;
                }
                pu1Temp += 6;
            }
        }
        /******* patterns of 22-345 & 22-345,**************/

        if ((*pu1Temp != ',') && (*pu1Temp != '\0') &&
            (ISDIGIT (*pu1Temp) != 0) &&
            (ISDIGIT (*(pu1Temp + 1)) != 0) &&
            (*(pu1Temp + 2) == '-') &&
            (ISDIGIT (*(pu1Temp + 3)) != 0) &&
            (ISDIGIT (*(pu1Temp + 4)) != 0) && (ISDIGIT (*(pu1Temp + 5)) != 0))
        {
            if (*(pu1Temp + 6) == '\0')
            {
                u4Second = (UINT4) (((*(pu1Temp + 3) - '0') * 100) +
                                    ((*(pu1Temp + 4) - '0') * 10) +
                                    ((*(pu1Temp + 5) - '0')));

                u4First =
                    (UINT4) (((*(pu1Temp) - '0') * 10) +
                             ((*(pu1Temp + 1) - '0')));

                if (u4Second < u4First)
                {
                    return CFA_FAILURE;
                }

                u4Temp = u4First;

                for (u4Hold = 0; u4Hold <= (u4Second - u4First); u4Hold++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                    u4Temp++;
                }
                return CFA_SUCCESS;

            }
            else if (*(pu1Temp + 6) == ',')
            {
                u4Second = (UINT4) (((*(pu1Temp + 3) - '0') * 100) +
                                    ((*(pu1Temp + 4) - '0') * 10) +
                                    ((*(pu1Temp + 5) - '0')));

                u4First =
                    (UINT4) (((*(pu1Temp) - '0') * 10) +
                             ((*(pu1Temp + 1) - '0')));

                if (u4Second < u4First)
                {
                    return CFA_FAILURE;
                }

                u4Temp = u4First;

                for (u4Hold = 0; u4Hold <= (u4Second - u4First); u4Hold++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                    u4Temp++;
                }
                pu1Temp += 7;
            }
        }
        /******* patterns of 22-3456 & 22-3456,**************/

        if ((*pu1Temp != ',') && (*pu1Temp != '\0') &&
            (ISDIGIT (*pu1Temp) != 0) &&
            (ISDIGIT (*(pu1Temp + 1)) != 0) &&
            (*(pu1Temp + 2) == '-') &&
            (ISDIGIT (*(pu1Temp + 3)) != 0) &&
            (ISDIGIT (*(pu1Temp + 4)) != 0) &&
            (ISDIGIT (*(pu1Temp + 5)) != 0) && (ISDIGIT (*(pu1Temp + 6)) != 0))
        {
            if (*(pu1Temp + 7) == '\0')
            {
                u4Second = (UINT4) (((*(pu1Temp + 3) - '0') * 1000) +
                                    ((*(pu1Temp + 4) - '0') * 100) +
                                    ((*(pu1Temp + 5) - '0') * 10) +
                                    ((*(pu1Temp + 6) - '0')));

                u4First =
                    (UINT4) (((*(pu1Temp) - '0') * 10) +
                             ((*(pu1Temp + 1) - '0')));

                if (u4Second < u4First)
                {
                    return CFA_FAILURE;
                }

                u4Temp = u4First;

                for (u4Hold = 0; u4Hold <= (u4Second - u4First); u4Hold++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                    u4Temp++;
                }
                return CFA_SUCCESS;

            }
            else if (*(pu1Temp + 7) == ',')
            {
                u4Second = (UINT4) (((*(pu1Temp + 3) - '0') * 1000) +
                                    ((*(pu1Temp + 4) - '0') * 100) +
                                    ((*(pu1Temp + 5) - '0') * 10) +
                                    ((*(pu1Temp + 6) - '0')));

                u4First =
                    (UINT4) (((*(pu1Temp) - '0') * 10) +
                             ((*(pu1Temp + 1) - '0')));

                if (u4Second < u4First)
                {
                    return CFA_FAILURE;
                }

                u4Temp = u4First;

                for (u4Hold = 0; u4Hold <= (u4Second - u4First); u4Hold++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                    u4Temp++;
                }
                pu1Temp += 8;
            }
        }
        /******* patterns of 222-345 & 222-345,**************/

        if ((*pu1Temp != ',') && (*pu1Temp != '\0') &&
            (ISDIGIT (*pu1Temp) != 0) &&
            (ISDIGIT (*(pu1Temp + 1)) != 0) &&
            (ISDIGIT (*(pu1Temp + 2)) != 0) &&
            (*(pu1Temp + 3) == '-') &&
            (ISDIGIT (*(pu1Temp + 4)) != 0) &&
            (ISDIGIT (*(pu1Temp + 5)) != 0) && (ISDIGIT (*(pu1Temp + 6)) != 0))
        {
            if (*(pu1Temp + 7) == '\0')
            {
                u4Second = (UINT4) (((*(pu1Temp + 4) - '0') * 100) +
                                    ((*(pu1Temp + 5) - '0') * 10) +
                                    ((*(pu1Temp + 6) - '0')));

                u4First = (UINT4) (((*(pu1Temp) - '0') * 100) +
                                   ((*(pu1Temp + 1) - '0') * 10) +
                                   ((*(pu1Temp + 2) - '0')));

                if (u4Second < u4First)
                {
                    return CFA_FAILURE;
                }

                u4Temp = u4First;

                for (u4Hold = 0; u4Hold <= (u4Second - u4First); u4Hold++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                    u4Temp++;
                }
                return CFA_SUCCESS;

            }
            else if (*(pu1Temp + 7) == ',')
            {
                u4Second = (UINT4) (((*(pu1Temp + 4) - '0') * 100) +
                                    ((*(pu1Temp + 5) - '0') * 10) +
                                    ((*(pu1Temp + 6) - '0')));

                u4First = (UINT4) (((*(pu1Temp) - '0') * 100) +
                                   ((*(pu1Temp + 1) - '0') * 10) +
                                   ((*(pu1Temp + 2) - '0')));

                if (u4Second < u4First)
                {
                    return CFA_FAILURE;
                }

                u4Temp = u4First;

                for (u4Hold = 0; u4Hold <= (u4Second - u4First); u4Hold++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                    u4Temp++;
                }
                pu1Temp += 8;
            }
        }
        /******* patterns of 222-3456 & 222-3456,**************/

        if ((*pu1Temp != ',') && (*pu1Temp != '\0') &&
            (ISDIGIT (*pu1Temp) != 0) &&
            (ISDIGIT (*(pu1Temp + 1)) != 0) &&
            (ISDIGIT (*(pu1Temp + 2)) != 0) &&
            (*(pu1Temp + 3) == '-') &&
            (ISDIGIT (*(pu1Temp + 4)) != 0) &&
            (ISDIGIT (*(pu1Temp + 5)) != 0) &&
            (ISDIGIT (*(pu1Temp + 6)) != 0) && (ISDIGIT (*(pu1Temp + 7)) != 0))
        {
            if (*(pu1Temp + 8) == '\0')
            {
                u4Second = (UINT4) (((*(pu1Temp + 4) - '0') * 1000) +
                                    ((*(pu1Temp + 5) - '0') * 100) +
                                    ((*(pu1Temp + 6) - '0') * 10) +
                                    ((*(pu1Temp + 7) - '0')));

                u4First = (UINT4) (((*(pu1Temp) - '0') * 100) +
                                   ((*(pu1Temp + 1) - '0') * 10) +
                                   ((*(pu1Temp + 2) - '0')));

                if (u4Second < u4First)
                {
                    return CFA_FAILURE;
                }

                u4Temp = u4First;

                for (u4Hold = 0; u4Hold <= (u4Second - u4First); u4Hold++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                    u4Temp++;
                }
                return CFA_SUCCESS;

            }
            else if (*(pu1Temp + 8) == ',')
            {
                u4Second = (UINT4) (((*(pu1Temp + 4) - '0') * 1000) +
                                    ((*(pu1Temp + 5) - '0') * 100) +
                                    ((*(pu1Temp + 6) - '0') * 10) +
                                    ((*(pu1Temp + 7) - '0')));

                u4First = (UINT4) (((*(pu1Temp) - '0') * 100) +
                                   ((*(pu1Temp + 1) - '0') * 10) +
                                   ((*(pu1Temp + 2) - '0')));

                if (u4Second < u4First)
                {
                    return CFA_FAILURE;
                }

                u4Temp = u4First;

                for (u4Hold = 0; u4Hold <= (u4Second - u4First); u4Hold++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                    u4Temp++;
                }
                pu1Temp += 9;
            }
        }
        /******* patterns of 2222-3456 & 2222-3456,**************/

        if ((*pu1Temp != ',') && (*pu1Temp != '\0') &&
            (ISDIGIT (*pu1Temp) != 0) &&
            (ISDIGIT (*(pu1Temp + 1)) != 0) &&
            (ISDIGIT (*(pu1Temp + 2)) != 0) &&
            (ISDIGIT (*(pu1Temp + 3)) != 0) &&
            (*(pu1Temp + 4) == '-') &&
            (ISDIGIT (*(pu1Temp + 5)) != 0) &&
            (ISDIGIT (*(pu1Temp + 6)) != 0) &&
            (ISDIGIT (*(pu1Temp + 7)) != 0) && (ISDIGIT (*(pu1Temp + 8)) != 0))
        {
            if (*(pu1Temp + 9) == '\0')
            {
                u4Second = (UINT4) (((*(pu1Temp + 5) - '0') * 1000) +
                                    ((*(pu1Temp + 6) - '0') * 100) +
                                    ((*(pu1Temp + 7) - '0') * 10) +
                                    ((*(pu1Temp + 8) - '0')));

                u4First = (UINT4) (((*(pu1Temp) - '0') * 1000) +
                                   ((*(pu1Temp + 1) - '0') * 100) +
                                   ((*(pu1Temp + 2) - '0') * 10) +
                                   ((*(pu1Temp + 3) - '0')));

                if (u4Second < u4First)
                {
                    return CFA_FAILURE;
                }

                u4Temp = u4First;

                for (u4Hold = 0; u4Hold <= (u4Second - u4First); u4Hold++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                    u4Temp++;
                }
                return CFA_SUCCESS;

            }
            else if (*(pu1Temp + 9) == ',')
            {
                u4Second = (UINT4) (((*(pu1Temp + 5) - '0') * 1000) +
                                    ((*(pu1Temp + 6) - '0') * 100) +
                                    ((*(pu1Temp + 7) - '0') * 10) +
                                    ((*(pu1Temp + 8) - '0')));

                u4First = (UINT4) (((*(pu1Temp) - '0') * 1000) +
                                   ((*(pu1Temp + 1) - '0') * 100) +
                                   ((*(pu1Temp + 2) - '0') * 10) +
                                   ((*(pu1Temp + 3) - '0')));

                if (u4Second < u4First)
                {
                    return CFA_FAILURE;
                }

                u4Temp = u4First;

                for (u4Hold = 0; u4Hold <= (u4Second - u4First); u4Hold++)
                {
                    CFA_MGMT_SET_MEMBER_VLAN (au1PortArray, u4Temp);
                    u4Temp++;
                }
                pu1Temp += 10;
            }
        }
    }
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name        : CfaIsMacAllZeros                                  */
/*                                                                           */
/* Description          : This function checks whether the given MAC address contains  */
/*                       all zeroes                                          */
/*                                                                           */
/* Input(s)             : pu1MacAddr - Mac address                           */
/*                                                                           */
/* Output(s)            : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Referred             : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified             : None                                               */
/*                                                                           */
/* Return Value(s)      : VLAN_TRUE - If the MAC is all zeroes               */
/*                       VLAN_FALSE - otherwise                              */
/*****************************************************************************/

INT4
CfaIsMacAllZeros (UINT1 *pu1MacAddr)
{
    if ((*pu1MacAddr == 0) && (*(pu1MacAddr + 1) == 0) &&
        (*(pu1MacAddr + 2) == 0) && (*(pu1MacAddr + 3) == 0) &&
        (*(pu1MacAddr + 4) == 0) && (*(pu1MacAddr + 5) == 0))
    {
        return CFA_TRUE;
    }
    return CFA_FALSE;
}

#ifndef NPSIM_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaGddGetHwAddr
 *
 *    Description        : Function is called for getting the hardware
 *                address of the Enet ports. This function can
 *                be called only after the opening of the ports
 *                - the ports should be open but Admin/Oper
 *                Status can be down.
 *
 *    Input(s)            : UINT4 u4IfIndex.
 *
 *    Output(s)            : UINT1 *au1HwAddr.
 *
 *    Global Variables Referred : gIfGlobal (Interface's global struct),
 *                gaPhysIfTable (GDD Reg Table) structure.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if address is obtained,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddGetHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
    UINT1               au1IfHwAddr[CFA_ENET_ADDR_LEN];

    UINT1               u1IfType = CFA_NONE;

    CFA_DBG1 (CFA_TRC_ALL, CFA_GDD,
              "Entering CfaGddGetHwAddr for interface %u.\n", u4IfIndex);
    MEMSET (au1IfHwAddr, 0, CFA_ENET_ADDR_LEN);
    if ((u4IfIndex > SYS_DEF_MAX_INTERFACES) || (u4IfIndex < 1))
    {

        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddGetHwAddr - "
                  "Interface %u not Valid - FAILURE.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

    /* 
     * When the request comes from ARP for the hardware address 
     * of VLAN interface, this function should not return FAILURE. Instead, 
     * copy the MAC address of the default bridged Ethernet interface into 
     * the MAC address of VLAN interface and ARP/RARP will update the same 
     * for VLAN interfaces.
     */
    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        if ((u1IfType == CFA_L3IPVLAN)
#ifdef WGS_WANTED
            || (u1IfType == CFA_L2VLAN)
#endif /* WGS_WANTED */
            )
        {
            if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
            {
                /* The stack interface uses the stack mac address
                 * which is different from the system mac address*/

                if (u4IfIndex == CFA_DEFAULT_STACK_IFINDEX)
                {
                    CfaGetIfHwAddr (CFA_DEFAULT_STACK_IFINDEX, au1HwAddr);
                }
                else
                {
                    CfaGetSysMacAddress (au1IfHwAddr);
                }

                MEMCPY (au1HwAddr, au1IfHwAddr, CFA_ENET_ADDR_LEN);

                return CFA_SUCCESS;
            }
            else
            {
                CfaGetSysMacAddress (au1IfHwAddr);

                MEMCPY (au1HwAddr, au1IfHwAddr, CFA_ENET_ADDR_LEN);

                return CFA_SUCCESS;
            }
        }
    }

/* cant get HW address for ports which are not registered - we dont have
socket/file descriptors for them and only ETH_SOCK is supported currently */
    if ((CFA_GDD_REGSTAT (u4IfIndex) == CFA_INVALID) ||
        (CFA_GDD_TYPE (u4IfIndex) != CFA_ETHERNET))
    {

        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddGetHwAddr - "
                  "Interface %u not Registered/Enet - FAILURE.\n", u4IfIndex);

        return (CFA_FAILURE);
    }
    CfaGetIfHwAddr (u4IfIndex, au1IfHwAddr);
    MEMCPY (au1HwAddr, au1IfHwAddr, CFA_ENET_ADDR_LEN);

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_GDD,
              "In CfaGddGetHwAddr - "
              "Got Address of interface %u - SUCCESS.\n", u4IfIndex);

    return (CFA_SUCCESS);
}
#endif /* NPSIM_WANTED */

INT4
CfaGetSlotAndPortNum (INT1 *pi1IfNum, INT4 *pi4SlotNum, INT4 *pi4PortNum)
{
    UINT2               u2StrLen;
    UINT2               u2Index = 0;
    BOOL1               bIsDigitFound = FALSE;
    INT1               *pi1Ptr;

    if (!(pi1IfNum) || !(*pi1IfNum))
        return FALSE;

    u2StrLen = (UINT2) STRLEN (pi1IfNum);

    pi1Ptr = &pi1IfNum[u2Index];

    for (; u2Index < u2StrLen; u2Index++)
    {
        if (!(ISDIGIT (pi1IfNum[u2Index])))
        {
            if ((pi1IfNum[u2Index] != '/') || (bIsDigitFound == FALSE))
                return FALSE;
            else
                break;
        }
        bIsDigitFound = TRUE;
    }

    /* Check if it is an index for port-channel type tokens */
    if (!(pi1IfNum[u2Index]))
    {
        *pi4PortNum = ATOI (pi1Ptr);
        return TRUE;
    }

    if ((pi1IfNum[u2Index] != '/'))
        return FALSE;

    pi1IfNum[u2Index] = '\0';

    *pi4SlotNum = ATOI (pi1Ptr);

    pi1IfNum[u2Index] = '/';

    u2Index++;

    pi1Ptr = &pi1IfNum[u2Index];
    bIsDigitFound = FALSE;

    for (; u2Index < u2StrLen; u2Index++)
    {
        if (!(ISDIGIT (pi1IfNum[u2Index])))
        {
            return FALSE;
        }
        bIsDigitFound = TRUE;
    }
    if (bIsDigitFound == FALSE)
        return FALSE;

    *pi4PortNum = ATOI (pi1Ptr);
    return TRUE;
}

INT4
CfaValidateIfNum (UINT4 u4IfType, INT1 *pi1IfNum, INT4 *pi4SlotNum,
                  UINT4 *pu4RetIfNum)
{
    INT4                i4PortNum = -1;

    *pi4SlotNum = -1;

    if (CfaGetSlotAndPortNum (pi1IfNum, pi4SlotNum, &i4PortNum) == FALSE)
    {
        return OSIX_FAILURE;
    }
    if ((u4IfType == CFA_LAGG) || (u4IfType == CFA_BRIDGED_INTERFACE) ||
        (u4IfType == CFA_PIP) || (u4IfType == CFA_ILAN) ||
        (u4IfType == CFA_PROP_VIRTUAL_INTERFACE) || (u4IfType == CFA_L3IPVLAN)
        || (u4IfType == CFA_PSEUDO_WIRE) || (u4IfType == CFA_VXLAN_NVE) ||
        (u4IfType == CFA_TAP) || (u4IfType == CFA_STATION_FACING_BRIDGE_PORT))
    {
        /* Slot num shud be -1 for Port-channel interfaces and
         * Port Number should be in range 1-65535 */
        if ((*pi4SlotNum != -1) || (i4PortNum < 1) || (i4PortNum > 65535))
        {
            return OSIX_FAILURE;
        }
        *pu4RetIfNum = (UINT4) i4PortNum;
        return OSIX_SUCCESS;
    }
    else if ((u4IfType == CFA_FA_ENET) || (u4IfType == CFA_GI_ENET) ||
             (u4IfType == CFA_XE_ENET) || (u4IfType == CFA_XL_ENET) ||
             (u4IfType == CFA_LVI_ENET) || (u4IfType == CFA_STACK_ENET))
    {
        /* Slot num shudn't be -1 for Ether type  interfaces */
        if (*pi4SlotNum == -1)
        {
            return OSIX_FAILURE;
        }
#ifndef MBSM_WANTED
        if (CfaGetIfIndex (u4IfType, i4PortNum, pu4RetIfNum) == CFA_FAILURE)
#else
        if (CfaGetSlotLocalPortNum (u4IfType, i4PortNum, *pi4SlotNum,
                                    pu4RetIfNum) == CFA_FAILURE)
#endif
        {
            return CLI_ERR_INVALID_INTERFACE_NUM;
        }
    }
#ifdef HDLC_WANTED
    else if (u4IfType == CFA_HDLC)
    {
        if (*pi4SlotNum == -1)
        {
            return OSIX_FAILURE;
        }
        *pu4RetIfNum = (UINT4) i4PortNum;
        return OSIX_SUCCESS;
    }
#endif
    else
    {
        return OSIX_FAILURE;
    }

    if (CfaValidateSlotAndPortNum (u4IfType, *pi4SlotNum, *pu4RetIfNum) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
CfaValidateSlotAndPortNum (UINT4 u4IfType, INT4 i4SlotNum, UINT4 u4PortNum)
{
    /* Portable function to get the interface index 
     * from the slot/port number & iface type */
    if ((u4IfType == CFA_FA_ENET) || (u4IfType == CFA_GI_ENET) ||
        (u4IfType == CFA_XE_ENET) || (u4IfType == CFA_XL_ENET) ||
        (u4IfType == CFA_LVI_ENET) || (u4IfType == CFA_STACK_ENET))
    {
        /* Check if the given slot, port number is valid for this 
         * type of interface.
         * 
         * For system with fixed number of ports[pizza box], let the
         * Slot number be zero.
         * For system with multi boards[Chassis], let the
         * Slot number not to exceed MBSM_MAX_SLOTS.
         */
#ifndef MBSM_WANTED
        if ((i4SlotNum != 0) ||
            (u4PortNum > CFA_PHYS_NUM ()) || (u4PortNum == 0))
#else
        if ((i4SlotNum < (MBSM_SLOT_INDEX_START + MBSM_MAX_CC_SLOTS)) ||
            (i4SlotNum >= (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START))
            || (u4PortNum > CFA_PHYS_NUM ()))
#endif
        {
            return OSIX_FAILURE;
        }
    }
    else
    {
        /* This case will be the general case which will be called from CLI 
         * lex function to validate the slot/port number. 
         * Here we don't know the Interface Type and hence
         * we check for max. ports and valid slot numbers.
         * 
         * For system with fixed number of ports[pizza box], let the
         * Slot number be zero.
         * For system with multi boards[Chassis], let the
         * Slot number not to exceed MBSM_MAX_SLOTS.
         */

#ifndef MBSM_WANTED
        if ((i4SlotNum != 0) ||
            (u4PortNum > CFA_PHYS_NUM ()) || (u4PortNum == 0))
#else
        if ((i4SlotNum > MBSM_MAX_SLOTS) || (u4PortNum > CFA_PHYS_NUM ()))
#endif
        {
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

INT4
CfaGetPhysIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    /* Portable function to get the interface name 
     * according to the slots/ports supported
     */
    INT4                i4Index;
    INT4                i4SlotNum = 0;
    INT4                i4IfNum = 0;
    INT4                i4PortNum = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1IfType;
    UINT1              *pu1IfName;
#ifdef LNXIP4_WANTED
    UINT1               u1Status = CFA_ENABLED;
#endif
    if (CfaGetEthernetType (u4IfIndex, &u1IfType) != CFA_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (u1IfType == CFA_ENET_UNKNOWN)
    {
        SPRINTF ((CHR1 *) pi1IfName, "Unknown");
        return CLI_SUCCESS;
    }

    if ((u1IfType != CFA_GI_ENET) && (u1IfType != CFA_FA_ENET) &&
        (u1IfType != CFA_XE_ENET) && (u1IfType != CFA_XL_ENET) &&
        (u1IfType != CFA_LVI_ENET) && (u1IfType != CFA_STACK_ENET))
    {
        return CLI_FAILURE;
    }

    CfaGetIfName (u4IfIndex, au1IfName);
#ifdef LNXIP4_WANTED
    if ((CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1Status) == CFA_SUCCESS) &&
        (u1Status == CFA_DISABLED))
    {
        STRNCPY (au1IfName, CFA_GDD_PORT_NAME (u4IfIndex),
                 STRLEN (CFA_GDD_PORT_NAME (u4IfIndex)));
        au1IfName[STRLEN (CFA_GDD_PORT_NAME (u4IfIndex))] = '\0';
    }
#endif

    for (i4Index = 0; i4Index < CLI_MAX_IFTYPES; i4Index++)
    {
        /* SUBTYPE changes. 
         * Here the second parameter for asCfaIfaces shows that subtype is zero.
         */
        if (asCfaIfaces[i4Index][0].u4IfType == (UINT4) u1IfType)
        {
            break;
        }
    }

    if (i4Index == CLI_MAX_IFTYPES)
    {
        return CLI_FAILURE;
    }

    pu1IfName = (au1IfName + 4);

    if (CfaGetSlotAndPortNum ((INT1 *) pu1IfName,
                              &i4SlotNum, &i4IfNum) == FALSE)
    {
        return CLI_FAILURE;
    }
    if (CfaGetPortNumFromIfIndex ((UINT4) u1IfType, u4IfIndex,
                                  i4SlotNum, i4IfNum,
                                  &i4PortNum) != CFA_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* pu1IfName contains both the slot number and port number (e,g: 2/1) */
    SNPRINTF ((CHR1 *) pi1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s%u/%u",
              asCfaIfaces[i4Index][0].au1IfName, i4SlotNum, i4PortNum);

    return CLI_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetL3SubIfName
 *
 *    Description         : This function gives the interface name
 *                          of a L3 sub-interface .
 *
 *    Input(s)            : u4IfIndex - Interface index
 *
 *    Output(s)           : pi1IfName
 *
 *    Returns             : CFA_SUCCESS-If interface is a L3-Sub interface
 *                          CFA_FAILURE  otherwise.
 *****************************************************************************/
INT4
CfaGetL3SubIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    /* Portable function to get the interface name 
     * according to the slots/ports supported
     */
    INT4                i4Index = 0;
    INT4                i4SlotNum = 0;
    INT4                i4Lport = 0;
    INT4                i4PortNum = 0;
    INT4                i4AggId = 0;
    INT4                i4LAggId = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1IfType = CFA_ENET_UNKNOWN;
    UINT1              *pu1IfName;

    MEMSET (au1IfName, 0, sizeof (au1IfName));

    CfaGetIfName (u4IfIndex, au1IfName);

    if (STRNCMP (au1IfName, "po", 2) != 0)
    {
        if (CfaGetEthernetType (u4IfIndex, &u1IfType) != CFA_SUCCESS)
        {
            return CFA_FAILURE;
        }

        if (u1IfType == CFA_ENET_UNKNOWN)
        {
            SPRINTF ((CHR1 *) pi1IfName, "Unknown");
            return CFA_SUCCESS;
        }

        if ((u1IfType != CFA_GI_ENET) && (u1IfType != CFA_FA_ENET) &&
            (u1IfType != CFA_XE_ENET) && (u1IfType != CFA_XL_ENET) &&
            (u1IfType != CFA_LVI_ENET) && (u1IfType != CFA_STACK_ENET))
        {
            CFA_DBG1 (CFA_TRC_ALL, CFA_IF,
                      "CfaGetL3SubIfName: IfType is Invalid for index %u.\n",
                      u4IfIndex);
            return CFA_FAILURE;
        }
        CfaGetIfName (u4IfIndex, au1IfName);
#if defined (WLC_WANTED) || defined (WTP_WANTED)
#ifdef LNXIP4_WANTED
        STRNCPY ((CHR1 *) au1IfName, CFA_CDB_PORT_NAME (u4IfIndex),
                 STRLEN (CFA_CDB_PORT_NAME (u4IfIndex)));
        au1IfName[STRLEN (CFA_CDB_PORT_NAME (u4IfIndex))] = '\0';
#endif
#endif
        for (i4Index = 0; i4Index < CLI_MAX_IFTYPES; i4Index++)
        {
            /* SUBTYPE changes. 
             * Here the second parameter for asCfaIfaces shows that subtype is zero.
             */
            if (asCfaIfaces[i4Index][0].u4IfType == (UINT4) u1IfType)
            {
                break;
            }
        }

        if (i4Index == CLI_MAX_IFTYPES)
        {
            CFA_DBG1 (CFA_TRC_ALL, CFA_IF,
                      "CfaGetL3SubIfName: IfType is Invalid for index %u.\n",
                      u4IfIndex);
            return CFA_FAILURE;
        }
        if (STRNCMP (au1IfName, "po", 2) != 0)

            pu1IfName = (au1IfName + 4);
        if (CfaCliGetPhysicalAndLogicalPortNum
            ((INT1 *) pu1IfName, &i4PortNum, &i4Lport,
             &i4SlotNum) == CFA_FAILURE)
        {
            CFA_DBG1 (CFA_TRC_ALL, CFA_IF,
                      "CfaGetL3SubIfName: Unable to get Port and slot details"
                      "  for index %u.\n", u4IfIndex);
            return CFA_FAILURE;
        }

        /* pu1IfName contains both the slot number and port number (e,g: 2/1.1) */
        SNPRINTF ((CHR1 *) pi1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s%u/%u.%d",
                  asCfaIfaces[i4Index][0].au1IfName, i4SlotNum, i4PortNum,
                  i4Lport);

    }
    else
    {
        pu1IfName = (au1IfName + 2);

        if (CfaCliGetPortChannelPhysicalAndLogicalPortNum
            ((INT1 *) pu1IfName, &i4AggId, &i4LAggId) == CFA_FAILURE)
        {
            CFA_DBG1 (CFA_TRC_ALL, CFA_IF,
                      "CfaGetL3SubIfName: Unable to get Port and Logical Port for"
                      "Port-channel SubInterface %u.\n", u4IfIndex);
            return CFA_FAILURE;
        }

        SNPRINTF ((CHR1 *) pi1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s%u.%u",
                  "po", i4AggId, i4LAggId);
    }
    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_get_iface_type_name                            */
/*                                                                           */
/*     DESCRIPTION      : This function returns interface name from interface*/
/*                                                                      type */
/*                                                                           */
/*     INPUT            : u4Type - interface type                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
/* SUBTYPE changes */
INT4               *
cli_get_iface_type_name (UINT4 u4Type)
{
    return (CfaUtilGetIfaceTypeName (u4Type, 0));
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaUtilGetIfaceTypeName                            */
/*                                                                           */
/*     DESCRIPTION      : This function returns interface name from interface*/
/*                        type and sub type                                  */
/*                                                                           */
/*     INPUT            : u4Type - interface type                            */
/*                        u4SubType - interface sub type.                    */
/*                                    1 for AC interface and for all other   */
/*                                    interfaces, it is zero.                */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4               *
CfaUtilGetIfaceTypeName (UINT4 u4Type, UINT4 u4SubType)
{
    UINT4               u4Count = 0;

    for (u4Count = 0; u4Count < CLI_MAX_IFTYPES; ++u4Count)
    {
        if (asCfaIfaces[u4Count][u4SubType].u4IfType == u4Type)
        {
            return ((INT4 *) (VOID *) asCfaIfaces[u4Count][u4SubType].
                    au1IfName);

        }
    }
    return (FALSE);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaUtilGetSubTypeFromNameAndType                   */
/*                                                                           */
/*     DESCRIPTION      : This function returns subtype from name and type   */
/*                                                                           */
/*     INPUT            : pu1Name  - interface name                          */
/*                        u4Type    - interface type                         */
/*                                                                           */
/*     OUTPUT           : u4SubType - interface sub type.                    */
/*                                    1 for AC interface and for all other   */
/*                                    interfaces, it is zero.                */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS/CFA_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
/* SUBTYPE changes */
INT4
CfaUtilGetSubTypeFromNameAndType (UINT1 *pu1Name, UINT4 u4Type,
                                  INT4 *pi4SubType)
{
    INT4                i4TypeFlag = 0;
    INT4                i4Count = 0;

    for (i4Count = 0; i4Count < CLI_MAX_IFTYPES; i4Count++)
    {
        if (asCfaIfaces[i4Count][0].u4IfType == u4Type)
        {
            i4TypeFlag = i4Count;
            break;
        }
    }

    if (i4TypeFlag == 0)
    {
        return CFA_FAILURE;
    }

    for (i4Count = 0; i4Count < CLI_MAX_IFSUBTYPES; i4Count++)
    {
        if (CLI_STRSTR (pu1Name,
                        asCfaIfaces[i4TypeFlag][i4Count].au1IfName) != NULL)
        {
            *pi4SubType = i4Count;
            return CFA_SUCCESS;
        }
    }
    return CFA_FAILURE;
}

INT4
cli_get_if_type (UINT1 *pu1Interface)
{
    tCfaIfaceInfo       sIfaceInfo;

    if (CfaGetIfaceInfo (pu1Interface, &sIfaceInfo) == FALSE)
    {
        return CLI_ERROR;
    }

    return ((INT4) sIfaceInfo.u4IfType);

}

/*****************************************************************************
 *
 *    Function Name       : CfaSetDynamicIfName 
 *
 *    Description         : This function is used to set the Interface name 
 *                          dynamically.
 *
 *    Input(s)            : u4IfIndex  - interface index
 *                          pu1Name    - Pointer to the interface name
 *                          u1Count    - size
 *
 *    Output(s)           : None 
 *    Global Variables Referred : None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *****************************************************************************/
VOID
CfaSetDynamicIfName (UINT4 u4IfIndex, UINT1 *pu1Name, UINT1 u1Count)
{
    CFA_DS_LOCK ();

    SPRINTF ((CHR1 *) CFA_CDB_IF_ALIAS (u4IfIndex), "%s%d", pu1Name, u1Count);
    CFA_DS_UNLOCK ();
    return;
}

/*****************************************************************************
 *
 *    Function Name       : CfaTxFillVlanInfo 
 *
 *    Description         : This is an utility routine to fill the given 
 *                          parameters in the Vlan info structure.
 *
 *    Input(s)            : DestAddr  -  Destination Macaddress
 *                          u2OutPort -  Output port
 *                          bTag      -  Whether the packet should be tagged
 *                                       before transmission if the packet is
 *                                       to be transmitted on a single port.
 *                          Ports     -  Ports on which the packet has to be 
 *                                       sent as tagged.
 *                          UnTagPorts-  Ports on which the packet has to be 
 *                                       sent as untagged.
 *
 *    Output(s)           : pVlanInfo -  Pointer to the filled Vlan information
 *
 *    Global Variables Referred : None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *****************************************************************************/
VOID
CfaTxFillVlanInfo (tMacAddr DestAddr, UINT4 u4OutPort, BOOL1 bTag,
                   tPortList TagPorts, tPortList UnTagPorts,
                   tCfaVlanInfo * pVlanInfo)
{
    UINT2               u2EtherType = 0;

    if (u4OutPort == VLAN_INVALID_PORT)
    {
        if (CFA_IS_ENET_MAC_MCAST (DestAddr))
        {
            pVlanInfo->u2PktType = CFA_NP_MCAST_PKT;
        }
        else if (CFA_IS_ENET_MAC_BCAST (DestAddr))
        {
            pVlanInfo->u2PktType = CFA_NP_BCAST_PKT;
        }
        else
        {
            pVlanInfo->u2PktType = CFA_NP_UNKNOWN_UCAST_PKT;

        }

        MEMCPY (pVlanInfo->unPortInfo.TxPorts.pTagPorts, TagPorts,
                sizeof (tPortList));

        MEMCPY (pVlanInfo->unPortInfo.TxPorts.pUnTagPorts, UnTagPorts,
                sizeof (tPortList));
    }
    else
    {
        pVlanInfo->u2PktType = CFA_NP_KNOWN_UCAST_PKT;

        pVlanInfo->unPortInfo.TxUcastPort.u2TxPort = (UINT2) u4OutPort;
#ifdef VLAN_WANTED
        VlanGetPortEtherType (u4OutPort, &u2EtherType);
#endif
        pVlanInfo->unPortInfo.TxUcastPort.u2EtherType = u2EtherType;

        if (bTag == VLAN_FALSE)
        {
            pVlanInfo->unPortInfo.TxUcastPort.u1Tag = CFA_NP_UNTAGGED;
        }
        else
        {
            pVlanInfo->unPortInfo.TxUcastPort.u1Tag = CFA_NP_TAGGED;
        }
    }
}

#ifdef WTP_WANTED
/*****************************************************************************
 *
 *    Function Name       : CfaTxToWssWlanPorts 
 *
 *    Description         : This function is used to send packet to WLAN 
 *                          interface in WTP in case of Local Routing.  
 *
 *    Input(s)            : pu1DataBuf - Data buffer
 *                           VlanId    - Vlan Id 
 *                           u4PktSize  - packet size
 *
 *    Output(s)           : None 
 *    Global Variables Referred : None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *****************************************************************************/
INT4
CfaTxToWssWlanPorts (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    UINT1               u1BssOperStatus = 0;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    unApHdlrMsgStruct   ApHdlrMsgStruct;

    CfaGetIfOperStatus (u4IfIndex, &u1BssOperStatus);
    if (u1BssOperStatus != CFA_IF_UP)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_L2,
                 "The VAP interface is not Operationally UP\n");
        printf ("WLAN Oper Status DOWN\r\n");
        return (CFA_FAILURE);
    }

    /* Allocate CRU buffer */
    pBuf =
        (tCRU_BUF_CHAIN_HEADER *) CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0);
    if (pBuf == NULL)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_L2, "CRU_BUF Alloc FAILED\n");
        printf ("CRU BUF Alloc FAILURE\r\n");
        return (CFA_FAILURE);
    }

    /* Copy the received packet */
    CRU_BUF_Copy_OverBufChain (pBuf, pu1DataBuf, 0, u4PktSize);

    MEMSET (&ApHdlrMsgStruct, 0, sizeof (unApHdlrMsgStruct));

    ApHdlrMsgStruct.ApHdlrQueueReq.pRcvBuf = pBuf;
    ApHdlrMsgStruct.ApHdlrQueueReq.u4IfIndex = u4IfIndex;
    ApHdlrMsgStruct.ApHdlrQueueReq.u1MacType = LOCAL_ROUTING_TYPE;
    ApHdlrMsgStruct.ApHdlrQueueReq.u4MsgType = APHDLR_DATA_RX_MSG;
    /* Invoke the WSSIF module to send the packet to MAC Handler */
    if (WssIfProcessApHdlrMsg (WSS_APHDLR_CAPWAP_QUEUE_REQ,
                               &ApHdlrMsgStruct) != OSIX_SUCCESS)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_L2,
                 "WSS_APHDLR_CAPWAP_QUEUE_REQ Failed in CFA\r\n");
        printf ("ApHdlr Returned Failure\r\n");
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}
#endif

/*****************************************************************************
 *
 *    Function Name       : CfaL3VlanIntfWrite 
 *
 *    Description          : This function is used to determine the ports    
 *                           for the vlan on which data has to be transmitted
 *                           and send the data
 *
 *    Input(s)             : pu1DataBuf - Data buffer
 *                           u4PktSize  - packet size
 *                           VlanInfo   - Vlan Related Information (VlanId,
 *                                        Member Portlist, PktType etc..)           
 *
 *    Output(s)           : None 
 *    Global Variables Referred : None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *****************************************************************************/

INT4
CfaL3VlanIntfWrite (UINT1 *pu1DataBuf, UINT4 u4PktSize, tCfaVlanInfo VlanInfo)
{
    UINT4               u4IfIndex;
    INT4                i4RetVal;
    BOOL1               bResult;
#ifdef WTP_WANTED
    UINT1               u1IfType;
#endif

    if (VlanInfo.u2PktType == CFA_NP_KNOWN_UCAST_PKT)
    {
        i4RetVal
            =
            CfaTxIvrPktOnPort (pu1DataBuf, (INT4) u4PktSize, VlanInfo.u2VlanId,
                               (UINT4) VlanInfo.unPortInfo.TxUcastPort.u2TxPort,
                               (BOOL1) VlanInfo.unPortInfo.TxUcastPort.u1Tag);
        return i4RetVal;
    }

    /* 
     * Unknown Unicast, Broadcast, Multicast packets
     * has to be transmitted on 
     * VlanInfo.unPortInfo.TxPorts.TagPorts are tagged ports
     * VlanInfo.unPortInfo.TxPorts.UnTagPorts are untagged ports
     *
     */

    for (u4IfIndex = 1; u4IfIndex <= CFA_MAX_PSW_IF_INDEX; u4IfIndex++)
    {
        /*
         * Tagged Port list and Untagged Port list are mutually exclusive
         * here. This is not as it is in Vlan Egress ports and Vlan Untagged
         * ports. So transmit on both Untagged and Tagged ports.
         */
        OSIX_BITLIST_IS_BIT_SET ((*VlanInfo.unPortInfo.TxPorts.pTagPorts),
                                 u4IfIndex, sizeof (tPortList), bResult);

        if (bResult == OSIX_TRUE)
        {

#ifdef WTP_WANTED
            if (CfaGetIfType (u4IfIndex, &u1IfType) != CFA_SUCCESS)
            {
                CFA_DBG (ALL_FAILURE_TRC, CFA_L2,
                         "CfaGetIfType Returned Error\r\n");
                continue;
            }

            if (u1IfType == CFA_WLAN_RADIO)
            {
                if (CfaTxToWssWlanPorts (pu1DataBuf, u4IfIndex, u4PktSize)
                    != CFA_SUCCESS)
                {
                    CFA_DBG (ALL_FAILURE_TRC, CFA_L2,
                             "CfaTxToWssWlanPorts FAILED\n");
                }
                continue;
            }
#endif

            CfaTxIvrPktOnPort (pu1DataBuf, (INT4) u4PktSize, VlanInfo.u2VlanId,
                               u4IfIndex, CFA_NP_TAGGED);
        }

        OSIX_BITLIST_IS_BIT_SET ((*VlanInfo.unPortInfo.TxPorts.pUnTagPorts),
                                 u4IfIndex, sizeof (tPortList), bResult);
        if (bResult == OSIX_TRUE)
        {

#ifdef WTP_WANTED
            CfaGetIfType (u4IfIndex, &u1IfType);
            if (u1IfType == CFA_WLAN_RADIO)
            {
                if (CfaTxToWssWlanPorts (pu1DataBuf, u4IfIndex, u4PktSize)
                    != CFA_SUCCESS)
                {
                    CFA_DBG (ALL_FAILURE_TRC, CFA_L2,
                             "CfaTxToWssWlanPorts FAILED\n");
                }
                continue;
            }
#endif
            CfaTxIvrPktOnPort (pu1DataBuf, (INT4) u4PktSize, VlanInfo.u2VlanId,
                               u4IfIndex, CFA_NP_UNTAGGED);
        }

        /* Aviod looping for other logical interfaces present 
         * after (Physical + port chanel) > && < (Min pseudo wire index) */
        if (u4IfIndex == BRG_MAX_PHY_PLUS_LOG_PORTS)
        {
            u4IfIndex = (CFA_MIN_PSW_IF_INDEX) - 1;
        }
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaTxIvrPktOnPort 
 *
 *    Description          : This function is used to send the packet on the 
 *                           given port. It tags/Untags the packet based on
 *                           bIsOutTag. Converts the Given ifIndex to Physical
 *                           IfIndex if it is Port-Channel Index
 *                            
 *    Input(s)             : pu1DataBuf - Data buffer
 *                           u4PktSize   - packet size
 *                           VlanId      - Vlan ID    
 *                           u4PktSize   - packet size
 *                           u4IfIndex   - Port on which the packet is to be sent
 *                           bIsOutTag   - Whether to tag or not.
 *
 *    Output(s)           : None 
 *    Global Variables Referred : None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *****************************************************************************/
INT4
CfaTxIvrPktOnPort (UINT1 *pu1DataBuf, INT4 u4PktSize,
                   tVlanId VlanId, UINT4 u4IfIndex, BOOL1 bIsOutTag)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT1              *pu1TxBuf;
    UINT4               u4CurrentPktSize;
    UINT4               u4Mtu = 0;
    BOOL1               bPktTag = TRUE;
    UINT1               u1IfType = 0;
    UINT2               u2PhyPort;
#ifdef VLAN_WANTED
    UINT1               u1Priority = 7;    /* 7 - is Highest Priority */
#endif
    u4CurrentPktSize = (UINT4) u4PktSize;

    if (!((pu1DataBuf[12] == 0x81) && (pu1DataBuf[13] == 0x00)))
    {
        /* Frame is Untagged */
        bPktTag = FALSE;

        if (bIsOutTag == CFA_NP_TAGGED)
        {
            u4CurrentPktSize += VLAN_TAG_LEN;
        }
    }

    if ((pu1TxBuf = MemAllocMemBlk (gCfaPktPoolId)) == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaGddWrite - " "Malloc Failed -  FAILURE.\n");

        return CFA_FAILURE;
    }

    if (bIsOutTag == CFA_NP_TAGGED)
    {
        if (bPktTag == TRUE)
        {
            MEMCPY (pu1TxBuf, pu1DataBuf, u4PktSize);
        }
        else
        {
            MEMCPY (pu1TxBuf, pu1DataBuf, VLAN_TAG_OFFSET);
#ifdef VLAN_WANTED
            if (VlanTagOutFrameForIvr (pu1TxBuf + VLAN_TAG_OFFSET, u4IfIndex,
                                       VlanId, u1Priority) == VLAN_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddWrite - "
                         "Vlan Tagging Failed -  FAILURE.\n");
                MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1TxBuf);
                return CFA_FAILURE;
            }
#else
            UNUSED_PARAM (VlanId);
#endif

            MEMCPY (pu1TxBuf + VLAN_TAGGED_HEADER_SIZE,
                    pu1DataBuf + VLAN_TAG_OFFSET, u4PktSize - VLAN_TAG_OFFSET);
        }
    }
    else
    {
        if (bPktTag == TRUE)
        {
            MEMCPY (pu1TxBuf, pu1DataBuf, VLAN_TAG_OFFSET);

            MEMCPY (pu1TxBuf + VLAN_TAG_OFFSET,
                    pu1DataBuf + VLAN_TAGGED_HEADER_SIZE,
                    u4PktSize - VLAN_TAGGED_HEADER_SIZE);

            u4CurrentPktSize = u4CurrentPktSize - VLAN_TAG_LEN;
        }
        else
        {
            MEMCPY (pu1TxBuf, pu1DataBuf, u4PktSize);
        }
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

    if (u1IfType == CFA_LAGG)
    {
        /* Get the physical port corresponding to the port-channel */
        if (L2IwfGetPotentialTxPortForAgg ((UINT2) u4IfIndex, &u2PhyPort)
            == L2IWF_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Cannot obtain Potential LA port for transmission.\n");

            MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1TxBuf);

            return CFA_FAILURE;
        }

        u4IfIndex = (UINT4) u2PhyPort;
    }

#ifdef MPLS_WANTED
    if (u1IfType == CFA_PSEUDO_WIRE)
    {
        CfaGetIfMtu (u4IfIndex, &u4Mtu);
        if (u4CurrentPktSize > u4Mtu)
        {
            /* CHECK FOR NEGATIVE SCENORIO ON PW:-
             * PW is created as an interface. 
             * It is mapped with MPLS pseudo wire.
             * For MPLS psuedo wire- to carry the traffic,
             * it will have underlying IVR interface.
             * If the PW interface created in CFA is part
             * of THIS IVR interface, then it keeps on 
             * updating the same labels as specified in the
             * below flow. 
             * 1.[MPLS]  MPLS PW (after adding labels) -->
             * 2.[CFA] IVR (to tx the pkt) -->
             * 3.[CFA] IVR gets its ports (PW interface) -> 
             * 4.[CFA] Gives pkt to MPLS to add MPLS labels
             * Now, it goes to step (1) again and this keeps
             * on adding labels and crash happens.
             * To avoid this negative scenorio, this check is
             * added */

            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Discarding packet of PW which is a member port of IVR."
                     "This IVR is used to carry MPLS PW traffic.\r\n");
            CFA_IF_SET_OUT_DISCARD (u4IfIndex);

            MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1TxBuf);
            return CFA_FAILURE;
        }
        pBuf = CRU_BUF_Allocate_MsgBufChain (u4CurrentPktSize, 0);
        if (pBuf == NULL)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "CRU buffer allocation is failed while sending the"
                     "packet on Pseudo wire interface\r\n");
            MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1TxBuf);
            return CFA_FAILURE;
        }
        CRU_BUF_Copy_OverBufChain (pBuf, pu1TxBuf, 0, u4CurrentPktSize);

        MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1TxBuf);
        if (CfaIwfMplsProcessOutGoingL2Pkt (pBuf, u4IfIndex, 0,
                                            CFA_LINK_UCAST) == MPLS_SUCCESS)
        {
            return CFA_SUCCESS;
        }
        /* No need to free the CRU buff MPLS will take care of Releasing it */
        return CFA_FAILURE;
    }
#else
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4Mtu);
#endif

    if (EoamMuxParActOnTx (u4IfIndex, NULL) == EOAM_DISCARD)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaTxIvrPktOnPort: Mux Action DISCARD. Frame discarded.\n");
        MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1TxBuf);
        return (CFA_SUCCESS);
    }

    /* Transmit the packet */

    /* invoke the driver call based on the type of port - use of function pointer */
    if ((CFA_GDD_WRITE_FNPTR (u4IfIndex))
        (pu1TxBuf, u4IfIndex, u4CurrentPktSize) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaGddWrite - "
                 "unsuccessful Driver Write -  FAILURE.\n");
    }

    MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1TxBuf);

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       :CfaUtilTxL2PktOnPortList
 *
 *    Description         :This routine calls the CfaHandlePktFromL2 ()
 *                         routine for the given Frame for each port in the 
 *                         given port list.
 *
 *    Input(s)            : pTxPktOnPortListMsg - Structure containing the 
 *                                                frame and the port list
 *
 *    Output(s)           : None
 *    
 *    Global Variables Referred : None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Returns            : CFA_SUCCESS/ CFA_FAILURE.
 *****************************************************************************/
INT4
CfaUtilTxL2PktOnPortList (tCfaTxPktOnPortListMsg * pTxPktOnPortListMsg)
{
    INT4                i4RetVal = CFA_SUCCESS;
    UINT4               u4PktSize;
    UINT2               u2Protocol;
    UINT1               u1EncapType;
    UINT1               au1DestHwAddr[CFA_ENET_ADDR_LEN];
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag;
    UINT2               u2Port;
    tCRU_BUF_CHAIN_HEADER *pDupBuf;
    tCRU_BUF_CHAIN_HEADER *pBuf;

    pBuf = pTxPktOnPortListMsg->pFrame;
    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    u2Protocol = CFA_GET_PROTOCOL (pBuf);
    u1EncapType = CFA_GET_ENCAP_TYPE (pBuf);

    MEMCPY (au1DestHwAddr, (CRU_BMC_Get_DataPointer (pBuf)), CFA_ENET_ADDR_LEN);

    for (u2ByteInd = 0; u2ByteInd < BRG_PORT_LIST_SIZE; u2ByteInd++)
    {
        if (pTxPktOnPortListMsg->PortList[u2ByteInd] == 0)
        {
            continue;
        }

        u1PortFlag = pTxPktOnPortListMsg->PortList[u2ByteInd];

        for (u2BitIndex = 0;
             ((u2BitIndex < BITS_PER_BYTE) && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & 0x80) != 0)
            {
                u2Port = (UINT2) ((u2ByteInd * BITS_PER_BYTE) + u2BitIndex + 1);

                pDupBuf = CRU_BUF_Duplicate_BufChain (pBuf);

                if (pDupBuf == NULL)
                {
                    CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                             "In CfaMain TxPktOnPortlist duplicating"
                             "buffer failed.\n");
                    i4RetVal = CFA_FAILURE;
                    continue;
                }

                if (CfaHandlePktFromL2 (pDupBuf, u2Port, u4PktSize, u2Protocol,
                                        au1DestHwAddr, u1EncapType)
                    != CFA_SUCCESS)
                {
                    CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                             "In CfaMain TxPktOnPortlist packet dispatch "
                             "to IWF failed.\n");

                    i4RetVal = CFA_FAILURE;
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }

    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaGetIfaceInfo                                    */
/*                                                                           */
/*     DESCRIPTION      : This function returns interface info from mode name*/
/*                                                                           */
/*     INPUT            : pModName - Mode name                               */
/*                                                                           */
/*     OUTPUT           : pIfaceInfo- Interface info                         */
/*                                                                           */
/*     RETURNS          : TRUE/FALSE                                         */
/*                                                                           */
/*****************************************************************************/
/* SUBTYPE changes */
INT4
CfaGetIfaceInfo (UINT1 *pModName, tCfaIfaceInfo * pIfaceInfo)
{
    UINT4               u4Count = 0;
    UINT4               u4SubCount = 0;

    for (u4Count = 0; u4Count < CLI_MAX_IFTYPES; ++u4Count)
    {
        for (u4SubCount = 0; u4SubCount < CLI_MAX_IFSUBTYPES; ++u4SubCount)
        {
            if (STRLEN (asCfaIfaces[u4Count][u4SubCount].au1IfName) == 0)
            {
                continue;
            }
            if (CLI_STRSTR (pModName,
                            asCfaIfaces[u4Count][u4SubCount].au1IfName) != NULL)
            {
                MEMCPY (pIfaceInfo, &asCfaIfaces[u4Count][u4SubCount],
                        sizeof (tCfaIfaceInfo));
                return (TRUE);
            }
        }
    }
    return (FALSE);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaGetIfCountersFromSoftware                       */
/*                                                                           */
/*     DESCRIPTION      : This function returns interface statistics         */
/*                                                                           */
/*     INPUT            : u4IfIndex  - Interface Index                       */
/*                                                                           */
/*     OUTPUT           : pIfCounter- Pointer to IfCounters structure        */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS/CFA_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaGetIfCountersFromSoftware (UINT4 u4IfIndex, tIfCountersStruct * pIfCounter)
{
    CFA_DS_LOCK ();
    if (CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_TRUE)
    {
        pIfCounter->u4InOctets = CFA_GET_IF_IN_OCTETS (u4IfIndex);

        pIfCounter->u4InUcastPkts = CFA_GET_IF_IN_UCAST (u4IfIndex);

        pIfCounter->u4InMulticastPkts = CFA_GET_IF_IN_MCAST (u4IfIndex);

        pIfCounter->u4InBroadcastPkts = CFA_GET_IF_IN_BCAST (u4IfIndex);

        pIfCounter->u4HighInOctets = CFA_GET_IF_IN_HC_OCTETS (u4IfIndex);

        pIfCounter->u4InDiscards = CFA_GET_IF_IN_DISCARDS (u4IfIndex);

        pIfCounter->u4InErrors = CFA_GET_IF_IN_ERR (u4IfIndex);

        pIfCounter->u4InUnknownProtos = CFA_GET_IF_IN_UNKNOWNPROTOS (u4IfIndex);

        pIfCounter->u4OutOctets = CFA_GET_IF_OUT_OCTETS (u4IfIndex);

        pIfCounter->u4OutUcastPkts = CFA_GET_IF_OUT_UCAST (u4IfIndex);

        pIfCounter->u4OutMulticastPkts = CFA_GET_IF_OUT_MCAST (u4IfIndex);

        pIfCounter->u4OutBroadcastPkts = CFA_GET_IF_OUT_BCAST (u4IfIndex);

        pIfCounter->u4HighOutOctets = CFA_GET_IF_OUT_HC_OCTETS (u4IfIndex);

        pIfCounter->u4OutDiscards = CFA_GET_IF_OUT_DISCARDS (u4IfIndex);

        pIfCounter->u4OutErrors = CFA_GET_IF_OUT_ERR (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    CFA_DS_UNLOCK ();
    return (CFA_FAILURE);
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaGetIfCountersFromHardware                       */
/*                                                                           */
/*     DESCRIPTION      : This function gets the counters from H/W and fills */
/*                        in the Interface Structure                         */
/*                                                                           */
/*     INPUT            : u4IfIndex-Interface Index                          */
/*                                                                           */
/*     OUTPUT           : pIfCounters - Pointer to IfCounter Stucture        */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
CfaGetIfCountersFromHardware (UINT4 u4IfIndex, tIfCountersStruct * pIfCounter)
{
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_IN_OCTETS, &(pIfCounter->u4InOctets));
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_IN_UCAST_PKTS,
                    &(pIfCounter->u4InUcastPkts));
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_IN_MCAST_PKTS,
                    &(pIfCounter->u4InMulticastPkts));
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_IN_BCAST_PKTS,
                    &(pIfCounter->u4InBroadcastPkts));
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_HC_IN_OCTETS,
                    &(pIfCounter->u4HighInOctets));
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_IN_DISCARDS,
                    &(pIfCounter->u4InDiscards));
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_IN_ERRORS, &(pIfCounter->u4InErrors));
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_IN_UNKNOWN_PROTOS,
                    &(pIfCounter->u4InUnknownProtos));
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_OUT_OCTETS,
                    &(pIfCounter->u4OutOctets));
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_OUT_UCAST_PKTS,
                    &(pIfCounter->u4OutUcastPkts));
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_OUT_MCAST_PKTS,
                    &(pIfCounter->u4OutMulticastPkts));
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_OUT_BCAST_PKTS,
                    &(pIfCounter->u4OutBroadcastPkts));
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_HC_OUT_OCTETS,
                    &(pIfCounter->u4HighOutOctets));
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_OUT_DISCARDS,
                    &(pIfCounter->u4OutDiscards));
    CfaFsHwGetStat (u4IfIndex, NP_STAT_IF_OUT_ERRORS,
                    &(pIfCounter->u4OutErrors));
    return;
}
#endif

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaUtilUpdateLinkStatus                            */
/*                                                                           */
/*     DESCRIPTION      : This function retrieves the link status of an      */
/*                        interface and will update higher layers if there   */
/*                        is any change in the OperStatus of the interface.  */
/*                                                                           */
/*     INPUT            : u4StartIfIndex - Start interface index.            */
/*                        u4EndIfIndex   - End interface index.              */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
CfaUtilUpdateLinkStatus (UINT4 u4StartIfIndex, UINT4 u4EndIfIndex)
{
    UINT4               u4IfIndex;
    UINT1               u1OperStatus;
    UINT1               u1LinkStatus;
    UINT1               u1OldOperStatus = CFA_IF_DOWN;

    u4IfIndex = u4StartIfIndex;
    CFA_CDB_SCAN_WITH_LOCK (u4IfIndex, u4EndIfIndex, CFA_ALL_IFTYPE)
    {
        /* Flexible ifIndex changes 
         */
        if (u4IfIndex > u4EndIfIndex)
        {
            break;
        }

        if (CfaGetIfOperStatus (u4IfIndex, &u1OldOperStatus) == CFA_SUCCESS)
        {
            u1LinkStatus = CfaGddGetLinkStatus (u4IfIndex);

            if ((CFA_IF_ADMIN (u4IfIndex) == CFA_IF_UP)
                && (u1LinkStatus == CFA_IF_UP))
            {
                u1OperStatus = CFA_IF_UP;
            }
            else
            {
                u1OperStatus = CFA_IF_DOWN;
            }

            if (u1OperStatus != u1OldOperStatus)
            {
                CfaIfmHandleInterfaceStatusChange (u4IfIndex,
                                                   CFA_IF_ADMIN
                                                   (u4IfIndex),
                                                   u1OperStatus,
                                                   CFA_IF_LINK_STATUS_CHANGE);
            }
        }
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaUtilClearInterfacesCounters                     */
/*                                                                           */
/*     DESCRIPTION      : This function clears interface counters            */
/*                                                                           */
/*     INPUT            : i4Index - Index of the interface                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CfaUtilClearInterfacesCounters (INT4 i4Index)
{

    UINT1               u1Flag = 1;
    UINT1               u1BrgPortType = 0;
    INT4                i4PrevIndex = 0;
    INT4                i4RetVal = 0;
#ifdef NPAPI_WANTED
    UINT2               au2ConfPorts[L2IWF_MAX_PORTS_PER_CONTEXT];
    UINT2               u2NumPorts;
    INT4                i4TempIndex = 1;

    MEMSET (au2ConfPorts, 0, sizeof (au2ConfPorts));
#endif

    CfaGetIfBrgPortType ((UINT4) i4Index, &u1BrgPortType);

    if ((i4Index != 0) && ((CfaIsSispInterface ((UINT4) i4Index) == CFA_TRUE) ||
                           (u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)))
    {
        /* Operation not supported over this interface index
         * Interface Counters are not supported for SChannel Interface Index */
        return CFA_SUCCESS;
    }

    if (i4Index == 0)
    {
        if (nmhGetFirstIndexIfTable (&i4Index) == SNMP_FAILURE)
        {
            return CFA_SUCCESS;
        }
        i4PrevIndex = i4Index;
        u1Flag = 0;
    }

    do
    {
#ifdef NPAPI_WANTED

        if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
        {
            if (CfaIsLaggInterface (i4Index) == CFA_TRUE)
            {
                u2NumPorts = 0;

                if (L2IwfGetConfiguredPortsForPortChannel ((UINT2) i4Index,
                                                           au2ConfPorts,
                                                           &u2NumPorts)
                    == L2IWF_FAILURE)
                {
                    return CFA_FAILURE;
                }
                if (u2NumPorts > L2IWF_MAX_PORTS_PER_CONTEXT)
                {
                    return CFA_FAILURE;
                }

                for (i4TempIndex = 0; i4TempIndex < u2NumPorts; i4TempIndex++)
                {
                    CfaFsCfaHwClearStats (au2ConfPorts[i4TempIndex]);
#ifdef RMON_WANTED
                    if (gRmonEnableStatusFlag == CFA_TRUE)
                    {
                        if (FsRmonHwSetEtherStatsTable
                            (au2ConfPorts[i4TempIndex], FNP_TRUE)
                            != FNP_SUCCESS)
                        {
                            return CFA_FAILURE;
                        }
                    }
#endif
                }
#ifdef NPSIM_WANTED
                CfaFsCfaHwClearStats ((UINT4) i4Index);
#endif
            }
            else
            {
                CfaFsCfaHwClearStats ((UINT4) i4Index);
#ifdef RMON_WANTED
                if (gRmonEnableStatusFlag == CFA_TRUE)
                {
                    if (FsRmonHwSetEtherStatsTable
                        (i4Index, FNP_TRUE) != FNP_SUCCESS)
                    {
                        return CLI_FAILURE;
                    }
                }
#endif
            }

        }
#else
        CFA_CDB_IF_COUNTERS ((UINT4) i4Index).u4InOctets = 0;
        CFA_CDB_IF_COUNTERS ((UINT4) i4Index).u4HighInOctets = 0;
        CFA_CDB_IF_COUNTERS ((UINT4) i4Index).u4InUcastPkts = 0;
        CFA_CDB_IF_COUNTERS ((UINT4) i4Index).u4InMulticastPkts = 0;
        CFA_CDB_IF_COUNTERS ((UINT4) i4Index).u4InBroadcastPkts = 0;
        CFA_CDB_IF_COUNTERS ((UINT4) i4Index).u4InDiscards = 0;
        CFA_CDB_IF_COUNTERS ((UINT4) i4Index).u4InErrors = 0;
        CFA_CDB_IF_COUNTERS ((UINT4) i4Index).u4InUnknownProtos = 0;
        CFA_CDB_IF_COUNTERS ((UINT4) i4Index).u4OutOctets = 0;
        CFA_CDB_IF_COUNTERS ((UINT4) i4Index).u4HighOutOctets = 0;
        CFA_CDB_IF_COUNTERS ((UINT4) i4Index).u4OutUcastPkts = 0;
        CFA_CDB_IF_COUNTERS ((UINT4) i4Index).u4OutMulticastPkts = 0;
        CFA_CDB_IF_COUNTERS ((UINT4) i4Index).u4OutBroadcastPkts = 0;
        CFA_CDB_IF_COUNTERS ((UINT4) i4Index).u4OutDiscards = 0;
        CFA_CDB_IF_COUNTERS ((UINT4) i4Index).u4OutErrors = 0;
#endif
        /* For Pseudowire interface also clear the statistics */
        if (i4Index == BRG_MAX_PHY_PLUS_LOG_PORTS)
        {
            i4PrevIndex = CFA_MIN_PSW_IF_INDEX - 1;
        }
        i4RetVal = nmhGetNextIndexIfTable (i4PrevIndex, &i4Index);
        i4PrevIndex = i4Index;
    }
    while ((u1Flag == 0) && ((i4Index <= CFA_MAX_NVE_IF_INDEX)
                             || (CfaIsL3SubIfIndex ((UINT4) i4Index) ==
                                 CFA_TRUE)) && (i4RetVal != SNMP_FAILURE));

    return (CFA_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCheckAttacks                                    */
/*                                                                           */
/*     DESCRIPTION      : This function fills the ICMP header (if present)   */
/*                        and higher layer info (tcp or udp) from the packet.*/
/*                        It checks for various DOS attacks and will return  */
/*                        return success if threat is not present. Otherwise */
/*                        returns the type of attack detected.               */
/*                                                                           */
/*     INPUT            : pBuf - Incoming Packet                             */
/*                        pIpHdr - IP Header                                 */
/*                        u1HdrLen - IP Header len                           */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS / Failure indicating attack type       */
/*                                                                           */
/*****************************************************************************/
UINT4
CfaCheckAttacks (tCRU_BUF_CHAIN_HEADER * pBuf, t_IP_HEADER * pIpHdr,
                 UINT1 u1HdrLen)
{
    UINT4               u4Attack;
    tHLInfo             HLInfo;
    tIcmpInfo           IcmpHdr;
    UINT2               u2Offset = 0xFFFF;    /* Invalid offset */

    MEMSET (&HLInfo, 0, sizeof (tHLInfo));
    MEMSET (&IcmpHdr, 0, sizeof (tIcmpInfo));

    u2Offset = (UINT2) ((pIpHdr->u2Fl_offs) << IP_FLAGS_BITS);
    /* Update HLInfo structure if the packet is UDP or TCP */
    if ((u2Offset == 0) &&
        (pIpHdr->u1Proto == CFA_TCP || pIpHdr->u1Proto == CFA_UDP))
    {
        CfaUpdateHLInfoFromPkt (pBuf, &HLInfo, pIpHdr->u1Proto, u1HdrLen);
    }

    /* Updation of Icmp type and Code in the ICMP info struct  */
    if (pIpHdr->u1Proto == CFA_ICMP)
    {
        CfaUpdateIcmpInfoFromPkt (pBuf, &IcmpHdr, u1HdrLen);
    }

    u4Attack = CfaCheckDOSAttacks (pBuf, pIpHdr, &HLInfo, &IcmpHdr, u1HdrLen);
    return u4Attack;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaUpdateHLInfoFromPkt                             */
/*                                                                           */
/*     DESCRIPTION      : Updates the Higher layer TCP/UDP information       */
/*                        like Src and Dest port and If the packet is for    */
/*                        TCP, updates the Ack and Rst bits.                 */
/*                                                                           */
/*     INPUT            : pBuf     -- Pointer to the packet                  */
/*                        pHLInfo  -- Pointer to the HLInfo                  */
/*                        u1Proto  -- Protocol ( UDP or TCP )                */
/*                        u1IpHeadLen - IP header length                     */
/*                                                                           */
/*     OUTPUT           : pHLInfo  -- Fills this structure                   */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
CfaUpdateHLInfoFromPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tHLInfo * pHLInfo,
                        UINT1 u1Proto, UINT1 u1IpHeadLen)
{
    UINT1               u1TcpCodeBit;

    /* Get the Source port  */
    CFA_GET_2_BYTE (pBuf, u1IpHeadLen, pHLInfo->u2SrcPort);

    /* Get the Destination port  */
    CFA_GET_2_BYTE (pBuf, (u1IpHeadLen + 2), pHLInfo->u2DestPort);

    if (u1Proto == CFA_TCP)
    {
        /* Get the TCP Ack, Rst Bit and Fin Bit */
        CFA_GET_1_BYTE (pBuf, (u1IpHeadLen + CFA_TCP_CODE_BIT_OFFSET),
                        u1TcpCodeBit);

        /* get the Rst bit from the code bit and right shift by two times */
        pHLInfo->u1Rst = (UINT1) ((u1TcpCodeBit & CFA_TCP_RST_MASK) >> 2);

        /* get the Ack bit from the code bit and right shift by Four times */
        pHLInfo->u1Ack = (UINT1) ((u1TcpCodeBit & CFA_TCP_ACK_MASK) >> 4);

        /* get the Fin bit from the code bit */
        pHLInfo->u1Fin = (UINT1) (u1TcpCodeBit & CFA_TCP_FIN_MASK);

        /* Get the Syn bit from the code bit */
        pHLInfo->u1Syn = (UINT1) ((u1TcpCodeBit & CFA_TCP_SYN_MASK) >> 1);

        CFA_DBG1 (CFA_TRC_ENET_PKT_DUMP, CFA_FFM, "\n SrcPort = %d \n",
                  pHLInfo->u2SrcPort);
        CFA_DBG1 (CFA_TRC_ENET_PKT_DUMP, CFA_FFM, "\n DestPort = %d \n",
                  pHLInfo->u2DestPort);
        CFA_DBG1 (CFA_TRC_ENET_PKT_DUMP, CFA_FFM, "\n Rst Bit = %d \n",
                  pHLInfo->u1Rst);
        CFA_DBG1 (CFA_TRC_ENET_PKT_DUMP, CFA_FFM, "\n Ack Bit = %d \n",
                  pHLInfo->u1Ack);
        CFA_DBG1 (CFA_TRC_ENET_PKT_DUMP, CFA_FFM, "\n Fin Bit = %d \n",
                  pHLInfo->u1Fin);
        CFA_DBG1 (CFA_TRC_ENET_PKT_DUMP, CFA_FFM, "\n Syn Bit = %d \n",
                  pHLInfo->u1Syn);

    }
    else
    {                            /* u1Proto is UDP */

        /* the following fields are not used, if the packet is UDP and 
         * initialised to the Zero.
         */
        pHLInfo->u1Rst = 0;
        pHLInfo->u1Ack = 0;
        pHLInfo->u1Fin = 0;
        pHLInfo->u1Syn = 0;

        CFA_DBG1 (CFA_TRC_ENET_PKT_DUMP, CFA_FFM, "\n SrcPort = %d \n",
                  pHLInfo->u2SrcPort);
        CFA_DBG1 (CFA_TRC_ENET_PKT_DUMP, CFA_FFM, "\n Destination Port = %d \n",
                  pHLInfo->u2DestPort);
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : CfaUpdateIcmpInfoFromPkt                         */
/*                                                                          */
/*    Description        : Updates the IcmpInfo srtucture with pkt info     */
/*                                                                          */
/*    Input(s)           : pBuf     -- Pointer to the packet                */
/*                         pIcmp    -- Pointer to the IcmpInfo              */
/*                         u1IpHeadLen - Value of the IP header length      */
/*                                                                          */
/*    Output(s)          : Fills the pIcmp structure                        */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
CfaUpdateIcmpInfoFromPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                          tIcmpInfo * pIcmp, UINT1 u1IpHeadLen)
{
    /* Get the Icmp Type from the pkt */
    CFA_GET_1_BYTE (pBuf, u1IpHeadLen, pIcmp->u1Type);
    CFA_DBG1 (CFA_TRC_ENET_PKT_DUMP, CFA_FFM, "\n Icmp type = %d \n",
              pIcmp->u1Type);

    /* Get the Icmp code from the pkt */
    CFA_GET_1_BYTE (pBuf, (u1IpHeadLen + 1), pIcmp->u1Code);
    CFA_DBG1 (CFA_TRC_ENET_PKT_DUMP, CFA_FFM, "\n Icmp code = %d \n",
              pIcmp->u1Code);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCheckDOSAttacks                                 */
/*                                                                           */
/*     DESCRIPTION      : This function checks the following DOS attacks.    */
/*                        1. Land attack 2. Smurf attack 3. Unknown protocol */
/*                        4. ICMP Router advertisements 5.Nuke attack        */
/*                        6. TCP short header attack. 7. TCP X-MAS scan      */
/*                        8. TCP NULL scan 9. W2k Domain controller attack   */
/*                        10.Snork attack 11. Ascend attack.                 */
/*                        12. UDP loop back attack.                          */
/*                        13. UDP short header attack.                       */
/*                                                                           */
/*     INPUT            : pBuf       - Incoming Packet                       */
/*                        pIpHdr     - IP Header                             */
/*                        pHLInfo    - Tcp or Udp informations               */
/*                        pIcmpHdr   - Icmp header informations              */
/*                        u1HdrLen   - IP Header len                         */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS / Failure indicating attack type       */
/*                                                                           */
/*****************************************************************************/
UINT4
CfaCheckDOSAttacks (tCRU_BUF_CHAIN_HEADER * pBuf, t_IP_HEADER * pIpHdr,
                    tHLInfo * pHLInfo, tIcmpInfo * pIcmpHdr, UINT1 u1HdrLen)
{
    UINT1               u1TransportLength;
    UINT1               u1Urg;
    UINT1               u1Push;
    UINT1               u1TcpCodeBit;

    /* Check for Land Attack */
    if (pIpHdr->u4Src == pIpHdr->u4Dest)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_FFM, "\n Land Attack detected \n");
        return CFA_LAND_ATTACK;
    }

    /* Dropping ICMP Router Advertisement Messages */
    if (pIcmpHdr->u1Type == CFA_IRDP_ADVERTISEMENT)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_FFM, "\n IRDP Advertisement detected \n");
    }

    if (pIpHdr->u1Proto == CFA_TCP)
    {
        /* Inspecting Win Nuke Attack:
         * Port Number 139 is NETBIOS Port
         */
        if (pHLInfo->u2DestPort == 139)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_FFM, "\n Win Nuke Attack detected \n");
            return CFA_WINNUKE_ATTACK;
        }

        /* TCP short header attack
         * prevention 
         */
        u1TransportLength = CfaGetTransportHeaderLength (pBuf,
                                                         pIpHdr, u1HdrLen);
        if (u1TransportLength < 20)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_FFM,
                     "\n TCP short header Attack detected \n");
            return CFA_TCP_SHTHDR;
        }

        /* TCP X-MAS scan attack check
         */

        CFA_GET_1_BYTE (pBuf,
                        (u1HdrLen + CFA_TCP_CODE_BIT_OFFSET), u1TcpCodeBit);

        /* Getting the urgent bit value */
        u1Urg = (UINT1) ((u1TcpCodeBit & CFA_TCP_URG_MASK) >> 5);

        /* Getting the Push bit value */
        u1Push = (UINT1) ((u1TcpCodeBit & CFA_TCP_PUSH_MASK) >> 3);

        /* In Tcp X-mas attack and Tcp Null scan attack
         * ignore the sequence number for validation.
         * If the other flags are matching failure conditions,
         * return failure
         */
        if ((pHLInfo->u1Fin) && (u1Urg) && (u1Push))
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_FFM, "\n TCP X-MAS Attack detected \n");
            return CFA_TCP_XMAS_ATTACK;
        }

        /* TCP NULL scan attack check
         */
        if ((pHLInfo->u1Fin == 0) &&
            (u1Urg == 0) &&
            (u1Push == 0) &&
            (pHLInfo->u1Rst == 0) &&
            (pHLInfo->u1Ack == 0) && (pHLInfo->u1Syn == 0))
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_FFM, "\n TCP NULL Attack detected \n");
            return CFA_TCP_NULL_ATTACK;
        }
    }
    else if (pIpHdr->u1Proto == CFA_UDP)
    {
        /* W2k Domain controller attack 
         * prevention, attack on port no 
         * 464
         */
        if (pHLInfo->u2DestPort == 464)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_FFM,
                     "\n W2k Domain controller Attack detected \n");
            return CFA_W2KDC_ATTACK;
        }

        /* Preventing Snork Attack */
        if ((pHLInfo->u2DestPort == 135) &&
            ((pHLInfo->u2SrcPort == 7) ||
             (pHLInfo->u2SrcPort == 19) || (pHLInfo->u2SrcPort == 135)))
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_FFM, "\n Snork Attack detected \n");
            return CFA_SNORK_ATTACK;
        }

        /* Prevention of Ascend attack:
         * This attack is specific to Ascend Routers.
         * Port No 9 is discard Port
         */
        if (pHLInfo->u2DestPort == 9)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_FFM, "\n Ascend Attack detected \n");
            return CFA_ASCEND_ATTACK;
        }

        /* UDP port loopback attack prevention:
         * This attack is generated when echo packets are exchanged 
         * endlessly between src port 7, 17 or 19 and dest port 7, 17 or 19.
         */

        if (((pHLInfo->u2SrcPort == 7) || (pHLInfo->u2SrcPort == 17) ||
             (pHLInfo->u2SrcPort == 19)) &&
            ((pHLInfo->u2DestPort == 7) || (pHLInfo->u2DestPort == 17) ||
             (pHLInfo->u2DestPort == 19)))
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_FFM,
                     "\n UDP port loopback attack detected \n");
            return CFA_UDP_LOOPBACK_ATTACK;
        }

        /* UDP Short Header attack check:
         * attack with UDP header with length less than 8 and it can
         take a value zero Because fragmented UDP packet will not have 
         the UDP header*/
        u1TransportLength = CfaGetTransportHeaderLength (pBuf,
                                                         pIpHdr, u1HdrLen);
        if ((u1TransportLength < 8) && (u1TransportLength != 0))
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_FFM, "\n UDP short header detected \n");
            return CFA_UDP_SHTHDR;
        }
    }
    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaValidateIpAddress                               */
/*     DESCRIPTION      : This function returns the type of the IpAdress it  */
/*                        belongs.i,e BroadCast,MultiCast etc.               */
/*     INPUT            : u4IpAddr - IpAddress for Validation                */
/*     OUTPUT           : The Type IpAdrress belongs.                        */
/*     RETURNS          : CFA_BCAST_ADDR     or                              */
/*                        CFA_MCAST_ADDR     or                              */
/*                        CFA_ZERO_NETW_ADDR or                              */
/*                        CFA_LOOPBACK_ADDR  or                              */
/*                        CFA_UCAST_ADDR     or                              */
/*                        CFA_INVALID_ADDR   or                              */
/*                        CFA_CLASSE_ADDR                                    */
/*****************************************************************************/
UINT4
CfaValidateIpAddress (UINT4 u4IpAddr)
{
    /*If Address is 255.255.255.255 */
    if ((u4IpAddr & 0xffffffff) == 0xffffffff)
    {
        return (CFA_BCAST_ADDR);
    }

    /*If Address is 127.x.x.x */
    if ((u4IpAddr & 0xff000000) == 0x7f000000)
    {
        return (CFA_LOOPBACK_ADDR);
    }

    /*If Address Multicast, i.e between 224.x.x.x and 239.x.x.x */
    if ((u4IpAddr & 0xf0000000) == 0xe0000000)
    {
        return (CFA_MCAST_ADDR);
    }

    /*If Address is 0.0.0.0 */
    if ((u4IpAddr & 0xffffffff) == 0)
    {
        return (CFA_ZERO_ADDR);
    }

    /*If Address is 0.x.x.x */
    if ((u4IpAddr & 0xff000000) == 0)
    {
        return (CFA_ZERO_NETW_ADDR);
    }

    /*If Address is CLASS-E, i.e betwwen 240.x.x.x and 254.x.x.x */
    if ((u4IpAddr & 0xf8000000) == 0xf0000000)
    {
        return (CFA_CLASSE_ADDR);
    }

    /*If Address 255.x.x.x */
    if ((u4IpAddr & 0xff000000) == 0xff000000)
    {
        return (CFA_INVALID_ADDR);
    }

    /*If Address CLASS-A and A.0.0.0 or A.255.255.255 */
    if ((u4IpAddr & 0x80000000) == 0)
    {
        if ((u4IpAddr & 0x00ffffff) == 0)
        {
            return (CFA_CLASS_NETADDR);
        }

        else if ((u4IpAddr & 0x00ffffff) == 0x00ffffff)
        {
            return (CFA_CLASS_BCASTADDR);
        }
    }
    /*If Address CLASS-B and B.0.0.0 or B.255.255.255 */
    if ((u4IpAddr & 0xc0000000) == 0x80000000)
    {
        if ((u4IpAddr & 0x0000ffff) == 0)
        {
            return (CFA_CLASS_NETADDR);
        }

        else if ((u4IpAddr & 0x0000ffff) == 0x0000ffff)
        {
            return (CFA_CLASS_BCASTADDR);
        }
    }

    /*If Address CLASS-C and C.0.0.0 or C.255.255.255 */
    if ((u4IpAddr & 0xe0000000) == 0xc0000000)
    {
        if ((u4IpAddr & 0x000000ff) == 0)
        {
            return (CFA_CLASS_NETADDR);
        }

        else if ((u4IpAddr & 0x000000ff) == 0x000000ff)
        {
            return (CFA_CLASS_BCASTADDR);
        }
    }

    return (CFA_UCAST_ADDR);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : CfaGetTransportHeaderLength                      */
/*                                                                          */
/*    Description        : Gets the header length of the Transport layer    */
/*                         (TCP/UDP).                                       */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the packet            */
/*                         pIpHdr       -- Pointer to IP Header             */
/*                         u1Headlen    -- IP Header Length                 */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : u1Length     -- Length of the Transport layer    */
/*                                         header.                          */
/****************************************************************************/
UINT1
CfaGetTransportHeaderLength (tCRU_BUF_CHAIN_HEADER * pBuf,
                             t_IP_HEADER * pIpHdr, UINT1 u1Headlen)
{
    UINT4               u4HeaderLenOffset = 0;
    UINT1               u1Length = 0;

    if (pIpHdr->u1Proto == CFA_TCP)
    {
        u4HeaderLenOffset = (UINT4) (CFA_TCP_LENGTH_OFFSET + u1Headlen);
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1Length,
                                   u4HeaderLenOffset, CFA_HEADER_LEN);
        u1Length = (UINT1) ((u1Length >> 4) & CFA_HEADER_LEN_MASK);
        u1Length = (UINT1) (u1Length * 4);
    }
    else                        /* udp */
    {
        if ((pIpHdr->u2Fl_offs & 0x1fff) == 0)
        {
            if ((pIpHdr->u2Totlen - u1Headlen) >= CFA_UDP_HEADER_LEN)
            {
                /* Valid length */
                u1Length = CFA_UDP_HEADER_LEN;
            }
        }
    }
    return u1Length;
}

/********************************************************************************
* Function Name    : CfaGetL3L4Info () 
*
* Description      : The function retrieves the Layer 3 and Layer 4 information.
*
* Input(s)         : pBuf         - Pointer to the received frame.
*
* Output(s)        : pu4SrcIp     - Pointer to the location where the Source  
*                                   IP address is to be stored.                
*                    pu1L3Proto   - Pointer to the location where the Protocol
*                                   field present in the IP header is to be    
*                                   stored.
*                    pu2L4DstPort - Pointer to the location where the Dst
*                                   Port field present in the Layer 4 header
*                                   is to be stored.
* Global Variables 
* Referred         : None.
*
* Global Variables 
* Modified         : None. 
*
* Exceptions or OS
* Error Handling   : None.
*
* Use of Recursion : None.
*
* Returns          : None.
*******************************************************************************/
VOID
CfaGetL3L4Info (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4SrcIp,
                UINT1 *pu1L3Proto, UINT2 *pu2L4DstPort)
{
    t_IP_HEADER        *pIpHdr = NULL;
    t_IP_HEADER         TmpIpHdr;
    UINT4               u4L4PktOffset = 0;
    UINT2               u2Port = 0;
    UINT2               u2Offset = 0xFFFF;    /* Invalid offset */

    pIpHdr =
        (t_IP_HEADER *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                                               IP_HDR_LEN);

    if (pIpHdr == NULL)
    {
        pIpHdr = &TmpIpHdr;

        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pIpHdr, 0, IP_HDR_LEN);
    }

    /* MEMCPY done here to avoid alignment issue in ARM processor */
    else
    {
        MEMCPY ((UINT1 *) (&TmpIpHdr), pIpHdr, IP_HDR_LEN);
        pIpHdr = &TmpIpHdr;
    }
    u2Offset = (UINT2) ((OSIX_NTOHS (pIpHdr->u2Fl_offs)) << IP_FLAGS_BITS);

    *pu4SrcIp = OSIX_NTOHL (pIpHdr->u4Src);
    *pu1L3Proto = pIpHdr->u1Proto;
    if (u2Offset != 0)
    {
        /*Since its fragmented packet L4 Header will not be here */
        *pu2L4DstPort = 0;
    }
    else
    {

        u4L4PktOffset = (UINT4) (pIpHdr->u1Ver_hdrlen & 0xF) * 4;

        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Port, u4L4PktOffset + 2,
                                   sizeof (UINT2));
        *pu2L4DstPort = OSIX_NTOHS (u2Port);
    }
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CfaEnetTxLoopbackTestFrame 
 *                                                                          
 *    DESCRIPTION      : This function is called by 
 *                       1. Fault Management Module to Tx Layer 2 Loopback 
 *                          test data when the remote is in loopback mode. 
 *                       2. EOAM Parser module to loopback the received 
 *                          loopback test data in local loopback mode.
 *
 *    INPUT            : u4IfIndex - Interface index 
 *                       pBuf - Pointer to CRU Buf that has loopback test data
 *                       u4PktSize - Test data size 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : CFA_SUCCESS - when tx is successful.
 *                       CFA_FAILURE - when tx fails.
 ****************************************************************************/
INT4
CfaEnetTxLoopbackTestFrame (UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER * pBuf,
                            UINT4 u4PktSize)
{
    tCfaIfInfo          CfaIfInfo;
    UINT1              *pu1DataBuf = NULL;
    INT4                i4RetVal = CFA_SUCCESS;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    /* Get EOAM params for the interface. */
    CfaGetIfInfo (u4IfIndex, &CfaIfInfo);

    /* This function transmits loopback test frames on ethernet interfaces */
    if (CfaIfInfo.u1IfType != CFA_ENET)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                 "Error in CfaEnetTxLoopbackTestFrame - "
                 "IfType not ENET -  FAILURE.\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return (CFA_FAILURE);
    }

    /* case 1: Loopback test data tx can be done only when the remote is in
     * loopback state. i.e., when u1RemoteLB = EOAM_ENABLED,
     * case 2: When a loopback test data is received in local loopback mode, 
     * the test data is looped back on the same interface. 
     * i.e., when u1ParserState = EOAM_LOOPBACK */
    if ((CfaIfInfo.EoamParams.u1RemoteLB != EOAM_ENABLED) &&
        (CfaIfInfo.EoamParams.u1ParState != EOAM_LOOPBACK))
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                 "Error in CfaEnetTxLoopbackTestFrame - "
                 "Not in Local/Remote Loopback mode-  FAILURE.\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return (CFA_FAILURE);
    }

    /* Remote is in Loopback mode and the MUX state is in 
     * FWD state. Allow only Loopback frames and discard
     * all other frames. */
    if ((pu1DataBuf = CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0, u4PktSize))
        == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                 "Error in CfaEnetTxLoopbackTestFrame - "
                 "CRU Buf pointer not linear -  FAILURE.\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return (CFA_FAILURE);
    }

    if ((CFA_GDD_WRITE_FNPTR (u4IfIndex))
        (pu1DataBuf, u4IfIndex, u4PktSize) != CFA_SUCCESS)
    {
        CFA_IF_SET_OUT_ERR (u4IfIndex);
        CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                 "Error in CfaEnetTxLoopbackTestFrame - "
                 "unsuccessful Driver Write -  FAILURE.\n");
        i4RetVal = CFA_FAILURE;
    }
    else
    {
        CFA_IF_SET_OUT_OCTETS (u4IfIndex, u4PktSize);
    }
    /* Free the CRU buffer */
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

    return (i4RetVal);
}

/***************************************************************************** 
 * 
 *    Function Name       : CfaMgmtVlanListValid 
 * 
 *    Description         : This function checks whether the VLAN list for the 
 *                          management L2 VLAN interface is valid. In case of 
 *                          Provider core bridge or provider edge bridge if 
 *                          any of the vlan in the list contains a customer 
 *                          port as a member port, then the corresponding L2 
 *                          interface should not made active. 
 * 
 *    Input(s)            : MgmtVlanList - List of MGMT VLANs configured
 * 
 *    Output(s)           : None 
 * 
 *    Global Variables Referred : None 
 * 
 *    Global Variables Modified : None. 
 * 
 *    Exceptions or Operating 
 *    System Error Handling     : None. 
 * 
 *    Use of Recursion          : None. 
 * 
 *    Returns                   : CFA_TRUE if the vlan list is valid, otherwise 
 *                                CFA_FALSE. 
 * 
 *****************************************************************************/
INT4
CfaMgmtVlanListValid (tMgmtVlanList MgmtVlanList)
{
    UINT1               u1VlanFlag = 0;
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    UINT2               u2VlanId;

    for (u2ByteInd = 0; u2ByteInd < CFA_MGMT_VLAN_LIST_SIZE; u2ByteInd++)
    {
        if (MgmtVlanList[u2ByteInd] != 0)
        {
            u1VlanFlag = MgmtVlanList[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < CFA_MGMT_VLANS_PER_BYTE) && (u1VlanFlag != 0));
                 u2BitIndex++)
            {
                if ((u1VlanFlag & CFA_MGMT_BIT8) != 0)
                {
                    u2VlanId =
                        (UINT2) ((u2ByteInd * CFA_MGMT_VLANS_PER_BYTE) +
                                 u2BitIndex + 1);
#ifdef VLAN_WANTED
                    if (VlanPbIsCreateIvrValid (u2VlanId) == VLAN_FALSE)
                    {
                        return CFA_FALSE;
                    }
#endif

                }
                u1VlanFlag = (UINT1) (u1VlanFlag << 1);
            }
        }
    }

    return (CFA_TRUE);
}

#ifndef MBSM_WANTED
/****************************************************************************/
/*                                                                          */
/*    Function Name      : CfaGetIfIndex                                    */
/*                                                                          */
/*    Description        : Gets the ifIndex of a port by searching in       */
/*                         the gaCfaCommonIfInfo based on the ether type    */
/*                         and PortNum ( PortNum is the number of a Port in */
/*                         a particular type (Gig or Fe))                   */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : u4IfIndex                                        */
/****************************************************************************/
INT4
CfaGetIfIndex (UINT4 u4IfType, INT4 i4PortNum, UINT4 *pu4RetValIfIndex)
{
    UINT4               u4PortNo;
    UINT4               u4IfIndex = 0;
    UINT1               u1EtherType = 0;
    INT4                i4PortCount = 0;
    UINT1               au1IfAlias[CFA_MAX_PORT_NAME_LENGTH];

    for (u4PortNo = 1; u4PortNo <= SYS_DEF_MAX_PHYSICAL_INTERFACES; u4PortNo++)
    {
        SNPRINTF ((CHR1 *) au1IfAlias,
                  CFA_MAX_PORT_NAME_LENGTH, "Slot%u/%u", 0, u4PortNo);
        CfaGetIfIndexFromAliasName (au1IfAlias, &u4IfIndex);
        CfaGetEthernetType (u4IfIndex, &u1EtherType);
        if (u1EtherType == (UINT1) u4IfType)
        {
            i4PortCount++;
            if (i4PortCount == i4PortNum)
            {
                *pu4RetValIfIndex = u4IfIndex;
                return CFA_SUCCESS;
            }
        }
    }
    return CFA_FAILURE;
}
#endif
/****************************************************************************/
/*                                                                          */
/*    Function Name      : CfaGetPortNumFromIfIndex                         */
/*                                                                          */
/*    Description        : Gets the PortNum of a port by searching in       */
/*                         the gaCfaCommonIfInfo based on the ether type    */
/*                         and IfIndex ( Note: PortNum is the number of a   */
/*                         Port in a particular type (Gig or Fe))           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : i4PortNum                                        */
/****************************************************************************/
INT4
CfaGetPortNumFromIfIndex (UINT4 u4IfType, UINT4 u4IntIndex, INT4 i4SlotNum,
                          INT4 i4SlotPortNo, INT4 *pi4PortNum)
{
#ifndef MBSM_WANTED
    UINT4               u4IfIndex = 0;
    INT4                i4PortCount = 1;
    UINT4               u4TempSlotNo = 1;
    UINT1               u1EtherType = 0;
    UINT1               au1IfAlias[CFA_MAX_PORT_NAME_LENGTH];
#endif

    UNUSED_PARAM (u4IntIndex);

#ifdef MBSM_WANTED

    UNUSED_PARAM (u4IfType);
    UNUSED_PARAM (i4SlotNum);
    *pi4PortNum = i4SlotPortNo;

#else
    for (u4TempSlotNo = 1; u4TempSlotNo < (UINT4) i4SlotPortNo; u4TempSlotNo++)
    {
        SNPRINTF ((CHR1 *) au1IfAlias,
                  CFA_MAX_PORT_NAME_LENGTH, "Slot%u/%u", i4SlotNum,
                  u4TempSlotNo);
        CfaGetIfIndexFromAliasName (au1IfAlias, &u4IfIndex);
        CfaGetEthernetType (u4IfIndex, &u1EtherType);
        if (u1EtherType == (UINT1) u4IfType)
        {
            i4PortCount++;
        }

    }
    *pi4PortNum = i4PortCount;
#endif
    return CFA_SUCCESS;
}

#if UNIQUE_VLAN_MAC_WANTED

/*****************************************************************************
 *
 *    Function Name        : CfaGddGetVlanHwAddr
 *
 *    Description        : Function is called for getting the hardware
 *                address of the Vlan interface.
 *
 *    Input(s)            : UINT4 u4IfIndex.
 *
 *    Output(s)            : UINT1 *au1HwAddr.
 *
 *    Global Variables Referred : gIssSysGroupInfo.BaseMacAddr
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if address is obtained,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddGetVlanHwAddr (UINT4 u4IfIndex, UINT1 *pau1HwAddr)
{
    UINT1               au1IfHwAddr[CFA_ENET_ADDR_LEN];

    UINT1               u1IfType;
    UINT1               u1OldMac;

    CFA_DBG1 (CFA_TRC_ALL, CFA_GDD,
              "Entering CfaGddGetVlanHwAddr for interface %u.\n", u4IfIndex);

    MEMSET (au1IfHwAddr, 0, CFA_ENET_ADDR_LEN);

    if ((u4IfIndex > SYS_DEF_MAX_INTERFACES) || (u4IfIndex < 1))
    {

        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddGetHwAddr - "
                  "Interface %u not Valid - FAILURE.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        if ((u1IfType == CFA_L3IPVLAN)
#ifdef WGS_WANTED
            || (u1IfType == CFA_L2VLAN)
#endif /* WGS_WANTED */
            )
        {
            if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
            {

                /* The stack interface uses the stack mac address
                 * which is different from the system mac address*/
                if (u4IfIndex == CFA_DEFAULT_STACK_IFINDEX)
                {
                    CfaGetIfHwAddr (CFA_DEFAULT_STACK_IFINDEX, au1IfHwAddr);
                }
                else
                {
                    CfaGetSysMacAddress (au1IfHwAddr);
                }
            }
            else
            {
                CfaGetSysMacAddress (au1IfHwAddr);
                u1OldMac = au1IfHwAddr[5];
                au1IfHwAddr[5] = au1IfHwAddr[5] + u4IfIndex;
                if (au1IfHwAddr[5] < u1OldMac)
                {
                    au1IfHwAddr[4] += 1;
                    if (au1IfHwAddr[4] == 0x00)
                    {
                        au1IfHwAddr[3] += 1;
                        if (au1IfHwAddr[3] == 0x00)
                        {
                            au1IfHwAddr[2] += 1;
                            if (au1IfHwAddr[2] == 0x00)
                            {
                                au1IfHwAddr[1] += 1;
                            }
                        }
                    }
                }
            }

            /* If AggIndex exceeds 256, au1IfHwAddr[5]
             * value >= old-value.The other octets will not get updated.
             * So we need to check it.
             */
            if ((u4IfIndex / 256) != 0)
            {
                u1OldMac = au1IfHwAddr[4];

                /* If 5th octet New value is less than old value,
                 * the other octets needs to be updated.
                 */
                au1IfHwAddr[4] += (u4IfIndex / 256);

                if (au1IfHwAddr[4] <= u1OldMac)
                {
                    au1IfHwAddr[3] += 1;
                    if (au1IfHwAddr[3] == 0x00)
                    {
                        au1IfHwAddr[2] += 1;
                        if (au1IfHwAddr[2] == 0x00)
                        {
                            au1IfHwAddr[1] += 1;
                        }
                    }

                }
            }

            MEMCPY (pau1HwAddr, au1IfHwAddr, CFA_ENET_ADDR_LEN);
            return CFA_SUCCESS;
        }
    }

    return (CFA_FAILURE);
}
#endif
/****************************************************************************/
/*                                                                          */
/*    Function Name      :  CfaGetPswHwAddr                                 */
/*                                                                          */
/*    Description        : This function returns the Hw address for the     */
/*                         given u4IfIndex.                                 */
/*                                                                          */
/*    Input(s)           : u4IfIndex                                        */
/*                                                                          */
/*    Output(s)          : *pau1HwAddr                                      */
/*                                                                          */
/*    Returns            : CFA_FAILURE or CFA_SUCCESS                       */
/****************************************************************************/
INT4
CfaGetPswHwAddr (UINT4 u4IfIndex, UINT1 *pau1HwAddr)
{

    UINT1               au1IfHwAddr[CFA_ENET_ADDR_LEN];
    UINT1               u1OldMac;

    CfaGetSysMacAddress (au1IfHwAddr);
    u1OldMac = au1IfHwAddr[5];
    au1IfHwAddr[5] = (UINT1) (au1IfHwAddr[5] + u4IfIndex);
    if (au1IfHwAddr[5] < u1OldMac)
    {
        au1IfHwAddr[4] = (UINT1) (au1IfHwAddr[4] + 1);
        if (au1IfHwAddr[4] == 0x00)
        {
            au1IfHwAddr[3] = (UINT1) (au1IfHwAddr[3] + 1);
            if (au1IfHwAddr[3] == 0x00)
            {
                au1IfHwAddr[2] = (UINT1) (au1IfHwAddr[2] + 1);
                if (au1IfHwAddr[2] == 0x00)
                {
                    au1IfHwAddr[1] = (UINT1) (au1IfHwAddr[1] + 1);
                }
            }
        }
    }

    /* If AggIndex exceeds 256, au1IfHwAddr[5]
     * value >= old-value.The other octets will not get updated.
     * So we need to check it.
     */
    if ((u4IfIndex / 256) != 0)
    {
        u1OldMac = au1IfHwAddr[4];

        /* If 5th octet New value is less than old value,
         * the other octets needs to be updated.
         */
        au1IfHwAddr[4] = (UINT1) (au1IfHwAddr[4] + (u4IfIndex / 256));

        if (au1IfHwAddr[4] <= u1OldMac)
        {
            au1IfHwAddr[3] = (UINT1) (au1IfHwAddr[3] + 1);
            if (au1IfHwAddr[3] == 0x00)
            {
                au1IfHwAddr[2] = (UINT1) (au1IfHwAddr[2] + 1);
                if (au1IfHwAddr[2] == 0x00)
                {
                    au1IfHwAddr[1] = (UINT1) (au1IfHwAddr[1] + 1);
                }
            }

        }
    }

    MEMCPY (pau1HwAddr, au1IfHwAddr, CFA_ENET_ADDR_LEN);
    return CFA_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : CfaSetIfMainAdminStatus                          */
/*                                                                          */
/*    Description        : Sets the admin status for the corresponding      */
/*                         IfIndex                                          */
/*                                                                          */
/*    Input(s)           : i4IfMainIndex ,i4SetValIfMainAdminStatus          */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : CFA_FAILURE or CFA_SUCCESS                       */
/****************************************************************************/
INT4
CfaSetIfMainAdminStatus (INT4 i4IfMainIndex, INT4 i4SetValIfMainAdminStatus)
{
    UINT4               u4IfIndex = (UINT4) i4IfMainIndex;
    UINT1               u1IsPhysInterface = CFA_FALSE;
    UINT1               u1IsVirtualInterface = CFA_FALSE;
    UINT1               u1IfType = CFA_NONE;
    UINT1               u1OperStatus = CFA_IF_DOWN;
    UINT1               u1VipOperStatusFlag = CFA_FALSE;
#ifdef NPAPI_WANTED
    UINT1               u1LoopStatus;
#endif

#ifdef MBSM_WANTED
    UINT4               u4SlotId = MBSM_INVALID_SLOT_ID;
    /* In DISS model, without RM making "admin up" of remote ports
     * is blocked until the slot is made active.
     */
    if ((i4SetValIfMainAdminStatus == CFA_IF_UP) &&
        (i4IfMainIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES) &&
        (ISS_GET_STACKING_MODEL () == ISS_DISS_STACKING_MODEL))
    {
        MbsmGetSlotIdFromRemotePort (u4IfIndex, &u4SlotId);
        if ((MbsmGetSlotIndexStatus (u4SlotId) != MBSM_STATUS_ACTIVE) &&
            (u4SlotId != (UINT4) IssGetSwitchid ()))
        {
            return CFA_SUCCESS;
        }
    }
#endif

    /* Null check has to be provided here to avoid crash as MSR does not call
     * Test routine */
    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        CFA_DS_LOCK ();
        if (CFA_CDB_IF_ADMIN_STATUS (u4IfIndex) == i4SetValIfMainAdminStatus)
        {
            CFA_DS_UNLOCK ();
            return CFA_SUCCESS;
        }
        CFA_DS_UNLOCK ();
    }
    else
    {
        if (CFA_IF_ENTRY (u4IfIndex) == NULL)
        {
            return CFA_FAILURE;
        }
        CFA_DS_LOCK ();
        if (CFA_IF_ADMIN (u4IfIndex) == i4SetValIfMainAdminStatus)
        {
            CFA_DS_UNLOCK ();
            return CFA_SUCCESS;
        }
        CFA_DS_UNLOCK ();
    }
    CfaGetIfType (u4IfIndex, &u1IfType);
    switch (u1IfType)
    {
#if defined (WLC_WANTED) || defined (WTP_WANTED)
        case CFA_RADIO:
#endif
        case CFA_ENET:
            u1IsPhysInterface = CFA_TRUE;    /*Intentional fall thru */

        case CFA_PIP:
        case CFA_BRIDGED_INTERFACE:
            if (CfaIsVirtualInterface (u4IfIndex) == CFA_TRUE)
            {
                u1IsVirtualInterface = CFA_TRUE;
            }
            /* Fall through */
        case CFA_ILAN:
        case CFA_PPP:
        case CFA_HDLC:
        case CFA_L3IPVLAN:
        case CFA_L3SUB_INTF:
        case CFA_LOOPBACK:
        case CFA_LAGG:
        case CFA_PROP_VIRTUAL_INTERFACE:
#if defined WGS_WANTED || OPENFLOW_WANTED
        case CFA_L2VLAN:
#endif /* WGS_WANTED */
        case CFA_TUNNEL:
#ifdef MPLS_WANTED
        case CFA_MPLS:
#endif /* MPLS_WANTED */
#ifdef VPN_WANTED
#ifdef IKE_WANTED
        case CFA_VPNC:
#endif /* IKE_WANTED */
#endif /* VPN_WANTED */
        case CFA_TELINK:
        case CFA_PSEUDO_WIRE:
#if defined (WLC_WANTED) || defined (WTP_WANTED)
        case CFA_WLAN_RADIO:
        case CFA_CAPWAP_VIRT_RADIO:
        case CFA_CAPWAP_DOT11_PROFILE:
        case CFA_CAPWAP_DOT11_BSS:
#endif
            break;

#ifdef VXLAN_WANTED
        case CFA_VXLAN_NVE:
            VxlanPortRemoveDynamicNveEntries (u4IfIndex,
                                              i4SetValIfMainAdminStatus);
            break;
#endif

#ifdef MPLS_WANTED
        case CFA_MPLS_TUNNEL:

#ifdef NPAPI_WANTED
            /* Configure/Remove  an L3 interface in H/W */
            if ((i4SetValIfMainAdminStatus == CFA_IF_UP) ||
                (i4SetValIfMainAdminStatus == CFA_IF_DOWN))
            {
#ifdef L2RED_WANTED
                if (CFA_NODE_STATUS () == CFA_NODE_ACTIVE)
#endif
                {
                    if (CfaIfmConfigL3MplsTunnelInterface (u4IfIndex,
                                                           i4SetValIfMainAdminStatus,
                                                           FALSE) ==
                        CFA_FAILURE)
                    {
                        return CFA_FAILURE;
                    }
                }
            }
#endif
            break;
#endif /* MPLS_WANTED */

#ifdef VCPEMGR_WANTED
        case CFA_TAP:
            break;
#endif

        default:
            return CFA_FAILURE;

    }                            /* end of Switch */

    if (i4SetValIfMainAdminStatus == CFA_IF_TEST)
    {
        if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
        {
            CFA_IF_ADMIN (u4IfIndex) = CFA_IF_TEST;
        }
        CfaSetCdbPortAdminStatus (u4IfIndex, CFA_IF_TEST);

        CfaSetIfOperStatus (u4IfIndex, CFA_IF_TEST);
        if (CfaIsMgmtPort (u4IfIndex) == TRUE)
        {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
            /*Disable promiscous mode for the  OOB port */
            CfaGddOobConfigPort (u4IfIndex, CFA_ENET_DIS_PROMIS);
#endif
        }
        else
        {
            /*Disable promiscous mode for the port */
            CfaGddConfigPort (u4IfIndex, CFA_ENET_DIS_PROMIS, NULL);
        }

        /* remove the port from the polling table */
        CfaGddOsRemoveIfFromList (u4IfIndex);

        return CFA_SUCCESS;
    }

    /* Get the Link Status from Driver if it is not a OOB Interface */
    if (u1IfType == CFA_LOOPBACK)
    {
        u1OperStatus = (UINT1) i4SetValIfMainAdminStatus;
    }
    else if ((CfaIsMgmtPort (u4IfIndex) == FALSE) &&
#if defined (WLC_WANTED) || defined (WTP_WANTED)
             (CfaIsLinuxVlanIntf (u4IfIndex) == FALSE) &&
             (CfaIsWssIntf (u4IfIndex) == FALSE))
#else
             (CfaIsLinuxVlanIntf (u4IfIndex) == FALSE))
#endif
{
    /* Problem: When a not connected Port is made Admin-Up in Active Node, 
     * in Standby the Port OperStatus is shown as NP instead of DOWN. Because
     * (1) In Standby Node, CfaGddGetLinkStatus function is called which will 
     * return CFA_IF_NP since Port is not visible to Standby.
     * (2) Port OperStatus is finished before Cli syncUp.
     * Solution: Standby should not invoke this CfaGddGetLinkStatus function.
     * Instead it should get from Software data-structure since its 
     * synced already */
    if (CFA_NODE_STATUS () == CFA_NODE_ACTIVE)
    {
        u1OperStatus = CfaGddGetLinkStatus (u4IfIndex);
    }
    else                        /* IDLE (or) Standby */
    {
        /* This function will get triggered thro' MSR restoration in
         * the standby node when CFA is in IDLE state.
         * So we intentionally call this function in the IDLE state */
        CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
    }
}
    else
    {
        u1OperStatus = CFA_IF_UP;
    }

    if ((i4SetValIfMainAdminStatus == CFA_IF_UP)
        && ((u1IsPhysInterface == CFA_TRUE)
            || (u1IsVirtualInterface == CFA_TRUE))
        && ((u1OperStatus == CFA_IF_DOWN) || (u1OperStatus == CFA_IF_NP)))
    {
        if (CfaIfmHandleInterfaceStatusChange
            (u4IfIndex,
             (UINT1) i4SetValIfMainAdminStatus,
             u1OperStatus, CFA_IF_LINK_STATUS_CHANGE) == CFA_SUCCESS)
        {
            return CFA_SUCCESS;
        }
        else
        {
            return CFA_FAILURE;
        }
    }

    if (i4SetValIfMainAdminStatus == CFA_LOOPBACK_LOCAL)
    {
#ifdef NPAPI_WANTED
        CFA_IF_ADMIN (u4IfIndex) = (UINT1) i4SetValIfMainAdminStatus;
        CfaSetCdbPortAdminStatus (u4IfIndex, CFA_LOOPBACK_LOCAL);
        if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
        {
            if (CfaFsHwUpdateAdminStatusChange (u4IfIndex, FNP_LOOPBACK) !=
                FNP_SUCCESS)
            {
                return CFA_FAILURE;
            }
            return CFA_SUCCESS;
        }
#endif
    }
    if (i4SetValIfMainAdminStatus == CFA_NO_LOOPBACK_LOCAL)
    {
        if (CFA_IF_ADMIN (u4IfIndex) == CFA_IF_DOWN)
        {
            return CFA_FAILURE;
        }
#ifdef NPAPI_WANTED
        CfaGetCdbPortAdminStatus (u4IfIndex, &u1LoopStatus);
        if (u1LoopStatus == CFA_LOOPBACK_LOCAL)
        {
            if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
            {
                if (CfaFsHwUpdateAdminStatusChange (u4IfIndex, FNP_NOLOOPBACK)
                    != FNP_SUCCESS)
                {
                    return CFA_FAILURE;
                }
                CFA_IF_ADMIN (u4IfIndex) = CFA_IF_UP;
                CfaSetCdbPortAdminStatus (u4IfIndex, CFA_IF_UP);
                return CFA_SUCCESS;
            }
        }
#endif
    }

    CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
#if defined (WLC_WANTED) || defined (WTP_WANTED)
        if (CfaIsWssIntf (u4IfIndex) == TRUE)
        {
            if (CfaIfmHandleWssInterfaceStatusChange (u4IfIndex,
                                                      i4SetValIfMainAdminStatus,
                                                      u1OperStatus) !=
                CFA_SUCCESS)
            {
                return CFA_FAILURE;
            }
        }
#endif

        if (CfaIfmHandleInterfaceStatusChange (u4IfIndex,
                                               (UINT1)
                                               i4SetValIfMainAdminStatus,
                                               u1OperStatus,
                                               CFA_TRUE) == CFA_SUCCESS)
        {
            return CFA_SUCCESS;
        }
        else
        {
            return CFA_FAILURE;
        }
    }
    else
    {
        CFA_DS_LOCK ();
        CFA_CDB_IF_ADMIN_STATUS (u4IfIndex) = (UINT1) i4SetValIfMainAdminStatus;
        CFA_DS_UNLOCK ();

        if (CfaIsVipInterface (u4IfIndex) == CFA_TRUE)
        {
            L2IwfGetVipOperStatusFlag (u4IfIndex, &u1VipOperStatusFlag);

            /* Set the Operstatus only if the Flag is set as TRUE */
            if (u1VipOperStatusFlag == CFA_TRUE)
            {
                if (CfaSetVipOperStatus (u4IfIndex, i4SetValIfMainAdminStatus)
                    == CFA_FAILURE)
                {
                    return CFA_FAILURE;
                }
            }
        }
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaUtilIfGetSysSpecificPortID
 *
 *    Description         : This function retrieves the system specific portID
 *                          in the CDB database 
 *
 *    Input(s)            : i4IfIndex - interface index of the interface 
 *                                          whose system specific portID 
 *                                          is to be known.                                          
 *                                     
 *    Output(s)           : None.
 *
 *    Returns             : CFA_SUCCESS/CFA_FAILURE  
 *****************************************************************************/
INT4
CfaUtilIfGetSysSpecificPortID (INT4 i4IfIndex, UINT4 *pu4SysSpecificPortID)
{
    *pu4SysSpecificPortID = 0;

    if ((CFA_IFINDEX_IS_PHY_LAG_PW_IFACE ((UINT4) i4IfIndex) == FALSE) ||
        (i4IfIndex <= 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID ((UINT4) i4IfIndex) == CFA_FALSE)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    *pu4SysSpecificPortID = CFA_CDB_IF_SYS_SPECIFIC_PORTID ((UINT4) i4IfIndex);
    CFA_DS_UNLOCK ();

    return CFA_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : CfaIfIsBridgedInterface                          */
/*                                                                          */
/*    Description        : Checks whether the specified interface is        */
/*                           bridgged interface or not                      */
/*                                                                          */
/*    Input(s)           : i4IfIndex -  interface index                     */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : CFA_TRUE/CFA_FALSE                               */
/****************************************************************************/
UINT1
CfaIfIsBridgedInterface (INT4 i4IfIndex)
{
    UINT1               u1BridgedIfaceStatus = CFA_DISABLED;

    if ((CfaGetIfBridgedIfaceStatus ((UINT4) i4IfIndex, &u1BridgedIfaceStatus)
         == CFA_SUCCESS) && (u1BridgedIfaceStatus == CFA_ENABLED))
    {
        return CFA_TRUE;
    }
    return CFA_FALSE;
}

/*****************************************************************************   
 *   
 *    Function Name        : CfaUtilSetDefaultBridgedIface   
 *   
 *    Description        : This function sets a the Default Port as Router port   
 *                         or switch port   
 *   
 *    Input(s)            : u4PortType - The Type of Port it has to be   
 *   
 *    Output(s)            : None   
 *   
 *    Exceptions or Operating   
 *    System Error Handling    : None.   
 *   
 *    Use of Recursion        : None.   
 *   
 *    Returns            : CFA_SUCCESS/CFA_FAILURE.   
 *   
 *****************************************************************************/
INT4
CfaUtilSetDefaultBridgedIface (UINT4 u4IfIndex, INT4 i4BridgedIfStat)
{
    UINT1               u1OperStatus = 0;

    /*Make the Default Port as Admin down */
    CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);

    if (CfaIfmHandleInterfaceStatusChange (u4IfIndex, (UINT1) CFA_IF_DOWN,
                                           u1OperStatus, CFA_TRUE)
        != CFA_SUCCESS)
    {
        return CFA_FAILURE;
    }

    /*Set the Default Port as RouterPort or SwitchPort */
    if (CfaIfmUpdateInterfaceStatus (u4IfIndex, (UINT2) i4BridgedIfStat) !=
        CFA_SUCCESS)
    {
        return CFA_FAILURE;
    }

    CfaSetIfBridgedIfaceStatus (u4IfIndex, (UINT1) i4BridgedIfStat);

    /*Make the Default Router Port as Admin Up */
    CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);

    CfaIfmHandleInterfaceStatusChange (u4IfIndex, (UINT1) CFA_IF_UP,
                                       u1OperStatus, CFA_TRUE);

    return CFA_SUCCESS;
}

#ifdef MBSM_WANTED
/****************************************************************************/
/*                                                                          */
/*    Function Name      : CfaGetSlotLocalPortNum                           */
/*                                                                          */
/*    Description        : Gets the local Port Num for the Slot             */
/*                                                                          */
/*    Input(s)           : u4IfType - Interface type                        */
/*                         i4PortNum - PortNumber                           */
/*                         i4SlotNum - Slot Number                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : pu4RetValIfIndex - IfIndex specific to slot      */
/****************************************************************************/
INT4
CfaGetSlotLocalPortNum (UINT4 u4IfType, INT4 i4PortNum, INT4 i4SlotNum,
                        UINT4 *pu4RetValIfIndex)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4TempIfIndex;
    UINT4               u4TotalPortCount = 0;
    UINT4               u4StackingModel = ISS_STACKING_MODEL_NONE;
    INT4                i4PortCount = 0;
    UINT1               u1EtherType = 0;

    tMbsmSlotInfo       MbsmSlotInfo;

    if (MbsmGetSlotInfo (i4SlotNum, &MbsmSlotInfo) == MBSM_FAILURE)
    {
        return CFA_FAILURE;
    }
    u4TempIfIndex = MbsmSlotInfo.u4StartIfIndex;
    for (u4IfIndex = u4TempIfIndex; u4IfIndex <
         (u4TempIfIndex + MbsmSlotInfo.u4NumPorts); u4IfIndex++)
    {
        CfaGetEthernetType (u4IfIndex, &u1EtherType);

        if (u1EtherType == (UINT1) u4IfType)
        {
            i4PortCount++;
            if (IssGetColdStandbyFromNvRam () != ISS_COLDSTDBY_ENABLE)
            {
                if (i4PortCount == i4PortNum)
                {
                    u4StackingModel = ISS_GET_STACKING_MODEL ();
                    /* For Dual Unit Stacking, Connecting Port count of previous slot
                       should be used to calculate the Slot Port Number */
                    if (((u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL) ||
                         (u4StackingModel == ISS_DISS_STACKING_MODEL))
                        && (i4SlotNum != MBSM_SLOT_INDEX_START))
                    {
                        MbsmGetPrevSlotsTotalPortCount (i4SlotNum,
                                                        &u4TotalPortCount);

                        u4IfIndex = u4IfIndex % u4TotalPortCount;
                    }
                    else
                    {
                        u4IfIndex = (u4IfIndex % MBSM_MAX_PORTS_PER_SLOT);
                        if (u4IfIndex == 0)
                        {
                            u4IfIndex = MBSM_MAX_PORTS_PER_SLOT;
                        }
                    }
                    *pu4RetValIfIndex = u4IfIndex;
                    return CFA_SUCCESS;
                }
            }
            else
            {
                if (i4PortCount == i4PortNum)
                {
                    u4IfIndex = (u4IfIndex % MBSM_MAX_POSSIBLE_PORTS_PER_SLOT);
                    if (u4IfIndex == 0)
                    {
                        u4IfIndex = MBSM_MAX_PORTS_PER_SLOT;
                    }
                    *pu4RetValIfIndex = u4IfIndex;
                    return CFA_SUCCESS;
                }
            }
        }

    }
    return CFA_FAILURE;
}
#endif

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCreateVip                                       */
/*                                                                           */
/*     DESCRIPTION      : This function is called from PBB module to handle  */
/*                        creation of Virtual Intance Port                   */
/*                                                                           */
/*     INPUT            : u4ContextId - Context Id in which VIP will exist   */
/*                                                                           */
/*     OUTPUT           : *pu2LocalPortId - Pointer to Local port Id of the  */
/*                         created interface                                 */
/*                        *pi4IfIndex - Pointer to IfIndex of the created    */
/*                         interface                                         */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS/CFA_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCreateVip (INT4 *pi4IfIndex)
{
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE Alias;
    UINT1               au1Tmp[CFA_MAX_PORT_NAME_LENGTH];
    UINT1              *pu1Alias = NULL;
    UINT4               u4IfType = 0;
    UINT1              *pu1IfaceInfo = NULL;
    UINT4               u4Index = 0;

    MEMSET (au1Tmp, 0, CFA_MAX_PORT_NAME_LENGTH);
    pu1Alias = &au1Tmp[0];

    u4IfType = CFA_BRIDGED_INTERFACE;

    CFA_DS_LOCK ();

    /* Flexible ifIndex changes ===
       to support free ifindex being returned within the
       required range */

    if (OSIX_FAILURE == CfaGetFreeVipInterfaceIndex (&u4Index))
    {
        CFA_DS_UNLOCK ();
        return (CFA_FAILURE);
    }

    *pi4IfIndex = (INT4) u4Index;

    CFA_DS_UNLOCK ();

    pu1IfaceInfo = (UINT1 *) cli_get_iface_type_name (u4IfType);
    SPRINTF ((CHR1 *) pu1Alias, "%s%d", pu1IfaceInfo, u4Index);

    CFA_LOCK ();

    if (nmhSetIfMainRowStatus (u4Index, CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }

    Alias.i4_Length = (INT4) STRLEN (pu1Alias);
    Alias.pu1_OctetList = pu1Alias;

    nmhSetIfAlias (u4Index, &Alias);

    /* Set the Type */
    if (nmhTestv2IfMainType (&u4ErrCode, (INT4) u4Index, CFA_BRIDGED_INTERFACE)
        == SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (u4Index, CFA_RS_DESTROY);
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }

    if (nmhSetIfMainType (u4Index, CFA_BRIDGED_INTERFACE) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }

    /* All the mandatory parameters are set. 
     * So make the row active */
    nmhSetIfMainRowStatus (u4Index, CFA_RS_ACTIVE);

    CFA_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaGetIlanPorts                                    */
/*                                                                           */
/*     DESCRIPTION      : This function is called from PBB module to get the */
/*                        logically connected port of the given port         */
/*                                                                           */
/*     INPUT            : u4InIfIndex - IfIndex of the port specified        */
/*                                                                           */
/*     OUTPUT           : **pOutInfo - Pointer to the link list of structure */
/*                         containing associated IfIndex info                */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS/CFA_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaGetIlanPorts (UINT4 u4InIfIndex, tTMO_SLL * pOutInfo)
{
    tStackInfoStruct   *pLowStackListScan = NULL;
    tStackInfoStruct   *pHighStackListScan = NULL;
    tTMO_SLL           *pHighStack = NULL;
    tTMO_SLL           *pLowStack = NULL;
    tCfaILanPortStruct *pOutInfoNode = NULL;
    tCfaILanPortStruct *pScanNode = NULL;
    tCfaILanPortStruct *pPrevNode = NULL;

    if (pOutInfo == NULL)
    {
        return CFA_FAILURE;
    }
    else
    {
        TMO_SLL_Init (pOutInfo);
        pLowStack = &CFA_IF_STACK_LOW (u4InIfIndex);

        TMO_SLL_Scan (pLowStack, pLowStackListScan, tStackInfoStruct *)
        {
            if (pLowStackListScan->u4IfIndex != CFA_NONE)
            {
                break;
            }
            else
            {
                continue;
            }
        }

        if (pLowStackListScan != NULL)
        {
            pHighStackListScan =
                (tStackInfoStruct *)
                CFA_IF_STACK_HIGH_ENTRY (pLowStackListScan->u4IfIndex);
            pHighStack = &CFA_IF_STACK_HIGH (pLowStackListScan->u4IfIndex);

            /* If entry exists for this ILAN interface, get the logically
             * connected interface */
            if (pHighStack != NULL)
            {
                TMO_SLL_Scan (pHighStack, pHighStackListScan,
                              tStackInfoStruct *)
                {
                    if ((pHighStackListScan->u4IfIndex != u4InIfIndex) &&
                        (pHighStackListScan->u4IfIndex != CFA_NONE))
                    {
                        if ((pOutInfoNode = MemAllocMemBlk
                             (gCfaILanPortPoolId)) == NULL)
                        {
                            if (TMO_SLL_Count (pOutInfo) > 0)
                            {
                                TMO_SLL_FreeNodes (pOutInfo,
                                                   gCfaILanPortPoolId);
                            }
                            return (CFA_FAILURE);
                        }
                        TMO_SLL_Init_Node (&pOutInfoNode->NextEntry);

                        pOutInfoNode->u4IfIndex = pHighStackListScan->u4IfIndex;

                        TMO_SLL_Scan (pOutInfo, pScanNode, tCfaILanPortStruct *)
                        {
                            if (pScanNode->u4IfIndex > pOutInfoNode->u4IfIndex)
                            {
                                break;
                            }
                            else
                            {
                                pPrevNode = pScanNode;
                            }
                        }

                        if (pPrevNode == NULL)
                        {
                            /* Node to be inserted in first position */
                            TMO_SLL_Insert (pOutInfo, NULL,
                                            (tTMO_SLL_NODE *) & (pOutInfoNode->
                                                                 NextEntry));
                        }
                        else if (pScanNode == NULL)
                        {
                            /* Node to be inserted in last position */
                            TMO_SLL_Insert (pOutInfo,
                                            (tTMO_SLL_NODE *) & (pPrevNode->
                                                                 NextEntry),
                                            (tTMO_SLL_NODE *) & (pOutInfoNode->
                                                                 NextEntry));
                        }
                        else
                        {
                            /* Node to be inserted in middle */
                            TMO_SLL_Insert_In_Middle (pOutInfo,
                                                      (tTMO_SLL_NODE *) &
                                                      (pPrevNode->NextEntry),
                                                      (tTMO_SLL_NODE *) &
                                                      (pOutInfoNode->NextEntry),
                                                      (tTMO_SLL_NODE *) &
                                                      (pScanNode->NextEntry));
                        }
                    }
                }
            }
        }
        return CFA_SUCCESS;
    }

}

/*****************************************************************************/
/* Function Name      : CfaTestIfPipName                                     */
/*                                                                           */
/* Description        : This function tests the Provider Instance Port name  */
/*                      for the interface                                    */
/*                                                                           */
/* Input(s)           : i4IfIndex:Interface index                            */
/*                      pCfaPipName:Pointer to PIP name                      */
/*                                                                           */
/* Output             : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaTestIfPipName (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pCfaPipName)
{
    UINT4               u4ErrCode;

    if (nmhTestv2IfAlias (&u4ErrCode, i4IfIndex, pCfaPipName) == SNMP_FAILURE)
    {
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfPipName                                      */
/*                                                                           */
/* Description        : This function get the Provider Instance Port name    */
/*                      for the interface                                    */
/*                                                                           */
/* Input(s)           : i4IfIndex:Interface index                            */
/*                                                                           */
/* Output             : pCfaPipName:Pointer to PIP nameNone                  */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfPipName (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pCfaPipName)
{
    nmhGetIfAlias (i4IfIndex, pCfaPipName);

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfPipName                                      */
/*                                                                           */
/* Description        : This function sets the Provider Instance Port name   */
/*                      for the interface                                    */
/*                                                                           */
/* Input(s)           : i4IfIndex:Interface index                            */
/*                      pCfaPipName:Pointer to PIP name                      */
/*                                                                           */
/* Output             : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaSetIfPipName (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pCfaPipName)
{
    CFA_LOCK ();
    if (nmhSetIfAlias ((UINT4) i4IfIndex, pCfaPipName) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }

    CFA_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetNumberOfBebPorts                               */
/*                                                                           */
/* Description        : This function get the number of interfaces present   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output             : i4NumBebPorts                                        */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    :  i4NumBebPorts                                       */
/*****************************************************************************/
INT4
CfaGetNumberOfBebPorts (VOID)
{
    INT4                i4Index = 0;
    INT4                i4PrevIndex = 0;
    INT4                i4NumBebPorts = 0;
    UINT1               u1BrgPortType = CFA_INVALID_BRIDGE_PORT;

    if (nmhGetFirstIndexIfMainTable (&i4Index) == SNMP_FAILURE)
    {
        i4NumBebPorts = 0;
    }
    do
    {
        if (CfaIsILanInterface ((UINT4) i4Index) == CFA_FALSE)
        {
            if (CfaGetIfBrgPortType ((UINT4) i4Index, &u1BrgPortType)
                == CFA_SUCCESS)
            {
                if ((u1BrgPortType == CFA_PROVIDER_NETWORK_PORT) ||
                    (u1BrgPortType == CFA_CNP_PORTBASED_PORT) ||
                    (u1BrgPortType == CFA_CNP_STAGGED_PORT) ||
                    (u1BrgPortType == CFA_CNP_CTAGGED_PORT) ||
                    (u1BrgPortType == CFA_PROVIDER_INSTANCE_PORT) ||
                    (u1BrgPortType == CFA_CUSTOMER_BACKBONE_PORT))
                {
                    ++i4NumBebPorts;
                }
            }
        }
        i4PrevIndex = i4Index;
    }
    while ((i4PrevIndex != (CFA_MIN_VIP_IF_INDEX - 1))
           && (nmhGetNextIndexIfMainTable (i4PrevIndex, &i4Index) ==
               SNMP_SUCCESS));

    return (i4NumBebPorts);
}

/*****************************************************************************/
/* Function Name      : CfaSetVipOperStatus                                  */
/*                                                                           */
/* Description        : This function sets the Oper Status of the VIP        */
/*                                                                           */
/* Input(s)           : u4VipIfIndex - VIP Interface Index                   */
/*                      i4VipOperStatus - VIP Oper status                    */
/*                                                                           */
/* Output             : i4NumBebPorts                                        */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    :  i4NumBebPorts                                       */
/*****************************************************************************/
INT4
CfaSetVipOperStatus (UINT4 u4VipIfIndex, INT4 i4VipOperStatus)
{
    UINT1               u1PrevOperStatus = CFA_IF_DOWN;

    CfaGetIfOperStatus (u4VipIfIndex, &u1PrevOperStatus);

    if (u1PrevOperStatus == (UINT1) i4VipOperStatus)
    {
        return CFA_SUCCESS;
    }

    CfaSetIfOperStatus (u4VipIfIndex, (UINT1) i4VipOperStatus);
#ifdef NPAPI_WANTED
    if (CfaIsMgmtPort (u4VipIfIndex) == FALSE)
    {
        if (i4VipOperStatus == CFA_IF_UP)
        {
#ifdef MBSM_WANTED
            if (u1PrevOperStatus != CFA_IF_NP)
            {
#endif
                if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
                {
                    if (CfaFsHwUpdateAdminStatusChange (u4VipIfIndex,
                                                        FNP_TRUE) !=
                        FNP_SUCCESS)
                    {
                        /* if Hardware port could not bring up then no need to 
                         * intimate Admin status change to the upper layer 
                         */
                        return CFA_FAILURE;
                    }
                }
            }
#ifdef MBSM_WANTED
        }
#endif
    }
#endif

    L2IwfPortOperStatusIndication (u4VipIfIndex, (UINT1) i4VipOperStatus);

#ifdef NPAPI_WANTED
#ifdef MBSM_WANTED
    if (u1PrevOperStatus != CFA_IF_NP)
    {
#endif
        if (i4VipOperStatus == CFA_IF_DOWN)
        {
            if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
            {
                if (CfaFsHwUpdateAdminStatusChange (u4VipIfIndex,
                                                    FNP_FALSE) != FNP_SUCCESS)
                {
                    return CFA_FAILURE;
                }
            }
        }
#ifdef MBSM_WANTED
    }
#endif
#endif

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaValidateIfMainTableIndex                          */
/*                                                                           */
/* Description        : This function validates the index of the IfMainTable */
/*                                                                           */
/* Input(s)           : i4IfIndex:Interface index                            */
/*                                                                           */
/* Output             : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaValidateIfMainTableIndex (INT4 i4IfIndex)
{
    UINT4               u4IfIndex;

    if (((UINT4) i4IfIndex > CFA_MAX_INTERFACES ()) || (i4IfIndex <= 0))
    {
        return (CFA_FAILURE);
    }

    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        if (CFA_IF_ENTRY_USED (u4IfIndex))
        {
            return CFA_SUCCESS;
        }
        else
        {
            return CFA_FAILURE;
        }
    }
    else
    {
        CFA_DS_LOCK ();
        if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
        {
            CFA_DS_UNLOCK ();
            return CFA_SUCCESS;
        }
        else
        {
            CFA_DS_UNLOCK ();
            return CFA_FAILURE;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaIPVXSetIpAddress                                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the IP address of an IVR        */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
/*                        u4IpAddr, u4SubnetMask - IPAddress, Subnet Mask    */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS/CFA_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIPVXSetIpAddress (UINT4 u4IfIndex, UINT4 u4IpAddr, UINT4 u4IpSubnetMask)
{
    UINT4               u4ErrCode;
    UINT4               u4BcastAddr;
    UINT4               u4PrevIpAddr = 0;
    UINT4               u4PrevSubnetMask;
    INT4                i4Method = 0;

    nmhGetIfIpAddrAllocMethod ((INT4) u4IfIndex, &i4Method);

    if ((i4Method != CFA_IP_ALLOC_MAN) && (i4Method != CFA_IP_ALLOC_NONE))
    {
        return (CFA_FAILURE);
    }
    /* Test Ip addr and Sub net mask */
    if (CfaIPVXTestIpAddrpSubnetMask ((INT4) u4IfIndex,
                                      u4IpAddr, u4IpSubnetMask) == CFA_FAILURE)
    {
        return (CFA_FAILURE);
    }
    nmhGetIfIpAddr ((INT4) u4IfIndex, &u4PrevIpAddr);
    if (nmhSetIfIpAddr (u4IfIndex, u4IpAddr) == SNMP_FAILURE)
    {
        return (CFA_FAILURE);
    }

    nmhGetIfIpSubnetMask ((INT4) u4IfIndex, &u4PrevSubnetMask);

    if (nmhSetIfIpSubnetMask (u4IfIndex, u4IpSubnetMask) == SNMP_FAILURE)
    {
        nmhSetIfIpAddr (u4IfIndex, u4PrevIpAddr);
        return (CFA_FAILURE);
    }

    u4BcastAddr = u4IpAddr | (~(u4IpSubnetMask));

    if (nmhTestv2IfIpBroadcastAddr (&u4ErrCode, (INT4) u4IfIndex,
                                    u4BcastAddr) == SNMP_FAILURE)
    {
        nmhSetIfIpAddr (u4IfIndex, u4PrevIpAddr);
        nmhSetIfIpSubnetMask (u4IfIndex, u4PrevSubnetMask);
        return (CFA_FAILURE);
    }

    if (nmhSetIfIpBroadcastAddr (u4IfIndex, u4BcastAddr) == SNMP_FAILURE)
    {
        return (CFA_FAILURE);
    }
    return (CFA_SUCCESS);
}

/****************************************************************************
 Function    :  CfaIPVXSetIfIpSubnetMask
 Description :  This function is used to set the Subnet Mask for a given
                interface.
 Input       :  i4IfMainIndex  -  Interface index
                u4SetValIfIpSubnetMask - Subnet Mask

 Output      :  None
 Returns     :  CFA_SUCCESS or CFA_FAILURE
****************************************************************************/
INT1
CfaIPVXSetIfIpSubnetMask (INT4 i4IfMainIndex, UINT4 u4SetValIfIpSubnetMask)
{
    UINT4               u4IfIndex;
    UINT1               u1SubnetMask;
    UINT4               u4SubnetMask;
    UINT4               u4BcastAddr;
    UINT1               u1IfType = 0;
    UINT1               u1IsActive = CFA_FALSE;
    UINT1               u1IsAlreadyRegWithIp = CFA_FALSE;
    UINT1               u1Counter = 0;
    tIpConfigInfo       IpIfInfo;
    UINT4               u4Flag = 0;
    tStackInfoStruct   *pScanNode = NULL;

    MEMSET (&IpIfInfo, 0, sizeof (tIpConfigInfo));
    u4IfIndex = (UINT4) i4IfMainIndex;

    if (CfaIsVipInterface (u4IfIndex) == CFA_TRUE)
    {
        return CFA_FAILURE;
    }
    else
    {
        while ((u1Counter <= CFA_MAX_CIDR) &&
               (u4CidrSubnetMask[u1Counter] != (UINT4) u4SetValIfIpSubnetMask))
        {
            ++u1Counter;
        }
        if (u1Counter > CFA_MAX_CIDR)
        {
            return CFA_FAILURE;
        }

        u1SubnetMask = u1Counter;

        /* Null check has to be provided here to avoid crash as MSR does not call
         * Test routine */
        if (CFA_IF_ENTRY (u4IfIndex) == NULL)
        {
            return CFA_FAILURE;
        }

        if (CfaIpIfGetIfInfo (u4IfIndex, &IpIfInfo) == CFA_FAILURE)
        {
            return CFA_FAILURE;
        }

        u4SubnetMask = u4CidrSubnetMask[u1SubnetMask];
        u4BcastAddr = (~u4SubnetMask | IpIfInfo.u4Addr);

        /* register/update with IP only if RS is active */
        if (CFA_IF_RS (u4IfIndex) == CFA_RS_ACTIVE)
        {
            u1IsActive = CFA_TRUE;
            pScanNode =
                (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY ((UINT2)
                                                              u4IfIndex);
            CFA_CHECK_IF_NULL (pScanNode, CFA_FAILURE);

            if ((CFA_IF_STACK_IFINDEX (pScanNode) == CFA_NONE) &&
                (CFA_IF_STACK_STATUS (pScanNode) == CFA_RS_ACTIVE) &&
                (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX))
            {
                u1IsAlreadyRegWithIp = CFA_TRUE;
            }
        }

        CfaGetIfType (u4IfIndex, &u1IfType);

        switch (u1IfType)
        {

            case CFA_ENET:
            case CFA_L3IPVLAN:
            case CFA_LOOPBACK:
#ifdef WGS_WANTED
            case CFA_L2VLAN:
#endif /* WGS_WANTED */

                u4Flag = CFA_IP_IF_NETMASK | CFA_IP_IF_BCASTADDR;

                IpIfInfo.u4NetMask = u4SubnetMask;
                IpIfInfo.u4BcastAddr = u4BcastAddr;

                if (CfaIpIfSetIfInfo ((UINT4) i4IfMainIndex, u4Flag,
                                      &IpIfInfo) != CFA_SUCCESS)
                {
                    return CFA_FAILURE;
                }
                if (u1IsActive == CFA_TRUE)
                {
                    if (u1IsAlreadyRegWithIp == CFA_TRUE)
                    {
                        if (CfaIfmConfigNetworkInterface
                            (CFA_NET_IF_UPD, u4IfIndex, 0, NULL) != CFA_SUCCESS)
                        {
                            return CFA_FAILURE;
                        }
                    }
                }
                return CFA_SUCCESS;

            default:
                return CFA_FAILURE;    /* not expected */

        }
    }
}

/****************************************************************************
 Function    :  CfaIPVXSetIfIpBroadcastAddr
 Description :  This function is used to set the broadcast address for a 
                given interface.

 Input       :  i4IfMainIndex  -  Interface index
                u4SetValIfIpBroadcastAddr - Broadcast address.

 Output      :  None
 Returns     :  CFA_SUCCESS or CFA_FAILURE
****************************************************************************/
INT1
CfaIPVXSetIfIpBroadcastAddr (INT4 i4IfMainIndex,
                             UINT4 u4SetValIfIpBroadcastAddr)
{
    UINT4               u4IfIndex = CFA_NONE;
    UINT1               u1IsActive = CFA_FALSE;
    UINT1               u1IsAlreadyRegWithIp = CFA_FALSE;
    tIpConfigInfo       IpIfInfo;
    tStackInfoStruct   *pScanNode = NULL;
    UINT4               u4Flag = 0;

    u4IfIndex = (UINT4) i4IfMainIndex;
    /* Null check has to be provided here to avoid crash as MSR does not call
     * Test routine */

    if (CfaIsVipInterface (u4IfIndex) == CFA_TRUE)
    {
        return CFA_FAILURE;
    }
    else
    {
        if (CFA_IF_ENTRY (u4IfIndex) == NULL)
        {
            return CFA_FAILURE;
        }
        /* register/update with IP only if RS is active */
        if (CFA_IF_RS (u4IfIndex) == CFA_RS_ACTIVE)
        {
            u1IsActive = CFA_TRUE;
            pScanNode =
                (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY ((UINT2)
                                                              u4IfIndex);
            CFA_CHECK_IF_NULL (pScanNode, CFA_FAILURE);

            if ((CFA_IF_STACK_IFINDEX (pScanNode) == CFA_NONE) &&
                (CFA_IF_STACK_STATUS (pScanNode) == CFA_RS_ACTIVE) &&
                (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX))
            {
                u1IsAlreadyRegWithIp = CFA_TRUE;
            }
        }

        u4Flag = CFA_IP_IF_BCASTADDR;

        IpIfInfo.u4BcastAddr = (UINT4) u4SetValIfIpBroadcastAddr;

        if (CfaIpIfSetIfInfo ((UINT4) i4IfMainIndex, u4Flag,
                              &IpIfInfo) != CFA_SUCCESS)
        {
            return CFA_FAILURE;
        }

        if (u1IsActive == CFA_TRUE)
        {
            if (u1IsAlreadyRegWithIp == CFA_TRUE)
            {
                if (CfaIfmConfigNetworkInterface (CFA_NET_IF_UPD, u4IfIndex,
                                                  0, NULL) != CFA_SUCCESS)
                {
                    return CFA_FAILURE;
                }
            }
        }

        return CFA_SUCCESS;
    }
}

/****************************************************************************
 Function    :  CfaIPVXSetIfIpAddr
 Description :  This function is used to set the IP address for 
                the interface.
 Input       :  i4IfMainIndex    -- Interface Index
                u4SetValIfIpAddr -- Interface IP address
 Output      :  None 
 Returns     :  CFA_SUCCESS or CFA_FAILURE
****************************************************************************/
INT1
CfaIPVXSetIfIpAddr (INT4 i4IfMainIndex, UINT4 u4SetValIfIpAddr)
{
    UINT4               u4IfIndex = CFA_NONE;
    UINT1               u1IfType = CFA_NONE;
    UINT1               u1IsAlreadyRegWithIp = CFA_FALSE;
    tIpConfigInfo       IpIfInfo;
    tStackInfoStruct   *pScanNode = NULL;
    UINT4               u4Flag = 0;

    u4IfIndex = (UINT4) i4IfMainIndex;
    /* Null check has to be provided here to avoid crash as MSR does not call
     * Test routine */

    if (CfaIsVipInterface (u4IfIndex) == CFA_TRUE)
    {
        return CFA_FAILURE;
    }
    else
    {
        if (CFA_IF_ENTRY (u4IfIndex) == NULL)
        {
            return CFA_FAILURE;
        }
        /* register/update with IP only if RS is active */
        if (CFA_IF_RS (u4IfIndex) == CFA_RS_ACTIVE)
        {
            pScanNode =
                (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY ((UINT2)
                                                              u4IfIndex);
            CFA_CHECK_IF_NULL (pScanNode, CFA_FAILURE);
            if ((CFA_IF_STACK_IFINDEX (pScanNode) == CFA_NONE) &&
                (CFA_IF_STACK_STATUS (pScanNode) == CFA_RS_ACTIVE) &&
                (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX))
            {
                u1IsAlreadyRegWithIp = CFA_TRUE;
                UNUSED_PARAM (u1IsAlreadyRegWithIp);
            }
        }

        if (CfaIpIfGetIfInfo (u4IfIndex, &IpIfInfo) == CFA_FAILURE)
        {
            return CFA_FAILURE;
        }

        if (u4SetValIfIpAddr == IpIfInfo.u4Addr)
        {
            return CFA_SUCCESS;
        }
        CfaGetIfType (u4IfIndex, &u1IfType);

        switch (u1IfType)
        {

            case CFA_ENET:
            case CFA_LOOPBACK:
            case CFA_L3IPVLAN:
#ifdef WGS_WANTED
            case CFA_L2VLAN:
#endif /* WGS_WANTED */
                u4Flag = CFA_IP_IF_PRIMARY_ADDR;

                MEMSET (&IpIfInfo, 0, sizeof (tIpConfigInfo));
                IpIfInfo.u4Addr = u4SetValIfIpAddr;
                if (CfaIpIfSetIfInfo (u4IfIndex, u4Flag,
                                      &IpIfInfo) == CFA_FAILURE)
                {
                    return CFA_FAILURE;
                }
                return CFA_SUCCESS;

            default:
                return CFA_FAILURE;    /* not expected */

        }
    }
}

/****************************************************************************
 Function    :  CfaIPVXTestIpAddrpSubnetMask

 Description :  This function is used to test IP address and subnet mask

 Input       :  i4IfMainIndex - Index of the interface
                u4TestValIfIpAddr - Ipaddress for the interface
                u4TestValIfIpSubnetMask -subnetmask for the interface
                
 Output      :  None
 
 Returns     :  CFA_SUCCESS or CFA_FAILURE
****************************************************************************/
INT4
CfaIPVXTestIpAddrpSubnetMask (INT4 i4IfMainIndex,
                              UINT4 u4TestValIfIpAddr,
                              UINT4 u4TestValIfIpSubnetMask)
{
    UINT2               u2IfIndex;
    UINT1               u1IfType;
    UINT1               u1BridgedIfaceStatus;
    tIpConfigInfo       IpIfInfo;
    tStackInfoStruct   *pScanNode = NULL;
    UINT1               u1Counter;
    UINT4               u4BroadCastAddr;
    UINT4               u4NetworkIpAddr;

    u2IfIndex = (UINT2) i4IfMainIndex;

    if (u2IfIndex > SYS_DEF_MAX_INTERFACES)
    {
        return CFA_FAILURE;
    }

    if (CfaIpIfGetIfInfo ((UINT4) u2IfIndex, &IpIfInfo) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }
    if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
    {
        return CFA_FAILURE;
    }

    if (u4TestValIfIpAddr != 0)
    {
        if (!((CFA_IS_ADDR_CLASS_A (u4TestValIfIpAddr) ?
               (IP_IS_ZERO_NETWORK (u4TestValIfIpAddr) ? 0 : 1) : 0) ||
              (CFA_IS_ADDR_CLASS_B (u4TestValIfIpAddr)) ||
              (CFA_IS_ADDR_CLASS_C (u4TestValIfIpAddr))))
        {
            return CFA_FAILURE;
        }
    }

    /* IP address should not be configured for the bridged interface */
    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        CfaGetIfBridgedIfaceStatus ((UINT4) u2IfIndex, &u1BridgedIfaceStatus);
        if (u1BridgedIfaceStatus == CFA_ENABLED)
        {
            return CFA_FAILURE;
        }
    }

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

/*
    if ((CFA_IF_ADMIN (u2IfIndex) == CFA_IF_UP) &&
        ((u1IfType == CFA_ENET) || (u1IfType == CFA_L3IPVLAN)
         || (u1IfType == CFA_LOOPBACK)))
    {
        return CFA_FAILURE;
    }
    */

    pScanNode = (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u2IfIndex);
    CFA_CHECK_IF_NULL (pScanNode, CFA_FAILURE);
/* the interface should not have any other interface above it */
    if (CFA_IF_STACK_IFINDEX (pScanNode) != CFA_NONE)
    {
        return CFA_FAILURE;
    }

    if ((u4TestValIfIpAddr == IpIfInfo.u4Addr) &&
        (u4TestValIfIpSubnetMask == IpIfInfo.u4NetMask))
    {
        return CFA_SUCCESS;
    }
    if (u4TestValIfIpAddr != IpIfInfo.u4Addr)
    {
        /* Dont allow configuration of primary address same as that
         * of secondary */
        if (CfaIpIfIsOurInterfaceAddress ((UINT4) u2IfIndex,
                                          u4TestValIfIpAddr) == CFA_SUCCESS)
        {
            return CFA_FAILURE;
        }
    }

    /* The valid subnet address is from 0 to 32 */
    for (u1Counter = 0; u1Counter <= CFA_MAX_CIDR; ++u1Counter)
    {
        if (u4CidrSubnetMask[u1Counter] == u4TestValIfIpSubnetMask)
        {
            break;
        }
    }

    if (u1Counter != 0)
    {
        if ((CFA_CDB_IF_TYPE (u2IfIndex) == CFA_LOOPBACK) &&
            ((u1Counter != CFA_MAX_CIDR)))
        {
            /* Loopback interface mask should be 32 bit mask. */
            return CFA_FAILURE;
        }
    }

    if (u1Counter > CFA_MAX_CIDR)
    {
        return CFA_FAILURE;
    }

    if (CfaIpIfValidateIfIpSubnet ((UINT4) u2IfIndex,
                                   u4TestValIfIpAddr,
                                   u4TestValIfIpSubnetMask) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }

/*when 'no ipaddress' command for primary ipaddress is executed then u4TestValIfIpAddr and u4TestValIfIpSubnetMask can have a value ZERO,if ipaddress command is executed u4TestValIfIpAddr and u4TestValIfIpSubnetMask always not equal to zero*/
    if (u4TestValIfIpAddr == 0 && u4TestValIfIpSubnetMask == 0)
    {
        return CFA_SUCCESS;
    }
    if (u4TestValIfIpSubnetMask != 0xffffffff)
    {
        /* ipaddress should not be Broadcast Address */
        u4BroadCastAddr = u4TestValIfIpAddr | (~(u4TestValIfIpSubnetMask));
        if (u4TestValIfIpAddr == u4BroadCastAddr)
        {
            return CFA_FAILURE;
        }

        /* ipaddress should not be Network Address */
        u4NetworkIpAddr = u4TestValIfIpAddr & (~(u4TestValIfIpSubnetMask));
        if (u4NetworkIpAddr == 0)
        {
            return CFA_FAILURE;
        }
    }
    return CFA_SUCCESS;
}

/****************************************************************************
 *  Function    :  CfaIncMsrForIfIpTable
 *
 *   Input       :  i4IfIndex   - The interface index where the entry is added
 *                  u4IpAddress - IP address of the cache entry
 *                  cDataType   - Datatype of the configured value.
 *                  pSetVal     - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *                  IsRowStatus - Rowstatus (TRUE/FALSE)
 *
 * t
 * Description :  This function sends notifications to MSR and RM
 *                 for the NetToMedia Table.
 *
 * Output      :  None.
 *
 * Returns     :  IPVX_SUCCESS or IPVX_FAILURE
 *
 * ****************************************************************************/
VOID
CfaIncMsrForIfIpTable (INT4 i4IfIndex, INT4 i4SetVal, CHR1 cDatatype,
                       UINT4 *pu4ObjectId, UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = 0;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (SnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    switch (cDatatype)
    {
        case 'i':
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IfIndex, i4SetVal));
            break;
        case 'p':
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p", i4IfIndex, i4SetVal));
            break;
        default:
            break;
    }
    return;
}

/****************************************************************************
 Function    :  CfaCliGetIVRNameFromIfNameAndCxtId

 Description :  This function is used to Get the IVR interface name from the
                interface name and l2 context id. Name will have If name followed
                by space followed by l2 context name.

 Input       :  u4L2CxtId - Context Id                 
                pu1IfName - Interface alias name                 
                
 Output      :  pu1IvrName - IVR interface name.
 
 Returns     :  CFA_SUCCESS or CFA_FAILURE
****************************************************************************/
INT1
CfaCliGetIVRNameFromIfNameAndCxtId (UINT4 u4L2CxtId, UINT1 *pu1IfName,
                                    UINT1 *pu1IvrName)
{
    UINT1               u1Length = 0;

    u1Length = (UINT1) (STRLEN (pu1IfName));
    MEMCPY (pu1IvrName, pu1IfName, u1Length);
    pu1IvrName[u1Length++] = ' ';
    if (VcmGetAliasName (u4L2CxtId, &pu1IvrName[u1Length]) == VCM_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name       : CfaIsIfEntryProgrammingAllowed                  */
/*                                                                          */
/*    Description         : This function checks whether gapIfTable related */
/*                          programming is allowed in this interface or not.*/
/*                          For SISP and VIP interfaces, it is not allowed. */
/*                                                                          */
/*    Input(s)            : u4Index - interface index                       */
/*                                                                          */
/*    Output(s)           : NONE                                            */
/*                                                                          */
/*    Global Variables Referred : None.                                     */
/*                                                                          */
/*    Global Variables Modified : None.                                     */
/*                                                                          */
/*    Exceptions or Operating                                               */
/*    System Error Handling    : None.                                      */
/*                                                                          */
/*    Use of Recursion        : None.                                       */
/*                                                                          */
/*    Returns            : CFA_TRUE-If programming is allowed               */
/****************************************************************************/
UINT1
CfaIsIfEntryProgrammingAllowed (UINT4 u4IfIndex)
{
    if ((CfaIsVipOrSispInterface (u4IfIndex) == CFA_TRUE) ||
        (CfaIsOpenFlowIndex (u4IfIndex) == CFA_TRUE))

    {
        return CFA_FALSE;
    }
    return CFA_TRUE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaIsL3SubIfNum
 *
 *    Description         : This function checks whether the interface
 *                          is a L3 sub-interface or not.
 *
 *    Input(s)            : pi1IfNum - interface Number 
 *
 *    Output(s)           : NONE
 *
 *    Returns             : CFA_SUCCESS-If interface is a L3-Sub interface
 *                          CFA_FAILURE  otherwise.
 *****************************************************************************/
INT1
CfaIsL3SubIfNum (INT1 *pi1IfNum)
{
    UINT2               u2Index = 0;
    INT1               *pi1Ptr;

    if (!(pi1IfNum) || !(*pi1IfNum))
        return FAILURE;

    pi1Ptr = &pi1IfNum[u2Index];

    /* If '.' is present in the interface number 
     * then it is L3Subinterface port*/

    if (CLI_STRSTR (pi1Ptr, ".") != NULL)
    {
        return CFA_SUCCESS;
    }
    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaIsL3SubIfIndex
 *
 *    Description         : This function checks whether the interface
 *                          is a L3 sub-interface or not.
 *
 *    Input(s)            : u4IfIndex - interface Index
 *
 *    Output(s)           : NONE
 *
 *    Returns             : CFA_TRUE-If interface is a L3-Sub interface
 *                          CFA_FALSE otherwise.
 *****************************************************************************/
INT1
CfaIsL3SubIfIndex (UINT4 u4IfIndex)
{
    UINT1               u1RetVal = CFA_FALSE;

    if ((u4IfIndex >= CFA_MIN_L3SUB_IF_INDEX)
        && (u4IfIndex <= CFA_MAX_L3SUB_IF_INDEX))
    {
        u1RetVal = CFA_TRUE;
    }

    return (INT1) u1RetVal;
}

/*****************************************************************************
 *
 *    Function Name       : CfaIsL3SubLogicalPort
 *
 *    Description         : This function checks whether the Logical Port Number
 *                          is a L3 sub-interface Logical Port Number or not.
 *
 *    Input(s)            : Logical Port Number - i4LPortNum
 *
 *    Output(s)           : NONE
 *
 *    Returns             : CFA_TRUE-If Logical Port Number is a L3 Sub-interface Logical Port Number
 *                          CFA_FALSE otherwise.
 *****************************************************************************/

INT1
CfaIsL3SubLogicalPort (INT4 i4LPortNum)
{
    if ((i4LPortNum <= SYS_DEF_MAX_L3SUB_IFACES) && (i4LPortNum > 0))

    {
        return CFA_TRUE;
    }
    return CFA_FALSE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name       : CfaCopyPhyPortPropToLogicalInterface            */
/*                                                                          */
/*    Description         : This function will inherit the necessary        */
/*                          properties like duplexity; speed etc., for all  */
/*                          the logical secondary ports that are mapped over*/
/*                          the given physical index. This needs to be      */
/*                          invoked, whenever there is a change in the SISP */
/*                          enable physical.s port properties.              */
/*                                                                          */
/*    Input(s)            : u4Index - interface index                       */
/*                                                                          */
/*    Output(s)           : NONE                                            */
/*                                                                          */
/*    Global Variables Referred : None.                                     */
/*                                                                          */
/*    Global Variables Modified : None.                                     */
/*                                                                          */
/*    Exceptions or Operating                                               */
/*    System Error Handling    : None.                                      */
/*                                                                          */
/*    Use of Recursion        : None.                                       */
/*                                                                          */
/*    Returns            : CFA_SUCCESS/CFA_FAILURE                          */
/****************************************************************************/
INT4
CfaCopyPhyPortPropToLogicalInterface (UINT4 u4IfIndex)
{
    UINT4               au4SispPorts[SYS_DEF_MAX_NUM_CONTEXTS];
    UINT4               u4PortCnt = 0;
    UINT4               u4PortNum = 0;
    UINT4               u4TempPortCnt = 0;
    INT4                i4BrgPortType = 0;

    MEMSET (au4SispPorts, 0, sizeof (au4SispPorts));

    /* Get the  SISP API to get the list of logical interfaces running over
     * this physical interface
     * */
    if (CfaVcmSispGetSispPortsOfPhysicalPort (u4IfIndex, VCM_FALSE,
                                              (VOID *) au4SispPorts,
                                              &u4PortCnt) == VCM_FAILURE)
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    for (; u4TempPortCnt < u4PortCnt; u4TempPortCnt++)
    {
        u4PortNum = au4SispPorts[u4TempPortCnt];

        CFA_CDB_IF_OPER (u4PortNum) = CFA_CDB_IF_OPER (u4IfIndex);

        CFA_CDB_IF_SPEED (u4PortNum) = CFA_CDB_IF_SPEED (u4IfIndex);

        CFA_CDB_IF_ADMIN_STATUS (u4PortNum) =
            CFA_CDB_IF_ADMIN_STATUS (u4IfIndex);

        CFA_CDB_IF_HIGHSPEED (u4PortNum) = CFA_CDB_IF_HIGHSPEED (u4IfIndex);

        CFA_CDB_IF_DUPLEXSTATUS (u4PortNum) =
            CFA_CDB_IF_DUPLEXSTATUS (u4IfIndex);

        /* Bridge port type is maintained in two places, CFA and L2IWF
         * Copy to all the places from here */
        CFA_CDB_IF_BRIDGE_PORT_TYPE (u4PortNum) =
            CFA_CDB_IF_BRIDGE_PORT_TYPE (u4IfIndex);

        MEMCPY (&(CFA_CDB_IF_ENTRY (u4PortNum)->au1MacAddr),
                &(CFA_CDB_IF_ENTRY (u4IfIndex)->au1MacAddr), CFA_ENET_ADDR_LEN);
    }

    i4BrgPortType = CFA_CDB_IF_BRIDGE_PORT_TYPE (u4IfIndex);
    CFA_DS_UNLOCK ();

    /* Updating L2IWF database within CFA DS lock may lead to Deadlock 
     * situation. So, update L2IWF after releasing CFA DS lock */
    for (u4TempPortCnt = 0; u4TempPortCnt < u4PortCnt; u4TempPortCnt++)
    {
        u4PortNum = au4SispPorts[u4TempPortCnt];
        /* Set the physical port's bridge port type for 
         * all logical ports in L2IWF */
        L2IwfPbSetPortType (u4PortNum, i4BrgPortType);
    }
    return CFA_SUCCESS;
}

/***************************************************************************/
/* Function Name    : CfaCopyBridgePortPropToSispPorts                     */
/*                                                                         */
/* Description      : This function used to copy the bridge port property  */
/*                    of a physical or port channel port to all of its     */
/*                    logical ports.                                       */
/*                                                                         */
/* Input(s)         : u4PhyIfIndex  - Physical or Port channel ID.         */
/*                    u1PropType - Bridge port property type               */
/*                    u4PropVal - Bridge port property value               */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : CFA_SUCCESS / CFA_FAILURE                            */
/***************************************************************************/
INT4
CfaCopyBridgePortPropToSispPorts (UINT4 u4PhyIfIndex, UINT1 u1PropType,
                                  UINT4 u4PropVal)
{
    UINT4               au4SispPorts[SYS_DEF_MAX_NUM_CONTEXTS];
    UINT4               u4PortNum = 0;
    UINT4               u4PortCnt = 0;
    UINT4               u4TempPortCnt = 0;

    MEMSET (au4SispPorts, 0, sizeof (au4SispPorts));

    if (CfaVcmSispGetSispPortsOfPhysicalPort (u4PhyIfIndex, VCM_FALSE,
                                              (VOID *) au4SispPorts,
                                              &u4PortCnt) == VCM_FAILURE)
    {
        return CFA_SUCCESS;
    }

    for (; u4TempPortCnt < u4PortCnt; u4TempPortCnt++)
    {
        u4PortNum = au4SispPorts[u4TempPortCnt];

        switch (u1PropType)
        {
            case CFA_PORT_PROP_BRG_TYPE:
                /* Set the bridge port type in L2IWF and indicate to 
                 * higher layer protocols */
                CfaSispSetBrgPortType (u4PortNum, (UINT1) u4PropVal);
                break;
            case CFA_PORT_PROP_OPER_STATUS:
                /* Set the port oper status in L2IWF */
                L2IwfSetBridgePortOperStatus (u4PortNum, (UINT1) u4PropVal);
                /* Higher indication is not given here */
                break;
            default:
                return CFA_SUCCESS;
        }
    }
    return CFA_SUCCESS;
}

/***************************************************************************/
/* Function Name    : CfaSispSetBrgPortType                                */
/*                                                                         */
/* Description      : This function used to set the bridge port type for   */
/*                    the sisp logical interfaces in CFA.                  */
/*                                                                         */
/* Input(s)         : u4IfIndex     - SISP Logical index.                  */
/*                    u1BrgPortType - Bridge Port Type                     */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : CFA_SUCCESS / CFA_FAILURE                            */
/***************************************************************************/
INT4
CfaSispSetBrgPortType (UINT4 u4IfIndex, UINT1 u1BrgPortType)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1OperStatus = CFA_IF_DOWN;

    /* ASSUMPTIONS:-
     * 1) All the common validations like port-channel, port in port-channel,
     *    Vlan related validation should be performed for the appropriate
     *    physical interface, before invoking this function
     *
     * 2) SISP logical interfaces can take the following port types alone
     *    Prop-PNP & PNP
     * */

    /* If the port is not mapped to any context, then don't
     * allow setting of brg port type. */
    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                      &u2LocalPort) == VCM_FAILURE)
    {
        return CFA_FAILURE;
    }

    /* Give port delete indication to the modules that may get affected
     * by bridge port type change. */
    L2IwfPortDeleteIndication (u4IfIndex);
    EoamApiNotifyIfDelete (u4IfIndex);

    CfaSetIfBrgPortType (u4IfIndex, u1BrgPortType);

    /* Give port create indication. On getting the port create msg, the upper 
     * layer modules will get read the port type and update themselves. */
    L2IwfPortCreateIndication (u4IfIndex);
    EoamApiNotifyIfCreate (u4IfIndex);

    CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);

    if (u1OperStatus == CFA_IF_UP)
    {
        EoamApiNotifyIfOperChg (u4IfIndex, u1OperStatus);
        L2IwfPortOperStatusIndication (u4IfIndex, u1OperStatus);
    }

    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CfaSnmpSendTrap                                           */
/*                                                                           */
/* Description  : Routine to send trap to SNMP .                             */
/*                                                                           */
/* Input        : i4TrapId   : Trap Identifier                               */
/*                pi1TrapOid : Pointer to the trap OID                       */
/*                u1OidLen   : OID Length                                    */
/*                pTrapInfo  : Pointer to the trap information.              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
CfaSnmpSendTrap (UINT1 u1TrapId, INT1 *pi1TrapOid, UINT1 u1OidLen,
                 VOID *pTrapInfo)
{
#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid;
    tSNMP_OID_TYPE     *pOid;
    UINT1               au1Buf[CFA_OBJECT_NAME_LEN];
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    UINT4              *pu4IfIndex = NULL;
    tSNMP_COUNTER64_TYPE SnmpCounter64Type;

    UNUSED_PARAM (u1OidLen);
    MEMSET (au1Buf, 0, CFA_OBJECT_NAME_LEN);
    pEnterpriseOid = (tSNMP_OID_TYPE *) SNMP_AGT_GetOidFromString (pi1TrapOid);
    if (pEnterpriseOid == NULL)
    {
        return;
    }
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    u4SpecTrapType = (UINT4) u1TrapId;
    SnmpCounter64Type.msn = 0;
    SnmpCounter64Type.lsn = 0;

    switch (u1TrapId)
    {

        case CFA_IF_CREATE_TRAP:
        case CFA_IF_DELETE_TRAP:

            pu4IfIndex = (UINT4 *) pTrapInfo;
            SPRINTF ((char *) au1Buf, "ifMainIndex");
            pOid = (tSNMP_OID_TYPE *) CfaMakeObjIdFromDotNew ((UINT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) *pu4IfIndex,
                                      NULL, NULL, SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pStartVb = pVbList;
            break;

        default:
            SNMP_FreeOid (pEnterpriseOid);
            return;
    }

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                              pStartVb);

#else /* SNMP_3_WANTED */
    UNUSED_PARAM (u1TrapId);
    UNUSED_PARAM (*pi1TrapOid);
    UNUSED_PARAM (u1OidLen);
    UNUSED_PARAM (pTrapInfo);
#endif /* SNMP_3_WANTED */

}

/*****************************************************************************/
/*                                                                           */
/* Function     : CfaIncMsrForTunlIfTable                                    */
/*                                                                           */
/* Description  : Routine to send notification to SNMP                       */
/*                                                                           */
/* Input        : u4CxtId, i4AddrType, LocalAddress,                         */
/*                RemoteAddress, i4EncapMethod, i4ConfigId, cDatatype,       */
/*                pSetVal, pu4ObjectId, u4OIdLen, IsRowStatus                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
CfaIncMsrForTunlIfTable (UINT4 u4CxtId, INT4 i4AddrType,
                         tSNMP_OCTET_STRING_TYPE * LocalAddress,
                         tSNMP_OCTET_STRING_TYPE * RemoteAddress,
                         INT4 i4EncapMethod, INT4 i4ConfigId,
                         CHR1 cDatatype, VOID *pSetVal,
                         UINT4 *pu4ObjectId, UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    tSNMP_OCTET_STRING_TYPE *pSetString;
    UINT4               u4SeqNum = 0;
    UINT4               u4SetValue = 0;
    INT4                i4SetValue = 0;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);

    MEMSET (&SnmpNotifyInfo, 0, sizeof (SnmpNotifyInfo));
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = CfaLock;
    SnmpNotifyInfo.pUnLockPointer = CfaUnlock;
    SnmpNotifyInfo.u4Indices = 6;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %s %i %i %i",
                              u4CxtId, i4AddrType, LocalAddress, RemoteAddress,
                              i4EncapMethod, i4ConfigId, i4SetValue));
            break;

        case 'u':
            u4SetValue = *(UINT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %s %i %i %u",
                              u4CxtId, i4AddrType, LocalAddress, RemoteAddress,
                              i4EncapMethod, i4ConfigId, u4SetValue));

            break;

        case 's':
            pSetString = (tSNMP_OCTET_STRING_TYPE *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %s %i %i %s",
                              u4CxtId, i4AddrType, LocalAddress, RemoteAddress,
                              i4EncapMethod, i4ConfigId, pSetString));

            break;

        default:
            break;

    }
#ifndef ISS_WANTED
    UNUSED_PARAM (u4CxtId);
    UNUSED_PARAM (i4AddrType);
    UNUSED_PARAM (LocalAddress);
    UNUSED_PARAM (RemoteAddress);
    UNUSED_PARAM (i4EncapMethod);
    UNUSED_PARAM (i4ConfigId);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : CfaValIfTypeProtoDenyTblIndex                        */
/*                                                                           */
/* Description        : This function validates the index of the IfType      */
/*                      Protocol Deny table                                  */
/*                                                                           */
/* Input(s)           : i4ContextId   - ContextId                            */
/*                      i4ifType      - Interface type of port               */
/*                      i4BrgPortType - Bridge Port Type of Port             */
/*                      i4Protocol    - Protocol Id                          */
/*                                                                           */
/* Output             : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaValIfTypeProtoDenyTblIndex (INT4 i4ContextId, INT4 i4IfType, INT4
                               i4BrgPortType, INT4 i4Protocol)
{
    if ((i4ContextId < 0) || (i4ContextId >= SYS_DEF_MAX_NUM_CONTEXTS))
    {
        return CFA_FAILURE;
    }

    if ((i4IfType != CFA_ENET) &&
        (i4IfType != CFA_PROP_VIRTUAL_INTERFACE) &&
        (i4IfType != CFA_LAGG) &&
        (i4IfType != CFA_PIP) && (i4IfType != CFA_BRIDGED_INTERFACE))
    {
        CLI_SET_ERR (CLI_CFA_INVALID_IFTYPE_DENY);
        return CFA_FAILURE;
    }

    if ((i4BrgPortType < 0) || (i4BrgPortType >= CFA_MAX_BRGPORTTYPE_PROT_DENY))
    {
        return CFA_FAILURE;
    }

    if (i4BrgPortType == CFA_PROVIDER_EDGE_PORT)
    {
        /* Provider Edge Port is an internal port known only to STP, and if 
         * CEP is denied in STP, then PEP will not be internally created by 
         * STP, hence not allow this port to be denied 
         */
        return CFA_FAILURE;
    }

    if ((i4Protocol < 0) || (i4Protocol >= CFA_MAX_PROT_DENY))
    {
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaInterfaceMtuChangeIndication
 *
 *    Description          : This function validates the ifIndex and calls 
 *                           CfaInterfaceStatusChangeIndication to upate the 
 *                           interface status.
 *    Input(s)             : u4IfIndex - Interface index
 *                           u4Mtu     - Mtu value configured
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.
 *****************************************************************************/
VOID
CfaInterfaceMtuChangeIndication (UINT4 u4IfIndex, UINT4 u4Mtu)
{
    tCfaExtTriggerMsg  *pExtTriggerMsg;

    if ((pExtTriggerMsg = (tCfaExtTriggerMsg *)
         MemAllocMemBlk (ExtTriggerMsgPoolId)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaInterfaceMtuChangeIndication - "
                  "Mempool allocation failed %d FAIL.\n", u4IfIndex);
        return;
    }

    pExtTriggerMsg->uExtTrgMsg.IfUpdateMsg.u4IfIndex = u4IfIndex;
    pExtTriggerMsg->uExtTrgMsg.IfUpdateMsg.u4Mtu = u4Mtu;
    pExtTriggerMsg->i4MsgType = CFA_IF_MTU_UPDATE_CHANGE_MSG;

    if (OsixQueSend (CFA_EXT_TRIGGER_QID, (UINT1 *) &pExtTriggerMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaInterfaceMtuChangeIndication - "
                  "Message Enqueue to CFA on interface %d FAIL.\n", u4IfIndex);

        MemReleaseMemBlock (ExtTriggerMsgPoolId, (UINT1 *) pExtTriggerMsg);
        return;
    }

    /* send an explicit event to the task */
    OsixEvtSend (CFA_TASK_ID, CFA_EXT_TRIGGER_EVENT);

    CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
              "Exiting CfaInterfaceMtuChangeIndication -"
              "Pkt Enqueue to CFA on interface %d SUCCESS.\n", u4IfIndex);
    return;
}

/*****************************************************************************
 *
 *    Function Name        : CfaSetIfMainMtu
 *
 *    Description          : This function configures the interface MTU
 *
 *    Input(s)             : u4IfIndex - Interface index
 *                           u4Mtu     - Mtu value configured
 *
 *    Output(s)            : None.
 *
 *    Returns              : CFA_SUCCESS/CFA_FAILURE
 *****************************************************************************/
INT4
CfaSetIfMainMtu (UINT4 u4IfIndex, UINT4 u4Mtu)
{
    UINT1               u1IfType = CFA_NONE;
    UINT1               u1IsActive = CFA_FALSE;
    UINT1               u1IsAlreadyRegWithIp = CFA_FALSE;
    UINT4               u4IfMtu = u4Mtu;
    tStackInfoStruct   *pScanNode = NULL;

    /* Null check has to be provided here to avoid crash as MSR does not call
     * Test routine */
    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        if (CFA_IF_ENTRY (u4IfIndex) == NULL)
        {
            return CFA_FAILURE;
        }
    }
    else
    {
        return CFA_FAILURE;
    }

    CfaGetIfMtu (u4IfIndex, &u4IfMtu);
    if (u4IfMtu != u4Mtu)
    {

        if (CFA_IF_RS (u4IfIndex) == CFA_RS_ACTIVE)
        {
            pScanNode =
                (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY ((UINT2)
                                                              u4IfIndex);
            CFA_CHECK_IF_NULL (pScanNode, CFA_FAILURE);
            u1IsActive = CFA_TRUE;
            if ((CFA_IF_STACK_IFINDEX (pScanNode) == CFA_NONE) &&
                (CFA_IF_STACK_STATUS (pScanNode) == CFA_RS_ACTIVE) &&
                (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX))
            {
                u1IsAlreadyRegWithIp = CFA_TRUE;
            }
        }

        CfaGetIfType (u4IfIndex, &u1IfType);

#ifdef NPAPI_WANTED
        if ((u1IfType == CFA_ENET) || (u1IfType == CFA_LAGG))
        {
            if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
            {
                /* Program the new MTU in h/w */
                if (FNP_FAILURE == CfaFsCfaHwSetMtu (u4IfIndex, u4Mtu))
                {
                    return CFA_FAILURE;
                }
            }
        }
#endif
        if (u1IfType == CFA_ENET)
        {
#ifdef LA_WANTED
            LaUpdateRestoreMtu ((UINT2) u4IfIndex, u4Mtu);
#endif
        }

        CfaSetIfMtu (u4IfIndex, u4Mtu);

        switch (u1IfType)
        {
            case CFA_PIP:
            case CFA_BRIDGED_INTERFACE:
            case CFA_TUNNEL:
            case CFA_ENET:
            case CFA_LAGG:
            case CFA_L3IPVLAN:
                /* While configuring for logical VLAN interfaces, care
                 * should be taken to, configure this value as the
                 * lowest of the MTU values of the member ports.
                 * Administrator also have to take care of dynamically 
                 * added ports.
                 */

                if (u1IsActive == CFA_TRUE)
                {
                    if (u1IsAlreadyRegWithIp == CFA_TRUE)
                    {
                        if (CfaIfmConfigNetworkInterface (CFA_NET_IF_UPD,
                                                          u4IfIndex, 0,
                                                          NULL) != CFA_SUCCESS)
                        {
                            return CFA_FAILURE;
                        }
                    }
                }
                break;

#ifdef MPLS_WANTED
            case CFA_MPLS:
#endif /* MPLS_WANTED */
            case CFA_TELINK:
                break;

            default:
                return CFA_FAILURE;    /* not expected */

        }

    }

    return CFA_SUCCESS;
}

/****************************************************************************
 *  Function    :  CfaUtilDeletePortChannelInterface
 *
 *  Description :  This function is used to delete the Port Channel interface
 *                 using the nmh  routine which is used to trigger the mib object
 *                                deletion in the MSR Tree if incremental save enabled.
 *
 * Input       :  u4IfIndex         - Index of the Port channel interface
 *
 * Output      :  None
 *
 * Returns     :  CFA_SUCCESS or CFA_FAILURE
 *****************************************************************************/
INT4
CfaUtilDeletePortChannelInterface (UINT4 u4IfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetIfMainRowStatus (u4IfIndex, DESTROY);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CFA_FAILURE);
    }

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetVlanIfIndex
 *
 *    Description         : This function gives the packet to VLAN module for
 *                          getting the VLAN ID. It identifies the associated
 *                          VLAN interface index and returns it.
 *
 *    Input(s)            : u4IfIndex - Incoming physical port's interface index
 *                          pBuf      - Incoming data buffer.
 *                          pVlanId   - Pointer to Vlan Id. 
 *
 *    Returns            :  VLAN interface index if success. Else, returns the 
 *                          invalid interface index.
 *****************************************************************************/

UINT4
CfaGetVlanIfIndex (UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER * pBuf,
                   tVlanIfaceVlanId * pVlanId)
{
    tVlanIfaceVlanId    VlanId = 0;
    UINT2               u2AggId;
    INT4                i4RetVal;
    UINT4               u4VlanIfIndex = CFA_INVALID_INDEX;
    UINT4               u4L2CxtId = 0;
    BOOL1               bResult = OSIX_FALSE;

    if (L2IwfGetPortChannelForPort (u4IfIndex, &u2AggId) == L2IWF_SUCCESS)
    {
        u4IfIndex = u2AggId;
    }

    if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4L2CxtId) == VCM_FAILURE)
    {
        return CFA_INVALID_INDEX;
    }

    i4RetVal = VlanIdentifyVlanId (pBuf, u4IfIndex, &VlanId);

    *pVlanId = VlanId;

    if (i4RetVal == VLAN_FORWARD)
    {
        OSIX_BITLIST_IS_BIT_SET (gSecVlanList, VlanId, sizeof (tSecVlanList),
                                 bResult);
        if (bResult == OSIX_TRUE)
        {
            u4VlanIfIndex = gu4SecIvrIfIndex;
        }
        else
        {
            u4VlanIfIndex = CfaGetVlanInterfaceIndexInCxt (u4L2CxtId, VlanId);

            if (u4VlanIfIndex == CFA_INVALID_INDEX)
            {
                /* If interface index is not obtained in CFA_L3IP_VLAN Type
                 * check if the vlan is present in any of the L3Subinterface*/
                u4VlanIfIndex = CfaGetL3SubIfVlanIndexInCxt (u4L2CxtId, VlanId);
            }

        }

    }

    return u4VlanIfIndex;
}

/*****************************************************************************
 *
 *    Function Name       :CfaUtilTxPktOnVlanMemberPorts
 *
 *    Description         :This routine transmits the given Frame on the member
 *                         ports of the given Vlan.
 *
 *    Input(s)            : pu1DataBuf  - Pointer to the frame buffer
 *                          u4PktSize   - Size of the frame
 *                          VlanId      - Vlan on which the frame is to be
 *                                        transmitted
 *                          bBcast      - Whether this frame is to be
 *                                        broadcasted
 *
 *    Output(s)           : None
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/ CFA_FAILURE.
 *****************************************************************************/

INT4
CfaUtilTxPktOnVlanMemberPorts (UINT1 *pu1DataBuf, tVlanId VlanId, BOOL1 bBcast,
                               UINT4 u4PktSize)
{
    tMacAddr            DestAddr;
    tCfaVlanInfo        VlanInfo;
    UINT4               u4BridgeMode;
#ifdef NPAPI_WANTED
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2EtherType = 0;
    UINT1               u1PortFlag;
#endif
    UINT4               u4OutPort;
    BOOL1               bIsTag;
    tPortList          *pTagPorts = NULL;
    tPortList          *pUnTagPorts = NULL;
    INT4                i4RetVal;
    UINT1              *pu1TempBuf = NULL;
    UINT1               TempDataBuf[CFA_ENET_MIN_UNTAGGED_FRAME_SIZE];
    UINT4               u4MinFrameMtu = 0;
    INT4                i4RetStat = CFA_SUCCESS;
    UINT1               u1PaddingDone = CFA_FALSE;
    MEMCPY (DestAddr, pu1DataBuf, CFA_ENET_ADDR_LEN);

    VlanInfo.u2VlanId = VlanId;
#ifdef NPAPI_WANTED
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
#ifdef MBSM_WANTED
        /* If the packet belongs to STACK IVR, then send the
         * packet via stack port using ATP*/
        if (VlanId == CFA_DEFAULT_STACK_VLAN_ID)
        {
            if (CfaMbsmNpTxOnStackInterface (pu1DataBuf, u4PktSize) ==
                FNP_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaUtilTxPktOnVlanMemberPorts - "
                         "Unable to tx on stack interface.\n");
                return CFA_FAILURE;
            }
            return CFA_SUCCESS;
        }
#endif
    }
#endif
    pTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaUtilTxPktOnVlanMemberPorts: "
                 "Error in allocating memory for pTagPorts\r\n");
        return CFA_FAILURE;
    }
    MEMSET (pTagPorts, 0, sizeof (tPortList));
    pUnTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pUnTagPorts == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaUtilTxPktOnVlanMemberPorts: "
                 "Error in allocating memory for pUnTagPorts\r\n");
        return CFA_FAILURE;
    }
    MEMSET (pUnTagPorts, 0, sizeof (tPortList));

    i4RetVal = VlanIvrGetTxPortOrPortList (DestAddr, VlanId, (UINT1) bBcast,
                                           &u4OutPort, &bIsTag, *pTagPorts,
                                           *pUnTagPorts);

    if (i4RetVal == VLAN_FORWARD)
    {
        /*ASSUMPTION: The Packet reached here will be untagged always.
         * Since bcoz this thread is called only for the outgoing packet
         * from IP.*/

        u4MinFrameMtu = CFA_ENET_MIN_UNTAGGED_FRAME_SIZE;

        if (u4PktSize < u4MinFrameMtu)
        {
            pu1TempBuf = pu1DataBuf;
            MEMSET (TempDataBuf, 0, u4MinFrameMtu);
            MEMCPY (TempDataBuf, pu1DataBuf, u4PktSize);
            pu1DataBuf = &TempDataBuf[0];
            u4PktSize = u4MinFrameMtu;
            u1PaddingDone = CFA_TRUE;
        }

        CfaTxFillVlanInfo (DestAddr, u4OutPort,
                           bIsTag, *pTagPorts, *pUnTagPorts, &VlanInfo);
        if (L2IwfGetBridgeMode (L2IWF_DEFAULT_CONTEXT, &u4BridgeMode) ==
            L2IWF_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaGddWrite - "
                     "L2IWF-GetBridgeMode -  FAILURE.\n");
            i4RetStat = CFA_FAILURE;
        }
        else
        {
#ifdef NPAPI_WANTED
            if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
            {
                if ((u4BridgeMode == L2IWF_CUSTOMER_BRIDGE_MODE) ||
                    (u4BridgeMode == L2IWF_PROVIDER_BRIDGE_MODE))
                {
                    if (CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo)
                        != FNP_SUCCESS)
                    {
                        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                 "Error in CfaGddWrite - "
                                 "Unsuccessful Driver Write -  FAILURE.\n");
                        i4RetStat = CFA_FAILURE;
                    }
                }
                else if ((u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE) ||
                         (u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE))
                {
                    if (u4OutPort != VLAN_INVALID_PORT)
                    {
                        if (CfaHwL3VlanIntfWrite
                            (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                        {
                            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                     "Error in CfaGddWrite - "
                                     "Unsuccessful Driver Write -  FAILURE.\n");
                            i4RetStat = CFA_FAILURE;
                        }
                    }
                    else
                    {
                        for (u2ByteInd = 0; u2ByteInd < BRG_PORT_LIST_SIZE;
                             u2ByteInd++)
                        {
                            if ((*pTagPorts)[u2ByteInd] == 0)
                            {
                                continue;
                            }

                            u1PortFlag = (*pTagPorts)[u2ByteInd];

                            for (u2BitIndex = 0;
                                 ((u2BitIndex < BITS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
                            {
                                if ((u1PortFlag & 0x80) != 0)
                                {
                                    VlanInfo.u2PktType = CFA_NP_KNOWN_UCAST_PKT;

                                    VlanInfo.unPortInfo.TxUcastPort.u2TxPort =
                                        (UINT2) ((u2ByteInd * BITS_PER_BYTE) +
                                                 u2BitIndex + 1);

                                    u4OutPort =
                                        VlanInfo.unPortInfo.TxUcastPort.
                                        u2TxPort;

#ifdef VLAN_WANTED
                                    VlanGetPortEtherType (u4OutPort,
                                                          &u2EtherType);
#endif
                                    VlanInfo.unPortInfo.TxUcastPort.
                                        u2EtherType = u2EtherType;

                                    VlanInfo.unPortInfo.TxUcastPort.u1Tag =
                                        CFA_NP_TAGGED;

                                    CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize,
                                                          VlanInfo);
                                }
                                u1PortFlag = (UINT1) (u1PortFlag << 1);
                            }
                        }

                        VlanInfo.u2PktType = CFA_NP_UNKNOWN_UCAST_PKT;

                        MEMSET (&VlanInfo.unPortInfo.TxPorts.pTagPorts, 0,
                                sizeof (tPortList));
                        /*
                         * In 802.1ad Bridges, for untagged ports, the tag ethertype
                         * need not be filled in based on the port ethertype.
                         * Hence calling the API for packet transmission on L3
                         * interfaces directly.
                         */
                        if (CfaHwL3VlanIntfWrite
                            (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                        {
                            i4RetStat = CFA_FAILURE;
                        }
                    }
                }
            }
#else
            if (CfaL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo) !=
                CFA_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddWrite - "
                         "Unsuccessful Driver Write -  FAILURE.\n");
                i4RetStat = CFA_FAILURE;

            }
#endif
        }
        if (u1PaddingDone == CFA_TRUE)
        {
            pu1DataBuf = pu1TempBuf;
        }
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        return i4RetStat;
    }
    FsUtilReleaseBitList ((UINT1 *) pTagPorts);
    FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);

    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaIsVipOrSispInterface
 *
 *    Description         : This function checks whether the interface 
 *                          is a VIP or SISP interface or not.
 *
 *    Input(s)            : u4Index - interface index
 *
 *    Output(s)           : NONE
 *
 *    Returns             : CFA_TRUE-If interface is a VIP/SISP interface
 *                          CFA_FALSE otherwise.
 *****************************************************************************/
UINT1
CfaIsVipOrSispInterface (UINT4 u4IfIndex)
{
    UINT1               u1RetVal = CFA_FALSE;

    if ((u4IfIndex >= CFA_MIN_SISP_IF_INDEX)
        && (u4IfIndex <= CFA_MAX_VIP_IF_INDEX))
    {
        u1RetVal = CFA_TRUE;
    }
    return u1RetVal;
}

/*****************************************************************************
 *
 *    Function Name       : CfaIsOpenFlowIndex
 *
 *    Description         : This function checks whether the interface 
 *                          is a Openflow interface or not.
 *
 *    Input(s)            : u4Index - interface index
 *
 *    Output(s)           : NONE
 *
 *    Returns             : CFA_TRUE-If interface is a OPENFLOW interface
 *                          CFA_FALSE otherwise.
 *****************************************************************************/
UINT1
CfaIsOpenFlowIndex (UINT4 u4IfIndex)
{
    UINT1               u1RetVal = CFA_FALSE;

    if ((u4IfIndex >= CFA_MIN_OF_IF_INDEX)
        && (u4IfIndex <= CFA_MAX_OF_IF_INDEX))
    {
        u1RetVal = CFA_TRUE;
    }
    return u1RetVal;
}

/****************************************************************************
 *  Function    :  CfaDoIPSrcGuardValidation
 *
 *  Description :  This function is used to get the VLAN ID information from
 *                 VLAN module and proceed on the IP source guard validation.
 *
 *  Input       :  pBuf         - Pointer to Source CRU buffer
 *                 u4IfIndex    - Ingress interface index
 *                 pu1SrcAddr   - Source MAC address
 *
 *  Output      :  None
 *
 *  Returns     :  CFA_SUCCESS or CFA_FAILURE
 *****************************************************************************/
PUBLIC INT4
CfaDoIPSrcGuardValidation (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                           tEnetV2Header * pEthHdr)
{
    t_IP_HEADER        *pIpHdr = NULL;
    t_IP_HEADER         TmpIpHdr;
    UINT4               u4SrcIp = 0;
    tVlanTag            VlanTag;
    UINT4               u4TagOffset = 0;
    UINT4               u4L2CxtId = 0;
    UINT1               u1EgrOption = 0;
    UINT1               u1Result = OSIX_FALSE;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IWF,
              "Entering CfaDoIPSrcGuardValidation for interface %d.\n",
              u4IfIndex);

    /* Required inputs for IP source guard validation is source MAC address, 
     * Outer VLAN ID, Source IP address and Ingress interface index. 
     *
     * IP source guard validation is not applicable for DHCP packets. 
     * IP source guard validation applies only for non-DHCP packets.
     *
     * Before invoking the IP source guard validation routines, required 
     * validation will be done. If it fails before IP source guard validation 
     * routines, this function returns success to proceed further 
     * processing. 
     */

    /* To get whether the incoming packet is a DHCP packet or not */
    if (L2IwfIsDhcpPacket (pBuf, &u1Result, u4IfIndex) == L2IWF_SUCCESS)
    {
        /* As IP source guard is not applicable for DHCP packets, 
         * allows for further processing of IP packets */
        if (u1Result == OSIX_TRUE)
        {
            CFA_DBG1 (CFA_TRC_ALL | CFA_TRC_ALL_TRACK, CFA_IWF,
                      "Exiting CfaDoIPSrcGuardValidation - DHCP packet skipped"
                      " in IP source guard validation for port : %d \n",
                      u4IfIndex);
            return CFA_SUCCESS;
        }
    }

    if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4L2CxtId) == VCM_FAILURE)
    {
        return CFA_SUCCESS;
    }

    /* Gets the Outer VLAN ID from VLAN module. If it fails, retuns 
     * success as this function is applicable only for IP source 
     * guard validation routines. */
    if (VlanGetVlanInfoFromFrame (u4L2CxtId, (UINT2) u4IfIndex,
                                  pBuf, &VlanTag, &u1EgrOption, &u4TagOffset)
        == VLAN_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                  "Exiting CfaDoIPSrcGuardValidation - VLAN Identification"
                  "on port %d - FAILURE \n", u4IfIndex);
        return CFA_SUCCESS;
    }

    /* To get the source IP address from the incoming frame */
    pIpHdr = (t_IP_HEADER *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear
        (pBuf, ((u4TagOffset + VLAN_TYPE_OR_LEN_SIZE)), IP_HDR_LEN);

    if (pIpHdr == NULL)
    {
        pIpHdr = &TmpIpHdr;
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pIpHdr,
                                   ((u4TagOffset + VLAN_TYPE_OR_LEN_SIZE)),
                                   IP_HDR_LEN);
    }
    /* MEMCPY done here to avoid alignment issue in ARM processor */
    else
    {
        MEMCPY ((UINT1 *) (&TmpIpHdr), pIpHdr, IP_HDR_LEN);
        pIpHdr = &TmpIpHdr;
    }

    u4SrcIp = OSIX_NTOHL (pIpHdr->u4Src);

    /* IP source guard validation routine */
    if (IpdbApiDoIpSrcGuardValidation (u4L2CxtId,
                                       VlanTag.OuterVlanTag.u2VlanId,
                                       pEthHdr->au1SrcAddr, u4SrcIp,
                                       u4IfIndex) == OSIX_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                  "Exiting CfaDoIPSrcGuardValidation - IP source guard "
                  "validation failure on interface index: %d \n", u4IfIndex);
        return CFA_FAILURE;
    }

    CFA_DBG1 (CFA_TRC_ALL, CFA_IWF,
              "Exiting CfaDoIPSrcGuardValidation with SUCCESS for "
              "interface %d.\n", u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaFreeILanListNodes                                 */
/*                                                                           */
/* Description        : This function is to free the ILan List nodes         */
/*                      to gCfaILanPortPoolId.                               */
/*                                                                           */
/* Input(s)           : pList - Pointer to the ILan Port List.               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
CfaFreeILanListNodes (tTMO_SLL * pList)
{
    TMO_SLL_FreeNodes (pList, gCfaILanPortPoolId);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : CfaGetNextIvrIfIndex                             */
/*                                                                           */
/*    Description         : This function returns the next valid IVR interfac*/
/*                          index                                            */
/*                                                                           */
/*    Input(s)            : u4IfIndex                                        */
/*                                                                           */
/*    Output(s)           : p4TmpIfIndex                                     */
/*                                                                           */
/*    Returns             : CFA_SUCCESS / CFA_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT4
CfaGetNextIvrIfIndex (UINT4 u4IfIndex, UINT4 *p4TmpIfIndex)
{
    UINT4               u4Index;
    UINT4               u4StartIndex = u4IfIndex;

    if (u4IfIndex < CFA_MIN_IVR_IF_INDEX)
    {
        u4StartIndex = CFA_MIN_IVR_IF_INDEX - 1;
    }

    CFA_DS_LOCK ();

    u4Index = u4StartIndex + 1;
    CFA_CDB_SCAN (u4Index, CFA_MAX_IVR_IF_INDEX, CFA_L3IPVLAN)
    {
        if (CFA_CDB_IS_INTF_VALID (u4Index) == CFA_TRUE)
        {
            *p4TmpIfIndex = u4Index;
            CFA_DS_UNLOCK ();
            return CFA_SUCCESS;
        }
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaUtilRegisterCallBack                              */
/*                                                                           */
/* Description        : This function registers the call backs from          */
/*                      application                                          */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gaCfaCustCallBack                                    */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaUtilRegisterCallBack (UINT4 u4Event, tFsCbInfo * pFsCbInfo)
{
    INT4                i4RetVal = CFA_SUCCESS;

    switch (u4Event)
    {
        case CFA_CUST_FWL_CHECK_EVENT:
            CFA_UTIL_CALL_BACK[u4Event].pCfaCustFwlCheck =
                pFsCbInfo->pCfaCustFwlCheck;
            break;

        default:
            i4RetVal = CFA_FAILURE;
            break;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : CfaUtilCallBack                                      */
/*                                                                           */
/* Description        : This function processes the callback events invoked  */
/*                      in the program.                                      */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      u4IfIndex - Interface index of the Interface         */
/*                      which is being turned up                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gaFwlUtilCallBack                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaUtilCallBack (UINT4 u4Event, UINT4 u4IfIndex)
{
    INT4                i4RetVal = CFA_SUCCESS;

    switch (u4Event)
    {
        case CFA_CUST_FWL_CHECK_EVENT:
            if (NULL != (CFA_UTIL_CALL_BACK[u4Event]).pCfaCustFwlCheck)
            {
                if (ISS_FAILURE ==
                    CFA_UTIL_CALL_BACK[u4Event].pCfaCustFwlCheck (u4IfIndex))
                {
                    i4RetVal = CFA_FAILURE;
                }
            }
            break;
        default:
            i4RetVal = CFA_FAILURE;
            break;
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : CfaUtilGetInterfaceIndexFromAlias                    */
/*                                                                           */
/* Description        : This function is to get the Interface index from the */
/*                      Alias name                                           */
/*                                                                           */
/* Input(s)           : pu1IfName - pointer containing the alias name        */
/*                                                                           */
/* Output(s)          : pu4IfIndex - pointer containing the IfIndex          */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaUtilGetInterfaceIndexFromAlias (UINT1 *pu1IfName, UINT4 *pu4IfIndex)
{
    INT1                ai1SlotAndPort[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4IfType = 0;
    UINT4               u4IfSubType = 0;
    UINT4               u4SubCount = 0;
    UINT4               u4IfNameLen = 0;
    UINT1               u1IfNameMatch = OSIX_FALSE;

    MEMSET (ai1SlotAndPort, 0, CFA_MAX_PORT_NAME_LENGTH);

    if ((pu1IfName == NULL) || (pu4IfIndex == NULL))
    {
        return CFA_FAILURE;
    }

    for (u4IfType = 0; u4IfType < CLI_MAX_IFTYPES; u4IfType++)
    {
        for (u4SubCount = 0; u4SubCount < CLI_MAX_IFSUBTYPES; u4SubCount++)
        {
            /* If the strlength of the Interface name in the array asCfaIfaces 
             * is zero, (i.e. Interface name is NULL )then break the loop*/
            if ((u4IfNameLen = STRLEN
                 (asCfaIfaces[u4IfType][u4SubCount].au1IfName)) == 0)
            {
                continue;
            }
            if (STRNCMP (asCfaIfaces[u4IfType][u4SubCount].au1IfName,
                         pu1IfName, u4IfNameLen) == 0)
            {
                u4IfType = asCfaIfaces[u4IfType][u4SubCount].u4IfType;
                u4IfSubType = u4SubCount;
                u1IfNameMatch = OSIX_TRUE;
                break;
            }
        }
        if (u1IfNameMatch == OSIX_TRUE)
        {
            break;
        }
    }

    if (u1IfNameMatch == OSIX_TRUE)
    {
        STRNCPY (ai1SlotAndPort, pu1IfName + u4IfNameLen,
                 MEM_MAX_BYTES (STRLEN (pu1IfName + u4IfNameLen),
                                CFA_MAX_PORT_NAME_LENGTH));
        ai1SlotAndPort[STRLEN (pu1IfName + u4IfNameLen)] = '\0';

        if (CfaCliGetIndexFromSubType (u4IfType, u4IfSubType, ai1SlotAndPort,
                                       pu4IfIndex) == CLI_SUCCESS)
        {
            return CFA_SUCCESS;
        }
    }

    return CFA_FAILURE;
}

/*****************************************************************************/
/*  Function Name   : CfaCheckSecForOutboundTraffic                          */
/*  Description     : This function qualifies the frames for security        */
/*                    processing.                                            */
/*                    (i) The frames egressing on bridged interfaces are     */
/*                        qualified when                                     */
/*                        -> VLAN id is security VLAN                        */
/*                        -> Security bridging is enabled.                   */
/*                        -> Protocol is CFA_ENET_IPV4                       */
/*                        -> The bridged interface is of WAN type.           */
/*                    (ii) The frames egressing on L3 VLAN interfaces are    */
/*                         qualified when                                    */
/*                        -> Security bridging is enabled.                   */
/*                        -> L3 VLAN index is security IVR interface         */
/*                        -> The destination MAC is learnt on any of the     */
/*                           port of the security VLAN list.                 */
/*                        -> The learnt port is WAN type.                    */
/*  Input(s)        : pBuf - Input buffer.                                   */
/*                    u4IfIndex - Input interface index.                     */
/*  Output(s)       : *pu1Result - If this packet has ingressed through      */
/*                    a security VLAN and security bridged mode is enabled,  */
/*                    OSIX_TRUE is set.                                      */
/*                    OSIX_FALSE otherwise.                                  */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  gSecVlanList , gu4SecModeForBridgedTraffic*/
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
VOID
CfaChkSecForOutBoundBridgedFrame (tCRU_BUF_CHAIN_HEADER * pBuf,
                                  UINT4 u4IfIndex, UINT1 *pu1Result)
{
    tMacAddr            MacAddr;
    INT1                i1RetValue = 0;
    UINT1              *pSecVlanList = NULL;
    UINT4               u4SecMode = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2Protocol = 0;
    UINT2               u2LenOrType = 0;
    UINT2               u2Port = 0;
    UINT2               u2TCI = 0;
    UINT1               u1Flag = 0;
    UINT1               u1NwType = 0;
    UINT1               u1Status = 0;
    UINT1               u1BridgedIfaceStatus = 0;
    BOOL1               bResult = 0;

    UNUSED_PARAM (i1RetValue);

    pSecVlanList = UtilVlanAllocVlanListSize (sizeof (tSecVlanList));
    if (pSecVlanList == NULL)
    {
        return;
    }

    MEMSET (pSecVlanList, 0, sizeof (tSecVlanList));
    *pu1Result = OSIX_FALSE;

    CRU_BUF_Copy_FromBufChain (pBuf, MacAddr, 0, CFA_ENET_ADDR_LEN);
    CfaGetSecurityBridgeMode (&u4SecMode);
    if (u4SecMode == CFA_SEC_BRIDGE_DISABLED)
    {
        UtilVlanReleaseVlanListSize (pSecVlanList);
        return;
    }

    if (CfaGetSecIvrIndex () == CFA_INVALID_INDEX)
    {
        UtilVlanReleaseVlanListSize (pSecVlanList);
        return;
    }
    i1RetValue = CfaGetSecVlanList (pSecVlanList);

    CRU_BUF_Copy_FromBufChain (pBuf,
                               (UINT1 *) &u2LenOrType,
                               VLAN_TAG_OFFSET, CFA_ENET_TYPE_OR_LEN);
    u2LenOrType = OSIX_NTOHS (u2LenOrType);
    CRU_BUF_Copy_FromBufChain (pBuf,
                               (UINT1 *) &u2Protocol,
                               VLAN_TAGGED_HEADER_SIZE, CFA_ENET_TYPE_OR_LEN);
    u2Protocol = OSIX_NTOHS (u2Protocol);

    if (VLAN_PROTOCOL_ID == u2LenOrType)
    {
        CRU_BUF_Copy_FromBufChain (pBuf,
                                   (UINT1 *) &u2TCI,
                                   VLAN_TAG_VLANID_OFFSET,
                                   CFA_ENET_TYPE_OR_LEN);
        /* Isolate the VLAN Id from the TCI field  */
        u2VlanId = (OSIX_NTOHS (u2TCI)) & CFA_VLAN_VID_MASK;
    }

    /* Scenario 1: Bridged interface 
     */
    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);
    CfaGetIfNwType (u4IfIndex, &u1NwType);
    if ((u1BridgedIfaceStatus == CFA_ENABLED)
        && (CFA_NETWORK_TYPE_WAN == u1NwType))
    {
        if ((u2LenOrType == VLAN_PROTOCOL_ID) && (u2Protocol == CFA_ENET_IPV4))
        {
            OSIX_BITLIST_IS_BIT_SET (pSecVlanList, u2VlanId,
                                     sizeof (tSecVlanList), bResult);
            *pu1Result = (UINT1) bResult;
        }
    }

    /* Scenario 2: Security IVR interface
     */
    if (CfaGetSecIvrIndex () == u4IfIndex)
    {
        if (u2VlanId == 0)
        {
            u2Protocol = u2LenOrType;
            for (u2VlanId = 0; u2VlanId < CFA_MAX_VLAN_ID; u2VlanId++)
            {
                OSIX_BITLIST_IS_BIT_SET (gSecVlanList, u2VlanId,
                                         sizeof (tSecVlanList), bResult);
                if (bResult == OSIX_TRUE)
                {
                    if ((VLAN_SUCCESS ==
                         VlanGetFdbEntryDetails (u2VlanId, MacAddr,
                                                 &u2Port, &u1Status))
                        && (u1Status == VLAN_FDB_LEARNT))
                    {
                        u1Flag = OSIX_TRUE;
                        break;
                    }
                }
            }
        }
        else
        {
            if ((VLAN_SUCCESS == VlanGetFdbEntryDetails (u2VlanId, MacAddr,
                                                         &u2Port, &u1Status))
                && (u1Status == VLAN_FDB_LEARNT))
            {
                u1Flag = OSIX_TRUE;
            }
        }

        if (OSIX_TRUE == u1Flag)
        {
            CfaGetIfNwType (u2Port, &u1NwType);
            if ((u1NwType == CFA_NETWORK_TYPE_WAN) &&
                (u2Protocol == CFA_ENET_IPV4))
            {
                *pu1Result = OSIX_TRUE;
            }
            UtilVlanReleaseVlanListSize (pSecVlanList);
            return;
        }
    }
    UtilVlanReleaseVlanListSize (pSecVlanList);
    return;
}

/*****************************************************************************/
/*  Function Name   : CfaUtilTxPktOnSecVlanMemberPort                        */
/*  Description     : This function does the following                       */
/*                    (i) Scans the security VLAN list and transmits the fram*/
/*                        -> When unicast mac address is learnt on any of    */
/*                           of the member ports of the VLAN.                */
/*                        -> Transmitted on all the security VLANs for       */
/*                           broadcast and multicast destination address.    */
/*  Input(s)        : pu1DataBuf - Input buffer.                             */
/*                    u4PktSize - Packet size.                               */
/*                    u4IfL2CxtId - Context Identifier.                      */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  gSecVlanList                              */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
VOID
CfaUtilTxPktOnSecVlanMemberPorts (UINT4 u4IfL2CxtId,
                                  UINT1 *pu1DataBuf, UINT4 u4PktSize)
{
    UINT1              *pSecVlanList = NULL;
    tMacAddr            MacAddr;
    UINT2               u2Port = 0;
    UINT2               u2VlanId = 0;
    UINT1               u1Status = 0;
    BOOL1               bResult = OSIX_FALSE;

    MEMCPY (MacAddr, pu1DataBuf, CFA_ENET_ADDR_LEN);

    pSecVlanList = UtilVlanAllocVlanListSize (sizeof (tSecVlanList));
    if (pSecVlanList == NULL)
    {
        return;
    }

    MEMSET (pSecVlanList, 0, sizeof (tSecVlanList));
    if (CfaGetSecVlanList (pSecVlanList) == CFA_SUCCESS)
    {
        for (u2VlanId = 0; u2VlanId < CFA_MAX_VLAN_ID; u2VlanId++)
        {
            OSIX_BITLIST_IS_BIT_SET (gSecVlanList, u2VlanId,
                                     sizeof (tSecVlanList), bResult);
            if (OSIX_TRUE == bResult)
            {
                if (((VLAN_SUCCESS ==
                      VlanGetFdbEntryDetails (u2VlanId, MacAddr, &u2Port,
                                              &u1Status)))
                    || (CFA_IS_ENET_MAC_BCAST (MacAddr))
                    || (CFA_IS_ENET_MAC_MCAST (MacAddr)))
                {
                    CfaGddTxPktOnVlanMemberPortsInCxt (u4IfL2CxtId,
                                                       pu1DataBuf, u2VlanId,
                                                       CFA_TRUE, u4PktSize);
                    if (u1Status == VLAN_FDB_LEARNT)
                    {
                        break;
                    }
                }
            }
        }                        /* End of outer for */
    }
    UtilVlanReleaseVlanListSize (pSecVlanList);
    return;
}

/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : CfaCheckBrgPortType                                     */
/*                                                                            */
/* DESCRIPTION      : This function validates the given ifIndex is CFA_ENET or*/
/*                    CFA_LAGG or CFA_PSEUDO_WIRE.                             */
/*                                                                            */
/* INPUT            : u4IfIndex -Interface Index.                             */
/*                                                                            */
/* OUTPUT           : NONE                                                    */
/*                                                                            */
/* RETURNS          : TRUE/FALSE                                              */
/*                                                                            */
/******************************************************************************/
INT4
CfaCheckBrgPortType (UINT4 u4IfIndex)
{

    UINT1               u1IfType = 0;
    UINT1               u1SubType = 0;

    CfaGetIfType (u4IfIndex, &u1IfType);
    CfaGetIfSubType (u4IfIndex, &u1SubType);

    if ((u1IfType == CFA_ENET) || (u1IfType == CFA_LAGG) ||
        (u1IfType == CFA_CAPWAP_DOT11_BSS) ||
        (u1IfType == CFA_PSEUDO_WIRE) ||
        (u1IfType == CFA_VXLAN_NVE) ||
        (u1IfType == CFA_TAP) ||
        ((u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
         (CFA_PROP_VIRTUAL_INTERFACE == CFA_SUBTYPE_AC_INTERFACE)))
    {
        return TRUE;
    }
    return FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaGetAddrAllocMethodForSecIp                      */
/*                                                                           */
/*     DESCRIPTION      : This function gets the address allocation method   */
/*                        for secondary IP                                   */
/*                                                                           */
/*     INPUT            : u4IfIndex     - Interface Index                    */
/*                        u4SecondaryIp - Secondary IP Address               */
/*                                                                           */
/*     OUTPUT           : *pu1AddrAllocMethod - Address Allocation Method    */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
VOID
CfaGetAddrAllocMethodForSecIp (UINT4 u4IfIndex, UINT4 u4SecondaryIp,
                               UINT1 *pu1AddrAllocMethod)
{
    tIpAddrInfo         AddrInfo;

    MEMSET ((UINT1 *) &AddrInfo, 0, sizeof (tIpAddrInfo));

    if (CfaIpIfGetSecondaryAddressInfo (u4IfIndex, u4SecondaryIp, &AddrInfo)
        == CFA_FAILURE)
    {
        *pu1AddrAllocMethod = CFA_IP_IF_MANUAL;
        return;
    }

    *pu1AddrAllocMethod = AddrInfo.u1AllocMethod;

    return;
}

#ifdef MBSM_WANTED
/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : CfaCreateRmIPInterface                                  */
/*                                                                            */
/* DESCRIPTION      : This function creates an IP Interface over the          */
/*                    connecting port . This IP Interface will be used for    */
/*                    RM TCP/IP communication when RM Type is STK             */
/*                                                                            */
/* INPUT            : NONE                                                    */
/*                                                                            */
/* OUTPUT           : NONE                                                    */
/*                                                                            */
/* RETURNS          : CFA_SUCCESS / CFA_FAILURE                               */
/*                                                                            */
/******************************************************************************/
INT4
CfaCreateRmIPInterface (VOID)
{

    UINT4               u4StackPortIndex = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4IpAddress = 0;
    UINT4               u4SubnetMask = 0;
    UINT1              *pu1RmStackInterface = NULL;

    /* Incase of FSIP, no need to create the entry */
    if (IssGetRMTypeFromNvRam () != ISS_RM_STACK_INTERFACE_TYPE_INBAND)
    {
        return CFA_SUCCESS;
    }
    u4IpAddress = IssGetRMIpAddress ();
    pu1RmStackInterface = IssGetRMStackInterface ();
    u4StackPortIndex = ATOI (pu1RmStackInterface + 3);
    if (ATOI (pu1RmStackInterface + 3) >= SYS_DEF_MAX_INFRA_SYS_PORT_COUNT)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Exceeds the MAX supported Stacking port count, %d .\n",
                  SYS_DEF_MAX_INFRA_SYS_PORT_COUNT);
        return CFA_FAILURE;
    }
    u4IfIndex = gLocalUnitHwPortInfo.au4ConnectingPortIfIndex[u4StackPortIndex];
    if (CfaIfmUpdateInterfaceStatus (u4IfIndex, CFA_DISABLED) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Unable to Set Interface Status for Interface %d .\n",
                  u4IfIndex);
        return CFA_FAILURE;
    }
    u4SubnetMask = IssGetRMSubnetMask ();
    CfaIPVXSetIpAddress (u4IfIndex, u4IpAddress, u4SubnetMask);
    IpUpdateInterfaceStatus ((UINT2) CFA_IF_IPPORT (u4IfIndex),
                             NULL, CFA_IF_UP);

    return CFA_SUCCESS;

}
#endif
/*****************************************************************************
* *    Function Name            : CfaGetDefaultNetMask
* *
* *    Description              : Derives the default netmask from the IP address
* *
* *    Input(s)                  : None
* *
* *    Output(s)                 : None.
* *
* *    Global Variables Referred : gIpIfInfo
* *
* *    Returns                   : CFA_SUCCESS/CFA_FAILURE
* *****************************************************************************/

VOID
CfaGetDefaultNetMask (UINT4 u4IpAddress, UINT4 *pu4NetMask)
{
    /* find the default subnet mask if the subnet mask provided in the
     *  MIB variable is zero */
    if (CFA_IS_ADDR_CLASS_A (u4IpAddress))
    {
        *pu4NetMask = 0xff000000;    /* 255.0.0.0 */
    }
    else
    {
        if (CFA_IS_ADDR_CLASS_B (u4IpAddress))
        {
            *pu4NetMask = 0xffff0000;    /* 255.255.0.0 */
        }
        else
        {
            if (CFA_IS_ADDR_CLASS_C (u4IpAddress))
            {
                *pu4NetMask = 0xffffff00;    /* 255.255.0.0 */
            }
            else
            {
                *pu4NetMask = 0;    /* 0.0.0.0 */
            }
        }
    }
}

#if defined (WLC_WANTED) || defined (WTP_WANTED)
/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : CfaIfmHandleWssInterfaceStatusChange                    */
/*                                                                            */
/* DESCRIPTION      : This function will validate the interface for which     */
/*                    admin status changed is received and invoke             */
/*                    corresponding WSS module                                */
/*                                                                            */
/* INPUT            : u4IfIndex -Interface Index.                             */
/*                                                                            */
/* OUTPUT           : NONE                                                    */
/*                                                                            */
/* RETURNS          : TRUE/FALSE                                              */
/*                                                                            */
/******************************************************************************/
UINT4
CfaIfmHandleWssInterfaceStatusChange (UINT4 u4IfIndex, INT4 i4AdminStatus,
                                      UINT1 u1OperStatus)
{
    UINT4               u4RetVal = OSIX_FAILURE;
    UINT1               u1IfType = 0;
    tRadioIfMsgStruct   RadioIfMsgStruct;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        return u4RetVal;
    }
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));

    /* Get the interface type for ifindex */
    CfaGetIfType (u4IfIndex, &u1IfType);

    switch (u1IfType)
    {
        case CFA_CAPWAP_VIRT_RADIO:

            /* Fill the radio interface admin structure */
            RadioIfMsgStruct.unRadioIfMsg.RadioIfSetAdminOperStatus.isPresent =
                OSIX_TRUE;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfSetAdminOperStatus.u4IfIndex =
                u4IfIndex;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfSetAdminOperStatus.
                u1AdminStatus = i4AdminStatus;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfSetAdminOperStatus.
                u1OperStatus = u1OperStatus;
            u4RetVal =
                WssIfProcessRadioIfMsg (WSS_RADIOIF_ADMIN_STATUS_CHG_MSG,
                                        &RadioIfMsgStruct);
            break;
        case CFA_CAPWAP_DOT11_PROFILE:
            /* Fill the wlan interface admin structure */
            pWssWlanMsgStruct->unWssWlanMsg.WssWlanSetAdminOperStatus.
                u4IfIndex = u4IfIndex;
            pWssWlanMsgStruct->unWssWlanMsg.WssWlanSetAdminOperStatus.
                u1AdminStatus = i4AdminStatus;
            pWssWlanMsgStruct->unWssWlanMsg.WssWlanSetAdminOperStatus.
                u1OperStatus = u1OperStatus;
            u4RetVal =
                WssIfProcessWssWlanMsg (WSS_WLAN_PROFILE_ADMIN_STATUS_CHG_MSG,
                                        pWssWlanMsgStruct);
            break;
        case CFA_CAPWAP_DOT11_BSS:
            /* Fill the wlan interface admin structure */
            pWssWlanMsgStruct->unWssWlanMsg.WssWlanSetAdminOperStatus.
                u4IfIndex = u4IfIndex;
            pWssWlanMsgStruct->unWssWlanMsg.WssWlanSetAdminOperStatus.
                u1AdminStatus = i4AdminStatus;
            pWssWlanMsgStruct->unWssWlanMsg.WssWlanSetAdminOperStatus.
                u1OperStatus = u1OperStatus;
            u4RetVal =
                WssIfProcessWssWlanMsg (WSS_WLAN_BSS_ADMIN_STATUS_CHG_MSG,
                                        pWssWlanMsgStruct);

            /*Give indication to L2IWF about oper status change .
             * This handles giving indication to PNAC module */

            L2IwfPortOperStatusIndication (u4IfIndex, u1OperStatus);
            break;
            /* For WLAN Binding interface no processing required */
        case CFA_WLAN_RADIO:
            L2IwfPortOperStatusIndication (u4IfIndex, u1OperStatus);
            u4RetVal = CFA_SUCCESS;
            break;

        default:
            break;
    }
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    return u4RetVal;
}

/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : CfaProcessWssIfMsg                                      */
/*                                                                            */
/* DESCRIPTION      : This function will check the op code received and will  */
/*                    invoke the corresponding function to process the        */
/*                    request                                                 */
/*                                                                            */
/* INPUT            : u1OpCode - op code indicating the type of operation to  */
/*                    be performed                                            */
/*                    pWssMsgStruct - Pointer to CFA process structure        */
/*                                                                            */
/* OUTPUT           : NONE                                                    */
/*                                                                            */
/* RETURNS          : TRUE/FALSE                                              */
/*                                                                            */
/******************************************************************************/
UINT4
CfaProcessWssIfMsg (UINT1 u1OpCode, tCfaMsgStruct * pCfaMsgStruct)
{
#if defined WLC_WANTED || defined (WTP_WANTED)
    UINT4               u4IfType = 0;
    UINT4               u4IfIndex = 0;
    UINT1               u1AdminStatus = 0;
    UINT1               u1OperStatus = 0;
    UINT4               u4ErrCode = 0;
    UINT1               au1SlotName[CFA_MAX_PORT_NAME_LENGTH] = CFA_SLOT_1;
    INT4                i4RowStatus;
    INT4                i4AdminStatus = 0;
    if (pCfaMsgStruct == NULL)
    {
        return CFA_FAILURE;
    }

    switch (u1OpCode)
    {
        case CFA_WSS_CREATE_IFINDEX_MSG:
            u4IfType =
                (UINT4) pCfaMsgStruct->unCfaMsg.CfaCreateIfIndex.u1IfType;
            if (CfaCreateInterfaceIndex (u4IfType, &u4IfIndex) != CLI_SUCCESS)
            {
                return CFA_FAILURE;
            }
            pCfaMsgStruct->unCfaMsg.CfaCreateIfIndex.u4IfIndex = u4IfIndex;
            break;

        case CFA_WSS_OPER_STATUS_CHG_MSG:
            u4IfType =
                (UINT4) pCfaMsgStruct->unCfaMsg.CfaCreateIfIndex.u1IfType;
            u4IfIndex = pCfaMsgStruct->unCfaMsg.CfaCreateIfIndex.u4IfIndex;
            u1OperStatus =
                pCfaMsgStruct->unCfaMsg.CfaCreateIfIndex.u1OperStatus;

            if (u4IfType == CFA_CAPWAP_DOT11_BSS)
            {
                /* When the binding interface is made up after the processing of
                 * config response,oper status change indication is given to
                 * CFA-->L2-->PNAC */

                if (CfaIfmHandleInterfaceStatusChange
                    (u4IfIndex, CFA_IF_UP,
                     u1OperStatus, CFA_IF_LINK_STATUS_CHANGE) != CFA_SUCCESS)
                {
                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                              "Error in CfaIfmHandleInterfaceStatusChange -"
                              "Update fail for interface %d\n", u4IfIndex);
                }

            }
            else
            {

                CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
            }
            break;

        case CFA_WSS_ADMIN_STATUS_CHG_MSG:
            u4IfIndex = pCfaMsgStruct->unCfaMsg.CfaCreateIfIndex.u4IfIndex;
            u1AdminStatus =
                pCfaMsgStruct->unCfaMsg.CfaCreateIfIndex.u1AdminStatus;

            if (CfaSetIfMainAdminStatus (u4IfIndex, u1AdminStatus) !=
                CFA_SUCCESS)
            {
                return CFA_FAILURE;
            }
            break;

        case CFA_WSS_DELETE_IFINDEX_MSG:
            u4IfIndex = pCfaMsgStruct->unCfaMsg.CfaCreateIfIndex.u4IfIndex;
            if (nmhTestv2IfMainRowStatus (&u4ErrCode, (INT4) u4IfIndex,
                                          (INT4) CFA_RS_DESTROY) ==
                SNMP_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                         "\r%% Unable to delete interface\r\n");
                return (CFA_FAILURE);
            }

            nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);

            break;

        case CFA_WSS_CREATE_RADIO_INTF_MSG:
#ifdef WTP_WANTED
            if (CfaIfmBringupAllRadioInterfaces (NULL) != CFA_SUCCESS)
            {
                return CFA_FAILURE;
            }
#endif
            break;
        case CFA_WSS_SET_MAC_ADDR:
            u4IfIndex = pCfaMsgStruct->unCfaMsg.CfaCreateIfIndex.u4IfIndex;
            if (CfaSetIfHwAddr (u4IfIndex, pCfaMsgStruct->unCfaMsg.
                                CfaCreateIfIndex.CfaWssMacAddr,
                                CFA_ENET_ADDR_LEN) != CFA_SUCCESS)
            {
                return CFA_FAILURE;
            }
            break;
        case CFA_WSS_WTP_INIT:
            if (CfaGetInterfaceIndexFromName (au1SlotName, &u4IfIndex)
                != OSIX_FAILURE)
            {
                nmhGetIfMainRowStatus ((INT4) u4IfIndex, &i4RowStatus);
                CfaGetIfType (u4IfIndex, (UINT1 *) &u4IfType);
                nmhSetIfAdminStatus (u4IfIndex, CFA_IF_DOWN);
                nmhSetIfIvrBridgedIface (u4IfIndex, CFA_DISABLED);

                nmhGetIfMainAdminStatus ((INT4) u4IfIndex, &i4AdminStatus);
                if (i4AdminStatus == CFA_IF_UP)
                {
                    nmhSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_DOWN);
                }

                nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);
                nmhSetIfIpAddrAllocMethod (u4IfIndex, CFA_IP_ALLOC_POOL);
                nmhSetIfIpAddrAllocProtocol (u4IfIndex, CFA_PROTO_DHCP);
                nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE);
                nmhSetIfAdminStatus (u4IfIndex, CFA_IF_UP);
            }
            break;
        default:
            return CFA_FAILURE;
    }
    return CFA_SUCCESS;
#else
    UNUSED_PARAM (u1OpCode);
    UNUSED_PARAM (pCfaMsgStruct);
    return CFA_SUCCESS;
#endif
}

/**************************************************************************
 * FUNCTION NAME : CfaCreateInterfaceIndex
 *
 * DESCRIPTION   : This function will check for the free interface index 
 *                 for the given INTERFACE TYPE  and then invoke the 
 *                 appropriate function to create an ifEntry for 
 *                 capwap tunnel interface and WSS interfaces (virtual radio, 
 *                 WLAN profile and WLAN BSS)
 * 
 * INPUTS        : CliHandle - CLI handler
 *                 u4IfType  - Type of Interface to be created 
 *
 * OUTPUT        : pu4CfaIndex - Interface index value.
 *
 * RETURNS       : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
UINT1
CfaCreateInterfaceIndex (UINT4 u4IfType, UINT4 *pu4CfaIndex)
{
    UINT4               u4IfIndex = 0;
    UINT1               au1Tmp[CFA_MAX_PORT_NAME_LENGTH];
    UINT1              *pu1Alias = NULL;
    tCfaIfInfo         *pCfaIfInfo = NULL;
    UINT1              *pu1IfaceInfo = NULL;

    MEMSET (au1Tmp, 0, CFA_MAX_PORT_NAME_LENGTH);

    pu1Alias = &au1Tmp[0];

    /* Get a free interface index for the interface type */
    if (CfaGetFreeInterfaceIndex (&u4IfIndex, u4IfType) != OSIX_SUCCESS)
    {
        return CFA_FAILURE;
    }

    /* Get the alias name from the iftype and ifindex */
    pu1IfaceInfo = (UINT1 *) cli_get_iface_type_name (u4IfType);
    SPRINTF ((CHR1 *) pu1Alias, "%s%u", pu1IfaceInfo, u4IfIndex);

    switch (u4IfType)
    {
        case CFA_WLAN_RADIO:
            if (CfaCreateRadioInterface (u4IfIndex,
                                         pu1Alias, u4IfType) != OSIX_SUCCESS)
            {
                CFA_DS_LOCK ();

                pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
                if (NULL != pCfaIfInfo)
                {
                    CFA_CDB_SET_INTF_VALID (u4IfIndex, CFA_FALSE);
                    CFA_DS_UNLOCK ();

                    /* This means that the node exists and since we
                     * incurred a failure above, we will remove the node
                     * otherwise system will be in an unstable state */
                    CfaIfUtlIfDbRem (u4IfIndex, CFA_CDB_IFDB_REQ);
                }
                else
                {
                    CFA_DS_UNLOCK ();
                }
                return CFA_FAILURE;
            }
            break;

        case CFA_CAPWAP_VIRT_RADIO:
        case CFA_CAPWAP_DOT11_PROFILE:
        case CFA_CAPWAP_DOT11_BSS:
            if (CfaCreateWssInterface (u4IfIndex,
                                       pu1Alias, u4IfType) != OSIX_SUCCESS)
            {
                CFA_DS_LOCK ();

                pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
                if (NULL != pCfaIfInfo)
                {
                    CFA_CDB_SET_INTF_VALID (u4IfIndex, CFA_FALSE);
                    CFA_DS_UNLOCK ();

                    /* This means that the node exists and since we
                     * incurred a failure above, we will remove the node
                     * otherwise system will be in an unstable state */
                    CfaIfUtlIfDbRem (u4IfIndex, CFA_CDB_IFDB_REQ);
                }
                else
                {
                    CFA_DS_UNLOCK ();
                }
                return CFA_FAILURE;
            }
            break;
        case CFA_TUNNEL:
            break;

        default:
            return CFA_FAILURE;
    }
    /* If the interface creation is success, send the ifindex to the calling
     * module */
    *pu4CfaIndex = u4IfIndex;

    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCreateRadioInterface                            */
/*                                                                           */
/*     DESCRIPTION      : This function creates an interface for a           */
/*                        WLAN bound to a Radio Interface                    */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be created   */
/*                        pu1IfName - Name of the interface                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCreateRadioInterface (UINT4 u4IfIndex, UINT1 *pu1IfName, INT4 i4IfType)
{
    UINT4               u4ErrCode = 0;
    tSNMP_OCTET_STRING_TYPE Alias;

    /* Validate the received input */
    if (pu1IfName == NULL)
    {
        return (CFA_FAILURE);
    }

    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
    {
        return (CFA_FAILURE);
    }

    Alias.i4_Length = STRLEN (pu1IfName);
    Alias.pu1_OctetList = pu1IfName;

    /* we can directly call set routine, because validation is done already
     * in the function CfaCreateWssInterfaceIndex() */
    nmhSetIfAlias (u4IfIndex, &Alias);

    /*CFA_UNLOCK (); */

    /* Set the Type */
    if (nmhTestv2IfMainType (&u4ErrCode, u4IfIndex, i4IfType) == SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return (CFA_FAILURE);
    }

    if (nmhSetIfMainType (u4IfIndex, i4IfType) == SNMP_FAILURE)
    {
        return (CFA_FAILURE);
    }

    /* All the mandatory parameters are set. 
     * So make the row active */
    nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE);
    return (CFA_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCreateWssInterface                              */
/*                                                                           */
/*     DESCRIPTION      : This function creates an interface for a virtual   */
/*                        radio, WLAN Profile or WLAN BSS                    */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be created   */
/*                        pu1IfName - Name of the interface                  */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCreateWssInterface (UINT4 u4IfIndex, UINT1 *pu1IfName, INT4 i4IfType)
{
    UINT4               u4ErrCode = 0;
    tSNMP_OCTET_STRING_TYPE Alias;

    /* Validate the received input */
    if (pu1IfName == NULL)
    {
        return (CFA_FAILURE);
    }

    /* Check whether the received ifindex is for WSS */
    if (CfaIsWssIntf (u4IfIndex) != TRUE)
    {
        return (CFA_FAILURE);
    }

    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
    {
        return (CFA_FAILURE);
    }

    Alias.i4_Length = STRLEN (pu1IfName);
    Alias.pu1_OctetList = pu1IfName;

    /* we can directly call set routine, because validation is done already
     * in the function CfaCreateWssInterfaceIndex() */
    CFA_LOCK ();

    nmhSetIfAlias (u4IfIndex, &Alias);

    CFA_UNLOCK ();

    /* Set the Type */
    if (nmhTestv2IfMainType (&u4ErrCode, u4IfIndex, i4IfType) == SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        return (CFA_FAILURE);
    }

    if (nmhSetIfMainType (u4IfIndex, i4IfType) == SNMP_FAILURE)
    {
        return (CFA_FAILURE);
    }

    /* All the mandatory parameters are set. 
     * So make the row active */
    nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE);

    return (CFA_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaWssWlcProcessRxFrame                            */
/*                                                                           */
/*     DESCRIPTION      : This function transmits the WSS Frame from WLC     */
/*                                                                           */
/*     INPUT            : u4IfIdx - Index of the interface                   */
/*                        pBuf - Pointer to the B=Frame Buffer               */
/*                        u2VlanId - VLAN Id                                 */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaWssProcessRxFrame (UINT4 u4IfIdx, tCRU_BUF_CHAIN_HEADER * pBuf,
                      UINT2 u2VlanId)
{
    UINT4               u4VlanIfIndex = 0;
    UINT2               u2Protocol = 0;
    UINT4               u4PktSize = 0;

    if (NULL == pBuf)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "CfaWssProcessRxFrame - BUF is NULL\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return CFA_FAILURE;
    }

#ifdef WLC_WANTED
    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    if (CFA_FAILURE == CfaIwfEnetProcessRxFrame (pBuf, u4IfIdx,
                                                 u4PktSize, u2Protocol,
                                                 CFA_ENCAP_ENETV2))
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Processing of packet posted from WSS failed\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return CFA_FAILURE;
    }

    UNUSED_PARAM (u4VlanIfIndex);
    UNUSED_PARAM (u2VlanId);
#endif

#ifdef WTP_WANTED

    u4VlanIfIndex = CfaGetVlanInterfaceIndex (u2VlanId);
    if (CFA_INVALID_INDEX == u4VlanIfIndex)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "Invalid VLAN Index\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return CFA_FAILURE;
    }

    /* CfaIwfEnetProcessRxFrame will be called directly from here later,
       which will take care of sending it to FSIP or Linux IP */
#ifdef LNXIP4_WANTED
    CfaHandleInBoundPktToLnxIpTap (u4VlanIfIndex, pBuf, NULL, IP_ID);
#endif

    UNUSED_PARAM (u2Protocol);
    UNUSED_PARAM (u4PktSize);
    UNUSED_PARAM (u4IfIdx);
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
#endif

    return CFA_SUCCESS;
}

#endif

/******************************************************************************
 * Function           : CfaGetStandbyNodeCount
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : Number of standby nodes up.
 * Action             : Routine to get the number of peer nodes that are up.
 ******************************************************************************/
INT4
CfaGetStandbyNodeCount ()
{
#ifdef RM_WANTED
    return (RmGetStandbyNodeCount ());
#else
#ifdef MBSM_WANTED
    return (MbsmGetPeerNodeCount ());
#else
    return 0;
#endif
#endif
}

/******************************************************************************
 * Function           : CfaGetPeerNodeCount
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : Number of standby nodes up.
 * Action             : Routine to get the number of peer nodes that are up.
 ******************************************************************************/
INT4
CfaGetPeerNodeCount ()
{
#ifdef RM_WANTED
    return (RmGetPeerNodeCount ());
#else
#ifdef MBSM_WANTED
    return (MbsmGetPeerNodeCount ());
#else
    return 0;
#endif
#endif
}

/******************************************************************************
 * Function           : CfaGetRetrieveNodeState
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : Node state.
 * Action             : Routine to get the node state.
 ******************************************************************************/
UINT4
CfaGetRetrieveNodeState ()
{
    UINT4               u4SwitchId = 0;
    UINT1               u1DissRole = 0;
#ifdef RM_WANTED
    UNUSED_PARAM (u4SwitchId);
    UNUSED_PARAM (u1DissRole);
    return (RmRetrieveNodeState ());
#elif MBSM_WANTED
    u4SwitchId = IssGetSwitchid ();
    MbsmGetDissRoleFromSlotId (u4SwitchId, &u1DissRole);
    return ((UINT4) u1DissRole);
#else
    UNUSED_PARAM (u4SwitchId);
    UNUSED_PARAM (u1DissRole);
#ifdef NPSIM_WANTED
    return ((UINT4) ISS_DISS_ROLE_MASTER);
#else
    return ((UINT4) IssGetDissRolePlayed ());
#endif
#endif
}

/********************************************************************************
 * Function           : CfaCheckCentralizedProto
 * DESCRIPTION      :   This function checks whether protocol is centralized or not.    
 * Input(s)           : DestAddr - Destination mac-address
 * Output(s)          : None.
 * Returns            : CFA_TRUE/CFA_FALSE
 ******************************************************************************/
UINT4
CfaCheckCentralizedProto (tMacAddr DestAddr)
{
    tMacAddr            StpAddr = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x00 };
    tMacAddr            IgsAddr = { 0x01, 0x00, 0x5e, 0x00, 0x00, 0x00 };
    tMacAddr            BcastAddr = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };
    tMacAddr            SwitchMac;

    /* For Non-DISS Stacking model all, packets are centralised */
    if (ISS_GET_STACKING_MODEL () != ISS_DISS_STACKING_MODEL)
    {
        return CFA_TRUE;
    }

    CfaGetSysMacAddress (SwitchMac);
#ifdef ISS_TEST_WANTED
    if (gi4CentralizedIPSupport != CFA_TRUE)
    {

        /* Check if the trapped packet is IGS or STP packet */
        if ((MEMCMP (DestAddr, StpAddr, CFA_ENET_ADDR_LEN) == 0) ||
            (MEMCMP (DestAddr, IgsAddr, (CFA_ENET_ADDR_LEN - 3)) == 0))
        {
            return CFA_TRUE;
        }
        /* the trapped packet has to be posted to CFA Q */
        else
        {
            return CFA_FALSE;
        }
    }
    else
#endif
    {
        /* Check if the trapped packet is Broadcast, IGS or STP packet */
        if ((MEMCMP (DestAddr, StpAddr, CFA_ENET_ADDR_LEN) == 0) ||
            (MEMCMP (DestAddr, IgsAddr, (CFA_ENET_ADDR_LEN - 3)) == 0) ||
            (MEMCMP (DestAddr, BcastAddr, (CFA_ENET_ADDR_LEN)) == 0) ||
            (MEMCMP (DestAddr, SwitchMac, (CFA_ENET_ADDR_LEN)) == 0))
        {
            return CFA_TRUE;
        }
        /* the trapped packet has to be posted to CFA Q */
        else
        {
            return CFA_FALSE;
        }
    }
}

/********************************************************************************
 * Function           : CfaIpGetAddrTypeInCxt  
 * DESCRIPTION        : This function checks whether protocol is centralized or not.    
 * Input(s)           : u4ContextId - Context Id             
 *                      u4Addr - Destination IP address 
 * Output(s)          : None.
 * Returns            : CFA_FAILURE - Class E address, which are reserved
 *                      CFA_UCAST_ADDR - If the u4Addr is unicast address
 *                      CFA_MCAST_ADDR - If the u4Addr is multicast address 
 *                      CFA_BCAST_ADDR - If the u4Addr is broadcast address 
 ******************************************************************************/
INT4
CfaIpGetAddrTypeInCxt (UINT4 u4ContextId, UINT4 u4Addr)
{
    /* Class E address except - 255.255.255.255 */
    if ((IP_IS_ADDR_CLASS_E (u4Addr) == TRUE) && (u4Addr != IP_GEN_BCAST_ADDR))
    {
        return CFA_FAILURE;
    }

    /* Check for Multicast */
    if (IP_IS_ADDR_CLASS_D (u4Addr) == TRUE)
    {
        return CFA_MCAST_ADDR;
    }

    if ((IP_IS_ADDR_CLASS_A (u4Addr)) ||
        (IP_IS_ADDR_CLASS_B (u4Addr)) || (IP_IS_ADDR_CLASS_C (u4Addr)))
    {
        /* If the address is interface specific broadcast address, then return 
         * broadcast */
        if (CfaIpIfIsLocalBroadcastInCxt (u4ContextId, u4Addr) == CFA_SUCCESS)
        {
            return CFA_BCAST_ADDR;
        }
        return CFA_UCAST_ADDR;
    }

    /* Limited broadcast (255.255.255.255) */
    return CFA_BCAST_ADDR;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaIvrSetIpAddressForPortVlanId                    */
/*                                                                           */
/*     DESCRIPTION      : This function sets the IP address of an IVR        */
/*                         interface while dummy vlan id is changed          */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
/*                        u4IpAddr, u4SubnetMask - IPAddress, Subnet Mask    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SUCCESS/FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIvrSetIpAddressForPortVlanId (UINT4 u4IfIndex, UINT4 u4IpAddr,
                                 UINT4 u4IpSubnetMask)
{
    UINT4               u4ErrCode;
    UINT4               u4BcastAddr = 0;
    UINT4               u4PrevIpAddr;
    UINT4               u4PrevSubnetMask;
    INT4                i4AdminStatus = 0;
#ifdef VXLAN_WANTED
    INT4                i4IfaceType = 0;
#endif

    if (L2IwfIsPortInPortChannel (u4IfIndex) == CFA_SUCCESS)
    {
        CLI_SET_ERR (CLI_CFA_PORT_IN_AGG);
        return (CLI_FAILURE);

    }
#ifdef VXLAN_WANTED
    if (nmhGetIfMainType ((INT4) u4IfIndex, &i4IfaceType) == SNMP_SUCCESS)
    {
        if (i4IfaceType == CFA_LOOPBACK)
        {
            if (VxlanIsLoopbackUsed (u4IfIndex) == VXLAN_SUCCESS)
            {
                return CLI_FAILURE;

            }
        }
    }
#endif

    if (nmhTestv2IfIpAddr (&u4ErrCode, (INT4) u4IfIndex, u4IpAddr) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);

    }

    /* Test Ip addr and Sub net mask */
    if (CfaTestIfIpAddrAndIfIpSubnetMask ((INT4) u4IfIndex,
                                          u4IpAddr, u4IpSubnetMask)
        == CLI_FAILURE)
    {
        if (CLI_GET_ERR (&u4ErrCode) != CLI_SUCCESS)
        {
            CLI_SET_ERR (CLI_CFA_INVALID_IPADDR);
        }
        return (CLI_FAILURE);
    }

    nmhGetIfMainAdminStatus ((INT4) u4IfIndex, &i4AdminStatus);
    if (i4AdminStatus == CFA_IF_UP)
    {
        nmhSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_DOWN);
    }

    nmhGetIfIpAddr ((INT4) u4IfIndex, &u4PrevIpAddr);
    if (nmhSetIfIpAddr (u4IfIndex, u4IpAddr) == SNMP_FAILURE)
    {
        if (i4AdminStatus == CFA_IF_UP)
        {
            nmhSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_UP);
        }
        if (CLI_GET_ERR (&u4ErrCode) != CLI_SUCCESS)
        {
            CLI_SET_ERR (CLI_CFA_INVALID_IPADDR);
        }
        return (CLI_FAILURE);
    }

    nmhGetIfIpSubnetMask ((INT4) u4IfIndex, &u4PrevSubnetMask);

    if (nmhSetIfIpSubnetMask (u4IfIndex, u4IpSubnetMask) == SNMP_FAILURE)
    {
        if (i4AdminStatus == CFA_IF_UP)
        {
            nmhSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_UP);
        }
        nmhSetIfIpAddr (u4IfIndex, u4PrevIpAddr);
        CLI_SET_ERR (CLI_CFA_INVALID_SUBNET);
        return (CLI_FAILURE);
    }

    u4BcastAddr = u4IpAddr | (~(u4IpSubnetMask));

    if (nmhTestv2IfIpBroadcastAddr (&u4ErrCode, (INT4) u4IfIndex,
                                    u4BcastAddr) == SNMP_FAILURE)
    {
        if (i4AdminStatus == CFA_IF_UP)
        {
            nmhSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_UP);
        }
        nmhSetIfIpAddr (u4IfIndex, u4PrevIpAddr);
        nmhSetIfIpSubnetMask (u4IfIndex, u4PrevSubnetMask);
        return (CLI_FAILURE);
    }

    if (nmhSetIfIpBroadcastAddr (u4IfIndex, u4BcastAddr) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (i4AdminStatus == CFA_IF_UP)
    {
        nmhSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_UP);
    }
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaIvrDeleteIpAddressForPortVlanId                 */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the secondary IP address of  */
/*                        an IVR interface while port vlan id is changed.    */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
                                                                                                                                                                                                                                                                                                                                                                                                                     /*                        u4IpAddr  - IPAddress                              *//*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIvrDeleteIpAddressForPortVlanId (UINT4 u4IfIndex, UINT4 u4IpAddr)
{
    UINT4               u4ErrCode;
    UINT4               u4PrimaryAddr = 0;
    INT4                i4NextIfIndex = 0;
    UINT4               u4SecondaryAddr = 0;
    tIpAddrInfo         SecondaryInfo;
#ifdef VXLAN_WANTED
    INT4                i4IfaceType = 0;
#endif

    MEMSET (&SecondaryInfo, 0, sizeof (tIpAddrInfo));

#ifdef VXLAN_WANTED
    if (nmhGetIfMainType ((INT4) u4IfIndex, &i4IfaceType) == SNMP_SUCCESS)
    {
        if (i4IfaceType == CFA_LOOPBACK)
        {
            if (VxlanIsLoopbackUsed (u4IfIndex) == VXLAN_SUCCESS)
            {
                return CLI_FAILURE;

            }
        }
    }
#endif
    nmhGetIfIpAddr ((INT4) u4IfIndex, &u4PrimaryAddr);

    if (u4IpAddr == u4PrimaryAddr)
    {
        /* Primary address is coming for deletion.If there is any secondary
         *          *          * address availble on the interface, Dont allow deletion of
         *                   *                   * primary address */
        if ((nmhGetNextIndexIfSecondaryIpAddressTable ((INT4) u4IfIndex,
                                                       &i4NextIfIndex, 0,
                                                       &u4SecondaryAddr) ==
             SNMP_SUCCESS) && ((INT4) u4IfIndex == i4NextIfIndex))
        {
            CLI_SET_ERR (CFA_CLI_PRIMARY_IP_DEL_ERR);
            return CLI_FAILURE;
        }
        return (CfaIvrSetIpAddressForPortVlanId (u4IfIndex, 0, 0));
    }
    else if (CfaIpIfGetSecondaryAddressInfo (u4IfIndex, u4IpAddr,
                                             &SecondaryInfo) == CFA_FAILURE)
    {
        CLI_SET_ERR (CLI_CFA_UNASSIGNED_IPADDR_ERR);
        return CLI_FAILURE;
    }

    if (CfaIpIfGetSecondaryAddressInfo (u4IfIndex, u4IpAddr,
                                        &SecondaryInfo) == CFA_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (SecondaryInfo.u1AllocMethod == CFA_IP_IF_VIRTUAL)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2IfSecondaryIpRowStatus (&u4ErrCode, (INT4) u4IfIndex, u4IpAddr,
                                         CFA_RS_DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIfSecondaryIpRowStatus (u4IfIndex, u4IpAddr,
                                      CFA_RS_DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaIfSetOOBLocalNodeSecondaryIpAddress             */
/*                                                                           */
/*     DESCRIPTION      : This function sets the secondary IP address/mask   */
/*                        of OOB in local node and create a new entry in     */
/*                        secondary ip database                              */
/*                                                                           */
/*     INPUT            : u4IpAddr - IPAddress                               */
/*                        u4IpMask - Ip Mask                                 */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS/CFA_FAILURE                            */
/*****************************************************************************/
INT4
CfaIfSetOOBLocalNodeSecondaryIpAddress (UINT4 u4IpAddr, UINT4 u4IpMask)
{
    tIpAddrInfo         SecondaryInfo;
    UINT1               u1CidrMask = 0;
    UINT4               u4OldIpAddr = 0;
    UINT4               u4OldIpMask = 0;
#if defined(CLI_LNXIP_WANTED) || defined(SNMP_LINXIP_WANTED)
    tIpConfigInfo       IpConfigInfo;
    UINT4               u4Flag = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
#endif

    MEMSET (&SecondaryInfo, 0, sizeof (tIpAddrInfo));

    /* Fetch the Secondary IP and Mask for the OOB interface */
    if (IssGetSwitchid () == CFA_NODE0)
    {
        u4OldIpAddr = gu4IfOOBNode0SecondaryIpAddress;
        u4OldIpMask = gu4IfOOBNode0SecondaryIpMask;
    }
    else
    {
        u4OldIpAddr = gu4IfOOBNode1SecondaryIpAddress;
        u4OldIpMask = gu4IfOOBNode1SecondaryIpMask;
    }

    /* IP address and Mask are set as 0, so destroy the secondary address entry */
    if (u4IpAddr == 0)
    {
        /* Secondary IP is not present and user is trying to destroy the Row */
        if (u4OldIpAddr == 0)
        {
            return CFA_SUCCESS;
        }

        u1CidrMask = CfaGetCidrSubnetMaskIndex (u4OldIpMask);
        SecondaryInfo.u1SubnetMask = u1CidrMask;
        SecondaryInfo.u4Addr = u4OldIpAddr;
        SecondaryInfo.u4BcastAddr = (~u4OldIpMask | u4OldIpAddr);
        SecondaryInfo.i4RowStatus = CFA_RS_DESTROY;

        if (CfaIpIfSetSecondaryAddressInfo (CFA_OOB_MGMT_IFINDEX,
                                            CFA_IP_IF_SECONDAY_ROW_STATUS,
                                            &SecondaryInfo) != CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IF,
                     "Failed to Delete the secondary ip entry.\n");
            return CFA_FAILURE;
        }
        return CFA_SUCCESS;
    }

    /* Mask is not present so set with default Mask */
    if (u4IpMask == 0)
    {
        /* Create secondary ip database with default mask */
        CfaGetDefaultNetMask (u4IpAddr, &u4IpMask);
    }

    /* Check whether the Row is already Present */
    if (u4OldIpAddr != 0)
    {
        u1CidrMask = CfaGetCidrSubnetMaskIndex (u4OldIpMask);
        SecondaryInfo.u1SubnetMask = u1CidrMask;
        SecondaryInfo.u4Addr = u4OldIpAddr;
        SecondaryInfo.u4BcastAddr = (~u4OldIpMask | u4OldIpAddr);

        /* Delete the Old Secondary IP Entry for the OOB interface */
        SecondaryInfo.i4RowStatus = CFA_RS_DESTROY;
        if (CfaIpIfSetSecondaryAddressInfo (CFA_OOB_MGMT_IFINDEX,
                                            CFA_IP_IF_SECONDAY_ROW_STATUS,
                                            &SecondaryInfo) != CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IF,
                     "Failed to Delete Old secondary ip entry.\n");
            return CFA_FAILURE;
        }
    }

    u1CidrMask = CfaGetCidrSubnetMaskIndex (u4IpMask);
    SecondaryInfo.u1SubnetMask = u1CidrMask;
    SecondaryInfo.u4Addr = u4IpAddr;
    SecondaryInfo.u4BcastAddr = (~u4IpMask | u4IpAddr);

    /* Create the Secondary IP Entry for the OOB interface */
    SecondaryInfo.i4RowStatus = CFA_RS_CREATEANDGO;
    if (CfaIpIfSetSecondaryAddressInfo (CFA_OOB_MGMT_IFINDEX,
                                        CFA_IP_IF_SECONDAY_ROW_STATUS |
                                        CFA_IP_IF_NETMASK | CFA_IP_IF_BCASTADDR,
                                        &SecondaryInfo) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IF,
                 "Failed to Create secondary ip entry.\n");
        return CFA_FAILURE;
    }
#if defined(CLI_LNXIP_WANTED) || defined(SNMP_LINXIP_WANTED)
    MEMSET (&IpConfigInfo, 0, sizeof (tIpConfigInfo));
    IpConfigInfo.u4Addr = u4IpAddr;
    IpConfigInfo.u4NetMask = u4IpMask;
    IpConfigInfo.u4BcastAddr = (~u4IpMask | u4IpAddr);

    u4Flag = CFA_IP_IF_ALLOC_PROTO | CFA_IP_IF_ALLOC_METHOD |
        CFA_IP_IF_PRIMARY_ADDR | CFA_IP_IF_NETMASK |
        CFA_IP_IF_FORWARD | CFA_IP_IF_BCASTADDR;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    if (CfaGetSecondaryOobInterfaceName (au1IfName) == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IF,
                 "Failed to Fetch secondary OOB name.\n");
        return CFA_FAILURE;
    }

    if (CfaIpUpdateInterfaceParams (au1IfName, u4Flag,
                                    &IpConfigInfo) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IF,
                 "Failed to Create secondary ip entry in Linux .\n");
        return CFA_FAILURE;
    }
#endif

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaUtilCreateSChannelInterface
 *
 *    Description         : This function creates S-Channel interface in CFA
 *                          and thereby indicating to L2IWF to create the 
 *                          S-Channel interface to VLAN, IGMP Snooping and 
 *                          FIP Snooping.
 *                          
 *    Input(s)            : u4IfIndex - S-Channel interface index.
 *                          u4UapIfIndex - UAP interface index.
 *
 *    Output(s)           : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE
 *
 *****************************************************************************/
INT4
CfaUtilCreateSChannelInterface (UINT4 u4UapIfIndex, UINT4 u4IfIndex,
                                UINT1 u1Status)
{

    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;
    UINT1               au1NullString[2] = "";
    UINT2               u2LocalPortId = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4ContextId = CFA_INVALID_CONTEXT;
    UINT1               u1OperStatus = CFA_IF_DOWN;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    CFA_DS_LOCK ();
    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        /* Already interface is present */
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_DS_UNLOCK ();

    if (CfaIfmCreateAndInitIfEntry (u4IfIndex, CFA_INVALID_TYPE,
                                    au1NullString) == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IF,
                 "Failed to create S-Channel interface.\n");
        return CFA_FAILURE;
    }
    /* nmhSetIfMainRowStatus - CFA_RS_CREATEANDWAIT ends */

    SPRINTF ((CHR1 *) au1IfName, "%s%u", "s-channel",
             (u4IfIndex - CFA_MIN_EVB_SBP_INDEX + 1));
    CfaSetIfAlias (u4IfIndex, au1IfName);

    /* nmhSetIfMainType - starts */
    CfaSetIfIpv4AddressChange (u4IfIndex, CFA_FALSE);

    CfaSetIfName (u4IfIndex, au1IfName);
    CfaSetIfType (u4IfIndex, CFA_BRIDGED_INTERFACE);

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        /* NOTE: check the descriptor name */
        STRNCPY (CFA_IF_DESCR (u4IfIndex), CFA_BRIDGED_INTERFACE_DESCR,
                 STRLEN (CFA_BRIDGED_INTERFACE_DESCR));
        CFA_IF_DESCR (u4IfIndex)[STRLEN (CFA_BRIDGED_INTERFACE_DESCR)] = '\0';
        CFA_IF_TRAP_EN (u4IfIndex) = CFA_DISABLED;    /*as it is not a physical if */
    }
    CfaSetIfIpPort (u4IfIndex, CFA_INVALID_INDEX);

    CfaSetIfHighSpeed (u4IfIndex, 0);
    CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_ENABLED);
    CfaSetIfInternalStatus (u4IfIndex, CFA_TRUE);

    /* NOTE: why the below things required */
    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        /*There is no Recv Addr table for Internal interface */
        TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

        CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
        pHighStackListScan =
            (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
        CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
        pLowStackListScan =
            (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
        CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);
        CFA_IF_STACK_STATUS (pHighStackListScan) = CFA_RS_NOTINSERVICE;
        CFA_IF_STACK_STATUS (pLowStackListScan) = CFA_RS_ACTIVE;
        ++CFA_IFNUMBER ();
    }
    /* nmhSetIfMainType - ends */

    /* nmhTestv2IfMainBrgPortType - starts */
    /* NOTE: check whether CFA_IS_BRG_PORT_TYPE_VALID to be added with SBP and 
     *       UAP */
    /* NOTE: do we have to set L2IwfSetVlanPortAccpFrmType before calling this */

    CfaSetIfBrgPortType (u4IfIndex, (UINT1) CFA_STATION_FACING_BRIDGE_PORT);

    /* NOTE: check the following two items required */
    L2IwfSetInterfaceType (u4ContextId, VLAN_S_INTERFACE_TYPE);
    L2IwfIntfTypeChangeIndication (u4IfIndex, VLAN_S_INTERFACE_TYPE);
    /* nmhSetIfMainRowStatus - CFA_RS_ACTIVE */
    CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
    CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_ACTIVE);

    if (!(CFA_ENET_CREATE_DELETE_ALLOWED))
    {
        /* NOTE: UAP context should be retrieved ? */
        VcmCfaCreateIfaceMapping (L2IWF_DEFAULT_CONTEXT, u4IfIndex);
    }

    /* NOTE: do we need to get UAP if index status  
     * the status for the SBP port is only admin status. 
     * when creating the port, admin status would be as ACTIVE.*/
    CfaGetIfOperStatus (u4UapIfIndex, &u1OperStatus);

    /* NOTE : This check should not be moved above/below
     * Kindly take care of it, while
     * doing any code changes in this function */
    if (u1Status != 0)
    {
        /* Reasons to add this check:
         * 1.In case of hybrid mode, the status would be sent
         *   explicitly as down.
         * 2.For default S-Channel interface, the status would be 
         *   sent expclicitly as up.
         */
        u1OperStatus = u1Status;
    }
    CfaSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_UP);
    /*Below code has been added to reduce coverity errors */
    if (CfaSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_UP) == CFA_SUCCESS)
    {
        /*do nothing */
    }
    CfaSetIfOperStatus (u4IfIndex, u1OperStatus);

    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId)
        == VCM_SUCCESS)
    {
        L2IwfPortCreateIndication (u4IfIndex);
        L2IwfPortOperStatusIndication (u4IfIndex, u1OperStatus);
    }

    CFA_CDB_IF_DB_SET_INTF_VALID (u4IfIndex, CFA_TRUE);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaUtilDeleteSChannelInterface 
 *
 *    Description         : This function creates S-Channel interface in CFA
 *                          and thereby indicating to L2IWF to create the 
 *                          S-Channel interface to VLAN, IGMP Snooping and 
 *                          FIP Snooping.
 *                          
 *    Input(s)            : u4IfIndex - S-Channel interface index.
 *
 *    Output(s)           : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE
 *
 *****************************************************************************/
INT4
CfaUtilDeleteSChannelInterface (UINT4 u4IfIndex)
{
    tStackInfoStruct   *pHighStackListScan = NULL;
    tStackInfoStruct   *pLowStackListScan = NULL;
    tTMO_SLL           *pHighStack = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    CFA_DS_LOCK ();
    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) != CFA_TRUE)
    {
        /* Already interface is not present */
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    CFA_DS_UNLOCK ();

    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId)
        == VCM_SUCCESS)
    {
        L2IwfPortDeleteIndication (u4IfIndex);
    }
    VcmUnMapPortFromContext (u4IfIndex);

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        pHighStackListScan =
            (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
        pHighStack = &CFA_IF_STACK_LOW (u4IfIndex);
        /* If any ILAN entry for port exists in stack,
         * delete that entry. */
        TMO_SLL_Scan (pHighStack, pHighStackListScan, tStackInfoStruct *)
        {
            if (pHighStackListScan->u4IfIndex != CFA_NONE)
            {
                if ((nmhSetIfStackStatus ((INT4) u4IfIndex,
                                          (INT4) pHighStackListScan->u4IfIndex,
                                          CFA_RS_DESTROY)) == SNMP_FAILURE)
                {
                    /*return (OSIX_FAILURE); */
                }
                break;
            }
        }
        while (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex))
        {
            pHighStackListScan = (tStackInfoStruct *)
                CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);

            CFA_CHECK_IF_NULL (pHighStackListScan, CFA_FAILURE);
            if (CFA_IF_STACK_IFINDEX (pHighStackListScan) != CFA_NONE)
            {
                /* First Delete the Lower Layer Stack entry for this interface
                 * from the Higher Layer Stack entry */
                CfaIfmDeleteStackEntry (u4IfIndex, CFA_HIGH,
                                        CFA_IF_STACK_IFINDEX
                                        (pHighStackListScan));
            }
            else
            {

                CfaIfmUpdateStackForDeletion (&CFA_IF_STACK_HIGH (u4IfIndex),
                                              CFA_FALSE, CFA_NONE);
            }
        }

        while (CFA_IF_STACK_LOW_ENTRY (u4IfIndex))
        {
            pLowStackListScan =
                (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);

            CFA_CHECK_IF_NULL (pLowStackListScan, CFA_FAILURE);
            if (CFA_IF_STACK_IFINDEX (pLowStackListScan) != CFA_NONE)
            {
                /* First Delete this entry from the lower layer's stack value. */
                CfaIfmDeleteStackEntry (CFA_IF_STACK_IFINDEX
                                        (pLowStackListScan), CFA_HIGH,
                                        u4IfIndex);

            }
            else
            {
                CfaIfmUpdateStackForDeletion (&CFA_IF_STACK_LOW (u4IfIndex),
                                              CFA_FALSE, CFA_NONE);
            }
        }
    }

    /* delete the interface from the IfTable */
    if (CfaIfmDeleteIfEntry (u4IfIndex, CFA_BRIDGED_INTERFACE) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IF,
                 "Failed to delete S-Channel interface.\n");
        return (CFA_FAILURE);
    }

    return (CFA_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : CfaUtilSetSChannelOperStatus                         */
/*                                                                           */
/* Description        : This function sets the Oper Status of the S-Channel  */
/*                                                                           */
/* Input(s)           : u4IfIndex - S-Channel Interface Index                */
/*                      u1OperStatus - S-Channel Oper status                 */
/*                                                                           */
/* Output             : NONE                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/FAILURE                                  */
/*****************************************************************************/

INT4
CfaUtilSetSChannelOperStatus (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    UINT1               u1PrevOperStatus = CFA_IF_DOWN;

    CfaGetIfOperStatus (u4IfIndex, &u1PrevOperStatus);

    if (u1PrevOperStatus == u1OperStatus)
    {
        return CFA_SUCCESS;
    }

    CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
    L2IwfPortOperStatusIndication (u4IfIndex, u1OperStatus);

    return (CFA_SUCCESS);
}

#ifdef MBSM_WANTED
/*****************************************************************************
 *    Function Name       : CfaUtilIsConnectingPorts
 *    Description         : This function returns checks whether the interface
 *                          is a stacking interface.
 *    Input(s)            : Interface index. 
 *    Output(s)           : None. 
 *    Returns             : CFA_SUCCESS/CFA_FAILURE.
 *****************************************************************************/
INT4
CfaUtilIsConnectingPorts (UINT4 u4IfIndex)
{
    UINT4               u4StackPort = FALSE;
    UINT4               u4RemoteStackPort = FALSE;

    if (ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL)
    {

        CfaGetRmConnectingPortIfIndex (&u4StackPort);
        /* Logic to get the remote stack port :
         * Check the Local stack port * 2 is greater then 
         * MAX_PHYSICAL_INTERFACE. 
         * If so remote Stack is, local stack port divided by 2 
         * Else remote Stack is, local stack port multiplied by 2
         */
        if ((u4StackPort * 2) > SYS_DEF_MAX_PHYSICAL_INTERFACES)
        {
            u4RemoteStackPort = u4StackPort / 2;
        }
        else
        {
            u4RemoteStackPort = u4StackPort * 2;
        }
        if ((u4IfIndex == u4StackPort) || (u4IfIndex == u4RemoteStackPort))
        {
            return (CFA_SUCCESS);
        }
    }
    return (CFA_FAILURE);

}
#endif
/*****************************************************************************
 *    Function Name       : CfaGetSecondaryOobInterfaceName
 *    Description         : This function returns Secondary OOB interface Name
 *    Input(s)            : None
 *    Output(s)           : Secondary OOB  interface name
 *    Returns             : CFA_SUCCESS/CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGetSecondaryOobInterfaceName (UINT1 *pu1IfName)
{
    UINT1              *pReplaceStr = NULL;

    if (CFA_MGMT_PORT == FALSE)
    {
        return CFA_FAILURE;
    }

    /* Default Secondary OOB interface is :2 of MGMT interface
     * if MGMT interface name is eth0 or eth0:1 , secondary interface name would
     * be eth0:2 */

    STRNCPY (pu1IfName, CFA_CDB_IF_ALIAS (CFA_DEFAULT_ROUTER_IFINDEX),
             STRLEN (CFA_CDB_IF_ALIAS (CFA_DEFAULT_ROUTER_IFINDEX)));

    pReplaceStr = (UINT1 *) STRSTR ((CONST UINT1 *) pu1IfName, ":");
    if (pReplaceStr != NULL)
    {
        /* : is present in interface name so replace it with
         * Secondary OOB Prefix Name :2 */
        STRNCPY (pReplaceStr, CFA_SECONDARY_OOB_PREFIX_NAME,
                 STRLEN (CFA_SECONDARY_OOB_PREFIX_NAME));
        pReplaceStr[STRLEN (CFA_SECONDARY_OOB_PREFIX_NAME)] = '\0';
    }
    else
    {
        /* : is not present so take the full name and append
         * Secondary OOB Prefix Name :2 */
        STRNCAT (pu1IfName, CFA_SECONDARY_OOB_PREFIX_NAME,
                 STRLEN (CFA_SECONDARY_OOB_PREFIX_NAME));
        pu1IfName[STRLEN (pu1IfName) + STRLEN (CFA_SECONDARY_OOB_PREFIX_NAME)] =
            '\0';
    }

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaIsParentPort                                      */
/*                                                                           */
/* Description        : This function Validates whether the Physical port has*/
/*                      any L3 subinterface associated with it. If present   */
/*                      returns CFA_TRUE else returns CFA_FALSE              */
/*                                                                           */
/* Input(s)           : u4ParentIfIndex = Phyical port index                 */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_TRUE and CFA_FALSE                               */
/*****************************************************************************/
UINT4
CfaIsParentPort (UINT4 u4ParentIfIndex)
{
    UINT4               u4EndIfIndex = 0;
    UINT4               u4ParIfIndex = 0;
    UINT4               u4IfIndex = 0;

    u4IfIndex = CFA_MIN_L3SUB_IF_INDEX;
    u4EndIfIndex = CFA_MAX_L3SUB_IF_INDEX;
    /* Scan between Minimum L3Subifindex and Maximum l3subifindex */
    CFA_CDB_SCAN (u4IfIndex, u4EndIfIndex, CFA_L3SUB_INTF)
    {
        if ((CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_TRUE))
        {
            /* Get the parent index for this L3Subif index */
            CfaGetL3SubIfParentIndex (u4IfIndex, &u4ParIfIndex);

            if (u4ParentIfIndex == u4ParIfIndex)
            {
                /* Return CFA_TRUE if the parent index matches */
                return CFA_TRUE;
            }
        }
    }
    /* No L3Subinterface associated with this index */
    return CFA_FALSE;
}

/*****************************************************************************
 *    Function Name       : CfaIsVlanOverLapping
 *    Description         : This function checks whether the VLAN configured 
 *                          overlaps with anyother subinterface belonging to 
 *                          same parent                               
 *    Input(s)            : u4L3SubIfIndex - Subinterface Index
 *                          u2VlanId       - Encapsulation VLAN ID
 *    Output(s)           : NONE
 *    Returns             : CFA_TRUE/CFA_FALSE
 *****************************************************************************/
UINT4
CfaIsVlanOverLapping (UINT4 u4L3SubIfIndex, UINT2 u2VlanId)
{
    UINT4               u4EndIfIndex = 0;
    UINT4               u4ParIfIndex = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4ParentIfIndex = 0;
    UINT2               u2IfIvrVlanId = 0;
    /* Get ParentIndex for the given L3subinterface index */
    CfaGetL3SubIfParentIndex (u4L3SubIfIndex, &u4ParentIfIndex);

    u4IfIndex = CFA_MIN_L3SUB_IF_INDEX;
    u4EndIfIndex = CFA_MAX_L3SUB_IF_INDEX;
    /* Scan between Minimum L3Subifindex and Maximum l3subifindex */
    CFA_CDB_SCAN (u4IfIndex, u4EndIfIndex, CFA_L3SUB_INTF)
    {
        if ((CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_TRUE))
        {
            u2IfIvrVlanId = CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex);

            if (u2VlanId == u2IfIvrVlanId)
            {
                /* Get the parent index for this L3Subif index */
                CfaGetL3SubIfParentIndex (u4IfIndex, &u4ParIfIndex);

                if (u4ParentIfIndex == u4ParIfIndex)
                {
                    /* Return CFA_TRUE if the parent index matches */
                    return CFA_TRUE;
                }
            }
        }
    }
    /* No L3Subinterface associated with this index */
    return CFA_FALSE;
}

/*****************************************************************************
 *    Function Name       : CfaIsL3SubIfVLAN
 *    Description         : This function checks whether the VLAN configured
 *                          is associated to any subinterface Port.
 *    Input(s)            : u2VlanId       - Encapsulation VLAN ID
 *    Output(s)           : NONE
 *    Returns             : CFA_TRUE/CFA_FALSE
 *****************************************************************************/
UINT4
CfaIsL3SubIfVLAN (UINT2 u2VlanId)
{
    UINT4               u4EndIfIndex = 0;
    UINT4               u4IfIndex = 0;
    UINT2               u2IfIvrVlanId = 0;

    u4IfIndex = CFA_MIN_L3SUB_IF_INDEX;
    u4EndIfIndex = CFA_MAX_L3SUB_IF_INDEX;
    /* Scan between Minimum L3Subifindex and Maximum l3subifindex */
    CFA_CDB_SCAN (u4IfIndex, u4EndIfIndex, CFA_L3SUB_INTF)
    {
        if ((CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_TRUE))
        {
            u2IfIvrVlanId = CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex);

            if (u2VlanId == u2IfIvrVlanId)
            {
                return CFA_TRUE;
            }
        }
    }
    /* No L3Subinterface associated with this index */
    return CFA_FALSE;
}

/*****************************************************************************
 *    Function Name       : CfaVlanOperDownForSubIf
 *    Description         : This function will Make all Sub-Interfaces as
 *                          Down associated in current Vlan.
 *    Input(s)            : u2VlanId       - Encapsulation VLAN ID
 *    Output(s)           : NONE
 *    Returns             : CFA_TRUE/CFA_FALSE
 *****************************************************************************/
UINT4
CfaVlanOperDownForSubIf (UINT2 u2VlanId)
{
    UINT4               u4EndIfIndex = 0;
    UINT4               u4IfIndex = 0;
    UINT2               u2IfIvrVlanId = 0;
    UINT1               u1IntfOperStatus = CFA_IF_DOWN;
    u4IfIndex = CFA_MIN_L3SUB_IF_INDEX;
    u4EndIfIndex = CFA_MAX_L3SUB_IF_INDEX;

    CFA_CDB_SCAN (u4IfIndex, u4EndIfIndex, CFA_L3SUB_INTF)
    {
        if (CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_TRUE)
        {
            u2IfIvrVlanId = CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex);

            if (u2VlanId == u2IfIvrVlanId)
            {
                /* Sending Oper Down Indication to Sub-Interface of ports
                 * Associated with This Vlan */
                CfaInterfaceStatusChangeIndication (u4IfIndex,
                                                    u1IntfOperStatus);
            }
        }

    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *    Function Name       : CfaIsL3VlanOverLapWithL3SubVlan
 *    Description         : This function checks whether the VLAN configured
 *                          overlaps with anyother subinterface 
 *    Input(s)            : u4ContextId    - ContextId
 *                          u2VlanId       - Encapsulation VLAN ID
 *    Output(s)           : NONE
 *    Returns             : CFA_TRUE/CFA_FALSE
 *****************************************************************************/
UINT4
CfaIsL3VlanOverLapWithL3SubVlan (UINT4 u4ContextId, UINT2 u2VlanId)
{
    UINT4               u4EndIfIndex = 0;
    UINT4               u4IfIndex = 0;
    UINT2               u2IfIvrVlanId = 0;
    UINT4               u4IfCxtId = 0;

    u4IfIndex = CFA_MIN_L3SUB_IF_INDEX;
    u4EndIfIndex = CFA_MAX_L3SUB_IF_INDEX;
    /* Scan between Minimum L3Subifindex and Maximum l3subifindex */
    CFA_CDB_SCAN (u4IfIndex, u4EndIfIndex, CFA_L3SUB_INTF)
    {
        if ((CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_TRUE))
        {
            u2IfIvrVlanId = CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex);

            if (u2VlanId == u2IfIvrVlanId)
            {
                if ((VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4IfCxtId)
                     != VCM_FAILURE) && (u4IfCxtId == u4ContextId))

                {
                    return CFA_TRUE;
                }
            }
        }
    }
    /* No L3Subinterface associated with this index */
    return CFA_FALSE;
}

/*****************************************************************************
 *    Function Name       : CfaVlanValidateL3SubIf
 *    Description         : This function checks the given port is a parent
 *                          port and part of the given vlan. 
 *    Input(s)            : u4IfIndex      - Interface index
 *                          u2VlanId       - L2VlanId
 *    Output(s)           : NONE
 *    Returns             : CFA_TRUE/CFA_FALSE
 *****************************************************************************/
INT4
CfaVlanValidateL3SubIf (UINT4 u4IfIndex, UINT2 u2L2VlanId)
{
    UINT4               u4IfL2CxtId = 0;

    if (CfaIsParentPort (u4IfIndex) == CFA_FALSE)
    {
        return CFA_FAILURE;
    }

    if (VcmGetL2CxtIdForIpIface (u4IfIndex, &u4IfL2CxtId) != CLI_FAILURE)
    {

        if (CfaGetL3XSubIfVlanIndexInCxt (u4IfL2CxtId, u2L2VlanId,
                                          u4IfIndex) != CFA_INVALID_INDEX)
        {
            return CFA_SUCCESS;
        }
    }
    return CFA_FAILURE;
}
