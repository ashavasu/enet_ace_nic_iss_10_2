/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddwtga.c,v 1.8 2012/12/27 12:51:42 siva Exp $ 
 *
 * Description:This file contains the routines for the     
 *             Gemeric Device Driver Module of the CFA.    
 *             These routines are called at various places     
 *             in the CFA modules for interfacing with the    
 *             device drivers in the system. These routines   
 *             have to be modified when moving onto different  
 *             drivers. 
 *               Drivers currently supported are                  
 *                  SOCK_PACKET for Ethernet                      
 *                  WANIC HDLC driver                             
 *                  SANGOMA wanpipe driver                        
 *                  ETINC driver                                  
 *                  ATMVC's support                               
 *                              
 *******************************************************************/
#include "cfainc.h"
#include "cfaport.h"
#include "gddipdx.h"
#include "issnvram.h"
#include "fsvlan.h"

#ifdef NPAPI_WANTED
#include "cfanp.h"
#endif
#include "gddwtga.h"


/*******        DEBUG SWITCHES     *************/
//#define SHOW_DEBUG_MSG
//#define DEBUG_RXPKT_DUMP_EN_


/* SemId for NPAPI Lock and Unlock */
tOsixSemId          gNpapiSemId;
UINT1               au1NpapiSemName[] = "WTGNPAPI";

static UINT1        au1IntMapTable[SYS_DEF_MAX_PHYSICAL_INTERFACES][2][10] =
    { {"Slot0/1", "vlan4081"},
{"Slot0/2", "vlan4082"},
{"Slot0/3", "vlan4083"},
{"Slot0/4", "vlan4084"},
//{"Slot0/5", "eth4"},
//{"Slot0/6", "eth5"},
//{"Slot0/7", "eth6"},
//{"Slot0/8", "eth7"},
//{"Slot0/9", "eth8"},
};

tPortList gu1NullPortList;

/*****************************************************************************
 *
 *    Function Name        : CfaGddInit
 *
 *    Description        : This function performs the initialisation of
 *                the Generic Device Driver Module of CFA. This
 *                initialization routine should be called
 *                from the init of IFM after we have obtained
 *                the number of physical ports after parsing of
 *                the Config file. This function initializes the FD list
 *                and ifIndex array of the polling table.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : FdTable
 *
 *    Global Variables Modified : FdTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddInit (VOID)
{
    tMacAddr            au1SwitchMac;    /* Base MAC address */

    /* Create semaphore for NPAPI module */
    if (OsixSemCrt (au1NpapiSemName, &gNpapiSemId) != OSIX_SUCCESS)
    {
        PRINTF ("Failed to created semaphore for NPAPI module.\n");
        return CFA_FAILURE;
    }
    /* After creation semaphore must first be given before it can be taken,
     * because semafore is initialized by 0 on creation */
    OsixSemGive (gNpapiSemId);

    CfaGetSysMacAddress (au1SwitchMac);    /* Get the MAC address */
    CfaNpUpdateSwitchMac (au1SwitchMac);    /* Update the MAC address */

    if (CfaNpInit () != FNP_SUCCESS)
    {
        PRINTF ("ERROR[NP] - CfaNpInit returned failure\n\r");
        return CFA_FAILURE;
    }

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *    Function Name      : CfaGddProcessRecvInterruptEvent ()
 *    Description        : This function is invoked when an driver
 *                         event occurs. This routine invokes the packet
 *                         processing function if a packet is received
 *                         else it invokes the function which process the
 *                         driver status if status indication message is
 *                         received
 *    Input(s)           : VOID
 *    Output(s)          : None.

 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Returns                  : None.
 *****************************************************************************/
PUBLIC INT4
CfaGddProcessRecvInterruptEvent (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pBuf;
    UINT4               u4IfIndex;
    UINT4               u4PktSize;
    UINT1               u1IfType;

#ifdef DEBUG_RXPKT_DUMP_EN_
    UINT1               dumpBuf[256];
    UINT1 *             dumpBufScaner;
#endif

    while (OsixQueRecv (CFA_PACKET_MUX_QID, (UINT1 *) &pBuf,
                        OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        CFA_LOCK ();
        u4IfIndex = pBuf->ModuleData.InterfaceId.u4IfIndex;
        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
        CfaGetIfType ((UINT4) u4IfIndex, &u1IfType);
 
#ifdef DEBUG_RXPKT_DUMP_EN_
		printf ("CfaGddProcessRecvInterruptEvent: PktRcvd on IfIndex=%d Size=%d\n", u4IfIndex, u4PktSize);
		CRU_BUF_Copy_FromBufChain(pBuf, dumpBuf, 0, sizeof(dumpBuf));
		printf ("----- CfaGddProcessRecvInterruptEvent: Packet data (Hex) ---------\n");
		for(dumpBufScaner=dumpBuf; dumpBufScaner<dumpBuf+sizeof(dumpBuf); dumpBufScaner++)
			printf("%02x ", *dumpBufScaner);
		printf("\n");
#endif

		if( (u4IfIndex > 8) || (u4IfIndex == 0) )
		{
			CRU_BUF_Release_MsgBufChain (pBuf, FALSE);			
			//printf ("CfaGddProcessRecvInterruptEvent:ifIndex:%d out of scope\n",u4IfIndex);
			//CFA_UNLOCK ();
			//return (CFA_FAILURE);				
		}
		else if(CFA_IF_ENTRY(u4IfIndex) == NULL)
		{
			//printf ("CfaGddProcessRecvInterruptEvent:GET NULL RX_FNPTR; CFA_IF_ENTRY=NULL ifIndex:%d\n",u4IfIndex);
			//CFA_UNLOCK ();
			//return (CFA_FAILURE);			
		}
		else if(CFA_GDD_ENTRY(u4IfIndex) == NULL)
		{
			//printf ("CfaGddProcessRecvInterruptEvent:GET NULL RX_FNPTR; CFA_GDD_ENTRY=NULL ifIndex:%d\n",u4IfIndex);

			//CFA_UNLOCK ();
			//return (CFA_FAILURE);			
		}
    	else if (CFA_GDD_ENTRY(u4IfIndex)->fn_hl_rx == NULL )
		{
#ifdef DEBUG_RXPKT_DUMP_EN_
			printf ("CfaGddProcessRecvInterruptEvent:GET NULL RX_FNPTR; fn_read:%p, fn_write%p\n",
					CFA_GDD_ENTRY(u4IfIndex)->fn_read, CFA_GDD_ENTRY(u4IfIndex)->fn_write);
#endif
			//CFA_UNLOCK ();
			//return (CFA_FAILURE);
		}
        else if ((CFA_GDD_HL_RX_FNPTR (u4IfIndex))
            (pBuf, u4IfIndex, u4PktSize, u1IfType, CFA_ENCAP_NONE) 
		    != CFA_SUCCESS)
        {
            /* release buffer which were not successfully sent to higher layer
             */
#ifdef DEBUG_RXPKT_DUMP_EN_
        	printf ("CfaGddProcessRecvInterruptEvent: RX_FNPTR Fail\n");
#endif
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CFA_IF_SET_IN_DISCARD (u4IfIndex);
        }
        CFA_UNLOCK ();
    }
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddConfigPort
 *
 *    Description        : Function is called for configuration of the
 *                Enet ports. No such support is available for
 *                Wanic at present. Only the Promiscuous mode
 *                and multicast addresses can be configured at
 *                present for Enet ports. This is called
 *                directly whenever the Manager configures the
 *                ifTable for Ethernet ports. It can also be
 *                called indirectly by Bridge or RIP, etc.
 *                through the CfaIfmEnetConfigMcastAddr API.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *                UINT1 u1ConfigOption,
 *                VOID *pConfigParam.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaPhysIfTable (GDD Reg Table) structure.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if configuration succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption, VOID *pConfigParam)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1ConfigOption);
    UNUSED_PARAM (pConfigParam);
    return (CFA_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      :  CfaGddGetLinkStatus                             */
/*                                                                          */
/*    Description        : Gets the Physical Link Status of the device      */
/*    Input(s)           : Logical Port ,Vlan and the Data Buffer           */
/*                                                                          */
/*    Output(s)          : Link Status for the specific IfIndex             */
/*                                                                          */
/****************************************************************************/

UINT1
CfaGddGetLinkStatus (UINT4 u4IfIndex)
{
    UINT1               u1IfType;
    UINT1               u1OperStatus;

    CfaGetIfType (u4IfIndex, &u1IfType);
    if (u1IfType == CFA_ENET)
    {
        return CfaCfaNpGetLinkStatus (u4IfIndex);
    }
    else
    {
        CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
        return (u1OperStatus);
    }
}

/**************************************************************************/
VOID
CfaGddOsAddIfToList (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return;
}

/**************************************************************************/
VOID
CfaGddOsRemoveIfFromList (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddGetHwAddr
 *
 *    Description        : Function is called for getting the hardware
 *                address of the Enet ports. This function can
 *                be called only after the opening of the ports
 *                - the ports should be open but Admin/Oper
 *                Status can be down.
 *
 *    Input(s)            : UINT4 u4IfIndex.
 *
 *    Output(s)            : UINT1 *au1HwAddr.
 *
 *    Global Variables Referred : gIfGlobal (Interface's global struct),
 *                gaPhysIfTable (GDD Reg Table) structure.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if address is obtained,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddGetOsHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
    UINT1               au1TempAddr[CFA_ENET_ADDR_LEN] =
        { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05 };
    tNVRAM_DATA         NvRamData;
    UINT4               u4TempIfIndex = 0;
    static UINT1        au1Tempvar[CFA_ENET_ADDR_LEN] =
        { 0x00, 0x00, 0x00, 0x00, 0x00, 0x000 };
    UINT4               u4TempCount = 0;
    MEMSET (&NvRamData, 0, sizeof (tNVRAM_DATA));

    if (NvRamRead (&NvRamData) == FNP_SUCCESS)
    {
        MEMCPY (au1HwAddr, NvRamData.au1SwitchMac, CFA_ENET_ADDR_LEN);
    }
    else
    {
        MEMCPY (au1HwAddr, au1TempAddr, CFA_ENET_ADDR_LEN);
    }

    if (CfaIsL3IpVlanInterface (u4IfIndex) == CFA_SUCCESS)
    {
        return (CFA_SUCCESS);
    }

    u4TempIfIndex = u4IfIndex - 1;
    au1HwAddr[5] += u4TempIfIndex;
    for (u4TempCount = (CFA_ENET_ADDR_LEN - 1); u4TempCount > 1; u4TempCount--)
    {
        if ((au1HwAddr[u4TempCount] == 0x00) && u4TempIfIndex)
        {
            au1Tempvar[u4TempCount - 1]++;
        }
        au1HwAddr[u4TempCount - 1] =
            au1HwAddr[u4TempCount - 1] + au1Tempvar[u4TempCount - 1];
    }
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddShutdown
 *
 *    Description        : This function performs the shutdown of
 *                the Generic Device Driver Module of CFA. This
 *                routine frees the polling table structure.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : FdTable.
 *
 *    Global Variables Modified : FdTable.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
VOID
CfaGddShutdown (VOID)
{
}

/**************************************************************************/
INT4
CfaGddSetGddType (UINT4 u4IfIndex)
{
    UINT1               u1IfType;

    CfaGetIfType (u4IfIndex, &u1IfType);
    CFA_GDD_TYPE (u4IfIndex) = u1IfType;

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthOpen
 *
 *    Description        : This function opens the Ethernet port and stores the
                           descriptor in the interface table. Since the port is 
                           opened already during the registration time, this 
                           function is dummy.
 *
 *    Input(s)            : u4IfIndex - Interface Index
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable (Interface table)
 *                                structure
 *
 *    Global Variables Modified :  gapIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if open succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthOpen (UINT4 u4IfIndex)
{

    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthClose
 *
 *    Description        : First disables the promiscuous mode of 
 *                         the port, if enabled. The multicast address
 *                         list is lost when the port is closed.i
 *                         We dont need to check if the call is a 
 *                         success or not.
 *
 *    Input(s)            : u4IfIndex - Interface index.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable
 *
 *    Global Variables Modified : gapIfTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if close succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthClose (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthRead
 *
 *    Description        : This function reads the data from the ethernet port.
 *
 *    Input(s)            : pu1DataBuf - Pointer to the linear buffer.
 *                          u4IfIndex - IfIndex of the interface
 *                          pu4PktSize - Pointer to the packet size
 *
 *    Output(s)            : pu1DataBuf, pu1PktSize
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4PktSize);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthWrite
 *
 *    Description        : Writes the data to the ethernet driver.
 *
 *    Input(s)            : pu1DataBuf - Pointer to the linear buffer.
 *                          u4IfIndex - MIB-2 interface index
 *                          u4PktSize - Size of the buffer.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable (Interface table)
 *                                structure,
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if write succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    if (CfaNpPortWrite (pu1DataBuf, u4IfIndex, u4PktSize) != FNP_SUCCESS)
    {
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/**************************************************************************/
INT4
CfaGddOsProcessRecvEvent (VOID)
{
#if 0
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4IfIndex;
    UINT4               u4PktSize;
    UINT1               u1IfType;

/*
 * CCOMM TODO: We need to implement NPCfaUtilPortRead(). 
 *        For now, just return success.
 */

    while ((NpCfaUtilPortRead (&pBuf) != FNP_FAILURE))
    {

        if (pBuf == NULL)
        {
            continue;
        }

        CFA_LOCK ();
        u4IfIndex = pBuf->ModuleData.InterfaceId.u4IfIndex;
        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

        if (CfaGetIfType (u4IfIndex, &u1IfType) == CFA_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            PRINTF
                ("In CfaMain Pkt recvd in interrupt mode failed to deliver to HL.\n");
        }
        else
        {
            if ((CFA_GDD_HL_RX_FNPTR ((UINT2) u4IfIndex))
                (pBuf, u4IfIndex, u4PktSize, u1IfType,
                 CFA_ENCAP_NONE) != CFA_SUCCESS)
            {
                /* release buffer which were not successfully sent to higher
                 * layer */
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
            }
            else
            {
                /* increment after successful handling of packet */
                CFA_IF_SET_IN_OCTETS (u4IfIndex, u4PktSize);
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "In CfaMain Pkt recvd in interrupt mode delivered to HL.\n");
            }
        }

        CFA_UNLOCK ();
    }
#endif
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name       :CfaGddTxPktOnVlanMemberPorts 
 *
 *    Description         :This routine transmits the given Frame on the member
 *                         ports of the given Vlan.
 *
 *    Input(s)            : u4L2ContextId - L2 Context Id to which VlanId is 
 *                                          associated
 *                        : pu1DataBuf  - Pointer to the frame buffer 
 *                          u4PktSize   - Size of the frame 
 *                          VlanId      - Vlan on which the frame is to be 
 *                                        transmitted
 *                          bBcast      - Whether this frame is to be 
 *                                        broadcasted  
 *
 *    Output(s)           : None
 *
 *    Global Variables Referred : None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/ CFA_FAILURE.
 *****************************************************************************/

INT4
CfaGddTxPktOnVlanMemberPortsInCxt (UINT4 u4L2ContextId,
                                   UINT1 *pu1DataBuf, tVlanId VlanId,
                                   BOOL1 bBcast, UINT4 u4PktSize)
{
    tPortList          *pTagPorts = NULL;
    tPortList          *pUnTagPorts = NULL;
    tMacAddr            DestAddr;
    tCfaVlanInfo        VlanInfo;
    UINT4               u4BridgeMode;
#ifdef NPAPI_WANTED
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2EtherType = 0;
    UINT1               u1PortFlag;
#endif
    UINT4               u4OutPort;
    BOOL1               bIsTag;
    INT4                i4RetVal;
    UINT1              *pu1TempBuf = NULL;
    UINT1               TempDataBuf[CFA_ENET_MIN_UNTAGGED_FRAME_SIZE];
    UINT4               u4MinFrameMtu = 0;
    INT4                i4RetStat = CFA_SUCCESS;
    UINT1               u1PaddingDone = CFA_FALSE;
    MEMCPY (DestAddr, pu1DataBuf, CFA_ENET_ADDR_LEN);

    MEMSET (TempDataBuf, 0, sizeof (TempDataBuf));
    MEMSET (gu1NullPortList, 0, sizeof (tPortList));

    VlanInfo.u2VlanId = VlanId;
#ifdef NPAPI_WANTED
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
#ifdef MBSM_WANTED
        /* If the packet belongs to STACK IVR, then send the
         * packet via stack port using ATP*/
        if (VlanId == CFA_DEFAULT_STACK_VLAN_ID)
        {
            if (MbsmNpTxOnStackInterface (pu1DataBuf, u4PktSize) == FNP_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaUtilTxPktOnVlanMemberPorts - "
                         "Unable to tx on stack interface.\n");
                return CFA_FAILURE;
            }
            return CFA_SUCCESS;
        }
#endif
    }
#endif

    pTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        return CFA_FAILURE;
    }

    pUnTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pUnTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        return CFA_FAILURE;
    }

    MEMSET (*pTagPorts, 0, sizeof (tPortList));
    MEMSET (*pUnTagPorts, 0, sizeof (tPortList));
    i4RetVal = VlanIvrGetTxPortOrPortListInCxt (u4L2ContextId,
                                                DestAddr, VlanId, bBcast,
                                                &u4OutPort, &bIsTag, *pTagPorts,
                                                *pUnTagPorts);

    if (i4RetVal == VLAN_FORWARD)
    {
        /*ASSUMPTION: The Packet reached here will be untagged always.
         * Since bcoz this thread is called only for the outgoing packet
         * from IP.*/

        u4MinFrameMtu = CFA_ENET_MIN_UNTAGGED_FRAME_SIZE;

        if (u4PktSize < u4MinFrameMtu)
        {
            pu1TempBuf = pu1DataBuf;
            MEMCPY (TempDataBuf, pu1DataBuf, u4PktSize);
            pu1DataBuf = TempDataBuf;
            u4PktSize = u4MinFrameMtu;
            u1PaddingDone = CFA_TRUE;
        }

        CfaTxFillVlanInfo (DestAddr, u4OutPort,
                           bIsTag, *pTagPorts, *pUnTagPorts, &VlanInfo);
        if (L2IwfGetBridgeMode (L2IWF_DEFAULT_CONTEXT, &u4BridgeMode) ==
            L2IWF_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaGddWrite - "
                     "L2IWF-GetBridgeMode -  FAILURE.\n");
            i4RetStat = CFA_FAILURE;
        }
        else
        {
#ifdef NPAPI_WANTED
            if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
            {
                if ((u4BridgeMode == L2IWF_CUSTOMER_BRIDGE_MODE) ||
                    (u4BridgeMode == L2IWF_PROVIDER_BRIDGE_MODE))
                {
                    if (CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo)
                        != FNP_SUCCESS)
                    {
                        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                 "Error in CfaGddWrite - "
                                 "Unsuccessful Driver Write -  FAILURE.\n");
                        i4RetStat = CFA_FAILURE;
                    }
                }
                else if ((u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE) ||
                         (u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE))
                {
                    if (u4OutPort != VLAN_INVALID_PORT)
                    {
                        if (CfaHwL3VlanIntfWrite
                            (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                        {
                            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                     "Error in CfaGddWrite - "
                                     "Unsuccessful Driver Write -  FAILURE.\n");
                            i4RetStat = CFA_FAILURE;
                        }
                    }
                    else
                    {
                        for (u2ByteInd = 0; u2ByteInd < BRG_PORT_LIST_SIZE;
                             u2ByteInd++)
                        {
                            if ((*pTagPorts)[u2ByteInd] == 0)
                            {
                                continue;
                            }

                            u1PortFlag = (*pTagPorts)[u2ByteInd];

                            for (u2BitIndex = 0;
                                 ((u2BitIndex < BITS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
                            {
                                if ((u1PortFlag & 0x80) != 0)
                                {
                                    VlanInfo.u2PktType = CFA_NP_KNOWN_UCAST_PKT;

                                    VlanInfo.unPortInfo.TxUcastPort.u2TxPort =
                                        (UINT2) ((u2ByteInd * BITS_PER_BYTE) +
                                                 u2BitIndex + 1);

                                    u4OutPort =
                                        VlanInfo.unPortInfo.TxUcastPort.
                                        u2TxPort;

#ifdef VLAN_WANTED
                                    VlanGetPortEtherType (u4OutPort,
                                                          &u2EtherType);
#endif
                                    VlanInfo.unPortInfo.TxUcastPort.
                                        u2EtherType = u2EtherType;

                                    VlanInfo.unPortInfo.TxUcastPort.u1Tag =
                                        CFA_NP_TAGGED;

                                    CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize,
                                                          VlanInfo);
                                }
                                u1PortFlag = (UINT1) (u1PortFlag << 1);
                            }
                        }
                        MEMSET (VlanInfo.unPortInfo.TxPorts.pTagPorts, 0,
                                sizeof (tPortList));
                        /* 
                         * In 802.1ad Bridges, for untagged ports, the tag ethertype 
                         * need not be filled in based on the port ethertype. 
                         * Hence calling the API for packet transmission on L3 
                         * interfaces directly.
                         */
                         if (MEMCMP (VlanInfo.unPortInfo.TxPorts.pUnTagPorts,
                                     gu1NullPortList, sizeof(tPortList)) != 0)
                         {
                             if (CfaHwL3VlanIntfWrite
                                (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                             {
                                 i4RetStat = CFA_FAILURE;
                             }
                        }
                    }
                }
            }
#else
            if (CfaL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo) !=
                CFA_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddWrite - "
                         "Unsuccessful Driver Write -  FAILURE.\n");
                i4RetStat = CFA_FAILURE;

            }
#endif
        }
        if (u1PaddingDone == CFA_TRUE)
        {
            pu1DataBuf = pu1TempBuf;
        }
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pUnTagPorts));
        return i4RetStat;
    }

    FsUtilReleaseBitList ((UINT1 *) pTagPorts);
    FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pTagPorts));
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pUnTagPorts));
    return CFA_FAILURE;
}

/*************** [G.M] those API should mode to enet implemenatation **********************/
/*****************************************************************************
 *
 *    Function Name        : CfaGddGetLnxIntfnameForPort
 *
 *    Description          : This function Returns LinuxEth Name Based on Slot 
 *                           Referred. 
 *    Input(s)            :  u4IfIndex - Interface index
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure 
 *                
 *    Global Variables Modified : gaIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : EthName if Slot is found Else NULL
 *
 *****************************************************************************/
UINT1              *
CfaGddGetLnxIntfnameForPort (UINT2 u2Index)
{
    INT4                i4Index;
    UINT1               au1Temp[CFA_MAX_PORT_NAME_LENGTH];

    if (u2Index == 0)
    {
        return NULL;
    }

    for (i4Index = 0; i4Index < SYS_DEF_MAX_PHYSICAL_INTERFACES; i4Index++)
    {
        MEMSET (au1Temp, 0, CFA_MAX_PORT_NAME_LENGTH);

        STRCPY (au1Temp, au1IntMapTable[i4Index]);

        fprintf(stdout,"CFA_GDD_PORT_NAME:%s\r\n",CFA_GDD_PORT_NAME (u2Index));
        fprintf(stdout,"sizeof:%d\r\n",STRLEN (CFA_GDD_PORT_NAME (u2Index)));

        if (MEMCMP (CFA_GDD_PORT_NAME (u2Index), au1Temp,
                    STRLEN (CFA_GDD_PORT_NAME (u2Index))) == 0)
        {
            return au1IntMapTable[i4Index][1];
        }
    }
    return NULL;
}

INT4
CfaNpInit (VOID)
{
    return FNP_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddSetLnxIntfnameForPort
 *
 *    Description          : This function sets Interface Name for the interface
 *                           index.
 *    Input(s)            :  u4IfIndex - Interface index
 *                           pu1IfName - Interface name
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure
 *
 *    Global Variables Modified : gaIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : NULL
 *
 *****************************************************************************/
VOID        
CfaGddSetLnxIntfnameForPort (UINT4 u4Index, UINT1 * pu1IfName)
{
    return;
}

/*****************************************************************************
 *    Function Name             : CfaGddEthWriteWithPri
 *    Description               : Writes the data to the ethernet driver.
 *    Input(s)                  : pu1DataBuf - Pointer to the linear buffer.
 *                                u4IfIndex  - MIB-2 interface index
 *                                u4PktSize  - Size of the buffer.
 *                                u1Priority - Traffic class on which Pkt to
 *					        be TXed
 *    Output(s)                 : None.
 *    Global Variables Referred : None
 *    Global Variables Modified : None.
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : CFA_SUCCESS if write succeeds,
 *                                otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGddEthWriteWithPri (UINT1 *pData, UINT4 u4IfIndex, UINT4 u4PktLen, 
		       UINT1 u1Priority)
{
    if(CfaGddEthWrite(pData,u4IfIndex,u4PktLen) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }
    UNUSED_PARAM (u1Priority);
    return CFA_SUCCESS;
}
/*
INT4
CfaHwL3VlanIntfWrite (UINT1 *pu1DataBuf, UINT4 u4PktSize, tCfaVlanInfo VlanInfo)
{
	return FNP_SUCCESS;
}
*/
