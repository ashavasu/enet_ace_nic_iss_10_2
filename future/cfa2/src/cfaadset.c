#include "cfainc.h"

extern UINT1        gCFANpSyncBlk;

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsCfaHwCreateInternalPort
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsCfaHwCreateInternalPort (UINT4 u4IfIndex)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.FsCfaHwCreateInternalPort.u4IfIndex = u4IfIndex;
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (CFA_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncFsCfaHwCreateInternalPortSync (NpSync.
                                                     FsCfaHwCreateInternalPort,
                                                     RM_CFA_APP_ID,
                                                     NPSYNC_FS_CFA_HW_CREATE_INTERNAL_PORT);
            }
        }
#undef FsCfaHwCreateInternalPort
        i4RetVal = FsCfaHwCreateInternalPort (u4IfIndex);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (CFA_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncFsCfaHwCreateInternalPortSync (NpSync.
                                                         FsCfaHwCreateInternalPort,
                                                         RM_CFA_APP_ID,
                                                         NPSYNC_FS_CFA_HW_CREATE_INTERNAL_PORT);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (CFA_NPSYNC_BLK () == OSIX_FALSE)
        {
            CfaHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_FS_CFA_HW_CREATE_INTERNAL_PORT,
                                           0);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsCfaHwDeleteInternalPort
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsCfaHwDeleteInternalPort (UINT4 u4IfIndex)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.FsCfaHwDeleteInternalPort.u4IfIndex = u4IfIndex;
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (CFA_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncFsCfaHwDeleteInternalPortSync (NpSync.
                                                     FsCfaHwDeleteInternalPort,
                                                     RM_CFA_APP_ID,
                                                     NPSYNC_FS_CFA_HW_DELETE_INTERNAL_PORT);
            }
        }
#undef FsCfaHwDeleteInternalPort
        i4RetVal = FsCfaHwDeleteInternalPort (u4IfIndex);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (CFA_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncFsCfaHwDeleteInternalPortSync (NpSync.
                                                         FsCfaHwDeleteInternalPort,
                                                         RM_CFA_APP_ID,
                                                         NPSYNC_FS_CFA_HW_DELETE_INTERNAL_PORT);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (CFA_NPSYNC_BLK () == OSIX_FALSE)
        {
            CfaHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_FS_CFA_HW_DELETE_INTERNAL_PORT,
                                           0);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsCfaHwCreateILan
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsCfaHwCreateILan (UINT4 u4ILanIndex, tHwPortArray ILanPortArray)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.FsCfaHwCreateILan.u4ILanIndex = u4ILanIndex;
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (CFA_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncFsCfaHwCreateILanSync (NpSync.FsCfaHwCreateILan,
                                             RM_CFA_APP_ID,
                                             NPSYNC_FS_CFA_HW_CREATE_I_LAN);
            }
        }
#undef FsCfaHwCreateILan
        i4RetVal = FsCfaHwCreateILan (u4ILanIndex, ILanPortArray);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (CFA_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncFsCfaHwCreateILanSync (NpSync.FsCfaHwCreateILan,
                                                 RM_CFA_APP_ID,
                                                 NPSYNC_FS_CFA_HW_CREATE_I_LAN);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (CFA_NPSYNC_BLK () == OSIX_FALSE)
        {
            CfaHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_FS_CFA_HW_CREATE_I_LAN, 0);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsCfaHwDeleteILan
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsCfaHwDeleteILan (UINT4 u4ILanIndex)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.FsCfaHwDeleteILan.u4ILanIndex = u4ILanIndex;
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (CFA_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncFsCfaHwDeleteILanSync (NpSync.FsCfaHwDeleteILan,
                                             RM_CFA_APP_ID,
                                             NPSYNC_FS_CFA_HW_DELETE_I_LAN);
            }
        }
#undef FsCfaHwDeleteILan
        i4RetVal = FsCfaHwDeleteILan (u4ILanIndex);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (CFA_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncFsCfaHwDeleteILanSync (NpSync.FsCfaHwDeleteILan,
                                                 RM_CFA_APP_ID,
                                                 NPSYNC_FS_CFA_HW_DELETE_I_LAN);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (CFA_NPSYNC_BLK () == OSIX_FALSE)
        {
            CfaHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_FS_CFA_HW_DELETE_I_LAN, 0);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsCfaHwRemovePortFromILan
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsCfaHwRemovePortFromILan (UINT4 u4ILanIndex, UINT4 u4PortIndex)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.FsCfaHwRemovePortFromILan.u4ILanIndex = u4ILanIndex;
    NpSync.FsCfaHwRemovePortFromILan.u4PortIndex = u4PortIndex;
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (CFA_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncFsCfaHwRemovePortFromILanSync (NpSync.
                                                     FsCfaHwRemovePortFromILan,
                                                     RM_CFA_APP_ID,
                                                     NPSYNC_FS_CFA_HW_REMOVE_PORT_FROM_I_LAN);
            }
        }
#undef FsCfaHwRemovePortFromILan
        i4RetVal = FsCfaHwRemovePortFromILan (u4ILanIndex, u4PortIndex);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (CFA_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncFsCfaHwRemovePortFromILanSync (NpSync.
                                                         FsCfaHwRemovePortFromILan,
                                                         RM_CFA_APP_ID,
                                                         NPSYNC_FS_CFA_HW_REMOVE_PORT_FROM_I_LAN);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (CFA_NPSYNC_BLK () == OSIX_FALSE)
        {
            CfaHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_FS_CFA_HW_REMOVE_PORT_FROM_I_LAN,
                                           0);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsCfaHwAddPortToILan
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsCfaHwAddPortToILan (UINT4 u4ILanIndex, UINT4 u4PortIndex)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.FsCfaHwAddPortToILan.u4ILanIndex = u4ILanIndex;
    NpSync.FsCfaHwAddPortToILan.u4PortIndex = u4PortIndex;
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (CFA_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncFsCfaHwAddPortToILanSync (NpSync.FsCfaHwAddPortToILan,
                                                RM_CFA_APP_ID,
                                                NPSYNC_FS_CFA_HW_ADD_PORT_TO_I_LAN);
            }
        }
#undef FsCfaHwAddPortToILan
        i4RetVal = FsCfaHwAddPortToILan (u4ILanIndex, u4PortIndex);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (CFA_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncFsCfaHwAddPortToILanSync (NpSync.FsCfaHwAddPortToILan,
                                                    RM_CFA_APP_ID,
                                                    NPSYNC_FS_CFA_HW_ADD_PORT_TO_I_LAN);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (CFA_NPSYNC_BLK () == OSIX_FALSE)
        {
            CfaHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_FS_CFA_HW_ADD_PORT_TO_I_LAN,
                                           0);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsCfaHwCreateInternalPortSync
 *
 * -------------------------------------------------------------
 */
VOID
NpSyncFsCfaHwCreateInternalPortSync (tNpSyncFsCfaHwCreateInternalPort
                                     aHwCreateInternalPort, UINT4 u4AppId,
                                     UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, aHwCreateInternalPort.u4IfIndex);
        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsCfaHwDeleteInternalPortSync
 *
 * -------------------------------------------------------------
 */
VOID
NpSyncFsCfaHwDeleteInternalPortSync (tNpSyncFsCfaHwDeleteInternalPort
                                     aHwDeleteInternalPort, UINT4 u4AppId,
                                     UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, aHwDeleteInternalPort.u4IfIndex);
        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsCfaHwCreateILanSync
 *
 * -------------------------------------------------------------
 */
VOID
NpSyncFsCfaHwCreateILanSync (tNpSyncFsCfaHwCreateILan aHwCreateILan,
                             UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, aHwCreateILan.u4ILanIndex);
        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsCfaHwDeleteILanSync
 *
 * -------------------------------------------------------------
 */
VOID
NpSyncFsCfaHwDeleteILanSync (tNpSyncFsCfaHwDeleteILan aHwDeleteILan,
                             UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, aHwDeleteILan.u4ILanIndex);
        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsCfaHwRemovePortFromILanSync
 *
 * -------------------------------------------------------------
 */
VOID
NpSyncFsCfaHwRemovePortFromILanSync (tNpSyncFsCfaHwRemovePortFromILan
                                     aHwRemovePortFromILan, UINT4 u4AppId,
                                     UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        sizeof (UINT4) + sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              aHwRemovePortFromILan.u4ILanIndex);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              aHwRemovePortFromILan.u4PortIndex);
        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId, u4AppId) ==
            RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsCfaHwAddPortToILanSync
 *
 * -------------------------------------------------------------
 */
VOID
NpSyncFsCfaHwAddPortToILanSync (tNpSyncFsCfaHwAddPortToILan aHwAddPortToILan,
                                UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        sizeof (UINT4) + sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, aHwAddPortToILan.u4ILanIndex);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, aHwAddPortToILan.u4PortIndex);
        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}
