/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddqnx.c,v 1.6 2016/07/23 11:41:26 siva Exp $
 *
 * Description:This file contains the routines for the
 *             Gemeric Device Driver Module of the CFA.
 *             These routines are called at various places
 *             in the CFA modules for interfacing with the
 *             device drivers in the system. These routines
 *             have to be modified when moving onto different
 *             drivers.
 *               Drivers currently supported are
 *                  SOCK_PACKET for Ethernet
 *                  WANIC HDLC driver
 *                  SANGOMA wanpipe driv
 *                  ETINC driver
 *                  ATMVC's support
 *
 *******************************************************************/

#include "cfainc.h"
#include "gddapi.h"

/**************************** GLOBAL DEFINITIONS *****************************/

tFdListStruct       FdTable;    /* list of all the file descriptors given to the select
                                   call during polling */

UINT1              *CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
VOID               CfaGddSetLnxIntfnameForPort (UINT4 u4Index, UINT1 * pu1IfName);

/* Array containing the Mapping between Slot and Eth Interfaces */
static UINT1        au1IntMapTable[SYS_DEF_MAX_PHYSICAL_INTERFACES][2][CFA_MAX_PORT_NAME_LENGTH] =
    { {"Slot0/1", "en0"},
{"Slot0/2", "en1"},
{"Slot0/3", "en2"},
{"Slot0/4", "en3"},
{"Slot0/5", "en4"},
{"Slot0/6", "en5"},
{"Slot0/7", "en6"},
{"Slot0/8", "en7"},
{"Slot0/9", "en8"},
{"Slot0/10", "en9"},
{"Slot0/11", "en10"},
{"Slot0/12", "en11"},
{"Slot0/13", "en12"},
{"Slot0/14", "en13"},
{"Slot0/15", "en14"},
{"Slot0/16", "en15"},
{"Slot0/17", "en16"},
{"Slot0/18", "en17"},
{"Slot0/19", "en18"},
{"Slot0/20", "en19"},
{"Slot0/21", "en20"},
{"Slot0/22", "en21"},
{"Slot0/23", "en22"},
{"Slot0/24", "en23"}
};

/*****************************************************************************
 *
 *    Function Name        : CfaGddInit
 *
 *    Description        : This function performs the initialisation of
 *                the Generic Device Driver Module of CFA. This
 *                initialization routine should be called
 *                from the init of IFM after we have obtained
 *                the number of physical ports after parsing of
 *                the Config file. This function initializes the FD list
 *                and ifIndex array of the polling table.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : FdTable
 *
 *    Global Variables Modified : FdTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddInit (VOID)
{

    CFA_DBG (CFA_TRC_ALL, CFA_GDD, "Entering CfaGddInit \n");

    /* allocate memory for the GDD File Descriptor - polling table */
    CFA_GDD_FDLIST () = &aGddFdList[0];

    /* allocate memory for the ifIndex array - polling table */
    CFA_GDD_IFINDEX_LIST () = &au4GddIfIndexList[0];

    CFA_GDD_FDLIST_LAST_INDEX () = 0;

    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_GDD,
             "Exiting CfaGddInit - successful Init.\n");

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddShutdown
 *
 *    Description        : This function performs the shutdown of
 *                the Generic Device Driver Module of CFA. This
 *                routine frees the polling table structure.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : FdTable.
 *
 *    Global Variables Modified : FdTable.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
VOID
CfaGddShutdown (VOID)
{
    CFA_DBG (CFA_TRC_ALL, CFA_GDD, "Entering CfaGddShutdown \n");

    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_GDD, "Exiting CfaGddShutdown - SUCCESS.\n");
}

/**************************************************************************/
INT4
CfaGddSetGddType (UINT4 u4IfIndex)
{
    UINT1               u1IfType;

    CfaGetIfType (u4IfIndex, &u1IfType);

    CFA_GDD_TYPE (u4IfIndex) = u1IfType;

    return (CFA_SUCCESS);
}

/**************************************************************************/
VOID
CfaGddOsRemoveIfFromList (UINT4 u4IfIndex)
{
    UINT2               u2PollTableIndex = 0;
/* remove the port from the polling table */
    for (; u2PollTableIndex < CFA_GDD_FDLIST_LAST_INDEX (); ++u2PollTableIndex)
    {
        if (u4IfIndex == CFA_GDD_FDLIST_IFINDEX (u2PollTableIndex))
        {
            CFA_GDD_FDLIST_DEINIT (u2PollTableIndex);
            break;
        }
    }
    return;
}

/**************************************************************************/
VOID
CfaGddOsAddIfToList (UINT4 u4IfIndex)
{
    UINT1               u1IfType;

    CfaGetIfType (u4IfIndex, &u1IfType);

    CFA_GDD_FDLIST_INIT (u4IfIndex, CFA_GDD_PORT_DESC (u4IfIndex));
}

/**************************************************************************/
INT4
CfaGddOsProcessRecvEvent (VOID)
{
    UINT4               u4IfIndex;
    INT4                i4ReadPortNum = 0;
    UINT2               u2PollTableIndex = 0;
    UINT1              *pu1DataBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4PktSize;
    UINT2               u2PortIfIndex;
    UINT1               u1IfType;

/* MAX_DRIVER_MTU * 2 in order to allow for the worst case of byte stuffing
 * for async interfaces where the packet size doubles over the MTU */
    if ((pu1DataBuf = MemAllocMemBlk (gCfaDriverMtuPoolId)) == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaGddProcessRecvEvent - " "No Memory - FAILURE.\n");

        return (CFA_FAILURE);
    }

    if ((i4ReadPortNum = poll (CFA_GDD_FDLIST (), CFA_GDD_FDLIST_LAST_INDEX (),
                               10)) < 0)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaGddProcessRecvEvent - Poll Call - FAILURE.\n");
        return (CFA_FAILURE);
    }

    while ((i4ReadPortNum > 0) &&
           (u2PollTableIndex < CFA_GDD_FDLIST_LAST_INDEX ()))
    {
        u4IfIndex = CFA_GDD_FDLIST_IFINDEX (u2PollTableIndex);

        /* Check the validity of the index */
        if (u4IfIndex == 0)
        {
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                      "Error in CfaGddProcessRecvEvent\n"
                      "Value at index %d in FdTable is 0.\n", u2PollTableIndex);

            MemReleaseMemBlock (gCfaDriverMtuPoolId, (UINT1 *) pu1DataBuf);

            return (CFA_FAILURE);
        }

        CfaGetIfType (u4IfIndex, &u1IfType);

        u2PortIfIndex = (UINT2) u4IfIndex;

        /* if data is not available then dont do read operation */
        if (!CFA_GDD_IS_DATA_AVAIL (u2PollTableIndex))
        {
            ++u2PollTableIndex;
            continue;
        }

        --i4ReadPortNum;
        /* now read all the data from the interface */

        if ((CFA_GDD_READ_FNPTR (u2PortIfIndex))
            (pu1DataBuf, u4IfIndex, &u4PktSize) != CFA_FAILURE)
        {
            CFA_LOCK ();

            CFA_DBG1 (CFA_TRC_ALL, CFA_GDD,
                      "In CfaGddProcessRecvEvent - got data from interface %d.\n",
                      u4IfIndex);
            CFA_DBG1 (CFA_TRC_ALL, CFA_GDD,
                      "In CfaGddProcessRecvEvent - got %d bytes.\n", u4PktSize);

#ifdef RMON_WANTED
/* Forward the frames to RMON module only when SW_FWD is enabled */
#ifdef SW_FWD
/* send packet to RMON before converting the buffer to CRU */
            if (CFA_GDD_TYPE (u2PortIfIndex) == CFA_ETHERNET)
            {
                /*invoke rmon update tables routine */
                if (CfaIwfRmonUpdateTables (pu1DataBuf, u4IfIndex, u4PktSize)
                    != CFA_FAILURE)
                {
                    CFA_DBG (CFA_TRC_ALL, CFA_GDD,
                             "In CfaGddProcessRecvEvent - update RMON tables by invoking CfaIwfRmonUpdateTables \n");
                }
                else
                {
                    CFA_DBG (CFA_TRC_ERROR, CFA_GDD,
                             "In CfaGddProcessRecvEvent - invoke CfaIwfRmonUpdateTables function failed \n");
                }
            }
#endif /* SW_FWD */
#endif /* RMON_WANTED */
/* allocate CRU Buf */
            if ((pBuf = CRU_BUF_Allocate_MsgBufChain ((u4PktSize +
                                                       CFA_MAX_FRAME_HEADER_SIZE),
                                                      CFA_MAX_FRAME_HEADER_SIZE))
                == NULL)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddProcessRecvEvent - "
                         "No CRU Buf Available - FAILURE.\n");

                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                MemReleaseMemBlock (gCfaDriverMtuPoolId, (UINT1 *) pu1DataBuf);
                CFA_UNLOCK ();
                return CFA_FAILURE;
            }

/* copy data from linear buffer into CRU buffer - it is expected that the
higher layers will fill the CRU interface struct if required */
            if (CRU_BUF_Copy_OverBufChain (pBuf, pu1DataBuf, 0, u4PktSize) ==
                CRU_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddProcessRecvEvent - "
                         "unable to copy to CRU Buf- FAILURE.\n");

                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                CFA_UNLOCK ();
                /*continue; */
            }

/* send the buffer to the higher layer - the port is not opened unless
some layer is defined abover this */

            if (CFA_GDD_HL_RX_FNPTR (u2PortIfIndex) == NULL)
            {
/* Higher layer is not found. Release buffer */
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                CFA_UNLOCK ();
                /*continue; */
            }

            if ((CFA_GDD_HL_RX_FNPTR (u2PortIfIndex))
                (pBuf, u4IfIndex, u4PktSize, u1IfType,
                 CFA_ENCAP_NONE) != CFA_SUCCESS)
            {
/* release buffer which were not successfully sent to higher layer */
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            }

            CFA_UNLOCK ();
        }                        /* end of while loop - read while data is available */

        ++u2PollTableIndex;

    }                            /* end of while loop - polling */

/* free the linear buffer allocated */
    MemReleaseMemBlock (gCfaDriverMtuPoolId, (UINT1 *) pu1DataBuf);
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddConfigPort
 *
 *    Description        : Function is called for configuration of the
 *                Enet ports. No such support is available for
 *                Wanic at present. Only the Promiscuous mode
 *                and multicast addresses can be configured at
 *                present for Enet ports. This is called
 *                directly whenever the Manager configures the
 *                ifTable for Ethernet ports. It can also be
 *                called indirectly by Bridge or RIP, etc.
 *                through the CfaIfmEnetConfigMcastAddr API.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *                UINT1 u1ConfigOption,
 *                VOID *pConfigParam.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaPhysIfTable (GDD Reg Table) structure.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if configuration succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption, VOID *pConfigParam)
{
    (VOID) u4IfIndex;
    (VOID) u1ConfigOption;
    (void) pConfigParam;

}

/*****************************************************************************
 *
 *    Function Name        : CfaGddGetOsHwAddr
 *
 *    Description        : Function is called for Opening the port
 *                         and getting the Hw Address of the port
 *
 *    Input(s)            : UINT4 u4IfIndex.
 *
 *    Output(s)            : UINT1 *au1HwAddr.
 *
 *    Global Variables Referred : gIfGlobal (Interface's global struct),
 *                gaPhysIfTable (GDD Reg Table) structure.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if address is obtained,
 *                otherwise CFA_FAILURE.
 *
*****************************************************************************/

INT4
CfaGddGetOsHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
    struct ifaddrs     *ifaddr, *ifa;
    char                host[NI_MAXHOST];
    UINT1              *pu1PortName = NULL;
    struct ifreq        ifr;
    int                 len = CFA_MAX_DRIVER_MTU;

    if (u4IfIndex == 0)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddGetOsHwAddr - "
                  " Unknown Interface %u  - FAILURE.\n", u4IfIndex);
        return CFA_FAILURE;
    }

    /* Open the raw network interface */
    if (0 > (CFA_GDD_PORT_DESC (u4IfIndex) = open ("/dev/socket/bpf", O_RDWR)))
    {
        perror ("open BPF FAILED");
        return CFA_FAILURE;
    }

    pu1PortName = CfaGddGetLnxIntfnameForPort (u4IfIndex);

    if (pu1PortName == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddConfigPort - "
                  "Not able to get Interface name for %d - FAILURE.\n",
                  u4IfIndex);
        return CFA_FAILURE;
    }

    sprintf (ifr.ifr_name, "%s", pu1PortName);

    if (ioctl (CFA_GDD_PORT_DESC (u4IfIndex), BIOCSBLEN, &len))
    {
        perror (" Failure To Set Buffer Length ");
        return CFA_FAILURE;
    }

    if (ioctl (CFA_GDD_PORT_DESC (u4IfIndex), BIOCSETIF, &ifr) < 0)
    {
        perror ("Failed to register interface  ");
        return CFA_FAILURE;
    }

    if (ioctl (CFA_GDD_PORT_DESC (u4IfIndex), BIOCPROMISC, NULL) < 0)
    {
        perror ("Failed to set promiscous ");
        return CFA_FAILURE;
    }

    if (getifaddrs (&ifaddr) == -1)
    {
        perror (" Unable to Get the Hw Address getifaddrs\n");
        return (CFA_FAILURE);
    }

    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
    {
        if ((STRCMP (ifa->ifa_name, ifr.ifr_name)) == 0)
        {
            getnameinfo (ifa->ifa_addr,
                         sizeof (struct sockaddr_in),
                         host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
            MEMCPY (au1HwAddr, host, 6);
        }
    }
    freeifaddrs (ifaddr);
    return CFA_SUCCESS;
}

INT4
CfaGddLinuxEthSockOpen (UINT4 u4IfIndex)
{
    /* For QNX, BPF (Barkley Packet Filters ) are used to 
       Read/Write packets. Sockets calls are not used */
    (VOID) u4IfIndex;

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthOpen
 *
 *    Description        : This function opens the Ethernet port and stores the
                           descriptor in the interface table. Since the port is 
                           opened already during the registration time, this 
                           function is dummy.
 *
 *    Input(s)            : u4IfIndex - Interface Index
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable (Interface table)
 *                                structure
 *
 *    Global Variables Modified :  gapIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if open succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthOpen (UINT4 u4IfIndex)
{
/* socket already opened during register call */
    /* during admin status down promiscous mode is disabled, when we make the *
     * admin status up we have to enable the promiscuous mode */
    /* CfaGddConfigPort (u4IfIndex, CFA_ENET_EN_PROMISC, NULL); */
    u4IfIndex = u4IfIndex;        /* for warning removal */
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthClose
 *
 *    Description        : First disables the promiscuous mode of 
 *                         the port, if enabled. The multicast address
 *                         list is lost when the port is closed.i
 *                         We dont need to check if the call is a 
 *                         success or not.
 *
 *    Input(s)            : u4IfIndex - Interface index.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable
 *
 *    Global Variables Modified : gapIfTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if close succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthClose (UINT4 u4IfIndex)
{
    /* first disable the promiscuous mode of the port,
     * if enabled. the multicast address list is lost
     * when the port is closed. We dont need to check if the
     * call is a success or not. 
     */

    CfaGddConfigPort (u4IfIndex, CFA_ENET_DIS_PROMIS, NULL);

    /* closing the SOCK PACKET results in deletion of
     * the ethX interface from Linux itself - hence
     * commented out close(CFA_GDD_PORT_DESC(u4IfIndex));
     */
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthrite
 *
 *    Description        : Writes the data to the ethernet driver.
 *
 *    Input(s)            : pu1DataBuf - Pointer to the linear buffer.
 *                          u4IfIndex - MIB-2 interface index
 *                          u4PktSize - Size of the buffer.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable (Interface table)
 *                                structure,
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if write succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    struct ifreq        ifr;
    UINT1              *pu1PortName = NULL;
    INT4                i4NrawFd;
    char                dev[] = "/dev/io-net/";
    char                buf[20];

    pu1PortName = CfaGddGetLnxIntfnameForPort (u4IfIndex);

    if (pu1PortName == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddConfigPort - "
                  "Not able to get Interface name for %d - FAILURE.\n",
                  u4IfIndex);
        return CFA_FAILURE;
    }

    sprintf (ifr.ifr_name, "%s", pu1PortName);
    sprintf ((char *) &buf, "%s%s", dev, pu1PortName);

    /* Open the io - net interface */
    if (0 > (i4NrawFd = open ((char *) &buf, O_WRONLY)))
    {
        perror ("open NRAW FAILED");
        return CFA_FAILURE;
    }

    if (ioctl (i4NrawFd, BIOCSETIF, &ifr) > 0)
    {
        perror ("Bound Failure e ");
        return CFA_FAILURE;
    }

    /* write out our packet */
    if (u4PktSize != write (i4NrawFd, (VOID *) pu1DataBuf, u4PktSize))
    {
        perror (" Write Packets Failed ");
        return CFA_FAILURE;
    }
    close (i4NrawFd);

    return CFA_SUCCESS;
}

/*****************************************************************************
 *    Function Name             : CfaGddEthWriteWithPri
 *    Description               : Writes the data to the ethernet driver.
 *    Input(s)                  : pu1DataBuf - Pointer to the linear buffer.
 *                                u4IfIndex  - MIB-2 interface index
 *                                u4PktSize  - Size of the buffer.
 *                                u1Priority - Traffic class on which Pkt to
 *					        be TXed
 *    Output(s)                 : None.
 *    Global Variables Referred : None
 *    Global Variables Modified : None.
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : CFA_SUCCESS if write succeeds,
 *                                otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGddEthWriteWithPri (UINT1 *pData, UINT4 u4IfIndex, UINT4 u4PktLen, 
		       UINT1 u1Priority)
{
    if(CfaGddEthWrite(pData,u4IfIndex,u4PktLen) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }
    UNUSED_PARAM (u1Priority);
    return CFA_SUCCESS;
}


/*****************************************************************************
 *
 *    Function Name        : CfaGddEthRead
 *
 *    Description        : This function reads the data from the ethernet port.
 *
 *    Input(s)            : pu1DataBuf - Pointer to the linear buffer.
 *                          u4IfIndex - IfIndex of the interface
 *                          pu4PktSize - Pointer to the packet size
 *
 *    Output(s)            : pu1DataBuf, pu1PktSize
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    INT2                i2IpTotalLen = 0;
    UINT4               u4ExtraBytes = 0;
    UINT1              *pu1PortName = NULL;
    INT4                i4ReadBytes;
    UINT2               u2LenOrType;
    UINT2               u2Protocol = 0xFF;
    UINT1               u1EnetHeaderSize;
    struct ifreq        ifr;
    tEnetV2Header      *pEthHdr;
    tEnetSnapHeader    *pSnapEthHdr;
    struct bpf_hdr     *hdr;
    char               *Pkt = NULL;

    if ((Pkt = MEM_MALLOC (CFA_MAX_DRIVER_MTU, char *)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_GDD,
                  "Exiting CfaGddEthRead - malloc fail for Pkt %d\n",
                  u4IfIndex);
        return (CFA_FAILURE);

    }

    pu1PortName = CfaGddGetLnxIntfnameForPort (u4IfIndex);

    if (pu1PortName == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddConfigPort - "
                  "Not able to get Interface name for %d - FAILURE.\n",
                  u4IfIndex);
        return CFA_FAILURE;
    }

    sprintf (ifr.ifr_name, "%s", pu1PortName);

    /* Get data from interface */
    if ((i4ReadBytes =
         read (CFA_GDD_PORT_DESC (u4IfIndex), Pkt, CFA_MAX_DRIVER_MTU)) < 0)
    {
        perror (" Failure Read Packets ");
        return CFA_FAILURE;

    }

    hdr = (struct bpf_hdr *) Pkt;

    memcpy (pu1DataBuf, Pkt + hdr->bh_hdrlen, hdr->bh_caplen);

    CFA_IF_SET_IN_OCTETS (u4IfIndex, (UINT4) i4ReadBytes);

    pEthHdr = (tEnetV2Header *) pu1DataBuf;
    u2LenOrType = OSIX_NTOHS (pEthHdr->u2LenOrType);

    if (CFA_ENET_IS_TYPE (u2LenOrType))
    {
        u2Protocol = u2LenOrType;
        u1EnetHeaderSize = CFA_ENET_V2_HEADER_SIZE;
    }
    else
    {
        pSnapEthHdr = (tEnetSnapHeader *) pu1DataBuf;
        u1EnetHeaderSize = CFA_ENET_SNAP_HEADER_SIZE;

        /* check for LLC control frame first - we expect only LLC in SNAP now */
        if (pSnapEthHdr->u1Control == CFA_LLC_CONTROL_UI)
        {
            /*check for presence of SNAP which may carry IP/ARP/RARP after LLC */
            if ((pSnapEthHdr->u1DstLSap == CFA_LLC_SNAP_SAP) ||
                (pSnapEthHdr->u1SrcLSap == CFA_LLC_SNAP_SAP) ||
                (pSnapEthHdr->u1Oui1 == 0x00) ||
                (pSnapEthHdr->u1Oui2 == 0x00) || (pSnapEthHdr->u1Oui3 == 0x00))
            {
                /* determine which protocol is being carried */
                u2Protocol = OSIX_NTOHS (pSnapEthHdr->u2ProtocolType);
            }                    /* end of SNAP framing */

        }                        /* end of LLC framing */

    }

    if (u2Protocol == CFA_ENET_IPV4)
    {
        MEMCPY (&i2IpTotalLen, (pu1DataBuf + u1EnetHeaderSize
                                + IP_PKT_OFF_LEN), sizeof (INT2));
        i2IpTotalLen = OSIX_NTOHS (i2IpTotalLen);

        if ((i2IpTotalLen + u1EnetHeaderSize) > i4ReadBytes)
        {
            return CFA_FAILURE;
        }

        u4ExtraBytes = (UINT4) i4ReadBytes - (i2IpTotalLen + u1EnetHeaderSize);
    }
#ifdef IP6_WANTED
    else if (u2Protocol == CFA_ENET_IPV6)
    {
        MEMCPY (&i2IpTotalLen, (pu1DataBuf + u1EnetHeaderSize +
                                IPV6_OFF_PAYLOAD_LEN), sizeof (INT2));
        i2IpTotalLen = OSIX_NTOHS (i2IpTotalLen);

        if ((i2IpTotalLen + u1EnetHeaderSize + IPV6_HEADER_LEN) > i4ReadBytes)
        {
            return CFA_FAILURE;
        }

        u4ExtraBytes =
            (UINT4) i4ReadBytes - (i2IpTotalLen + u1EnetHeaderSize +
                                   IPV6_HEADER_LEN);
    }
#endif /*IP6_WANTED */

    MEM_FREE (Pkt);

    (*pu4PktSize) = (UINT4) i4ReadBytes - u4ExtraBytes;

    return CFA_SUCCESS;
}

UINT1
CfaGddGetLinkStatus (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_IF_UP;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddGetLnxIntfnameForPort
 *
 *    Description          : This function Returns LinuxEth Name Based on Slot 
 *                           Referred. 
 *    Input(s)            :  u4IfIndex - Interface index
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure 
 *                
 *    Global Variables Modified : gaIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : EthName if Slot is found Else NULL
 *
 *****************************************************************************/
UINT1              *
CfaGddGetLnxIntfnameForPort (UINT2 u2Index)
{
    INT4                i4Index;
    UINT1               au1Temp[CFA_MAX_PORT_NAME_LENGTH];

    for (i4Index = 0; i4Index < SYS_DEF_MAX_PHYSICAL_INTERFACES; i4Index++)
    {
        MEMSET (au1Temp, 0, CFA_MAX_PORT_NAME_LENGTH);

        STRCPY (au1Temp, au1IntMapTable[i4Index]);

        if (MEMCMP (CFA_GDD_PORT_NAME (u2Index), au1Temp,
                    STRLEN (CFA_GDD_PORT_NAME (u2Index))) == 0)
        {
            return au1IntMapTable[i4Index][1];
        }
    }
    return NULL;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddSetLnxIntfnameForPort
 *
 *    Description          : This function sets Interface Name for the interface
 *                           index.
 *    Input(s)            :  u4IfIndex - Interface index
 *                           pu1IfName - Interface name
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure
 *
 *    Global Variables Modified : gaIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : NULL
 *
 *****************************************************************************/
VOID        
CfaGddSetLnxIntfnameForPort (UINT4 u4Index, UINT1 * pu1IfName)
{

    if (u4Index == 0)
    {
        return;
    }

    STRNCPY (au1IntMapTable[u4Index], CFA_GDD_PORT_NAME (u4Index),
            CFA_MAX_PORT_NAME_LENGTH); 

    STRNCPY (au1IntMapTable[u4Index][1], pu1IfName, CFA_MAX_PORT_NAME_LENGTH);
    CfaSetIfName (u4Index, pu1IfName);
    return;
}
#ifdef MBSM_WANTED
/*****************************************************************************
 *    Function Name       : CfaMbsmGddInit
 *    Description         : This function performs the initialisation of
 *                          the  Device Driver Module of CFA. This routine
 *                          1. creates the Queue for reception of packets from
 *                             driver
 *                          2. creates the memory pool for the driver messages
 *                          3. initializes the driver to cfa mapping tables
 *    Input(s)            : None.
 *    Output(s)           : None.
 *
 *    Global Variables Referred : _devices, _ndevices
 *    Global Variables Modified : gFsDrvMemPoolId, gaCfaNpIndexMap
 *                                gaCfaNpDevMap
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if initialisation succeeds,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

PUBLIC INT4
CfaMbsmGddInit (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *    Function Name        : CfaMbsmGddDeInit
 *    Description         : This function performs the shutdown of
 *                the Generic Device Driver Module of CFA. This
 *                shutdown routine should be called
 *                before the protocols are informed about the shutdown.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : FdTable
 *
 *    Global Variables Modified : FdTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
CfaMbsmGddDeInit (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return CFA_SUCCESS;
}
#endif /* MBSM_WANTED */

/*****************************************************************************
 *
 *    Function Name       :CfaGddTxPktOnVlanMemberPorts 
 *
 *    Description         :This routine transmits the given Frame on the member
 *                         ports of the given Vlan.
 *
 *    Input(s)            : u4L2ContextId - L2 Context Id to which VlanId is 
 *                                          associated
 *                        : pu1DataBuf  - Pointer to the frame buffer 
 *                          u4PktSize   - Size of the frame 
 *                          VlanId      - Vlan on which the frame is to be 
 *                                        transmitted
 *                          bBcast      - Whether this frame is to be 
 *                                        broadcasted  
 *
 *    Output(s)           : None
 *
 *    Global Variables Referred : None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/ CFA_FAILURE.
 *****************************************************************************/

INT4
CfaGddTxPktOnVlanMemberPortsInCxt (UINT4 u4L2ContextId,
                                   UINT1 *pu1DataBuf, tVlanId VlanId,
                                   BOOL1 bBcast, UINT4 u4PktSize)
{
    tPortList          *pTagPorts = NULL;
    tPortList          *pUnTagPorts = NULL;
    tMacAddr            DestAddr;
    tCfaVlanInfo        VlanInfo;
    UINT4               u4BridgeMode;
#ifdef NPAPI_WANTED
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2EtherType = 0;
    UINT1               u1PortFlag;
#endif
    UINT4               u4OutPort;
    BOOL1               bIsTag;
    INT4                i4RetVal;
    UINT1              *pu1TempBuf = NULL;
    UINT1               TempDataBuf[CFA_ENET_MIN_UNTAGGED_FRAME_SIZE];
    UINT4               u4MinFrameMtu = 0;
    INT4                i4RetStat = CFA_SUCCESS;
    UINT1               u1PaddingDone = CFA_FALSE;
    MEMCPY (DestAddr, pu1DataBuf, CFA_ENET_ADDR_LEN);

    MEMSET (TempDataBuf, 0, sizeof (TempDataBuf));
    VlanInfo.u2VlanId = VlanId;
#ifdef NPAPI_WANTED
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
#ifdef MBSM_WANTED
        /* If the packet belongs to STACK IVR, then send the
         * packet via stack port using ATP*/
        if (VlanId == CFA_DEFAULT_STACK_VLAN_ID)
        {
            if (CfaMbsmNpTxOnStackInterface (pu1DataBuf, u4PktSize) ==
                FNP_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaUtilTxPktOnVlanMemberPorts - "
                         "Unable to tx on stack interface.\n");
                return CFA_FAILURE;
            }
            return CFA_SUCCESS;
        }
#endif
    }
#endif

    pTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        return CFA_FAILURE;
    }

    pUnTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pUnTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        return CFA_FAILURE;
    }

    MEMSET (*pTagPorts, 0, sizeof (tPortList));
    MEMSET (*pUnTagPorts, 0, sizeof (tPortList));
    i4RetVal = VlanIvrGetTxPortOrPortListInCxt (u4L2ContextId,
                                                DestAddr, VlanId, bBcast,
                                                &u4OutPort, &bIsTag, *pTagPorts,
                                                *pUnTagPorts);

    if (i4RetVal == VLAN_FORWARD)
    {
        /*ASSUMPTION: The Packet reached here will be untagged always.
         * Since bcoz this thread is called only for the outgoing packet
         * from IP.*/

        u4MinFrameMtu = CFA_ENET_MIN_UNTAGGED_FRAME_SIZE;

        if (u4PktSize < u4MinFrameMtu)
        {
            pu1TempBuf = pu1DataBuf;
            MEMCPY (TempDataBuf, pu1DataBuf, u4PktSize);
            pu1DataBuf = TempDataBuf;
            u4PktSize = u4MinFrameMtu;
            u1PaddingDone = CFA_TRUE;
        }

        CfaTxFillVlanInfo (DestAddr, u4OutPort,
                           bIsTag, *pTagPorts, *pUnTagPorts, &VlanInfo);
        if (L2IwfGetBridgeMode (L2IWF_DEFAULT_CONTEXT, &u4BridgeMode) ==
            L2IWF_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaGddWrite - "
                     "L2IWF-GetBridgeMode -  FAILURE.\n");
            i4RetStat = CFA_FAILURE;
        }
        else
        {
#ifdef NPAPI_WANTED
            if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
            {
                if ((u4BridgeMode == L2IWF_CUSTOMER_BRIDGE_MODE) ||
                    (u4BridgeMode == L2IWF_PROVIDER_BRIDGE_MODE))
                {
                    if (CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo)
                        != FNP_SUCCESS)
                    {
                        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                 "Error in CfaGddWrite - "
                                 "Unsuccessful Driver Write -  FAILURE.\n");
                        i4RetStat = CFA_FAILURE;
                    }
                }
                else if ((u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE) ||
                         (u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE))
                {
                    if (u4OutPort != VLAN_INVALID_PORT)
                    {
                        if (CfaHwL3VlanIntfWrite
                            (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                        {
                            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                     "Error in CfaGddWrite - "
                                     "Unsuccessful Driver Write -  FAILURE.\n");
                            i4RetStat = CFA_FAILURE;
                        }
                    }
                    else
                    {
                        for (u2ByteInd = 0; u2ByteInd < BRG_PORT_LIST_SIZE;
                             u2ByteInd++)
                        {
                            if ((*pTagPorts)[u2ByteInd] == 0)
                            {
                                continue;
                            }

                            u1PortFlag = (*pTagPorts)[u2ByteInd];

                            for (u2BitIndex = 0;
                                 ((u2BitIndex < BITS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
                            {
                                if ((u1PortFlag & 0x80) != 0)
                                {
                                    VlanInfo.u2PktType = CFA_NP_KNOWN_UCAST_PKT;

                                    VlanInfo.unPortInfo.TxUcastPort.u2TxPort =
                                        (UINT2) ((u2ByteInd * BITS_PER_BYTE) +
                                                 u2BitIndex + 1);

                                    u4OutPort =
                                        VlanInfo.unPortInfo.TxUcastPort.
                                        u2TxPort;

#ifdef VLAN_WANTED
                                    VlanGetPortEtherType (u4OutPort,
                                                          &u2EtherType);
#endif
                                    VlanInfo.unPortInfo.TxUcastPort.
                                        u2EtherType = u2EtherType;

                                    VlanInfo.unPortInfo.TxUcastPort.u1Tag =
                                        CFA_NP_TAGGED;

                                    CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize,
                                                          VlanInfo);
                                }
                                u1PortFlag = (UINT1) (u1PortFlag << 1);
                            }
                        }
                        MEMSET (VlanInfo.unPortInfo.TxPorts.pTagPorts, 0,
                                sizeof (tPortList));
                        /* 
                         * In 802.1ad Bridges, for untagged ports, the tag ethertype 
                         * need not be filled in based on the port ethertype. 
                         * Hence calling the API for packet transmission on L3 
                         * interfaces directly.
                         */
                        if (CfaHwL3VlanIntfWrite
                            (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                        {
                            i4RetStat = CFA_FAILURE;
                        }
                    }
                }
            }
#else
            if (CfaL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo) !=
                CFA_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddWrite - "
                         "Unsuccessful Driver Write -  FAILURE.\n");
                i4RetStat = CFA_FAILURE;

            }
#endif
        }
        if (u1PaddingDone == CFA_TRUE)
        {
            pu1DataBuf = pu1TempBuf;
        }
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pUnTagPorts));
        return i4RetStat;
    }

    FsUtilReleaseBitList ((UINT1 *) pTagPorts);
    FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pTagPorts));
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pUnTagPorts));
    return CFA_FAILURE;
}
