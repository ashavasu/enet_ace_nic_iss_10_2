/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: cfaiss.c,v 1.55 2015/11/05 13:11:58 siva Exp $
*
* Description: This file contains the portable routines related to
*              interface configuration.
*******************************************************************/

#include "cfainc.h"
#ifndef MBSM_WANTED                /* Pizza box */
/* Global Variable */
UINT1               agCfaNpIfName[SYS_DEF_MAX_PHYSICAL_INTERFACES +
                                  1][CFA_MAX_PORT_NAME_LENGTH + 1];
UINT1              *gpu1InvalidPortList = NULL;

/*****************************************************************************
 *
 *    Function Name        : CfaNpFillInterfaceName
 *
 *    Description          : This function fills the alias names for the 
 *                           the interface.
 *
 *    Input(s)             : None
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None,
 *
 *    Global Variables Modified : gIssSysGroupInfo,
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : None.
 *
 *****************************************************************************/
VOID
CfaNpFillInterfaceName (VOID)
{
    UINT4               u4SlotNo;
    UINT4               u4PortNo;

    UINT4               u4IfIndex = 1;

    for (u4PortNo = 1, u4SlotNo = 0; u4PortNo <= ISS_MAX_PORTS; u4PortNo++)
    {
        SNPRINTF ((CHR1 *) agCfaNpIfName[u4IfIndex++],
                  CFA_MAX_PORT_NAME_LENGTH, "%s%u/%u",
                  ISS_ALIAS_PREFIX, u4SlotNo, u4PortNo);
    }

    for (u4IfIndex = 1; u4IfIndex <= ISS_MAX_PORTS; u4IfIndex++)
    {

        if (!STRCMP (gIssSysGroupInfo.au1IssDefaultInterface,
                     (UINT1 *) agCfaNpIfName[u4IfIndex]))
        {
            /* Swap the position of the current Index with index one */
            STRCPY (agCfaNpIfName[1], gIssSysGroupInfo.au1IssDefaultInterface);
            SNPRINTF ((CHR1 *) agCfaNpIfName[u4IfIndex],
                      CFA_MAX_PORT_NAME_LENGTH, "%s%d/%d",
                      ISS_ALIAS_PREFIX, 0, 1);
        }
    }
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmBringupAllInterfaces 
 *
 *    Description          : This function Brings up all the interfaces  
 *                           except the default interface. The Admin status
 *                           of the interfaces created is not made UP. 
 *
 *    Input(s)             : None
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None,
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : CFA_SUCCESS/CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaIfmBringupAllInterfaces (INT1 *pi1Dummy)
{
    UINT4               u4Index;
    UINT4               u4Count = 0;
    UINT1               u1BridgedIfaceStatus;
    INT1                ai1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    BOOL1               bResult = OSIX_FALSE;
    tCfaIfInfo          IfInfo;

    UNUSED_PARAM (pi1Dummy);
    MEMSET (ai1IfName, 0, sizeof (ai1IfName));

    if (VcmGetSystemModeExt (CFA_PROTOCOL_ID) == VCM_MI_MODE)
    {
        /* In case of MI, by default the ports will not get created. 
         * So to acheive this the check is required. */
        lrInitComplete (OSIX_SUCCESS);
        return (CFA_SUCCESS);
    }

    gpu1InvalidPortList = FsUtilAllocBitList (sizeof (tPortList));
    if (gpu1InvalidPortList == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\r\n");
        return CFA_FAILURE;
    }

    MEMSET (gpu1InvalidPortList, 0, sizeof (tPortList));

    CFA_LOCK ();

#ifdef MBSM_WANTED
    for (u4Index = 1; u4Index <= ISS_MAX_PORTS; u4Index++)
#else
    for (u4Index = 1; u4Index <= gIssSysGroupInfo.u4FrontPanelPortCount;
         u4Index++)
#endif
    {
        /* If "Management port" is available, create interfaces 'including'
         * default interface , because CfaIfmDefaultInterfaceInit
         * would not have been called to create the default interface index.
         * 
         * "Otherwise" : Start Creating interfaces 'excluding' default interface
         * index , because CfaIfmDefaultInterfaceInit() has created the
         * default interface.
         */
        if ((CFA_MGMT_PORT == TRUE) || (u4Index != CFA_DEFAULT_ROUTER_IFINDEX))
        {
            if (CfaIfmCreateAndInitIfEntry (u4Index, CFA_ENET,
                                            agCfaNpIfName[u4Index]) !=
                CFA_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                         "Error in CfaIfmBringupAllInterfaces- "
                         "interface creation - FAILURE.\n");
                continue;
            }

            /* make Rowstatus active */
            CFA_IF_RS (u4Index) = CFA_RS_ACTIVE;
            CfaSetIfActiveStatus (u4Index, CFA_TRUE);
            CfaSetCdbRowStatus (u4Index, CFA_RS_ACTIVE);

            /* Management Port Status indication should not go to the
             * higher L2 Modules
             * */
            if (CfaIsMgmtPort (u4Index) == FALSE)
            {
                /* Creating the interface mapping in VCM */
                VcmCfaCreateIfaceMapping (L2IWF_DEFAULT_CONTEXT, u4Index);
            }
        }

        /* 
         * When IVR is enabled, the interface create indication will be 
         * given to L2 only when it is a bridged interface.
         * Otherwise, all the interfaces will be registered with L2 and IP.
         */

        CfaGetIfInfo (u4Index, &IfInfo);
        if (gu4IsIvrEnabled == CFA_ENABLED)
        {
            CfaGetIfBridgedIfaceStatus (u4Index, &u1BridgedIfaceStatus);

            if (u1BridgedIfaceStatus == CFA_ENABLED)
            {
                /* Management Port Status indication should not go to the
                 * higher L2 Modules
                 * */
                if (CfaIsMgmtPort (u4Index) == FALSE)
                {
                    L2IwfPortCreateIndication (u4Index);
                    EoamApiNotifyIfCreate (u4Index);
                }
#ifdef ISS_WANTED
                if (IfInfo.u1IfType == CFA_ENET)
                {
                    IssCreatePort ((UINT2) u4Index, ISS_ALL_TABLES);
                }
#ifdef QOSX_WANTED
                if (!((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
                      (IfInfo.u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
                {
                    QosCreatePort (u4Index);
                }
#endif
#endif
            }
        }
        else
        {
            /* Management Port Status indication should not go to the
             * higher L2 Modules
             * */
            if (CfaIsMgmtPort (u4Index) == FALSE)
            {
                L2IwfPortCreateIndication (u4Index);
                EoamApiNotifyIfCreate (u4Index);
            }
#ifdef ISS_WANTED
            if (IfInfo.u1IfType == CFA_ENET)
            {
                IssCreatePort ((UINT2) u4Index, ISS_ALL_TABLES);
            }
#ifdef QOSX_WANTED
            if (!((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
                  (IfInfo.u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
            {
                QosCreatePort (u4Index);
            }
#endif
#endif
        }

        CFA_DBG (CFA_TRC_ALL_TRACK, CFA_IFM,
                 "Exiting CfaIfmBringupAllInterfaces- "
                 "Default interface init - SUCCESS.\n");
    }
    /*Getting the invalid interface list count inorder to remove holes */
    for (u4Index = 1; u4Index <= gIssSysGroupInfo.u4FrontPanelPortCount;
         u4Index++)
    {
        OSIX_BITLIST_IS_BIT_SET (gpu1InvalidPortList, u4Index,
                                 sizeof (tPortList), bResult);
        if (bResult == OSIX_TRUE)
        {
            u4Count++;
        }
        else
        {
            u4Count = 0;
        }
    }

    /*Diplaying the invalid interface information */
    for (u4Index = 1;
         u4Index <= (gIssSysGroupInfo.u4FrontPanelPortCount - u4Count);
         u4Index++)
    {
        OSIX_BITLIST_IS_BIT_SET (gpu1InvalidPortList, u4Index,
                                 sizeof (tPortList), bResult);
        if (bResult == OSIX_TRUE)
        {
            CfaGetPhysIfName (u4Index, ai1IfName);
            if (STRLEN (ai1IfName) != 0)
            {
                UtlTrcLog (1, 1, "CFA", "Interface %s not present\n",
                           ai1IfName);
            }
        }
    }
    FsUtilReleaseBitList (gpu1InvalidPortList);
    gpu1InvalidPortList = NULL;
    CFA_UNLOCK ();

    lrInitComplete (OSIX_SUCCESS);

    return (CFA_SUCCESS);
}

#else /* Multiboard */
/*******************************************************************************
 *
 *    Function Name        : CfaIfmBringupAllStackInterfaces 
 *
 *    Description          : This function Brings up all the stack interfaces.  
 *                           
 *    Input(s)             : None
 *
 *    Output(s)            : None.
 *
 *    Returns              : CFA_SUCCESS/CFA_FAILURE.
 *
 ******************************************************************************/
INT4
CfaIfmBringupAllStackInterfaces (VOID)
{
    UINT4               u4NameLen = 0;
    UINT2               u2Index;
    UINT1               u1PortNum = 1;
    UINT1               au1AliasName[CFA_MAX_PORT_NAME_LENGTH];

    CFA_LOCK ();

    sprintf ((char *) au1AliasName, "%s", CFA_MBSM_STK_ALIAS_PREFIX);
    u4NameLen = STRLEN (au1AliasName);

    for (u2Index = CFA_STACK_PORT_START_INDEX;
         u2Index <= MBSM_MAX_PORTS_PER_SLOT + ISS_MAX_STK_PORTS; u2Index++)
    {
        sprintf ((char *) &(au1AliasName[u4NameLen]), "-%u", u1PortNum);

        if (CfaIfmCreateAndInitIfEntry (u2Index, CFA_ENET,
                                        au1AliasName) != CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                     "Error in CfaIfmBringupAllStackInterfaces- "
                     "interface creation - FAILURE.\n");
            CFA_UNLOCK ();
            return (CFA_FAILURE);
        }
        /* make Rowstatus active */
        CFA_IF_RS (u2Index) = CFA_RS_ACTIVE;
        CfaSetIfActiveStatus (u2Index, CFA_TRUE);
        /* Bridged status should be disabled for stack interface */
        CfaSetIfBridgedIfaceStatus (u2Index, CFA_DISABLED);
        u1PortNum++;
    }

    CFA_UNLOCK ();
    return (CFA_SUCCESS);
}

#ifdef MBSM_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaMbsmIfmBringUpConnectingInterface 
 *
 *    Description          : This function creates the interface specified
 *                           by the interface index.
 *
 *    Input(s)             : u4IfIndex - Interface index to be created.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None,
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : CFA_SUCCESS/CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaMbsmIfmBringUpConnectingInterface (UINT4 u4IfIndex, INT1 *pi1AliasName)
{

    if (u4IfIndex > ISS_MAX_PORTS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaMbsmIfmBringUpConnectingInterface- "
                 "interface creation - FAILURE.\n");
        return (CFA_FAILURE);
    }

    /* Invoked from cfamain thread , so CFA_LOCK has been handled there itself */

    if (CfaIfmCreateAndInitIfEntry
        (u4IfIndex, CFA_OTHER, (UINT1 *) pi1AliasName) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaMbsmIfmBringUpConnectingInterface- "
                 "interface creation - FAILURE.\n");

        return (CFA_FAILURE);
    }

    /* make Rowstatus active */
    CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;

    CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_ACTIVE);
    CFA_IF_ADMIN (u4IfIndex) = CFA_IF_UP;
    CfaSetCdbPortAdminStatus (u4IfIndex, CFA_IF_UP);
    CfaSetIfOperStatus (u4IfIndex, CFA_IF_UP);
    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_IFM,
             "Exiting CfaMbsmIfmBringUpConnectingInterface- "
             "Default interface init - SUCCESS.\n");

    return (CFA_SUCCESS);

}
#endif
/*****************************************************************************
 *
 *    Function Name        : CfaMbsmIfmBringUpInterface 
 *
 *    Description          : This function creates the interface specified
 *                           by the interface index.
 *
 *    Input(s)             : u4IfIndex - Interface index to be created.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None,
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : CFA_SUCCESS/CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaMbsmIfmBringUpInterface (UINT4 u4IfIndex, INT1 *pi1AliasName)
{

    tCfaIfInfo          IfInfo;
    UINT1               u1BridgedIfaceStatus;
    UINT1               u1BrgPortType = CFA_CUSTOMER_BRIDGE_PORT;
    tHwPortInfo         HwPortInfo;

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);
    if (u4IfIndex > ISS_MAX_PORTS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaMbsmIfmBringUpInterface- "
                 "interface creation - FAILURE.\n");
        return (CFA_FAILURE);
    }

    /* Invoked from cfamain thread , so CFA_LOCK has been handled there itself */

    if (CfaIfmCreateAndInitIfEntry (u4IfIndex, CFA_ENET, (UINT1 *) pi1AliasName)
        != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaMbsmIfmBringUpInterface- "
                 "interface creation - FAILURE.\n");

        return (CFA_FAILURE);
    }

    /* make Rowstatus active */
    CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;

    CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_ACTIVE);
    CfaGetIfInfo (u4IfIndex, &IfInfo);

    /* In case of pizza box, ports will be created in CfaBringupAllInterfces
     * function. And this is done before VlanInit. 
     * In VlanInit, default bridge port type will be updated for all ports.
     * In case of MBSM, ports are created only after VlanInit (all mod init).
     * So get default port type from vlan and set it. */
    u1BrgPortType = VlanGetDefPortType (u4IfIndex);
    CfaSetIfBrgPortType (u4IfIndex, u1BrgPortType);

    /* 
     * When IVR is enabled, the interface create indication will be 
     * given to L2 only when it is a bridged interface.
     * Otherwise, all the interfaces will be registered with L2 and IP.
     */

    if (VcmGetSystemModeExt (CFA_PROTOCOL_ID) == VCM_SI_MODE)
    {
        VcmCfaCreateIfaceMapping (L2IWF_DEFAULT_CONTEXT, u4IfIndex);
    }
    else
    {
        if (u4IfIndex == HwPortInfo.au4ConnectingPortIfIndex[0])    /* connecting ports in dual node stacking model */
        {
            VcmCfaCreateIfaceMapping (L2IWF_DEFAULT_CONTEXT, u4IfIndex);
        }
    }

    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        CfaGetIfBridgedIfaceStatus ((UINT4) u4IfIndex, &u1BridgedIfaceStatus);

        if (u1BridgedIfaceStatus == CFA_ENABLED)
        {
            L2IwfPortCreateIndication (u4IfIndex);
            EoamApiNotifyIfCreate (u4IfIndex);
#ifdef ISS_WANTED
            if (IfInfo.u1IfType == CFA_ENET)
            {
                if (IssCreatePort (u4IfIndex, ISS_ALL_TABLES) == ISS_FAILURE)
                {
                    return CFA_FAILURE;
                }
            }
#ifdef QOSX_WANTED
            if (!((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
                  (IfInfo.u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
            {
                QosCreatePort (u4IfIndex);
            }
#endif
#endif
        }
    }
    else
    {
        L2IwfPortCreateIndication (u4IfIndex);
        EoamApiNotifyIfCreate (u4IfIndex);
#ifdef ISS_WANTED
        if (IfInfo.u1IfType == CFA_ENET)
        {
            if (IssCreatePort (u4IfIndex, ISS_ALL_TABLES) == ISS_FAILURE)
            {
                return CFA_FAILURE;
            }
        }
#ifdef QOSX_WANTED
        if (!((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
              (IfInfo.u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
        {
            QosCreatePort (u4IfIndex);
        }
#endif
#endif
    }

    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_IFM,
             "Exiting CfaMbsmIfmBringUpInterface- "
             "Default interface init - SUCCESS.\n");

    return (CFA_SUCCESS);
}
#endif
/*****************************************************************************
 *
 *    Function Name             : CfaIssFillProtoInfoAndNotifyPkt
 *
 *    Description               : This function obtains the necessary info from
 *                                the packet and the received interface and 
 *                                invokes the call back provided by the PTP 
 *                                interface.
 *
 *    Input(s)                  : pCfaInfo - Pointer to CfaInfo              
 *                                u2LenOrType - ENET Type of PTP.            
 *
 *    Output(s)                 : None.
 *
 *    Global Variables Referred : None,
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS/CFA_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
CfaIssFillProtoInfoAndNotifyPkt (tCfaRegInfo * pCfaInfo, UINT2 u2LenOrType)
{
    UINT4               u4Offset = 0;
    UINT4               u4Pvid = 0;
    INT4                i4ModId = 0;
    INT4                i4RetValue = 0;
    UINT2               u2AggId = 0;
    UINT1               u1IfType = 0;
    BOOL1               b1FlgVfi = OSIX_FALSE;

    UNUSED_PARAM (i4RetValue);

    if (L2IwfGetPortChannelForPort (pCfaInfo->u4IfIndex, &u2AggId) ==
        L2IWF_SUCCESS)
    {
        pCfaInfo->u4IfIndex = (UINT4) u2AggId;
    }

    if (u2LenOrType == CFA_ENET_PTP)
    {
        i4ModId = PTP_MODULE;
    }
    else
    {
        /* Invalid module identifier */
        return CFA_FAILURE;
    }

    i4RetValue = VlanGetTagLenInFrame (pCfaInfo->CfaPktInfo.pBuf,
                                       pCfaInfo->u4IfIndex, &u4Offset);

    /*Check is done for VFI */
    CfaGetIfaceType (pCfaInfo->u4IfIndex, &u1IfType);
    if (u1IfType == CFA_PSEUDO_WIRE)
    {
        VlanApiGetPvid (pCfaInfo->u4IfIndex, &u4Pvid);
        if ((u4Pvid > VLAN_VFI_MIN_ID) && (u4Pvid <= VLAN_VFI_MAX_ID))
        {
            b1FlgVfi = OSIX_TRUE;
        }
    }

    if ((u4Offset != CFA_VLAN_TAG_OFFSET) || (b1FlgVfi == OSIX_TRUE))
    {
        if ((VlanIdentifyVlanIdAndUntagFrame (i4ModId,
                                              pCfaInfo->CfaPktInfo.pBuf,
                                              pCfaInfo->u4IfIndex,
                                              &(pCfaInfo->CfaPktInfo.VlanId)))
            == VLAN_NO_FORWARD)
        {
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                      "VlanIdentifyVlanIdAndUntagFrame failed for IfIndex %d"
                      "\r\n", pCfaInfo->u4IfIndex);
            return (CFA_FAILURE);
        }
    }
    else if (L2IwfGetInstPortState (0, pCfaInfo->u4IfIndex)
             == AST_PORT_STATE_DISCARDING)
    {
        return CFA_FAILURE;
    }

    if (CfaNotifyIfPktRcvd (u2LenOrType, pCfaInfo) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                  "Unable to provide Packet to PTP module.. %d\r\n",
                  pCfaInfo->u4IfIndex);
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmMapDefaultInterfaceForDefaultContext
 *
 *    Description          : This function Bring up the default interface (0/1)
 *                          i and map it to default context. The Admin status
 *                          of the interface created is not made UP. Currently
 *                          it is used while setting the base bridge mode of
 *                          the switch to dot1q mode.
 *
 *    Input(s)             : None
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None,
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : CFA_SUCCESS/CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmMapDefaultInterfaceForDefaultContext (VOID)
{
    UINT4               u4Index = CFA_ROUTER_IFINDEX;
    UINT1               u1BridgedIfaceStatus;
    tCfaIfInfo          IfInfo;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    STRNCPY (au1IfName, IssGetInterfaceFromNvRam (),
             MEM_MAX_BYTES(STRLEN (IssGetInterfaceFromNvRam ()),CFA_MAX_PORT_NAME_LENGTH));
    au1IfName[STRLEN (IssGetInterfaceFromNvRam ())] = '\0';
    if (CfaIfmCreateAndInitIfEntry (u4Index, CFA_ENET,
                                    au1IfName) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmMapDefaultInterfaceForDefaultContext - "
                 "interface creation - FAILURE.\n");

        return (CFA_FAILURE);
    }

    /* make Rowstatus active */
    CFA_IF_RS (u4Index) = CFA_RS_ACTIVE;
    CfaSetIfActiveStatus (u4Index, CFA_TRUE);
    CfaSetCdbRowStatus (u4Index, CFA_RS_ACTIVE);

    /* Management Port Status indication should not go to the
     * higher L2 Modules
     * */
    if (CfaIsMgmtPort (u4Index) == FALSE)
    {
        /* Creating the interface mapping in VCM */
        VcmCfaCreateIfaceMapping (L2IWF_DEFAULT_CONTEXT, u4Index);
    }

    /*
     * When IVR is enabled, the interface create indication will be
     * given to L2 only when it is a bridged interface.
     * Otherwise, all the interfaces will be registered with L2 and IP.
     */

    CfaGetIfInfo (u4Index, &IfInfo);
    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        CfaGetIfBridgedIfaceStatus (u4Index, &u1BridgedIfaceStatus);

        if (u1BridgedIfaceStatus == CFA_ENABLED)
        {
            /* Management Port Status indication should not go to the
             * higher L2 Modules
             * */
            if (CfaIsMgmtPort (u4Index) == FALSE)
            {
                L2IwfPortCreateIndication (u4Index);
                EoamApiNotifyIfCreate (u4Index);
            }

#ifdef ISS_WANTED
            if (IfInfo.u1IfType == CFA_ENET)
            {
                if (IssCreatePort ((UINT2) u4Index, ISS_ALL_TABLES) ==
                    ISS_FAILURE)
                {
                    return CFA_FAILURE;
                }
            }
#ifdef QOSX_WANTED
            if (!((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
                  (IfInfo.u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
            {
                QosCreatePort (u4Index);
            }
#endif
#endif
        }
    }
    else
    {
        /* Management Port Status indication should not go to the
         * higher L2 Modules
         * */
        if (CfaIsMgmtPort (u4Index) == FALSE)
        {
            L2IwfPortCreateIndication (u4Index);
            EoamApiNotifyIfCreate (u4Index);
        }
#ifdef ISS_WANTED
        if (IfInfo.u1IfType == CFA_ENET)
        {
            if (IssCreatePort ((UINT2) u4Index, ISS_ALL_TABLES) == ISS_FAILURE)
            {
                return CFA_FAILURE;
            }
        }
#ifdef QOSX_WANTED
        if (!((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
              (IfInfo.u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
        {
            QosCreatePort (u4Index);
        }
#endif
#endif
    }
    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_IFM,
             "Exiting CfaIfmMapDefaultInterfaceForDefaultContext - "
             "Default interface init - SUCCESS.\n");

    return (CFA_SUCCESS);
}

#ifdef WTP_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaIfmBringupAllRadioInterfaces 
 *
 *    Description          : This function Brings up all the radio interfaces.  
 *                           The Admin status of the interfaces created is not 
 *                           made UP. 
 *
 *    Input(s)             : None
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None,
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : CFA_SUCCESS/CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaIfmBringupAllRadioInterfaces (INT1 *pi1Dummy)
{
    UINT4               u4Index = 0;
    INT1                ai1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               u1RadioId = 0;
    tRadioIfMsgStruct   RadioIfMsgStruct;

    UNUSED_PARAM (pi1Dummy);

    MEMSET (ai1IfName, 0, sizeof (ai1IfName));
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));

    CFA_LOCK ();

    /* Initialize all the radio interface available in the system */
    for (u4Index = SYS_DEF_MAX_ENET_INTERFACES + 1;
         u4Index <= SYS_DEF_MAX_PHYSICAL_INTERFACES; u4Index++)
    {
        if (CfaIfmCreateAndInitIfEntry (u4Index, CFA_RADIO,
                                        agCfaNpIfName[u4Index]) != CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                     "Error in CfaIfmBringupAllRadioInterfaces- "
                     "interface creation - FAILURE.\n");
            continue;
        }

        /* make Rowstatus active */
        CFA_IF_RS (u4Index) = CFA_RS_ACTIVE;
        CfaSetIfActiveStatus (u4Index, CFA_TRUE);
        CfaSetCdbRowStatus (u4Index, CFA_RS_ACTIVE);

        RadioIfMsgStruct.unRadioIfMsg.RadioIfCreate.u4IfIndex = u4Index;
        RadioIfMsgStruct.unRadioIfMsg.RadioIfCreate.u1RadioId = ++u1RadioId;
        RadioIfMsgStruct.unRadioIfMsg.RadioIfCreate.u1IfType = CFA_RADIO;
        RadioIfMsgStruct.unRadioIfMsg.RadioIfCreate.u1AdminStatus = CFA_IF_UP;

        if (WssIfProcessRadioIfMsg
            (WSS_RADIOIF_CREATE_PHY_RADIO_INTF_MSG,
             &RadioIfMsgStruct) != OSIX_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                     "Error in invoking Radio IF module - FAILURE\n");
        }

    }

    CFA_UNLOCK ();
    return (CFA_SUCCESS);
}
#endif
#ifndef MBSM_WANTED
/****************************************************************************/
/*                                                                          */
/*    Function Name      : CfaGetAliasNameFromIfIndex                       */
/*                                                                          */
/*    Description        : Gets the alias name of a given port based        */
/*                         on the interface index                           */
/*                                                                          */
/*    Input(s)           :  u4IfIndex - Interface index                     */
/*                                                                          */
/*    Output(s)          :  pu1IfAlias - Alias/Slot name                    */
/*                                                                          */
/*    Returns            : OSIX_SUCCESS/OSIX_FAILURE                        */
/****************************************************************************/

INT4
CfaGetAliasNameFromIfIndex (UINT4 u4IfIndex, UINT1 *pu1IfAlias)
{
    SNPRINTF ((CHR1 *) pu1IfAlias,
              CFA_MAX_PORT_NAME_LENGTH, "%s", agCfaNpIfName[u4IfIndex]);
    return OSIX_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : CfaGetIfIndexFromAliasName                       */
/*                                                                          */
/*    Description        : Gets the interface index of a given port based   */
/*                         on the alias name of that interface              */
/*                                                                          */
/*    Input(s)           : pu1IfAlias - Alias/Slot name                     */
/*                                                                          */
/*    Output(s)          : pu4IfIndex - Interface index                     */
/*                                                                          */
/*    Returns            : OSIX_SUCCESS/OSIX_FAILURE                        */
/****************************************************************************/

INT4
CfaGetIfIndexFromAliasName (UINT1 *pu1IfAlias, UINT4 *pu4IfIndex)
{
    UINT4               u4IfIndex;

    for (u4IfIndex = 1; u4IfIndex <= CFA_PHYS_NUM (); u4IfIndex++)
    {
        if (STRCMP (pu1IfAlias, agCfaNpIfName[u4IfIndex]) == 0)
        {
            *pu4IfIndex = u4IfIndex;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

#endif
