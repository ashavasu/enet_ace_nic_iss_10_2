/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfaapi.c,v 1.282 2018/01/08 12:28:32 siva Exp $
 *
 * Description: This file contains the routines exported by CFA
 *              to the othe modules.
 *******************************************************************/
#include "cfainc.h"
#include "fscfacli.h"
#include "ifmibcli.h"
#include "rtm.h"
#include "cfa.h"
#ifdef NPSIM_WANTED
#include "gddnpsim.h"
#endif
#ifdef OPENFLOW_WANTED
#include "ofcl.h"
#endif /* OPENFLOW_WANTED */
#ifdef LNXIP4_WANTED
#ifdef VRF_WANTED
#include "fssocket.h"
#endif
#endif
#ifdef NPAPI_WANTED
extern tOsixTaskId  gCfaTxPktTaskId;
#endif

#ifndef CFA_INTERRUPT_MODE
/* CFA's timer list which has GDD poll timer */
extern tTmrAppTimer GddPollTimer;    /* GDD poll timer */
#endif /* CFA_INTERRUPT_MODE */

#ifdef SNMP_2_WANTED
extern UINT4        Dot3PauseAdminMode[11];
#endif

extern tTimerListId CfaGddTimerListId;
extern UINT1        gau1IvrPortBitMaskMap[CFA_MGMT_VLANS_PER_BYTE];
/* Tunnel If Table */
extern tTMO_SLL     TnlIfSLL;
#define CFA_OPENFLOW_VLAN_ALIAS_PREFIX ((const char *)"ofvlan")
/*****************************************************************************
 *
 *    Function Name        : CfaModuleStart
 *
 *    Description        : This function is an API to start the CFA module
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaModuleStart (VOID)
{
    CFA_LOCK ();
    if (CfaHandleModuleStart () == CFA_FAILURE)
    {
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_UNLOCK ();
#ifndef MBSM_WANTED
    CfaIfmBringupAllInterfaces (NULL);
#endif
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaShutdown
 *
 *    Description        : This function is an API to shutdown the CFA module
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaShutdown (VOID)
{
    CFA_LOCK ();
    if (CfaHandleShutdown () == CFA_FAILURE)
    {
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfBridgedIfaceStatus                           */
/*                                                                           */
/* Description        : This function sets the bridged interface status      */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                      u1Status: status to be set                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfBridgedIfaceStatus (UINT4 u4IfIndex, UINT1 u1Status)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;

    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    CFA_CDB_IF_BRIDGED_IFACE_STATUS (u4IfIndex) = u1Status;
    CFA_DS_UNLOCK ();
    CfaCopyPhyPortPropToLogicalInterface (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaCheckNonPhysicalInterfaces
 *
 *    Description        : This function checks whether non physical
 *                         interfaces are present
 *
 *    Input(s)            : None
 *
 *    Output(s)            : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS - if no non-physical ports are present
 *                         CFA_FAILURE - if any 1 non-physical port is present.
 *
 *****************************************************************************/
INT4
CfaCheckNonPhysicalInterfaces ()
{
    UINT4               u4IfIndex = 1;
    UINT1               u1IfType = 0;

    CFA_CDB_SCAN_WITH_LOCK (u4IfIndex, SYS_DEF_MAX_INTERFACES, CFA_ALL_IFTYPE)
    {
        CfaGetIfType (u4IfIndex, &u1IfType);

        /* Check whether the interface is not a Physical interface */
#if defined (WLC_WANTED) || defined (WTP_WANTED)
        if ((u1IfType != CFA_ENET) && (u1IfType != CFA_RADIO))
#else
        /* Stacking port is also a physical interface of type CFA_OTHER
         * present in the control plane stacking model system.
         * Hence allowing the port of type CFA_OTHER as well.*/
        if ((u1IfType != CFA_ENET) && (u1IfType != CFA_OTHER))
#endif
        {
            if (CFA_IF_ENTRY_USED (u4IfIndex))
            {
                return CFA_FAILURE;
            }

        }
    }

    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetIfMainStorageType                            */
/*                                                                           */
/*     DESCRIPTION      : This function Sets the storage type of the         */
/*                        corresponding row of ifMainTable                   */
/*     INPUT            : u4IfIndex - Index of the interface to be updated   */
/*                        i4StorageType - Storage type set by the application*/
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetIfMainStorageType (UINT4 u4IfIndex, INT4 i4StorageType)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;

    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    pCfaIfInfo->i4IfMainStorageType = i4StorageType;
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name      : CfaCreateDefaultIVRInterface 
 *
 *    Description        : This function creates the Default IVR interface
 *
 *    Input(s)            : None
 *
 *    Output(s)            : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaCreateDefaultIVRInterface ()
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tSNMP_OCTET_STRING_TYPE AliasName;
    UINT4               u4SeqNum;
    UINT1               u1IfType;
    UINT1               u1OperStatus;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1DefaultVlanInterfaceName[CFA_MAX_PORT_NAME_LENGTH];

    /*Create Default IVR interface */
    MEMSET (au1DefaultVlanInterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

#ifdef WGS_WANTED
    u1IfType = CFA_L2VLAN;
    STRNCPY (au1DefaultVlanInterfaceName, CFA_DEFAULT_MGMT_INTERFACE,
             STRLEN (CFA_DEFAULT_MGMT_INTERFACE));
    au1DefaultVlanInterfaceName[STRLEN (CFA_DEFAULT_MGMT_INTERFACE)] = '\0';
#else
    u1IfType = CFA_L3IPVLAN;
    SNPRINTF ((CHR1 *) au1DefaultVlanInterfaceName,
              sizeof (au1DefaultVlanInterfaceName), "%s%u",
              CFA_DEFAULT_VLAN_INTERFACE, CFA_DEFAULT_VLAN_ID);
#endif /* WGS_WANTED */

    if (CfaIfmCreateAndInitIfEntry (CFA_DEFAULT_ROUTER_VLAN_IFINDEX,
                                    u1IfType,
                                    au1DefaultVlanInterfaceName) != CFA_SUCCESS)
    {
        return (CFA_FAILURE);
    }

    CfaSetIfAlias (CFA_DEFAULT_ROUTER_VLAN_IFINDEX,
                   au1DefaultVlanInterfaceName);

    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        if (CfaIfmInitVlanIfEntry (CFA_DEFAULT_ROUTER_VLAN_IFINDEX,
                                   au1DefaultVlanInterfaceName) == CFA_FAILURE)
        {
            return CFA_FAILURE;
        }
    }

    CFA_IF_RS (CFA_DEFAULT_ROUTER_VLAN_IFINDEX) = ACTIVE;

    /* make Rowstatus active */
    CfaSetIfActiveStatus (CFA_DEFAULT_ROUTER_VLAN_IFINDEX, CFA_TRUE);

    /* we register the interface with IP */
    if (CfaIfmConfigNetworkInterface (CFA_NET_IF_NEW,
                                      CFA_DEFAULT_ROUTER_VLAN_IFINDEX, 0,
                                      NULL) != CFA_SUCCESS)
    {
        CFA_IF_RS (CFA_DEFAULT_ROUTER_VLAN_IFINDEX) = CFA_RS_NOTINSERVICE;
        CfaSetIfActiveStatus (CFA_DEFAULT_ROUTER_VLAN_IFINDEX, CFA_FALSE);
        return CFA_FAILURE;
    }
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    CfaGetIfOperStatus (CFA_DEFAULT_ROUTER_VLAN_IFINDEX, &u1OperStatus);

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    CfaGetIfName (CFA_DEFAULT_ROUTER_VLAN_IFINDEX, au1IfName);

    /*Give an indication to IP to update its oper and admin status */
    if (u1OperStatus == CFA_IF_UP)
    {
        if ((CfaUpdtIvrInterface (CFA_DEFAULT_ROUTER_VLAN_IFINDEX,
                                  CFA_IF_CREATE)) == CFA_FAILURE)
        {
            CFA_IF_RS (CFA_DEFAULT_ROUTER_VLAN_IFINDEX) = CFA_RS_NOTINSERVICE;
            CfaSetIfActiveStatus (CFA_DEFAULT_ROUTER_VLAN_IFINDEX, CFA_FALSE);
            return (CFA_FAILURE);
        }

        IpUpdateInterfaceStatus ((UINT2) CFA_IF_IPPORT
                                 (CFA_DEFAULT_ROUTER_VLAN_IFINDEX), au1IfName,
                                 u1OperStatus);
    }
#endif /*IP_WANTED */

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, IfMainRowStatus,
                          u4SeqNum, TRUE, CfaLock, CfaUnlock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      CFA_DEFAULT_ROUTER_VLAN_IFINDEX, CFA_RS_CREATEANDWAIT));

    AliasName.pu1_OctetList = au1DefaultVlanInterfaceName;
    AliasName.i4_Length = CFA_MAX_PORT_NAME_LENGTH;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, IfAlias,
                          u4SeqNum, TRUE, CfaLock, CfaUnlock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s",
                      CFA_DEFAULT_ROUTER_VLAN_IFINDEX, &AliasName));

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, IfMainType,
                          u4SeqNum, TRUE, CfaLock, CfaUnlock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      CFA_DEFAULT_ROUTER_VLAN_IFINDEX, CFA_L3IPVLAN));

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, IfMainRowStatus,
                          u4SeqNum, TRUE, CfaLock, CfaUnlock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      CFA_DEFAULT_ROUTER_VLAN_IFINDEX, CFA_RS_ACTIVE));

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaPostPktFromL2                                     */
/*                                                                           */
/* Description        : This function posts the Layer 2 packets to CFA task. */
/*                                                                           */
/* Input(s)           : pBuf       - Pointer the buffer                      */
/*                      u4IfIndex  - Interface Index                         */
/*                      u4PktSize  - Size of the packet                      */
/*                      u2Protocol - protocol type of the frame              */
/*                      u1EncapType- Encapsulation type                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
CfaPostPktFromL2 (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                  UINT4 u4PktSize, UINT2 u2Protocol, UINT1 u1EncapType)
{
    UNUSED_PARAM (u4PktSize);

    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_L2, "Error in CfaPostPktFromL2 -"
                 "CFA is not initialized %d.\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return (CFA_FAILURE);
    }
    /* check if the IfIndex is valid */
    if (CfaValidateIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (ALL_FAILURE_TRC, CFA_L2,
                  "Error in CfaPostPktFromL2 - invalid interface %d.\n",
                  u4IfIndex);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

        return (CFA_FAILURE);
    }

    CFA_SET_COMMAND (pBuf, L2_TO_CFA);
    CFA_SET_IFINDEX (pBuf, u4IfIndex);
    CFA_SET_PROTOCOL (pBuf, u2Protocol);
    CFA_SET_ENCAP_TYPE (pBuf, u1EncapType);

    /* enqueue the packet to CFA task */
    if (OsixQueSend (CFA_PACKET_QID, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG1 (BUFFER_TRC | ALL_FAILURE_TRC, CFA_L2,
                  "Error In CfaPostPktFromL2 -"
                  "Pkt En-queue to CFA on interface %d FAIL.\n", u4IfIndex);

        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

        return (CFA_FAILURE);
    }

    /* send an explicit event to the task */
    OsixEvtSend (CFA_TASK_ID, CFA_PACKET_ARRIVAL_EVENT);

    CFA_DBG1 (BUFFER_TRC, CFA_L2,
              "Exiting CfaPostPktFromL2 -"
              "Pkt En-queue to CFA on interface %d SUCCESS.\n", u4IfIndex);

    return CFA_SUCCESS;
}

#if defined (WLC_WANTED) || defined (WTP_WANTED)
/*****************************************************************************/
/* Function Name      : CfaPostPktFromWss                                     */
/*                                                                            */
/* Description        : This function posts the 802.3 packets received on the */
/* BSS (WAP interface) to CFA task.                                           */
/*                                                                            */
/* Input(s)           : pBuf       - Pointer the buffer                       */
/*                      u4IfIndex  - Interface Index                          */
/*                      u4PktSize  - Size of the packet                       */
/*                      u2Protocol - protocol type of the frame (Holds the    */
/*                                   VlanID)                                  */
/*                      u1EncapType- Encapsulation type                       */
/*                                                                            */
/* Output(s)          : None                                                  */
/*                                                                            */
/* Global Variables                                                           */
/* Referred           : None                                                  */
/*                                                                            */
/* Global Variables                                                           */
/* Modified           : None                                                  */
/*                                                                            */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                               */
/*                                                                            */
/******************************************************************************/
INT4
CfaPostPktFromWss (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                   UINT4 u4PktSize, UINT2 u2Protocol, UINT1 u1EncapType)
{
    UNUSED_PARAM (u4PktSize);

    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_L2, "Error in CfaPostPktFromWss -"
                 "CFA is not initialised %d.\n");
        return (CFA_FAILURE);
    }
    /* check if the IfIndex is valid */
    if (CfaValidateIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (ALL_FAILURE_TRC, CFA_L2,
                  "Error in CfaPostPktFromWss - invalid interface %d.\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_SET_COMMAND (pBuf, WSS_TO_CFA);
    CFA_SET_IFINDEX (pBuf, u4IfIndex);
    CFA_SET_PROTOCOL (pBuf, u2Protocol);
    CFA_SET_ENCAP_TYPE (pBuf, u1EncapType);

    /* enqueue the packet to CFA task */
    if (OsixQueSend (CFA_PACKET_QID, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG1 (BUFFER_TRC | ALL_FAILURE_TRC, CFA_L2,
                  "Error In CfaPostPktFromL2 -"
                  "Pkt Enqueue to CFA on interface %d FAIL.\n", u4IfIndex);

        /*CRU_BUF_Release_MsgBufChain (pBuf, FALSE); */

        return (CFA_FAILURE);
    }

    /* send an explicit event to the task */
    OsixEvtSend (CFA_TASK_ID, CFA_PACKET_ARRIVAL_EVENT);

    CFA_DBG1 (BUFFER_TRC, CFA_MAIN,
              "Exiting CfaPostPktFromWss -"
              "Pkt Enqueue to CFA on interface %d SUCCESS.\n", u4IfIndex);

    return CFA_SUCCESS;
}
#endif
/*****************************************************************************/
/* Function Name      : CfaPostPktFromL2OnPortList                           */
/*                                                                           */
/* Description        : This function posts the packet from IGS that is to   */
/*                      transmitted on multiple ports to the CFA task        */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pBuf       - Pointer the buffer                      */
/*                      PortList   - Bitlist of ports on which               */
/*                                   frame is to be transmitted.             */
/*                      u4PktSize  - Size of the packet                      */
/*                      u2Protocol - protocol type of the frame              */
/*                      u1EncapType- Encapsulation type                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
CfaPostPktFromL2OnPortList (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *PortList,
                            UINT4 u4PktSize, UINT2 u2Protocol,
                            UINT1 u1EncapType)
{
    tCfaTxPktOnPortListMsg *pTxPktOnPortListMsg;

    UNUSED_PARAM (u4PktSize);

    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaPostPktFromL2OnPortList -"
                 " CFA is not initialized");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return CFA_FAILURE;
    }
    if ((pTxPktOnPortListMsg = (tCfaTxPktOnPortListMsg *)
         MemAllocMemBlk (TxPktOnPortListMsgPoolId)) == NULL)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaPostPktFromL2OnPortList - "
                 "Mempool allocation failed FAIL.\n");

        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

        return CFA_FAILURE;
    }

    pTxPktOnPortListMsg->pFrame = pBuf;
    MEMCPY (pTxPktOnPortListMsg->PortList, PortList, sizeof (tPortList));

    CFA_SET_PROTOCOL (pBuf, u2Protocol);
    CFA_SET_ENCAP_TYPE (pBuf, u1EncapType);

    /* enqueue the packet to CFA task */
    if (OsixQueSend (CFA_PACKET_PLIST_QID, (UINT1 *) &pTxPktOnPortListMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG (BUFFER_TRC | ALL_FAILURE_TRC, CFA_L2,
                 "Error In CfaPostPktFromL2OnPortList -"
                 "Pkt Enqueue to CFA on interface FAIL.\n");

        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

        MemReleaseMemBlock (TxPktOnPortListMsgPoolId,
                            (UINT1 *) pTxPktOnPortListMsg);
        return (CFA_FAILURE);
    }

    /* send an explicit event to the task */
    OsixEvtSend (CFA_TASK_ID, CFA_PACKET_PORTLIST_ARRIVAL_EVENT);

    CFA_DBG (BUFFER_TRC, CFA_L2,
             "Exiting CfaPostPktFromL2OnPortList -"
             "Pkt En-queue to CFA SUCCESS.\n");

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaInterfaceStatusChangeIndication
 *
 *    Description          : This function validates the ifIndex and calls 
 *                           CfaInterfaceStatusChangeIndication to upate the 
 *                           interface status.
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns          : CFA_SUCCESS if initialisation succeeds,
 *                       otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaInterfaceStatusChangeIndication (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tCfaExtTriggerMsg  *pExtTriggerMsg;

    /*  When ISSU is in progress, Links status change will not processed.
     *  At the end of ISSU, hardware audit will be performed to acquire 
     *  correct link status as present in the hardware */
    if (IssuGetMaintModeOperation () == OSIX_TRUE)
    {
        return CFA_SUCCESS;
    }

    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaInterfaceStatusChangeIndication - "
                 "CFA is not initialized");
        return CFA_FAILURE;
    }
    if ((pExtTriggerMsg = (tCfaExtTriggerMsg *)
         MemAllocMemBlk (ExtTriggerMsgPoolId)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaInterfaceStatusChangeIndication - "
                  "Mempool allocation failed %d FAIL.\n", u4IfIndex);
        return CFA_FAILURE;
    }

    pExtTriggerMsg->uExtTrgMsg.LinkStatusMsg.u4IfIndex = u4IfIndex;
    pExtTriggerMsg->uExtTrgMsg.LinkStatusMsg.u1LinkStatus = u1OperStatus;
    pExtTriggerMsg->i4MsgType = CFA_LINK_STATUS_CHANGE_MSG;

    if (OsixQueSend (CFA_EXT_TRIGGER_QID, (UINT1 *) &pExtTriggerMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaInterfaceStatusChangeIndication - "
                  "Message En-queue to CFA on interface %d FAIL.\n", u4IfIndex);

        MemReleaseMemBlock (ExtTriggerMsgPoolId, (UINT1 *) pExtTriggerMsg);
        return (CFA_FAILURE);
    }

    /* send an explicit event to the task */
    OsixEvtSend (CFA_TASK_ID, CFA_EXT_TRIGGER_EVENT);

    CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
              "Exiting CfaHandlePktFromIsIs -"
              "Pkt Enqueue to CFA on interface %d SUCCESS.\n", u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaIvrNotifyMclagOperChangeIndication
 *
 *    Description          : This function sends indication to CFA about the 
 *                           interface status of Mclag-Vlan.
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns          : CFA_SUCCESS if initialisation succeeds,
 *                       otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaIvrNotifyMclagOperChangeIndication (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
#ifdef ICCH_WANTED
    tCfaExtTriggerMsg  *pExtTriggerMsg = NULL;

    if (IssuGetMaintModeOperation () == OSIX_TRUE)
    {
        return CFA_SUCCESS;
    }

    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaIvrNotifyMclagOperChangeIndication - "
                 "CFA is not initialized \r\n");
        return CFA_FAILURE;
    }

    if ((pExtTriggerMsg = (tCfaExtTriggerMsg *)
         MemAllocMemBlk (ExtTriggerMsgPoolId)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaIvrNotifyMclagOperChangeIndication - "
                  "Mempool allocation failed %d FAIL.\r\n", u4IfIndex);
        return CFA_FAILURE;
    }

    pExtTriggerMsg->uExtTrgMsg.MclagOperStatusMsg.u4IfIndex = u4IfIndex;
    pExtTriggerMsg->uExtTrgMsg.MclagOperStatusMsg.u1MclagIvrOperStatus =
        u1OperStatus;
    pExtTriggerMsg->i4MsgType = CFA_MCLAG_STATUS_CHANGE_MSG;

    if (OsixQueSend (CFA_EXT_TRIGGER_QID, (UINT1 *) &pExtTriggerMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaIvrNotifyMclagOperChangeIndication - "
                  "Message En-queue to CFA on interface %d FAIL.\r\n",
                  u4IfIndex);

        MemReleaseMemBlock (ExtTriggerMsgPoolId, (UINT1 *) pExtTriggerMsg);
        return (CFA_FAILURE);
    }

    /* send an explicit event to the task */
    OsixEvtSend (CFA_TASK_ID, CFA_EXT_TRIGGER_EVENT);

    CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
              "Exiting CfaIvrNotifyMclagOperChangeIndication -"
              "Pkt Enqueue to CFA on interface %d SUCCESS.\r\n", u4IfIndex);
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1OperStatus);
#endif

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaUfdIsLastDownlinkInGroup                          */
/*                                                                           */
/* Description        : This function checks whether the given port is the   */
/*                      last downlink in the UFD group                       */
/*                                                                           */
/* Input(s)           : u4IfIndex                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_TRUE/ CFA_FALSE                                  */
/*****************************************************************************/
INT4
CfaUfdIsLastDownlinkInGroup (UINT4 u4IfIndex)
{
    if (CFA_UFD_MODULE_STATUS () == CFA_UFD_ENABLED)
    {
        /*Taking/Releasing Lock inside the CfaUfdCheckLastDownlinkInGroup */
        if (CfaUfdCheckLastDownlinkInGroup (u4IfIndex) != OSIX_FALSE)
        {
            return OSIX_TRUE;
        }
    }
    return OSIX_FALSE;
}

/*****************************************************************************
 *
 *    Function Name        : CfaApiPortChannelUpdateInfo
 *
 *    Description        : This function is for Posting a message from LA to 
 *                         CFA for the following information.
 *                         1. Adding a port to a portChannel.
 *                         2. Port channel oper status indication.
 *
 *    Input(s)            : pCfaLaInfoMsg  - LA information to be notified to
 *                                           CFA.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE,
 *
 *****************************************************************************/
INT4
CfaApiPortChannelUpdateInfo (tCfaLaInfoMsg * pCfaLaInfoMsg)
{
    tCfaExtTriggerMsg  *pExtTriggerMsg = NULL;

    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaApiPortChannelUpdateInfo- "
                 "CFA is not initialized.\n");
        return (CFA_FAILURE);
    }
    if ((pExtTriggerMsg = (tCfaExtTriggerMsg *)
         MemAllocMemBlk (ExtTriggerMsgPoolId)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaApiPortChannelUpdateInfo - "
                  "Mempool allocation failed %d FAIL.\n",
                  pCfaLaInfoMsg->u4IfIndex);
        return (CFA_FAILURE);
    }

    if (pCfaLaInfoMsg->u1MsgType == CFA_LA_ADD_PORT_TO_PORT_CHANNEL_MSG)
    {
        pExtTriggerMsg->i4MsgType = CFA_ADD_PORT_TO_PORT_CHANNEL_MSG;
        pExtTriggerMsg->uExtTrgMsg.IfUpdateMsg.u4IfIndex =
            pCfaLaInfoMsg->u4IfIndex;
    }

    if (pCfaLaInfoMsg->u1MsgType == CFA_LA_PORT_CHANNEL_OPER_MSG)
    {
        pExtTriggerMsg->i4MsgType = CFA_PORT_CHANNEL_OPER_MSG;
        pExtTriggerMsg->uExtTrgMsg.LinkStatusMsg.u4IfIndex =
            pCfaLaInfoMsg->u4IfIndex;
        pExtTriggerMsg->uExtTrgMsg.LinkStatusMsg.u1LinkStatus =
            pCfaLaInfoMsg->u1OperStatus;
    }

    if (OsixQueSend (CFA_EXT_TRIGGER_QID, (UINT1 *) &pExtTriggerMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaApiPortChannelUpdateInfo - "
                  "Message En-queue to CFA on interface %d FAIL.\n",
                  pCfaLaInfoMsg->u4IfIndex);

        MemReleaseMemBlock (ExtTriggerMsgPoolId, (UINT1 *) pExtTriggerMsg);
        return (CFA_FAILURE);
    }

    /* send an explicit event to the task */
    OsixEvtSend (CFA_TASK_ID, CFA_EXT_TRIGGER_EVENT);

    CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
              "Exiting CfaApiPortChannelUpdateInfo -"
              "Enqueue to CFA on interface %d SUCCESS.\n",
              pCfaLaInfoMsg->u4IfIndex);
    return CFA_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name        : CfaUpdateVipOperStatus
 *
 *    Description          : This function is used to trigger CFA for 
 *                           updating the OperStatus of the VIP
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns          : CFA_SUCCESS / CFA_FAILURE.
 *****************************************************************************/
INT4
CfaUpdateVipOperStatus (UINT4 u4VipIfIndex, UINT1 u1OperStatus)
{
    tCfaExtTriggerMsg  *pExtTriggerMsg;

    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaUpdateVipOperStatus - " "CFA is not initialized");
        return CFA_FAILURE;
    }
    if ((pExtTriggerMsg = (tCfaExtTriggerMsg *)
         MemAllocMemBlk (ExtTriggerMsgPoolId)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaUpdateVipOperStatus - "
                  "Mempool allocation failed %d FAIL.\n", u4VipIfIndex);
        return CFA_FAILURE;
    }

    pExtTriggerMsg->uExtTrgMsg.VipOperStatusMsg.u4IfIndex = u4VipIfIndex;
    pExtTriggerMsg->uExtTrgMsg.VipOperStatusMsg.u1VipOperStatus = u1OperStatus;
    pExtTriggerMsg->i4MsgType = CFA_VIP_OPER_STATUS_MSG;

    if (OsixQueSend (CFA_EXT_TRIGGER_QID, (UINT1 *) &pExtTriggerMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaUpdateVipOperStatus - "
                  "Message En-queue to CFA on interface %d FAIL.\n",
                  u4VipIfIndex);

        MemReleaseMemBlock (ExtTriggerMsgPoolId, (UINT1 *) pExtTriggerMsg);
        return (CFA_FAILURE);
    }

    /* send an explicit event to the task */
    OsixEvtSend (CFA_TASK_ID, CFA_EXT_TRIGGER_EVENT);

    CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
              "Exiting CfaHandlePktFromIsIs -"
              "Pkt Enqueue to CFA on interface %d SUCCESS.\n", u4VipIfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaIsThisInterfaceVlan                               */
/*                                                                           */
/* Description        : This function checks whether the given Vlan is an    */
/*                      interface Vlan or not.                               */
/*                                                                           */
/* Input(s)           : VlanId                                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_TRUE/ CFA_FALSE                                  */
/*****************************************************************************/

BOOL1
CfaIsThisInterfaceVlan (UINT2 VlanId)
{
    INT4                i4RetVal = CFA_FALSE;
    BOOL1               bResult;

    CFA_DS_LOCK ();

#ifdef WGS_WANTED
    OSIX_BITLIST_IS_BIT_SET (gMgmtVlanList, VlanId, VLAN_LIST_SIZE, bResult);
#else
    OSIX_BITLIST_IS_BIT_SET (gIntfVlanBitList, VlanId, VLAN_LIST_SIZE, bResult);
#endif

    if (bResult == OSIX_TRUE)
    {
        i4RetVal = CFA_TRUE;
    }

    CFA_DS_UNLOCK ();

    return ((BOOL1) i4RetVal);
}

/*****************************************************************************/
/* Function Name      : CfaCheckStatusAndSendTrap                            */
/*                                                                           */
/* Description        : This function sends SNMP trap if trap is enabled     */
/*                                                                           */
/* Input(s)           : u2Port - Port Number                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
CfaCheckStatusAndSendTrap (UINT2 u2Port)
{
    /* This API is called from MSR and hence we can safely take the CFA 
     * Protocol lock here since CFA will never take the MSR lock */

    UINT1               u1TrapStatus;
    UINT1               u1OperStatus = CFA_IF_DOWN;

    CFA_LOCK ();

    if (!(u2Port <= 0 || u2Port > SYS_DEF_MAX_INTERFACES))
    {

        if (CfaGetLinkTrapEnabledStatus (u2Port, &u1TrapStatus) == CFA_SUCCESS)
        {
            if (u1TrapStatus == CFA_TRUE)
            {
                CfaGetIfOperStatus (u2Port, &u1OperStatus);
                CfaSnmpifSendTrap (u2Port, u1OperStatus,
                                   CFA_IF_ENTRY (u2Port)->u1AdminStatus);
            }
        }
    }

    CFA_UNLOCK ();
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetSysMacAddress
 *
 *    Description         : This function returns the Switch's Base Mac address
 *                          read from NVRAM
 *    Input(s)            : None. 
 *
 *    Output(s)           : pSwitchMac - the Switch Mac address.
 *
 *    Global Variables Referred : gIssSysGroupInfo.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None 
 *                         
 *
 *****************************************************************************/

VOID
CfaGetSysMacAddress (tMacAddr SwitchMac)
{
    MEMCPY (SwitchMac, gIssSysGroupInfo.BaseMacAddr, CFA_ENET_ADDR_LEN);
    return;
}

/*****************************************************************************
 *
 *    Function Name       : CfaIsOpenflowPort
 *
 *    Description         : This function verifies whether given interface is
 *                          Openflow Interface
 *
 *    Input(s)            : Interface Index
 *
 *    Output(s)           : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE
 *
 *
 *****************************************************************************/
INT4
CfaIsOpenflowPort (UINT4 u4IfIndex)
{
    UINT1               u1SubType = 0;

    CfaGetIfSubType (u4IfIndex, &u1SubType);
    if (u1SubType == CFA_SUBTYPE_OPENFLOW_INTERFACE)
    {
        return CFA_SUCCESS;
    }
    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaCreateOpenflowVlan
 *
 *    Description         : This function creates vlan interface in CFA
 *                          
 *    Input(s)            : VlanId
 *
 *    Output(s)           : None.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : OSIX_SUCCESS/OSIX_FAILURE
 *                         
 *
 *****************************************************************************/
INT4
CfaCreateOpenflowVlan (UINT4 u4VlanId, UINT4 *pu4IfIndex)
{
    tSNMP_OCTET_STRING_TYPE Alias;
    UINT1              *pu1Alias;
    UINT4               u4ErrCode = SNMP_ERR_NO_ERROR;
    UINT4               u4IfIndex = 0;
    UINT1               au1IfName[CFA_MAX_IFALIAS_LENGTH];

    pu1Alias = au1IfName;
    STRNCPY (au1IfName, CFA_OPENFLOW_VLAN_ALIAS_PREFIX,
             STRLEN (CFA_OPENFLOW_VLAN_ALIAS_PREFIX));
    au1IfName[STRLEN (CFA_OPENFLOW_VLAN_ALIAS_PREFIX)] = '\0';
    SPRINTF ((CHR1 *) pu1Alias, "%s%u", au1IfName, u4VlanId);

    /* Get the Free Openflow Interface Index from the OpenflowInterface Index's Pool */
    if (CfaGetFreeInterfaceIndex (&u4IfIndex, CFA_L2VLAN) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    CFA_LOCK ();
    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return OSIX_FAILURE;
    }
    Alias.i4_Length = (INT4) STRLEN (pu1Alias);
    Alias.pu1_OctetList = pu1Alias;

    /* we can directly call set routine, because validation is done already
     * in the function CfaCliCreateInterface() */
    nmhSetIfAlias (u4IfIndex, &Alias);

    /* Set the Type */
    if (nmhTestv2IfMainType (&u4ErrCode, (INT4) u4IfIndex, CFA_L2VLAN) ==
        SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CFA_UNLOCK ();
        return (OSIX_FAILURE);
    }
    if (nmhSetIfMainType (u4IfIndex, CFA_L2VLAN) == SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CFA_UNLOCK ();
        return (OSIX_FAILURE);
    }
    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return OSIX_FAILURE;
    }
    if (nmhTestv2IfMainExtSubType
        (&u4ErrCode, (INT4) u4IfIndex,
         CFA_SUBTYPE_OPENFLOW_INTERFACE) == SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CFA_UNLOCK ();
        return (OSIX_FAILURE);
    }

    if (nmhSetIfMainExtSubType
        ((INT4) u4IfIndex, CFA_SUBTYPE_OPENFLOW_INTERFACE) == SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CFA_UNLOCK ();
        return (OSIX_FAILURE);
    }
    if (nmhTestv2IfMainAdminStatus (&u4ErrCode, (INT4) u4IfIndex, CFA_IF_UP) ==
        SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CFA_UNLOCK ();
        return (OSIX_FAILURE);
    }
    if (nmhSetIfMainAdminStatus (u4IfIndex, CFA_IF_UP) == SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CFA_UNLOCK ();
        return (OSIX_FAILURE);
    }

    *pu4IfIndex = u4IfIndex;
    CFA_UNLOCK ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaDeleteOpenflowVlan
 *
 *    Description         : This function deletes vlan interface in CFA
 *                          
 *    Input(s)            : VlanId
 *
 *    Output(s)           : None.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : OSIX_SUCCESS/OSIX_FAILURE
 *                         
 *
 *****************************************************************************/

INT4
CfaDeleteOpenflowVlan (UINT4 u4VlanId, UINT4 *pu4IfIndex)
{
    UINT4               u4ErrCode = SNMP_ERR_NO_ERROR;
    UINT4               u4IfIndex = 0;
    UINT4               u4RetValue = 0;
    UINT1              *pu1Alias = NULL;
    UINT1               au1IfName[CFA_MAX_IFALIAS_LENGTH];

    UNUSED_PARAM (u4RetValue);

    pu1Alias = au1IfName;
    STRNCPY (au1IfName, CFA_OPENFLOW_VLAN_ALIAS_PREFIX,
             STRLEN (CFA_OPENFLOW_VLAN_ALIAS_PREFIX));
    au1IfName[STRLEN (CFA_OPENFLOW_VLAN_ALIAS_PREFIX)] = '\0';
    SPRINTF ((CHR1 *) pu1Alias, "%s%u", au1IfName, u4VlanId);

    u4RetValue = CfaGetInterfaceIndexFromName (pu1Alias, &u4IfIndex);
    CFA_LOCK ();
    if (nmhTestv2IfMainRowStatus (&u4ErrCode, (INT4) u4IfIndex,
                                  CFA_RS_DESTROY) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return (OSIX_FAILURE);
    }

    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return (OSIX_FAILURE);
    }

    CFA_UNLOCK ();
    *pu4IfIndex = u4IfIndex;

    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : CfaIsThisOpenflowVlan                                */
/*                                                                           */
/* Description        : This function checks whether the given Vlan is an    */
/*                      Openflow Vlan or not.                                */
/*                                                                           */
/* Input(s)           : VlanId                                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_TRUE/ CFA_FALSE                                  */
/*****************************************************************************/

BOOL1
CfaIsThisOpenflowVlan (UINT4 u4VlanId)
{
    UINT1               u1SubType = 0;
    BOOL1               bResult = OSIX_FAILURE;
    UINT1              *pu1Alias = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4RetValue = 0;
    UINT1               au1IfName[CFA_MAX_IFALIAS_LENGTH];

    UNUSED_PARAM (u4RetValue);

    pu1Alias = au1IfName;

    STRNCPY (au1IfName, CFA_OPENFLOW_VLAN_ALIAS_PREFIX,
             STRLEN (CFA_OPENFLOW_VLAN_ALIAS_PREFIX));
    au1IfName[STRLEN (CFA_OPENFLOW_VLAN_ALIAS_PREFIX)] = '\0';
    SPRINTF ((CHR1 *) pu1Alias, "%s%u", au1IfName, u4VlanId);

    u4RetValue = CfaGetInterfaceIndexFromName (pu1Alias, &u4IfIndex);

    CfaGetIfSubType (u4IfIndex, &u1SubType);
    if (u1SubType == CFA_SUBTYPE_OPENFLOW_INTERFACE)
    {
        bResult = OSIX_SUCCESS;
    }

    return bResult;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGetIfInfo
 *
 *    Description          : This function returns the interface related params
 *                           assigned to this interface to the external modules.
 *
 *    Input(s)             : u4IfIndex, *pIfInfo 
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred :None 
 *
 *    Global Variables Modified :None 
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            :CFA_SUCCESS if u4IfIndex is valid 
 *                        CFA_FAILURE if u4IfIndex is Invalid 
 *****************************************************************************/
INT4
CfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    UINT1               u1DefRouterIdx = CFA_FALSE;
    INT4                i4IfValid = CFA_FALSE;

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in - CfaGetIfInfo "
                  "Invalid Interface Index %d - FAIL 1\n", u4IfIndex);
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_FALSE)
    {
        CFA_DS_UNLOCK ();
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in - CfaGetIfInfo "
                  "Invalid Interface Index %d \n", u4IfIndex);
        return CFA_FAILURE;
    }

    /* Fill the IfInfo structure, that is passed in, even if the interface is 
     * not active and return FAILURE after filling. */

    pIfInfo->u1IfType = CFA_CDB_IF_TYPE (u4IfIndex);
    pIfInfo->u1IfSubType = CFA_CDB_IF_SUB_TYPE (u4IfIndex);
    pIfInfo->u4IfMtu = CFA_CDB_IF_MTU (u4IfIndex);
    pIfInfo->u1IfOperStatus = CFA_CDB_IF_OPER (u4IfIndex);
    pIfInfo->u1IfAdminStatus = CFA_CDB_IF_ADMIN_STATUS (u4IfIndex);
    pIfInfo->i4IsInternalPort = CFA_CDB_IS_INTF_INTERNAL (u4IfIndex);
    pIfInfo->u4IfSpeed = CFA_CDB_IF_SPEED (u4IfIndex);
    pIfInfo->u4IfHighSpeed = CFA_CDB_IF_HIGHSPEED (u4IfIndex);
    pIfInfo->i4Active = CFA_CDB_IS_INTF_ACTIVE (u4IfIndex);
    pIfInfo->u1WanType = CFA_CDB_IF_WAN_TYPE (u4IfIndex);
    pIfInfo->u1NwType = CFA_CDB_IF_NW_TYPE (u4IfIndex);
    pIfInfo->i4Valid = CFA_CDB_IS_INTF_VALID (u4IfIndex);
    pIfInfo->u2VlanId = CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex);
    pIfInfo->u1PrefixLen = 0;
    CFA_CDB_GET_ENCAP_STATUS (u4IfIndex, (pIfInfo->u1IsEncapSet));

    STRNCPY (pIfInfo->au1IfName, CFA_CDB_IF_ALIAS (u4IfIndex),
             sizeof (pIfInfo->au1IfName) - CFA_STR_DELIM_SIZE);
    pIfInfo->au1IfName[sizeof (pIfInfo->au1IfName) - 1] = '\0';

    if (STRLEN (CFA_CDB_IF_ALIAS_NAME (u4IfIndex)) != 0)
    {
        STRNCPY (pIfInfo->au1IfAliasName, CFA_CDB_IF_ALIAS_NAME (u4IfIndex),
                 sizeof (pIfInfo->au1IfAliasName) - CFA_STR_DELIM_SIZE);
        pIfInfo->au1IfAliasName[sizeof (pIfInfo->au1IfAliasName) - 1] = '\0';
    }
    MEMCPY (pIfInfo->au1Descr, CFA_CDB_IF_DESC (u4IfIndex),
            CFA_MAX_DESCR_LENGTH);

    pIfInfo->i4IpPort = CFA_CDB_IF_IP_IPPORT (u4IfIndex);
#ifdef LNXIP4_WANTED
    pIfInfo->u4IndexMgrPort = CFA_CDB_IDX_MGR_PORT (u4IfIndex);
#endif

    pIfInfo->u1DuplexStatus = CFA_CDB_IF_DUPLEXSTATUS (u4IfIndex);
    pIfInfo->u1AutoNegSupport = CFA_CDB_IF_AUTONEGSUPPORT (u4IfIndex);
    pIfInfo->u1AutoNegStatus = CFA_CDB_IF_AUTONEGSTATUS (u4IfIndex);
    pIfInfo->u2AdvtCapability = CFA_CDB_IF_ADVT_CAPABILITY (u4IfIndex);
    pIfInfo->u2OperMauType = CFA_CDB_IF_OPER_MAU_TYPE (u4IfIndex);
    pIfInfo->u2VlanId = CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex);
    pIfInfo->u1BridgedIface
        = (UINT1) CFA_CDB_IF_BRIDGED_IFACE_STATUS (u4IfIndex);
    pIfInfo->u4ACPort = CFA_CDB_IF_AC_PORT (u4IfIndex);
    pIfInfo->u2ACVlan = CFA_CDB_IF_AC_VLAN (u4IfIndex);

    if ((pIfInfo->u1IfType == CFA_ENET) || (pIfInfo->u1IfType == CFA_LAGG)
        || (pIfInfo->u1IfType == CFA_PROP_VIRTUAL_INTERFACE)
        || (pIfInfo->u1IfType == CFA_BRIDGED_INTERFACE)
        || (pIfInfo->u1IfType == CFA_PIP) || (pIfInfo->u1IfType == CFA_PPP)
        || (pIfInfo->u1IfType == CFA_PSEUDO_WIRE)
        || (pIfInfo->u1IfType == CFA_VXLAN_NVE)
        || (pIfInfo->u1IfType == CFA_TAP) || (pIfInfo->u1IfType == CFA_OTHER))

    {
        CFA_CDB_CHECK_IF_TYPE (pIfInfo->u1IfType, u1DefRouterIdx);

        if (CFA_FALSE == u1DefRouterIdx)
        {
            CFA_CDB_GET_ENET_IWF_LOCAL_MAC (u4IfIndex, pIfInfo->au1MacAddr);
        }
        else
        {
            i4IfValid = CFA_CDB_IS_INTF_VALID (CFA_DEFAULT_ROUTER_IFINDEX);
            if (i4IfValid == CFA_TRUE)
            {
                CFA_CDB_GET_ENET_IWF_LOCAL_MAC (CFA_DEFAULT_ROUTER_IFINDEX,
                                                pIfInfo->au1MacAddr);
            }
        }
    }
    else if ((pIfInfo->u1IfType == CFA_L3IPVLAN) ||
             (pIfInfo->u1IfType == CFA_LOOPBACK) ||
             (pIfInfo->u1IfType == CFA_L3SUB_INTF)
#ifdef WGS_WANTED
             || (pIfInfo->u1IfType == CFA_L2VLAN)
#endif /* WGS_WANTED */
        )
    {
        CFA_CDB_CHECK_IF_TYPE (pIfInfo->u1IfType, u1DefRouterIdx);

        if (CFA_FALSE == u1DefRouterIdx)
        {
            CFA_CDB_GET_ENET_IWF_LOCAL_MAC (u4IfIndex, pIfInfo->au1MacAddr);
        }
        else
        {
            i4IfValid = CFA_CDB_IS_INTF_VALID (CFA_DEFAULT_ROUTER_IFINDEX);
            if (i4IfValid == CFA_TRUE)
            {
                CFA_CDB_GET_ENET_IWF_LOCAL_MAC (CFA_DEFAULT_ROUTER_IFINDEX,
                                                pIfInfo->au1MacAddr);
            }
        }
    }
#if defined (WLC_WANTED) || defined (WTP_WANTED)
    if ((pIfInfo->u1IfType == CFA_RADIO) ||
        (pIfInfo->u1IfType == CFA_WLAN_RADIO) ||
        (pIfInfo->u1IfType == CFA_CAPWAP_VIRT_RADIO) ||
        (pIfInfo->u1IfType == CFA_CAPWAP_DOT11_PROFILE) ||
        (pIfInfo->u1IfType == CFA_CAPWAP_DOT11_BSS))
    {
        CFA_CDB_GET_ENET_IWF_LOCAL_MAC (u4IfIndex, pIfInfo->au1MacAddr);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
#endif

    MEMCPY (pIfInfo->au1PeerMacAddr,
            CFA_CDB_IF_ENTRY (u4IfIndex)->au1PeerMacAddr, CFA_ENET_ADDR_LEN);
    pIfInfo->u4UnnumAssocIPIf = CFA_CDB_IF_ENTRY (u4IfIndex)->u4UnnumAssocIPIf;

    /* Get the bridge port type. */
    pIfInfo->u1BrgPortType = CFA_CDB_IF_BRIDGE_PORT_TYPE (u4IfIndex);

    pIfInfo->EoamParams.u1EoamStatus = CFA_CDB_IF_EOAM_STATUS (u4IfIndex);
    pIfInfo->EoamParams.u1MuxState = CFA_CDB_IF_EOAM_MUX_STATE (u4IfIndex);
    pIfInfo->EoamParams.u1ParState = CFA_CDB_IF_EOAM_PAR_STATE (u4IfIndex);
    pIfInfo->EoamParams.u1UniDirSupp = CFA_CDB_IF_EOAM_UNIDIR_SUPP (u4IfIndex);
    pIfInfo->EoamParams.u1LinkFault = CFA_CDB_IF_EOAM_LINK_FAULT (u4IfIndex);
    pIfInfo->EoamParams.u1RemoteLB = CFA_CDB_IF_EOAM_REMOTE_LB (u4IfIndex);
    pIfInfo->u1PauseAdminNode = CFA_CDB_IF_PAUSE_ADMIN_MODE (u4IfIndex);
    CFA_DS_UNLOCK ();
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaNotifySpeedToHigherLayer
 *
 *    Description          : This function notifies higher layer when speed 
 *                           of interface changes.
 *
 *    Input(s)             : u4IfIndex
 *
 *    Output(s)            : None.
 *
 *    Returns            :CFA_SUCCESS if u4IfIndex is valid
 *                        CFA_FAILURE if u4IfIndex is Invalid
 *****************************************************************************/
INT4
CfaNotifySpeedToHigherLayer (UINT4 u4IfIndex)
{

    UINT1               u1IfType = CFA_NONE;
    UINT1               u1IsActive = CFA_FALSE;
    UINT1               u1IsAlreadyRegWithIp = CFA_FALSE;

    if (CFA_IF_RS (u4IfIndex) == CFA_RS_ACTIVE)
    {
        u1IsActive = CFA_TRUE;
        if (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX)
        {
            u1IsAlreadyRegWithIp = CFA_TRUE;
        }
    }
    CfaGetIfType (u4IfIndex, &u1IfType);

    if (u1IfType == CFA_INVALID_TYPE)
    {
        return CFA_FAILURE;
    }
    switch (u1IfType)
    {
        case CFA_PIP:
        case CFA_BRIDGED_INTERFACE:
        case CFA_TUNNEL:
        case CFA_ENET:
        case CFA_LAGG:
        case CFA_L3IPVLAN:

            if (u1IsActive == CFA_TRUE)
            {
                if (u1IsAlreadyRegWithIp == CFA_TRUE)
                {
                    if (CfaIfmConfigNetworkInterface (CFA_NET_IF_UPD,
                                                      u4IfIndex, 0,
                                                      NULL) != CFA_SUCCESS)
                    {
                        return CFA_FAILURE;
                    }
                }
            }
            break;
        case CFA_TELINK:
            break;

        default:
            return CFA_FAILURE;    /* not expected */

    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaSetIfInfo
 *
 *    Description          : This function updates the interface related params
 *                           assigned to this interface by the external modules.
 *
 *    Input(s)             : i4CfaIfParam, u4IfIndex, *pIfInfo 
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred :None 
 *
 *    Global Variables Modified :None 
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            :CFA_SUCCESS if u4IfIndex is valid 
 *                        CFA_FAILURE if u4IfIndex is Invalid 
 *****************************************************************************/
INT4
CfaSetIfInfo (INT4 i4CfaIfParam, UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tCfaLaInfoMsg       CfaLaInfoMsg;
    UINT4               u4SeqNum = 0;
    INT4                i4IfValidStatus = CFA_FALSE;
    UINT1               u1IfType = 0;
    UINT1               u1DefRouterIdx = CFA_FALSE;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    MEMSET (&CfaLaInfoMsg, 0, sizeof (tCfaLaInfoMsg));

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in - CfaSetIfInfo "
                  "Invalid Interface Index %d - FAIL 1\n", u4IfIndex);
        return CFA_FAILURE;
    }

    if (pIfInfo == NULL)
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in - CfaSetIfInfo "
                  "Invalid Interface Index %d \n", u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    switch (i4CfaIfParam)
    {
        case IF_TYPE:

            CFA_DS_UNLOCK ();
            CfaIfUtlIfTypeDbUpdate (u4IfIndex, pIfInfo->u1IfType,
                                    CFA_CDB_IFTYPE_REQ);
            CFA_DS_LOCK ();
            CFA_CDB_IF_TYPE (u4IfIndex) = pIfInfo->u1IfType;
            break;

        case IF_OPER_STATUS:

            if (CFA_CDB_IF_OPER (u4IfIndex) != pIfInfo->u1IfOperStatus)
            {
                CFA_CDB_IF_OPER (u4IfIndex) = pIfInfo->u1IfOperStatus;
                /* Do not update the time for openflow vlan interface */
                if ((CfaIsOpenFlowIndex (u4IfIndex)) == CFA_FALSE)
                {
                    OsixGetSysTime (&CFA_IF_CHNG (u4IfIndex));
                }

                if (CFA_CDB_IF_TYPE (u4IfIndex) == CFA_LAGG)
                {
                    CFA_DS_UNLOCK ();
                    /* Port channel oper indication for CFA UFD */
                    CfaLaInfoMsg.u4IfIndex = u4IfIndex;
                    CfaLaInfoMsg.u1OperStatus = pIfInfo->u1IfOperStatus;
                    CfaLaInfoMsg.u1MsgType = CFA_LA_PORT_CHANNEL_OPER_MSG;
                    CfaApiPortChannelUpdateInfo (&CfaLaInfoMsg);
                    CFA_DS_LOCK ();
                }
            }

            break;

        case IF_SPEED:

            CFA_CDB_IF_SPEED (u4IfIndex) = pIfInfo->u4IfSpeed;
            CFA_CDB_IF_HIGHSPEED (u4IfIndex) = pIfInfo->u4IfHighSpeed;
            break;

        case IF_MAC_ADDRESS:

            u1IfType = CFA_CDB_IF_TYPE (u4IfIndex);
            CFA_CDB_CHECK_IF_TYPE (u1IfType, u1DefRouterIdx);
            if (CFA_FALSE == u1DefRouterIdx)
            {
                CFA_CDB_SET_ENET_IWF_LOCAL_MAC (u4IfIndex, pIfInfo->au1MacAddr,
                                                CFA_ENET_ADDR_LEN);
            }
            else
            {
                i4IfValidStatus =
                    CFA_CDB_IS_INTF_VALID (CFA_DEFAULT_ROUTER_IFINDEX);
                if (i4IfValidStatus == CFA_TRUE)
                {

                    CFA_CDB_SET_ENET_IWF_LOCAL_MAC (CFA_DEFAULT_ROUTER_IFINDEX,
                                                    pIfInfo->au1MacAddr,
                                                    CFA_ENET_ADDR_LEN);
                }
            }

            break;

        case IF_NAME:

            STRNCPY (CFA_CDB_IF_ALIAS (u4IfIndex),
                     pIfInfo->au1IfName, CFA_MAX_PORT_NAME_LENGTH - 1);
            break;

        case IF_DESC:

            STRNCPY (CFA_CDB_IF_DESC (u4IfIndex),
                     pIfInfo->au1Descr, CFA_MAX_DESCR_LENGTH - 1);
            break;

        case IF_MTU:

            CFA_CDB_IF_MTU (u4IfIndex) = pIfInfo->u4IfMtu;
            break;

        case IF_VLANID_OF_IVR:

            CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex) = pIfInfo->u2VlanId;
            break;

        case IF_DUPLEX_STATUS:

            CFA_CDB_IF_DUPLEXSTATUS (u4IfIndex) = pIfInfo->u1DuplexStatus;
            break;

        case IF_BRIDGE_IFACE_STATUS:

            CFA_CDB_IF_BRIDGED_IFACE_STATUS (u4IfIndex) =
                pIfInfo->u1BridgedIface;
            break;

        case IF_IP_PORTNUM:
            CFA_CDB_IF_IP_IPPORT (u4IfIndex) = pIfInfo->i4IpPort;
            break;

        case IF_EOAM_STATUS:
            CFA_CDB_IF_EOAM_STATUS (u4IfIndex) =
                pIfInfo->EoamParams.u1EoamStatus;
            break;

        case IF_EOAM_MUX_STATE:
            CFA_CDB_IF_EOAM_MUX_STATE (u4IfIndex) =
                pIfInfo->EoamParams.u1MuxState;
            break;

        case IF_EOAM_PARSER_STATE:
            CFA_CDB_IF_EOAM_PAR_STATE (u4IfIndex) =
                pIfInfo->EoamParams.u1ParState;
            break;

        case IF_EOAM_UNIDIR_SUPP:
            CFA_CDB_IF_EOAM_UNIDIR_SUPP (u4IfIndex) =
                pIfInfo->EoamParams.u1UniDirSupp;
            break;

        case IF_EOAM_LINK_FAULT:
            CFA_CDB_IF_EOAM_LINK_FAULT (u4IfIndex) =
                pIfInfo->EoamParams.u1LinkFault;
            break;

        case IF_EOAM_REMOTE_LB:
            CFA_CDB_IF_EOAM_REMOTE_LB (u4IfIndex) =
                pIfInfo->EoamParams.u1RemoteLB;
            break;
        case IF_BRG_PORT_TYPE:
            CFA_CDB_IF_BRIDGE_PORT_TYPE (u4IfIndex) = pIfInfo->u1BrgPortType;
            CFA_DS_UNLOCK ();
            /* Only Increment MSR trigger should be given
               RM_SYNC should not be given */
            u4SeqNum = 0;
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, IfMainBrgPortType,
                                  u4SeqNum, FALSE, NULL, NULL, 1, SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              u4IfIndex, pIfInfo->u1BrgPortType));
            CFA_DS_LOCK ();
            break;
        case IF_PAUSE_ADMIN_MODE:
            CFA_CDB_IF_PAUSE_ADMIN_MODE (u4IfIndex) = pIfInfo->u1PauseAdminNode;

            CFA_DS_UNLOCK ();
            RM_GET_SEQ_NUM (&u4SeqNum);
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Dot3PauseAdminMode,
                                  u4SeqNum, FALSE, NULL, NULL, 1, SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              u4IfIndex, pIfInfo->u1PauseAdminNode));
            CFA_DS_LOCK ();
            break;
        case IF_AUTONEG_STATUS:
            CFA_CDB_IF_AUTONEGSTATUS (u4IfIndex) = pIfInfo->u1AutoNegStatus;
            break;
#ifdef LNXIP4_WANTED
        case IF_IP_IDX_MGR_NUM:
            CFA_CDB_IDX_MGR_PORT (u4IfIndex) = pIfInfo->u4IndexMgrPort;
            break;
#endif
        case IF_L3VLAN_INTF_STATS:
            CFA_CDB_IP_INTF_STATS (u4IfIndex) = pIfInfo->u4IpIntfStats;
        default:
            break;
    }

    CFA_DS_UNLOCK ();

    CfaCopyPhyPortPropToLogicalInterface (u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : CfaGetIfOperStatus                                   */
/*                                                                           */
/* Description        : This function gets the oper-status of the interface  */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu1OperStatus:Pointer to the oper status             */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfOperStatus (UINT4 u4IfIndex, UINT1 *pu1OperStatus)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu1OperStatus = CFA_CDB_IF_OPER (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfMainStorageType                              */
/*                                                                           */
/* Description        : This function gets the StorageType of the interface  */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pi4IfMainStorageType:Pointer to the Storage Type     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfMainStorageType (UINT4 u4IfIndex, INT4 *pi4IfMainStorageType)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pi4IfMainStorageType = CFA_CDB_IF_STORAGE_TYPE (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaGetNextActivePort                                 */
/*                                                                           */
/* Description        : This function gets the next active port              */
/*                                                                           */
/* Input(s)           : u2Port   :Port Number                                */
/*                                                                           */
/* Output(s)          : pu4NextPort:Pointer to the next active port          */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/

INT4
CfaGetNextActivePort (UINT4 u4Port, UINT4 *pu4NextPort)
{
    UINT4               u4NextPort;

    if (u4Port >= CFA_MAX_INTERFACES ())
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();
    u4NextPort = u4Port + 1;
    CFA_CDB_SCAN (u4NextPort, SYS_DEF_MAX_INTERFACES, CFA_ALL_IFTYPE)
    {
        /* Flexible ifIndex changes */
        if (((CFA_CDB_IF_TYPE (u4NextPort) == CFA_PROP_VIRTUAL_INTERFACE) &&
             (CFA_CDB_IF_SUB_TYPE (u4NextPort) == CFA_SUBTYPE_SISP_INTERFACE))
            || (CFA_CDB_IF_BRIDGE_PORT_TYPE (u4NextPort) ==
                CFA_VIRTUAL_INSTANCE_PORT))
        {
            continue;
        }

        if (CFA_CDB_IS_INTF_ACTIVE (u4NextPort) == CFA_TRUE)
        {
            *pu4NextPort = u4NextPort;
            CFA_DS_UNLOCK ();
            return CFA_SUCCESS;
        }
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

#ifdef CLI_WANTED
/*****************************************************************************/
/* Function Name      : CfaCliGetIfList                                      */
/*                                                                           */
/* Description        : This function calls the functions which convert      */
/*                      the given string to interface list                   */
/*                                                                           */
/* Input(s)           : pi1IfName :Port Number                               */
/*                      pi1IfListStr : Pointer to the string                 */
/*                      pu1IfListArray:Pointer to the string in which the    */
/*                      bits for the port list will be set                   */
/*                      u4IfListArrayLen:Array Length                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

INT4
CfaCliGetIfList (INT1 *pi1IfName, INT1 *pi1IfListStr, UINT1 *pu1IfListArray,
                 UINT4 u4IfListArrayLen)
{
    UINT4               u4IfType = 0;
    UINT4               u4SubType = 0;

    if (CfaCliValidateXInterfaceName (pi1IfName, NULL, &u4IfType)
        == CLI_FAILURE)
    {
        if (CfaCliValidateXSubTypeIntName (pi1IfName, NULL, &u4IfType,
                                           &u4SubType) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if ((u4IfType == CFA_LAGG) || (u4IfType == CFA_PIP) ||
        (u4IfType == CFA_BRIDGED_INTERFACE) || (u4IfType == CFA_ILAN)
        || ((u4IfType == CFA_PROP_VIRTUAL_INTERFACE) && (u4SubType ==
                                                         CFA_SUBTYPE_SISP_INTERFACE))
        || (u4IfType == CFA_PPP) || (u4IfType == CFA_PSEUDO_WIRE)
        || (u4IfType == CFA_VXLAN_NVE)
        || (u4IfType == CFA_STATION_FACING_BRIDGE_PORT)
        || (u4IfType == CFA_TAP))

    {
        if (CliStrToPortList
            ((UINT1 *) pi1IfListStr, pu1IfListArray, u4IfListArrayLen,
             u4IfType) == OSIX_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    else if ((u4IfType == CFA_PROP_VIRTUAL_INTERFACE) && (u4SubType ==
                                                          CFA_SUBTYPE_AC_INTERFACE))
    {
        if (CliStrToPortListExt ((UINT1 *) pi1IfListStr, pu1IfListArray,
                                 u4IfListArrayLen, u4IfType,
                                 u4SubType) == OSIX_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else if ((u4IfType == CFA_ENET) ||
             (u4IfType == CFA_FA_ENET) || (u4IfType == CFA_GI_ENET) ||
             (u4IfType == CFA_XE_ENET) || (u4IfType == CFA_XL_ENET) ||
             (u4IfType == CFA_LVI_ENET))
    {
        if (CliStrToIfaceList ((UINT1 *) pi1IfListStr, pu1IfListArray,
                               u4IfListArrayLen, u4IfType) == OSIX_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

#endif /*CLI_WANTED */
/*****************************************************************************
 *
 *    Function Name       : CfaGetVlanId
 *
 *    Description         : This function returns the VLAN ID for a particular
 *                          VLAN interface.
 *
 *    Input(s)            : Interface index. 
 *
 *    Output(s)           : VLAN ID.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :  VLAN ID correspoding to the VLAN interface index.
 *    
 *****************************************************************************/

INT1
CfaGetVlanId (UINT4 u4IfIndex, tVlanIfaceVlanId * pVlanId)
{
    if (CfaGetIfIvrVlanId (u4IfIndex, pVlanId) == CFA_SUCCESS)
    {
        return CFA_SUCCESS;
    }
    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetLoopbackId
 *
 *    Description         : This function returns the Loopback ID for a particular
 *                          Loopback interface.
 *
 *    Input(s)            : Interface index. 
 *
 *    Output(s)           : Loopback ID.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :  Loopback ID correspoding to the Loopback interface 
 *                          index.
 *    
 *****************************************************************************/

INT1
CfaGetLoopbackId (UINT4 u4IfIndex, UINT2 *pLoopbackId)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pLoopbackId = CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name       :CfaVlanTagFrame 
 *
 *    Description         :Attaches the Vlan tag to the given frame with 
 *                         the given Vlan and priority.
 *
 *    Input(s)            : pBuff       - Pointer to the frame buffer
 *                          VlanId      - VlanId to be set in the tag
 *                          u1Priority  - Priority to be set in the tag
 *
 *    Output(s)           : None
 *
 *    Global Variables Referred : None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *****************************************************************************/
VOID
CfaVlanTagFrame (UINT1 *pBuff, tVlanIfaceVlanId VlanId, UINT1 u1Priority)
{
    UINT2               u2Tag = 0;
    UINT2               u2Protocol = 0;

    u2Protocol = OSIX_HTONS (VLAN_PROTOCOL_ID);
    u2Tag = (UINT2) u1Priority;
    u2Tag = (UINT2) (u2Tag << VLAN_TAG_PRIORITY_SHIFT);
    u2Tag = (UINT2) (u2Tag | (VlanId & VLAN_ID_MASK));
    u2Tag = (UINT2) (OSIX_HTONS (u2Tag));
    MEMCPY (&pBuff[0], (UINT1 *) &u2Protocol, VLAN_TYPE_OR_LEN_SIZE);
    MEMCPY (&pBuff[VLAN_TYPE_OR_LEN_SIZE], (UINT1 *) &u2Tag, VLAN_TAG_SIZE);
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetVlanInterfaceIndex
 *
 *    Description         : This function returns the VLAN interface index 
 *
 *    Input(s)            : VLAN ID. 
 *
 *    Output(s)           : VLAN Interface Index.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :  VLAN interface index corresponding to the 
 *                          VLAN ID,if success. Else, returns the invalid 
 *                          interface index.
 *****************************************************************************/

UINT4
CfaGetVlanInterfaceIndex (tVlanIfaceVlanId VlanId)
{
    UINT2               u2IfIvrVlanId;
#ifdef WGS_WANTED
    UINT1               u1Result = 0;
#else
    UINT4               u4IfIndex = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1VlanIfName[CFA_MAX_PORT_NAME_LENGTH];
#endif /* WGS_WANTED */

    CFA_DS_LOCK ();

#ifdef WGS_WANTED
    UNUSED_PARAM (u2IfIvrVlanId);

    if ((CFA_CDB_IS_INTF_ACTIVE (CFA_MGMT_IF_INDEX) == CFA_TRUE))
    {
        CFA_MGMT_IS_MEMBER_VLAN (CFA_MGMT_VLANLIST (), VlanId, u1Result);
    }
    if (u1Result == CFA_TRUE)
    {
        CFA_DS_UNLOCK ();
        return CFA_MGMT_IF_INDEX;
    }
#else /* WGS_WANTED */
    u4IfIndex = CFA_MIN_IVR_IF_INDEX;
    CFA_CDB_SCAN (u4IfIndex, (CFA_MAX_IVR_IF_INDEX), CFA_L3IPVLAN)
    {
        if ((CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_TRUE))
        {
            u2IfIvrVlanId = CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex);

            if (VlanId == u2IfIvrVlanId)
            {
                MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                MEMSET (au1VlanIfName, 0, CFA_MAX_PORT_NAME_LENGTH);

                SNPRINTF ((CHR1 *) au1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s%d",
                          CFA_L3IPVLAN_NAME_PREFIX, VlanId);
                STRNCPY (au1VlanIfName, CFA_CDB_IF_ALIAS (u4IfIndex),
                         sizeof (au1VlanIfName) - CFA_STR_DELIM_SIZE);
                au1VlanIfName[sizeof (au1VlanIfName) - 1] = '\0';

                if (STRCMP (au1IfName, au1VlanIfName) == 0)
                {
                    CFA_DS_UNLOCK ();
                    return u4IfIndex;
                }
            }
        }
    }
#endif
    CFA_DS_UNLOCK ();
    return CfaIvrMapGetVlanIfIndex (VCM_DEFAULT_CONTEXT, VlanId);
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetVlanInterfaceIndexInCxt
 *
 *    Description         : This function returns the VLAN interface index 
 *
 *    Input(s)            : L2ContextId, VLAN ID. 
 *
 *    Output(s)           : VLAN Interface Index.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :  VLAN interface index corresponding to the 
 *                          VLAN ID,if success. Else, returns the invalid 
 *                          interface index.
 *****************************************************************************/

UINT4
CfaGetVlanInterfaceIndexInCxt (UINT4 u4L2ContextId, tVlanIfaceVlanId VlanId)
{
    UINT2               u2IfIvrVlanId;
#ifdef WGS_WANTED
    UINT1               u1Result = 0;
#else
    UINT4               u4IfIndex = 0;
    UINT4               u4IfL2CxtId = 0;
    UINT4               u4EndIfIndex = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1VlanIfName[CFA_MAX_PORT_NAME_LENGTH];
#endif /* WGS_WANTED */

    CFA_DS_LOCK ();

#ifdef WGS_WANTED
    UNUSED_PARAM (u4L2ContextId);
    UNUSED_PARAM (u2IfIvrVlanId);

    if ((CFA_CDB_IS_INTF_ACTIVE (CFA_MGMT_IF_INDEX) == CFA_TRUE))
    {
        CFA_MGMT_IS_MEMBER_VLAN (CFA_MGMT_VLANLIST (), VlanId, u1Result);
    }
    if (u1Result == CFA_TRUE)
    {
        CFA_DS_UNLOCK ();
        return CFA_MGMT_IF_INDEX;
    }
#else /* WGS_WANTED */
    u4IfIndex = CFA_MIN_IVR_IF_INDEX;
    u4EndIfIndex = CFA_MAX_IVR_IF_INDEX;
    CFA_CDB_SCAN (u4IfIndex, u4EndIfIndex, CFA_L3IPVLAN)
    {
        if ((CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_TRUE))
        {
            u2IfIvrVlanId = CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex);

            if (VlanId == u2IfIvrVlanId)
            {
                MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                MEMSET (au1VlanIfName, 0, CFA_MAX_PORT_NAME_LENGTH);

                SNPRINTF ((CHR1 *) au1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s%d",
                          CFA_L3IPVLAN_NAME_PREFIX, VlanId);
                STRNCPY (au1VlanIfName, CFA_CDB_IF_ALIAS (u4IfIndex),
                         sizeof (au1VlanIfName) - CFA_STR_DELIM_SIZE);
                au1VlanIfName[sizeof (au1VlanIfName) - 1] = '\0';

                /* Going to take the Lock of Other Module. (VCM_LOCK).
                 * So releasing my Lock here..
                 * To Avoid Dead Lock Scenario
                 */
                CFA_DS_UNLOCK ();
                if ((STRCMP (au1IfName, au1VlanIfName) == 0) &&
                    (VcmGetL2CxtIdForIpIface (u4IfIndex,
                                              &u4IfL2CxtId) != VCM_FAILURE) &&
                    (u4IfL2CxtId == u4L2ContextId))
                {
                    /* Already lock is released. 
                     * So no need to Unlock before returning
                     */
                    return u4IfIndex;
                }
                /* Taking my Lock Again */
                CFA_DS_LOCK ();
            }
        }
    }
#endif
    CFA_DS_UNLOCK ();
    return CfaIvrMapGetVlanIfIndex (u4L2ContextId, VlanId);
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetVlanInterfaceL3IndexInCxt
 *
 *    Description         : This function returns the VLAN interface index 
 *
 *    Input(s)            : L3ContextId, VLAN ID. 
 *
 *    Output(s)           : VLAN Interface Index.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :  VLAN interface index corresponding to the 
 *                          VLAN ID,if success. Else, returns the invalid 
 *                          interface index.
 *****************************************************************************/

UINT4
CfaGetVlanInterfaceL3IndexInCxt (UINT4 u4L3ContextId, tVlanIfaceVlanId VlanId)
{
    UINT2               u2IfIvrVlanId;
#ifdef WGS_WANTED
    UINT1               u1Result = 0;
#else
    UINT4               u4IfIndex = 0;
    UINT4               u4IfL3CxtId = 0;
    UINT4               u4EndIfIndex = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1VlanIfName[CFA_MAX_PORT_NAME_LENGTH];
#endif /* WGS_WANTED */

    CFA_DS_LOCK ();

#ifdef WGS_WANTED
    UNUSED_PARAM (u4L3ContextId);
    UNUSED_PARAM (u2IfIvrVlanId);

    if ((CFA_CDB_IS_INTF_ACTIVE (CFA_MGMT_IF_INDEX) == CFA_TRUE))
    {
        CFA_MGMT_IS_MEMBER_VLAN (CFA_MGMT_VLANLIST (), VlanId, u1Result);
    }
    if (u1Result == CFA_TRUE)
    {
        CFA_DS_UNLOCK ();
        return CFA_MGMT_IF_INDEX;
    }
#else /* WGS_WANTED */
    u4IfIndex = CFA_MIN_IVR_IF_INDEX;
    u4EndIfIndex = CFA_MAX_IVR_IF_INDEX;
    CFA_CDB_SCAN (u4IfIndex, u4EndIfIndex, CFA_L3IPVLAN)
    {
        if ((CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_TRUE))
        {
            u2IfIvrVlanId = CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex);

            if (VlanId == u2IfIvrVlanId)
            {
                MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                MEMSET (au1VlanIfName, 0, CFA_MAX_PORT_NAME_LENGTH);

                SNPRINTF ((CHR1 *) au1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s%d",
                          CFA_L3IPVLAN_NAME_PREFIX, VlanId);
                STRNCPY (au1VlanIfName, CFA_CDB_IF_ALIAS (u4IfIndex),
                         sizeof (au1VlanIfName) - CFA_STR_DELIM_SIZE);
                au1VlanIfName[sizeof (au1VlanIfName) - 1] = '\0';

                /* Going to take the Lock of Other Module. (VCM_LOCK).
                 * So releasing my Lock here..
                 * To Avoid Dead Lock Scenario
                 */
                CFA_DS_UNLOCK ();
                if ((STRCMP (au1IfName, au1VlanIfName) == 0) &&
                    (VcmGetContextIdFromCfaIfIndex (u4IfIndex,
                                                    &u4IfL3CxtId) !=
                     VCM_FAILURE) && (u4IfL3CxtId == u4L3ContextId))
                {
                    /* Already lock is released. 
                     * So no need to Unlock before returning
                     */
                    return u4IfIndex;
                }
                /* Taking my Lock Again */
                CFA_DS_LOCK ();
            }
        }
    }
#endif
    CFA_DS_UNLOCK ();
    return CfaIvrMapGetVlanIfIndex (u4L3ContextId, VlanId);
}

/*****************************************************************************
 *
 *    Function Name       : CfaNotifyToL3SubIfVlanIndexInCxt
 *
 *    Description         : This function gets the L3SubIfIndex for the
 *                          corresponding VLANID and sends the notification to
 *                          the respective L3Subinterface on interface status
 *                          change when parent port Oper status is changed.
 *
 *    Input(s)            : L2ContextId, VLAN ID. 
 *
 *    Output(s)           : L3Sub Interface Index.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :  L3Subinterface index corresponding to the 
 *                          VLAN ID,if success. Else, returns the invalid 
 *                          interface index.
 *****************************************************************************/
INT4
CfaNotifyToL3SubIfVlanIndexInCxt (UINT2 u2PortId, UINT4 u4L2ContextId,
                                  tVlanIfaceVlanId VlanId, UINT1 u1OperStatus)
{

    UINT2               u2IfIvrVlanId;
    UINT4               u4IfIndex = 0;
    UINT4               u4IfL2CxtId = 0;
    UINT4               u4EndIfIndex = 0;
    UINT4               u4ParentIfIndex = 0;
    UINT1               u1IntfOperStatus = 0;

    CFA_DS_LOCK ();

    u4IfIndex = CFA_MIN_L3SUB_IF_INDEX;
    u4EndIfIndex = CFA_MAX_L3SUB_IF_INDEX;

    CFA_CDB_SCAN (u4IfIndex, u4EndIfIndex, CFA_L3SUB_INTF)
    {
        if ((CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_TRUE))
        {
            u2IfIvrVlanId = CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex);

            if (VlanId == u2IfIvrVlanId)
            {

                /* Going to take the Lock of Other Module. (VCM_LOCK).
                 * So releasing my Lock here..
                 * To Avoid Dead Lock Scenario
                 */
                CFA_DS_UNLOCK ();
                /* Get the Parent(Physical port) interface index from
                 * logical index */
                if (CfaGetL3SubIfParentIndex (u4IfIndex, &u4ParentIfIndex)
                    == CFA_FAILURE)
                {
                    CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                             "CfaGetL3SubIfParentIndex: Unable to get Parent IfIndex");
                    return CFA_FAILURE;
                }
                /* If PortId is Not Null, Then Check with ParentIndex of Current
                 * Sub-Interface and Trigger the Status Change for Sub-Intf */
                if (u4ParentIfIndex == u2PortId)
                {
                    if ((VcmGetL2CxtIdForIpIface (u4IfIndex, &u4IfL2CxtId)
                         != VCM_FAILURE) && (u4IfL2CxtId == u4L2ContextId)
                        && (u4IfIndex != CFA_INVALID_INDEX))

                    {
                        VLAN_UNLOCK ();
                        if (u1OperStatus == CFA_IF_UP)
                        {

                            if ((CfaVlanIsMemberPort ((UINT4) u2IfIvrVlanId,
                                                      u4ParentIfIndex) ==
                                 CFA_SUCCESS))
                            {
                                u1IntfOperStatus = CFA_IF_UP;
                            }
                            else
                            {
                                u1IntfOperStatus = CFA_IF_DOWN;
                            }
                        }
                        VLAN_LOCK ();

                        /* Taking my Lock Again */
                        CFA_DS_LOCK ();
                        /* send interface status change notification to
                         * this interface index */
                        CfaInterfaceStatusChangeIndication (u4IfIndex,
                                                            u1IntfOperStatus);
                    }
                }
                /* While Mapping the Ports with Vlan for First Time 
                 * PortId will be Null */
                if (u2PortId == 0)
                {
                    if ((VcmGetL2CxtIdForIpIface (u4IfIndex, &u4IfL2CxtId)
                         != VCM_FAILURE) && (u4IfL2CxtId == u4L2ContextId)
                        && (u4IfIndex != CFA_INVALID_INDEX))

                    {
                        VLAN_UNLOCK ();
                        if (u1OperStatus == CFA_IF_UP)
                        {

                            if ((CfaVlanIsMemberPort ((UINT4) u2IfIvrVlanId,
                                                      u4ParentIfIndex) ==
                                 CFA_SUCCESS))
                            {
                                u1IntfOperStatus = CFA_IF_UP;
                            }
                            else
                            {
                                u1IntfOperStatus = CFA_IF_DOWN;
                            }
                        }
                        VLAN_LOCK ();

                        /* Taking my Lock Again */
                        CFA_DS_LOCK ();
                        /* send interface status change notification to
                         * this interface index */
                        CfaInterfaceStatusChangeIndication (u4IfIndex,
                                                            u1IntfOperStatus);
                    }
                }
            }                    /* End of VLAN_ID Check */
        }                        /* End of Intf Active */
    }                            /* End of CFA_CDB_SCAN */

    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetL3SubIfVlanIndexInCxt
 *
 *    Description         : This function returns the L3SubIfIndex for the
 *                          corresponding VLANID 
 *
 *    Input(s)            : L2ContextId, VLAN ID. 
 *
 *    Output(s)           : L3Sub Interface Index.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :  L3Subinterface index corresponding to the 
 *                          VLAN ID,if success. Else, returns the invalid 
 *                          interface index.
 *****************************************************************************/
UINT4
CfaGetL3SubIfVlanIndexInCxt (UINT4 u4L2ContextId, tVlanIfaceVlanId VlanId)
{
    UINT2               u2IfIvrVlanId;
    UINT4               u4IfIndex = 0;
    UINT4               u4IfL2CxtId = 0;
    UINT4               u4EndIfIndex = 0;

    CFA_DS_LOCK ();

    u4IfIndex = CFA_MIN_L3SUB_IF_INDEX;
    u4EndIfIndex = CFA_MAX_L3SUB_IF_INDEX;

    CFA_CDB_SCAN (u4IfIndex, u4EndIfIndex, CFA_L3SUB_INTF)
    {
        if ((CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_TRUE))
        {
            u2IfIvrVlanId = CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex);

            if (VlanId == u2IfIvrVlanId)
            {

                /* Going to take the Lock of Other Module. (VCM_LOCK).
                 * So releasing my Lock here..
                 * To Avoid Dead Lock Scenario
                 */
                CFA_DS_UNLOCK ();

                if ((VcmGetL2CxtIdForIpIface (u4IfIndex, &u4IfL2CxtId)
                     != VCM_FAILURE) && (u4IfL2CxtId == u4L2ContextId))

                {
                    /* Already lock is released. 
                     * So no need to Unlock before returning
                     */
                    return u4IfIndex;
                }
                /* Taking my Lock Again */
                CFA_DS_LOCK ();
            }
        }
    }

    CFA_DS_UNLOCK ();
    return CFA_INVALID_INDEX;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetL3SubIfVlanIndexInCxt
 *
 *    Description         : This function returns the L3SubIfIndex for the
 *                          corresponding VLANID 
 *
 *    Input(s)            : L2ContextId, VLAN ID. 
 *
 *    Output(s)           : L3Sub Interface Index.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :  L3Subinterface index corresponding to the 
 *                          VLAN ID,if success. Else, returns the invalid 
 *                          interface index.
 *****************************************************************************/
UINT4
CfaGetL3XSubIfVlanIndexInCxt (UINT4 u4L2ContextId, tVlanIfaceVlanId VlanId,
                              UINT4 u4ParIfIndex)
{
    UINT2               u2IfIvrVlanId;
    UINT4               u4IfIndex = CFA_INVALID_INDEX;
    UINT4               u4IfL2CxtId = 0;
    UINT4               u4EndIfIndex = 0;
    UINT4               u4ParentIfIndex = 0;

    CFA_DS_LOCK ();

    u4IfIndex = CFA_MIN_L3SUB_IF_INDEX;
    u4EndIfIndex = CFA_MAX_L3SUB_IF_INDEX;

    CFA_CDB_SCAN (u4IfIndex, u4EndIfIndex, CFA_L3SUB_INTF)
    {
        if ((CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_TRUE))
        {
            u2IfIvrVlanId = CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex);

            if (VlanId == u2IfIvrVlanId)
            {

                /* Going to take the Lock of Other Module. (VCM_LOCK).
                 * So releasing my Lock here..
                 * To Avoid Dead Lock Scenario
                 */

                CFA_DS_UNLOCK ();

                CfaGetL3SubIfParentIndex (u4IfIndex, &u4ParentIfIndex);
                if ((VcmGetL2CxtIdForIpIface (u4IfIndex, &u4IfL2CxtId)
                     != VCM_FAILURE) && (u4IfL2CxtId == u4L2ContextId) &&
                    (u4ParentIfIndex == u4ParIfIndex))

                {
                    /* Already lock is released. 
                     * So no need to Unlock before returning
                     */
                    return u4IfIndex;
                }
                /* Taking my Lock Again */
                CFA_DS_LOCK ();
            }
        }
    }
    CFA_DS_UNLOCK ();
    u4IfIndex = CFA_INVALID_INDEX;
    return u4IfIndex;
}

/*****************************************************************************
 *
 *    Function Name       : CfaIvrNotifyVlanIfOperStatusInCxt 
 *
 *    Description         : This function updates the oper status of L3 VLAN 
 *                          interface when there is a change in oper status
 *                          indication from VLAN.
 *
 *    Input(s)            : L2ContextId, VlanId and u1InputOperStatus
 *
 *    Output(s)           : None. 
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :    CFA_SUCCESS when the oper status updation is 
 *                            successful. Else CFA_FAILURE.
 *        
 *****************************************************************************/

INT1
CfaIvrNotifyVlanIfOperStatusInCxt (UINT4 u4L2ContextId,
                                   tVlanIfaceVlanId VlanId, UINT1 u1OperStatus)
{
    UINT4               u4IfIndex;
    UINT2               u2PortId = 0;

    u4IfIndex = (UINT2) CfaGetVlanInterfaceIndexInCxt (u4L2ContextId, VlanId);

    if (u4IfIndex != CFA_INVALID_INDEX)
    {
        CfaInterfaceStatusChangeIndication (u4IfIndex, u1OperStatus);
    }
    /* If VALID Index is not Found ,check if the VLAN Index is present in 
     *  Layer 3 Subinterface and send notification to Subinterface*/
    CfaNotifyToL3SubIfVlanIndexInCxt (u2PortId, u4L2ContextId, VlanId,
                                      u1OperStatus);

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaIvrNotifyMclagIfOperStatusInCxt
 *
 *    Description         : This function updates the oper status of L3 VLAN
 *                          based on Mclag port membership in L2 VLAN
 *
 *    Input(s)            : L2ContextId, VlanId and u1InputOperStatus
 *
 *    Output(s)           : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :    CFA_SUCCESS when the oper status updation is
 *                            successful. Else CFA_FAILURE.
 *
 *****************************************************************************/

INT1
CfaIvrNotifyMclagIfOperStatusInCxt (UINT4 u4L2ContextId,
                                    tVlanIfaceVlanId VlanId, UINT1 u1OperStatus)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = CfaGetVlanInterfaceIndexInCxt (u4L2ContextId, VlanId);

    if (u4IfIndex != CFA_INVALID_INDEX)
    {
        CfaIvrNotifyMclagOperChangeIndication (u4IfIndex, u1OperStatus);
    }

    return CFA_SUCCESS;
}

#ifdef PORT_MAC_WANTED
/*****************************************************************************
 *
 *    Function Name       : CfaIvrVlanIfAssignMac
 *
 *    Description         : This function assigns the active port MAC as L3
 *                          VLAN MAC when there is a oper status "up"
 *                          indication from CFA module for making L3 VLAN
 *                          interface status as active.
 *
 *    Input(s)            : u4IfIndex - L3 VLAN interface index.
 *
 *    Output(s)           : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :    CFA_SUCCESS when the mac address assignment is
 *                            successful. Else CFA_FAILURE.
 *
 *****************************************************************************/

INT1
CfaIvrVlanIfAssignMac (UINT4 u4IfIndex)
{
    UINT1               au1HwAddr[CFA_ENET_ADDR_LEN];
    UINT4               u4Port = 1;
    tPortList           VlanPortList;
    tVlanIfaceVlanId    VlanId;
    BOOL1               bResult = OSIX_FALSE;

    MEMSET (au1HwAddr, 0, CFA_ENET_ADDR_LEN);
    MEMSET (&VlanPortList, 0, sizeof (tPortList));

    /* Validate interface index */
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    /* Get the VlanId from interface index */
    if (CfaGetVlanId (u4IfIndex, &VlanId) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }

    /* Get the member portlist for the given VlanId */
    if (L2IwfGetVlanEgressPorts (VlanId, VlanPortList) == L2IWF_FAILURE)
    {
        return CFA_FAILURE;
    }

    /* Get the member port of the vlan */
    CFA_CDB_SCAN_WITH_LOCK (u4Port, BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                            CFA_ALL_IFTYPE)
    {
        OSIX_BITLIST_IS_BIT_SET (VlanPortList, u4Port,
                                 BRG_PORT_LIST_SIZE, bResult);

        if (bResult == OSIX_TRUE)
        {
            /* Get the member port's MAC Address */
            if (CfaGetIfHwAddr (u4Port, au1HwAddr) == CFA_FAILURE)
            {
                return CFA_FAILURE;
            }

            /* Assign port MAC as interface VLAN MAC */
            CfaSetIfHwAddr (u4IfIndex, au1HwAddr, CFA_ENET_ADDR_LEN);

            return CFA_SUCCESS;
        }
    }
    return CFA_FAILURE;
}
#endif

/*****************************************************************************
 *
 *    Function Name        : CfaDeletePortChannelIndication
 *
 *    Description        : This function is for Posting a message 
 *                         for deleting the PortChannel
 *                         Interface. 
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if deletion succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaDeletePortChannelIndication (UINT4 u4IfIndex)
{
    tCfaExtTriggerMsg  *pExtTriggerMsg;

    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaDeletePortChannelIndication - "
                 "CFA is not initialized.\n");
        return (CFA_FAILURE);
    }
    if ((pExtTriggerMsg = (tCfaExtTriggerMsg *)
         MemAllocMemBlk (ExtTriggerMsgPoolId)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaDeletePortChannelIndication - "
                  "Mempool allocation failed %d FAIL.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    pExtTriggerMsg->i4MsgType = CFA_PORT_CHANNEL_DELETE_MSG;
    pExtTriggerMsg->uExtTrgMsg.u2PortChannelId = (UINT2) u4IfIndex;

    if (OsixQueSend (CFA_EXT_TRIGGER_QID, (UINT1 *) &pExtTriggerMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaDeletePortChannelIndication - "
                  "Message En-queue to CFA on interface %d FAIL.\n", u4IfIndex);

        MemReleaseMemBlock (ExtTriggerMsgPoolId, (UINT1 *) pExtTriggerMsg);
        return (CFA_FAILURE);
    }

/* send an explicit event to the task */
    OsixEvtSend (CFA_TASK_ID, CFA_EXT_TRIGGER_EVENT);

    CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
              "Exiting CfaDeletePortChannelIndication -"
              "Pkt Enqueue to CFA on interface %d SUCCESS.\n", u4IfIndex);
    return CFA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : CfaGetIfCounters                                     */
/*                                                                           */
/* Description        : This function Gets the interface counters            */
/*                                                                           */
/* Input(s)           : u4IfIndex -Interface Index                           */
/*                                                                           */
/*                                                                           */
/* Output(s)          : pIfCounter-Pointer to the interface counter structure*/
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/

INT4
CfaGetIfCounters (UINT4 u4IfIndex, tIfCountersStruct * pIfCounter)
{
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }
#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    /* If L3 switching is disabled, there will not be any
     * * hardware routing and hence we can rely on the software
     * * to give us the correct values of l3 vlan counters*/

    /* For OOB Port,Statistics should be retrieved from Software */
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        CfaGetIfCountersFromSoftware (u4IfIndex, pIfCounter);
        return CFA_SUCCESS;
    }
#endif
    CfaGetIfCountersFromHardware (u4IfIndex, pIfCounter);
#else
    CfaGetIfCountersFromSoftware (u4IfIndex, pIfCounter);
#endif
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetFreeInterfaceIndex
 *
 *    Description         : This function returns the first available
 *                          free interface index of the required type
 *
 *    Input(s)            : u1IfType - Type of free interface required
 *
 *    Output(s)           : pu4Index - Pointer to interface index
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : OSIX_SUCCESS if free index is available
 *                         otherwise OSIX_FAILURE.
 *
 *****************************************************************************/
UINT4
CfaGetFreeInterfaceIndex (UINT4 *pu4Index, UINT4 u4IfType)
{
    return (CfaGetFreeIndexForSubType (pu4Index, u4IfType, 0));
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetFreeIndexForSubType
 *
 *    Description         : This function returns the first available
 *                          free interface index of the required subtype
 *
 *    Input(s)            : u1IfType - Type of free interface required
 *                          u1SubType - SubType of free interface required
 *
 *    Output(s)           : pu4Index - Pointer to interface index
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : OSIX_SUCCESS if free index is available
 *                         otherwise OSIX_FAILURE.
 *
 *****************************************************************************/
UINT4
CfaGetFreeIndexForSubType (UINT4 *pu4Index, UINT4 u4IfType, UINT4 u4SubType)
{
    UINT4               u4Index = 0;
    UINT4               u4IndexMin = 0;
    UINT4               u4IndexMax = 0;
    UINT4               u4LastIndex = 0;
    tCfaIfInfo         *pCfaIfInfo = NULL;
    UINT1               u1IndexFound = CFA_FALSE;
    UINT1               u1IdxFoundValid = CFA_FALSE;

    CFA_DS_LOCK ();

    switch (u4IfType)
    {
        case CFA_ENET:
            /* for ethernet interface */
            u4IndexMin = 1;
            u4IndexMax = SYS_DEF_MAX_ENET_INTERFACES;
            break;

        case CFA_L3SUB_INTF:
            u4IndexMin = CFA_MIN_L3SUB_IF_INDEX;
            u4IndexMax = CFA_MAX_L3SUB_IF_INDEX;
            break;

#ifdef LA_WANTED
        case CFA_LAGG:
            /* for portchannel interface */
            u4IndexMin = SYS_DEF_MAX_PHYSICAL_INTERFACES + 1;
            u4IndexMax = (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG);
            break;
#endif

        case CFA_L3IPVLAN:
            /* for IVR interface */
            u4IndexMin = CFA_MIN_IVR_IF_INDEX;
            u4IndexMax = CFA_MAX_IVR_IF_INDEX;
            break;

        case CFA_TUNNEL:
            /* for IP Tunnel interface */
            u4IndexMin = CFA_MIN_TNL_IF_INDEX;
            u4IndexMax = CFA_MAX_TNL_IF_INDEX;
            break;

#ifdef MPLS_WANTED
        case CFA_MPLS:
            /* for MPLS interface */
            u4IndexMin = CFA_MIN_MPLS_IF_INDEX;
            u4IndexMax = CFA_MAX_MPLS_IF_INDEX;
            break;

        case CFA_MPLS_TUNNEL:
            /* for MPLS Tunnel interface */
            u4IndexMin = CFA_MIN_MPLSTNL_IF_INDEX;
            u4IndexMax = CFA_MAX_MPLSTNL_IF_INDEX;
            break;
#endif

        case CFA_LOOPBACK:
            /* For loopback interface */
            u4IndexMin = CFA_MIN_LOOPBACK_IF_INDEX;
            u4IndexMax = CFA_MAX_LOOPBACK_IF_INDEX;
            break;

        case CFA_PIP:
        case CFA_BRIDGED_INTERFACE:
            /* For internal interface */
            u4IndexMin = CFA_MIN_INTERNAL_IF_INDEX;
            u4IndexMax = CFA_MAX_INTERNAL_IF_INDEX;
            break;

        case CFA_ILAN:
            /* For ilan interface */
            u4IndexMin = CFA_MIN_ILAN_IF_INDEX;
            u4IndexMax = CFA_MAX_ILAN_IF_INDEX;
            break;

        case CFA_PROP_VIRTUAL_INTERFACE:

            if (u4SubType == CFA_SUBTYPE_SISP_INTERFACE)
            {
                u4IndexMin = CFA_MIN_SISP_IF_INDEX;
                u4IndexMax = CFA_MAX_SISP_IF_INDEX;
            }
            else if (u4SubType == CFA_SUBTYPE_AC_INTERFACE)
            {
                u4IndexMin = CFA_MIN_AC_IF_INDEX;
                u4IndexMax = CFA_MAX_AC_IF_INDEX;
            }
            break;

#ifdef VPN_WANTED
#ifdef IKE_WANTED
        case CFA_VPNC:
            /* For VPN dynamic interface */
            u4IndexMin = CFA_MIN_VPNC_IF_INDEX;
            u4IndexMax = CFA_MAX_VPNC_IF_INDEX;
            break;
#endif /* IKE_WANTED */
#endif /* VPN_WANTED */

#ifdef PPP_WANTED

        case CFA_PPP:
            /* For PPP interface */
            u4IndexMin = CFA_MIN_PPP_IF_INDEX;
            u4IndexMax = CFA_MAX_PPP_IF_INDEX;
            break;
#endif /* PPP_WANTED */

#ifdef TLM_WANTED

        case CFA_TELINK:
            /* For TLM Interface */
            u4IndexMin = CFA_MIN_TELINK_IF_INDEX;
            u4IndexMax = CFA_MAX_TELINK_IF_INDEX;
            break;

#endif /* TLM_WANTED */

        case CFA_PSEUDO_WIRE:
            /* For Pseudowire Interface */
            u4IndexMin = CFA_MIN_PSW_IF_INDEX;
            u4IndexMax = CFA_MAX_PSW_IF_INDEX;
            break;

#if defined (WLC_WANTED) || defined (WTP_WANTED)
        case CFA_RADIO:
            /* for radio interface */
            u4IndexMin = SYS_DEF_MAX_ENET_INTERFACES + 1;
            u4IndexMax = SYS_DEF_MAX_PHYSICAL_INTERFACES;
            break;

        case CFA_CAPWAP_VIRT_RADIO:
            u4IndexMin = CFA_MIN_WSS_VIRT_RADIO;
            u4IndexMax = CFA_MAX_WSS_VIRT_RADIO;
            break;

        case CFA_CAPWAP_DOT11_PROFILE:
            u4IndexMin = CFA_MIN_WSS_DOT11_SSID;
            u4IndexMax = CFA_MAX_WSS_DOT11_SSID;
            break;

        case CFA_WLAN_RADIO:
        case CFA_CAPWAP_DOT11_BSS:
            /* For WSS Interfaces */
            u4IndexMin = CFA_MIN_WSS_BSSID;
            u4IndexMax = CFA_MAX_WSS_BSSID;
            break;
#endif
#ifdef OPENFLOW_WANTED
        case CFA_L2VLAN:
            /* For Openflow vlan Interface */
            u4IndexMin = CFA_MIN_OF_IF_INDEX;
            u4IndexMax = CFA_MAX_OF_IF_INDEX;
            break;
#endif /*OPENFLOW_WANTED */

#ifdef VXLAN_WANTED
            /* For VXLAN-NVE interface */
        case CFA_VXLAN_NVE:
            u4IndexMin = CFA_MIN_NVE_IF_INDEX;
            u4IndexMax = CFA_MAX_NVE_IF_INDEX;
            break;
#endif
#ifdef HDLC_WANTED
        case CFA_HDLC:
            /* For Openflow vlan Interface */
            u4IndexMin = CFA_MIN_HDLC_INDEX;
            u4IndexMax = CFA_MAX_HDLC_INDEX;
            break;
#endif
#ifdef VCPEMGR_WANTED
        case CFA_TAP:
            /* For TAP interface */
            u4IndexMin = CFA_MIN_TAP_IF_INDEX;
            u4IndexMax = CFA_MAX_TAP_IF_INDEX;
            break;
#endif
        default:
            /* unknown interface type */
            u4IndexMin = 0;
            u4IndexMax = 0;
            CFA_DS_UNLOCK ();
            return (OSIX_FAILURE);

    }

    if (u4IndexMax < u4IndexMin)
    {
        /* Base code change done for EVB purpose
         * because PW Interface index pool is zero
         * and s-channel interface index pool also
         * starts with the same ID  */
        u4IndexMin = 0;
        u4IndexMax = 0;
        CFA_DS_UNLOCK ();
        return (OSIX_FAILURE);
    }
    /* Flexible ifIndex changes ===
       to support free ifindex being returned within the
       required range */

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IndexMin);

    if (NULL == pCfaIfInfo)
    {
        *pu4Index = u4IndexMin;
        CFA_DS_UNLOCK ();
        return (OSIX_SUCCESS);
    }
    else
    {
        u4LastIndex = u4IndexMin;
        u4Index = u4IndexMin;

        CFA_CDB_SCAN (u4Index, u4IndexMax, CFA_ALL_IFTYPE)
        {
            if (u4Index > u4IndexMax)
            {
                break;
            }

            if (CFA_CDB_IS_INTF_VALID (u4Index) != CFA_TRUE)
            {
                CFA_CDB_IF_DB_SET_INTF_VALID (u4Index, CFA_TRUE);
                u1IndexFound = CFA_TRUE;
                u1IdxFoundValid = CFA_TRUE;
                break;
            }
            u4LastIndex = u4Index;
        }
    }

    if ((u4LastIndex != u4IndexMax) && (CFA_TRUE != u1IdxFoundValid))
    {
        u4LastIndex++;
        u4Index = u4LastIndex;
        u1IndexFound = CFA_TRUE;
    }

    if (CFA_TRUE == u1IndexFound)
    {
        *pu4Index = u4Index;
        CFA_DS_UNLOCK ();
        return (OSIX_SUCCESS);
    }

    CFA_DS_UNLOCK ();
    return (OSIX_FAILURE);
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetFreeVipInterfaceIndex
 *
 *    Description         : This function returns the first available
 *                          free VIP interface index
 *
 *    Input(s)            : None
 *
 *    Output(s)           : pu4Index - Pointer to interface index
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : OSIX_SUCCESS if free VIP index is available
 *                         otherwise OSIX_FAILURE.
 *
 *****************************************************************************/
UINT4
CfaGetFreeVipInterfaceIndex (UINT4 *pu4Index)
{
    UINT4               u4Index;
    UINT4               u4IndexMin = 0;
    UINT4               u4IndexMax = 0;
    tCfaIfInfo         *pCfaIfInfo = NULL;
    UINT1               u1IndexFound = CFA_FALSE;
    UINT1               u1IdxFoundValid = CFA_FALSE;
    UINT4               u4LastIndex = 0;

    /* Calling function takes the CDB lock 
     */

    u4IndexMin = CFA_MIN_VIP_IF_INDEX;
    u4IndexMax = CFA_MAX_VIP_IF_INDEX;

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IndexMin);

    if (NULL == pCfaIfInfo)
    {
        *pu4Index = u4IndexMin;
        return (OSIX_SUCCESS);
    }
    else
    {
        u4LastIndex = u4IndexMin;
        u4Index = u4IndexMin;

        CFA_CDB_SCAN (u4Index, u4IndexMax, CFA_ALL_IFTYPE)
        {
            if (CFA_CDB_IS_INTF_VALID (u4Index) != CFA_TRUE)
            {
                CFA_CDB_IF_DB_SET_INTF_VALID (u4Index, CFA_TRUE);
                u1IndexFound = CFA_TRUE;
                u1IdxFoundValid = CFA_TRUE;
                break;
            }
            u4LastIndex = u4Index;
            u4LastIndex = u4Index;
        }
    }

    if ((u4LastIndex != u4IndexMax) && (CFA_TRUE != u1IdxFoundValid))
    {
        u4LastIndex++;
        u4Index = u4LastIndex;
        u1IndexFound = CFA_TRUE;
    }

    if (CFA_TRUE == u1IndexFound)
    {
        *pu4Index = u4Index;
        return (OSIX_SUCCESS);
    }

    return (OSIX_FAILURE);
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetInterfaceIndexFromName
 *
 *    Description         : This function retrieves the interface index
 *                          for the given interface name.
 *
 *    Input(s)            : pu1Alias - Pointer to interface alias name
 *                          pu4Index - Pointer to interface index
 *
 *    Output(s)           : pu4Index - Pointer to interface index
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : OSIX_SUCCESS if alias name found in the interface
 *                         table 
 *                         otherwise OSIX_FAILURE.
 *
 *****************************************************************************/
UINT4
CfaGetInterfaceIndexFromName (UINT1 *pu1Alias, UINT4 *pu4Index)
{
    UINT4               u4Index = 1;

    if (pu1Alias == NULL)
    {
        return (OSIX_FAILURE);
    }

    CFA_DS_LOCK ();

    CFA_CDB_SCAN (u4Index, BRG_MAX_PHY_PLUS_LAG_INT_PORTS, CFA_ALL_IFTYPE)
    {
        if (CFA_CDB_IS_INTF_VALID (u4Index) != CFA_TRUE)
        {
            continue;
        }
#ifdef LNXIP4_WANTED
        if ((CFA_CDB_IF_TYPE (u4Index) == CFA_ENET) &&
            (CFA_CDB_IF_BRIDGED_IFACE_STATUS (u4Index) != CFA_ENABLED) &&
            (STRCMP (CFA_GDD_PORT_NAME (u4Index), pu1Alias) == 0))
        {
            *pu4Index = u4Index;
            CFA_DS_UNLOCK ();
            return (OSIX_SUCCESS);
        }
#endif

#ifdef MPLS_WANTED
        if (STRSTR (pu1Alias, "mplstunnel") != NULL)
        {
            if (STRCMP (CFA_CDB_IF_ALIAS_NAME (u4Index), pu1Alias) == 0)
            {
                /* Matching L3 IP VLAN entry found */
                *pu4Index = u4Index;
                CFA_DS_UNLOCK ();
                return (OSIX_SUCCESS);
            }
        }
        else
        {
#endif
            if (STRCMP (CFA_CDB_IF_ALIAS (u4Index), pu1Alias) == 0)
            {
                /* Matching L3 IP VLAN entry found */
                *pu4Index = u4Index;
                CFA_DS_UNLOCK ();
                return (OSIX_SUCCESS);
            }
#ifdef MPLS_WANTED
        }
#endif
    }
    if (STRSTR (pu1Alias, CFA_OPENFLOW_VLAN_ALIAS_PREFIX) != NULL)
    {
        /* Check for openflow vlan interface */
        u4Index = CFA_MIN_OF_IF_INDEX;
        CFA_CDB_SCAN (u4Index, CFA_MAX_OF_IF_INDEX, CFA_ALL_IFTYPE)
        {
            if (CFA_CDB_IS_INTF_VALID (u4Index) != CFA_TRUE)
            {
                continue;
            }
            if (STRCMP (CFA_CDB_IF_ALIAS (u4Index), pu1Alias) == 0)
            {
                /* Matching Openflow VLAN entry found */
                *pu4Index = u4Index;
                CFA_DS_UNLOCK ();
                return (OSIX_SUCCESS);
            }
        }
    }

    CFA_DS_UNLOCK ();
    return (OSIX_FAILURE);
}

/*****************************************************************************
 *
 *    Function Name       : CfaIsAliasNameSet
 *
 *    Description         : This function checks if interface alias name is 
 *                          set for the given interaface.
 *
 *    Input(s)            : u4IfIndex - The interface index.          
 *
 *    Output(s)           : None
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : OSIX_TRUE is alias name is set                       
 *                         otherwise OSIX_FALSE.
 *
 *****************************************************************************/
INT4
CfaIsAliasNameSet (UINT4 u4IfIndex)
{
    UINT1               au1IfName[CFA_MAX_IFALIAS_LENGTH];

    MEMSET (au1IfName, 0, CFA_MAX_IFALIAS_LENGTH);
    if (CfaGetIfName (u4IfIndex, au1IfName) == CFA_SUCCESS)
    {
        if (au1IfName[0] == '\0')
        {
            return OSIX_FALSE;
        }
    }
    return OSIX_TRUE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetInterfaceIndexFromNameInCxt
 *
 *    Description         : This function retrieves the interface index
 *                          for the given interface name.
 *
 *    Input(s)            : pu1Alias - Pointer to interface alias name
 *                          pu4Index - Pointer to interface index
 *
 *    Output(s)           : pu4Index - Pointer to interface index
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : OSIX_SUCCESS if alias name found in the interface
 *                         table 
 *                         otherwise OSIX_FAILURE.
 *
 *****************************************************************************/
UINT4
CfaGetInterfaceIndexFromNameInCxt (UINT4 u4L2ContextId, UINT1 *pu1Alias,
                                   UINT4 *pu4Index)
{
    UINT4               u4Index = 1;
    UINT4               u4IfL2CxtId;

    if (pu1Alias == NULL)
    {
        return (OSIX_FAILURE);
    }

    CFA_DS_LOCK ();

    CFA_CDB_SCAN (u4Index, SYS_DEF_MAX_INTERFACES, CFA_ALL_IFTYPE)
    {
        /* Flexible ifIndex changes 
         */
        if (((CFA_CDB_IF_TYPE (u4Index) == CFA_PROP_VIRTUAL_INTERFACE) &&
             (CFA_CDB_IF_SUB_TYPE (u4Index) == CFA_SUBTYPE_SISP_INTERFACE)) ||
            (CFA_CDB_IF_BRIDGE_PORT_TYPE (u4Index) ==
             CFA_VIRTUAL_INSTANCE_PORT))
        {
            continue;
        }

        if (CFA_CDB_IS_INTF_VALID (u4Index) != CFA_TRUE)
        {
            continue;
        }
#ifdef LNXIP4_WANTED
        if ((CFA_CDB_IF_TYPE (u4Index) == CFA_ENET) &&
            (CFA_CDB_IF_BRIDGED_IFACE_STATUS (u4Index) != CFA_ENABLED) &&
            (STRCMP (CFA_GDD_PORT_NAME (u4Index), pu1Alias) == 0))
        {
            *pu4Index = u4Index;
            CFA_DS_UNLOCK ();
            return (OSIX_SUCCESS);
        }
#endif

        if (STRCMP (CFA_CDB_IF_ALIAS (u4Index), pu1Alias) == 0)
        {
            if ((CFA_CDB_IF_TYPE (u4Index) == CFA_L3IPVLAN) &&
                (STRNCASECMP (pu1Alias, CFA_L3IPVLAN_NAME_PREFIX,
                              STRLEN (CFA_L3IPVLAN_NAME_PREFIX)) == 0))
            {
                /* This is an L3 Vlan Interface  
                 * Alias name of L3IP VLAN interface is not unique.
                 * Validate the underlying L2 VLAN contextId
                 */

                /* Going to take the Lock of Other Module. (VCM_LOCK).
                 * So releasing my Lock here..
                 * To Avoid Dead Lock Scenario
                 */
                CFA_DS_UNLOCK ();
                if ((VcmGetL2CxtIdForIpIface (u4Index,
                                              &u4IfL2CxtId) == VCM_SUCCESS)
                    && (u4IfL2CxtId == u4L2ContextId))
                {
                    /* Matching L3 IP VLAN entry found */
                    *pu4Index = u4Index;
                    return (OSIX_SUCCESS);
                }
                /* Taking My lock Again */
                CFA_DS_LOCK ();
            }
            else
            {
                *pu4Index = u4Index;
                CFA_DS_UNLOCK ();
                return (OSIX_SUCCESS);
            }
        }
    }

    CFA_DS_UNLOCK ();
    return (OSIX_FAILURE);
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetInterfaceNameFromIndex
 *
 *    Description         : This function retrieves the interface name 
 *                          for the given interface index.
 *
 *    Input(s)            : u4Index - Interface Index
 *                          pu1Alias - Pointer to interface alias name
 *
 *
 *    Output(s)           : puAlias - Pointer to interface alias name
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : OSIX_SUCCESS if interface is valid
 *                         otherwise OSIX_FAILURE.
 *
 *****************************************************************************/
UINT4
CfaGetInterfaceNameFromIndex (UINT4 u4Index, UINT1 *pu1Alias)
{
    if (CfaGetIfName (u4Index, pu1Alias) == CFA_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    else
    {
        return OSIX_FAILURE;
    }
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetIvrStatus
 *
 *    Description         : This function returns the IVR status 
 *
 *    Input(s)            : None. 
 *
 *    Output(s)           : None.
 *
 *    Global Variables Referred : gu4IsIvrEnabled.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :  CFA_ENABLED - If IVR is enabled.
 *                          CFA_DISABLED - If IVR is disabled.
 *****************************************************************************/

UINT1
CfaGetIvrStatus ()
{
    return ((UINT1) gu4IsIvrEnabled);
}

/*****************************************************************************
 *
 *    Function Name       : CfaCliGetIfName
 *
 *    Description         : This function is used to get the Interface name 
 *                          for given interface index.
 *
 *    Input(s)            : Interface index
 *
 *
 *    Output(s)           : pi1IfName - Interface name of the given 
 *                          interface index.
 *
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion    : None.
 *
 *    Returns             : CLI_SUCCESS or CLI_FAILURE.
 *****************************************************************************/
INT4
CfaCliGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    INT4                i4Status = CLI_SUCCESS;
    UINT4               u4L2CxtId = 0;
    UINT1               u1IfType;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4ParentIndex = 0;
    if (!(pi1IfName))
    {
        return CLI_FAILURE;
    }

    if (u4IfIndex == 0)
    {
        CLI_STRNCPY (pi1IfName, "0", STRLEN ("0"));
        pi1IfName[STRLEN ("0")] = '\0';
        return CLI_SUCCESS;
    }

    if (CfaIsMgmtPort (u4IfIndex) == TRUE)
    {
        STRNCPY (au1IfName, OOB_ALIAS_NAME,
                 sizeof (au1IfName) - CFA_STR_DELIM_SIZE);
        au1IfName[sizeof (au1IfName) - 1] = '\0';
        SNPRINTF ((CHR1 *) pi1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s",
                  au1IfName);
        return CLI_SUCCESS;
    }

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CLI_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_FALSE)
    {
        CFA_DS_UNLOCK ();
        return CLI_FAILURE;
    }

    u1IfType = CFA_CDB_IF_TYPE (u4IfIndex);
    STRNCPY (au1IfName, CFA_CDB_IF_ALIAS (u4IfIndex),
             sizeof (au1IfName) - CFA_STR_DELIM_SIZE);
    au1IfName[sizeof (au1IfName) - 1] = '\0';

    CFA_DS_UNLOCK ();

    /* Process ifType and form the IfName */
    switch (u1IfType)
    {
            /* Need to port the function CfaGetPhysIfName 
             * for different Interface Names, if supported 
             * that may be based on some parameters of that interface
             */
        case CFA_OTHER:
        case CFA_ENET:
            if (CfaGetPhysIfName (u4IfIndex, pi1IfName) == CLI_FAILURE)
            {
                i4Status = CLI_FAILURE;
            }
            break;

        case CFA_L3SUB_INTF:
            CfaGetL3SubIfParentIndex (u4IfIndex, &u4ParentIndex);
            if ((u4ParentIndex >= (SYS_DEF_MAX_PHYSICAL_INTERFACES + 1)) &&
                (u4ParentIndex <=
                 (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG)))
            {
                SNPRINTF ((CHR1 *) pi1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s",
                          au1IfName);
                break;
            }

            if (CfaGetL3SubIfName (u4IfIndex, pi1IfName) == CFA_FAILURE)
            {
                i4Status = CLI_FAILURE;
            }
            break;

        case CFA_L3IPVLAN:

            if (VcmGetL2CxtIdForIpIface ((UINT4) u4IfIndex, &u4L2CxtId)
                == VCM_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (u4L2CxtId == VCM_DEFAULT_CONTEXT)
            {
                SNPRINTF ((CHR1 *) pi1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s",
                          au1IfName);
                break;
            }

            i4Status = CfaCliGetIVRNameFromIfNameAndCxtId (u4L2CxtId, au1IfName,
                                                           (UINT1 *) pi1IfName);
            break;
        case CFA_LOOPBACK:
        case CFA_L2VLAN:
            SNPRINTF ((CHR1 *) pi1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s",
                      au1IfName);
            break;
        case CFA_LAGG:
        case CFA_PIP:
        case CFA_BRIDGED_INTERFACE:
        case CFA_ILAN:
        case CFA_PROP_VIRTUAL_INTERFACE:
            /* Intentional Fall through */
            SNPRINTF ((CHR1 *) pi1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s",
                      au1IfName);
            break;

        case CFA_PPP:
        case CFA_TUNNEL:
        case CFA_MPLS:
        case CFA_MPLS_TUNNEL:
        case CFA_VPNC:
        case CFA_TELINK:
        case CFA_PSEUDO_WIRE:
#ifdef HDLC_WANTED
        case CFA_HDLC:
#endif
#ifdef VXLAN_WANTED
        case CFA_VXLAN_NVE:
#endif
#ifdef VCPEMGR_WANTED
        case CFA_TAP:
#endif

            SNPRINTF ((CHR1 *) pi1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s",
                      au1IfName);
            break;

#if defined (WLC_WANTED) || defined (WTP_WANTED)
        case CFA_RADIO:
        case CFA_WLAN_RADIO:
        case CFA_CAPWAP_VIRT_RADIO:
        case CFA_CAPWAP_DOT11_PROFILE:
        case CFA_CAPWAP_DOT11_BSS:
            SNPRINTF ((CHR1 *) pi1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s",
                      au1IfName);
            break;
#endif

        default:
            i4Status = CLI_FAILURE;
    }

    return i4Status;
}

/*****************************************************************************
 *
 *    Function Name       : CfaCliConfGetIfName
 *
 *    Description         : This function returns Interface name for specified
 *                          interface
 *
 *    Input(s)            : u4IfIndex - Interface Index 
 *
 *    Output(s)           : pi1IfName - Pointer to buffer
 *
 *
 *    Returns            : CLI_SUCCESS if name assigned for pi1IfName
 *                         CLI_FAILURE if name is not assign pi1IfName
 *                         CFA_FAILURE if u4IfIndex is not valid interface
 *
 *****************************************************************************/
INT4
CfaCliConfGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    UINT1               u1IfType;
    UINT1               u1SubType = 0;
    UINT1               u1EtherType = CFA_ENET_UNKNOWN;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1VlanSwitchName[CFA_CLI_MAX_IF_NAME_LEN];
    INT4                i4Status = CLI_SUCCESS;

    if (!(pi1IfName))
    {
        return CLI_FAILURE;
    }

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_FALSE)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    u1IfType = CFA_CDB_IF_TYPE (u4IfIndex);
    u1SubType = CFA_CDB_IF_SUB_TYPE (u4IfIndex);
    STRNCPY (au1IfName, CFA_CDB_IF_ALIAS (u4IfIndex),
             sizeof (au1IfName) - CFA_STR_DELIM_SIZE);
    au1IfName[sizeof (au1IfName) - 1] = '\0';

    CFA_DS_UNLOCK ();
    /* Process ifType and form the IfName */
    switch (u1IfType)
    {
            /* Need to port the function CfaGetPhysIfName 
             * for different Interface Names, if supported 
             * that may be based on some parameters of that interface
             */

        case CFA_PPP:
            STRNCPY (pi1IfName, "ppp ", CFA_MAX_PORT_NAME_LENGTH);
            pi1IfName[STRLEN ("ppp ")] = '\0';
            STRNCAT (pi1IfName + STRLEN (pi1IfName),
                     au1IfName + STRLEN (pi1IfName) - 1,
                     CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) - 1);
            break;

        case CFA_ENET:
            if (CfaIsMgmtPort (u4IfIndex) == TRUE)
            {
                STRNCPY (au1IfName, OOB_ALIAS_NAME,
                         sizeof (au1IfName) - CFA_STR_DELIM_SIZE);
                au1IfName[sizeof (au1IfName) - 1] = '\0';
                SNPRINTF ((CHR1 *) pi1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s",
                          au1IfName);
                i4Status = CFA_SUCCESS;
            }
            else if (CfaGetPhysIfName (u4IfIndex,
                                       (INT1 *) au1IfName) == CLI_SUCCESS)
            {
                CfaGetEthernetType (u4IfIndex, &u1EtherType);
                if (u1EtherType == CFA_ENET_UNKNOWN)
                {
                    STRNCPY (pi1IfName, "Unknown", CFA_MAX_PORT_NAME_LENGTH);
                    pi1IfName[STRLEN ("Unknown")] = '\0';
                }
#ifdef CFA_UNIQUE_INTF_NAME
                if (u1EtherType == CFA_GI_ENET)
                {
                    STRNCPY (pi1IfName, "ethernet ", CFA_MAX_PORT_NAME_LENGTH);
                    pi1IfName[STRLEN ("ethernet ")] = '\0';
                    STRNCAT (pi1IfName + STRLEN (pi1IfName), au1IfName + 2,
                             CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) - 1);
                }
#else
                if (u1EtherType == CFA_GI_ENET)
                {
                    STRNCPY (pi1IfName, "gigabitethernet ",
                             CFA_MAX_PORT_NAME_LENGTH);
                    pi1IfName[STRLEN ("gigabitethernet ")] = '\0';
                    STRNCAT (pi1IfName + STRLEN (pi1IfName), au1IfName + 2,
                             CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) - 1);
                }
#endif
                else if (u1EtherType == CFA_FA_ENET)
                {
                    STRNCPY (pi1IfName, "fastethernet ",
                             CFA_MAX_PORT_NAME_LENGTH);
                    pi1IfName[STRLEN ("fastethernet ")] = '\0';
                    STRNCAT (pi1IfName + STRLEN (pi1IfName), au1IfName + 2,
                             CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) - 1);
                }
                else if (u1EtherType == CFA_XE_ENET)
                {
                    STRNCPY (pi1IfName, "extreme-ethernet ",
                             CFA_MAX_PORT_NAME_LENGTH);
                    pi1IfName[STRLEN ("extreme-ethernet ")] = '\0';
                    STRNCAT (pi1IfName + STRLEN (pi1IfName),
                             au1IfName + 2,
                             CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) - 1);
                }
                else if (u1EtherType == CFA_XL_ENET)
                {
                    STRNCPY (pi1IfName, "XL-ethernet ",
                             CFA_MAX_PORT_NAME_LENGTH);
                    pi1IfName[STRLEN ("XL-ethernet ")] = '\0';
                    STRNCAT (pi1IfName + STRLEN (pi1IfName),
                             au1IfName + 2,
                             CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) - 1);
                }
                else if (u1EtherType == CFA_LVI_ENET)
                {
                    STRNCPY (pi1IfName, "LVI-ethernet ",
                             CFA_MAX_PORT_NAME_LENGTH);
                    pi1IfName[STRLEN ("LVI-ethernet ")] = '\0';
                    STRNCAT (pi1IfName + STRLEN (pi1IfName), au1IfName + 2,
                             CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) - 1);
                }
            }
            else
            {
                i4Status = CLI_FAILURE;
            }
            break;

        case CFA_L3SUB_INTF:
            if (CfaGetL3SubIfName (u4IfIndex,
                                   (INT1 *) au1IfName) == CLI_SUCCESS)
            {
                if (STRNCMP (au1IfName, "po", 2) == 0)
                {
                    STRNCPY (pi1IfName, "port-channel 0/",
                             CFA_MAX_PORT_NAME_LENGTH);
                    pi1IfName[STRLEN ("port-channel 0/")] = '\0';
                    STRNCAT (pi1IfName + STRLEN (pi1IfName), au1IfName + 2,
                             CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) - 1);
                }
                else
                {
                    CfaGetEthernetType (u4IfIndex, &u1EtherType);
                    if (u1EtherType == CFA_ENET_UNKNOWN)
                    {
                        STRNCPY (pi1IfName, "Unknown",
                                 CFA_MAX_PORT_NAME_LENGTH);
                        pi1IfName[STRLEN ("Unknown")] = '\0';
                    }
                    if (u1EtherType == CFA_GI_ENET)
                    {
                        STRNCPY (pi1IfName, "gigabitethernet ",
                                 CFA_MAX_PORT_NAME_LENGTH);
                        pi1IfName[STRLEN ("gigabitethernet ")] = '\0';
                        STRNCAT (pi1IfName + STRLEN (pi1IfName), au1IfName + 2,
                                 CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) -
                                 1);
                    }
                    else if (u1EtherType == CFA_FA_ENET)
                    {
                        STRNCPY (pi1IfName, "fastethernet ",
                                 CFA_MAX_PORT_NAME_LENGTH);
                        pi1IfName[STRLEN ("fastethernet ")] = '\0';
                        STRNCAT (pi1IfName + STRLEN (pi1IfName), au1IfName + 2,
                                 CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) -
                                 1);
                    }
                    else if (u1EtherType == CFA_XE_ENET)
                    {
                        STRNCPY (pi1IfName, "extreme-ethernet ",
                                 CFA_MAX_PORT_NAME_LENGTH);
                        pi1IfName[STRLEN ("extreme-ethernet ")] = '\0';
                        STRNCAT (pi1IfName + STRLEN (pi1IfName),
                                 au1IfName + 2,
                                 CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) -
                                 1);
                    }
                    else if (u1EtherType == CFA_XL_ENET)
                    {
                        STRNCPY (pi1IfName, "XL-ethernet ",
                                 CFA_MAX_PORT_NAME_LENGTH);
                        pi1IfName[STRLEN ("XL-ethernet ")] = '\0';
                        STRNCAT (pi1IfName + STRLEN (pi1IfName),
                                 au1IfName + 2,
                                 CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) -
                                 1);
                    }
                    else if (u1EtherType == CFA_LVI_ENET)
                    {
                        STRNCPY (pi1IfName, "LVI-ethernet ",
                                 CFA_MAX_PORT_NAME_LENGTH);
                        pi1IfName[STRLEN ("LVI-ethernet ")] = '\0';
                        STRNCAT (pi1IfName + STRLEN (pi1IfName), au1IfName + 2,
                                 CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) -
                                 1);
                    }
                }
            }
            else
            {
                i4Status = CLI_FAILURE;
            }
            break;

        case CFA_L3IPVLAN:
        case CFA_L2VLAN:
            if (CfaIsLinuxVlanIntf (u4IfIndex) == TRUE)
            {
                /* to print as "linuxvlan ethx.y" */
                STRNCPY (pi1IfName, "linuxvlan ", CFA_MAX_PORT_NAME_LENGTH);
                pi1IfName[STRLEN ("linuxvlan ")] = '\0';
                STRNCAT (pi1IfName + STRLEN (pi1IfName), au1IfName,
                         CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) - 1);
            }
            else
            {
                STRNCPY (pi1IfName, "vlan ", CFA_MAX_PORT_NAME_LENGTH);
                pi1IfName[STRLEN ("vlan ")] = '\0';
                STRNCAT (pi1IfName + STRLEN (pi1IfName),
                         au1IfName + 4,
                         CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) - 1);
            }
            if (u1IfType == CFA_L3IPVLAN)
            {
                MEMSET (au1VlanSwitchName, 0, CFA_CLI_MAX_IF_NAME_LEN);
                if (VcmCliGetL2CxtNameForIpInterface (u4IfIndex,
                                                      au1VlanSwitchName) ==
                    VCM_SUCCESS)
                {
                    STRNCPY (pi1IfName + STRLEN (pi1IfName), " switch ",
                             CFA_MAX_PORT_NAME_LENGTH);
                    pi1IfName[STRLEN (" switch ")] = '\0';
                    STRNCAT (pi1IfName + STRLEN (pi1IfName),
                             au1VlanSwitchName,
                             CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) - 1);
                }
            }
            break;

        case CFA_LOOPBACK:
            STRNCPY (pi1IfName, "loopback ", CFA_MAX_PORT_NAME_LENGTH);
            pi1IfName[STRLEN ("loopback ")] = '\0';
            STRNCAT (pi1IfName + STRLEN (pi1IfName), au1IfName + 8,
                     CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) - 1);
            break;

        case CFA_LAGG:
            STRNCPY (pi1IfName, "port-channel ", CFA_MAX_PORT_NAME_LENGTH);
            pi1IfName[STRLEN ("port-channel ")] = '\0';
            STRNCAT (pi1IfName + STRLEN (pi1IfName), au1IfName + 2,
                     CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) - 1);

            break;
        case CFA_TUNNEL:
            STRNCPY (pi1IfName, "tunnel ", CFA_MAX_PORT_NAME_LENGTH);
            pi1IfName[STRLEN ("tunnel ")] = '\0';
            STRNCAT (pi1IfName + STRLEN (pi1IfName), au1IfName + 6,
                     CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) - 1);

            break;

        case CFA_PSEUDO_WIRE:
            STRNCPY (pi1IfName, "pw ", STRLEN ("pw "));
            pi1IfName[STRLEN ("pw ")] = '\0';
            STRCAT (pi1IfName + STRLEN (pi1IfName), au1IfName + 2);
            break;

#ifdef VXLAN_WANTED
        case CFA_VXLAN_NVE:
            STRNCPY (pi1IfName, "nve ", STRLEN ("nve "));
            pi1IfName[STRLEN ("nve ")] = '\0';
            STRCAT (pi1IfName + STRLEN (pi1IfName),
                    au1IfName + CFA_MAX_VXLAN_PORT_NAME_LENGTH);
            break;
#endif

        case CFA_PIP:
        case CFA_BRIDGED_INTERFACE:
            if (CfaIsSbpInterface (u4IfIndex) == CFA_TRUE)
            {
                STRNCPY (pi1IfName, "s-channel ", CFA_MAX_PORT_NAME_LENGTH);
                pi1IfName[STRLEN ("s-channel ")] = '\0';
                STRNCAT (pi1IfName + STRLEN (pi1IfName), au1IfName + 9,
                         CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) - 1);
                break;
            }
            STRNCPY (pi1IfName, "virtual ", CFA_MAX_PORT_NAME_LENGTH);
            pi1IfName[STRLEN ("virtual ")] = '\0';
            STRNCAT (pi1IfName + STRLEN (pi1IfName), au1IfName + 7,
                     CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) - 1);
            break;

        case CFA_PROP_VIRTUAL_INTERFACE:
            if (u1SubType == CFA_SUBTYPE_SISP_INTERFACE)
            {
                STRNCPY (pi1IfName, "sisp ", CFA_MAX_PORT_NAME_LENGTH);
                pi1IfName[STRLEN ("sisp ")] = '\0';
                STRNCAT (pi1IfName + STRLEN (pi1IfName), au1IfName + 4,
                         CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) - 1);
            }
            else if (u1SubType == CFA_SUBTYPE_AC_INTERFACE)
            {
                STRNCPY (pi1IfName, "ac ", CFA_MAX_PORT_NAME_LENGTH);
                pi1IfName[STRLEN ("ac ")] = '\0';
                STRNCAT (pi1IfName + STRLEN (pi1IfName), au1IfName + 2,
                         CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) - 1);
                SPRINTF ((CHR1 *) pi1IfName, "%s", au1IfName);
            }
            break;

        case CFA_HDLC:
            STRNCPY (pi1IfName, "serial ", CFA_MAX_PORT_NAME_LENGTH);
            pi1IfName[STRLEN ("serial ")] = '\0';
            STRNCAT (pi1IfName + STRLEN (pi1IfName),
                     au1IfName + STRLEN (pi1IfName) - 1,
                     CFA_MAX_PORT_NAME_LENGTH - STRLEN (pi1IfName) - 1);
            break;
#ifdef VCPEMGR_WANTED
        case CFA_TAP:
            STRNCPY (pi1IfName, "tap ", STRLEN ("tap "));
            pi1IfName[STRLEN ("tap ")] = '\0';
            STRCAT (pi1IfName + STRLEN (pi1IfName),
                    au1IfName + CFA_MAX_TAP_PORT_NAME_LENGTH);
            break;
#endif
        default:
            i4Status = CLI_FAILURE;
    }

    return i4Status;
}

/*****************************************************************************
 *
 *    Function Name       : CfaCliGetL3SubIfIndex    
 *
 *    Description         : This function is used to get the Interface index 
 *                          of L3 Sub-interface, given the interface name
 *                          and interface number.
 *
 *    Input(s)            : Interface name and interface number
 *
 *
 *    Output(s)           : pu4IfIndex - IfIndex value of the given 
 *                          interface number
 *
 *    Returns            : CLI_SUCCESS or CLI_FAILURE.
 *****************************************************************************/

INT4
CfaCliGetL3SubIfIndex (INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex)
{
    INT4                i4PhyPort = 0;
    INT4                i4LPort = 0;
    INT4                i4SlotNum = 0;
    UINT1               au1IfName[IF_PREFIX_LEN];
    INT1                ai1IfNum[IF_PREFIX_LEN];

    MEMSET (ai1IfNum, 0, sizeof (ai1IfNum));

    if (STRNCMP (pi1IfName, "po", 2) != 0)
    {

        /* Get the Physical port and logical port from the l3 sub interface port */
        if (CfaCliGetPhysicalAndLogicalPortNum (pi1IfNum, &i4PhyPort, &i4LPort,
                                                &i4SlotNum) == CFA_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                     "In CfaCliGetL3SubIfIndex :CfaCliGetPhysicalAndLogicalPortNum Failure");
            return CLI_FAILURE;
        }

        SPRINTF ((CHR1 *) ai1IfNum, "%d/%d", i4SlotNum, i4PhyPort);
        if (CfaCliGetIfIndex (pi1IfName, ai1IfNum, pu4IfIndex) == CLI_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                     "In CfaCliGetL3SubIfIndex :CfaCliGetIfIndex failed");
            return CLI_FAILURE;
        }

        MEMSET (au1IfName, 0, IF_PREFIX_LEN);
        SPRINTF ((CHR1 *) au1IfName, "Slot%d/%d.%d", i4SlotNum, i4PhyPort,
                 i4LPort);

        /* Get the L3SubInterface Index from Interface alias name */
        if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) ==
            OSIX_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                     "In CfaCliGetL3SubIfIndex: CfaGetInterfaceIndexFromName Failure");
            return CLI_FAILURE;
        }
    }
    else
    {
        if (CfaCliGetPhysicalAndLogicalPortNum (pi1IfNum, &i4PhyPort, &i4LPort,
                                                &i4SlotNum) == CFA_FAILURE)
        {

            CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                     "In CfaCliGetL3SubIfIndex :CfaCliGetPhysicalAndLogicalPortNum Failure");
            return CLI_FAILURE;
        }

        MEMSET (au1IfName, 0, IF_PREFIX_LEN);
        SPRINTF ((CHR1 *) au1IfName, "po%d.%d", i4PhyPort, i4LPort);

        if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) ==
            OSIX_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                     "In CfaCliGetL3SubIfIndex: CfaGetInterfaceIndexFromName Failure");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaCliGetIfIndex     
 *
 *    Description         : This function is used to get the Interface index 
 *                          a particular interface, given the interface name
 *                          and interface number.
 *                          This function is used by protocols which does not 
 *                          support Multiple instance (or Multi VRF)
 *
 *    Input(s)            : Interface name and interface number
 *
 *
 *    Output(s)           : pu4IfIndex - IfIndex value of the given 
 *                          interface number
 *
 *    Global Variables Referred : asCfaIfaces (supported Interface types) 
 *                                structure.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CLI_SUCCESS or CLI_FAILURE.
 *****************************************************************************/

INT4
CfaCliGetIfIndex (INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex)
{

    UINT1               au1L2DefaultSwName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1L2DefaultSwName, 0, VCM_ALIAS_MAX_LEN);
    STRNCPY (au1L2DefaultSwName, "default",
             sizeof (au1L2DefaultSwName) - CFA_STR_DELIM_SIZE);
    au1L2DefaultSwName[sizeof (au1L2DefaultSwName) - 1] = '\0';

    return (CfaCliGetIfIndexInCxt (au1L2DefaultSwName, pi1IfName, pi1IfNum,
                                   pu4IfIndex));
}

/*****************************************************************************
 *
 *    Function Name       : CfaCliGetIfIndexInCxt     
 *
 *    Description         : This function is used to get the Interface index 
 *                          a particular interface, given the interface name
 *                          and interface number.
 *
 *    Input(s)            : L2 switch name (context name)
 *                          Interface name and interface number
 *
 *
 *    Output(s)           : pu4IfIndex - IfIndex value of the given 
 *                          interface number
 *
 *    Global Variables Referred : asCfaIfaces (supported Interface types) 
 *                                structure.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CLI_SUCCESS or CLI_FAILURE.
 *****************************************************************************/

INT4
CfaCliGetIfIndexInCxt (UINT1 *pu1L2SwitchName, INT1 *pi1IfName,
                       INT1 *pi1IfNum, UINT4 *pu4IfIndex)
{
    INT4                i4Index;
    INT4                i4Len = 0;
    INT4                i4StrLen = 0;
    INT4                i4IfTypeLen = 0;
    INT4                i4MatchIfType = -1;
    INT4                i4MatchIndex = -1;
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4SubCount = -1;
    INT4                i4MatchSubIndex = -1;
    UINT4               u4IfNum = 0;
    UINT4               u4L2ContextId;
    BOOL1               bIsIfTypeConflict = OSIX_FALSE;
    UINT1               au1IfName[IF_PREFIX_LEN];
    INT4                i4SlotNum;
#ifdef HDLC_WANTED
    INT4                i4ParentIndex = 0;
    UINT4               u4SecondaryIndex = 0;
#endif

    if (!(pi1IfName) || !(*pi1IfName) || !(pi1IfNum))
    {
        return CLI_FAILURE;
    }

    i4StrLen = (INT4) STRLEN (pi1IfName);
    /* Check for longest match with  any interface type's available */

    for (i4Index = 0; i4Index < CLI_MAX_IFTYPES; i4Index++)
    {
        for (i4SubCount = 0; i4SubCount < CLI_MAX_IFSUBTYPES; i4SubCount++)
        {

            i4IfTypeLen =
                (INT4) STRLEN (asCfaIfaces[i4Index][i4SubCount].au1IfName);

            i4Len = (i4StrLen < i4IfTypeLen) ? (i4StrLen) : (i4IfTypeLen);

            if (i4Len == 0)
            {
                continue;
            }
            if (STRNCASECMP (asCfaIfaces[i4Index][i4SubCount].au1IfName,
                             pi1IfName, i4Len) == 0)
            {
                if (i4MatchIfType != -1)
                {
                    bIsIfTypeConflict = OSIX_TRUE;
                    break;
                }
                i4MatchIfType =
                    (INT4) asCfaIfaces[i4Index][i4SubCount].u4IfType;
                i4MatchIndex = i4Index;
                i4MatchSubIndex = i4SubCount;
            }
        }

        if (bIsIfTypeConflict == OSIX_TRUE)
        {
            break;
        }
    }

    if ((i4MatchIfType == -1) || (bIsIfTypeConflict == OSIX_TRUE))
    {
        return CLI_FAILURE;
    }

    /* if name shud be formed based on the match index or 
     * based on the match iftype
     * i.e, for fa, gi the interface name shud be mathced 
     * against the interface type and shud take the prefix as  ISS_ALIAS_PREFIX
     */
    STRNCPY (au1IfName, asCfaIfaces[i4MatchIndex][i4MatchSubIndex].au1IfName,
             sizeof (au1IfName) - CFA_STR_DELIM_SIZE);
    au1IfName[sizeof (au1IfName) - 1] = '\0';

    /* Process ifNum and get the interface index */
    switch (i4MatchIfType)
    {

            /* Intentional fall through, as we currently treat both these types as same */
        case CFA_LVI_ENET:
        case CFA_XL_ENET:
        case CFA_XE_ENET:
        case CFA_FA_ENET:
        case CFA_GI_ENET:
            if (CfaValidateIfNum
                ((UINT4) i4MatchIfType, pi1IfNum, &i4SlotNum,
                 &u4IfNum) != OSIX_SUCCESS)
            {
                return CLI_FAILURE;
            }

#ifdef MBSM_WANTED
            /* concatinate to form like:  Slot0/1 */
            SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d/%d",
                      ISS_ALIAS_PREFIX, i4SlotNum, u4IfNum);

            if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) ==
                OSIX_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
            }
#else
            *pu4IfIndex = u4IfNum;
#endif
            if (CfaValidateCfaIfIndex (*pu4IfIndex) == CFA_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
            }

            break;

        case CFA_L3IPVLAN:
            if (STRNCASECMP (pi1IfNum, CFA_LINUX_L3IPVLAN_NAME_PREFIX,
                             STRLEN (CFA_LINUX_L3IPVLAN_NAME_PREFIX)) == 0)
            {
                STRNCPY ((CHR1 *) au1IfName, pi1IfNum,
                         IF_PREFIX_LEN - CFA_STR_DELIM_SIZE);
                au1IfName[IF_PREFIX_LEN - 1] = '\0';
            }
            else
            {
                SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                          asCfaIfaces[i4MatchIndex][i4MatchSubIndex].au1IfName,
                          *(INT4 *) (VOID *) pi1IfNum);
            }
            if (STRNCASECMP (au1IfName, CFA_L3IPVLAN_NAME_PREFIX,
                             STRLEN (CFA_L3IPVLAN_NAME_PREFIX)) == 0)
            {
                i4RetVal = CLI_FAILURE;
                if ((pu1L2SwitchName) && (*pu1L2SwitchName))
                {

                    if ((VcmIsSwitchExist (pu1L2SwitchName,
                                           &u4L2ContextId) == VCM_TRUE) &&
                        (CfaGetInterfaceIndexFromNameInCxt (u4L2ContextId,
                                                            au1IfName,
                                                            pu4IfIndex) ==
                         OSIX_SUCCESS))
                    {
                        i4RetVal = CLI_SUCCESS;
                    }
                }
                else
                {
                    if (CfaGetInterfaceIndexFromNameInCxt (VCM_DEFAULT_CONTEXT,
                                                           au1IfName,
                                                           pu4IfIndex) ==
                        OSIX_SUCCESS)
                    {
                        i4RetVal = CLI_SUCCESS;
                    }

                }
            }
            else
            {
                if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) ==
                    OSIX_FAILURE)
                {
                    i4RetVal = CLI_FAILURE;
                }
            }
            break;
        case CFA_PIP:
        case CFA_BRIDGED_INTERFACE:
        case CFA_LAGG:
        case CFA_PROP_VIRTUAL_INTERFACE:
            if (i4MatchSubIndex == CFA_SUBTYPE_SISP_INTERFACE)
            {
                /* pi1IfNum is a string which contains the interface index */
                if (CfaValidateIfNum
                    ((UINT4) i4MatchIfType, pi1IfNum, &i4SlotNum,
                     &u4IfNum) != OSIX_SUCCESS)
                {
                    return CLI_FAILURE;
                }
                SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                          asCfaIfaces[i4MatchIndex][i4MatchSubIndex].au1IfName,
                          u4IfNum);
                if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) ==
                    OSIX_FAILURE)
                {
                    i4RetVal = CLI_FAILURE;
                }
            }
            if (i4MatchSubIndex == CFA_SUBTYPE_AC_INTERFACE)
            {
                if (CfaValidateIfNum
                    ((UINT4) i4MatchIfType, pi1IfNum, &i4SlotNum,
                     &u4IfNum) != OSIX_SUCCESS)
                {
                    return CLI_FAILURE;
                }
                SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                          asCfaIfaces[i4MatchIndex][i4MatchSubIndex].au1IfName,
                          ATOI (pi1IfNum));
                if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) ==
                    OSIX_FAILURE)
                {
                    i4RetVal = CLI_FAILURE;
                }
            }

            break;

        case CFA_TUNNEL:
        case CFA_LOOPBACK:
#ifdef MPLS_WANTED
        case CFA_MPLS_TUNNEL:
#endif
            /* pi1IfNum is a string which
             * contains the Tunnel index */
            SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                      asCfaIfaces[i4MatchIndex][i4MatchSubIndex].au1IfName,
                      *(INT4 *) (VOID *) pi1IfNum);
            if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) ==
                OSIX_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
            }

            break;

        case CFA_ILAN:
            /* pi1IfNum is a string which
             * contains the Internal interface index */
            SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                      asCfaIfaces[i4MatchIndex][i4MatchSubIndex].au1IfName,
                      ATOI (pi1IfNum));
            if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) ==
                OSIX_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
            }

            break;

#ifdef PPP_WANTED
        case CFA_PPP:

            SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                      asCfaIfaces[i4MatchIndex][i4MatchSubIndex].au1IfName,
                      *(INT4 *) (VOID *) pi1IfNum);

            if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) ==
                OSIX_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
            }
            break;

#endif /* PPP_WANTED */
        case CFA_PSEUDO_WIRE:
        case CFA_STATION_FACING_BRIDGE_PORT:
#ifdef VXLAN_WANTED
        case CFA_VXLAN_NVE:
#endif
#ifdef VCPEMGR_WANTED
        case CFA_TAP:
#endif
            if (CfaValidateIfNum
                ((UINT4) i4MatchIfType, pi1IfNum, &i4SlotNum,
                 &u4IfNum) != OSIX_SUCCESS)
            {
                return CLI_FAILURE;
            }
            SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                      asCfaIfaces[i4MatchIndex][i4MatchSubIndex].au1IfName,
                      ATOI (pi1IfNum));
            if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) ==
                OSIX_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
            }
            break;
#ifdef HDLC_WANTED
        case CFA_HDLC:
            if (CfaValidateIfNum
                ((UINT4) i4MatchIfType, pi1IfNum, &i4ParentIndex,
                 &u4SecondaryIndex) != OSIX_SUCCESS)
            {
                return CLI_FAILURE;
            }
            SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%ld/%ld",
                      asCfaIfaces[i4MatchIndex][i4MatchSubIndex].au1IfName,
                      i4ParentIndex, u4SecondaryIndex);
            if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) ==
                OSIX_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
            }
            break;
#endif
#if defined (WLC_WANTED) || defined (WTP_WANTED)
        case CFA_RADIO:
        case CFA_CAPWAP_VIRT_RADIO:
        case CFA_CAPWAP_DOT11_PROFILE:
        case CFA_CAPWAP_DOT11_BSS:
        case CFA_WLAN_RADIO:
            if (CfaValidateIfNum (i4MatchIfType, pi1IfNum, &i4SlotNum, &u4IfNum)
                != OSIX_SUCCESS)
            {
                return CLI_FAILURE;
            }
            SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                      asCfaIfaces[i4MatchIndex][i4MatchSubIndex].au1IfName,
                      ATOI (pi1IfNum));
            if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) ==
                OSIX_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
            }
            break;
#endif

        default:
            break;
    }

    return i4RetVal;
}

/*****************************************************************************
 *
 *    Function Name       : CfaCliGetPswIndex     
 *
 *    Description         : This function is used to get the Interface index 
 *                          a Port Channel interfaces, given the interface name
 *                          and port-channel number.
 *
 *    Input(s)            : Pseudo wire interface name and index
 *
 *
 *    Output(s)           : pu4IfIndex - IfIndex value of the given 
 *                          port-channel number
 *
 *    Global Variables Referred : asCfaIfaces (supported Interface types) 
 *                                structure.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CLI_SUCCESS or CLI_FAILURE.
 *****************************************************************************/

INT4
CfaCliGetPswIndex (INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex)
{
    INT4                i4Index = 0;
    INT4                i4Len = 0;
    INT4                i4IfTypeLen = 0;
    INT4                i4MatchIfType = -1;
    INT4                i4MatchIndex = -1;
    INT4                i4RetVal = CLI_SUCCESS;
    BOOL1               bIsIfTypeConflict = OSIX_FALSE;
    UINT1               au1IfName[IF_PREFIX_LEN];

    if (!(pi1IfName) || !(*pi1IfName) || !(pi1IfNum))
    {
        return CLI_FAILURE;
    }

    i4Len = (INT4) STRLEN (pi1IfName);

    /* Check for longest match with  any interface type's available */

    for (i4Index = 0; i4Index < CLI_MAX_IFTYPES; i4Index++)
    {
        if (asCfaIfaces[i4Index][0].u4IfType == CFA_PSEUDO_WIRE)
        {
            i4IfTypeLen = (INT4) STRLEN (asCfaIfaces[i4Index][0].au1IfName);

            i4Len = (i4Len < i4IfTypeLen) ? (i4Len) : (i4IfTypeLen);

            if (STRNCASECMP
                (asCfaIfaces[i4Index][0].au1IfName, pi1IfName, i4Len) == 0)
            {
                if (i4MatchIfType != -1)
                {
                    bIsIfTypeConflict = OSIX_TRUE;
                    break;
                }
                i4MatchIfType = (INT4) asCfaIfaces[i4Index][0].u4IfType;
                i4MatchIndex = i4Index;
            }
        }
    }

    if ((i4MatchIfType == -1) || (bIsIfTypeConflict == OSIX_TRUE))
    {
        return CLI_FAILURE;
    }

    /* if name shud be formed based on the match index or 
     * based on the match iftype
     */
    STRNCPY (au1IfName, asCfaIfaces[i4MatchIndex][0].au1IfName,
             STRLEN (asCfaIfaces[i4MatchIndex][0].au1IfName));
    au1IfName[STRLEN (asCfaIfaces[i4MatchIndex][0].au1IfName)] = '\0';

    /* pi1IfNum is a integer pointer which
     * contains the port channel index */
    SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
              asCfaIfaces[i4MatchIndex][0].au1IfName,
              *(INT4 *) (VOID *) pi1IfNum);
    if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) == OSIX_FAILURE)
    {
        i4RetVal = CLI_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************
 * 
 *    Function Name       : CfaCliGetACIndex
 * 
 *    Description         : This function is used to get the Interface index
 *                          a attachment circuit interfaces, given the interface name
 *                           and attachment circuit number.
 * 
 *    Input(s)            : Attachment circuit interface name and index
 *   
 *    
 *    Output(s)           : pu4IfIndex - IfIndex value of the given
 *                          attachment circuit number
 *       
 *    Global Variables Referred : asCfaIfaces (supported Interface types)
 *                                         structure.
 *          
 *    Global Variables Modified : None.
 *                   
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *                      
 *    Use of Recursion        : None.
 *       
 *    Returns            : CLI_SUCCESS or CLI_FAILURE.
 ******************************************************************************/
INT4
CfaCliGetACIndex (INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex)
{
    INT4                i4Index = 0;
    INT4                i4SubType = 0;
    INT4                i4Len = 0;
    INT4                i4IfTypeLen = 0;
    INT4                i4MatchIfType = -1;
    INT4                i4MatchSubType = -1;
    INT4                i4MatchIndex = -1;
    INT4                i4RetVal = CLI_SUCCESS;
    BOOL1               bIsIfTypeConflict = OSIX_FALSE;
    UINT1               au1IfName[IF_PREFIX_LEN];

    if (!(pi1IfName) || !(*pi1IfName) || !(pi1IfNum))
    {
        return CLI_FAILURE;
    }

    i4Len = (INT4) STRLEN (pi1IfName);

    /* Check for longest match with  any interface type's available */
    for (i4Index = 0; i4Index < CLI_MAX_IFTYPES; i4Index++)
    {
        if (asCfaIfaces[i4Index][0].u4IfType == CFA_PROP_VIRTUAL_INTERFACE)
        {
            for (i4SubType = 0; i4SubType < CLI_MAX_IFSUBTYPES; i4SubType++)
            {
                i4IfTypeLen =
                    (INT4) STRLEN (asCfaIfaces[i4Index][i4SubType].au1IfName);

                i4Len = (i4Len < i4IfTypeLen) ? (i4Len) : (i4IfTypeLen);

                if (STRNCASECMP
                    (asCfaIfaces[i4Index][i4SubType].au1IfName, pi1IfName,
                     i4Len) == 0)
                {
                    if (i4MatchIfType != -1)
                    {
                        bIsIfTypeConflict = OSIX_TRUE;
                        break;
                    }
                    i4MatchIfType =
                        (INT4) asCfaIfaces[i4Index][i4SubType].u4IfType;
                    i4MatchSubType = i4SubType;
                    i4MatchIndex = i4Index;
                }
            }
            if (i4MatchIndex < 0)
            {
                break;
            }
        }
    }

    if ((i4MatchIfType == -1) || (bIsIfTypeConflict == OSIX_TRUE))
    {
        return CLI_FAILURE;
    }

    /* if name shud be formed based on the match index or
     * based on the match iftype
     */
    MEMSET (au1IfName, 0, IF_PREFIX_LEN);
    STRNCPY (au1IfName, asCfaIfaces[i4MatchIndex][i4MatchSubType].au1IfName,
             STRLEN (asCfaIfaces[i4MatchIndex][i4MatchSubType].au1IfName));
    au1IfName[STRLEN (asCfaIfaces[i4MatchIndex][i4MatchSubType].au1IfName)] =
        '\0';

    /* pi1IfNum is a integer pointer which
     * ontains the attachment circuit index */
    SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
              asCfaIfaces[i4MatchIndex][i4MatchSubType].au1IfName,
              *(INT4 *) (VOID *) pi1IfNum);
    if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) == OSIX_FAILURE)
    {
        i4RetVal = CLI_FAILURE;
    }

    return i4RetVal;

}

#ifdef VXLAN_WANTED
/*****************************************************************************
 *
 *    Function Name       : CfaCliGetNveIndex     
 *
 *    Description         : This function is used to get the Interface index 
 *                          a Port Channel interfaces, given the interface name
 *                          and port-channel number.
 *
 *    Input(s)            : NVE interface name and index
 *
 *
 *    Output(s)           : pu4IfIndex - IfIndex value of the given 
 *                          port-channel number
 *
 *    Global Variables Referred : asCfaIfaces (supported Interface types) 
 *                                structure.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CLI_SUCCESS or CLI_FAILURE.
 *****************************************************************************/

INT4
CfaCliGetNveIndex (INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex)
{
    INT4                i4Index = 0;
    INT4                i4Len = 0;
    INT4                i4IfTypeLen = 0;
    INT4                i4MatchIfType = -1;
    INT4                i4MatchIndex = -1;
    INT4                i4RetVal = CLI_SUCCESS;
    BOOL1               bIsIfTypeConflict = OSIX_FALSE;
    UINT1               au1IfName[IF_PREFIX_LEN];

    MEMSET (au1IfName, 0, IF_PREFIX_LEN);
    if (!(pi1IfName) || !(*pi1IfName) || !(pi1IfNum))
    {
        return CLI_FAILURE;
    }

    i4Len = (INT4) STRLEN (pi1IfName);

    /* Check for longest match with  any interface type's available */

    for (i4Index = 0; i4Index < CLI_MAX_IFTYPES; i4Index++)
    {
        if (asCfaIfaces[i4Index][0].u4IfType == CFA_VXLAN_NVE)
        {
            i4IfTypeLen = (INT4) STRLEN (asCfaIfaces[i4Index][0].au1IfName);

            i4Len = (i4Len < i4IfTypeLen) ? (i4Len) : (i4IfTypeLen);

            if (STRNCASECMP
                (asCfaIfaces[i4Index][0].au1IfName, pi1IfName, i4Len) == 0)
            {
                if (i4MatchIfType != -1)
                {
                    bIsIfTypeConflict = OSIX_TRUE;
                    break;
                }
                i4MatchIfType = (INT4) asCfaIfaces[i4Index][0].u4IfType;
                i4MatchIndex = i4Index;
            }
        }
    }

    if ((i4MatchIfType == -1) || (bIsIfTypeConflict == OSIX_TRUE))
    {
        return CLI_FAILURE;
    }

    /* if name shud be formed based on the match index or 
     * based on the match iftype
     */
    STRNCPY (au1IfName, asCfaIfaces[i4MatchIndex][0].au1IfName,
             STRLEN (asCfaIfaces[i4MatchIndex][0].au1IfName));
    au1IfName[STRLEN (asCfaIfaces[i4MatchIndex][0].au1IfName)] = '\0';

    /* pi1IfNum is a integer pointer which
     * contains the port channel index */
    SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
              asCfaIfaces[i4MatchIndex][0].au1IfName,
              *(INT4 *) (VOID *) pi1IfNum);
    if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) == OSIX_FAILURE)
    {
        i4RetVal = CLI_FAILURE;
    }

    return i4RetVal;
}
#endif
#ifdef VCPEMGR_WANTED
/*****************************************************************************
 *
 *    Function Name       : CfaCliGetTapIndex     
 *
 *    Description         : This function is used to get the Interface index 
 *                          of interfaces, given the interface name
 *                          and TAP interface number.
 *
 *    Input(s)            : TAP interface name
 *
 *
 *    Output(s)           : pu4IfIndex - IfIndex value of the given 
 *                          TAP interface number
 *
 *    Global Variables Referred : asCfaIfaces (supported Interface types) 
 *                                structure.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CLI_SUCCESS or CLI_FAILURE.
 *****************************************************************************/

INT4
CfaCliGetTapIndex (INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex)
{
    INT4                i4Index = 0;
    INT4                i4Len = 0;
    INT4                i4IfTypeLen = 0;
    INT4                i4MatchIfType = -1;
    INT4                i4MatchIndex = -1;
    INT4                i4RetVal = CLI_SUCCESS;
    BOOL1               bIsIfTypeConflict = OSIX_FALSE;
    UINT1               au1IfName[IF_PREFIX_LEN];

    MEMSET (au1IfName, 0, IF_PREFIX_LEN);
    if (!(pi1IfName) || !(*pi1IfName) || !(pi1IfNum))
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaCliGetTapIndex - "
                 "pi1IfName/pi1IfName/pi1IfNum is NULL.\n");
        return CLI_FAILURE;
    }

    i4Len = (INT4) STRLEN (pi1IfName);

    /* Check for longest match with  any interface type's available */

    for (i4Index = 0; i4Index < CLI_MAX_IFTYPES; i4Index++)
    {
        if (asCfaIfaces[i4Index][0].u4IfType == CFA_TAP)
        {
            i4IfTypeLen = (INT4) STRLEN (asCfaIfaces[i4Index][0].au1IfName);

            i4Len = (i4Len < i4IfTypeLen) ? (i4Len) : (i4IfTypeLen);

            if (STRNCASECMP
                (asCfaIfaces[i4Index][0].au1IfName, pi1IfName, i4Len) == 0)
            {
                if (i4MatchIfType != -1)
                {
                    bIsIfTypeConflict = OSIX_TRUE;
                    break;
                }
                i4MatchIfType = (INT4) asCfaIfaces[i4Index][0].u4IfType;
                i4MatchIndex = i4Index;
            }
        }
    }

    if ((i4MatchIfType == -1) || (bIsIfTypeConflict == OSIX_TRUE))
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaCliGetTapIndex - "
                 "i4MatchIfType/bIsIfTypeConflict validation failed.\n");
        return CLI_FAILURE;
    }

    /* ifname should be formed based on the match index or 
     * based on the match iftype
     */
    STRNCPY (au1IfName, asCfaIfaces[i4MatchIndex][0].au1IfName,
             STRLEN (asCfaIfaces[i4MatchIndex][0].au1IfName));
    au1IfName[STRLEN (asCfaIfaces[i4MatchIndex][0].au1IfName)] = '\0';

    /* pi1IfNum is a integer pointer which
     * contains the port channel index */
    SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
              asCfaIfaces[i4MatchIndex][0].au1IfName,
              *(INT4 *) (VOID *) pi1IfNum);
    if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) == OSIX_FAILURE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaCliGetTapIndex - "
                 "CfaGetInterfaceIndexFromName Failed.\n");
        i4RetVal = CLI_FAILURE;
    }
    CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "Exiting CfaCliGetTapIndex.\n");
    return i4RetVal;
}
#endif
/*****************************************************************************
 *
 *    Function Name       : CfaCliGetOobIfIndex     
 *
 *    Description         : This function is used to get the Interface index 
 *                          of the oob interfaces. This function will return the *                          oob Interface index only if routing over Oob is 
 *                          enabled. This called to get index of oob for 
 *                          arp/route addition over Oob Interface.
 *
 *    Input(s)            : None
 *
 *
 *    Output(s)           : pu4IfIndex - IfIndex value of the Ob Interface
 *
 *    Global Variables Referred: gu4MgmtIntfRouting  
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CLI_SUCCESS or CLI_FAILURE.
 *****************************************************************************/

INT4
CfaCliGetOobIfIndex (UINT4 *pu4IfIndex)
{
    if (CFA_OOB_MGMT_INTF_ROUTING == TRUE)
    {
        *pu4IfIndex = CFA_OOB_MGMT_IFINDEX;
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaCliGetPoIndex     
 *
 *    Description         : This function is used to get the Interface index 
 *                          a Port Channel interfaces, given the interface name
 *                          and port-channel number.
 *
 *    Input(s)            : Port channel interface name and port-channel index
 *
 *
 *    Output(s)           : pu4IfIndex - IfIndex value of the given 
 *                          port-channel number
 *
 *    Global Variables Referred : asCfaIfaces (supported Interface types) 
 *                                structure.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CLI_SUCCESS or CLI_FAILURE.
 *****************************************************************************/

INT4
CfaCliGetPoIndex (INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex)
{
    INT4                i4Index;
    INT4                i4Len = 0;
    INT4                i4IfTypeLen = 0;
    INT4                i4MatchIfType = -1;
    INT4                i4MatchIndex = -1;
    INT4                i4RetVal = CLI_SUCCESS;
    BOOL1               bIsIfTypeConflict = OSIX_FALSE;
    UINT1               au1IfName[IF_PREFIX_LEN];

    if (!(pi1IfName) || !(*pi1IfName) || !(pi1IfNum))
    {
        return CLI_FAILURE;
    }

    i4Len = (INT4) STRLEN (pi1IfName);

    /* Check for longest match with  any interface type's available */

    for (i4Index = 0; i4Index < CLI_MAX_IFTYPES; i4Index++)
    {
        if (asCfaIfaces[i4Index][0].u4IfType == CFA_LAGG)
        {
            i4IfTypeLen = (INT4) STRLEN (asCfaIfaces[i4Index][0].au1IfName);

            i4Len = (i4Len < i4IfTypeLen) ? (i4Len) : (i4IfTypeLen);

            if (STRNCASECMP
                (asCfaIfaces[i4Index][0].au1IfName, pi1IfName, i4Len) == 0)
            {
                if (i4MatchIfType != -1)
                {
                    bIsIfTypeConflict = OSIX_TRUE;
                    break;
                }
                i4MatchIfType = (INT4) asCfaIfaces[i4Index][0].u4IfType;
                i4MatchIndex = i4Index;
            }
        }
    }

    if ((i4MatchIfType == -1) || (bIsIfTypeConflict == OSIX_TRUE))
    {
        return CLI_FAILURE;
    }

    /* if name shud be formed based on the match index or 
     * based on the match iftype
     */
    STRNCPY (au1IfName, asCfaIfaces[i4MatchIndex][0].au1IfName,
             sizeof (au1IfName) - CFA_STR_DELIM_SIZE);
    au1IfName[sizeof (au1IfName) - 1] = '\0';

    /* pi1IfNum is a integer pointer which
     * contains the port channel index */
    SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
              asCfaIfaces[i4MatchIndex][0].au1IfName,
              *(UINT4 *) (VOID *) pi1IfNum);
    if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) == OSIX_FAILURE)
    {
        i4RetVal = CLI_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************
 *
 *    Function Name       : CfaCliGetVirutalIndex     
 *
 *    Description         : This function is used to get the Interface index 
 *                          of a virtual interface, given the interface name
 *                          and virtual interface index.
 *
 *    Input(s)            : Virtual interface name and index
 *
 *
 *    Output(s)           : pu4IfIndex - IfIndex value of the given virtual 
 *                           interface
 *
 *    Global Variables Referred : asCfaIfaces (supported Interface types) 
 *                                structure.
 *               
 *    Returns            : CLI_SUCCESS or CLI_FAILURE.
 *****************************************************************************/

INT4
CfaCliGetVirtualIndex (INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex)
{
    INT4                i4Index;
    INT4                i4Len = 0;
    INT4                i4IfTypeLen = 0;
    INT4                i4MatchIfType = -1;
    INT4                i4MatchIndex = -1;
    INT4                i4RetVal = CLI_SUCCESS;
    BOOL1               bIsIfTypeConflict = OSIX_FALSE;
    UINT1               au1IfName[IF_PREFIX_LEN];

    if (!(pi1IfName) || !(*pi1IfName) || !(pi1IfNum))
    {
        return CLI_FAILURE;
    }

    i4Len = (INT4) STRLEN (pi1IfName);

    /* Check for longest match with  any interface type's available */

    for (i4Index = 0; i4Index < CLI_MAX_IFTYPES; i4Index++)
    {
        if (asCfaIfaces[i4Index][0].u4IfType == CFA_BRIDGED_INTERFACE)
        {
            i4IfTypeLen = (INT4) STRLEN (asCfaIfaces[i4Index][0].au1IfName);

            i4Len = (i4Len < i4IfTypeLen) ? (i4Len) : (i4IfTypeLen);

            if (STRNCASECMP
                (asCfaIfaces[i4Index][0].au1IfName, pi1IfName, i4Len) == 0)
            {
                if (i4MatchIfType != -1)
                {
                    bIsIfTypeConflict = OSIX_TRUE;
                    break;
                }
                i4MatchIfType = (INT4) asCfaIfaces[i4Index][0].u4IfType;
                i4MatchIndex = i4Index;
            }
        }
    }

    if ((i4MatchIfType == -1) || (bIsIfTypeConflict == OSIX_TRUE))
    {
        return CLI_FAILURE;
    }

    /* if name shud be formed based on the match index or 
     * based on the match iftype
     */
    STRNCPY (au1IfName, asCfaIfaces[i4MatchIndex][0].au1IfName,
             sizeof (au1IfName) - CFA_STR_DELIM_SIZE);
    au1IfName[sizeof (au1IfName) - 1] = '\0';

    /* pi1IfNum is a integer pointer which
     * contains the port channel index */
    SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
              asCfaIfaces[i4MatchIndex][0].au1IfName,
              *(UINT4 *) (VOID *) pi1IfNum);
    if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) == OSIX_FAILURE)
    {
        i4RetVal = CLI_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************
 *
 *    Function Name       : CfaCliGetIndexFromType     
 *
 *    Description         : This function is used to get the Interface index 
 *                          of an interface, given the interface type 
 *                          and port number.
 *
 *    Input(s)            : u4IfType   - interface type
 *                          pi1IfNum   - port number
 *
 *    Output(s)           : pu4IfIndex - IfIndex value of the given 
 *                          port number
 *
 *    Global Variables Referred : asCfaIfaces (supported Interface types) 
 *                                structure.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CLI_SUCCESS or CLI_FAILURE.
 *****************************************************************************/

INT4
CfaCliGetIndexFromType (UINT4 u4IfType, INT1 *pi1IfNum, UINT4 *pu4IfIndex)
{
    return (CfaCliGetIndexFromSubType (u4IfType, 0, pi1IfNum, pu4IfIndex));
}

/*****************************************************************************
 *
 *    Function Name       : CfaCliGetIndexFromSubType     
 *
 *    Description         : This function is used to get the Interface index 
 *                          of an interface, given the interface type,
 *                          port number and subtype.
 *
 *    Input(s)            : u4IfType   - interface type
 *                          u4SubType  - interface sub type
 *                          pi1IfNum   - port number
 *
 *    Output(s)           : pu4IfIndex - IfIndex value of the given 
 *                          port number
 *
 *    Global Variables Referred : asCfaIfaces (supported Interface types) 
 *                                structure.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CLI_SUCCESS or CLI_FAILURE.
 *****************************************************************************/

INT4
CfaCliGetIndexFromSubType (UINT4 u4IfType, UINT4 u4SubType, INT1 *pi1IfNum,
                           UINT4 *pu4IfIndex)
{
    UINT4               u4IfNum = 0;
    INT4                i4Index;
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4SlotNum;
    UINT1               au1IfName[IF_PREFIX_LEN];

    if (!(pi1IfNum))
    {
        return CLI_FAILURE;
    }

    /* Check for longest match with  any interface type's available */

    for (i4Index = 0; i4Index < CLI_MAX_IFTYPES; i4Index++)
    {
        if (asCfaIfaces[i4Index][u4SubType].u4IfType == u4IfType)
        {
            break;
        }
    }

    /* Process ifNum and get the interface index */
    switch (u4IfType)
    {
        case CFA_LVI_ENET:
        case CFA_XL_ENET:
        case CFA_XE_ENET:
        case CFA_ENET:
        case CFA_FA_ENET:
        case CFA_GI_ENET:
            if (CfaValidateIfNum (u4IfType, pi1IfNum, &i4SlotNum, &u4IfNum)
                != OSIX_SUCCESS)
            {
                return CLI_FAILURE;
            }
#ifdef MBSM_WANTED
            SNPRINTF ((CHR1 *) au1IfName, sizeof (au1IfName), "%s%d/%u",
                      ISS_ALIAS_PREFIX, i4SlotNum, u4IfNum);
            if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) ==
                OSIX_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
            }
#else
            *pu4IfIndex = u4IfNum;
#endif

            if (CfaValidateCfaIfIndex (*pu4IfIndex) == CFA_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
            }

            break;
        case CFA_LAGG:
        case CFA_BRIDGED_INTERFACE:
        case CFA_ILAN:
        case CFA_PROP_VIRTUAL_INTERFACE:
            if (i4Index == CLI_MAX_IFTYPES)
            {
                return CLI_FAILURE;
            }
            if (u4SubType == CFA_SUBTYPE_AC_INTERFACE)
            {
                SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%ld",
                          asCfaIfaces[i4Index][u4SubType].au1IfName,
                          ATOI ((const INT1 *) pi1IfNum));
                if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) ==
                    OSIX_FAILURE)
                {
                    i4RetVal = CLI_FAILURE;
                }
                break;
            }
        case CFA_L3IPVLAN:
            if (i4Index == CLI_MAX_IFTYPES)
            {
                return CLI_FAILURE;
            }
            /* pi1IfNum is a string which contains the interface index */
            if (CfaValidateIfNum (u4IfType, pi1IfNum, &i4SlotNum, &u4IfNum)
                != OSIX_SUCCESS)
            {
                return CLI_FAILURE;
            }
            SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                      asCfaIfaces[i4Index][0].au1IfName,
                      ATOI ((const INT1 *) pi1IfNum));
            if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) ==
                OSIX_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
            }

            break;

#ifdef PPP_WANTED
        case CFA_PPP:
            if (i4Index == CLI_MAX_IFTYPES)
            {
                return CLI_FAILURE;
            }
            SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%ld",
                      asCfaIfaces[i4Index][0].au1IfName,
                      ATOI ((const INT1 *) pi1IfNum));
            if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) ==
                OSIX_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
            }
            break;

#endif /* PPP_WANTED */
        case CFA_PSEUDO_WIRE:
        case CFA_STATION_FACING_BRIDGE_PORT:
#ifdef VXLAN_WANTED
        case CFA_VXLAN_NVE:
#endif
#ifdef VCPEMGR_WANTED
        case CFA_TAP:
#endif
            if (i4Index == CLI_MAX_IFTYPES)
            {
                return CLI_FAILURE;
            }
            SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%ld",
                      asCfaIfaces[i4Index][0].au1IfName,
                      ATOI ((const INT1 *) pi1IfNum));
            if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) ==
                OSIX_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
            }
            break;

        default:
            /* For other types the interface index will 
             * be the given port number itself.
             * Used for <port_list> token conversion
             */
            u4IfNum = (UINT4) ATOI (pi1IfNum);
            if ((u4IfNum < 1) || (u4IfNum > CFA_MAX_VLAN_ID))
                i4RetVal = CLI_FAILURE;
            else
                *pu4IfIndex = u4IfNum;
            break;
    }

    return i4RetVal;
}

/*****************************************************************************
 *
 *    Function Name       : CfaIsPhysicalInterface
 *
 *    Description         : This function validates the interface type
 *                          for the given interface index is Physical
 *                          or virtual.
 *
 *    Input(s)            : u4Index - interface index
 *
 *    Output(s)           : NONE
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : return CFA_TRUE if it's a Physical Interface
 *                         otherwise CFA_FALSE
 *
 *****************************************************************************/
UINT1
CfaIsPhysicalInterface (UINT4 u4IfIndex)
{
    UINT1               u1RetVal = CFA_FALSE;
    UINT1               u1IfType = CFA_NONE;

    CfaGetIfType (u4IfIndex, &u1IfType);

    switch (u1IfType)
    {
            /* Fall thru' if the type of the Interface is
             * a PHY Interface.
             */
        case CFA_ENET:
            if (CfaIsMgmtPort (u4IfIndex) == TRUE)
            {
                u1RetVal = CFA_FALSE;
            }
            else
            {
                u1RetVal = CFA_TRUE;
            }
            break;
#if defined (WLC_WANTED) || defined (WTP_WANTED)
        case CFA_RADIO:
            u1RetVal = CFA_TRUE;
            break;
#endif
        default:
            u1RetVal = CFA_FALSE;
            break;
    }
    return u1RetVal;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGetCidrSubnetMaskIndex
 *
 *    Description          : This function gets the index of the subnetmask 
 *                           from the cidr table corresponding to the given
 *                           subnet mask. Assumption of this function is that 
 *                           it is called with valid subnet.
 *
 *    Input(s)             : u4SubnetMask
 *
 *    Output(s)            : None.
 *
 *    Returns              : Index of the subnetmask
 ******************************************************************************/
UINT1
CfaGetCidrSubnetMaskIndex (UINT4 u4SubnetMask)
{
    INT4                i4CidrSubnetMaskIdx;

    /* scan the CidrSubnetMask tbl for index of the subnetmask corresponding to
     * the IpAddr */
    for (i4CidrSubnetMaskIdx = CFA_MAX_CIDR; i4CidrSubnetMaskIdx >= 0;
         i4CidrSubnetMaskIdx--)
    {
        if (u4SubnetMask == u4CidrSubnetMask[i4CidrSubnetMaskIdx])
        {
            return ((UINT1) i4CidrSubnetMaskIdx);
        }
    }

    return CFA_MAX_CIDR;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGetCidrSubnetMaskIndex
 *
 *    Description          : This function gets the subnetmask for a given 
 *                           prefix len from the cidr table 
 *                           Assumption of this function is that 
 *                           it is called with valid subnet.
 *
 *    Input(s)             : u4NetPrefixLen
 *
 *    Output(s)            : None.
 *
 *    Returns              : Index of the subnetmask
 ******************************************************************************/
UINT4
CfaGetCidrSubnetMask (UINT4 u4NetPrefixLen)
{
    return (u4CidrSubnetMask[u4NetPrefixLen]);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGetDefaultRouterIfIndex
 *
 *    Description          : This function gets the index of the subnetmask 
 *                           from the cidr table corresponding to the given
 *                           subnet mask. Assumption of this function is that 
 *                           it is called with valid subnet.
 *
 *    Input(s)             : u4SubnetMask
 *
 *    Output(s)            : None.
 *
 *    Returns              : Index of the subnetmask
 ******************************************************************************/
UINT4
CfaGetDefaultRouterIfIndex (VOID)
{
    UINT4               u4DefIfIndex;
    /* Determine the Default Interface Index */

    if (CFA_MGMT_PORT == TRUE)
    {
        u4DefIfIndex = CFA_OOB_MGMT_IFINDEX;
    }
    else
    {
        if (gu4IsIvrEnabled == CFA_ENABLED)
        {
            u4DefIfIndex = CFA_DEFAULT_ROUTER_VLAN_IFINDEX;
        }
        else
        {
            u4DefIfIndex = CFA_DEFAULT_ROUTER_IFINDEX;
        }
    }

    return u4DefIfIndex;
}

/*****************************************************************************
 *
 *    Function Name        : CfaValidateIfIndex
 *
 *    Description        : Provides interface to the external modules for 
 *                         validation (interface exists and its CFA MIB's 
 *                         ifMainRowStatus is ACTIVE) of the MIB-2 ifIndex 
 *                         given to an interface. 
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable.
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if the ifIndex is valid,
 *                otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaValidateIfIndex (UINT4 u4IfIndex)
{
    INT4                i4RetVal = CFA_FAILURE;

    if (u4IfIndex > CFA_MAX_INTERFACES () || u4IfIndex == 0)
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_TRUE)
    {
        i4RetVal = CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();

    return i4RetVal;
}

/******************************************************************************
 * Function           : CfaTnlMgrTxFrame
 * Input(s)           : pTnlPktInfo - HL should fill-in the respective fields  
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This routine is used to receive packets from Ip6 
 *                      for processing. 
 ******************************************************************************/
INT4
CfaTnlMgrTxFrame (tTnlPktTxInfo * pTnlPktInfo)
{
    UINT4               u4ByteCount = 0;
    tTnlIfNode         *pTnlEntry = NULL;
    tTnlGREHdr          GreHdr;
    UINT4               u4GreHdrLen;
    UINT2               u2Cksum;
    UINT1               u1OperStatus = CFA_IF_DOWN;
    tTnlIfNode         *pTnlIfNode = NULL;
    tCfaTnlCxt         *pTunlCxt = NULL;
#ifdef OPENFLOW_WANTED
    tEnetV2Header       EnetV2Header;
    UINT4               u4SrcIpAddr = ZERO;
    UINT4               u4DstIpAddr = ZERO;
    UINT1               u1EncapType = ZERO;
#endif /* OPENFLOW_WANTED */

    /* Assumption: pkt has ip6 hdr */

    /* HL should fill in the following fields of tTnlPktTxInfo:
     * pBuf, u2Len, u4IfIndex, TnlDestAddr, IpHdr & GreProto */

    /* Get the Tunnel Entry from the given output interface */

    CFA_TNL_DS_LOCK ();
    if ((pTunlCxt = CfaTnlGetCxt (pTnlPktInfo->u4ContextId)) == NULL)
    {
        /* No TnlEntry. Pkt Dropped... */
        CFA_IF_SET_OUT_ERR ((UINT2) pTnlPktInfo->u4IfIndex);
        CFA_TNL_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    TMO_SLL_Scan (&(pTunlCxt->TnlIfSLL), pTnlIfNode, tTnlIfNode *)
    {
        if (pTnlIfNode->u4IfIndex == pTnlPktInfo->u4IfIndex)
        {
            if ((pTnlIfNode->u1DirFlag == TNL_BIDIRECTIONAL) ||
                ((pTnlIfNode->u1DirFlag == TNL_UNIDIRECTIONAL) &&
                 (pTnlIfNode->u1Direction == TNL_OUTGOING)))
            {
                /* Matching Entry found */
                pTnlEntry = pTnlIfNode;
                break;
            }

            /* TnlEntry doesn't support OUTGOING direction. 
             * Continue to scan the next entry in TnlIfTable */
        }
    }

    CFA_TNL_DS_UNLOCK ();

    if (pTnlEntry != NULL)
    {
        CfaGetIfOperStatus (pTnlEntry->u4IfIndex, &u1OperStatus);
    }

    if ((pTnlEntry == NULL) || (u1OperStatus == CFA_IF_DOWN))
    {
        /* No TnlEntry. Pkt Dropped... */
        CFA_IF_SET_OUT_ERR ((UINT2) pTnlPktInfo->u4IfIndex);
        return CFA_FAILURE;
    }

    pTnlPktInfo->TnlSrcAddr.Ip4TnlAddr = pTnlEntry->LocalAddr.Ip4TnlAddr;

    if (pTnlEntry->u4EncapsMethod == TNL_TYPE_GRE)
    {
        MEMSET (&GreHdr, 0, MAX_GRE_HDR_LEN);

        /* Set ProtoType in GRE hdr */
        GreHdr.u2ProtoType = OSIX_HTONS (pTnlPktInfo->u2Proto);

        /* In the IP Header filed, Proto field should be GRE. */
        pTnlPktInfo->IpHdr.u1Proto = TNL_GRE_PROTO;

        if (pTnlEntry->bCksumFlag == TNL_CKSUM_ENABLE)
        {
            u4GreHdrLen = MAX_GRE_HDR_LEN;

            /* Set Cksum flag in GRE hdr */
            (GreHdr.u2CkFlgAndVer) |= GRE_SET_CKSUM_FLAG;
            GreHdr.u2CkFlgAndVer = OSIX_HTONS (GreHdr.u2CkFlgAndVer);

            /* Prepend GRE header */
            if (CRU_BUF_Prepend_BufChain (pTnlPktInfo->pBuf, (UINT1 *) &GreHdr,
                                          u4GreHdrLen) != CRU_SUCCESS)
            {
                CFA_IF_SET_OUT_ERR ((UINT2) pTnlPktInfo->u4IfIndex);
                return CFA_FAILURE;
            }

            /* Calc IP chksum and update in GRE hdr */
            u4ByteCount = pTnlPktInfo->u2Len + u4GreHdrLen;
            u2Cksum = UtlIpCSumCruBuf (pTnlPktInfo->pBuf, u4ByteCount, 0);
            u2Cksum = OSIX_HTONS (u2Cksum);
            if (CRU_BUF_Copy_OverBufChain
                (pTnlPktInfo->pBuf, (UINT1 *) &u2Cksum, GRE_CKSUM_OFFSET,
                 sizeof (UINT2)) != CRU_SUCCESS)
            {
                CFA_IF_SET_OUT_ERR ((UINT2) pTnlPktInfo->u4IfIndex);
                return CFA_FAILURE;
            }
        }
        else                    /* checksum disabled in GRE */
        {
            u4GreHdrLen = MIN_GRE_HDR_LEN;

            /* Prepend GRE header */
            if (CRU_BUF_Prepend_BufChain (pTnlPktInfo->pBuf, (UINT1 *) &GreHdr,
                                          u4GreHdrLen) != CRU_SUCCESS)
            {
                CFA_IF_SET_OUT_ERR ((UINT2) pTnlPktInfo->u4IfIndex);
                return CFA_FAILURE;
            }
        }

        /* GRE header is added. So update the IPHdr length. */
        pTnlPktInfo->IpHdr.u2Len = (UINT2) (pTnlPktInfo->u2Len + u4GreHdrLen);
    }

    /* Incr stats */
    u4ByteCount = CRU_BUF_Get_ChainValidByteCount (pTnlPktInfo->pBuf);
    CFA_IF_SET_OUT_OCTETS ((UINT2) pTnlPktInfo->u4IfIndex, u4ByteCount);

    switch (pTnlEntry->u4EncapsMethod)
    {
        case TNL_TYPE_GRE:
        case TNL_TYPE_IPV6IP:
        case TNL_TYPE_SIXTOFOUR:
        case TNL_TYPE_COMPAT:
        case TNL_TYPE_ISATAP:
#if defined (LNXIP4_WANTED) && defined (IP6_WANTED)
            if (CfaTnlEnqPktToLinuxIp (pTnlPktInfo,
                                       pTnlEntry->u4EncapsMethod) ==
                CFA_FAILURE)
            {
                CFA_IF_SET_OUT_ERR (pTnlPktInfo->u4IfIndex);
                return CFA_FAILURE;
            }
#endif
#ifdef IP_WANTED
            if (CfaTnlEnqPktToIp (pTnlPktInfo) == CFA_FAILURE)
            {
                CFA_IF_SET_OUT_ERR (pTnlPktInfo->u4IfIndex);
                return CFA_FAILURE;
            }
#endif
            break;

#ifdef OPENFLOW_WANTED
        case TNL_TYPE_OPENFLOW:
            MEMSET (&EnetV2Header, ZERO, CFA_ENET_V2_HEADER_SIZE);

            /* Add and fill ethernet header */
            EnetV2Header.u2LenOrType = OSIX_HTONS (CFA_ENET_IPV4);
            /* Fill in the Switch Mac as Source */
            CfaGetSysMacAddress (EnetV2Header.au1SrcAddr);

            /* Fetch the Destination MAC for ARP */
            u1EncapType = CFA_ENCAP_ENETV2;
            CRU_BUF_Copy_FromBufChain (pTnlPktInfo->pBuf,
                                       (UINT1 *) &u4DstIpAddr, IP_PKT_OFF_DEST,
                                       CFA_IP_V4_ADDR_LEN);
            u4DstIpAddr = OSIX_NTOHL (u4DstIpAddr);
            if (ArpResolveWithIndex (pTnlPktInfo->u4IfIndex, u4DstIpAddr,
                                     (INT1 *) EnetV2Header.au1DstAddr,
                                     &u1EncapType) == ARP_FAILURE)
            {
                /*
                 * If the ARP resolve fails, the destination might
                 * be on different subnet, so let the controller
                 * take care of filling the destination MAC.
                 */
            }

            CRU_BUF_Copy_FromBufChain (pTnlPktInfo->pBuf,
                                       (UINT1 *) &u4SrcIpAddr, IP_PKT_OFF_SRC,
                                       CFA_IP_V4_ADDR_LEN);
            u4SrcIpAddr = OSIX_NTOHL (u4SrcIpAddr);
            if (!u4SrcIpAddr)
            {
                /* 
                 * this might be the switch pinging the other switch/host
                 * over OpenFlow. Fill in the Tunnel IP Address as Src.
                 */
                u4SrcIpAddr = pTnlEntry->LocalAddr.Ip4TnlAddr;
                u4SrcIpAddr = OSIX_HTONL (u4SrcIpAddr);
                CRU_BUF_Copy_OverBufChain (pTnlPktInfo->pBuf,
                                           (UINT1 *) &u4SrcIpAddr,
                                           IP_PKT_OFF_SRC, CFA_IP_V4_ADDR_LEN);

                /* update the IP checksum too */
                u2Cksum = ZERO;
                CRU_BUF_Copy_OverBufChain (pTnlPktInfo->pBuf,
                                           (UINT1 *) &u2Cksum,
                                           OFC_IP_CHKSUM_OFF, sizeof (UINT2));
                u2Cksum = UtlIpCSumCruBuf (pTnlPktInfo->pBuf, IP_HDR_LEN, ZERO);
                u2Cksum = OSIX_HTONS (u2Cksum);
                CRU_BUF_Copy_OverBufChain (pTnlPktInfo->pBuf,
                                           (UINT1 *) &u2Cksum,
                                           OFC_IP_CHKSUM_OFF, sizeof (UINT2));
            }

            CRU_BUF_Prepend_BufChain (pTnlPktInfo->pBuf,
                                      (UINT1 *) &EnetV2Header,
                                      CFA_ENET_V2_HEADER_SIZE);

            /* lift the packet for Openflow hybrid processing */
            if (OfcHandleOpenflowSwModeProcess (&pTnlPktInfo->u4IfIndex,
                                                pTnlPktInfo->pBuf) ==
                OFC_SUCCESS)
            {
                if (OfcHybridHandlePktFromIss (pTnlPktInfo->u4IfIndex,
                                               pTnlPktInfo->pBuf) ==
                    OFC_SUCCESS)
                {
                    return CFA_SUCCESS;
                }
                return CFA_FAILURE;
            }
            break;
#endif
        default:
            return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGetTnlEntryInCxt
 * Input(s)           : pTnlIfEntry - pointer to tunnelIf struct 
 *                      u4AddrType - type of addr (IPV4/IPV6)
 *                      pTnlSrcAddr - source addr of the tunnel
 *                      pTnlDestAddr - dest addr of the tunnel
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This routine scans the TnlIfTbl to find the matching 
 *                      TnlEntry with the specified addr type, src addr &
 *                      dest addr. 
 ******************************************************************************/
INT4
CfaGetTnlEntryInCxt (UINT4 u4ContextId, tTnlIfEntry * pTnlIfEntry,
                     UINT4 u4AddrType,
                     tTnlIpAddr * pTnlSrcAddr, tTnlIpAddr * pTnlDestAddr)
{
    return (CfaTnlFindEntryInCxt
            (u4ContextId, u4AddrType, pTnlSrcAddr, pTnlDestAddr, 0,
             pTnlIfEntry));
}

/******************************************************************************
 * Function           : CfaGetTnlEntryFromIfIndex
 * Input(s)           : u4IfIndex - Index of the interface
 *                      pTnlIfEntry - pointer to the TnlEntry
 * Output(s)          : TnlNode (SLL node)
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This routine scans the TnlIfTbl to find the matching 
 *                      TnlEntry with the specified IfIndex.
 ******************************************************************************/
INT4
CfaGetTnlEntryFromIfIndex (UINT4 u4IfIndex, tTnlIfEntry * pTnlIfEntry)
{
    return (CfaFindTnlEntryFromIfIndex (u4IfIndex, pTnlIfEntry));
}

/*****************************************************************************
 *
 *    Function Name        : CfaInterfaceConfigIpParams
 *
 *    Description          : This function Posts a Message to CFA Task
 *                              for Configuring the Ip Information
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns          : CFA_SUCCESS if initialisation succeeds,
 *                       otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaInterfaceConfigIpParams (UINT4 u4IfIndex, tIpInterfaceParams IpInfo)
{
    tCfaExtTriggerMsg  *pExtTriggerMsg;

    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaInterfaceConfigIpParams - "
                 "CFA is not initialized.\n");
        return CFA_FAILURE;
    }
    if ((pExtTriggerMsg = (tCfaExtTriggerMsg *)
         MemAllocMemBlk (ExtTriggerMsgPoolId)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaInterfaceConfigIpParams - "
                  "Mempool allocation failed %d FAIL.\n", u4IfIndex);

        return CFA_FAILURE;
    }

    pExtTriggerMsg->uExtTrgMsg.IpInfoMsg.u4IfIndex = u4IfIndex;

    pExtTriggerMsg->uExtTrgMsg.IpInfoMsg.IpInfo.u4IpAddress =
        IpInfo.u4IpAddress;
    pExtTriggerMsg->uExtTrgMsg.IpInfoMsg.IpInfo.u4SubnetMask =
        IpInfo.u4SubnetMask;
    pExtTriggerMsg->uExtTrgMsg.IpInfoMsg.IpInfo.u4Mtu = IpInfo.u4Mtu;
    pExtTriggerMsg->uExtTrgMsg.IpInfoMsg.IpInfo.u4DNSPrimaryIp =
        IpInfo.u4DNSPrimaryIp;
    pExtTriggerMsg->uExtTrgMsg.IpInfoMsg.IpInfo.u4DNSSecondaryIp =
        IpInfo.u4DNSSecondaryIp;
    pExtTriggerMsg->uExtTrgMsg.IpInfoMsg.IpInfo.u4StatusFlag =
        IpInfo.u4StatusFlag;

    pExtTriggerMsg->i4MsgType = CFA_IPINFO_CONFIG_MSG;

    if (OsixQueSend (CFA_EXT_TRIGGER_QID, (UINT1 *) &pExtTriggerMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaInterfaceConfigIpParams - "
                  "Message En queue to CFA on interface %d FAIL.\n", u4IfIndex);

        MemReleaseMemBlock (ExtTriggerMsgPoolId, (UINT1 *) pExtTriggerMsg);
        return (CFA_FAILURE);
    }

    /* send an explicit event to the task */
    OsixEvtSend (CFA_TASK_ID, CFA_EXT_TRIGGER_EVENT);

    CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
              "Exiting CfaInterfaceConfigIpParams -"
              "Pkt Enqueue to CFA on interface %d SUCCESS.\n", u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaConfigBaseBridgeMode
 *
 *    Description          : This function posts a Message to CFA Task
 *                           for Configuring the default router port and 
 *                           create/delete IVR interfaces.
 *
 *    Input(s)             : None
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns          : CFA_SUCCESS / CFA_FAILURE.
 *****************************************************************************/
INT4
CfaConfigBaseBridgeMode (UINT1 u1BaseBridgeMode)
{
    tCfaExtTriggerMsg  *pExtTriggerMsg;

    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaConfigBaseBridgeMode - "
                 "CFA is not initialized.\n");
        return CFA_FAILURE;
    }
    if ((pExtTriggerMsg = (tCfaExtTriggerMsg *)
         MemAllocMemBlk (ExtTriggerMsgPoolId)) == NULL)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaConfigBaseBridgeMode - "
                 "Mempool allocation failed\n");

        return CFA_FAILURE;
    }

    pExtTriggerMsg->uExtTrgMsg.BaseBridgeModeMsg.u1BaseBridgeMode =
        u1BaseBridgeMode;
    pExtTriggerMsg->i4MsgType = CFA_BASE_BRIDGE_MODE_MSG;

    if (OsixQueSend (CFA_EXT_TRIGGER_QID, (UINT1 *) &pExtTriggerMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaConfigBaseBridgeMode - "
                 "Message En queue to CFA on interface\n");

        MemReleaseMemBlock (ExtTriggerMsgPoolId, (UINT1 *) pExtTriggerMsg);
        return (CFA_FAILURE);
    }

    /* send an explicit event to the task */
    OsixEvtSend (CFA_TASK_ID, CFA_EXT_TRIGGER_EVENT);

    CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
             "Exiting CfaConfigBaseBridgeMode -"
             "Pkt Enqueue to CFA SUCCESS.\n");
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaIsMgmtPortEnabled
 *
 *    Description          : This function returns the Management Port Status
 *
 *    Input(s)             : None
 *
 *    Output(s)            : None
 *
 *    Returns              : .OSIX_TRUE/OSIX_FALSE
 *
 *****************************************************************************/
INT4
CfaIsMgmtPortEnabled (VOID)
{
    return ((INT4) gu4MgmtPort);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIsDualOobEnabled
 *
 *    Description          : This function returns the Dual Oob Status
 *
 *    Input(s)             : None
 *
 *    Output(s)            : None
 *
 *    Returns              : OSIX_TRUE/OSIX_FALSE
 *
 *****************************************************************************/
UINT4
CfaIsDualOobEnabled (VOID)
{
    /* This value is fetched from a compilation flag DUAL_OOB
     * It is initialized in Cfa Init 
     * It is set TRUE is OOB interface is enabled in both 
     * Active and StandBy nodes */

    return gu4DualOob;
}

/******************************************************************************
 * Function           : CfaIsMgmtPort
 * Input(s)           : u4IfIndex - Index of the interface
 * Output(s)          : None
 * Returns            : TRUE/FALSE
 * Action             : This routine checks whetether the incoming interface 
 *                      is OOB Interface
 ******************************************************************************/
UINT4
CfaIsMgmtPort (UINT4 u4IfIndex)
{
    if ((u4IfIndex == CFA_OOB_MGMT_IFINDEX) && (CFA_MGMT_PORT == TRUE))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

#if defined (WLC_WANTED) || defined (WTP_WANTED)
/******************************************************************************
 * Function           : CfaIsRadioIntf
 * Input(s)           : u4IfIndex - Index of the interface
 * Output(s)          : None
 * Returns            : TRUE/FALSE
 * Action             : This routine checks whetether the incoming interface 
 *                      is WSS Interface
 ******************************************************************************/
#ifdef WTP_WANTED
UINT4
CfaIsRadioIntf (UINT4 u4IfIndex)
{
    if ((u4IfIndex >= SYS_DEF_MAX_ENET_INTERFACES + 1) &&
        (u4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}
#endif
/******************************************************************************
 * Function           : CfaIsWssIntf
 * Input(s)           : u4IfIndex - Index of the interface
 * Output(s)          : None
 * Returns            : TRUE/FALSE
 * Action             : This routine checks whetether the incoming interface 
 *                      is WSS Interface
 ******************************************************************************/
UINT4
CfaIsWssIntf (UINT4 u4IfIndex)
{
    if ((u4IfIndex >= CFA_MIN_WSS_IF_INDEX) &&
        (u4IfIndex <= CFA_MAX_WSS_IF_INDEX))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}
#endif
/******************************************************************************
 * Function           : CfaIsLinuxVlanIntf
 * Input(s)           : u4IfIndex - Index of the interface
 * Output(s)          : None
 * Returns            : TRUE/FALSE
 * Action             : This routine checks whetether the incoming interface 
 *                      is a linux vlan Interface
 ******************************************************************************/
UINT4
CfaIsLinuxVlanIntf (UINT4 u4IfIndex)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1IfType;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return FALSE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_FALSE)
    {
        CFA_DS_UNLOCK ();
        return FALSE;
    }

    u1IfType = CFA_CDB_IF_TYPE (u4IfIndex);
    STRNCPY (au1IfName, CFA_CDB_IF_ALIAS (u4IfIndex),
             sizeof (au1IfName) - CFA_STR_DELIM_SIZE);
    au1IfName[sizeof (au1IfName) - 1] = '\0';
    CFA_DS_UNLOCK ();

    /* check the interface type && routing over oob is wanted && 
       interface name starts with the name prefix "eth" */
    if ((u1IfType == CFA_L3IPVLAN) && (CFA_OOB_MGMT_INTF_ROUTING == TRUE))
    {
        if (STRNCASECMP (au1IfName, CFA_LINUX_L3IPVLAN_NAME_PREFIX,
                         STRLEN (CFA_LINUX_L3IPVLAN_NAME_PREFIX)) == 0)
        {
            return TRUE;
        }
    }
    if ((u1IfType == CFA_LAGG) && (CFA_OOB_MGMT_INTF_ROUTING == TRUE))
    {
        if (STRNCASECMP (au1IfName, CFA_LINUX_L3LAGG_NAME_PREFIX,
                         STRLEN (CFA_LINUX_L3LAGG_NAME_PREFIX)) == 0)
        {
            return TRUE;
        }
    }

    return FALSE;
}

/******************************************************************************
 * Function           : CfaIsValidLinuxVlanName
 * Input(s)           : pu1IfName - pointer to the name of the interface
 * Output(s)          : None
 * Returns            : TRUE/FALSE
 * Action             : This routine checks whetether the incoming interface 
 *                      is a linux vlan Interface
 ******************************************************************************/
UINT4
CfaIsValidLinuxVlanName (UINT1 *pu1IfName)
{
    INT1               *pi1VlanId;
    UINT2               u2IfIvrVlanId = 0;
    if (STRNCASECMP (pu1IfName, CFA_LINUX_OOB_PORT_NAME,
                     STRLEN (CFA_LINUX_OOB_PORT_NAME)) != 0)
    {
        return FALSE;
    }
    /* Assumption linux vlan interface will have name as eth0.100 */
    pi1VlanId = (INT1 *) STRCHR (pu1IfName, '.');

    if (pi1VlanId == NULL)
    {
        return FALSE;
    }

    pi1VlanId++;
    u2IfIvrVlanId = (UINT2) ATOI (pi1VlanId);
    if ((u2IfIvrVlanId < 1) || (u2IfIvrVlanId > CFA_MAX_VLAN_ID))
    {
        return FALSE;
    }

    return TRUE;
}

/******************************************************************************
 * Function           : CfaGetIfaceType 
 * Input(s)           : u4IfIndex - Index of the interface
 * Output(s)          : IfType
 * Returns            : TRUE/FALSE
 * Action             : This routine returns the type of the incoming interface 
 ******************************************************************************/
INT4
CfaGetIfaceType (UINT4 u4IfIndex, UINT1 *pu1IfType)
{
    *pu1IfType = 0;
    if (CfaGetIfType (u4IfIndex, pu1IfType) == CFA_SUCCESS)
    {
        return CFA_SUCCESS;
    }
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaIsLoopBackIntf
 * Input(s)           : u4IfIndex - Index of the interface
 * Output(s)          : None
 * Returns            : TRUE/FALSE
 * Action             : This routine checks whetether the incoming interface 
 *                      is a loop back Interface
 ******************************************************************************/
UINT4
CfaIsLoopBackIntf (UINT4 u4IfIndex)
{
    UINT1               u1IfType = CFA_NONE;
    CfaGetIfType (u4IfIndex, &u1IfType);
    if (u1IfType == CFA_LOOPBACK)
    {
        return TRUE;
    }
    return FALSE;
}

/*****************************************************************************/
/* Function Name      : CfaSetPauseMode                                      */
/*                                                                           */
/* Description        : Sets the pause mode for the given interface.         */
/*                                                                           */
/* Input(s)           : i4IssPortCtrlIndex - IfIndex of the port.            */
/*                      i4PortCtrlMode - Pause mode to be set.               */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
CfaSetPauseMode (INT4 i4IssPortCtrlIndex, INT4 i4PortCtrlMode)
{
    INT4                i4PauseAdminMode = PAUSE_DISABLED;

    if (i4PortCtrlMode == ISS_NONEGOTIATION)
    {
        CFA_DS_LOCK ();
        i4PauseAdminMode =
            CFA_CDB_IF_PAUSE_ADMIN_MODE ((UINT4) i4IssPortCtrlIndex);
        CFA_DS_UNLOCK ();

        if (CfaIsPhysicalInterface ((UINT4) i4IssPortCtrlIndex) != CFA_TRUE)
        {
            return;
        }
#ifdef NPAPI_WANTED
        if (CfaFsEtherHwSetPauseAdminMode ((UINT4) i4IssPortCtrlIndex,
                                           &i4PauseAdminMode) != FNP_SUCCESS)
        {
            return;
        }
#endif
    }
    else
    {
        /* Port control node has changed from NO-Negotiation to 
         * AutoNegotiation. Hence first make the registers to default 
         * mode. */
        CFA_DS_LOCK ();
        CFA_CDB_IF_PAUSE_ADMIN_MODE ((UINT4) i4IssPortCtrlIndex) =
            PAUSE_ENABLED_BOTH;
        CFA_DS_UNLOCK ();
        i4PauseAdminMode = ETH_PAUSE_ENABLED_XMIT_AND_RCV;
#ifdef NPAPI_WANTED
        CfaFsEtherHwSetPauseAdminMode (i4IssPortCtrlIndex, &i4PauseAdminMode);
#else
        UNUSED_PARAM (i4IssPortCtrlIndex);
        UNUSED_PARAM (i4PauseAdminMode);
#endif
    }
}

#ifdef L2RED_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaRmCallBk
 *
 *    Description          : This function Posts a Message to CFA Task when
 *                           GO_STANDBY/RM_CONFIG_RESTORE_COMPLETE event is
 *                           received from RM. When GO_ACTIVE event is received
 *                           from RM, then it calls the CfaNpSimRmCallBk 
 *                           function to register this new active node with
 *                           NPSIM.
 *
 *    Input(s)             : u1Event - Event given by RM module             
                             pData   - Msg to be enqueue                        
                             u2DataLen - Msg size                              
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS / CFA_FAILURE
 *****************************************************************************/
INT4
CfaRmCallBk (UINT1 u1Event, tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2PktLen)
{
    tCfaExtTriggerMsg  *pExtTriggerMsg;

    if ((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP)
        || (u1Event == RM_STANDBY_DOWN))
    {
        if (pBuf == NULL)
        {
            return RM_FAILURE;
        }
    }
    if (CFA_INITIALISED != TRUE)
    {
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pBuf);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            RmReleaseMemoryForMsg ((UINT1 *) pBuf);
        }
        return RM_FAILURE;
    }

    if ((pExtTriggerMsg = (tCfaExtTriggerMsg *) MemAllocMemBlk
         (ExtTriggerMsgPoolId)) == NULL)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "Error In CfaRmCallBk - "
                 "Mempool allocation failed.\n");

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pBuf);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            RmReleaseMemoryForMsg ((UINT1 *) pBuf);
        }
        return RM_FAILURE;
    }

    pExtTriggerMsg->i4MsgType = CFA_RM_MESSAGE;
    pExtTriggerMsg->uExtTrgMsg.CfaRmMsg.pFrame = pBuf;
    pExtTriggerMsg->uExtTrgMsg.CfaRmMsg.u2Length = u2PktLen;
    pExtTriggerMsg->uExtTrgMsg.CfaRmMsg.u1Event = u1Event;

    if (OsixQueSend (CFA_EXT_TRIGGER_QID, (UINT1 *) &pExtTriggerMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "Error In CfaRmCallBk - "
                 "Message En queue to CFA FAIL.\n");

        MemReleaseMemBlock (ExtTriggerMsgPoolId, (UINT1 *) pExtTriggerMsg);

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pBuf);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            RmReleaseMemoryForMsg ((UINT1 *) pBuf);
        }

        return RM_FAILURE;
    }

    /* send an explicit event to the task */
    OsixEvtSend (CFA_TASK_ID, CFA_EXT_TRIGGER_EVENT);

    CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "Exiting CfaRmCallBk -"
             "Pkt Enqueue to CFA SUCCESS.\n");

    return RM_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name        : CfaGetNodeStatusFromRm
 *
 *    Description          : This function gets the node status from RM, and
 *                           stores it in gCfaNodeStatus.
 *
 *    Input(s)             : u1Event - Event given by RM module             
                             pData   - Msg to be enqueue                        
                             u2DataLen - Msg size                              
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : gCfaNodeStatus.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : None.
 *****************************************************************************/
VOID
CfaGetNodeStatusFromRm (VOID)
{
    UINT4               u4NodeState;
    u4NodeState = RmGetNodeState ();

    switch (u4NodeState)
    {
        case RM_ACTIVE:
            CFA_NODE_STATUS () = CFA_NODE_ACTIVE;
            break;
        case RM_STANDBY:
            CFA_NODE_STATUS () = CFA_NODE_STANDBY;
            break;
        default:
            CFA_NODE_STATUS () = CFA_NODE_IDLE;
            break;
    }
}

#endif /* L2RED_WANTED */

/*****************************************************************************
 *
 *    Function Name        : CfaGetIfIndexFromIfNameAndIfNum
 *
 *    Description          : This function gets the IfIndex from the 
 *                           Interface Name and Interface Num
 *
 *    Input(s)             : pu1IfName -Interface Name 
                             pu1IfNum  - Interface Num                        
                             pu4IfIndex- Ifindex                              
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : None.
 *****************************************************************************/
INT4
CfaGetIfIndexFromIfNameAndIfNum (UINT1 *pu1IfName,
                                 UINT1 *pu1IfNum, UINT4 *pu4IfIndex)
{
    INT4                i4Len = 0;
    INT4                i4IfTypeLen = 0;
    INT4                i4MatchIfType = -1;
    INT4                i4MatchIndex = -1;
    UINT4               u4IfNum = 0;
    INT4                i4SlotNum;

    if (!(pu1IfName) || !(*pu1IfName) || !(pu1IfNum))
    {
        return CLI_FAILURE;
    }

    i4Len = (INT4) STRLEN (pu1IfName);

    /* Check for longest match with  any interface type's available */

    if ((pu1IfName[0] == 'f') || (pu1IfName[0] == 'F'))
    {

        i4IfTypeLen = (INT4) STRLEN (asCfaIfaces[1][0].au1IfName);

        i4Len = (i4Len < i4IfTypeLen) ? (i4Len) : (i4IfTypeLen);

        if (i4Len != 0)
        {
            if (STRNCASECMP (asCfaIfaces[1][0].au1IfName, pu1IfName, i4Len) ==
                0)
            {
                i4MatchIfType = (INT4) asCfaIfaces[1][0].u4IfType;
                i4MatchIndex = 1;
            }
        }
    }
    else if ((pu1IfName[0] == 'g') || (pu1IfName[0] == 'G'))
    {

        i4IfTypeLen = (INT4) STRLEN (asCfaIfaces[0][0].au1IfName);

        i4Len = (i4Len < i4IfTypeLen) ? (i4Len) : (i4IfTypeLen);

        if (i4Len != 0)
        {
            if (STRNCASECMP (asCfaIfaces[0][0].au1IfName, pu1IfName, i4Len) ==
                0)
            {
                i4MatchIfType = (INT4) asCfaIfaces[0][0].u4IfType;
                i4MatchIndex = 0;
            }
        }
    }
    else if ((pu1IfName[0] == 'e') || (pu1IfName[0] == 'E'))
    {

        i4IfTypeLen = (INT4) STRLEN (asCfaIfaces[2][0].au1IfName);

        i4Len = (i4Len < i4IfTypeLen) ? (i4Len) : (i4IfTypeLen);

        if (i4Len != 0)
        {
            if (STRNCASECMP (asCfaIfaces[2][0].au1IfName, pu1IfName, i4Len) ==
                0)
            {
                i4MatchIfType = (INT4) asCfaIfaces[2][0].u4IfType;
                i4MatchIndex = 2;
            }
        }
    }
    else if ((pu1IfName[0] == 'x') || (pu1IfName[0] == 'X'))
    {
        i4IfTypeLen = (INT4) STRLEN (asCfaIfaces[3][0].au1IfName);
        i4Len = (i4Len < i4IfTypeLen) ? (i4Len) : (i4IfTypeLen);

        if (i4Len != 0)
        {
            if (STRNCASECMP (asCfaIfaces[3][0].au1IfName, pu1IfName, i4Len) ==
                0)
            {
                i4MatchIfType = (INT4) asCfaIfaces[3][0].u4IfType;
                i4MatchIndex = 3;
            }
        }

    }
    else if ((pu1IfName[0] == 'l') || (pu1IfName[0] == 'L'))
    {
        i4IfTypeLen = (INT4) STRLEN (asCfaIfaces[4][0].au1IfName);
        i4Len = (i4Len < i4IfTypeLen) ? (i4Len) : (i4IfTypeLen);

        if (i4Len != 0)
        {
            if (STRNCASECMP (asCfaIfaces[4][0].au1IfName, pu1IfName, i4Len) ==
                0)
            {
                i4MatchIfType = (INT4) asCfaIfaces[4][0].u4IfType;
                i4MatchIndex = 4;
            }
        }

    }

    if (i4MatchIfType == -1)
    {
        return CLI_FAILURE;
    }

    STRNCPY (pu1IfName, asCfaIfaces[i4MatchIndex][0].au1IfName,
             sizeof (asCfaIfaces[i4MatchIndex][0].au1IfName) -
             CFA_STR_DELIM_SIZE);
    pu1IfName[sizeof (asCfaIfaces[i4MatchIndex][0].au1IfName) - 1] = '\0';

    if (CfaValidateIfNum ((UINT4) i4MatchIfType, (INT1 *) pu1IfNum,
                          &i4SlotNum, &u4IfNum) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }
#ifdef MBSM_WANTED
    SPRINTF ((CHR1 *) pu1IfName, "%s%d/%u", ISS_ALIAS_PREFIX,
             i4SlotNum, u4IfNum);
#else
    CfaGetAliasNameFromIfIndex (u4IfNum, pu1IfName);
#endif
    return (CfaGetIfIndexFromSlotAndPort
            ((INT4) i4SlotNum, u4IfNum, pu4IfIndex));
}

/*****************************************************************************/
/* Function Name      : CfaGetIfIndexFromSlotAndPort                         */
/*                                                                           */
/* Description        : To get the IfIndex from Slot and Port.               */
/*                                                                           */
/* Input(s)           : i4SlotNum - Slot Id                                  */
/*                      u4IfNum   - Interface Number                         */
/*                                                                           */
/* Output(s)          : pu4IfIndex - Interface Index.                        */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/ CLI_FAILURE.                            */
/*****************************************************************************/
INT4
CfaGetIfIndexFromSlotAndPort (INT4 i4SlotNum, UINT4 u4IfNum, UINT4 *pu4IfIndex)
{
#ifndef MBSM_WANTED
    UNUSED_PARAM (i4SlotNum);
    *pu4IfIndex = u4IfNum;
    return CLI_SUCCESS;
#else
    tMbsmSlotInfo       SlotInfo;

    if (MbsmGetSlotInfo (i4SlotNum, &SlotInfo) == MBSM_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (SlotInfo.i4ModuleType != MBSM_LINE_CARD)
    {
        return CLI_FAILURE;
    }

    if (u4IfNum > SlotInfo.u4NumPorts)
    {
        return CLI_FAILURE;
    }

    *pu4IfIndex = SlotInfo.u4StartIfIndex + (u4IfNum - 1);

    return CLI_SUCCESS;
#endif
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetL3SubIfParentIndex
 *
 *    Description         : This function is used to get the L3SubInterface`s 
 *                          Parent Interface, given the child(l3subif) index
 *                          
 *
 *    Input(s)            : Interface name and interface number
 *
 *
 *    Output(s)           : pu4IfIndex - IfIndex value of the given
 *                          interface number
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGetL3SubIfParentIndex (UINT4 u4L3SubIfIndex, UINT4 *pu4IfIndex)
{
    INT4                i4PhyPort = 0;
    INT4                i4LPort = 0;
    INT4                i4SlotNum = 0;
    INT4                i4AggId = 0;
    INT4                i4LAggId = 0;
    UINT1               au1PoIfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1IfName[IF_PREFIX_LEN];
    UINT1              *pu1IfName;

    MEMSET (au1IfName, 0, IF_PREFIX_LEN);

    /* Get the Logical interface name from logical interface index */
    CfaGetIfName (u4L3SubIfIndex, au1IfName);
    if (STRNCMP (au1IfName, "po", 2) != 0)
    {
        /* Separating till Slot Number */
        pu1IfName = (au1IfName + 4);

        /* Get the Physical port and logical port from the l3 sub interface port */
        if (CfaCliGetPhysicalAndLogicalPortNum ((INT1 *) pu1IfName, &i4PhyPort,
                                                &i4LPort,
                                                &i4SlotNum) == CFA_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                     "CfaGetL3SubIfParentIndex: Unable to get Phy port number");
            return CFA_FAILURE;
        }

        /* Get the L3SubInterface Index from Interface alias name */
        if (CfaGetIfIndexFromSlotAndPort
            (i4SlotNum, (UINT4) i4PhyPort, pu4IfIndex) == CLI_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                     "CfaGetL3SubIfParentIndex: Unable to get IfIndex");
            return CFA_FAILURE;
        }
    }
    else
    {
        pu1IfName = (au1IfName + 2);
        if (CfaCliGetPortChannelPhysicalAndLogicalPortNum
            ((INT1 *) pu1IfName, &i4AggId, &i4LAggId) != CFA_FAILURE)
        {
            SNPRINTF ((CHR1 *) au1PoIfName, CFA_MAX_PORT_NAME_LENGTH, "%s%u",
                      "po", i4AggId);
            if (CfaGetInterfaceIndexFromName (au1PoIfName, pu4IfIndex) ==
                OSIX_FAILURE)
            {
                return CFA_FAILURE;
            }
        }
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetParentPortOperStatus
 *
 *    Description         : This function is used to get the L3SubInterface`s 
 *                          Parent Port Oper Status, given the child(l3subif)
 *                          index.
 *                          
 *
 *    Input(s)            : u4L3SubIfIndex = L3 Subinterface index 
 *
 *
 *    Output(s)           : pu1OperStatus - Parent Port OperStatus
 *                          
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGetParentPortOperStatus (UINT4 u4L3SubIfIndex, UINT1 *pu1OperStatus)
{
    UINT4               u4ParentIfIndex = 0;

    /* Check whether the IfIndex is L3SubInterface */
    if (CfaIsL3SubIfIndex (u4L3SubIfIndex) == CFA_FALSE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                 "CfaIsL3SubIfIndex: Not a L3SubInterface");
        return CFA_FAILURE;
    }
    /* Get the Parent(Physical port) interface index from logical index */
    if (CfaGetL3SubIfParentIndex (u4L3SubIfIndex, &u4ParentIfIndex)
        == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                 "CfaGetL3SubIfParentIndex: Unable to get Parent IfIndex");
        return CFA_FAILURE;
    }

    /* Get the oper Status of the parent (physical port) Index */
    CfaGetIfOperStatus (u4ParentIfIndex, pu1OperStatus);

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaNotifyProtoToApp                                     */
/*                                                                           */
/* Description        : Notify the Appilcations                              */
/*                                                                           */
/* Input(s)           : u1ModeId - Specifies from which module the           */
/*                      notication is given.                                 */
/*                      It can take the values -                             */
/*                      LA_NOTIFY - when indictaion is send from LA          */
/*                      VLAN_NOTIFY - when indication is from VLAN           */
/*                      RSTP_NOTIFY -when indication is from RSTP            */
/*                      MSTP_NOTIFY - when indication is from MSTP           */
/*                      and so on                                            */
/*                      NotifyProtoToApp - contains the indications specific */
/*                      to the notification.                                 */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
CfaNotifyProtoToApp (UINT1 u1ModeId, tNotifyProtoToApp NotifyProtoToApp)
{

    switch (u1ModeId)
    {
        case LA_NOTIFY:

            switch (NotifyProtoToApp.LaNotify.u1Action)
            {
                case LA_NOTIFY_LINK_STATUS:

                    /*This case indicates a port channel becoming
                     *oper up or oper down.The Index of the port
                     *channel is contained in NotifyProtoToApp.LaNotify.
                     *LaInfo.u2IfKey and the oper status is contained in
                     *NotifyProtoToApp.LaNotify.u1OperStatus.*/
                    break;

                case LA_NOTIFY_PORT_IN_BUNDLE_STATUS:
                    /*This case indicates a port becoming
                     *oper up or oper down in a port channel.
                     *The Index of the portchannel is contained in 
                     *NotifyProtoToApp.LaNotify.LaInfo.u2IfKey,the
                     *Index of the port is contained in 
                     *NotifyProtoToApp.LaNotify.LaInfo.u4IfIndex and 
                     *the oper status is contained in
                     *NotifyProtoToApp.LaNotify.u1OperStatus.*/
                    break;

                case LA_NOTIFY_DEFAULTED:

                    /*This case indicates a port entering defaulted state.
                     *The Index of the port is contained in
                     *NotifyProtoToApp.LaNotify.LaInfo.u4IfIndex*/
                    break;

                case LA_NOTIFY_PARAMETER_CHANGE:
                    /*This indicates a change in the parmaters like 
                     * key , port index or system id of the partner.
                     * The new parameters are contained in 
                     * NotifyProtoToApp.LaNotify.LaInfo*/
                    break;

                case LA_ERROR_REC_THRESHOLD_EXCEEDED:
                case LA_NOTIFY_RECOVERY:
                    /*This indicates that port stuck in one of the
                       state and recovery needs to be triggered */

                    /* Triggering - Admin Down/Up */

                    CFA_DBG1 (CFA_TRC_ALL | CFA_TRC_ALL_TRACK, CFA_MAIN,
                              "CFA-LA: Triggering Recovery Mechanism (Admin Down & UP) for Port %d \r\n",
                              NotifyProtoToApp.LaNotify.LaInfo.u2IfIndex);

                    CFA_DS_LOCK ();
                    if (CFA_CDB_IF_OPER
                        (NotifyProtoToApp.LaNotify.LaInfo.u2IfIndex) ==
                        CFA_IF_UP)
                    {
                        CFA_DS_UNLOCK ();
#ifdef LA_WANTED
                        LA_UNLOCK ();
#endif
                        CFA_LOCK ();
                        CfaSetIfMainAdminStatus (NotifyProtoToApp.LaNotify.
                                                 LaInfo.u2IfIndex, CFA_IF_DOWN);
                        if (NotifyProtoToApp.LaNotify.u1Action ==
                            LA_NOTIFY_RECOVERY)
                        {
                            CfaSetIfMainAdminStatus (NotifyProtoToApp.LaNotify.
                                                     LaInfo.u2IfIndex,
                                                     CFA_IF_UP);
                        }
                        CFA_UNLOCK ();
#ifdef LA_WANTED
                        LA_LOCK ();
#endif
                    }
                    else
                    {
                        CFA_DS_UNLOCK ();
                    }
                    break;
                default:        /*undefined action */
                    break;
            }
            break;
        case STP_NOTIFY:

            CFA_DBG1 (CFA_TRC_ALL | CFA_TRC_ALL_TRACK, CFA_MAIN,
                      "CFA-STP: Triggering BPDU Guard Action (Admin Down & UP) for Port %d \r\n",
                      NotifyProtoToApp.STPNotify.u4IfIndex);

            CFA_LOCK ();

            if (NotifyProtoToApp.STPNotify.u1AdminStatus == CFA_IF_DOWN)
            {
                CfaSetIfMainAdminStatus ((INT4) NotifyProtoToApp.STPNotify.
                                         u4IfIndex, CFA_IF_DOWN);
            }

            CFA_UNLOCK ();
            break;

        default:                /*Invalid ModeId */
            break;
    }
}

/******************************************************************************
 * Function           : CfaSendEventToCfaTask
 * Input(s)           : u4Event - Event 
 * Output(s)          : None
 * Returns            : CFA_SUCCESS/CFA_FAILURE
 * Action             : Other modules should call this routine to
 *                      post any event to CFA task.
 ******************************************************************************/

INT4
CfaSendEventToCfaTask (UINT4 u4Event)
{
    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_MAIN, "CFA is not initialized\n");
        return CFA_FAILURE;
    }

    if (CFA_TASK_ID == 0)
    {
        if (OsixGetTaskId (SELF, (const UINT1 *) CFA_TASK_NAME,
                           &CFA_TASK_ID) != OSIX_SUCCESS)
        {
            CFA_DBG (ALL_FAILURE_TRC, CFA_MAIN, "Event send to RM failed\n");
            return CFA_FAILURE;
        }
    }
    OsixEvtSend (CFA_TASK_ID, u4Event);
    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaApiGetOIDFromObjName
 * Input(s)           : pObjNameStr - Object Name string 
 * Output(s)          : pRetValOidStr - OID string in tSNMP_OID_TYPE format
 * Returns            : CFA_SUCCESS/CFA_FAILURE
 * Action             : Other modules should call this routine to
 *                      Get the OID for a given object name.
 ******************************************************************************/
INT4
CfaApiGetOIDFromObjName (UINT1 *pObjNameStr, tSNMP_OID_TYPE * pRetValOidStr)
{
    tSNMP_OID_TYPE     *pOidStr = NULL;
/* NOTE: Currently this API will work for only 3 Obj
   au4IfIndex
   au4IfAdminStatus
   au4IfOperStatus
*/
    pOidStr = CfaMakeObjIdFromDotNew (pObjNameStr);
    if (pOidStr == NULL)
    {
        return CFA_FAILURE;
    }

    /* CfaMakeObjIdFromDotNew  will return 1 extra octet at the end of the 
     * OID list , it shold be removed from the final list */
    pOidStr->u4_Length -= 1;

    MEMCPY (pRetValOidStr->pu4_OidList, pOidStr->pu4_OidList,
            pOidStr->u4_Length * sizeof (UINT4));
    pRetValOidStr->u4_Length = pOidStr->u4_Length;

    if (pOidStr != NULL)
    {
        SNMP_FreeOid (pOidStr);
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetInfo
 *
 *    Description         : This is a generic API that can be used to 
 *                          retrive information in the CFA module based on the
 *                          input parameters.
 *
 *    Input(s)            : CfaInput contains:
 *                               - Type of Information required.
 *                               - interface index of the interface
 *    Output              : pCfaOutput contains:
 *                               - System Specific Port Identifier
 *
 *    Returns             : CFA_SUCCESS/CFA_FAILURE
 *****************************************************************************/
INT4
CfaGetInfo (tCfaInfoInput CfaInput, tCfaInfoOutput * pCfaOutput)
{
    UINT4               u4SysSpecificPortID = 0;

    if (CfaUtilIfGetSysSpecificPortID
        ((INT4) CfaInput.u4IfIndex, &u4SysSpecificPortID) == CFA_FAILURE)
    {
        pCfaOutput->u4SysSpecificPortId = u4SysSpecificPortID;
        return CFA_FAILURE;
    }

    pCfaOutput->u4SysSpecificPortId = u4SysSpecificPortID;

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmMakeDefaultRouterVlanUp
 *
 *    Description          : This function makes the default router 
 *                           interface up.
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.     
 *
 *****************************************************************************/
VOID
CfaIfmMakeDefaultRouterVlanUp (VOID)
{
    CFA_LOCK ();
    CFA_RED_IPINIT_COMPLETE_FLAG () = CFA_TRUE;
    CFA_UNLOCK ();
}

/*****************************************************************************
 *
 *    Function Name        : CfaTestv2IfMainRowStatus
 *
 *    Description          : This function is called from PBB to test destroy of
 *                           VIP ifindex rowstatus.
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.     
 *
 *****************************************************************************/

INT4
CfaTestv2IfMainRowStatus (UINT4 *pu4ErrCode, INT4 u4IfIndex, INT4 u4RowStatus)
{
    CFA_LOCK ();
    if (nmhTestv2IfMainRowStatus (pu4ErrCode, u4IfIndex, u4RowStatus) ==
        SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return SNMP_FAILURE;
    }
    else
    {
        CFA_UNLOCK ();
        return SNMP_SUCCESS;
    }

}

/*****************************************************************************
 *
 *    Function Name        : CfaSetIfMainRowStatus
 *
 *    Description          : This function is called from PBB to set destroy of
 *                           VIP ifindex rowstatus.
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.     
 *
 *****************************************************************************/

INT4
CfaSetIfMainRowStatus (INT4 u4IfIndex, INT4 u4RowStatus)
{
    CFA_LOCK ();
    if (nmhSetIfMainRowStatus (u4IfIndex, u4RowStatus) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return SNMP_FAILURE;
    }
    else
    {
        CFA_UNLOCK ();
        return SNMP_SUCCESS;
    }
}

/*****************************************************************************
 *
 *    Function Name        : CfaSetIfMainBrgPortType
 *
 *    Description          : This function is called from PBB to set Bridge port type
 *                           VIP ifindex 
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.     
 *
 *****************************************************************************/

INT1
CfaSetIfMainBrgPortType (INT4 i4IfMainIndex, INT4 i4SetValIfMainBrgPortType)
{
    CFA_LOCK ();
    if (nmhSetIfMainBrgPortType (i4IfMainIndex, i4SetValIfMainBrgPortType) ==
        SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return SNMP_FAILURE;
    }
    else
    {
        CFA_UNLOCK ();
        return SNMP_SUCCESS;
    }
}

/*****************************************************************************/
/* Function Name      : CfaTestIfPipHwAddr                                   */
/*                                                                           */
/* Description        : This function tests the MacAddress of the interface  */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      pu1HwAddr:Pointer to Mac Address                     */
/*                      u2Length:Length of Mac Address                       */
/*                                                                           */
/* Output             : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaTestIfPipHwAddr (UINT4 u4IfIndex, UINT1 *pu1HwAddr, UINT2 u2Length)
{
    UINT4               u4ErrCode;
    tMacAddr            PortMac;

    MEMSET (PortMac, 0, sizeof (tMacAddr));

    if (u2Length != CFA_ENET_ADDR_LEN)
    {
        return CFA_FAILURE;
    }

    MEMCPY (PortMac, pu1HwAddr, CFA_ENET_ADDR_LEN);

    if (nmhTestv2IfMainExtMacAddress (&u4ErrCode, (INT4) u4IfIndex, PortMac)
        == SNMP_FAILURE)
    {
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfPipHwAddr                                    */
/*                                                                           */
/* Description        : This function tests the MacAddress of the interface  */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      pu1HwAddr:Pointer to Mac Address                     */
/*                      u2Length:Length of Mac Address                       */
/*                                                                           */
/* Output             : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaSetIfPipHwAddr (UINT4 u4IfIndex, UINT1 *pu1HwAddr, UINT2 u2Length)
{
    tMacAddr            PortMac;

    MEMSET (PortMac, 0, sizeof (tMacAddr));

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) != CFA_TRUE)
    {
        return CFA_FAILURE;
    }

    if (u2Length != CFA_ENET_ADDR_LEN)
    {
        return CFA_FAILURE;
    }

    MEMCPY (PortMac, pu1HwAddr, CFA_ENET_ADDR_LEN);

    CFA_LOCK ();

    if (nmhSetIfMainExtMacAddress (u4IfIndex, PortMac) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }

    CFA_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaIsStackVlanIntf                                   */
/*                                                                           */
/* Description        : This function checks if the interface                */
/*                      is a stack index                                     */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output             : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaIsStackVlanIntf (UINT4 u4IfIndex)
{
    if (u4IfIndex == CFA_DEFAULT_STACK_IFINDEX)
    {
        if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
        {
            return CFA_SUCCESS;
        }
        else
        {
            return CFA_FAILURE;
        }
    }
    else
    {
        return CFA_FAILURE;
    }
}

/****************************************************************************
 Function    :  CfaGetIfIpAddr
 Input       :  The Indices
                IfMainIndex

                The Object
                retValIfIpAddr
 Output      :  Stores Ip information of an interface

 Returns     :  CFA_SUCCESS or CFA_FAILURE
****************************************************************************/
INT1
CfaGetIfIpAddr (INT4 i4IfMainIndex, UINT4 *pu4RetValIfIpAddr)
{
    if (nmhGetIfIpAddr (i4IfMainIndex, pu4RetValIfIpAddr) == SNMP_SUCCESS)
    {
        return CFA_SUCCESS;
    }
    return CFA_FAILURE;
}

/****************************************************************************
 Function    :  CfaUpdateIfaceMapping

 Description :  This function is called from VCM whenever an IP interface is 
                getting unmapped from one context and mapped to a different
                context. This function takes care of clearing the IP address
                configured for the interface. 

 Input       :  The Interface index
                Context id 

 Output      :  None
 
 Returns     :  CFA_SUCCESS or CFA_FAILURE
****************************************************************************/
INT4
CfaUpdateIfaceMapping (UINT4 u4IfIndex, UINT4 u4ContextId)
{
    tIpIfRecord         IpIntfInfo;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tIpIfRecord        *pIpIntf = NULL;
    tIpAddrInfo        *pSecondaryIpInfo = NULL;
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    tRtmRouteUpdateInfo IpRtInfo;
#endif
#ifdef NPAPI_WANTED
    UINT1               au1IfName[CFA_MAX_DESCR_LENGTH];
    UINT1               au1AliasName[CFA_MAX_PORT_NAME_LENGTH];
    UINT2               u2VlanId;
#endif

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    CFA_IPIFTBL_LOCK ();
    IpIntfInfo.u4IfIndex = u4IfIndex;
    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);

    if (pIpIntf == NULL)
    {
        CFA_IPIFTBL_UNLOCK ();
        CFA_DBG (BUFFER_TRC, CFA_L2, "RBTreeGet Failed\n");
        return CFA_FAILURE;
    }
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    /* Delete the local routes in RTM */
    MEMSET (&IpRtInfo, 0, sizeof (tRtmRouteUpdateInfo));
    IpRtInfo.u4ContextId = u4ContextId;
    IpRtInfo.u4NewIpAddr = pIpIntf->u4IpAddr;
    IpRtInfo.u4NewNetMask = u4CidrSubnetMask[pIpIntf->u1SubnetMask];
    IpRtInfo.u4IfIndex = (UINT4) CFA_IF_IPPORT ((UINT2) u4IfIndex);
    RtmIntfNotifyRoute (RTM_INTF_ROUTE_DEL_MSG, &IpRtInfo);
#endif

    /* Reset the IP address configured over the interface as the interface is
     * getting mapped to a different context.
     */
    pIpIntf->u4IpAddr = 0;
    pIpIntf->u4BcastAddr = 0;
    pIpIntf->u1SubnetMask = 0;
    pIpIntf->u1AddrAllocMethod = CFA_IP_ALLOC_MAN;
    pIpIntf->u1AddrAllocProto = CFA_PROTO_DHCP;

    /* Delete all the secondary address configured over this interface */
    while ((pSecondaryIpInfo = (tIpAddrInfo *)
            TMO_SLL_First (&(pIpIntf->SecondaryIpList))) != NULL)
    {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
        /* Delete the local routes for secondary addresses in RTM */
        MEMSET (&IpRtInfo, 0, sizeof (tRtmRouteUpdateInfo));
        IpRtInfo.u4ContextId = u4ContextId;
        IpRtInfo.u4NewIpAddr = pSecondaryIpInfo->u4Addr;
        IpRtInfo.u4NewNetMask =
            u4CidrSubnetMask[pSecondaryIpInfo->u1SubnetMask];
        IpRtInfo.u4IfIndex = (UINT4) CFA_IF_IPPORT ((UINT2) u4IfIndex);
        RtmIntfNotifyRoute (RTM_INTF_ROUTE_DEL_MSG, &IpRtInfo);
#endif
        SnmpNotifyInfo.pu4ObjectId = IfSecondaryIpRowStatus;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IfSecondaryIpRowStatus) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = TRUE;
        SnmpNotifyInfo.pLockPointer = NULL;
        SnmpNotifyInfo.pUnLockPointer = NULL;
        SnmpNotifyInfo.u4Indices = 2;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i", (INT4) u4IfIndex,
                          pSecondaryIpInfo->u4Addr, CFA_RS_DESTROY));
#ifdef NPAPI_WANTED
        MEMSET (au1IfName, 0, CFA_MAX_DESCR_LENGTH);
        MEMSET (au1AliasName, 0, CFA_MAX_PORT_NAME_LENGTH);
        CfaGetIfName (u4IfIndex, au1AliasName);
        SPRINTF ((CHR1 *) au1IfName, "%s:%d", au1AliasName,
                 pSecondaryIpInfo->u2AliasIndex);
        if (CfaGetIfIvrVlanId (u4IfIndex, &u2VlanId) == CFA_SUCCESS)
        {
            CfaFsNpIpv4DeleteIpInterface (u4ContextId, au1IfName, u4IfIndex,
                                          u2VlanId);
        }

#endif

        TMO_SLL_Delete (&(pIpIntf->SecondaryIpList),
                        &pSecondaryIpInfo->NextNode);
        MemReleaseMemBlock (gIpIfInfo.SecondaryIpPool,
                            (UINT1 *) pSecondaryIpInfo);
    }

    CFA_IPIFTBL_UNLOCK ();

    /* To avoid dead lock between MSR lock and IPIfTBL lock,
     * sending MSR notification is moved after CFA_IPIFTBL_UNLOCK */
    CfaIncMsrForIfIpTable ((INT4) u4IfIndex, 0, 'p', IfIpAddr,
                           (sizeof (IfIpAddr) / sizeof (UINT4)), FALSE);

    CfaIncMsrForIfIpTable ((INT4) u4IfIndex, 0, 'p', IfIpBroadcastAddr,
                           (sizeof (IfIpAddr) / sizeof (UINT4)), FALSE);

    CfaIncMsrForIfIpTable ((INT4) u4IfIndex, 0, 'p', IfIpSubnetMask,
                           (sizeof (IfIpAddr) / sizeof (UINT4)), FALSE);

    CfaIncMsrForIfIpTable ((INT4) u4IfIndex, CFA_IP_ALLOC_MAN, 'i',
                           IfIpAddrAllocMethod,
                           (sizeof (IfIpAddr) / sizeof (UINT4)), FALSE);

    CfaIncMsrForIfIpTable ((INT4) u4IfIndex, CFA_PROTO_DHCP, 'i',
                           IfIpAddrAllocProtocol,
                           (sizeof (IfIpAddr) / sizeof (UINT4)), FALSE);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaCliGetFirstIfIndexFromName
 *
 *    Description         : This function retrieves the interface index
 *                          for the given interface name.
 *
 *    Input(s)            : pu1Alias - Pointer to interface alias name
 *                          pu4Index - Pointer to interface index
 *
 *    Output(s)           : pu4Index - Pointer to interface index
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : OSIX_SUCCESS if alias name found in the interface
 *                         table 
 *                         otherwise OSIX_FAILURE.
 *
 *****************************************************************************/
UINT4
CfaCliGetFirstIfIndexFromName (UINT1 *pu1Alias, UINT4 *pu4Index)
{
    UINT4               u4Index = 1;
    if (pu1Alias == NULL)
    {
        return (OSIX_FAILURE);
    }

    CFA_DS_LOCK ();

    CFA_CDB_SCAN (u4Index, SYS_DEF_MAX_INTERFACES, CFA_ALL_IFTYPE)
    {
        /* Flexible ifIndex changes
         */
        if (((CFA_CDB_IF_TYPE (u4Index) == CFA_PROP_VIRTUAL_INTERFACE) &&
             (CFA_CDB_IF_SUB_TYPE (u4Index) == CFA_SUBTYPE_SISP_INTERFACE)) ||
            (CFA_CDB_IF_BRIDGE_PORT_TYPE (u4Index) ==
             CFA_VIRTUAL_INSTANCE_PORT))
        {
            continue;
        }

        if (CFA_CDB_IS_INTF_VALID (u4Index) != CFA_TRUE)
        {
            continue;
        }
        if (STRCMP (CFA_CDB_IF_ALIAS (u4Index), pu1Alias) == 0)
        {
            /* Matching L3 IP VLAN entry found */
            *pu4Index = u4Index;
            CFA_DS_UNLOCK ();
            return (OSIX_SUCCESS);
        }
    }

    CFA_DS_UNLOCK ();
    return (OSIX_FAILURE);
}

/*****************************************************************************
 *
 *    Function Name       : CfaCliGetNextIfIndexFromName
 *
 *    Description         : This function retrieves the next interface index
 *                          for the given interface name.
 *
 *    Input(s)            : pu1Alias - Pointer to interface alias name
 *                          u4Index - Interface index
 *                          pu4Index - Pointer to interface index
 *
 *    Output(s)           : pu4Index - Pointer to interface index
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : OSIX_SUCCESS if alias name found in the interface
 *                         table 
 *                         otherwise OSIX_FAILURE.
 *
 *****************************************************************************/
UINT4
CfaCliGetNextIfIndexFromName (UINT1 *pu1Alias, UINT4 u4IfIndex, UINT4 *pu4Index)
{
    UINT4               u4Index = 0;

    if (pu1Alias == NULL)
    {
        return (OSIX_FAILURE);
    }
    u4IfIndex++;
    if (u4IfIndex >= SYS_DEF_MAX_INTERFACES)
    {
        return (OSIX_FAILURE);
    }
    CFA_DS_LOCK ();

    u4Index = u4IfIndex;

    CFA_CDB_SCAN (u4Index, SYS_DEF_MAX_INTERFACES, CFA_ALL_IFTYPE)
    {
        /* Flexible ifIndex changes
         */
        if (((CFA_CDB_IF_TYPE (u4Index) == CFA_PROP_VIRTUAL_INTERFACE) &&
             (CFA_CDB_IF_SUB_TYPE (u4Index) == CFA_SUBTYPE_SISP_INTERFACE)) ||
            (CFA_CDB_IF_BRIDGE_PORT_TYPE (u4Index) ==
             CFA_VIRTUAL_INSTANCE_PORT))
        {
            continue;
        }

        if (CFA_CDB_IS_INTF_VALID (u4Index) != CFA_TRUE)
        {
            continue;
        }
        if (STRCMP (CFA_CDB_IF_ALIAS (u4Index), pu1Alias) == 0)
        {
            /* Matching L3 IP VLAN entry found */
            *pu4Index = u4Index;
            CFA_DS_UNLOCK ();
            return (OSIX_SUCCESS);
        }
    }

    CFA_DS_UNLOCK ();
    return (OSIX_FAILURE);
}

/*****************************************************************************
 *
 *    Function Name        : CfaDelDefL3VlanInterface
 *
 *    Description        : This function is an API to delete the Default L3 Vlan
 *                         interface
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaDelDefL3VlanInterface (VOID)
{
    UINT4               u4DefIfIndex = 0;
    u4DefIfIndex = CfaGetDefaultRouterIfIndex ();
    CFA_LOCK ();
    if (CFA_IF_ENTRY (u4DefIfIndex) == NULL)
    {
        CFA_UNLOCK ();
        return CFA_SUCCESS;
    }
    if (CfaIfmDeleteVlanInterface (u4DefIfIndex, CFA_TRUE) == CFA_FAILURE)
    {
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaNotifyStartProcessPktEvent
 *
 *    Description        : This function is used to send an event to cfa task
 *                         for starting the packet process.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaNotifyStartPktProcessEvent (VOID)
{
    /* Event used in CFA to start packet RX only after all
     * protocols/modules completed it's initializations.
     */
    if (CfaSendEventToCfaTask (CFA_PACKET_PROCESS_START_EVENT) != CFA_SUCCESS)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_MAIN, "Pkt Event send to Cfa Failed\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaNotifyStartBootupEvent
 *
 *    Description        : This function is used to send an event to cfa task
 *                         to start boot up.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaNotifyStartBootupEvent (VOID)
{
    if (CfaSendEventToCfaTask (CFA_BOOTUP_START_EVENT) != CFA_SUCCESS)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_MAIN, "Boot Event send to Cfa Failed\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *  Function    :  CfaHandleDhcpFallback
 *   Description :  This API configures default IP address and sets the mode
 *                   to manual.
 *                    Input       :  The Indices
 *                                    u4IfIndex
 *
 *                                     Output      :  None.
 *
 *                                      Returns     :  CFA_SUCCESS or CFA_FAILURE
 *                                      ****************************************************************************/
INT1
CfaHandleDhcpFallback (UINT4 u4IfIndex)
{
    INT4                i4Result = 0;
    INT4                i4RetValue = 0;

    UINT4               u4PrevIpAddr = 0;
    UINT4               u4PrevSubnetMask = 0;
    UINT4               u4BcastAddr = 0;
    UINT4               u4ErrCode = 0;

    tIpConfigInfo       IpInfo;

    tCfaDefPortParams   CfaDefPortParams;

    MEMSET (&CfaDefPortParams, 0, sizeof (tCfaDefPortParams));
    MEMSET (&IpInfo, 0, sizeof (tIpConfigInfo));

    /* Setting back to static IP address configuration.  */
    i4Result = nmhGetIfIpAddrAllocMethod ((INT4) u4IfIndex, &i4RetValue);

    if (i4Result == SNMP_FAILURE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandleDhcpFallback -"
                 " CFA failed in getting IpAddrAllocmethod");
        return CFA_FAILURE;
    }

    i4Result = nmhTestv2IfIpAddrAllocMethod (&u4ErrCode, (INT4) u4IfIndex,
                                             CFA_IP_ALLOC_MAN);
    if (i4Result == SNMP_FAILURE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandleDhcpFallback -"
                 " CFA IpAddrAllocMethod validation failed");
        return CFA_FAILURE;
    }

    i4Result = nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);

    if (i4Result == SNMP_FAILURE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandleDhcpFallback -"
                 " CFA failed in setting the IfMain rowstatus to NOTINSERVICE");
        return CFA_FAILURE;
    }

    i4Result = nmhSetIfIpAddrAllocMethod (u4IfIndex, CFA_IP_ALLOC_MAN);

    if (i4Result == SNMP_FAILURE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandleDhcpFallback -"
                 " CFA IpAddrAllocMethod configuration failed");
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE);
        return CFA_FAILURE;
    }

    i4Result = nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE);

    if (i4Result == SNMP_FAILURE)
    {
        nmhSetIfIpAddrAllocMethod (u4IfIndex, i4RetValue);
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandleDhcpFallback -"
                 " CFA configuration of IfMainRowStatus to ACTIVE failed");
        return CFA_FAILURE;
    }

    /* Setting the default IP address taken from nvram. */
    CfaGetDefaultPortParams (&CfaDefPortParams);

    /* Test Ip addr and Sub net mask */
    if (CfaTestIfIpAddrAndIfIpSubnetMask ((INT4) u4IfIndex,
                                          CfaDefPortParams.u4IpAddress,
                                          CfaDefPortParams.u4SubnetMask)
        == CLI_FAILURE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandleDhcpFallback -"
                 " CFA IpAddrAndIfIpSubnetMask validation failed");
        return (CFA_FAILURE);
    }
    nmhGetIfIpAddr ((INT4) u4IfIndex, &u4PrevIpAddr);
    if (nmhSetIfIpAddr (u4IfIndex, CfaDefPortParams.u4IpAddress)
        == SNMP_FAILURE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandleDhcpFallback -"
                 " CFA IpAddr configuration failed");
        return (CFA_FAILURE);
    }

    nmhGetIfIpSubnetMask ((INT4) u4IfIndex, &u4PrevSubnetMask);
    if (nmhSetIfIpSubnetMask (u4IfIndex, CfaDefPortParams.u4SubnetMask)
        == SNMP_FAILURE)
    {
        nmhSetIfIpAddr (u4IfIndex, u4PrevIpAddr);
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandleDhcpFallback -"
                 " CFA IP Subnet Mask configuration failed");
        return (CFA_FAILURE);
    }

    u4BcastAddr =
        CfaDefPortParams.u4IpAddress | (~(CfaDefPortParams.u4SubnetMask));
    if (nmhTestv2IfIpBroadcastAddr (&u4ErrCode, (INT4) u4IfIndex,
                                    u4BcastAddr) == SNMP_FAILURE)
    {
        nmhSetIfIpAddr (u4IfIndex, u4PrevIpAddr);
        nmhSetIfIpSubnetMask (u4IfIndex, u4PrevSubnetMask);
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandleDhcpFallback -"
                 " CFA IpBroadcastAddr validation failed");
        return (CFA_FAILURE);
    }

    if (nmhSetIfIpBroadcastAddr (u4IfIndex, u4BcastAddr) == SNMP_FAILURE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandleDhcpFallback -"
                 " CFA IpBraodcastAddr configuration failed");
        return (CFA_FAILURE);
    }
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaCopyPhysicalIfaceProperty                         */
/*                                                                           */
/* Description        : This API is invoked by VCM-SISP module whenever SISP */
/*                      is enabled on a port. CFA will inherit the properties*/
/*                      for the logical port from the provided physical port.*/
/*                                                                           */
/* Input(s)           : u4DstIfIndex - Logical port's interface index        */
/*                    : u4SrcIfIndex - Interface index of the physical over  */
/*                                     this logical runs.                    */
/*                                                                           */
/* Output             : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaCopyPhysicalIfaceProperty (UINT4 u4DstIfIndex, UINT4 u4SrcIfIndex)
{
    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_ACTIVE (u4DstIfIndex) != CFA_TRUE)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    /* Before updating the properties, change the row status as 
     * Not in Service
     * */
    CFA_CDB_SET_INTF_ACTIVE (u4DstIfIndex, CFA_FALSE);

    CFA_CDB_IF_OPER (u4DstIfIndex) = CFA_CDB_IF_OPER (u4SrcIfIndex);

    CFA_CDB_IF_SPEED (u4DstIfIndex) = CFA_CDB_IF_SPEED (u4SrcIfIndex);

    CFA_CDB_IF_ADMIN_STATUS (u4DstIfIndex) =
        CFA_CDB_IF_ADMIN_STATUS (u4SrcIfIndex);

    CFA_CDB_IF_HIGHSPEED (u4DstIfIndex) = CFA_CDB_IF_HIGHSPEED (u4SrcIfIndex);

    CFA_CDB_IF_DUPLEXSTATUS (u4DstIfIndex) =
        CFA_CDB_IF_DUPLEXSTATUS (u4SrcIfIndex);

    MEMCPY (&(CFA_CDB_IF_ENTRY (u4DstIfIndex)->au1MacAddr),
            &(CFA_CDB_IF_ENTRY (u4SrcIfIndex)->au1MacAddr), CFA_ENET_ADDR_LEN);

    CFA_CDB_SET_INTF_ACTIVE (u4DstIfIndex, CFA_TRUE);

    CFA_DS_UNLOCK ();

    return CFA_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name       : CfaSispResetInterfaceProp                       */
/*                                                                          */
/*    Description         : This function will initialise the properties of */
/*                          the SISP logical interfaces that will be        */
/*                          inherited from the corresponding physical       */
/*                          interfaces. This should be invoked when SISP    */
/*                          interface is unmapped from a physical port.     */
/*                                                                          */
/*    Input(s)            : u4SispIfIndex  - interface index                */
/*                                                                          */
/*    Output(s)           : NONE                                            */
/*                                                                          */
/*    Global Variables Referred : None.                                     */
/*                                                                          */
/*    Global Variables Modified : None.                                     */
/*                                                                          */
/*    Exceptions or Operating                                               */
/*    System Error Handling    : None.                                      */
/*                                                                          */
/*    Use of Recursion        : None.                                       */
/*                                                                          */
/*    Returns            : CFA_SUCCESS                                      */
/****************************************************************************/
INT4
CfaSispResetInterfaceProp (UINT4 u4SispIfIndex)
{
    CFA_DS_LOCK ();

    CFA_CDB_IF_OPER (u4SispIfIndex) = CFA_IF_DOWN;

    CFA_CDB_IF_SPEED (u4SispIfIndex) = 0;

    CFA_CDB_IF_ADMIN_STATUS (u4SispIfIndex) = CFA_IF_DOWN;

    CFA_CDB_IF_HIGHSPEED (u4SispIfIndex) = 0;

    CFA_CDB_IF_DUPLEXSTATUS (u4SispIfIndex) = 0;

    MEMSET (&(CFA_CDB_IF_ENTRY (u4SispIfIndex)->au1MacAddr), 0,
            CFA_ENET_ADDR_LEN);

    CFA_DS_UNLOCK ();

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaPostPktParamsFromL2                               */
/*                                                                           */
/* Description        : This function posts the parameters that needs to be  */
/*                      added into the PDU when egressed through the back    */
/*                      plane interface. This needs to be invoked only for   */
/*                      backplane interfaces.                                */
/*                                                                           */
/* Input(s)           : pBackPlaneParams - Pointer to structure containing   */
/*                                         the set of params that needs to   */
/*                                         be pushed into the pkt.           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
CfaPostPktParamsFromL2 (tCfaBackPlaneParams * pBackPlaneParams)
{
    /* enqueue the packet to CFA task */
    if (OsixQueSend (CFA_PACKET_PARAMS_QID, (UINT1 *) &pBackPlaneParams,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG (BUFFER_TRC | ALL_FAILURE_TRC, CFA_L2,
                 "Error In CfaPostPktParamsFromL2-"
                 "Backplane Params Enqueue to CFA FAIL.\n");

        return (CFA_FAILURE);
    }

    /* no need to send an explicit event. Packet Arrival event itself
     * will take care of processing the same.
     * */
    CFA_DBG (BUFFER_TRC, CFA_L2,
             "Exiting CfaPostPktParamsFromL2 -"
             "Pkt Enqueue to CFA on interfaced SUCCESS.\n");

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name             : CfaApiGetBackPlaneStatusForIface    
 *
 *    Description               : This function is an API to identify whether    *                                the provided interface is an backplane intf
 *                                or not. Backplane interfaces are interfaces
 *                                that can be used to communicate between two
 *                                nodes in a distributed system.
 *
 *    Input(s)                  : None.
 *
 *    Output(s)                 : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_TRUE/CFA_FALSE.
 *
 *****************************************************************************/
UINT1
CfaApiGetBackPlaneStatusForIface (UINT4 u4IfIndex)
{
    CFA_DS_LOCK ();

    if (CFA_IF_ENTRY_USED (u4IfIndex))
    {
        CFA_DS_UNLOCK ();
        return (CFA_IF_IS_BACKPLANE (u4IfIndex));
    }

    CFA_DS_UNLOCK ();
    return CFA_FALSE;
}

/*****************************************************************************
*    Function Name            : CfaIpIfGetIfIndexForNetInCxt
*
*    Description              :  Verifes whether the IP address belongs to any 
*                                of the interface's network in the specified
*                                context and returns the corresponding physical
*                                interface index
*    
*    Input(s)                  : u4ContextId - the context identifier
*                                u4IpAddress - Network Address to be verified
*
*    Output(s)                 : pu4IfIndex - Interface index on which the 
*                                              network is configured.
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE
*****************************************************************************/
INT4
CfaIpIfGetIfIndexForNetInCxt (UINT4 u4ContextId, UINT4 u4IpAddress,
                              UINT4 *pu4IfIndex)
{
    tIpIfRecord        *pIpIntf;
    tIpAddrInfo        *pSecIpInfo;
    INT4                i4RetVal = CFA_FAILURE;
    UINT4               u4IfCxtId = 0;
    UINT4               u4NetMask;

    *pu4IfIndex = 0;

    CFA_IPIFTBL_LOCK ();

    /* Get the first IP interface node from RBTree */
    pIpIntf = RBTreeGetFirst (gIpIfInfo.pIpIfTable);
    if (pIpIntf != NULL)
    {
        do
        {
            /* A Valid IP interface associated with the given
             * Interface */
            if (CFA_IF_IPPORT ((UINT2) pIpIntf->u4IfIndex) != CFA_INVALID_INDEX)
            {

                VcmGetContextIdFromCfaIfIndex (pIpIntf->u4IfIndex, &u4IfCxtId);
                if (u4IfCxtId != u4ContextId)
                {
                    pIpIntf = (tIpIfRecord *) RBTreeGetNext
                        (gIpIfInfo.pIpIfTable, (tRBElem *) pIpIntf, NULL);
                    continue;
                }
                u4NetMask = u4CidrSubnetMask[pIpIntf->u1SubnetMask];
                if ((pIpIntf->u4IpAddr != 0) &&
                    (((pIpIntf->u4IpAddr) & u4NetMask) ==
                     (u4IpAddress & u4NetMask)))
                {
                    *pu4IfIndex = pIpIntf->u4IfIndex;
                    i4RetVal = CFA_SUCCESS;
                    break;
                }

                TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo,
                              tIpAddrInfo *)
                {
                    if (pSecIpInfo->i4RowStatus != CFA_RS_ACTIVE)
                    {
                        continue;
                    }
                    u4NetMask = u4CidrSubnetMask[pSecIpInfo->u1SubnetMask];
                    if (((pSecIpInfo->u4Addr) & u4NetMask) ==
                        (u4IpAddress & u4NetMask))
                    {
                        *pu4IfIndex = pIpIntf->u4IfIndex;
                        i4RetVal = CFA_SUCCESS;
                        break;
                    }
                }
                if (i4RetVal == CFA_SUCCESS)
                {
                    break;
                }
            }
            pIpIntf = (tIpIfRecord *) RBTreeGetNext (gIpIfInfo.pIpIfTable,
                                                     (tRBElem *) pIpIntf, NULL);
        }
        while (pIpIntf != NULL);
    }
    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : CfaIndicateConfigRestoreFail                     */
/*                                                                           */
/*    Description         : This function will be called from MSR module,    */
/*                          when MSR restore fails.                          */
/*                          This API will take care of programming default   */
/*                          entries in CFA module on MSR restoration         */
/*                          failure.                                         */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
CfaIndicateConfigRestoreFail (VOID)
{
    UINT1               au1DefaultVlanInterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1IfType = 0;

    MEMSET (au1DefaultVlanInterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);

#ifdef WGS_WANTED
    u1IfType = CFA_L2VLAN;
    STRNCPY (au1DefaultVlanInterfaceName, CFA_DEFAULT_MGMT_INTERFACE,
             STRLEN (CFA_DEFAULT_MGMT_INTERFACE));
    au1DefaultVlanInterfaceName[STRLEN (CFA_DEFAULT_MGMT_INTERFACE)] = '\0';
#else
    u1IfType = CFA_L3IPVLAN;
    SNPRINTF ((CHR1 *) au1DefaultVlanInterfaceName,
              sizeof (au1DefaultVlanInterfaceName), "%s%u",
              CFA_DEFAULT_VLAN_INTERFACE, CFA_DEFAULT_VLAN_ID);

#endif

    if (CfaIfmCreateAndInitIfEntry (CFA_DEFAULT_ROUTER_VLAN_IFINDEX,
                                    u1IfType, au1DefaultVlanInterfaceName)
        != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN, "Error in CfaIndicateConfigRestore"
                 "Fail - interface creation - FAILURE.\n");
        return;
    }

    CfaSetIfAlias (CFA_DEFAULT_ROUTER_VLAN_IFINDEX,
                   au1DefaultVlanInterfaceName);

    /* make Rowstatus active */
    CFA_IF_RS (CFA_DEFAULT_ROUTER_VLAN_IFINDEX) = CFA_RS_ACTIVE;

    CfaSetIfActiveStatus (CFA_DEFAULT_ROUTER_VLAN_IFINDEX, CFA_TRUE);

    CfaSetCdbRowStatus (CFA_DEFAULT_ROUTER_VLAN_IFINDEX, CFA_RS_ACTIVE);

    CfaProcessIpInitComplete ();
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : CfaGetIfAutoNegEntry                             */
/*                                                                           */
/*    Description         : This API will be called from MSR module,         */
/*                          for validating the Auto neg entries              */
/*                                                                           */
/*    Input(s)            : u4IfIndex - IfIndex for ports                    */
/*                        : u4IfMauIndex - Mau Index                         */
/*                                                                           */
/*    Output(s)           : pIfMauEntry - configured Auto negoitiation       */
/*                          values for MAU entries                           */
/*                                                                           */
/*    Returns             : CFA_SUCCESS/CFA_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
CfaGetIfAutoNegEntry (UINT4 u4IfIndex, UINT4 u4IfMauIndex,
                      tIfMauStruct * pIfMauEntry)
{
    UINT1               u1IfType = CFA_NONE;
    tIfMauStruct       *pCurrIfMauEntry = NULL;

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0) ||
        (u4IfMauIndex > MAU_MAX_IF_COUNT))
    {
        return CFA_FAILURE;
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u4IfIndex)) && (u1IfType == CFA_ENET))
    {
        /* Update the AdminStatus in the MAU table */
        pCurrIfMauEntry = CfaCdbGetIfMauEntry (u4IfIndex, u4IfMauIndex);
        if (pCurrIfMauEntry != NULL)
        {
            MEMCPY (pIfMauEntry, pCurrIfMauEntry, sizeof (tIfMauStruct));
        }
        else
        {
            return CFA_FAILURE;
        }
    }
    else
    {
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

/****************************************************************************
 Function    :  CfaUpdateVrfInterfaceMac

 Description :  This function is used to update interface mac as VRF mac 

 Input       :  The Interface index
                Context id

 Output      :  None

 Returns     :  CFA_SUCCESS or CFA_FAILURE
****************************************************************************/
INT4
CfaUpdateVrfInterfaceMac (UINT4 u4IfIndex, UINT4 u4ContextId)
{
    UINT1               au1IfHwAddr[CFA_ENET_ADDR_LEN];

    /* When an interface is mapped to VR,
     * assign the mac address of the VR to the interface */
    if (VcmGetVRMacAddr (u4ContextId, au1IfHwAddr) == VCM_SUCCESS)
    {
        CfaSetIfHwAddr (u4IfIndex, au1IfHwAddr, CFA_ENET_ADDR_LEN);
        return CFA_SUCCESS;
    }
    return CFA_FAILURE;
}

/****************************************************************************
 Function    :  CfaApiUpdtIvrInterface

 Description :  This function is used to update L3 Interface

 Input       :  The Interface index
        The Status

 Output      :  None

 Returns     :  CFA_SUCCESS or CFA_FAILURE
****************************************************************************/
INT4
CfaApiUpdtIvrInterface (UINT4 u4IfIndex, UINT1 u1Status)
{

    CFA_LOCK ();
    if (CfaUpdtIvrInterface (u4IfIndex, u1Status) == CFA_FAILURE)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_L2, "Error in CfaApiUpdtIvrInterface -"
                 "L3 Interface Programming failed  %d.\n");

        CFA_UNLOCK ();
        return CFA_FAILURE;

    }
    CFA_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaPostPktOverVlan                                   */
/*                                                                           */
/* Description        : This function posts the Layer 2 packets to CFA task. */
/*                      The Packet needs to be flooded over all the member   */
/*                      ports of the VLAN.                                   */
/*                                                                           */
/* Input(s)           : pBuf       - Pointer the buffer                      */
/*                      u4ContextId- Context Identifier                      */
/*                      VlanId     - Vlan Identifier.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE.                           */
/*                                                                           */
/*****************************************************************************/
INT4
CfaPostPktOverVlan (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ContextId,
                    UINT2 VlanId)
{
    if (gu1CfaInitialised != TRUE)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_L2, "Error in CfaPostPktOverVlan -"
                 "CFA is not initialized %d.\n");

        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_FAILURE;
    }

    CFA_SET_COMMAND (pBuf, VLAN_TO_CFA);
    CFA_SET_IFINDEX (pBuf, u4ContextId);
    CFA_SET_PROTOCOL (pBuf, VlanId);

    /* enqueue the packet to CFA task */
    if (OsixQueSend (CFA_PACKET_QID, (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN) !=
        OSIX_SUCCESS)
    {
        CFA_DBG1 (BUFFER_TRC | ALL_FAILURE_TRC, CFA_L2,
                  "Error In CfaPostPktOverVlan -"
                  "Pkt Enqueue to CFA on Vlan %d FAIL.\n", VlanId);

        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

        return OSIX_FAILURE;
    }

    /* send an explicit event to the task */
    OsixEvtSend (CFA_TASK_ID, CFA_PACKET_ARRIVAL_EVENT);

    CFA_DBG1 (BUFFER_TRC, CFA_L2,
              "Exiting CfaPostPktOverVlan -"
              "Pkt Enqueue to CFA on Vlan %d SUCCESS.\n", VlanId);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGetSlotAndPortFromIfIndex
 *
 *    Description          : This function is used to get slot and
 *                           port form Interface Index value
 *
 *    Input(s)             : u4IfIndex - Interface Index
 *
 *    Output(s)            : pi4SlotNum - Slot Number
 *                           pi4PortNum - PortNumber
 *
 *    Returns              : CFA_SUCCESS / CFA_FAILURE
 *
 *****************************************************************************/
INT4
CfaGetSlotAndPortFromIfIndex (UINT4 u4IfIndex, INT4 *pi4SlotNum,
                              INT4 *pi4PortNum)
{
    UINT1              *pu1IfNum = NULL;
    UINT1               au1Alias[IF_PREFIX_LEN] = { 0 };
    UINT1               u1IfType = 0;
    INT4                i4IfTypeLen = 0;
    INT4                i4Index = 0;

    MEMSET (au1Alias, 0, IF_PREFIX_LEN);

    if (CfaGetIfName (u4IfIndex, au1Alias) == CFA_SUCCESS)
    {
        CfaGetIfType (u4IfIndex, &u1IfType);

        for (i4Index = 0; i4Index < CLI_MAX_IFTYPES; i4Index++)
        {
            if (u1IfType == asCfaIfaces[i4Index][0].u4IfType)
            {
                i4IfTypeLen = (INT4) STRLEN (asCfaIfaces[i4Index][0].au1IfName);

                if (MEMCMP (au1Alias, asCfaIfaces[i4Index][0].au1IfName,
                            i4IfTypeLen) == 0)
                {
                    pu1IfNum = (au1Alias + i4IfTypeLen);
                    break;
                }
            }
        }

        if (CfaGetSlotAndPortNum ((INT1 *) pu1IfNum, pi4SlotNum,
                                  pi4PortNum) == TRUE)
        {
            return CFA_SUCCESS;
        }
    }
    return CFA_FAILURE;

}

/*****************************************************************************/
/* Function Name      : CfaGetIfPortSecState                                 */
/*                                                                           */
/* Description        : This function gets the port state for the            */
/*                      given interface.                                     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pu1PortSecState -Pointer to port state for the       */
/*                                       given port                          */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
PUBLIC INT4
CfaGetIfPortSecState (UINT4 u4IfIndex, UINT1 *pu1PortSecState)
{
    if ((u4IfIndex > SYS_MAX_INTERFACES) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu1PortSecState = CFA_CDB_IF_PORT_SEC_STATE (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************
 *    Function Name       : CfaGetUpLinkPortList
 *
 *    Description         : This function Gets the uplink portlist for the
 *                          given VLAN
 *
 *    Input(s)            : u4ContextId - Context Identifier 
 *                          VlanId - VLAN Identifier.                          
 *
 *    Output(s)           : PortList - The upstream portlist for the given VLAN
 *
 *    Returns             : CFA_SUCCESS/CFA_FAILURE
 *****************************************************************************/
PUBLIC INT4
CfaGetUpLinkPortList (UINT4 u4ContextId, UINT2 VlanId, tPortList PortList)
{
    return (CfaGetTrustedPortList (u4ContextId, VlanId, PortList));
}

/*****************************************************************************/
/* Function Name      : CfaGetIfPortType                                     */
/*                                                                           */
/* Description        : This function gets the port type for the given       */
/*                      interface.                                           */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pu1PortType - Pointer to port type for the           */
/*                                    given port.                            */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
PUBLIC INT4
CfaGetIfPortType (UINT4 u4IfIndex, UINT1 *pu1PortType)
{
    UINT1               u1PortSecState = 0;

    if (CfaGetIfPortSecState (u4IfIndex, &u1PortSecState) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }

    if (u1PortSecState == CFA_PORT_STATE_TRUSTED)
    {
        *pu1PortType = CFA_PORT_TYPE_UPLINK;
    }
    else if (u1PortSecState == CFA_PORT_STATE_UNTRUSTED)
    {
        *pu1PortType = CFA_PORT_TYPE_DOWNLINK;
    }
    else
    {
        *pu1PortType = CFA_PORT_TYPE_NONE;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *    Function Name       : CfaGetTrustedPortList
 *
 *    Description         : This function Gets the trusted portlist for the
 *                          given VLAN
 *
 *    Input(s)            : u4ContextId - Context Identifier 
 *                          VlanId - VLAN Identifier.
 *
 *    Output(s)           : PortList - The trusted portlist for the given VLAN
 *
 *    Returns             : CFA_SUCCESS/CFA_FAILURE
 *****************************************************************************/
PUBLIC INT4
CfaGetTrustedPortList (UINT4 u4ContextId, UINT2 VlanId, tPortList PortList)
{
    INT4                i4ByteIndex = -1;
    UINT4               u4IfIndex = 1;
    UINT1               u1Result = OSIX_TRUE;
    UINT1               u1IfType = 0;
    UINT4               u4IfRange = 0;
    UINT1               u1IfLoopCount = 0;

    if (L2IwfMiGetVlanEgressPorts (u4ContextId, VlanId, PortList)
        != L2IWF_SUCCESS)
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    /* Flexible ifIndex change: Loop twice, once for physical interfaces
     * and then for LAG interfaces 
     */
    for (u1IfType = CFA_ENET, u4IfRange = SYS_DEF_MAX_PHYSICAL_INTERFACES,
         u1IfLoopCount = 0; u1IfLoopCount < 2; u1IfType = CFA_LAGG,
         u4IfRange = BRG_MAX_PHY_PLUS_LOG_PORTS, u1IfLoopCount++)
    {
        CFA_CDB_SCAN (u4IfIndex, u4IfRange, u1IfType)
        {
            /* If the bitmap contain 8 concecutive zeros, starting from position
             * zero of the byte, then entire byte will be zero, and there is no
             * need for looping through it */

            if (((u4IfIndex - 1) % 8) == 0)
            {
                i4ByteIndex++;
                if (PortList[i4ByteIndex] == 0)
                {
                    u4IfIndex = u4IfIndex + 8;
                    continue;
                }
            }

            OSIX_BITLIST_IS_BIT_SET (PortList, u4IfIndex,
                                     sizeof (tPortList), u1Result);
            if (u1Result == OSIX_TRUE)
            {
                if (CFA_CDB_IF_PORT_SEC_STATE (u4IfIndex) !=
                    CFA_PORT_STATE_TRUSTED)
                {
                    OSIX_BITLIST_RESET_BIT (PortList, u4IfIndex,
                                            sizeof (tPortList));
                }
            }
        }
    }
    CFA_DS_UNLOCK ();
    UNUSED_PARAM (u1IfType);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaApiSetIfPortSecState                              */
/*                                                                           */
/* Description        : This function sets port security state for the       */
/*                      given interface.                                     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                      u1PortSecState - Port Security state of the interface*/
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaApiSetIfPortSecState (UINT4 u4IfIndex, UINT1 u1PortSecState)
{
    UINT1               u1IfType = 0;

    CFA_DS_LOCK ();

    if (CfaValidateIfMainTableIndex ((INT4) u4IfIndex) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM, "Error in - CfaApiSetIfPortSecState "
                  "Invalid Interface Index %d\n", u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    u1IfType = CFA_CDB_IF_TYPE (u4IfIndex);

    /* the IfType must have been specified first */
    /* PortSecState can be set only for ENET interfaces */
    if ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG))
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    /* the RowStatus should be active first */
    if (CFA_IF_RS (u4IfIndex) != CFA_RS_ACTIVE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM, "Error in - CfaApiSetIfPortSecState "
                  "Interface %d is not active \n", u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    if (CFA_IS_PORT_SEC_STATE_VALID (u1PortSecState) == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM, "Error in - CfaApiSetIfPortSecState "
                  "Invalid Port Security State for index %d \n", u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    CFA_DS_UNLOCK ();

    CfaSetIfPortSecState (u4IfIndex, u1PortSecState);

    return CFA_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : CfaGetHLIfIndex                                  */
/*                                                                          */
/*    Description        : Gets the stacked higher layer index from lower   */
/*                         layer index                                      */
/*                                                                          */
/*    Input(s)           : u2LLIfIndex - Lower layer index                  */
/*                                                                          */
/*    Output(s)          : pu2HLIfIndex - Higher layer index                */
/*                                                                          */
/*    Returns            : CFA_SUCCESS/CFA_FAILURE                          */
/****************************************************************************/
INT4
CfaGetHLIfIndex (UINT4 u4LLIfIndex, UINT4 *pu4HLIfIndex)
{
    UINT4               u4HLIfIndex;
    tStackInfoStruct   *pScanNode = NULL;

    /* This function is called from within the CFA_LOCK at all places
     * currently. Hence the CFA_LOCK should not be taken again here. */

    pScanNode = (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4LLIfIndex);

    if (pScanNode == NULL)
    {
        return CFA_FAILURE;
    }

    u4HLIfIndex = CFA_IF_STACK_IFINDEX (pScanNode);
    if (u4HLIfIndex != CFA_NONE)
    {
        *pu4HLIfIndex = u4HLIfIndex;
        return CFA_SUCCESS;
    }

    return CFA_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : CfaGetLLIfIndex                                  */
/*                                                                          */
/*    Description        : Gets the stacked lower layer index from higher   */
/*                         layer index                                      */
/*                                                                          */
/*    Input(s)           : u4HLIfIndex - Lower layer index                  */
/*                                                                          */
/*    Output(s)          : pu4LLIfIndex - Higher layer index                */
/*                                                                          */
/*    Returns            : CFA_SUCCESS/CFA_FAILURE                          */
/****************************************************************************/
INT4
CfaGetLLIfIndex (UINT4 u4HLIfIndex, UINT4 *pu4LLIfIndex)
{
    UINT4               u4LLIfIndex;
    tStackInfoStruct   *pScanNode = NULL;

    pScanNode = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4HLIfIndex);
    if (pScanNode == NULL)
    {
        return CFA_FAILURE;
    }

    u4LLIfIndex = CFA_IF_STACK_IFINDEX (pScanNode);
    if (u4LLIfIndex != CFA_NONE)
    {
        *pu4LLIfIndex = u4LLIfIndex;
        return CFA_SUCCESS;
    }

    return CFA_FAILURE;
}

/****************************************************************************
 Function    :  CfaTesrIfIpAddrAndIfIpSubnetMask

 Input       :  i4IfMainIndex - Index of the interface
                u4TestValIfIpAddr - Ipaddress for the interface
                u4TestValIfIpSubnetMask -subnetmask for the interface
                
 Output      :  None
 
 Returns     :  CLI_SUCCESS or CLI_FAILURE
****************************************************************************/
INT4
CfaTestIfIpAddrAndIfIpSubnetMask (INT4 i4IfMainIndex,
                                  UINT4 u4TestValIfIpAddr,
                                  UINT4 u4TestValIfIpSubnetMask)
{
    UINT2               u2IfIndex;
    UINT1               u1IfType;
    UINT1               u1BridgedIfaceStatus;
    tIpConfigInfo       IpIfInfo;
    UINT1               u1Counter;
    UINT4               u4BroadCastAddr;
    UINT4               u4NetworkIpAddr;

    u2IfIndex = (UINT2) i4IfMainIndex;

    if (u2IfIndex <= 0 || u2IfIndex > SYS_DEF_MAX_INTERFACES)
    {
        return CLI_FAILURE;
    }

    if (CfaIpIfGetIfInfo ((UINT4) u2IfIndex, &IpIfInfo) == CFA_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (u4TestValIfIpAddr != 0)
    {
        if (!((CFA_IS_ADDR_CLASS_A (u4TestValIfIpAddr) ?
               (IP_IS_ZERO_NETWORK (u4TestValIfIpAddr) ? 0 : 1) : 0) ||
              (CFA_IS_ADDR_CLASS_B (u4TestValIfIpAddr)) ||
              (CFA_IS_ADDR_CLASS_C (u4TestValIfIpAddr)) ||
              (CFA_IS_ADDR_CLASS_E (u4TestValIfIpAddr))))
        {
            CLI_SET_ERR (CLI_CFA_ZERO_IP_ERR);
            return CLI_FAILURE;
        }
    }

    /* IP address should not be configured for the bridged interface */
    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        CfaGetIfBridgedIfaceStatus ((UINT4) u2IfIndex, &u1BridgedIfaceStatus);
        if (u1BridgedIfaceStatus == CFA_ENABLED)
        {
            CLI_SET_ERR (CLI_CFA_IP_BRIDGE_ERR);
            return CLI_FAILURE;
        }
    }

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((u4TestValIfIpAddr == IpIfInfo.u4Addr) &&
        (u4TestValIfIpSubnetMask == IpIfInfo.u4NetMask))
    {
        return CLI_SUCCESS;
    }
    if (u4TestValIfIpAddr != IpIfInfo.u4Addr)
    {
        /* Dont allow configuration of primary address same as that
         * of secondary */
        if (CfaIpIfIsOurInterfaceAddress ((UINT4) u2IfIndex,
                                          u4TestValIfIpAddr) == CFA_SUCCESS)
        {
            CLI_SET_ERR (CFA_CLI_SAME_IP_ERR);
            return CLI_FAILURE;
        }
    }

    /* The valid subnet address is from 0 to 32 */
    for (u1Counter = 0; u1Counter <= CFA_MAX_CIDR; ++u1Counter)
    {
        if (u4CidrSubnetMask[u1Counter] == u4TestValIfIpSubnetMask)
        {
            break;
        }
    }

    if (u1Counter != 0)
    {
        if ((CFA_CDB_IF_TYPE (u2IfIndex) == CFA_LOOPBACK) &&
            ((u1Counter != CFA_MAX_CIDR)))
        {
            /* Loopback interface mask should be 32 bit mask. */
            CLI_SET_ERR (CLI_CFA_WRONG_SUBNET_ERR);
            return CLI_FAILURE;
        }
    }

    if (u1Counter > CFA_MAX_CIDR)
    {
        return CLI_FAILURE;
    }

/* the interface should not have any other interface above it */
    if (CfaIpIfValidateIfIpSubnet ((UINT4) u2IfIndex,
                                   u4TestValIfIpAddr,
                                   u4TestValIfIpSubnetMask) == CFA_FAILURE)
    {
        CLI_SET_ERR (CLI_CFA_SUBNET_ERR);
        return CLI_FAILURE;
    }

/*when 'no ipaddress' command for primary ipaddress is executed then u4TestValIfIpAddr and u4TestValIfIpSubnetMask can have a value ZERO,if ipaddress command is executed u4TestValIfIpAddr and u4TestValIfIpSubnetMask always not equal to zero*/
    if (u4TestValIfIpAddr == 0 && u4TestValIfIpSubnetMask == 0)
    {
        return CLI_SUCCESS;
    }
    /*when netmask is 31 bit mask we need NOT to check ip address with broadcast address */
    if ((u4TestValIfIpSubnetMask != 0xffffffff)
        && (u4TestValIfIpSubnetMask != IP_ADDR_31BIT_MASK))
    {
        /* ipaddress should not be Broadcast Address */
        u4BroadCastAddr = u4TestValIfIpAddr | (~(u4TestValIfIpSubnetMask));
        if (u4TestValIfIpAddr == u4BroadCastAddr)
        {
            CLI_SET_ERR (CLI_CFA_INVALID_SUBNET);
            return CLI_FAILURE;
        }

        /* ipaddress should not be Network Address */
        u4NetworkIpAddr = u4TestValIfIpAddr & (~(u4TestValIfIpSubnetMask));
        if (u4NetworkIpAddr == 0)
        {
            CLI_SET_ERR (CLI_CFA_INVALID_SUBNET);
            return CLI_FAILURE;
        }
    }
#ifdef ICCH_WANTED
    /* Configuration should be blocked, if the IP Address belongs to ICCL IP subnet
       as ICCL IP Address,(which will be reflected from next reboot) */
    if (OSIX_SUCCESS ==
        CfaIsIcchNextIpAddress (u4TestValIfIpAddr, u4TestValIfIpSubnetMask))
    {
        CLI_SET_ERR (CLI_CFA_IP_NEXT_ICCL);
        return CLI_FAILURE;
    }
#endif

    return CLI_SUCCESS;
}

/****************************************************************************
 Function    :  CfaCheckSecurityForBridging

 Input       :  pBuf - Buffer

                u4IfIndex - IfIndex
                
 Output      :  pu1IsSecWantedForBridgedTraffic - Indicates whether security 
                is needed for Bridged Traffic
 
 Returns     :  None
****************************************************************************/
VOID
CfaCheckSecurityForBridging (tCRU_BUF_CHAIN_HEADER * pBuf,
                             UINT4 u4IfIndex,
                             UINT1 *pu1IsSecWantedForBridgedTraffic)
{
    UINT1              *pSecVlanList = NULL;
    UINT4               u4IvrIndex = 0;
    UINT4               u4SecMode = CFA_SEC_BRIDGE_DISABLED;
    UINT2               u2LenOrType = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2TCI = 0;
    UINT2               u2Protocol = 0;
    UINT1               u1BridgedIfaceStatus = CFA_DISABLED;
    UINT1               u1NwType = 0;
    BOOL1               bResult = OSIX_FALSE;

    CfaGetSecurityBridgeMode (&u4SecMode);
    if (u4SecMode == CFA_SEC_BRIDGE_DISABLED)
    {
        *pu1IsSecWantedForBridgedTraffic = (UINT1) bResult;
        return;
    }

    u4IvrIndex = CfaGetSecIvrIndex ();
    if ((u4IvrIndex == u4IfIndex) || (u4IvrIndex == CFA_INVALID_INDEX))
    {
        *pu1IsSecWantedForBridgedTraffic = (UINT1) bResult;
        return;
    }
    pSecVlanList = UtilVlanAllocVlanListSize (sizeof (tSecVlanList));
    if (pSecVlanList == NULL)
    {
        *pu1IsSecWantedForBridgedTraffic = (UINT1) bResult;
        return;
    }

    MEMSET (pSecVlanList, 0, sizeof (tSecVlanList));

    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);
    CfaGetIfNwType (u4IfIndex, &u1NwType);
    if ((u1BridgedIfaceStatus == CFA_ENABLED)
        && (CFA_NETWORK_TYPE_WAN == u1NwType))
    {
        CRU_BUF_Copy_FromBufChain (pBuf,
                                   (UINT1 *) &u2LenOrType,
                                   VLAN_TAG_OFFSET, CFA_ENET_TYPE_OR_LEN);
        u2LenOrType = OSIX_NTOHS (u2LenOrType);
        CRU_BUF_Copy_FromBufChain (pBuf,
                                   (UINT1 *) &u2Protocol,
                                   VLAN_TAGGED_HEADER_SIZE,
                                   CFA_ENET_TYPE_OR_LEN);
        u2Protocol = OSIX_NTOHS (u2Protocol);
        if ((u2LenOrType == VLAN_PROTOCOL_ID) && (u2Protocol == CFA_ENET_IPV4))
        {
            CRU_BUF_Copy_FromBufChain (pBuf,
                                       (UINT1 *) &u2TCI,
                                       VLAN_TAG_VLANID_OFFSET,
                                       CFA_ENET_TYPE_OR_LEN);
            u2VlanId = (OSIX_NTOHS (u2TCI)) & CFA_VLAN_VID_MASK;
            MEMSET (pSecVlanList, 0, sizeof (tSecVlanList));

            if (CfaGetSecVlanList (pSecVlanList) == CFA_FAILURE)
            {
                *pu1IsSecWantedForBridgedTraffic = (UINT1) bResult;
                UtilVlanReleaseVlanListSize (pSecVlanList);
                return;
            }
            OSIX_BITLIST_IS_BIT_SET (pSecVlanList, u2VlanId,
                                     sizeof (tSecVlanList), bResult);
            *pu1IsSecWantedForBridgedTraffic = (UINT1) bResult;

        }
    }
    UtilVlanReleaseVlanListSize (pSecVlanList);
    return;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetSecVlanList
 *
 *    Description         : This function returns the VLAN List for the 
 *                          Security interface.
 *
 *    Input(s)            : None. 
 *
 *    Output(s)           : VLAN List.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :  CFA_SUCCESS or CFA_FAILURE.
 *    
 *****************************************************************************/

INT1
CfaGetSecVlanList (tSecVlanList SecVlanList)
{
    CFA_DS_LOCK ();
    MEMCPY (SecVlanList, CFA_SEC_VLANLIST (), sizeof (tSecVlanList));
    CFA_DS_UNLOCK ();

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetSecurityBridgeMode                             */
/*                                                                           */
/* Description        : This function returns the status for Security for    */
/*                      Bridged Traffic                                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu4SecMode  - Pointer to Indicate Security Mode for  */
/*                                    bridged Traffic                        */
/* Global Variables                                                          */
/* Referred           : gu4SecModeForBridgedTraffic                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CfaGetSecurityBridgeMode (UINT4 *pu4SecMode)
{
    *pu4SecMode = gu4SecModeForBridgedTraffic;
    return;
}

/*****************************************************************************/
/* Function Name      : CfaGetSecIvrIndex                                    */
/*                                                                           */
/* Description        : This function returns the IfIndex of L3 Interface    */
/*                      used for security processing of Bridged Traffic      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu4SecIvrIfIndex  - Pointer to Indicate IfIndex of   */
/*                                          L3 Interface                     */
/* Global Variables                                                          */
/* Referred           : gu4SecIvrIfIndex                                     */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
UINT4
CfaGetSecIvrIndex (VOID)
{
    return gu4SecIvrIfIndex;
}

/*****************************************************************************/
/* Function Name      : CfaGetSecVlanId                                      */
/*                                                                           */
/* Description        : This function returns the Security VLAN ID associated*/
/*                      with the IVR interface index and MAC address.        */
/*                                                                           */
/* Input(s)           : u4IvrIndex - Security IVR index.                     */
/*                      MAC address - MAC address learnt through a Security  */
/*                                    VLAN.                                  */
/*                                                                           */
/* Output (s)         : VLAN ID - VLAN identifier bound  with the security   */
/*                      IVR index, through which the destination MAC address */
/*                      is learnt.                                           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gSecVlanList                                         */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value (s)   : None                                                 */
/*****************************************************************************/
INT4
CfaGetSecVlanId (UINT4 u4IvrIndex, tMacAddr pMacAddress, UINT2 *pVlanId)
{
    UINT1              *pSecVlanList = NULL;
    UINT2               u2VlanId = 0;
    UINT2               u2Port = 0;
    UINT1               u1Status = 0;
    BOOL1               bResult = OSIX_FALSE;

    if (gu4SecModeForBridgedTraffic == CFA_SEC_BRIDGE_DISABLED)
    {
        return CFA_FAILURE;
    }

    if (CfaGetSecIvrIndex () != u4IvrIndex)
    {
        return CFA_FAILURE;
    }

    /* Allocating memory for Vlan List Size */
    pSecVlanList = UtilVlanAllocVlanListSize (sizeof (tSecVlanList));
    if (pSecVlanList == NULL)
    {
        return CFA_FAILURE;
    }

    MEMSET (pSecVlanList, 0, CFA_SEC_VLAN_LIST_SIZE);

    if (CfaGetSecVlanList (pSecVlanList) == CFA_SUCCESS)
    {
        for (u2VlanId = 0; u2VlanId < CFA_MAX_VLAN_ID; u2VlanId++)
        {
            OSIX_BITLIST_IS_BIT_SET (gSecVlanList, u2VlanId,
                                     sizeof (tSecVlanList), bResult);
            if (OSIX_TRUE == bResult)
            {
                if (VLAN_SUCCESS == VlanGetFdbEntryDetails (u2VlanId,
                                                            pMacAddress,
                                                            &u2Port, &u1Status))
                {
                    if (VLAN_FDB_LEARNT == u1Status)
                    {
                        *pVlanId = u2VlanId;
                        /* Releasing memory for Vlan List Size */
                        UtilVlanReleaseVlanListSize (pSecVlanList);
                        return CFA_SUCCESS;
                    }
                }
            }
        }
    }
    /* Releasing memory for Vlan List Size */
    UtilVlanReleaseVlanListSize (pSecVlanList);
    return CFA_FAILURE;
}

#ifdef TLM_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaApiGetMplsIfFromTeLinkIf 
 *
 *    Description          : Getting MPLS Interface on which TE Link
 *                           Interface is stacked over
 *
 *    Input(s)             : u4TeLinkIf - TeLink Interface (200) 
 *                           bLockReq - Lock requested
 *                           TRUE - Needs to take CFA_LOCK ()
 *                           FALSE - Need not take CFA_LOCK ()
 *
 *    Output(s)            : pu4MplsIf - MPLS Interface 
 *
 *    Returns              : CFA_SUCCESS if succeeds, 
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaApiGetMplsIfFromTeLinkIf (UINT4 u4TeLinkIf, UINT4 *pu4MplsIf, BOOL1 bLockReq)
{
    tCfaIfInfo          IfInfo;

    *pu4MplsIf = CFA_NONE;
    MEMSET (&IfInfo, CFA_NONE, sizeof (tCfaIfInfo));

    CFA_FLAG_LOCK (bLockReq);

    if (CfaValidateCfaIfIndex (u4TeLinkIf) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaApiGetMplsIfFromTeLinkIf - "
                  "Invalid Interface Index - %d\n", u4TeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CfaGetIfInfo (u4TeLinkIf, &IfInfo) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetMplsIfFromTeLinkIf - "
                  "Fetching IfInfo failed for IfIndex - %d\n", u4TeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (IfInfo.u1IfType != CFA_TELINK)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetMplsIfFromTeLinkIf - "
                  "Invalid interface type for IfIndex - %d\n", u4TeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CFA_IF_ENTRY (u4TeLinkIf) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetMplsIfFromTeLinkIf - "
                  "Invalid Interface Index %d - \n", u4TeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CfaGetMplsIfFromTeLinkIf (u4TeLinkIf, pu4MplsIf) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetMplsIfFromTeLinkIf - "
                  "Fetching MPLS interface failed for TE Link If - %d \n",
                  u4TeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    CFA_FLAG_UNLOCK (bLockReq);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaApiGetL3IfFromTeLinkIf
 *
 *    Description          : Getting L3IPVLAN or Router port on which TE Link
 *                           Interface is stacked over
 *
 *    Input(s)             : u4TeLinkIf - TeLink Interface (200) 
 *                           bLockReq - Lock requested
 *                           TRUE - Needs to take CFA_LOCK ()
 *                           FALSE - Need not take CFA_LOCK ()
 *
 *    Output(s)            : pu4L3IfIndex - L3IPVLAN Interface or Router port 
 *
 *    Returns              : CFA_SUCCESS if succeeds, 
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaApiGetL3IfFromTeLinkIf (UINT4 u4TeLinkIf, UINT4 *pu4L3IfIndex,
                           BOOL1 bLockReq)
{
    tCfaIfInfo          IfInfo;

    *pu4L3IfIndex = CFA_NONE;
    MEMSET (&IfInfo, CFA_NONE, sizeof (tCfaIfInfo));

    CFA_FLAG_LOCK (bLockReq);

    if (CfaValidateCfaIfIndex (u4TeLinkIf) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaApiGetL3IfFromTeLinkIf - "
                  "Invalid Interface Index - %d\n", u4TeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CfaGetIfInfo (u4TeLinkIf, &IfInfo) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetL3IfFromTeLinkIf - "
                  "Fetching IfInfo failed for IfIndex - %d\n", u4TeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (IfInfo.u1IfType != CFA_TELINK)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetL3IfFromTeLinkIf - "
                  "Invalid interface type for IfIndex - %d\n", u4TeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CFA_IF_ENTRY (u4TeLinkIf) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetL3IfFromTeLinkIf - "
                  "Invalid Interface Index %d - \n", u4TeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CfaGetL3IfFromTeLinkIf (u4TeLinkIf, pu4L3IfIndex) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetL3IfFromTeLinkIf - "
                  "Fetching L3 interface failed for TE Link If - %d\n",
                  u4TeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    CFA_FLAG_UNLOCK (bLockReq);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaApiGetTeLinkIfFromL3If
 *
 *    Description          : Geting TE Link interface which is stacked over
 *                           L3 interface
 *
 *    Input(s)             : u4L3IpIntf - L3 Interface
 *                           u1Level    - Level at which TE Link If is required
 *                           bLockReq - Lock requested -
 *                           TRUE - Needs to take CFA_LOCK ()
 *                           FALSE - Need not take CFA_LOCK ()
 *
 *    Output(s)            : pu4TeLinkIf - TE Link Interface (200) 
 *
 *    Returns              : CFA_SUCCESS if succeeds, 
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaApiGetTeLinkIfFromL3If (UINT4 u4L3IpIntf, UINT4 *pu4TeLinkIf,
                           UINT1 u1Level, BOOL1 bLockReq)
{
    *pu4TeLinkIf = CFA_NONE;

    CFA_FLAG_LOCK (bLockReq);

    if (CfaValidateCfaIfIndex (u4L3IpIntf) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaApiGetTeLinkIfFromL3If - "
                  "Invalid Interface Index %d - FAIL\n", u4L3IpIntf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    /* Although Virtual Instance Ports are IfIndices, no entries are allocated  
     * in gapIfTable for them, hence if the port is VIP or SISP logical 
     * interfaces then return FAILURE 
     */

    if (CfaIsIfEntryProgrammingAllowed (u4L3IpIntf) == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaApiGetTeLinkIfFromL3If - "
                  "No TE Link is layered over Virtual Instance Port and SISP "
                  "Logical Interface Indices's %d - FAIL\r\n", u4L3IpIntf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CFA_IF_ENTRY (u4L3IpIntf) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaApiGetTeLinkIfFromL3If - "
                  "Invalid Interface Index %d - FAIL\n", u4L3IpIntf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CfaGetTeLinkIfFromL3If (u4L3IpIntf, u1Level, pu4TeLinkIf)
        == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaApiGetTeLinkIfFromL3If - "
                  "No TE Link Interface is stacked over this L3 If %d\n",
                  u4L3IpIntf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    CFA_FLAG_UNLOCK (bLockReq);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaApiGetHLTeLinkFromLLTeLink
 *
 *    Description          : Geting High TE Link interface which is stacked over
 *                           Lower TE Link interface
 *
 *    Input(s)             : u4LLTeLinkIf - LL TE Link Interface (200)
 *                           bLockReq - Lock requested
 *                           TRUE - Needs to take CFA_LOCK ()
 *                           FALSE - Need not take CFA_LOCK ()
 *
 *    Output(s)            : pu4HLTeLinkIf - TE Link Interface (200)
 *
 *    Returns              : CFA_SUCCESS if succeeds,
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaApiGetHLTeLinkFromLLTeLink (UINT4 u4LLTeLinkIf, UINT4 *pu4HLTeLinkIf,
                               BOOL1 bLockReq)
{
    tCfaIfInfo          IfInfo;
    UINT1               u1IfType = CFA_NONE;

    *pu4HLTeLinkIf = CFA_NONE;
    MEMSET (&IfInfo, CFA_NONE, sizeof (tCfaIfInfo));

    CFA_FLAG_LOCK (bLockReq);

    if (CfaValidateCfaIfIndex (u4LLTeLinkIf) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaApiGetHLTeLinkFromLLTeLink - "
                  "Invalid Interface Index - %d\n", u4LLTeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CfaGetIfInfo (u4LLTeLinkIf, &IfInfo) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetHLTeLinkFromLLTeLink - "
                  "Fetching IfInfo failed for IfIndex - %d\n", u4LLTeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (IfInfo.u1IfType != CFA_TELINK)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetHLTeLinkFromLLTeLink - "
                  "Invalid interface type for IfIndex - %d\n", u4LLTeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CFA_IF_ENTRY (u4LLTeLinkIf) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetHLTeLinkFromLLTeLink - "
                  "Invalid Interface Index %d - \n", u4LLTeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CfaGetHLIfFromLLIf (u4LLTeLinkIf, pu4HLTeLinkIf, &u1IfType)
        == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetHLTeLinkFromLLTeLink - "
                  "Unable to get Higher Layer TE Link If from Lower Layer "
                  "TE Link If %d - \n", u4LLTeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (u1IfType != CFA_TELINK)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetHLTeLinkFromLLTeLink - "
                  "Unable to get Higher Layer TE Link If from Lower Layer "
                  "TE Link If %d - \n", u4LLTeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);

        *pu4HLTeLinkIf = CFA_NONE;
        return CFA_FAILURE;
    }

    CFA_FLAG_UNLOCK (bLockReq);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaApiGetFirstLLTeLinkFromHLTeLink
 *
 *    Description          : Geting First Lower TE Link interface which is stacked below
 *                           Higher TE Link interface
 *
 *    Input(s)             : u4HLTeLinkIf - HL TE Link Interface (200)
 *                           bLockReq     - Lock requested
 *                           TRUE - Needs to take CFA_LOCK ()
 *                           FALSE - Need not take CFA_LOCK ()
 *
 *    Output(s)            : pu4LLTeLinkIf - TE Link Interface (200)
 *
 *    Returns              : CFA_SUCCESS if succeeds,
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaApiGetFirstLLTeLinkFromHLTeLink (UINT4 u4HLTeLinkIf, UINT4 *pu4LLTeLinkIf,
                                    BOOL1 bLockReq)
{
    return CfaApiGetNextLLTeLinkFromHLTeLink (u4HLTeLinkIf, 0, pu4LLTeLinkIf,
                                              bLockReq);
}

/*****************************************************************************
 *
 *    Function Name        : CfaApiGetNextLLTeLinkFromHLTeLink
 *
 *    Description          : Geting Next Lower TE Link interface of a specfic
 *                           Lower TE Link interface which is stacked below
 *                           Higher TE Link interface
 *
 *    Input(s)             : u4HLTeLinkIf     - HL TE Link Interface (200)
 *                           u4CurLLTeLinkIf  - Current LL TE Link If (200)
 *                           bLockReq         - Lock requested
 *                           TRUE - Needs to take CFA_LOCK ()
 *                           FALSE - Need not take CFA_LOCK ()
 *
 *    Output(s)            : pu4NextLLTeLinkIf - TE Link Interface (200)
 *
 *    Returns              : CFA_SUCCESS if succeeds,
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaApiGetNextLLTeLinkFromHLTeLink (UINT4 u4HLTeLinkIf, UINT4 u4CurLLTeLinkIf,
                                   UINT4 *pu4NextLLTeLinkIf, BOOL1 bLockReq)
{
    tTMO_SLL           *pIfStack = NULL;
    tStackInfoStruct   *pScanNode = NULL;
    UINT1               u1IfType = CFA_NONE;

    *pu4NextLLTeLinkIf = CFA_NONE;

    CFA_FLAG_LOCK (bLockReq);

    if (CfaValidateCfaIfIndex (u4HLTeLinkIf) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaApiGetNextLLTeLinkFromHLTeLink - "
                  "Invalid Interface Index - %d\n", u4HLTeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    CfaGetIfType (u4HLTeLinkIf, &u1IfType);

    if (u1IfType != CFA_TELINK)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetNextLLTeLinkFromHLTeLink - "
                  "Invalid interface type for IfIndex - %d\n", u4HLTeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CFA_IF_ENTRY (u4HLTeLinkIf) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetNextLLTeLinkFromHLTeLink - "
                  "Invalid Interface Index %d - \n", u4HLTeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    pIfStack = &CFA_IF_STACK_LOW (u4HLTeLinkIf);

    if (u4CurLLTeLinkIf == CFA_NONE)
    {
        pScanNode = (tStackInfoStruct *) TMO_SLL_First (pIfStack);
    }
    else
    {
        TMO_SLL_Scan (pIfStack, pScanNode, tStackInfoStruct *)
        {
            if (pScanNode->u4IfIndex == u4CurLLTeLinkIf)
            {
                pScanNode
                    = (tStackInfoStruct *) TMO_SLL_Next (pIfStack,
                                                         &pScanNode->NextEntry);
                break;
            }
        }
    }

    if (pScanNode == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetNextLLTeLinkFromHLTeLink - "
                  "No more Lower Stacking Available for IfIndex - %d\n",
                  u4HLTeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    u1IfType = CFA_NONE;

    CfaGetIfType (pScanNode->u4IfIndex, &u1IfType);

    if (u1IfType != CFA_TELINK)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetNextLLTeLinkFromHLTeLink - "
                  "No more Lower Stacking Available for IfIndex - %d\n",
                  u4HLTeLinkIf);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    *pu4NextLLTeLinkIf = pScanNode->u4IfIndex;

    CFA_FLAG_UNLOCK (bLockReq);
    return CFA_SUCCESS;
}

#endif /* TLM_WANTED */

#ifdef MPLS_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaApiGetTeLinkIfFromMplsTnlIf 
 *
 *    Description          : Getting TE Link If on which MPLS Tnl interface is
 *                           stacked over
 *
 *                           This API only gets the topmost TE Link Interface
 *                           (Bundled TE Link Interface). It does not get
 *                           the individual individual (component) TE Link
 *                           interfaces.
 *
 *    Input(s)             : u4MplsTnlIfIndex - MPLS Tnl Interface (150) 
 *                           bLockReq - Lock requested - 
 *                           TRUE - Needs to take CFA_LOCK ()
 *                           FALSE - Need not take CFA_LOCK ()
 *
 *    Output(s)            : pu4TeLinkIf - TE Link Interface (200) 
 *
 *    Returns              : CFA_SUCCESS if succeeds, 
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaApiGetTeLinkIfFromMplsTnlIf (UINT4 u4MplsTnlIfIndex, UINT4 *pu4TeLinkIf,
                                BOOL1 bLockReq)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4MplsIfIndex = 0;

    *pu4TeLinkIf = CFA_NONE;
    MEMSET (&IfInfo, CFA_NONE, sizeof (tCfaIfInfo));

    CFA_FLAG_LOCK (bLockReq);
    if (CfaValidateCfaIfIndex (u4MplsTnlIfIndex) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetTeLinkIfFromMplsTnlIf "
                  "Invalid Interface Index - %d\n", u4MplsTnlIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }
    if (CfaGetIfInfo (u4MplsTnlIfIndex, &IfInfo) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetTeLinkIfFromMplsTnlIf "
                  "Fetching IfInfo failed for IfIndex - %d\n",
                  u4MplsTnlIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;

    }
    if (IfInfo.u1IfType != CFA_MPLS_TUNNEL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaApiGetTeLinkIfFromMplsTnlIf "
                  "Invalid interface type for IfIndex - %d\n",
                  u4MplsTnlIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;

    }
    if (CFA_IF_ENTRY (u4MplsTnlIfIndex) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaApiGetTeLinkIfFromMplsTnlIf "
                  "Invalid Interface Index %d - \n", u4MplsTnlIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CfaGetMplsIfFromMplsTnlIf (u4MplsTnlIfIndex, &u4MplsIfIndex)
        == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaApiGetTeLinkIfFromMplsTnlIf "
                  "Unable to fetch MPLS Interface Index for %d\n",
                  u4MplsTnlIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CfaGetTeLinkIfFromMplsIf (u4MplsIfIndex, pu4TeLinkIf) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaApiGetTeLinkIfFromMplsTnlIf "
                  "Unable to fetch TE Link If from MPLS If %d\n",
                  u4MplsIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    CFA_FLAG_UNLOCK (bLockReq);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaApiGetTeLinkIfFromMplsIf 
 *
 *    Description          : Getting TE Link Interface on which MPLS interface
 *                           is stacked over
 *
 *    Input(s)             : u4MplsIfIndex - MPLS Interface (166) 
 *                           bLockReq - Lock requested - 
 *                           TRUE - Needs to take CFA_LOCK ()
 *                           FALSE - Need not take CFA_LOCK ()
 *
 *    Output(s)            : pu4TeLinkIf - TE Link Interface 
 *
 *    Returns              : CFA_SUCCESS if succeeds, 
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaApiGetTeLinkIfFromMplsIf (UINT4 u4MplsIfIndex, UINT4 *pu4TeLinkIf,
                             BOOL1 bLockReq)
{
    tCfaIfInfo          IfInfo;

    *pu4TeLinkIf = CFA_NONE;
    MEMSET (&IfInfo, CFA_NONE, sizeof (tCfaIfInfo));

    CFA_FLAG_LOCK (bLockReq);

    if (CfaValidateCfaIfIndex (u4MplsIfIndex) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaApiGetTeLinkIfFromMplsIf - "
                  "Invalid Interface Index - %d\n", u4MplsIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CfaGetIfInfo (u4MplsIfIndex, &IfInfo) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetTeLinkIfFromMplsIf "
                  "Fetching IfInfo failed for IfIndex - %d\n", u4MplsIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (IfInfo.u1IfType != CFA_MPLS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetTeLinkIfFromMplsIf "
                  "Invalid interface type for IfIndex - %d\n", u4MplsIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CFA_IF_ENTRY (u4MplsIfIndex) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetTeLinkIfFromMplsIf "
                  "Invalid Interface Index %d - \n", u4MplsIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    if (CfaGetTeLinkIfFromMplsIf (u4MplsIfIndex, pu4TeLinkIf) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "CfaApiGetTeLinkIfFromMplsIf "
                  "Fetching TE Link interface failed for MPLS IfIndex - %d\n",
                  u4MplsIfIndex);
        CFA_FLAG_UNLOCK (bLockReq);
        return CFA_FAILURE;
    }

    CFA_FLAG_UNLOCK (bLockReq);
    return CFA_SUCCESS;
}
#endif /* MPLS_WANTED */

/****************************************************************************
 *
 *    FUNCTION NAME    : CfaApiGetPortDesc
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to get the port name.
 *
 *    INPUT            : u4ContextId - Context ID.
 *                     : u4IfIndex   - Interface Index
 *    OUTPUT           : pPortDesc -PortDesc
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
CfaApiGetPortDesc (UINT4 u4IfIndex, tSNMP_OCTET_STRING_TYPE * pPortDesc)
{
    pPortDesc->i4_Length = (INT4) STRLEN (CFA_CDB_IF_DESCRIPTION (u4IfIndex));
    MEMCPY (pPortDesc->pu1_OctetList,
            CFA_CDB_IF_DESCRIPTION (u4IfIndex), pPortDesc->i4_Length);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CfaGetInterfaceBrgPortType
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to get the port type.
 *
 *    INPUT            : u4IfIndex   - Interface Index
 *    OUTPUT           : pi4BrgPortType  -PortType
 *
 *    RETURNS          : CFA_SUCCESS/CFA_FAILURE
 *
 ****************************************************************************/

INT4
CfaGetInterfaceBrgPortType (UINT4 u4IfIndex, INT4 *pi4BrgPortType)
{

    UINT1               u1BrgPortType = 0;

    if (CfaGetIfBrgPortType (u4IfIndex, &u1BrgPortType) == CFA_SUCCESS)
    {
        *pi4BrgPortType = (INT4) u1BrgPortType;
        return CFA_SUCCESS;
    }
    return CFA_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CfaApiGetHighIfIndex
 *
 *    DESCRIPTION      : This function is used to get the higher layer 
 *                       interface index.
 *
 *    INPUT            : u4IfIndex - Interface Index
 *    OUTPUT           : pu4HighIfIndex - Higher ifIndex
 *
 *    RETURNS          : CFA_SUCCESS/CFA_FAILURE
 *
 ****************************************************************************/

INT4
CfaApiGetHighIfIndex (UINT4 u4IfIndex, UINT4 *pu4HighIfIndex)
{
    CFA_LOCK ();
    if (CFA_IF_ENTRY_USED (u4IfIndex))
    {
        *pu4HighIfIndex = CFA_IF_HIGH_INDEX (u4IfIndex);
        CFA_UNLOCK ();
        return CFA_SUCCESS;

    }
    CFA_UNLOCK ();
    return CFA_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CfaApiCheckIfEntryExist
 *
 *    DESCRIPTION      : This function is used to check if ifEntry is used or
 *                       not
 *
 *    INPUT            : u4IfIndex - Interface Index
 *    OUTPUT           : None
 *
 *    RETURNS          : CFA_SUCCESS/CFA_FAILURE
 *
 ****************************************************************************/

INT4
CfaApiCheckIfEntryExist (UINT4 u4IfIndex)
{
    CFA_LOCK ();
    if (CFA_IF_ENTRY_USED (u4IfIndex))
    {
        CFA_UNLOCK ();
        return CFA_SUCCESS;

    }
    CFA_UNLOCK ();
    return CFA_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CfaApiGetIfAdminStatus
 *
 *    DESCRIPTION      : This function is used to get the interface admin
 *                       status
 *
 *    INPUT            : u4IfIndex - Interface Index
 *    OUTPUT           : pu1IfAdminStatus - Interface admin status
 *
 *    RETURNS          : CFA_SUCCESS/CFA_FAILURE
 *
 ****************************************************************************/

INT4
CfaApiGetIfAdminStatus (UINT4 u4IfIndex, UINT1 *pu1IfAdminStatus)
{
    CFA_LOCK ();
    if (CFA_IF_ENTRY_USED (u4IfIndex))
    {
        *pu1IfAdminStatus = CFA_IF_ADMIN (u4IfIndex);
        CFA_UNLOCK ();
        return CFA_SUCCESS;
    }
    CFA_UNLOCK ();
    return CFA_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CfaApiGetWanEntryPersist
 *
 *    DESCRIPTION      : This function is used to get the interface admin
 *                       status. Curretly called from PPP module. 
 *
 *    INPUT            : u4IfIndex - Interface Index
 *    OUTPUT           : pu1IfWanPersist - Wan interface persistence
 *
 *    RETURNS          : CFA_SUCCESS/CFA_FAILURE
 *
 ****************************************************************************/

INT4
CfaApiGetWanEntryPersist (UINT4 u4IfIndex, UINT1 *pu1IfWanPersist)
{
    CFA_LOCK ();
    if (CFA_IF_ENTRY_USED (u4IfIndex))
    {
        if (CFA_WAN_ENTRY (u4IfIndex))
        {
            *pu1IfWanPersist = CFA_IF_PERST (u4IfIndex);
            CFA_UNLOCK ();
            return CFA_SUCCESS;
        }
    }
    CFA_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCreateAndWaitPhysicalInterface                  */
/*                                                                           */
/*     DESCRIPTION      : This is API to create an entry in IF Port table.   */
/*                        It keeps the port entry row status as in NOT READY */
/*                        state. This API is invoked from VLAN module        */
/*     INPUT            : pu1IfName - Porrt IfIndex name                     */
/*                      : pu1IfNum -  Port number (slotId/portId)            */
/*                      : pu4IfIndex - Port IfIndex value                    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS/CFA_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCreateAndWaitPhysicalInterface (UINT1 *pu1IfName,
                                   UINT1 *pu1IfNum, UINT4 *pu4IfIndex)
{

    UINT4               u4ErrCode = 0;
    tSNMP_OCTET_STRING_TYPE Alias;
    UINT4               u4IfIndex = 0;

    if ((pu1IfName == NULL) || (pu1IfNum == NULL) || (pu4IfIndex == NULL))
    {
        return CFA_FAILURE;
    }

    MEMSET (&Alias, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    CFA_LOCK ();
    if (CfaGetIfIndexFromIfNameAndIfNum (pu1IfName, pu1IfNum,
                                         pu4IfIndex) != CLI_SUCCESS)
    {
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }

    u4IfIndex = *pu4IfIndex;

    if (nmhTestv2IfMainRowStatus (&u4ErrCode, (INT4) u4IfIndex,
                                  CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return (CFA_FAILURE);
    }

    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_CREATEANDWAIT) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return (CFA_FAILURE);
    }

    Alias.i4_Length = (INT4) STRLEN (pu1IfName);
    Alias.pu1_OctetList = pu1IfName;

    nmhSetIfAlias (u4IfIndex, &Alias);

    if (nmhTestv2IfMainType (&u4ErrCode, (INT4) u4IfIndex, CFA_ENET) ==
        SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CFA_UNLOCK ();
        return (CFA_FAILURE);
    }

    if (nmhSetIfMainType (u4IfIndex, CFA_ENET) == SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CFA_UNLOCK ();
        return (CFA_FAILURE);
    }
    CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);
    CFA_UNLOCK ();

    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaActivatePhysicalInterface                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets the ifIndex entry created       */
/*                        externally to active. It is invoked from VLAN      */
/*     INPUT            : u4IfIndex - Port IfIndex                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS/CFA_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaActivatePhysicalInterface (UINT4 u4IfIndex)
{
    UINT4               u4ErrCode = 0;

    CFA_LOCK ();

    if (nmhTestv2IfMainRowStatus (&u4ErrCode, (INT4) u4IfIndex,
                                  CFA_RS_ACTIVE) == SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CFA_UNLOCK ();
        return (CFA_FAILURE);
    }

    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_ACTIVE) == SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CFA_UNLOCK ();
        return (CFA_FAILURE);
    }
    if (nmhSetIfMainAdminStatus (u4IfIndex, CFA_IF_UP) == SNMP_FAILURE)
    {
        nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY);
        CFA_UNLOCK ();
        return (CFA_FAILURE);
    }

    CFA_UNLOCK ();

    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaGetInterfaceNameFromIfType                      */
/*                                                                           */
/*     DESCRIPTION      : This function fetches the interface name for a     */
/*                        interface IfType. It is invoked from VLAN Module   */
/*     INPUT            : u4IfType - IfType for which ifName to be returned  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS/CFA_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaGetInterfaceNameFromIfType (INT1 *pi1IfName, UINT4 u4IfType)
{
    INT4                i4Index = 0;
#define CFA_CLI_MAX_IFTYPES   6
    tCfaIfaceInfo       ai1IfTypes[CFA_CLI_MAX_IFTYPES] = {
#ifdef CFA_UNIQUE_INTF_NAME
        {CFA_GI_ENET, "ethernet", "0"}
        ,
#else
        {CFA_GI_ENET, "gigabitethernet", "0"}
        ,
#endif
        {CFA_FA_ENET, "fastethernet", "0"}
        ,
        {CFA_XE_ENET, "extreme-ethernet", "0"}
        ,
        {CFA_XL_ENET, "XL-ethernet", "0"}
        ,
        {CFA_LVI_ENET, "LVI-ethernet", "0"}
        ,
        {CFA_ILAN, "i-lan", "0"}
    };

    if (!(pi1IfName))
    {
        return CFA_FAILURE;
    }

    for (i4Index = 0; i4Index < CFA_CLI_MAX_IFTYPES; i4Index++)
    {
        if (u4IfType == ai1IfTypes[i4Index].u4IfType)
        {
            STRNCPY (pi1IfName, ai1IfTypes[i4Index].au1IfName,
                     STRLEN (ai1IfTypes[i4Index].au1IfName));
            return CFA_SUCCESS;
        }
    }
    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaDeleteInterfaceExt                              */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the given interface          */
/*                        It is invoked from VLAN module                     */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaDeleteInterfaceExt (UINT4 u4IfIndex)
{
    UINT4               u4ErrCode = 0;

    CFA_LOCK ();
    if (nmhTestv2IfMainRowStatus (&u4ErrCode, (INT4) u4IfIndex,
                                  CFA_RS_DESTROY) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return (CFA_FAILURE);
    }

    if (nmhSetIfMainRowStatus (u4IfIndex, CFA_RS_DESTROY) != SNMP_SUCCESS)
    {
        CFA_UNLOCK ();
        return (CFA_FAILURE);
    }

    CFA_UNLOCK ();
    return (CFA_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaGetRmConnectingPortIfIndex                      */
/*                                                                           */
/*     DESCRIPTION      : This function returns the ISS If-Index of the      */
/*                        connecting port used in Dual Unit Stacking         */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : pu4ConnectingPortIfIndex - If-Index of the         */
/*                        connecting port used in Dual Unit Stacking         */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
INT4
CfaGetRmConnectingPortIfIndex (UINT4 *pu4ConnectingPortIfIndex)
{
    *pu4ConnectingPortIfIndex =
        gLocalUnitHwPortInfo.au4ConnectingPortIfIndex[0];
    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetLocalUnitPortInformation                     */
/*                                                                           */
/*     DESCRIPTION      : This function is called from MBSM module to        */
/*                        store the local unit port information              */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : pu4ConnectingPortIfIndex - If-Index of the         */
/*                        connecting port used in Dual Unit Stacking         */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
VOID
CfaSetLocalUnitPortInformation (tHwPortInfo * pHwPortInfo)
{
    MEMCPY (&gLocalUnitHwPortInfo, pHwPortInfo, sizeof (tHwPortInfo));
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaGetLocalUnitPortInformation                     */
/*                                                                           */
/*     DESCRIPTION      : This function is called to fetch the               */
/*                        local unit port information of the current system  */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : pu4ConnectingPortIfIndex - If-Index of the         */
/*                        connecting port used in Dual Unit Stacking         */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
VOID
CfaGetLocalUnitPortInformation (tHwPortInfo * pHwPortInfo)
{
    MEMCPY (pHwPortInfo, &gLocalUnitHwPortInfo, sizeof (tHwPortInfo));
}

/*****************************************************************************/
/* Function Name      : CfaGetShowCmdOutputAndCalcChkSum                     */
/*                                                                           */
/* Description        : This funcion handles the execution of show commands  */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu2SwAudChkSum                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCESS/MBSM_FAILURE                             */
/*****************************************************************************/
INT4
CfaGetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum)
{
#if (defined CLI_WANTED && defined RM_WANTED)
    if (RmGetNodeState () == RM_ACTIVE)
    {
        if (CfaCliGetShowCmdOutputToFile ((UINT1 *) CFA_AUDIT_FILE_ACTIVE) !=
            CFA_SUCCESS)
        {
            CFA_DBG (ALL_FAILURE_TRC, CFA_MAIN, "GetShRunFile Failed\n");
            return CFA_FAILURE;
        }
        if (CfaCliCalcSwAudCheckSum
            ((UINT1 *) CFA_AUDIT_FILE_ACTIVE, pu2SwAudChkSum) != CFA_SUCCESS)
        {
            CFA_DBG (ALL_FAILURE_TRC, CFA_MAIN, "CalcSwAudChkSum Failed\n");
            return CFA_FAILURE;
        }
    }
    else if (RmGetNodeState () == RM_STANDBY)
    {
        if (CfaCliGetShowCmdOutputToFile ((UINT1 *) CFA_AUDIT_FILE_STDBY) !=
            CFA_SUCCESS)
        {
            CFA_DBG (ALL_FAILURE_TRC, CFA_MAIN, "GetShRunFile Failed\n");
            return CFA_FAILURE;
        }
        if (CfaCliCalcSwAudCheckSum
            ((UINT1 *) CFA_AUDIT_FILE_STDBY, pu2SwAudChkSum) != CFA_SUCCESS)
        {
            CFA_DBG (ALL_FAILURE_TRC, CFA_MAIN, "CalcSwAudChkSum Failed\n");
            return CFA_FAILURE;
        }
    }
    else
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_MAIN,
                 "Node is neither active nor standby\n");
        return CFA_FAILURE;
    }
#else
    UNUSED_PARAM (pu2SwAudChkSum);
#endif
    return CFA_SUCCESS;
}

#ifdef MPLS_WANTED
/****************************************************************************
 *
 *    FUNCTION NAME    : CfaApiGetFirstMplsIfInfo
 *
 *    DESCRIPTION      : This function is used to get the first valid MPLS or
 *                       MPLS Tunnel interface index and their related
 *                       interface parameters
 *                  1. OperStatus
 *                  2. VlanId
 *                  3. MacAddress.
 *
 * Input(s)       : None.
 *
 * Output(s)      : pu4IfIndex - Interface index of first valid MPLS or MPLS Tunnel interface.
 *                  pIfInfo - Interface Info structure.
 *
 * Returns        : CFA_SUCCESS/FAILURE
 *
 ****************************************************************************/

INT4
CfaApiGetFirstMplsIfInfo (UINT4 *pu4IfIndex, tCfaIfInfo * pIfInfo)
{
    INT4                i4RetVal = CFA_FAILURE;

    CFA_LOCK ();
    i4RetVal = CfaCdbGetFirstMplsIfInfo (pu4IfIndex, pIfInfo);
    CFA_UNLOCK ();
    return i4RetVal;
}

/******************************************************************************
 * Function       : CfaApiGetNextMplsIfInfo                                                                        *
 * Description    : This function will return the next valid MPLS or MPLS Tunnel interface                         *                  index and their related interface parameters
 *                  1. OperStatus                                                                                  *                  2. VlanId
 *                  3. MacAddress.                                                                                 *
 * Input(s)       : u4IfIndex                                                                                      *
 * Output(s)      : pIfInfo - Interface Info structure.                                                            *                  pu4NextIndex - Interface index of next valid MPLS or MPLS Tunnel interface.
 *                                                                                                                 * Returns        : CFA_SUCCESS/FAILURE
 *                                                                                                                 ******************************************************************************/

INT4
CfaApiGetNextMplsIfInfo (UINT4 u4IfIndex, UINT4 *pu4NextIndex,
                         tCfaIfInfo * pIfInfo)
{
    INT4                i4RetVal = CFA_FAILURE;
    CFA_LOCK ();
    i4RetVal = CfaCdbGetNextMplsIfInfo (u4IfIndex, pu4NextIndex, pIfInfo);
    CFA_UNLOCK ();
    return i4RetVal;
}
#endif /*MPLS_WANTED */
/*****************************************************************************
 *
 *    Function Name       : CfaMsrGetIfName
 *
 *    Description         : This function is used to get the Interface name 
 *                          for given interface index.
 *
 *    Input(s)            : Interface index
 *
 *
 *    Output(s)           : pi1IfName - Interface name of the given 
 *                          interface index.
 *
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion    : None.
 *
 *    Returns             : CLI_SUCCESS or CLI_FAILURE.
 *****************************************************************************/
INT4
CfaMsrGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    INT4                i4Status = CLI_SUCCESS;
    UINT4               u4L2CxtId = 0;
    UINT1               u1IfType;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    if (!(pi1IfName))
    {
        return CLI_FAILURE;
    }

    if (u4IfIndex == 0)
    {
        CLI_STRNCPY (pi1IfName, "0", STRLEN ("0"));
        pi1IfName[STRLEN ("0")] = '\0';
        return CLI_SUCCESS;
    }

    if (CfaIsMgmtPort (u4IfIndex) == TRUE)
    {
        STRNCPY (au1IfName, OOB_ALIAS_NAME,
                 sizeof (au1IfName) - CFA_STR_DELIM_SIZE);
        au1IfName[sizeof (au1IfName) - 1] = '\0';
        SNPRINTF ((CHR1 *) pi1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s",
                  au1IfName);
        return CLI_SUCCESS;
    }

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CLI_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_FALSE)
    {
        CFA_DS_UNLOCK ();
        return CLI_FAILURE;
    }

    u1IfType = CFA_CDB_IF_TYPE (u4IfIndex);
    STRNCPY (au1IfName, CFA_CDB_IF_ALIAS (u4IfIndex),
             sizeof (au1IfName) - CFA_STR_DELIM_SIZE);
    au1IfName[sizeof (au1IfName) - 1] = '\0';

    CFA_DS_UNLOCK ();

    /* Process ifType and form the IfName */
    switch (u1IfType)
    {
            /* Need to port the function CfaGetPhysIfName 
             * for different Interface Names, if supported 
             * that may be based on some parameters of that interface
             */
        case CFA_OTHER:
        case CFA_ENET:
            if (CfaGetPhysIfName (u4IfIndex, pi1IfName) == CLI_FAILURE)
            {
                i4Status = CLI_FAILURE;
            }
            break;
        case CFA_L3IPVLAN:
        case CFA_L3SUB_INTF:

            if (VcmGetL2CxtIdForIpIface ((UINT4) u4IfIndex, &u4L2CxtId)
                == VCM_FAILURE)
            {
                return CLI_FAILURE;
            }

            SNPRINTF ((CHR1 *) pi1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s",
                      au1IfName);
            break;
        case CFA_LOOPBACK:
        case CFA_L2VLAN:
            SNPRINTF ((CHR1 *) pi1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s",
                      au1IfName);
            break;
        case CFA_LAGG:
        case CFA_PIP:
        case CFA_BRIDGED_INTERFACE:
        case CFA_ILAN:
        case CFA_PROP_VIRTUAL_INTERFACE:
            /* Intentional Fall through */
            SNPRINTF ((CHR1 *) pi1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s",
                      au1IfName);
            break;

        case CFA_PPP:
        case CFA_TUNNEL:
        case CFA_MPLS:
        case CFA_MPLS_TUNNEL:
        case CFA_VPNC:
        case CFA_TELINK:
        case CFA_PSEUDO_WIRE:
            SNPRINTF ((CHR1 *) pi1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s",
                      au1IfName);
            break;

#if defined (WLC_WANTED) || defined (WTP_WANTED)
        case CFA_RADIO:
        case CFA_WLAN_RADIO:
        case CFA_CAPWAP_VIRT_RADIO:
        case CFA_CAPWAP_DOT11_PROFILE:
        case CFA_CAPWAP_DOT11_BSS:
            SNPRINTF ((CHR1 *) pi1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s",
                      au1IfName);
            break;
#endif

        default:
            i4Status = CLI_FAILURE;
            break;
    }

    return i4Status;
}

/*****************************************************************************/
/* Function Name      : CfaIsPortVlanExists                                   */
/*                                                                           */
/* Description        : This function function is used to check whether the  */
/*                      the VLAN ID passed is already used by cfa to achieve  */
/*                      Router-Ports or not.                                 */
/*                                                                           */
/* Input(s)           : u2PortVlanId  :VLAN ID which is to be checked        */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
INT4
CfaIsPortVlanExists (UINT2 u2PortVlanId)
{
    tIpIfRecord        *pIpIntf = NULL;

    CFA_IPIFTBL_LOCK ();
    pIpIntf = RBTreeGetFirst (gIpIfInfo.pIpIfTable);

    while (pIpIntf != NULL)
    {
        if (pIpIntf->u2PortVlanId == u2PortVlanId)
        {
            CFA_IPIFTBL_UNLOCK ();
            return OSIX_SUCCESS;
        }

        pIpIntf = RBTreeGetNext (gIpIfInfo.pIpIfTable,
                                 (tRBElem *) pIpIntf, NULL);
    }                            /* End of while */

    CFA_IPIFTBL_UNLOCK ();
    return OSIX_FAILURE;
}

#ifdef LNXIP4_WANTED
#ifdef VRF_WANTED

UINT4
CfaUpdatePortDescforIndex (UINT4 u4IfIndex, INT4 i4PortDesc)
{
    UINT1               u1IfType = 0;
    CfaGetIfType (u4IfIndex, &u1IfType);
    if (u1IfType != CFA_LAGG)
    {
        close (CFA_GDD_PORT_DESC (u4IfIndex));
        CFA_LOCK ();
        CFA_GDD_PORT_DESC (u4IfIndex) = i4PortDesc;
        CFA_UNLOCK ();
    }
    return CFA_SUCCESS;

}

UINT4
CfaVrfUpdateInterfaceSockInfo (UINT4 u4IfIndex, INT4 i4PortDesc)
{

    struct sockaddr_ll  Enet;
    struct ifreq        ifr;
    INT1                ai1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1              *pu1PortName = NULL;
    MEMSET (&Enet, 0, sizeof (Enet));
    MEMSET (&ifr, 0, sizeof (ifr));
    MEMSET (ai1IfName, 0, sizeof (ai1IfName));

    if (u4IfIndex > SYS_DEF_MAX_INTERFACES)
    {
        return CFA_FAILURE;
    }

    tCfaIfInfo          CfaIfInfo;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if (CfaGetIfInfo (u4IfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }

    pu1PortName = CfaIfInfo.au1IfName;

    /*If No Mapping Exists,No Such Physical Interface can Exists.So Just
     *      *        return FAILURE */
    if (pu1PortName == NULL)
    {
        printf ("Invalid Device Name Configuration \n");
        return (CFA_FAILURE);
    }

    sprintf (ifr.ifr_name, "%s", pu1PortName);

    if (ioctl (i4PortDesc, SIOCGIFINDEX, (char *) &ifr) < 0)
    {
        perror ("SIOCGIFINDEX:");
        close (i4PortDesc);
        return CFA_FAILURE;
    }

    Enet.sll_family = AF_PACKET;
    Enet.sll_ifindex = ifr.ifr_ifindex;
    if (bind (i4PortDesc, (struct sockaddr *) &Enet, sizeof (Enet)) < 0)
    {
        perror ("Bind error\n");
        close (i4PortDesc);
        return CFA_FAILURE;
    }

    /* set options for non-blocking mode */
    if (fcntl (i4PortDesc, F_SETFL, O_NONBLOCK) < 0)
    {
        perror ("Failure in setting ETH SOCK option\n");
        close (i4PortDesc);
    }

    KW_FALSEPOSITIVE_FIX3 (i4PortDesc);
    CfaUpdatePortDescforIndex (u4IfIndex, i4PortDesc);
    return (CFA_SUCCESS);

}
#endif
#endif /* LNXIP4_WANTED */
/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : CfaCreateIcchIPInterface                                */
/*                                                                            */
/* DESCRIPTION      : This function creates a port channel interface after    */
/*                    reading it from the iccl.conf. In addition to this      */
/*                    the function does the following.                        */
/*                    if ICCL IP interface is mentioned as po<x> in iccl.conf,*/
/*                    then                                                    */
/*                    1. po<x> is created in the system. [With admin up]      */
/*                    2. vlan <x> is created with po<x> as member port.       */
/*                    3. interface vlan <x> is created in the system.         */
/*                    4. IP address is read from iccl.conf and is assigned to */
/*                       the L3IPVLAN interface.[admin up]                    */
/*                                                                            */
/* INPUT            : u2InstanceId - ICCL instance ID                         */
/*                                                                            */
/* OUTPUT           : NONE                                                    */
/*                                                                            */
/* RETURNS          : CFA_SUCCESS / CFA_FAILURE                               */
/*                                                                            */
/******************************************************************************/
INT4
CfaCreateIcchIPInterface (UINT2 u2InstanceId)
{
    tIpConfigInfo       IpCfgInfo;
    UINT1               au1L3IcchVlan[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1IcchInterface[CFA_MAX_PORT_NAME_LENGTH];
    tCfaIfInfo         *pCfaIfInfo = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4IpAddress = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4SubnetMask = 0;
    UINT4               u4IcchIfIndex = 0;
    UINT4               u4Flag = 0;
    UINT4               u4IcchVlan = 0;
    INT4                i4OperCode = 0;
    CHR1               *pIcclIntfName = NULL;

    MEMSET (&IpCfgInfo, 0, sizeof (tIpConfigInfo));
    MEMSET (au1L3IcchVlan, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1IcchInterface, 0, CFA_MAX_PORT_NAME_LENGTH);

    pIcclIntfName = CfaIcchApiGetIcclIpInterface (u2InstanceId);
    if (pIcclIntfName != NULL)
    {
        STRNCPY (au1IcchInterface, pIcclIntfName,
                 MEM_MAX_BYTES (STRLEN (pIcclIntfName),
                                CFA_MAX_PORT_NAME_LENGTH - 1));
    }

    /* ICCL interface should be a port-channel interface */
    if (STRNCMP (au1IcchInterface, "po", 2) != 0)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "ICCH Interface is not a port channel interface, .\n");
        return CFA_FAILURE;
    }

    u4IcchVlan = CfaIcchApiGetIcclVlanId (u2InstanceId);

    /* Create the port channel interface ICCH_INTERFACE as specified in iccl.conf */
    if (CfaGetInterfaceIndexFromName (au1IcchInterface, &u4IfIndex)
        == OSIX_FAILURE)
    {
        if (CfaGetFreeInterfaceIndex (&u4IfIndex, CFA_LAGG) == OSIX_SUCCESS)
        {
#ifdef CLI_WANTED
            if (CfaCreatePortChannelInterface (0, u4IfIndex, au1IcchInterface)
                == OSIX_FAILURE)
#endif
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                          "ICCH interface creation failed %d.\n", u4IfIndex);
                CFA_DS_LOCK ();
                pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
                if (pCfaIfInfo != NULL)
                {
                    CFA_CDB_SET_INTF_VALID (u4IfIndex, CFA_FALSE);

                    CFA_DS_UNLOCK ();

                    /* This means that the node exists and since we
                     * incurred a failure above, we will remove the node
                     * otherwise system will be in an unstable state */
                    CfaIfUtlIfDbRem (u4IfIndex, CFA_CDB_IFDB_REQ);
                }
                else
                {
                    CFA_DS_UNLOCK ();
                }
                return CFA_FAILURE;
            }
        }
    }

    CfaIcchSetIcclIfIndex (u4IfIndex);
    u4IcchIfIndex = u4IfIndex;

    if (CfaSetIfName (u4IfIndex, au1IcchInterface) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "ICCH Interface name setting is failed %d\n", u4IfIndex);
        CfaDeleteIcchIPInterface (u4IcchIfIndex, u4IfIndex, u4IcchVlan);
        return CFA_FAILURE;
    }

    /* Map the interface to the input context */
    VcmCfaCreateIfaceMapping (L2IWF_DEFAULT_CONTEXT, u4IfIndex);

    L2IwfPortCreateIndication (u4IfIndex);

#ifdef CLI_WANTED
    /* Make the port channel interface administratively up */
    if (CfaSetInterfaceAdminStatus (0, u4IfIndex, CFA_IF_UP) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "ICCH L3 VLAN admin up failed %d\n", u4IfIndex);
        CfaDeleteIcchIPInterface (u4IcchIfIndex, u4IfIndex, u4IcchVlan);
        return CFA_FAILURE;
    }
#endif

    /* Create VLAN <x>, corresponding to po<x> and make po<x> as tagged member of 
     * it. 
     */
    if (VlanApiUpdateStaticVlanInfo
        (u4ContextId, (tVlanId) u4IcchVlan, u4IfIndex,
         VLAN_CREATE) == VLAN_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "ICCH VLAN creation failed  %d\n", u4IcchVlan);
        CfaDeleteIcchIPInterface (u4IcchIfIndex, u4IfIndex, u4IcchVlan);
        return CFA_FAILURE;
    }

    /* Create an L3 IPVLAN over vlan<x> and assign to it. */

    SPRINTF ((CHR1 *) au1L3IcchVlan, "vlan%d", u4IcchVlan);
    if (CfaGetInterfaceIndexFromName (au1L3IcchVlan, &u4IfIndex)
        == OSIX_FAILURE)
    {
        if (CfaGetFreeInterfaceIndex ((UINT4 *) &u4IfIndex, CFA_L3IPVLAN)
            == OSIX_SUCCESS)
        {
            if (CfaIfmCreateAndInitIfEntry (u4IfIndex,
                                            CFA_L3IPVLAN, au1L3IcchVlan)
                == CFA_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                         "Error in CfaCreateIcchIPInterface"
                         "Fail - interface creation - FAILURE.\n");
                CfaDeleteIcchIPInterface (u4IcchIfIndex, u4IfIndex, u4IcchVlan);
                return CFA_FAILURE;
            }

            if (CfaSetIfAlias (u4IfIndex, au1L3IcchVlan) == CFA_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                         "Error in CfaCreateIcchIPInterface"
                         "Fail - interface alias name  - FAILURE.\n");
                CfaDeleteIcchIPInterface (u4IcchIfIndex, u4IfIndex, u4IcchVlan);
                return CFA_FAILURE;
            }

            /* make Rowstatus active */
            CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;

            if (CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE) == CFA_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                         "Error in CfaCreateIcchIPInterface"
                         "Fail - interface active status - FAILURE.\n");
                CfaDeleteIcchIPInterface (u4IcchIfIndex, u4IfIndex, u4IcchVlan);
                return CFA_FAILURE;
            }

            CfaSetCdbRowStatus (u4IfIndex, CFA_RS_ACTIVE);

            if (VcmCfaCreateIPIfaceMapping (VCM_DEFAULT_CONTEXT, u4IfIndex) ==
                VCM_FAILURE)
            {
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "CfaCreateIcchIPInterface - "
                         "Def interface mapping in VCM - FAIL.\n");
                CfaDeleteIcchIPInterface (u4IcchIfIndex, u4IfIndex, u4IcchVlan);
                return CFA_FAILURE;
            }
            /* ICCL IP address does not exist. So, assign new IP address */
            i4OperCode = CFA_NET_IF_NEW;
        }
    }
    else
    {
        /* ICCL IP address exists already. So, update the IP address */
        i4OperCode = CFA_NET_IF_UPD;
    }

    u4SubnetMask = CfaIcchApiGetIcclIpMask (u2InstanceId);
    u4IpAddress = CfaIcchApiGetIcclIpAddr (u2InstanceId);
    IpCfgInfo.u4Addr = u4IpAddress;
    IpCfgInfo.u4NetMask = u4SubnetMask;
    IpCfgInfo.u4BcastAddr = (~u4SubnetMask | u4IpAddress);
    IpCfgInfo.u1IpForwardEnable = CFA_ENABLED;
    IpCfgInfo.u1PrefixLen = 0;
    IpCfgInfo.u1AddrAllocMethod = CFA_IP_ALLOC_MAN;

    /* Set all the IP interface params */
    u4Flag = CFA_IP_IF_ALLOC_PROTO | CFA_IP_IF_ALLOC_METHOD |
        CFA_IP_IF_PRIMARY_ADDR | CFA_IP_IF_NETMASK |
        CFA_IP_IF_FORWARD | CFA_IP_IF_BCASTADDR;

    /* Create mapping in VCM */
    if (CfaIfmConfigNetworkInterface ((UINT1) i4OperCode, u4IfIndex,
                                      u4Flag, &IpCfgInfo) == CFA_FAILURE)
    {

        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "CfaCreateIcchIPInterface - "
                 "Def interface registration with IP - FAIL.\n");
        CfaDeleteIcchIPInterface (u4IcchIfIndex, u4IfIndex, u4IcchVlan);
        return CFA_FAILURE;
    }

    if (CfaIpIfSetIfInfo (u4IfIndex, u4Flag, &IpCfgInfo) == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "CfaCreateIcchIPInterface - "
                 "Setting IP related info to interface - FAIL.\n");
        CfaDeleteIcchIPInterface (u4IcchIfIndex, u4IfIndex, u4IcchVlan);
        return CFA_FAILURE;
    }
    CfaIcchSetIcclL3IfIndex (u4IfIndex);

#ifdef CLI_WANTED
    /* Make the L3 VLAN interface administratively up */
    if (CfaSetInterfaceAdminStatus (0, u4IfIndex, CFA_IF_UP) == CFA_FAILURE)
    {
        CfaDeleteIcchIPInterface (u4IcchIfIndex, u4IfIndex, u4IcchVlan);
        return CFA_FAILURE;
    }
#endif
    return CFA_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : CfaDeleteIcchIPInterface                                */
/*                                                                            */
/* DESCRIPTION      : This function de-init the ICCL  port channel interface  */
/*                    if ICCH IP interface is mentioned as po<x> in iccl.conf,*/
/*                    then                                                    */
/*                    1. po<x> is deleted in the system. [With admin up]      */
/*                    2. vlan <x> is deleted.                                 */
/*                    3. interface vlan <x> is deleted in the system.         */
/*                                                                            */
/* INPUT            : u4IcchIfIndex -ICCL interface index.                    */
/*                    u4L3IfIndex   - ICCL L3 interface index.                */
/*                    u4VlanId      - ICCL VLAN.                              */
/*                                                                            */
/* OUTPUT           : NONE                                                    */
/*                                                                            */
/* RETURNS          : None                                                    */
/*                                                                            */
/******************************************************************************/
VOID
CfaDeleteIcchIPInterface (UINT4 u4IcchIfIndex, UINT4 u4L3IfIndex,
                          UINT4 u4VlanId)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    if ((u4VlanId == 0) && (u4IcchIfIndex != 0))
    {
        /* Derive the local port information corresponding to this CFA index */
        if (VcmGetContextInfoFromIfIndex
            (u4IcchIfIndex, &u4ContextId, &u2LocalPort) == VCM_FAILURE)
        {
            CFA_DBG2 (CFA_TRC_ERROR, CFA_IFM,
                      "Unable to get context from inteface index %d %d.\n",
                      u4ContextId, u4IcchIfIndex);
        }
        /* Delete VLAN <x>, corresponding to po<x> */
        if (VlanApiUpdateStaticVlanInfo (u4ContextId, (tVlanId) u4VlanId,
                                         u4IcchIfIndex,
                                         VLAN_DELETE) == VLAN_FAILURE)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "ICCH VLAN deletion failed  %d\n", u4VlanId);
        }
    }

#ifdef CLI_WANTED
    if (u4IcchIfIndex != 0)
    {
        if (OSIX_FAILURE == CfaDeleteInterface (0, u4IcchIfIndex))
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Failed to delete the ICCH interface  %d\n",
                      u4IcchIfIndex);
        }
    }

    if (u4L3IfIndex != 0)
    {
        if (OSIX_FAILURE == CfaDeleteInterface (0, u4L3IfIndex))
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Failed to delete the ICCH L3 interface %d\n",
                      u4L3IfIndex);
        }
    }
#else
    UNUSED_PARAM (u4L3IfIndex);
#endif

    return;
}

#ifdef DHCPC_WANTED
/*****************************************************************************/
/* Function Name      : CfaDhcpReleaseUpdate                                 */
/*                                                                           */
/* Description        : This function function is used to check whether the  */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
CfaDhcpReleaseUpdate (UINT4 u4IfIndex)
{
    UINT1               u1IsAlreadyRegWithIp = CFA_FALSE;

    if (CFA_IF_RS (u4IfIndex) == CFA_RS_ACTIVE)
    {
        if (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX)
        {
            u1IsAlreadyRegWithIp = CFA_TRUE;
        }
    }

    if (u1IsAlreadyRegWithIp == CFA_TRUE)
    {
        if (CfaIfmConfigNetworkInterface (CFA_NET_IF_UPD, u4IfIndex,
                                          0, NULL) != CFA_SUCCESS)
        {
            CLI_SET_ERR (CFA_CLI_IFM_CONFIG_ERR);
            return;
        }
    }
    return;
}
#endif

/*****************************************************************************
 *
 *    Function Name       : CfaCreateSChannelInterface
 *
 *    Description         : This function creates S-Channel interface in CFA
 *                          and thereby indicating to L2IWF to create the 
 *                          S-Channel interface to VLAN, IGMP Snooping and 
 *                          FIP Snooping.
 *                          
 *    Input(s)            : u4IfIndex - S-Channel interface index.
 *                          u4UapIfIndex - UAP interface index.
 *
 *    Output(s)           : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE
 *
 *****************************************************************************/
INT4
CfaCreateSChannelInterface (UINT4 u4UapIfIndex, UINT4 u4IfIndex, UINT1 u1Status)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return (CFA_FAILURE);
    }

    CFA_DS_LOCK ();
    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        /* Already interface is present */
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    if (CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_TRUE)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_DS_UNLOCK ();

    CFA_LOCK ();

    if (CfaUtilCreateSChannelInterface (u4UapIfIndex, u4IfIndex,
                                        u1Status) == CFA_FAILURE)
    {
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_UNLOCK ();
    return CFA_SUCCESS;
}

/***********************************************************************************
 *
 *    Function Name       : CfaCreateDynamicSChannelInterface
 *
 *    Description         : This function creates dynamic S-Channel interface in CFA
 *                          and thereby indicating to L2IWF to create the 
 *                          S-Channel interface to VLAN, IGMP Snooping and 
 *                          FIP Snooping.
 *                          
 *    Input(s)            : u4IfIndex - S-Channel interface index.
 *                          u4UapIfIndex - UAP interface index.
 *
 *    Output(s)           : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE
 *
 ************************************************************************************/
INT4
CfaCreateDynamicSChannelInterface (UINT4 u4UapIfIndex, UINT4 u4IfIndex)
{
    tCfaExtTriggerMsg  *pExtTriggerMsg;
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return (CFA_FAILURE);
    }

    CFA_DS_LOCK ();
    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        /* Already interface is present */
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    if (CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_TRUE)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_DS_UNLOCK ();
    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaCreateDynamicSChannelInterface - "
                 "CFA is not initialized");
        return CFA_FAILURE;
    }
    if ((pExtTriggerMsg = (tCfaExtTriggerMsg *)
         MemAllocMemBlk (ExtTriggerMsgPoolId)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaCreateDynamicSChannelInterface - "
                  "Mempool allocation failed %d FAIL.\n", u4IfIndex);
        return CFA_FAILURE;
    }
    pExtTriggerMsg->uExtTrgMsg.DynSchannelIf.u4SChIfIndex = u4IfIndex;
    pExtTriggerMsg->uExtTrgMsg.DynSchannelIf.u4UapIfIndex = u4UapIfIndex;
    pExtTriggerMsg->i4MsgType = CFA_CREATE_DYN_SCH_IF;

    if (OsixQueSend (CFA_EXT_TRIGGER_QID, (UINT1 *) &pExtTriggerMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaCreateDynamicSChannelInterface - "
                  "Message En-queue to CFA on interface %d FAIL.\n", u4IfIndex);

        MemReleaseMemBlock (ExtTriggerMsgPoolId, (UINT1 *) pExtTriggerMsg);
        return (CFA_FAILURE);
    }
    OsixEvtSend (CFA_TASK_ID, CFA_EXT_TRIGGER_EVENT);
    KW_FALSEPOSITIVE_FIX (pExtTriggerMsg);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaDeleteSChannelInterface 
 *
 *    Description         : This function creates S-Channel interface in CFA
 *                          and thereby indicating to L2IWF to create the 
 *                          S-Channel interface to VLAN, IGMP Snooping and 
 *                          FIP Snooping.
 *                          
 *    Input(s)            : u4IfIndex - S-Channel interface index.
 *
 *    Output(s)           : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE
 *
 *****************************************************************************/
INT4
CfaDeleteSChannelInterface (UINT4 u4IfIndex)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return (CFA_FAILURE);
    }
    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_FALSE)
    {
        /* Already interface is not present */
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    CFA_DS_UNLOCK ();

    CFA_LOCK ();

    if (CfaUtilDeleteSChannelInterface (u4IfIndex) == CFA_FAILURE)
    {
        CFA_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_UNLOCK ();
    return (CFA_SUCCESS);
}

/****************************************************************************************
 *
 *    Function Name       : CfaDeleteDynamicSChannelInterface 
 *
 *    Description         : This function deletes dynamic  S-Channel interface in CFA
 *                          and thereby indicating to L2IWF to delete the 
 *                          S-Channel interface to VLAN, IGMP Snooping and 
 *                          FIP Snooping.
 *                          
 *    Input(s)            : u4IfIndex - S-Channel interface index.
 *
 *    Output(s)           : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE
 *
 *******************************************************************************************/
INT4
CfaDeleteDynamicSChannelInterface (UINT4 u4IfIndex)
{
    tCfaExtTriggerMsg  *pExtTriggerMsg;

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return (CFA_FAILURE);
    }
    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) != CFA_TRUE)
    {
        /* Already interface is not present */
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    CFA_DS_UNLOCK ();
    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaDeleteDynamicSChannelInterface - "
                 "CFA is not initialized");
        return CFA_FAILURE;
    }
    if ((pExtTriggerMsg = (tCfaExtTriggerMsg *)
         MemAllocMemBlk (ExtTriggerMsgPoolId)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaDeleteDynamicSChannelInterface - "
                  "Mempool allocation failed %d FAIL.\n", u4IfIndex);
        return CFA_FAILURE;
    }
    pExtTriggerMsg->uExtTrgMsg.DynSchannelIf.u4SChIfIndex = u4IfIndex;
    pExtTriggerMsg->i4MsgType = CFA_DELETE_DYN_SCH_IF;

    if (OsixQueSend (CFA_EXT_TRIGGER_QID, (UINT1 *) &pExtTriggerMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaDeleteDynamicSChannelInterface - "
                  "Message En-queue to CFA on interface %d FAIL.\n", u4IfIndex);

        MemReleaseMemBlock (ExtTriggerMsgPoolId, (UINT1 *) pExtTriggerMsg);
        return (CFA_FAILURE);
    }
    OsixEvtSend (CFA_TASK_ID, CFA_EXT_TRIGGER_EVENT);
    KW_FALSEPOSITIVE_FIX (pExtTriggerMsg);
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetFreeSChIndexForUap
 *
 *    Description         : This function returns the first available
 *                          free interface index from SBP interface pool for 
 *                          the provided UAP interface index.
 *
 *    Input(s)            : u1UapIfIndex - UAP Interface Index.         
 *
 *    Output(s)           : pu4Index - Pointer to interface index
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Returns            : CFA_SUCCESS if free index is available
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGetFreeSChIndexForUap (UINT4 u4UapIfIndex, UINT4 *pu4Index)
{
    UINT4               u4Index = 0;
    UINT4               u4IndexMin = CFA_MIN_EVB_SBP_INDEX;
    UINT4               u4IndexMax = CFA_MAX_EVB_SBP_INDEX;
    UINT4               u4LastIndex = 0;
    tCfaIfInfo         *pCfaIfInfo = NULL;
    UINT1               u1IndexFound = CFA_FALSE;
    UINT1               u1IdxFoundValid = CFA_FALSE;

    /*____________________________________________
     *|             |              |              |
     *| UAP1        | UAP2         | UAP3         |
     *|_____________|______________|______________|
     *|             |              |              |
     *|SBP1,SBP2,etc| SBP1,SBP2,etc|SBP1,SBP2,etc |
     *|_____________|______________|______________|
     */
    u4IndexMin = (CFA_MIN_EVB_SBP_INDEX + (VLAN_EVB_MAX_SBP_PER_UAP *
                                           (u4UapIfIndex - 1)));

    u4IndexMax = u4IndexMin + VLAN_EVB_MAX_SBP_PER_UAP - 1;
    /* Flexible ifIndex changes ===
       to support free ifindex being returned within the
       required range */

    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IndexMin);

    if (NULL == pCfaIfInfo)
    {
        *pu4Index = u4IndexMin;
        CFA_DS_UNLOCK ();
        return (CFA_SUCCESS);
    }
    else
    {
        u4LastIndex = u4IndexMin;
        u4Index = u4IndexMin;

        CFA_CDB_SCAN (u4Index, u4IndexMax, CFA_ALL_IFTYPE)
        {
            if (u4Index > u4IndexMax)
            {
                break;
            }

            if (CFA_CDB_IS_INTF_VALID (u4Index) != CFA_TRUE)
            {
                u1IndexFound = CFA_TRUE;
                u1IdxFoundValid = CFA_TRUE;
                break;
            }
            u4LastIndex = u4Index;
        }
    }

    if ((u4LastIndex != u4IndexMax) && (CFA_TRUE != u1IdxFoundValid))
    {
        u4LastIndex++;
        u4Index = u4LastIndex;
        u1IndexFound = CFA_TRUE;
    }
    if (CFA_TRUE == u1IndexFound)
    {
        *pu4Index = u4Index;
        CFA_DS_UNLOCK ();
        return (CFA_SUCCESS);
    }
    CFA_DS_UNLOCK ();
    return (CFA_FAILURE);
}

/*****************************************************************************
 *
 *    Function Name                : CfaCliGetSbpIndex 
 *
 *    Description                  : This function is used to get the 
 *                                   Interface index a attachment circuit 
 *                                   interfaces, given the interface name
 *                                   and attachment circuit number.
 *
 *    Input(s)                     : Attachment circuit interface name 
 *                                   and index
 *
 *    Output(s)                    : pu4IfIndex - IfIndex value of the given
 *                                   attachment circuit number
 *
 *    Global Variables Referred    : asCfaIfaces (supported Interface types)
 *                                         structure.
 *
 *    Global Variables Modified    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling        : None.
 *
 *    Use of Recursion             : None.
 *
 *    Returns                      : CLI_SUCCESS or CLI_FAILURE.
 ******************************************************************************/
INT4
CfaCliGetSbpIndex (INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex)
{
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4Index = CFA_DEFAULT_ENABLE;
    INT4                i4Len = CFA_DEFAULT_ENABLE;
    INT4                i4IfTypeLen = CFA_DEFAULT_ENABLE;
    INT4                i4MatchIfType = CFA_INVALID_IFINDEX;
    INT4                i4MatchIndex = CFA_INVALID_IFINDEX;
    BOOL1               bIsIfTypeConflict = OSIX_FALSE;
    UINT1               au1IfName[IF_PREFIX_LEN];

    if (!(pi1IfName) || !(*pi1IfName) || !(pi1IfNum))
    {
        return CLI_FAILURE;
    }

    i4Len = (INT4) STRLEN (pi1IfName);

    /* Check for longest match with  any interface type's available */

    for (i4Index = 0; i4Index < CLI_MAX_IFTYPES; i4Index++)
    {
        if (asCfaIfaces[i4Index][0].u4IfType == CFA_STATION_FACING_BRIDGE_PORT)
        {
            i4IfTypeLen = (INT4) STRLEN (asCfaIfaces[i4Index][0].au1IfName);

            i4Len = (i4Len < i4IfTypeLen) ? (i4Len) : (i4IfTypeLen);

            if (STRNCASECMP
                (asCfaIfaces[i4Index][0].au1IfName, pi1IfName, i4Len) == 0)
            {
                if (i4MatchIfType != CFA_INVALID_IFINDEX)
                {
                    bIsIfTypeConflict = OSIX_TRUE;
                    break;
                }
                i4MatchIfType = (INT4) asCfaIfaces[i4Index][0].u4IfType;
                i4MatchIndex = i4Index;
            }
        }
    }

    if ((i4MatchIfType == CFA_INVALID_IFINDEX)
        || (bIsIfTypeConflict == OSIX_TRUE))
    {
        return CLI_FAILURE;
    }

    /* if name shud be formed based on the match index or
     * based on the match iftype
     */
    STRNCPY (au1IfName, asCfaIfaces[i4MatchIndex][0].au1IfName,
             STRLEN (asCfaIfaces[i4MatchIndex][0].au1IfName));
    au1IfName[STRLEN (asCfaIfaces[i4MatchIndex][0].au1IfName)] = '\0';

    /* pi1IfNum is a integer pointer which
     * contains the port channel index */
    SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
              asCfaIfaces[i4MatchIndex][0].au1IfName,
              *(INT4 *) (VOID *) pi1IfNum);
    if (CfaGetInterfaceIndexFromName (au1IfName, pu4IfIndex) == OSIX_FAILURE)
    {
        i4RetVal = CLI_FAILURE;
    }

    return i4RetVal;

}

/*****************************************************************************/
/* Function Name      : CfaApiSetInterfaceMtu                                */
/*                                                                           */
/* Description        : This function sets the Mtu for all the Member Ports  */
/*                      for the given Vlan making the Admin Status Down and  */
/*                      and Admin status is made UP after changing the Mtu   */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                      u4IfMtu   - MTU to be set                            */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaApiSetInterfaceMtu (UINT4 u4IfIndex, UINT4 u4IfMtu)
{
    INT4                i4RetVal = 0;

    i4RetVal = CfaSetIfMtu (u4IfIndex, u4IfMtu);

    if (i4RetVal == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Setting MTU failed for interface %d\n", u4IfIndex);
        return CFA_FAILURE;
    }
#ifdef NPAPI_WANTED
    if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
    {
        /* Program the new MTU in h/w */
        i4RetVal = CfaFsCfaHwSetMtu (u4IfIndex, u4IfMtu);
        if (i4RetVal == FNP_FAILURE)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Setting MTU failed for interface in hardware %d\n",
                      u4IfIndex);
            return CFA_FAILURE;
        }
    }
#endif

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaUpdateSChannelOperStatus
 *
 *    Description          : This function is used to trigger CFA for
 *                           updating the OperStatus of the S-Channel Interface
 *
 *    Input(s)             : u4SchIfIndex - S-Channel IfIndex
 *                           u1OperStatus - Oper Status
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns          : CFA_SUCCESS / CFA_FAILURE.
 *****************************************************************************/

INT4
CfaUpdateSChannelOperStatus (UINT4 u4SchIfIndex, UINT1 u1OperStatus)
{
    tCfaExtTriggerMsg  *pExtTriggerMsg = NULL;

    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaUpdateSChannelOperStatus - "
                 "CFA is not initialized");
        return CFA_FAILURE;
    }
    if ((pExtTriggerMsg = (tCfaExtTriggerMsg *)
         MemAllocMemBlk (ExtTriggerMsgPoolId)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaUpdateSChannelOperStatus - "
                  "Mempool allocation failed %d FAIL.\n", u4SchIfIndex);
        return CFA_FAILURE;
    }

    pExtTriggerMsg->uExtTrgMsg.LinkStatusMsg.u4IfIndex = u4SchIfIndex;
    pExtTriggerMsg->uExtTrgMsg.LinkStatusMsg.u1LinkStatus = u1OperStatus;
    pExtTriggerMsg->i4MsgType = CFA_SCH_OPER_STATUS_MSG;

    if (OsixQueSend (CFA_EXT_TRIGGER_QID, (UINT1 *) &pExtTriggerMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaUpdateSChannelOperStatus  - "
                  "Message En-queue to CFA on interface %d FAIL.\n",
                  u4SchIfIndex);

        MemReleaseMemBlock (ExtTriggerMsgPoolId, (UINT1 *) pExtTriggerMsg);
        return (CFA_FAILURE);
    }

    /* send an explicit event to the task */
    OsixEvtSend (CFA_TASK_ID, CFA_EXT_TRIGGER_EVENT);
    KW_FALSEPOSITIVE_FIX (pExtTriggerMsg);
    CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
              "Exiting CfaHandlePktFromIsIs -"
              "Pkt Enqueue to CFA on interface %d SUCCESS.\n", u4SchIfIndex);
    return CFA_SUCCESS;

}

/******************************************************************************
 * FUNCTION    : CfaSetIfIp6Addr
 * DESCRIPTION : Routine to set an IPv6 address for an interface in cfa DB
 * INPUTS      : u4IfIndex - interface Index for which address is set
 *               pIp6Addr - pointer holding the new ipv6 address
 *               i4PrefixLen - Prefix Len to set
 *               i4Command  - OSIX_TRUE/OSIX_FALSE
 * OUTPUTS     : None
 * RETURNS     : CFA_SUCCESS - Successful updation of CFA DB
 *               CFA_FAILURE - Falied to update CFA DB.
 ******************************************************************************/
INT4
CfaSetIfIp6Addr (UINT4 u4IfIndex, tIp6Addr * pIp6Addr, INT4 i4PrefixLen,
                 INT4 i4Command)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;

    CFA_DBG2 (CFA_TRC_ALL, CFA_MAIN,
              "Entering CfaSetIfIp6Addr for interface %d with i4Command %d\n",
              u4IfIndex, i4Command);
    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DBG1 (ALL_FAILURE_TRC, CFA_MAIN,
                  "Error in configuring IPv6 address for interface %d.\n",
                  u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    if (i4Command == OSIX_TRUE)
    {
        MEMCPY (&(pCfaIfInfo->IfIp6Addr), pIp6Addr, sizeof (tIp6Addr));
        pCfaIfInfo->u1PrefixLen = (UINT1) i4PrefixLen;
    }
    else
    {
        MEMSET (&(pCfaIfInfo->IfIp6Addr), 0, sizeof (tIp6Addr));
        pCfaIfInfo->u1PrefixLen = 0;
    }

    CFA_DS_UNLOCK ();

#if defined (IP6_WANTED) && (defined (CLI_LNXIP_WANTED) || defined (SNMP_LNXIP_WANTED))
    if (CfaIsMgmtPort (u4IfIndex) == TRUE)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Configuring ipv6 address for OOB interface %d\n", u4IfIndex);
        CfaLinuxIpUpdateIpv6Addr (u4IfIndex, pIp6Addr, i4PrefixLen, i4Command);
    }
#endif
    CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
              "Exiting CfaSetIfIp6Addr-"
              "configuring IPv6 address on interface %d SUCCESS.\n", u4IfIndex);
    return CFA_SUCCESS;
}

/******************************************************************************
 * FUNCTION    : CfaGetIfIp6Addr
 * DESCRIPTION : Routine to get an IPv6 address for an interface in CFA
 * INPUTS      : u4IfIndex - Interface index for which address to be obtained
 * OUTPUTS     : pIpConfigInfo - output structure whether ipv6 addr is filled
 * RETURNS     : CFA_SUCCESS - Successful retrieval of IPv6 address for interface
 *               CFA_FAILURE - failed to get IPv6 adddress for interface
 ******************************************************************************/
INT4
CfaGetIfIp6Addr (UINT4 u4IfIndex, tIpConfigInfo * pIpConfigInfo)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;

    CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
              "Entering CfaGetIfIp6Addr for interface %d\n", u4IfIndex);
    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DBG1 (ALL_FAILURE_TRC, CFA_MAIN,
                  "Error in getting IPv6 address for interface %d.\n",
                  u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    MEMCPY (&(pIpConfigInfo->Ip6Addr), &(pCfaIfInfo->IfIp6Addr),
            sizeof (tIp6Addr));
    pIpConfigInfo->u1PrefixLen = pCfaIfInfo->u1PrefixLen;

    CFA_DS_UNLOCK ();
    CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
              "Exiting CfaGetIfIp6Addr-"
              "obtaining IPv6 address on interface %d SUCCESS.\n", u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaFsbGddEthWrite
 *
 *    Description        : Writes the data to the ethernet driver.
 *
 *    Input(s)            : pu1DataBuf - Pointer to the linear buffer.
 *                          u4IfIndex - MIB-2 interface index
 *                          u4PktSize - Size of the buffer.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable (Interface table)
 *                                structure,
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if write succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaFsbGddEthWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    return (CfaGddEthWrite (pu1DataBuf, u4IfIndex, u4PktSize));
}

/*****************************************************************************/
/* Function Name      : CfaUpdateIcclFdbEntry                                */
/*                                                                           */
/* Description        : This function creates or deletes static mac-address  */
/*                      with the given HB peer MAC address and ICCL interface*/
/*                      index for the given VLAN                             */
/*                                                                           */
/* Input(s)           : pu1HbPeerMac - HB peer MAC address                   */
/*                      u4IcchIfIndex - ICCL interface index                 */
/*                      u1Action - Decision whether to create or remove      */
/*                      the FDB entry.                                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
CfaUpdateIcclFdbEntry (UINT1 *pu1HbPeerMac, UINT4 u4IcchIfIndex, UINT1 u1Action)
{
#if (defined HB_WANTED && ICCH_WANTED)
    tMacAddr            DestMacAddr;
    UINT4               u4IvrIndex = 0;
    UINT2               u2VlanId = 0;

    MEMSET (DestMacAddr, 0, sizeof (tMacAddr));
    MEMCPY (DestMacAddr, pu1HbPeerMac, sizeof (tMacAddr));

    for (u4IvrIndex = CFA_MIN_IVR_IF_INDEX; u4IvrIndex <= CFA_MAX_IVR_IF_INDEX;
         u4IvrIndex++)
    {
        if (CfaGetIfIvrVlanId (u4IvrIndex, &u2VlanId) == CFA_SUCCESS)
        {
            if (u1Action == HB_FDB_ADD)
            {
                /* Add the static MAC-Address for ICCL interface */
                VlanHwAddFdbEntry (u4IcchIfIndex, DestMacAddr, u2VlanId,
                                   VLAN_PERMANENT);
            }
            else
            {
                VlanHwDelFdbEntry (u4IcchIfIndex, DestMacAddr, u2VlanId);
            }
            u2VlanId = 0;
        }
    }
#else
    UNUSED_PARAM (pu1HbPeerMac);
    UNUSED_PARAM (u4IcchIfIndex);
    UNUSED_PARAM (u1Action);
#endif
    return;
}

/*****************************************************************************
*    Function Name            : CfaIcclIpIfIsLocalNet
*
*    Description              :  Verifes whether the IP address belongs to any
*                                of the interface's network in the specified
*                                context
*
*    Input(s)                  : u4ContextId - the context identifier
*                                u4IpAddress - Address to be verified against
*                                the interface broadcast address
*
*    Output(s)                 : None.
*
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE
*****************************************************************************/
INT4
CfaIcclIpIfIsLocalNet (UINT4 u4IpAddress, UINT4 u4SubnetMask)
{
    tIpIfRecord        *pIpIntf;
    tIpAddrInfo        *pSecIpInfo;
    INT4                i4RetVal = CFA_FAILURE;
    UINT4               u4IfCxtId = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4MaxSubnetMask = u4SubnetMask;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
    CFA_IPIFTBL_LOCK ();

    /* Get the first IP interface node from RBTree */
    pIpIntf = RBTreeGetFirst (gIpIfInfo.pIpIfTable);
    if (pIpIntf != NULL)
    {
        do
        {
            /* A Valid IP interface associated with the given
             * Interface */
            if (CFA_IF_IPPORT ((UINT2) pIpIntf->u4IfIndex) != CFA_INVALID_INDEX)
            {

                VcmGetContextIdFromCfaIfIndex (pIpIntf->u4IfIndex, &u4IfCxtId);
                if (u4IfCxtId != u4ContextId)
                {
                    pIpIntf = (tIpIfRecord *) RBTreeGetNext
                        (gIpIfInfo.pIpIfTable, (tRBElem *) pIpIntf, NULL);
                    continue;
                }
                u4NetMask = u4CidrSubnetMask[pIpIntf->u1SubnetMask];

                /*Check for maximum mask between ICCL subnet and other IVR subnet mask */
                if (u4NetMask > u4SubnetMask)
                {
                    u4MaxSubnetMask = u4NetMask;
                }
                else
                {
                    u4MaxSubnetMask = u4SubnetMask;
                }

                if ((((pIpIntf->u4IpAddr) & u4MaxSubnetMask) ==
                     (u4IpAddress & u4MaxSubnetMask))
                    && (pIpIntf->u4IpAddr != 0))
                {
                    i4RetVal = CFA_SUCCESS;
                    break;
                }

                TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo,
                              tIpAddrInfo *)
                {
                    if (pSecIpInfo->i4RowStatus != CFA_RS_ACTIVE)
                    {
                        continue;
                    }
                    u4NetMask = u4CidrSubnetMask[pSecIpInfo->u1SubnetMask];

                    /*Check for maximum mask between ICCL subnet and other IVR subnet mask */
                    if (u4NetMask > u4SubnetMask)
                    {
                        u4MaxSubnetMask = u4NetMask;
                    }
                    else
                    {
                        u4MaxSubnetMask = u4SubnetMask;
                    }

                    if ((((pSecIpInfo->u4Addr) & u4MaxSubnetMask) ==
                         (u4IpAddress & u4MaxSubnetMask))
                        && (pIpIntf->u4IpAddr != 0))
                    {
                        i4RetVal = CFA_SUCCESS;
                        break;
                    }
                }
                if (i4RetVal == CFA_SUCCESS)
                {
                    break;
                }
            }
            pIpIntf = (tIpIfRecord *) RBTreeGetNext (gIpIfInfo.pIpIfTable,
                                                     (tRBElem *) pIpIntf, NULL);
        }
        while (pIpIntf != NULL);
    }
    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : CfaCliGetPhysicalAndLogicalPortNum                   */
/*                                                                           */
/* Description        : This function gets the physical port number and the  */
/*                      logical port number for the given interface number   */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pi1IfNum - Interface number of format                */
/*                                 <slotnumber/portnumber.logicalportnumber> */
/*                                                                           */
/* Output(s)          : pi4PhyPort - Physical port number                    */
/*                      pi4LPortNum - Logical port number                    */
/*                      pi4SlotNum  - Slot number                            */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_FAILURE or CFA_SUCCESS                           */
/*****************************************************************************/
INT4
CfaCliGetPhysicalAndLogicalPortNum (INT1 *pi1IfNum, INT4 *pi4PhyPort,
                                    INT4 *pi4LPortNum, INT4 *pi4SlotNum)
{
    UINT2               u2StrLen = 0;
    UINT2               u2Index = 0;
    UINT2               u2Idx = 0;
    UINT2               u2SubLen = 0;
    BOOL1               bIsDigitFound = FALSE;
    BOOL1               bIsSubFlag = FALSE;
    INT1               *pi1Ptr;
    INT1               *pi1SubStr = NULL;
    INT1                ai1Token[MAX_LINE_LEN];

    MEMSET (ai1Token, '\0', MAX_LINE_LEN);
    if (!(pi1IfNum) || !(*pi1IfNum))
        return CFA_FAILURE;
    if (*pi1IfNum == '-')
    {
        pi1IfNum++;
    }
    u2StrLen = (UINT2) STRLEN (pi1IfNum);

    pi1Ptr = &pi1IfNum[u2Index];
    for (; u2Index < u2StrLen; u2Index++)
    {
        if (!(isdigit (pi1IfNum[u2Index])))
        {
            if ((pi1IfNum[u2Index] != '/') || (bIsDigitFound == FALSE))
                return CFA_FAILURE;
            else
                break;
        }
        bIsDigitFound = TRUE;
    }
    /* Separating till slot number */
    if ((pi1IfNum[u2Index] != '/'))
        return CFA_FAILURE;

    pi1IfNum[u2Index] = '\0';

    *pi4SlotNum = ATOI (pi1Ptr);

    pi1IfNum[u2Index] = '/';

    u2Index++;

    pi1Ptr = &pi1IfNum[u2Index];

    /* If the char "." exists in the interface index then it 
     * is sub-channel interface*/
    if (CLI_STRSTR (pi1Ptr, ".") != NULL)
    {
        bIsSubFlag = TRUE;
        u2SubLen = (UINT2) CLI_STRLEN (CLI_STRSTR (pi1Ptr, "."));
    }

    /*Separating the parent and child channel indices */
    if (bIsSubFlag == TRUE)
    {
        for (u2Idx = 0; u2Idx < u2StrLen; u2Idx++)
        {
            if (pi1Ptr[u2Idx] == '.')
            {
                u2StrLen = (UINT2) (u2StrLen - u2SubLen);
                MEMCPY (ai1Token, pi1Ptr, u2StrLen);
                ai1Token[u2Idx] = '\0';
                u2Idx++;
                pi1SubStr = &pi1Ptr[u2Idx];
                break;
            }

        }

    }
    bIsDigitFound = FALSE;

    for (; u2Index < u2StrLen; u2Index++)
    {
        if (!(isdigit (pi1IfNum[u2Index])))
        {
            return CFA_FAILURE;
        }
        bIsDigitFound = TRUE;
    }
    if (bIsDigitFound == FALSE)
        return CFA_FAILURE;

    *pi4PhyPort = ATOI (ai1Token);

    bIsDigitFound = FALSE;
    /*Validation for child channel indices */
    if ((bIsSubFlag == TRUE) & (pi1SubStr != NULL))
    {
        for (u2Idx = 0; pi1SubStr[u2Idx] != '\0'; u2Idx++)
        {
            if (!(isdigit (pi1SubStr[u2Idx])))
            {
                return CFA_FAILURE;
            }
            /* Subinterface found */
            bIsDigitFound = TRUE;
        }
        if (bIsDigitFound == FALSE)
            return FAILURE;
        *pi4LPortNum = ATOI (pi1SubStr);
    }
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaCliValidatePhysicalPort                           */
/*                                                                           */
/* Description        : This function Validates whether the Physical port is */
/*                      is present or not ,if present checks whether it is   */
/*                      switch port , as L3 subinterface can`t be created    */
/*                      over router port.                                    */
/*                                                                           */
/* Input(s)           : i4PhyPort  - Port number                             */
/*                      i4SlotNum  - Slot Number                             */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_FAILURE and CLI_SUCCESS                          */
/*****************************************************************************/
UINT4
CfaCliValidatePhysicalPort (tCliHandle CliHandle, INT4 i4PhyPort,
                            INT4 i4SlotNum)
{
    UINT4               u4IfIndex = 0;
    INT4                i4RowStatus = CFA_RS_INVALID;
    UINT1               u1BridgedIfaceStatus = CFA_DISABLED;
    UINT4               u4RetStatus = CLI_SUCCESS;

#ifndef MBSM_WANTED
    if ((i4SlotNum != 0) || (i4PhyPort > CFA_PHYS_NUM ()) || (i4PhyPort == 0))
#else
    if ((i4SlotNum < (MBSM_SLOT_INDEX_START + MBSM_MAX_CC_SLOTS)) ||
        (i4SlotNum >= (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START))
        || (i4PhyPort > CFA_PHYS_NUM ()))
#endif
    {
        return CLI_FAILURE;
    }

    /* Get Interface Index from given Port num and Slot number */
    if (CfaGetIfIndexFromSlotAndPort (i4SlotNum, (UINT4) i4PhyPort, &u4IfIndex)
        == CLI_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                 "CfaCliValidatePhysicalPort: Unable to get IfIndex");
        u4RetStatus = CLI_FAILURE;
    }

    /* Get the RowStatus of the Physical Interface Index */
    if (nmhGetIfMainRowStatus ((INT4) u4IfIndex, &i4RowStatus) == SNMP_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                 "CfaCliValidatePhysicalPort: Unable to get IfIndex");
        u4RetStatus = CLI_FAILURE;
    }

    /* If the Rowstatus of the Physical interface index is Invalid ,
     *  Return Failure */
    if ((i4RowStatus == CFA_RS_INVALID) || (u4RetStatus == CLI_FAILURE))
    {
        CliPrintf (CliHandle, "\r%% Parent Physical Port %d/%d not"
                   " present.\r\n", i4SlotNum, i4PhyPort);
        return CLI_FAILURE;
    }

    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);

    /* If the Physical Port is router port (CFA_DISABLED) throw error */
    if (u1BridgedIfaceStatus == CFA_DISABLED)
    {
        CliPrintf (CliHandle, "\r%% Parent port cannot be router port \r");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaL3SubIfShutDownNotify                             */
/*                                                                           */
/* Description        : This function notify the operstatus of Parent port to */
/*                      L3subinteface   .                                    */
/*                                                                           */
/* Input(s)           : u4IfMainIndex - Parent index                         */
/*                      u1OperStatus  - Operstatus of the Parent             */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_FAILURE and CFA_SUCCESS                          */
/*****************************************************************************/

UINT4
CfaL3SubIfShutDownNotify (UINT4 u4IfMainIndex, UINT1 u1OperStatus)
{
    UINT4               u4EndIfIndex = 0;
    UINT4               u4ParIfIndex = 0;
    UINT4               u4IfIndex = 0;

    u4IfIndex = CFA_MIN_L3SUB_IF_INDEX;
    u4EndIfIndex = CFA_MAX_L3SUB_IF_INDEX;
    /* Scan between Minimum L3Subifindex and Maximum l3subifindex */
    CFA_CDB_SCAN (u4IfIndex, u4EndIfIndex, CFA_L3SUB_INTF)
    {
        if ((CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_TRUE))
        {
            /* Get the parent index for this L3Subif index */
            CfaGetL3SubIfParentIndex (u4IfIndex, &u4ParIfIndex);

            if (u4ParIfIndex == u4IfMainIndex)
            {
                CfaInterfaceStatusChangeIndication (u4IfIndex, u1OperStatus);
            }
        }
    }
    /* No L3Subinterface associated with this index */
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaCliGetPortChannelPhysicalAndLogicalPortNum        */
/*                                                                           */
/* Description        : This function gets the physical port number and the  */
/*                      logical port number for the given interface number   */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pi1IfNum - Interface number of format                */
/*                                 <portnumber.logicalportnumber> */
/*                                                                           */
/* Output(s)          : pi4PhyPort - Physical port number                    */
/*                      pi4LPortNum - Logical port number                    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_FAILURE or CFA_SUCCESS                           */
/*****************************************************************************/

INT4
CfaCliGetPortChannelPhysicalAndLogicalPortNum (INT1 *pi1IfNum,
                                               INT4 *pi4AggId, INT4 *pi4LAggId)
{

    UINT2               u2StrLen = 0;
    UINT2               u2Index = 0;
    UINT2               u2Idx = 0;
    UINT2               u2SubLen = 0;
    BOOL1               bIsDigitFound = FALSE;
    BOOL1               bIsSubFlag = FALSE;
    INT1               *pi1Ptr;
    INT1               *pi1SubStr = NULL;
    INT1                ai1Token[MAX_LINE_LEN];

    MEMSET (ai1Token, '\0', MAX_LINE_LEN);
    if (!(pi1IfNum) || !(*pi1IfNum))
        return CFA_FAILURE;
    if (*pi1IfNum == '-')
    {
        pi1IfNum++;
    }
    u2StrLen = (UINT2) STRLEN (pi1IfNum);

    pi1Ptr = &pi1IfNum[u2Index];

    /* If the char "." exists in the interface index then it
     * is sub-channel interface*/
    if (CLI_STRSTR (pi1Ptr, ".") != NULL)
    {
        bIsSubFlag = TRUE;
        u2SubLen = (UINT2) CLI_STRLEN (CLI_STRSTR (pi1Ptr, "."));
    }

    /*Separating the parent and child channel indices */
    if (bIsSubFlag == TRUE)
    {
        for (u2Idx = 0; u2Idx < u2StrLen; u2Idx++)
        {
            if (pi1Ptr[u2Idx] == '.')
            {
                u2StrLen = (UINT2) (u2StrLen - u2SubLen);
                MEMCPY (ai1Token, pi1Ptr, u2StrLen);
                ai1Token[u2Idx] = '\0';
                u2Idx++;
                pi1SubStr = &pi1Ptr[u2Idx];
                break;
            }

        }

    }
    bIsDigitFound = FALSE;

    for (; u2Index < u2StrLen; u2Index++)
    {
        if (!(isdigit (pi1IfNum[u2Index])))
        {
            return CFA_FAILURE;

        }
        bIsDigitFound = TRUE;
    }
    if (bIsDigitFound == FALSE)
        return CFA_FAILURE;

    *pi4AggId = ATOI (ai1Token);

    bIsDigitFound = FALSE;
    /*Validation for child channel indices */
    if ((bIsSubFlag == TRUE) & (pi1SubStr != NULL))
    {
        for (u2Idx = 0; pi1SubStr[u2Idx] != '\0'; u2Idx++)
        {
            if (!(isdigit (pi1SubStr[u2Idx])))
            {
                return CFA_FAILURE;
            }
            /* Subinterface found */
            bIsDigitFound = TRUE;
        }
        if (bIsDigitFound == FALSE)
            return FAILURE;
        *pi4LAggId = ATOI (pi1SubStr);
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
*
*    Function Name             : CfaGetGlobalModTrc
*
*    Description               : This function is used to Get the Global variable for Trace events. Here Array of 1 represents Module level trace and
*                                Array of 0 represents Link level trace
*
*    Input(s)                  : None
*
*    Output(s)                 : gau4ModTrcEvents
*
*    Global Variables Referred : gau4ModTrcEvents
*
*    Returns                   : gau4ModTrcEvents[0]
*******************************************************************************/
UINT4
CfaGetGlobalModTrc ()
{
    return gau4ModTrcEvents[0];
}

/*****************************************************************************
 *    Function Name                : CfaGetSysSuppTimeFormat
 *    Description                  : This function returns the system supported 
 *                             timestamp format
 *    Input(s)                     : None. 
 *    Output(s)                    : u1SysSuppTimeFormat
 *    Global Variables Referred    : None.
 *    Global Variables Modified    : None. 
 *    Exceptions or Operating
 *    System Error Handling        : None.
 *    Use of Recursion             : None.
 *    Returns                      : None 
 *****************************************************************************/
VOID
CfaGetSysSuppTimeFormat (UINT1 *u1SysSuppTimeFormat)
{
    *u1SysSuppTimeFormat = 0x02;    /* PTP format */
    return;
}

#ifdef NPAPI_WANTED

  /*****************************************************************************
   *
   *    Function Name       : CfaPostPacketToNP
   *
   *    Description         : This function enqueues the STP packet to CfaAstPktTx
   *                          task.
   *
   *    Input(s)            : pBuf - CRU Buffer containing pkt to be transmitted
   *                          u4IfIndex - Interface index
   *             u4PktSize - Packet size
   *
   *    Output(s)           : None
   *
   *    Global Variables Modified : None.
   *
   *    Exceptions or Operating
   *    System Error Handling    : None.
   *
   *    Use of Recursion        : None.
   *
   *    Returns            : CFA_SUCCESS/CFA_FAILURE
   *
   *****************************************************************************/

INT4
CfaPostPacketToNP (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                   UINT4 u4PktSize)
{
    UNUSED_PARAM (u4PktSize);

    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_L2, "Error in CfaPostPacketToNP -"
                 "CFA is not initialized %d.\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return (CFA_FAILURE);
    }
    /* check if the IfIndex is valid */
    if (CfaValidateIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (ALL_FAILURE_TRC, CFA_L2,
                  "Error in CfaPostPacketToNP - invalid interface %d.\n",
                  u4IfIndex);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

        return (CFA_FAILURE);
    }
    CFA_SET_IFINDEX (pBuf, u4IfIndex);

    if (OsixQueSend (CFA_AST_PACKET_QID, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG1 (BUFFER_TRC | ALL_FAILURE_TRC, CFA_L2,
                  "Error In CfaPostPacketToNP -"
                  "Pkt En-queue to CFA on interface %d FAIL.\n", u4IfIndex);

        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

        return (CFA_FAILURE);
    }

    /* send an explicit event to the task */
    OsixEvtSend (CFA_AST_PKTTX_TASK_ID, CFA_AST_BPDU_EVENT);

    CFA_DBG1 (BUFFER_TRC, CFA_L2,
              "Exiting CfaPostPacketToNP -"
              "Pkt En-queue to CFA on interface %d SUCCESS.\n", u4IfIndex);

    return CFA_SUCCESS;
}

#endif
/*****************************************************************************
 *
 *    Function Name        : CfaInterfaceLAPortCreateIndication
 *
 *    Description          : This function validates the ifIndex and calls
 *                           post event to CFA for processing  the physical port
 *                           creation from LA module, with the Port channel is
 *                           deleted.
 *
 *    Input(s)             : u4IfIndex - Interface index
 *                           u1OperStatus - Oper Status
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.
 *****************************************************************************/
VOID
CfaInterfaceLAPortCreateIndication (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tCfaExtTriggerMsg  *pExtTriggerMsg = NULL;

    if ((pExtTriggerMsg = (tCfaExtTriggerMsg *)
         MemAllocMemBlk (ExtTriggerMsgPoolId)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaInterfaceLAPortCreateIndication  - "
                  "Mempool allocation failed %d FAIL.\n", u4IfIndex);
        return;
    }

    pExtTriggerMsg->uExtTrgMsg.LinkStatusMsg.u4IfIndex = u4IfIndex;
    pExtTriggerMsg->uExtTrgMsg.LinkStatusMsg.u1LinkStatus = u1OperStatus;
    pExtTriggerMsg->i4MsgType = CFA_LA_PORT_CREATE_IND_MSG;

    if (OsixQueSend (CFA_EXT_TRIGGER_QID, (UINT1 *) &pExtTriggerMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaInterfaceLAPortCreateIndication  - "
                  "Message Enqueue to CFA on interface %d FAIL.\n", u4IfIndex);

        MemReleaseMemBlock (ExtTriggerMsgPoolId, (UINT1 *) pExtTriggerMsg);
        return;
    }

    /* send an explicit event to the task */
    OsixEvtSend (CFA_TASK_ID, CFA_EXT_TRIGGER_EVENT);

    CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
              "Exiting CfaInterfaceLAPortCreateIndication -"
              "Pkt Enqueue to CFA on interface %d SUCCESS.\n", u4IfIndex);
    return;
}

/*****************************************************************************
 *
 *    Function Name        : CfaSetInterfaceLAPortCreateIndication
 *
 *    Description          : This function validates the ifIndex and calls
 *                           L2IwfCreatePortIndicationFromLA to update the
 *                           interface status.
 *
 *    Input(s)             : u4IfIndex - Interface index
 *                           u1OperStatus - Operational Status
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.
 *****************************************************************************/
VOID
CfaSetInterfaceLAPortCreateIndication (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    UINT1               u1CfaOperStatus = CFA_IF_DOWN;

    /* When deleting the port channel, the ports that are present in the
     * port channel are to be created again in L2 modules. Since this
     * flow from LA is handled via CfaMain by queue posting from LA,
     * the latest oper status is taken from CFA.
     * Note: CfaSetInterfaceLAPortCreateIndication and CFA oper status
     *       update from the HW are pushed in the same queue - to avoid
     *       the race conditions.
     */
    CfaGetIfOperStatus (u4IfIndex, &u1CfaOperStatus);

    L2IwfCreatePortIndicationFromLA (u4IfIndex, u1CfaOperStatus);
    UNUSED_PARAM (u1OperStatus);
}

/*****************************************************************************
 *
 *    Function Name        : CfaInterfaceHLPortOperIndication
 *
 *    Description          : This function validates the ifIndex and calls
 *                           CfaInterfaceStatusChangeIndication to upate the
 *                           interface status.
 *    Input(s)             : u4IfIndex - Interface index
 *                           u1OperStatus -
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.
 *****************************************************************************/
VOID
CfaInterfaceHLPortOperIndication (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tCfaExtTriggerMsg  *pExtTriggerMsg = NULL;

    if ((pExtTriggerMsg = (tCfaExtTriggerMsg *)
         MemAllocMemBlk (ExtTriggerMsgPoolId)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaInterfaceHLPortOperIndication  - "
                  "Mempool allocation failed %d FAIL.\n", u4IfIndex);
        return;
    }

    pExtTriggerMsg->uExtTrgMsg.LinkStatusMsg.u4IfIndex = u4IfIndex;
    pExtTriggerMsg->uExtTrgMsg.LinkStatusMsg.u1LinkStatus = u1OperStatus;
    pExtTriggerMsg->i4MsgType = CFA_HL_PORT_OPER_IND_MSG;

    if (OsixQueSend (CFA_EXT_TRIGGER_QID, (UINT1 *) &pExtTriggerMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaInterfaceHLPortOperIndication  - "
                  "Message Enqueue to CFA on interface %d FAIL.\n", u4IfIndex);

        MemReleaseMemBlock (ExtTriggerMsgPoolId, (UINT1 *) pExtTriggerMsg);
        return;
    }

    /* send an explicit event to the task */
    OsixEvtSend (CFA_TASK_ID, CFA_EXT_TRIGGER_EVENT);

    CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
              "Exiting CfaInterfaceHLPortOperIndication -"
              "Pkt Enqueue to CFA on interface %d SUCCESS.\n", u4IfIndex);
    return;
}

/*****************************************************************************
 *
 *    Function Name        : CfaSetInterfaceHLPortOperIndication
 *
 *    Description          : This function validates the ifIndex and calls
 *                           CfaInterfaceStatusChangeIndication to upate the
 *                           interface status.
 *    Input(s)             : u4IfIndex - Interface index
 *                           u1OperStatus -
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.
 *****************************************************************************/
INT4
CfaSetInterfaceHLPortOperIndication (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    if (L2IwfUtlBrgHLPortOperIndication (u4IfIndex, u1OperStatus) ==
        L2IWF_FAILURE)
    {
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

/********************************************************************************/
/*                                                                              */
/*    Function Name       : CfaApiSetInterfaceAdminStatus                       */
/*                                                                              */
/*    Description         : This function set the admin status of the           */
/*                          interface                                           */
/*                                                                              */
/*    Input(s)            : u4IfIndex - Index of the Interface to set Admin     */
/*                                      status                                  */
/*                          u4AdminStatus - Admin Status of the interface       */
/*                                                                              */
/*    Output(s)           : None                                                */
/*                                                                              */
/*    Global Variables Modified : None.                                         */
/*                                                                              */
/*    Exceptions or Operating                                                   */
/*    System Error Handling    : None.                                          */
/*                                                                              */
/*    Use of Recursion        : None.                                           */
/*                                                                              */
/*    Returns            : CFA_SUCCESS/CFA_FAILURE                              */
/*                                                                              */
/********************************************************************************/
INT4
CfaApiSetInterfaceAdminStatus (UINT4 u4IfIndex, UINT4 u4AdminStatus)
{
#ifdef CLI_WANTED
    INT4                i4Instance = 0;
    return (CfaSetInterfaceAdminStatus (i4Instance, u4IfIndex, u4AdminStatus));
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4AdminStatus);
    return CFA_FAILURE;
#endif
}
