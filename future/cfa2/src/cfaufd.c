/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfaufd.c,v 1.17 2016/06/20 09:59:32 siva Exp $
 *
 * Description:This file contains utility routines of ufd module.
 *
 *******************************************************************/
#ifndef __CFAUFD_C__
#define __CFAUFD_C__

#include "cfainc.h"
#include "isspiinc.h"

/*****************************************************************************
 *
 *    Function Name        : CfaUfdStart
 *
 *    Description        : This function initialises the Ufd related DBs.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE or CFA_ ERR_MEM_FAILURE.
 *
 *****************************************************************************/
INT4
CfaUfdStart (VOID)
{
    gCfaUfdGroupMemPoolId =
        CFAMemPoolIds[MAX_CFA_UFD_GROUP_INFO_BLOCKS_SIZING_ID];

    if (CfaIfUfdGroupInfoDbCreate () == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                 "CfaUfdStart - UfdGroupInfoTable failed.\n");
        return CFA_FAILURE;
    }
    CFA_UFD_SYSTEM_CONTROL () = CFA_UFD_START;
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaUfdShutdown
 *
 *    Description        : This function deletes the UFD related memory pools.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaUfdShutdown (VOID)
{
    if (CFA_UFD_SYSTEM_CONTROL () != CFA_UFD_START)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                 "CfaUfdShutdown - UFD system is not running. Cannot Shut down.\n");
        return CFA_SUCCESS;
    }
    CfaUfdDisable ();
    /* Removing the RBTree Group Entries */
    CfaUfdDeleteAllGroupEntries ();
    /* Deleting the RbTree, since the module is shutdown */
    if (gUfdGroupInfoTable != NULL)
    {
        RBTreeDelete (gUfdGroupInfoTable);
        gUfdGroupInfoTable = NULL;
    }

    CFA_UFD_SYSTEM_CONTROL () = CFA_UFD_SHUTDOWN;
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaUfdEnable
 *
 *    Description        : This function enables the Ufd.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Returns            : None
 *
 *****************************************************************************/

VOID
CfaUfdEnable ()
{
    tUfdGroupInfo      *pUfdGroupInfoTable = NULL;

    pUfdGroupInfoTable = (tUfdGroupInfo *) RBTreeGetFirst (gUfdGroupInfoTable);
    
    if (CFA_UFD_MODULE_STATUS () == CFA_UFD_ENABLED)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                 "CfaUfdEnable - UFD is already Enabled.\n");
        return;
    }
    CFA_UFD_MODULE_STATUS () = CFA_UFD_ENABLED;
    if (pUfdGroupInfoTable != NULL)
    {
        CfaUfdPortChangeUpToErrorDisabled ();
    }
    return;
}

/*****************************************************************************
 *
 *    Function Name        : CfaUfdDisable
 *
 *    Description        : This function disables the Ufd.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/

VOID
CfaUfdDisable ()
{
    tUfdGroupInfo      *pUfdGroupInfoTable = NULL;

    pUfdGroupInfoTable = (tUfdGroupInfo *) RBTreeGetFirst (gUfdGroupInfoTable);

    if (CFA_UFD_MODULE_STATUS () == CFA_UFD_DISABLED)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                 "CfaUfdDisable - UFD is already Disabled.\n");
        return;
    }
    if (CFA_UFD_SYSTEM_CONTROL () != CFA_UFD_START)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                 "CfaUfdDisable - UFD is not started.\n");
        return;
    }

    CFA_UFD_MODULE_STATUS () = CFA_UFD_DISABLED;
    if (pUfdGroupInfoTable != NULL)
    {
        CfaUfdPortChangeErrorDisabledToUp();
    }
    return;
}

/*
*****************************************************************************
 *
 *    Function Name        : CfaUfdGetSystemControl
 *
 *    Description        : This function gets the Ufd system control.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Returns            : None
 *
 *****************************************************************************/

VOID
CfaUfdGetSystemControl (UINT1 *pu1UfdSystemControl)
{

    *pu1UfdSystemControl = gUfdSystemControl;
    return;
}

/*****************************************************************************
 *
 *    Function Name        : CfaUfdGetModuleStatus
 *
 *    Description        : This function gets the UFD Module Status.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Returns            :  None.
 *
 *****************************************************************************/

VOID
CfaUfdGetModuleStatus (UINT1 *pu1UfdModuleStatus)
{

    *pu1UfdModuleStatus = gUfdModuleStatus;
    return;

}

/*****************************************************************************
 *
 *    Function Name        : CfaUfdGetSystemControl
 *
 *    Description        : This function gets the Ufd group count.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/

VOID
CfaUfdGetGroupCount (UINT4 *pu4UfdGroupCount)
{

    *pu4UfdGroupCount = gu4UfdGroupCount;
    return;

}

/*****************************************************************************/
/* Function Name      : CfaIfUfdGroupInfoTableCmp                            */
/*                                                                           */
/* Description        : Compare function for the tUfdGroupInfo RBTree        */
/*                                                                           */
/* Input(s)           : RBElement pointers for comparison                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1 - if first RBTree group id is > second RBTree      */
/*                     -1 - if first RBTree group id is < second RBTree      */
/*                      0 - if both are same                                 */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIfUfdGroupInfoTableCmp (tCfaIfDbElem * pRBElem1, tCfaIfDbElem * pRBElem2)
{

    tUfdGroupInfo      *pUfdGroupInfoEntry1 = (tUfdGroupInfo *) pRBElem1;
    tUfdGroupInfo      *pUfdGroupInfoEntry2 = (tUfdGroupInfo *) pRBElem2;
    if (pUfdGroupInfoEntry1->u4UfdGroupId > pUfdGroupInfoEntry2->u4UfdGroupId)
    {
        return 1;
    }
    else if (pUfdGroupInfoEntry1->u4UfdGroupId <
             pUfdGroupInfoEntry2->u4UfdGroupId)
    {
        return -1;
    }

    return 0;
}

/*****************************************************************************
 *
 *    Function Name      : CfaIfUfdGroupInfoDbCreate
 *
 *    Description        : This function creates the Ufd Group Info DB.
 *
 *    Input(s)           : None.
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if creation succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaIfUfdGroupInfoDbCreate (VOID)
{
    gUfdGroupInfoTable =
        RBTreeCreateEmbedded (0, (tCfaIfDbCmpFn) CfaIfUfdGroupInfoTableCmp);
    if (gUfdGroupInfoTable == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                 "CfaIfUfdGroupInfoDbCreate - UFD GroupInfo RBTree failed\n");
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaIfChangePortRole
 *
 *    Description        : This function is called from CFA when oper status
 *                         of a port is made up or down.
 *
 *    Input(s)           : u4PortIndex
 *                         u1PortRole
 *
 *    Output(s)          : None
 *
 *    Returns            : CFA_SUCCESS if changes succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfChangePortRole (UINT4 u4PortIndex, UINT1 u1PortRole)
{
    tUfdGroupInfo       UfdGroupInfo;
    tUfdGroupInfo      *pUfdGroupInfoEntry = NULL;
    tCfaIfInfo         *pCfaIfInfo = NULL;
    UINT4               u4UfdGroupId = 0;
    UINT4               u4IfIndex = 0;
    UINT1               u1UfdOperStatus = 0;
    UINT1               u1CurrentPortRole = 0;

    if (CfaGetIfUfdGroupId (u4PortIndex, &u4UfdGroupId) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                 "CfaIfChangePortRole - Failure in getting UFD group Id\n");
        return CFA_FAILURE;
    }
    if (u4UfdGroupId == 0)
    {
        if (CfaSetIfPortRole (u4PortIndex, u1PortRole) != CFA_SUCCESS)
        {

            CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                     "CfaIfChangePortRole - Failure in setting portrole\n");
            return CFA_FAILURE;
        }
        return CFA_SUCCESS;
    }
    /* Get ufd operstatus of port */
    if (CfaGetIfUfdOperStatus (u4PortIndex, &u1UfdOperStatus) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                 "CfaIfChangePortRole - Failure in getting UFD port oper status\n");
        return CFA_FAILURE;
    }

    MEMSET (&UfdGroupInfo, 0, sizeof (tUfdGroupInfo));

    UfdGroupInfo.u4UfdGroupId = u4UfdGroupId;
    pUfdGroupInfoEntry = (tUfdGroupInfo *)
        RBTreeGet (gUfdGroupInfoTable, (tCfaIfDbElem *) & UfdGroupInfo);
    if (pUfdGroupInfoEntry == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                 "CfaIfChangePortRole - The port belongs to group which is not "
                 "existing\n");
        return CFA_FAILURE;
    }

    if (CfaGetIfPortRole (u4PortIndex, &u1CurrentPortRole) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                 "CfaIfChangePortRole - Failure in getting portrole\n");
        return CFA_FAILURE;
    }

    switch (u1CurrentPortRole)
    {
        case CFA_PORT_ROLE_UPLINK:
            /* Uplink to Downlink */
            if (pUfdGroupInfoEntry->u1UfdGroupStatus == CFA_UFD_GROUP_UP)
            {
                if (u1UfdOperStatus == CFA_IF_UP)
                {
                    /*Check any other active uplink port is present 
                     * in the group if none present, make active downlink port 
                     * oper status to moved to be 'UFD Error disabled'*/
                    if (CfaUfdCheckAnyActiveUplinkInGroup
                        (u4PortIndex, u4UfdGroupId) != CFA_TRUE)
                    {
                        /* scanning the downlink ports in the group and making the active
                         * downlink port status to Error Disabled state*/
                        for (u4IfIndex = 1; u4IfIndex <=
                             (SYS_DEF_MAX_PHYSICAL_INTERFACES +
                              LA_MAX_AGG_INTF); u4IfIndex++)
                        {
                            pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
                            if (pCfaIfInfo == NULL)
                            {
                                CFA_DBG1 (CFA_TRC_ERROR, CFA_UFD,
                                          "CfaIfChangePortRole - Interface %d not "
                                          "exists\r\n", u4IfIndex);
                                continue;
                            }

                            if (pCfaIfInfo->u4UfdGroupId ==
                                pUfdGroupInfoEntry->u4UfdGroupId
                                && pCfaIfInfo->u1UfdOperStatus == CFA_IF_UP
                                && pCfaIfInfo->u1PortRole ==
                                CFA_PORT_ROLE_DOWNLINK)
                            {
                                if (CFA_UFD_MODULE_STATUS () == CFA_UFD_ENABLED)
                                {
                                    /*  Send status indication to CFA */
                                    if (CfaIfmHandleInterfaceStatusChange
                                            (u4IfIndex, CFA_IF_DOWN, CFA_IF_DOWN,
                                             CFA_FALSE) != CFA_SUCCESS)
                                    {
                                        CFA_DBG1 (CFA_TRC_ERROR, CFA_UFD,
                                                "CfaIfChangePortRole - Changing interface "
                                                "status to down for index %d failed\n",
                                                u4IfIndex);
                                        /*if downlink port failed to 
                                         * make the oper status change, continue*/
                                        continue;
                                    }
                                }
                                CfaSetIfUfdOperStatus (u4IfIndex,
                                                       CFA_IF_UFD_ERR_DISABLED);
                                CfaSnmpifUfdSendTrap (u4IfIndex,
                                                      CFA_IF_UFD_ERR_DISABLED);
                                /* Increment port counter */
                                pCfaIfInfo->u4UfdDownlinkDisabledCount++;
                            }    /* End of if */
                        }        /* End of for */
                        pUfdGroupInfoEntry->u1UfdGroupStatus =
                            CFA_UFD_GROUP_DOWN;

                        if (CFA_UFD_MODULE_STATUS () == CFA_UFD_ENABLED)
                        {
                            /*  Send status indication to CFA */
                            if (CfaIfmHandleInterfaceStatusChange
                                    (u4PortIndex, CFA_IF_DOWN, CFA_IF_DOWN,
                                     CFA_FALSE) != CFA_SUCCESS)
                            {
                                CFA_DBG1 (CFA_TRC_ERROR, CFA_UFD,
                                        "CfaIfChangePortRole - Changing interface status "
                                        "to down for port %d failed\n",
                                        u4PortIndex);
                                return CFA_FAILURE;
                            }
                        }
                        CfaSetIfUfdOperStatus (u4PortIndex,
                                               CFA_IF_UFD_ERR_DISABLED);
                        CfaSnmpifUfdSendTrap (u4PortIndex,
                                              CFA_IF_UFD_ERR_DISABLED);
                        pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4PortIndex);
                        if (pCfaIfInfo == NULL)
                        {
                            CFA_DBG1 (CFA_TRC_ERROR, CFA_UFD,
                                      "CfaIfChangePortRole - Interface %d does not "
                                      "exist\r\n", u4PortIndex);
                            return CFA_FAILURE;
                        }

                        /* Increment port counter */
                        pCfaIfInfo->u4UfdDownlinkDisabledCount++;
                    }            /* End of if - CHeck any other Active uplink */
                }                /* End of if - Oper status up */
            }                    /* End of if - group status up */

            if (CfaSetIfPortRole (u4PortIndex, u1PortRole) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_UFD,
                          "CfaIfChangePortRole - Changing port role from uplink "
                          "to downlink failed for port %d\r\n", u4PortIndex);
                return CFA_FAILURE;
            }
            pUfdGroupInfoEntry->u4UfdDownlinkCount++;
            pUfdGroupInfoEntry->u4UfdUplinkCount--;
            /* Update the designated uplink port
             * when desig uplink port is changed to downlink*/
            if (pUfdGroupInfoEntry->u4UfdGroupDesigUplinkPort == u4PortIndex)
            {
                if (CfaUfdChangeDesigUplink (u4PortIndex, u4UfdGroupId)
                    != CFA_SUCCESS)
                {
                    CFA_DBG1 (CFA_TRC_ERROR, CFA_UFD,
                              "CfaIfChangePortRole - Changing Designated uplink "
                              "port is failed for port %d\r\n", u4PortIndex);
                    return CFA_FAILURE;
                }
            }

            /* Updating port list */
            OSIX_BITLIST_SET_BIT (pUfdGroupInfoEntry->UfdGroupDownlinkPortList,
                                  u4PortIndex, sizeof (tPortList));
            OSIX_BITLIST_RESET_BIT ((pUfdGroupInfoEntry->
                                     UfdGroupUplinkPortList), u4PortIndex,
                                    sizeof (tPortList));
            break;

        case CFA_PORT_ROLE_DOWNLINK:
            /* Downlink to Uplink */

            if (pUfdGroupInfoEntry->u4UfdDownlinkCount <= 1)
            {
                if (CFA_UFD_MODULE_STATUS () == CFA_UFD_ENABLED)
                {
                    CFA_DBG1 (CFA_TRC_ERROR, CFA_UFD,
                            "CfaIfChangePortRole - Since this is the last downlink "
                            "port , the port role cant not be changed for port %d\n",
                            u4PortIndex);
                    return CFA_FAILURE;
                }
                else
                {
                    CfaUfdRemoveAllPortsInGroup (u4UfdGroupId);
                    if (CfaSetIfPortRole (u4PortIndex, u1PortRole) != CFA_SUCCESS)
                    {
                        CFA_DBG1 (CFA_TRC_ERROR, CFA_UFD,
                                "CfaIfChangePortRole - Changing port role from downlink "
                                "to uplink failed for port %d\n", u4PortIndex);
                        return CFA_FAILURE;
                    }
                    break;
                }
            }
            else
            {
                if (pUfdGroupInfoEntry->u1UfdGroupStatus == CFA_UFD_GROUP_DOWN)
                {
                    if (u1UfdOperStatus == CFA_IF_UFD_ERR_DISABLED)
                    {
                        /* Scanning the Downlink ports */
                        for (u4IfIndex = 1; u4IfIndex <=
                             (SYS_DEF_MAX_PHYSICAL_INTERFACES +
                              LA_MAX_AGG_INTF); u4IfIndex++)
                        {
                            if (u4IfIndex != u4PortIndex)
                            {
                                pCfaIfInfo =
                                    CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
                                if (pCfaIfInfo == NULL)
                                {
                                    CFA_DBG1 (CFA_TRC_ERROR, CFA_UFD,
                                              "CfaIfChangePortRole - Interface %d not "
                                              "exists\r\n", u4IfIndex);
                                    continue;
                                }
                                if (pCfaIfInfo->u4UfdGroupId ==
                                    pUfdGroupInfoEntry->u4UfdGroupId
                                    && pCfaIfInfo->u1UfdOperStatus ==
                                    CFA_IF_UFD_ERR_DISABLED
                                    && pCfaIfInfo->u1PortRole ==
                                    CFA_PORT_ROLE_DOWNLINK)
                                {
                                    CfaSetIfUfdOperStatus (u4IfIndex,
                                                           CFA_IF_UP);

                                    CfaSnmpifUfdSendTrap (u4IfIndex, CFA_IF_UP);
                                    if (CFA_UFD_MODULE_STATUS () == CFA_UFD_ENABLED)
                                    {
                                        if (CfaIfmHandleInterfaceStatusChange
                                                (u4IfIndex, CFA_IF_UP, CFA_IF_UP,
                                                 CFA_FALSE) != CFA_SUCCESS)
                                        {
                                            CFA_DBG1 (CFA_TRC_ERROR, CFA_UFD,
                                                    "CfaIfChangePortRole - Changing interface "
                                                    "status to down for index %d failed\n",
                                                    u4IfIndex);
                                            continue;
                                        }
                                    }
                                    /* Increment port counter */
                                    pCfaIfInfo->u4UfdDownlinkEnabledCount++;
                                }    /* End if */
                            }    /* End if - port index other than u4PortIndex */
                        }        /* End for */
                        pUfdGroupInfoEntry->u1UfdGroupStatus = CFA_UFD_GROUP_UP;
                        CfaSetIfUfdOperStatus (u4PortIndex, CFA_IF_UP);
                        CfaSnmpifUfdSendTrap (u4PortIndex, CFA_IF_UP);
                        if (CFA_UFD_MODULE_STATUS () == CFA_UFD_ENABLED)
                        {
                            if (CfaIfmHandleInterfaceStatusChange
                                    (u4PortIndex, CFA_IF_UP, CFA_IF_UP,
                                     CFA_FALSE) != CFA_SUCCESS)
                            {
                                CFA_DBG1 (CFA_TRC_ERROR, CFA_UFD,
                                        "CfaIfChangePortRole - Changing interface "
                                        "status to down for port %d failed\n",
                                        u4PortIndex);
                                return CFA_FAILURE;
                            }
                        }
                        pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4PortIndex);
                        if (pCfaIfInfo == NULL)
                        {
                            CFA_DBG1 (CFA_TRC_ERROR, CFA_UFD,
                                      "CfaIfChangePortRole - Interface %d does not "
                                      "exists\r\n", u4IfIndex);
                            return CFA_FAILURE;
                        }
                        /* Increment port counter */
                        pCfaIfInfo->u4UfdDownlinkEnabledCount++;
                    }            /* End if - Ufd Oper Status is Error Disabled */
                }                /* End if - Ufd Group status down */
            }                    /* End if - more than one downlink is there */
            if (CfaSetIfPortRole (u4PortIndex, u1PortRole) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_UFD,
                          "CfaIfChangePortRole - Changing port role from downlink "
                          "to uplink failed for port %d\n", u4PortIndex);
                return CFA_FAILURE;
            }
            /* when no uplink in the group, very first uplink port
               should be the designated uplink */
            if (pUfdGroupInfoEntry->u4UfdUplinkCount == 0)
            {
                pUfdGroupInfoEntry->u4UfdGroupDesigUplinkPort = u4PortIndex;
            }
            pUfdGroupInfoEntry->u4UfdDownlinkCount--;
            pUfdGroupInfoEntry->u4UfdUplinkCount++;
            /* Updating port list */
            OSIX_BITLIST_RESET_BIT ((pUfdGroupInfoEntry->
                                     UfdGroupDownlinkPortList), u4PortIndex,
                                    sizeof (tPortList));
            OSIX_BITLIST_SET_BIT ((pUfdGroupInfoEntry->UfdGroupUplinkPortList),
                                  u4PortIndex, sizeof (tPortList));
            /* Resetting downlink enabled/disabled counts to zero */
            CFA_CDB_IF_UFD_DOWNLINKDISABLED_COUNT (u4PortIndex) = 0;
            CFA_CDB_IF_UFD_DOWNLINKENABLED_COUNT (u4PortIndex) = 0;
            break;
        default:
            break;
    }                            /* End of switch */
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdCreateGroup
 *
 *    Description        : This function adds a row to the Ufd Group Info DB.
 *
 *    Input(s)           : UfdGroupId.
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if addition succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaUfdCreateGroup (INT4 i4UfdGroupId)
{
    tUfdGroupInfo      *pUfdGroupInfoEntry = NULL;

    pUfdGroupInfoEntry =
        (tUfdGroupInfo *) MemAllocMemBlk (gCfaUfdGroupMemPoolId);

    if (NULL == pUfdGroupInfoEntry)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                 "CfaUfdCreateGroup - memory allocation failed for tUfdGroupInfo\n");
        return CFA_FAILURE;
    }

    MEMSET (pUfdGroupInfoEntry, 0, sizeof (tUfdGroupInfo));

    pUfdGroupInfoEntry->u4UfdGroupId = (UINT4) i4UfdGroupId;
    pUfdGroupInfoEntry->u1UfdGroupStatus = CFA_UFD_GROUP_DOWN;
    pUfdGroupInfoEntry->u4UfdUplinkCount = 0;
    pUfdGroupInfoEntry->u4UfdDownlinkCount = 0;
    pUfdGroupInfoEntry->u4UfdGroupDesigUplinkPort = 0;

    if ((INT4)
        RBTreeAdd (gUfdGroupInfoTable,
                   (tCfaIfDbElem *) pUfdGroupInfoEntry) != RB_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_UFD,
                 "Failure in UFD Group Info RBTree add\n");
        MemReleaseMemBlock (gCfaUfdGroupMemPoolId,
                            (UINT1 *) pUfdGroupInfoEntry);
    }
    gu4UfdGroupCount++;

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdDeleteGroup
 *
 *    Description        : This function deletes a row from the UfdGroupInfoDB.
 *
 *    Input(s)           : UfdGroupId.
 *
 *    Output(s)          : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
VOID
CfaUfdDeleteGroup (INT4 i4UfdGroupId)
{
    tUfdGroupInfo       UfdGroupInfo;
    tUfdGroupInfo      *pUfdGroupInfoEntry = NULL;
    tCfaIfInfo         *pCfaIfInfo = NULL;
    UINT4               u4IfIndex = 0;
    MEMSET (&UfdGroupInfo, 0, sizeof (tUfdGroupInfo));

    UfdGroupInfo.u4UfdGroupId = (UINT4) i4UfdGroupId;
    pUfdGroupInfoEntry = (tUfdGroupInfo *)
        RBTreeGet (gUfdGroupInfoTable, (tCfaIfDbElem *) & UfdGroupInfo);
    if (pUfdGroupInfoEntry != NULL)
    {
        /* Delete the group details in ports table */
        for (u4IfIndex = 1; u4IfIndex <=
             (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF); u4IfIndex++)
        {
            pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
            if (pCfaIfInfo == NULL)
            {
                continue;
            }
            if (pCfaIfInfo->u4UfdGroupId == pUfdGroupInfoEntry->u4UfdGroupId)
            {
                pCfaIfInfo->u4UfdGroupId = 0;
                pCfaIfInfo->u4UfdDownlinkDisabledCount = 0;
                pCfaIfInfo->u4UfdDownlinkEnabledCount = 0;
                if (pCfaIfInfo->u1UfdOperStatus == CFA_IF_UFD_ERR_DISABLED)
                {
                    CfaSetIfUfdOperStatus (u4IfIndex, CFA_IF_UP);
                    CfaSnmpifUfdSendTrap (u4IfIndex, CFA_IF_UP);
                    if (CfaIfmHandleInterfaceStatusChange (u4IfIndex,
                                                           CFA_IF_UP, CFA_IF_UP,
                                                           CFA_FALSE) !=
                        CFA_SUCCESS)
                    {
                        /* port status setting failed; moving to next port */

                        continue;
                    }
                    /* Increment port counter */
                    pCfaIfInfo->u4UfdDownlinkEnabledCount++;
                }
            }
        }
        /* Delete the group entry with particular group id */

        RBTreeRemove (gUfdGroupInfoTable, (tRBElem *) pUfdGroupInfoEntry);

        MemReleaseMemBlock (gCfaUfdGroupMemPoolId,
                            (UINT1 *) pUfdGroupInfoEntry);
        pUfdGroupInfoEntry = NULL;
        gu4UfdGroupCount--;
    }
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdAddPortToGroup
 *
 *    Description        : This function adds a port to given Ufd Group.
 *
 *    Input(s)           : UfdGroupId , PortIndex.
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if addition of port succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaUfdAddPortToGroup (UINT4 u4PortIndex, UINT4 u4UfdGroupId)
{
    tUfdGroupInfo      *pUfdGroupInfo = NULL;
    UINT4               u4CurrentUfdGroupId = 0;
    UINT1               u1IfPortRole = 0;

    /* Check if already present in group */
    if (CfaGetIfUfdGroupId (u4PortIndex, &u4CurrentUfdGroupId) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdAddPortToGroup - Given port %d is not existing\r\n",
                  u4PortIndex);
        return CFA_FAILURE;
    }
    if (u4CurrentUfdGroupId == u4UfdGroupId)
    {
        CFA_DBG2 (CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdAddPortToGroup - Given port %d is already a member "
                  "in given group %d\n", u4PortIndex, u4UfdGroupId);
        return CFA_SUCCESS;
    }
    else if (u4CurrentUfdGroupId != 0)
    {
        CFA_DBG2 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdAddPortToGroup - Port %d is already a member in "
                  "another group %d\r\n", u4PortIndex, u4CurrentUfdGroupId);
        return CFA_FAILURE;
    }

    /* Check port role */
    if (CfaGetIfPortRole (u4PortIndex, &u1IfPortRole) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdAddPortToGroup - Port role getting failed for port %d\n",
                  u4PortIndex);
        return CFA_FAILURE;
    }

    pUfdGroupInfo = CfaIfUfdGroupInfoDbGet (u4UfdGroupId);
    if (pUfdGroupInfo == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdAddPortToGroup - Group  %d does not exist\n",
                  u4UfdGroupId);
        return CFA_FAILURE;
    }

    if (u1IfPortRole == CFA_PORT_ROLE_UPLINK)
    {
        /* Add uplink  port to the Group */
        if (CfaUfdAddUplinkPort (u4PortIndex, u4UfdGroupId) != CFA_SUCCESS)
        {
            CFA_DBG2 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                      "CfaUfdAddPortToGroup - Error in adding the uplink port "
                      "%d in group %d\n", u4PortIndex, u4UfdGroupId);
            return CFA_FAILURE;
        }
        /* Adding port to port list */
        OSIX_BITLIST_SET_BIT ((pUfdGroupInfo->UfdGroupUplinkPortList),
                              (UINT2) u4PortIndex, sizeof (tPortList));
    }
    else if (u1IfPortRole == CFA_PORT_ROLE_DOWNLINK)
    {
        /* Add Downlink port to the Group */
        if (CfaUfdAddDownlinkPort (u4PortIndex, u4UfdGroupId) != CFA_SUCCESS)
        {
            CFA_DBG2 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                      "CfaUfdAddPortToGroup - Error in adding downlink port "
                      "%d in group %d\n", u4PortIndex, u4UfdGroupId);
            return CFA_FAILURE;
        }
        /* Adding port to port list */
        OSIX_BITLIST_SET_BIT ((pUfdGroupInfo->UfdGroupDownlinkPortList),
                              (UINT2) u4PortIndex, sizeof (tPortList));
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdRemovePortFromGroup
 *
 *    Description        : This function deletes a port from the Ufd Group.
 *
 *    Input(s)           : UfdGroupId , u4PortIndex.
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if deletion of port succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaUfdRemovePortFromGroup (UINT4 u4PortIndex, UINT4 u4UfdGroupId)
{
    tUfdGroupInfo      *pUfdGroupInfo = NULL;
    UINT1               u1IfPortRole = 0;

    if (CfaGetIfUfdGroupId (u4PortIndex, &u4UfdGroupId) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdRemovePortFromGroup- Given port %d is not existing\r\n",
                  u4PortIndex);
        return CFA_FAILURE;
    }

    /* Check port role */
    if (CfaGetIfPortRole (u4PortIndex, &u1IfPortRole) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdRemovePortFromGroup - Port role getting failed for port %d\n",
                  u4PortIndex);
        return CFA_FAILURE;
    }

    pUfdGroupInfo = CfaIfUfdGroupInfoDbGet (u4UfdGroupId);
    if (pUfdGroupInfo == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdRemovePortFromGroup - Group  %d does not exist\n",
                  u4UfdGroupId);
        return CFA_FAILURE;
    }

    if (u1IfPortRole == CFA_PORT_ROLE_UPLINK)
    {
        /* Remove uplink  port from the Group */
        if (CfaUfdRemoveUplinkPort (u4PortIndex, u4UfdGroupId) != CFA_SUCCESS)
        {
            CFA_DBG2 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                      "CfaUfdRemovePortFromGroup - Error in removing the uplink port "
                      "%d from group %d\n", u4PortIndex, u4UfdGroupId);
            return CFA_FAILURE;
        }
        /* Removing port from port list */
        OSIX_BITLIST_RESET_BIT ((pUfdGroupInfo->UfdGroupUplinkPortList),
                                (UINT2) u4PortIndex, sizeof (tPortList));

    }
    else if (u1IfPortRole == CFA_PORT_ROLE_DOWNLINK)
    {
        /* Remove Downlink port from the Group */
        if (CfaUfdRemoveDownlinkPort (u4PortIndex, u4UfdGroupId) != CFA_SUCCESS)
        {
            CFA_DBG2 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                      "CfaUfdRemovePortFromGroup - Error in removing the downlink port "
                      "%d from group %d\n", u4PortIndex, u4UfdGroupId);
            return CFA_FAILURE;
        }
        /* Removing port from port list */
        OSIX_BITLIST_RESET_BIT ((pUfdGroupInfo->UfdGroupDownlinkPortList),
                                (UINT2) u4PortIndex, sizeof (tPortList));

        /* Reset ports enabled/disabled counts to zero */
        CFA_CDB_IF_UFD_DOWNLINKDISABLED_COUNT (u4PortIndex) = 0;
        CFA_CDB_IF_UFD_DOWNLINKENABLED_COUNT (u4PortIndex) = 0;

    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdAddUplinkPort
 *
 *    Description        : This function adds a Uplink port to given Ufd Group.
 *
 *    Input(s)           : UfdGroupId , PortIndex.
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if addition of port succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaUfdAddUplinkPort (UINT4 u4PortIndex, UINT4 u4UfdGroupId)
{
    tUfdGroupInfo      *pUfdGroupInfoEntry = NULL;
    tCfaIfInfo         *pCfaIfInfo = NULL;
    UINT4               u4IfIndex;
    UINT1               u1IfAdminStatus = 0;
    UINT1               u1IfOperStatus = 0;

    pUfdGroupInfoEntry = CfaIfUfdGroupInfoDbGet (u4UfdGroupId);
    /* Check for existence of group id */
    if (pUfdGroupInfoEntry == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdAddUplinkPort - Group  %d does not exist\n",
                  u4UfdGroupId);
        return CFA_FAILURE;
    }
    /* Check if portlist has reached maximum */
    if (pUfdGroupInfoEntry->u4UfdUplinkCount == CFA_UFD_GROUP_MAX_UPLINK)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdAddUplinkPort - Uplink portlist reached max count %d\n",
                  pUfdGroupInfoEntry->u4UfdUplinkCount);
        return CFA_FAILURE;
    }
    CfaGetIfOperStatus (u4PortIndex, &u1IfOperStatus);
        /* Check port status */
        if (CfaGetCdbPortAdminStatus (u4PortIndex, &u1IfAdminStatus) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                      "CfaUfdAddUplinkPort - Getting  Admin status "
                      "failed for port %d\n", u4PortIndex);
            return CFA_FAILURE;
        }


    /* Check group status */
    /* If group is up, add the port */
    if (pUfdGroupInfoEntry->u1UfdGroupStatus == CFA_UFD_GROUP_UP)
    {
        if (CfaSetIfUfdGroupId (u4PortIndex, u4UfdGroupId) != CFA_SUCCESS)
        {
            CFA_DBG2 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                      "CfaUfdAddUplinkPort - Setting  group Id %d is "
                      "failed for port %d\n", u4UfdGroupId, u4PortIndex);
            return CFA_FAILURE;
        }
    }
    else if (pUfdGroupInfoEntry->u1UfdGroupStatus == CFA_UFD_GROUP_DOWN)
    {
        if (u1IfAdminStatus == CFA_IF_UP && u1IfOperStatus == CFA_IF_UP)
        {
            /* No active Uplink port(s) is present
             * Change status for downlink ports */
            for (u4IfIndex = 1; u4IfIndex <=
                 (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF);
                 u4IfIndex++)
            {
                pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
                if (pCfaIfInfo == NULL)
                {
                    continue;
                }

                if (pCfaIfInfo->u4UfdGroupId == u4UfdGroupId &&
                    pCfaIfInfo->u1PortRole == CFA_PORT_ROLE_DOWNLINK)
                {
                    if (pCfaIfInfo->u1UfdOperStatus == CFA_IF_UFD_ERR_DISABLED)
                    {
                        /* make ufd port status up;
                         * Ufd group status up;
                         * send portstatus indication to cfa;*/
                        pCfaIfInfo->u1UfdOperStatus = CFA_IF_UP;
                        CfaSnmpifUfdSendTrap (u4IfIndex, CFA_IF_UP);
                        if (CfaIfmHandleInterfaceStatusChange
                            (u4IfIndex, CFA_IF_UP, CFA_IF_UP,
                             CFA_FALSE) != CFA_SUCCESS)
                        {
                            continue;
                        }
                        pCfaIfInfo->u4UfdDownlinkEnabledCount++;
                    }
                }
            }
            pUfdGroupInfoEntry->u1UfdGroupStatus = CFA_UFD_GROUP_UP;
            if (CfaSetIfUfdGroupId (u4PortIndex, u4UfdGroupId) != CFA_SUCCESS)
            {
                CFA_DBG2 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                          "CfaUfdAddUplinkPort - Setting  group Id %d is "
                          "failed for port %d at port oper up\n", u4UfdGroupId,
                          u4PortIndex);
                return CFA_FAILURE;
            }
        }
        else
        {
            /* Add port to group */
            if (CfaSetIfUfdGroupId (u4PortIndex, u4UfdGroupId) != CFA_SUCCESS)
            {
                CFA_DBG2 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                          "CfaUfdAddUplinkPort - Setting  group Id %d is "
                          "failed for port %d at port oper down\n",
                          u4UfdGroupId, u4PortIndex);
                return CFA_FAILURE;
            }

        }
    }

     /* FIx for bug 7475 - Updating UfdOperStatus at addition of uplink ports*/
     if (u1IfAdminStatus == CFA_IF_UP && u1IfOperStatus == CFA_IF_UP)
     {

         if (CfaSetIfUfdOperStatus (u4PortIndex, 
                                      CFA_IF_UP) != CFA_SUCCESS)
         {
             CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                       "CfaUfdAddUplinkPort - Setting  UFD "
                       "oper status failed for port %d at adding uplink port\n",
                       u4PortIndex);
             return CFA_FAILURE;
         }
     }
     else
     {
         if (CfaSetIfUfdOperStatus (u4PortIndex,
                                     CFA_IF_DOWN) != CFA_SUCCESS)
         {
             CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                     "CfaUfdAddUplinkPort - Setting  UFD "
                     "oper status failed for port %d at adding uplink port\n",
                     u4PortIndex);
             return CFA_FAILURE;
         }
     }

    /* when no uplink in the group, very first uplink port
       should be the designated uplink */
    if (pUfdGroupInfoEntry->u4UfdUplinkCount == 0)
    {
        pUfdGroupInfoEntry->u4UfdGroupDesigUplinkPort = u4PortIndex;
    }
    pUfdGroupInfoEntry->u4UfdUplinkCount++;
    /* NOTE : Updation of portlist to be done by the calling function */
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdAddDownlinkPort
 *
 *    Description        : This function adds downlink port to given Ufd Group.
 *
 *    Input(s)           : UfdGroupId , PortIndex.
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if addition of port succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaUfdAddDownlinkPort (UINT4 u4PortIndex, UINT4 u4UfdGroupId)
{
    tUfdGroupInfo      *pUfdGroupInfoEntry = NULL;
    UINT1               u1IfAdminStatus = 0;
    UINT1               u1IfOperStatus = 0;

    pUfdGroupInfoEntry = CfaIfUfdGroupInfoDbGet (u4UfdGroupId);
    /* Check for existence of group id */
    if (pUfdGroupInfoEntry == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdAddDownlinkPort - Group  %d does not exist\n",
                  u4UfdGroupId);
        return CFA_FAILURE;
    }
    /* Check if portlist has reached maximum */
    if (pUfdGroupInfoEntry->u4UfdDownlinkCount == CFA_UFD_GROUP_MAX_DOWNLINK)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdAddDownlinkPort - Downlink portlist reached max count %d\n",
                  pUfdGroupInfoEntry->u4UfdDownlinkCount);
        return CFA_FAILURE;
    }
    CfaGetIfOperStatus (u4PortIndex, &u1IfOperStatus);

    /* Check port status */
    if (CfaGetCdbPortAdminStatus (u4PortIndex, &u1IfAdminStatus) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdAddDownlinkPort - Getting Admin status "
                  "failed for port %d\n", u4PortIndex);
        return CFA_FAILURE;
    }

    if (u1IfAdminStatus == CFA_IF_UP && u1IfOperStatus == CFA_IF_UP)
    {
        if (pUfdGroupInfoEntry->u1UfdGroupStatus == CFA_UFD_GROUP_UP)
        {
            /* Add port to group */
            if (CfaSetIfUfdGroupId (u4PortIndex, u4UfdGroupId) != CFA_SUCCESS)
            {
                CFA_DBG2 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                          "CfaUfdAddDownlinkPort - Setting  group Id %d is "
                          "failed for port %d at port oper up\n", u4UfdGroupId,
                          u4PortIndex);
                return CFA_FAILURE;
            }
        }
        else if (pUfdGroupInfoEntry->u1UfdGroupStatus == CFA_UFD_GROUP_DOWN)
        {
            /* add port make it err_disabled
             * send status indication to CFA */
            if (CfaSetIfUfdGroupId (u4PortIndex, u4UfdGroupId) != CFA_SUCCESS)
            {
                CFA_DBG2 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                          "CfaUfdAddDownlinkPort - Setting  group Id %d is "
                          "failed for port %d at group status down\n",
                          u4UfdGroupId, u4PortIndex);
                return CFA_FAILURE;
            }
            if (CfaIfmHandleInterfaceStatusChange
                (u4PortIndex, (UINT1) CFA_IF_DOWN, (UINT1) CFA_IF_DOWN,
                 CFA_FALSE) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_UFD,
                          "CfaUfdAddDownlinkPort - Changing interface "
                          "status to down for port %d failed\n", u4PortIndex);
                return CFA_FAILURE;
            }
            if (CfaSetIfUfdOperStatus (u4PortIndex, CFA_IF_UFD_ERR_DISABLED)
                != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                          "CfaUfdAddDownlinkPort - Setting  UFD oper status "
                          "failed for port %d\n", u4PortIndex);
                return CFA_FAILURE;
            }
            CfaSnmpifUfdSendTrap (u4PortIndex, CFA_IF_UFD_ERR_DISABLED);
            /* Increment port counter */
            CFA_CDB_IF_UFD_DOWNLINKDISABLED_COUNT (u4PortIndex) =
                CFA_CDB_IF_UFD_DOWNLINKDISABLED_COUNT (u4PortIndex) + 1;
        }
    }
    else
    {
        if (CfaSetIfUfdGroupId (u4PortIndex, u4UfdGroupId) != CFA_SUCCESS)
        {
            CFA_DBG2 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                      "CfaUfdAddDownlinkPort - Setting  group Id %d is "
                      "failed for port %d at oper status down\n",
                      u4UfdGroupId, u4PortIndex);
            return CFA_FAILURE;
        }
        if (CfaSetIfUfdOperStatus (u4PortIndex, CFA_IF_DOWN)
                != CFA_SUCCESS)
        {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                          "CfaUfdAddDownlinkPort - Setting  UFD oper status "
                          "failed for port %d\n", u4PortIndex);
                return CFA_FAILURE;
            }

    }
    pUfdGroupInfoEntry->u4UfdDownlinkCount++;
    /* NOTE : Updation of portlist to be done by the calling function */
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdRemoveUplinkPort
 *
 *    Description        : This function deletes Uplink port from the Ufd Group.
 *
 *    Input(s)           : UfdGroupId , u4PortIndex.
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if deletion of port succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaUfdRemoveUplinkPort (UINT4 u4PortIndex, UINT4 u4UfdGroupId)
{
    tUfdGroupInfo      *pUfdGroupInfoEntry = NULL;
    tCfaIfInfo         *pCfaIfInfo = NULL;
    UINT4               u4IfIndex;
    UINT1               u1IfUfdOperStatus = 0;

    if (CfaGetIfUfdGroupId (u4PortIndex, &u4UfdGroupId) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdRemoveUplinkPort - Given port %d is not existing\r\n",
                  u4PortIndex);
        return CFA_FAILURE;
    }

    pUfdGroupInfoEntry = CfaIfUfdGroupInfoDbGet (u4UfdGroupId);
    /* Check for existence of group id */
    if (pUfdGroupInfoEntry == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdRemoveUplinkPort - Group  %d does not exist\n",
                  u4UfdGroupId);
        return CFA_FAILURE;
    }
    if (pUfdGroupInfoEntry->u1UfdGroupStatus == CFA_UFD_GROUP_UP)
    {
        /* Check port status */
        if (CfaGetIfUfdOperStatus (u4PortIndex, &u1IfUfdOperStatus)
            != CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                     "CfaUfdRemoveUplinkPort - Failure in getting UFD "
                     "port oper status\n");
            return CFA_FAILURE;
        }
        if (u1IfUfdOperStatus == CFA_IF_UP)
        {
            /* Check this port is only active member in the group */
            if (CfaUfdCheckAnyActiveUplinkInGroup
                (u4PortIndex, u4UfdGroupId) != CFA_TRUE)
            {

                /* No active Uplink port(s) is present
                 * Change status for downlink ports */
                for (u4IfIndex = 1; u4IfIndex <=
                     (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF);
                     u4IfIndex++)
                {
                    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
                    if (pCfaIfInfo == NULL)
                    {
                        continue;
                    }

                    if (pCfaIfInfo->u4UfdGroupId == u4UfdGroupId &&
                        pCfaIfInfo->u1PortRole == CFA_PORT_ROLE_DOWNLINK &&
                        pCfaIfInfo->u1UfdOperStatus == CFA_IF_UP)
                    {

                        /*  Send status indication to CFA */
                        if (CfaIfmHandleInterfaceStatusChange
                            (u4IfIndex, CFA_IF_DOWN, CFA_IF_DOWN,
                             CFA_FALSE) != CFA_SUCCESS)
                        {
                            continue;
                        }
                        /* Make the port to be Error_disable */
                        if (CfaSetIfUfdOperStatus
                            (u4IfIndex, CFA_IF_UFD_ERR_DISABLED) != CFA_SUCCESS)
                        {
                            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                                      CFA_UFD,
                                      "CfaUfdRemoveUplinkPort - Setting  UFD oper status "
                                      "failed for port %d\n", u4IfIndex);
                            return CFA_FAILURE;
                        }
                        CfaSnmpifUfdSendTrap (u4IfIndex,
                                              CFA_IF_UFD_ERR_DISABLED);
                        /* Increment port disabled count */
                        pCfaIfInfo->u4UfdDownlinkDisabledCount++;
                    }
                }

                /*  Make group status is Down */

                pUfdGroupInfoEntry->u1UfdGroupStatus = CFA_UFD_GROUP_DOWN;
            }
        }
    }
    /* This will be hit in cases of :
     * 1. There are more active uplink ports
     * so simply remove the port */
    /* 2.if group is up and port is down */
    /* 3. if group status is down */
    if (CfaSetIfUfdGroupId (u4PortIndex, 0) != CFA_SUCCESS)
    {
        CFA_DBG2 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdRemoveUplinkPort - Setting  group Id %d is "
                  "failed for port %d at port oper up\n", u4UfdGroupId,
                  u4PortIndex);
        return CFA_FAILURE;
    }
    pUfdGroupInfoEntry->u4UfdUplinkCount--;
    if (pUfdGroupInfoEntry->u4UfdGroupDesigUplinkPort == u4PortIndex)
    {
        if (CfaUfdChangeDesigUplink (u4PortIndex, u4UfdGroupId) != CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                     "CfaUfdRemoveUplinkPort - Changing Designated uplink "
                     "port is failed\n");
            return CFA_FAILURE;
        }
    }
    /* NOTE : Updation of portlist to be done by the calling function */
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdRemoveDownlinkPort
 *
 *    Description        : This function deletes Downlink port from the Ufd Group.
 *
 *    Input(s)           : UfdGroupId , u4PortIndex.
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if deletion of port succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaUfdRemoveDownlinkPort (UINT4 u4PortIndex, UINT4 u4UfdGroupId)
{
    tUfdGroupInfo      *pUfdGroupInfoEntry = NULL;
    UINT1               u1IfUfdOperStatus = 0;

    if (CfaGetIfUfdGroupId (u4PortIndex, &u4UfdGroupId) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdRemoveDownlinkPort - Given port %d is not existing\r\n",
                  u4PortIndex);
        return CFA_FAILURE;
    }

    pUfdGroupInfoEntry = CfaIfUfdGroupInfoDbGet (u4UfdGroupId);
    /* Check for existence of group id */
    if (pUfdGroupInfoEntry == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdRemoveDownlinkPort - Group  %d does not exist\n",
                  u4UfdGroupId);
        return CFA_FAILURE;
    }

    if (pUfdGroupInfoEntry->u4UfdDownlinkCount == 1)
    {
        if (pUfdGroupInfoEntry->u4UfdUplinkCount > 0)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                     "CfaUfdRemoveDownlinkPort - Given group has uplinks and only one "
                     "downlink , which cant be deleted");
            return CFA_FAILURE;
        }
    }

    if (pUfdGroupInfoEntry->u1UfdGroupStatus == CFA_UFD_GROUP_DOWN)
    {
        /* Check port status */
        if (CfaGetIfUfdOperStatus (u4PortIndex, &u1IfUfdOperStatus)
            != CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                     "CfaUfdRemoveDownlinkPort - Failure in getting "
                     " UFD port oper status\n");
            return CFA_FAILURE;
        }
        if (u1IfUfdOperStatus == CFA_IF_UFD_ERR_DISABLED)
        {
            /* make port up */
            if (CfaSetIfUfdOperStatus (u4PortIndex, CFA_IF_UP) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                          "CfaUfdRemoveDownlinkPort- Setting  UFD oper status "
                          "failed for port %d\n", u4PortIndex);
                return CFA_FAILURE;
            }

            CfaSnmpifUfdSendTrap (u4PortIndex, CFA_IF_UP);

            /*  Send status indication to CFA */
            if (CfaIfmHandleInterfaceStatusChange (u4PortIndex, CFA_IF_UP,
                                                   CFA_IF_UP, CFA_FALSE)
                != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                          "CfaUfdRemoveDownlinkPort- Setting  oper status as UP "
                          "failed for port %d\n", u4PortIndex);
                return CFA_FAILURE;
            }

        }
    }

    /* when no uplinks are there and this is the only uplink, simply delete */
    /* Or if the UfdGroupStatus is UP or DOWN,with more downlinks, simply delete */
    if (CfaSetIfUfdGroupId (u4PortIndex, 0) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdRemoveDownlinkPort - Resetting group id is "
                  "failed for port %d \n", u4PortIndex);
        return CFA_FAILURE;
    }
    pUfdGroupInfoEntry->u4UfdDownlinkCount--;
    /* NOTE : Updation of portlist to be done by the calling function */

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdChangeDesigUplink
 *
 *    Description        : This function is called from CFA when desig uplink
 *                         port is changed
 *
 *    Input(s)           : u4PortIndex
 *                         u4UfdGroupId
 *
 *    Output(s)          : None
 *
 *    Returns            : CFA_SUCCESS if changes succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaUfdChangeDesigUplink (UINT4 u4PortIndex, UINT4 u4UfdGroupId)
{
    tUfdGroupInfo       UfdGroupInfo;
    tUfdGroupInfo      *pUfdGroupInfoEntry = NULL;
    UINT4               u4DesigUplink = 0;
    UINT2               u2ByteIndex = 0;
    UINT1               u1PortFlag = 0;
    UINT2               u2BitIndex = 0;

    MEMSET (&UfdGroupInfo, 0, sizeof (tUfdGroupInfo));

    UfdGroupInfo.u4UfdGroupId = u4UfdGroupId;
    pUfdGroupInfoEntry = (tUfdGroupInfo *)
        RBTreeGet (gUfdGroupInfoTable, (tCfaIfDbElem *) & UfdGroupInfo);
    if (pUfdGroupInfoEntry == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                 "CfaUfdChangeDesigUplink - The port belongs to group "
                 "which is not existing\r\n");
        return CFA_FAILURE;
    }
    /*if no uplink port present in the group, 
     * make designated uplink port as zero*/
    if (pUfdGroupInfoEntry->u4UfdUplinkCount == 0)
    {
        pUfdGroupInfoEntry->u4UfdGroupDesigUplinkPort = 0;
        return CFA_SUCCESS;
    }
    for (u2ByteIndex = 0; u2ByteIndex < BRG_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = pUfdGroupInfoEntry->UfdGroupUplinkPortList[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < BITS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & INTERNAL_BIT8) != 0)
            {
                u4DesigUplink =
                    (UINT4) ((u2ByteIndex * BITS_PER_BYTE) + u2BitIndex + 1);
                if (u4DesigUplink != u4PortIndex)
                {
                    pUfdGroupInfoEntry->u4UfdGroupDesigUplinkPort =
                        u4DesigUplink;
                    break;
                }

            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
        /*if designated uplink port is selected
         * no need to scan the other ports in the group*/
        if (pUfdGroupInfoEntry->u4UfdGroupDesigUplinkPort != u4PortIndex)
        {
            break;
        }
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdSetGroupStatus
 *
 *    Description        : This function set the group status.
 *
 *    Input(s)           : UfdGroupId.
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if deletion succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaUfdSetGroupStatus (UINT4 u4UfdGroupId, INT4 i4UfdGroupStatus)
{

    tUfdGroupInfo      *pUfdGroupInfoEntry = NULL;

    pUfdGroupInfoEntry = CfaIfUfdGroupInfoDbGet (u4UfdGroupId);
    if (pUfdGroupInfoEntry == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdSetGroupStatus - Group  %d does not exist\n",
                  u4UfdGroupId);
        return CFA_FAILURE;
    }
    pUfdGroupInfoEntry->u1UfdGroupStatus = (UINT1) i4UfdGroupStatus;

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdGetGroupStatus
 *
 *    Description        : This function gets Ufd group status from UfdGroup.
 *
 *    Input(s)           : UfdGroupId.
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if RBTreeGet succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaUfdGetGroupStatus (UINT4 u4UfdGroupId, UINT1 *u1UfdGroupStatus)
{
    tUfdGroupInfo      *pUfdGroupInfoEntry = NULL;

    pUfdGroupInfoEntry = CfaIfUfdGroupInfoDbGet (u4UfdGroupId);
    if (pUfdGroupInfoEntry == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdGetGroupStatus - Group  %d does not exist\n",
                  u4UfdGroupId);
        return CFA_FAILURE;
    }
    *u1UfdGroupStatus = pUfdGroupInfoEntry->u1UfdGroupStatus;

    return CFA_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdDeleteAllGroupEntries
 *
 *    Description        : This function delete all Ufd groups.
 *
 *    Input(s)           : UfdGroupId.
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if RBTreeGet succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaUfdDeleteAllGroupEntries (VOID)
{

    tUfdGroupInfo      *pUfdGroupInfoTable = NULL;

    pUfdGroupInfoTable = (tUfdGroupInfo *) RBTreeGetFirst (gUfdGroupInfoTable);

    while (pUfdGroupInfoTable != NULL)
    {
        CfaUfdDeleteGroup ((INT4) pUfdGroupInfoTable->u4UfdGroupId);
        pUfdGroupInfoTable = (tUfdGroupInfo *) RBTreeGetNext
            (gUfdGroupInfoTable, (tRBElem *) pUfdGroupInfoTable, NULL);
    }
    if (gUfdGroupInfoTable != NULL)
    {
        RBTreeDelete (gUfdGroupInfoTable);
        gUfdGroupInfoTable = NULL;
    }

    return CFA_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdCheckPortRole
 *
 *    Description        : This function check port role.
 *
 *    Input(s)           : UfdGroupId, PortRole.
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_TRUE if PortRole of given port is same as
 *                         port role passed, otherwise CFA_FALSE.
 *
 *****************************************************************************/

INT4
CfaUfdCheckPortRole (UINT4 u4IfIndex, UINT1 u1PortRole)
{
    UINT1               u1ActualPortRole = 0;
    if (CfaGetIfPortRole (u4IfIndex, &u1ActualPortRole) == CFA_SUCCESS)
    {
        if (u1ActualPortRole == u1PortRole)
        {
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                      "CfaUfdCheckPortRole - Port role matches for port %d\n",
                      u4IfIndex);
            return CFA_TRUE;
        }
    }

    return CFA_FALSE;
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdCheckAnyActiveUplinkInGroup
 *
 *    Description        : This function check whether the group is having any other 
 *                         active uplink port
 *
 *    Input(s)           : UfdGroupId - Group Id whose member ports has to 
 *                           be searched for.
 *                         u4IfIndex - Index of CFA port, which is 
 *                           a member of given UFD group.
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_TRUE if retrieval of an active port, belonging
 *                         to given group, other than given port , is found.
 *                         otherwise CFA_FALSE.
 *
 *****************************************************************************/

INT4
CfaUfdCheckAnyActiveUplinkInGroup (UINT4 u4IfIndex, UINT4 u4UfdGroupId)
{
    UINT4               u4PortIndex;
    tCfaIfInfo         *pCfaIfInfo = NULL;

    for (u4PortIndex = 1; u4PortIndex <=
         (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF); u4PortIndex++)
    {
        pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4PortIndex);
        if (pCfaIfInfo == NULL)
        {
            continue;
        }

        if (pCfaIfInfo->u4UfdGroupId == u4UfdGroupId &&
            pCfaIfInfo->u1UfdOperStatus == CFA_IF_UP &&
            pCfaIfInfo->u1PortRole == CFA_PORT_ROLE_UPLINK &&
            pCfaIfInfo->u4CfaIfIndex != u4IfIndex)
        {
            /* One link, other than given link, which is in Up state,
             * belonging to given group id is found. So return TRUE */
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                      "CfaUfdCheckAnyOtherActiveUplinkInGroup -Active port is %d\n",
                      u4PortIndex);
            return CFA_TRUE;
        }
    }

    /* No active link except the given (u4IfIndex) link 
     * is present with given UFD group Id*/

    return CFA_FALSE;
}

/*****************************************************************************
 *
 *    Function Name      : CfaIfUfdGroupInfoDbGet
 *
 *    Description        : This function gets a row from the UfdGroupInfoDB.
 *
 *    Input(s)           : UfdGroupId.
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if retrieval of the row succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
tUfdGroupInfo      *
CfaIfUfdGroupInfoDbGet (UINT4 u4UfdGroupId)
{
    tUfdGroupInfo       UfdGroupInfo;
    tUfdGroupInfo      *pUfdGroupInfoEntry = NULL;
    MEMSET (&UfdGroupInfo, 0, sizeof (tUfdGroupInfo));

    UfdGroupInfo.u4UfdGroupId = (UINT4) u4UfdGroupId;
    pUfdGroupInfoEntry = (tUfdGroupInfo *) RBTreeGet (gUfdGroupInfoTable,
                                                      (tCfaIfDbElem *) &
                                                      UfdGroupInfo);

    return pUfdGroupInfoEntry;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfUfdOperStatus                                */
/*                                                                           */
/* Description        : This function sets ufd oper status of an interface   */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu4IfUfdOperStatus: Pointer to ufd oper status       */
/*                      of the interface.                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/

INT4
CfaSetIfUfdOperStatus (UINT4 u4IfIndex, UINT4 u4IfUfdOperStatus)
{
    if ((u4IfIndex > (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF))
        || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        CFA_CDB_IF_UFD_OPER (u4IfIndex) = (UINT1) u4IfUfdOperStatus;
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfUfdOperStatus                                */
/*                                                                           */
/* Description        : This function gets ufd oper status of an interface   */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu4IfUfdOperStatus: Pointer to ufd oper status       */
/*                      of the interface.                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/

INT4
CfaGetIfUfdOperStatus (UINT4 u4IfIndex, UINT1 *pu1IfUfdOperStatus)
{
    if ((u4IfIndex > (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF))
        || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu1IfUfdOperStatus = CFA_CDB_IF_UFD_OPER (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfPortRole                                     */
/*                                                                           */
/* Description        : This function sets port role(uplink or downlink)     */
/*                      of the interface.                                    */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : u4IfPortRole:Pointer to port role of the interface.  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/

INT4
CfaSetIfPortRole (UINT4 u4IfIndex, UINT1 u1IfPortRole)
{
    if ((u4IfIndex > (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF))
        || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        CFA_CDB_IF_ROLE (u4IfIndex) = u1IfPortRole;
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfPortRole                                     */
/*                                                                           */
/* Description        : This function gets port role(uplink or downlink)     */
/*                      of the interface.                                    */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu1IfPortRole:Pointer to port role of the interface. */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/

INT4
CfaGetIfPortRole (UINT4 u4IfIndex, UINT1 *pu1IfPortRole)
{
    if ((u4IfIndex > (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF))
        || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu1IfPortRole = CFA_CDB_IF_ROLE (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}
/*****************************************************************************/
/* Function Name      : CfaSetIfDesigUplinkStatus                            */
/*                                                                           */
/* Description        : This function sets the interface whether the         */ 
/*                      designated uplink or not                             */
/*                      of the interface.                                    */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : u4IfDesigUplinkStatus :Pointer to Designated         */
/*                      Uplink Status  of the interface.                     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/

INT4
CfaSetIfDesigUplinkStatus (UINT4 u4IfIndex, UINT1 u1IfDesigUplinkStatus)
{
    UINT4 u4DesigIndex = 0;
    if ((u4IfIndex > (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF))
        || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }
    CFA_DS_LOCK ();
    if (CfaUfdCheckAnyDesiguplinkPresent (u4IfIndex, &u4DesigIndex) == CFA_TRUE)
    {
        CFA_CDB_IF_DESIG_UPLINK_STATUS (u4DesigIndex) = 0;    
    }
    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        CFA_CDB_IF_DESIG_UPLINK_STATUS (u4IfIndex) = u1IfDesigUplinkStatus;
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}
/*****************************************************************************/
/* Function Name      : CfaSetIfDesigUplinkStatus                            */
/*                                                                           */
/* Description        : This function sets the interface whether the         */
/*                      designated uplink or not                             */
/*                      of the interface.                                    */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : u4IfDesigUplinkStatus :Pointer to Designated         */
/*                      Uplink Status  of the interface.                     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfDesigUplinkStatus (UINT4 u4IfIndex, UINT1 *pu1IfDesigUplinkStatus)
{
    if ((u4IfIndex > (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF))
        || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu1IfDesigUplinkStatus = CFA_CDB_IF_DESIG_UPLINK_STATUS (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfUfdGroupId                                   */
/*                                                                           */
/* Description        : This function sets UFD group Id of the interface     */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu4IfUfdGroupId:Pointer to groupid of the interface. */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/

INT4
CfaSetIfUfdGroupId (UINT4 u4IfIndex, UINT4 u4IfUfdGroupId)
{
    if ((u4IfIndex > (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF))
        || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        CFA_CDB_IF_UFD_GROUP_ID (u4IfIndex) = u4IfUfdGroupId;
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfUfdGroupId                                   */
/*                                                                           */
/* Description        : This function gets UFD group Id of the interface     */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu4IfUfdGroupId:Pointer to groupid of the interface. */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/

INT4
CfaGetIfUfdGroupId (UINT4 u4IfIndex, UINT4 *pu4IfUfdGroupId)
{
    if ((u4IfIndex > (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF))
        || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu4IfUfdGroupId = CFA_CDB_IF_UFD_GROUP_ID (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfUfdDownlinkDisabledCount                     */
/*                                                                           */
/* Description        : This function gets UFD disabled downlink count.      */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu4IfUfdGroupId:Pointer to groupid of the interface. */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/

INT4
CfaGetIfUfdDownlinkDisabledCount (UINT4 u4IfIndex,
                                  UINT4 *pu4IfUfdDownlinkDisabledCount)
{
    if ((u4IfIndex > (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF))
        || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu4IfUfdDownlinkDisabledCount =
            CFA_CDB_IF_UFD_DOWNLINKDISABLED_COUNT (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfUfdDownlinkEnabledCount                      */
/*                                                                           */
/* Description        : This function gets UFD enabled downlink count.       */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu4IfUfdGroupId:Pointer to groupid of the interface. */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/

INT4
CfaGetIfUfdDownlinkEnabledCount (UINT4 u4IfIndex,
                                 UINT4 *pu4IfUfdDownlinkEnabledCount)
{
    if ((u4IfIndex > (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF))
        || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu4IfUfdDownlinkEnabledCount =
            CFA_CDB_IF_UFD_DOWNLINKENABLED_COUNT (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name      : CfaGetFirstUfdGroup 
 *
 *    Description        : This function returns the first group in the Ufd group table.
 *
 *    Input(s)           : None
 *
 *    Output(s)          : pu4UfdGroupId
 *
 *    Returns            : CFA_SUCCESS if RBTreeGetFirst succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGetFirstUfdGroup (UINT4 *pu4UfdGroupId)
{
    tUfdGroupInfo      *pUfdGroupInfoEntry = NULL;
    pUfdGroupInfoEntry = (tUfdGroupInfo *) RBTreeGetFirst (gUfdGroupInfoTable);
    if (pUfdGroupInfoEntry == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                 "CfaGetFirstUfdGroup -No entry present\n");
        return CFA_FAILURE;
    }
    *pu4UfdGroupId = pUfdGroupInfoEntry->u4UfdGroupId;
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaGetNextUfdGroup 
 *
 *    Description        : This function returns the next group in the Ufd group table.
 *
 *    Input(s)           : u4UfdGroupId
 *
 *    Output(s)          : pu4UfdGroupId
 *
 *    Returns            : CFA_SUCCESS if RBTreeGetNext succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGetNextUfdGroup (UINT4 u4UfdGroupId, UINT4 *pu4NextUfdGroupId)
{
    tUfdGroupInfo      *pUfdGroupInfoEntry = NULL;
    tUfdGroupInfo       UfdGroupInfo;
    MEMSET (&UfdGroupInfo, 0, sizeof (tUfdGroupInfo));

    UfdGroupInfo.u4UfdGroupId = u4UfdGroupId;

    pUfdGroupInfoEntry = (tUfdGroupInfo *) RBTreeGetNext (gUfdGroupInfoTable,
                                                          (tRBElem *) &
                                                          UfdGroupInfo, NULL);
    if (pUfdGroupInfoEntry == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaGetNextUfdGroup -No next entry present after %d\n",
                  u4UfdGroupId);
        return CFA_FAILURE;
    }
    *pu4NextUfdGroupId = pUfdGroupInfoEntry->u4UfdGroupId;
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaNotifyUfdOperStatusUpdate
 *
 *    Description        : This function is called from CFA when oper status
 *                         of a port is made up or down.
 *
 *    Input(s)           : u4UfdGroupId
 *
 *    Output(s)          : pu4UfdGroupId
 *
 *    Returns            : CFA_SUCCESS if changes succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaNotifyUfdOperStatusUpdate (UINT4 u4PortIndex, UINT1 u1OperStatus)
{
    UINT4               u4CurrentUfdGroupId = 0;
    UINT1               u1IfUfdOperStatus = 0;

    /* Check if port already present in group */
    if (CfaGetIfUfdGroupId (u4PortIndex, &u4CurrentUfdGroupId) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaNotifyUfdOperStatusUpdate- Given port %d is not existing\r\n",
                  u4PortIndex);
        return CFA_FAILURE;
    }
    if (u4CurrentUfdGroupId == 0)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                 "CfaNotifyUfdOperStatusUpdate - Given port has no ufd group dependency\r\n");
        if (CfaSetIfUfdOperStatus (u4PortIndex, u1OperStatus) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                      "CfaNotifyUfdOperStatusUpdate - Setting  UFD oper status "
                      "failed for port %d\n", u4PortIndex);
            return CFA_FAILURE;
        }
        return CFA_SUCCESS;
    }
    /* Get ufd operstatus of port */
    if (CfaGetIfUfdOperStatus (u4PortIndex, &u1IfUfdOperStatus) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                 "CfaNotifyUfdOperStatusUpdate - Failure in getting "
                 "UFD port oper status\n");
        return CFA_FAILURE;
    }
    if (u1IfUfdOperStatus == u1OperStatus)
    {
        return CFA_SUCCESS;
    }

    switch (u1OperStatus)
    {
        case CFA_IF_DOWN:

            if (CfaUfdChangeOperStatusToDown (u4PortIndex) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                          "CfaNotifyUfdOperStatusUpdate- OperStatusToDown failed for port %d\r\n",
                          u4PortIndex);
                return CFA_FAILURE;
            }
            break;
        case CFA_IF_UP:
            if (CfaUfdChangeOperStatusToUp (u4PortIndex) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                          "CfaNotifyUfdOperStatusUpdate- OperStatusToUp failed for port %d\r\n",
                          u4PortIndex);
                return CFA_FAILURE;
            }
            break;
        default:
            break;
    }
    /* Group status or Port UFD status is down. No need to do any changes */
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdChangeOperStatusToDown
 *
 *    Description        : This function is changing the oper status to down   
 *
 *    Input(s)           : u4PortIndex
 *                         u1OperStatus
 *
 *    Output(s)          : None
 *
 *    Returns            : CFA_SUCCESS if changes succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaUfdChangeOperStatusToDown (UINT4 u4PortIndex)
{
    tUfdGroupInfo       UfdGroupInfo;
    tUfdGroupInfo      *pUfdGroupInfoEntry = NULL;
    tCfaIfInfo         *pCfaIfInfo = NULL;
    UINT4               u4CurrentUfdGroupId = 0;
    UINT4               u4IfIndex = 0;
    UINT1               u1IfPortRole = 0;
    UINT1               u1IfUfdOperStatus = 0;
    gu4UfdPortFlag = 0;

    if (CfaGetIfUfdGroupId (u4PortIndex, &u4CurrentUfdGroupId) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdChangeOperStatusToDown - Given port %d is not existing\r\n",
                  u4PortIndex);
        return CFA_FAILURE;
    }
    MEMSET (&UfdGroupInfo, 0, sizeof (tUfdGroupInfo));

    UfdGroupInfo.u4UfdGroupId = u4CurrentUfdGroupId;
    pUfdGroupInfoEntry = (tUfdGroupInfo *)
        RBTreeGet (gUfdGroupInfoTable, (tCfaIfDbElem *) & UfdGroupInfo);
    if (pUfdGroupInfoEntry == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                 "CfaUfdChangeOperStatusToDown - The port belongs to group "
                 "which is not existing!!!\r\n");
        return CFA_FAILURE;
    }

    /* Get ufd operstatus of port */
    if (CfaGetIfUfdOperStatus (u4PortIndex, &u1IfUfdOperStatus) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                 "CfaUfdChangeOperStatusToDown - Failure in getting "
                 "UFD port oper status\n");
        return CFA_FAILURE;
    }

    /* Check port role */
    if (CfaGetIfPortRole (u4PortIndex, &u1IfPortRole) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                 "CfaUfdChangeOperStatusToDown - Failure in getting "
                 "port role \n");
        return CFA_FAILURE;
    }

    if (u1IfPortRole == CFA_PORT_ROLE_UPLINK)
    {
        if (pUfdGroupInfoEntry->u1UfdGroupStatus == CFA_UFD_GROUP_UP)
        {
            if (u1IfUfdOperStatus == CFA_IF_UP)
            {
                if (CfaUfdCheckAnyActiveUplinkInGroup (u4PortIndex,
                                                       u4CurrentUfdGroupId) ==
                    CFA_TRUE)
                {
                    /* Simply make uplink port down since there
                     * are other active uplinks in the group */
                    if (CfaSetIfUfdOperStatus (u4PortIndex, CFA_IF_DOWN)
                        != CFA_SUCCESS)
                    {
                        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                                  "CfaUfdChangeOperStatusToDown - Setting  UFD "
                                  "oper status failed for port %d at Group UP-If UP\n",
                                  u4PortIndex);
                        return CFA_FAILURE;
                    }
                }
                else
                {
                    /* No active Uplink port(s) is present
                     * Change status for downlink ports */
                    for (u4IfIndex = 1; u4IfIndex <=
                         (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF);
                         u4IfIndex++)
                    {
                        pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
                        if (pCfaIfInfo == NULL)
                        {
                            continue;
                        }
                        if (pCfaIfInfo->u4UfdGroupId ==
                            pUfdGroupInfoEntry->u4UfdGroupId
                            && pCfaIfInfo->u1UfdOperStatus == CFA_IF_UP
                            && pCfaIfInfo->u1PortRole == CFA_PORT_ROLE_DOWNLINK)
                        {
                            pCfaIfInfo->u1UfdOperStatus =
                                CFA_IF_UFD_ERR_DISABLED;
                            CfaSnmpifUfdSendTrap (u4IfIndex,
                                                  CFA_IF_UFD_ERR_DISABLED);
                           if (CFA_UFD_MODULE_STATUS () == CFA_UFD_ENABLED)
                           {
                               gu4UfdPortFlag = 1;
                               if (CfaIfmHandleInterfaceStatusChange
                                       (pCfaIfInfo->u4CfaIfIndex, CFA_IF_DOWN,
                                        CFA_IF_DOWN, CFA_FALSE) != CFA_SUCCESS)
                               {
                                   /* If any downlink ports failed 
                                    * to change the oper status, continue*/
                                   continue;
                               }
                               gu4UfdPortFlag = 0;
                           }
                            pCfaIfInfo->u4UfdDownlinkDisabledCount++;
                        }
                    }
                    /*Make the uplink port to be down */
                    if (CfaSetIfUfdOperStatus (u4PortIndex, CFA_IF_DOWN)
                        != CFA_SUCCESS)
                    {
                        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                                  "CfaUfdChangeOperStatusToDown - Setting  UFD oper status "
                                  "failed for port %d at Group UP IF UP and No other ports\n",
                                  u4PortIndex);
                        return CFA_FAILURE;
                    }
                    pUfdGroupInfoEntry->u1UfdGroupStatus = CFA_UFD_GROUP_DOWN;
                }
            }
        }
    }
    else if (u1IfPortRole == CFA_PORT_ROLE_DOWNLINK)
    {
        if (CfaSetIfUfdOperStatus (u4PortIndex, CFA_IF_DOWN) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                      "CfaUfdChangeOperStatusToDown - Setting  UFD oper status "
                      "failed for port %d at Downlink\n", u4PortIndex);
            return CFA_FAILURE;
        }
        CFA_CDB_IF_UFD_DOWNLINKDISABLED_COUNT (u4PortIndex) =
            CFA_CDB_IF_UFD_DOWNLINKDISABLED_COUNT (u4PortIndex) + 1;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdChangeOperStatusToUp
 *
 *    Description        : This function is changing the oper status to Up
 *
 *    Input(s)           : u4PortIndex
 *                         u1OperStatus
 *
 *    Output(s)          : None
 *
 *    Returns            : CFA_SUCCESS if changes succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaUfdChangeOperStatusToUp (UINT4 u4PortIndex)
{
    tUfdGroupInfo       UfdGroupInfo;
    tUfdGroupInfo      *pUfdGroupInfoEntry = NULL;
    tCfaIfInfo         *pCfaIfInfo = NULL;
    UINT4               u4CurrentUfdGroupId = 0;
    UINT4               u4IfIndex = 0;
    UINT1               u1IfPortRole = 0;
    UINT1               u1IfUfdOperStatus = 0;
    gu4UfdPortFlag = 0;

    if (CfaGetIfUfdGroupId (u4PortIndex, &u4CurrentUfdGroupId) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "CfaUfdChangeOperStatusToUp - Given port %d is not existing\r\n",
                  u4PortIndex);
        return CFA_FAILURE;
    }
    MEMSET (&UfdGroupInfo, 0, sizeof (tUfdGroupInfo));

    UfdGroupInfo.u4UfdGroupId = u4CurrentUfdGroupId;
    pUfdGroupInfoEntry = (tUfdGroupInfo *)
        RBTreeGet (gUfdGroupInfoTable, (tCfaIfDbElem *) & UfdGroupInfo);
    if (pUfdGroupInfoEntry == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                 "CfaUfdChangeOperStatusToUp - The port belongs to group which "
                 "is not existing!!!\r\n");
        return CFA_FAILURE;
    }

    /* Get ufd operstatus of port */
    if (CfaGetIfUfdOperStatus (u4PortIndex, &u1IfUfdOperStatus) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                 "CfaUfdChangeOperStatusToUp - Failure in getting UFD "
                 " port oper status\n");
        return CFA_FAILURE;
    }

    /* Check port role */
    if (CfaGetIfPortRole (u4PortIndex, &u1IfPortRole) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                 "CfaUfdChangeOperStatusToUp - Failure in getting "
                 " port role\n");
        return CFA_FAILURE;
    }

    if (u1IfPortRole == CFA_PORT_ROLE_UPLINK)
    {
        if (pUfdGroupInfoEntry->u1UfdGroupStatus == CFA_UFD_GROUP_UP)
        {
            if (CfaSetIfUfdOperStatus (u4PortIndex, CFA_IF_UP) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                          "CfaUfdChangeOperStatusToUp - Setting  UFD oper status "
                          "failed for port %d at group up\n", u4PortIndex);
                return CFA_FAILURE;
            }
            return CFA_SUCCESS;
        }
        else
        {
            for (u4IfIndex = 1; u4IfIndex <=
                 (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF);
                 u4IfIndex++)
            {
                pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
                if (pCfaIfInfo == NULL)
                {
                    continue;
                }

                if (pCfaIfInfo->u4UfdGroupId == pUfdGroupInfoEntry->u4UfdGroupId
                    && pCfaIfInfo->u1UfdOperStatus == CFA_IF_UFD_ERR_DISABLED
                    && pCfaIfInfo->u1PortRole == CFA_PORT_ROLE_DOWNLINK)
                {
                    pCfaIfInfo->u1UfdOperStatus = CFA_IF_UP;
                    CfaSnmpifUfdSendTrap (u4IfIndex, CFA_IF_UP);
                    if (CFA_UFD_MODULE_STATUS () == CFA_UFD_ENABLED)
                    {
                        gu4UfdPortFlag = 1;
                        if (CfaIfmHandleInterfaceStatusChange
                                (pCfaIfInfo->u4CfaIfIndex, CFA_IF_UP, CFA_IF_UP,
                                 CFA_FALSE) != CFA_SUCCESS)
                        {
                            continue;
                        }
                        gu4UfdPortFlag = 0;
                    }
                    pCfaIfInfo->u4UfdDownlinkEnabledCount++;
                }
            }
            if (CfaSetIfUfdOperStatus (u4PortIndex, CFA_IF_UP) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                          "CfaUfdChangeOperStatusToUp - Setting  UFD oper status "
                          "failed for port %d at group down\n", u4PortIndex);
                return CFA_FAILURE;
            }
            CfaSnmpifUfdSendTrap (u4PortIndex, CFA_IF_UP);
            pUfdGroupInfoEntry->u1UfdGroupStatus = CFA_UFD_GROUP_UP;
        }

    }
    else if (u1IfPortRole == CFA_PORT_ROLE_DOWNLINK)
    {
        /* For making a downlink port up, if it is a group which is down,
         * and the port is down, we make it error disabled */
        if (pUfdGroupInfoEntry->u1UfdGroupStatus == CFA_UFD_GROUP_DOWN)
        {
            if (u1IfUfdOperStatus == CFA_IF_DOWN)
            {
                if (CfaSetIfUfdOperStatus (u4PortIndex, CFA_IF_UFD_ERR_DISABLED)
                    != CFA_SUCCESS)
                {
                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                              "CfaUfdChangeOperStatusToUp - Setting  UFD oper status "
                              "failed for port %d at Downlink - group Down\n",
                              u4PortIndex);
                    return CFA_FAILURE;
                }
                CfaSnmpifUfdSendTrap (u4PortIndex, CFA_IF_UFD_ERR_DISABLED);
                if (CFA_UFD_MODULE_STATUS () == CFA_UFD_ENABLED)
                {
                    /* FIX for Bug 7329 - The admin status of the LTD interface is 
                     * not down when the LTM interface is down and the UFD group is
                     * down */
                    gu4UfdPortFlag = 1;
                    if (CfaIfmHandleInterfaceStatusChange
                            (u4PortIndex, CFA_IF_DOWN, CFA_IF_DOWN,
                             CFA_FALSE) != CFA_SUCCESS)
                    {
                        return CFA_FAILURE;
                    }
                    gu4UfdPortFlag = 0;
                }
            }
            CFA_CDB_IF_UFD_DOWNLINKDISABLED_COUNT (u4PortIndex) =
                CFA_CDB_IF_UFD_DOWNLINKDISABLED_COUNT (u4PortIndex) + 1;

        }
        else
        {
            if (CfaSetIfUfdOperStatus (u4PortIndex, CFA_IF_UP) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                          "CfaUfdChangeOperStatusToUp - Setting  UFD oper status "
                          "failed for port %d at Downlink - Group UP\n",
                          u4PortIndex);
                return CFA_FAILURE;
            }
            CfaSnmpifUfdSendTrap (u4PortIndex, CFA_IF_UP);
            CFA_CDB_IF_UFD_DOWNLINKENABLED_COUNT (u4PortIndex) =
                CFA_CDB_IF_UFD_DOWNLINKENABLED_COUNT (u4PortIndex) + 1;
        }
    }

    return CFA_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdSeperatePortlistOnPortRole
 *
 *    Description        : This function is a utility to split portlist
 *                         based on its port role.
 *
 *    Input(s)           : Portlist
 *
 *    Output(s)          : Uplink and downlink portlist
 *
 *    Returns            : CFA_SUCCESS if changes succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaUfdSeperatePortlistOnPortRole (tPortList IfPortList,
                                  tPortList * pUplinkPorts,
                                  tPortList * pDownlinkPorts)
{
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4PortRole = 0;

    for (u2ByteIndex = 0; u2ByteIndex < BRG_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = IfPortList[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < BITS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & INTERNAL_BIT8) != 0)
            {
                u4IfIndex =
                    (UINT4) ((u2ByteIndex * BITS_PER_BYTE) + u2BitIndex + 1);

                nmhGetIfMainPortRole ((INT4) u4IfIndex, &i4PortRole);
                if (i4PortRole == CFA_PORT_ROLE_DOWNLINK)
                {

                    OSIX_BITLIST_SET_BIT ((*pDownlinkPorts),
                                          (UINT2) u4IfIndex,
                                          sizeof (tPortList));

                }
                else
                {
                    OSIX_BITLIST_SET_BIT ((*pUplinkPorts),
                                          (UINT2) u4IfIndex,
                                          sizeof (tPortList));
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }

    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdCountPortlistSetBits
 *
 *    Description        : This function is a utility to count bits set in 
 *                         a port list.
 *
 *    Input(s)           : Portlist
 *
 *    Output(s)          : Bit count
 *
 *    Returns            : CFA_SUCCESS if changes succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

UINT4
CfaUfdCountPortlistSetBits (tPortList IfPortList)
{
    UINT4               u4Count = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;

    for (u2ByteIndex = 0; u2ByteIndex < CFA_UFD_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = IfPortList[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < BITS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & INTERNAL_BIT8) != 0)
            {
                u4Count++;
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    return u4Count;
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdValidateAddPortList
 *
 *    Description        : This function is a utility to validate the portlist
 *                         to be added to a ufd group.
 *
 *    Input(s)           : Portlist, GroupId
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if changes succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaUfdValidateAddPortList (tPortList IfPortList, UINT4 u4UfdGroupId)
{
    tUfdGroupInfo      *pUfdGroupInfo = NULL;

    tPortList          *pUplinkPorts = NULL;
    tPortList          *pDownlinkPorts = NULL;
    UINT4               u4CurrentUfdGroupId = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Index = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;
    UINT1               u1OriginalBit = 0;
    UINT1               u1BitToAdd = 0;
    UINT1               u1BrgPortType = 0;
    UINT1               u1BridgedIfaceStatus = 0;

    /* Allocate memory for ports */
    pUplinkPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pUplinkPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                 "\rCfaUfdValidateAddPortList - pUplinkPorts alloc failed\n");
        return CFA_FAILURE;
    }

    pDownlinkPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
    if (pDownlinkPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                 "\rCfaUfdValidateAddPortList - pDownlinkPorts alloc failed\n");
        FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
        return CFA_FAILURE;
    }

    MEMSET (pUplinkPorts, 0, sizeof (tPortList));
    MEMSET (pDownlinkPorts, 0, sizeof (tPortList));

    CfaUfdSeperatePortlistOnPortRole (IfPortList, pUplinkPorts, pDownlinkPorts);

    /* Filter exact port list to be added; Reset ports already added */
    pUfdGroupInfo = CfaIfUfdGroupInfoDbGet (u4UfdGroupId);
    if (pUfdGroupInfo == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "\rCfaUfdValidateAddPortList - Group  %d does not exist\n",
                  u4UfdGroupId);
        FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
        FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
        return CFA_FAILURE;
    }
    for (u4Index = 0; u4Index < BRG_PORT_LIST_SIZE; u4Index++)
    {
        u1BitToAdd = (*pUplinkPorts)[u4Index];
        u1OriginalBit = pUfdGroupInfo->UfdGroupUplinkPortList[u4Index];
        (*pUplinkPorts)[u4Index] = (UINT1) ((~u1OriginalBit) & (u1BitToAdd));
        /* pUplinkPorts has ports that are not in original list 
         * and there in 'to be added' list */

        u1BitToAdd = (*pDownlinkPorts)[u4Index];
        u1OriginalBit = pUfdGroupInfo->UfdGroupDownlinkPortList[u4Index];
        (*pDownlinkPorts)[u4Index] = (UINT1) ((~u1OriginalBit) & (u1BitToAdd));
    }

    if (((CfaUfdCountPortlistSetBits (*pUplinkPorts) +
          pUfdGroupInfo->u4UfdUplinkCount) > CFA_UFD_GROUP_MAX_UPLINK)
        ||
        ((CfaUfdCountPortlistSetBits (*pDownlinkPorts) +
          pUfdGroupInfo->u4UfdDownlinkCount) > CFA_UFD_GROUP_MAX_DOWNLINK))
    {
        /* Count exceeds max up/downlink ports */
        CLI_SET_ERR(CLI_CFA_UFD_GROUP_MAX_LIMIT_REACHED_ERR);
        FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
        FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
        return CFA_FAILURE;
    }

    /* In case of adding uplink ports,
     *there should be atleast one downlink port */
    if ((pUfdGroupInfo->u4UfdDownlinkCount == 0) &&   
         /* no downlink port in group */
        (CfaUfdCountPortlistSetBits (*pDownlinkPorts) == 0) &&    
        /* no downlink port to be added */
        (CfaUfdCountPortlistSetBits (*pUplinkPorts) > 0))   
         /* But uplink port is there to be added */
    {
        CLI_SET_ERR(CLI_CFA_UFD_GROUP_ZERO_DOWNLINK_ERR);
        FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
        FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
        return CFA_FAILURE;
    }

    /* Check if this port is member of anyother port */
    for (u2ByteIndex = 0; u2ByteIndex < BRG_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = ((*pDownlinkPorts)[u2ByteIndex] |
                      (*pUplinkPorts)[u2ByteIndex]);
        for (u2BitIndex = 0; ((u2BitIndex < BITS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & INTERNAL_BIT8) != 0)
            {
                u4IfIndex =
                    (UINT4) ((u2ByteIndex * BITS_PER_BYTE) + u2BitIndex + 1);
#ifdef LA_WANTED
                if (LaIsPortInPortChannel (u4IfIndex) == CFA_SUCCESS)
                {
                    /* Port channel interface ; Cannot be added */
                    CLI_SET_ERR(CLI_CFA_UFD_GROUP_PORTCHANNEL_PORT_ERR);
                    FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
                    FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
                    return CFA_FAILURE;
                }
#endif

                CfaGetIfBrgPortType (u4IfIndex, &u1BrgPortType);

                if(u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
                {
                    /* S-Channel Interface cannot be added to UFD Group */
                    CLI_SET_ERR (CLI_CFA_SBP_ERR);
                    FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
                    FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
                    return CFA_FAILURE;
                }

                if (CfaGetIfUfdGroupId (u4IfIndex, &u4CurrentUfdGroupId)
                    == CFA_FAILURE)
                {
                    /* Invalid interface ; Cannot be added */
                    CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                             "\rInvalid interface cannot be added to"
                             "UFD group\n");
                    FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
                    FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
                    return CFA_FAILURE;
                }
                if (u4CurrentUfdGroupId != 0)
                {
                    /* already a member of another ufd group */
                    CLI_SET_ERR(CLI_CFA_UFD_PORT_ANOTHER_GROUP_MEMBER);
                    CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                             "\rAlready member of another"
                             "UFD group;cannot add\r");
                    FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
                    FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
                    return CFA_FAILURE;
                }
              
                CfaGetIfBridgedIfaceStatus ((UINT4)u4IfIndex,&u1BridgedIfaceStatus);
                if (u1BridgedIfaceStatus == CFA_DISABLED)
                {
                    /* Router port cannot be added to UFD group */ 
                    CLI_SET_ERR (CLI_CFA_UFD_ROUTER_PORT_ERR); 
                    CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                             "\rPort is an router port;"
                             "Cannot be added to UFD group\n");
                    FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
                    FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
                    return CFA_FAILURE;    
                }

                /* Check if port is an OOB port */
                if ((CFA_MGMT_PORT == TRUE) &&
                    (u4IfIndex == CFA_OOB_MGMT_IFINDEX))
                {
                    CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                             "\rPort is an OOB port;"
                             "Cannot add the downlink port\n");

                    FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
                    FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
                    return CFA_FAILURE;
                }

            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);

        }
    }

    FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
    FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdValidateRemovePortList
 *
 *    Description        : This function is a utility to validate the portlist
 *                         to be deleted from a ufd group.
 *
 *    Input(s)           : Portlist, GroupId
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if changes succeeds,
 *                otherwise CFA_FAILURE.
 *
*****************************************************************************/
INT4
CfaUfdValidateRemovePortList (tPortList IfPortList, UINT4 u4UfdGroupId)
{
    tUfdGroupInfo      *pUfdGroupInfo = NULL;

    tPortList          *pUplinkPorts = NULL;
    tPortList          *pDownlinkPorts = NULL;
    /* NOTE: The calling function should set bits (in IfPortList) 
     *  for ports that are to be deleted */

    /* Allocate memory for ports */
    pUplinkPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pUplinkPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                 "\rCfaUfdValidateRemovePortList- pUplinkPorts alloc failed\n");
        return CFA_FAILURE;
    }

    pDownlinkPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
    if (pDownlinkPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                 "\rCfaUfdValidateRemovePortList- pDownlinkPorts alloc failed\n");
        FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
        return CFA_FAILURE;
    }

    MEMSET (pUplinkPorts, 0, sizeof (tPortList));
    MEMSET (pDownlinkPorts, 0, sizeof (tPortList));
    CfaUfdSeperatePortlistOnPortRole (IfPortList, pUplinkPorts, pDownlinkPorts);

    /* Filter exact port list to be deleted; 
     * Reset ports that are not in group
     * i.e. which belongs another group or no group */
    pUfdGroupInfo = CfaIfUfdGroupInfoDbGet (u4UfdGroupId);
    if (pUfdGroupInfo == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                  "\rCfaUfdValidateRemovePortList - Group  %d does not exist\n",
                  u4UfdGroupId);
        FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
        FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
        return CFA_FAILURE;
    }
    /* In case of deleting downlink ports,
     * if there are uplink ports, consider not 
     * to delete to the last downlink port */
    if ((CfaUfdCountPortlistSetBits(*pDownlinkPorts) == 0) &&
        (pUfdGroupInfo->u4UfdUplinkCount > 0))
    {
        /* We are trying to delete all downlink ports */
        CLI_SET_ERR(CLI_CFA_UFD_LAST_DOWNLINK_REM_ERR);
        FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
        FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
        return CFA_FAILURE;
    }

    FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
    FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
    return CFA_SUCCESS;
}
/*****************************************************************************
 *
 *    Function Name      : CfaUfdCheckForAlpha
 *
 *    Description        : This function checks if given string 
 *                         has no special characters.
 *
 *    Input(s)           : Ufd Group Name.
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if changes succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaUfdCheckForAlpha (UINT1 *pu1UfdGroupName)
{
    UINT2               u2Temp = 0;
    UINT2               u2Len = 0;

    u2Len = (UINT2) (STRLEN (pu1UfdGroupName));

    for (u2Temp = 0; u2Temp < u2Len; u2Temp++)
    {
        if (((*(pu1UfdGroupName + u2Temp) >= CFA_UFD_CALPHA_ASCII_START)
             && (*(pu1UfdGroupName + u2Temp) <= CFA_UFD_CALPHA_ASCII_END))
            || ((*(pu1UfdGroupName + u2Temp) >= CFA_UFD_SALPHA_ASCII_START)
                && (*(pu1UfdGroupName + u2Temp) <= CFA_UFD_SALPHA_ASCII_END))
            || ((*(pu1UfdGroupName + u2Temp) >= CFA_UFD_NUMERIC_ASCII_START)
                && (*(pu1UfdGroupName + u2Temp) <= CFA_UFD_NUMERIC_ASCII_END)))
        {
            continue;
        }
        else
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                     "CfaUfdCheckForAlphaNumeric -Only characters are accepted!\r\n");
            return CFA_FAILURE;
        }
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdCheckGroupNameUnique
 *
 *    Description        : This function checks if given ufd group name
 *                         is unique or not.
 *
 *    Input(s)           : Ufd Group Name.
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if changes succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaUfdCheckGroupNameUnique (UINT1 *pu1UfdGroupName, UINT4 u4UfdGroupId)
{
    tUfdGroupInfo      *pUfdGroupInfo = NULL;
    pUfdGroupInfo = RBTreeGetFirst (gUfdGroupInfoTable);

    while (pUfdGroupInfo != NULL)
    {

        if ((pUfdGroupInfo->u4UfdGroupId != u4UfdGroupId) &&
            (MEMCMP (pUfdGroupInfo->au1UfdGroupName,
                    pu1UfdGroupName, CFA_UFD_GROUP_MAX_NAME_LEN) == 0))
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                     "CfaUfdCheckGroupNameUnique -Ufd Group Name should be "
                     " unique!\r\n");
            return CFA_FAILURE;
        }
        pUfdGroupInfo = RBTreeGetNext (gUfdGroupInfoTable,
                                       (tRBElem *) pUfdGroupInfo, NULL);
    }
    return CFA_SUCCESS;
}


/*****************************************************************************
 *
 *    Function Name      : CfaUfdCheckAnyDesiguplinkPresent
 *
 *    Description        : This function check whether any port is already
 *                         configured as designated uplink port
 *
 *    Input(s)           :   u4IfIndex - Index of CFA port, which is
 *                           a member of given UFD group.
 *                           u4DesigIndex - Index of the already configured
 *                           designated port
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_TRUE if retrieval of an desig uplink port, 
 *                         other than given port , is found.
 *                         otherwise CFA_FALSE.
 *
 *****************************************************************************/


INT4
CfaUfdCheckAnyDesiguplinkPresent (UINT4 u4IfIndex, UINT4 *pu4DesigIfIndex)
{
    UINT4               u4PortIndex = 0;
    tCfaIfInfo         *pCfaIfInfo = NULL;

    *pu4DesigIfIndex = 0;

    for (u4PortIndex = 1; u4PortIndex <=
         SYS_DEF_MAX_PHYSICAL_INTERFACES; u4PortIndex++)
    {
        pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4PortIndex);
        if (pCfaIfInfo == NULL)
        {
            continue;
        }

        if (pCfaIfInfo->u1DesigUplinkStatus == CFA_ENABLED &&
            pCfaIfInfo->u1PortRole == CFA_PORT_ROLE_UPLINK &&
            pCfaIfInfo->u4CfaIfIndex != u4IfIndex)
        {

            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                      "CfaUfdCheckAnyDesiguplinkPresent  -Active port is %d\n",
                      u4PortIndex);
            *pu4DesigIfIndex = pCfaIfInfo->u4CfaIfIndex;
            return CFA_TRUE;
        }
    }
    /* No desig uplink port is present */

    return CFA_FALSE;
}
/*****************************************************************************
 *
 *    Function Name      : CfaShStart
 *
 *    Description        : This function Starts the Split Horizon on the system.
 *                         
 *
 *    Input(s)           : None.
 *
 *    Output(s)          : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/

VOID
CfaShStart (VOID)
{
    CFA_SH_SYSTEM_CONTROL = CFA_SH_START;
    return;
}

/*****************************************************************************
 *
 *    Function Name      : CfaShShutdown
 *
 *    Description        : This function Shuts down the Split Horizon on the system.
 *
 *
 *    Input(s)           : None.
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if changes succeeds,
 *                otherwise CFA_FAILURE.                                                                                       *
 *****************************************************************************/

INT4
CfaShShutdown (VOID)
{
    CFA_SH_SYSTEM_CONTROL = CFA_SH_SHUTDOWN;
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaShEnable
 *
 *    Description        : This function enables the split-horizon in the system
 *
 *
 *    Input(s)           : i4ShModuleStatus - SplitHorizon Module status
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if changes succeeds,
 *                otherwise CFA_FAILURE.                                                                                       *
 *****************************************************************************/

INT4 CfaShEnable(INT4 i4ShModuleStatus)
{

#ifdef NPAPI_WANTED
    tIssHwUpdtPortIsolation IssHwPortIsolationEntry;
    MEMSET (&IssHwPortIsolationEntry,0,sizeof(IssHwPortIsolationEntry));
#endif

    if (CFA_SH_MODULE_STATUS == CFA_SH_ENABLED)
    {
        return CFA_SUCCESS;
    }
    if (CfaConfigureShRules(i4ShModuleStatus) == CFA_SUCCESS)
    {
        CFA_SH_MODULE_STATUS  = CFA_SH_ENABLED;
    }
    else
    {
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaShDisable
 *
 *    Description        : This function disbles the split-horizon in the system
 *
 *
 *    Input(s)           : i4ShModuleStatus - SplitHorizon Module status
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if changes succeeds,
 *                otherwise CFA_FAILURE.                                                                                       *
 *****************************************************************************/


INT4 CfaShDisable(INT4 i4shModuleStatus)
{
    if (CFA_SH_MODULE_STATUS == CFA_SH_DISABLED)
    {
        return CFA_SUCCESS;
    }
    if (CFA_SH_SYSTEM_CONTROL != CFA_SH_START ||
            CFA_SH_MODULE_STATUS != CFA_SH_ENABLED)
    {
        return CFA_SUCCESS;
    }


    if (CfaConfigureShRules( i4shModuleStatus) == CFA_SUCCESS)
    {
        CFA_SH_MODULE_STATUS = CFA_SH_DISABLED;
    }
    else
    {
        return CFA_FAILURE;

    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : CfaConfigureShRules
 *
 *    Description        : This function configures Split-horizon rules in 
                           the system.
 *
 *
 *    Input(s)           : i4Action - Enable/Disable Split-Horizon
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if changes succeeds,
 *                otherwise CFA_FAILURE.                                                                                       *
 *****************************************************************************/


INT4 CfaConfigureShRules (INT4 i4Action)
{
    UINT4               u4IfIndex = 0 , u4Index = 0;
    UINT4               u1IndexUL = 0;
    UINT4               u4Scan = 0;
    UINT4               u4VlanId = 0;
    UINT4               au4UplinkPorts[CFA_UFD_GROUP_MAX_UPLINK];
    UINT4               au4DownlinkPorts[CFA_UFD_GROUP_MAX_DOWNLINK];
    UINT4               au4OtherDownlinkPorts[CFA_UFD_GROUP_MAX_DOWNLINK];
    UINT1               u1PortRole = 0;
    UINT1               u1IndexDL =0;
    UINT1               u1Index =0;
    UINT1               u1OperStatus=0;
    UINT1               u1IfType =0;
    UINT1               u1BridgedIfaceStatus = CFA_DISABLED;
    UINT1               u1DesigUplinkStatus = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4DesignatedUplinkPort=0;
    tIssUpdtPortIsolation IssUpdtPortIsolation;
#ifdef NPAPI_WANTED
    UINT1               u1FlowIndex =0;
    tIssHwUpdtPortIsolation IssHwPortIsolationEntry;
    MEMSET (&IssHwPortIsolationEntry,0,sizeof(IssHwPortIsolationEntry));
#endif


    MEMSET (&au4UplinkPorts , 0 , CFA_UFD_GROUP_MAX_UPLINK);
    MEMSET (&au4DownlinkPorts , 0 , CFA_UFD_GROUP_MAX_DOWNLINK);
    MEMSET (&au4OtherDownlinkPorts , 0 , CFA_UFD_GROUP_MAX_DOWNLINK);
    MEMSET (&IssUpdtPortIsolation, 0, sizeof (tIssUpdtPortIsolation));

    CFA_DBG (CFA_TRC_ALL, CFA_UFD, "Segregating ports based on their Port Roles\n");
    for ( u4IfIndex = 1; u4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES; u4IfIndex++)
    {
        if (((CfaGetIfType (u4IfIndex, &u1IfType)) ||
        (CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus)) ||
        (CfaGetIfOperStatus (u4IfIndex, &u1OperStatus))||
        (CfaGetIfPortRole (u4IfIndex,&u1PortRole))||
        (CfaGetIfDesigUplinkStatus (u4IfIndex , &u1DesigUplinkStatus)))!= CFA_SUCCESS)
        {
            /* Fix for Coverity Warning (CHECKED_RETURN)*/
            continue;
        }
        if ((u1IfType == CFA_ENET) &&
                (u1BridgedIfaceStatus !=  CFA_DISABLED))
        {
            if (i4Action == CFA_SH_ENABLED)
            {
                if ((u1OperStatus == CFA_IF_UP))
                {
                    if (u1PortRole == CFA_PORT_ROLE_DOWNLINK)
                    {
                        au4DownlinkPorts[u1IndexDL] = u4IfIndex;
                        u1IndexDL++;
                    }

                    else if (u1PortRole == CFA_PORT_ROLE_UPLINK)
                    {
                        au4UplinkPorts[u1IndexUL] = u4IfIndex;
                        u1IndexUL++;

                        if (u1DesigUplinkStatus == CFA_ENABLED)
                        {
                            i4DesignatedUplinkPort = (INT4)u4IfIndex;                        
                        }
                    }
                }

            }
            else
            {
                if(u4IfIndex != 0)
                {
                    if (u1PortRole == CFA_PORT_ROLE_DOWNLINK)
                    {
                        au4DownlinkPorts[u1IndexDL] = u4IfIndex;
                        u1IndexDL++;
                    }

                    else if (u1PortRole == CFA_PORT_ROLE_UPLINK)
                    {
                        au4UplinkPorts[u1IndexUL] = u4IfIndex;
                        u1IndexUL++;
                        if (u1DesigUplinkStatus == CFA_ENABLED)
                        {
                            i4DesignatedUplinkPort = (INT4)u4IfIndex;
                        }
                    }
                }

            }

        }
    }

/* If Designated Uplink is Set , Retrieve it 
 * else set the First uplink port as designated one */

    if (i4DesignatedUplinkPort == 0)
    {
        i4DesignatedUplinkPort = (INT4)au4UplinkPorts[0];
    }

    if (u1IndexUL == 0 || u1IndexDL == 0)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                "Split Horizon rules cannot be configured\n");
        return CFA_FAILURE;
    }

    CFA_DBG (CFA_TRC_ALL, CFA_UFD,
            "For every uplink port add list of downlink ports in Egress list \n");


    for (u4Index = 0 ; u4Index < u1IndexUL;u4Index++)
    {

        u4IfIndex = au4UplinkPorts[u4Index];
            for (u4Scan = 0; u4Scan < u1IndexDL; u4Scan++)
            {
                MEMSET (&IssUpdtPortIsolation , 0 ,sizeof (IssUpdtPortIsolation));
                IssUpdtPortIsolation.pu4EgressPorts = (UINT4 *) &(au4DownlinkPorts[u4Scan]);
                IssUpdtPortIsolation.u4IngressPort = u4IfIndex;
                IssUpdtPortIsolation.InVlanId = (tVlanId) u4VlanId;
                IssUpdtPortIsolation.u2NumEgressPorts = 1;
                if (i4Action == CFA_SH_ENABLED)
                {
                    IssUpdtPortIsolation.u1Action = ISS_PI_ADD;
                }
                else if (i4Action == CFA_SH_DISABLED)
                {
                    IssUpdtPortIsolation.u1Action = ISS_PI_DELETE;
                }

                if (CfaShModifyPortEntries (&IssUpdtPortIsolation , u4Scan) 
                        != CFA_SUCCESS)
                {
                    CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,                                     " Addition of Egress port List Failed \n");
                }
            }
    }

    CFA_DBG (CFA_TRC_ALL, CFA_UFD,
            "For every downlink port add list of Uplink ports in Egress list \n");

    for (u4Index = 0; u4Index < u1IndexDL; u4Index++)
    {

        u4IfIndex = au4DownlinkPorts[u4Index];
        u4Scan =0;
        u1Index=0;

            while (u4Scan < u1IndexDL)
            {
                MEMSET (&IssUpdtPortIsolation, 0, sizeof (IssUpdtPortIsolation));
                if (0 == u4Scan)
                {   
                    au4OtherDownlinkPorts[u4Scan] =  (UINT4)i4DesignatedUplinkPort;
                }

                if (u4Scan != 0)
                {

                    if (au4DownlinkPorts[u4Index] == au4DownlinkPorts[u1Index])
                    {
                        u1Index++;
                        continue;
                    }
                    else 
                    {

                        au4OtherDownlinkPorts[u4Scan] = au4DownlinkPorts[u1Index];
                        u1Index++;
                    }
                }

                IssUpdtPortIsolation.pu4EgressPorts =
                    (UINT4 *) &(au4OtherDownlinkPorts[u4Scan]);
                IssUpdtPortIsolation.u4IngressPort = u4IfIndex;
                IssUpdtPortIsolation.InVlanId = (tVlanId) u4VlanId;
                IssUpdtPortIsolation.u2NumEgressPorts = 1;
                if (i4Action == CFA_SH_ENABLED)
                {
                    
                    IssUpdtPortIsolation.u1Action = ISS_PI_ADD;

                }
                else if (i4Action == CFA_SH_DISABLED)
                {
                    IssUpdtPortIsolation.u1Action = ISS_PI_DELETE;
                }
                
                CfaShModifyPortEntries (&IssUpdtPortIsolation, u4Scan);

                u4Scan++;
            }
    }

#ifdef NPAPI_WANTED

    switch (i4Action)
    {

        case CFA_SH_ENABLED :

            /* I :  a) Add Downlink ports and Designated Uplink ports 
                       as Mcast listeners.
                    b) Downlink Ports as Flow Source */
                           
        
            MEMSET (&IssHwPortIsolationEntry, 0, sizeof (tIssHwUpdtPortIsolation));
            u1Index = 0;

            for ( u1FlowIndex = 0 ; u1FlowIndex < u1IndexDL; u1FlowIndex++)
            {

                IssHwPortIsolationEntry.au4EgressPorts[u1Index] = au4DownlinkPorts[u1FlowIndex];
                u1Index++;
                IssHwPortIsolationEntry.u2NumEgressPorts++;
            }

            IssHwPortIsolationEntry.u4IngressPort = i4DesignatedUplinkPort; 
            IssHwPortIsolationEntry.u1Action = ISS_PI_CONFIGURE;
            if (IssHwConfigPortIsolationEntry(&IssHwPortIsolationEntry) != FNP_SUCCESS)
            {
                return CFA_FAILURE;
            }


            /* Rule 1 : a) Add Downlink Ports as Mcast listeners ,
                        b) Add all the uplink ports as flow source*/
             

            MEMSET (&IssHwPortIsolationEntry, 0, sizeof (tIssHwUpdtPortIsolation));
            u1Index=0;

            for ( u1FlowIndex = 0 ; u1FlowIndex < u1IndexUL; u1FlowIndex++)
            {
                IssHwPortIsolationEntry.au4EgressPorts[u1Index] = au4UplinkPorts[u1FlowIndex];
                IssHwPortIsolationEntry.u2NumEgressPorts++;
                u1Index++;
            }
            IssHwPortIsolationEntry.u4IngressPort = 0; 
            IssHwPortIsolationEntry.u1Action = ISS_PI_CONFIGURE;
            if (IssHwConfigPortIsolationEntry (&IssHwPortIsolationEntry) != FNP_SUCCESS)
            { 
                return CFA_FAILURE;
            }

            break;


        case CFA_SH_DISABLED :
            
            MEMSET (&IssHwPortIsolationEntry, 0, sizeof (tIssHwUpdtPortIsolation));
            IssHwPortIsolationEntry.u1Action = ISS_PI_REMOVE; 
            
            if (IssHwConfigPortIsolationEntry(&IssHwPortIsolationEntry) != FNP_SUCCESS)
            {
                return CFA_FAILURE;
            }
            break;

        default :
            break;

    }

#endif


    return i4RetStatus;

}

/*****************************************************************************
 *
 *    Function Name      : CfaShModifyPortEntries
 *
 *    Description        : This function adds the egress port list for the
                           corresponding ingress port and in turn populates 
                           the port isolation table.
 *
 *
 *    Input(s)           : pIssUpdtPortIsolation - Port Isolation Data structure
                           u4Scan - Index                           
 *
 *    Output(s)          : None.
 *
 *    Returns            : CFA_SUCCESS if changes succeeds,
 *                otherwise CFA_FAILURE.                                                                                       *
 *****************************************************************************/


INT4
CfaShModifyPortEntries (tIssUpdtPortIsolation * pIssUpdtPortIsolation,
                        UINT4 u4Scan)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4VlanId = 0;
    UINT4               u4EgressPort = 0;
    if (NULL == pIssUpdtPortIsolation)
    {
        return CFA_FAILURE;
    }
    u4IfIndex = pIssUpdtPortIsolation->u4IngressPort;
    u4VlanId = pIssUpdtPortIsolation->InVlanId;
    u4EgressPort = pIssUpdtPortIsolation->pu4EgressPorts[0];

    if (pIssUpdtPortIsolation->u1Action == ISS_PI_ADD)
    {
        if (IssPIGetPortIsolationEntry ((UINT4)u4IfIndex,(tVlanId) u4VlanId,
                    (UINT4)u4EgressPort)  == ISS_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                    "Egress ports already present in the PI table\n");
            return CFA_FAILURE;
        }


        if (IssPIUpdtPortIsolationEntry (pIssUpdtPortIsolation) != ISS_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                    "Addition of Egress ports to the PI table Failed\n");
            return CFA_FAILURE; 

        }

    }

    else if ( pIssUpdtPortIsolation->u1Action == ISS_PI_DELETE)
    {

        if (IssPIUpdtPortIsolationEntry (pIssUpdtPortIsolation) != ISS_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                    "Deletion of Egress ports to the PI table Failed\n");
        }

    }


    UNUSED_PARAM (u4Scan);
    return CFA_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : CfaUfdCheckLastDownlinkInGroup                       */
/*                                                                           */
/* Description        : This function checks whether the given port is the   */
/*                      last downlink in the UFD group                       */
/*                                                                           */
/* Input(s)           : u4IfIndex                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
INT4
CfaUfdCheckLastDownlinkInGroup (UINT4 u4IfIndex)
{

    tUfdGroupInfo       UfdGroupInfo;
    tUfdGroupInfo      *pUfdGroupInfoEntry = NULL;
    UINT4               u4UfdGroupId = 0;
    UINT1               u1IfPortRole = 0;

    if (CfaGetIfPortRole(u4IfIndex, &u1IfPortRole) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                 "CfaUfdCheckLastDownlinkInGroup - Failure in getting portrole\n");
        return OSIX_FALSE;
    }
    if (CfaGetIfUfdGroupId (u4IfIndex, &u4UfdGroupId) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                "CfaUfdCheckLastDownlinkInGroup - Failure in getting UFD group Id\n");
        return OSIX_FALSE;
    }

    if (u4UfdGroupId == 0)
    {
        return OSIX_FALSE;
    }

    CFA_DS_LOCK ();

    MEMSET (&UfdGroupInfo, 0, sizeof (tUfdGroupInfo));

    UfdGroupInfo.u4UfdGroupId = u4UfdGroupId;
    pUfdGroupInfoEntry = (tUfdGroupInfo *)
        RBTreeGet (gUfdGroupInfoTable, (tCfaIfDbElem *) & UfdGroupInfo);


    if (pUfdGroupInfoEntry == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                "CfaUfdCheckLastDownlinkInGroup  - The port belongs to group which is not "
                "existing\n");
        CFA_DS_UNLOCK ();
        return OSIX_FALSE;
    }

    if (u1IfPortRole == CFA_PORT_ROLE_DOWNLINK)
    {
        if ((pUfdGroupInfoEntry->u4UfdDownlinkCount == 1) &&
                (pUfdGroupInfoEntry->u4UfdUplinkCount > 0))
        {
            CFA_DS_UNLOCK ();
            return OSIX_TRUE;
        }

    }
    CFA_DS_UNLOCK ();
    return OSIX_FALSE;
}

/*****************************************************************************/
/* Function Name      : CfaUfdPortChangeUpToErrorDisabled                    */
/*                                                                           */
/* Description        : This function is used to change the admin status     */
/*                      Up to 'UFD error disabled' for all the ports         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CfaUfdPortChangeUpToErrorDisabled()
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    UINT4               u4IfIndex = 0;


    for (u4IfIndex = 1; u4IfIndex <=
            (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF); u4IfIndex++)
    {
        pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
        if (pCfaIfInfo == NULL)
        {
            continue;
        }
        if (pCfaIfInfo->u4UfdGroupId != 0)
        {
            if (pCfaIfInfo->u1UfdOperStatus == CFA_IF_UFD_ERR_DISABLED)
            {
                CfaSnmpifUfdSendTrap (u4IfIndex, CFA_IF_UFD_ERR_DISABLED);
                if (CfaIfmHandleInterfaceStatusChange (u4IfIndex,
                            CFA_IF_DOWN, CFA_IF_DOWN,
                            CFA_FALSE) !=
                        CFA_SUCCESS)
                {
                    /* port status setting failed; moving to next port */

                    continue;
                }
                /* Increment port counter */
                pCfaIfInfo->u4UfdDownlinkDisabledCount++;
            }
        }
    }
}
/*****************************************************************************/
/* Function Name      : CfaUfdPortChangeErrorDisabledToUp                    */
/*                                                                           */
/* Description        : This function is used to change the admin status     */ 
/*                      'UFD error disabled' to UP for all the ports         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CfaUfdPortChangeErrorDisabledToUp()
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    UINT4               u4IfIndex = 0;

    for (u4IfIndex = 1; u4IfIndex <=
             (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF); u4IfIndex++)
    {
        pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
        if (pCfaIfInfo == NULL)
        {
            continue;
        }
        if (pCfaIfInfo->u4UfdGroupId != 0)
        {
            if (pCfaIfInfo->u1UfdOperStatus == CFA_IF_UFD_ERR_DISABLED)
            {
                CfaSnmpifUfdSendTrap (u4IfIndex, CFA_IF_UP);
                if (CfaIfmHandleInterfaceStatusChange (u4IfIndex,
                            CFA_IF_UP, CFA_IF_UP,
                            CFA_FALSE) !=
                        CFA_SUCCESS)
                {
                    /* port status setting failed; moving to next port */

                    continue;
                }
                /* Increment port counter */
                pCfaIfInfo->u4UfdDownlinkEnabledCount++;
            }
        }
    }
}

/*****************************************************************************
 *
 *    Function Name      : CfaUfdRemoveAllPortsInGroup
 *
 *    Description        : This function is used to delete all the uplink
 *                         and downlink ports which are present in the group
 *
 *    Input(s)           : u4UfdGroupId
 *                         
 *
 *    Output(s)          : None
 *
 *    Returns            : None
 *                         
 *
 *****************************************************************************/
VOID CfaUfdRemoveAllPortsInGroup(UINT4 u4UfdGroupId)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    tUfdGroupInfo       UfdGroupInfo;
    tUfdGroupInfo      *pUfdGroupInfoEntry = NULL;
    UINT4               u4IfIndex = 0;
    UINT1               u1IfPortRole = 0;

    MEMSET (&UfdGroupInfo, 0, sizeof (tUfdGroupInfo));

    UfdGroupInfo.u4UfdGroupId = u4UfdGroupId;
    pUfdGroupInfoEntry = (tUfdGroupInfo *)
        RBTreeGet (gUfdGroupInfoTable, (tCfaIfDbElem *) & UfdGroupInfo);


    if (pUfdGroupInfoEntry == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                "CfaUfdRemoveAllPortsInGroup  - The port belongs to group which is not "
                "existing\n");
        return;
    }

    for (u4IfIndex = 1; u4IfIndex <=
            (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF); u4IfIndex++)
    {
        pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
        if (pCfaIfInfo == NULL)
        {
            continue;
        }
        if (pCfaIfInfo->u4UfdGroupId == u4UfdGroupId)
        {
            if (CfaSetIfUfdGroupId (u4IfIndex, 0) != CFA_SUCCESS)
            {
                CFA_DBG2 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                        "CfaUfdRemoveUplinkPort - Setting  group Id %d is "
                        "failed for port %d at port oper up\n", u4UfdGroupId,
                        u4IfIndex);
                continue;
            }
            /* Check port role */
            if (CfaGetIfPortRole (u4IfIndex, &u1IfPortRole) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                        "CfaUfdRemoveAllPortsInGroup - Port role getting failed for port %d\n",
                        u4IfIndex);
                continue;
            }

            if (u1IfPortRole == CFA_PORT_ROLE_UPLINK)
            {
                /* Removing uplink port from port list */
                OSIX_BITLIST_RESET_BIT ((pUfdGroupInfoEntry->UfdGroupUplinkPortList),
                        (UINT2) u4IfIndex, sizeof (tPortList));
                pUfdGroupInfoEntry->u4UfdUplinkCount--;
            }
            else
            {
                /* Removing downlink port from port list */
                OSIX_BITLIST_RESET_BIT ((pUfdGroupInfoEntry->UfdGroupDownlinkPortList),
                        (UINT2) u4IfIndex, sizeof (tPortList));

                /* Reset ports enabled/disabled counts to zero */
                CFA_CDB_IF_UFD_DOWNLINKDISABLED_COUNT (u4IfIndex) = 0;
                CFA_CDB_IF_UFD_DOWNLINKENABLED_COUNT (u4IfIndex) = 0;
                pUfdGroupInfoEntry->u4UfdDownlinkCount--;
            }

        }
    }
    pUfdGroupInfoEntry->u1UfdGroupStatus = CFA_UFD_GROUP_DOWN;
    pUfdGroupInfoEntry->u4UfdGroupDesigUplinkPort = 0;
    return;
}
#endif
