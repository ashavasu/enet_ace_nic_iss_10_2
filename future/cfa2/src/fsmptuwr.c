# include  "lr.h"
# include "cfa.h"
# include  "fssnmp.h"
# include  "fsmptulw.h"
# include  "fsmptuwr.h"
# include  "fsmptudb.h"

INT4
GetNextIndexFsMITunlIfTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMITunlIfTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[3].pOctetStrValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             &(pNextMultiIndex->pIndex[5].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMITunlIfTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].pOctetStrValue,
             pNextMultiIndex->pIndex[3].pOctetStrValue,
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pFirstMultiIndex->pIndex[5].i4_SLongValue,
             &(pNextMultiIndex->pIndex[5].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterFSMPTU ()
{
    SNMPRegisterMibWithLock (&fsmptuOID, &fsmptuEntry,
                             CfaLock, CfaUnlock, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsmptuOID, (const UINT1 *) "fsmptunl");
}

VOID
UnRegisterFSMPTU ()
{
    SNMPUnRegisterMib (&fsmptuOID, &fsmptuEntry);
    SNMPDelSysorEntry (&fsmptuOID, (const UINT1 *) "fsmptunl");
}

INT4
FsMITunlIfHopLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMITunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMITunlIfHopLimit (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].pOctetStrValue,
                                      pMultiIndex->pIndex[3].pOctetStrValue,
                                      pMultiIndex->pIndex[4].i4_SLongValue,
                                      pMultiIndex->pIndex[5].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsMITunlIfSecurityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMITunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMITunlIfSecurity (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].pOctetStrValue,
                                      pMultiIndex->pIndex[3].pOctetStrValue,
                                      pMultiIndex->pIndex[4].i4_SLongValue,
                                      pMultiIndex->pIndex[5].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsMITunlIfTOSGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMITunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMITunlIfTOS (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].i4_SLongValue,
                                 pMultiIndex->pIndex[2].pOctetStrValue,
                                 pMultiIndex->pIndex[3].pOctetStrValue,
                                 pMultiIndex->pIndex[4].i4_SLongValue,
                                 pMultiIndex->pIndex[5].i4_SLongValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FsMITunlIfFlowLabelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMITunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMITunlIfFlowLabel (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       pMultiIndex->pIndex[3].pOctetStrValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       pMultiIndex->pIndex[5].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsMITunlIfMTUGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMITunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMITunlIfMTU (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].i4_SLongValue,
                                 pMultiIndex->pIndex[2].pOctetStrValue,
                                 pMultiIndex->pIndex[3].pOctetStrValue,
                                 pMultiIndex->pIndex[4].i4_SLongValue,
                                 pMultiIndex->pIndex[5].i4_SLongValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FsMITunlIfDirFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMITunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMITunlIfDirFlag (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiIndex->pIndex[2].pOctetStrValue,
                                     pMultiIndex->pIndex[3].pOctetStrValue,
                                     pMultiIndex->pIndex[4].i4_SLongValue,
                                     pMultiIndex->pIndex[5].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsMITunlIfDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMITunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMITunlIfDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       pMultiIndex->pIndex[3].pOctetStrValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       pMultiIndex->pIndex[5].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsMITunlIfEncaplmtGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMITunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMITunlIfEncaplmt (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].pOctetStrValue,
                                      pMultiIndex->pIndex[3].pOctetStrValue,
                                      pMultiIndex->pIndex[4].i4_SLongValue,
                                      pMultiIndex->pIndex[5].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsMITunlIfEncapOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMITunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMITunlIfEncapOption (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiIndex->pIndex[2].pOctetStrValue,
                                         pMultiIndex->pIndex[3].pOctetStrValue,
                                         pMultiIndex->pIndex[4].i4_SLongValue,
                                         pMultiIndex->pIndex[5].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsMITunlIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMITunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMITunlIfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiIndex->pIndex[2].pOctetStrValue,
                                   pMultiIndex->pIndex[3].pOctetStrValue,
                                   pMultiIndex->pIndex[4].i4_SLongValue,
                                   pMultiIndex->pIndex[5].i4_SLongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsMITunlIfAliasGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMITunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMITunlIfAlias (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiIndex->pIndex[2].pOctetStrValue,
                                   pMultiIndex->pIndex[3].pOctetStrValue,
                                   pMultiIndex->pIndex[4].i4_SLongValue,
                                   pMultiIndex->pIndex[5].i4_SLongValue,
                                   pMultiData->pOctetStrValue));

}

INT4
FsMITunlIfCksumFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMITunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMITunlIfCksumFlag (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       pMultiIndex->pIndex[3].pOctetStrValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       pMultiIndex->pIndex[5].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsMITunlIfPmtuFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMITunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMITunlIfPmtuFlag (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].pOctetStrValue,
                                      pMultiIndex->pIndex[3].pOctetStrValue,
                                      pMultiIndex->pIndex[4].i4_SLongValue,
                                      pMultiIndex->pIndex[5].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsMITunlIfStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMITunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMITunlIfStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].pOctetStrValue,
                                    pMultiIndex->pIndex[3].pOctetStrValue,
                                    pMultiIndex->pIndex[4].i4_SLongValue,
                                    pMultiIndex->pIndex[5].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FsMITunlIfHopLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMITunlIfHopLimit (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].pOctetStrValue,
                                      pMultiIndex->pIndex[3].pOctetStrValue,
                                      pMultiIndex->pIndex[4].i4_SLongValue,
                                      pMultiIndex->pIndex[5].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsMITunlIfTOSSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMITunlIfTOS (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].i4_SLongValue,
                                 pMultiIndex->pIndex[2].pOctetStrValue,
                                 pMultiIndex->pIndex[3].pOctetStrValue,
                                 pMultiIndex->pIndex[4].i4_SLongValue,
                                 pMultiIndex->pIndex[5].i4_SLongValue,
                                 pMultiData->i4_SLongValue));

}

INT4
FsMITunlIfFlowLabelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMITunlIfFlowLabel (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       pMultiIndex->pIndex[3].pOctetStrValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       pMultiIndex->pIndex[5].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsMITunlIfDirFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMITunlIfDirFlag (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiIndex->pIndex[2].pOctetStrValue,
                                     pMultiIndex->pIndex[3].pOctetStrValue,
                                     pMultiIndex->pIndex[4].i4_SLongValue,
                                     pMultiIndex->pIndex[5].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsMITunlIfDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMITunlIfDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       pMultiIndex->pIndex[3].pOctetStrValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       pMultiIndex->pIndex[5].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsMITunlIfEncaplmtSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMITunlIfEncaplmt (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].pOctetStrValue,
                                      pMultiIndex->pIndex[3].pOctetStrValue,
                                      pMultiIndex->pIndex[4].i4_SLongValue,
                                      pMultiIndex->pIndex[5].i4_SLongValue,
                                      pMultiData->u4_ULongValue));

}

INT4
FsMITunlIfEncapOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMITunlIfEncapOption (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiIndex->pIndex[2].pOctetStrValue,
                                         pMultiIndex->pIndex[3].pOctetStrValue,
                                         pMultiIndex->pIndex[4].i4_SLongValue,
                                         pMultiIndex->pIndex[5].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsMITunlIfAliasSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMITunlIfAlias (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiIndex->pIndex[2].pOctetStrValue,
                                   pMultiIndex->pIndex[3].pOctetStrValue,
                                   pMultiIndex->pIndex[4].i4_SLongValue,
                                   pMultiIndex->pIndex[5].i4_SLongValue,
                                   pMultiData->pOctetStrValue));

}

INT4
FsMITunlIfCksumFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMITunlIfCksumFlag (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       pMultiIndex->pIndex[3].pOctetStrValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       pMultiIndex->pIndex[5].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsMITunlIfPmtuFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMITunlIfPmtuFlag (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].pOctetStrValue,
                                      pMultiIndex->pIndex[3].pOctetStrValue,
                                      pMultiIndex->pIndex[4].i4_SLongValue,
                                      pMultiIndex->pIndex[5].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsMITunlIfStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMITunlIfStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].pOctetStrValue,
                                    pMultiIndex->pIndex[3].pOctetStrValue,
                                    pMultiIndex->pIndex[4].i4_SLongValue,
                                    pMultiIndex->pIndex[5].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FsMITunlIfHopLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsMITunlIfHopLimit (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiIndex->pIndex[2].pOctetStrValue,
                                         pMultiIndex->pIndex[3].pOctetStrValue,
                                         pMultiIndex->pIndex[4].i4_SLongValue,
                                         pMultiIndex->pIndex[5].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsMITunlIfTOSTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2FsMITunlIfTOS (pu4Error,
                                    pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].pOctetStrValue,
                                    pMultiIndex->pIndex[3].pOctetStrValue,
                                    pMultiIndex->pIndex[4].i4_SLongValue,
                                    pMultiIndex->pIndex[5].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FsMITunlIfFlowLabelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsMITunlIfFlowLabel (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].pOctetStrValue,
                                          pMultiIndex->pIndex[3].pOctetStrValue,
                                          pMultiIndex->pIndex[4].i4_SLongValue,
                                          pMultiIndex->pIndex[5].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsMITunlIfDirFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsMITunlIfDirFlag (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiIndex->pIndex[2].pOctetStrValue,
                                        pMultiIndex->pIndex[3].pOctetStrValue,
                                        pMultiIndex->pIndex[4].i4_SLongValue,
                                        pMultiIndex->pIndex[5].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsMITunlIfDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsMITunlIfDirection (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].pOctetStrValue,
                                          pMultiIndex->pIndex[3].pOctetStrValue,
                                          pMultiIndex->pIndex[4].i4_SLongValue,
                                          pMultiIndex->pIndex[5].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsMITunlIfEncaplmtTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsMITunlIfEncaplmt (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiIndex->pIndex[2].pOctetStrValue,
                                         pMultiIndex->pIndex[3].pOctetStrValue,
                                         pMultiIndex->pIndex[4].i4_SLongValue,
                                         pMultiIndex->pIndex[5].i4_SLongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsMITunlIfEncapOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsMITunlIfEncapOption (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[2].
                                            pOctetStrValue,
                                            pMultiIndex->pIndex[3].
                                            pOctetStrValue,
                                            pMultiIndex->pIndex[4].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[5].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsMITunlIfAliasTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsMITunlIfAlias (pu4Error,
                                      pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].pOctetStrValue,
                                      pMultiIndex->pIndex[3].pOctetStrValue,
                                      pMultiIndex->pIndex[4].i4_SLongValue,
                                      pMultiIndex->pIndex[5].i4_SLongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsMITunlIfCksumFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsMITunlIfCksumFlag (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].pOctetStrValue,
                                          pMultiIndex->pIndex[3].pOctetStrValue,
                                          pMultiIndex->pIndex[4].i4_SLongValue,
                                          pMultiIndex->pIndex[5].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsMITunlIfPmtuFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsMITunlIfPmtuFlag (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiIndex->pIndex[2].pOctetStrValue,
                                         pMultiIndex->pIndex[3].pOctetStrValue,
                                         pMultiIndex->pIndex[4].i4_SLongValue,
                                         pMultiIndex->pIndex[5].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsMITunlIfStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsMITunlIfStatus (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       pMultiIndex->pIndex[3].pOctetStrValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       pMultiIndex->pIndex[5].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsMITunlIfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMITunlIfTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
