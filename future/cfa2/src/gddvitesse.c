/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddvitesse.c,v 1.2 2016/07/23 11:41:26 siva Exp $
 *
 * Description:This file contains the routines for the     
 *             Generic Device Driver Module of the CFA.    
 *             These routines are called at various places     
 *             in the CFA modules for interfacing with the    
 *             device drivers in the system. These routines   
 *             have to be modified when moving onto different  
 *             drivers. 
 *               Drivers currently supported are                  
 *                  SOCK_PACKET for Ethernet                      
 *                  WANIC HDLC driver                             
 *                  SANGOMA wanpipe driver                        
 *                  ETINC driver                                  
 *                  ATMVC's support                               
 *                              
 *******************************************************************/
#include "cfainc.h"
#include "cfaport.h"
#include "gddipdx.h"
#include "issnvram.h"
#include "fsvlan.h"
#include "vtss_switch.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <linux/limits.h>
#include <dirent.h>

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "gddvitesse.h"

#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <asm/byteorder.h>
#include <endian.h>


static int vlan = ETH_VLAN;
/*****************************************************************************
 *
 *    Function Name        : CfaGddInit
 *
 *    Description          : This function performs the initialisation of
 *                           the Generic Device Driver Module of CFA. This
 * 	                     initialization routine should be called
 *           		     from the init of IFM after we have obtained
 *         		     the number of physical ports after parsing of
 *		             the Config file. This function initializes the FD list
 *           		     and ifIndex array of the polling table.
 *
 *    Input(s)             : None
 *
 *    Output(s)            : None
 *
 *    Returns              : CFA_SUCCESS if initialisation succeeds,
 *                           otherwise CFA_FAILURE
 *
 *****************************************************************************/
INT4
CfaGddInit (VOID)
{
    trace_init();

    if (OsixCreateSem ((const UINT1 *)"VTS1", 1, 0, &gVtssTxSemId) != OSIX_SUCCESS)
    {
        CFA_INIT_COMPLETE (OSIX_FAILURE);
	PRINTF ("ERROR[NP] - OsixCreateSem for gVtssTxSemId returned failure\n\r");
        return (CFA_FAILURE);
    }
    if (OsixCreateSem ((const UINT1 *)"VTS2", 1, 0, &gVtssApiSemId) != OSIX_SUCCESS)
    {
        CFA_INIT_COMPLETE (OSIX_FAILURE);
	PRINTF ("ERROR[NP] - OsixCreateSem for gVtssApiSemId returned failure\n\r");
        return (CFA_FAILURE);
    }
    if (OsixCreateSem ((const UINT1 *)"VTS3", 1, 0, &gVtssNpapiSemId) != OSIX_SUCCESS)
    {
        CFA_INIT_COMPLETE (OSIX_FAILURE);
	PRINTF ("ERROR[NP] - OsixCreateSem for gVtssNpapiSemId returned failure\n\r");
        return (CFA_FAILURE);
    }

#if defined(VTSS_USERMODE)

    if(CfaNpboardInit()!=CFA_SUCCESS)
    {
       PRINTF ("ERROR[NP] - CfaNpboardInit returned failure\n\r");
       return (CFA_FAILURE);
    }
#endif
    return (CFA_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : CfaGddGetLinkStatus                              */
/*                                                                          */
/*    Description        : Gets the Physical Link Status of the device      */
/*									    */
/*    Input(s)           : u4IfIndex - Interface index                      */
/*                                                                          */
/*    Output(s)          : None 				            */
/*                                                                          */
/*    Returns		 : Link status specific to u4IfIndex		    */
/*									    */
/****************************************************************************/
UINT1
CfaGddGetLinkStatus (UINT4 u4IfIndex)
{
    if (CfaCfaNpGetLinkStatus (u4IfIndex) != CFA_IF_UP)
    {
        return CFA_IF_DOWN;
    }
    return (CFA_IF_UP);
}

/*****************************************************************************
 *
 *    Function Name      : CfaGddEthWrite
 *
 *    Description        : Writes the data to the ethernet driver.
 *
 *    Input(s)           : pu1DataBuf - Pointer to the linear buffer
 *                         u4IfIndex - MIB-2 interface index
 *                         u4PktSize - Size of the buffer
 *
 *    Output(s)          : None
 *
 *    Returns            : CFA_SUCCESS if write succeeds,
 *                         otherwise CFA_FAILURE
 *
 *****************************************************************************/
INT4
CfaGddEthWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
 if (CfaNpPortWrite (pu1DataBuf, u4IfIndex ,u4PktSize ) != CFA_SUCCESS)
    {
	PRINTF ("ERROR[NP] - CfaNpPortWrite returned failure\n\r");
        return CFA_FAILURE;
    }
    return (CFA_SUCCESS);

}
/*****************************************************************************
 *    Function Name             : CfaGddEthWriteWithPri
 *    Description               : Writes the data to the ethernet driver.
 *    Input(s)                  : pu1DataBuf - Pointer to the linear buffer.
 *                                u4IfIndex  - MIB-2 interface index
 *                                u4PktSize  - Size of the buffer.
 *                                u1Priority - Traffic class on which Pkt to
 *					        be TXed
 *    Output(s)                 : None.
 *    Global Variables Referred : None
 *    Global Variables Modified : None.
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : CFA_SUCCESS if write succeeds,
 *                                otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGddEthWriteWithPri (UINT1 *pData, UINT4 u4IfIndex, UINT4 u4PktLen, 
		       UINT1 u1Priority)
{
    if(CfaGddEthWrite(pData,u4IfIndex,u4PktLen) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }
    UNUSED_PARAM (u1Priority);
    return CFA_SUCCESS;
}
/****************************************************************************
 * Function Name      : CfaGddProcessRecvPktEvent
 *
 * Description        :
 *
 *
 * Input(s)           : void
 *
 * Output(s)          : None
 *
 * Return Value(s)    : SNMP_SUCCESS / SNMP_FAILURE
 *************************************************************************/

PUBLIC INT4
CfaGddProcessRecvPktEvent (VOID)
{
   if (CfaNpProcessRecvPktEvent()!=CFA_SUCCESS)
   {
       PRINTF ("ERROR[NP] - CfaNpProcessRecvPktEvent returned failure\n\r");
       return (CFA_FAILURE);
   }

   else
   {
       return (CFA_SUCCESS);
   }
   
}


/****************************************************************************
 * Function Name      : CfaGddOsProcessRecvEvent
 *
 * Description        :
 *
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : CFA_SUCCESS / CFA_FAILURE
 *****************************************************************************/
INT4
CfaGddOsProcessRecvEvent (VOID)
{
   if(CfaGNpOsProcessRecvEvent()==CFA_FAILURE)
   {
      PRINTF ("ERROR[NP] - CfaGNpOsProcessRecvEvent returned failure\n\r");
      return CFA_FAILURE;
   } 
   else
   {
      return CFA_SUCCESS;
   }   

}

#ifdef MBSM_WANTED

/*****************************************************************************
 *    Function Name       : CfaMbsmGddInit
 *    Description         : This function performs the initialisation of
 *                          the  Device Driver Module of CFA. This routine
 *                          1. creates the Queue for reception of packets from
 *                             driver
 *                          2. creates the memory pool for the driver messages
 *                          3. initializes the driver to cfa mapping tables
 *    Input(s)            : pSlotInfo - structure pointer to Slot info structure
 *    Output(s)           : None
 *    Returns             : CFA_SUCCESS
 *****************************************************************************/
PUBLIC INT4
CfaMbsmGddInit (tMbsmSlotInfo * pSlotInfo)
{
    /* Update the Driver to cfa mapping tables for the given slot.
     */

    UNUSED_PARAM (pSlotInfo);
    return CFA_SUCCESS;
}

#endif /* MBSM_WANTED */



/*****************************************************************************
 *
 *    Function Name      : CfaGddShutdown
 *
 *    Description        : This function performs the shutdown of
 *                         the Generic Device Driver Module of CFA. This
 *                         routine frees the polling table structure.
 *
 *    Input(s)           : None
 *
 *    Output(s)          : None
 *
 *    Returns            : None
 *
 *****************************************************************************/
VOID
CfaGddShutdown (VOID)
{
   return;
}

/**************************************************************************
*   Function Name        : CfaGddSetGddType                               *
*									  *
*   Description          : This function sets the Device driver type      *
*                                                                         *
*   Input                : u4IfIndex - Interface index                    *
*                                                                         *
*   Output               : None                                           *
*                                                                         *
*   Returns              : CFA_SUCCESS                                    *
**************************************************************************/
INT4
CfaGddSetGddType (UINT4 u4IfIndex)
{
    UINT1               u1IfType;

    CfaGetIfType (u4IfIndex, &u1IfType);
    CFA_GDD_TYPE (u4IfIndex) = u1IfType;

    return (CFA_SUCCESS);
}

/**************************************************************************
*   Function Name        : CfaGddOsRemoveIfFromList                       *
*   Description          :                                                *
*                                                                         *
*   Input                : u4IfIndex - Interface index                    *
*                                                                         *
*   Output               : None                                           *
*                                                                         *
*   Returns              : None                                           *
**************************************************************************/
VOID
CfaGddOsRemoveIfFromList (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return;
}

/**************************************************************************
*   Function Name        : CfaGddOsAddIfToList                            *
*									  *
*   Description          :                                                *
*                                                                         *
*   Input                : u4IfIndex - Interface index                    *
*                                                                         *
*   Output               : None                                           *
*                                                                         *
*   Returns              : None                                           *
**************************************************************************/
VOID
CfaGddOsAddIfToList (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return;
}

/****************************************************************************
 * Function Name      : dump
 *
 * Description        :
 *
 *
 * Input(s)           : buf - Address of the buffer
 *                      len - Lengh of buffer
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ***************************************************************************/
static void __attribute__ ((unused))
dump(const unsigned char *buf, size_t len)
{
    int i = 0;
    int k = len;

    while (i < k) {
        int j = 0;
        printf("%04x:", i);
        while (j+i < k && j < 16) {
            printf(" %02x", buf[i+j]);
            j++;
        }
        putchar('\n');
        i += 16;
    }
}

/**************************************************************************
*   Function Name        : CfaGddProcessRecvInterruptEvent                *
*									  *
*   Description          :                                                *
*                                                                         *
*   Input                : None                                           *
*                                                                         *
*   Output               : None                                           *
*                                                                         *
*   Returns              : CFA_SUCCESS                                    *
**************************************************************************/
PUBLIC INT4
CfaGddProcessRecvInterruptEvent (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pBuf;
    UINT4               u4IfIndex;
    UINT4               u4PktSize;
    UINT1               u1IfType;

    while (OsixReceiveFromQ (SELF, (UINT1 *) "CFAMUX", OSIX_NO_WAIT, 0, &pBuf)
           == OSIX_SUCCESS)
    {
        CFA_LOCK ();

        u4IfIndex = pBuf->ModuleData.InterfaceId.u4IfIndex;

        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

        if (CfaGetIfType (u4IfIndex, &u1IfType) == CFA_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                     "In CfaMain Pkt recvd in interrupt mode failed to deliver to HL.\n");
        }
        else
        {
            /* Handover the Packet to Receive module */
            if ((CFA_GDD_HL_RX_FNPTR (u4IfIndex)) (pBuf, u4IfIndex, u4PktSize,
                                                   u1IfType, CFA_ENCAP_NONE)
                != CFA_SUCCESS)
            {
                /* release buffer which were not successfully sent to higher
                 * layer */
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
            }
            else
            {
                /* increment after successful handling of packet */
                CFA_IF_SET_IN_OCTETS (u4IfIndex, u4PktSize);
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "In CfaMain Pkt recvd in interrupt mode delivered to HL.\n");
            }
        }
        CFA_UNLOCK ();
    }
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name      : CfaGddConfigPort
 *
 *    Description        : Function is called for configuration of the
 *               	   Enet ports. No such support is available for
 *	                   Wanic at present. Only the Promiscuous mode
 *         	           and multicast addresses can be configured at
 *	                   present for Enet ports. This is called
 *            		   directly whenever the Manager configures the
 *              	   ifTable for Ethernet ports. It can also be
 *                         called indirectly by Bridge or RIP, etc.
 * 	                   through the CfaIfmEnetConfigMcastAddr API.
 *
 *    Input(s)           : u4IfIndex - Interface index
 *                         u1ConfigOption - Configuration option
 *                         pConfigParam - pointer to config parameters
 *
 *    Output(s)          : None
 *
 *    Returns            : CFA_SUCCESS
 *
 *****************************************************************************/
INT4
CfaGddConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption, VOID *pConfigParam)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1ConfigOption);
    UNUSED_PARAM (pConfigParam);
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name      : CfaGddGetOsHwAddr
 *
 *    Description        : Function is called for getting the hardware
 *                         address of the Enet ports. This function can
 *                         be called only after the opening of the ports
 *                         - the ports should be open but Admin/Oper
 *                         Status can be down.
 *
 *    Input(s)           : u4IfIndex - Interface index
 *
 *    Output(s)          : au1HwAddr - pointer to hardware address
 *
 *    Returns            : CFA_SUCCESS
 *
 *****************************************************************************/
INT4
CfaGddGetOsHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
    UINT1               au1TempAddr[CFA_ENET_ADDR_LEN] =
        { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05 };
    tNVRAM_DATA         NvRamData;
    UINT4               u4TempIfIndex = 0;
    static UINT1        au1Tempvar[CFA_ENET_ADDR_LEN] =
        { 0x00, 0x00, 0x00, 0x00, 0x00, 0x000 };
    UINT4               u4TempCount = 0;
    MEMSET (&NvRamData, 0, sizeof (tNVRAM_DATA));

    if (NvRamRead (&NvRamData) == FNP_SUCCESS)
    {
        MEMCPY (au1HwAddr, NvRamData.au1SwitchMac, CFA_ENET_ADDR_LEN);
    }
    else
    {
        MEMCPY (au1HwAddr, au1TempAddr, CFA_ENET_ADDR_LEN);
    }

    if (CfaIsL3IpVlanInterface (u4IfIndex) == CFA_SUCCESS)
    {
        return (CFA_SUCCESS);
    }

    u4TempIfIndex = u4IfIndex - 1;
    au1HwAddr[5] += u4TempIfIndex;
    for (u4TempCount = (CFA_ENET_ADDR_LEN - 1); u4TempCount > 1; u4TempCount--)
    {
        if ((au1HwAddr[u4TempCount] == 0x00) && u4TempIfIndex)
        {
            au1Tempvar[u4TempCount - 1]++;
        }
        au1HwAddr[u4TempCount - 1] =
            au1HwAddr[u4TempCount - 1] + au1Tempvar[u4TempCount - 1];
    }
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthOpen
 *
 *    Description          : This function opens the Ethernet port and stores the
 *                           descriptor in the interface table. Since the port is
 *                           opened already during the registration time, this
 *                           function is dummy.
 *
 *    Input(s)             : u4IfIndex - Interface Index
 *
 *    Output(s)            : None
 *
 *    Returns              : CFA_SUCCESS
 *
 *****************************************************************************/
INT4
CfaGddEthOpen (UINT4 u4IfIndex)
{

    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthClose
 *
 *    Description          : First disables the promiscuous mode of
 *                           the port, if enabled. The multicast address
 *                           list is lost when the port is closed.
 *                           We dont need to check if the call is a
 *                           success or not.
 *
 *    Input(s)             : u4IfIndex - Interface index
 *
 *    Output(s)            : None
 *
 *    Returns            : CFA_SUCCESS
 *
 *****************************************************************************/
INT4
CfaGddEthClose (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthRead
 *
 *    Description          : This function reads the data from the ethernet port.
 *
 *    Input(s)             : pu1DataBuf - Pointer to the linear buffer.
 *                           u4IfIndex - IfIndex of the interface
 *                           pu4PktSize - Pointer to the packet size
 *
 *    Output(s)            : pu1DataBuf, pu1PktSize
 *
 *    Returns              : CFA_SUCCESS
 *
 *****************************************************************************/
INT4
CfaGddEthRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4PktSize);
    return CFA_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : CfaGddSendIntrEvent                              */
/*                                                                          */
/*    Description        : Sends an event to CFA to indicate packet arrival */
/*                         at the driver                                    */
/*									    */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*									    */
/*    Returns		 : CFA_SUCESS					    */
/*                                                                          */
/****************************************************************************/
UINT4
CfaGddSendIntrEvent (VOID)
{
    return (CFA_SUCCESS);
}

#ifdef MBSM_WANTED
/*****************************************************************************
 *    Function Name       : CfaMbsmGddDeInit
 *    Description         : This function performs the shutdown of
 *                          the Generic Device Driver Module of CFA. This
 *                          shutdown routine should be called
 *                          before the protocols are informed about the shutdown.
 *    Input(s)            : pSlotInfo - structure pointer to Slot info structure
 *    Output(s)           : None
 *    Returns             : CFA_SUCCESS
 *****************************************************************************/
PUBLIC INT4
CfaMbsmGddDeInit (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *    Function Name       : CfaMbsmGddInit
 *    Description         : This function performs the initialisation of
 *                          the  Device Driver Module of CFA. This routine
 *                          1. creates the Queue for reception of packets from
 *                             driver
 *                          2. creates the memory pool for the driver messages
 *                          3. initializes the driver to cfa mapping tables
 *    Input(s)            : pSlotInfo - structure pointer to Slot info structure
 *    Output(s)           : None
 *    Returns             : CFA_SUCCESS
 *****************************************************************************/
PUBLIC INT4
CfaMbsmGddInit (tMbsmSlotInfo * pSlotInfo)
{
    /* Update the Driver to cfa mapping tables for the given slot.
     */

    UNUSED_PARAM (pSlotInfo);
    return CFA_SUCCESS;
}

#endif /* MBSM_WANTED */

