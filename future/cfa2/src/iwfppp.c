/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: iwfppp.c,v 1.22 2014/11/10 12:36:56 siva Exp $
 *
 *
 * Description:This file contains the routines for the    
 *             Inter-Working Functionality Module of the CFA.  
 *             These routines are mostly used in the data path 
 *             for encapsulation/de-encapsulation of packets   
 *             over the interfaces. This file contains         
 *             routines for the PPP interfaces.            
 *
 *******************************************************************/
#ifdef PPP_WANTED
#include "cfainc.h"
#include "ppp.h"
/**************************** GLOBAL DEFINITIONS *****************************/
/* ACCM default array */
UINT1               gCfaDefaultTxACCM[] = {
    0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};
UINT1               gCfaDefaultRxACCM[] = { 0xff, 0xff, 0xff, 0xff };

/*FCS Lookup Table for FCS size 16*/
UINT2               AsyncFcsTable16[256] = {
    0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
    0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
    0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
    0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
    0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
    0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
    0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
    0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
    0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
    0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
    0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
    0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
    0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
    0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
    0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
    0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
    0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
    0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
    0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
    0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
    0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
    0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
    0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
    0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
    0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
    0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
    0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
    0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
    0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
    0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
    0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
    0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
};

/*FCS Lookup Table for FCS size 32*/
UINT4               AsyncFcsTable32[256] = {
    0x00000000, 0x77073096, 0xee0e612c, 0x990951ba,
    0x076dc419, 0x706af48f, 0xe963a535, 0x9e6495a3,
    0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988,
    0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91,
    0x1db71064, 0x6ab020f2, 0xf3b97148, 0x84be41de,
    0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
    0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec,
    0x14015c4f, 0x63066cd9, 0xfa0f3d63, 0x8d080df5,
    0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
    0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b,
    0x35b5a8fa, 0x42b2986c, 0xdbbbc9d6, 0xacbcf940,
    0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
    0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116,
    0x21b4f4b5, 0x56b3c423, 0xcfba9599, 0xb8bda50f,
    0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
    0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d,
    0x76dc4190, 0x01db7106, 0x98d220bc, 0xefd5102a,
    0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
    0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818,
    0x7f6a0dbb, 0x086d3d2d, 0x91646c97, 0xe6635c01,
    0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e,
    0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457,
    0x65b0d9c6, 0x12b7e950, 0x8bbeb8ea, 0xfcb9887c,
    0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
    0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2,
    0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb,
    0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
    0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9,
    0x5005713c, 0x270241aa, 0xbe0b1010, 0xc90c2086,
    0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
    0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4,
    0x59b33d17, 0x2eb40d81, 0xb7bd5c3b, 0xc0ba6cad,
    0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,
    0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683,
    0xe3630b12, 0x94643b84, 0x0d6d6a3e, 0x7a6a5aa8,
    0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
    0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe,
    0xf762575d, 0x806567cb, 0x196c3671, 0x6e6b06e7,
    0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc,
    0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5,
    0xd6d6a3e8, 0xa1d1937e, 0x38d8c2c4, 0x4fdff252,
    0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
    0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60,
    0xdf60efc3, 0xa867df55, 0x316e8eef, 0x4669be79,
    0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
    0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f,
    0xc5ba3bbe, 0xb2bd0b28, 0x2bb45a92, 0x5cb36a04,
    0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
    0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a,
    0x9c0906a9, 0xeb0e363f, 0x72076785, 0x05005713,
    0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38,
    0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21,
    0x86d3d2d4, 0xf1d4e242, 0x68ddb3f8, 0x1fda836e,
    0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
    0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c,
    0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45,
    0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2,
    0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db,
    0xaed16a4a, 0xd9d65adc, 0x40df0b66, 0x37d83bf0,
    0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
    0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6,
    0xbad03605, 0xcdd70693, 0x54de5729, 0x23d967bf,
    0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
    0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
};
/*****************************************************************************
 *                                   
 *    Function Name             : CfaIwfPppHandleIncomingPkt
 *
 *    Description               : These function checks the Admin Status of the 
 *                                higher layer registered over this HDLC interface. 
 *                                If the Admin Status is not UP, it returns FAILURE. 
 *                                
 *                                If the PPP Bypass is enabled, the encapsulation/
 *                                decapsulation is done in CFA otherwise the packet 
 *                                is sent to PPP module. 
 *
 *    Input(s)                  : PBuf - Pointer to the CRU Buffer. 
 *                                U2IfIndex - The MIB-II ifIndex of port over which 
 *                                            the packet is to be transmitted.
 *                                U4PktSize - Size of the buffer to be transmitted.
 *                                U2Protocol - Protocol ID.
 *
 *    Output(s)                 : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating 
 *      System Error Handling   : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
#ifdef __STDC__
INT4
CfaIwfPppHandleIncomingPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                            UINT4 u4PktSize, UINT2 u2Protocol,
                            UINT1 u1EncapsType)
#else /*  __STDC__ */
INT4
CfaIwfPppHandleIncomingPkt (pBuf, u4IfIndex, u4PktSize, u2Protocol,
                            u1EncapsType)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     UINT4               u4IfIndex = 0;
     UINT4               u4PktSize = 0;
     UINT2               u2Protocol = 0;
     UINT1               u1EncapsType = 0;
#endif /*  __STDC__ */
{
    UINT4               u4PppIfIndex = 0;
#ifdef PPP_BYPASS_WANTED
    UINT2               u2TempHdr = 0;
    UINT1              *pu1TempHdrPtr = NULL;
#endif /* PPP_BYPASS_WANTED */

    UINT1               u1CurOperStatus = CFA_IF_DOWN;
    UINT1               u1IfType = CFA_NONE;

    CfaGetIfType (u4IfIndex, &u1IfType);
    if (u1IfType == CFA_MP)
    {
        /* If an external Link Layer Processor is used 
         *   - Buffer passed in contains the entire PPP packet reassembled
         *     from fragments.
         *   - Rx interface index passed in will be that of the MP interface
         *     directly instead of any lower layer physical interface index.
         */
        u4PppIfIndex = u4IfIndex;
    }
    else
    {
        u4PppIfIndex =
            CFA_IF_STACK_IFINDEX (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex));
    }

    if (u4PppIfIndex == 0)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                  "Exiting CfaIwfPppHandleIncomingPkt - "
                  "Invalid Interface index of PPP %d - FAIL\n", u4PppIfIndex);

        return CFA_FAILURE;        /* buffer released by GDD */
    }

    /* Temporarily put the check in the data path for triggering incoming
       calls over a demand circuit - to be replaced by a API/trigger from the
       lower driver or interface */
    CfaGetIfOperStatus (u4PppIfIndex, &u1CurOperStatus);

    if (((u1CurOperStatus == CFA_IF_DORM) ||
         (u1CurOperStatus == CFA_IF_DOWN)) &&
        (CFA_IF_ADMIN (u4PppIfIndex) == CFA_IF_UP))
    {

        if (PppUpdateInterfaceStatus (u4PppIfIndex, CFA_IF_UP, CFA_IF_UNK)
            != PPP_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                      "Exiting CfaIwfPppHandleIncomingPkt - "
                      "interface %d Oper update - FAIL\n", u4PppIfIndex);
            return CFA_FAILURE;    /* buffer released by GDD */
        }
    }
    /* end of temporary check in the data path */

#ifdef PPP_BYPASS_WANTED
    CFA_IF_SET_IN_OCTETS (u4PppIfIndex, u4PktSize);
    CFA_IF_SET_IN_BCAST (u4PppIfIndex);

    /* we first remove the lower physical layer header, if required. Also,
       verify the lower layer FCS and ACCM if applicable */

    switch (u2Protocol)
    {

        case CFA_PROT_ASYNC:
            /* for async phys layer - we have removed the ACCM and verified FCS, if 
               required in the GDD */

        case CFA_PROT_HDLC:

            pu1TempHdrPtr = (UINT1 *) CRU_BMC_Get_DataPointer (pBuf);

            u2TempHdr = *((UINT2 *) pu1TempHdrPtr);
            u2TempHdr = OSIX_NTOHS (u2TempHdr);
            if (!CFA_IS_HDLC_HDR (u2TempHdr))
            {
                /* there is no HDLC header in the packet */
                if (!CFA_PPP_IS_ACFC_RX (u4PppIfIndex))
                {
                    /* drop the packet */
                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                              "Exiting CfaIwfPppHandleIncomingPkt - "
                              "Link framing error on iface %d - FAIL\n",
                              u4PppIfIndex);

                    CFA_IF_SET_IN_ERR (u4PppIfIndex);
                    return (CFA_FAILURE);    /* buffer is released by GDD */
                }
                else
                {
                    /* LCP packets must not use ACFC - check */
                    if (CFA_IS_LCP_PKT (u2TempHdr))
                    {
                        /* drop the packet */
                        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                                  "Exiting CfaIwfPppHandleIncomingPkt - "
                                  "bad LCP framing on iface %d - FAIL\n",
                                  u4PppIfIndex);

                        CFA_IF_SET_IN_ERR (u4PppIfIndex);
                        return (CFA_FAILURE);    /* buffer is released by GDD */
                    }

                }
            }                    /* end of link hdr not present */
            else
            {
                /* link hdr is present - strip the header */
                u4PktSize -= CFA_PPP_HDLC_HEADER_SIZE;

                if (CRU_BUF_Move_ValidOffset (pBuf, CFA_PPP_HDLC_HEADER_SIZE)
                    != CRU_SUCCESS)
                {

                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                              "Error in CfaIwfPppHandleIncomingPkt - "
                              "Cant Strip Frame on interface %d.\n",
                              u4PppIfIndex);
                    CFA_IF_SET_IN_ERR (u4PppIfIndex);
                    return (CFA_FAILURE);    /* buffer is released by GDD */
                }
            }
            break;

        case CFA_PROT_AAL5:
            /* if the encaptype is illegal send to PPP task for taking suitable action */
            if (u1EncapsType != CFA_IF_ENCAP (u4IfIndex))
            {
                if (PppHandlePacketFromLl (pBuf, u4PppIfIndex, u4PktSize,
                                           u1EncapsType) != SUCCESS)
                {

                    CFA_DBG1 (CFA_TRC_ALL_TRACK | CFA_TRC_ERROR, CFA_IWF,
                              "Exiting CfaIwfPppHandleIncomingPkt - "
                              "Enqueue bad encapsulation in-pkt iface %d to PPP - FAIL\n",
                              u4PppIfIndex);
                    CFA_IF_SET_IN_DISCARD (u4PppIfIndex);
                    return CFA_FAILURE;
                }
                else
                {
                    CFA_DBG2 (CFA_TRC_ALL_TRACK, CFA_IWF,
                              "Exiting CfaIwfPppHandleIncomingPkt - "
                              "sent bad encapsulation in-pkt to PPP iface %d prot %d\n",
                              u4PppIfIndex, u2Protocol);
                    return CFA_SUCCESS;
                }
            }                    /* for other packets the encap has been stripped by the lower layer */
            break;

        default:
            /* drop the packet - UNK */
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                      "Exiting CfaIwfPppHandleIncomingPkt - "
                      "UNK Lower Layer on iface %d - FAIL\n", u4PppIfIndex);

            CFA_IF_SET_IN_ERR (u4PppIfIndex);
            return (CFA_FAILURE);    /* buffer is released by GDD */
            break;

    }                            /* end of switch statement */

    /* we first check if the link is non_plain - if non_plain then
       we send it to the next PPP IWF routine */
    if (!CFA_PPP_IS_NON_PLAIN_RX (u4PppIfIndex))
    {

        /* since we havent yet found the protocol of the payload we give
           protocol as PPP */

        /* incoming control packet are sent to PPP from this called PPP IWF function */

        CfaIwfPppHandleIncomingPktFromPpp (pBuf,
                                           u4PppIfIndex, u4PktSize,
                                           CFA_PROT_PPP);

        return CFA_SUCCESS;        /* the buffer is already released so we always 
                                   give success */
    }                            /* end of not non_plain link */
#endif /* PPP_BYPASS_WANTED */

    CFA_DUMP (CFA_TRC_PPP_PKT_DUMP, pBuf);

    /* at this point the pkt is either on a non_plain link or is a control
       packet - send to PPP task */
    /* straightaway enqueue it to ppp task */
    if (PppHandlePacketFromLl (pBuf, u4PppIfIndex, u4PktSize, u1EncapsType) !=
        PPP_SUCCESS)
    {

        CFA_DBG1 (CFA_TRC_ALL_TRACK | CFA_TRC_ERROR, CFA_IWF,
                  "Exiting CfaIwfPppHandleIncomingPkt -"
                  "Enqueue in-pkt on iface %d to PPP- FAIL\n", u4PppIfIndex);
        CFA_IF_SET_IN_DISCARD (u4PppIfIndex);
        return CFA_FAILURE;

    }
    else
    {
        CFA_DBG2 (CFA_TRC_ALL_TRACK, CFA_IWF,
                  "Exiting CfaIwfPppHandleIncomingPkt - "
                  "sent in-pkt to PPP iface %d protocol %d\n",
                  u4PppIfIndex, u2Protocol);
        return CFA_SUCCESS;
    }

}

/*****************************************************************************
 *                                                                                                 
 *    Function Name             : CfaIwfPppHandleIncomingPktFromPpp
 *
 *    Description               : If the PPP bypass is enabled and the protocol 
 *                                type is PPP, then this function does the PPP 
 *                                bypassing. The incoming packet is a PFC packet 
 *                                and the interface does not support the PFC then 
 *                                drops the packet. If PPP bypass is enabled strip
 *                                of the PPP header and send the packet to various 
 *                                modules based on the protocol field. 
 *
 *    Input(s)                  : PBuf - Pointer to the CRU Buffer.
 *                                U2IfIndex - MIB-2 ifIndex of port over which the 
 *                                            packet is to be transmitted.
 *                                U4PktSize - Size of the buffer to be transmitted.
 *                                U2Protocol - Protocol ID.

 *
 *    Output(s)                 : None.
 *
 *    Global Variables Referred : gapIfTable structure.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating 
 *      System Error Handling   : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
#ifdef __STDC__
INT4
CfaIwfPppHandleIncomingPktFromPpp (tCRU_BUF_CHAIN_HEADER * pBuf,
                                   UINT4 u4IfIndex, UINT4 u4PktSize,
                                   UINT2 u2Protocol)
#else /* __STDC__ */
INT4
CfaIwfPppHandleIncomingPktFromPpp (pBuf, u4IfIndex, u4PktSize, u2Protocol)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     UINT4               u4IfIndex;
     UINT4               u4PktSize;
     UINT2               u2Protocol;
#endif /* __STDC__ */
{
#ifdef PPP_BYPASS_WANTED
    UINT2               u2TempHdr = 0;
    UINT4               u4LowerIfIndex = u4IfIndex;
    UINT1              *pu1TempHdrPtr = NULL;
    UINT1               u1HeaderLen = 0;
    UINT1               u1IsPktFromPpp = CFA_TRUE;
#endif /* PPP_BYPASS_WANTED */
#ifndef KERNEL_WANTED
    UINT1               u1L3Proto = 0;
    UINT2               u2L4DstPort = 0;
    UINT4               u4SrcIp = 0;
    BOOL1               bOOBFlag = OSIX_FALSE;
#endif /* KERNEL_WANTED */

    INT4                i4Index = 0;
    UINT1               au1Llc[CFA_LLC_HEADER_SIZE] = { 0x42, 0x42, 0x03 };
    UINT1               au1EnetLLCHeader[CFA_ENET_LLC_HEADER_SIZE];
    UINT4               u4PktLen = 0;
    UINT2               u2TypeLen = 0;

    memset (au1EnetLLCHeader, 0, sizeof (au1EnetLLCHeader));
#ifndef PPP_BYPASS_WANTED
    UNUSED_PARAM (u4PktSize);
#endif
#ifndef RTPHC_WANTED
    UNUSED_PARAM (u4PktSize);
#endif

#ifdef PPP_BYPASS_WANTED
    /* check the protocol - if it is PPP then first remove the PPP framing */
    if (u2Protocol == CFA_PROT_PPP)
    {

        /* get the protocol field of the frame */
        pu1TempHdrPtr = (UINT1 *) CRU_BMC_Get_DataPointer (pBuf);
        u2TempHdr = *(UINT2 *) pu1TempHdrPtr;
        u2TempHdr = OSIX_NTOHS (u2TempHdr);

        if (CFA_IS_PFC_PKT (u2TempHdr))
        {
            /* check for protocol field compression */
            if (!CFA_PPP_IS_PFC_RX (u4IfIndex))
            {
                /* drop the packet */
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Exiting CfaIwfPppHandleIncomingPktFromPpp - "
                          "PPP framing error on interface %d - FAILURE\n",
                          u4IfIndex);

                CFA_IF_SET_IN_ERR (u4IfIndex);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return (CFA_FAILURE);
            }
            else
            {
                u2TempHdr = u2TempHdr >> 8;
            }
            ++u1HeaderLen;
            --u4PktSize;

        }                        /* end of PFC pkt */
        else
        {
            u1HeaderLen += CFA_PPP_PID_SIZE;
            u4PktSize -= CFA_PPP_PID_SIZE;
        }

        /* assign the PPP PID to the protocol type */
        u2Protocol = u2TempHdr;
        u1IsPktFromPpp = CFA_FALSE;
        u4LowerIfIndex =
            CFA_IF_STACK_IFINDEX (CFA_IF_STACK_LOW_ENTRY (u4IfIndex));
    }                            /* end of frame was PPP packet from IWF */
#endif /* PPP_BYPASS_WANTED */

    /* de-multiplex the packets on the basis of protocol - dispatch to 
       appropriate modules */
    switch (u2Protocol)
    {

#ifdef RTPHC_WANTED
        case CFA_RTP_CONTXT_PID:
            if (CFA_IF_COMPR_EN (u4IfIndex) == CFA_TRUE)
            {

#ifdef PPP_BYPASS_WANTED
                /* strip ppp header */
                if (CRU_BUF_Move_ValidOffset (pBuf, u1HeaderLen) != CRU_SUCCESS)
                {

                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                              "Error in CfaIwfPppHandleIncomingPktFromPpp - "
                              "Cant Strip Frame on interface %d.\n", u4IfIndex);
                    CFA_IF_SET_IN_ERR (u4IfIndex);
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    return (CFA_FAILURE);
                }
#endif /* PPP_BYPASS_WANTED */

                /* send to RTP context state pkt handler */
                RtphcProcessContextStatePkt (pBuf, u4PktSize, u4IfIndex);
                /* irrespective of the return case - release the buffer */
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return CFA_SUCCESS;
            }
            else
            {
                /* drop the packet */
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Exiting CfaIwfPppHandleIncomingPktFromPpp - "
                          "recd RTPHC context pkt on interface %d - FAILURE\n",
                          u4IfIndex);

                CFA_IF_SET_IN_UNK (u4IfIndex);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return (CFA_FAILURE);
            }

            break;

        case CFA_RTP_COMP_FULLHEAD_PID:
        case CFA_RTP_COMP_RTP8_PID:
        case CFA_RTP_COMP_UDP8_PID:
        case CFA_RTP_COMP_RTP16_PID:
        case CFA_RTP_COMP_UDP16_PID:
            if (CFA_IF_COMPR_EN (u4IfIndex) == CFA_TRUE)
            {

#ifdef PPP_BYPASS_WANTED
                /* strip ppp header */
                if (CRU_BUF_Move_ValidOffset (pBuf, u1HeaderLen) != CRU_SUCCESS)
                {

                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                              "Error in CfaIwfPppHandleIncomingPktFromPpp - "
                              "Cant Strip Frame on interface %d.\n", u4IfIndex);
                    CFA_IF_SET_IN_ERR (u4IfIndex);
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    return (CFA_FAILURE);
                }
#endif /* PPP_BYPASS_WANTED */

                /* send to RTP decompressor  */
                if (RtphcDecompressRtpHdr
                    (pBuf, u4PktSize, u4IfIndex, u2Protocol) != RTPHC_SUCCESS)
                {
                    /* failure in decompressing - drop the packet */
                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                              "Exiting CfaIwfPppHandleIncomingPktFromPpp - "
                              "error in RTPHC decompressinterface %d - "
                              "FAILURE\n", u4IfIndex);

                    CFA_IF_SET_IN_ERR (u4IfIndex);
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    return (CFA_FAILURE);
                }

                /* update the pkt size */
                u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

            }
            else
            {
                /* drop the packet */
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Exiting CfaIwfPppHandleIncomingPktFromPpp - "
                          "recd RTPHC compressed pkt on interface %d - "
                          "FAILURE\n", u4IfIndex);

                CFA_IF_SET_IN_UNK (u4IfIndex);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return (CFA_FAILURE);
            }

            /* we let the control fall through to IP - since the payload is IP */

#endif /* RTPHC_WANTED */

        case CFA_IPV4_PID:

#ifdef PPP_BYPASS_WANTED
            /* strip ppp header if the protocol was originally IP and not RTPHC */
#ifdef RTPHC_WANTED
            if (u2Protocol == CFA_IPV4_PID)
            {
#endif /* RTPHC_WANTED */
                if (CRU_BUF_Move_ValidOffset (pBuf, u1HeaderLen) != CRU_SUCCESS)
                {

                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                              "Error in CfaIwfPppHandleIncomingPktFromPpp - "
                              "Cant Strip Frame on interface %d.\n", u4IfIndex);
                    CFA_IF_SET_IN_ERR (u4IfIndex);
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    return (CFA_FAILURE);
                }
#ifdef RTPHC_WANTED
            }
#endif /* RTPHC_WANTED */
#endif /* PPP_BYPASS_WANTED */

            /* check if this interface is registered with IP - MP pkts will be
               sent to PPP task */
            if (CFA_IF_IPPORT (u4IfIndex) == CFA_INVALID_INDEX)
            {

                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Exiting CfaIwfPppHandleIncomingPktFromPpp - "
                          "interface %d not reg with IP - FAILURE\n",
                          u4IfIndex);

                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return (CFA_FAILURE);
            }
#ifdef ISS_WANTED
#ifndef KERNEL_WANTED
            if (CfaIsMgmtPort (u4IfIndex) == FALSE)
            {
                bOOBFlag = OSIX_FALSE;

            }
            else
            {
                bOOBFlag = OSIX_TRUE;
            }

            CfaGetL3L4Info (pBuf, &u4SrcIp, &u1L3Proto, &u2L4DstPort);
            /* Apply filter  for the management packets only
             * when IPAuthMgr is  applied at user space*/
            if (IssApplyFilter (u4IfIndex, 0, u4SrcIp,
                                u1L3Proto, u2L4DstPort,
                                bOOBFlag) == ISS_FAILURE)
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Exiting CfaIwfPppHandleIncomingPktFromPpp - Pkt"
                          "recvd on Port %d dropped on IP Authorization Failure"
                          ".\n", u4IfIndex);

                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return (CFA_FAILURE);
            }
#endif /* Kernel Wanted */
#endif /* ISS_WANTED */
            if (CfaProcessIncomingIpPkt (pBuf, u4IfIndex, CFA_LINK_UNK, NULL)
                != CFA_SUCCESS)
            {

                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Exiting CfaIwfPppHandleIncomingPktFromPpp - "
                          "IP Pkt on interface %d - FAILURE\n", u4IfIndex);

                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return (CFA_FAILURE);
            }
            return (CFA_SUCCESS);
            break;

        case CFA_STAP_PID:

            /* This is a Spanning tree BPDU in the old format */
#ifdef PPP_BYPASS_WANTED
            /* strip ppp header */
            if (CRU_BUF_Move_ValidOffset (pBuf, u1HeaderLen) != CRU_SUCCESS)
            {

                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Error in CfaIwfPppHandleIncomingPktFromPpp - "
                          "Cant Strip Frame on interface %d.\n", u4IfIndex);
                CFA_IF_SET_IN_ERR (u4IfIndex);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return (CFA_FAILURE);
            }
#endif /* PPP_BYPASS_WANTED */

            /* The old format does not have Eth header.
               Hence have to add Eth header before giving to L2Iwf */

            u4PktLen = CRU_BUF_Get_ChainValidByteCount (pBuf);

            /* Filling in the Bridge Group Address */
            au1EnetLLCHeader[0] = (UINT1) 0x01;
            au1EnetLLCHeader[1] = (UINT1) 0x80;
            au1EnetLLCHeader[2] = (UINT1) 0xC2;
            au1EnetLLCHeader[3] = (UINT1) 0x00;
            au1EnetLLCHeader[4] = (UINT1) 0x00;
            au1EnetLLCHeader[5] = (UINT1) 0x00;
            i4Index = 6;

            CfaGetIfHwAddr (u4IfIndex, &au1EnetLLCHeader[6]);
            i4Index += 6;

            /* The length in the type/length field of the mac frame, is
             * size of the mac client data following the type/length field
             * (ie) BPDU length + LLC header size */

            u2TypeLen = (UINT2) (u4PktLen + CFA_LLC_HEADER_SIZE);
            u2TypeLen = (UINT2) (OSIX_HTONS (u2TypeLen));
            MEMCPY (&au1EnetLLCHeader[i4Index], &u2TypeLen, 2);
            i4Index += 2;

            MEMCPY (&au1EnetLLCHeader[i4Index], au1Llc, CFA_LLC_HEADER_SIZE);
            i4Index += CFA_LLC_HEADER_SIZE;

            CRU_BUF_Prepend_BufChain (pBuf, au1EnetLLCHeader,
                                      CFA_ENET_LLC_HEADER_SIZE);

            L2IwfHandleIncomingLayer2Pkt (pBuf, u4IfIndex, BRG_DATA_FRAME);

            return CFA_SUCCESS;
            break;

        case CFA_BPDU_PID:

            /* This is an L2 control or data packet in the new format */
#ifdef PPP_BYPASS_WANTED
            /* strip ppp header */
            if (CRU_BUF_Move_ValidOffset (pBuf, u1HeaderLen) != CRU_SUCCESS)
            {

                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Error in CfaIwfPppHandleIncomingPktFromPpp - "
                          "Cant Strip Frame on interface %d.\n", u4IfIndex);
                CFA_IF_SET_IN_ERR (u4IfIndex);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return (CFA_FAILURE);
            }
#endif /* PPP_BYPASS_WANTED */
#ifdef BRIDGE_WANTED
            /* find out the if the Enet frame inside the packet is a STAP PDU */
            if (CfaIwfEnetDeEncapBridgeFrame
                (pBuf, (UINT4) u4IfIndex, &u2Protocol) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Error in CfaIwfPppHandleIncomingPktFromPpp - "
                          "Bad Frame for Bridge on iface %d.\n", u4IfIndex);
                CFA_IF_SET_IN_ERR (u4IfIndex);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return (CFA_FAILURE);
            }
#endif
            /* send to L2 modules */
            L2IwfHandleIncomingLayer2Pkt (pBuf, u4IfIndex, BRG_DATA_FRAME);

            return (CFA_SUCCESS);    /* buffer has been released by L2 */

            break;

#ifdef ISIS_WANTED
        case CFA_ISIS_PID:
            /* send the packet to ISIS packet handler */
            if (IsisDlliRecv (pBuf, u4IfIndex) != ISIS_SUCCESS)
            {

                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Exiting CfaIwfPppHandleIncomingPktFromPpp - IsIs Pkt on interface %d - FAILURE\n",
                          u4IfIndex);

                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return (CFA_FAILURE);
            }
            return (CFA_SUCCESS);
            break;
#endif /* ISIS_WANTED */

        default:
#ifdef PPP_BYPASS_WANTED
            /* unknown protocol discard if the packet was from PPP. Otherwise it could
               have been a incoming control packet from the incoming PPP IWF */
            if (u1IsPktFromPpp == CFA_TRUE)
            {
#endif /* PPP_BYPASS_WANTED */
                /* drop the packet */
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Exiting CfaIwfPppHandleIncomingPktFromPpp - "
                          "recd UNK pkt on interface %d - FAILURE\n",
                          u4IfIndex);

                CFA_IF_SET_IN_UNK (u4IfIndex);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return (CFA_FAILURE);

#ifdef PPP_BYPASS_WANTED
            }

            /* the packet could be a PPP control packet - send to PPP task */
            if (PppHandlePacketFromLl (pBuf, u4IfIndex, u4PktSize,
                                       CFA_IF_ENCAP (u4LowerIfIndex)) !=
                SUCCESS)
            {

                CFA_DBG1 (CFA_TRC_ALL_TRACK | CFA_TRC_ERROR, CFA_IWF,
                          "Exiting CfaIwfPppHandleIncomingPktFromPpp - "
                          "Enqueue control pkt on iface %d to PPP - FAIL\n",
                          u4IfIndex);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return CFA_FAILURE;

            }
            else
            {
                CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Exiting CfaIwfPppHandleIncomingPktFromPpp - "
                          "sent control pkt on interface %d to PPP\n",
                          u4IfIndex);
                return CFA_SUCCESS;
            }
#endif /* PPP_BYPASS_WANTED */

            break;

    }                            /* end of switch */

}

/*****************************************************************************
 *                                                                                                 
 *    Function Name             : CfaIwfPppHandleOutgoingPkt
 *
 *    Description               : This function handles all the outgoing PPP 
 *                                packets on this interface. If the compression
 *                                is enabled and interface supports the compression 
 *                                then the packets are send to the RTPHC compression 
 *                                module otherwise they are dropped. 
 *
 *                                If the PPP bypass is not enabled or PPP bypass is 
 *                                enabled and the Link is a non_plain plain link 
 *                                then all the packets are enqued PPP task. 
 *
 *                                Otherwise if PFC compression is enable then prepend 
 *                                the link layer header and Transmit the packet on the
 *                                outgoing interface
 *
 *    Input(s)                  : PBuf - Pointer to the CRU Buffer 
 *                                U2IfIndex - mib-2 ifIndex of the driver port.   
 *                                U4PktSize - Size of the buffer to be transmitted.
 *                                U2Protocol - Protocol ID.
 *
 *    Output(s)                 : None.
 *
 *    Global Variables Referred : gapIfTable structure.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating 
 *      System Error Handling   : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
#ifdef __STDC__
INT4
CfaIwfPppHandleOutgoingPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                            UINT4 u4IfIndex, UINT4 u4PktSize, UINT2 u2Protocol)
#else /*  __STDC__ */
INT4
CfaIwfPppHandleOutgoingPkt (pBuf, u4IfIndex, u4PktSize, u2Protocol)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     UINT4               u4IfIndex;
     UINT4               u4PktSize;
     UINT2               u2Protocol;
#endif /*  __STDC__ */
{
#ifdef PPP_BYPASS_WANTED
    UINT2               u2TempPid;
    UINT1               u1TempPid;
#endif /* PPP_BYPASS_WANTED */

#ifdef RTPHC_WANTED
    /* send to RTP compressor - if enabled */
    if ((u2Protocol == CFA_IPV4_PID)
        && (CFA_IF_COMPR_EN (u4IfIndex) == CFA_TRUE))
    {

        if (RtphcCompressRtpHdr (pBuf, u4PktSize, u4IfIndex, &u2Protocol)
            == RTPHC_SUCCESS)
        {
            /* update the pkt size */
            u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
        }
    }
#endif /* RTPHC_WANTED */

#ifdef BRIDGE_WANTED
    /* the protocol type can be STAP-PDU or normal frame for forwarding */
    if (u2Protocol == CFA_PROT_BPDU)
    {
        /* call API for Enet encapsulation of data frame */
        if (CfaIwfEnetEncapBridgeFrame (pBuf, u4IfIndex, &u4PktSize)
            != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                      "Error in CfaIwfPppHandleOutgoingPkt - "
                      "Framing fail for Bridge on iface %d.\n", u4IfIndex);
            CFA_IF_SET_OUT_ERR (u4IfIndex);
            return (CFA_FAILURE);
        }
        else
        {
            /* change the protocol to PPP's PID */
            u2Protocol = CFA_BPDU_PID;
        }
    }
    else if (u2Protocol == CFA_PROT_STAP)
    {
        u2Protocol = CFA_STAP_PID;
    }
    else if (u2Protocol == CFA_PROT_L2_DATA)
    {
        /* call API for Enet encapsulation of data frame */
        if (CfaIwfEnetEncapBridgeFrame (pBuf, u4IfIndex, &u4PktSize)
            != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                      "Error in CfaIwfPppHandleOutgoingPkt - "
                      "Framing fail for Bridge on iface %d.\n", u4IfIndex);
            CFA_IF_SET_OUT_ERR (u4IfIndex);
            return (CFA_FAILURE);
        }
        /* PID is same for both BPDUs and L2 data */
        u2Protocol = CFA_BPDU_PID;
    }

#endif /* BRIDGE_WANTED */

#ifdef PPP_BYPASS_WANTED
    /* check if the link is non plain - if so then send to PPP task along with
       the protocol type of the payload */
    if (CFA_PPP_IS_NON_PLAIN_TX (u4IfIndex))
    {
#endif /* PPP_BYPASS_WANTED */

        /* straightaway enqueue it to ppp task */
        if (PppHandlePacketFromHl (pBuf, u4IfIndex, u4PktSize, u2Protocol)
            != PPP_SUCCESS)
        {

            CFA_IF_SET_OUT_DISCARD (u4IfIndex);

            CFA_DBG1 (CFA_TRC_ALL_TRACK | CFA_TRC_ERROR, CFA_IWF,
                      "Exiting CfaIwfPppHandleOutgoingPkt - "
                      "Enqueue outgoing pkt on iface %d to PPP - FAIL\n",
                      u4IfIndex);
            return CFA_FAILURE;

        }
        else
        {
            CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
                      "Exiting CfaIwfPppHandleOutgoingPkt - "
                      "sent outgoing pkt on interface %d to PPP\n", u4IfIndex);
            return CFA_SUCCESS;
        }
#ifdef PPP_BYPASS_WANTED
    }                            /* end of non plain link */

    /* at this point we have to do the PFC compression if necessary and send
       the packet for link layer header prepending and Tx to LL */

    /* normally we do not expect any control packets - but in the case of RTPHC
       we would be getting context state and UDP/RTP 16 packets. We should not do
       PFC for these packets - hence a check if RTPHC is present in the system */

    if ((CFA_PPP_IS_PFC_TX (u4IfIndex))
#ifdef RTPHC_WANTED
        && (CFA_IS_PFC_POSSIBLE (u2Protocol))
#endif /* RTPHC_WANTED */
        )
    {
        u1TempPid = (UINT1) u2Protocol;    /* the PID < 256 */
        ++u4PktSize;

        if (CRU_BUF_Prepend_BufChain (pBuf, &u1TempPid, 1) != CRU_SUCCESS)
        {
            CFA_IF_SET_OUT_ERR (u4IfIndex);

            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                      "Exiting CfaIwfPppHandleOutgoingPkt - "
                      "Framing on interface %d - FAILURE\n", u4IfIndex);
            return CFA_FAILURE;
        }
    }
    else
    {
        u2TempPid = OSIX_HTONS (u2Protocol);
        u4PktSize += CFA_PPP_PID_SIZE;

        if (CRU_BUF_Prepend_BufChain
            (pBuf, (UINT1 *) &u2TempPid, CFA_PPP_PID_SIZE) != CRU_SUCCESS)
        {
            CFA_IF_SET_OUT_ERR (u4IfIndex);

            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                      "Exiting CfaIwfPppHandleOutgoingPkt - "
                      "Framing on interface %d - FAILURE\n", u4IfIndex);
            return CFA_FAILURE;
        }
    }

    /* directly call the outgoing interface */
    if (CfaIwfPppHandleOutGoingPktFromPpp
        (pBuf, u4IfIndex, u4PktSize, u2Protocol) != CFA_SUCCESS)
    {

        /* in case of failure - buf would have been already freed */
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                  "Exiting CfaIwfPppHandleOutgoingPkt - "
                  "Sending out on interface %d - FAILURE\n", u4IfIndex);
    }
    else
    {

        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
                  "Exiting CfaIwfPppHandleOutgoingPkt - "
                  "Sending out on interface %d - SUCCESS\n", u4IfIndex);
    }
    return CFA_SUCCESS;
#endif /* PPP_BYPASS_WANTED */

}

/*****************************************************************************
 *                                                                                                 
 *    Function Name             : CfaIwfPppHandleOutGoingPktFromPpp
 *
 *    Description               : If the ACFC negotiation is not enabled then 
 *                                this function does the physical layer framing. 
 *                                If the protocol type is LCP and ACFC is 
 *                                negotiatiated then framing is done. 
 *
 *                                Finally get the lower layer ifIndex from the stack 
 *                                table (For PPP the lower layer is the device driver 
 *                                port) and call Gdd write function to send the packet 
 *                                to device driver.
 *
 *    Input(s)                  : PBuf - Pointer to the CRU Buffer.
 *                                MIB-2 ifIndex of the driver port.
 *                                U4PktSize - Size of the buffer to be transmitted.
 *                                U2Protocol - Protocol ID.
 *
 *    Output(s)                 : None.
 *
 *    Global Variables Referred : gapIfTable.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating 
 *      System Error Handling   : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
#ifdef __STDC__
INT4
CfaIwfPppHandleOutGoingPktFromPpp (tCRU_BUF_CHAIN_HEADER * pBuf,
                                   UINT4 u4IfIndex, UINT4 u4PktSize,
                                   UINT2 u2Protocol)
#else /* __STDC__ */
INT4
CfaIwfPppHandleOutGoingPktFromPpp (pBuf, u4IfIndex, u4PktSize, u2Protocol)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     UINT4               u4IfIndex;
     UINT4               u4PktSize;
     UINT2               u2Protocol;
#endif /* __STDC__ */
{
    UINT4               u4PhysIfIndex;
    UINT1               u1CurType;
    UINT1               au1SerialInterface[12];
    tCfaIfInfo          IfInfo;
    UINT4               u4HdlcIfIndex;

    MEMSET(&IfInfo, 0, sizeof(tCfaIfInfo));

    if (CFA_IF_ENTRY (u4IfIndex) == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return CFA_FAILURE;
    }

    CfaGetIfType (u4IfIndex, &u1CurType);

    if (u1CurType == CFA_MP)
    {
        /* If an external processor is used for multilink fragmentation
         * and reassembly then -
         *   - Buffer passed to NP should contain the entire PPP packet.
         *   - Tx interface index passed should be that of the MP interface
         *     directly instead of any lower layer physical interface index.
         */
        u4PhysIfIndex = u4IfIndex;
    }
    else
    {
        u4PhysIfIndex =
            CFA_IF_STACK_IFINDEX (CFA_IF_STACK_LOW_ENTRY (u4IfIndex));
    }

#ifdef PPP_BYPASS_WANTED
    UINT1               au1HdlcHdr[CFA_PPP_HDLC_HEADER_SIZE] = { 0xff, 0x03 };
#endif /* PPP_BYPASS_WANTED */

    CFA_DUMP (CFA_TRC_PPP_PKT_DUMP, pBuf);

    /* LL Classification for the PPP Interface:- 
     * 1. L2TP
     * 2. HDLC
     */
    if (u4PhysIfIndex == 0)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return (CFA_FAILURE);
    }

    if (PPP_SUCCESS == PPPIsLLIL2TP (u4IfIndex))
    {
#ifdef L2TP_WANTED
        if (CfaIwfL2tpProcessRxFrame
            (pBuf, u4IfIndex, u4PktSize, u2Protocol,
             CFA_ENCAP_NONE) != CFA_SUCCESS)
        {
            CFA_IF_SET_OUT_ERR (u4IfIndex);

            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                      "Exiting CfaIwfPppHandleOutgoingPktFromPpp - "
                      "Send to L2TP %d - FAILURE\n", u4IfIndex);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return (CFA_FAILURE);
        }
        return (CFA_SUCCESS);
#endif /* L2TP_WANTED */
    }
    CfaGetIfType (u4PhysIfIndex, &u1CurType);

#ifdef PPP_BYPASS_WANTED
    /* add the physical layer framing, if ACFC is not negotiated. Also if the
       protocol is LCP then header is put even if ACFC is negotiated */
    if (((u1CurType == CFA_ASYNC) || (u1CurType == CFA_HDLC)) &&
        ((!CFA_PPP_IS_ACFC_TX (u4IfIndex)) || (u2Protocol == CFA_LCP_PID)))
    {

        if (CRU_BUF_Prepend_BufChain
            (pBuf, au1HdlcHdr, CFA_PPP_HDLC_HEADER_SIZE) != CRU_SUCCESS)
        {
            CFA_IF_SET_OUT_ERR (u4IfIndex);

            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                      "Exiting CfaIwfPppHandleOutgoingPktFromPpp - "
                      "Framing on interface %d - FAILURE\n", u4IfIndex);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return CFA_FAILURE;
        }
        u4PktSize += CFA_PPP_HDLC_HEADER_SIZE;

    }

    CFA_IF_SET_OUT_OCTETS (u4IfIndex, u4PktSize);
    CFA_IF_SET_OUT_BCAST (u4IfIndex);
#endif /* PPP_BYPASS_WANTED */
#ifdef HDLC_WANTED
    if (u1CurType == CFA_HDLC)
    {
        u4HdlcIfIndex = u4PhysIfIndex;
        CfaGetIfAlias (u4HdlcIfIndex, au1SerialInterface);

        if (CRU_BUF_Prepend_BufChain
            (pBuf, au1SerialInterface, 12) != CRU_SUCCESS)
        {
            CFA_IF_SET_OUT_ERR (u4IfIndex);

            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                      "Exiting CfaIwfPppHandleOutgoingPktFromPpp - "
                      "Framing on interface %d - FAILURE\n", u4IfIndex);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return CFA_FAILURE;
        }
        u4PktSize += 12;

        if (CfaPppGetPhyIndexFromHdlcIndex (u4HdlcIfIndex, &u4PhysIfIndex)
            != CFA_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CFA_IF_SET_OUT_DISCARD (u4IfIndex);
            return CFA_FAILURE;
        }
    }
#endif
    if (CfaGetIfInfo (u4PhysIfIndex, &IfInfo) != CFA_SUCCESS)
    {
        /* Memory should be freed wherever it is called */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        CFA_IF_SET_OUT_DISCARD (u4IfIndex);
        return CFA_FAILURE;
    }

    /* currently we have only physical ports below PPP */
    if (CfaGddWrite (pBuf, u4PhysIfIndex, u4PktSize, CFA_FALSE, &IfInfo) !=
        CFA_SUCCESS)
    {

        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                  "Error in CfaIwfPppHandleOutGoingPktFromPpp - "
                  "write to LL on interface %d -FAIL.\n", u4IfIndex);
        CFA_IF_SET_OUT_DISCARD (u4IfIndex);
        return CFA_FAILURE;

    }

    CFA_DBG2 (CFA_TRC_ALL_TRACK, CFA_IWF,
              "In CfaIwfPppHandleOutGoingPktFromPpp - "
              "write to LL on iface %d protocol % d - SUCCESS.\n",
              u4IfIndex, u2Protocol);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *                                                                                                 
 *    Function Name             : CfaIwfPppProcessTxAsyncPkt
 *
 *    Description               : This function computes the checksum and does 
 *                                the byte stuffing according to Asynchronous 
 *                                Control Character Mapping.
 *
 *    Input(s)                  : PData - Pointer to the CRU Buffer.
 *                                u24MaxLength - Size of the buffer to be transmitted.
 *                                pAsyncParams - Pointer to the tLlAsyncParams 
 *                                               structure.
 *
 *    Output(s)                 : pNewData - Pointer to the output data.
 *                                pu4Length - Pointer to the length of new data.     
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating 
 *      System Error Handling   : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
#ifdef __STDC__
INT4
CfaIwfPppProcessTxAsyncPkt (tLlAsyncParams * pAsyncParams,
                            tCRU_BUF_CHAIN_HEADER * pData, UINT4 *pu4Length,
                            UINT1 *pNewData, UINT2 u2MaxLength)
#else /* __STDC__ */
INT4
CfaIwfPppProcessTxAsyncPkt (pAsyncParams, pData, pu4Length, pNewData,
                            u2MaxLength)
     tLlAsyncParams     *pAsyncParams;
     tCRU_BUF_CHAIN_HEADER *pData;
     UINT4              *pu4Length;
     UINT1              *pNewData;
     UINT2               u2MaxLength;
#endif /* __STDC__ */
{

    UINT1               u1FCSBytes = 0;
    UINT4               u4FCS = 0;
    tCRU_BUF_DATA_DESC *pDataDesc = NULL;
    UINT1              *pTempDataPtr = NULL;
    UINT2               u2BlockLen;
    UINT2               u2TempLen;
    UINT1               u1Octet;

    u1FCSBytes = (UINT1) (pAsyncParams->u1TxFcsSize / 8);
    *pu4Length += u1FCSBytes;
    u2TempLen = (UINT2) *pu4Length;

    if (u1FCSBytes)
    {
        if (pAsyncParams->u1TxFcsSize == 16)
        {
            u4FCS = CFA_PPP_INIT_FCS_16;
            u4FCS &= 0x0000ffff;
        }
        if (pAsyncParams->u1TxFcsSize == 32)
        {
            u4FCS = CFA_PPP_INIT_FCS_32;
        }
    }

    pDataDesc = pData->pFirstValidDataDesc;

    while (u2TempLen)
    {

        if (pDataDesc != NULL)
        {
            pTempDataPtr = pDataDesc->pu1_FirstValidByte;
            u2BlockLen = (UINT2) pDataDesc->u4_ValidByteCount;
            pDataDesc = pDataDesc->pNext;
        }
        else
            u2BlockLen = 0;

        while (((u2BlockLen) || (u2TempLen <= u1FCSBytes)) && (u2TempLen))
        {

            if (u1FCSBytes)
            {

                if (u2TempLen > u1FCSBytes)
                {
                    u1Octet = *pTempDataPtr++;
                    u2BlockLen--;
                    if (pAsyncParams->u1TxFcsSize == 16)
                    {
                        u4FCS = (u4FCS >> 8) ^
                            AsyncFcsTable16[(u4FCS ^ u1Octet) & 0xff];
                    }
                    else
                    {
                        u4FCS = (u4FCS >> 8) ^
                            AsyncFcsTable32[(u4FCS ^ u1Octet) & 0xff];
                    }
                }
                else
                {
                    u4FCS = (u2TempLen == u1FCSBytes) ? (u4FCS ^ 0xffffffff) :
                        (u4FCS >> 8);
                    u1Octet = (UINT1) u4FCS & 0x00ff;
                }
            }                    /* for FCS */
            else
            {
                u1Octet = *pTempDataPtr++;
                u2BlockLen--;
            }                    /* if not FCS */

            if ((u1Octet == FLAG_SEQ) || (u1Octet == CTRL_ESC) ||
                (MAPPED_IN_ACCM (u1Octet, pAsyncParams->pTxACCMap)
                 && (u1Octet != 0x5e)))
            {
                *pNewData++ = CTRL_ESC;
                (*pu4Length)++;
                *pNewData++ = (u1Octet ^ 0x20);
            }
            else
            {
                *pNewData++ = u1Octet;
            }

            --u2TempLen;

            if ((*pu4Length) > (UINT4) (u2MaxLength - 2))
                return CFA_FAILURE;

        }                        /* end of while(u2Len) */

    }                            /* end of chained buf */

    return CFA_SUCCESS;

}

/*****************************************************************************
 *                                                                                                 
 *    Function Name             : CfaIwfPppProcessRxAsyncPkt
 *
 *    Description               : This function validates the check sum of the 
 *                                incoming packet and does the byte stuffing 
 *                                according to the ACCM.
 *
 *    Input(s)                  : PData - Pointer to the CRU Buffer.
 *                                pAsyncParams - Pointer to the tLlAsyncParams 
 *                                               structure.
 *                                pu4Length - Pointer to the length of the buffer. 
 *
 *    Output(s)                 : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating 
 *      System Error Handling   : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS or CFA_FAILURE
 *
 *****************************************************************************/
#ifdef __STDC__
INT4
CfaIwfPppProcessRxAsyncPkt (tLlAsyncParams * pAsyncParams, UINT1 *pData,
                            UINT4 *pu4Length)
#else /* __STDC__ */
INT4
CfaIwfPppProcessRxAsyncPkt (pAsyncParams, pData, pu4Length)
     tLlAsyncParams     *pAsyncParams;
     UINT1              *pData;
     UINT4              *pu4Length;
#endif /* __STDC__ */
{

    UINT1               u1Octet;
    UINT2               u2TempLength;
    UINT4               u4FCS = 0;
    UINT1              *pu1ReadOffset = NULL;
    UINT1              *pu1WriteOffset = NULL;
    UINT1               u1FcsStatus = 0;

    u2TempLength = (UINT2) *pu4Length;
    pu1ReadOffset = pu1WriteOffset = pData;

    if (pAsyncParams->u1RxFcsSize == 16)
    {
        u4FCS = CFA_PPP_INIT_FCS_16;
        u4FCS &= 0x0000ffff;
    }
    else
    {
        u4FCS = CFA_PPP_INIT_FCS_32;
    }

    while (u2TempLength)
    {
        u1Octet = *pu1ReadOffset++;

        if (!((u1Octet < 0x20) &&
              MAPPED_IN_ACCM (u1Octet, pAsyncParams->pRxACCMap)))
        {

            if (u1Octet == CTRL_ESC)
            {
                u1Octet = *pu1ReadOffset++;
                (*pu4Length)--;
                u1Octet ^= 0x20;
                *pu1WriteOffset++ = u1Octet;
                u2TempLength--;
            }
            else
            {
                *pu1WriteOffset++ = u1Octet;
            }
        }

        if (pAsyncParams->u1RxFcsSize)
        {
            if (pAsyncParams->u1RxFcsSize == 16)
            {
                u4FCS =
                    (u4FCS >> 8) ^ AsyncFcsTable16[(u4FCS ^ u1Octet) & 0xff];
            }
            else
            {
                u4FCS =
                    (u4FCS >> 8) ^ AsyncFcsTable32[(u4FCS ^ u1Octet) & 0xff];
            }
        }                        /* for FCS */

        if (u2TempLength > 0)
            --u2TempLength;
    }

    *pu4Length -= pAsyncParams->u1RxFcsSize / 8;

    if (pAsyncParams->u1RxFcsSize)
    {
        if (pAsyncParams->u1RxFcsSize == 16)
        {
            u1FcsStatus = ((u4FCS == CFA_PPP_GOOD_FCS_16) ? 1 : 0);
        }
        if (pAsyncParams->u1RxFcsSize == 32)
        {
            u1FcsStatus = ((u4FCS == CFA_PPP_GOOD_FCS_32) ? 1 : 0);
        }
    }
    else
    {
        u1FcsStatus = 1;
    }

    if (u1FcsStatus)
    {
        return (CFA_SUCCESS);
    }
    else
    {
        return (CFA_FAILURE);
    }

}

/*****************************************************************************
 *                                                                    
 *    Function Name             : CfaIwfPPPoEHandleOutGoingPkt 
 *
 *    Description               : Basing on the ifType this function would call
 *                  the corresponding Tx functions 
 *
 *    Input(s)                  : PBuf - Pointer to the CRU Buffer.
 *                                u4IfIndex - ifIndex of the driver port.
 *                                u4PktSize - Size of the buffer to be Tx
 *                                u2Protocol - Protocol ID.
 *                                au1DestHwAddr - destination media address
 *
 *    Output(s)                 : None.
 *
 *    Global Variables Referred : gapIfTable.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating 
 *      System Error Handling   : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
#ifdef __STDC__
INT4
CfaIwfPPPoEHandleOutGoingPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                              UINT4 u4PktSize, UINT2 u2Protocol,
                              UINT1 *au1DestHwAddr)
#else /* __STDC__ */
INT4
CfaIwfPPPoEHandleOutGoingPkt (pBuf, u4IfIndex, u4PktSize, u2Protocol,
                              au1DestHwAddr)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     UINT4               u4IfIndex;
     UINT4               u4PktSize;
     UINT2               u2Protocol;
     UINT1              *au1DestHwAddr;
#endif /* __STDC__ */
{
    INT4                i4RetVal = CFA_FAILURE;
    UINT1               u1IfType = 0;
    i4RetVal = CfaGetIfType (u4IfIndex, &u1IfType);

    if (CFA_FAILURE == i4RetVal)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "CfaIwfPPPoEHandleOutGoingPkt: "
                 "CfaGetIfType() returned failure! \r\n");
        return (i4RetVal);
    }
    /* Get the PPPoE Header Information from the 
     */
    switch (u1IfType)
    {
        case CFA_ENET:
            if (CfaIwfEnetProcessTxFrame
                (pBuf, u4IfIndex, au1DestHwAddr, u4PktSize,
                 u2Protocol, CFA_ENCAP_ENETV2) != CFA_SUCCESS)
            {
                /* release of the pkt in IWF/GDD */
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "PPPoE pkt dispatch to IWF fail.\n");
                return (CFA_FAILURE);
            }
            break;
        default:
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                     "Lower layer interface is neither  "
                     "Ethernet nor Atmvc\n");

            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return (CFA_FAILURE);
            break;
    }
    return (CFA_SUCCESS);
}
#ifdef HDLC_WANTED
/*****************************************************************************
 *                                                                                                 
 *    Function Name             : CfaPppGetPhyIndexFromHdlcIndex
 *
 *    Description               : This function to get physical Index based on 
 *                                HDLC Index.                                 
 *
 *    Input(s)                  : u4HdlcIfIndex - HDLC Index.
 *
 *    Output(s)                 : pu4PhysIfIndex - Physical Index.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Returns                   : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/

INT4 CfaPppGetPhyIndexFromHdlcIndex (UINT4 u4HdlcIfIndex, UINT4 *pu4PhysIfIndex)
{
    *pu4PhysIfIndex = (u4HdlcIfIndex - CFA_MIN_HDLC_INDEX) + 1;
    if ((*pu4PhysIfIndex >= 1) && (*pu4PhysIfIndex <= 10))
    {
        return (CFA_SUCCESS);
    }
    return (CFA_FAILURE);
}
#endif
#endif /* PPP_WANTED */
