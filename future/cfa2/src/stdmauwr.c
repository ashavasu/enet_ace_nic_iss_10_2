/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains the mid level wrapper routines for
 *              the MAU MIB (RFC 4836).
 *
 *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "fssnmp.h"
#include "stdmauwr.h"
#include "stdmaulw.h"
#include "stdmaudb.h"

#ifdef SNMP_2_WANTED
VOID
RegisterSTDMAU ()
{
    SNMPRegisterMibWithLock (&stdmauOID, &stdmauEntry, CfaLock, CfaUnlock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&stdmauOID, (const UINT1 *) "mauMod");
}

VOID
UnRegisterSTDMAU ()
{
    SNMPUnRegisterMib (&stdmauOID, &stdmauEntry);
    SNMPDelSysorEntry (&stdmauOID, (const UINT1 *) "stdmau");
}
#endif
INT4
RpMauGroupIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRpMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;
    return SNMP_SUCCESS;
}

INT4
RpMauPortIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRpMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[1].i4_SLongValue;
    return SNMP_SUCCESS;
}

INT4
RpMauIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRpMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[2].i4_SLongValue;
    return SNMP_SUCCESS;
}

INT4
RpMauTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRpMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRpMauType (pMultiIndex->pIndex[0].i4_SLongValue,
                             pMultiIndex->pIndex[1].i4_SLongValue,
                             pMultiIndex->pIndex[2].i4_SLongValue,
                             pMultiData->pOidValue));
}

INT4
RpMauStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{

    return (nmhTestv2RpMauStatus (pu4Error,
                                  pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].i4_SLongValue,
                                  pMultiIndex->pIndex[2].i4_SLongValue,
                                  &pMultiData->i4_SLongValue));
}

INT4
RpMauStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetRpMauStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                               pMultiIndex->pIndex[1].i4_SLongValue,
                               pMultiIndex->pIndex[2].i4_SLongValue,
                               pMultiData->i4_SLongValue));
}

INT4
RpMauStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRpMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRpMauStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                               pMultiIndex->pIndex[1].i4_SLongValue,
                               pMultiIndex->pIndex[2].i4_SLongValue,
                               &pMultiData->i4_SLongValue));
}

INT4
RpMauMediaAvailableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRpMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRpMauMediaAvailable (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       &pMultiData->i4_SLongValue));
}

INT4
RpMauMediaAvailableStateExitsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRpMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRpMauMediaAvailableStateExits
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
RpMauJabberStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRpMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRpMauJabberState (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    &pMultiData->i4_SLongValue));
}

INT4
RpMauJabberingStateEntersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRpMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRpMauJabberingStateEnters
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
RpMauFalseCarriersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRpMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRpMauFalseCarriers (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      &pMultiData->u4_ULongValue));
}

INT4
RpJackTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRpJackTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRpJackType (pMultiIndex->pIndex[0].i4_SLongValue,
                              pMultiIndex->pIndex[1].i4_SLongValue,
                              pMultiIndex->pIndex[2].i4_SLongValue,
                              pMultiIndex->pIndex[3].i4_SLongValue,
                              &pMultiData->i4_SLongValue));
}

INT4
RpMauTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2RpMauTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IfMauIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;
    return SNMP_SUCCESS;
}

INT4
IfMauIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[1].i4_SLongValue;
    return SNMP_SUCCESS;
}

INT4
IfMauTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauType (pMultiIndex->pIndex[0].i4_SLongValue,
                             pMultiIndex->pIndex[1].i4_SLongValue,
                             pMultiData->pOidValue));
}

INT4
IfMauStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{

    return (nmhTestv2IfMauStatus (pu4Error,
                                  pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].i4_SLongValue,
                                  &pMultiData->i4_SLongValue));
}

INT4
IfMauStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetIfMauStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                               pMultiIndex->pIndex[1].i4_SLongValue,
                               pMultiData->i4_SLongValue));
}

INT4
IfMauStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                               pMultiIndex->pIndex[1].i4_SLongValue,
                               &pMultiData->i4_SLongValue));
}

INT4
IfMauMediaAvailableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauMediaAvailable (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &pMultiData->i4_SLongValue));
}

INT4
IfMauMediaAvailableStateExitsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauMediaAvailableStateExits
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
IfMauJabberStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauJabberState (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    &pMultiData->i4_SLongValue));
}

INT4
IfMauJabberingStateEntersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauJabberingStateEnters
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
IfMauFalseCarriersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauFalseCarriers (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      &pMultiData->u4_ULongValue));
}

INT4
IfMauTypeListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauTypeList (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].i4_SLongValue,
                                 &pMultiData->i4_SLongValue));
}

INT4
IfMauDefaultTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{

    return (nmhTestv2IfMauDefaultType (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiData->pOidValue));
}

INT4
IfMauDefaultTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetIfMauDefaultType (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiData->pOidValue));
}

INT4
IfMauDefaultTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauDefaultType (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiData->pOidValue));
}

INT4
IfMauAutoNegSupportedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauAutoNegSupported (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &pMultiData->i4_SLongValue));
}

INT4
IfMauTypeListBitsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauTypeListBits (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiData->pOctetStrValue));
}

INT4
IfJackTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfJackTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfJackType (pMultiIndex->pIndex[0].i4_SLongValue,
                              pMultiIndex->pIndex[1].i4_SLongValue,
                              pMultiIndex->pIndex[2].i4_SLongValue,
                              &pMultiData->i4_SLongValue));
}

INT4
IfMauTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IfMauTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IfMauAutoNegAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{

    return (nmhTestv2IfMauAutoNegAdminStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              i4_SLongValue,
                                              &pMultiData->i4_SLongValue));
}

INT4
IfMauAutoNegAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetIfMauAutoNegAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiData->i4_SLongValue));
}

INT4
IfMauAutoNegAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauAutoNegTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauAutoNegAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &pMultiData->i4_SLongValue));
}

INT4
IfMauAutoNegRemoteSignalingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauAutoNegTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauAutoNegRemoteSignaling
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, &pMultiData->i4_SLongValue));
}

INT4
IfMauAutoNegConfigGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauAutoNegTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauAutoNegConfig (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      &pMultiData->i4_SLongValue));
}

INT4
IfMauAutoNegCapabilityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauAutoNegTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauAutoNegCapability (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &pMultiData->i4_SLongValue));
}

INT4
IfMauAutoNegCapAdvertisedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{

    return (nmhTestv2IfMauAutoNegCapAdvertised (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[1].
                                                i4_SLongValue,
                                                &pMultiData->i4_SLongValue));
}

INT4
IfMauAutoNegCapAdvertisedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetIfMauAutoNegCapAdvertised
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));
}

INT4
IfMauAutoNegCapAdvertisedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauAutoNegTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauAutoNegCapAdvertised
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, &pMultiData->i4_SLongValue));
}

INT4
IfMauAutoNegCapReceivedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauAutoNegTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauAutoNegCapReceived (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &pMultiData->i4_SLongValue));
}

INT4
IfMauAutoNegRestartTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{

    return (nmhTestv2IfMauAutoNegRestart (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &pMultiData->i4_SLongValue));
}

INT4
IfMauAutoNegRestartSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetIfMauAutoNegRestart (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiData->i4_SLongValue));
}

INT4
IfMauAutoNegRestartGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauAutoNegTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauAutoNegRestart (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &pMultiData->i4_SLongValue));
}

INT4
IfMauAutoNegCapabilityBitsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauAutoNegTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauAutoNegCapabilityBits
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->pOctetStrValue));
}

INT4
IfMauAutoNegCapAdvertisedBitsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{

    return (nmhTestv2IfMauAutoNegCapAdvertisedBits (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[1].
                                                    i4_SLongValue,
                                                    pMultiData->
                                                    pOctetStrValue));
}

INT4
IfMauAutoNegCapAdvertisedBitsSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{

    return (nmhSetIfMauAutoNegCapAdvertisedBits
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->pOctetStrValue));
}

INT4
IfMauAutoNegCapAdvertisedBitsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauAutoNegTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauAutoNegCapAdvertisedBits
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->pOctetStrValue));
}

INT4
IfMauAutoNegCapReceivedBitsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauAutoNegTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauAutoNegCapReceivedBits
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->pOctetStrValue));
}

INT4
IfMauAutoNegRemoteFaultAdvertisedTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{

    return (nmhTestv2IfMauAutoNegRemoteFaultAdvertised (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiIndex->pIndex[1].
                                                        i4_SLongValue,
                                                        &pMultiData->
                                                        i4_SLongValue));
}

INT4
IfMauAutoNegRemoteFaultAdvertisedSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{

    return (nmhSetIfMauAutoNegRemoteFaultAdvertised
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));
}

INT4
IfMauAutoNegRemoteFaultAdvertisedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauAutoNegTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauAutoNegRemoteFaultAdvertised
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, &pMultiData->i4_SLongValue));
}

INT4
IfMauAutoNegRemoteFaultReceivedGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceIfMauAutoNegTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauAutoNegRemoteFaultReceived
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, &pMultiData->i4_SLongValue));
}

INT4
BroadMauIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceBroadMauBasicTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;
    return SNMP_SUCCESS;
}

INT4
BroadMauIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceBroadMauBasicTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[1].i4_SLongValue;
    return SNMP_SUCCESS;
}

INT4
BroadMauXmtRcvSplitTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceBroadMauBasicTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBroadMauXmtRcvSplitType (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &pMultiData->i4_SLongValue));
}

INT4
BroadMauXmtCarrierFreqGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceBroadMauBasicTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBroadMauXmtCarrierFreq (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &pMultiData->i4_SLongValue));
}

INT4
BroadMauTranslationFreqGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceBroadMauBasicTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBroadMauTranslationFreq (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &pMultiData->i4_SLongValue));
}

INT4
IfMauAutoNegTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IfMauAutoNegTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexBroadMauBasicTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    INT4                i4broadMauIfIndex;
    INT4                i4broadMauIndex;
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexBroadMauBasicTable (&i4broadMauIfIndex,
                                                &i4broadMauIndex)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexBroadMauBasicTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4broadMauIfIndex,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &i4broadMauIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4broadMauIfIndex;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4broadMauIndex;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexIfMauAutoNegTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    INT4                i4ifMauIfIndex;
    INT4                i4ifMauIndex;
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIfMauAutoNegTable (&i4ifMauIfIndex,
                                               &i4ifMauIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIfMauAutoNegTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4ifMauIfIndex,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &i4ifMauIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4ifMauIfIndex;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4ifMauIndex;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexIfJackTable (tSnmpIndex * pFirstMultiIndex,
                         tSnmpIndex * pNextMultiIndex)
{
    INT4                i4ifMauIfIndex;
    INT4                i4ifMauIndex;
    INT4                i4ifJackIndex;
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIfJackTable (&i4ifMauIfIndex,
                                         &i4ifMauIndex,
                                         &i4ifJackIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIfJackTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4ifMauIfIndex,
             pFirstMultiIndex->pIndex[1].i4_SLongValue, &i4ifMauIndex,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &i4ifJackIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4ifMauIfIndex;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4ifMauIndex;
    pNextMultiIndex->pIndex[2].i4_SLongValue = i4ifJackIndex;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexIfMauTable (tSnmpIndex * pFirstMultiIndex,
                        tSnmpIndex * pNextMultiIndex)
{
    INT4                i4ifMauIfIndex;
    INT4                i4ifMauIndex;
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIfMauTable (&i4ifMauIfIndex,
                                        &i4ifMauIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIfMauTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4ifMauIfIndex,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &i4ifMauIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4ifMauIfIndex;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4ifMauIndex;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexRpJackTable (tSnmpIndex * pFirstMultiIndex,
                         tSnmpIndex * pNextMultiIndex)
{
    INT4                i4rpMauGroupIndex;
    INT4                i4rpMauPortIndex;
    INT4                i4rpMauIndex;
    INT4                i4rpJackIndex;
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexRpJackTable (&i4rpMauGroupIndex,
                                         &i4rpMauPortIndex,
                                         &i4rpMauIndex,
                                         &i4rpJackIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexRpJackTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4rpMauGroupIndex,
             pFirstMultiIndex->pIndex[1].i4_SLongValue, &i4rpMauPortIndex,
             pFirstMultiIndex->pIndex[2].i4_SLongValue, &i4rpMauIndex,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &i4rpJackIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4rpMauGroupIndex;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4rpMauPortIndex;
    pNextMultiIndex->pIndex[2].i4_SLongValue = i4rpMauIndex;
    pNextMultiIndex->pIndex[3].i4_SLongValue = i4rpJackIndex;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexRpMauTable (tSnmpIndex * pFirstMultiIndex,
                        tSnmpIndex * pNextMultiIndex)
{
    INT4                i4rpMauGroupIndex;
    INT4                i4rpMauPortIndex;
    INT4                i4rpMauIndex;
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexRpMauTable (&i4rpMauGroupIndex,
                                        &i4rpMauPortIndex,
                                        &i4rpMauIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexRpMauTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4rpMauGroupIndex,
             pFirstMultiIndex->pIndex[1].i4_SLongValue, &i4rpMauPortIndex,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &i4rpMauIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4rpMauGroupIndex;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4rpMauPortIndex;
    pNextMultiIndex->pIndex[2].i4_SLongValue = i4rpMauIndex;
    return SNMP_SUCCESS;
}

INT4
IfMauHCFalseCarriersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIfMauTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIfMauHCFalseCarriers (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        &(pMultiData->u8_Counter64Value)));

}
