
/* $Id: stdethnc.c,v 1.2 2016/10/21 13:42:21 siva Exp $
    ISS Wrapper module
    module EtherLike-MIB

 */

# include  "lr.h"
# include  "cfa.h"
# include  "fssnmp.h"
# include  "stdethlw.h"
# include  "stdethnc.h"


/********************************************************************
* FUNCTION NcDot3StatsAlignmentErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3StatsAlignmentErrorsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsAlignmentErrors )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3StatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3StatsAlignmentErrors(
                 i4Dot3StatsIndex,
                 pu4Dot3StatsAlignmentErrors );

    return i4RetVal;


} /* NcDot3StatsAlignmentErrorsGet */

/********************************************************************
* FUNCTION NcDot3StatsFCSErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3StatsFCSErrorsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsFCSErrors )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3StatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3StatsFCSErrors(
                 i4Dot3StatsIndex,
                 pu4Dot3StatsFCSErrors );

    return i4RetVal;


} /* NcDot3StatsFCSErrorsGet */

/********************************************************************
* FUNCTION NcDot3StatsSingleCollisionFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3StatsSingleCollisionFramesGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsSingleCollisionFrames )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3StatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3StatsSingleCollisionFrames(
                 i4Dot3StatsIndex,
                 pu4Dot3StatsSingleCollisionFrames );

    return i4RetVal;


} /* NcDot3StatsSingleCollisionFramesGet */

/********************************************************************
* FUNCTION NcDot3StatsMultipleCollisionFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3StatsMultipleCollisionFramesGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsMultipleCollisionFrames )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3StatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3StatsMultipleCollisionFrames(
                 i4Dot3StatsIndex,
                 pu4Dot3StatsMultipleCollisionFrames );

    return i4RetVal;


} /* NcDot3StatsMultipleCollisionFramesGet */

/********************************************************************
* FUNCTION NcDot3StatsSQETestErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3StatsSQETestErrorsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsSQETestErrors )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3StatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3StatsSQETestErrors(
                 i4Dot3StatsIndex,
                 pu4Dot3StatsSQETestErrors );

    return i4RetVal;


} /* NcDot3StatsSQETestErrorsGet */

/********************************************************************
* FUNCTION NcDot3StatsDeferredTransmissionsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3StatsDeferredTransmissionsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsDeferredTransmissions )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3StatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3StatsDeferredTransmissions(
                 i4Dot3StatsIndex,
                 pu4Dot3StatsDeferredTransmissions );

    return i4RetVal;


} /* NcDot3StatsDeferredTransmissionsGet */

/********************************************************************
* FUNCTION NcDot3StatsLateCollisionsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3StatsLateCollisionsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsLateCollisions )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3StatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3StatsLateCollisions(
                 i4Dot3StatsIndex,
                 pu4Dot3StatsLateCollisions );

    return i4RetVal;


} /* NcDot3StatsLateCollisionsGet */

/********************************************************************
* FUNCTION NcDot3StatsExcessiveCollisionsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3StatsExcessiveCollisionsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsExcessiveCollisions )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3StatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3StatsExcessiveCollisions(
                 i4Dot3StatsIndex,
                 pu4Dot3StatsExcessiveCollisions );

    return i4RetVal;


} /* NcDot3StatsExcessiveCollisionsGet */

/********************************************************************
* FUNCTION NcDot3StatsInternalMacTransmitErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3StatsInternalMacTransmitErrorsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsInternalMacTransmitErrors )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3StatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3StatsInternalMacTransmitErrors(
                 i4Dot3StatsIndex,
                 pu4Dot3StatsInternalMacTransmitErrors );

    return i4RetVal;


} /* NcDot3StatsInternalMacTransmitErrorsGet */

/********************************************************************
* FUNCTION NcDot3StatsCarrierSenseErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3StatsCarrierSenseErrorsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsCarrierSenseErrors )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3StatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3StatsCarrierSenseErrors(
                 i4Dot3StatsIndex,
                 pu4Dot3StatsCarrierSenseErrors );

    return i4RetVal;


} /* NcDot3StatsCarrierSenseErrorsGet */

/********************************************************************
* FUNCTION NcDot3StatsFrameTooLongsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3StatsFrameTooLongsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsFrameTooLongs )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3StatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3StatsFrameTooLongs(
                 i4Dot3StatsIndex,
                 pu4Dot3StatsFrameTooLongs );

    return i4RetVal;


} /* NcDot3StatsFrameTooLongsGet */

/********************************************************************
* FUNCTION NcDot3StatsInternalMacReceiveErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3StatsInternalMacReceiveErrorsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsInternalMacReceiveErrors )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3StatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3StatsInternalMacReceiveErrors(
                 i4Dot3StatsIndex,
                 pu4Dot3StatsInternalMacReceiveErrors );

    return i4RetVal;


} /* NcDot3StatsInternalMacReceiveErrorsGet */

/********************************************************************
* FUNCTION NcDot3StatsEtherChipSetGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3StatsEtherChipSetGet (
                INT4 i4Dot3StatsIndex,
                UINT1 *pDot3StatsEtherChipSet )
{

    INT4 i4RetVal = SNMP_SUCCESS;
	UNUSED_PARAM (i4Dot3StatsIndex);
	UNUSED_PARAM (pDot3StatsEtherChipSet);
    return i4RetVal;


} /* NcDot3StatsEtherChipSetGet */

/********************************************************************
* FUNCTION NcDot3StatsSymbolErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3StatsSymbolErrorsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsSymbolErrors )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3StatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3StatsSymbolErrors(
                 i4Dot3StatsIndex,
                 pu4Dot3StatsSymbolErrors );

    return i4RetVal;


} /* NcDot3StatsSymbolErrorsGet */

/********************************************************************
* FUNCTION NcDot3StatsDuplexStatusGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3StatsDuplexStatusGet (
                INT4 i4Dot3StatsIndex,
                INT4 *pi4Dot3StatsDuplexStatus )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3StatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3StatsDuplexStatus(
                 i4Dot3StatsIndex,
                 pi4Dot3StatsDuplexStatus );

    return i4RetVal;


} /* NcDot3StatsDuplexStatusGet */

/********************************************************************
* FUNCTION NcDot3StatsRateControlAbilityGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3StatsRateControlAbilityGet (
                INT4 i4Dot3StatsIndex,
                INT4 *pi4Dot3StatsRateControlAbility )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3StatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3StatsRateControlAbility(
                 i4Dot3StatsIndex,
                 pi4Dot3StatsRateControlAbility );

    return i4RetVal;


} /* NcDot3StatsRateControlAbilityGet */

/********************************************************************
* FUNCTION NcDot3StatsRateControlStatusGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3StatsRateControlStatusGet (
                INT4 i4Dot3StatsIndex,
                INT4 *pi4Dot3StatsRateControlStatus )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3StatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3StatsRateControlStatus(
                 i4Dot3StatsIndex,
                 pi4Dot3StatsRateControlStatus );

    return i4RetVal;


} /* NcDot3StatsRateControlStatusGet */

/********************************************************************
* FUNCTION NcDot3CollFrequenciesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3CollFrequenciesGet (
                INT4 i4IfIndex,
                INT4 i4Dot3CollCount,
                UINT4 *pu4Dot3CollFrequencies )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3CollTable(
                 i4IfIndex,
                 i4Dot3CollCount) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3CollFrequencies(
                 i4IfIndex,
                 i4Dot3CollCount,
                 pu4Dot3CollFrequencies );

    return i4RetVal;


} /* NcDot3CollFrequenciesGet */

/********************************************************************
* FUNCTION NcDot3ControlFunctionsSupportedGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3ControlFunctionsSupportedGet (
                INT4 i4Dot3StatsIndex,
                UINT1	*pu1ControlFunctionsSupported )
{

    INT4 i4RetVal;
	tSNMP_OCTET_STRING_TYPE	Dot3ControlFunctionsSupported;
	UINT1	i1Result = 0;

	MEMSET (&Dot3ControlFunctionsSupported, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

	Dot3ControlFunctionsSupported.pu1_OctetList = &i1Result;

    if (nmhValidateIndexInstanceDot3ControlTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3ControlFunctionsSupported(
                 i4Dot3StatsIndex,
                 &Dot3ControlFunctionsSupported);

	MEMCPY (pu1ControlFunctionsSupported, Dot3ControlFunctionsSupported.pu1_OctetList, 1);
    return i4RetVal;


} /* NcDot3ControlFunctionsSupportedGet */

/********************************************************************
* FUNCTION NcDot3ControlInUnknownOpcodesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3ControlInUnknownOpcodesGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3ControlInUnknownOpcodes )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3ControlTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3ControlInUnknownOpcodes(
                 i4Dot3StatsIndex,
                 pu4Dot3ControlInUnknownOpcodes );

    return i4RetVal;


} /* NcDot3ControlInUnknownOpcodesGet */

/********************************************************************
* FUNCTION NcDot3HCControlInUnknownOpcodesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3HCControlInUnknownOpcodesGet (
                INT4 i4Dot3StatsIndex,
                unsigned long long *pu8Dot3HCControlInUnknownOpcodes )
{

    INT4 i4RetVal;
	tSNMP_COUNTER64_TYPE Dot3HCControlInUnknownOpcodes;

	MEMSET (&Dot3HCControlInUnknownOpcodes, 0, sizeof(tSNMP_COUNTER64_TYPE));

    if (nmhValidateIndexInstanceDot3ControlTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3HCControlInUnknownOpcodes(
                 i4Dot3StatsIndex,
                 &Dot3HCControlInUnknownOpcodes);

	*pu8Dot3HCControlInUnknownOpcodes = 
			((*pu8Dot3HCControlInUnknownOpcodes | Dot3HCControlInUnknownOpcodes.msn) << 32) | Dot3HCControlInUnknownOpcodes.lsn;

    return i4RetVal;


} /* NcDot3HCControlInUnknownOpcodesGet */

/********************************************************************
* FUNCTION NcDot3PauseAdminModeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3PauseAdminModeSet (
                INT4 i4Dot3StatsIndex,
                INT4 i4Dot3PauseAdminMode )
{

    INT4 i4RetVal;

    i4RetVal = nmhSetDot3PauseAdminMode(
                 i4Dot3StatsIndex,
                i4Dot3PauseAdminMode);

    return i4RetVal;


} /* NcDot3PauseAdminModeSet */

/********************************************************************
* FUNCTION NcDot3PauseAdminModeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3PauseAdminModeTest (UINT4 *pu4Error,
                INT4 i4Dot3StatsIndex,
                INT4 i4Dot3PauseAdminMode )
{

    INT4 i4RetVal;

    i4RetVal = nmhTestv2Dot3PauseAdminMode(pu4Error,
                 i4Dot3StatsIndex,
                i4Dot3PauseAdminMode);

    return i4RetVal;


} /* NcDot3PauseAdminModeTest */

/********************************************************************
* FUNCTION NcDot3PauseOperModeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3PauseOperModeGet (
                INT4 i4Dot3StatsIndex,
                INT4 *pi4Dot3PauseOperMode )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3PauseTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3PauseOperMode(
                 i4Dot3StatsIndex,
                 pi4Dot3PauseOperMode );

    return i4RetVal;


} /* NcDot3PauseOperModeGet */

/********************************************************************
* FUNCTION NcDot3InPauseFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3InPauseFramesGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3InPauseFrames )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3PauseTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3InPauseFrames(
                 i4Dot3StatsIndex,
                 pu4Dot3InPauseFrames );

    return i4RetVal;


} /* NcDot3InPauseFramesGet */

/********************************************************************
* FUNCTION NcDot3OutPauseFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3OutPauseFramesGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3OutPauseFrames )
{

    INT4 i4RetVal;

    if (nmhValidateIndexInstanceDot3PauseTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3OutPauseFrames(
                 i4Dot3StatsIndex,
                 pu4Dot3OutPauseFrames );

    return i4RetVal;


} /* NcDot3OutPauseFramesGet */

/********************************************************************
* FUNCTION NcDot3HCInPauseFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3HCInPauseFramesGet (
                INT4 i4Dot3StatsIndex,
                unsigned long long *pu8Dot3HCInPauseFrames )
{

    INT4 i4RetVal;
	tSNMP_COUNTER64_TYPE	Dot3HCInPauseFrames;

	MEMSET (&Dot3HCInPauseFrames, 0, sizeof(tSNMP_COUNTER64_TYPE));

    if (nmhValidateIndexInstanceDot3PauseTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3HCInPauseFrames(
                 i4Dot3StatsIndex,
                 &Dot3HCInPauseFrames);

    *pu8Dot3HCInPauseFrames = ((*pu8Dot3HCInPauseFrames | Dot3HCInPauseFrames.msn) << 32) | Dot3HCInPauseFrames.lsn;


    return i4RetVal;


} /* NcDot3HCInPauseFramesGet */

/********************************************************************
* FUNCTION NcDot3HCOutPauseFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3HCOutPauseFramesGet (
                INT4 i4Dot3StatsIndex,
                unsigned long long *pu8Dot3HCOutPauseFrames )
{

    INT4 i4RetVal;
	tSNMP_COUNTER64_TYPE	Dot3HCOutPauseFrames;

	MEMSET (&Dot3HCOutPauseFrames, 0, sizeof(tSNMP_COUNTER64_TYPE));

    if (nmhValidateIndexInstanceDot3PauseTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3HCOutPauseFrames(
                 i4Dot3StatsIndex,
                 &Dot3HCOutPauseFrames);

    *pu8Dot3HCOutPauseFrames = ((*pu8Dot3HCOutPauseFrames | Dot3HCOutPauseFrames.msn) << 32) | Dot3HCOutPauseFrames.lsn;

    return i4RetVal;


} /* NcDot3HCOutPauseFramesGet */

/********************************************************************
* FUNCTION NcDot3HCStatsAlignmentErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3HCStatsAlignmentErrorsGet (
                INT4 i4Dot3StatsIndex,
                unsigned long long *pu8Dot3HCStatsAlignmentErrors )
{

    INT4 i4RetVal;
	tSNMP_COUNTER64_TYPE	Dot3HCStatsAlignmentErrors;

	MEMSET (&Dot3HCStatsAlignmentErrors, 0, sizeof(tSNMP_COUNTER64_TYPE));

    if (nmhValidateIndexInstanceDot3HCStatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3HCStatsAlignmentErrors(
                 i4Dot3StatsIndex,
                 &Dot3HCStatsAlignmentErrors);

    *pu8Dot3HCStatsAlignmentErrors = 
			((*pu8Dot3HCStatsAlignmentErrors | Dot3HCStatsAlignmentErrors.msn) << 32) | Dot3HCStatsAlignmentErrors.lsn;

    return i4RetVal;


} /* NcDot3HCStatsAlignmentErrorsGet */

/********************************************************************
* FUNCTION NcDot3HCStatsFCSErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3HCStatsFCSErrorsGet (
                INT4 i4Dot3StatsIndex,
                unsigned long long *pu8Dot3HCStatsFCSErrors )
{

    INT4 i4RetVal;
	tSNMP_COUNTER64_TYPE	Dot3HCStatsFCSErrors;

	MEMSET (&Dot3HCStatsFCSErrors, 0, sizeof(tSNMP_COUNTER64_TYPE));

    if (nmhValidateIndexInstanceDot3HCStatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3HCStatsFCSErrors(
                 i4Dot3StatsIndex,
                 &Dot3HCStatsFCSErrors);

    *pu8Dot3HCStatsFCSErrors = ((*pu8Dot3HCStatsFCSErrors | Dot3HCStatsFCSErrors.msn) << 32) | Dot3HCStatsFCSErrors.lsn;

    return i4RetVal;


} /* NcDot3HCStatsFCSErrorsGet */

/********************************************************************
* FUNCTION NcDot3HCStatsInternalMacTransmitErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3HCStatsInternalMacTransmitErrorsGet (
                INT4 i4Dot3StatsIndex,
                unsigned long long *pu8Dot3HCStatsInternalMacTransmitErrors )
{

    INT4 i4RetVal;
	tSNMP_COUNTER64_TYPE	Dot3HCStatsInternalMacTransmitErrors;

	MEMSET (&Dot3HCStatsInternalMacTransmitErrors, 0, sizeof(tSNMP_COUNTER64_TYPE));

    if (nmhValidateIndexInstanceDot3HCStatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3HCStatsInternalMacTransmitErrors(
                 i4Dot3StatsIndex,
                 &Dot3HCStatsInternalMacTransmitErrors);

    *pu8Dot3HCStatsInternalMacTransmitErrors = 
			((*pu8Dot3HCStatsInternalMacTransmitErrors | Dot3HCStatsInternalMacTransmitErrors.msn) << 32) | Dot3HCStatsInternalMacTransmitErrors.lsn;

    return i4RetVal;


} /* NcDot3HCStatsInternalMacTransmitErrorsGet */

/********************************************************************
* FUNCTION NcDot3HCStatsFrameTooLongsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3HCStatsFrameTooLongsGet (
                INT4 i4Dot3StatsIndex,
                unsigned long long *pu8Dot3HCStatsFrameTooLongs )
{

    INT4 i4RetVal;
	tSNMP_COUNTER64_TYPE	Dot3HCStatsFrameTooLongs;

	MEMSET (&Dot3HCStatsFrameTooLongs, 0, sizeof(tSNMP_COUNTER64_TYPE));

    if (nmhValidateIndexInstanceDot3HCStatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3HCStatsFrameTooLongs(
                 i4Dot3StatsIndex,
                 &Dot3HCStatsFrameTooLongs);

    *pu8Dot3HCStatsFrameTooLongs = ((*pu8Dot3HCStatsFrameTooLongs | Dot3HCStatsFrameTooLongs.msn) << 32) | Dot3HCStatsFrameTooLongs.lsn;

    return i4RetVal;


} /* NcDot3HCStatsFrameTooLongsGet */

/********************************************************************
* FUNCTION NcDot3HCStatsInternalMacReceiveErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3HCStatsInternalMacReceiveErrorsGet (
                INT4 i4Dot3StatsIndex,
                unsigned long long *pu8Dot3HCStatsInternalMacReceiveErrors )
{

    INT4 i4RetVal;
	tSNMP_COUNTER64_TYPE	Dot3HCStatsInternalMacReceiveErrors;

	MEMSET (&Dot3HCStatsInternalMacReceiveErrors, 0, sizeof(tSNMP_COUNTER64_TYPE));

    if (nmhValidateIndexInstanceDot3HCStatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3HCStatsInternalMacReceiveErrors(
                 i4Dot3StatsIndex,
                 &Dot3HCStatsInternalMacReceiveErrors);

    *pu8Dot3HCStatsInternalMacReceiveErrors = ((*pu8Dot3HCStatsInternalMacReceiveErrors | Dot3HCStatsInternalMacReceiveErrors.msn) << 32) | Dot3HCStatsInternalMacReceiveErrors.lsn;

    return i4RetVal;


} /* NcDot3HCStatsInternalMacReceiveErrorsGet */

/********************************************************************
* FUNCTION NcDot3HCStatsSymbolErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT4 NcDot3HCStatsSymbolErrorsGet (
                INT4 i4Dot3StatsIndex,
                unsigned long long *pu8Dot3HCStatsSymbolErrors )
{

    INT4 i4RetVal;
	tSNMP_COUNTER64_TYPE	Dot3HCStatsSymbolErrors;

	MEMSET (&Dot3HCStatsSymbolErrors, 0, sizeof(tSNMP_COUNTER64_TYPE));

    if (nmhValidateIndexInstanceDot3HCStatsTable(
                 i4Dot3StatsIndex) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i4RetVal = nmhGetDot3HCStatsSymbolErrors(
                 i4Dot3StatsIndex,
                 &Dot3HCStatsSymbolErrors);

    *pu8Dot3HCStatsSymbolErrors = ((*pu8Dot3HCStatsSymbolErrors | Dot3HCStatsSymbolErrors.msn) << 32) | Dot3HCStatsSymbolErrors.lsn;
	
    return i4RetVal;


} /* NcDot3HCStatsSymbolErrorsGet */

/* END i_EtherLike_MIB.c */
