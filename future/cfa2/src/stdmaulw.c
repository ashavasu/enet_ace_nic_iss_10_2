/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdmaulw.c,v 1.28 
 *
 * Description: This file contains the low level routines for the
 *              MAU MIB (RFC 2668).
 *
 *******************************************************************/

#include "cfainc.h"
#include "ifmibcli.h"
#include "fsisscli.h"

                      /*  Base Oids of all the MauType Values */
UINT4               au4IfMauType[] = { 1, 3, 6, 1, 2, 1, 26, 4 };
UINT4               au4IfMauDefaultType[] = { 1, 3, 6, 1, 2, 1, 26, 4 };    /* Base Oids of all the MauType Values */

/* LOW LEVEL Routines for Table : IfMauTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIfMauTable
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT4
nmhValidateIndexInstanceIfMauTable (INT4 i4ifMauIfIndex, INT4 i4ifMauIndex)
{
    UINT2               u2IfIndex;
    UINT1               u1IfType;

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0))
    {
        return (SNMP_FAILURE);
    }

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET))
    {
        if ((i4ifMauIndex > 0) && (i4ifMauIndex <= MAU_MAX_IF_COUNT))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetIfMauType
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauType (INT4 i4ifMauIfIndex,
                 INT4 i4ifMauIndex, tSNMP_OID_TYPE * pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    UINT4               u4Val = 2;

    if (pElement == NULL)
    {
        return SNMP_FAILURE;
    }

    pElement->u4_Length = 2;
    pElement->pu4_OidList[0] = 0;
    pElement->pu4_OidList[1] = 0;
#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4ifMauIfIndex) != CFA_TRUE)
    {
        if ((i4RetVal != SNMP_FAILURE) && (u4Val != 0))
        {
            MEMCPY (pElement->pu4_OidList, au4IfMauType,
                    sizeof (UINT4) * MAU_TYPE_LEN);
            pElement->pu4_OidList[MAU_TYPE_LEN] = u4Val;
            pElement->u4_Length = MAU_TYPE_LEN + 1;
        }

        return SNMP_SUCCESS;
    }

    if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
    {
        if (CfaFsMauHwGetMauType ((UINT4) i4ifMauIfIndex,
                                  i4ifMauIndex, &u4Val) != FNP_SUCCESS)
        {
            i4RetVal = SNMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);
#endif
    if ((i4RetVal != SNMP_FAILURE) && (u4Val != 0))
    {
        MEMCPY (pElement->pu4_OidList, au4IfMauType,
                sizeof (UINT4) * MAU_TYPE_LEN);
        pElement->pu4_OidList[MAU_TYPE_LEN] = u4Val;
        pElement->u4_Length = MAU_TYPE_LEN + 1;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2IfMauStatus
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                testValIfMauStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhTestv2IfMauStatus (UINT4 *pu4Error,
                      INT4 i4ifMauIfIndex, INT4 i4ifMauIndex, INT4 *pElement)
{
    UINT2               u2IfIndex;
    INT4                i4Val = *pElement;
    INT4                i4RetVal = SNMP_SUCCESS;
    UINT1               u1IfType;

    UNUSED_PARAM (pElement);

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0))
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET))
    {
        if ((i4ifMauIndex > 0) && (i4ifMauIndex <= MAU_MAX_IF_COUNT))
        {
            if ((i4Val >= MAU_STATUS_OTHER) && (i4Val <= MAU_STATUS_RESET))
            {
                *pu4Error = 0;
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4Error = SNMP_ERR_WRONG_VALUE;
                i4RetVal = SNMP_FAILURE;
            }
        }
        else
        {
            *pu4Error = SNMP_ERR_NO_CREATION;
            i4RetVal = SNMP_FAILURE;
        }
    }
    else
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        i4RetVal = SNMP_FAILURE;
    }
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhSetIfMauStatus
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                setValIfMauStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhSetIfMauStatus (INT4 i4ifMauIfIndex, INT4 i4ifMauIndex, INT4 i4Element)
{
    UINT2               u2IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif
    INT4                i4RetVal = SNMP_SUCCESS;

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0) ||
        (i4ifMauIndex > MAU_MAX_IF_COUNT))
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET) &&
        (CfaIsMgmtPort ((UINT4) u2IfIndex) == FALSE))
    {
        if (i4Element == MAU_STATUS_RESET)
        {
            if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
            {
                if (CfaFsMauHwSetMauStatus (i4ifMauIfIndex, i4ifMauIndex,
                                            &i4Element) != FNP_SUCCESS)
                {
                    i4RetVal = SNMP_FAILURE;
                }
            }
        }
        else
        {
            switch (i4Element)
            {
                case MAU_STATUS_OPERATIONAL:
                    i4RetVal = nmhSetIfAdminStatus (i4ifMauIfIndex, CFA_IF_UP);
                    break;
                case MAU_STATUS_SHUTDOWN:
                    i4RetVal =
                        nmhSetIfAdminStatus (i4ifMauIfIndex, CFA_IF_DOWN);
                    break;
                default:
                    i4RetVal = SNMP_FAILURE;
            }

        }
    }
#endif
    CfaCdbSetIfMauStatus ((UINT4) i4ifMauIfIndex, i4Element);
    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIfMauStatus
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauStatus (INT4 i4ifMauIfIndex, INT4 i4ifMauIndex, INT4 *pElement)
{
    UINT2               u2IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif
    INT4                i4RetVal = SNMP_SUCCESS;
    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0) ||
        (i4ifMauIndex > MAU_MAX_IF_COUNT))
    {
        return SNMP_FAILURE;
    }

    *pElement = MAU_STATUS_UNKNOWN;
#ifdef NPAPI_WANTED
    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET)
        && (CfaIsMgmtPort ((UINT4) u2IfIndex) == FALSE))
    {
        if (nmhGetIfAdminStatus (i4ifMauIfIndex, pElement) == SNMP_SUCCESS)
        {
            switch (*pElement)
            {
                case CFA_IF_UP:
                    *pElement = MAU_STATUS_OPERATIONAL;
                    i4RetVal = SNMP_SUCCESS;
                    break;
                case CFA_IF_DOWN:
                    *pElement = MAU_STATUS_SHUTDOWN;
                    i4RetVal = SNMP_SUCCESS;
                    break;
                default:
                    *pElement = MAU_STATUS_UNKNOWN;
                    i4RetVal = SNMP_SUCCESS;
                    break;
            }

        }
        else
        {
            i4RetVal = SNMP_FAILURE;
        }

    }
#endif
    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetIfMauMediaAvailable
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauMediaAvailable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauMediaAvailable (INT4 i4ifMauIfIndex,
                           INT4 i4ifMauIndex, INT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    *pElement = MAU_MEDIA_UNKNOWN;
#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4ifMauIfIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

    if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
    {
        if (CfaFsMauHwGetMediaAvailable ((UINT4) i4ifMauIfIndex,
                                         i4ifMauIndex, pElement) != FNP_SUCCESS)
        {
            i4RetVal = SNMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetIfMauMediaAvailableStateExits
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauMediaAvailableStateExits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauMediaAvailableStateExits (INT4 i4ifMauIfIndex,
                                     INT4 i4ifMauIndex, UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    *pElement = 0;
#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4ifMauIfIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

    if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
    {
        if (CfaFsMauHwGetMediaAvailStateExits ((UINT4) i4ifMauIfIndex,
                                               i4ifMauIndex,
                                               pElement) != FNP_SUCCESS)
        {
            i4RetVal = SNMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetIfMauJabberState
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauJabberState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauJabberState (INT4 i4ifMauIfIndex, INT4 i4ifMauIndex, INT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    *pElement = MAU_JABBER_UNKNOWN;
#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4ifMauIfIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

    if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
    {
        if (CfaFsMauHwGetJabberState ((UINT4) i4ifMauIfIndex,
                                      i4ifMauIndex, pElement) != FNP_SUCCESS)
        {
            i4RetVal = SNMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetIfMauJabberingStateEnters
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauJabberingStateEnters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauJabberingStateEnters (INT4 i4ifMauIfIndex,
                                 INT4 i4ifMauIndex, UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    *pElement = 0;
#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4ifMauIfIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

    if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
    {
        if (CfaFsMauHwGetJabberingStateEnters ((UINT4) i4ifMauIfIndex,
                                               i4ifMauIndex,
                                               pElement) != FNP_SUCCESS)
        {
            i4RetVal = SNMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetIfMauFalseCarriers
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauFalseCarriers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauFalseCarriers (INT4 i4ifMauIfIndex,
                          INT4 i4ifMauIndex, UINT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    *pElement = 0;
#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4ifMauIfIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

    if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
    {
        if (CfaFsMauHwGetFalseCarriers ((UINT4) i4ifMauIfIndex,
                                        i4ifMauIndex, pElement) != FNP_SUCCESS)
        {
            i4RetVal = SNMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetIfMauTypeList
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauTypeList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauTypeList (INT4 i4ifMauIfIndex, INT4 i4ifMauIndex, INT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);

    *pElement = 0;

    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2IfMauDefaultType
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                testValIfMauDefaultType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhTestv2IfMauDefaultType (UINT4 *pu4Error,
                           INT4 i4ifMauIfIndex,
                           INT4 i4ifMauIndex, tSNMP_OID_TYPE * pElement)
{
    UINT2               u2IfIndex;
    INT4                i4RetVal = SNMP_SUCCESS;
    UINT1               u1IfType;

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0)
        || (pElement == NULL))
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET))
    {
        if ((i4ifMauIndex > 0) && (i4ifMauIndex <= MAU_MAX_IF_COUNT))
        {
            if ((pElement->u4_Length == MAU_TYPE_LEN + 1) &&
                (MEMCMP (pElement->pu4_OidList, au4IfMauDefaultType,
                         sizeof (UINT4) * MAU_TYPE_LEN) == 0) &&
                ((pElement->pu4_OidList[MAU_TYPE_LEN] >= MAU_TYPE_AUI) &&
                 (pElement->pu4_OidList[MAU_TYPE_LEN] <=
                  MAU_TYPE_10GIG_BASE_X)))
            {
                *pu4Error = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4Error = SNMP_ERR_WRONG_VALUE;
                i4RetVal = SNMP_FAILURE;
            }
        }
        else
        {
            *pu4Error = SNMP_ERR_NO_CREATION;
            i4RetVal = SNMP_FAILURE;
        }
    }
    else
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        i4RetVal = SNMP_FAILURE;
    }
    return (i4RetVal);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IfMauTable
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfMauTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfMauDefaultType
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                setValIfMauDefaultType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhSetIfMauDefaultType (INT4 i4ifMauIfIndex,
                        INT4 i4ifMauIndex, tSNMP_OID_TYPE * pElement)
{
    UINT4               u4Val = 0;
    INT4                i4RetVal = SNMP_SUCCESS;
    UINT2               u2IfIndex;
#ifdef NPAPI_WANTED
    INT4                i4AutoNegVal = 0;
    UINT1               u1IfType;
#endif

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0) ||
        (i4ifMauIndex > MAU_MAX_IF_COUNT))
    {
        return SNMP_FAILURE;
    }
    if (pElement == NULL)
    {
        return SNMP_FAILURE;
    }

    /* ifMauDefaultType is not configured. Hence no need to set anything */
    if (pElement->u4_Length != MAU_TYPE_LEN + 1)
    {
        return SNMP_FAILURE;
    }

    u4Val = pElement->pu4_OidList[MAU_TYPE_LEN];
#ifdef NPAPI_WANTED
    nmhGetIssPortCtrlMode (i4ifMauIfIndex, &i4AutoNegVal);
    if (i4AutoNegVal == ISS_NONEGOTIATION)
    {
        CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

        if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET)
            && (CfaIsMgmtPort ((UINT4) u2IfIndex) == FALSE))
        {
            if (nmhSetIssPortCtrlMode (i4ifMauIfIndex, ISS_NONEGOTIATION)
                != SNMP_FAILURE)
            {
                switch (u4Val)
                {
                    case MAU_TYPE_10_BASE_TFD:
                        if ((nmhSetIssPortCtrlDuplex
                             (i4ifMauIfIndex, ISS_FULLDUP) != SNMP_FAILURE)
                            &&
                            (nmhSetIssPortCtrlSpeed (i4ifMauIfIndex, ISS_10MBPS)
                             != SNMP_FAILURE))
                        {
                            i4RetVal = SNMP_SUCCESS;
                        }
                        else
                        {
                            i4RetVal = SNMP_FAILURE;
                        }
                        break;
                    case MAU_TYPE_10_BASE_THD:
                        if ((nmhSetIssPortCtrlDuplex
                             (i4ifMauIfIndex, ISS_HALFDUP) != SNMP_FAILURE)
                            &&
                            (nmhSetIssPortCtrlSpeed (i4ifMauIfIndex, ISS_10MBPS)
                             != SNMP_FAILURE))
                        {
                            i4RetVal = SNMP_SUCCESS;
                        }
                        else
                        {
                            i4RetVal = SNMP_FAILURE;
                        }
                        break;
                    case MAU_TYPE_100_BASE_TXFD:
                        if ((nmhSetIssPortCtrlDuplex
                             (i4ifMauIfIndex, ISS_FULLDUP) != SNMP_FAILURE)
                            &&
                            (nmhSetIssPortCtrlSpeed
                             (i4ifMauIfIndex, ISS_100MBPS) != SNMP_FAILURE))
                        {
                            i4RetVal = SNMP_SUCCESS;
                        }
                        else
                        {
                            i4RetVal = SNMP_FAILURE;
                        }
                        break;
                    case MAU_TYPE_100_BASE_TXHD:
                        if ((nmhSetIssPortCtrlDuplex
                             (i4ifMauIfIndex, ISS_HALFDUP) != SNMP_FAILURE)
                            &&
                            (nmhSetIssPortCtrlSpeed
                             (i4ifMauIfIndex, ISS_100MBPS) != SNMP_FAILURE))
                        {
                            i4RetVal = SNMP_SUCCESS;
                        }
                        else
                        {
                            i4RetVal = SNMP_FAILURE;
                        }
                        break;
                    case MAU_TYPE_1000_BASE_TFD:
                        if ((nmhSetIssPortCtrlDuplex
                             (i4ifMauIfIndex, ISS_FULLDUP) != SNMP_FAILURE)
                            && (nmhSetIssPortCtrlSpeed (i4ifMauIfIndex, ISS_1GB)
                                != SNMP_FAILURE))
                        {
                            i4RetVal = SNMP_SUCCESS;
                        }
                        else
                        {
                            i4RetVal = SNMP_FAILURE;
                        }
                        break;
                    case MAU_TYPE_1000_BASE_THD:
                        if ((nmhSetIssPortCtrlDuplex
                             (i4ifMauIfIndex, ISS_HALFDUP) != SNMP_FAILURE)
                            && (nmhSetIssPortCtrlSpeed (i4ifMauIfIndex, ISS_1GB)
                                != SNMP_FAILURE))
                        {
                            i4RetVal = SNMP_SUCCESS;
                        }
                        else
                        {
                            i4RetVal = SNMP_FAILURE;
                        }
                        break;
                    case MAU_TYPE_10GIG_BASE_X:
                        if ((nmhSetIssPortCtrlDuplex
                             (i4ifMauIfIndex, ISS_FULLDUP) != SNMP_FAILURE)
                            &&
                            (nmhSetIssPortCtrlSpeed (i4ifMauIfIndex, ISS_10GB)
                             != SNMP_FAILURE))
                        {
                            i4RetVal = SNMP_SUCCESS;
                        }
                        else
                        {
                            i4RetVal = SNMP_FAILURE;
                        }
                        break;
                    case MAU_TYPE_2500_BASE_THD:
                        if ((nmhSetIssPortCtrlDuplex
                             (i4ifMauIfIndex, ISS_HALFDUP) != SNMP_FAILURE)
                            &&
                            (nmhSetIssPortCtrlSpeed
                             (i4ifMauIfIndex, ISS_2500MBPS) != SNMP_FAILURE))
                        {
                            i4RetVal = SNMP_SUCCESS;
                        }
                        else
                        {
                            i4RetVal = SNMP_FAILURE;
                        }
                        break;

                    case MAU_TYPE_2500_BASE_TFD:
                        if ((nmhSetIssPortCtrlDuplex
                             (i4ifMauIfIndex, ISS_FULLDUP) != SNMP_FAILURE)
                            &&
                            (nmhSetIssPortCtrlSpeed
                             (i4ifMauIfIndex, ISS_2500MBPS) != SNMP_FAILURE))
                        {
                            i4RetVal = SNMP_SUCCESS;
                        }
                        else
                        {
                            i4RetVal = SNMP_FAILURE;
                        }
                        break;
                    default:
                        i4RetVal = SNMP_FAILURE;

                }
            }
            else
            {
                i4RetVal = SNMP_FAILURE;
            }

        }
    }
    else if (i4AutoNegVal != ISS_NONEGOTIATION)
    {
        switch (u4Val)
        {
            case MAU_TYPE_10_BASE_TFD:
                if ((IssSetPortCtrlDuplex (i4ifMauIfIndex, ISS_FULLDUP)
                     != SNMP_FAILURE) &&
                    (IssSetPortCtrlSpeed (i4ifMauIfIndex, ISS_10MBPS)
                     != SNMP_FAILURE))
                {
                    i4RetVal = SNMP_SUCCESS;
                }
                else
                {
                    i4RetVal = SNMP_FAILURE;
                }
                break;
            case MAU_TYPE_10_BASE_THD:
                if ((IssSetPortCtrlDuplex (i4ifMauIfIndex, ISS_HALFDUP)
                     != SNMP_FAILURE) &&
                    (IssSetPortCtrlSpeed (i4ifMauIfIndex, ISS_10MBPS)
                     != SNMP_FAILURE))
                {
                    i4RetVal = SNMP_SUCCESS;
                }
                else
                {
                    i4RetVal = SNMP_FAILURE;
                }
                break;
            case MAU_TYPE_100_BASE_TXFD:
                if ((IssSetPortCtrlDuplex (i4ifMauIfIndex, ISS_FULLDUP)
                     != SNMP_FAILURE) &&
                    (IssSetPortCtrlSpeed (i4ifMauIfIndex, ISS_100MBPS)
                     != SNMP_FAILURE))
                {
                    i4RetVal = SNMP_SUCCESS;
                }
                else
                {
                    i4RetVal = SNMP_FAILURE;
                }
                break;
            case MAU_TYPE_100_BASE_TXHD:
                if ((IssSetPortCtrlDuplex (i4ifMauIfIndex, ISS_HALFDUP)
                     != SNMP_FAILURE) &&
                    (IssSetPortCtrlSpeed (i4ifMauIfIndex, ISS_100MBPS)
                     != SNMP_FAILURE))
                {
                    i4RetVal = SNMP_SUCCESS;
                }
                else
                {
                    i4RetVal = SNMP_FAILURE;
                }
                break;
            case MAU_TYPE_1000_BASE_TFD:
                if ((IssSetPortCtrlDuplex (i4ifMauIfIndex, ISS_FULLDUP)
                     != SNMP_FAILURE) &&
                    (IssSetPortCtrlSpeed (i4ifMauIfIndex, ISS_1GB)
                     != SNMP_FAILURE))
                {
                    i4RetVal = SNMP_SUCCESS;
                }
                else
                {
                    i4RetVal = SNMP_FAILURE;
                }
                break;
            case MAU_TYPE_1000_BASE_THD:
                if ((IssSetPortCtrlDuplex (i4ifMauIfIndex, ISS_HALFDUP)
                     != SNMP_FAILURE) &&
                    (IssSetPortCtrlSpeed (i4ifMauIfIndex, ISS_1GB)
                     != SNMP_FAILURE))
                {
                    i4RetVal = SNMP_SUCCESS;
                }
                else
                {
                    i4RetVal = SNMP_FAILURE;
                }
                break;
            case MAU_TYPE_2500_BASE_THD:
                if ((IssSetPortCtrlDuplex (i4ifMauIfIndex, ISS_HALFDUP)
                     != SNMP_FAILURE) &&
                    (IssSetPortCtrlSpeed (i4ifMauIfIndex, ISS_2500MBPS)
                     != SNMP_FAILURE))
                {
                    i4RetVal = SNMP_SUCCESS;
                }
                else
                {
                    i4RetVal = SNMP_FAILURE;
                }
                break;
            case MAU_TYPE_2500_BASE_TFD:
                if ((IssSetPortCtrlDuplex (i4ifMauIfIndex, ISS_FULLDUP)
                     != SNMP_FAILURE) &&
                    (IssSetPortCtrlSpeed (i4ifMauIfIndex, ISS_2500MBPS)
                     != SNMP_FAILURE))
                {
                    i4RetVal = SNMP_SUCCESS;
                }
                else
                {
                    i4RetVal = SNMP_FAILURE;
                }
                break;
            case MAU_TYPE_10GIG_BASE_X:
                if ((IssSetPortCtrlDuplex (i4ifMauIfIndex, ISS_FULLDUP)
                     != SNMP_FAILURE) &&
                    (IssSetPortCtrlSpeed (i4ifMauIfIndex, ISS_10GB)
                     != SNMP_FAILURE))
                {
                    i4RetVal = SNMP_SUCCESS;
                }
                else
                {
                    i4RetVal = SNMP_FAILURE;
                }
                break;
            default:
                i4RetVal = SNMP_FAILURE;

        }
    }
    else
    {

        return SNMP_FAILURE;
    }
#endif
    CfaCdbSetIfMauDefaultType ((UINT4) i4ifMauIfIndex, u4Val);

    return ((INT1) i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetIfMauDefaultType
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauDefaultType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauDefaultType (INT4 i4ifMauIfIndex,
                        INT4 i4ifMauIndex, tSNMP_OID_TYPE * pElement)
{
    UINT2               u2IfIndex;
    UINT4               u4Val = 0;
    INT4                i4RetVal = SNMP_SUCCESS;

#ifdef NPAPI_WANTED
    UINT1               u1IfType;
    INT4                i4PortSpeed = 0;
    INT4                i4PortDuplex = 0;
#endif

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0) ||
        (i4ifMauIndex > MAU_MAX_IF_COUNT))
    {
        return SNMP_FAILURE;
    }
    if (pElement == NULL)
    {
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET)
        && (CfaIsMgmtPort ((UINT4) u2IfIndex) == FALSE))
    {
        if ((nmhGetIssPortCtrlSpeed (i4ifMauIfIndex, &i4PortSpeed)
             != SNMP_FAILURE) &&
            (nmhGetIssPortCtrlDuplex (i4ifMauIfIndex, &i4PortDuplex)
             != SNMP_FAILURE))
        {
            switch (i4PortSpeed)
            {
                case ISS_10MBPS:
                    if (i4PortDuplex == ISS_FULLDUP)
                    {
                        u4Val = MAU_TYPE_10_BASE_TFD;
                    }
                    else
                    {
                        u4Val = MAU_TYPE_10_BASE_THD;
                    }
                    break;
                case ISS_100MBPS:
                    if (i4PortDuplex == ISS_FULLDUP)
                    {
                        u4Val = MAU_TYPE_100_BASE_TXFD;
                    }
                    else
                    {
                        u4Val = MAU_TYPE_100_BASE_TXHD;
                    }
                    break;
                case ISS_1GB:
                    if (i4PortDuplex == ISS_FULLDUP)
                    {
                        u4Val = MAU_TYPE_1000_BASE_TFD;
                    }
                    else
                    {
                        u4Val = MAU_TYPE_1000_BASE_THD;
                    }
                    break;
                case ISS_2500MBPS:
                    if (i4PortDuplex == ISS_FULLDUP)
                    {
                        u4Val = MAU_TYPE_2500_BASE_TFD;
                    }
                    else
                    {
                        u4Val = MAU_TYPE_2500_BASE_THD;
                    }
                    break;
                case ISS_10GB:
                    if (i4PortDuplex == ISS_FULLDUP)
                    {
                        u4Val = MAU_TYPE_10GIG_BASE_X;
                    }
                    break;
                default:
                    pElement->u4_Length = 2;
                    pElement->pu4_OidList[0] = 0;
                    pElement->pu4_OidList[1] = 0;
                    return SNMP_SUCCESS;
            }

        }
        else
        {
            i4RetVal = SNMP_FAILURE;
        }

    }
#else
    u4Val = 2;
#endif

    if ((i4RetVal != SNMP_FAILURE) && (u4Val != 0))
    {
        MEMCPY (pElement->pu4_OidList, au4IfMauDefaultType,
                sizeof (UINT4) * MAU_TYPE_LEN);
        pElement->pu4_OidList[MAU_TYPE_LEN] = u4Val;
        pElement->u4_Length = MAU_TYPE_LEN + 1;
    }
    else
    {
#ifdef NPAPI_WANTED
        pElement->u4_Length = 2;
        pElement->pu4_OidList[0] = 0;
        pElement->pu4_OidList[1] = 0;
#endif
    }
    return ((INT1) i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetIfMauAutoNegSupported
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauAutoNegSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauAutoNegSupported (INT4 i4ifMauIfIndex,
                             INT4 i4ifMauIndex, INT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    *pElement = CFA_DISABLED;
#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4ifMauIfIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

    if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
    {
        if (CfaFsMauHwGetAutoNegSupported ((UINT4) i4ifMauIfIndex,
                                           i4ifMauIndex,
                                           pElement) != FNP_SUCCESS)
        {
            i4RetVal = SNMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetIfMauTypeListBits
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauTypeListBits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauTypeListBits (INT4 i4ifMauIfIndex, INT4 i4ifMauIndex,
                         tSNMP_OCTET_STRING_TYPE * pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4Result = 0;

#ifdef NPAPI_WANTED
    INT4                i4Value = 0;

    pElement->i4_Length = MAU_TYPE_LIST_LEN;
    if (CfaIsPhysicalInterface ((UINT4) i4ifMauIfIndex) != CFA_TRUE)
    {
        pElement->pu1_OctetList[0] = (UINT1) i4Result;
        return SNMP_SUCCESS;
    }

    if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
    {
        if (CfaFsMauHwGetTypeListBits ((UINT4) i4ifMauIfIndex,
                                       i4ifMauIndex, &i4Result) != FNP_SUCCESS)
        {
            i4RetVal = SNMP_FAILURE;

        }
    }
    if (i4Result & (1 << (MAU_BYTE_LEN - MAU_TYPE_10_BASE_THD - 1)))
    {
        i4Value = MAU_TYPE_10_BASE_THD;
        pElement->pu1_OctetList[i4Value ] =
            (1 << (MAU_TYPE_LIST_LEN - (i4Value % MAU_OCTET_LEN)));

    }
    if (i4Result & (1 << (MAU_BYTE_LEN - MAU_TYPE_10_BASE_TFD - 1)))
    {
        i4Value = MAU_TYPE_10_BASE_TFD;
        pElement->pu1_OctetList[i4Value ] =
            (1 << (MAU_TYPE_LIST_LEN - (i4Value % MAU_OCTET_LEN)));

    }
    if (i4Result & (1 << (MAU_BYTE_LEN - MAU_TYPE_100_BASE_TXHD - 1)))
    {
        i4Value = MAU_TYPE_100_BASE_TXHD;
        pElement->pu1_OctetList[i4Value] =
            (1 << (MAU_TYPE_LIST_LEN - (i4Value % MAU_OCTET_LEN)));

    }
    if (i4Result & (1 << (MAU_BYTE_LEN - MAU_TYPE_100_BASE_TXFD - 1)))
    {
        i4Value = MAU_TYPE_100_BASE_TXFD;
        pElement->pu1_OctetList[i4Value] =
            (1 << (MAU_TYPE_LIST_LEN - (i4Value % MAU_OCTET_LEN)));

    }
    if (i4Result & (1 << (MAU_BYTE_LEN - MAU_TYPE_1000_BASE_TFD - 1)))
    {
        i4Value = MAU_TYPE_1000_BASE_TFD;
        pElement->pu1_OctetList[i4Value] =
            (1 << (MAU_TYPE_LIST_LEN - (i4Value % MAU_OCTET_LEN)));

    }
    if (i4Result & (1 << (MAU_BYTE_LEN_EXT - MAU_TYPE_2500_BASE_TFD - 1)))
    {
        i4Value = MAU_TYPE_2500_BASE_TFD ;
        pElement->pu1_OctetList[i4Value] =
            (1 << (MAU_TYPE_LIST_LEN - (i4Value % MAU_OCTET_LEN)));
    }
    if (i4Result & (1 << (MAU_BYTE_LEN - MAU_TYPE_10GIG_BASE_X - 1)))
    {
        i4Value = MAU_TYPE_10GIG_BASE_X;
        pElement->pu1_OctetList[i4Value] =
            (1 << (MAU_TYPE_LIST_LEN - (i4Value % MAU_OCTET_LEN)));
    }
#else
    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);

    pElement->i4_Length = sizeof (UINT1);
    pElement->pu1_OctetList[0] = (UINT1) i4Result;
#endif
    return (i4RetVal);
}

/* LOW LEVEL Routines for Table : IfJackTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIfJackTable
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex
                IfJackIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT4
nmhValidateIndexInstanceIfJackTable (INT4 i4ifMauIfIndex,
                                     INT4 i4ifMauIndex, INT4 i4ifJackIndex)
{
    UINT2               u2IfIndex;
    UINT1               u1IfType;

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0))
    {
        return (SNMP_FAILURE);
    }

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET))
    {
        if ((i4ifMauIndex > 0) && (i4ifMauIndex <= MAU_MAX_IF_COUNT)
            && (i4ifJackIndex > 0) && (i4ifJackIndex <= MAU_MAX_JACK_COUNT))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIfJackType
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex
                IfJackIndex

                The Object
                retValIfJackType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfJackType (INT4 i4ifMauIfIndex,
                  INT4 i4ifMauIndex, INT4 i4ifJackIndex, INT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    *pElement = MAU_JACK_OTHER;
#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4ifMauIfIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

    if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
    {
        if (CfaFsMauHwGetJackType ((UINT4) i4ifMauIfIndex,
                                   i4ifMauIndex, i4ifJackIndex,
                                   pElement) != FNP_SUCCESS)
        {
            i4RetVal = SNMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);
    UNUSED_PARAM (i4ifJackIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2IfMauAutoNegAdminStatus
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                testValIfMauAutoNegAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhTestv2IfMauAutoNegAdminStatus (UINT4 *pu4Error,
                                  INT4 i4ifMauIfIndex,
                                  INT4 i4ifMauIndex, INT4 *pElement)
{
    UINT2               u2IfIndex;
    INT4                i4Val = *pElement;
    INT4                i4RetVal = SNMP_SUCCESS;
    UINT1               u1IfType = CFA_NONE;

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0))
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET))
    {
        if ((i4ifMauIndex > 0) && (i4ifMauIndex <= MAU_MAX_IF_COUNT))
        {
            if ((i4Val == CFA_ENABLED) || (i4Val == CFA_DISABLED))
            {
                *pu4Error = 0;
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4Error = SNMP_ERR_WRONG_VALUE;
                i4RetVal = SNMP_FAILURE;
            }
        }
        else
        {
            *pu4Error = SNMP_ERR_NO_CREATION;
            i4RetVal = SNMP_FAILURE;
        }
    }
    else
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        i4RetVal = SNMP_FAILURE;
    }
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhSetIfMauAutoNegAdminStatus
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                setValIfMauAutoNegAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhSetIfMauAutoNegAdminStatus (INT4 i4ifMauIfIndex,
                               INT4 i4ifMauIndex, INT4 i4Element)
{
    UINT2               u2IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0) ||
        (i4ifMauIndex > MAU_MAX_IF_COUNT))
    {
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET)
        && (CfaIsMgmtPort ((UINT4) u2IfIndex) == FALSE))
    {
        if (nmhSetIssPortCtrlMode (i4ifMauIfIndex, ISS_AUTO) != SNMP_FAILURE)
        {
            if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
            {
                if (CfaFsMauHwSetAutoNegAdminStatus ((UINT4) i4ifMauIfIndex,
                                                     i4ifMauIndex, &i4Element)
                    != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
        }
    }
#endif
    CfaCdbSetIfMauAutoNegAdminStatus ((UINT4) i4ifMauIfIndex, i4Element);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IfMauAutoNegTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIfMauAutoNegTable
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT4
nmhValidateIndexInstanceIfMauAutoNegTable (INT4 i4ifMauIfIndex,
                                           INT4 i4ifMauIndex)
{
    UINT2               u2IfIndex;
    UINT1               u1IfType;

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0))
    {
        return (SNMP_FAILURE);
    }

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET))
    {
        if ((i4ifMauIndex > 0) && (i4ifMauIndex <= MAU_MAX_IF_COUNT))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetIfMauAutoNegAdminStatus
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauAutoNegAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauAutoNegAdminStatus (INT4 i4ifMauIfIndex,
                               INT4 i4ifMauIndex, INT4 *pElement)
{
    UINT2               u2IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0) ||
        (i4ifMauIndex > MAU_MAX_IF_COUNT))
    {
        return SNMP_FAILURE;
    }

    *pElement = CFA_DISABLED;
#ifdef NPAPI_WANTED
    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET)
        && (CfaIsMgmtPort ((UINT4) u2IfIndex) == FALSE))
    {
        if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
        {
            if (CfaFsMauHwGetAutoNegAdminStatus ((UINT4) i4ifMauIfIndex,
                                                 i4ifMauIndex, pElement)
                != FNP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
        }
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMauAutoNegRemoteSignaling
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauAutoNegRemoteSignaling
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauAutoNegRemoteSignaling (INT4 i4ifMauIfIndex,
                                   INT4 i4ifMauIndex, INT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    *pElement = MAU_NO_AUTO_SIG_DETECTED;
#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4ifMauIfIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

    if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
    {
        if (CfaFsMauHwGetAutoNegRemoteSignaling ((UINT4) i4ifMauIfIndex,
                                                 i4ifMauIndex,
                                                 pElement) != FNP_SUCCESS)
        {
            i4RetVal = SNMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetIfMauAutoNegConfig
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauAutoNegConfig
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauAutoNegConfig (INT4 i4ifMauIfIndex,
                          INT4 i4ifMauIndex, INT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    *pElement = MAU_AUTO_OTHER;
#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4ifMauIfIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

    if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
    {
        if (CfaFsMauHwGetAutoNegAdminConfig ((UINT4) i4ifMauIfIndex,
                                             i4ifMauIndex,
                                             pElement) != FNP_SUCCESS)
        {
            i4RetVal = SNMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);
#endif
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetIfMauAutoNegCapability
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauAutoNegCapability
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauAutoNegCapability (INT4 i4ifMauIfIndex,
                              INT4 i4ifMauIndex, INT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);
    *pElement = 0;
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2IfMauAutoNegCapAdvertised
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                testValIfMauAutoNegCapAdvertised
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhTestv2IfMauAutoNegCapAdvertised (UINT4 *pu4Error,
                                    INT4 i4ifMauIfIndex,
                                    INT4 i4ifMauIndex, INT4 *pElement)
{
    INT4                i4RetVal = SNMP_FAILURE;

    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);
    UNUSED_PARAM (pElement);
    *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhSetIfMauAutoNegCapAdvertised
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                setValIfMauAutoNegCapAdvertised
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhSetIfMauAutoNegCapAdvertised (INT4 i4ifMauIfIndex,
                                 INT4 i4ifMauIndex, INT4 i4Element)
{
    INT4                i4RetVal = SNMP_FAILURE;

    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);
    UNUSED_PARAM (i4Element);
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetIfMauAutoNegCapAdvertised
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauAutoNegCapAdvertised
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauAutoNegCapAdvertised (INT4 i4ifMauIfIndex,
                                 INT4 i4ifMauIndex, INT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);
    *pElement = 0;
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhGetIfMauAutoNegCapReceived
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauAutoNegCapReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauAutoNegCapReceived (INT4 i4ifMauIfIndex,
                               INT4 i4ifMauIndex, INT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);
    *pElement = 0;
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2IfMauAutoNegRestart
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                testValIfMauAutoNegRestart
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhTestv2IfMauAutoNegRestart (UINT4 *pu4Error,
                              INT4 i4ifMauIfIndex,
                              INT4 i4ifMauIndex, INT4 *pElement)
{
    UINT2               u2IfIndex;
    INT4                i4Val = *pElement;
    INT4                i4RetVal = SNMP_SUCCESS;
    UINT1               u1IfType;

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0))
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET))
    {
        if ((i4ifMauIndex > 0) && (i4ifMauIndex <= MAU_MAX_IF_COUNT))
        {
            if ((i4Val == MAU_AUTO_RESTART) || (i4Val == MAU_AUTO_NO_RESTART))
            {
                *pu4Error = 0;
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4Error = SNMP_ERR_WRONG_VALUE;
                i4RetVal = SNMP_FAILURE;
            }
        }
        else
        {
            *pu4Error = SNMP_ERR_NO_CREATION;
            i4RetVal = SNMP_FAILURE;
        }
    }
    else
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        i4RetVal = SNMP_FAILURE;
    }
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhSetIfMauAutoNegRestart
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                setValIfMauAutoNegRestart
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhSetIfMauAutoNegRestart (INT4 i4ifMauIfIndex,
                           INT4 i4ifMauIndex, INT4 i4Element)
{
    UINT2               u2IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0) ||
        (i4ifMauIndex > MAU_MAX_IF_COUNT))
    {
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET)
        && (CfaIsMgmtPort ((UINT4) u2IfIndex) == FALSE))
    {
        if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
        {
            if (CfaFsMauHwSetAutoNegRestart ((UINT4) i4ifMauIfIndex,
                                             i4ifMauIndex,
                                             &i4Element) != FNP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
        }
    }
#endif

    CfaCdbSetIfMauAutoNegRestart ((UINT4) i4ifMauIfIndex, i4Element);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMauAutoNegRestart
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauAutoNegRestart
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauAutoNegRestart (INT4 i4ifMauIfIndex,
                           INT4 i4ifMauIndex, INT4 *pElement)
{
    UINT2               u2IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0) ||
        (i4ifMauIndex > MAU_MAX_IF_COUNT))
    {
        return SNMP_FAILURE;
    }

    *pElement = MAU_AUTO_NO_RESTART;
#ifdef NPAPI_WANTED
    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET)
        && (CfaIsMgmtPort ((UINT4) u2IfIndex) == FALSE))
    {
        if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
        {
            if (CfaFsMauHwGetAutoNegRestart ((UINT4) i4ifMauIfIndex,
                                             i4ifMauIndex,
                                             pElement) != FNP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
        }
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMauAutoNegCapabilityBits
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauAutoNegCapabilityBits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauAutoNegCapabilityBits (INT4 i4ifMauIfIndex,
                                  INT4 i4ifMauIndex,
                                  tSNMP_OCTET_STRING_TYPE * pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4Result = 0;

    pElement->i4_Length = MAU_AUTO_NEG_BIT_SIZE;

#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4ifMauIfIndex) != CFA_TRUE)
    {
        pElement->pu1_OctetList[0] = (UINT1) i4Result;
        return SNMP_SUCCESS;
    }

    if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
    {
        if (CfaFsMauHwGetAutoNegCapBits ((UINT4) i4ifMauIfIndex,
                                         i4ifMauIndex,
                                         &i4Result) != FNP_SUCCESS)
        {
            i4RetVal = SNMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);
#endif
    MEMCPY (pElement->pu1_OctetList, &i4Result, pElement->i4_Length);
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2IfMauAutoNegCapAdvertisedBits
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                testValIfMauAutoNegCapAdvertisedBits
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhTestv2IfMauAutoNegCapAdvertisedBits (UINT4 *pu4Error,
                                        INT4 i4ifMauIfIndex,
                                        INT4 i4ifMauIndex,
                                        tSNMP_OCTET_STRING_TYPE * pElement)
{
    UINT2               u2IfIndex;
    INT4                i4Val;
    INT4                i4RetVal = SNMP_SUCCESS;
    UINT1               u1IfType;

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    MEMCPY (&i4Val, pElement->pu1_OctetList, pElement->i4_Length);
    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0))
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET))
    {
        if ((i4ifMauIndex > 0) && (i4ifMauIndex <= MAU_MAX_IF_COUNT))
        {
            if ((i4Val >= 0) && (i4Val <= 65535))
            {
                *pu4Error = 0;
                return SNMP_SUCCESS;
            }
        }
        else
        {
            *pu4Error = SNMP_ERR_NO_CREATION;
            i4RetVal = SNMP_FAILURE;
        }
    }
    else
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        i4RetVal = SNMP_FAILURE;
    }
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhSetIfMauAutoNegCapAdvertisedBits
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                setValIfMauAutoNegCapAdvertisedBits
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhSetIfMauAutoNegCapAdvertisedBits (INT4 i4ifMauIfIndex,
                                     INT4 i4ifMauIndex,
                                     tSNMP_OCTET_STRING_TYPE * pElement)
{
    UINT2               u2IfIndex;
    INT4                i4SetValue;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
    INT4                i4IssANCapBits = 0;
#endif

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0) ||
        (i4ifMauIndex > MAU_MAX_IF_COUNT))
    {
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET)
        && (CfaIsMgmtPort ((UINT4) u2IfIndex) == FALSE))
    {
        MEMCPY (&i4SetValue, pElement->pu1_OctetList, pElement->i4_Length);
        if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
        {
            if (i4SetValue & MAU_AUTO_B_OTHER)
            {
                IssGetPortANCapBits (i4ifMauIfIndex, &i4IssANCapBits);
                if (CfaIssHwSetPortAutoNegAdvtCapBits ((UINT4) i4ifMauIfIndex,
                                                       &i4SetValue,
                                                       &i4IssANCapBits) !=
                    FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
            else if (CfaFsMauHwSetAutoNegCapAdvtBits ((UINT4)
                                                      i4ifMauIfIndex,
                                                      i4ifMauIndex, &i4SetValue)
                     != FNP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
        }
    }
#else
    MEMCPY (&i4SetValue, pElement->pu1_OctetList, pElement->i4_Length);
#endif
    CfaCdbSetIfMauAutoNegCapAdvtBits ((UINT4) i4ifMauIfIndex, i4SetValue);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMauAutoNegCapAdvertisedBits
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauAutoNegCapAdvertisedBits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauAutoNegCapAdvertisedBits (INT4 i4ifMauIfIndex,
                                     INT4 i4ifMauIndex,
                                     tSNMP_OCTET_STRING_TYPE * pElement)
{
    UINT2               u2IfIndex;
    INT4                i4Result = 0;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0) ||
        (i4ifMauIndex > MAU_MAX_IF_COUNT))
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET)
        && (CfaIsMgmtPort ((UINT4) u2IfIndex) == FALSE))
    {
        if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
        {
            if (CfaFsMauHwGetAutoNegCapAdvtBits ((UINT4) i4ifMauIfIndex,
                                                 i4ifMauIndex, &i4Result)
                != FNP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
        }
    }
#endif
    MEMCPY (pElement->pu1_OctetList, &i4Result, MAU_AUTO_NEG_BIT_SIZE);
    pElement->i4_Length = MAU_AUTO_NEG_BIT_SIZE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMauAutoNegCapReceivedBits
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauAutoNegCapReceivedBits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauAutoNegCapReceivedBits (INT4 i4ifMauIfIndex,
                                   INT4 i4ifMauIndex,
                                   tSNMP_OCTET_STRING_TYPE * pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4Result = 0;

    pElement->i4_Length = MAU_AUTO_NEG_BIT_SIZE;

#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4ifMauIfIndex) != CFA_TRUE)
    {
        pElement->pu1_OctetList[0] = (UINT1) i4Result;
        return SNMP_SUCCESS;
    }

    if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
    {
        if (CfaFsMauHwGetAutoNegCapRcvdBits ((UINT4) i4ifMauIfIndex,
                                             i4ifMauIndex,
                                             &i4Result) != FNP_SUCCESS)
        {
            i4RetVal = SNMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);
#endif
    MEMCPY (pElement->pu1_OctetList, &i4Result, pElement->i4_Length);
    return (i4RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2IfMauAutoNegRemoteFaultAdvertised
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                testValIfMauAutoNegRemoteFaultAdvertised
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhTestv2IfMauAutoNegRemoteFaultAdvertised (UINT4 *pu4Error,
                                            INT4 i4ifMauIfIndex,
                                            INT4 i4ifMauIndex, INT4 *pElement)
{
    UINT2               u2IfIndex;
    INT4                i4Val = *pElement;
    INT4                i4RetVal = SNMP_SUCCESS;
    UINT1               u1IfType;

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0))
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET))
    {
        if ((i4ifMauIndex > 0) && (i4ifMauIndex <= MAU_MAX_IF_COUNT))
        {
            if ((i4Val >= MAU_AUTO_NO_ERROR) && (i4Val <= MAU_AUTO_ERROR))
            {
                *pu4Error = 0;
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4Error = SNMP_ERR_WRONG_VALUE;
                i4RetVal = SNMP_FAILURE;
            }
        }
        else
        {
            *pu4Error = SNMP_ERR_NO_CREATION;
            i4RetVal = SNMP_FAILURE;
        }
    }
    else
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        i4RetVal = SNMP_FAILURE;
    }
    return (i4RetVal);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IfMauAutoNegTable
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfMauAutoNegTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfMauAutoNegRemoteFaultAdvertised
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                setValIfMauAutoNegRemoteFaultAdvertised
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhSetIfMauAutoNegRemoteFaultAdvertised (INT4 i4ifMauIfIndex,
                                         INT4 i4ifMauIndex, INT4 i4Element)
{
    UINT2               u2IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0) ||
        (i4ifMauIndex > MAU_MAX_IF_COUNT))
    {
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET)
        && (CfaIsMgmtPort ((UINT4) u2IfIndex) == FALSE))
    {
        if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
        {
            if (CfaFsMauHwSetAutoNegRemFltAdvt ((UINT4) i4ifMauIfIndex,
                                                i4ifMauIndex,
                                                &i4Element) != FNP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
        }
    }
#endif
    CfaCdbSetIfMauAutoNegRemFaultAdvt ((UINT4) i4ifMauIfIndex, i4Element);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMauAutoNegRemoteFaultAdvertised
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauAutoNegRemoteFaultAdvertised
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauAutoNegRemoteFaultAdvertised (INT4 i4ifMauIfIndex,
                                         INT4 i4ifMauIndex, INT4 *pElement)
{
    UINT2               u2IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif

    u2IfIndex = (UINT2) i4ifMauIfIndex;

    if ((u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u2IfIndex == 0) ||
        (i4ifMauIndex > MAU_MAX_IF_COUNT))
    {
        return SNMP_FAILURE;
    }

    *pElement = MAU_AUTO_NO_ERROR;
#ifdef NPAPI_WANTED
    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    if ((CFA_IF_ENTRY_USED (u2IfIndex)) && (u1IfType == CFA_ENET)
        && (CfaIsMgmtPort ((UINT4) u2IfIndex) == FALSE))
    {
        if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
        {
            if (CfaFsMauHwGetAutoNegRemFltAdvt ((UINT4) i4ifMauIfIndex,
                                                i4ifMauIndex,
                                                pElement) != FNP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
        }
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMauAutoNegRemoteFaultReceived
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object
                retValIfMauAutoNegRemoteFaultReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetIfMauAutoNegRemoteFaultReceived (INT4 i4ifMauIfIndex,
                                       INT4 i4ifMauIndex, INT4 *pElement)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    *pElement = MAU_AUTO_NO_ERROR;
#ifdef NPAPI_WANTED
    if (CfaIsPhysicalInterface ((UINT4) i4ifMauIfIndex) != CFA_TRUE)
    {
        return SNMP_SUCCESS;
    }

    if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
    {
        if (CfaFsMauHwGetAutoNegRemFltRcvd
            ((UINT4) i4ifMauIfIndex, i4ifMauIndex, pElement) != FNP_SUCCESS)
        {
            i4RetVal = SNMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);
#endif
    return (i4RetVal);
}

/* LOW LEVEL Routines for Table : BroadMauBasicTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceBroadMauBasicTable
 Input       :  The Indices
                BroadMauIfIndex
                BroadMauIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT4
nmhValidateIndexInstanceBroadMauBasicTable (INT4 i4broadMauIfIndex,
                                            INT4 i4broadMauIndex)
{
    UNUSED_PARAM (i4broadMauIfIndex);
    UNUSED_PARAM (i4broadMauIndex);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetBroadMauXmtRcvSplitType
 Input       :  The Indices
                BroadMauIfIndex
                BroadMauIndex

                The Object
                retValBroadMauXmtRcvSplitType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetBroadMauXmtRcvSplitType (INT4 i4broadMauIfIndex,
                               INT4 i4broadMauIndex, INT4 *pElement)
{
    UNUSED_PARAM (i4broadMauIfIndex);
    UNUSED_PARAM (i4broadMauIndex);
    UNUSED_PARAM (pElement);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetBroadMauXmtCarrierFreq
 Input       :  The Indices
                BroadMauIfIndex
                BroadMauIndex

                The Object
                retValBroadMauXmtCarrierFreq
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetBroadMauXmtCarrierFreq (INT4 i4broadMauIfIndex,
                              INT4 i4broadMauIndex, INT4 *pElement)
{
    UNUSED_PARAM (i4broadMauIfIndex);
    UNUSED_PARAM (i4broadMauIndex);
    UNUSED_PARAM (pElement);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetBroadMauTranslationFreq
 Input       :  The Indices
                BroadMauIfIndex
                BroadMauIndex

                The Object
                retValBroadMauTranslationFreq
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetBroadMauTranslationFreq (INT4 i4broadMauIfIndex,
                               INT4 i4broadMauIndex, INT4 *pElement)
{
    UNUSED_PARAM (i4broadMauIfIndex);
    UNUSED_PARAM (i4broadMauIndex);
    UNUSED_PARAM (pElement);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexBroadMauBasicTable
 Input       :  The Indices
                BroadMauIfIndex
                BroadMauIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT4
nmhGetFirstIndexBroadMauBasicTable (INT4 *pi4broadMauIfIndex,
                                    INT4 *pi4broadMauIndex)
{
    UNUSED_PARAM (pi4broadMauIfIndex);
    UNUSED_PARAM (pi4broadMauIndex);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexBroadMauBasicTable
 Input       :  The Indices
                BroadMauIfIndex
                nextBroadMauIfIndex
                BroadMauIndex
                nextBroadMauIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT4
nmhGetNextIndexBroadMauBasicTable (INT4 i4broadMauIfIndex,
                                   INT4 *pi4broadMauIfIndexOUT,
                                   INT4 i4broadMauIndex,
                                   INT4 *pi4broadMauIndexOUT)
{
    UNUSED_PARAM (i4broadMauIfIndex);
    UNUSED_PARAM (i4broadMauIndex);
    UNUSED_PARAM (pi4broadMauIfIndexOUT);
    UNUSED_PARAM (pi4broadMauIndexOUT);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIfMauAutoNegTable
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT4
nmhGetFirstIndexIfMauAutoNegTable (INT4 *pi4ifMauIfIndex, INT4 *pi4ifMauIndex)
{
    UINT2               u2Index = 1;
    UINT1               u1IfType = CFA_NONE;

    /* Get the First Used Index */
    CFA_CDB_SCAN_WITH_LOCK (u2Index, BRG_MAX_PHY_PLUS_LOG_PORTS, CFA_ENET)
    {
        CfaGetIfType ((UINT4) u2Index, &u1IfType);

        if ((CFA_IF_ENTRY_USED (u2Index)) && (u1IfType == CFA_ENET))
        {
            *pi4ifMauIfIndex = (INT4) u2Index;
            *pi4ifMauIndex = 1;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIfMauAutoNegTable
 Input       :  The Indices
                IfMauIfIndex
                nextIfMauIfIndex
                IfMauIndex
                nextIfMauIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT4
nmhGetNextIndexIfMauAutoNegTable (INT4 i4ifMauIfIndex,
                                  INT4 *pi4ifMauIfIndexOUT,
                                  INT4 i4ifMauIndex, INT4 *pi4ifMauIndexOUT)
{
    UINT4               u4IfIndex;
    UINT4               u4Counter;
    UINT1               u1IfType = CFA_NONE;

    if ((i4ifMauIfIndex < 0) || (i4ifMauIndex < 0))
    {
        return SNMP_FAILURE;
    }
    u4IfIndex = (UINT4) i4ifMauIfIndex;

    if (i4ifMauIndex < MAU_MAX_IF_COUNT)
    {
        *pi4ifMauIfIndexOUT = (INT4) u4IfIndex;
        *pi4ifMauIndexOUT = i4ifMauIndex + 1;
        return SNMP_SUCCESS;
    }

    /* Next Index should always return the next used Index */
    u4Counter = u4IfIndex + 1;
    CFA_CDB_SCAN_WITH_LOCK (u4Counter, BRG_MAX_PHY_PLUS_LOG_PORTS, CFA_ENET)
    {
        CfaGetIfType (u4Counter, &u1IfType);

        if ((CFA_IF_ENTRY_USED ((UINT2) u4Counter)) && (u1IfType == CFA_ENET))
        {
            *pi4ifMauIfIndexOUT = (INT4) u4Counter;
            *pi4ifMauIndexOUT = 1;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIfJackTable
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex
                IfJackIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT4
nmhGetFirstIndexIfJackTable (INT4 *pi4ifMauIfIndex,
                             INT4 *pi4ifMauIndex, INT4 *pi4ifJackIndex)
{
    UINT2               u2Index = 1;
    UINT1               u1IfType = CFA_NONE;

    /* Get the First Used Index */
    CFA_CDB_SCAN_WITH_LOCK (u2Index, BRG_MAX_PHY_PLUS_LOG_PORTS, CFA_ENET)
    {
        CfaGetIfType ((UINT4) u2Index, &u1IfType);

        if ((CFA_IF_ENTRY_USED (u2Index)) && (u1IfType == CFA_ENET))
        {
            *pi4ifMauIfIndex = (INT4) u2Index;
            *pi4ifMauIndex = 1;
            *pi4ifJackIndex = 1;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIfJackTable
 Input       :  The Indices
                IfMauIfIndex
                nextIfMauIfIndex
                IfMauIndex
                nextIfMauIndex
                IfJackIndex
                nextIfJackIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT4
nmhGetNextIndexIfJackTable (INT4 i4ifMauIfIndex,
                            INT4 *pi4ifMauIfIndexOUT,
                            INT4 i4ifMauIndex,
                            INT4 *pi4ifMauIndexOUT,
                            INT4 i4ifJackIndex, INT4 *pi4ifJackIndexOUT)
{
    UINT4               u4IfIndex;
    UINT4               u4Counter;
    UINT1               u1IfType = CFA_NONE;

    if ((i4ifMauIfIndex < 0) || (i4ifMauIndex < 0) || (i4ifJackIndex < 0))
    {
        return SNMP_FAILURE;
    }
    u4IfIndex = (UINT4) i4ifMauIfIndex;

    if ((u4IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return (SNMP_FAILURE);
    }

    if (u4IfIndex == 0)
    {
        u4IfIndex = 1;
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

    if (!((CFA_IF_ENTRY_USED ((UINT2) u4IfIndex)) && (u1IfType == CFA_ENET)))
    {
        return (SNMP_FAILURE);
    }

    if (i4ifJackIndex > MAU_MAX_JACK_COUNT)
    {
        i4ifJackIndex = 0;
    }

    if (i4ifMauIndex > MAU_MAX_IF_COUNT)
    {
        i4ifMauIndex = 0;
    }

    if (i4ifJackIndex < MAU_MAX_JACK_COUNT)
    {
        *pi4ifMauIfIndexOUT = (INT4) u4IfIndex;

        if (i4ifMauIndex < MAU_MAX_IF_COUNT)
        {
            /*last two indices are 0s */
            *pi4ifMauIndexOUT = 1;
        }
        else
        {
            /*only the last index is 0 */
            *pi4ifMauIndexOUT = i4ifMauIndex;
        }
        *pi4ifJackIndexOUT = i4ifJackIndex + 1;
        return SNMP_SUCCESS;
    }

    if (i4ifMauIndex < MAU_MAX_IF_COUNT)
    {
        *pi4ifMauIfIndexOUT = (INT4) u4IfIndex;
        *pi4ifMauIndexOUT = i4ifMauIndex + 1;
        *pi4ifJackIndexOUT = 1;
        return SNMP_SUCCESS;
    }
    /* Next Index should always return the next used Index */
    u4Counter = u4IfIndex + 1;
    CFA_CDB_SCAN_WITH_LOCK (u4Counter, BRG_MAX_PHY_PLUS_LOG_PORTS, CFA_ENET)
    {
        CfaGetIfType (u4Counter, &u1IfType);

        if ((CFA_IF_ENTRY_USED ((UINT2) u4Counter)) && (u1IfType == CFA_ENET))
        {
            *pi4ifMauIfIndexOUT = (INT4) u4Counter;
            *pi4ifMauIndexOUT = 1;
            *pi4ifJackIndexOUT = 1;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIfMauTable
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT4
nmhGetFirstIndexIfMauTable (INT4 *pi4ifMauIfIndex, INT4 *pi4ifMauIndex)
{
    UINT2               u2Index = 1;
    UINT1               u1IfType = CFA_NONE;

    /* Get the First Used Index */
    CFA_CDB_SCAN_WITH_LOCK (u2Index, BRG_MAX_PHY_PLUS_LOG_PORTS, CFA_ENET)
    {
        CfaGetIfType ((UINT4) u2Index, &u1IfType);

        if ((CFA_IF_ENTRY_USED (u2Index)) && (u1IfType == CFA_ENET))
        {
            *pi4ifMauIfIndex = (INT4) u2Index;
            *pi4ifMauIndex = 1;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIfMauTable
 Input       :  The Indices
                IfMauIfIndex
                nextIfMauIfIndex
                IfMauIndex
                nextIfMauIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT4
nmhGetNextIndexIfMauTable (INT4 i4ifMauIfIndex,
                           INT4 *pi4ifMauIfIndexOUT,
                           INT4 i4ifMauIndex, INT4 *pi4ifMauIndexOUT)
{
    UINT4               u4IfIndex;
    UINT4               u4Counter;
    UINT1               u1IfType = CFA_IF_DOWN;

    if ((i4ifMauIfIndex < 0) || (i4ifMauIndex < 0))
    {
        return SNMP_FAILURE;
    }
    u4IfIndex = (UINT4) i4ifMauIfIndex;

    if (i4ifMauIndex < MAU_MAX_IF_COUNT)
    {
        *pi4ifMauIfIndexOUT = (INT4) u4IfIndex;
        *pi4ifMauIndexOUT = i4ifMauIndex + 1;
        return SNMP_SUCCESS;
    }

    /* Next Index should always return the next used Index */
    u4Counter = u4IfIndex + 1;
    CFA_CDB_SCAN_WITH_LOCK (u4Counter, BRG_MAX_PHY_PLUS_LOG_PORTS, CFA_ENET)
    {
        CfaGetIfType (u4Counter, &u1IfType);

        if ((CFA_IF_ENTRY_USED ((UINT2) u4Counter)) && (u1IfType == CFA_ENET))
        {
            *pi4ifMauIfIndexOUT = (INT4) u4Counter;
            *pi4ifMauIndexOUT = 1;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* The following low level routines are stubbed out to return SNMP failure
 * as Repeaters are not supported in the MAU MIB of RFC 2668.
 */
/****************************************************************************
 Function    :  nmhGetFirstIndexRpJackTable
 Input       :  The Indices
                RpMauGroupIndex
                RpMauPortIndex
                RpMauIndex
                RpJackIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT4
nmhGetFirstIndexRpJackTable (INT4 *pi4rpMauGroupIndex,
                             INT4 *pi4rpMauPortIndex,
                             INT4 *pi4rpMauIndex, INT4 *pi4rpJackIndex)
{
    UNUSED_PARAM (pi4rpMauGroupIndex);
    UNUSED_PARAM (pi4rpMauPortIndex);
    UNUSED_PARAM (pi4rpMauIndex);
    UNUSED_PARAM (pi4rpJackIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexRpJackTable
 Input       :  The Indices
                RpMauGroupIndex
                nextRpMauGroupIndex
                RpMauPortIndex
                nextRpMauPortIndex
                RpMauIndex
                nextRpMauIndex
                RpJackIndex
                nextRpJackIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT4
nmhGetNextIndexRpJackTable (INT4 i4rpMauGroupIndex,
                            INT4 *pi4rpMauGroupIndexOUT,
                            INT4 i4rpMauPortIndex,
                            INT4 *pi4rpMauPortIndexOUT,
                            INT4 i4rpMauIndex,
                            INT4 *pi4rpMauIndexOUT,
                            INT4 i4rpJackIndex, INT4 *pi4rpJackIndexOUT)
{
    UNUSED_PARAM (i4rpMauGroupIndex);
    UNUSED_PARAM (pi4rpMauGroupIndexOUT);
    UNUSED_PARAM (i4rpMauPortIndex);
    UNUSED_PARAM (pi4rpMauPortIndexOUT);
    UNUSED_PARAM (i4rpMauIndex);
    UNUSED_PARAM (pi4rpMauIndexOUT);
    UNUSED_PARAM (i4rpJackIndex);
    UNUSED_PARAM (pi4rpJackIndexOUT);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexRpMauTable
 Input       :  The Indices
                RpMauGroupIndex
                RpMauPortIndex
                RpMauIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT4
nmhGetFirstIndexRpMauTable (INT4 *pi4rpMauGroupIndex,
                            INT4 *pi4rpMauPortIndex, INT4 *pi4rpMauIndex)
{
    UNUSED_PARAM (pi4rpMauGroupIndex);
    UNUSED_PARAM (pi4rpMauPortIndex);
    UNUSED_PARAM (pi4rpMauIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexRpMauTable
 Input       :  The Indices
                RpMauGroupIndex
                nextRpMauGroupIndex
                RpMauPortIndex
                nextRpMauPortIndex
                RpMauIndex
                nextRpMauIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT4
nmhGetNextIndexRpMauTable (INT4 i4rpMauGroupIndex,
                           INT4 *pi4rpMauGroupIndexOUT,
                           INT4 i4rpMauPortIndex,
                           INT4 *pi4rpMauPortIndexOUT,
                           INT4 i4rpMauIndex, INT4 *pi4rpMauIndexOUT)
{
    UNUSED_PARAM (i4rpMauGroupIndex);
    UNUSED_PARAM (pi4rpMauGroupIndexOUT);
    UNUSED_PARAM (i4rpMauPortIndex);
    UNUSED_PARAM (pi4rpMauPortIndexOUT);
    UNUSED_PARAM (i4rpMauIndex);
    UNUSED_PARAM (pi4rpMauIndexOUT);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : RpMauTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceRpMauTable
 Input       :  The Indices
                RpMauGroupIndex
                RpMauPortIndex
                RpMauIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT4
nmhValidateIndexInstanceRpMauTable (INT4 i4rpMauGroupIndex,
                                    INT4 i4rpMauPortIndex, INT4 i4rpMauIndex)
{
    UNUSED_PARAM (i4rpMauGroupIndex);
    UNUSED_PARAM (i4rpMauPortIndex);
    UNUSED_PARAM (i4rpMauIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetRpMauType
 Input       :  The Indices
                RpMauGroupIndex
                RpMauPortIndex
                RpMauIndex

                The Object
                retValRpMauType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetRpMauType (INT4 i4rpMauGroupIndex,
                 INT4 i4rpMauPortIndex,
                 INT4 i4rpMauIndex, tSNMP_OID_TYPE * pElement)
{
    UNUSED_PARAM (i4rpMauGroupIndex);
    UNUSED_PARAM (i4rpMauPortIndex);
    UNUSED_PARAM (i4rpMauIndex);
    UNUSED_PARAM (pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2RpMauStatus
 Input       :  The Indices
                RpMauGroupIndex
                RpMauPortIndex
                RpMauIndex

                The Object
                testValRpMauStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhTestv2RpMauStatus (UINT4 *pu4Error,
                      INT4 i4rpMauGroupIndex,
                      INT4 i4rpMauPortIndex, INT4 i4rpMauIndex, INT4 *pElement)
{
    UNUSED_PARAM (i4rpMauGroupIndex);
    UNUSED_PARAM (i4rpMauPortIndex);
    UNUSED_PARAM (i4rpMauIndex);
    UNUSED_PARAM (pElement);
    UNUSED_PARAM (pu4Error);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2RpMauTable
 Input       :  The Indices
                RpMauGroupIndex
                RpMauPortIndex
                RpMauIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2RpMauTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetRpMauStatus
 Input       :  The Indices
                RpMauGroupIndex
                RpMauPortIndex
                RpMauIndex

                The Object
                setValRpMauStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhSetRpMauStatus (INT4 i4rpMauGroupIndex,
                   INT4 i4rpMauPortIndex, INT4 i4rpMauIndex, INT4 i4Element)
{
    UNUSED_PARAM (i4rpMauGroupIndex);
    UNUSED_PARAM (i4rpMauPortIndex);
    UNUSED_PARAM (i4rpMauIndex);
    UNUSED_PARAM (i4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetRpMauStatus
 Input       :  The Indices
                RpMauGroupIndex
                RpMauPortIndex
                RpMauIndex

                The Object
                retValRpMauStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetRpMauStatus (INT4 i4rpMauGroupIndex,
                   INT4 i4rpMauPortIndex, INT4 i4rpMauIndex, INT4 *pElement)
{
    UNUSED_PARAM (i4rpMauGroupIndex);
    UNUSED_PARAM (i4rpMauPortIndex);
    UNUSED_PARAM (i4rpMauIndex);
    UNUSED_PARAM (pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetRpMauMediaAvailable
 Input       :  The Indices
                RpMauGroupIndex
                RpMauPortIndex
                RpMauIndex

                The Object
                retValRpMauMediaAvailable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetRpMauMediaAvailable (INT4 i4rpMauGroupIndex,
                           INT4 i4rpMauPortIndex,
                           INT4 i4rpMauIndex, INT4 *pElement)
{
    UNUSED_PARAM (i4rpMauGroupIndex);
    UNUSED_PARAM (i4rpMauPortIndex);
    UNUSED_PARAM (i4rpMauIndex);
    UNUSED_PARAM (pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetRpMauMediaAvailableStateExits
 Input       :  The Indices
                RpMauGroupIndex
                RpMauPortIndex
                RpMauIndex

                The Object
                retValRpMauMediaAvailableStateExits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetRpMauMediaAvailableStateExits (INT4 i4rpMauGroupIndex,
                                     INT4 i4rpMauPortIndex,
                                     INT4 i4rpMauIndex, UINT4 *pElement)
{
    UNUSED_PARAM (i4rpMauGroupIndex);
    UNUSED_PARAM (i4rpMauPortIndex);
    UNUSED_PARAM (i4rpMauIndex);
    UNUSED_PARAM (pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetRpMauJabberState
 Input       :  The Indices
                RpMauGroupIndex
                RpMauPortIndex
                RpMauIndex

                The Object
                retValRpMauJabberState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetRpMauJabberState (INT4 i4rpMauGroupIndex,
                        INT4 i4rpMauPortIndex,
                        INT4 i4rpMauIndex, INT4 *pElement)
{
    UNUSED_PARAM (i4rpMauGroupIndex);
    UNUSED_PARAM (i4rpMauPortIndex);
    UNUSED_PARAM (i4rpMauIndex);
    UNUSED_PARAM (pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetRpMauJabberingStateEnters
 Input       :  The Indices
                RpMauGroupIndex
                RpMauPortIndex
                RpMauIndex

                The Object
                retValRpMauJabberingStateEnters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetRpMauJabberingStateEnters (INT4 i4rpMauGroupIndex,
                                 INT4 i4rpMauPortIndex,
                                 INT4 i4rpMauIndex, UINT4 *pElement)
{
    UNUSED_PARAM (i4rpMauGroupIndex);
    UNUSED_PARAM (i4rpMauPortIndex);
    UNUSED_PARAM (i4rpMauIndex);
    UNUSED_PARAM (pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetRpMauFalseCarriers
 Input       :  The Indices
                RpMauGroupIndex
                RpMauPortIndex
                RpMauIndex

                The Object
                retValRpMauFalseCarriers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetRpMauFalseCarriers (INT4 i4rpMauGroupIndex,
                          INT4 i4rpMauPortIndex,
                          INT4 i4rpMauIndex, UINT4 *pElement)
{
    UNUSED_PARAM (i4rpMauGroupIndex);
    UNUSED_PARAM (i4rpMauPortIndex);
    UNUSED_PARAM (i4rpMauIndex);
    UNUSED_PARAM (pElement);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : RpJackTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceRpJackTable
 Input       :  The Indices
                RpMauGroupIndex
                RpMauPortIndex
                RpMauIndex
                RpJackIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT4
nmhValidateIndexInstanceRpJackTable (INT4 i4rpMauGroupIndex,
                                     INT4 i4rpMauPortIndex,
                                     INT4 i4rpMauIndex, INT4 i4rpJackIndex)
{
    UNUSED_PARAM (i4rpMauGroupIndex);
    UNUSED_PARAM (i4rpMauPortIndex);
    UNUSED_PARAM (i4rpMauIndex);
    UNUSED_PARAM (i4rpJackIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetRpJackType
 Input       :  The Indices
                RpMauGroupIndex
                RpMauPortIndex
                RpMauIndex
                RpJackIndex

                The Object
                retValRpJackType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
nmhGetRpJackType (INT4 i4rpMauGroupIndex,
                  INT4 i4rpMauPortIndex,
                  INT4 i4rpMauIndex, INT4 i4rpJackIndex, INT4 *pElement)
{
    UNUSED_PARAM (i4rpMauGroupIndex);
    UNUSED_PARAM (i4rpMauPortIndex);
    UNUSED_PARAM (i4rpMauIndex);
    UNUSED_PARAM (i4rpJackIndex);
    UNUSED_PARAM (pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfMauHCFalseCarriers
 Input       :  The Indices
                IfMauIfIndex
                IfMauIndex

                The Object 
                retValIfMauHCFalseCarriers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
nmhGetIfMauHCFalseCarriers (INT4 i4ifMauIfIndex, INT4 i4ifMauIndex,
                            tSNMP_COUNTER64_TYPE * pElement)
{
    UNUSED_PARAM (i4ifMauIfIndex);
    UNUSED_PARAM (i4ifMauIndex);

    /* Not supported */
    pElement->msn = 0;
    pElement->lsn = 0;
    return SNMP_SUCCESS;
}
