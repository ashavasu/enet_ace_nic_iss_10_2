/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddbcmapi.c,v 1.73 2017/11/14 07:31:10 siva Exp $
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/
#include "cfainc.h"
#include "fsvlan.h"
#include "vlangarp/garp.h"
#include "igs.h"

#ifdef NPAPI_WANTED
#ifndef CFA_INTERRUPT_MODE
#error "Enable INTERRUPT_MODE in CFA"
#endif
#include "cfanp.h"
#endif
#include "gddbcmapi.h"

#ifdef OPENFLOW_WANTED
#include "ofcl.h"
#endif

PRIVATE INT4        NpCfaIsVlanTagRequest (tCRU_BUF_CHAIN_HEADER * pBuf);

UINT1              *CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
VOID                CfaGddSetLnxIntfnameForPort (UINT4 u4Index,
                                                 UINT1 *pu1IfName);

#ifdef LNXIP4_WANTED
static UINT1       
    au1IntMapTable[SYS_DEF_MAX_PHYSICAL_INTERFACES][2]
    [CFA_MAX_PORT_NAME_LENGTH];
#else
/* Array containing the Mapping between Slot and Eth Interfaces 
 * In case of LinuxIP, ports are referred based on the Linux Eth name 
 * to which it is mapped */
static UINT1       
    au1IntMapTable[SYS_DEF_MAX_PHYSICAL_INTERFACES][2][CFA_MAX_PORT_NAME_LENGTH]
    = { {"Slot0/1", "eth0"},
{"Slot0/2", "eth1"},
{"Slot0/3", "eth2"},
{"Slot0/4", "eth3"},
{"Slot0/5", "eth4"},
{"Slot0/6", "eth5"},
{"Slot0/7", "eth6"},
{"Slot0/8", "eth7"},
{"Slot0/9", "eth8"},
{"Slot0/10", "eth9"},
{"Slot0/11", "eth10"},
{"Slot0/12", "eth11"},
{"Slot0/13", "eth12"},
{"Slot0/14", "eth13"},
{"Slot0/15", "eth14"},
{"Slot0/16", "eth15"},
{"Slot0/17", "eth16"},
{"Slot0/18", "eth17"},
{"Slot0/19", "eth18"},
{"Slot0/20", "eth19"},
{"Slot0/21", "eth20"},
{"Slot0/22", "eth21"},
{"Slot0/23", "eth22"},
{"Slot0/24", "eth23"}
};
#endif

/*****************************************************************************
 *    Function Name      : CfaGddProcessRecvInterruptEvent ()
 *    Description        : This function is invoked when an driver 
 *                         event occurs. This routine invokes the packet
 *                         processing function if a packet is received 
 *                         else it invokes the function which process the
 *                         driver status if status indication message is
 *                         received
 *    Input(s)           : VOID
 *    Output(s)          : None.
 
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Returns                  : None.
 *****************************************************************************/

PUBLIC INT4
CfaGddProcessRecvInterruptEvent (VOID)
{
#ifdef IP6_WANTED
    tIp6Addr            Ip6Addr;
#endif
    tCRU_BUF_CHAIN_HEADER *pCruBuf;
    UINT4               u4IfIndex = CFA_ZERO;
    UINT4               u4DestIP = 0;
    UINT4               u4ContextId = CFA_ZERO;
    UINT4               u4PktSize;
    UINT4               u4VlanIfIndex = CFA_ZERO;
    UINT2               u2Protocol = 0;
    UINT2               u2Offset = 0;
    UINT2               u2Tag = CFA_ZERO;
    UINT1               u1BridgedIfaceStatus = CFA_DISABLED;

    UINT1               au1EnetHdr[VLAN_TAG_OFFSET];
#ifdef MPLS_WANTED
    UINT1               au1DstHdr[CFA_ENET_ADDR_LEN];
    UINT2               u2VlanTag = 0;
    UINT2               u2EtherMplsType = 0;
#endif
    UINT1               u1IfType;
    UINT4               u4PktCount = 0;
#ifdef OPENFLOW_WANTED
    UINT4               u4VlanIndex = 0;
    UINT1               u1SubType = CFA_SUBTYPE_SISP_INTERFACE;
#endif

    while (OsixQueRecv (CFA_PACKET_MUX_QID, (UINT1 *) &pCruBuf,
                        OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        CFA_LOCK ();

#ifdef MPLS_WANTED
        /* In katana 2 ,when MPLS PW is programmed , LACP/STP packet are recieved with Priority Tag
         *Priority tag is removed for further Processing in Higher Layer Module*/
        CRU_BUF_Copy_FromBufChain (pCruBuf, au1DstHdr, 0, CFA_ENET_ADDR_LEN);
        if (CFA_IS_ENET_MAC_MCAST (au1DstHdr))
        {

            CRU_BUF_Copy_FromBufChain ((pCruBuf), (UINT1 *) &u2EtherMplsType,
                                       VLAN_TAG_OFFSET, VLAN_TYPE_OR_LEN_SIZE);
            u2EtherMplsType = OSIX_NTOHS (u2EtherMplsType);

            if (u2EtherMplsType == 0x8100)
            {
                CRU_BUF_Copy_FromBufChain ((pCruBuf), (UINT1 *) &u2VlanTag,
                                           VLAN_TAG_VLANID_OFFSET,
                                           VLAN_TAG_SIZE);

                u2VlanTag = OSIX_NTOHS (u2VlanTag);
                u2VlanTag = (tVlanId) (u2VlanTag & VLAN_ID_MASK);
                if (u2VlanTag == 0)
                {
                    CRU_BUF_Copy_FromBufChain (pCruBuf, au1EnetHdr, 0,
                                               VLAN_TAG_OFFSET);
                    CRU_BUF_Move_ValidOffset (pCruBuf,
                                              VLAN_TAG_OFFSET +
                                              VLAN_TAG_PID_LEN);
                    CRU_BUF_Prepend_BufChain (pCruBuf, au1EnetHdr,
                                              VLAN_TAG_OFFSET);

                }
            }

        }
#endif

        CfaGetVlanIfIndex (u4IfIndex, pCruBuf, &u2Tag);

        VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);

        CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);

        if (u1BridgedIfaceStatus == CFA_ENABLED)
        {
            u4VlanIfIndex = CfaGetVlanInterfaceIndexInCxt (u4ContextId, u2Tag);
            VcmGetContextIdFromCfaIfIndex (u4VlanIfIndex, &u4ContextId);
        }
        /* extract the interface index and packet size from buffer */
        if (NpCfaIsVlanTagRequest (pCruBuf) == CFA_FALSE)
        {
            CRU_BUF_Copy_FromBufChain (pCruBuf, au1EnetHdr, 0, VLAN_TAG_OFFSET);
            CRU_BUF_Move_ValidOffset (pCruBuf,
                                      VLAN_TAG_OFFSET + VLAN_TAG_PID_LEN);
            CRU_BUF_Prepend_BufChain (pCruBuf, au1EnetHdr, VLAN_TAG_OFFSET);
        }

        u4IfIndex = pCruBuf->ModuleData.InterfaceId.u4IfIndex;
        u4PktSize = pCruBuf->pFirstValidDataDesc->u4_ValidByteCount;

#ifdef OPENFLOW_WANTED
        if (OfcHandleVlanPkt (pCruBuf, &u4VlanIndex) == OSIX_SUCCESS)
        {
            if (OpenflowNotifyPktRecv (u4VlanIndex, pCruBuf) == OFC_FAILURE)
            {
                CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
            }
            CFA_UNLOCK ();
            continue;
        }
#endif /* OPENFLOW_WANTED */

        if (CfaGetIfType (u4IfIndex, &u1IfType) == CFA_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                     "In CfaMain Pkt recvd in interrupt mode failed to deliver to HL.\n");
        }
        else
        {

#ifdef OPENFLOW_WANTED
            CfaGetIfSubType ((UINT4) u4IfIndex, &u1SubType);
            if (u1SubType == CFA_SUBTYPE_OPENFLOW_INTERFACE)
            {
                CFA_IF_SET_IN_OCTETS (u4IfIndex, u4PktSize);
                CFA_IF_SET_IN_UCAST (u4IfIndex);
                /*
                 * Make the openflow ports visble to the ISS context when the Client
                 * context associated with the IfIndex is in fail stand alone mode
                 * and also process the packets via CFA.Else process the
                 * packets through the Openflow Pipeline Process.
                 */
                if (OfcHandleOpenflowSwModeProcess (&u4IfIndex, pCruBuf) !=
                    OFC_SUCCESS)
                {
                    if (OpenflowNotifyPktRecv (u4IfIndex, pCruBuf) ==
                        OFC_FAILURE)
                    {
                        CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
                    }
                    CFA_UNLOCK ();
                    continue;
                }
            }
#endif

            /* Handover the Packet to Receive module */
            if ((CFA_GDD_HL_RX_FNPTR (u4IfIndex))
                (pCruBuf, u4IfIndex, u4PktSize, u1IfType,
                 CFA_ENCAP_NONE) != CFA_SUCCESS)
            {
                CRU_BUF_Copy_FromBufChain (pCruBuf, (UINT1 *) &u2Protocol,
                                           CFA_VLAN_TAG_OFFSET,
                                           CFA_VLAN_PROTOCOL_SIZE);
                u2Protocol = OSIX_NTOHS (u2Protocol);
                u2Offset = CFA_VLAN_TAG_OFFSET;

                while ((CFA_VLAN_PROTOCOL_ID == u2Protocol) ||
                       (VLAN_PROVIDER_PROTOCOL_ID == u2Protocol))
                {
                    u2Offset += CFA_ENET_TYPE_OR_LEN + CFA_ENET_TYPE_OR_LEN;

                    CRU_BUF_Copy_FromBufChain (pCruBuf, (UINT1 *) &u2Protocol,
                                               (UINT4) (u2Offset +
                                                        CFA_VLAN_TAG_OFFSET),
                                               CFA_VLAN_PROTOCOL_SIZE);
                    u2Protocol = OSIX_NTOHS (u2Protocol);
                }

                CRU_BUF_Copy_FromBufChain (pCruBuf, (UINT1 *) &u2Protocol,
                                           u2Offset, CFA_VLAN_PROTOCOL_SIZE);
                u2Protocol = OSIX_NTOHS (u2Protocol);
                u2Offset = (UINT2) (u2Offset + CFA_VLAN_PROTOCOL_SIZE);
                if (CFA_ENET_IPV4 == u2Protocol)
                {
                    CRU_BUF_Copy_FromBufChain (pCruBuf, (UINT1 *) &u4DestIP,
                                               (UINT4) (u2Offset +
                                                        IP_PKT_OFF_DEST),
                                               sizeof (UINT4));
                    u4DestIP = OSIX_NTOHL (u4DestIP);
                    /* Remove the Entry created in the DLF Hash Table */

                    CfaFsCfaHwRemoveIpNetRcvdDlfInHash (u4ContextId, u4DestIP,
                                                        0xffffffff);
                }
#ifdef IP6_WANTED
                else if (CFA_ENET_IPV6 == u2Protocol)
                {
                    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
                    u2Offset = u2Offset + IP6_OFFSET_FOR_DESTADDR_FIELD;
                    CRU_BUF_Copy_FromBufChain (pCruBuf, (UINT1 *) &u4DestIP,
                                               u2Offset, sizeof (UINT4));
                    u4DestIP = OSIX_NTOHL (u4DestIP);
                    Ip6Addr.u4_addr[0] = u4DestIP;

                    u2Offset += sizeof (UINT4);
                    CRU_BUF_Copy_FromBufChain (pCruBuf, (UINT1 *) &u4DestIP,
                                               u2Offset, sizeof (UINT4));
                    u4DestIP = OSIX_NTOHL (u4DestIP);
                    Ip6Addr.u4_addr[1] = u4DestIP;

                    u2Offset += sizeof (UINT4);
                    CRU_BUF_Copy_FromBufChain (pCruBuf, (UINT1 *) &u4DestIP,
                                               u2Offset, sizeof (UINT4));
                    u4DestIP = OSIX_NTOHL (u4DestIP);
                    Ip6Addr.u4_addr[2] = u4DestIP;

                    u2Offset += sizeof (UINT4);
                    CRU_BUF_Copy_FromBufChain (pCruBuf, (UINT1 *) &u4DestIP,
                                               u2Offset, sizeof (UINT4));
                    u4DestIP = OSIX_NTOHL (u4DestIP);
                    Ip6Addr.u4_addr[3] = u4DestIP;

                    /* Remove the Entry created in the DLF Hash Table */
                    CfaFsCfaHwRemoveIp6NetRcvdDlfInHash (u4ContextId, Ip6Addr,
                                                         IP6_MAX_PREFIX_LEN);
                }
#endif

                /* release buffer which were not successfully sent to higher layer
                 */
                CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "In CfaMain Pkt recvd in interrupt mode failed to deliver to HL.\n");
            }
            else
            {
                /* increment after successful handling of packet */
                CFA_IF_SET_IN_OCTETS (u4IfIndex, u4PktSize);
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "In CfaMain Pkt recvd in interrupt mode delivered to HL.\n");
            }
        }
        CFA_UNLOCK ();
        u4PktCount++;
        if (u4PktCount >= CFA_MAX_NO_OF_PACKET_PER_READ)
        {
            OsixEvtSend (CFA_TASK_ID, CFA_GDD_INTERRUPT_EVENT);
            break;
        }
    }
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *    Function Name       : CfaGddInit
 *    Description         : This function performs the initialisation of
 *                          the  Device Driver Module of CFA. This routine
 *                          1. creates the Queue for reception of packets from
 *                             driver
 *                          2. creates the memory pool for the driver messages
 *                          3. initializes the driver to cfa mapping tables
 *    Input(s)            : None.
 *    Output(s)           : None.
 *
 *    Global Variables Referred : None 
 *    Global Variables Modified : gaCfaNpIndexMap 
 *                               
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if initialisation succeeds,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
CfaGddInit (VOID)
{
    tMacAddr            au1SwitchMac;    /* Base MAC address */

    CfaGetSysMacAddress (au1SwitchMac);

    CfaNpUpdateSwitchMac (au1SwitchMac);

    /* Initialize the Borad and UnitInfo structures */
    if (CfaNpInit () != FNP_SUCCESS)
    {
        PRINTF ("ERROR[NP] - CfaNpInit returned failure\n\r");
        return (CFA_FAILURE);
    }

    return (CFA_SUCCESS);
}

#ifdef MBSM_WANTED
/*****************************************************************************
 *    Function Name       : CfaMbsmGddInit
 *    Description         : This function performs the initialisation of
 *                          the  Device Driver Module of CFA. This routine
 *                          1. creates the Queue for reception of packets from
 *                             driver
 *                          2. creates the memory pool for the driver messages
 *                          3. initializes the driver to cfa mapping tables
 *    Input(s)            : None.
 *    Output(s)           : None.
 *
 *    Global Variables Referred : _devices, _ndevices
 *    Global Variables Modified : gFsDrvMemPoolId, gaCfaNpIndexMap
 *                                gaCfaNpDevMap
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if initialisation succeeds,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

PUBLIC INT4
CfaMbsmGddInit (tMbsmSlotInfo * pSlotInfo)
{
    /* Update the Driver to cfa mapping tables for the given slot.
     */
    if (CfaCfaMbsmNpSlotInit (pSlotInfo) == FNP_FAILURE)
    {
        PRINTF ("[CFA] CfaMbsmNpUpdateSlotInfo returned failure\r\n");
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

#endif /* MBSM_WANTED */

/*****************************************************************************
 *    Function Name        : CfaGddShutdown
 *    Description         : This function performs the initialisation of
 *                the Generic Device Driver Module of CFA. This
 *                initialization routine should be called
 *                from the init of IFM after we have obtained
 *                the number of physical ports after parsing of
 *                the Config file. This function initializes the FD list
 *                and ifIndex array of the polling table.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : FdTable
 *
 *    Global Variables Modified : FdTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

PUBLIC VOID
CfaGddShutdown (VOID)
{
    OsixDeleteQ (SELF, (const UINT1 *) CFA_PACKET_QUEUE_BCM);
}

#ifdef MBSM_WANTED
/*****************************************************************************
 *    Function Name        : CfaMbsmGddDeInit
 *    Description         : This function performs the shutdown of
 *                the Generic Device Driver Module of CFA. This
 *                shutdown routine should be called
 *                before the protocols are informed about the shutdown.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : FdTable
 *
 *    Global Variables Modified : FdTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
CfaMbsmGddDeInit (tMbsmSlotInfo * pSlotInfo)
{
    /* Re-initialize the Slot info paramaters for the given slot */
    if (CfaCfaMbsmNpSlotDeInit (pSlotInfo) == FNP_FAILURE)
    {
        PRINTF ("[CFA] CfaMbsmNpDeInit returned failure\r\n");
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

#endif /* MBSM_WANTED */

VOID
CfaGddOsRemoveIfFromList (UINT4 u4IfIndex)
{
    (VOID) u4IfIndex;
}

INT4
CfaGddOsProcessRecvEvent (VOID)
{
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddGetOsHwAddr
 *
 *    Description        : Function is called for getting the hardware
 *                         address of the Enet ports at Interface initialisation.
 *                         This function can be called only at port creation time.
 *                         The MAC address is obtained in the same way as it is used
 *                         for IndexMap table initialisation at GddInit.
 *
 *    Input(s)           : UINT4 u4IfIndex.
 *
 *    Output(s)          : UINT1 *au1HwAddr.
 *
 *    Returns            : CFA_SUCCESS if address is obtained,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaGddGetOsHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
    if (CfaIsL3IpVlanInterface (u4IfIndex) == CFA_SUCCESS)
    {
        /* For IVR interfaces, Get the Switch Mac Address from CDB table */
        CfaGetIfHwAddr (u4IfIndex, au1HwAddr);
        return CFA_SUCCESS;
    }

    /* Get the port mac address from NP for each port
     * at port creation & store it in CFA CDB table.
     * Here onwards protocol should refer CDB table 
     * for port mac retrieval.
     */
    CfaNpGetHwAddr (u4IfIndex, au1HwAddr);

    return CFA_SUCCESS;
}

INT4
CfaGddConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption, VOID *pConfigParam)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pConfigParam);
    UNUSED_PARAM (u1ConfigOption);
    return CFA_SUCCESS;
}

INT4
CfaGddSetGddType (UINT4 u4IfIndex)
{
    UINT1               u1IfType;

    CfaGetIfType (u4IfIndex, &u1IfType);

    CFA_GDD_TYPE (u4IfIndex) = u1IfType;
    return (CFA_SUCCESS);
}

PUBLIC INT4
CfaGddEthSockOpen (UINT4 u4IfIndex)
{
    INT4                i4RetVal = CFA_SUCCESS;
    UNUSED_PARAM (u4IfIndex);
    return i4RetVal;
}

PUBLIC INT4
CfaGddEthSockClose (UINT4 u4IfIndex)
{
    INT4                i4RetVal = CFA_SUCCESS;
    UNUSED_PARAM (u4IfIndex);
    return i4RetVal;
}

PUBLIC INT4
CfaGddEthSockRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    INT4                i4RetVal = CFA_FAILURE;

    i4RetVal = CfaNpPortRead (pu1DataBuf, u4IfIndex, pu4PktSize);
    if (i4RetVal == FNP_SUCCESS)
    {
        i4RetVal = CFA_SUCCESS;
    }
    else
    {
        i4RetVal = CFA_FAILURE;
    }
    return i4RetVal;
}

PUBLIC VOID
CfaGddOsAddIfToList (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
}

/* Link up/down changes start */
/*****************************************************************************
 *    Function Name        : CfaGddGetLinkStatus
 *    Description         : This function  retrives the status of the Link
 *    Input(s)            : None.
 *    Output(s)            : None.
 *    Global Variables Referred : FdTable
 *    Global Variables Modified : FdTable
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Use of Recursion        : None.
 *    Returns            : CFA_IF_DOWN if the Link is Down
 *                         else, CFA_IF_UP.
 *
 *****************************************************************************/

PUBLIC UINT1
CfaGddGetLinkStatus (UINT4 u4IfIndex)
{
    return CfaCfaNpGetLinkStatus (u4IfIndex);
}

/*****************************************************************************
 *    Function Name        : CfaGddGetPhyAndLinkStatus
 *    Description         : This function  retrives the status of the Link and
 *                          PHY
 *    Input(s)            : None.
 *    Output(s)            : None.
 *    Global Variables Referred : FdTable
 *    Global Variables Modified : FdTable
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Use of Recursion        : None.
 *    Returns            : CFA_IF_DOWN if the Link  or PHY is Down
 *                         else, CFA_IF_UP both LINK and PHY is up.
 *
 *****************************************************************************/

PUBLIC UINT1
CfaGddGetPhyAndLinkStatus (UINT2 u2IfIndex)
{
    return CfaNpGetPhyAndLinkStatus (u2IfIndex);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthOpen
 *
 *    Description        : This function opens the Ethernet port and stores the
                           descriptor in the interface table. Since the port is 
                           opened already during the registration time, this 
                           function is dummy.
 *
 *    Input(s)            : u4IfIndex - Interface Index
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable (Interface table)
 *                                structure
 *
 *    Global Variables Modified :  gapIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if open succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthOpen (UINT4 u4IfIndex)
{
    if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
    {
        if (CfaNpPortOpen (u4IfIndex) != FNP_SUCCESS)
        {
            return CFA_FAILURE;
        }
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthClose
 *
 *    Description        : First disables the promiscuous mode of 
 *                         the port, if enabled. The multicast address
 *                         list is lost when the port is closed.i
 *                         We dont need to check if the call is a 
 *                         success or not.
 *
 *    Input(s)            : u4IfIndex - Interface index.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable
 *
 *    Global Variables Modified : gapIfTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if close succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthClose (UINT4 u4IfIndex)
{
    if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
    {
        if (CfaNpPortClose (u4IfIndex) != FNP_SUCCESS)
        {
            return CFA_FAILURE;
        }
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthWrite
 *
 *    Description        : Writes the data to the ethernet driver.
 *
 *    Input(s)            : pu1DataBuf - Pointer to the linear buffer.
 *                          u4IfIndex - MIB-2 interface index
 *                          u4PktSize - Size of the buffer.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable (Interface table)
 *                                structure,
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if write succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
#ifdef OPENFLOW_WANTED
    UINT1               u1SubType = CFA_SUBTYPE_SISP_INTERFACE;
#endif

    if (CfaNpPortWrite (pu1DataBuf, u4IfIndex, u4PktSize) != FNP_SUCCESS)
    {
        return CFA_FAILURE;
    }

#ifdef OPENFLOW_WANTED
    CfaGetIfSubType ((UINT4) u4IfIndex, &u1SubType);
    if (u1SubType == CFA_SUBTYPE_OPENFLOW_INTERFACE)
    {
        CFA_IF_SET_OUT_UCAST (u4IfIndex);
        CFA_IF_SET_OUT_OCTETS (u4IfIndex, u4PktSize);
    }
#endif

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthWriteWithPri
 *
 *    Description        : Writes the data to the ethernet driver.
 *
 *    Input(s)            : pu1DataBuf - Pointer to the linear buffer.
 *                          u4IfIndex - MIB-2 interface index
 *                          u4PktSize - Size of the buffer.
 *                          u1Priority - Traffic class on which Pkt to be TXed
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if write succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthWriteWithPri (UINT1 *pData, UINT4 u4IfIndex, UINT4 u4PktLen,
                       UINT1 u1Priority)
{
    if (CfaGddEthWrite (pData, u4IfIndex, u4PktLen) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }
    UNUSED_PARAM (u1Priority);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthRead
 *
 *    Description        : This function reads the data from the ethernet port.
 *
 *    Input(s)            : pu1DataBuf - Pointer to the linear buffer.
 *                          u4IfIndex - IfIndex of the interface
 *                          pu4PktSize - Pointer to the packet size
 *
 *    Output(s)            : pu1DataBuf, pu1PktSize
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    if (CfaNpPortRead (pu1DataBuf, u4IfIndex, pu4PktSize) != FNP_SUCCESS)
    {
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : NpCfaIsVlanTagRequest
 *
 *    Description        : This function will check, whether VLAN tag is
 *                         required for the incoming packet.
 *
 *    Input(s)           : pBuf - Pointer to the linear buffer.
 *
 *    Output(s)          : None. 
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            : CFA_TRUE or CFA_FALSE.
 *
 *****************************************************************************/
PRIVATE INT4
NpCfaIsVlanTagRequest (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tEnetV2Header      *pEthHdr = NULL;
    UINT4               u4StackPort = 0;
    UINT4               u4VlanOffset = CFA_VLAN_TAG_OFFSET;
    UINT4               u4ContextId = VLAN_DEF_CONTEXT_ID;
    UINT4               u4IfIndex;
    INT4                i4RetVal;
    UINT4               u4TempIfIndex = 0;
    UINT2               u2AggId = 0;
    UINT2               u2LenOrType = 0;
    UINT2               u2VlanTag = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2Pvid = 0;
    UINT1               au1GmrpAddr[6] = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x20 };
    UINT1               au1GvrpAddr[6] = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x21 };
    UINT1               au1StpAddr[6] = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x00 };
    UINT1               u1PortAccpFrmType;
    tMacAddr            ProviderGmrpAddr;
    tMacAddr            ProviderLacpAddr;
    tMacAddr            ProviderDot1xAddr;
    tMacAddr            ProviderGvrpAddr;
    tMacAddr            ProviderStpAddr;
    tMacAddr            ProviderMvrpAddr;
    tMacAddr            ProviderMmrpAddr;
    tMacAddr            ProviderEoamAddr;
    tMacAddr            ProviderEcfmAddr;
    tMacAddr            ProviderLldpAddr;
    tMacAddr            ProviderElmiAddr;
    tMacAddr            ProviderIgmpAddr;
    BOOL1               bIsTunnelPort;
    UINT1               u1IfEtherType;
    UINT1               u1BrgPortType;

    /* CFA_GET_IFINDEX should not be used here, because in the packet
     * processing thread, Cfa Interface index is filled in
     * "ModuleData.InterfaceId" data structure.
     */
    u4IfIndex = pBuf->ModuleData.InterfaceId.u4IfIndex;
    CfaGetEthernetType (u4IfIndex, &u1IfEtherType);
    CfaGetRmConnectingPortIfIndex (&u4StackPort);
    if ((u1IfEtherType == CFA_STACK_ENET) || (u4IfIndex == u4StackPort))
    {
        /* If Packets received for Stack interface,
         * then the vlan tag should not be removed */
        return CFA_TRUE;
    }

    if ((CFA_CDB_IF_BRIDGED_IFACE_STATUS (u4IfIndex) == CFA_DISABLED))
    {
        /* If Packets received in Router Port,
         * then the vlan tag should be removed */
        return CFA_FALSE;
    }
    /* If the port belongs to port-channel, then get the port-channel id.
     * If the port is not part of any port-channel, then u2AggId will be
     * same as the given u4IfIndex. */
    if (L2IwfGetPortChannelForPort (u4IfIndex, &u2AggId) == L2IWF_SUCCESS)
    {
        u4TempIfIndex = u2AggId;
    }
    else
    {
        u4TempIfIndex = u4IfIndex;
    }

    pEthHdr = (tEnetV2Header *) CRU_BMC_Get_DataPointer (pBuf);

    L2IwfGetPortVlanTunnelStatus (u4TempIfIndex, &bIsTunnelPort);

    if (bIsTunnelPort == OSIX_TRUE)
    {
        /* 
         * In SDK:5.2.4 bpdus and gvrp packets were received as untagged on 
         * tunnel external ports (tunnel ports). On tunnel internal ports 
         * bpdus and gvrp pkts were received 8100 tagged.
         * In SDK:5.4.3, bpdus and gvrp pkts were received tagged on both
         * tunnel internal and external ports.
         * Hence in case of tunnel external ports, if tagged packets are 
         * received with pvid vlan, then untag them.
         */
        u2LenOrType = OSIX_NTOHS (pEthHdr->u2LenOrType);

        if (u2LenOrType == VLAN_PROTOCOL_ID)
        {

            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2VlanTag,
                                       VLAN_TAG_VLANID_OFFSET, VLAN_TAG_SIZE);

            u2VlanTag = OSIX_NTOHS (u2VlanTag);

            u2VlanId = (tVlanId) (u2VlanTag & VLAN_ID_MASK);

            L2IwfGetVlanPortPvid (u4TempIfIndex, &u2Pvid);

            if (u2VlanId != u2Pvid)
            {
                return CFA_TRUE;
            }

            CfaGetIfBrgPortType (u4TempIfIndex, &u1BrgPortType);

            if ((MEMCMP (pEthHdr->au1DstAddr, au1GvrpAddr, CFA_ENET_ADDR_LEN) ==
                 0)
                || (MEMCMP (pEthHdr->au1DstAddr, au1StpAddr, CFA_ENET_ADDR_LEN)
                    == 0))
            {
                if ((u1BrgPortType == CFA_CNP_PORTBASED_PORT) ||
                    (u1BrgPortType == CFA_CUSTOMER_EDGE_PORT))
                {
                    return CFA_TRUE;
                }
            }
        }
        else
        {
            return CFA_TRUE;
        }
    }

    MEMSET (ProviderGmrpAddr, 0, sizeof (tMacAddr));
    MEMSET (ProviderMvrpAddr, 0, sizeof (tMacAddr));
    MEMSET (ProviderMmrpAddr, 0, sizeof (tMacAddr));
    MEMSET (ProviderDot1xAddr, 0, sizeof (tMacAddr));
    MEMSET (ProviderLacpAddr, 0, sizeof (tMacAddr));
    MEMSET (ProviderGvrpAddr, 0, sizeof (tMacAddr));
    MEMSET (ProviderLldpAddr, 0, sizeof (tMacAddr));
    MEMSET (ProviderElmiAddr, 0, sizeof (tMacAddr));
    MEMSET (ProviderEcfmAddr, 0, sizeof (tMacAddr));
    MEMSET (ProviderEoamAddr, 0, sizeof (tMacAddr));
    MEMSET (ProviderStpAddr, 0, sizeof (tMacAddr));
    MEMSET (ProviderIgmpAddr, 0, sizeof (tMacAddr));

    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_GMRP_PROTO_ID,
                              ProviderGmrpAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_DOT1X_PROTO_ID,
                              ProviderDot1xAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_LACP_PROTO_ID,
                              ProviderLacpAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_GVRP_PROTO_ID,
                              ProviderGvrpAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_STP_PROTO_ID,
                              ProviderStpAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_MVRP_PROTO_ID,
                              ProviderMvrpAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_MMRP_PROTO_ID,
                              ProviderMmrpAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_LLDP_PROTO_ID,
                              ProviderLldpAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_EOAM_PROTO_ID,
                              ProviderEoamAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_ECFM_PROTO_ID,
                              ProviderEcfmAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_ELMI_PROTO_ID,
                              ProviderElmiAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_IGMP_PROTO_ID,
                              ProviderIgmpAddr);

    if ((MEMCMP (pEthHdr->au1DstAddr, au1GmrpAddr, CFA_ENET_ADDR_LEN) == 0) ||
        (MEMCMP (pEthHdr->au1DstAddr, ProviderGvrpAddr, CFA_ENET_ADDR_LEN) == 0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderStpAddr, CFA_ENET_ADDR_LEN) ==
            0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderLacpAddr, CFA_ENET_ADDR_LEN) ==
            0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderDot1xAddr, CFA_ENET_ADDR_LEN)
            == 0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderMvrpAddr, CFA_ENET_ADDR_LEN)
            == 0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderMmrpAddr, CFA_ENET_ADDR_LEN)
            == 0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderLldpAddr, CFA_ENET_ADDR_LEN)
            == 0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderElmiAddr, CFA_ENET_ADDR_LEN)
            == 0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderEcfmAddr, CFA_ENET_ADDR_LEN)
            == 0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderEoamAddr, CFA_ENET_ADDR_LEN)
            == 0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderIgmpAddr, CFA_ENET_ADDR_LEN)
            == 0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderGmrpAddr, CFA_ENET_ADDR_LEN) ==
            0))
    {
        /* GMRP packet - retain VLAN tag */
        return CFA_TRUE;
    }

    if (VlanGetTagLenInFrame (pBuf, u4IfIndex, &u4VlanOffset) == VLAN_SUCCESS)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2LenOrType,
                                   u4VlanOffset, CFA_ENET_TYPE_OR_LEN);
        u2LenOrType = OSIX_NTOHS (u2LenOrType);
    }

    VlanNpRetainTag (u4IfIndex, pEthHdr->au1DstAddr, u2LenOrType, &i4RetVal);
    if (i4RetVal == VLAN_TRUE)
    {
        return CFA_TRUE;
    }

    /* Retain tag for all the L3 protocols  and Dot1x packets as well */
    if ((u2LenOrType == CFA_ENET_IPV4) || (u2LenOrType == CFA_ENET_ARP) ||
        (u2LenOrType == CFA_ENET_RARP) || (u2LenOrType == CFA_ENET_IPV6)
        || (u2LenOrType == CFA_ENET_MPLS))
    {
        /* Most hardwares add a vlan tag to the received untagged frames before sending to
           the CPU. For the port that accepts only untagged & priority tagged frame, the
           Vlan ID in the tags needs to be removed */
        L2IwfGetVlanPortAccpFrmType (u4IfIndex, &u1PortAccpFrmType);
        if (u1PortAccpFrmType ==
            VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES)
        {
            return CFA_FALSE;
        }
        return CFA_TRUE;
    }

    return CFA_FALSE;
}

/*****************************************************************************
 *
 *    Function Name       :CfaGddTxPktOnVlanMemberPorts 
 *
 *    Description         :This routine transmits the given Frame on the member
 *                         ports of the given Vlan.
 *
 *    Input(s)            : u4L2ContextId - L2 Context Id to which VlanId is 
 *                                          associated
 *                        : pu1DataBuf  - Pointer to the frame buffer 
 *                          u4PktSize   - Size of the frame 
 *                          VlanId      - Vlan on which the frame is to be 
 *                                        transmitted
 *                          bBcast      - Whether this frame is to be 
 *                                        broadcasted  
 *
 *    Output(s)           : None
 *
 *    Global Variables Referred : None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/ CFA_FAILURE.
 *****************************************************************************/

INT4
CfaGddTxPktOnVlanMemberPortsInCxt (UINT4 u4L2ContextId,
                                   UINT1 *pu1DataBuf, tVlanId VlanId,
                                   BOOL1 bBcast, UINT4 u4PktSize)
{
    tPortList          *pTagPorts = NULL;
    tPortList          *pUnTagPorts = NULL;
    tMacAddr            DestAddr;
    tCfaVlanInfo        VlanInfo;
    UINT4               u4BridgeMode;
#ifdef NPAPI_WANTED
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2EtherType = 0;
    UINT1               u1PortFlag;
#endif
    UINT4               u4OutPort;
    BOOL1               bIsTag;
    INT4                i4RetVal;
    UINT1              *pu1TempBuf = NULL;
    UINT1               TempDataBuf[CFA_ENET_MIN_UNTAGGED_FRAME_SIZE];
    UINT4               u4MinFrameMtu = 0;
    INT4                i4RetStat = CFA_SUCCESS;
    UINT1               u1PaddingDone = CFA_FALSE;
    MEMCPY (DestAddr, pu1DataBuf, CFA_ENET_ADDR_LEN);

    MEMSET (TempDataBuf, 0, sizeof (TempDataBuf));
    VlanInfo.u2VlanId = VlanId;
#ifdef NPAPI_WANTED
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
#ifdef MBSM_WANTED
        /* If the packet belongs to STACK IVR, then send the
         * packet via stack port using ATP*/
        if (VlanId == CFA_DEFAULT_STACK_VLAN_ID)
        {
            if (CfaMbsmNpTxOnStackInterface (pu1DataBuf, u4PktSize) ==
                FNP_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddTxPktOnVlanMemberPorts - "
                         "Unable to tx on stack interface.\n");
                return CFA_FAILURE;
            }
            return CFA_SUCCESS;
        }
#endif
    }
#endif

    pTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        return CFA_FAILURE;
    }

    pUnTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pUnTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        return CFA_FAILURE;
    }

    MEMSET (*pTagPorts, 0, sizeof (tPortList));
    MEMSET (*pUnTagPorts, 0, sizeof (tPortList));
    i4RetVal = VlanIvrGetTxPortOrPortListInCxt (u4L2ContextId,
                                                DestAddr, VlanId, bBcast,
                                                &u4OutPort, &bIsTag, *pTagPorts,
                                                *pUnTagPorts);

    if (i4RetVal == VLAN_FORWARD)
    {
        /*ASSUMPTION: The Packet reached here will be untagged always.
         * Since bcoz this thread is called only for the outgoing packet
         * from IP.*/

        u4MinFrameMtu = CFA_ENET_MIN_UNTAGGED_FRAME_SIZE;

        if (u4PktSize < u4MinFrameMtu)
        {
            pu1TempBuf = pu1DataBuf;
            MEMCPY (TempDataBuf, pu1DataBuf, u4PktSize);
            pu1DataBuf = TempDataBuf;
            u4PktSize = u4MinFrameMtu;
            u1PaddingDone = CFA_TRUE;
        }

        CfaTxFillVlanInfo (DestAddr, u4OutPort,
                           bIsTag, *pTagPorts, *pUnTagPorts, &VlanInfo);
        if (L2IwfGetBridgeMode (L2IWF_DEFAULT_CONTEXT, &u4BridgeMode) ==
            L2IWF_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaGddWrite - "
                     "L2IWF-GetBridgeMode -  FAILURE.\n");
            i4RetStat = CFA_FAILURE;
        }
        else
        {
#ifdef NPAPI_WANTED
            if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
            {
                if ((u4BridgeMode == L2IWF_CUSTOMER_BRIDGE_MODE) ||
                    (u4BridgeMode == L2IWF_PROVIDER_BRIDGE_MODE))
                {
                    if (CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo)
                        != FNP_SUCCESS)
                    {
                        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                 "Error in CfaGddWrite - "
                                 "Unsuccessful Driver Write -  FAILURE.\n");
                        i4RetStat = CFA_FAILURE;
                    }
                }
                else if ((u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE) ||
                         (u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE))
                {
                    if (u4OutPort != VLAN_INVALID_PORT)
                    {
                        if (CfaHwL3VlanIntfWrite
                            (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                        {
                            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                     "Error in CfaGddWrite - "
                                     "Unsuccessful Driver Write -  FAILURE.\n");
                            i4RetStat = CFA_FAILURE;
                        }
                    }
                    else
                    {
                        for (u2ByteInd = 0; u2ByteInd < BRG_PORT_LIST_SIZE;
                             u2ByteInd++)
                        {
                            if ((*pTagPorts)[u2ByteInd] == 0)
                            {
                                continue;
                            }

                            u1PortFlag = (*pTagPorts)[u2ByteInd];

                            for (u2BitIndex = 0;
                                 ((u2BitIndex < BITS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
                            {
                                if ((u1PortFlag & 0x80) != 0)
                                {
                                    VlanInfo.u2PktType = CFA_NP_KNOWN_UCAST_PKT;

                                    VlanInfo.unPortInfo.TxUcastPort.u2TxPort =
                                        (UINT2) ((u2ByteInd * BITS_PER_BYTE) +
                                                 u2BitIndex + 1);

                                    u4OutPort =
                                        VlanInfo.unPortInfo.TxUcastPort.
                                        u2TxPort;

#ifdef VLAN_WANTED
                                    VlanGetPortEtherType (u4OutPort,
                                                          &u2EtherType);
#endif
                                    VlanInfo.unPortInfo.TxUcastPort.
                                        u2EtherType = u2EtherType;

                                    VlanInfo.unPortInfo.TxUcastPort.u1Tag =
                                        CFA_NP_TAGGED;

                                    CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize,
                                                          VlanInfo);
                                }
                                u1PortFlag = (UINT1) (u1PortFlag << 1);
                            }
                        }
                        MEMSET (VlanInfo.unPortInfo.TxPorts.pTagPorts, 0,
                                sizeof (tPortList));
                        VlanInfo.u2PktType = CFA_NP_UNKNOWN_UCAST_PKT;
                        /* 
                         * In 802.1ad Bridges, for untagged ports, the tag ethertype 
                         * need not be filled in based on the port ethertype. 
                         * Hence calling the API for packet transmission on L3 
                         * interfaces directly.
                         */
                        if (CfaHwL3VlanIntfWrite
                            (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                        {
                            i4RetStat = CFA_FAILURE;
                        }
                    }
                }
            }
#else
            if (CfaL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo) !=
                CFA_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddWrite - "
                         "Unsuccessful Driver Write -  FAILURE.\n");
                i4RetStat = CFA_FAILURE;

            }
#endif
        }
        if (u1PaddingDone == CFA_TRUE)
        {
            pu1DataBuf = pu1TempBuf;
        }
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pUnTagPorts));
        return i4RetStat;
    }

    FsUtilReleaseBitList ((UINT1 *) pTagPorts);
    FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pTagPorts));
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pUnTagPorts));
    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddGetLnxIntfnameForPort
 *
 *    Description          : This function Returns LinuxEth Name Based on Slot
 *                           Referred.
 *    Input(s)            :  u4IfIndex - Interface index
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure
 *
 *    Global Variables Modified : gaIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : EthName if Slot is found Else NULL
 *
 *****************************************************************************/
UINT1              *
CfaGddGetLnxIntfnameForPort (UINT2 u2Index)
{
    INT4                i4Index;
    UINT1               au1Temp[CFA_MAX_PORT_NAME_LENGTH];

    if (u2Index == 0)
    {
        return NULL;
    }

    for (i4Index = 0; i4Index < SYS_DEF_MAX_PHYSICAL_INTERFACES; i4Index++)
    {
        MEMSET (au1Temp, 0, CFA_MAX_PORT_NAME_LENGTH);

        STRCPY (au1Temp, au1IntMapTable[i4Index]);

        if (MEMCMP (CFA_GDD_PORT_NAME (u2Index), au1Temp,
                    STRLEN (CFA_GDD_PORT_NAME (u2Index))) == 0)
        {
            return au1IntMapTable[i4Index][1];
        }
    }
    return NULL;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddSetLnxIntfnameForPort
 *
 *    Description          : This function sets Interface Name for the interface
 *                           index.
 *    Input(s)            :  u4IfIndex - Interface index
 *                           pu1IfName - Interface name
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure
 *
 *    Global Variables Modified : gaIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : NULL
 *
 *****************************************************************************/
VOID
CfaGddSetLnxIntfnameForPort (UINT4 u4Index, UINT1 *pu1IfName)
{

    if (u4Index == 0)
    {
        return;
    }

    STRNCPY (au1IntMapTable[u4Index], CFA_GDD_PORT_NAME (u4Index),
             CFA_MAX_PORT_NAME_LENGTH);

    STRNCPY (au1IntMapTable[u4Index][1], pu1IfName, CFA_MAX_PORT_NAME_LENGTH);
    CfaSetIfName (u4Index, pu1IfName);
    return;
}
