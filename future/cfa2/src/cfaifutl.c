/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: cfaifutl.c,v 1.8 2013/07/04 12:54:57 siva Exp $
 *
 * Description: This file contains the wrappers required for
 *              supporting the flexible interface index changes.
 *******************************************************************/

#define _CFAIFUTL_C_

#include "cfainc.h"

tCfaIfDb            gCfaGapIfTable;
/*****************************************************************************/
/* Function Name      : CfaIfUtlAssert                                       */
/*                                                                           */
/* Description        : This function is used to assert the exception        */
/*                                                                           */
/* Input(s)           : InterfaceIndex                                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
CfaIfUtlAssert (UINT4 u4IfIndex)
{
    FSAP_ASSERT (0);
    UNUSED_PARAM (u4IfIndex);
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlCreateCdbMemPool                             */
/*                                                                           */
/* Description        : This function is used to get the CDB mempool         */
/*                      When CDB is used as an array this function should    */
/*                      just return                                          */
/*                                                                           */
/* Input(s)           : VOID                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
CfaIfUtlCreateCdbMemPool (VOID)
{
    CFA_DS_LOCK ();
    if (CFA_IF_DB != CFA_IFDB_ARRAY)
    {
        CFA_CDB_IF_MEMPOOL = CFAMemPoolIds[MAX_CFA_DEF_INTF_IN_SYS_SIZING_ID];
    }
    CFA_DS_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlCreateIfDb                                   */
/*                                                                           */
/* Description        : This function creates the specified IfDb             */
/*                                                                           */
/* Input(s)           : Cfa RBtree type to be created                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gCfaCdbIfTable                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIfUtlCreateIfDb (tCfaIfDbReqType eCfaIfDbReqType)
{
    CFA_DBG (CFA_TRC_ALL, CFA_IF, "ENTER CfaIfUtlCreateRBTree\n");

    if (CFA_IF_DB == CFA_IFDB_ARRAY)
    {
        return CFA_SUCCESS;
    }

    switch (eCfaIfDbReqType)
    {

        case CFA_CDB_IFDB_REQ:

            CFA_DS_LOCK ();

            gCfaCdbIfTable =
                RBTreeCreateEmbedded (0,
                                      (tCfaIfDbCmpFn) CfaIfUtlIfDbIndicesCmp);
            if (NULL == gCfaCdbIfTable)
            {
                CFA_DBG (CFA_TRC_ALL, CFA_IF,
                         "Failure in creating CDB RBTree\n");

                CFA_DS_UNLOCK ();

                return CFA_FAILURE;
            }

            CFA_DS_UNLOCK ();
            break;

        case CFA_GAP_IFDB_REQ:

            gCfaGapIfTable =
                RBTreeCreateEmbedded (0,
                                      (tCfaIfDbCmpFn)
                                      CfaIfUtlGapIfDbIndicesCmp);

            if (NULL == gCfaGapIfTable)
            {
                CFA_DBG (CFA_TRC_ALL, CFA_IF,
                         "Failure in creating GAPIF RBTree\n");

                return CFA_FAILURE;
            }

            break;

        default:
            break;
    }

    CFA_DBG (CFA_TRC_ALL, CFA_IF, "EXIT CfaIfUtlCreateRBTree\n");

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlDestroyIfDb                                  */
/*                                                                           */
/* Description        : This function destroys the specified IfDb            */
/*                                                                           */
/* Input(s)           : Cfa RBtree type to be destroyed                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gCfaCdbIfTable                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gCfaCdbIfTable                                       */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
CfaIfUtlDestroyIfDb (tCfaIfDbReqType eCfaIfDbReqType)
{
    tCfaIfInfo         *pCfaIfInfoEntry = NULL;
    tCfaIfInfo         *pCfaIfInfoNextEntry = NULL;
    tIfInfoStruct      *pCfaGapIfInfoEntry = NULL;
    tIfInfoStruct      *pCfaGapIfInfoNextEntry = NULL;

    CFA_DBG (CFA_TRC_ALL, CFA_IF, "ENTER CfaIfUtlDestroyRBTree\n");

    if (CFA_IF_DB == CFA_IFDB_ARRAY)
    {
        return;
    }

    switch (eCfaIfDbReqType)
    {
        case CFA_CDB_IFDB_REQ:

            CFA_DS_LOCK ();

            if (NULL != gCfaCdbIfTable)
            {
                /* Scan through all entries and remove */
                pCfaIfInfoEntry =
                    (tCfaIfInfo *) RBTreeGetFirst (gCfaCdbIfTable);

                while (NULL != pCfaIfInfoEntry)
                {
                    RBTreeRemove (gCfaCdbIfTable,
                                  (tCfaIfDbElem *) pCfaIfInfoEntry);

                    MemReleaseMemBlock (CFA_CDB_IF_MEMPOOL,
                                        (UINT1 *) pCfaIfInfoEntry);

                    pCfaIfInfoNextEntry =
                        (tCfaIfInfo *) RBTreeGetNext (gCfaCdbIfTable,
                                                      (tCfaIfDbElem *)
                                                      pCfaIfInfoEntry, NULL);

                    pCfaIfInfoEntry = pCfaIfInfoNextEntry;
                }
                RBTreeDestroy (gCfaCdbIfTable, NULL, 0);
                gCfaCdbIfTable = NULL;
            }

            CFA_DS_UNLOCK ();
            break;
        case CFA_GAP_IFDB_REQ:

            if (NULL != gCfaGapIfTable)
            {
                /* Scan through all entries and remove */
                pCfaGapIfInfoEntry =
                    (tIfInfoStruct *) RBTreeGetFirst (gCfaGapIfTable);

                while (NULL != pCfaGapIfInfoEntry)
                {
                    RBTreeRemove (gCfaGapIfTable,
                                  (tIfInfoStruct *) pCfaGapIfInfoEntry);

                    MemReleaseMemBlock (CFA_IF_MEMPOOL,
                                        (UINT1 *) pCfaGapIfInfoEntry);

                    pCfaGapIfInfoNextEntry =
                        (tIfInfoStruct *) RBTreeGetNext (gCfaGapIfTable,
                                                         (tIfInfoStruct *)
                                                         pCfaGapIfInfoEntry,
                                                         NULL);

                    pCfaGapIfInfoEntry = pCfaGapIfInfoNextEntry;
                }
                RBTreeDestroy (gCfaGapIfTable, NULL, 0);
                gCfaGapIfTable = NULL;
            }
            break;
        default:
            break;
    }

    CFA_DBG (CFA_TRC_ALL, CFA_IF, "EXIT CfaIfUtlDestroyRBTree\n");
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlIfDbIndicesCmp                               */
/*                                                                           */
/* Description        : Compare function for the CfaCdbIf RBTree             */
/*                                                                           */
/* Input(s)           : RBElement pointers for comparison                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Positive/Negative integer value                      */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIfUtlIfDbIndicesCmp (tCfaIfDbElem * pRBElem1, tCfaIfDbElem * pRBElem2)
{

    tCfaIfInfo         *pCfaIfInfoEntry1 = (tCfaIfInfo *) pRBElem1;
    tCfaIfInfo         *pCfaIfInfoEntry2 = (tCfaIfInfo *) pRBElem2;

    if (pCfaIfInfoEntry1->u4CfaIfIndex > pCfaIfInfoEntry2->u4CfaIfIndex)
    {
        return 1;
    }
    else if (pCfaIfInfoEntry1->u4CfaIfIndex < pCfaIfInfoEntry2->u4CfaIfIndex)
    {
        return -1;
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlGapIfDbIndicesCmp                            */
/*                                                                           */
/* Description        : Compare function for the GapIf RBTree                */
/*                                                                           */
/* Input(s)           : RBElement pointers for comparison                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Positive/Negative integer value                      */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIfUtlGapIfDbIndicesCmp (tIfInfoStruct * pRBElem1, tIfInfoStruct * pRBElem2)
{
    tIfInfoStruct      *pCfaGapIfInfoEntry1 = (tIfInfoStruct *) pRBElem1;
    tIfInfoStruct      *pCfaGapIfInfoEntry2 = (tIfInfoStruct *) pRBElem2;

    if (pCfaGapIfInfoEntry1->u4CfaGapIfIndex >
        pCfaGapIfInfoEntry2->u4CfaGapIfIndex)
    {
        return 1;
    }
    else if (pCfaGapIfInfoEntry1->u4CfaGapIfIndex <
             pCfaGapIfInfoEntry2->u4CfaGapIfIndex)
    {
        return -1;
    }
    return 0;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlIfDbAdd                                      */
/*                                                                           */
/* Description        : Adds a node to the RBTree                            */
/*                                                                           */
/* Input(s)           : IfIndex, tCfaIfDbReqType                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : RBTree pointer                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIfUtlIfDbAdd (UINT4 u4CfaIfIndex, tCfaIfDbReqType eCfaIfDbReqType)
{
    tCfaIfInfo         *pCfaIfInfoEntry = NULL;
    tIfInfoStruct      *pCfaGapIfInfoEntry = NULL;
    INT4                i4RetVal = 0;
    if (CFA_CDB_IFDB_REQ == eCfaIfDbReqType)
    {
        if (CFA_IF_DB == CFA_IFDB_ARRAY)
        {
            return CfaIfUtlIfArrayDbAdd (u4CfaIfIndex);
        }

        CFA_DS_LOCK ();

        pCfaIfInfoEntry = (tCfaIfInfo *) MemAllocMemBlk (CFA_CDB_IF_MEMPOOL);

        if (NULL == pCfaIfInfoEntry)
        {
            CFA_DS_UNLOCK ();
            return CFA_FAILURE;
        }

        MEMSET (pCfaIfInfoEntry, 0, sizeof (tCfaIfInfo));

        pCfaIfInfoEntry->u4CfaIfIndex = u4CfaIfIndex;

        if (RB_SUCCESS !=
            RBTreeAdd (gCfaCdbIfTable, (tCfaIfDbElem *) pCfaIfInfoEntry))
        {
            CFA_DBG (CFA_TRC_ALL, CFA_IF, "Failure in CDB RBTree add\n");
            MemReleaseMemBlock (CFA_CDB_IF_MEMPOOL, (UINT1 *) pCfaIfInfoEntry);
            CFA_DS_UNLOCK ();
            return CFA_FAILURE;
        }

        CFA_DS_UNLOCK ();
    }
    if (CFA_GAP_IFDB_REQ == eCfaIfDbReqType)
    {
        pCfaGapIfInfoEntry = (tIfInfoStruct *) MemAllocMemBlk (CFA_IF_MEMPOOL);

        if (NULL == pCfaGapIfInfoEntry)
        {
            return CFA_FAILURE;
        }

        MEMSET (pCfaGapIfInfoEntry, 0, sizeof (tIfInfoStruct));

        pCfaGapIfInfoEntry->u4CfaGapIfIndex = u4CfaIfIndex;

        if (CFA_IF_DB == CFA_IFDB_ARRAY)
        {
            i4RetVal = CfaIfUtlGapIfArrayDbAdd (pCfaGapIfInfoEntry);
        }
        else
        {
            i4RetVal = (INT4) RBTreeAdd (gCfaGapIfTable,
                                         (tIfInfoStruct *) pCfaGapIfInfoEntry);
        }
        if (i4RetVal != CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_IF, "Failure in GAPIF RBTree add\n");
            MemReleaseMemBlock (CFA_IF_MEMPOOL, (UINT1 *) pCfaGapIfInfoEntry);
            return CFA_FAILURE;
        }

    }
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlIfTypeDbNodeInit                             */
/*                                                                           */
/* Description        : Inits the DLL node                                   */
/*                                                                           */
/* Input(s)           : u4IfIndex, CfaIfTypeDbReq                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIfUtlIfTypeDbNodeInit (UINT4 u4IfIndex, tCfaIfTypeDbReq eCfaIfTypeDbReq)
{
    tCfaIfInfo         *pCfaIfInfoEntry = NULL;
    tCfaIfTypeNode     *pCfaIfTypeNode = NULL;

    if (CFA_IF_DB == CFA_IFDB_ARRAY)
    {
        return CFA_SUCCESS;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IFTYPE_REQ == eCfaIfTypeDbReq)
    {
        pCfaIfInfoEntry = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

        if (NULL == pCfaIfInfoEntry)
        {
            CFA_DS_UNLOCK ();
            return CFA_FAILURE;
        }

        pCfaIfTypeNode = &(pCfaIfInfoEntry->CfaIfTypeNode);

        TMO_DLL_Init_Node (pCfaIfTypeNode);
    }

    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlIfTypeDbAddNode                              */
/*                                                                           */
/* Description        : Adds the node to the DLL                             */
/*                                                                           */
/* Input(s)           : u4IfIndex, CfaIfTypeDbReq                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIfUtlIfTypeDbAddNode (UINT4 u4IfIndex, tCfaIfTypeDbReq eCfaIfTypeDbReq)
{
    tCfaIfInfo         *pCfaIfInfoEntry = NULL;
    tCfaIfTypeDb       *pCfaCurIfTypeDb = NULL;
    tCfaIfTypeNode     *pCfaIfTypeNode = NULL;
    tCfaIfTypeDb       *pCfaIfTypeDb = NULL;

    if (CFA_IF_DB == CFA_IFDB_ARRAY)
    {
        return CFA_SUCCESS;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IFTYPE_REQ == eCfaIfTypeDbReq)
    {
        pCfaIfInfoEntry = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

        if (NULL == pCfaIfInfoEntry)
        {
            CFA_DS_UNLOCK ();
            return CFA_FAILURE;
        }

        pCfaCurIfTypeDb = CFA_CDB_IF_TYPE_DB;

        pCfaIfTypeDb = &(pCfaCurIfTypeDb[(pCfaIfInfoEntry->u1IfType) - 1]);

        pCfaIfTypeNode = &((pCfaIfInfoEntry)->CfaIfTypeNode);

        TMO_DLL_Add (pCfaIfTypeDb, pCfaIfTypeNode);
    }

    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : CfaIfUtlIfTypeDbInit                                 */
/*                                                                           */
/* Description        : Sets the DLL pointers to NULL                        */
/*                                                                           */
/* Input(s)           : CfaIfTypeDbReq                                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
CfaIfUtlIfTypeDbInit (tCfaIfTypeDbReq eCfaIfTypeDbReq)
{
    UINT4               u4IfTypeIdx = 0;
    tCfaIfTypeDb       *pCfaIfTypeDb = NULL;

    if (CFA_IF_DB == CFA_IFDB_ARRAY)
    {
        return;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IFTYPE_REQ == eCfaIfTypeDbReq)
    {
        /* Assign the global interface type db pointer to
         * pCfaIfTypeDb. CFA_CDB_IF_TYPE_DB is used across
         * code to access the global CDB interface type db pointer */

        pCfaIfTypeDb = CFA_CDB_IF_TYPE_DB;
    }

    for (u4IfTypeIdx = 0; u4IfTypeIdx < CFA_MAX_IF_TYPES; u4IfTypeIdx++)
    {
        TMO_DLL_Init (&(pCfaIfTypeDb[u4IfTypeIdx]));
    }

    CFA_DS_UNLOCK ();

    return;

}

/*****************************************************************************/
/* Function Name      : CfaIfUtlIfTypeDbUpdate                               */
/*                                                                           */
/* Description        : Removes the node from the previous IfType location   */
/*                      and adds it to the appropriate new IfType location   */
/*                      in the DLL                                           */
/*                                                                           */
/* Input(s)           : IfIndex, IfType, CfaIfTypeDbReq                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : RBTree pointer                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIfUtlIfTypeDbUpdate (UINT4 u4IfIndex, UINT1 u1IfType,
                        tCfaIfTypeDbReq eCfaIfTypeDbReq)
{
    tCfaIfInfo         *pCfaIfInfoEntry = NULL;
    tCfaIfTypeDb       *pCfaCurIfTypeDb = NULL;
    tCfaIfTypeDb       *pCfaIfTypeDb = NULL;
    tCfaIfTypeNode     *pCfaIfTypeNode = NULL;
    UINT1               u1TempIfType = 0;

    if (CFA_IF_DB == CFA_IFDB_ARRAY)
    {
        return CFA_SUCCESS;
    }

    CFA_DS_LOCK ();
    if (CFA_CDB_IFTYPE_REQ == eCfaIfTypeDbReq)
    {
        /* Assign the global interface type db pointer to
         * pCfaCurIfTypeDb. CFA_CDB_IF_TYPE_DB is used across
         * code to access the global CDB interface type db pointer */
        pCfaCurIfTypeDb = CFA_CDB_IF_TYPE_DB;

        pCfaIfInfoEntry = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

        if (NULL == pCfaIfInfoEntry)
        {
            CFA_DS_UNLOCK ();
            return CFA_FAILURE;
        }

        u1TempIfType = (pCfaIfInfoEntry)->u1IfType;

        if (u1IfType != u1TempIfType)
        {
            pCfaIfTypeDb = &(pCfaCurIfTypeDb[(pCfaIfInfoEntry->u1IfType) - 1]);

            pCfaIfTypeNode = &(pCfaIfInfoEntry->CfaIfTypeNode);

            if (TMO_DLL_Count (pCfaIfTypeDb) != 0)
            {
                TMO_DLL_Delete (pCfaIfTypeDb, pCfaIfTypeNode);
            }

            if ((u1IfType > 0) && (u1IfType < CFA_MAX_IF_TYPES))
            {
                TMO_DLL_Add (&(pCfaCurIfTypeDb[u1IfType - 1]), pCfaIfTypeNode);
            }
        }
    }
    CFA_DS_UNLOCK ();

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlIfTypeDbRem                                  */
/*                                                                           */
/* Description        : Removes the node from the DLL                        */
/*                                                                           */
/* Input(s)           : u4IfIndex, CfaIfTypeDbReq                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIfUtlIfTypeDbRem (UINT4 u4CfaIfIndex, tCfaIfTypeDbReq eCfaIfTypeDbReq)
{
    tCfaIfInfo         *pCfaIfInfoEntry = NULL;
    tCfaIfTypeDb       *pCfaCurIfTypeDb = NULL;
    tCfaIfTypeNode     *pCfaIfTypeNode = NULL;
    tCfaIfTypeDb       *pCfaIfTypeDb = NULL;

    if (CFA_IF_DB == CFA_IFDB_ARRAY)
    {
        return CFA_SUCCESS;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IFTYPE_REQ == eCfaIfTypeDbReq)
    {
        /* Assign the global interface type db pointer to
         * pCfaCurIfTypeDb. CFA_CDB_IF_TYPE_DB is used across
         * code to access the global CDB interface type db pointer */

        pCfaCurIfTypeDb = CFA_CDB_IF_TYPE_DB;

        pCfaIfInfoEntry = CfaIfUtlCdbGetIfDbEntry (u4CfaIfIndex);

        if (NULL == pCfaIfInfoEntry)
        {
            CFA_DS_UNLOCK ();
            return CFA_FAILURE;
        }

        pCfaIfTypeDb = &(pCfaCurIfTypeDb[(pCfaIfInfoEntry->u1IfType) - 1]);

        pCfaIfTypeNode = &(pCfaIfInfoEntry->CfaIfTypeNode);

        TMO_DLL_Delete (pCfaIfTypeDb, pCfaIfTypeNode);
    }

    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlIfDbRem                                      */
/*                                                                           */
/* Description        : Removes a node from the CDB RBTree and GapIf RBTree  */
/*                                                                           */
/* Input(s)           :  u4IfIndex, tCfaIfTypeDbReq                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : RBTree pointer                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIfUtlIfDbRem (UINT4 u4IfIndex, tCfaIfDbReqType eCfaIfDbReqType)
{
    tCfaIfInfo         *pCfaIfInfoEntry = NULL;
    tIfInfoStruct      *pCfaGapIfInfoEntry = NULL;

    if (CFA_CDB_IFDB_REQ == eCfaIfDbReqType)
    {
        if (CFA_IF_DB == CFA_IFDB_ARRAY)
        {
            return CfaIfUtlIfArrayDbRem (u4IfIndex);
        }

        CFA_DS_LOCK ();
        pCfaIfInfoEntry = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

        if (NULL == pCfaIfInfoEntry)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_IF, "Failure in CDB RBTree Get\n");
            CFA_DS_UNLOCK ();
            return CFA_FAILURE;
        }

        if (RBTreeRemove (gCfaCdbIfTable, (tCfaIfDbElem *) pCfaIfInfoEntry)
            != RB_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_IF, "Failure in CDB RBTree remove\n");
            CFA_DS_UNLOCK ();
            return CFA_FAILURE;
        }

        MemReleaseMemBlock (CFA_CDB_IF_MEMPOOL, (UINT1 *) pCfaIfInfoEntry);
        CFA_DS_UNLOCK ();
    }
    if (CFA_GAP_IFDB_REQ == eCfaIfDbReqType)
    {

        pCfaGapIfInfoEntry = CfaIfUtlGetGapIfDbEntry (u4IfIndex);

        if (NULL == pCfaGapIfInfoEntry)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_IF, "Failure in GAP RBTree Get\n");
            return CFA_FAILURE;
        }

        if (CFA_IF_DB == CFA_IFDB_RB)
        {
            if (RBTreeRemove
                (gCfaGapIfTable,
                 (tIfInfoStruct *) pCfaGapIfInfoEntry) != RB_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ALL, CFA_IF, "Failure in GAP RBTree remove\n");
                return CFA_FAILURE;
            }

        }

        if ((MemReleaseMemBlock (CFA_IF_MEMPOOL, (UINT1 *) pCfaGapIfInfoEntry))
            != MEM_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_IF,
                     "Error in releasing ifEntry MemBlock.\n");
            return CFA_FAILURE;
        }
        if (CFA_IF_DB == CFA_IFDB_ARRAY)
        {
            return CfaIfUtlGapIfArrayDbRem (u4IfIndex);
        }

    }

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlIfDbGet                                      */
/*                                                                           */
/* Description        : Gets a node from the RBTree                          */
/*                      It is expected that the calling function takes the   */
/*                      CFA_DS_LOCK                                          */
/*                                                                           */
/* Input(s)           : tCfaList, tCfaIfDbElem                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Pointer to node OR NULL                              */
/*                                                                           */
/*****************************************************************************/
tCfaIfDbElem       *
CfaIfUtlIfDbGet (tCfaIfDb CfaIfDb, tCfaIfDbElem * pCfaIfDbElem)
{
    /* It is expected that the calling function takes the CFA_DS_LOCK
     */

    tCfaIfDbElem       *pCfaCurIfDbElem = NULL;
    UINT4               u4CfaIfIndex = 0;

    if (CFA_IF_DB == CFA_IFDB_ARRAY)
    {
        u4CfaIfIndex = ((tCfaIfInfo *) (pCfaIfDbElem))->u4CfaIfIndex;
        return (CfaIfUtlIfArrayDbGet (u4CfaIfIndex));
    }

    pCfaCurIfDbElem = RBTreeGet (CfaIfDb, pCfaIfDbElem);

    return pCfaCurIfDbElem;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlCdbGet                                       */
/*                                                                           */
/* Description        : Gets the node from the CDB RBTree. This is           */
/*                      used only from the CDB related MACRO's since an      */
/*                      assert will be done in this call if the node does    */
/*                      not exist.                                           */
/*                      It is expected that the calling function takes the   */
/*                      CFA_DS_LOCK                                          */
/*                                                                           */
/* Input(s)           : u4IfIndex                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Pointer to node OR NULL                              */
/*                                                                           */
/*****************************************************************************/
tCfaIfInfo         *
CfaIfUtlCdbGet (UINT4 u4IfIndex)
{
    /* It is expected that the calling function takes the CFA_DS_LOCK
     */

    tCfaIfInfo         *pCfaIfInfo = NULL;

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if ((NULL == pCfaIfInfo) && (CFA_IF_DB != CFA_IFDB_ARRAY))
    {
        /* This indicates an issue. Ideally should not happen */
        CfaIfUtlAssert (u4IfIndex);
    }
    return pCfaIfInfo;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlCdbGetIfDbEntry                              */
/*                                                                           */
/* Description        : Gets the node from the CDB RBTree and returns the    */
/*                      same. This function will be called from cfacdb.c     */
/*                      from places where it is needed to check if the node  */
/*                      exists or not before proceeding.                     */
/*                      It is expected that the calling function takes the   */
/*                      CFA_DS_LOCK                                          */
/*                                                                           */
/* Input(s)           : u4IfIndex                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Pointer to node OR NULL                              */
/*                                                                           */
/*****************************************************************************/
tCfaIfInfo         *
CfaIfUtlCdbGetIfDbEntry (UINT4 u4IfIndex)
{
    /* It is expected that the calling function takes the CFA_DS_LOCK
     */

    tCfaIfInfo          CfaIfInfo;
    tCfaIfInfo         *pCfaIfInfo = NULL;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    CfaIfInfo.u4CfaIfIndex = u4IfIndex;

    pCfaIfInfo = (tCfaIfInfo *) CfaIfUtlIfDbGet (gCfaCdbIfTable, &CfaIfInfo);

    return pCfaIfInfo;
}

/* Function definitions towards array based retrievals */

/*****************************************************************************/
/* Function Name      : CfaIfUtlIfArrayDbGet                                 */
/*                                                                           */
/* Description        : Gets the array location corresponding to the         */
/*                      ifIndex                                              */
/*                      It is expected that the calling function takes the   */
/*                      CFA_DS_LOCK                                          */
/*                                                                           */
/* Input(s)           : u4CfaIfIndex                                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Pointer to node OR NULL                              */
/*                                                                           */
/*****************************************************************************/
tCfaIfDbElem       *
CfaIfUtlIfArrayDbGet (UINT4 u4CfaIfIndex)
{
    /* Calling function is expected to take the lock */
    tCfaIfDbElem       *pCfaCurIfDbElem = NULL;

    pCfaCurIfDbElem = (tCfaIfDbElem *) & gaCfaCommonIfInfo[u4CfaIfIndex - 1];

    return pCfaCurIfDbElem;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlIfArrayDbAdd                                 */
/*                                                                           */
/* Description        : Populates the ifIndex in the array                   */
/*                                                                           */
/* Input(s)           : IfIndex                                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIfUtlIfArrayDbAdd (UINT4 u4CfaIfIndex)
{
    CFA_DS_LOCK ();

    gaCfaCommonIfInfo[u4CfaIfIndex - 1].u4CfaIfIndex = u4CfaIfIndex;

    CFA_DS_UNLOCK ();

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlIfArrayDbRem                                 */
/*                                                                           */
/* Description        : Sets the ifIndex in the array to zero                */
/*                                                                           */
/* Input(s)           : u4IfIndex                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : RBTree pointer                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIfUtlIfArrayDbRem (UINT4 u4CfaIfIndex)
{
    CFA_DS_LOCK ();

    gaCfaCommonIfInfo[u4CfaIfIndex - 1].u4CfaIfIndex = 0;

    CFA_DS_UNLOCK ();

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlIfDbGetIndex                                 */
/*                                                                           */
/* Description        : This functions gets the requisite ifIndex based on   */
/*                      the start index.                                     */
/*                                                                           */
/* Input(s)           : u4CfaIfIndex - start index, u1IfType - ifType        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : IfIndex - Current interface index                    */
/*                                                                           */
/*****************************************************************************/
UINT4
CfaIfUtlIfDbGetIndex (UINT4 u4CfaIfIndex, UINT1 u1IfType)
{
    tCfaIfTypeNode     *pCfaIfTypeNode = NULL;
    tCfaIfTypeNode     *pCfaCurIfTypeListNode = NULL;
    tCfaIfInfo         *pCfaIfInfoEntry = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT1               u1IfIndexFound = CFA_FALSE;
    UINT4               u4CfaCurIfIndex = 0;

    if (u1IfType == 0)
    {
        pCfaIfInfoEntry = CfaIfUtlCdbGetIfDbEntry (u4CfaIfIndex);

        if (pCfaIfInfoEntry == NULL)
        {
            /* With the given ifIndex a node may not exist. Thus
             * in case the node dosent exist, do a GetNextIndex
             */
            MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

            CfaIfInfo.u4CfaIfIndex = u4CfaIfIndex;

            pCfaIfInfoEntry = (tCfaIfInfo *) RBTreeGetNext
                (gCfaCdbIfTable, &CfaIfInfo, NULL);

            if (pCfaIfInfoEntry != NULL)
            {
                u4CfaCurIfIndex = pCfaIfInfoEntry->u4CfaIfIndex;
            }
        }
        else
        {
            u4CfaCurIfIndex = pCfaIfInfoEntry->u4CfaIfIndex;
        }
    }
    else
    {
        pCfaIfTypeNode = TMO_DLL_First (&(CFA_CDB_IF_TYPE_DB[u1IfType - 1]));

        if (pCfaIfTypeNode != NULL)
        {
            do
            {
                pCfaIfInfoEntry = (tCfaIfInfo *) (VOID *)
                    ((UINT1 *) pCfaIfTypeNode -
                     CFA_CALC_CDB_IF_TYPE_NODE_OFFSET);

                u4CfaCurIfIndex = pCfaIfInfoEntry->u4CfaIfIndex;

                if (u4CfaCurIfIndex >= u4CfaIfIndex)
                {
                    u1IfIndexFound = CFA_TRUE;
                    break;
                }

                pCfaCurIfTypeListNode = TMO_DLL_Next
                    (&(CFA_CDB_IF_TYPE_DB[u1IfType - 1]), pCfaIfTypeNode);

                pCfaIfTypeNode = pCfaCurIfTypeListNode;

            }
            while (pCfaIfTypeNode != NULL);
        }

        if (u1IfIndexFound != CFA_TRUE)
        {
            u4CfaCurIfIndex = 0;
        }
    }
    return u4CfaCurIfIndex;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlGapIfArrayDbRem                              */
/*                                                                           */
/* Description        : Sets the gapifIndex in the array to NULL             */
/*                                                                           */
/* Input(s)           : u4IfIndex                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIfUtlGapIfArrayDbRem (UINT4 u4CfaIfIndex)
{
    CFA_GAP_IF_SET_IF_ENTRY (u4CfaIfIndex, NULL);

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlGapIfGet                                     */
/*                                                                           */
/* Description        : Gets the node from the GAPIF RBTree. This is         */
/*                      used only from the GAPIF related MACRO's since an    */
/*                      assert will be done in this call if the node does    */
/*                      not exist.                                           */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4IfIndex                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Pointer to node OR NULL                              */
/*                                                                           */
/*****************************************************************************/
tIfInfoStruct      *
CfaIfUtlGapIfGet (UINT4 u4IfIndex)
{
    tIfInfoStruct      *pCfaGapIfInfo = NULL;

    pCfaGapIfInfo = CfaIfUtlGetGapIfDbEntry (u4IfIndex);

    if ((NULL == pCfaGapIfInfo) && (CFA_IF_DB != CFA_IFDB_ARRAY))
    {
        /* This indicates an issue. Ideally should not happen */
        CfaIfUtlAssert (u4IfIndex);
    }
    return pCfaGapIfInfo;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlGetGapIfDbEntry                              */
/*                                                                           */
/* Description        : Gets the node from the GAPIF RBTree and returns the  */
/*                      same.                                                */
/* Input(s)           : u4IfIndex                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Pointer to node OR NULL                              */
/*                                                                           */
/*****************************************************************************/
tIfInfoStruct      *
CfaIfUtlGetGapIfDbEntry (UINT4 u4IfIndex)
{
    tIfInfoStruct       CfaGapIfInfo;
    tIfInfoStruct      *pCfaGapIfInfo = NULL;

    MEMSET (&CfaGapIfInfo, 0, sizeof (tIfInfoStruct));

    CfaGapIfInfo.u4CfaGapIfIndex = u4IfIndex;

    pCfaGapIfInfo =
        (tIfInfoStruct *) CfaIfUtlGapIfDbGet (gCfaGapIfTable, &CfaGapIfInfo);

    return pCfaGapIfInfo;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlGapIfArrayDbGet                              */
/*                                                                           */
/* Description        : Gets the array location corresponding to the         */
/*                      ifIndex                                              */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4CfaIfIndex                                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Pointer to node OR NULL                              */
/*                                                                           */
/*****************************************************************************/
tIfInfoStruct      *
CfaIfUtlGapIfArrayDbGet (UINT4 u4CfaIfIndex)
{
    tIfInfoStruct      *pCfaIfInfo = NULL;

    pCfaIfInfo = (tIfInfoStruct *) gapIfTable[u4CfaIfIndex - 1];

    return pCfaIfInfo;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlGapIfDbGet                                   */
/*                                                                           */
/* Description        : Gets a node from the RBTree                          */
/*                                                                           */
/* Input(s)           : tCfaIfDb,   tIfInfoStruct                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Pointer to node OR NULL                              */
/*                                                                           */
/*****************************************************************************/
tIfInfoStruct      *
CfaIfUtlGapIfDbGet (tCfaIfDb CfaGapIfDb, tIfInfoStruct * pCfaGapIfDbElem)
{
    tIfInfoStruct      *pCfaGapIfElem = NULL;
    UINT4               u4CfaIfIndex = 0;
    if (CFA_IF_DB == CFA_IFDB_ARRAY)
    {
        u4CfaIfIndex = ((tIfInfoStruct *) (pCfaGapIfDbElem))->u4CfaGapIfIndex;
        return (CfaIfUtlGapIfArrayDbGet (u4CfaIfIndex));
    }
    pCfaGapIfElem = (tIfInfoStruct *) RBTreeGet (CfaGapIfDb, pCfaGapIfDbElem);
    return pCfaGapIfElem;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlGapIfArrayDbAdd                              */
/*                                                                           */
/* Description        : This function sets the GapIf entry pointer to the    */
/*                      array                                                */
/*                                                                           */
/* Input(s)           : tIfInfoStruct *pIfInfoStruct                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIfUtlGapIfArrayDbAdd (tIfInfoStruct * pIfInfoStruct)
{

    gapIfTable[(pIfInfoStruct->u4CfaGapIfIndex) - 1] = pIfInfoStruct;

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlIfDbGetNextIndex                             */
/*                                                                           */
/* Description        : This functions gets the next requisite ifIndex based */
/*                      on the given ifIndex. The given ifIndex block should */
/*                      be existing.                                         */
/*                                                                           */
/* Input(s)           : u4CfaIfIndex, u1IfType                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : IfIndex - next interface index                       */
/*                                                                           */
/*****************************************************************************/
UINT4
CfaIfUtlIfDbGetNextIndex (UINT4 u4CfaIfIndex, UINT1 u1IfType)
{
    tCfaIfInfo         *pCfaIfInfoEntry = NULL;
    tCfaIfTypeNode     *pCfaIfTypeNode = NULL;
    tCfaIfTypeNode     *pCfaCurIfTypeListNode = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4CfaCurIfIndex = 0;

    /* The block with the given u4CfaIfIndex  should be 
     * existing 
     */
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    CfaIfInfo.u4CfaIfIndex = u4CfaIfIndex;

    if (u1IfType == 0)
    {
        pCfaIfInfoEntry = (tCfaIfInfo *)
            RBTreeGetNext (gCfaCdbIfTable, &CfaIfInfo, NULL);

        if (pCfaIfInfoEntry != NULL)
        {
            u4CfaCurIfIndex = pCfaIfInfoEntry->u4CfaIfIndex;
        }
    }
    else
    {
        pCfaIfInfoEntry = (tCfaIfInfo *) RBTreeGet (gCfaCdbIfTable,
                                                    (tCfaIfDbElem *) &
                                                    CfaIfInfo);

        if (pCfaIfInfoEntry != NULL)
        {
            pCfaIfTypeNode = (tCfaIfTypeNode *) (VOID *) ((UINT1 *)
                                                          pCfaIfInfoEntry +
                                                          FSAP_OFFSETOF
                                                          (tCfaIfInfo,
                                                           CfaIfTypeNode));

            pCfaCurIfTypeListNode = TMO_DLL_Next
                (&(CFA_CDB_IF_TYPE_DB[u1IfType - 1]), pCfaIfTypeNode);

            if (pCfaCurIfTypeListNode != NULL)
            {
                pCfaIfInfoEntry = (tCfaIfInfo *) (VOID *) ((UINT1 *)
                                                           pCfaCurIfTypeListNode
                                                           -
                                                           FSAP_OFFSETOF
                                                           (tCfaIfInfo,
                                                            CfaIfTypeNode));

                u4CfaCurIfIndex = pCfaIfInfoEntry->u4CfaIfIndex;
            }
        }
    }

    return u4CfaCurIfIndex;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlIfDbGetIndexWithLock                         */
/*                                                                           */
/* Description        : This function calls the CfaIfUtlIfDbGetIndex         */
/*                      taking the CDB lock                                  */
/*                                                                           */
/* Input(s)           : u4CfaIfIndex, u1IfType                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : IfIndex - current interface index                    */
/*                                                                           */
/*****************************************************************************/
UINT4
CfaIfUtlIfDbGetIndexWithLock (UINT4 u4CfaIfIndex, UINT1 u1IfType)
{
    UINT4               u4CfaCurIfIndex = 0;

    CFA_DS_LOCK ();

    u4CfaCurIfIndex = CfaIfUtlIfDbGetIndex (u4CfaIfIndex, u1IfType);

    CFA_DS_UNLOCK ();

    return u4CfaCurIfIndex;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlIfDbGetNextIndexWithLock                     */
/*                                                                           */
/* Description        : This function calls the CfaIfUtlIfDbGetNextIndex     */
/*                      taking the CDB lock                                  */
/*                                                                           */
/* Input(s)           : u4CfaIfIndex, u1IfType                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : IfIndex - next interface index                       */
/*                                                                           */
/*****************************************************************************/
UINT4
CfaIfUtlIfDbGetNextIndexWithLock (UINT4 u4CfaIfIndex, UINT1 u1IfType)
{
    UINT4               u4CfaCurIfIndex = 0;

    CFA_DS_LOCK ();

    u4CfaCurIfIndex = CfaIfUtlIfDbGetNextIndex (u4CfaIfIndex, u1IfType);

    CFA_DS_UNLOCK ();

    return u4CfaCurIfIndex;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlCreateMauMemPool                             */
/*                                                                           */
/* Description        : This function is used to get the MAU  mempool.       */
/*                                                                           */
/* Input(s)           : VOID                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
CfaIfUtlCreateMauMemPool (VOID)
{
    CFA_DS_LOCK ();
    CFA_MAU_IF_MEMPOOL = CFAMemPoolIds[MAX_CFA_MAU_INTF_SIZING_ID];
    CFA_DS_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlMauMemAlloc                                  */
/*                                                                           */
/* Description        : This function allocates memory for the Mau pointer   */
/*                                                                           */
/* Input(s)           : u4IfIndex : Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIfUtlMauMemAlloc (UINT4 u4IfIndex)
{
    tCfaIfInfo         *pCfaIfInfoEntry = NULL;
    CFA_DS_LOCK ();

    pCfaIfInfoEntry = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
    if (pCfaIfInfoEntry != NULL)
    {
        if (NULL != CFA_CDB_IF_MAU_ENTRY (pCfaIfInfoEntry))
        {
            MEMSET (CFA_CDB_IF_MAU_ENTRY (pCfaIfInfoEntry),
                    0, sizeof (tIfMauStruct));
            CFA_DS_UNLOCK ();
            return CFA_SUCCESS;
        }
        if ((CFA_CDB_IF_MAU_ENTRY (pCfaIfInfoEntry) =
             MemAllocMemBlk (CFA_MAU_IF_MEMPOOL)) != NULL)
        {
            MEMSET (CFA_CDB_IF_MAU_ENTRY (pCfaIfInfoEntry),
                    0, sizeof (tIfMauStruct));
            CFA_DS_UNLOCK ();
            return CFA_SUCCESS;
        }
    }
    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlMauMemRelease                                */
/*                                                                           */
/* Description        : This function realease the memory allocated          */
/*                      for the mau pointer                                  */
/*                                                                           */
/* Input(s)           : u4IfIndex : Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIfUtlMauMemRelease (UINT4 u4IfIndex)
{
    tCfaIfInfo         *pCfaIfInfoEntry = NULL;
    CFA_DS_LOCK ();

    pCfaIfInfoEntry = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
    if (pCfaIfInfoEntry != NULL)
    {
        if (CFA_CDB_IF_MAU_ENTRY (pCfaIfInfoEntry) != NULL)
        {
            MemReleaseMemBlock (CFA_MAU_IF_MEMPOOL, (UINT1 *)
                                (CFA_CDB_IF_MAU_ENTRY (pCfaIfInfoEntry)));
            CFA_CDB_IF_MAU_ENTRY (pCfaIfInfoEntry) = NULL;
        }
    }
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlGapIfInit                                    */
/*                                                                           */
/* Description        : Sets the IfTable pointers to NULL                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
CfaIfUtlGapIfInit (VOID)
{
    UINT4               u4Index = 0;
    if (CFA_IF_DB == CFA_IFDB_ARRAY)
    {
        for (u4Index = 1; u4Index <= SYS_DEF_MAX_INTERFACES; ++u4Index)
        {
            CFA_GAP_IF_SET_IF_ENTRY (u4Index, NULL);
        }
    }
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlGapIfDbGetIndex                              */
/*                                                                           */
/* Description        : This function gets the required IfIndex from the     */
/*                      GapIfDB                                              */
/*                                                                           */
/* Input(s)           : UINT4 u4CfaGapIfIndex                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : IfIndex                                              */
/*                                                                           */
/*****************************************************************************/
UINT4
CfaIfUtlGapIfDbGetIndex (UINT4 u4CfaGapIfIndex)
{
    tIfInfoStruct      *pCfaIfInfoEntry = NULL;
    tIfInfoStruct       CfaGapIfInfo;
    UINT4               u4CfaCurIfIndex = 0;

    pCfaIfInfoEntry = CfaIfUtlGetGapIfDbEntry (u4CfaGapIfIndex);

    if (pCfaIfInfoEntry == NULL)
    {
        /* With the given ifIndex a node may not exist. Thus
         * in case the node dosent exist, do a GetNextIndex
         */
        MEMSET (&CfaGapIfInfo, 0, sizeof (tIfInfoStruct));

        CfaGapIfInfo.u4CfaGapIfIndex = u4CfaGapIfIndex;

        pCfaIfInfoEntry = (tIfInfoStruct *) RBTreeGetNext
            (gCfaGapIfTable, &CfaGapIfInfo, NULL);

        if (pCfaIfInfoEntry != NULL)
        {
            u4CfaCurIfIndex = pCfaIfInfoEntry->u4CfaGapIfIndex;
        }
    }
    else
    {
        u4CfaCurIfIndex = pCfaIfInfoEntry->u4CfaGapIfIndex;
    }

    return u4CfaCurIfIndex;
}

/*****************************************************************************/
/* Function Name      : CfaIfUtlGapIfDbGetNextIndex                          */
/*                                                                           */
/* Description        : This functions gets the next requisite ifIndex based */
/*                      on the given ifIndex in GAPIFDB. The given ifIndex   */
/*                      block should                                         */
/*                      be existing.                                         */
/*                                                                           */
/* Input(s)           : u4CfaGapIfIndex                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : IfIndex - next interface index                       */
/*                                                                           */
/*****************************************************************************/
UINT4
CfaIfUtlGapIfDbGetNextIndex (UINT4 u4CfaGapIfIndex)
{
    tIfInfoStruct      *pCfaIfInfoEntry = NULL;
    tIfInfoStruct       CfaGapIfInfo;
    UINT4               u4CfaCurIfIndex = 0;

    /* The block with the given u4CfaIfIndex  should be
     * existing
     */
    MEMSET (&CfaGapIfInfo, 0, sizeof (tIfInfoStruct));

    CfaGapIfInfo.u4CfaGapIfIndex = u4CfaGapIfIndex;

    pCfaIfInfoEntry = (tIfInfoStruct *)
        RBTreeGetNext (gCfaGapIfTable, &CfaGapIfInfo, NULL);

    if (pCfaIfInfoEntry != NULL)
    {
        u4CfaCurIfIndex = pCfaIfInfoEntry->u4CfaGapIfIndex;
    }

    return u4CfaCurIfIndex;
}
