/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfaiph.c,v 1.118 2017/12/19 13:41:53 siva Exp $
 *
 * Description: This file contains the routines for the     
 *              Fault Management Module (FM) of the CFA.    
 *              These routines are called at various places     
 *              in the CFA modules for handling faults in    
 *              CFA and logging them.               
 *
 *******************************************************************/
#include "cfainc.h"
#include "iss.h"
#ifdef LNXIP4_WANTED
#include "lnxip.h"
#endif

/*****************************************************************************
*
*    Function Name         : CfaProcessIncomingIpPkt
*
*    Description           : This function will be called by IWF modules
*                whenever they recieve datagram for IP. This
*                function checks whether the packet is valid
*                and fast forwardable. If yes, it does further
*                                            processing. If not, it sends the packet to
*                                            IP.
*
*    Input(s)          : Pointer to CRU Buffer containing IP packet and
*                        IfIndex on which it has been received.
*                        u1PktType - Type of the packet.
*
*    Output(s)         : None.
*
*    Global Variables Referred : gFfConfigInfo.
*
*    Global Variables Modified : None.
*
*    Exceptions or Operating
*      System Error Handling   : None.
*
*    Use of Recursion      : None.
*
*    Returns           : CFA_SUCCESS if packet is processed
*                successfully, else CFA_FAILURE.
*
*****************************************************************************/
INT4
CfaProcessIncomingIpPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                         UINT1 u1PktType, tEnetV2Header * pEthHdr)
{
    t_IP_HEADER         IPHdr;
    tCfaIfInfo          CfaIfInfo;
#if defined(LNXIP4_WANTED) && !defined(KERNEL_WANTED)
#ifndef LINUX_ARP_NETLINK
    tArpQMsg            ArpQMsg;
#endif
#endif
    INT4                i4IpHeaderLen = 0;
    INT4                i4DestAddrType = CFA_FAILURE;
    UINT4               u4CxtId = 0;
#ifdef MPLS_L3VPN_WANTED
    UINT4               u4ContextId = 0;
#endif
    UINT1               u1IfType;

    UNUSED_PARAM (u1IfType);
    UNUSED_PARAM (i4DestAddrType);

#ifndef LNXIP4_WANTED
    UNUSED_PARAM (pEthHdr);
#endif

#if !defined(MPLS_WANTED) && !defined(IP_WANTED)
    UNUSED_PARAM (u1PktType);
#endif

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    CFA_DBG (CFA_TRC_ALL, CFA_FFM, "Entering CfaProcessIncomingIpPkt \n");

    /* Get the context id for this interface */
    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4CxtId);

    /* Extract the IP header */
    if ((i4IpHeaderLen = CfaExtractIpHdr (&IPHdr, pBuf)) == CFA_FAILURE)
    {
#ifdef IP_WANTED
/* update IP InHdrErrors */
        IpUpdateInHdrErrInCxt (u4CxtId);
#endif
        CFA_DBG (CFA_TRC_ALL, CFA_FFM,
                 "Packet Extraction Failure from CRU buffer"
                 "Exiting CfaProcessIncomingIpPkt \n");
        return CFA_FAILURE;        /* buf released by IWF/GDD */
    }

#if defined(LNXIP4_WANTED) && !defined(KERNEL_WANTED)
#ifndef LINUX_ARP_NETLINK
    MEMSET (&ArpQMsg, 0, sizeof (tArpQMsg));

    if ((IP_IS_ADDR_CLASS_A (IPHdr.u4Dest) || IP_IS_ADDR_CLASS_B (IPHdr.u4Dest)
         || IP_IS_ADDR_CLASS_C (IPHdr.u4Dest)))
    {
        /* In case of Linux IP, IP unicast packet in the slow path is given 
         * to ARP module to trigger installation of route/ARP entries */
        ArpQMsg.u4MsgType = IP_PKT_FOR_RESOLVE;
        ArpQMsg.u2Protocol = CFA_ENET_IPV4;
        ArpQMsg.u4EncapType = CFA_ENCAP_ENETV2;
        ArpQMsg.pBuf = pBuf;
        ArpQMsg.u2Port = CFA_IF_IPPORT (u4IfIndex);

#ifdef L3_SWITCHING_WANTED
        if (ArpEnqueueIpPkt (&ArpQMsg) != ARP_SUCCESS)
        {
            CFA_IF_SET_IN_DISCARD (u4IfIndex);

            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                      CFA_IWF,
                      "Exiting CfaProcessIncomingIpPkt - IP Pkt on interface %d - FAILURE\n",
                      u4IfIndex);
            return (CFA_FAILURE);    /* buffer is released by GDD */
        }
        return CFA_SUCCESS;
#endif
    }
#endif
    /* If Linux IP TAP is enable Give Packet to TAP Module */
    CfaGetIfInfo (u4IfIndex, &CfaIfInfo);
    if ((CfaIfInfo.u1IfType == CFA_L3IPVLAN) || 
	(CfaIfInfo.u1IfType == CFA_ENET) || (CfaIfInfo.u1IfType == CFA_PPP) ||
          ((CfaIfInfo.u1IfType == CFA_LAGG) &&
          (CfaIfInfo.u1BridgedIface == CFA_DISABLED)))
    {
        CfaHandleInBoundPktToLnxIpTap (u4IfIndex, pBuf, pEthHdr, IP_ID);
        /* We have already taken a copy of Cru Buf.
         * Release the Buffer */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return CFA_SUCCESS;
    }
#ifndef LINUX_ARP_NETLINK
    if (ArpQMsg.pBuf == NULL)
    {
        /* Since Packets are not consumed by ARP module, this CRU buffer can be
         * freed here */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }
    return CFA_SUCCESS;
#else
    /* Don't Process the all other IP Packet.All other IP packets
       should have bypassed to L3Protocols through Linux Kernel */
    return CFA_FAILURE;
#endif
#endif

#ifdef IP_WANTED
/* Check if the interface is up */
    if (CFA_IF_ADMIN (u4IfIndex) != CFA_IF_UP)
    {
        IpUpdateInIfaceErrInCxt (u4CxtId);
        CFA_DBG1 (CFA_TRC_ALL, CFA_FFM,
                  "Interface %d is not operational. "
                  "Exiting CfaProcessIncomingIpPkt \n", u4IfIndex);
        return CFA_FAILURE;        /* buf released by IWF/GDD */
    }

#endif

#ifdef IP_WANTED
    if (CfaIpChksumCompute (pBuf, (UINT4) i4IpHeaderLen, 0)
        != CFA_IP_VALID_CHKSUM)
    {
        /* update IP InHdrErrors */
        IpUpdateInHdrErrInCxt (u4CxtId);
        CFA_DBG (CFA_TRC_ALL, CFA_FFM,
                 "Invalid Checksum in the header Exiting"
                 "CfaProcessIncomingIpPkt \n");
        return CFA_FAILURE;        /* buf released by IWF/GDD */
    }
#endif
#ifdef MPLS_WANTED
    if (u1PktType == CFA_LINK_UCAST)
    {
#ifdef MPLS_L3VPN_WANTED
        if (MplsIsIpPktForL3Vpn (u4IfIndex, &u4ContextId) == MPLS_TRUE)
        {
            if (MplsProcessL3VpnPacket
                (pBuf, u4IfIndex, IPHdr.u4Dest, u4ContextId) == MPLS_SUCCESS)
            {
                return CFA_SUCCESS;
            }
        }
#endif
        if (MplsProcessIpPkt (pBuf, u4IfIndex, IPHdr.u4Dest, TRUE) ==
            MPLS_SUCCESS)
        {
            /* MPLS will take care of forwarding the
             * IP pakcet with Label */
            return CFA_SUCCESS;
        }
    }
#endif

    /* DOS attacks are prevented here */
    if (CfaCheckAttacks (pBuf, &IPHdr, (UINT1) i4IpHeaderLen) != CFA_SUCCESS)
    {
        /* Error, update IP In discards */
#ifdef IP_WANTED
        IpUpdateInIfaceErrInCxt (u4CxtId);
#endif
        return CFA_FAILURE;        /* buf released by IWF/GDD */
    }

#ifdef IP_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);

    if (u1IfType == CFA_PPP)
    {
        CfaIfSetInOctets (u4IfIndex, IPHdr.u2Totlen);

        i4DestAddrType = CfaIpGetAddrTypeInCxt (0, IPHdr.u4Dest);

        if (i4DestAddrType == CFA_UCAST_ADDR)
        {
            CfaIfSetInUCast (u4IfIndex);
        }
        else if (i4DestAddrType == CFA_BCAST_ADDR)
        {
            CfaIfSetInBCast (u4IfIndex);
        }
        else if (i4DestAddrType == CFA_MCAST_ADDR)
        {
            CfaIfSetInMCast (u4IfIndex);
        }
    }

    if (IpHandlePktFromCfa (pBuf, (UINT2) CFA_IF_IPPORT (u4IfIndex),
                            u1PktType) != IP_SUCCESS)
    {
/* Error, update IP In discards */
        IpUpdateInIfaceErrInCxt (u4CxtId);
        CFA_DBG (CFA_TRC_ALL, CFA_FFM,
                 "Error in delivering the message to IP "
                 "Exiting CfaProcessIncomingIpPkt \n");
        return CFA_FAILURE;        /* buf released by IWF/GDD */
    }
    else
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_FFM,
                 "Successfully delivered the message to IP : "
                 "Exiting CfaProcessIncomingIpPkt \n");
        return CFA_SUCCESS;
    }
#else

    return CFA_SUCCESS;            /* If nothing is defined return success */
#endif /* IP_WANTED */
}

/*****************************************************************************
*
*    Function Name         : CfaHandlePktFromIp
*
*    Description           : Receives data from the IP modules(Including IP, 
*                            ARP, RARP and InARP packets) for transmission over 
*                            the specified interface and enqueues the message 
*                            to the CFA task. 
*                           
*    Input(s)          : pBuf - Pointer to the CRU buffer
*                        u4IfIndex - MIB-2 Interface index
*
*
*    Output(s)         : None.
*
*    Global Variables Referred : None
*
*    Global Variables Modified : None
*
*    Exceptions or Operating
*      System Error Handling   : None.
*
*    Use of Recursion      : None.
*
*    Returns           : None.
*****************************************************************************/
VOID
CfaHandleIpPktFromIp (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex)
{
#ifdef MPLS_WANTED
    t_IP_HEADER        *pIpHdr = NULL;
    t_IP_HEADER         TmpIpHdr;
    UINT4               u4IPAddr = 0;
#endif
    CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Entering CfaHandlePktFromIp \n");

#ifdef MPLS_WANTED
    pIpHdr =
        (t_IP_HEADER *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                                               CFA_IP_HDR_LEN);
    if (pIpHdr == NULL)
    {
        pIpHdr = &TmpIpHdr;
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pIpHdr, 0, CFA_IP_HDR_LEN);
    }
    u4IPAddr = OSIX_NTOHL (pIpHdr->u4Dest);

#ifndef NPAPI_WANTED
    if ((IP_IS_ADDR_CLASS_A (u4IPAddr) || IP_IS_ADDR_CLASS_B (u4IPAddr) ||
         IP_IS_ADDR_CLASS_C (u4IPAddr)))
    {
        if (MplsProcessIpPkt (pBuf, u4IfIndex, u4IPAddr, TRUE) == MPLS_SUCCESS)
        {
            /* MPLS will take care of forwarding the
             * IP pakcet with Label */
            return;
        }
    }
#else
    UNUSED_PARAM (u4IPAddr);
#endif
#endif

    if (CfaProcessOutgoingIpPkt (pBuf, u4IfIndex) != CFA_SUCCESS)
    {
        /* error -  discard the packet - counter already updated */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }

    CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Exiting CfaHandlePktFromIp \n");

}

#ifdef IP6_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaHandlePktFromIPv6
 *
 *    Description          : Process data from the IPv6 modules for
 *                           transmission over the specified interface.
 *
 *    Input(s)            : tCRU_BUF_CHAIN_HEADER *pBuf,
 *                          UINT4 u4IfIndex,
 *                          UINT1 *au1DestHwAddr.
 *                          UINT4 u4PktSize.
 *                          UINT2 u2Protocol.
 *                          UINT2 u1EncapType.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None. 
 *                        
 *****************************************************************************/
VOID
CfaHandlePktFromIpv6 (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                      UINT1 *au1DestHwAddr, UINT4 u4PktSize,
                      UINT2 u2Protocol, UINT1 u1EncapType)
{
    UINT1               u1IfType;
    INT4                i4RetVal = CFA_FAILURE;

    CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Entering CfaProcessPktFromIpv6 \n");

    CfaGetIfType (u4IfIndex, &u1IfType);

    switch (u1IfType)
    {
        case CFA_ENET:

#ifdef WGS_WANTED
        case CFA_L2VLAN:
#endif
        case CFA_L3IPVLAN:
        case CFA_L3SUB_INTF:
        case CFA_LAGG:
        case CFA_PSEUDO_WIRE:
#ifdef VXLAN_WANTED
        case CFA_VXLAN_NVE:
#endif
            i4RetVal = CfaIwfEnetProcessTxFrame
                (pBuf, u4IfIndex, au1DestHwAddr, u4PktSize,
                 u2Protocol, u1EncapType);
            break;

        default:
            CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Invalid Outgoing Interface \n");
            break;
    }

    if (i4RetVal == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "CFA IPv6 Packet dispatch to IWF FAIL.\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }

    CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Exiting CfaProcessPktFromArpForIvr \n");
}
#endif /* IP6_WANTED */

/*****************************************************************************
*
*    Function Name         : CfaProcessOutgoingIpPkt
*
*    Description           : Does some processing on the outgoing interface 
*                            and transmits the packets. 
*
*    Input(s)          : None.
*
*    Output(s)         : None.
*
*    Global Variables Referred : gapIfTable.
*
*    Global Variables Modified : None.
*
*    Exceptions or Operating
*      System Error Handling   : None.
*
*    Use of Recursion      : None.
*
*    Returns           : CFA_SUCCESS or CFA_FAILURE.
*****************************************************************************/
INT4
CfaProcessOutgoingIpPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex)
{
    UINT4               u4PktSize;
    UINT1               u1IfType;
    t_IP_HEADER         IPHdr;
    INT4                i4DestAddrType = CFA_FAILURE;

    CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Entering CfaProcessOutgoingIpPkt \n");

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    CfaExtractIpHdr (&IPHdr, pBuf);

    CfaGetIfType (u4IfIndex, &u1IfType);

    if (u1IfType == CFA_PPP)
    {
        CfaIfSetOutOctets (u4IfIndex, u4PktSize);

        i4DestAddrType = CfaIpGetAddrTypeInCxt (0, IPHdr.u4Dest);

        if (i4DestAddrType == CFA_UCAST_ADDR)
        {
            CfaIfSetOutUCast (u4IfIndex);
        }
        else if (i4DestAddrType == CFA_BCAST_ADDR)
        {
            CfaIfSetOutBCast (u4IfIndex);
        }
        else if (i4DestAddrType == CFA_MCAST_ADDR)
        {
            CfaIfSetOutMCast (u4IfIndex);
        }
    }

    CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Exiting CfaProcessOutgoingIpPkt \n");
    return (CfaProcessOutgoingIpPktForIwf (pBuf, u4IfIndex, u4PktSize));
}

/*****************************************************************************
*
*    Function Name         : CfaProcessOutgoingIpPktForIwf
*
*    Description           : Transmits the received packet to the respective data 
*                            link layer protocol IWF module based on the ifIndex.
*
*    Input(s)          : PBuf - Pointer to the CRU buffer.
*                        u4IfIndex - The ifIndex of the topmost outgoing interface
*                        U4PktSize - Size of the CRU buffer.
*
*    Output(s)         : None
*
*    Global Variables Referred : gapIfTable
*
*    Global Variables Modified : None
*
*    Exceptions or Operating
*      System Error Handling   : None.
*
*    Use of Recursion      : None.
*
*    Returns           : CFA_SUCCESS or CFA_FAILURE.
*****************************************************************************/
INT4
CfaProcessOutgoingIpPktForIwf (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                               UINT4 u4PktSize)
{
    UINT1               u1IfType;
    UINT1               au1DestHwAddr[CFA_ENET_ADDR_LEN];
    UINT1               au1DestHwAddrTemp[CFA_ENET_ADDR_LEN];
    INT4                i4RetVal = CFA_FAILURE;
    UINT4               u4CxtId = 0;

    UINT4               u4IfIpAddr = 0;

    CFA_DBG (CFA_TRC_ALL, CFA_FFM,
             "\n Entering CfaProcessOutgoingPktForIwf \n");
    MEMSET (au1DestHwAddrTemp, 0, CFA_ENET_ADDR_LEN);

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4CxtId);
    CfaGetIfType (u4IfIndex, &u1IfType);

    CFA_GET_DEST_MEDIA_ADDR (pBuf, au1DestHwAddr);

/* based on the interface type, send the pkt to the respective IWF
routines */
    switch (u1IfType)
    {
        case CFA_OTHER:
        case CFA_LAGG:
        case CFA_ENET:            /* Fall Through */
        case CFA_L3IPVLAN:        /* Fall Through */
        case CFA_L3SUB_INTF:
        case CFA_PSEUDO_WIRE:    /* Fall Through */
#ifdef VXLAN_WANTED
        case CFA_VXLAN_NVE:
#endif
        case CFA_VPNC:            /* Fall Through */
#ifdef WGS_WANTED
        case CFA_L2VLAN:
#endif /* WGS_WANTED */
        case CFA_TUNNEL:

            CFA_DBG (CFA_TRC_ALL, CFA_FFM,
                     "\n Outgoing Interface is VLAN/Ethernet interface \n");

            if (CfaGetIfIpAddr ((INT4) u4IfIndex, &u4IfIpAddr) == CFA_SUCCESS)
            {
                if ((u4IfIpAddr == 0) &&
                    (MEMCMP
                     (au1DestHwAddr, au1DestHwAddrTemp,
                      CFA_ENET_ADDR_LEN) == 0))
                {

                    if (CfaGetIfUnnumPeerMac (u4IfIndex, au1DestHwAddr) !=
                        CFA_SUCCESS)
                    {
                        CFA_DBG (CFA_TRC_ALL, CFA_FFM,
                                 "IPIF: Unable to fetch Destination Mac for unnumbered outgoing interface\n");
                    }

                }
            }
            i4RetVal =
                CfaIwfEnetProcessTxFrame (pBuf, u4IfIndex,
                                          au1DestHwAddr, u4PktSize,
                                          CFA_ENET_IPV4,
                                          CFA_GET_ENCAP_TYPE (pBuf));

            break;

#ifdef PPP_WANTED
        case CFA_PPP:
            CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Outgoing Interface is PPP  \n");
            i4RetVal =
                CfaIwfPppHandleOutgoingPkt (pBuf, u4IfIndex, u4PktSize,
                                            CFA_IPV4_PID);

            break;
#endif /* PPP_WANTED */

        default:
            CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Invalid Outgoing Interface \n");
            /* update IP OutDiscards */
#ifdef IP_WANTED
            IpUpdateOutIfaceErrInCxt (u4CxtId);
#endif
            break;                /* i4RetVal would be CFA_FAILURE */
    }

    CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Exiting CfaProcessOutgoingIpPkt \n");
    return i4RetVal;            /* we dont release buf here */
}

/*****************************************************************************
 *
 *    Function Name        : CfaExtractIpHdr
 *
 *    Description        : This function extracts the IP header from the input 
 *                         buffer and stores in pIp.
 *
 *    Input(s)            :   pIp - Pointer to the IP header.
 *                            pBuf -Pointer to the CRU buffer
 *
 *    Output(s)            : pIp - Extracted IP header.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
INT4
CfaExtractIpHdr (t_IP_HEADER * pIp, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    t_IP_HEADER        *pIpHdr = NULL;
    t_IP_HEADER         TmpIpHdr;
    UINT1               u1Tmp, u1Hlen;

    CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Entering CfaExtractIpHdr function \n");

    pIpHdr =
        (t_IP_HEADER *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                                               CFA_IP_HDR_LEN);
    if (pIpHdr == NULL)
    {

/* The header is not contiguous in the buffer */
        pIpHdr = &TmpIpHdr;

/* Copy the header */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pIpHdr, 0, CFA_IP_HDR_LEN);
    }

    pIp->u1Ver_hdrlen = u1Tmp = pIpHdr->u1Ver_hdrlen;

/* check version */
    if ((u1Tmp >> 4) != CFA_IP_V4)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Version is not 4 \n");
        return CFA_FAILURE;
    }

    u1Hlen = (UINT1) ((u1Tmp & 0x0f) << 2);
    pIp->u1Tos = pIpHdr->u1Tos;
    pIp->u2Totlen = OSIX_NTOHS (pIpHdr->u2Totlen);

/* check header and total lengths */
    if ((u1Hlen < CFA_IP_HDR_LEN) || (pIp->u2Totlen <= u1Hlen))
    {
        CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Header Length is incorrect \n");
        return CFA_FAILURE;
    }

    pIp->u2Id = pIpHdr->u2Id;
    pIp->u2Fl_offs = OSIX_NTOHS (pIpHdr->u2Fl_offs);
    pIp->u1Ttl = pIpHdr->u1Ttl;
    pIp->u1Proto = pIpHdr->u1Proto;
    pIp->u2Cksum = OSIX_NTOHS (pIpHdr->u2Cksum);

    MEMCPY (&pIp->u4Src, &pIpHdr->u4Src, sizeof (UINT4));
    pIp->u4Src = OSIX_NTOHL (pIp->u4Src);

    MEMCPY (&pIp->u4Dest, &pIpHdr->u4Dest, sizeof (UINT4));
    pIp->u4Dest = OSIX_NTOHL (pIp->u4Dest);

    CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Exiting CfaExtractIpHdr function \n");

    return u1Hlen;                /* return the header length */
}

/*****************************************************************************
 *
 *    Function Name        : CfaIpChksumCompute
 *
 *    Description        : This function calculates the check sum of the buffer.
 *
 *    Input(s)            : Pbuf - Pointer to the CRU buffer 
 *                          U4Size - Size of the packet.
 *                          U4ReadOffset-Offset from where the header starts.

 *
 *    Output(s)            : None
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : Checksum value
 *
 *****************************************************************************/
UINT2
CfaIpChksumCompute (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Size,
                    UINT4 u4ReadOffset)
{
    tCRU_BUF_DATA_DESC *pDataDesc;
    UINT4               u4Sum = 0;
    UINT2               u2Len = 0;
    UINT2               u2Tmp = 0;
    UINT1              *pu1Buf;

    CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Entering CfaIpChksumCompute \n");

    pDataDesc = pBuf->pFirstValidDataDesc;
    pu1Buf = (pDataDesc->pu1_FirstValidByte) + u4ReadOffset;

    while (u4Size > 0)
    {
        u2Len = (UINT2) (pDataDesc->u4_ValidByteCount - u4ReadOffset);
        u4ReadOffset = 0;

        u2Len = (UINT2) ((u2Len < u4Size) ? u2Len : u4Size);
        u4Size -= (UINT4) u2Len;

        for (; u2Len > 1; pu1Buf += 2, u2Len = (UINT2) (u2Len - 2))
        {
            u4Sum += (OSIX_NTOHS (*(UINT2 *) (VOID *) pu1Buf));
        }

/* Add the last byte if number of bytes is odd */
        if (u2Len == 1)
        {
            u2Tmp = (UINT2) ((*pu1Buf));
            u4Sum += OSIX_NTOHS (u2Tmp);
        }

        pDataDesc = pDataDesc->pNext;

        if (!pDataDesc)
        {
            break;
        }

        pu1Buf = pDataDesc->pu1_FirstValidByte;
    }                            /* end of while (u4Size > 0) */

    u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);
    u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);

    CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Exiting CfaIpChksumCompute \n");

    return ((UINT2) ~((UINT2) u4Sum));
}

/*****************************************************************************
 *
 *    Function Name        : CfaIpVerifyTtl
 *
 *    Description        : This function decrements the TTL field 
 *                         from received buffer.
 *
 *    Input(s)            : pBuf - Pointer to the CRU buffer.
 *                                            
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
VOID
CfaIpVerifyTtl (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4Sum;
    t_IP_HEADER        *pIpHdr;
    t_IP_HEADER         IpTempHdr;
    UINT1               u1CopyRequired = CFA_FALSE;

    CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Entering CfaIpVerifyTtl \n");

    if ((pIpHdr = (t_IP_HEADER *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear
         (pBuf, 0, (IP_HDR_LEN - 8))) == NULL)
    {
        if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &IpTempHdr, 0,
                                       (IP_HDR_LEN - 8)) == CRU_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_FFM,
                     "\n Error in CfaIpVerifyTtl - Copy from BUF failed.\n");
            return;                /* we return without decrementing TTL */
        }

        u1CopyRequired = CFA_TRUE;
        pIpHdr = &IpTempHdr;
    }

    (pIpHdr->u1Ttl)--;
    u4Sum = (UINT4) OSIX_NTOHS (pIpHdr->u2Cksum) + 0x100;
    pIpHdr->u2Cksum = (UINT2) OSIX_HTONS ((u4Sum + (u4Sum >> 16)));

    if (u1CopyRequired == CFA_TRUE)
    {
        if (CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pIpHdr, 0,
                                       (IP_HDR_LEN - 8)) == CRU_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_FFM,
                     "\n Error in CfaIpVerifyTtl - Copy to BUF failed.\n");
        }
    }

    CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Exiting CfaIpVerifyTtl \n");
}

/*****************************************************************************
 *
 *    Function Name        : CfaProcessPktFromArpForIvr
 *
 *    Description          : This function processes the packet from ARP 
 *                           module to CFA when IVR is enabled.
 *
 *    Input(s)            : tCRU_BUF_CHAIN_HEADER *pBuf,
 *                          UINT4 u4IfIndex,
 *                          UINT1 *au1DestHwAddr.
 *                          UINT4 u4PktSize.
 *                          UINT2 u2Protocol.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None. 
 *                        
 *****************************************************************************/
VOID
CfaProcessPktFromArpForIvr (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                            UINT1 *au1DestHwAddr, UINT4 u4PktSize,
                            UINT2 u2Protocol, UINT1 u1EncapType)
{
    UINT1               u1IfType;
    INT4                i4RetVal = CFA_FAILURE;

    CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Entering CfaProcessPktFromArpForIvr \n");

    CfaGetIfType (u4IfIndex, &u1IfType);

    switch (u1IfType)
    {
#ifdef OPENFLOW_WANTED
        case CFA_TUNNEL:
            /*
             * Openflow communication with ISS in Hybrid mode, happens
             * via user created tunnels.
             */
#endif /* OPENFLOW_WANTED */
        case CFA_OTHER:
        case CFA_ENET:
            i4RetVal = CfaIwfEnetProcessTxFrame
                (pBuf, u4IfIndex, au1DestHwAddr, u4PktSize,
                 u2Protocol, u1EncapType);
            break;

#ifdef WGS_WANTED
        case CFA_L2VLAN:
#endif
        case CFA_L3SUB_INTF:
        case CFA_L3IPVLAN:
        case CFA_LAGG:
        case CFA_PSEUDO_WIRE:
#ifdef VXLAN_WANTED
        case CFA_VXLAN_NVE:
#endif
#ifdef VCPEMGR_WANTED
        case CFA_TAP:
#endif
            i4RetVal = CfaIwfEnetProcessTxFrame
                (pBuf, u4IfIndex, au1DestHwAddr, u4PktSize,
                 u2Protocol, u1EncapType);

            break;
        default:
            CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Invalid Outgoing Interface \n");
            break;
    }
    if (i4RetVal == CFA_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }

    CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Exiting CfaProcessPktFromArpForIvr \n");
}

#ifdef WGS_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaIvrProcessPktFromMgmtIface
 *
 *    Description          : This function processes the packet from management 
 *                           VLAN interface. This will call the function for 
 *                           extracting the VLAN ID and further processing.
 *
 *    Input(s)            : tCRU_BUF_CHAIN_HEADER *pBuf,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None. 
 *                        
 *****************************************************************************/
INT4
CfaIvrProcessPktFromMgmtIface (UINT1 *pu1Buf, UINT4 u4PktSize)
{
    tMgmtVlanList       MgmtVlanList;
    tMacAddr            DestAddr;
    tVlanId             VlanId;
    BOOL1               bResult;
    BOOL1               bBcastFlag;
    INT4                i4RetVal;

    MEMSET (MgmtVlanList, 0, sizeof (tMgmtVlanList));

    if (CfaGetMgmtVlanList (MgmtVlanList) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }

    MEMCPY (DestAddr, pu1Buf, CFA_ENET_ADDR_LEN);

    if (CFA_IS_ENET_MAC_BCAST (DestAddr))
    {
        bBcastFlag = CFA_FALSE;
    }
    else
    {
        bBcastFlag = CFA_TRUE;
    }

    for (VlanId = 1; VlanId < CFA_MAX_VLAN_ID; VlanId++)
    {
        OSIX_BITLIST_IS_BIT_SET (MgmtVlanList, VlanId, sizeof (MgmtVlanList),
                                 bResult);

        if (bResult == OSIX_TRUE)
        {

            i4RetVal
                = CfaGddTxPktOnVlanMemberPortsInCxt (VLAN_DEF_CONTEXT_ID,
                                                     pu1Buf, VlanId,
                                                     bBcastFlag, u4PktSize);
        }
    }

    UNUSED_PARAM (i4RetVal);

    return CFA_SUCCESS;
}
#endif

/******************************************************************************
 * Function       : CfaUpdtIvrInterface 
 *
 * Description    : This function will update the h/w for the L3_IPVLAN interfaces
 *                  based on the Status of the IVR interface.
 *
 * Input(s)       : u4IfIndex - Interface index of the L3_IPVLAN interface.
 *                  u1Status  - CFA_IF_CREATE/CFA_IF_DELETE.
 *
 * Output(s)      : None.
 *
 * Returns        : CFA_SUCCESS/FAILURE
 *
 ******************************************************************************/

INT4
CfaUpdtIvrInterface (UINT4 u4IfIndex, UINT1 u1Status)
{
#ifdef L3_SWITCHING_WANTED
    INT4                i4RetVal;
    UINT4               u4IpAddress;
    UINT4               u4SubnetMask;
    UINT2               u2VlanId;
    UINT1               au1MacAddr[CFA_ENET_ADDR_LEN];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tIpConfigInfo       IpIntfInfo;
    UINT4               u4ContextId = 0;
    UINT1               u1IfType;
    tIpAddrInfo         SecondaryInfo;
    UINT4               u4SecondaryAddr = 0;
    UINT4               u4NextIfIndex = 0;
    UINT4               u4IfMtu = CFA_ZERO;
#ifdef    IP6_WANTED
    tFsNpIntInfo        IntInfo;
    UINT4               u4Addr = 0;
#endif
#ifdef IP_WANTED
    tFsNpIp4IntInfo     IpIntInfo;
#endif
#ifdef TUNNEL_WANTED
    tTnlIfEntry         TnlIfEntry;
#endif
    tL3MtuInfo          L3MtuInfo;

    MEMSET (&L3MtuInfo, CFA_ZERO, sizeof (tL3MtuInfo));
    /* Don't update the h/w for MGMT interface eth0 *
     * loob back interfae (lo) and Linux Vlan interface */

    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        (CfaIsLinuxVlanIntf (u4IfIndex) == TRUE))
    {
        return CFA_SUCCESS;
    }

    if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId) == VCM_FAILURE)
    {
/*  if context id fetch is failing, ignore for ppp case alone, since vrf
 *  support is not present for ppp
 */
#ifndef PPP_WANTED
        return VCM_FAILURE;
#endif
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

#ifdef IP_WANTED
    /* Check Interface Type is Tunnel or not */
    MEMSET (&IpIntInfo, 0, sizeof (tFsNpIp4IntInfo));

    if (u1IfType == CFA_TUNNEL)
    {
        if (CfaGetTnlEntryFromIfIndex (u4IfIndex, &TnlIfEntry) == CFA_SUCCESS)
        {
            IpIntInfo.u4PhyIfIndex = TnlIfEntry.u4PhyIfIndex;
            IpIntInfo.u4TnlIfIndex = TnlIfEntry.u4IfIndex;
            IpIntInfo.TunlInfo.u1TunlType = TnlIfEntry.u4EncapsMethod;
            IpIntInfo.u1IfType = u1IfType;
            IpIntInfo.TunlInfo.u4tunlSrc = TnlIfEntry.LocalAddr.Ip4TnlAddr;

            if (TnlIfEntry.u4EncapsMethod == TNL_TYPE_GRE)
            {
                IpIntInfo.TunlInfo.u4tunlDst = TnlIfEntry.RemoteAddr.Ip4TnlAddr;
            }
            IpIntInfo.TunlInfo.u1TunlFlag = TnlIfEntry.u1DirFlag;
            IpIntInfo.TunlInfo.u1TunlDir = TnlIfEntry.u1Direction;

            if (u1Status == CFA_IF_CREATE)
            {
                if (CfaGetIfIvrVlanId (IpIntInfo.u4PhyIfIndex, &u2VlanId) ==
                    CFA_FAILURE)
                {
                    return CFA_FAILURE;
                }

                IpIntInfo.u2VlanId = u2VlanId;
            }
        }

        if (u1Status == CFA_IF_CREATE)
        {
            CfaFsNpIpv4IntfStatus (u4ContextId, NP_IP_IF_UP, &IpIntInfo);
        }
        else if (u1Status == CFA_IF_DELETE)
        {
            CfaFsNpIpv4IntfStatus (u4ContextId, NP_IP_IF_DELETE, &IpIntInfo);
        }
    }
#endif

#ifdef    IP6_WANTED
    /* Check Interface Type is Tunnel or not */
    MEMSET (&IntInfo, 0, sizeof (tFsNpIntInfo));

    if (u1IfType == CFA_TUNNEL)
    {
        if (CfaGetTnlEntryFromIfIndex (u4IfIndex, &TnlIfEntry) == CFA_SUCCESS)
        {
            IntInfo.u4PhyIfIndex = TnlIfEntry.u4PhyIfIndex;
            IntInfo.u4TnlIfIndex = TnlIfEntry.u4IfIndex;
            IntInfo.TunlInfo.u1TunlType = TnlIfEntry.u4EncapsMethod;
            IntInfo.u1IfType = u1IfType;
            IntInfo.TunlInfo.tunlSrc.u4_addr[3] =
                TnlIfEntry.LocalAddr.Ip4TnlAddr;

            if ((TnlIfEntry.u4EncapsMethod == TNL_TYPE_IPV6IP) ||
                (TnlIfEntry.u4EncapsMethod == TNL_TYPE_GRE))
            {
                IntInfo.TunlInfo.tunlDst.u4_addr[3] =
                    TnlIfEntry.RemoteAddr.Ip4TnlAddr;
            }
            IntInfo.TunlInfo.u1TunlFlag = TnlIfEntry.u1DirFlag;
            IntInfo.TunlInfo.u1TunlDir = TnlIfEntry.u1Direction;

            if (u1Status == CFA_IF_CREATE)
            {
                if (CfaGetIfIvrVlanId (IntInfo.u4PhyIfIndex, &u2VlanId) ==
                    CFA_FAILURE)
                {
                    return CFA_FAILURE;
                }

                IntInfo.u2VlanId = u2VlanId;

                if ((TnlIfEntry.u4EncapsMethod == TNL_TYPE_SIXTOFOUR) ||
                    (TnlIfEntry.u4EncapsMethod == TNL_TYPE_ISATAP) ||
#ifdef OPENLFOW_WANTED
                    (TnlIfEntry.u4EncapsMethod == TNL_TYPE_OPENFLOW) ||
#endif /* OPENFLOW_WANTED */
                    (TnlIfEntry.u4EncapsMethod == TNL_TYPE_COMPAT))
                {
                    if (IpGetIpv4AddrFromArpTable (IntInfo.u4PhyIfIndex,
                                                   &u4Addr) == IP_FAILURE)
                    {
                        return CFA_FAILURE;
                    }
                    IntInfo.TunlInfo.tunlDst.u4_addr[3] = u4Addr;
                }
            }
        }

        if (u1Status == CFA_IF_CREATE)
        {
            CfaFsNpIpv6IntfStatus (u4ContextId, NP_IP6_IF_UP, &IntInfo);
        }
        else if (u1Status == CFA_IF_DELETE)
        {
            CfaFsNpIpv6IntfStatus (u4ContextId, NP_IP6_IF_DELETE, &IntInfo);
        }
        return CFA_SUCCESS;
    }
#endif
    if (u1IfType == CFA_LOOPBACK)
    {
        u2VlanId = FNP_ZERO;
    }
    else
    {
        CfaGetIfIvrVlanId (u4IfIndex, &u2VlanId);
    }

    if (CfaIpIfGetIfInfo (u4IfIndex, &IpIntfInfo) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }
    u4IpAddress = IpIntfInfo.u4Addr;
    u4SubnetMask = IpIntfInfo.u4NetMask;
    CfaGetIfHwAddr (u4IfIndex, au1MacAddr);

#if defined (LNXIP4_WANTED) && defined (NPAPI_WANTED)
    /* Vlan Id for the creation of filter for the Knet 
     * Interface is retrieved from the Dummy Vlan Id created 
     * for the specific Interface index */
    if ((u1IfType == CFA_ENET) &&
        (ISS_HW_SUPPORTED == IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT)))
    {
        u2VlanId = IpIntfInfo.u2PortVlanId;
    }
#endif

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    CfaGetIfName (u4IfIndex, au1IfName);

    i4RetVal = FNP_FAILURE;
    CfaGetIfMtu (u4IfIndex, &u4IfMtu);
    L3MtuInfo.u4IfIndex = u4IfIndex;
    L3MtuInfo.u4Mtu = u4IfMtu;
    L3MtuInfo.u4VrfId = u4ContextId;

    if ((u1IfType == CFA_L3IPVLAN) || (u1IfType == CFA_LOOPBACK)
        || (u1IfType == CFA_L3SUB_INTF))
    {
        if (CFA_NODE_STATUS () == CFA_NODE_ACTIVE)
        {
            if (u1Status == CFA_IF_CREATE)
            {
                i4RetVal =
                    CfaFsNpIpv4UpdateIpInterfaceStatus (u4ContextId, au1IfName,
                                                        u4IfIndex, u4IpAddress,
                                                        u4SubnetMask, u2VlanId,
                                                        au1MacAddr, CFA_IF_UP);
                FsCfaHwL3SetMtu (L3MtuInfo);
            }
            else if (u1Status == CFA_IF_DELETE)
            {
                i4RetVal =
                    CfaFsNpIpv4UpdateIpInterfaceStatus (u4ContextId, au1IfName,
                                                        u4IfIndex, u4IpAddress,
                                                        u4SubnetMask, u2VlanId,
                                                        au1MacAddr,
                                                        CFA_IF_DOWN);
            }
        }

        else
        {
            i4RetVal = FNP_SUCCESS;
        }
    }

    else
    {
        if (u1Status == CFA_IF_CREATE)
        {
            i4RetVal =
                CfaFsNpIpv4UpdateIpInterfaceStatus (u4ContextId, au1IfName,
                                                    u4IfIndex, u4IpAddress,
                                                    u4SubnetMask, u2VlanId,
                                                    au1MacAddr, CFA_IF_UP);
            FsCfaHwL3SetMtu (L3MtuInfo);

        }
        else if (u1Status == CFA_IF_DELETE)
        {
            i4RetVal =
                CfaFsNpIpv4UpdateIpInterfaceStatus (u4ContextId, au1IfName,
                                                    u4IfIndex, u4IpAddress,
                                                    u4SubnetMask, u2VlanId,
                                                    au1MacAddr, CFA_IF_DOWN);
        }
    }

    if (i4RetVal == FNP_FAILURE)
    {
        return CFA_FAILURE;
    }

    /* If the vlan is a primary vlan, then map or unmap the list of 
     * secondary vlans from this IVR interface based on u1Status. */
    if ((CfaIvrMapUpdtMappedVlans (u4ContextId, u4IfIndex, u2VlanId,
                                   u1Status)) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }
    /* NP Call for secondary IP address, if any exists */
    while (CfaIpIfGetNextSecondaryIpIndex (u4IfIndex, u4SecondaryAddr,
                                           &u4NextIfIndex,
                                           &u4SecondaryAddr) != CFA_FAILURE)
    {
        if (u4IfIndex == u4NextIfIndex)
        {
            if (CfaIpIfGetSecondaryAddressInfo (u4IfIndex, u4SecondaryAddr,
                                                &SecondaryInfo) == CFA_SUCCESS)
            {
                u4IpAddress = SecondaryInfo.u4Addr;
                u4SubnetMask = u4CidrSubnetMask[SecondaryInfo.u1SubnetMask];
                if (u1Status == CFA_IF_CREATE)
                {
                    i4RetVal = FsNpIpv4UpdateIpInterfaceStatus
                        (IP_VRID, au1IfName,
                         u4IfIndex, u4IpAddress,
                         u4SubnetMask, u2VlanId, au1MacAddr, CFA_IF_UP);
                }
                else if (u1Status == CFA_IF_DELETE)
                {
                    i4RetVal = FsNpIpv4UpdateIpInterfaceStatus
                        (IP_VRID, au1IfName,
                         u4IfIndex, u4IpAddress,
                         u4SubnetMask, u2VlanId, au1MacAddr, CFA_IF_DOWN);
                }

                if (i4RetVal == FNP_FAILURE)
                {
                    return CFA_FAILURE;
                }
            }
        }
        else
        {
            break;
        }
    }
#else

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
#endif /* L3_SWITCHING_WANTED */

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function       : CfaIpUpdateHostRoute
 *
 * Description    : This function does the update the host route
 *
 * Input(s)       : u4IfIndex
 *                  u4PeerIpAddr ,  i2Metric ,u1Status
 *
 * Output(s)      : None.
 *
 * Returns        : CFA_SUCCESS/FAILURE/CONTINUE
 *
 ******************************************************************************/

INT4
CfaIpUpdateHostRoute (UINT4 u4IfIndex, UINT4 u4PeerIpAddr,
                      INT2 i2Metric, UINT1 u1Status)
{
    tNetIpv4RtInfo      NetRtInfo;
    INT4                i4RetVal = IP_FAILURE;
    UINT2               u2Port = 0;
    UINT1               u1CmdType = 0;

    UNUSED_PARAM (u1Status);

    MEMSET (&NetRtInfo, 0, sizeof (tNetIpv4RtInfo));
    u2Port = (UINT2) (CfaGetIfIpPort (u4IfIndex));

    /* Add the Route with IpPort Number */
    NetRtInfo.u4RtIfIndx = u2Port;
    NetRtInfo.u4NextHop = 0x0;
    NetRtInfo.i4Metric1 = i2Metric;
    NetRtInfo.u2RtType = CIDR_REMOTE_ROUTE_TYPE;    /* remote destination */
    NetRtInfo.u2RtProto = CIDR_LOCAL_ID;
    NetRtInfo.u4DestNet = u4PeerIpAddr;
    NetRtInfo.u4DestMask = 0xffffffff;
    NetRtInfo.u4RtNxtHopAs = 0;
    NetRtInfo.u4RtAge = OsixGetSysUpTime ();
    if (u1Status == CFA_NET_IF_DEL)
    {
        NetRtInfo.u4RowStatus = (UINT4) IPFWD_DESTROY;
        u1CmdType = NETIPV4_DELETE_ROUTE;
    }
    else if (u1Status == CFA_NET_IF_NEW)
    {
        NetRtInfo.u4RowStatus = (UINT4) IPFWD_ACTIVE;
        u1CmdType = NETIPV4_ADD_ROUTE;
    }

    i4RetVal = NetIpv4LeakRoute (u1CmdType, &NetRtInfo);
    if (i4RetVal == NETIPV4_FAILURE)
    {
        return (CFA_FAILURE);
    }
    return CFA_SUCCESS;
}

/*MPLS_IPv6 add start*/
#ifdef MPLS_IPV6_WANTED
/*****************************************************************************
*
*    Function Name         : CfaProcessIncomingIpv6Pkt
*
*    Description           : This function will be called by IWF modules
*                whenever they recieve datagram for IPV6. This
*                function checks whether the packet is valid
*                and fast forwardable. If yes, it does further
*                                            processing. If not, it sends the packet to
*                                            IP.
*
*    Input(s)          : Pointer to CRU Buffer containing IP packet and
*                        IfIndex on which it has been received.
*                        u1PktType - Type of the packet.
*
*    Output(s)         : None.
*
*    Global Variables Referred : gFfConfigInfo.
*
*    Global Variables Modified : None.
*
*    Exceptions or Operating
*      System Error Handling   : None.
*
*    Use of Recursion      : None.
*
*    Returns           : CFA_SUCCESS if packet is processed
*                successfully, else CFA_FAILURE.
*
*****************************************************************************/
INT4
CfaProcessIncomingIpv6Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                           UINT1 u1PktType, tEnetV2Header * pEthHdr)
{
    tIp6Hdr             Ipv6Hdr;
    INT4                i4IpHeaderLen = 0;
#if !defined(MPLS_WANTED) && !defined(IP6_WANTED)
    UNUSED_PARAM (u1PktType);
#endif

    CFA_DBG (CFA_TRC_ALL, CFA_FFM, "Entering CfaProcessIncomingIpPkt \n");

    /* Extract the IPV6 header */
    if ((i4IpHeaderLen = CfaExtractIpv6Hdr (&Ipv6Hdr, pBuf)) == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_FFM,
                 "IPV6 Packet Extraction Failure from CRU buffer"
                 "Exiting CfaProcessIncomingIpPkt \n");
        return CFA_FAILURE;        /* buf released by IWF/GDD */
    }
#ifdef IP6_WANTED
/* Check if the interface is up */
    if (CFA_IF_ADMIN (u4IfIndex) != CFA_IF_UP)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_FFM,
                  "IPV6 Interface %d is not operational. "
                  "Exiting CfaProcessIncomingIpv6Pkt \n", u4IfIndex);
        return CFA_FAILURE;        /* buf released by IWF/GDD */
    }

#endif
#ifdef MPLS_WANTED
    if (u1PktType == CFA_LINK_UCAST)
    {
        /*MPLS_L3VPN Not Supported For IPV6 Commenting It */
        if (MplsProcessIpv6Pkt (pBuf, u4IfIndex, Ipv6Hdr.dstAddr, TRUE) ==
            MPLS_SUCCESS)
        {
            /* MPLS will take care of forwarding the
             * IP pakcet with Label */
            return CFA_SUCCESS;
        }
    }
#endif
    UNUSED_PARAM (pEthHdr);
    return CFA_NOTIFYV6;        /* Notify to IPv6 layer */

}

/*****************************************************************************
*
*    Function Name         : CfaExtractIpv6Hdr
*
*    Description           : This function Extracts the IPV6 Header
*                                 from the buffer received in IP Packet
*
*    Input(s)          : Pointer to CRU Buffer containing IP packet and
*                        pIp6Hdr IPV6 Header Structure.
*
*    Output(s)         : IPV6 Header Structure gets populated.
*
*    Returns           : CFA_SUCCESS if packet is processed
*                            successfully, else CFA_FAILURE.
*
*****************************************************************************/

INT4
CfaExtractIpv6Hdr (tIp6Hdr * pIp6Hdr, tCRU_BUF_CHAIN_HEADER * pBuf)
{

    tIp6Hdr             TmpIp6Hdr;
    tIp6Hdr            *pTmpIp6Hdr;
    UINT1               u1VerPrioFlow[4];
    MEMSET (&TmpIp6Hdr, 0, sizeof (tIp6Hdr));
    MEMSET (u1VerPrioFlow, 0, sizeof (u1VerPrioFlow));

    pTmpIp6Hdr =
        (tIp6Hdr *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                                           sizeof (tIp6Hdr));
    if (pTmpIp6Hdr == NULL)
    {
        /* The header is not contiguous in the buffer */
        pTmpIp6Hdr = &TmpIp6Hdr;
        /* Copy the header */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pTmpIp6Hdr,
                                   0, sizeof (tIp6Hdr));
    }

    MEMCPY (pIp6Hdr, pTmpIp6Hdr, sizeof (tIp6Hdr));
    pIp6Hdr->u2Len = OSIX_NTOHS (pIp6Hdr->u2Len);
    MEMCPY (u1VerPrioFlow, &pIp6Hdr->u4Head, sizeof (u1VerPrioFlow));
    if ((u1VerPrioFlow[0] >> 4) != CFA_IP_V6)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_FFM, "\n Version is not 6 \n");
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

#endif
/*MPLS_IPv6 add end*/
