/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfambsm.c,v 1.87 2016/07/02 09:42:15 siva Exp $
 *
 * Description:This file contains utility routines common to all
 *             the modules. 
 *
 *******************************************************************/
#include "cfainc.h"
#include "isspi.h"
#include "issnpwr.h"

/*****************************************************************************
 *
 *    Function Name        : CfaIsPortExist 
 *
 *    Description          : This function checks if the port is physically
 *                           present in the hardware and returns either
 *                           TRUE or FALSE.                     
 *
 *    Input(s)             : u4IfIndex - IfIndex to be checked for presence.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : CFA_TRUE if Port exist else CFA_FALSE.
 *
 *****************************************************************************/
INT4
CfaIsPortExist (UINT4 u4IfIndex)
{
    UINT1               u1OperStatus;

    if (CfaValidateIfIndex (u4IfIndex) == OSIX_FAILURE)
    {
        return CFA_FALSE;
    }

    CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);

    if ((u1OperStatus == CFA_IF_NP) || (u1OperStatus == CFA_IF_DOWN))
    {
        return CFA_FALSE;
    }

    return CFA_TRUE;
}

/*****************************************************************************
 *
 *    Function Name        : CfaMbsmMsgToCfa
 *
 *    Description          : This function is called by MBSM to post a message
 *                           to CFA.
 *
 *    Input(s)             : pMsg - Pointer to the message to be posted to  
 *                           CFA.
 *                           u1Cmd - MsgType to differentiate between packets
 *                           from different modules and events.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaMbsmMsgToCfa (UINT1 *pMsg, UINT1 u1Cmd)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tCfaModuleData     *pParms = NULL;
    INT4                i4NumEntries;

    if (CFA_INITIALISED != TRUE)
    {
        return CFA_FAILURE;
    }
    i4NumEntries = ((tMbsmCfaHwInfoMsg *) pMsg)->i4NumEntries;

    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMbsmCfaHwInfoMsg) +
                                              ((i4NumEntries - 1) *
                                               sizeof (tMbsmSlotPortInfo)) +
                                              sizeof (tCfaModuleData),
                                              0)) == NULL)
        return CFA_FAILURE;

    /* copy data from linear buffer into CRU buffer */
    if (CRU_BUF_Copy_OverBufChain (pBuf, pMsg, 0,
                                   sizeof (tMbsmCfaHwInfoMsg) +
                                   ((i4NumEntries - 1) *
                                    sizeof (tMbsmSlotPortInfo))) == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Error in CfaMbsmMsgToCfa - "
                 "unable to copy to CRU Buf- FAILURE.\n");

        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return (CFA_FAILURE);
    }
    pParms = GET_MODULE_DATA_PTR (pBuf);
    pParms->u1Command = u1Cmd;
    pParms->u1SlotCount = i4NumEntries;

    if (OsixQueSend (CFA_PACKET_QID, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaMbsmMsgToCfa - " "Pkt Enqueue to CFA FAILED.\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return (CFA_FAILURE);
    }

/* send an explicit event to the task */
    if (OsixEvtSend (CFA_TASK_ID, CFA_PACKET_ARRIVAL_EVENT) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaMbsmmsgToCfa - event send to CFA FAIL.\n");
/* dont return failure as buffer has already been enqueued */
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaHandleIfUpdatePktFromMbsm
 *
 *    Description          : This function is called by CFA on receiving a    
 *                           mesasge from MBSM.
 *
 *    Input(s)             : pMsg - Pointer to the message to be posted to  
 *                           CFA.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaHandleIfUpdatePktFromMbsm (tCRU_BUF_CHAIN_HEADER * pMsg)
{
    INT4                i4RetStatus = CFA_SUCCESS;
    UINT4               u4SlotCount = 0;
    UINT4               u4DataLen = 0;
    UINT4               u4Index = 0;
    UINT4               u4TmpIndex = 0;
    tMbsmSlotPortInfo  *pTempPtr = NULL;
    UINT1               u1IsBufLinear = TRUE;
    UINT1              *pSlotInfoMsg = NULL;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tMbsmSyncAckMsg     SyncAckMsg;
    UINT1               u1IfType;
    UINT4               u4PortIndex;
    UINT4               u4StartIfIndex = 0;
    UINT4               u4PortCount = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4CardIndex = MBSM_INVALID_CARDINDEX;
    UINT1               u1SpeedType = MBSM_SPEED_INVALID;
    UINT4               u4PortNum = 0;
    UINT4               u4ConnectingPortCount = 0;
#ifdef RM_WANTED
    UINT4               u4NodeId = 0;
#endif
    UINT4               u4IfMtu = CFA_ENET_MTU;
    UINT4               u4StackingModel = ISS_STACKING_MODEL_NONE;
    UINT1               u1LinkStatus = 0;
    INT4                i4Index = 0;
    INT4                i4IssPortCtrlSpeed = 0;
    UINT1               u1IsSetInPortList = OSIX_FALSE;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;
    tMbsmPortInfo      *pPortInfo = NULL;
    tCfaIfInfo         *pCfaIfInfo = NULL;
    tHwPortInfo         HwPortInfo;
    tHwIdInfo           HwIdInfo;

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE &&
        IssGetStackPortCountFromNvRam () == 0)
    {
        /* In Non-Stacking case, Slot number should be zero &
         * only one slot should be there */
        u4SlotCount = 1;
    }
    else
    {
        u4SlotCount = CFA_GET_SLOT_COUNT (pMsg);
    }

    if ((u4SlotCount > MBSM_MAX_SLOTS) || (u4SlotCount == 0))
    {
        return (CFA_FAILURE);
    }

    u4DataLen = sizeof (tMbsmCfaHwInfoMsg) +
        ((u4SlotCount - 1) * sizeof (tMbsmSlotPortInfo));

    pSlotInfoMsg = CRU_BUF_Get_DataPtr_IfLinear (pMsg, 0, u4DataLen);
    if (pSlotInfoMsg == NULL)
    {
        if ((pSlotInfoMsg = MemAllocMemBlk (gCfaMbsmSlotMsgPoolId)) == NULL)
        {

            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaHandleIfUpdatePktFromMbsm - No Memory -"
                     "FAILURE.\n");
            return (CFA_FAILURE);
        }

        u1IsBufLinear = FALSE;

        /* for aysnc the copy would be done by the byte stuffing function */
        if (CRU_BUF_Copy_FromBufChain (pMsg, pSlotInfoMsg, 0, u4DataLen) ==
            CRU_FAILURE)
        {
            CFA_DBG (CFA_TRC_ALL_TRACK | CFA_TRC_ERROR, CFA_GDD,
                     "Error in CfaHandleIfUpdatePktFromMbsm - "
                     "Copy_FromBufChain failed.\n");
            MemReleaseMemBlock (gCfaMbsmSlotMsgPoolId,
                                (UINT1 *) (pSlotInfoMsg));
            return CFA_FAILURE;
        }
    }

    /* Point to the SlotPortInfo field */
    pTempPtr = &(((tMbsmCfaHwInfoMsg *) pSlotInfoMsg)->FirstEntry);

    u4StackingModel = ISS_GET_STACKING_MODEL ();
    for (u4Index = 0; u4Index < u4SlotCount; u4Index++)
    {
        pSlotInfo = &(pTempPtr->MbsmSlotInfo);
        pPortInfo = &(pTempPtr->MbsmPortInfo);
        if ((pTempPtr->MbsmSlotInfo.i1Status == MBSM_STATUS_ACTIVE) ||
            (u4StackingModel == ISS_DISS_STACKING_MODEL))
        {
            /* Check for port/card message */
            if (!MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
            {
                    if (CfaMbsmGddInit (pSlotInfo) == CFA_FAILURE)
                    {
                        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                 "Error In CfaMbsmGddInit.\n");
                        i4RetStatus = CFA_FAILURE;
                        break;
                    }
            }
            if (CFA_IF_ENTRY_USED (pTempPtr->MbsmSlotInfo.u4StartIfIndex))
            {
                /* Interfaces already created */
            }
            else
            {
                /* Interfaces has to be created */
                CfaCreateInterfacesForSlot (pTempPtr->MbsmSlotInfo.i4SlotId,
                                            &(pTempPtr->MbsmPortInfo));
            }

            /* Update the If Params */
            CfaUpdateIfParamsForSlot (&(pTempPtr->MbsmPortInfo));
            /* Check for port/card message */
            if (0 == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
            {
                if (u4StackingModel == ISS_DATA_PLANE_STACKING_MODEL)
                {
                    if (CfaCfaMbsmRegisterWithNpDrv (pSlotInfo) == FNP_FAILURE)
                    {
                        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                 "Error In CfaMbsmRegisterWithNpDrv.\n");
                        i4RetStatus = CFA_FAILURE;
                        break;
                    }
                }
                else if ((u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL) ||
                         (u4StackingModel == ISS_DISS_STACKING_MODEL))
                {
                    if (MBSM_SLOT_INFO_IS_RM_PEER_UP_MSG (pSlotInfo) ==
                        OSIX_FALSE)
                    {
                        if (CfaCfaRegisterWithNpDrv () == FNP_FAILURE)
                        {
                            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                     "Error In CfaRegisterWithNpDrv.\n");
                            i4RetStatus = CFA_FAILURE;
                            break;
                        }
                    }
                }
            }

            if (&(pTempPtr->MbsmPortInfo) == NULL)
            {
                if (u1IsBufLinear == FALSE)
                {
                    MemReleaseMemBlock (gCfaMbsmSlotMsgPoolId,
                                        (UINT1 *) (pSlotInfoMsg));
                }
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "Error In CfaHandleIfUpdatePktFromMbsm - NULL message.\n");
                return (CFA_FAILURE);
            }
            if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
            {
                /* Whenever one slot is getting attached, it may miss the 
                 * operstate indication;So after created the port in CFA
                 * operstatus of stack ports needs to be updated */
                i4Index = CFA_STACK_PORT_START_INDEX;

                CFA_CDB_SCAN_WITH_LOCK (i4Index,
                                        (MBSM_MAX_PORTS_PER_SLOT +
                                         ISS_MAX_STK_PORTS), CFA_ALL_IFTYPE)
                {
                    /* Flexible ifIndex changes
                     */
                    if (i4Index > (MBSM_MAX_PORTS_PER_SLOT + ISS_MAX_STK_PORTS))
                    {
                        break;
                    }

                    u1LinkStatus = CfaGddGetLinkStatus (i4Index);
                    CfaMbsmHandleInterfaceStatusUpdate (i4Index, CFA_IF_UP,
                                                        u1LinkStatus);
                }
                /* There will not be any L2 entry for this STACK VLAN 4094.
                 * So we need to trigger operstatus update for StackIVR.
                 * If any one of the stack port is UP, then STACKIVR 
                 * will be UP otherwise it will be down.*/

                CfaIfmHandleInterfaceStatusChange (CFA_DEFAULT_STACK_IFINDEX,
                                                   CFA_IF_UP, CFA_IF_UP,
                                                   CFA_FALSE);
                /* After creating the ports in CFA, the oper status 
                 * should be updated*/
                CfaUpdateOperStatusDown (&(pTempPtr->MbsmPortInfo));
            }

            u4StartIfIndex = pTempPtr->MbsmPortInfo.u4StartIfIndex;
            if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
            {
                u4PortCount = MBSM_MAX_PORTS_PER_SLOT;
            }
            else
            {
                u4PortCount = pTempPtr->MbsmPortInfo.u4PortCount;
            }

            for (u4PortIndex = 1; u4PortIndex <= u4PortCount; u4PortIndex++)
            {
                u4IfIndex = u4StartIfIndex + (u4PortIndex - 1);

                CFA_DS_LOCK ();

                pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

                CFA_DS_UNLOCK ();

                if (NULL != pCfaIfInfo)
                {
                    OSIX_BITLIST_IS_BIT_SET (pPortInfo->PortList, u4IfIndex,
                                             sizeof (tPortList),
                                             u1IsSetInPortList);
                    OSIX_BITLIST_IS_BIT_SET (pPortInfo->PortListStatus,
                                             u4IfIndex, sizeof (tPortList),
                                             u1IsSetInPortListStatus);

                    /*Handling of Port Insert Event */
                    if ((OSIX_TRUE == u1IsSetInPortList)
                        && (OSIX_FALSE == u1IsSetInPortListStatus))
                    {
                        if (CfaFsHwGetEthernetType
                            ((UINT2) u4IfIndex, &u1IfType) == FNP_FAILURE)
                        {
                            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM, "Error in "
                                      "CfaIfmInitEnetIfEntry - Getting "
                                      "Ethernet type for interface %d\n",
                                      u4IfIndex);
                        }

                        CfaSetEthernetType (u4IfIndex, u1IfType);
                        if (pTempPtr->MbsmSlotInfo.i1Status == MBSM_STATUS_NP)
                        {
                            if (CfaUpdateOperStatusNotPresent
                                (&(pTempPtr->MbsmPortInfo)) == CFA_FAILURE)
                            {
                                return (CFA_FAILURE);
                            }
                        }
                        else
                        {
                            if (CfaUpdateOperStatusDown
                                (&(pTempPtr->MbsmPortInfo)) == CFA_FAILURE)
                            {
                                return (CFA_FAILURE);

                            }

                        }
                        if (IssGetColdStandbyFromNvRam () ==
                            ISS_COLDSTDBY_ENABLE)
                        {
                            /* Update port MTU after getting ATTACH indication */
                            if (CfaIsMgmtPort (u4IfIndex) != TRUE)
                            {
                                if (u1IfType == CFA_XE_ENET)
                                {
                                    if (CfaFsCfaHwGetMtu (u4IfIndex, &u4IfMtu)
                                        == FNP_FAILURE)
                                    {
                                        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                                 "Error in getting MTU for CFA_XE_ENET\r\n");
                                        u4IfMtu = CFA_ENET_MTU;
                                    }
                                }
                                else
                                {
                                    /* Update MTU only if it is not OOB port */
                                    MbsmGetPortMtu (u4IfIndex, &u4IfMtu);
                                }
                                CfaSetIfMtu ((UINT4) u4IfIndex, u4IfMtu);
                            }
                            if (CfaIssHwGetPortSpeed
                                (u4IfIndex, &i4IssPortCtrlSpeed) == FNP_SUCCESS)
                            {
                                CfaIssHwSetPortSpeed (u4IfIndex,
                                                      i4IssPortCtrlSpeed);
                            }
                            /* To update admin staus for higig ports */
                            MsrSetHigPortAdminStatus ();
                        }
                        if (CfaMbsmUpdateMtuToHw (u4IfIndex) == CFA_FAILURE)
                        {
                            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM, "Error in "
                                      "CfaMbsmUpdateMtuToHw - Setting "
                                      " MTU for interface %d\n", u4IfIndex);
                        }
                        /* Indications will not be given to Protocols in case of control
                           Plane Stacking . So Link Status Notification is done seperately */
                        if (u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL)
                            CfaIfmHandleInterfaceStatusChange (u4IfIndex,
                                                               CFA_IF_ADMIN
                                                               (u4IfIndex),
                                                               CFA_IF_DOWN,
                                                               CFA_IF_LINK_STATUS_CHANGE);
                    }
                    else if ((OSIX_FALSE == u1IsSetInPortList)
                             && (OSIX_TRUE == u1IsSetInPortListStatus))
                    {
                        /*Handling of Port Remove event */
                        if (CfaValidateIfIndex (u4IfIndex) != OSIX_SUCCESS)
                            continue;
                        CfaMbsmHandleInterfaceStatusUpdate (u4IfIndex,
                                                            CFA_IF_ADMIN
                                                            (u4IfIndex),
                                                            CFA_IF_NP);

                    }
                    else if ((u4StackingModel == ISS_DISS_STACKING_MODEL) &&
                             (OSIX_TRUE == u1IsSetInPortList) &&
                             (OSIX_TRUE == u1IsSetInPortListStatus))
                    {
                        /*Handling of Port Remove event */
                        if (CfaValidateIfIndex (u4IfIndex) != OSIX_SUCCESS)
                            continue;
                        CfaMbsmHandleInterfaceStatusUpdate (u4IfIndex,
                                                            CFA_IF_ADMIN
                                                            (u4IfIndex),
                                                            CFA_IF_NP);

                    }

                }
            }

            /* Update the Ethertype for connecting ports */
            if ((u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL) ||
                (u4StackingModel == ISS_DISS_STACKING_MODEL))
            {
                u4ConnectingPortCount =
                    pTempPtr->MbsmPortInfo.u4ConnectingPortCount;
                for (u4PortIndex = u4PortCount + 1;
                     u4TmpIndex < u4ConnectingPortCount;
                     u4PortIndex++, u4TmpIndex++)
                {
                    u4IfIndex = u4StartIfIndex + (u4PortIndex - 1);
                    if (CfaFsHwGetEthernetType
                        ((UINT2) u4IfIndex, &u1IfType) == FNP_FAILURE)
                    {
                        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM, "Error in "
                                  "CfaIfmInitEnetIfEntry - Getting "
                                  "Ethernet type for interface %d\n",
                                  u4IfIndex);
                    }
                    CfaSetEthernetType (u4IfIndex, u1IfType);

                }
            }

#ifdef RM_WANTED
            if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
            {
                RmGetNodeSwitchId (&u4NodeId);
                if (pTempPtr->MbsmSlotInfo.i4SlotId == (INT4) u4NodeId)
                {
                    /* After self slot attach, updating port mac address */
                    CfaUpdatePortMacAddressForSlot (pTempPtr->MbsmSlotInfo.
                                                    i4SlotId,
                                                    gIssSysGroupInfo.
                                                    BaseMacAddr);
                }

            }
#endif

            /* Indication should be given to Protocols only in case of
               Data Plane Stacking */
            if (u4StackingModel == ISS_DATA_PLANE_STACKING_MODEL)
            {
                SyncAckMsg.i4RetStatus = MBSM_SUCCESS;
                SyncAckMsg.i4SlotId = pTempPtr->MbsmSlotInfo.i4SlotId;

                MbsmSendSyncAckFromCfa (&SyncAckMsg);
            }
            else if (u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL)
            {
                if (MBSM_SLOT_INFO_IS_RM_PEER_UP_MSG (pSlotInfo) == OSIX_FALSE)
                {
                    if (CfaCreateRmIPInterface () == CFA_FAILURE)
                    {
                        i4RetStatus = CFA_FAILURE;
                        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                 "Error In CfaCreateRmIPInterface.\n");
                        break;
                    }
                    MbsmSendSelfAttachAckFromCfa ();
                }
                else
                {
                    /* When peer node comes up, hardware port info of peer node 
                     * should be set in our node and vice-versa to support dual 
                     * Node stacking for the features where Masking not applicable */
                    if (MBSM_SLOT_INFO_IS_RM_PEER_UP_MSG (pSlotInfo) ==
                        OSIX_TRUE)
                    {
                        u4StartIfIndex = pPortInfo->u4StartIfIndex;
                        for (u4IfIndex = u4StartIfIndex;
                             u4IfIndex <= (u4StartIfIndex + u4PortCount);
                             u4IfIndex++)
                        {
                            MEMSET (&HwIdInfo, 0, sizeof (tHwIdInfo));
                            HwIdInfo.u4IfIndex = u4IfIndex;
                            HwIdInfo.u1Status = FNP_TRUE;
                            CfaCfaNpGetHwInfo (&HwIdInfo);
                            CfaCfaNpRemoteSetHwInfo (&HwIdInfo);
                        }
                        MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
                        CfaCfaNpGetHwPortInfo (&HwPortInfo);
                        for (u4IfIndex = HwPortInfo.u4StartIfIndex;
                             u4IfIndex <= HwPortInfo.u4EndIfIndex; u4IfIndex++)
                        {
                            MEMSET (&HwIdInfo, 0, sizeof (tHwIdInfo));
                            HwIdInfo.u4IfIndex = u4IfIndex;
                            HwIdInfo.u1Status = FNP_TRUE;
                            CfaCfaNpGetHwInfo (&HwIdInfo);
                            CfaCfaNpRemoteSetHwInfo (&HwIdInfo);
                        }
                    }
                    SyncAckMsg.i4RetStatus = MBSM_SUCCESS;
                    SyncAckMsg.i4SlotId = pTempPtr->MbsmSlotInfo.i4SlotId;

                    MbsmSendSyncAckFromCfa (&SyncAckMsg);

                }
            }
            else if (u4StackingModel == ISS_DISS_STACKING_MODEL)
            {
                if (CfaCreateRmIPInterface () == CFA_FAILURE)
                {
                    i4RetStatus = CFA_FAILURE;
                    CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                             "Error In CfaCreateRmIPInterface.\n");
                    break;
                }
                MbsmSendSelfAttachAckFromCfa ();

                SyncAckMsg.i4RetStatus = MBSM_SUCCESS;
                SyncAckMsg.i4SlotId = pTempPtr->MbsmSlotInfo.i4SlotId;
                MbsmSendSyncAckFromCfa (&SyncAckMsg);
            }

        }
        else if (pTempPtr->MbsmSlotInfo.i1Status == MBSM_STATUS_DOWN)
        {
            if (u4StackingModel == ISS_DATA_PLANE_STACKING_MODEL)
            {
                if (CfaMbsmGddDeInit (pSlotInfo) == CFA_FAILURE)
                {
                    CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                             "Error In CfaMbsmGddDeInit.\n");
                    i4RetStatus = CFA_FAILURE;
                    break;
                }
            }
            /* Line Card Oper Down, so interface Oper Status made Down */
            CfaUpdateOperStatusDown (&(pTempPtr->MbsmPortInfo));
        }
        else if (pTempPtr->MbsmSlotInfo.i1Status == MBSM_STATUS_NP)
        {
            if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
            {
                if ((IssGetStackPortCountFromNvRam () != 0) &&
                    (pTempPtr->MbsmSlotInfo.i4SlotId != 0))
                {
                    if (u4StackingModel == ISS_DATA_PLANE_STACKING_MODEL)
                    {
                        if (CfaMbsmGddDeInit (pSlotInfo) == CFA_FAILURE)
                        {
                            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                     "Error In CfaMbsmGddDeInit.\n");
                            i4RetStatus = CFA_FAILURE;
                            break;
                        }
                    }
                }
            }
            else
            {
                if (u4StackingModel == ISS_DATA_PLANE_STACKING_MODEL)
                {
                    if (CfaMbsmGddDeInit (pSlotInfo) == CFA_FAILURE)
                    {
                        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                 "Error In CfaMbsmGddDeInit.\n");
                        i4RetStatus = CFA_FAILURE;
                        break;
                    }
                }
            }
            if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
            {
                /* In standalone case, need to create only slot 0 ports.
                 * If there is no entry in slotmodule.conf
                 * for slot 0,after getting attach
                 * indication, ports will be created */

                if (((IssGetStackPortCountFromNvRam () == 0) &&
                     (pTempPtr->MbsmSlotInfo.i4SlotId == 0)) ||
                    (IssGetStackPortCountFromNvRam () != 0))
                {

                    if (CFA_IF_ENTRY_USED
                        (pTempPtr->MbsmSlotInfo.u4StartIfIndex))
                    {
                        /* Interfaces already created, so just update Oper Status */
                    }
                    else
                    {
                        /* case of Pre-configuration, so interfaces has to be created ,
                         * and then update oper status
                         */
                        CfaCreateInterfacesForSlot (pTempPtr->MbsmSlotInfo.
                                                    i4SlotId,
                                                    &(pTempPtr->MbsmPortInfo));
                    }

                    /* update Oper Status */
                    CfaUpdateOperStatusNotPresent (&(pTempPtr->MbsmPortInfo));

                    if (&(pTempPtr->MbsmPortInfo) == NULL)
                    {
                        if (u1IsBufLinear == FALSE)
                        {
                            MemReleaseMemBlock (gCfaMbsmSlotMsgPoolId,
                                                (UINT1 *) (pSlotInfoMsg));
                        }
                        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                 "Error In CfaHandleIfUpdatePktFromMbsm "
                                 "- NULL message.\n");
                        return (CFA_FAILURE);
                    }
                }
            }
            else
            {
                if (CFA_IF_ENTRY_USED (pTempPtr->MbsmSlotInfo.u4StartIfIndex))
                {
                    /* Interfaces already created, so just update Oper Status */
                }
                else
                {
                    /* case of Pre-configuration, so interfaces
                     * has to be created , and then update
                     * oper status */
                    CfaCreateInterfacesForSlot (pTempPtr->MbsmSlotInfo.i4SlotId,
                                                &(pTempPtr->MbsmPortInfo));
                }

                /* update Oper Status */
                CfaUpdateOperStatusNotPresent (&(pTempPtr->MbsmPortInfo));

                if (&(pTempPtr->MbsmPortInfo) == NULL)
                {
                    if (u1IsBufLinear == FALSE)
                    {
                        MemReleaseMemBlock (gCfaMbsmSlotMsgPoolId,
                                            (UINT1 *) (pSlotInfoMsg));
                    }
                    CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                             "Error In CfaHandleIfUpdatePktFromMbsm "
                             "- NULL message.\n");
                    return (CFA_FAILURE);
                }
            }
#ifdef RM_WANTED
            if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
            {
                /* Whenever self slot is getting detached, it will not get the 
                 * operstate indication;So after created the port in CFA
                 * operstatus of stack ports needs to be updated */

                RmGetNodeSwitchId (&u4NodeId);
                if (pTempPtr->MbsmSlotInfo.i4SlotId == (INT4) u4NodeId)
                {
                    i4Index = CFA_STACK_PORT_START_INDEX;
                    CFA_CDB_SCAN_WITH_LOCK (i4Index,
                                            (MBSM_MAX_PORTS_PER_SLOT +
                                             ISS_MAX_STK_PORTS), CFA_ALL_IFTYPE)
                    {
                        /* Flexible ifIndex changes 
                         */
                        if (i4Index > (MBSM_MAX_PORTS_PER_SLOT +
                                       ISS_MAX_STK_PORTS))
                        {
                            break;
                        }
                        /* In Standby Node, all stack ports operstatus will be up */
                        CfaMbsmHandleInterfaceStatusUpdate (i4Index, CFA_IF_UP,
                                                            CFA_IF_UP);
                    }
                    /* There will not be any L2 entry for this STACK VLAN 4094.
                     * So we need to trigger operstatus update for StackIVR.
                     */
                    CfaIfmHandleInterfaceStatusChange
                        (CFA_DEFAULT_STACK_IFINDEX, CFA_IF_UP, CFA_IF_UP,
                         CFA_FALSE);
                }
            }
#endif
            u4StartIfIndex = pTempPtr->MbsmPortInfo.u4StartIfIndex;
            if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
            {
                u4PortCount = MBSM_MAX_PORTS_PER_SLOT;
            }
            else
            {
                u4PortCount = pTempPtr->MbsmPortInfo.u4PortCount;
            }

            for (u4PortIndex = 1; u4PortIndex <= u4PortCount; u4PortIndex++)
            {
                u4IfIndex = u4StartIfIndex + (u4PortIndex - 1);
                /* EtherType is set here based on the port speed of the port
                 * in the Card. This is only the Preconfiguration case.
                 * When the actual card attached, this value is updated 
                 * accordingly.
                 */

                i4CardIndex = (pTempPtr->MbsmSlotInfo.i4CardType);
                u4PortNum = 1;    /* To get the Port type of the card
                                 * it is sufficient to check the 
                                 * portSpeed Type for at least one 
                                 * port. So we are cheacking for the 
                                 * first port only.
                                 */

                MbsmGetPortSpeedTypeFromCardIndex (i4CardIndex, u4PortIndex,
                                                   &u1SpeedType);

                switch (u1SpeedType)
                {
                    case MBSM_SPEED_FAST_ETHER:
                        u1SpeedType = CFA_FA_ENET;
                        break;
                    case MBSM_SPEED_ETHER:
                    case MBSM_SPEED_GIG_ETHER:
                        u1SpeedType = CFA_GI_ENET;
                        break;
                    case MBSM_SPEED_10GIG_ETHER:
                        u1SpeedType = CFA_XE_ENET;
                        break;
                    case MBSM_SPEED_40GIG_ETHER:
                        u1SpeedType = CFA_XL_ENET;
                        break;
                    case MBSM_SPEED_56GIG_ETHER:
                        u1SpeedType = CFA_LVI_ENET;
                        break;
                    case MBSM_SPEED_NULL:
                    case MBSM_SPEED_INVALID:
                    default:
                        u1SpeedType = CFA_ENET_UNKNOWN;
                }
                CfaSetEthernetType (u4IfIndex, u1SpeedType);
            }
        }
        /* Point to the next SlotPortInfo */
        pTempPtr++;
    }

    if (u1IsBufLinear == FALSE)
    {
        MemReleaseMemBlock (gCfaMbsmSlotMsgPoolId, (UINT1 *) (pSlotInfoMsg));
    }
    if (pTempPtr->MbsmSlotInfo.i1Status == MBSM_STATUS_ACTIVE)
    {
        if (((u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL) ||
             (u4StackingModel == ISS_DISS_STACKING_MODEL)) &&
            (i4RetStatus == CFA_FAILURE))
        {
            if (MBSM_SLOT_INFO_IS_RM_PEER_UP_MSG (pSlotInfo) == OSIX_FALSE)
            {
                MbsmSendSelfAttachAckFromCfa ();
            }
        }
    }

    return i4RetStatus;
}

/*****************************************************************************
 *
 *    Function Name        : CfaUpdateOperStatusNotPresent
 *
 *    Description          : This function is used to set the Oper status of  
 *                           the interfaces associated to a slot to not present
 *                           when the slot is removed.
 *
 *    Input(s)             : pMsg - Pointer to the message containig slot   
 *                           information.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable
 *
 *    Global Variables Modified : gaIfTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaUpdateOperStatusNotPresent (tMbsmPortInfo * pPortInfo)
{
    UINT4               u4PortCount;
    UINT4               u4StartIfIndex;
    UINT4               u4Index;
    UINT4               u4IfIndex;
    tCfaIfInfo         *pCfaIfInfo = NULL;

    if (!pPortInfo)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaUpdateOperStatusNotPresent- NULL message.\n");
        return (CFA_FAILURE);
    }

    u4StartIfIndex = pPortInfo->u4StartIfIndex;
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        u4PortCount = MBSM_MAX_PORTS_PER_SLOT;
    }
    else
    {
        u4PortCount = pPortInfo->u4PortCount;
    }
    for (u4Index = 0; u4Index < u4PortCount; u4Index++)
    {
        u4IfIndex = u4StartIfIndex + u4Index;

        CFA_DS_LOCK ();

        pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

        CFA_DS_UNLOCK ();

        if (NULL != pCfaIfInfo)
        {
            if (CfaValidateIfIndex (u4IfIndex) != OSIX_SUCCESS)
                continue;

            CfaMbsmHandleInterfaceStatusUpdate (u4IfIndex,
                                                CFA_IF_ADMIN (u4IfIndex),
                                                CFA_IF_NP);
        }
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaUpdateOperStatusDown
 *
 *    Description          : This function is used to set the Oper status of  
 *                           the interfaces associated to a slot to Oper Down  
 *                           when the slot Oper Status is Down.
 *
 *    Input(s)             : pMsg - Pointer to the message containig slot   
 *                           information.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable
 *
 *    Global Variables Modified : gaIfTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaUpdateOperStatusDown (tMbsmPortInfo * pPortInfo)
{
    UINT4               u4PortCount;
    UINT4               u4StartIfIndex;
    UINT4               u4Index;
    UINT4               u4IfIndex;
    tCfaIfInfo         *pCfaIfInfo = NULL;

    if (!pPortInfo)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaUpdateOperStatusDown - NULL message.\n");
        return (CFA_FAILURE);
    }

    u4StartIfIndex = pPortInfo->u4StartIfIndex;
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        u4PortCount = MBSM_MAX_PORTS_PER_SLOT;
    }
    else
    {
        u4PortCount = pPortInfo->u4PortCount;
    }

    for (u4Index = 0; u4Index < u4PortCount; u4Index++)
    {
        u4IfIndex = u4StartIfIndex + u4Index;

        CFA_DS_LOCK ();

        pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

        CFA_DS_UNLOCK ();

        if (NULL != pCfaIfInfo)
        {
            if (CfaValidateIfIndex (u4IfIndex) != OSIX_SUCCESS)
                continue;

            CfaMbsmHandleInterfaceStatusUpdate (u4IfIndex,
                                                CFA_IF_ADMIN (u4IfIndex),
                                                CFA_IF_DOWN);
        }
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaMbsmDeleteInterfacesForAllSlot
 *
 *    Description          : This function is used to delete interfaces for 
 *                           all the slots present in system      
 *
 *    Input(s)             : None
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaMbsmDeleteInterfacesForAllSlot ()
{
    tMbsmPortInfo       MbsmPortInfo;
    tMbsmSlotInfo       MbsmSlotInfo;
    INT4                i4SlotNum = 0;
    INT4                i4RetVal = CFA_FAILURE;

    for (i4SlotNum = MBSM_SLOT_INDEX_START;
         i4SlotNum < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START); i4SlotNum++)
    {
        MEMSET (&MbsmPortInfo, 0, sizeof (tMbsmPortInfo));
        MEMSET (&MbsmSlotInfo, 0, sizeof (tMbsmSlotInfo));
        if (MbsmGetPortInfo (i4SlotNum, &MbsmPortInfo) == MBSM_FAILURE)
        {
            return CFA_FAILURE;
        }
        if (MbsmGetSlotInfo (i4SlotNum, &MbsmSlotInfo) == MBSM_FAILURE)
        {
            return CFA_FAILURE;
        }
        i4RetVal = CfaDeleteInterfacesForSlot (&MbsmPortInfo);
    }
    return i4RetVal;
}

/*****************************************************************************
 *
 *    Function Name        : CfaMbsmCreateInterfacesForAllSlot
 *
 *    Description          : This function is used to create interfaces for 
 *                           all the slots present in system      
 *
 *    Input(s)             : None
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaMbsmCreateInterfacesForAllSlot ()
{
    tMbsmPortInfo       MbsmPortInfo;
    tMbsmSlotInfo       MbsmSlotInfo;
    tHwPortInfo         HwPortInfo;
    INT4                i4SlotNum = 0;
    UINT4               u4Index = 0;
    UINT4               u4PortCount = 0;
    INT4                i4PortCtrlSpeed = 0;

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    for (i4SlotNum = MBSM_SLOT_INDEX_START;
         i4SlotNum < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START); i4SlotNum++)
    {
        MEMSET (&MbsmPortInfo, 0, sizeof (tMbsmPortInfo));
        MEMSET (&MbsmSlotInfo, 0, sizeof (tMbsmSlotInfo));
        if (MbsmGetPortInfo (i4SlotNum, &MbsmPortInfo) == MBSM_FAILURE)
        {
            return CFA_FAILURE;
        }
        if (MbsmGetSlotInfo (i4SlotNum, &MbsmSlotInfo) == MBSM_FAILURE)
        {
            return CFA_FAILURE;
        }
        CfaCreateInterfacesForSlot (i4SlotNum, &MbsmPortInfo);
        if (MbsmSlotInfo.i1Status == MBSM_STATUS_NP)
        {
            if (CfaUpdateOperStatusNotPresent (&MbsmPortInfo) == CFA_FAILURE)
            {
                return (CFA_FAILURE);
            }
        }
        else
        {
            if (CfaUpdateOperStatusDown (&MbsmPortInfo) == CFA_FAILURE)
            {
                return (CFA_FAILURE);

            }
            if ((ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL) ||
                (ISS_GET_STACKING_MODEL () == ISS_DISS_STACKING_MODEL))
            {
                /* Fetching boot up values from default port property 
                 * structure gCfaDefaultPortInfo and populating CFA */
                u4PortCount = (SYS_DEF_MAX_DATA_PORT_COUNT +
                               (HwPortInfo.u4NumConnectingPorts * 2));
                for (u4Index = 1; u4Index < u4PortCount; u4Index++)
                {
                    CfaSetIfSpeed (u4Index,
                                   gCfaDefaultPortInfo[u4Index].u4IfSpeed);
                    CfaSetIfHighSpeed (u4Index,
                                       gCfaDefaultPortInfo[u4Index].
                                       u4IfHighSpeed);
                    CfaSetIfDuplexStatus (u4Index,
                                          gCfaDefaultPortInfo[u4Index].
                                          u4DuplexStatus);
                    CfaSetIfAutoNegStatus (u4Index,
                                           gCfaDefaultPortInfo[u4Index].
                                           u4AutoNegStatus);
                    IssSysConvertCfaSpeedToIssSpeed (gCfaDefaultPortInfo
                                                     [u4Index].u4IfSpeed,
                                                     gCfaDefaultPortInfo
                                                     [u4Index].u4IfHighSpeed,
                                                     &i4PortCtrlSpeed);
                    CfaIssHwSetPortSpeed (u4Index, i4PortCtrlSpeed);
                    if (gCfaDefaultPortInfo[u4Index].u4DuplexStatus ==
                        ETH_STATS_DUP_ST_HALFDUP)
                    {
                        IsssysIssHwSetPortDuplex (u4Index, ISS_HALFDUP);
                    }
                    else if (gCfaDefaultPortInfo[u4Index].u4DuplexStatus ==
                             ETH_STATS_DUP_ST_FULLDUP)
                    {
                        IsssysIssHwSetPortDuplex (u4Index, ISS_FULLDUP);
                    }
                    if (gCfaDefaultPortInfo[u4Index].u4AutoNegStatus ==
                        CFA_ENABLED)
                    {
                        IsssysIssHwSetPortMode (u4Index, ISS_AUTO);
                    }
                    else if (gCfaDefaultPortInfo[u4Index].u4AutoNegStatus ==
                             CFA_DISABLED)
                    {
                        IsssysIssHwSetPortMode (u4Index, ISS_NONEGOTIATION);
                    }
                }

            }

        }
    }
    return CFA_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name        : CfaCreateInterfacesForSlot
 *
 *    Description          : This function is used to create interface for the
 *                           ports present for the Slot Id passed to this      
 *                           function.                 
 *
 *    Input(s)             : i4SlotId - Slot for which interfaces are created.
 *                         : pMsg - Pointer to the message containig slot   
 *                           information.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable
 *
 *    Global Variables Modified : gaIfTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaCreateInterfacesForSlot (INT4 i4SlotId, tMbsmPortInfo * pPortInfo)
{
    UINT4               u4PortCount;
    UINT4               u4StartIfIndex;
    UINT4               u4PortIndex;
    UINT4               u4IfIndex;
    UINT4               u4NameLen = 0;
    UINT4               u4Index = 0;
    INT1                ai1AliasName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4StackingModel = 0;
    UINT4               u4ConnectingPortCount = 0;
    if (!pPortInfo)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaCreateInterfacesForSlot - NULL message.\n");
        return (CFA_FAILURE);
    }
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            /*This check prevents creation of ports in Slot0
               When when switch configured for stacking */
            if (i4SlotId == 0)
            {
                return CFA_FAILURE;
            }
        }
    }
    u4StackingModel = ISS_GET_STACKING_MODEL ();

    if ((u4StackingModel != ISS_CTRL_PLANE_STACKING_MODEL) &&
        (u4StackingModel != ISS_DISS_STACKING_MODEL))
    {
        if (CFA_ENET_CREATE_DELETE_ALLOWED)
        {
            /* If Enet port creetion/deletion is allowed through configuration 
             * then it should NOT happen by default when a slot is
             * inserted/preconfigured. E.g. MI case */
            return CFA_SUCCESS;
        }
    }

    sprintf ((char *) ai1AliasName, "%s%d/", CFA_MBSM_ALIAS_PREFIX, i4SlotId);

    u4StartIfIndex = pPortInfo->u4StartIfIndex;
    u4PortCount = pPortInfo->u4PortCount;

    u4NameLen = STRLEN (ai1AliasName);
    if (u4PortCount > (UINT4) MBSM_MAX_PORTS_PER_SLOT)
    {
        u4PortCount = MBSM_MAX_PORTS_PER_SLOT;
    }

    for (u4PortIndex = 1; u4PortIndex <= u4PortCount; u4PortIndex++)
    {
        sprintf ((char *) &(ai1AliasName[u4NameLen]), "%u", u4PortIndex);
        u4IfIndex = u4StartIfIndex + (u4PortIndex - 1);
        CfaMbsmIfmBringUpInterface (u4IfIndex, ai1AliasName);
    }
    /* Create the Connecting Interfaces for Dual Unit Stacking */
    if ((u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL) ||
        (u4StackingModel == ISS_DISS_STACKING_MODEL))
    {
        u4ConnectingPortCount = pPortInfo->u4ConnectingPortCount;
        for (u4PortIndex = u4PortCount + 1; u4Index < u4ConnectingPortCount;
             u4PortIndex++, u4Index++)
        {
            sprintf ((char *) &(ai1AliasName[u4NameLen]), "%u", u4PortIndex);
            u4IfIndex = u4StartIfIndex + (u4PortIndex - 1);
            CfaMbsmIfmBringUpConnectingInterface (u4IfIndex, ai1AliasName);
        }

    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaUpdateIfParamsForSlot
 *
 *    Description          : This function is used to update the interface    
 *                           parameters for the ports which are physically     
 *                           present.                  
 *
 *    Input(s)             : pMsg - Pointer to the message containig slot   
 *                           information.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable
 *
 *    Global Variables Modified : gaIfTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaUpdateIfParamsForSlot (tMbsmPortInfo * pPortInfo)
{
    UINT4               u4PortCount;
    UINT4               u4StartIfIndex;
    UINT4               u4Index;
    UINT4               u4IfIndex;
    UINT1               u1IsSetInPortList = OSIX_FALSE;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;

    if (!pPortInfo)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaUpdateIfParamsForSlot - NULL message.\n");
        return (CFA_FAILURE);
    }

    u4StartIfIndex = pPortInfo->u4StartIfIndex;
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        u4PortCount = MBSM_MAX_PORTS_PER_SLOT;
    }
    else
    {
        u4PortCount = pPortInfo->u4PortCount;
    }

    for (u4Index = 0; u4Index < u4PortCount; u4Index++)
    {
        /* Check wether portlist bit is set/not If set only process otherwise simply continue */
        u4IfIndex = u4StartIfIndex + u4Index;
        OSIX_BITLIST_IS_BIT_SET (pPortInfo->PortList, u4IfIndex,
                                 sizeof (tPortList), u1IsSetInPortList);
        OSIX_BITLIST_IS_BIT_SET (pPortInfo->PortListStatus, u4IfIndex,
                                 sizeof (tPortList), u1IsSetInPortListStatus);

        /* Insert Event for this port happens */
        if ((u1IsSetInPortList == OSIX_TRUE)
            && (u1IsSetInPortListStatus == OSIX_FALSE))
        {
            if (CfaValidateIfIndex (u4IfIndex) != OSIX_SUCCESS)
            {
                continue;
            }
            /* During new slot insertion, all interface parameters 
             * for the newly attached slot are updated here */
            CfaUpdateHwParams (u4IfIndex);
        }
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmUpdateIfParams 
 *
 *    Description          : This function updates the interface parameters 
 *                           such as HW MAC, MTU and Speed.
 *
 *    Input(s)             : u4IfIndex - Interface index to be updated.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None,
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : CFA_SUCCESS/CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaIfmUpdateIfParams (UINT4 u4IfIndex)
{
    UINT4               u4IfSpeed;
    UINT4               u4IfHighSpeed;

    if (CfaValidateIfIndex (u4IfIndex) == OSIX_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmUpdateIfParams- "
                 "Invalid IfIndex - FAILURE.\n");
        return CFA_FAILURE;
    }

    CfaDetermineSpeed (u4IfIndex, &u4IfSpeed, &u4IfHighSpeed);
    CfaSetIfSpeed (u4IfIndex, u4IfSpeed);
    CfaSetIfHighSpeed (u4IfIndex, u4IfHighSpeed);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaMbsmHwConfig 
 *
 *    Description          : This function proceeds with the hardware       
 *                           configurations that were previously blocked due
 *                           to hardware not present.
 *
 *    Input(s)             : u4IfIndex - Interface index to be updated.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None,
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : CFA_SUCCESS/CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaMbsmHwConfig (UINT4 u4IfIndex)
{
    UINT1               au1HwAddr[CFA_MAX_MEDIA_ADDR_LEN];
    UINT4               u4IfMtu;
    UINT4               u4IfSpeed;
    UINT4               u4IfHighSpeed;

    if (CfaValidateIfIndex (u4IfIndex) == OSIX_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaMbsmHwConfig - " "Invalid IfIndex - FAILURE.\n");
        return CFA_FAILURE;
    }

    if (CfaGddGetOsHwAddr (u4IfIndex, au1HwAddr) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitEnetIfEntry - "
                  "unable to get the socket or MAC address for interface %d\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

    MbsmGetPortMtu (u4IfIndex, &u4IfMtu);

    CfaSetIfMtu (u4IfIndex, u4IfMtu);

    CfaDetermineSpeed (u4IfIndex, &u4IfSpeed, &u4IfHighSpeed);
    CfaSetIfSpeed (u4IfIndex, u4IfSpeed);
    CfaSetIfHighSpeed (u4IfIndex, u4IfHighSpeed);

    return (CFA_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmCfaCardInsert                                       */
/*                                                                           */
/* Description  : This function calls the respective protocol function to    */
/*                update the HW configuration when a card is inserted into   */
/*                the MBSM System.                                           */
/*                                                                           */
/* Input        : pPortMsg  - Pointer to Port Message Info.                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssMbsmCfaCardInsert (tMbsmProtoMsg * pProtMsg)
{
    tMbsmPortInfo      *pPortInfo = NULL;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    INT4                i4RetStatus = MBSM_SUCCESS;

    pPortInfo = &pProtMsg->MbsmPortInfo;
    pSlotInfo = &pProtMsg->MbsmSlotInfo;

#ifdef ISS_WANTED

    if (i4RetStatus == MBSM_SUCCESS)
    {
        i4RetStatus = IssMbsmCardInsertConfigCtrlTable (pPortInfo, pSlotInfo);
    }
    if (i4RetStatus == MBSM_SUCCESS)
    {
        i4RetStatus = IssMbsmCardInsertMirrorCtrlTable (pPortInfo, pSlotInfo);
    }

    if (i4RetStatus == MBSM_SUCCESS)
    {
        i4RetStatus = IssMbsmCardInsertRateCtrlTable (pPortInfo, pSlotInfo);
    }

    if (i4RetStatus == MBSM_SUCCESS)
    {
        i4RetStatus = IssMbsmCardInsertPortCtrlTable (pPortInfo, pSlotInfo);
    }

    if (i4RetStatus == MBSM_SUCCESS)
    {
        i4RetStatus = IssMbsmL2FilterCardInsert (pPortInfo, pSlotInfo);
    }

    if (i4RetStatus == MBSM_SUCCESS)
    {
        i4RetStatus = IssMbsmL3FilterCardInsert (pPortInfo, pSlotInfo);
    }

    if (i4RetStatus == MBSM_SUCCESS)
    {
        i4RetStatus = IssPIMbsmCardInsertPITable (pPortInfo, pSlotInfo);
    }

#ifdef DIFFSRV_WANTED
    if (i4RetStatus == MBSM_SUCCESS)
    {
        i4RetStatus = IssMbsmDiffservCardInsert (pPortInfo, pSlotInfo);
    }
#endif
    if (i4RetStatus == MBSM_SUCCESS)
    {
        i4RetStatus = IssMbsmCardInsertCpuMirroring (pSlotInfo);
    }
#endif /* ISS_WANTED */

    return i4RetStatus;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmCfaCardRemove                                        */
/*                                                                           */
/* Description  : This function calls the respective protocol function to   */
/*                update the HW configuration when a card is removed from    */
/*                the MBSM System.                                           */
/*                                                                           */
/* Input        : pPortMsg  - Pointer to Port Message Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssMbsmCfaCardRemove (tMbsmProtoMsg * pProtMsg)
{
    tMbsmPortInfo      *pPortInfo = NULL;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    INT4                i4RetStatus = MBSM_SUCCESS;

    pPortInfo = &pProtMsg->MbsmPortInfo;
    pSlotInfo = &pProtMsg->MbsmSlotInfo;

#ifdef ISS_WANTED
/* Check port/card message*/
    if (!MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
    {
        if (i4RetStatus == MBSM_SUCCESS)
        {
            i4RetStatus = IssMbsmCardRemoveMirrorCtrlTable (pPortInfo, pSlotInfo);
        }
        if (i4RetStatus == MBSM_SUCCESS)
        {
            i4RetStatus = IssMbsmL2FilterCardRemove (pPortInfo, pSlotInfo);
        }

        if (i4RetStatus == MBSM_SUCCESS)
        {
            i4RetStatus = IssMbsmL3FilterCardRemove (pPortInfo, pSlotInfo);
        }

        if (i4RetStatus == MBSM_SUCCESS)
        {
            i4RetStatus = IssPIMbsmCardRemovePITable (pPortInfo, pSlotInfo);
        }
    }
#endif /* ISS_WANTED */

    return i4RetStatus;
}

/*****************************************************************************
 *
 *    Function Name        : CfaDeleteInterfacesForSlot
 *
 *    Description          : This function is used to delete interface for the
 *                           ports present for the Slot Id passed to this      
 *                           function.                 
 *
 *    Input(s)             : i4SlotId - Slot for which interfaces are deleted.
 *                         : pMsg - Pointer to the message containig slot   
 *                           information.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable
 *
 *    Global Variables Modified : gaIfTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaDeleteInterfacesForSlot (tMbsmPortInfo * pPortInfo)
{
    UINT4               u4PortCount;
    UINT4               u4StartIfIndex;
    UINT4               u4Index;
    UINT4               u4IfIndex;
    UINT1               u1IfType;

    if (!pPortInfo)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaDeleteInterfacesForSlot - NULL message.\n");
        return (CFA_FAILURE);
    }

    /* First destroy all the SISP logical interfaces running over these
     * interfaces
     * */
    CfaMbsmDelSispInterfacesForSlot (pPortInfo);

    u4StartIfIndex = pPortInfo->u4StartIfIndex;
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        u4PortCount = MBSM_MAX_PORTS_PER_SLOT;
    }
    else
    {
        u4PortCount = pPortInfo->u4PortCount;
    }

    for (u4Index = 0; u4Index < u4PortCount; u4Index++)
    {
        u4IfIndex = u4StartIfIndex + u4Index;

        if (CfaGetIfType (u4IfIndex, &u1IfType) == CFA_FAILURE)
        {
            continue;
        }

        switch (u1IfType)
        {
                /* Support exists for Ethernet interfaces only, 
                 * other interfaces can be added later */
            case CFA_ENET:
                CfaIfmDeleteEnetInterface (u4IfIndex, CFA_TRUE);
                break;

            default:
                break;
        }
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaHandlePreConfigDelFromMbsm
 *
 *    Description          : This function is called by CFA on receiving a    
 *                           Pre-Config Delete mesasge from MBSM.
 *
 *    Input(s)             : pMsg - Pointer to the message to be posted to  
 *                           CFA.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaHandlePreConfigDelFromMbsm (tMbsmCfaHwInfoMsg * pMsg)
{
    INT4                i4NumEntries = 0;
    INT4                i4Index = 0;
    tMbsmSlotPortInfo  *pTempPtr = NULL;

    if (!pMsg)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandlePreConfigDelFromMbsm - NULL message.\n");
        return (CFA_FAILURE);
    }

    i4NumEntries = pMsg->i4NumEntries;
    if (i4NumEntries)
    {
        pTempPtr = &(pMsg->FirstEntry);
    }

    for (i4Index = 0; i4Index < i4NumEntries; i4Index++)
    {
        /* Delete the interfaced corresponding to the Slot */
        CfaDeleteInterfacesForSlot (&(pTempPtr->MbsmPortInfo));
        pTempPtr++;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaHandleCardUpdateFromMbsm
 *
 *    Description          : This function is called by CFA on receiving a    
 *                           card update mesasge from MBSM.
 *
 *    Input(s)             : pMsg - Pointer to the message to be posted to  
 *                           CFA.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaHandleCardUpdateFromMbsm (UINT1 *pMsg, INT1 i1IsInsert)
{
    tMbsmProtoMsg      *pSlotInfoMsg;
    tMbsmProtoAckMsg    protoAckMsg;
    INT4                i4RetStatus = MBSM_SUCCESS;
    UINT1               u1IsBufLinear = TRUE;

    pSlotInfoMsg =
        (tMbsmProtoMsg *) CRU_BUF_Get_DataPtr_IfLinear
        ((tCRU_BUF_CHAIN_HEADER *) (VOID *) pMsg, 0, (sizeof (tMbsmProtoMsg)));

    if (pSlotInfoMsg == NULL)
    {
        if ((pSlotInfoMsg = (tMbsmProtoMsg *) MemAllocMemBlk (gCfaMbsmProtoMsgPoolId)) == NULL)
        {

            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaHandleCardUpdateFromMbsm - No Memory -"
                     "FAILURE.\n");
            return (CFA_FAILURE);
        }

        u1IsBufLinear = FALSE;

        /* for aysnc the copy would be done by the byte stuffing function */
        if (CRU_BUF_Copy_FromBufChain ((tCRU_BUF_CHAIN_HEADER *) (VOID *) pMsg, 
                                        (UINT1 *)pSlotInfoMsg, 0, 
                                         (sizeof (tMbsmProtoMsg))) == CRU_FAILURE)
        {
            CFA_DBG (CFA_TRC_ALL_TRACK | CFA_TRC_ERROR, CFA_GDD,
                     "Error in CfaHandleCardUpdateFromMbsm - "
                     "Copy_FromBufChain failed.\n");
            MemReleaseMemBlock (gCfaMbsmProtoMsgPoolId,
                                (UINT1 *) (pSlotInfoMsg));
            return CFA_FAILURE;
        }
    }


    if (i1IsInsert)
    {
        /* Create the IVR interface in the new slot */
        i4RetStatus = CfaMbsmCreateIvrInterface (pSlotInfoMsg);
        /* Set the Mac address for the ports in the Slot */
        if (i4RetStatus == MBSM_SUCCESS)
        {
            i4RetStatus = CfaMbsmInterfaceMacAddr (pSlotInfoMsg);
        }

        if (i4RetStatus == MBSM_SUCCESS)
        {
            /* Card Insertion event */
            i4RetStatus = IssMbsmCfaCardInsert (pSlotInfoMsg);
        }
#ifdef MPLS_WANTED
        if (i4RetStatus == MBSM_SUCCESS)
        {
            /* Create the MPLS or MPLS Tunnel interface in the new slot */
            i4RetStatus = CfaMbsmCreateMplsInterface (pSlotInfoMsg);
        }
#endif
        if (i4RetStatus == MBSM_SUCCESS)
        {
            i4RetStatus = CfaMbsmCreateCustomParam (pSlotInfoMsg);
        }

        if (i4RetStatus == MBSM_SUCCESS)
        {
            i4RetStatus = CfaMbsmCardInsertIvrMapTable (pSlotInfoMsg);
        }

    }
    else
    {
        /* Card Removal event */
        i4RetStatus = IssMbsmCfaCardRemove (pSlotInfoMsg);
    }

    protoAckMsg.i4RetStatus = i4RetStatus;
    protoAckMsg.i4SlotId = pSlotInfoMsg->MbsmSlotInfo.i4SlotId;
    protoAckMsg.i4ProtoCookie = pSlotInfoMsg->i4ProtoCookie;

    if (i4RetStatus == MBSM_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         " CfaHandleCardUpdateFromMbsm handled a card update message from MBSM and returns success \n");
    }

    MbsmSendAckFromProto (&protoAckMsg);

    if (u1IsBufLinear == FALSE)
    {
        MemReleaseMemBlock (gCfaMbsmProtoMsgPoolId,
                              (UINT1 *) (pSlotInfoMsg));
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaHandleLinkStatusUpdateMsgFromMbsm 
 *
 *    Description          : This function is called by CFA on receiving
 *                           a link status update message from MBSM.
 *    Input(s)             : pMsg - Pointer to the message to be posted to  
 *                           CFA.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaHandleLinkStatusUpdateMsgFromMbsm (UINT1 *pMsg)
{
    tMbsmProtoMsg      *pSlotInfoMsg;
    tMbsmProtoAckMsg    protoAckMsg;
    INT4                i4RetStatus = MBSM_SUCCESS;
    tMbsmPortInfo      *pMbsmPortInfo;
    UINT1               u1IsSetInPortList = OSIX_FALSE;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;
    UINT1               u1LinkStatus;
    UINT1               u1OperStatus;
    UINT1               u1OldOperStatus = CFA_IF_DOWN;
    UINT4               u4IfIndex;
    UINT1               u1IsBufLinear = TRUE;

    pSlotInfoMsg =
        (tMbsmProtoMsg *) CRU_BUF_Get_DataPtr_IfLinear ((tCRU_BUF_CHAIN_HEADER
                                                         *) (VOID *) pMsg,
                                                        0,
                                                        (sizeof
                                                         (tMbsmProtoMsg)));
    if (pSlotInfoMsg == NULL)
    {
        if ((pSlotInfoMsg = (tMbsmProtoMsg *) MemAllocMemBlk (gCfaMbsmProtoMsgPoolId)) == NULL)
        {

            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaHandleLinkStatusUpdateMsgFromMbsm - No Memory -"
                     "FAILURE.\n");
            return (CFA_FAILURE);
        }

        u1IsBufLinear = FALSE;

        /* for aysnc the copy would be done by the byte stuffing function */
        if (CRU_BUF_Copy_FromBufChain ((tCRU_BUF_CHAIN_HEADER *) (VOID *) pMsg, 
                                        (UINT1 *)pSlotInfoMsg, 0, 
                                         (sizeof (tMbsmProtoMsg))) == CRU_FAILURE)
        {
            CFA_DBG (CFA_TRC_ALL_TRACK | CFA_TRC_ERROR, CFA_GDD,
                     "Error in CfaHandleLinkStatusUpdateMsgFromMbsm - "
                     "Copy_FromBufChain failed.\n");
            MemReleaseMemBlock (gCfaMbsmProtoMsgPoolId,
                                (UINT1 *) (pSlotInfoMsg));
            return CFA_FAILURE;
        }
    }


    pMbsmPortInfo = &(pSlotInfoMsg->MbsmPortInfo);
    if (!pMbsmPortInfo)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandleCardUpdateFromMbsm - NULL pMbsmPortInfo.\n");
        if (u1IsBufLinear == FALSE)
        {
            MemReleaseMemBlock (gCfaMbsmProtoMsgPoolId,
                              (UINT1 *) (pSlotInfoMsg));
        }
        return (CFA_FAILURE);
    }

    /* Update the link status of all the interfaces in those units. */
    u4IfIndex = pMbsmPortInfo->u4StartIfIndex;
    CFA_CDB_SCAN_WITH_LOCK (u4IfIndex,
                            (pMbsmPortInfo->u4PortCount +
                             pMbsmPortInfo->u4StartIfIndex - 1), CFA_ALL_IFTYPE)
    {
        /* Flexible ifIndex changes 
         */
        if (u4IfIndex > (pMbsmPortInfo->u4PortCount +
                         pMbsmPortInfo->u4StartIfIndex - 1))
        {
            break;
        }

        OSIX_BITLIST_IS_BIT_SET (pMbsmPortInfo->PortList,
                                 u4IfIndex, sizeof (tPortList),
                                 u1IsSetInPortList);
        OSIX_BITLIST_IS_BIT_SET (pMbsmPortInfo->PortListStatus,
                                 u4IfIndex, sizeof (tPortList),
                                 u1IsSetInPortListStatus);
        /*Handling Port Insert event */
        if ((u1IsSetInPortList == OSIX_TRUE)
            && (u1IsSetInPortListStatus == OSIX_FALSE))
        {
            if (CfaGetIfOperStatus (u4IfIndex, &u1OldOperStatus) == CFA_SUCCESS)
            {
                /* Incase of ISSU MM , dont fetch the Oper Status from the HW,
                 * Sync Oper Status from CFA CDB Table 
                 * Audit will be done at the end of ISSU MM */
                if (IssuGetMaintModeOperation() == OSIX_TRUE)
                {
                    u1LinkStatus = u1OldOperStatus;
                }
                else
                {
                    u1LinkStatus = CfaGddGetLinkStatus (u4IfIndex);
                }

                if ((CFA_IF_ADMIN (u4IfIndex) == CFA_IF_UP)
                    && (u1LinkStatus == CFA_IF_UP))
                {
                    u1OperStatus = CFA_IF_UP;
                }
                else
                {
                    u1OperStatus = CFA_IF_DOWN;
                }

                if (u1OperStatus != u1OldOperStatus)
                {
                    CfaIfmHandleInterfaceStatusChange (u4IfIndex,
                                                       CFA_IF_ADMIN
                                                       (u4IfIndex),
                                                       u1OperStatus,
                                                       CFA_IF_LINK_STATUS_CHANGE);
                }
            }
        }

    }
    protoAckMsg.i4RetStatus = i4RetStatus;
    protoAckMsg.i4SlotId = pSlotInfoMsg->MbsmSlotInfo.i4SlotId;
    protoAckMsg.i4ProtoCookie = pSlotInfoMsg->i4ProtoCookie;

    if (i4RetStatus == MBSM_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         " CfaHandleLinkStatusUpdateMsgFromMbsm handled a link status update message from MBSM" 
                            " and returns success \n");
    }

    MbsmSendAckFromProto (&protoAckMsg);

    if (u1IsBufLinear == FALSE)
    {
        MemReleaseMemBlock (gCfaMbsmProtoMsgPoolId,
			    (UINT1 *) (pSlotInfoMsg));
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaHandlePreConfigDelMsgFromMbsm
 *
 *    Description          : This function is called by CFA on receiving a    
 *                           Pre Configuration delete from MBSM.
 *
 *    Input(s)             : pMsg - Pointer to the message to be posted to  
 *                           CFA.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaHandlePreConfigDelMsgFromMbsm (UINT1 *pMsg)
{
    tMbsmCfaHwInfoMsg  *pSlotInfoMsg;
    UINT1               u1IsBufLinear = TRUE;

    pSlotInfoMsg =
        (tMbsmCfaHwInfoMsg *)
        CRU_BUF_Get_DataPtr_IfLinear ((tCRU_BUF_CHAIN_HEADER *) (VOID *) pMsg,
                                      0, (sizeof (tMbsmCfaHwInfoMsg)));
    if (pSlotInfoMsg == NULL)
    {
        if ((pSlotInfoMsg = MemAllocMemBlk (gCfaMbsmSlotMsgPoolId)) == NULL)
        {

            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaHandlePreConfigDelMsgFromMbsm - No Memory -"
                     "FAILURE.\n");
            return (CFA_FAILURE);
        }

        u1IsBufLinear = FALSE;

        /* for aysnc the copy would be done by the byte stuffing function */
        if (CRU_BUF_Copy_FromBufChain ((tCRU_BUF_CHAIN_HEADER *) (VOID *) pMsg, 
                                        (UINT1 *) pSlotInfoMsg, 0, 
                                        (sizeof (tMbsmCfaHwInfoMsg))) == CRU_FAILURE)
        {
            CFA_DBG (CFA_TRC_ALL_TRACK | CFA_TRC_ERROR, CFA_GDD,
                     "Error in CfaHandlePreConfigDelMsgFromMbsm - "
                     "Copy_FromBufChain failed.\n");
            MemReleaseMemBlock (gCfaMbsmSlotMsgPoolId,
                                (UINT1 *) (pSlotInfoMsg));
            return CFA_FAILURE;
        }
    }

    CfaHandlePreConfigDelFromMbsm (pSlotInfoMsg);

    if (u1IsBufLinear == FALSE)
    {
        MemReleaseMemBlock (gCfaMbsmSlotMsgPoolId, (UINT1 *) (pSlotInfoMsg));
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaMbsmProtoMsgToCfa
 *
 *    Description          : This function is called by MBSM to post a message
 *                           to CFA.
 *
 *    Input(s)             : pMsg - Pointer to the message to be posted to  
 *                           CFA.
 *                           u1Cmd - MsgType to differentiate between packets
 *                           from different modules and events.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE.
 *
 *****************************************************************************/
INT4
CfaMbsmProtoMsgToCfa (tMbsmProtoMsg * pMsg, INT4 u4Cmd)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tCfaModuleData     *pParms = NULL;

    if (CFA_INITIALISED != TRUE)
    {
        return MBSM_FAILURE;
    }
    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMbsmProtoMsg) +
                                              sizeof (tCfaModuleData),
                                              0)) == NULL)
        return MBSM_FAILURE;

    /* copy data from linear buffer into CRU buffer */
    if (CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pMsg, 0,
                                   sizeof (tMbsmProtoMsg)) == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Error in CfaMbsmProtoMsgToCfa - "
                 "unable to copy to CRU Buf- FAILURE.\n");

        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return (MBSM_FAILURE);
    }
    pParms = GET_MODULE_DATA_PTR (pBuf);
    pParms->u1Command = u4Cmd;

    if (OsixQueSend (CFA_PACKET_QID, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaMbsmMsgToCfa - " "Pkt Enqueue to CFA FAILED.\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return (MBSM_FAILURE);
    }

/* send an explicit event to the task */
    if (OsixEvtSend (CFA_TASK_ID, CFA_PACKET_ARRIVAL_EVENT) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaMbsmmsgToCfa - event send to CFA FAIL.\n");
/* dont return failure as buffer has already been enqueued */
    }

    return MBSM_SUCCESS;
}

/*****************************************************************************
 *    Function Name        : CfaNotifyBootComplete 
 *    Description          : This function is called by CFA after completing 
 *                           the boot operation. It sends BOOT_COMPLETE event
 *                           to MSR task to initiate restore operation.
 *    Input(s)             : None. 
 *    Output(s)            : None.
 *    Returns              : CFA_SUCCESS or CFA_FAILURE.
 *****************************************************************************/
VOID
CfaNotifyBootComplete (VOID)
{
#ifdef MSR_WANTED
    if (MsrSendEventToMsrTask (CFA_BOOT_COMPLETE_EVENT) == MSR_FAILURE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaNotifyBootComplete - event send to MSR Failed.\n");
    }
#endif

#ifdef MBSM_WANTED
    MbsmSetBootFlag (MBSM_TRUE);
#endif

    return;
}

/****************************************************************************
 *
 *    Function Name        : CfaMbsmHandleInterfaceStatusUpdate
 *
 *    Description        :   This function validates the ifIndex and calls 
 *                           CfaIfmHandleInterfaceStatusUpdate to upate the 
 *                           interface status.
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaMbsmHandleInterfaceStatusUpdate (UINT4 u4IfIndex, UINT1 u1AdminStatus,
                                    UINT1 u1OperStatus)
{
    UINT4               u4IfSpeed;
    UINT4               u4IfHighSpeed;
    UINT1               u1TmpOperStatus;
    INT4                i4Index = 0;

    /* check if the IfIndex is valid */
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                  "Error in CfaMbsmHandleInterfaceStatusUpdate -"
                  "invalid interface %d.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    if (!CFA_IF_ENTRY_USED (u4IfIndex))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                  "Error in CfaMbsmHandleInterfaceStatusUpdate -"
                  "unused interface %d.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
              "In CfaMbsmHandleInterfaceStatusUpdate -"
              " processing update for interface %d.\n", u4IfIndex);

    /* If the Oper Status is UP, then update the Speed
     * from target */
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* No need to get speed for stack ports
         * all stack ports are higig ports */
        if ((i4Index <= CFA_STACK_PORT_START_INDEX) &&
            (i4Index >= (MBSM_MAX_PORTS_PER_SLOT + ISS_MAX_STK_PORTS)))
        {
            if (u1OperStatus == CFA_IF_UP)
            {
                CfaDetermineSpeed (u4IfIndex, &u4IfSpeed, &u4IfHighSpeed);
            }
        }
    }
    else
    {
        if (u1OperStatus == CFA_IF_UP)
        {
            CfaDetermineSpeed (u4IfIndex, &u4IfSpeed, &u4IfHighSpeed);
        }
    }
    CfaGetIfOperStatus (u4IfIndex, &u1TmpOperStatus);

    if (u1TmpOperStatus != u1OperStatus)
    {
        return (CfaIfmHandleInterfaceStatusChange (u4IfIndex, u1AdminStatus,
                                                   u1OperStatus,
                                                   CFA_IF_LINK_STATUS_CHANGE));
    }
    return CFA_SUCCESS;
}

/******************************************************************************
 * Function       : CfaMbsmCreateIvrInterface
 *
 * Description    : This function will update the h/w for the L3_IPVLAN interfaces
 *                  based on the Status of the IVR interface.
 *
 * Input(s)       : u4IfIndex - Interface index of the L3_IPVLAN interface.
 *                  u1Status  - CFA_IF_CREATE/CFA_IF_DELETE.
 *
 * Output(s)      : None.
 *
 * Returns        : CFA_SUCCESS/FAILURE
 *
 ******************************************************************************/

PUBLIC INT4
CfaMbsmCreateIvrInterface (tMbsmProtoMsg * pProtMsg)
{
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tCfaIfInfo          IfInfo;
    tIpConfigInfo       IpIntfInfo;
    UINT4               u4IpAddress;
    UINT4               u4SubnetMask;
    UINT4               u4Index;
    UINT4               u4CurrIndex;
    UINT4               u4ContextId;
    INT4                i4RetStatus = MBSM_SUCCESS;
    UINT4               u4StartIndex = CFA_MIN_LOOPBACK_IF_INDEX;
    UINT4               u4EndIfIndex = CFA_MAX_LOOPBACK_IF_INDEX;

    pSlotInfo = &pProtMsg->MbsmSlotInfo;

    if (CfaCdbGetFirstIvrIfInfo (&u4Index, &IfInfo) == CFA_SUCCESS)
    {
        do
        {
            u4CurrIndex = u4Index;

            /* No need to update the H/W for MGMT port eth0,
               loop back interface lo and linux vlan interface */

            if ((CfaIsMgmtPort (u4CurrIndex) == FALSE) &&
                (CfaIsLoopBackIntf (u4CurrIndex) == FALSE) &&
                (CfaIsLinuxVlanIntf (u4CurrIndex) == FALSE))
            {
                if (CfaIpIfGetIfInfo (u4CurrIndex, &IpIntfInfo) == CFA_SUCCESS)
                {
                    u4IpAddress = IpIntfInfo.u4Addr;
                    u4SubnetMask = IpIntfInfo.u4NetMask;
                    if (VcmGetContextIdFromCfaIfIndex
                        (u4CurrIndex, &u4ContextId) != VCM_FAILURE)
                    {
                        if ((CfaFsNpMbsmIpv4CreateIpInterface
                             (u4ContextId, u4CurrIndex, u4IpAddress,
                              u4SubnetMask, IfInfo.u2VlanId, IfInfo.au1MacAddr,
                              pSlotInfo)) == FNP_FAILURE)

                        {
                            i4RetStatus = MBSM_FAILURE;
                        }
                    }
                    else
                    {
                        i4RetStatus = MBSM_FAILURE;
                    }
                }
            }
        }
        while ((i4RetStatus == MBSM_SUCCESS) &&
               (CfaCdbGetNextIvrIfInfo (u4CurrIndex, &u4Index, &IfInfo)
                == CFA_SUCCESS));
    }
    u4Index = u4StartIndex;
    CFA_CDB_SCAN (u4Index, u4EndIfIndex, CFA_LOOPBACK)
    {
        if (CfaIpIfGetIfInfo (u4Index, &IpIntfInfo) == CFA_SUCCESS)
        {
            u4IpAddress = IpIntfInfo.u4Addr;
            u4SubnetMask = IpIntfInfo.u4NetMask;
            if (VcmGetContextIdFromCfaIfIndex
                (u4Index, &u4ContextId) != VCM_FAILURE)
            {
                if ((CfaFsNpMbsmIpv4CreateIpInterface
                     (u4ContextId, u4Index, u4IpAddress,
                      u4SubnetMask, IfInfo.u2VlanId, IfInfo.au1MacAddr,
                      pSlotInfo)) == FNP_FAILURE)

                {
                    i4RetStatus = MBSM_FAILURE;
                }
            }
            else
            {
                i4RetStatus = MBSM_FAILURE;
            }
        }
    }

    return i4RetStatus;
}

/******************************************************************************
 * Function       : CfaMbsmUpdateMtuToHw 
 *
 * Description    : This function will update the h/w with the Port MTU 
 *
 * Input(s)       : u4IfIndex - Interface index for the port. 
 *
 * Output(s)      : None.
 *
 * Returns        : CFA_SUCCESS/FAILURE
 *
 ******************************************************************************/
PUBLIC INT4
CfaMbsmUpdateMtuToHw (UINT4 u4IfIndex)
{
    UINT4               u4IfMtu = CFA_ENET_MTU;

    if (CfaValidateIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        /* Not a valid IfIndex */
        return CFA_FAILURE;
    }

    CfaGetIfMtu (u4IfIndex, &u4IfMtu);

    /* Program the Hardware if and only if it is not the default value(1500).
     * This is because, by default this value (1500) would have been set into 
     * the hardware. So, if CFA has some other value, write the same into the 
     * H/W */
    if (u4IfMtu != CFA_ENET_MTU)
    {
        if (CfaFsCfaHwSetMtu ((UINT2) u4IfIndex, u4IfMtu) == FNP_FAILURE)
        {
            return CFA_FAILURE;
        }
    }
    return CFA_SUCCESS;
}

/******************************************************************************
 * Function       : CfaMbsmInterfaceMacAddr
 *
 * Description    : This function will update the h/w for MAC address
 *                      on an interface.
 *
 * Input(s)       :
 *
 *
 * Output(s)      : None.
 *
 * Returns        : MBSM_SUCCESS/FAILURE
 *
 ******************************************************************************/
PUBLIC INT4
CfaMbsmInterfaceMacAddr (tMbsmProtoMsg * pProtMsg)
{
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tMbsmPortInfo      *pPortInfo = NULL;
    tMacAddr            zeroAddr;
    UINT4               u4PortCount = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4MacLogic = 0;
    UINT1               u1IsSetInPortList = OSIX_FALSE;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;
    UINT1               au1HwAddr[CFA_MAX_MEDIA_ADDR_LEN];
    UINT1               au1CfaAddr[CFA_MAX_MEDIA_ADDR_LEN];

    MEMSET (zeroAddr, 0, CFA_ENET_ADDR_LEN);

    MEMSET (au1CfaAddr, 0, CFA_MAX_MEDIA_ADDR_LEN);
    MEMSET (au1HwAddr, 0, CFA_MAX_MEDIA_ADDR_LEN);

    pSlotInfo = &pProtMsg->MbsmSlotInfo;
    pPortInfo = &pProtMsg->MbsmPortInfo;

    u4PortCount = MBSM_PORT_INFO_PORTCOUNT (pPortInfo);
    u4IfIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo);

    u4MacLogic = IssCustGetMacAddrLogic ();

    while (u4PortCount != 0)
    {

        if (CfaValidateIfIndex (u4IfIndex) == CFA_FAILURE)
        {
            u4PortCount--;
            u4IfIndex++;
            continue;
        }
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST (pPortInfo), u4IfIndex,
                                 sizeof (tPortList), u1IsSetInPortList);
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS (pPortInfo),
                                 u4IfIndex, sizeof (tPortList),
                                 u1IsSetInPortListStatus);
        /*checks  the newly inserted ports */
        if ((u1IsSetInPortList == OSIX_TRUE)
            && (OSIX_FALSE == u1IsSetInPortListStatus))
        {
            /* Ports would have got created and admin status might have got 
             * set to up before card inserted. But during card insertion we 
             * need to program the mac address. So don't check for admin 
             * status here. */

            CfaGetIfHwAddr (u4IfIndex, au1CfaAddr);

            if (CfaGddGetOsHwAddr (u4IfIndex, au1HwAddr) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                          "Error in CfaIfmInitEnetIfEntry -"
                          " unable to get the socket or MAC address for interface %d\n",
                          u4IfIndex);

                u4PortCount--;
                u4IfIndex++;
                continue;
            }

            /* Check the Mac address from CFA */
            if (MEMCMP (au1CfaAddr, zeroAddr, CFA_ENET_ADDR_LEN) != 0)
            {

                /* check whether the Mac address from CFA is same as 
                   Mac address from hardware */
                if (MEMCMP (au1CfaAddr, au1HwAddr, CFA_ENET_ADDR_LEN) != 0)
                {
                    if (u4MacLogic == ISS_CUST_CTRL_PLANE_MAC)
                    {
                        /* Update the Mac Address from CFA in hardware */
                        if (CfaFsCfaMbsmSetMacAddr
                            ((UINT2) u4IfIndex, au1CfaAddr,
                             pSlotInfo) == FNP_FAILURE)
                        {
                            u4PortCount--;
                            u4IfIndex++;
                            continue;
                        }
                    }
                    else
                    {
                        /* Update the Mac Address from HW in CFA */
                        CfaSetIfHwAddr (u4IfIndex, au1HwAddr,
                                        CFA_ENET_ADDR_LEN);

                        /* del the address in the rcv addr table */
                        CfaIfmDeleteRcvAddrEntry (u4IfIndex, au1CfaAddr);

                        /* get the HW address from the driver - if failure then dont show it */
                        if (CfaIfmAddRcvAddrEntry
                            (u4IfIndex, au1HwAddr,
                             CFA_RCVADDR_NVOL) != CFA_SUCCESS)
                        {
                            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                      "Error in CfaIfmInitEnetIfEntry - "
                                      "RcvAddrTab UCAST add fail on interface %d\n",
                                      u4IfIndex);
                        }
                    }
                }
            }
            else
            {
                /* No address is configured for this port. So update the 
                 * Macaddress from hardware in CFA database */
                CfaSetIfHwAddr (u4IfIndex, au1HwAddr, CFA_ENET_ADDR_LEN);
            }
        }
        u4PortCount--;
        u4IfIndex++;
    }

    return MBSM_SUCCESS;
}

/******************************************************************************
 * Function       : CfaMbsmCreateCustomParam 
 *
 * Description    : This function will update the h/w for the opaque attributes
 *                   and TLV configured on an interface.
 *
 * Input(s)       :  
 *                  
 *
 * Output(s)      : None.
 *
 * Returns        : CFA_SUCCESS/FAILURE
 *
 ******************************************************************************/

PUBLIC INT4
CfaMbsmCreateCustomParam (tMbsmProtoMsg * pProtMsg)
{
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tMbsmPortInfo      *pPortInfo = NULL;
    tHwCustIfParamVal   HwCustIfParamVal;
    tTMO_SLL           *pTlvList = NULL;
    tIfTlvInfoStruct   *pIfTlvNode = NULL;
    UINT4               u4OpqAttrID = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4PortCount = 0;
    UINT1               u1IsSetInPortList = OSIX_FALSE;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;

    pSlotInfo = &pProtMsg->MbsmSlotInfo;
    pPortInfo = &pProtMsg->MbsmPortInfo;

    u4PortCount = MBSM_PORT_INFO_PORTCOUNT (pPortInfo);
    u4IfIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo);

    while (u4PortCount != 0)
    {
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST (pPortInfo), u4IfIndex,
                                 sizeof (tPortList), u1IsSetInPortList);
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS (pPortInfo),
                                 u4IfIndex, sizeof (tPortList),
                                 u1IsSetInPortListStatus);

        /* Only in insertion case need to program */
        if ((u1IsSetInPortList == OSIX_TRUE) && (OSIX_FALSE == u1IsSetInPortListStatus))    /* Port Insert */
        {
            if (CfaIfIsBridgedInterface (u4IfIndex) == CFA_TRUE)
            {
                for (u4OpqAttrID = 1; u4OpqAttrID <= CFA_MAX_OPAQUE_ATTRS;
                     u4OpqAttrID++)
                {
                    if ((CFA_IF_OPQ_CONFIGURED (u4IfIndex, u4OpqAttrID))
                        && (CFA_IF_OPQ_ATTR_RS (u4IfIndex, u4OpqAttrID) ==
                            CFA_RS_ACTIVE))
                    {
                        HwCustIfParamVal.OpaqueAttrbs.u4AttribID = u4OpqAttrID;
                        HwCustIfParamVal.OpaqueAttrbs.u4Attribute =
                            CFA_IF_OPQ_ATTR (u4IfIndex, u4OpqAttrID);

                        if (CfaFsHwMbsmSetCustIfParams
                            (u4IfIndex, NP_CUST_IF_PRM_TYPE_OPQ_ATTR,
                             HwCustIfParamVal, NP_ENTRY_ADD,
                             pSlotInfo) == FNP_FAILURE)
                        {
                            continue;
                        }
                    }
                }

                pTlvList = (tTMO_SLL *) & CFA_IF_TLV_INFO ((UINT2) u4IfIndex);

                TMO_SLL_Scan (pTlvList, pIfTlvNode, tIfTlvInfoStruct *)
                {
                    HwCustIfParamVal.TLV.u4Type = pIfTlvNode->u4TlvType;
                    HwCustIfParamVal.TLV.u4Length = pIfTlvNode->u4TlvLength;
                    HwCustIfParamVal.TLV.pu1Value = pIfTlvNode->pu1TlvVlaue;

                    if (CfaFsHwMbsmSetCustIfParams
                        (u4IfIndex, NP_CUST_IF_PRM_TYPE_TLV, HwCustIfParamVal,
                         NP_ENTRY_ADD, pSlotInfo) == FNP_FAILURE)
                    {
                        continue;
                    }
                }
            }
        }
        u4PortCount--;
        u4IfIndex++;
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaMbsmInitDefEtherType
 *
 *    Description          : This function is called by RM     
 *                           to set the default Interface Type to all the 
 *                           MBSM slots.
 *
 *    Input(s)             : i4SlotNum - Number number 
 *
 *    Output(s)            : None.
 *
 *    Returns              : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaMbsmInitDefEtherType (INT4 i4SlotNum)
{
    tMbsmSlotInfo       MbsmSlotInfo;
    tMbsmPortInfo       MbsmPortInfo;
    UINT4               u4StartIfIndex = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4PortIndex = 0;
    INT4                i4CardIndex = 0;
    UINT4               u4PortNum = 0;
    UINT4               u4PortCount = 0;
    UINT1               u1SpeedType = 0;

    if (MbsmGetSlotInfo (i4SlotNum, &MbsmSlotInfo) == MBSM_FAILURE)
    {
        return CFA_FAILURE;
    }
    if (MbsmGetPortInfo (i4SlotNum, &MbsmPortInfo) == MBSM_FAILURE)
    {
        return CFA_FAILURE;
    }

    CFA_LOCK ();
    if (CFA_IF_ENTRY_USED (MbsmSlotInfo.u4StartIfIndex))
    {
        /* Interfaces already created, so just update Oper Status */
    }
    else
    {
        /* case of Pre-configuration, so interfaces has to be created ,
         * and then update oper status
         */
        CfaCreateInterfacesForSlot (MbsmSlotInfo.i4SlotId, &MbsmPortInfo);
    }

    /* update Oper Status */
    if (CfaUpdateOperStatusNotPresent (&MbsmPortInfo) == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaMbsmInitDefEtherType" "- NULL message.\n");
        CFA_UNLOCK ();
        return (CFA_FAILURE);
    }
    CFA_UNLOCK ();
    u4StartIfIndex = MbsmPortInfo.u4StartIfIndex;
    u4PortCount = MbsmPortInfo.u4PortCount;

    for (u4PortIndex = 1; u4PortIndex <= u4PortCount; u4PortIndex++)
    {
        u4IfIndex = u4StartIfIndex + (u4PortIndex - 1);
        /* EtherType is set here based on the port speed of the port
         * in the Card. This is only the Preconfiguration case.
         * When the actual card attached, this value is updated 
         * accordingly.
         */

        i4CardIndex = (MbsmSlotInfo.i4CardType);
        u4PortNum = 1;            /* To get the Port type of the card
                                 * it is sufficient to check the 
                                 * portSpeed Type for at least one 
                                 * port. So we are cheacking for the 
                                 * first port only.
                                 */

        MbsmGetPortSpeedTypeFromCardIndex (i4CardIndex, u4PortNum,
                                           &u1SpeedType);
        switch (u1SpeedType)
        {
            case MBSM_SPEED_FAST_ETHER:
                u1SpeedType = CFA_FA_ENET;
                break;
            case MBSM_SPEED_ETHER:
            case MBSM_SPEED_GIG_ETHER:
                u1SpeedType = CFA_GI_ENET;
                break;
            case MBSM_SPEED_10GIG_ETHER:
                u1SpeedType = CFA_XE_ENET;
                break;
            case MBSM_SPEED_40GIG_ETHER:
                u1SpeedType = CFA_XL_ENET;
                break;
            case MBSM_SPEED_56GIG_ETHER:
                u1SpeedType = CFA_LVI_ENET;
                break;
            case MBSM_SPEED_NULL:
            case MBSM_SPEED_INVALID:
            default:
                u1SpeedType = CFA_ENET_UNKNOWN;
        }
        CfaSetEthernetType (u4IfIndex, u1SpeedType);
    }
    return CFA_SUCCESS;
}

#ifdef MPLS_WANTED
/******************************************************************************
 * Function       : CfaMbsmCreateMplsInterface
 *
 * Description    : This function will update the h/w for the L3_IPVLAN interfaces
 *                  based on the Status of the IVR interface.
 *
 * Input(s)       : u4IfIndex - Interface index of the L3_IPVLAN interface.
 *                  u1Status  - CFA_IF_CREATE/CFA_IF_DELETE.
 *
 * Output(s)      : None.
 *
 * Returns        : CFA_SUCCESS/FAILURE
 *
 ******************************************************************************/

PUBLIC INT4
CfaMbsmCreateMplsInterface (tMbsmProtoMsg * pProtMsg)
{
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tCfaIfInfo          IfInfo;
    UINT4               u4L3VlanIf = 0;
    UINT4               u4Index;
    UINT4               u4CurrIndex;
    UINT2               u2VlanId = 0;
    UINT1               au1MacAddress[MAC_ADDR_LEN];
    INT4                i4RetStatus = MBSM_SUCCESS;
    tMplsNpWrNpMplsCreateMplsInterface MplsCreateMplsInt;
    tMplsHwMplsIntInfo   MplsHwMplsIntInfo;
    MEMSET (&MplsCreateMplsInt, 0, sizeof(tMplsNpWrNpMplsCreateMplsInterface));
    MEMSET (&MplsHwMplsIntInfo,0,sizeof(tMplsHwMplsIntInfo));
 
    pSlotInfo = &pProtMsg->MbsmSlotInfo;

    if (CfaCdbGetFirstMplsIfInfo (&u4Index, &IfInfo) != CFA_SUCCESS)
    {
        return i4RetStatus;
    }
    do
    {
        u4CurrIndex = u4Index;

        if (u4CurrIndex > CFA_MAX_MPLS_IF_INDEX)
        {
            if ((CfaUtilGetIfIndexFromMplsTnlIf (u4CurrIndex,
                                                 &u4L3VlanIf,
                                                 FALSE) == CFA_SUCCESS)
                && (u4L3VlanIf != 0))
            {
                CfaGetIfIvrVlanId (u4L3VlanIf, &u2VlanId);
                CfaGetIfHwAddr (u4L3VlanIf, au1MacAddress);
                 MplsHwMplsIntInfo.u4CfaIfIndex = u4CurrIndex;
                 MplsHwMplsIntInfo.u2VlanId = u2VlanId;
                 MplsHwMplsIntInfo.u4L3Intf = u4L3VlanIf;
                 MEMCPY(MplsHwMplsIntInfo.au1MacAddress,au1MacAddress,MAC_ADDR_LEN);

                 MplsCreateMplsInt.pMplsHwMplsIntInfo=&MplsHwMplsIntInfo;


                if ((MplsNpMplsMbsmCreateMplsInterface (MplsCreateMplsInt.pMplsHwMplsIntInfo,
                                                        pSlotInfo )) ==
                    FNP_FAILURE)
                {
                    i4RetStatus = MBSM_FAILURE;
                }
            }
        }
    }
    while ((i4RetStatus == MBSM_SUCCESS) &&
           (CfaCdbGetNextMplsIfInfo (u4CurrIndex, &u4Index, &IfInfo)
            == CFA_SUCCESS));
    return i4RetStatus;
}
#endif /* MPLS_WANTED */

/******************************************************************************
 * Function           : CfaGetStackIvrOperStatus
 * Input(s)           : -
 * Output(s)          : pu1OperStatus - Opertational Sttaus of the Stack 
 *                                      Vlan Interface
 * Returns            : CFA_SUCCESS/CFA_FAILURE
 * Action             : Other modules should call this routine to
 *                      get the operational status of the stack Vlan Interface.
 ******************************************************************************/
INT4
CfaGetStackIvrOperStatus (UINT1 *pu1OperStatus)
{
    UINT2               u2Index;
    UINT1               u1PortOperStatus;

    u2Index = CFA_STACK_PORT_START_INDEX;
    CFA_CDB_SCAN_WITH_LOCK (u2Index,
                            (MBSM_MAX_PORTS_PER_SLOT + ISS_MAX_STK_PORTS),
                            CFA_ALL_IFTYPE)
    {
        /* Flexible ifIndex changes 
         */
        if (u2Index > (MBSM_MAX_PORTS_PER_SLOT + ISS_MAX_STK_PORTS))
        {
            break;
        }

        if ((CfaGetIfOperStatus (u2Index, &u1PortOperStatus) != CFA_SUCCESS))
        {
            return CFA_FAILURE;
        }
        if (u1PortOperStatus == CFA_IF_UP)
        {
            *pu1OperStatus = CFA_IF_UP;
            return CFA_SUCCESS;
        }
    }

    *pu1OperStatus = CFA_IF_DOWN;
    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaUpdatePortMacAddress
 * Input(s)           : u4Switchid - Slot number
 *                      pu1SwitchHwAddr - switch mac address
 * Output(s)          : None
 * Returns            : CFA_SUCCESS/CFA_FAILURE
 * Action             : To update slave port mac address  
 ******************************************************************************/
INT4
CfaUpdatePortMacAddress (UINT4 u4Switchid, UINT1 *pu1SwitchHwAddr)
{
    INT4                i4Retval = 0;

    CFA_LOCK ();
    i4Retval = CfaUpdatePortMacAddressForSlot (u4Switchid, pu1SwitchHwAddr);
    CFA_UNLOCK ();

    return (i4Retval);
}

/******************************************************************************

 * Function           : CfaUpdatePortMacAddressForSlot
 * Input(s)           : u4Switchid - Slot number
 *                      pu1SwitchHwAddr - switch mac address
 * Output(s)          : None
 * Returns            : CFA_SUCCESS/CFA_FAILURE
 * Action             : To update slave port mac address as continuation of 
 *                       its own switch-base mac addres 
 ******************************************************************************/
INT4
CfaUpdatePortMacAddressForSlot (UINT4 u4Switchid, UINT1 *pu1SwitchHwAddr)
{
    UINT4               u4PortIndex = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4StartIfIndex = 0;
    UINT1               au1HwAddr[CFA_MAX_MEDIA_ADDR_LEN];

    MbsmGetStartIfIndex ((INT4) u4Switchid, &u4StartIfIndex);
    for (u4PortIndex = 1; u4PortIndex <= (UINT4) MBSM_MAX_PORTS_PER_SLOT;
         u4PortIndex++)
    {
        u4IfIndex = u4StartIfIndex + (u4PortIndex - 1);
        if (!(CFA_IF_ENTRY_USED (u4IfIndex)))
        {
            /* Port Mac address should be updated
             * only if that interface is already created */
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      " Error in CfaUpdatePortMacAddressForSlot "
                      "IfIndex %ld not exist\r\n", u4IfIndex);
            return CFA_FAILURE;
        }
    }

    for (u4PortIndex = 1; u4PortIndex <= (UINT4) MBSM_MAX_PORTS_PER_SLOT;
         u4PortIndex++)
    {
        u4IfIndex = u4StartIfIndex + (u4PortIndex - 1);
        TMO_SLL_Init (&CFA_IF_RCVADDRTAB ((UINT2) u4IfIndex));

        /* gaCfaNpIndexMap should be updated for each port */
        CfaCfaMbsmUpdatePortMacAddress ((UINT1) u4Switchid, u4PortIndex,
                                        (UINT2) u4IfIndex, pu1SwitchHwAddr);
        if (CfaGddGetOsHwAddr ((UINT2) u4IfIndex, au1HwAddr) == CFA_SUCCESS)
        {
            /* Receive address table should be updated according
             * peer's switch base mac address */
            if (CfaIfmAddRcvAddrEntry
                ((UINT2) u4IfIndex, au1HwAddr, CFA_RCVADDR_NVOL) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                          "Error in  CfaUpdatePortMacAddressForSlot - "
                          " add fail on interface %d\n", u4IfIndex);
                return CFA_FAILURE;
            }
        }
    }
    return CFA_SUCCESS;
}

/******************************************************************************
 *
 * Function           : CfaMbsmDelSispInterfacesForSlot
 *
 * Input(s)           : pPortInfo - Inserted port information
 *
 * Output(s)          : This function will find the set of SISP enabled 
 *                      physical ports present in the card. For these ports, it
 *                      will find the SISP logical interfaces running over and 
 *                      will destroy these interfaces in the higher layers.
 *
 * Returns            : CFA_SUCCESS/CFA_FAILURE
 *
 ******************************************************************************/
INT4
CfaMbsmDelSispInterfacesForSlot (tMbsmPortInfo * pPortInfo)
{
    UINT4               au4SispPorts[SYS_DEF_MAX_NUM_CONTEXTS];
    UINT4               u4PortNum = 0;
    UINT4               u4TempPortCnt = 0;
    UINT4               u4PortCount = 0;
    UINT4               u4PortCnt = 0;
    UINT4               u4StartIfIndex = 0;
    UINT4               u4Index = 0;
    UINT4               u4IfIndex = 0;
    UINT1               u1IfActive = 0;
    tCfaIfInfo         *pCfaIfInfo = NULL;

    MEMSET (au4SispPorts, 0, sizeof (au4SispPorts));

    if (pPortInfo == NULL)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaMbsmDelSispInterfacesForSlot - NULL message.\n");
        return (CFA_FAILURE);
    }

    u4StartIfIndex = pPortInfo->u4StartIfIndex;

    u4PortCount = pPortInfo->u4PortCount;

    for (u4Index = 0; u4Index < u4PortCount; u4Index++)
    {
        u4IfIndex = u4StartIfIndex + u4Index;

        CFA_DS_LOCK ();

        pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

        CFA_DS_UNLOCK ();

        if (pCfaIfInfo != NULL)
        {
            if (CfaVcmSispGetSispPortsOfPhysicalPort (u4IfIndex, VCM_FALSE,
                                                      (VOID *) au4SispPorts,
                                                      &u4PortCnt) ==
                VCM_FAILURE)
            {
                continue;
            }

            for (; u4TempPortCnt < u4PortCnt; u4TempPortCnt++)
            {
                u4PortNum = au4SispPorts[u4TempPortCnt];

                if (u4PortNum == 0)
                {
                    continue;
                }

                CFA_DS_LOCK ();
                u1IfActive = CFA_CDB_IS_INTF_ACTIVE (u4PortNum);
                CFA_DS_UNLOCK ();

                if (u1IfActive != CFA_TRUE)
                {
                    /* Entry is not active. Skip the same */
                    continue;
                }

                /* Before updating in CFA, the port should be unmapped first in
                 * VCM */
                VcmUnMapPortFromContext (u4PortNum);

                CfaSetCdbRowStatus (u4PortNum, CFA_RS_NOTINSERVICE);
                CfaSetIfValidStatus (u4PortNum, CFA_FALSE);
                CfaSetIfActiveStatus (u4PortNum, CFA_FALSE);
            }
        }
    }                            /*For all phy interfaces in the attached slot */

    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : CfaMbsmCardInsertIvrMapTable                     */
/*                                                                           */
/*    Description         : This function reads the list of mapped vlans for */
/*                          the entire Vlan Interface range and updates the  */
/*                          hardware during card insertion event             */
/*                                                                           */
/*    Input(s)            : pProtMsg  - Pointer to Protocol Message Info     */
/*                                                                           */
/*    Output(s)           : NONE                                             */
/*                                                                           */
/*    Returns             : MBSM_SUCCESS or MBSM_FAILURE                     */
/*                                                                           */
/*****************************************************************************/

INT4
CfaMbsmCardInsertIvrMapTable (tMbsmProtoMsg * pProtMsg)
{
    tMbsmSlotInfo      *pSlotInfo = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4TmpIfIndex = 0;

    pSlotInfo = &pProtMsg->MbsmSlotInfo;

    while (CfaGetNextIvrIfIndex (u4IfIndex, &u4TmpIfIndex) != CFA_FAILURE)
    {
        u4IfIndex = u4TmpIfIndex;

        MEMSET (&gIpVlanMappingInfo, 0, sizeof (tNpIpVlanMappingInfo));

        /* Fetch list of secondary VLAN mapped to primary VLAN
         * IVR interface
         */
        if ((CfaIvrMapGetMappedVlans (u4IfIndex,
                                      gIpVlanMappingInfo.MappedVlanList)) > 0)
        {
            gIpVlanMappingInfo.u4IvrIfIndex = u4IfIndex;
            CfaGetIfIvrVlanId (u4IfIndex, &gIpVlanMappingInfo.IvrVlanId);
            VcmGetIfMapVcId (u4IfIndex, &gIpVlanMappingInfo.u4VrtId);
            gIpVlanMappingInfo.u1Action = CFA_ADD;

            if (CfaFsNpMbsmIpv4MapVlansToIpInterface (&gIpVlanMappingInfo,
                                                      pSlotInfo) == FNP_FAILURE)
            {
                return MBSM_FAILURE;
            }
        }
    }

    return MBSM_SUCCESS;
}
