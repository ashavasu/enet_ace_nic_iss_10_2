/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: ifmprpll.c,v 1.367 2018/01/16 10:46:30 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include  "cfainc.h"
#include  "cli.h"
#ifdef WGS_WANTED
#ifdef KERNEL_WANTED
#include "chrdev.h"
#endif
#endif
#include "ofcl.h"
#ifdef IP6_WANTED
#include "ipvx.h"
#include "ipv6.h"
#endif
#include "iss.h"
#ifdef LNXIP6_WANTED
extern INT4         Lip6RAConfig (UINT4 u4ContextId);
#endif

#ifdef MPLS_WANTED
#ifdef HVPLS_WANTED
extern UINT1        L2VpPortGetPortOwnerFromIndex (UINT4 u4IfIndex);
#endif
#endif

extern UINT4        IfIpAddr[12];
extern UINT4        IfIpSubnetMask[12];
extern UINT4        IfIpBroadcastAddr[12];
extern UINT4        IfmDebug[10];
extern UINT4        IfIpUnnumAssocIPIf[12];
extern tLnkUpSysControl gLnkUpSystemControl;

#if defined (LNXIP4_WANTED) && defined (NPAPI_WANTED)
extern VOID         CfaGddSetLnxIntfnameForPort (UINT4 u4Index,
                                                 UINT1 *au1IfName);
#endif
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIfMaxInterfaces
 Input       :  The Indices

                The Object 
                retValIfMaxInterfaces
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMaxInterfaces (INT4 *pi4RetValIfMaxInterfaces)
{
    tCfaSystemSize      CfaSizingStruct;

    GetCfaSizingParams (&CfaSizingStruct);

    *pi4RetValIfMaxInterfaces = (INT4) CfaSizingStruct.u4SystemMaxIface;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMaxPhysInterfaces
 Input       :  The Indices

                The Object 
                retValIfMaxPhysInterfaces
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMaxPhysInterfaces (INT4 *pi4RetValIfMaxPhysInterfaces)
{
    tCfaSystemSize      CfaSizingStruct;

    GetCfaSizingParams (&CfaSizingStruct);

    *pi4RetValIfMaxPhysInterfaces =
        (INT4) CfaSizingStruct.u4SystemMaxPhysicalIface;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfAvailableIndex
 Input       :  The Indices

                The Object 
                retValIfAvailableIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfAvailableIndex (INT4 *pi4RetValIfAvailableIndex)
{
    *pi4RetValIfAvailableIndex = (INT4) CFA_AVAIL_IFINDEX ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIfMaxInterfaces
 Input       :  The Indices

                The Object 
                setValIfMaxInterfaces
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMaxInterfaces (INT4 i4SetValIfMaxInterfaces)
{
    UNUSED_PARAM (i4SetValIfMaxInterfaces);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfMaxPhysInterfaces
 Input       :  The Indices

                The Object 
                setValIfMaxPhysInterfaces
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMaxPhysInterfaces (INT4 i4SetValIfMaxPhysInterfaces)
{
    UNUSED_PARAM (i4SetValIfMaxPhysInterfaces);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IfMaxInterfaces
 Input       :  The Indices

                The Object 
                testValIfMaxInterfaces
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMaxInterfaces (UINT4 *pu4ErrorCode, INT4 i4TestValIfMaxInterfaces)
{
    UNUSED_PARAM (i4TestValIfMaxInterfaces);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfMaxPhysInterfaces
 Input       :  The Indices

                The Object 
                testValIfMaxPhysInterfaces
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMaxPhysInterfaces (UINT4 *pu4ErrorCode,
                              INT4 i4TestValIfMaxPhysInterfaces)
{
    UNUSED_PARAM (i4TestValIfMaxPhysInterfaces);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IfMaxInterfaces
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfMaxInterfaces (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2IfMaxPhysInterfaces
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfMaxPhysInterfaces (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IfMainTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIfMainTable
 Input       :  The Indices
                IfMainIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIfMainTable (INT4 i4IfMainIndex)
{
    if (CfaValidateIfMainTableIndex (i4IfMainIndex) == CFA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIfMainTable
 Input       :  The Indices
                IfMainIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIfMainTable (INT4 *pi4IfMainIndex)
{
    UINT4               u4Index = 1;
#ifdef MBSM_WANTED
    UINT1               u1IfType = 0;
#endif

    /* Get the First Used Index */
    CFA_CDB_SCAN_WITH_LOCK (u4Index,
                            (BRG_MAX_PHY_PLUS_LAG_INT_PORTS +
                             CFA_MAX_NUM_OF_OF), CFA_ALL_IFTYPE)
    {
        if (CfaIsIfEntryProgrammingAllowed (u4Index) == CFA_FALSE)
        {
            CFA_DS_LOCK ();
            if (CFA_CDB_IS_INTF_VALID (u4Index) == CFA_TRUE)
            {
                CFA_DS_UNLOCK ();
                *pi4IfMainIndex = (INT4) u4Index;
                return SNMP_SUCCESS;
            }
            CFA_DS_UNLOCK ();
        }
        else
        {
#ifdef MBSM_WANTED
            if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
            {
                /* To skip the stack ports in WEB */
                CfaGetEthernetType ((UINT4) u4Index, &u1IfType);
                if (u1IfType == CFA_STACK_ENET)
                {
                    u4Index = u4Index + ISS_MAX_STK_PORTS;
                }
            }
#endif
            if (CFA_IF_ENTRY_USED (u4Index))
            {
                *pi4IfMainIndex = (INT4) u4Index;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIfMainTable
 Input       :  The Indices
                IfMainIndex
                nextIfMainIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIfMainTable (INT4 i4IfMainIndex, INT4 *pi4NextIfMainIndex)
{
    UINT4               u4IfIndex;
    UINT4               u4Counter;
#ifdef MBSM_WANTED
    UINT1               u1IfType;
#endif

    if ((i4IfMainIndex < 0) ||
        ((UINT4) i4IfMainIndex > BRG_MAX_PHY_PLUS_LAG_INT_PORTS))
    {
        return SNMP_FAILURE;
    }

    u4IfIndex = (UINT4) i4IfMainIndex;
    u4Counter = u4IfIndex + 1;

    /* Next Index should always return the next used Index */
    CFA_CDB_SCAN_WITH_LOCK (u4Counter,
                            (BRG_MAX_PHY_PLUS_LAG_INT_PORTS +
                             CFA_MAX_NUM_OF_OF), CFA_ALL_IFTYPE)
    {

        if (CfaIsIfEntryProgrammingAllowed (u4Counter) == CFA_FALSE)
        {
            CFA_DS_LOCK ();
            if (CFA_CDB_IS_INTF_VALID (u4Counter) == CFA_TRUE)
            {
                CFA_DS_UNLOCK ();
                *pi4NextIfMainIndex = (INT4) u4Counter;
                return SNMP_SUCCESS;
            }
            CFA_DS_UNLOCK ();
        }
        else
        {
#ifdef MBSM_WANTED
            if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
            {
                /* To skip the stack ports in WEB */
                CfaGetEthernetType (u4Counter, &u1IfType);
                if (u1IfType == CFA_STACK_ENET)
                {
                    u4Counter = u4Counter + ISS_MAX_STK_PORTS;
                }
            }
#endif
            if (CFA_IF_ENTRY_USED (u4Counter))
            {
                *pi4NextIfMainIndex = (INT4) u4Counter;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIfMainType
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfMainType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainType (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainType)
{
    UINT1               u1IfType;

    if (CfaGetIfType ((UINT4) i4IfMainIndex, &u1IfType) == CFA_SUCCESS)
    {
        *pi4RetValIfMainType = (INT4) u1IfType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfMainMtu
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfMainMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainMtu (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainMtu)
{
    UINT4               u4IfIndex;
    UINT4               u4IfMtu;

    u4IfIndex = (UINT4) i4IfMainIndex;

    CfaGetIfMtu (u4IfIndex, &u4IfMtu);
    *pi4RetValIfMainMtu = (INT4) u4IfMtu;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainAdminStatus
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfMainAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainAdminStatus (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainAdminStatus)
{
    UINT4               u4IfIndex;

    u4IfIndex = (UINT4) i4IfMainIndex;

    CFA_DS_LOCK ();

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        *pi4RetValIfMainAdminStatus = CFA_CDB_IF_ADMIN_STATUS (u4IfIndex);
    }
    else
    {
        *pi4RetValIfMainAdminStatus = (INT4) CFA_IF_ADMIN (u4IfIndex);
    }

    CFA_DS_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainOperStatus
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfMainOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainOperStatus (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainOperStatus)
{
    UINT1               u1OperStatus = 0;
    UINT1               u1BrgPortType = 0;
    UINT4               u4UapIfIndex = 0;
    UINT2               u2Svid = 0;

    CfaGetIfOperStatus ((UINT4) i4IfMainIndex, &u1OperStatus);
    CfaGetIfBrgPortType ((UINT4) i4IfMainIndex, &u1BrgPortType);
    if (u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        if (u1OperStatus == CFA_IF_UP)
        {
            *pi4RetValIfMainOperStatus = (INT4) CFA_IF_UP;
        }
        else                    /* OPER STATUS IS EITHER DOWN or NOT PRESENT FOR THIS S-CHANNEL */
        {
            /* get the UAP ID for this s-channel */
            VlanApiGetSChInfoFromSChIfIndex ((UINT4) i4IfMainIndex,
                                             &u4UapIfIndex, &u2Svid);
            /* Retreiving the Oper Status of the UAP */
            CfaGetIfOperStatus (u4UapIfIndex, &u1OperStatus);
            if (u1OperStatus == CFA_IF_NP)
            {
                *pi4RetValIfMainOperStatus = (INT4) CFA_IF_NP;
            }
            else
            {
                *pi4RetValIfMainOperStatus = (INT4) CFA_IF_DOWN;
            }

        }
    }
    else
    {
        *pi4RetValIfMainOperStatus = (INT4) u1OperStatus;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainEncapType
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfMainEncapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainEncapType (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainEncapType)
{
    UINT4               u4IfIndex;

    u4IfIndex = (UINT4) i4IfMainIndex;
    if ((CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
        && (CFA_IF_ENTRY_USED (u4IfIndex)))
    {
        *pi4RetValIfMainEncapType = (INT4) CFA_IF_ENCAP (u4IfIndex);
    }
    else
    {
        *pi4RetValIfMainEncapType = CFA_INVALID_TYPE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainBrgPortType
 Input       :  The Indices
                IfMainIndex
 
                The Object 
                retValIfMainBrgPortType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainBrgPortType (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainBrgPortType)
{

    UINT1               u1BrgPortType;

    if (CfaGetIfBrgPortType ((UINT4) i4IfMainIndex, &u1BrgPortType)
        == CFA_SUCCESS)
    {
        *pi4RetValIfMainBrgPortType = (INT4) u1BrgPortType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfMainSubType
 Input       :  The Indices
                IfMainIndex

                The Object
                retValIfMainSubType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainSubType (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainSubType)
{
    UINT1               u1IfSubType;

    if (CfaGetIfSubType ((UINT4) i4IfMainIndex, &u1IfSubType) == CFA_SUCCESS)
    {
        *pi4RetValIfMainSubType = (INT4) u1IfSubType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfMainNetworkType
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfMainNetworkType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainNetworkType (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainNetworkType)
{
    UINT1               u1NetworkType;

    if ((i4IfMainIndex < 0) || (i4IfMainIndex > (INT4) CFA_MAX_INTERFACES ()))
    {
        return SNMP_SUCCESS;
    }

    CfaGetIfNwType ((UINT4) i4IfMainIndex, &u1NetworkType);
    *pi4RetValIfMainNetworkType = u1NetworkType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainWanType
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfMainWanType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainWanType (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainWanType)
{
    UINT1               u1WanType;

    if ((i4IfMainIndex < 0) || (i4IfMainIndex > (INT4) CFA_MAX_INTERFACES ()))
    {
        *pi4RetValIfMainWanType = CFA_WAN_TYPE_OTHER;
        return SNMP_SUCCESS;
    }

    CfaGetIfWanType ((UINT4) i4IfMainIndex, &u1WanType);
    *pi4RetValIfMainWanType = (INT4) u1WanType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainRowStatus
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfMainRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainRowStatus (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainRowStatus)
{
    UINT4               u4IfIndex;
    UINT1               u1RowStatus;
    INT1                i1RetVal = SNMP_FAILURE;

    u4IfIndex = (UINT4) i4IfMainIndex;

    if ((CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE) &&
        (CFA_IF_ENTRY_USED (u4IfIndex)))
    {
        if (CfaGetIfRowStatus (u4IfIndex, &u1RowStatus) == CFA_SUCCESS)
        {
            *pi4RetValIfMainRowStatus = CFA_IF_RS (u4IfIndex);
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else
    {
        if (CfaGetIfRowStatus (u4IfIndex, &u1RowStatus) == CFA_SUCCESS)
        {
            *pi4RetValIfMainRowStatus = u1RowStatus;
            i1RetVal = SNMP_SUCCESS;
        }
    }
    return (i1RetVal);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIfMainType
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfMainType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainType (INT4 i4IfMainIndex, INT4 i4SetValIfMainType)
{
    INT4                i4SubType = -1;
    UINT4               u4IfIndex;
    UINT1               au1IfName[CFA_MAX_IFALIAS_LENGTH];
    UINT1               u1InvalidTypeFlag = CFA_FALSE;

    tCfaIfCreateParams  IfaceInfo;

    MEMSET (au1IfName, 0, CFA_MAX_IFALIAS_LENGTH);
    u4IfIndex = (UINT4) i4IfMainIndex;

    CFA_DS_LOCK ();
    CFA_CDB_IS_INTF_VALID (u4IfIndex);
    CFA_DS_UNLOCK ();
    /* Null check has to be provided here to avoid crash as MSR does not call
     * Test routine */
    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        if (CFA_IF_ENTRY (u4IfIndex) == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    else if (CFA_CDB_IS_INTF_VALID (u4IfIndex) != CFA_TRUE)
    {
        return SNMP_FAILURE;
    }

    CfaGetIfAlias (u4IfIndex, au1IfName);

    CfaSetIfIpv4AddressChange (u4IfIndex, CFA_FALSE);

    switch (i4SetValIfMainType)
    {
        case CFA_ENET:
            if (CfaIfmInitEnetIfEntry (u4IfIndex, au1IfName) == CFA_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            break;
#if defined (WGS_WANTED) || defined (OPENFLOW_WANTED)
        case CFA_L2VLAN:
#endif
            if (CfaIsOpenFlowIndex (u4IfIndex) == CFA_TRUE)
            {
                if (CfaIfmInitOfcVlanIfEntry (u4IfIndex, au1IfName) ==
                    CFA_SUCCESS)
                {
                    return SNMP_SUCCESS;
                }
                break;
            }
        case CFA_L3IPVLAN:
            if (gu4IsIvrEnabled == CFA_ENABLED)
            {
                if (CfaIfmInitVlanIfEntry (u4IfIndex, au1IfName) == CFA_SUCCESS)
                {
                    return SNMP_SUCCESS;
                }
            }
            break;
        case CFA_L3SUB_INTF:
            if (gu4IsIvrEnabled == CFA_ENABLED)
            {
                if (CfaIfmInitL3SubIfEntry (u4IfIndex, au1IfName) ==
                    CFA_SUCCESS)
                {
                    return SNMP_SUCCESS;
                }
            }
            break;

        case CFA_LOOPBACK:
            CfaSetIfType (u4IfIndex, CFA_LOOPBACK);
            if (CfaIfmInitLoopbackIfEntry (u4IfIndex, au1IfName) == CFA_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            break;

        case CFA_LAGG:
            if (LaIsLaStarted () == LA_TRUE)
            {
                if (CfaIfmInitPortChannelIfEntry
                    (u4IfIndex, au1IfName) == CFA_SUCCESS)
                {
                    return SNMP_SUCCESS;
                }
            }
            break;

        case CFA_PIP:
        case CFA_BRIDGED_INTERFACE:
            if (CfaIfmInitInternalIfEntry (u4IfIndex, au1IfName) == CFA_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            break;

        case CFA_ILAN:
            if (CfaIfmInitILanIfEntry (u4IfIndex, au1IfName) == CFA_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            break;

#ifdef PPP_WANTED
        case CFA_PPP:
            if (CfaIfmInitPppIfEntry (u4IfIndex, au1IfName) == CFA_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            break;
#endif /* PPP_WANTED */

#ifdef MPLS_WANTED
        case CFA_MPLS:
            if ((CfaIfmInitMplsIfEntry (u4IfIndex, au1IfName)) == CFA_SUCCESS)
            {
                return (SNMP_SUCCESS);
            }
            break;

        case CFA_MPLS_TUNNEL:
            if (CfaIfmInitMplsTunnelIfEntry (u4IfIndex, au1IfName) ==
                CFA_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            break;
#endif /* MPLS_WANTED */

        case CFA_TUNNEL:
            MEMSET (&IfaceInfo, 0, sizeof (tCfaIfCreateParams));
            MEMCPY (IfaceInfo.au1AliasName, au1IfName,
                    CFA_MAX_PORT_NAME_LENGTH);

            IfaceInfo.u4IfType = (UINT4) i4SetValIfMainType;
            if ((CfaIfmInitTunlIfEntry (u4IfIndex, &IfaceInfo)) == CFA_SUCCESS)
            {
                return (SNMP_SUCCESS);
            }
            break;

        case CFA_PROP_VIRTUAL_INTERFACE:
            if (CfaUtilGetSubTypeFromNameAndType
                ((UINT1 *) au1IfName, (UINT4) i4SetValIfMainType,
                 &i4SubType) == CFA_FAILURE)
            {
                return SNMP_FAILURE;
            }

            if (i4SubType == CFA_SUBTYPE_SISP_INTERFACE)
            {

                /* For SISP related logical interfaces, no initialisation is 
                 * required. When this port is mapped over a physical port, the 
                 * appropriate values of the port properties will be copied. Other
                 * than that no operation is needed.
                 * */
                CfaSetIfType (u4IfIndex, CFA_PROP_VIRTUAL_INTERFACE);
                CfaSetIfName (u4IfIndex, au1IfName);

                CFA_DS_LOCK ();
                STRNCPY (CFA_CDB_IF_DESC (u4IfIndex),
                         au1IfName, CFA_MAX_DESCR_LENGTH - 1);
                CFA_DS_UNLOCK ();
                CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_ENABLED);
                return SNMP_SUCCESS;
            }
            if (i4SubType == CFA_SUBTYPE_AC_INTERFACE)
            {
                if (CfaIfmInitACIfEntry (u4IfIndex, au1IfName) == CFA_SUCCESS)
                {
                    return SNMP_SUCCESS;
                }
            }
            break;

#ifdef TLM_WANTED
        case CFA_TELINK:
            if (CfaIfmInitTeLinkIfEntry (u4IfIndex, au1IfName) == CFA_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            else
            {
                break;
            }
#endif
        case CFA_PSEUDO_WIRE:
            if (CfaIfmInitPswIfEntry (u4IfIndex, au1IfName) == CFA_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            break;
#ifdef HDLC_WANTED
        case CFA_HDLC:
            if (CfaIfmInitHdlcIfEntry (u4IfIndex, au1IfName) == CFA_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            break;
#endif
#ifdef VXLAN_WANTED
        case CFA_VXLAN_NVE:
            if (CfaIfmInitNveIfEntry (u4IfIndex, au1IfName) == CFA_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            break;
#endif
#if ((defined (WLC_WANTED)) || (defined (WTP_WANTED)))
        case CFA_CAPWAP_VIRT_RADIO:
            if (CfaIfmInitVirtualRadioIfEntry (u4IfIndex, au1IfName) ==
                CFA_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            break;

        case CFA_CAPWAP_DOT11_PROFILE:
            if (CfaIfmInitWlanProfileIfEntry (u4IfIndex, au1IfName) ==
                CFA_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            break;

        case CFA_CAPWAP_DOT11_BSS:
            if (CfaIfmInitWlanBssIfEntry (u4IfIndex, au1IfName) == CFA_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            break;

        case CFA_RADIO:
            if (CfaIfmInitRadioIfEntry (u4IfIndex, au1IfName) == CFA_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            break;

        case CFA_WLAN_RADIO:
            if (CfaIfmInitWlanRadioIfEntry (u4IfIndex, au1IfName) ==
                CFA_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            break;
#endif
#ifdef VCPEMGR_WANTED
        case CFA_TAP:
            if (CfaIfmInitTapIfEntry (u4IfIndex, au1IfName) == CFA_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            break;
#endif
        default:
            u1InvalidTypeFlag = CFA_TRUE;
            break;
    }

    /* Couldn't set the ifMainType, but for this interface already buf 
     * allocation has been done. So release all the buffers associated with 
     * this interface */
    if (u1InvalidTypeFlag == CFA_FALSE)
    {
        CfaIfmDeleteIfEntry (u4IfIndex, CFA_INVALID_TYPE);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfMainBrgPortType
 Input       :  The Indices
                IfMainIndex
 
                The Object 
                setValIfMainBrgPortType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainBrgPortType (INT4 i4IfMainIndex, INT4 i4SetValIfMainBrgPortType)
{
#ifdef VLAN_WANTED
    INT4                i4RetVal = VLAN_FALSE;
#endif
    UINT4               u4IfIndex = 0;
    UINT4               u4SChIfIndex = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4Count = 0;
    UINT4               u4StartSbpIndex = 0;
    UINT4               u4EndSbpIndex = 0;
    UINT2               au2ConfPorts[L2IWF_MAX_PORTS_PER_CONTEXT];
    UINT2               u2NumPorts = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1BrgPortType = 0;
    UINT1               u1IfType = 0;
    UINT1               u1IfSubType = 0;
    UINT1               u1OperStatus = CFA_IF_DOWN;
    UINT1               u1InterfaceType = VLAN_INVALID_INTERFACE_TYPE;
    UINT1               u1AdminIntfTypeFlag = CFA_FALSE;
    UINT1               u1Status = L2IWF_DISABLED;

    u4IfIndex = (UINT4) i4IfMainIndex;

    if (CfaValidateCfaIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        /* Null check has to be provided here to avoid crash as MSR does 
         * not callTest routine */
        if (CFA_IF_ENTRY (u4IfIndex) == NULL)
        {
            return SNMP_FAILURE;
        }
        /* If the interface is active then only the vlan module will know
         * So if the row status is not active then return failure. */
        if (CFA_IF_RS (u4IfIndex) != CFA_RS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_FALSE)
        {
            return SNMP_FAILURE;
        }
    }

    /* If the port is not mapped to any context, then don't
     * allow setting of brg port type. */
    if (VcmGetContextInfoFromIfIndex ((UINT4) i4IfMainIndex, &u4ContextId,
                                      &u2LocalPort) == VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }

    CfaGetIfBrgPortType (u4IfIndex, &u1BrgPortType);

    if (u1BrgPortType == (UINT1) i4SetValIfMainBrgPortType)
    {
        if ((i4SetValIfMainBrgPortType == CFA_CNP_STAGGED_PORT) ||
            (i4SetValIfMainBrgPortType == CFA_CNP_CTAGGED_PORT))
        {
            L2IwfSetAdminIntfTypeFlag (u4ContextId, CFA_TRUE);
        }
        if (i4SetValIfMainBrgPortType == CFA_UPLINK_ACCESS_PORT)
        {
            return SNMP_SUCCESS;
        }
/*  Incase of ACTIVE node, if the Bridge Port type (u1BrgPortType) is same as the 
    value to be set (i4SetValIfMainBrgPortType), we can return SUCCESS stating that port type is already set as expected.
    But incase of STANDBY node, the L2IwfPortCreateIndication will not happen when a port is removed from the port-channel.
    Hence returning SUCCESS is possible only if the node status is ACTIVE.
*/
        if (CFA_NODE_STATUS () == CFA_NODE_ACTIVE)
        {
            return SNMP_SUCCESS;
        }
    }

    CfaGetIfType (u4IfIndex, &u1IfType);
    CfaGetIfSubType (u4IfIndex, &u1IfSubType);

    switch (u1IfType)
    {
        case CFA_LAGG:
            /* If the port-channel contains some member ports then don't allow
             * port type configuration for that port-channel. */
            L2IwfGetConfiguredPortsForPortChannel ((UINT2) u4IfIndex,
                                                   au2ConfPorts, &u2NumPorts);

            if (u2NumPorts != 0)
            {
                return SNMP_FAILURE;
            }
            break;

        case CFA_ENET:
            if (L2IwfIsPortInPortChannel ((UINT4) i4IfMainIndex) ==
                L2IWF_SUCCESS)
            {
                /* Port is part of port-channel, so don't allow this
                 * configuration. */
                return SNMP_FAILURE;
            }
            break;

        case CFA_PIP:
        case CFA_BRIDGED_INTERFACE:
            if (CfaIsVipInterface ((UINT4) i4IfMainIndex) == CFA_FALSE)
            {
                if (CFA_IS_INTERNAL_INTERFACE_PORT_TYPE_VALID
                    (i4SetValIfMainBrgPortType) == CFA_FALSE)
                {
                    /* Port type is not permissible for internal interface type */
                    return SNMP_FAILURE;
                }
            }
            else
            {
                if (i4SetValIfMainBrgPortType != CFA_VIRTUAL_INSTANCE_PORT)
                {
                    /* Port type is not permissible for internal interface type */
                    return SNMP_FAILURE;
                }
            }
            break;

        case CFA_PROP_VIRTUAL_INTERFACE:
            break;

        default:
            return SNMP_FAILURE;
    }

#ifdef VLAN_WANTED
    if (!((u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
          (u1IfSubType == CFA_SUBTYPE_AC_INTERFACE)))
    {

        /* Check whether the given port type is permissible from Vlan module. */
        i4RetVal = VlanValidatePortType ((UINT4) i4IfMainIndex,
                                         (UINT1) i4SetValIfMainBrgPortType);

        if (i4RetVal == VLAN_FALSE)
        {
            CLI_SET_ERR (CLI_CFA_BRG_PORT_TYPE_MODE_ERR);
            return SNMP_FAILURE;
        }
    }
#endif

    /* Change Interface Type only for S-C or C-S,
       For rest, normal flow should hit */
    if (((i4SetValIfMainBrgPortType == CFA_CNP_CTAGGED_PORT) ||
         (i4SetValIfMainBrgPortType == CFA_CNP_STAGGED_PORT)) &&
        (u1BrgPortType == CFA_CNP_STAGGED_PORT))
    {
        if (CfaIsVipInterface ((UINT4) i4IfMainIndex) == CFA_FALSE)
        {
            L2IwfGetInterfaceType (u4ContextId, &u1InterfaceType);
            L2IwfGetAdminIntfTypeFlag (u4ContextId, &u1AdminIntfTypeFlag);
            L2IwfGetCnpPortCount (u4ContextId, &u4Count);
            if (i4SetValIfMainBrgPortType == CFA_CNP_CTAGGED_PORT)
            {
                if ((u1AdminIntfTypeFlag == CFA_FALSE) ||
                    ((u1AdminIntfTypeFlag == CFA_TRUE)
                     && (u4Count == CFA_MAX_CNP_COUNT_TO_TOGGLE)))
                {
                    L2IwfSetAdminIntfTypeFlag (u4ContextId, CFA_TRUE);
                    if (u1InterfaceType == VLAN_S_INTERFACE_TYPE)
                    {
                        CfaSetIfBrgPortType (u4IfIndex,
                                             (UINT1) CFA_CNP_STAGGED_PORT);
                        L2IwfSetInterfaceType (u4ContextId,
                                               VLAN_C_INTERFACE_TYPE);

                        L2IwfIntfTypeChangeIndication (u4IfIndex,
                                                       VLAN_C_INTERFACE_TYPE);
                        return SNMP_SUCCESS;
                    }
                }
            }

            if (i4SetValIfMainBrgPortType == CFA_CNP_STAGGED_PORT)

            {
                if ((u1AdminIntfTypeFlag == CFA_FALSE) ||
                    ((u1AdminIntfTypeFlag == CFA_TRUE)
                     && (u4Count == CFA_MAX_CNP_COUNT_TO_TOGGLE)))
                {
                    L2IwfSetAdminIntfTypeFlag (u4ContextId, CFA_TRUE);
                    if (u1InterfaceType == VLAN_C_INTERFACE_TYPE)
                    {
                        CfaSetIfBrgPortType (u4IfIndex,
                                             (UINT1) CFA_CNP_STAGGED_PORT);
                        L2IwfSetInterfaceType (u4ContextId,
                                               VLAN_S_INTERFACE_TYPE);

                        L2IwfIntfTypeChangeIndication (u4IfIndex,
                                                       VLAN_S_INTERFACE_TYPE);
                        return SNMP_SUCCESS;
                    }
                }
            }
        }
    }

    /* When a physical or port channel port is deleted in L2IWF, SISP enabled
     * status will be lost. So before deletion of the port in L2IWF, Sisp 
     * control status will be get in a variable and restored after port
     * creation in L2IWF*/
    if (SISP_IS_LOGICAL_PORT (u4IfIndex) != VCM_TRUE)
    {
        L2IwfGetSispPortCtrlStatus (u4IfIndex, &u1Status);
    }

    if (u1BrgPortType == CFA_UPLINK_ACCESS_PORT)
    {
        u4StartSbpIndex = (CFA_MIN_EVB_SBP_INDEX + (VLAN_EVB_MAX_SBP_PER_UAP *
                                                    (u4IfIndex - 1)));
        u4EndSbpIndex = u4StartSbpIndex + VLAN_EVB_MAX_SBP_PER_UAP - 1;

        /* Deleting all the S-Channels present under this UAP */
        /* When the port type is changed from UAP to CBP, all
         * the S-Channels created in CFA are deleted instead of getting
         * indication from VLAN. This avoids the hang happens
         * when CFA->VLAN->CFA calls flow. */
        for (; u4StartSbpIndex <= u4EndSbpIndex; u4StartSbpIndex++)
        {
            CfaUtilDeleteSChannelInterface (u4StartSbpIndex);
        }
    }
    /* Give port delete indication to the modules that may get affected
     * by bridge port type indication. */
    L2IwfPortDeleteIndication (u4IfIndex);
#ifdef ISS_WANTED
    if (u1IfType == CFA_ENET)
    {
        IssDeletePort ((UINT2) u4IfIndex, ISS_ALL_TABLES);
    }
#endif

    if ((u1IfType == CFA_BRIDGED_INTERFACE) &&
        (i4SetValIfMainBrgPortType == CFA_PROVIDER_INSTANCE_PORT))
    {
        CfaSetIfType (u4IfIndex, CFA_PIP);
    }

    if ((u1IfType == CFA_PIP) &&
        (i4SetValIfMainBrgPortType == CFA_CUSTOMER_BACKBONE_PORT))
    {
        CfaSetIfType (u4IfIndex, CFA_BRIDGED_INTERFACE);
    }

    if (i4SetValIfMainBrgPortType == CFA_CNP_CTAGGED_PORT)
    {
        L2IwfSetInterfaceType (u4ContextId, VLAN_C_INTERFACE_TYPE);
        L2IwfSetAdminIntfTypeFlag (u4ContextId, CFA_TRUE);
        /* Update Bridge port type now. */
        CfaSetIfBrgPortType (u4IfIndex, (UINT1) CFA_CNP_STAGGED_PORT);
    }
    else
    {
        if ((i4SetValIfMainBrgPortType == CFA_CNP_PORTBASED_PORT) ||
            (i4SetValIfMainBrgPortType == CFA_CNP_STAGGED_PORT))
        {
            L2IwfSetInterfaceType (u4ContextId, VLAN_S_INTERFACE_TYPE);
            L2IwfSetAdminIntfTypeFlag (u4ContextId, CFA_TRUE);
        }
        /* Update Bridge port type now. */
        CfaSetIfBrgPortType (u4IfIndex, (UINT1) i4SetValIfMainBrgPortType);
    }

    /* Give port create indication. On getting the port create msg, the upper 
     * layer modules will get read the port type and update themselves. */
    L2IwfPortCreateIndication (u4IfIndex);

    if (SISP_IS_LOGICAL_PORT (u4IfIndex) != VCM_TRUE)
    {
        /* When SISP is enabled on the port, that will be restored in L2IWF,
         * after port create indication in L2IWF */
        if (u1Status == L2IWF_ENABLED)
        {
            L2IwfUpdateSispPortCtrlStatus (u4IfIndex, u1Status);
        }
    }
#ifdef ISS_WANTED
    if (u1IfType == CFA_ENET)
    {
        IssCreatePort ((UINT2) u4IfIndex, ISS_ALL_TABLES);
    }

#endif
#ifdef QOSX_WANTED
    if (!((u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
          (u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
    {
        QosCreatePort (u4IfIndex);
    }
#endif

    if (u1IfType == CFA_LAGG)
    {
        /* As Port-Channel is deleted, update the admin status of the 
         * port-channel to LA. If admin status is not updated then
         * the port-channel will not become up. */
        if (CFA_IF_ADMIN (u4IfIndex) == CFA_IF_UP)
        {
            LaUpdatePortChannelAdminStatus ((UINT2) u4IfIndex, CFA_IF_UP,
                                            &u1OperStatus);
            /* There is no need to update the u1OperStatus to CFA here.
             * As the port-channel is newly created, it will be down
             * until a port is aggregated to it. So the operstatus
             * will be down here. */
        }
    }

    CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
    if (u1OperStatus == CFA_IF_UP)
    {
        EoamApiNotifyIfOperChg (u4IfIndex, u1OperStatus);
        L2IwfPortOperStatusIndication (u4IfIndex, u1OperStatus);
    }

    if (SISP_IS_LOGICAL_PORT (u4IfIndex) != VCM_TRUE)
    {
        /* Copy physical port properties to the SISP ports created over it */
        CfaCopyBridgePortPropToSispPorts (u4IfIndex,
                                          CFA_PORT_PROP_BRG_TYPE,
                                          (UINT4) i4SetValIfMainBrgPortType);
    }
    if (i4SetValIfMainBrgPortType == CFA_UPLINK_ACCESS_PORT)
    {
        if (CfaGetFreeSChIndexForUap (u4IfIndex, &u4SChIfIndex) == CFA_SUCCESS)
        {
            CfaUtilCreateSChannelInterface (u4IfIndex, u4SChIfIndex,
                                            u1OperStatus);
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfMainSubType
 Input       :  The Indices
                IfMainIndex

                The Object
                setValIfMainSubType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainSubType (INT4 i4IfMainIndex, INT4 i4SetValIfMainSubType)
{
    INT4                i4IfMainType = 0;

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    nmhGetIfMainType (i4IfMainIndex, &i4IfMainType);

    if (((i4IfMainType == CFA_ENET)
         && (i4SetValIfMainSubType != CFA_ENET_UNKNOWN)) ||
        ((i4IfMainType == CFA_L2VLAN)
         && (i4SetValIfMainSubType == CFA_SUBTYPE_OPENFLOW_INTERFACE)) ||
        ((i4IfMainType == CFA_PPP)
         && (i4SetValIfMainSubType != CFA_ENET_UNKNOWN)))
    {
        if (CfaSetIfSubType
            ((UINT4) i4IfMainIndex,
             (UINT1) i4SetValIfMainSubType) == CFA_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfMainMtu
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfMainMtu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainMtu (INT4 i4IfMainIndex, INT4 i4SetValIfMainMtu)
{
    UINT4               u4IfIndex;
    UINT1               u1IfType;
    UINT1               u1IsActive = CFA_FALSE;
    UINT1               u1IsAlreadyRegWithIp = CFA_FALSE;
    UINT4               u4IfMtu;
#ifdef PPP_WANTED
    tIfLayerInfo        LowInterface;
    tIfLayerInfo        HighInterface;
    tPppIpInfo          IpInfo;
    UINT1               u1SubnetMask;
    UINT4               u4IpAddr;
#endif /* PPP_WANTED */
#if defined IP6_WANTED && defined LNXIP6_WANTED
    UINT4               u4RaMtu = 0;
    tLip6If            *pIf6Entry = NULL;
#endif

    u4IfIndex = (UINT4) i4IfMainIndex;

    if (CfaValidateCfaIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Null check has to be provided here to avoid crash as MSR does not call
     * Test routine */
    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        if (CFA_IF_ENTRY (u4IfIndex) == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }

    CfaGetIfMtu (u4IfIndex, &u4IfMtu);
    if (u4IfMtu != (UINT4) i4SetValIfMainMtu)
    {

        if (CFA_IF_RS (u4IfIndex) == CFA_RS_ACTIVE)
        {
            u1IsActive = CFA_TRUE;
            if (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX)
            {
                u1IsAlreadyRegWithIp = CFA_TRUE;
            }
        }

        CfaGetIfType (u4IfIndex, &u1IfType);

        if (u1IfType == CFA_INVALID_TYPE)
        {
            /* Silent ignore, for invalid port type.
             * Test routine already covered this check.
             * This code will hit only in case of MSR where
             * set is called without test routine.
             * Since user will be confused on getting MSR
             * errors this rare case is silently ignored.
             */
            return SNMP_SUCCESS;
        }

#ifdef NPAPI_WANTED
        if ((u1IfType == CFA_ENET) || (u1IfType == CFA_LAGG)
            || (u1IfType == CFA_L3IPVLAN))
        {
            if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
            {
                /* Program the new MTU in h/w */
                if (FNP_FAILURE == CfaFsCfaHwSetMtu (u4IfIndex,
                                                     (UINT4) i4SetValIfMainMtu))
                {
                    return SNMP_FAILURE;
                }
            }
        }
#endif
#ifdef LLDP_WANTED
        if ((u1IfType == CFA_ENET) || (u1IfType == CFA_LAGG))
        {
            LldpUpdateMtu (u4IfIndex, (UINT4) i4SetValIfMainMtu);
        }
#endif

        if (u1IfType == CFA_ENET)
        {
#ifdef LA_WANTED
            LaUpdateRestoreMtu ((UINT2) u4IfIndex, (UINT4) i4SetValIfMainMtu);
#endif
        }

        CfaSetIfMtu (u4IfIndex, (UINT4) i4SetValIfMainMtu);
#if defined IP6_WANTED && defined LNXIP6_WANTED
        IP6_TASK_LOCK ();
        Ip6GetRtrAdvtLinkMTU ((INT4) u4IfIndex, &u4RaMtu);
        if (u4RaMtu == IP6_INTERFACE_LINK_MTU)
        {
            pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);
            if (pIf6Entry != NULL)
            {

                if (Lip6RAConfig (pIf6Entry->u4ContextId) == OSIX_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
        }

        IP6_TASK_UNLOCK ();
#endif

        switch (u1IfType)
        {
            case CFA_PIP:
            case CFA_BRIDGED_INTERFACE:
            case CFA_TUNNEL:
            case CFA_ENET:
            case CFA_PROP_VIRTUAL_INTERFACE:
            case CFA_LAGG:
            case CFA_IFPWTYPE:
            case CFA_L3IPVLAN:
                /* While configuring for logical VLAN interfaces, care
                 * should be taken to, configure this value as the
                 * lowest of the MTU values of the member ports.
                 * Administrator also have to take care of dynamically 
                 * added ports.
                 */

                if (u1IsActive == CFA_TRUE)
                {
                    if (u1IsAlreadyRegWithIp == CFA_TRUE)
                    {
                        if (CfaIfmConfigNetworkInterface (CFA_NET_IF_UPD,
                                                          u4IfIndex, 0,
                                                          NULL) != CFA_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                    }
                }
                break;

#ifdef MPLS_WANTED
            case CFA_MPLS:
#endif /* MPLS_WANTED */
                break;
#ifdef PPP_WANTED
            case CFA_PPP:

                LowInterface.u1Command = HighInterface.u1Command =
                    CFA_LAYER_IGNORE;
                nmhGetIfIpAddr (u4IfIndex, &u4IpAddr);
                IpInfo.u4LocalIpAddr = u4IpAddr;
                IpInfo.u4PeerIpAddr = CFA_IF_PEER_IPADDR (u4IfIndex);
                IpInfo.u1SubnetMask = CFA_IF_IPSUBNET (u4IfIndex);

                if (u1IsActive == CFA_TRUE)
                {
                    u1SubnetMask = CFA_IF_IPSUBNET (u4IfIndex);
                    u4IpAddr = CFA_IF_IPADDR (u4IfIndex);

                    if (u1IfType == CFA_PPP)
                    {
                        if (CfaIfmUpdatePppInterface (u4IfIndex, &LowInterface,
                                                      &HighInterface, &IpInfo,
                                                      CFA_IF_IDLE_TIMEOUT
                                                      (u4IfIndex),
                                                      (UINT4) i4SetValIfMainMtu)
                            != CFA_SUCCESS)
                        {
                            return (SNMP_FAILURE);
                        }
                    }

                }
                break;
#endif /* PPP_WANTED */
            case CFA_HDLC:
                break;

            case CFA_TELINK:
                break;

            default:
                return SNMP_FAILURE;    /* not expected */

        }

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfMainAdminStatus
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfMainAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainAdminStatus (INT4 i4IfMainIndex, INT4 i4SetValIfMainAdminStatus)
{
    UINT1               u1UfdOperStatus = 0;

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#ifdef EOAM_WANTED
    if (i4SetValIfMainAdminStatus == CFA_IF_DOWN)
    {
        EoamApiIfShutDownNotify ((UINT4) i4IfMainIndex, CFA_IF_DOWN);
    }
#endif

    /* Incase of IVR,update the NW type as WAN if the member ports
     * of the VLAN are WAN ports */
    if ((i4IfMainIndex >= CFA_MIN_IVR_IF_INDEX)
        && (i4IfMainIndex <= CFA_MAX_IVR_IF_INDEX))
    {
        if (i4SetValIfMainAdminStatus == CFA_IF_UP)
        {
            if (CfaIfmCheckAndSetL3VlanIfWanType
                ((UINT4) i4IfMainIndex, CFA_IF_UPDATE) != CFA_SUCCESS)
            {
                return SNMP_FAILURE;
            }
        }
    }
    /* Fix for Bug 7050 - Admin and Oper status of a port which is a part of 
     * UFD group which is down is not correct*/
    if (CFA_UFD_MODULE_STATUS () == CFA_UFD_ENABLED)
    {
        if ((CfaGetIfUfdOperStatus ((UINT4) i4IfMainIndex, &u1UfdOperStatus)
             == CFA_SUCCESS) && (u1UfdOperStatus == CFA_IF_UFD_ERR_DISABLED))
        {
            CLI_SET_ERR (CLI_CFA_UFD_ERR_DISABLED_ERR);
            return SNMP_FAILURE;
        }
    }
    if (CfaSetIfMainAdminStatus (i4IfMainIndex, i4SetValIfMainAdminStatus)
        == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfMainEncapType
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfMainEncapType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainEncapType (INT4 i4IfMainIndex, INT4 i4SetValIfMainEncapType)
{
    UINT4               u4IfIndex;
    UINT1               u1IfType = CFA_NONE;
    UINT1               u1IsActive = CFA_FALSE;
    UINT1               u1IsAlreadyRegWithIp = CFA_FALSE;

    u4IfIndex = (UINT4) i4IfMainIndex;

    if (CfaValidateCfaIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Null check has to be provided here to avoid crash as MSR does not call
     * Test routine */
    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
    {
        if (CFA_IF_ENTRY (u4IfIndex) == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }

    if (CFA_IF_ENCAP (u4IfIndex) != (UINT1) i4SetValIfMainEncapType)
    {

        if (CFA_IF_RS (u4IfIndex) == CFA_RS_ACTIVE)
        {
            u1IsActive = CFA_TRUE;
            if (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX)

            {
                u1IsAlreadyRegWithIp = CFA_TRUE;
            }
        }

        CFA_IF_ENCAP (u4IfIndex) = (UINT1) i4SetValIfMainEncapType;

        CfaGetIfType (u4IfIndex, &u1IfType);

        switch (u1IfType)
        {

            case CFA_ENET:
                if (u1IsActive == CFA_TRUE)
                {
                    if (u1IsAlreadyRegWithIp == CFA_TRUE)
                    {
                        if (CfaIfmConfigNetworkInterface (CFA_NET_IF_UPD,
                                                          u4IfIndex, 0,
                                                          NULL) != CFA_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                    }
                }
                break;

            default:
                return SNMP_FAILURE;    /* not expected */

        }

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfMainNetworkType
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfMainNetworkType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainNetworkType (INT4 i4IfMainIndex, INT4 i4SetValIfMainNetworkType)
{
    UINT1               u1NetworkType = 0;
    /* The interface validity check is added here to avoid crash as MSR 
     * does not call Test routine. The interface doesn't exists in OS 
     * when a Modem is not connected to USB interface.*/

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (CfaIsIfEntryProgrammingAllowed ((UINT4) i4IfMainIndex) == CFA_TRUE)
    {
        if (CFA_IF_ENTRY ((UINT4) i4IfMainIndex) == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }

    CfaGetIfNwType ((UINT4) i4IfMainIndex, &u1NetworkType);

    if (u1NetworkType == (UINT1) i4SetValIfMainNetworkType)
    {
        return SNMP_SUCCESS;
    }

    CfaSetIfNwType ((UINT4) i4IfMainIndex, (UINT1) i4SetValIfMainNetworkType);

    if (CFA_IF_RS ((UINT4) i4IfMainIndex) == CFA_RS_ACTIVE)
    {
        if (i4SetValIfMainNetworkType == CFA_NETWORK_TYPE_LAN)
        {
            CfaSetIfWanType ((UINT4) i4IfMainIndex, CFA_WAN_TYPE_PRIVATE);
            CfaRegNotifyHigherLayer ((UINT4) i4IfMainIndex, CFA_IF_UPDATE);
        }

        else if (i4SetValIfMainNetworkType == CFA_NETWORK_TYPE_WAN)
        {
            /* LAN -> WAN 
             * By default the WAN interface is considered PUBLIC */
            nmhSetIfMainWanType (i4IfMainIndex, CFA_WAN_TYPE_PUBLIC);
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfMainWanType
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfMainWanType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainWanType (INT4 i4IfMainIndex, INT4 i4SetValIfMainWanType)
{
    tStackInfoStruct   *pScanNode = NULL;
    UINT1               u1WanType = 0;

    /* The interface validity check is added here to avoid crash as MSR 
     * does not call Test routine. The interface doesn't exists in OS 
     * when a Modem is not connected to USB interface.*/

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (CfaIsIfEntryProgrammingAllowed ((UINT4) i4IfMainIndex) == CFA_TRUE)
    {
        if (CFA_IF_ENTRY ((UINT4) i4IfMainIndex) == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }

    CfaGetIfWanType ((UINT4) i4IfMainIndex, &u1WanType);

    if (u1WanType == (UINT1) i4SetValIfMainWanType)
    {
        return SNMP_SUCCESS;
    }

    CfaSetIfWanType ((UINT4) i4IfMainIndex, (UINT1) i4SetValIfMainWanType);

    if (CFA_IF_RS ((UINT4) i4IfMainIndex) == CFA_RS_ACTIVE)
    {
        pScanNode =
            (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY ((UINT4)
                                                          i4IfMainIndex);
        if (pScanNode == NULL)
        {
            return SNMP_FAILURE;
        }

        if (CFA_IF_STACK_IFINDEX (pScanNode) == 0)
        {
            CfaRegNotifyHigherLayer ((UINT4) i4IfMainIndex, CFA_IF_UPDATE);
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfMainRowStatus
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfMainRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainRowStatus (INT4 i4IfMainIndex, INT4 i4SetValIfMainRowStatus)
{
    UINT4               u4IfIndex;
    UINT4               u4ContextId;
    UINT4               u4UfdGroupId = 0;
    UINT1               u1RowStatus;
    UINT1               u1IfType = CFA_INVALID_TYPE;
    UINT1               u1InterfaceExists = CFA_FALSE;
    UINT1               u1InterfaceUsable = CFA_FALSE;
    UINT1               au1NullString[2] = "";
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    UINT1               u1OperStatus = CFA_NONE;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
#endif
    UINT4               u4RetValue = CFA_SUCCESS;
#ifdef MBSM_WANTED
    INT4                i4SlotId;
    tMbsmSlotInfo       SlotInfo;
#endif
    UINT1               u1BridgedIfaceStatus;
#ifdef PPP_WANTED
    tIpConfigInfo      *pIpConfigInfo = NULL;
#endif
    UINT2               u2LocalPortId;
    UINT1               u1IfSubType = 0;
    UINT4               u4Index;
    tCfaIfInfo         *pCfaIfInfo = NULL;
#if defined (MPLS_WANTED) && defined (NPAPI_WANTED)
    UINT4               u4LLIfIndex = FNP_ZERO;
#endif
#ifdef LNXIP4_WANTED
    INT4                i4ProxyArpAdminStatus = 0;
#endif
    UINT1               u1BrgPortType = 0;
    UINT4               u4StartSbpIndex = 0;
    UINT4               u4EndSbpIndex = 0;

    u4IfIndex = (UINT4) i4IfMainIndex;

    u1RowStatus = (UINT1) i4SetValIfMainRowStatus;

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return (SNMP_FAILURE);
    }
    /* Default interface vlan1 will be automatically created, so avoiding configuration 
       from RM Active’s indication */
#ifdef RM_WANTED
    if ((CFA_NODE_STATUS () == CFA_NODE_STANDBY)
        && (u4IfIndex == BRG_MAX_PHY_PLUS_LOG_PORTS + 1)
        && (RmGetStaticConfigStatus () == RM_STATIC_CONFIG_IN_PROGRESS))
    {
        return (SNMP_SUCCESS);
    }
#endif
    if ((CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE) &&
        (CFA_IF_ENTRY_USED (u4IfIndex)))
    {
        u1InterfaceExists = CFA_TRUE;

        CfaGetIfType (u4IfIndex, &u1IfType);
        if (CFA_IF_RS (u4IfIndex) != CFA_RS_NOTREADY)
        {
            u1InterfaceUsable = CFA_TRUE;
            UNUSED_PARAM (u1InterfaceUsable);
        }
    }
    else
    {
        CfaGetIfType (u4IfIndex, &u1IfType);
    }

    CfaGetIfSubType (u4IfIndex, &u1IfSubType);

#ifdef VLAN_WANTED
    /* Check for not adding IVR Vlan in Dot1d Mode */
    /*Default vlan alone is allowed in Doltd mode */
    if ((i4IfMainIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES &&
         (i4IfMainIndex != CFA_DEFAULT_ROUTER_VLAN_IFINDEX)) &&
        (VlanGetBaseBridgeMode () == DOT_1D_BRIDGE_MODE))
    {
        CLI_SET_ERR (CLI_CFA_VLAN_BASE_BRIDGE_ENABLED);
        return SNMP_FAILURE;
    }
#endif

    switch (u1RowStatus)
    {

        case CFA_RS_CREATEANDWAIT:
        case CFA_RS_CREATEANDGO:

            /* give null string as port name for the phys interfaces */
            if (CfaIfmCreateAndInitIfEntry (u4IfIndex, CFA_INVALID_TYPE,
                                            au1NullString) == CFA_SUCCESS)
            {
                if (((u4IfIndex >= CFA_MIN_IVR_IF_INDEX)
                     && (u4IfIndex <= CFA_MAX_TNL_IF_INDEX)) ||
                    (CfaIsL3SubIfIndex (u4IfIndex) == CFA_TRUE))
                {
                    /* Create IP If Mapping in VCM as l2 context id needs to be
                     * set before setting ifAliasName.
                     */
                    if (VcmCfaCreateIPIfaceMapping
                        (VCM_DEFAULT_CONTEXT, u4IfIndex) == VCM_SUCCESS)
                    {
                        return SNMP_SUCCESS;
                    }
                    else
                    {
                        return SNMP_FAILURE;
                    }
                }
                return SNMP_SUCCESS;
            }
            else
            {
                return SNMP_FAILURE;
            }
            break;

        case CFA_RS_ACTIVE:

            if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
            {
                if (CFA_IF_ENTRY (u4IfIndex) == NULL)
                {
                    /* Interface is not present */
                    return SNMP_FAILURE;
                }

                if (CFA_IF_RS (u4IfIndex) == CFA_RS_ACTIVE)
                {
                    return SNMP_SUCCESS;
                }

                if (CFA_IF_RS (u4IfIndex) == CFA_RS_NOTINSERVICE)
                {
                    switch (u1IfType)
                    {

#ifdef PPP_WANTED
                        case CFA_PPP:
/* we now create the interface in PPP  */
                            if (u1IfType == CFA_PPP)
                            {
                                if (CfaIfmCreatePppInterface (u4IfIndex) !=
                                    CFA_SUCCESS)
                                {
                                    return SNMP_FAILURE;
                                }
                            }

                            CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
                            CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);
                            CfaSetCdbRowStatus (u4IfIndex, CFA_RS_ACTIVE);

                            /* Register the PPP interface with IP */
                            if (CFA_IF_STACK_IFINDEX
                                (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex)) == 0)
                            {

                                if (CFA_SUCCESS ==
                                    CfaIfmConfigNetworkInterface
                                    (CFA_NET_IF_NEW, u4IfIndex, 0,
                                     pIpConfigInfo))

                                {
                                    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                                              "In nmhSetIfMainRowStatus - "
                                              "IP Registration success - interface %d\n",
                                              u4IfIndex);
                                }
                                else
                                {
                                    /* Unsuccessful in registering with IP */
                                    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                              "Error in nmhSetIfMainRowStatus - "
                                              "IP Registration fail interface %d\n",
                                              u4IfIndex);
                                    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
                                    CfaSetIfActiveStatus (u4IfIndex, CFA_FALSE);
                                    CfaSetCdbRowStatus (u4IfIndex,
                                                        CFA_RS_NOTINSERVICE);
                                    return SNMP_FAILURE;
                                }
                            }

                            break;
#endif /* PPP_WANTED */

                            /* for IPOA - the change from NOTINSERVICE to ACTIVE will come from the 
                               IPOA MIB's client/server table */
                        case CFA_LAGG:
                        case CFA_PIP:
                        case CFA_BRIDGED_INTERFACE:
                            /* for these interfaces we simply set it to active */
                        case CFA_ENET:

                            CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
                            CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);

                            /* 
                             * When IVR is enabled, the interface create 
                             * indication will be given to L2 only when it is a 
                             * bridged interface else it will be given only to IP.
                             * Otherwise, all the interfaces will be registered
                             * with L2 and IP.
                             */

                            CfaGetIfBridgedIfaceStatus (u4IfIndex,
                                                        &u1BridgedIfaceStatus);

                            if (u1BridgedIfaceStatus == CFA_ENABLED)
                            {
                                if (!(CFA_ENET_CREATE_DELETE_ALLOWED))
                                {
                                    VcmCfaCreateIfaceMapping
                                        (L2IWF_DEFAULT_CONTEXT, u4IfIndex);
                                }
                            }
                            if (gu4IsIvrEnabled == CFA_ENABLED)
                            {
                                CfaGetIfBridgedIfaceStatus (u4IfIndex,
                                                            &u1BridgedIfaceStatus);

                                if (u1BridgedIfaceStatus == CFA_ENABLED)
                                {
                                    if (VcmGetContextInfoFromIfIndex (u4IfIndex,
                                                                      &u4ContextId,
                                                                      &u2LocalPortId)
                                        == VCM_SUCCESS)
                                    {
                                        L2IwfPortCreateIndication (u4IfIndex);
                                        EoamApiNotifyIfCreate (u4IfIndex);
                                        IssCreatePort ((UINT2) u4IfIndex,
                                                       ISS_ALL_TABLES);
                                    }
                                }
                                else
                                {
                                    u4RetValue =
                                        (UINT4) CfaIfmConfigNetworkInterface
                                        (CFA_NET_IF_NEW, u4IfIndex, 0, NULL);
                                }
                            }
                            else
                            {
                                if (VcmGetContextInfoFromIfIndex (u4IfIndex,
                                                                  &u4ContextId,
                                                                  &u2LocalPortId)
                                    == VCM_SUCCESS)
                                {
                                    L2IwfPortCreateIndication (u4IfIndex);
                                    EoamApiNotifyIfCreate (u4IfIndex);
                                    IssCreatePort ((UINT2) u4IfIndex,
                                                   ISS_ALL_TABLES);
#ifdef QOSX_WANTED
                                    QosCreatePort (u4IfIndex);
#endif
                                    u4RetValue =
                                        (UINT4) CfaIfmConfigNetworkInterface
                                        (CFA_NET_IF_NEW, u4IfIndex, 0, NULL);
                                }
                            }

                            if (u4RetValue != CFA_SUCCESS)

                            {
                                /* unsuccessful in registration IP - 
                                 * but we dont fail the interface creation */
                                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                          "Error in nmhSetExIfMainRowStatus - "
                                          "IP Registration fail interface %d\n",
                                          u4IfIndex);
                                CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
                                CfaSetIfActiveStatus (u4IfIndex, CFA_FALSE);
                                return SNMP_FAILURE;
                            }
                            else
                            {

                                CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                                          "In nmhSetIfMainRowStatus - "
                                          "IP Registration success - interface %d\n",
                                          u4IfIndex);
                            }
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
                            CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);

                            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

                            CfaGetIfName (u4IfIndex, au1IfName);
                            /*Give an indication to IP to update its oper and admin status */
                            if (u1OperStatus == CFA_IF_UP)
                            {
                                CfaGetIfBridgedIfaceStatus (u4IfIndex,
                                                            &u1BridgedIfaceStatus);

                                if ((gu4IsIvrEnabled == CFA_ENABLED) &&
                                    (u1BridgedIfaceStatus == CFA_DISABLED))
                                {
                                    IpUpdateInterfaceStatus ((UINT2)
                                                             CFA_IF_IPPORT
                                                             (u4IfIndex),
                                                             au1IfName,
                                                             u1OperStatus);
                                }
                            }
#endif /*IP_WANTED */
#ifdef MBSM_WANTED
                            if ((u1IfType == CFA_ENET) &&
                                (u4IfIndex != CFA_DEFAULT_ROUTER_VLAN_IFINDEX))
                            {
                                /* If physical interfaces can be created
                                 * at run-time by configuration, then
                                 * check MBSM for port oper status.
                                 * Eg: MI case */

                                MbsmGetSlotFromPort (u4IfIndex, &i4SlotId);

                                if (MbsmGetSlotInfo (i4SlotId, &SlotInfo)
                                    == MBSM_SUCCESS)
                                {
                                    if (SlotInfo.i1Status == MBSM_STATUS_DOWN)
                                    {
                                        CfaMbsmHandleInterfaceStatusUpdate
                                            (u4IfIndex,
                                             CFA_IF_ADMIN (u4IfIndex),
                                             CFA_IF_DOWN);
                                    }
                                    else if (SlotInfo.i1Status ==
                                             MBSM_STATUS_NP)
                                    {
                                        CfaMbsmHandleInterfaceStatusUpdate
                                            (u4IfIndex,
                                             CFA_IF_ADMIN (u4IfIndex),
                                             CFA_IF_NP);
                                    }
                                    else if (SlotInfo.i1Status ==
                                             MBSM_STATUS_ACTIVE)
                                    {
                                        CfaIfmUpdateIfParams (u4IfIndex);
                                    }
                                }

                            }
#endif
                            break;

                        case CFA_TUNNEL:
                            /* when VLAN interface is created then
                             * we register interface with IP with 0.0.0.0 IP
                             * address
                             */

                        case CFA_L3IPVLAN:
                            CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
                            CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);
                            /* we register the interface with IP */
                            if (CfaIfmConfigNetworkInterface (CFA_NET_IF_NEW,
                                                              u4IfIndex, 0,
                                                              NULL) !=
                                CFA_SUCCESS)
                            {
                                /* unsuccessful in registration IP - but 
                                 * we dont fail the interface creation */
                                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                          "Error in nmhSetExIfMainRowStatus - "
                                          "IP Registration fail interface %d\n",
                                          u4IfIndex);
                                CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
                                return SNMP_FAILURE;
                            }
                            else
                            {

                                CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                                          "In nmhSetExIfMainRowStatus - "
                                          "IP Registration success - interface %d\n",
                                          u4IfIndex);
                            }
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
                            CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);

                            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                            CfaGetIfName (u4IfIndex, au1IfName);

                            /*Give an indication to IP to update its oper and admin status */
                            if (u1OperStatus == CFA_IF_UP)
                            {
                                if ((CfaUpdtIvrInterface (u4IfIndex,
                                                          CFA_IF_CREATE)) ==
                                    CFA_FAILURE)
                                {
                                    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                              "Error in nmhSetExIfMainRowStatus - "
                                              "IP Interface creation fails for interface %d\n",
                                              u4IfIndex);
                                    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
                                    CfaSetIfActiveStatus (u4IfIndex, CFA_FALSE);

                                    return SNMP_FAILURE;
                                }

                                IpUpdateInterfaceStatus ((UINT2)
                                                         CFA_IF_IPPORT
                                                         (u4IfIndex), au1IfName,
                                                         u1OperStatus);
                            }

#endif /*IP_WANTED */
                            break;

                        case CFA_L3SUB_INTF:
                            CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
                            CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);
                            /* we register the interface with IP */
                            if (CfaIfmConfigNetworkInterface (CFA_NET_IF_NEW,
                                                              u4IfIndex, 0,
                                                              NULL) !=
                                CFA_SUCCESS)
                            {
                                /* unsuccessful in registration IP - but 
                                 * we dont fail the interface creation */
                                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                          "Error in nmhSetExIfMainRowStatus - "
                                          "IP Registration fail interface %d\n",
                                          u4IfIndex);
                                CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
                                return SNMP_FAILURE;
                            }
                            else
                            {

                                CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                                          "In nmhSetExIfMainRowStatus - "
                                          "IP Registration success - interface %d\n",
                                          u4IfIndex);
                            }
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
                            CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);

                            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                            CfaGetIfName (u4IfIndex, au1IfName);

                            /*Give an indication to IP to update its oper and admin status */
                            if (u1OperStatus == CFA_IF_UP)
                            {
                                if ((CfaUpdtIvrInterface (u4IfIndex,
                                                          CFA_IF_CREATE)) ==
                                    CFA_FAILURE)
                                {
                                    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                              "Error in nmhSetExIfMainRowStatus - "
                                              "IP Interface creation fails for interface %d\n",
                                              u4IfIndex);
                                    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
                                    CfaSetIfActiveStatus (u4IfIndex, CFA_FALSE);

                                    return SNMP_FAILURE;
                                }

                                IpUpdateInterfaceStatus ((UINT2)
                                                         CFA_IF_IPPORT
                                                         (u4IfIndex), au1IfName,
                                                         u1OperStatus);
                            }

#endif /*IP_WANTED */
                            break;

#ifdef WGS_WANTED
                        case CFA_L2VLAN:
                            CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
                            CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);
                            /* we register the interface with IP */
                            if (CfaIfmConfigNetworkInterface (CFA_NET_IF_NEW,
                                                              u4IfIndex, 0,
                                                              NULL) !=
                                CFA_SUCCESS)
                            {
                                /* unsuccessful in registration IP - but 
                                 * we dont fail the interface creation */
                                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                          "Error in nmhSetIfMainRowStatus - "
                                          "IP Registration fail interface %d\n",
                                          u4IfIndex);
                                CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
                                CfaSetIfActiveStatus (u4IfIndex, CFA_FALSE);
                                return SNMP_FAILURE;
                            }
                            else
                            {

                                CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                                          "In nmhSetIfMainRowStatus - "
                                          "IP Registration success - interface %d\n",
                                          u4IfIndex);
                            }
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
                            CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);

                            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                            CfaGetIfName (u4IfIndex, au1IfName);
                            /*Give an indication to IP to update its oper and admin status */
                            if (u1OperStatus == CFA_IF_UP)
                            {
                                IpUpdateInterfaceStatus ((UINT2)
                                                         CFA_IF_IPPORT
                                                         (u4IfIndex), au1IfName,
                                                         u1OperStatus);
                            }
#endif /*IP_WANTED */
                            break;
#endif
                        case CFA_LOOPBACK:

                            CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
                            CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);
                            /* we register the interface with IP */
                            if (CfaIfmConfigNetworkInterface (CFA_NET_IF_NEW,
                                                              u4IfIndex, 0,
                                                              NULL) !=
                                CFA_SUCCESS)
                            {
                                /* unsuccessful in registration IP - but 
                                 * we dont fail the interface creation */
                                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                          "Error in nmhSetExIfMainRowStatus - "
                                          "IP Registration fail interface %d\n",
                                          u4IfIndex);
                                CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
                                CfaSetIfActiveStatus (u4IfIndex, CFA_FALSE);
                                return SNMP_FAILURE;
                            }
                            else
                            {

                                CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                                          "In nmhSetIfMainRowStatus - "
                                          "IP Registration success - interface %d\n",
                                          u4IfIndex);
                            }

#if defined (IP_WANTED) || defined (LNXIP4_WANTED)

                            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                            CfaGetIfName (u4IfIndex, au1IfName);

                            CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
                            if (u1OperStatus == CFA_IF_UP)
                            {

                                /*Give an indication to IP to update its oper and
                                 * admin status */
                                if ((CfaUpdtIvrInterface (u4IfIndex,
                                                          CFA_IF_CREATE)) ==
                                    CFA_FAILURE)
                                {
                                    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                              "Error in nmhSetIfMainRowStatus - "
                                              "IP Interface creation fails for interface %d\n",
                                              u4IfIndex);
                                    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
                                    CfaSetIfActiveStatus (u4IfIndex, CFA_FALSE);

                                    return SNMP_FAILURE;
                                }

                                IpUpdateInterfaceStatus ((UINT2) CFA_IF_IPPORT
                                                         (u4IfIndex), au1IfName,
                                                         u1OperStatus);
                            }
#endif /*IP_WANTED */
                            break;

#ifdef MPLS_WANTED
                        case CFA_MPLS:
                        case CFA_MPLS_TUNNEL:
                            CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
                            CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);
#ifdef NPAPI_WANTED
                            if (u1IfType == CFA_MPLS)
                            {
                                if (CfaGetLLIfIndex (u4IfIndex, &u4LLIfIndex) ==
                                    CFA_SUCCESS)
                                {
                                    if (FsMplsHwEnableMplsIf (u4LLIfIndex, NULL)
                                        == FNP_SUCCESS)
                                    {
                                        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                                  "FsMplsHwEnableMplsIf success "
                                                  "for Index - %d\n",
                                                  u4IfIndex);
                                        break;
                                    }
                                    else
                                    {
                                        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                                  "FsMplsHwEnableMplsIf failed "
                                                  "for Index - %d\n",
                                                  u4IfIndex);
                                        return SNMP_FAILURE;
                                    }
                                }
                                else
                                {
                                    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                              "Failed to get the LLIfindex "
                                              "for Index - %d\n", u4IfIndex);
                                    break;
                                }
                            }
#endif
                            break;
#endif /* MPLS_WANTED */
                        case CFA_ILAN:
                            CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
                            CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);
                            CfaSetCdbRowStatus (u4IfIndex, CFA_RS_ACTIVE);
                            /* Update Bridge port type now. */
                            CfaSetIfBrgPortType (u4IfIndex,
                                                 CFA_CUSTOMER_BRIDGE_PORT);

                            break;

                        case CFA_TELINK:
                            CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
                            CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);
                            break;

                        case CFA_PSEUDO_WIRE:
                            CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
                            CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);

                            /*
                             *                          * When IVR is enabled, the interface create
                             * indication will be given to L2 only when it is a
                             * bridged interface else it will be given only to IP.
                             * Otherwise, all the interfaces will be registered
                             * with L2 and IP.
                             */

                            CfaGetIfBridgedIfaceStatus (u4IfIndex,
                                                        &u1BridgedIfaceStatus);

                            if (u1BridgedIfaceStatus == CFA_ENABLED)
                            {
                                if (!(CFA_PSW_CREATE_DELETE_ALLOWED))
                                {
                                    VcmCfaCreateIfaceMapping
                                        (L2IWF_DEFAULT_CONTEXT, u4IfIndex);
                                }
                            }

                            if (gu4IsIvrEnabled == CFA_ENABLED)
                            {
                                CfaGetIfBridgedIfaceStatus (u4IfIndex,
                                                            &u1BridgedIfaceStatus);

                                if (u1BridgedIfaceStatus == CFA_ENABLED)
                                {
                                    if (VcmGetContextInfoFromIfIndex (u4IfIndex,
                                                                      &u4ContextId,
                                                                      &u2LocalPortId)
                                        == VCM_SUCCESS)
                                    {
                                        L2IwfPortCreateIndication (u4IfIndex);
                                        EoamApiNotifyIfCreate (u4IfIndex);
                                        IssCreatePort ((UINT2) u4IfIndex,
                                                       ISS_ALL_TABLES);
#ifdef QOSX_WANTED
                                        QosCreatePort (u4IfIndex);
#endif
                                    }
                                }
                                else
                                {
                                    u4RetValue =
                                        (UINT4) CfaIfmConfigNetworkInterface
                                        (CFA_NET_IF_NEW, u4IfIndex, 0, NULL);
                                }
                            }
                            else
                            {
                                if (VcmGetContextInfoFromIfIndex (u4IfIndex,
                                                                  &u4ContextId,
                                                                  &u2LocalPortId)
                                    == VCM_SUCCESS)
                                {
                                    L2IwfPortCreateIndication (u4IfIndex);
                                    EoamApiNotifyIfCreate (u4IfIndex);
                                    IssCreatePort ((UINT2) u4IfIndex,
                                                   ISS_ALL_TABLES);
#ifdef QOSX_WANTED
                                    QosCreatePort (u4IfIndex);
#endif
                                    u4RetValue =
                                        (UINT4) CfaIfmConfigNetworkInterface
                                        (CFA_NET_IF_NEW, u4IfIndex, 0, NULL);
                                }
                            }
                            break;
                        case CFA_PROP_VIRTUAL_INTERFACE:
                            if (u1IfSubType == CFA_SUBTYPE_AC_INTERFACE)
                            {

                                CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
                                CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);

                                /*
                                 * When IVR is enabled, the interface create
                                 * indication will be given to L2 only when it is a
                                 * bridged interface else it will be given only to IP.
                                 * Otherwise, all the interfaces will be registered
                                 * with L2 and IP.
                                 */

                                CfaGetIfBridgedIfaceStatus (u4IfIndex,
                                                            &u1BridgedIfaceStatus);

                                if (u1BridgedIfaceStatus == CFA_ENABLED)
                                {
                                    if (!(CFA_AC_CREATE_DELETE_ALLOWED))
                                    {
                                        VcmCfaCreateIfaceMapping
                                            (L2IWF_DEFAULT_CONTEXT, u4IfIndex);
                                    }
                                }
                                if (gu4IsIvrEnabled == CFA_ENABLED)
                                {
                                    CfaGetIfBridgedIfaceStatus (u4IfIndex,
                                                                &u1BridgedIfaceStatus);

                                    if (u1BridgedIfaceStatus == CFA_ENABLED)
                                    {
                                        if (VcmGetContextInfoFromIfIndex
                                            (u4IfIndex, &u4ContextId,
                                             &u2LocalPortId) == VCM_SUCCESS)
                                        {
                                            L2IwfPortCreateIndication
                                                (u4IfIndex);
                                            EoamApiNotifyIfCreate (u4IfIndex);
                                            IssCreatePort ((UINT2) u4IfIndex,
                                                           ISS_ALL_TABLES);
#ifdef QOSX_WANTED
                                            if (u1IfSubType !=
                                                CFA_SUBTYPE_SISP_INTERFACE)
                                            {
                                                QosCreatePort (u4IfIndex);
                                            }
#endif
                                        }
                                    }
                                    else
                                    {
                                        u4RetValue =
                                            (UINT4) CfaIfmConfigNetworkInterface
                                            (CFA_NET_IF_NEW, u4IfIndex, 0,
                                             NULL);
                                    }
                                }
                                else
                                {
                                    if (VcmGetContextInfoFromIfIndex (u4IfIndex,
                                                                      &u4ContextId,
                                                                      &u2LocalPortId)
                                        == VCM_SUCCESS)
                                    {
                                        L2IwfPortCreateIndication (u4IfIndex);
                                        EoamApiNotifyIfCreate (u4IfIndex);
                                        IssCreatePort ((UINT2) u4IfIndex,
                                                       ISS_ALL_TABLES);
#ifdef QOSX_WANTED
                                        if (u1IfSubType !=
                                            CFA_SUBTYPE_SISP_INTERFACE)
                                        {
                                            QosCreatePort (u4IfIndex);
                                        }
#endif
                                        u4RetValue =
                                            (UINT4) CfaIfmConfigNetworkInterface
                                            (CFA_NET_IF_NEW, u4IfIndex, 0,
                                             NULL);
                                    }
                                }
                            }
                            break;
#ifdef VXLAN_WANTED
                        case CFA_VXLAN_NVE:
                            CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
                            CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);

                            /*
                             *                          * When IVR is enabled, the interface create
                             * indication will be given to L2 only when it is a
                             * bridged interface else it will be given only to IP.
                             * Otherwise, all the interfaces will be registered
                             * with L2 and IP.
                             */

                            CfaGetIfBridgedIfaceStatus (u4IfIndex,
                                                        &u1BridgedIfaceStatus);

                            if (u1BridgedIfaceStatus == CFA_ENABLED)
                            {
                                VcmCfaCreateIfaceMapping
                                    (L2IWF_DEFAULT_CONTEXT, u4IfIndex);
                            }

                            if (gu4IsIvrEnabled == CFA_ENABLED)
                            {
                                CfaGetIfBridgedIfaceStatus (u4IfIndex,
                                                            &u1BridgedIfaceStatus);

                                if (u1BridgedIfaceStatus == CFA_ENABLED)
                                {
                                    if (VcmGetContextInfoFromIfIndex (u4IfIndex,
                                                                      &u4ContextId,
                                                                      &u2LocalPortId)
                                        == VCM_SUCCESS)
                                    {
                                        L2IwfPortCreateIndication (u4IfIndex);
                                        EoamApiNotifyIfCreate (u4IfIndex);
                                        IssCreatePort ((UINT2) u4IfIndex,
                                                       ISS_ALL_TABLES);
                                    }
                                }
                                else
                                {
                                    u4RetValue =
                                        (UINT4) CfaIfmConfigNetworkInterface
                                        (CFA_NET_IF_NEW, u4IfIndex, 0, NULL);
                                }
                            }
                            else
                            {
                                if (VcmGetContextInfoFromIfIndex (u4IfIndex,
                                                                  &u4ContextId,
                                                                  &u2LocalPortId)
                                    == VCM_SUCCESS)
                                {
                                    L2IwfPortCreateIndication (u4IfIndex);
                                    EoamApiNotifyIfCreate (u4IfIndex);
                                    IssCreatePort ((UINT2) u4IfIndex,
                                                   ISS_ALL_TABLES);
                                    u4RetValue =
                                        (UINT4) CfaIfmConfigNetworkInterface
                                        (CFA_NET_IF_NEW, u4IfIndex, 0, NULL);
                                }
                            }
                            break;
#endif
#if ((defined (WLC_WANTED)) || (defined (WTP_WANTED)))
                        case CFA_CAPWAP_VIRT_RADIO:
                        case CFA_CAPWAP_DOT11_PROFILE:
                        case CFA_RADIO:
                        case CFA_WLAN_RADIO:
                            CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
                            CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);
                            CfaSetCdbRowStatus (u4IfIndex, CFA_RS_ACTIVE);
                            break;

                        case CFA_CAPWAP_DOT11_BSS:
                            CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
                            CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);
                            CfaSetCdbRowStatus (u4IfIndex, CFA_RS_ACTIVE);
                            VcmCfaCreateIfaceMapping (L2IWF_DEFAULT_CONTEXT,
                                                      u4IfIndex);
                            L2IwfPortCreateIndication (u4IfIndex);
                            IssCreatePort ((UINT2) u4IfIndex, ISS_ALL_TABLES);
                            break;
#endif
#ifdef HDLC_WANTED
                        case CFA_HDLC:
                            CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
                            CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);

                            /* HDLC Channel interfaces are considered Admin UP
                             * by default */
                            CfaIfmHandleInterfaceStatusChange (u4IfIndex,
                                                               CFA_IF_UP,
                                                               CFA_IF_DOWN,
                                                               CFA_TRUE);
                            break;
#endif
#ifdef VCPEMGR_WANTED
                        case CFA_TAP:
                            CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
                            CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);

                            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                            CfaGetIfName (u4IfIndex, au1IfName);

                            CfaGetIfBridgedIfaceStatus (u4IfIndex,
                                                        &u1BridgedIfaceStatus);

                            if (u1BridgedIfaceStatus == CFA_ENABLED)
                            {
                                VcmCfaCreateIfaceMapping
                                    (L2IWF_DEFAULT_CONTEXT, u4IfIndex);
                            }

                            if (gu4IsIvrEnabled == CFA_ENABLED)
                            {
                                CfaGetIfBridgedIfaceStatus (u4IfIndex,
                                                            &u1BridgedIfaceStatus);

                                if (u1BridgedIfaceStatus == CFA_ENABLED)
                                {
                                    if (VcmGetContextInfoFromIfIndex (u4IfIndex,
                                                                      &u4ContextId,
                                                                      &u2LocalPortId)
                                        == VCM_SUCCESS)
                                    {
                                        L2IwfPortCreateIndication (u4IfIndex);
                                        EoamApiNotifyIfCreate (u4IfIndex);
                                        IssCreatePort ((UINT2) u4IfIndex,
                                                       ISS_ALL_TABLES);
                                    }
                                }
                                else
                                {
                                    u4RetValue =
                                        (UINT4) CfaIfmConfigNetworkInterface
                                        (CFA_NET_IF_NEW, u4IfIndex, 0, NULL);
                                }
                            }
                            else
                            {
                                if (VcmGetContextInfoFromIfIndex (u4IfIndex,
                                                                  &u4ContextId,
                                                                  &u2LocalPortId)
                                    == VCM_SUCCESS)
                                {
                                    L2IwfPortCreateIndication (u4IfIndex);
                                    EoamApiNotifyIfCreate (u4IfIndex);
                                    IssCreatePort ((UINT2) u4IfIndex,
                                                   ISS_ALL_TABLES);
                                    u4RetValue =
                                        (UINT4) CfaIfmConfigNetworkInterface
                                        (CFA_NET_IF_NEW, u4IfIndex, 0, NULL);
                                }
                            }
                            if (CfaGddTapIfCreate
                                (au1IfName, u4IfIndex, CFA_TAP) == OSIX_FAILURE)
                            {
                                CFA_IF_RS (u4IfIndex) = CFA_RS_DESTROY;
                                CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                                         "CfaGddTapIfCreate Failed.\n");
                                return SNMP_FAILURE;
                            }
                            break;
#endif
                        default:
                            return SNMP_FAILURE;
                    }

                }                /* end of RS was NOTINSERVICE and has to be made active */
            }
            else
            {
                /* Can be either SISP or VIP interface */
                switch (u1IfType)
                {
                    case CFA_BRIDGED_INTERFACE:
                    case CFA_PROP_VIRTUAL_INTERFACE:
                        /* Intentional fall through */
                        /* Here CFA_BRIDGED_INTERFACE represents VIP, because
                         * of the earlier if check
                         * */
                        CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);
                        CfaSetCdbRowStatus (u4IfIndex, CFA_RS_ACTIVE);
                        L2IwfPortCreateIndication (u4IfIndex);
                        break;

                    case CFA_L2VLAN:
                        CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);
                        CfaSetCdbRowStatus (u4IfIndex, CFA_RS_ACTIVE);
                        break;
                    default:
                        break;
                }
            }
            return SNMP_SUCCESS;    /* end of RS ACTIVE */

        case CFA_RS_DESTROY:
            /* Before destroying a row, shutdown
             * the interface, which cleans up the H/W
             * related information like ARP/Route
             */
            CfaSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_DOWN);
            switch (u1IfType)
            {

#ifdef PPP_WANTED
                case CFA_PPP:
                    if (CfaIfmDeletePppInterface (u4IfIndex, CFA_TRUE)
                        != CFA_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
#ifdef DIFFSRV_WANTED
                    if ((u1IfType == CFA_PPP))
                    {
                        /* Initialize the DIFF serve data structures for the MP interface */
                        DsInitIfInfo (u4IfIndex, 0, CFA_FALSE);
                    }
#endif /* DIFFSRV_WANTED */
                    MsrNotifyPortDeletion (u4IfIndex);
                    break;
#endif /* PPP_WANTED */

                case CFA_BRIDGED_INTERFACE:

                    if (CfaIsVipInterface (u4IfIndex) == CFA_TRUE)
                    {
                        CFA_DS_LOCK ();

                        if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
                        {
                            u1InterfaceExists = CFA_TRUE;
                        }

                        CFA_DS_UNLOCK ();
                    }
                    /* Proceed furthur for VIP interfaces
                     * */
                    break;

                case CFA_PROP_VIRTUAL_INTERFACE:
                    if (u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)
                    {
                        /* SISP Interface */
                        CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);
                        VcmUnMapPortFromContext (u4IfIndex);

                        CFA_DS_LOCK ();
                        CFA_CDB_SET_INTF_VALID (u4IfIndex, CFA_FALSE);
                        CFA_CDB_SET_INTF_ACTIVE (u4IfIndex, CFA_FALSE);
                        CFA_DS_UNLOCK ();
                        MsrNotifyPortDeletion (u4IfIndex);
                    }
                    if (u1IfSubType == CFA_SUBTYPE_AC_INTERFACE)
                    {
                        if (CfaIfmDeleteACInterface
                            (u4IfIndex, CFA_TRUE) != CFA_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }

                    }
                    return SNMP_SUCCESS;

                case CFA_L2VLAN:
                    CfaSetIfActiveStatus (u4IfIndex, CFA_FALSE);
                    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_DESTROY);
                    if (CfaIfmDeleteIfEntry (u4IfIndex, u1IfType)
                        != CFA_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                    return SNMP_SUCCESS;

                default:
                    break;
            }

            if (u1InterfaceExists == CFA_TRUE)
            {
                MEMSET (CFA_CDB_IF_DESCRIPTION (u4IfIndex), 0,
                        CFA_MAX_INTF_DESC_LEN);

                switch (u1IfType)
                {

                    case CFA_ENET:
                        CfaGetIfBrgPortType (u4IfIndex, &u1BrgPortType);
                        if (u1BrgPortType == CFA_UPLINK_ACCESS_PORT)
                        {
                            u4StartSbpIndex = (CFA_MIN_EVB_SBP_INDEX +
                                               (VLAN_EVB_MAX_SBP_PER_UAP *
                                                (u4IfIndex - 1)));
                            u4EndSbpIndex = u4StartSbpIndex +
                                VLAN_EVB_MAX_SBP_PER_UAP - 1;

                            /* Deleting all the S-Channels present under this UAP */
                            /* When the port type is changed from UAP to CBP, all
                             * the S-Channels created in CFA are deleted instead of getting
                             * indication from VLAN. This avoids the hang happens
                             * when CFA->VLAN->CFA calls flow. */
                            for (; u4StartSbpIndex <= u4EndSbpIndex;
                                 u4StartSbpIndex++)
                            {
                                CfaUtilDeleteSChannelInterface
                                    (u4StartSbpIndex);
                            }
                        }
                        if (CFA_UFD_MODULE_STATUS () != CFA_UFD_ENABLED)
                        {
                            if (CfaUfdCheckLastDownlinkInGroup (u4IfIndex) !=
                                OSIX_FALSE)
                            {
                                if (CfaGetIfUfdGroupId
                                    (u4IfIndex, &u4UfdGroupId) == CFA_SUCCESS)
                                {
                                    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_IFM,
                                             "In nmhSetIfMainRowStatus  - Getting \
                                            the UFD Group Id failed\r\n ");
                                    if (u4UfdGroupId != 0)
                                    {
                                        CfaUfdRemoveAllPortsInGroup
                                            (u4UfdGroupId);
                                    }

                                }
                            }
                        }
                        if (CfaIfmDeleteEnetInterface
                            (u4IfIndex, CFA_TRUE) != CFA_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                        break;
                    case CFA_LAGG:
                        /* To update filter due to deleting port-channel interface */
                        ISS_LOCK ();
                        if (AclUpdateOverPortChannel (u4IfIndex) != CFA_SUCCESS)
                        {
                            ISS_UNLOCK ();
                            return SNMP_FAILURE;
                        }
                        ISS_UNLOCK ();
                        if (CFA_UFD_MODULE_STATUS () != CFA_UFD_ENABLED)
                        {
                            if (CfaUfdCheckLastDownlinkInGroup (u4IfIndex) !=
                                OSIX_FALSE)
                            {
                                if (CfaGetIfUfdGroupId
                                    (u4IfIndex, &u4UfdGroupId) == CFA_SUCCESS)
                                {
                                    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_IFM,
                                             "In nmhSetIfMainRowStatus  - Getting \
                                            the UFD Group Id failed\r\n ");

                                    if (u4UfdGroupId != 0)
                                    {
                                        CfaUfdRemoveAllPortsInGroup
                                            (u4UfdGroupId);
                                    }
                                }
                            }
                        }
                        if (CfaIfmDeletePortChannelInterface
                            (u4IfIndex, CFA_TRUE, CFA_TRUE) != CFA_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }

                        break;

                    case CFA_PIP:
                    case CFA_BRIDGED_INTERFACE:
                        if (CfaIfmDeleteInternalInterface
                            (u4IfIndex, CFA_TRUE, CFA_TRUE) != CFA_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                        break;

                    case CFA_ILAN:
                        if (CfaIfmDeleteILanInterface
                            (u4IfIndex, CFA_TRUE) != CFA_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                        break;

                    case CFA_L3IPVLAN:

                        if (gu4IsIvrEnabled == CFA_ENABLED)
                        {
                            if (CfaIfmCheckAndSetL3VlanIfWanType
                                (u4IfIndex, CFA_IF_DELETE) != CFA_SUCCESS)
                            {
                                return SNMP_FAILURE;
                            }

                            if (CfaIfmDeleteVlanInterface (u4IfIndex, CFA_TRUE)
                                != CFA_SUCCESS)
                            {
                                return SNMP_FAILURE;
                            }
#ifdef LNXIP4_WANTED
                            if (LnxIpGetProxyArpAdminStatus
                                ((INT4) u4IfIndex,
                                 &i4ProxyArpAdminStatus) == NETIPV4_SUCCESS)
                            {
                                if (LnxIpSetProxyArpAdminStatus
                                    ((INT4) u4IfIndex,
                                     CFA_PROXY_ARP_DISABLE) == NETIPV4_FAILURE)
                                {
                                    CLI_SET_ERR
                                        (CLI_CFA_PROXYARP_NOT_DELETED_ERR);
                                    return SNMP_FAILURE;

                                }
                            }
#endif

                            if (CfaIvrMapDeleteMappedVlans (u4IfIndex) ==
                                CFA_FAILURE)
                            {
                                return SNMP_FAILURE;
                            }
                        }
                        break;

                    case CFA_L3SUB_INTF:
                        if (gu4IsIvrEnabled == CFA_ENABLED)
                        {
                            if (CfaIfmCheckAndSetL3VlanIfWanType
                                (u4IfIndex, CFA_IF_DELETE) != CFA_SUCCESS)
                            {
                                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                          "CfaIfmCheckAndSetL3VlanIfWanType Failure "
                                          "for Index - %d\n", u4IfIndex);
                                return SNMP_FAILURE;
                            }

                            if (CfaIfmDeleteL3SubInterface (u4IfIndex, CFA_TRUE)
                                != CFA_SUCCESS)
                            {
                                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                          "CfaIfmDeleteL3SubInterface Failure "
                                          "for Index - %d\n", u4IfIndex);

                                return SNMP_FAILURE;
                            }
                        }
                        break;

                    case CFA_LOOPBACK:

                        if (CfaIfmDeleteLoopbackInterface (u4IfIndex, CFA_TRUE)
                            != CFA_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                        break;

                    case CFA_TUNNEL:
                        if (CfaIfmDeInitTunlInterface (u4IfIndex, CFA_TRUE)
                            != CFA_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                        break;
#ifdef MPLS_WANTED
                    case CFA_MPLS:
#ifdef NPAPI_WANTED
                        /* Will disable the MPLS processing in hardware on
                         * shutdown of the the MPLS enabled interface */
                        if (CfaGetLLIfIndex (u4IfIndex, &u4LLIfIndex) ==
                            CFA_SUCCESS)
                        {
                            if (FsMplsHwDisableMplsIf (u4LLIfIndex) ==
                                FNP_SUCCESS)
                            {
                                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                          "FsMplsHwDisableMplsIf success "
                                          "for Index - %d\n", u4IfIndex);
                            }
                            else
                            {
                                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                          "FsMplsHwDisableMplsIf failed for "
                                          "Index - %d\n", u4IfIndex);
                                return SNMP_FAILURE;
                            }
                        }
                        else
                        {
                            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                      "Failed to get the LLIfindex for "
                                      "Index - %d\n", u4IfIndex);
                        }
#endif
                        /* Intentional fall through to delete the Mpls *
                         * interface for both MPLS and MPLS_TUNNEL types */
                    case CFA_MPLS_TUNNEL:
                        if (CfaIfmDeleteMplsInterface (u4IfIndex,
                                                       CFA_TRUE) == CFA_SUCCESS)
                        {
                            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                      "CfaIfmDeleteMplsInterface success "
                                      "for Index - %d\n", u4IfIndex);
                            break;
                        }
                        else
                        {
                            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                      "CfaIfmDeleteMplsInterface failed "
                                      "for Index - %d\n", u4IfIndex);
                            return SNMP_FAILURE;
                        }
#endif /* MPLS_WANTED */

#ifdef TLM_WANTED
                    case CFA_TELINK:
                        if (CfaIfmDeleteTeLinkIf (u4IfIndex, CFA_TRUE)
                            != CFA_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                        else
                        {
                            break;
                        }
#endif
                    case CFA_PSEUDO_WIRE:
                        if (CfaIfmDeletePswInterface
                            (u4IfIndex, CFA_TRUE) != CFA_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                        break;
#ifdef VXLAN_WANTED
                    case CFA_VXLAN_NVE:
                        if (CfaIfmDeleteNveInterface
                            (u4IfIndex, CFA_TRUE) != CFA_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                        break;
#endif
#ifdef VCPEMGR_WANTED
                    case CFA_TAP:
                        if (CfaIfmDeleteTapInterface
                            (u4IfIndex, CFA_TRUE) != CFA_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                        break;
#endif
#if ((defined (WLC_WANTED)) || (defined (WTP_WANTED)))
                    case CFA_RADIO:
                        if (CfaIfmDeleteRadioInterface (u4IfIndex, CFA_TRUE)
                            != CFA_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                        break;

                    case CFA_WLAN_RADIO:
                    case CFA_CAPWAP_VIRT_RADIO:
                    case CFA_CAPWAP_DOT11_PROFILE:
                    case CFA_CAPWAP_DOT11_BSS:
                        if (CfaIfmDeleteWssInterface (u4IfIndex, CFA_TRUE)
                            != CFA_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                        break;
#endif
                    default:
                        if (CfaIfmDeleteIfEntry (u4IfIndex, u1IfType)
                            != CFA_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                        break;
                }                /* end of iftype switch */
                MsrNotifyPortDeletion (u4IfIndex);

                /* Check and reset the u4UnnumAssocIPIf interface field
                 ** if any of the interfaces is using this interface as barrowed interface*/

                u4Index = 1;
                CFA_CDB_SCAN_WITH_LOCK (u4Index, (CFA_MAX_INTERFACES ()),
                                        CFA_ALL_IFTYPE)
                {

                    if (CfaIsIfEntryProgrammingAllowed (u4Index) == CFA_FALSE)
                    {
                        if (CFA_CDB_IS_INTF_VALID (u4Index) == CFA_TRUE)
                        {

                            CFA_DS_LOCK ();
                            pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4Index);

                            if ((NULL != pCfaIfInfo)
                                && (pCfaIfInfo->u4UnnumAssocIPIf == u4IfIndex))
                            {
                                pCfaIfInfo->u4UnnumAssocIPIf = 0;
                            }
                            CFA_DS_UNLOCK ();
                        }
                    }
                    else
                    {
#ifdef MBSM_WANTED
                        if (IssGetColdStandbyFromNvRam () ==
                            ISS_COLDSTDBY_ENABLE)
                        {
                            /* To skip the stack ports */
                            CfaGetEthernetType (u4Index, &u1IfType);
                            if (u1IfType == CFA_STACK_ENET)
                            {
                                u4Index = u4Index + ISS_MAX_STK_PORTS;
                                CFA_DS_LOCK ();
                                pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4Index);

                                if ((NULL != pCfaIfInfo)
                                    && (pCfaIfInfo->u4UnnumAssocIPIf ==
                                        u4IfIndex))
                                {
                                    pCfaIfInfo->u4UnnumAssocIPIf = 0;
                                }

                                CFA_DS_UNLOCK ();
                            }
                        }
#endif
                        if (CFA_IF_ENTRY_USED (u4Index))
                        {

                            CFA_DS_LOCK ();
                            pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4Index);

                            if ((NULL != pCfaIfInfo)
                                && (pCfaIfInfo->u4UnnumAssocIPIf == u4IfIndex))
                            {
                                pCfaIfInfo->u4UnnumAssocIPIf = 0;
                            }
                            CFA_DS_UNLOCK ();
                        }
                    }
                }

                return SNMP_SUCCESS;
            }
            else
            {
                return SNMP_FAILURE;
            }

            /* End of DESTROY */
            break;

        case CFA_RS_NOTINSERVICE:
            if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
            {
                if (CFA_IF_RS (u4IfIndex) == CFA_RS_ACTIVE)
                {

                    switch (u1IfType)
                    {
#ifdef PPP_WANTED
                        case CFA_PPP:

                            if (CfaIfmDeletePppInterface (u4IfIndex, CFA_FALSE)
                                != CFA_SUCCESS)
                            {
                                return SNMP_FAILURE;
                            }

#ifdef DIFFSRV_WANTED
                            /* Initialize the DIFF serve data structures for
                             * PPP interface over HDLC  & PVC */
                            if (u1IfType == CFA_PPP)
                            {
                                /* The lower layer is the lower layer IfIndex of the
                                 * PPP interface, For Reseting the IfInfo, Lower layer
                                 * Ifindex is passed as 0 */
                                DsInitIfInfo (u4IfIndex, 0, CFA_FALSE);
                            }
#endif /* DIFFSRV_WANTED */
                            break;
#endif /* PPP_WANTED */

                        case CFA_ENET:
                            if (CfaIfmDeleteEnetInterface (u4IfIndex, CFA_FALSE)
                                != CFA_SUCCESS)
                            {
                                return SNMP_FAILURE;
                            }
                            break;
                        case CFA_LAGG:
                            if (CfaIfmDeletePortChannelInterface
                                (u4IfIndex, CFA_FALSE, CFA_TRUE) != CFA_SUCCESS)
                            {
                                return SNMP_FAILURE;
                            }
                            break;

                        case CFA_PIP:
                        case CFA_BRIDGED_INTERFACE:
                            if (CfaIfmDeleteInternalInterface
                                (u4IfIndex, CFA_FALSE, CFA_TRUE) != CFA_SUCCESS)
                            {
                                return SNMP_FAILURE;
                            }
                            break;

#ifdef WGS_WANTED
                        case CFA_L2VLAN:

                            if (CfaIfmDeleteVlanInterface (u4IfIndex, CFA_FALSE)
                                != CFA_SUCCESS)
                            {
                                return SNMP_FAILURE;
                            }
                            break;
#endif
                        case CFA_L3IPVLAN:
                            if (gu4IsIvrEnabled == CFA_ENABLED)
                            {
                                if (CfaIfmDeleteVlanInterface
                                    (u4IfIndex, CFA_FALSE) != CFA_SUCCESS)
                                {
                                    return SNMP_FAILURE;
                                }
                            }
                            break;

                        case CFA_L3SUB_INTF:
                            if (gu4IsIvrEnabled == CFA_ENABLED)
                            {
                                if (CfaIfmDeleteL3SubInterface
                                    (u4IfIndex, CFA_FALSE) != CFA_SUCCESS)
                                {
                                    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                              "CfaIfmDeleteL3SubInterface Failure "
                                              "for Index - %d\n", u4IfIndex);

                                    return SNMP_FAILURE;
                                }
                            }
                            break;

                        case CFA_LOOPBACK:

                            if (CfaIfmDeleteLoopbackInterface
                                (u4IfIndex, CFA_FALSE) != CFA_SUCCESS)
                            {
                                return SNMP_FAILURE;
                            }
                            break;

                        case CFA_TUNNEL:
                            if (CfaIfmDeInitTunlInterface (u4IfIndex, CFA_TRUE)
                                != CFA_SUCCESS)
                            {
                                return SNMP_FAILURE;
                            }
                            break;

                        case CFA_PSEUDO_WIRE:
                            if (CfaIfmDeletePswInterface (u4IfIndex, CFA_FALSE)
                                != CFA_SUCCESS)
                            {
                                return SNMP_FAILURE;
                            }
                            break;
#ifdef VXLAN_WANTED
                        case CFA_VXLAN_NVE:
                            if (CfaIfmDeleteNveInterface (u4IfIndex, CFA_FALSE)
                                != CFA_SUCCESS)
                            {
                                return SNMP_FAILURE;
                            }
                            break;
#endif
#ifdef VCPEMGR_WANTED
                        case CFA_TAP:
                            if (CfaIfmDeleteTapInterface (u4IfIndex, CFA_FALSE)
                                != CFA_SUCCESS)
                            {
                                return SNMP_FAILURE;
                            }
                            break;
#endif
                        case CFA_PROP_VIRTUAL_INTERFACE:
                            if (u1IfSubType == CFA_SUBTYPE_AC_INTERFACE)
                            {
                                CfaSetIfMainAdminStatus ((INT4) u4IfIndex,
                                                         CFA_IF_DOWN);
                                break;
                            }
#if ((defined (WLC_WANTED)) || (defined (WTP_WANTED)))
                        case CFA_RADIO:
                            if (CfaIfmDeleteRadioInterface
                                (u4IfIndex, CFA_FALSE) != CFA_SUCCESS)
                            {
                                return SNMP_FAILURE;
                            }
                            break;
                        case CFA_WLAN_RADIO:
                        case CFA_CAPWAP_VIRT_RADIO:
                        case CFA_CAPWAP_DOT11_PROFILE:
                        case CFA_CAPWAP_DOT11_BSS:
                            if (CfaIfmDeleteWssInterface (u4IfIndex, CFA_FALSE)
                                != CFA_SUCCESS)
                            {
                                return SNMP_FAILURE;
                            }
                            break;
#endif
                        default:
                            return SNMP_FAILURE;    /* not expected */
                    }            /* end of iftpye switch */
                }                /* end of active interface */
                /* notinservice interface */
                else
                {
                    return SNMP_SUCCESS;
                }

                CfaSetIfActiveStatus (u4IfIndex, CFA_FALSE);
                CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
                return SNMP_SUCCESS;
            }
            else
            {
                if (CfaIsVipInterface (u4IfIndex) == CFA_TRUE)
                {
                    CFA_DS_LOCK ();
                    if (CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_TRUE)
                    {
                        if (CfaIfmDeleteInternalInterface
                            (u4IfIndex, CFA_TRUE, CFA_TRUE) != CFA_SUCCESS)
                        {
                            CFA_DS_UNLOCK ();
                            return SNMP_FAILURE;
                        }
                    }
                    CFA_DS_UNLOCK ();
                }
                if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
                {
                    CfaSetIfActiveStatus (u4IfIndex, CFA_FALSE);
                    CfaSetIfValidStatus (u4IfIndex, CFA_FALSE);
                    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);
                    VcmUnMapPortFromContext (u4IfIndex);
                }
                return SNMP_SUCCESS;
            }
            /* end of RS NOTINSERVICE */

        default:
            return SNMP_FAILURE;    /* not expected */
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IfMainSubType
 Input       :  The Indices
                IfMainIndex

                The Object
                testValIfMainSubType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainSubType (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                        INT4 i4TestValIfMainSubType)
{
    UINT1               u1IfType = 0;
    UINT1               u1EthernetType;

    if (CfaValidateIfMainTableIndex (i4IfMainIndex) != CFA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    CfaGetIfType ((UINT4) i4IfMainIndex, &u1IfType);

    if ((u1IfType != CFA_ENET) && (i4TestValIfMainSubType != CFA_ENET_UNKNOWN))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValIfMainSubType != CFA_XE_ENET) &&
        (i4TestValIfMainSubType != CFA_FA_ENET) &&
        (i4TestValIfMainSubType != CFA_GI_ENET) &&
        (i4TestValIfMainSubType != CFA_XL_ENET) &&
        (i4TestValIfMainSubType != CFA_LVI_ENET) &&
        (i4TestValIfMainSubType != CFA_ENET_UNKNOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    CfaGetEthernetType ((UINT4) i4IfMainIndex, &u1EthernetType);

    if (u1EthernetType != i4TestValIfMainSubType)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfMainType
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfMainType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainType (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                     INT4 i4TestValIfMainType)
{
    UINT1               u1IfType;
    UINT4               u4IfIndex;
    UINT1               au1Str[CFA_MAX_PORT_NAME_LENGTH];
    UINT1              *pu1KeyStr;
    UINT1              *pu1AliasStr;
    UINT4               u4KeyStrLen;
    UINT4               u4AliasStrLen;
    UINT4               u4Counter;
    UINT2               u2VlanId = 0;
    INT4                i4SubType = -1;
    INT4                i4IfName = 0;
    INT4                i4Key;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4IntfIndex;
    UINT4               u4L2CxtId;
    tL2PvlanMappingInfo L2PvlanMappingInfo;

/* for index validation - because validation is commented in mid-level */

    u4IfIndex = (UINT4) i4IfMainIndex;

    MEMSET (au1Str, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    switch (i4TestValIfMainType)
    {
        case CFA_LAGG:
            /* for portchannel interface */
            if ((u4IfIndex < SYS_DEF_MAX_PHYSICAL_INTERFACES + 1) ||
                (u4IfIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;
        case CFA_L3SUB_INTF:
            /* For L3Subinterfaces */
            if ((u4IfIndex < CFA_MIN_L3SUB_IF_INDEX) ||
                (u4IfIndex > CFA_MAX_L3SUB_IF_INDEX))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;

        case CFA_L2VLAN:
            /* for openflow vlan interface */
            if ((u4IfIndex < CFA_MIN_OF_IF_INDEX) ||
                (u4IfIndex > CFA_MAX_OF_IF_INDEX))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;
        case CFA_L3IPVLAN:
            /* for IVR interface */

            if ((u4IfIndex < CFA_MIN_IVR_IF_INDEX)
                || (u4IfIndex > CFA_MAX_IVR_IF_INDEX))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            CfaGetIfAlias (u4IfIndex, au1IfName);
            if (STRLEN (au1IfName) <= STRLEN (CFA_VLAN_ALIAS_NAME))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            if (STRNCMP (au1IfName, CFA_VLAN_ALIAS_NAME,
                         STRLEN (CFA_VLAN_ALIAS_NAME)) == 0)
            {
                MEMSET (au1Str, 0, sizeof (au1Str));
                STRNCPY (au1Str, au1IfName, STRLEN (au1IfName));
                au1Str[STRLEN (au1IfName)] = '\0';
                pu1AliasStr = &(au1Str[4]);
                u2VlanId = (UINT2) ATOL (pu1AliasStr);
                u4AliasStrLen = STRLEN (pu1AliasStr);
                if (VcmGetL2CxtIdForIpIface (u4IfIndex, &u4L2CxtId) ==
                    VCM_SUCCESS)
                {
                    L2PvlanMappingInfo.u4ContextId = u4L2CxtId;
                    L2PvlanMappingInfo.InVlanId = u2VlanId;
                    L2PvlanMappingInfo.u1RequestType = L2IWF_VLAN_TYPE;
                    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);
                    if ((L2PvlanMappingInfo.u1VlanType == L2IWF_ISOLATED_VLAN)
                        || (L2PvlanMappingInfo.u1VlanType ==
                            L2IWF_COMMUNITY_VLAN))
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (CLI_CFA_PVLAN_TYPE_ERR);
                        return SNMP_FAILURE;
                    }
                }
                while (u4AliasStrLen != CFA_ZERO)
                {
                    if (!ISDIGIT (*pu1AliasStr))
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return SNMP_FAILURE;
                    }
                    pu1AliasStr++;
                    u4AliasStrLen--;
                }
                /* If the value is not within size of 16 bits, return */
                /* VLAN 4095, VLAN 4096, VLAN 4097 and VLAN 65535 are not configurable */
                if ((u2VlanId <= 0) || (u2VlanId > CFA_MAX_VLAN_ID ||
                                        (IS_STANDARD_VLAN_ID (u2VlanId))))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
                if (CfaIsPortVlanExists (u2VlanId) == OSIX_SUCCESS)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_CFA_PORT_VLAN_USED_ERR);
                    return SNMP_FAILURE;
                }
#ifdef PB_WANTED
                if (VlanPbIsCreateIvrValid (u2VlanId) == VLAN_FALSE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
#endif
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case CFA_LOOPBACK:
            /* for loopback interface */

            if ((u4IfIndex < CFA_MIN_LOOPBACK_IF_INDEX)
                || (u4IfIndex > CFA_MAX_LOOPBACK_IF_INDEX))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            /*
             * In case of loopback the creation should not be
             * allowed if name is not "loopbackXYZ" where XYZ can
             * be the loopback interface number*/

            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            CfaGetIfAlias (u4IfIndex, au1IfName);

            /* In case of loopback creation should not be allowed
             * strlen is greater than 11 or less than 8
             * (ie) "loopback110a" or "loopback" should be restricted*/
            if ((STRLEN (au1IfName) <= STRLEN (CFA_LOOPBACK_ALIAS_NAME))
                || (STRLEN (au1IfName) > CFA_LOOPBACK_MAX_ALIAS_LEN))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            if (STRNCMP
                (au1IfName, CFA_LOOPBACK_ALIAS_NAME,
                 STRLEN (CFA_LOOPBACK_ALIAS_NAME)) == CFA_ZERO)
            {
                i4IntfIndex =
                    ATOI (au1IfName + STRLEN (CFA_LOOPBACK_ALIAS_NAME));
                /* In case IfName is "loopback 0" it should be allowed
                 * but if it is "loopback a" then it should be restricted.*/
                MEMSET (au1Str, 0, sizeof (au1Str));
                STRNCPY (au1Str, au1IfName, STRLEN (au1IfName));
                au1Str[STRLEN (au1IfName)] = '\0';
                pu1AliasStr = &(au1Str[8]);
                u4AliasStrLen = STRLEN (pu1AliasStr);
                if ((u4AliasStrLen == CFA_ONE)
                    && (i4IntfIndex == CFA_MIN_LOOPBACK_VALUE))
                {
                    if ((*pu1AliasStr) - '0' != CFA_ZERO)
                    {
                        /* Given IfName is loopback x */

                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return SNMP_FAILURE;
                    }
                }
                else
                {
                    /* To check whether loopback id contains character in 
                     * between (ie) "loopback 1a0" or "loopback 10a"
                     * should be restricted*/
                    for (u4Counter = u4AliasStrLen; u4Counter > CFA_ZERO;
                         u4Counter--)
                    {
                        if (!ISDIGIT (*pu1AliasStr))
                        {
                            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                            return SNMP_FAILURE;
                        }
                        pu1AliasStr++;
                    }
                }

                if (((i4IntfIndex < CFA_MIN_LOOPBACK_VALUE)
                     || (i4IntfIndex > CFA_MAX_LOOPBACK_VALUE)))

                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }

            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
#if ((defined (WLC_WANTED)) || (defined (WTP_WANTED)))
        case CFA_RADIO:
            /* for Radio interface */
            if (u4IfIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;
#endif
        case CFA_ENET:
            /* for ethernet interface */
            if (u4IfIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;

        case CFA_TUNNEL:
            /* for IP Tunnel interface */
            if ((u4IfIndex < CFA_MIN_TNL_IF_INDEX) ||
                (u4IfIndex > CFA_MAX_TNL_IF_INDEX))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }

            break;

        case CFA_PIP:
            /* for internal interface */
            if ((u4IfIndex < CFA_MIN_INTERNAL_IF_INDEX)
                || (u4IfIndex > CFA_MAX_INTERNAL_IF_INDEX))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;

        case CFA_BRIDGED_INTERFACE:
            /* for internal interface */
            if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
            {
                if ((u4IfIndex < CFA_MIN_INTERNAL_IF_INDEX)
                    || (u4IfIndex > CFA_MAX_INTERNAL_IF_INDEX))
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    return SNMP_FAILURE;
                }
            }
            else
            {
                if ((u4IfIndex < CFA_MIN_VIP_IF_INDEX)
                    || (u4IfIndex > CFA_MAX_VIP_IF_INDEX))
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    return SNMP_FAILURE;
                }
            }

            break;

        case CFA_ILAN:
            /* for internal interface */
            if ((u4IfIndex < CFA_MIN_ILAN_IF_INDEX)
                || (u4IfIndex > CFA_MAX_ILAN_IF_INDEX))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;

#ifdef MPLS_WANTED
        case CFA_MPLS:
            /* for MPLS interface */
            if ((u4IfIndex < CFA_MIN_MPLS_IF_INDEX) ||
                (u4IfIndex > CFA_MAX_MPLS_IF_INDEX))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;

        case CFA_MPLS_TUNNEL:
            /* for MPLS tunnel interface */
            if ((u4IfIndex < CFA_MIN_MPLSTNL_IF_INDEX) ||
                (u4IfIndex > CFA_MAX_MPLSTNL_IF_INDEX))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;
#endif
        case CFA_PROP_VIRTUAL_INTERFACE:

            CfaGetIfAlias (u4IfIndex, au1IfName);

            if (CfaUtilGetSubTypeFromNameAndType (au1IfName,
                                                  CFA_PROP_VIRTUAL_INTERFACE,
                                                  &i4SubType) == CFA_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (i4SubType == CFA_SUBTYPE_SISP_INTERFACE)
            {
                if ((u4IfIndex < CFA_MIN_SISP_IF_INDEX) ||
                    (u4IfIndex > CFA_MAX_SISP_IF_INDEX))
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    return SNMP_FAILURE;
                }

                /* For SISP logical interfaces, the IfAlias should always be 
                 * formed using the convention string "sisp" followed by interface
                 * index. The interface index in this string is derived as follows
                 * CFA_MIN_SISP_IF_INDEX     ==> 1
                 * CFA_MIN_SISP_IF_INDEX + 1 ==> 2
                 * ...
                 * This needs to be checked.
                 * */
                CfaGetIfAlias (u4IfIndex, au1IfName);

                i4IfName = ATOL (&(au1IfName[CFA_MAX_SISP_NAME_LEN]));

                if (!((i4IfName > 0) && (i4IfName <= 65535)))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            if (i4SubType == CFA_SUBTYPE_AC_INTERFACE)
            {
                if ((u4IfIndex < CFA_MIN_AC_IF_INDEX) ||
                    (u4IfIndex > CFA_MAX_AC_IF_INDEX))
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    return SNMP_FAILURE;
                }

            }
            break;

#ifdef TLM_WANTED
        case CFA_TELINK:
            if ((u4IfIndex < CFA_MIN_TELINK_IF_INDEX) ||
                (u4IfIndex > CFA_MAX_TELINK_IF_INDEX))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            else
            {
                break;
            }
#endif
        case CFA_PSEUDO_WIRE:
            if ((u4IfIndex < CFA_MIN_PSW_IF_INDEX) ||
                (u4IfIndex > CFA_MAX_PSW_IF_INDEX))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;

#ifdef VXLAN_WANTED
        case CFA_VXLAN_NVE:
            if ((u4IfIndex < CFA_MIN_NVE_IF_INDEX) ||
                (u4IfIndex > CFA_MAX_NVE_IF_INDEX))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;
#endif

#ifdef VCPEMGR_WANTED
        case CFA_TAP:
            if ((u4IfIndex < CFA_MIN_TAP_IF_INDEX) ||
                (u4IfIndex > CFA_MAX_TAP_IF_INDEX))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;
#endif

#if ((defined (WLC_WANTED)) || (defined (WTP_WANTED)))
        case CFA_WLAN_RADIO:
        case CFA_CAPWAP_VIRT_RADIO:
        case CFA_CAPWAP_DOT11_PROFILE:
        case CFA_CAPWAP_DOT11_BSS:
            if ((i4IfMainIndex < CFA_MIN_WSS_IF_INDEX) ||
                (i4IfMainIndex > CFA_MAX_WSS_IF_INDEX))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;
#endif
        default:
            /* unknown interface type */
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    CfaGetIfType (u4IfIndex, &u1IfType);
/* once the interface is created - the ifType cant be changed.
The interface should be deleted first */
    if (u1IfType != CFA_INVALID_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

#ifdef MBSM_WANTED
    if (CfaIsPhysicalInterface (u4IfIndex) == CFA_TRUE)
    {
        /* Physical interfaces will not be allowed to create using
         * management protocols, but only by means of Line card
         * insertions  
         */
        CfaIfmDeleteIfEntry (u4IfIndex, CFA_INVALID_TYPE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#endif

    /* If the alias is for port-channel interface (CFA_LAGG), the string
     * should be of the form "po<KEY>" where KEY is the port-channel id
     * used as ActorAdminKey for the Aggregator in Link Aggregation module.
     * Validate the key for acceptable range.
     */

    if (i4TestValIfMainType == CFA_LAGG)
    {
        if ((u4IfIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES)
            && (u4IfIndex <= (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG)))
        {
            CfaGetIfAlias (u4IfIndex, au1IfName);

            if ((STRLEN (au1IfName) > 0) && (STRNCMP (au1IfName, "po", 2) == 0))
            {
                MEMSET (au1Str, 0, sizeof (au1Str));
                STRNCPY (au1Str, au1IfName, STRLEN (au1IfName));
                au1Str[STRLEN (au1IfName)] = '\0';

                pu1KeyStr = &(au1Str[2]);
                u4KeyStrLen = STRLEN (pu1KeyStr);
                if (u4KeyStrLen > 0)
                {
                    while (u4KeyStrLen != 0)
                    {
                        if (!ISDIGIT (*pu1KeyStr))
                        {
                            break;
                        }
                        u4KeyStrLen--;
                        pu1KeyStr++;
                    }
                    if (u4KeyStrLen == 0)
                    {
                        pu1KeyStr = &(au1Str[2]);
                        i4Key = ATOL (pu1KeyStr);
                        /* If the value is not within size UINT2, return */
                        if ((i4Key > 0) && (i4Key <= 65535))
                        {
                            return SNMP_SUCCESS;
                        }
                    }
                }
            }
        }
        CfaIfmDeleteIfEntry (u4IfIndex, CFA_INVALID_TYPE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

/* for creation a phys interface - the port name viz. eth1 should be first
given in ifAlias object of the standard ifXTable. Also the ifIndex should
be within the MAX_INTERFACES, else deallocate the memory allocated */

    /* Do not allow the creation of L3IpVlan interface type for L2 switches */

    if ((i4TestValIfMainType == CFA_ENET)
#ifndef WGS_WANTED
        || (i4TestValIfMainType == CFA_L3IPVLAN)
        || (i4TestValIfMainType == CFA_L3SUB_INTF)
#endif /* WGS_WANTED */
        || (i4TestValIfMainType == CFA_LAGG) || (i4TestValIfMainType == CFA_LOOPBACK) || (i4TestValIfMainType == CFA_PIP) || (i4TestValIfMainType == CFA_L2VLAN)    /* Openflow Vlan */
        || (i4TestValIfMainType == CFA_ILAN)
        || (i4TestValIfMainType == CFA_BRIDGED_INTERFACE)
        || (i4TestValIfMainType == CFA_PROP_VIRTUAL_INTERFACE)
#if ((defined (WLC_WANTED)) || (defined (WTP_WANTED)))
        || (i4TestValIfMainType == CFA_RADIO)
        || (i4TestValIfMainType == CFA_WLAN_RADIO)
        || (i4TestValIfMainType == CFA_CAPWAP_VIRT_RADIO)
        || (i4TestValIfMainType == CFA_CAPWAP_DOT11_PROFILE)
        || (i4TestValIfMainType == CFA_CAPWAP_DOT11_BSS)
#endif
        )
    {
        if (u4IfIndex > CFA_MAX_INTERFACES ())
        {
            CfaIfmDeleteIfEntry (u4IfIndex, CFA_INVALID_TYPE);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }

        CfaGetIfAlias (u4IfIndex, au1IfName);

        if ((STRLEN (au1IfName)) > 0)
        {
            return SNMP_SUCCESS;
        }
    }

    if ((i4TestValIfMainType == CFA_TUNNEL) ||
        (i4TestValIfMainType == CFA_MPLS) ||
        (i4TestValIfMainType == CFA_MPLS_TUNNEL) ||
        (i4TestValIfMainType == CFA_TELINK) ||
        (i4TestValIfMainType == CFA_PSEUDO_WIRE) ||
        (i4TestValIfMainType == CFA_VXLAN_NVE) ||
        (i4TestValIfMainType == CFA_TAP))
    {

        /* The interface should be within the max interface limit */
        if (u4IfIndex <= CFA_MAX_INTERFACES ())
        {
            return SNMP_SUCCESS;
        }
    }

    CfaIfmDeleteIfEntry (u4IfIndex, CFA_INVALID_TYPE);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfMainBrgPortType
 Input       :  The Indices
                IfMainIndex
 
                The Object 
                testValIfMainBrgPortType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainBrgPortType (UINT4 *pu4ErrorCode,
                            INT4 i4IfMainIndex, INT4 i4TestValIfMainBrgPortType)
{
    UINT4               u4ContextId = CFA_INVALID_CONTEXT;
#ifdef VLAN_WANTED
    INT4                i4RetVal = VLAN_FALSE;
    UINT1               u1PortAccpFrmType = 0;
#endif
    UINT4               u4IfIndex = 0;
    UINT4               u4BridgeMode = 0;
    UINT4               u4IcclIfIndex = 0;
    UINT2               u2LocalPort = 0;
    UINT2               au2ConfPorts[L2IWF_MAX_PORTS_PER_CONTEXT];
    UINT2               u2NumPorts = 0;
    UINT1               u1BrgPortType = 0;
    UINT1               u1IfType = 0;
    UINT1               u1IfSubType = 0;

    u4IfIndex = (UINT4) i4IfMainIndex;

    /* for index validation - because validation is commented in mid-level */
    if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (CfaIsSispInterface ((UINT4) i4IfMainIndex) == CFA_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* If the interface is active then only the vlan module will know. So if
     * the row status is not active then return failure. */
    if (CfaIsIfEntryProgrammingAllowed ((UINT4) i4IfMainIndex) == CFA_TRUE)
    {
        if (CFA_IF_RS (u4IfIndex) != CFA_RS_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        CFA_DS_LOCK ();
        if (CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_FALSE)
        {
            CFA_DS_UNLOCK ();
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        CFA_DS_UNLOCK ();
    }
    if (CFA_IS_BRG_PORT_TYPE_VALID (i4TestValIfMainBrgPortType) == CFA_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* If the port is not mapped to any context, then don't
     * allow setting of brg port type. */
    if (VcmGetContextInfoFromIfIndex ((UINT4) i4IfMainIndex, &u4ContextId,
                                      &u2LocalPort) != VCM_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_BRG_PORT_TYPE_MAP_ERR);
        return SNMP_FAILURE;
    }

    CfaGetIfType (u4IfIndex, &u1IfType);
    CfaGetIfSubType (u4IfIndex, &u1IfSubType);

    switch (u1IfType)
    {
        case CFA_LAGG:
            /* If the port-channel contains some member ports then don't allow
             * port type configuration for that port-channel. */
            L2IwfGetConfiguredPortsForPortChannel ((UINT2) u4IfIndex,
                                                   au2ConfPorts, &u2NumPorts);

            if (u2NumPorts != 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_CFA_BRG_PORT_TYPE_PC_ERR);
                return SNMP_FAILURE;
            }
            L2IwfGetBridgeMode (u4ContextId, &u4BridgeMode);

            if (u4BridgeMode != VLAN_CUSTOMER_BRIDGE_MODE)
            {
                /* ICCL interface should be Provider network port always */
                if (i4TestValIfMainBrgPortType != CFA_PROVIDER_NETWORK_PORT)
                {
                    CfaIcchGetIcclIfIndex (&u4IcclIfIndex);

                    if (u4IcclIfIndex == u4IfIndex)
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        CLI_SET_ERR (CLI_CFA_ICCH_NO_PNP_PORT_ERR);
                        return SNMP_FAILURE;
                    }
                }
            }
            break;

        case CFA_ENET:
            if (L2IwfIsPortInPortChannel ((UINT4) i4IfMainIndex) ==
                L2IWF_SUCCESS)
            {
                /* Port is part of port-channel, so don't allow this
                 * configuration. */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_CFA_BRG_PORT_TYPE_IN_PC_ERR);
                return SNMP_FAILURE;
            }
            break;

        case CFA_PIP:
        case CFA_BRIDGED_INTERFACE:

            if (CfaIsVipInterface ((UINT4) i4IfMainIndex) == CFA_FALSE)
            {
                if (CFA_IS_INTERNAL_INTERFACE_PORT_TYPE_VALID
                    (i4TestValIfMainBrgPortType) == CFA_FALSE)
                {
                    /* Port type is not permissible for internal interface type */
                    CLI_SET_ERR (CLI_CFA_BRG_PORT_TYPE_VIRTUAL_PORT_ERR);
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }
            else
            {
                if (i4TestValIfMainBrgPortType != CFA_VIRTUAL_INSTANCE_PORT)
                {
                    CLI_SET_ERR (CLI_CFA_BRG_PORT_TYPE_VIRTUAL_PORT_ERR);
                    /* Port type is not permissible for internal interface type */
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }
            break;

        case CFA_PROP_VIRTUAL_INTERFACE:

            L2IwfGetBridgeMode (u4ContextId, &u4BridgeMode);

            if (u4BridgeMode != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
            {
                CLI_SET_ERR (CLI_CFA_BRG_PORT_TYPE_MODE_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (u1IfSubType == CFA_SUBTYPE_AC_INTERFACE)
            {
                if ((i4TestValIfMainBrgPortType != CFA_CUSTOMER_EDGE_PORT) &&
                    (i4TestValIfMainBrgPortType != CFA_CNP_PORTBASED_PORT))
                {
                    CLI_SET_ERR (CLI_CFA_INVALID_AC_PORT_TYPE);
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_CFA_BRG_PORT_TYPE_PORT_ERR);
            return SNMP_FAILURE;
    }

    CfaGetIfBrgPortType (u4IfIndex, &u1BrgPortType);
    if (u1BrgPortType == (UINT1) i4TestValIfMainBrgPortType)
    {
        return SNMP_SUCCESS;
    }

#ifdef VLAN_WANTED

    if (!((u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
          (u1IfSubType == CFA_SUBTYPE_AC_INTERFACE)))
    {
        /* Check whether the given port type is permissible from Vlan module. */
        i4RetVal = VlanValidatePortType ((UINT4) i4IfMainIndex,
                                         (UINT1) i4TestValIfMainBrgPortType);

        if (i4RetVal == VLAN_FALSE)
        {
            if (L2IwfGetVlanPortAccpFrmType
                ((UINT4) i4IfMainIndex, &u1PortAccpFrmType) == L2IWF_FAILURE)
            {
                return SNMP_FAILURE;
            }

            /* On CNP - Port based acceptable frame types should be always
             * Admit only Untagged and priority tagged frames. */
            if ((i4TestValIfMainBrgPortType == CFA_CNP_PORTBASED_PORT) &&
                (u1PortAccpFrmType !=
                 VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES))
            {
                CLI_SET_ERR (CLI_CFA_ACCP_FRAME_TYPE_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            CLI_SET_ERR (CLI_CFA_BRG_PORT_TYPE_MODE_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (VlanApiEvbGetSystemStatus (u4ContextId) == VLAN_FALSE)
    {
        if (i4TestValIfMainBrgPortType == CFA_UPLINK_ACCESS_PORT)
        {
            CLI_SET_ERR (CLI_CFA_EVB_NOT_RUNNING_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfMainMtu
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfMainMtu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainMtu (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                    INT4 i4TestValIfMainMtu)
{
    UINT1               u1IfType;
    UINT1               u1BrgPortType;
    UINT4               u4IfIndex;
    UINT4               u4IfMtu;
    UINT4               u4IfSpeed = 0;
    UINT4               u4IfHighSpeed = 0;
    tTnlIfEntry         TnlEntry;
    tTnlIfEntry        *pTnlIfEntry = NULL;

#ifdef PPP_WANTED
    UINT4               u4CurMtu;
    UINT1               u1LlIfType;
#endif /* PPP_WANTED */

/* for index validation - because validation is commented in mid-level */
    if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    u4IfIndex = (UINT4) i4IfMainIndex;

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        CfaGetIfType (u4IfIndex, &u1IfType);

        /* the IfType must have been specified first */
        if (u1IfType == CFA_INVALID_TYPE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }

        CfaGetIfBrgPortType ((UINT4) i4IfMainIndex, &u1BrgPortType);

        if (u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
        {
            /* MTU Size Cannot be set for S-Channel Interface */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_CFA_SBP_ERR);
            return SNMP_FAILURE;
        }

        /* the interface should be admin down */
        if ((CFA_IF_ADMIN (u4IfIndex) == CFA_IF_UP))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_CFA_ADMIN_STATUS_MTU_ERR);
            return SNMP_FAILURE;
        }
        switch (u1IfType)
        {
#ifdef PPP_WANTED
            case CFA_PPP:
                /* currently we allow any MTU to be set, but it should not be greater than the
                   MTU of lower interface (if any) */
                if (CFA_IF_STACK_LOW_ENTRY (u4IfIndex) == NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

                if (CFA_IF_STACK_STATUS (CFA_IF_STACK_LOW_ENTRY (u4IfIndex))
                    == CFA_RS_ACTIVE)
                {
                    CfaGetIfMtu (CFA_IF_STACK_IFINDEX
                                 (CFA_IF_STACK_LOW_ENTRY (u4IfIndex)),
                                 &u4CurMtu);
                    if (u4CurMtu < (UINT4) i4TestValIfMainMtu)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (CLI_CFA_LL_MTU_ERR);
                        return SNMP_FAILURE;
                    }
                }

                CfaGetIfType (CFA_IF_STACK_IFINDEX
                              (CFA_IF_STACK_LOW_ENTRY (u4IfIndex)),
                              &u1LlIfType);
                if ((u1LlIfType == CFA_ENET)
                    && (i4TestValIfMainMtu > CFA_MAX_GIGA_ENET_MTU))
                {
                    /* PPP interfaces stacked over PVCs or ethernet run PPPoE.
                     * Hence the max. MTU possible is 1492 in these cases. */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_CFA_PPPOE_MTU_ERR);
                    return SNMP_FAILURE;
                }

                break;
#endif /* PPP_WANTED */

            case CFA_PROP_VIRTUAL_INTERFACE:
            case CFA_ENET:
#ifdef LA_WANTED
                if (LaIsPortInPortChannel (u4IfIndex) == CFA_SUCCESS)
                {
                    CLI_SET_ERR (CLI_CFA_PORT_IN_PORTCHANNEL);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
#endif

                CfaDetermineSpeed (u4IfIndex, &u4IfSpeed, &u4IfHighSpeed);
                if (u4IfSpeed < CFA_ENET_SPEED_1G)
                {                /* for fast ethernet maximum value allowed is 1522 */
                    if (i4TestValIfMainMtu > CFA_MAX_FAST_ENET_MTU)
                    {
                        CLI_SET_ERR (CLI_CFA_SIZE_MTU_ERR);
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return (SNMP_FAILURE);
                    }
                }
                /* Jumbo is to be supported only for more than 1G */
                /* MTU size should be divisible by 4. should that check be included? */
                else
                {
                    if ((i4TestValIfMainMtu < CFA_MIN_MTU_SIZE)
                        || (i4TestValIfMainMtu > CFA_MAX_GIGA_ENET_MTU))
                    {
                        CLI_SET_ERR (CLI_CFA_SIZE_MTU_ERR);
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }
                }
                break;
            case CFA_L3IPVLAN:
            case CFA_IFPWTYPE:

            case CFA_LAGG:

                if ((i4TestValIfMainMtu < CFA_MIN_MTU_SIZE)
                    || (i4TestValIfMainMtu > CFA_MAX_GIGA_ENET_MTU))
                {
                    CLI_SET_ERR (CLI_CFA_SIZE_MTU_ERR);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                break;

            case CFA_TUNNEL:
                pTnlIfEntry = &TnlEntry;
                if (CfaFindTnlEntryFromIfIndex
                    ((UINT4) i4IfMainIndex, pTnlIfEntry) == CFA_FAILURE)
                {
                    pTnlIfEntry = NULL;
                }

                if (pTnlIfEntry == NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

                CfaGetIfMtu (pTnlIfEntry->u4PhyIfIndex, &u4IfMtu);

                /* MTU val of 0 can be allowed for tunnel interface type.
                 * when set to 0, it should use the outgoing link MTU */
                if ((i4TestValIfMainMtu < 0) ||
                    ((UINT4) i4TestValIfMainMtu > (u4IfMtu - IP_HDR_LEN)))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                break;
            case CFA_LOOPBACK:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_CFA_LOOPBACK_MTU_ERR);
                return SNMP_FAILURE;
#if ((defined (WLC_WANTED)) || (defined (WTP_WANTED)))
            case CFA_WLAN_RADIO:
            case CFA_CAPWAP_VIRT_RADIO:
            case CFA_CAPWAP_DOT11_PROFILE:
            case CFA_CAPWAP_DOT11_BSS:
                if ((i4IfMainIndex < CFA_MIN_WSS_IF_INDEX) ||
                    (i4IfMainIndex > CFA_MAX_WSS_IF_INDEX))
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    return SNMP_FAILURE;
                }
                break;
#endif
            case CFA_HDLC:
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;

        }

        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhTestv2IfMainAdminStatus
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfMainAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainAdminStatus (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                            INT4 i4TestValIfMainAdminStatus)
{
#ifdef L3_SWITCHING_WANTED
#if defined  (IP6_WANTED) && defined  (TUNNEL_WANTED)
    tTnlIfEntry         TnlIfEntry;
    UINT4               u4Addr = 0;
#endif
#endif
    UINT1               u1IfType;
    UINT4               u4IfIndex;
    UINT4               u4IcchIfIndex = 0;
/*No configurations should be allowed for stack interface -vlan 4094 */
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1StackIfName[CFA_MAX_PORT_NAME_LENGTH];
#ifdef MPLS_WANTED
#ifdef HVPLS_WANTED
    UINT1               u1AppOwner = CFA_IF_APP_NONE;
#endif
#endif

#ifdef L3_SWITCHING_WANTED
#if defined  (IP6_WANTED) && defined  (TUNNEL_WANTED)
    MEMSET (&TnlIfEntry, 0, sizeof (tTnlIfEntry));
#endif
#endif
    u4IfIndex = (UINT4) i4IfMainIndex;

/* for index validation - because validation is commented in mid-level */
    if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_CFA_ADMIN_STATUS_SET_ERR);
        return SNMP_FAILURE;
    }

    CfaGetIfType (u4IfIndex, &u1IfType);
/* the IfType must have been specified first */
    if (u1IfType == CFA_INVALID_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_CFA_ADMIN_STATUS_SET_ERR);
        return SNMP_FAILURE;
    }

#ifdef MPLS_WANTED
#ifdef HVPLS_WANTED
    if ((u1IfType == CFA_IFPWTYPE)
        && (i4TestValIfMainAdminStatus == CFA_IF_DOWN))
    {
        u1AppOwner = L2VpPortGetPortOwnerFromIndex ((UINT4) i4IfMainIndex);
        if (u1AppOwner == CFA_IF_APP_ERPS)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CFA_CLI_PWIF_APP_ERPS);
            return SNMP_FAILURE;
        }
    }
#endif
#endif

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_ADMIN_STATUS_SET_ERR);
        return SNMP_FAILURE;
    }

    /* User is not allowed to set Admin status for ICCH interface */
    CfaIcchGetIcclIfIndex (&u4IcchIfIndex);
    if ((u4IfIndex == u4IcchIfIndex)
        && (i4TestValIfMainAdminStatus == CFA_IF_DOWN))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_ICCL_ADMIN_STATUS_SET_ERR);
        return SNMP_FAILURE;
    }

/* the RowStatus should be active first */
    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        CFA_DS_LOCK ();
        if (CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_FALSE)
        {
            CFA_DS_UNLOCK ();
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_CFA_ADMIN_STATUS_SET_ERR);
            return SNMP_FAILURE;
        }
        CFA_DS_UNLOCK ();
    }
    else if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        MEMSET (au1StackIfName, 0, CFA_MAX_PORT_NAME_LENGTH);

        SPRINTF ((CHR1 *) au1StackIfName, "%s%u",
                 CFA_DEFAULT_VLAN_INTERFACE, CFA_DEFAULT_STACK_VLAN_ID);

        CfaGetIfName (u4IfIndex, au1IfName);
        if (STRCMP (au1IfName, au1StackIfName) == 0)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_CFA_ADMIN_STATUS_SET_ERR);
            return SNMP_FAILURE;
        }
    }

    else
    {
        if (CFA_IF_RS (u4IfIndex) != CFA_RS_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_CFA_ADMIN_STATUS_SET_ERR);
            return SNMP_FAILURE;
        }
    }

    if (i4TestValIfMainAdminStatus == CFA_LOOPBACK_LOCAL)
    {
        if (CFA_CDB_IF_ADMIN_STATUS (u4IfIndex) != CFA_IF_UP)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_CFA_LOOPBACK_LOCAL_ERR);
            return SNMP_FAILURE;
        }
    }
    if (i4TestValIfMainAdminStatus == CFA_NO_LOOPBACK_LOCAL)
    {
        if (CFA_CDB_IF_ADMIN_STATUS (u4IfIndex) != CFA_LOOPBACK_LOCAL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_CFA_NO_LOOPBACK_LOCAL_ERR);
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValIfMainAdminStatus == CFA_IF_UP) ||
        (i4TestValIfMainAdminStatus == CFA_IF_TEST) ||
        (i4TestValIfMainAdminStatus == CFA_IF_DOWN) ||
        (i4TestValIfMainAdminStatus == CFA_LOOPBACK_LOCAL) ||
        (i4TestValIfMainAdminStatus == CFA_NO_LOOPBACK_LOCAL))
    {
        /* Firewall Module Status check before turning a WAN port up */
        if (i4TestValIfMainAdminStatus == CFA_IF_UP)
        {
            if (CfaUtilCallBack (CFA_CUST_FWL_CHECK_EVENT, u4IfIndex)
                == CFA_FAILURE)
            {
#ifdef FIREWALL_WANTED
                FwlLogMessage (FWL_FWL_MOD_STATUS_CHECK_FAIL,
                               0, NULL, FWL_LOG_MUST, FWLLOG_ALERT_LEVEL,
                               (UINT1 *) FWL_MSG_FWL_MOD_STATUS_CHECK_FAIL);
#endif
                *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
                CLI_SET_ERR (CLI_CFA_ADMIN_STATUS_SET_ERR);
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_CFA_ADMIN_STATUS_SET_ERR);
        return SNMP_FAILURE;
    }
#ifdef L3_SWITCHING_WANTED
#if defined  (IP6_WANTED) && defined  (TUNNEL_WANTED)
    if (u1IfType == CFA_TUNNEL)
    {
        if (CfaGetTnlEntryFromIfIndex (u4IfIndex, &TnlIfEntry) == CFA_SUCCESS)
        {
            if ((TnlIfEntry.u4EncapsMethod == TNL_TYPE_SIXTOFOUR) ||
                (TnlIfEntry.u4EncapsMethod == TNL_TYPE_ISATAP) ||
#ifdef OPENLFOW_WANTED
                (TnlIfEntry.u4EncapsMethod == TNL_TYPE_OPENFLOW) ||
#endif /* OPENFLOW_WANTED */
                (TnlIfEntry.u4EncapsMethod == TNL_TYPE_COMPAT))
            {
                if (IpGetIpv4AddrFromArpTable
                    ((INT4) TnlIfEntry.u4PhyIfIndex, &u4Addr) == IP_FAILURE)
                {
                    CLI_SET_ERR (CLI_CFA_NO_ARP_TUNNEL_ERR);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }
            }
        }
    }
#endif
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfMainEncapType
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfMainEncapType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainEncapType (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                          INT4 i4TestValIfMainEncapType)
{
    UINT4               u4IfIndex;
    UINT1               u1IfType = CFA_NONE;

    u4IfIndex = (UINT4) i4IfMainIndex;

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        /* for index validation - because validation is commented in mid-level */
        if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }

        CfaGetIfType (u4IfIndex, &u1IfType);

        /* the IfType must have been specified first */
        if (u1IfType == CFA_INVALID_TYPE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }

        switch (u1IfType)
        {

            case CFA_ENET:
                if ((i4TestValIfMainEncapType == CFA_ENCAP_OTHER) ||
                    (i4TestValIfMainEncapType == CFA_ENCAP_ENETV2) ||
                    (i4TestValIfMainEncapType == CFA_ENCAP_LLC_SNAP))
                {
                    return SNMP_SUCCESS;
                }
                break;

            case CFA_L3IPVLAN:
                if ((i4TestValIfMainEncapType == CFA_ENCAP_OTHER) ||
                    (i4TestValIfMainEncapType == CFA_ENCAP_ENETV2))
                {
                    return SNMP_SUCCESS;
                }
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;

        }

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2IfMainRowStatus
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfMainRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                          INT4 i4TestValIfMainRowStatus)
{
    UINT4               u4IfIndex;
    UINT4               u4IcclIfIndex = 0;
#ifdef MPLS_WANTED
    UINT4               u4PwIndex = 0;
#endif
    UINT1               u1RowStatus;
    UINT1               u1OldRowStatus;
    UINT1               u1IfType;
    UINT1               u1InterfaceExists = CFA_FALSE;
    UINT1               u1InterfaceUsable = CFA_FALSE;
    UINT1               u1BridgeIface = CFA_DISABLED;
    tTMO_SLL           *pIfStack = NULL;
    tStackInfoStruct   *pScanNode = NULL;

    u4IfIndex = (UINT4) i4IfMainIndex;
    u1RowStatus = (UINT1) i4TestValIfMainRowStatus;
    CfaGetIfType (u4IfIndex, &u1IfType);
    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgeIface);

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    /* Check whether any Sub-Interfaces are associated with Current Interface */
    if (CfaIsParentPort (u4IfIndex) == CFA_TRUE)
    {
        if (u1IfType == CFA_LAGG)
        {
            CLI_SET_ERR (CLI_CFA_L3SUB_LAG_IF_ERR);
        }
        else
        {
            CLI_SET_ERR (CLI_CFA_L3SUB_IF_PRESENT);
        }
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

#ifdef MPLS_WANTED
    if (u1IfType == CFA_PSEUDO_WIRE)
    {
        if (L2VpnApiGetPwIndexFromPwIfIndex (u4IfIndex, &u4PwIndex) !=
            OSIX_SUCCESS)
        {
            CLI_SET_ERR (CFA_CLI_PW_MAPPED_PWIF_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
#endif
    /* Not allow to delete the downlink port which is the only one downlink */
    /* member in the UFD group and also group is having uplink ports. */
    if (u1IfType == CFA_LAGG || u1IfType == CFA_ENET)
    {
        if (CFA_UFD_MODULE_STATUS () == CFA_UFD_ENABLED)
        {
            if (CfaUfdCheckLastDownlinkInGroup (u4IfIndex) != OSIX_FALSE)
            {
                CLI_SET_ERR (CLI_CFA_UFD_LAST_DOWNLINK_REM_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }

    if (u1IfType == CFA_ENET)
    {
        if (CfaIsParentPort (u4IfIndex) == CFA_TRUE)
        {
            CLI_SET_ERR (CLI_CFA_L3SUB_IF_PRESENT);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        }
    }

    if (VcmIsL3Interface (u4IfIndex) == VCM_FALSE)
    {
        /* Restrict the deletion of port if mapped to any context */
        if ((L2IwfIsPortMappedToContext (u4IfIndex) == L2IWF_SUCCESS)
            && (u1RowStatus == CFA_RS_DESTROY) &&
            (!((u1IfType == CFA_LAGG) && (u1BridgeIface == CFA_DISABLED))) &&
            (u1IfType != CFA_VXLAN_NVE))
        {
            CLI_SET_ERR (CFA_CLI_PORT_MAPPED_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
    }

    /* Block configuration on L3 ICCL Vlan */
    if (u4IfIndex == (UINT4) CfaIcchApiGetIcclVlanId (CFA_ICCH_DEFAULT_INST))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_PORT_ICCL_VLAN_ERR);
        return SNMP_FAILURE;
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

/* no change can be allowed for the default router interface */
/* No change is allowed for Mgmt interface in case of L2 switches */

    if (((CFA_MGMT_PORT == TRUE) && (u4IfIndex == CFA_OOB_MGMT_IFINDEX))
#ifdef WGS_WANTED
        || (u1IfType == CFA_L2VLAN)
#endif /* WGS_WANTED */
        )
    {
        CLI_SET_ERR (CFA_CLI_DEFAULT_PORT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (((CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE) &&
         (CFA_IF_ENTRY_USED (u4IfIndex))) ||
        ((CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE) &&
         (CfaValidateCfaIfIndex (u4IfIndex) == CFA_SUCCESS)))
    {
        u1InterfaceExists = CFA_TRUE;
        if (CfaGetIfRowStatus (u4IfIndex, &u1OldRowStatus) == CFA_SUCCESS)
        {
            if (u1OldRowStatus != CFA_RS_NOTREADY)
            {
                u1InterfaceUsable = CFA_TRUE;
            }

        }
    }
    else
    {
        if ((u1RowStatus != CFA_RS_CREATEANDWAIT)
            && (u1RowStatus != CFA_RS_CREATEANDGO))
        {

            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    /* check for the RowStatus Validity */
    switch (u1RowStatus)
    {

        case CFA_RS_CREATEANDWAIT:
        case CFA_RS_CREATEANDGO:
            if (CfaValidateCfaIfIndex (u4IfIndex) == CFA_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
            {
                if (u1InterfaceExists == CFA_FALSE)
                {
                    return SNMP_SUCCESS;
                }
            }
            else
            {
                return SNMP_SUCCESS;
            }
            break;

        case CFA_RS_ACTIVE:
            if ((u1InterfaceExists == CFA_TRUE) &&
                (u1InterfaceUsable == CFA_TRUE))
            {
                return SNMP_SUCCESS;
            }
            break;

        case CFA_RS_DESTROY:
#ifdef VRRP_WANTED
            if (((u1IfType == CFA_L3IPVLAN) ||
                 ((u1IfType == CFA_ENET) && (u1BridgeIface == CFA_DISABLED))))
            {
                if (VrrpApiValIfTrackIfExists ((INT4) u4IfIndex) == VRRP_OK)
                {
                    CLI_SET_ERR (CLI_CFA_NO_DEL_VRRP_TRACK_INUSE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
#endif

#if ((defined (WLC_WANTED)) || (defined (WTP_WANTED)))
            if (u1IfType == CFA_RADIO)
            {
                CLI_SET_ERR (CFA_CLI_DELETE_PORT);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
#endif

            if ((u1IfType == CFA_ENET) && !(CFA_ENET_CREATE_DELETE_ALLOWED))
            {
                CLI_SET_ERR (CFA_CLI_DELETE_PORT);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
#ifdef IP6_WANTED
            if ((Ip6UtlGetUnnumIfEntry (u4IfIndex)) == OSIX_FAILURE)
            {
                CLI_SET_ERR (CLI_CFA_DEL_UNNUMINTERFACE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;

            }
#endif
            /* Check with L2IWF whether the portchannel can be deleted */
            if (u1IfType == CFA_LAGG)
            {
                if (L2IwfIsPortChannelConfigAllowed (u4IfIndex) == OSIX_FALSE)
                {
                    CLI_SET_ERR (CFA_CLI_DEL_PORTCHANNEL);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            CfaIcchGetIcclIfIndex (&u4IcclIfIndex);
            if (u4IcclIfIndex == u4IfIndex)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_CFA_PORT_ICCL_INT_NO_DEL_ERR);
                return SNMP_FAILURE;
            }
            CfaApiIcchGetIcclL3IfIndex (&u4IcclIfIndex);
            if (u4IcclIfIndex == u4IfIndex)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_CFA_PORT_ICCL_IVR_NO_DEL_ERR);
                return SNMP_FAILURE;
            }

            /*
             * check if any higher layer node exist for this 
             * index. if so check its admin status. If it is up
             * dont allow to delete 
             */
            if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE)
            {
                if ((pIfStack = &CFA_IF_STACK_HIGH (u4IfIndex)) != NULL)
                {
                    if (pIfStack->u4_Count > 0)
                    {
                        TMO_SLL_Scan (pIfStack, pScanNode, tStackInfoStruct *)
                        {
                            if (pScanNode->u4IfIndex == CFA_NONE)
                            {
                                continue;
                            }
                            if (CFA_IF_ENTRY (pScanNode->u4IfIndex) == NULL)
                            {
                                continue;
                            }
                            if ((pScanNode->u4IfIndex != u4IfIndex) &&
                                (CFA_IF_ADMIN (pScanNode->u4IfIndex) ==
                                 CFA_IF_UP))
                            {
#ifdef PPP_WANTED
                                if (CFA_CDB_IF_TYPE (pScanNode->u4IfIndex) ==
                                    CFA_PPP)
                                {
                                    CLI_SET_ERR
                                        (CLI_CFA_PPP_LL_ALREADY_STACKED_ERR);
                                }
#endif
                                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                                CLI_SET_ERR (CLI_CFA_INTERFACE_STACK_ERR);
                                return SNMP_FAILURE;
                            }
                        }        /* tmp_sll_scan */

                    }            /* pIfStack->u4Count > 0 */
                }                /* pIfStack != NULL */

                if (u1InterfaceExists == CFA_TRUE)
                {
                    return SNMP_SUCCESS;
                }
            }
            else
            {
                return SNMP_SUCCESS;
            }

            break;

        case CFA_RS_NOTINSERVICE:
#ifdef PPP_WANTED
            if (u1IfType == CFA_PPP)
            {
                if (CFA_IF_ADMIN (u4IfIndex) == CFA_IF_UP)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_CFA_PPP_ADMIN_STATUS_ERR);
                    return SNMP_FAILURE;
                }
            }
#endif

            if (u1InterfaceExists == CFA_TRUE)
            {
                if (u1InterfaceUsable == CFA_TRUE)
                {
                    return SNMP_SUCCESS;
                }
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;

    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfMainNetworkType
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfMainNetworkType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainNetworkType (UINT4 *pu4ErrorCode,
                            INT4 i4IfMainIndex, INT4 i4TestValIfMainNetworkType)
{
    UINT2               u2IfIndex;
    UINT1               u1IfType;
    UINT1               u1BrgPortType = 0;

    CfaGetIfBrgPortType ((UINT4) i4IfMainIndex, &u1BrgPortType);
    if (u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        /* Setting the Network type is not applicable for
         * S-Channel Interface */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_SBP_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValIfMainNetworkType != CFA_NETWORK_TYPE_LAN) &&
        (i4TestValIfMainNetworkType != CFA_NETWORK_TYPE_WAN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* for index validation - because validation is commented in mid-level */
    if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    u2IfIndex = (UINT2) i4IfMainIndex;

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    /* the IfType must have been specified first */
    if (u1IfType == CFA_INVALID_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* the interface should be admin down */
    if ((CFA_IF_ADMIN (u2IfIndex) == CFA_IF_UP))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_ADMIN_STATUS_ERR);
        return SNMP_FAILURE;
    }
    if ((u1IfType != CFA_ENET) && (u1IfType != CFA_L3IPVLAN)
        && (u1IfType != CFA_LAGG) && (u1IfType != CFA_PPP))
    {
        /* Can be set only for ethernet/L3 vlan interfaces */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfMainWanType
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfMainWanType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainWanType (UINT4 *pu4ErrorCode,
                        INT4 i4IfMainIndex, INT4 i4TestValIfMainWanType)
{
    UINT2               u2IfIndex;
    UINT1               u1IfType;
    UINT1               u1IfNwType;

    /* WAN Type cannot be set as OTHER */
    if ((i4TestValIfMainWanType != CFA_WAN_TYPE_PRIVATE) &&
        (i4TestValIfMainWanType != CFA_WAN_TYPE_PUBLIC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* for index validation - because validation is commented in mid-level */
    if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    u2IfIndex = (UINT2) i4IfMainIndex;

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    /* the IfType must have been specified first */
    if (u1IfType == CFA_INVALID_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* the interface should be admin down */
    if ((CFA_IF_ADMIN (u2IfIndex) == CFA_IF_UP))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_ADMIN_STATUS_ERR);
        return SNMP_FAILURE;
    }

    /* Getting the type of interface */
    if (CfaGetIfNwType (u2IfIndex, &u1IfNwType) == CFA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (u1IfNwType != CFA_NETWORK_TYPE_WAN)
    {
        /* Can be set only for WAN interfaces */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_LAN_ERR);
        return SNMP_FAILURE;
    }
    if ((u1IfType != CFA_PPP) && (u1IfType != CFA_ENET))
    {
        /* Can be set only for PPP/MP or ethernet WAN interfaces */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IfMainTable
 Input       :  The Indices
                IfMainIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfMainTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IfIpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIfIpTable
 Input       :  The Indices
                IfMainIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIfIpTable (INT4 i4IfMainIndex)
{
    tIpConfigInfo       IpIfInfo;

    if (CfaIpIfGetIfInfo ((UINT4) i4IfMainIndex, &IpIfInfo) == CFA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIfIpTable
 Input       :  The Indices
                IfMainIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIfIpTable (INT4 *pi4IfMainIndex)
{
    UINT4               u4NextIndex;
    if (CfaIpIfGetNextIndexIpIfTable (0, &u4NextIndex) == CFA_SUCCESS)
    {
        *pi4IfMainIndex = (INT4) u4NextIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIfIpTable
 Input       :  The Indices
                IfMainIndex
                nextIfMainIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIfIpTable (INT4 i4IfMainIndex, INT4 *pi4NextIfMainIndex)
{
    UINT4               u4NextIndex;
    if (CfaIpIfGetNextIndexIpIfTable ((UINT4) i4IfMainIndex,
                                      &u4NextIndex) == CFA_SUCCESS)
    {
        *pi4NextIfMainIndex = (INT4) u4NextIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIfIpAddrAllocMethod
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfIpAddrAllocMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfIpAddrAllocMethod (INT4 i4IfMainIndex,
                           INT4 *pi4RetValIfIpAddrAllocMethod)
{
    UINT1               u1AllocMethod;

    if (CfaIfGetIpAllocMethod ((UINT4) i4IfMainIndex,
                               &u1AllocMethod) == CFA_SUCCESS)
    {
        *pi4RetValIfIpAddrAllocMethod = (INT4) u1AllocMethod;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfIpAddr
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfIpAddr (INT4 i4IfMainIndex, UINT4 *pu4RetValIfIpAddr)
{
    tIpConfigInfo       IpIfInfo;

    if (CfaIpIfGetIfInfo ((UINT4) i4IfMainIndex, &IpIfInfo) == CFA_SUCCESS)
    {
        *pu4RetValIfIpAddr = IpIfInfo.u4Addr;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfIpSubnetMask
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfIpSubnetMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfIpSubnetMask (INT4 i4IfMainIndex, UINT4 *pu4RetValIfIpSubnetMask)
{
    tIpConfigInfo       IpIfInfo;

    if (CfaIpIfGetIfInfo ((UINT4) i4IfMainIndex, &IpIfInfo) == CFA_SUCCESS)
    {
        *pu4RetValIfIpSubnetMask = IpIfInfo.u4NetMask;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfIpBroadcastAddr
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfIpBroadcastAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfIpBroadcastAddr (INT4 i4IfMainIndex, UINT4 *pu4RetValIfIpBroadcastAddr)
{
    tIpConfigInfo       IpIfInfo;

    if (CfaIpIfGetIfInfo ((UINT4) i4IfMainIndex, &IpIfInfo) == CFA_SUCCESS)
    {
        *pu4RetValIfIpBroadcastAddr = IpIfInfo.u4BcastAddr;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfIpForwardingEnable
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfIpForwardingEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfIpForwardingEnable (INT4 i4IfMainIndex,
                            INT4 *pi4RetValIfIpForwardingEnable)
{
    tIpConfigInfo       IpIfInfo;

    if (CfaIpIfGetIfInfo ((UINT4) i4IfMainIndex, &IpIfInfo) == CFA_SUCCESS)
    {
        *pi4RetValIfIpForwardingEnable = (INT4) IpIfInfo.u1IpForwardEnable;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfIpAddrAllocProtocol
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfIpAddrAllocProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfIpAddrAllocProtocol (INT4 i4IfMainIndex,
                             INT4 *pi4RetValIfIpAddrAllocProtocol)
{
    UINT1               u1AllocProto = 0;

    if (CfaIfGetIpAllocProto ((UINT4) i4IfMainIndex,
                              &u1AllocProto) == CFA_SUCCESS)
    {
        *pi4RetValIfIpAddrAllocProtocol = (INT4) u1AllocProto;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIfIpAddrAllocMethod
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfIpAddrAllocMethod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfIpAddrAllocMethod (INT4 i4IfMainIndex, INT4 i4SetValIfIpAddrAllocMethod)
{
    UINT4               u4IfIndex;
    UINT1               u1IsAlreadyRegWithIp = CFA_FALSE;
    UINT4               u4Flag = 0;
    UINT1               u1PrevAddrAllocMethod = 0;
    tIpConfigInfo       IpConfigInfo;
    tSnmpNotifyInfo     SnmpNotifyIpAddrInfo;
    tSnmpNotifyInfo     SnmpNotifyNetMaskInfo;
    tSnmpNotifyInfo     SnmpNotifyBcastAddrInfo;

    u4IfIndex = (UINT4) i4IfMainIndex;
    /* Null check has to be provided here to avoid crash as MSR does not call
     * Test routine */

    if (CfaValidateCfaIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        if (CFA_IF_ENTRY (u4IfIndex) == NULL)
        {
            return SNMP_FAILURE;
        }
        /* register/update with IP only if RS is active */
        if (CFA_IF_RS (u4IfIndex) == CFA_RS_ACTIVE)
        {
            if (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX)
            {
                u1IsAlreadyRegWithIp = CFA_TRUE;
            }
        }

        if (CfaIpIfGetIfInfo (u4IfIndex, &IpConfigInfo) == CFA_SUCCESS)
        {
            if (i4SetValIfIpAddrAllocMethod ==
                (INT1) IpConfigInfo.u1AddrAllocMethod)
            {
                return SNMP_SUCCESS;
            }
        }
        u1PrevAddrAllocMethod = IpConfigInfo.u1AddrAllocMethod;
#ifdef DHCPC_WANTED
        /* Inform AllocMethod Change to DHCP */
        DhcpUtilHandleAlloMethodChange (i4IfMainIndex,
                                        IpConfigInfo.u1AddrAllocMethod,
                                        i4SetValIfIpAddrAllocMethod,
                                        IpConfigInfo.u1AddrAllocProto);
#endif /* DHCPC_WANTED */
        u4Flag = CFA_IP_IF_PRIMARY_ADDR | CFA_IP_IF_NETMASK |
            CFA_IP_IF_ALLOC_METHOD;
        MEMSET (&IpConfigInfo, 0, sizeof (tIpConfigInfo));

        IpConfigInfo.u1AddrAllocMethod = (UINT1) i4SetValIfIpAddrAllocMethod;
        IpConfigInfo.u4Addr = 0;
        IpConfigInfo.u4NetMask = 0;

        if (CfaIpIfSetIfInfo ((UINT4) i4IfMainIndex, u4Flag,
                              &IpConfigInfo) != CFA_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        SnmpNotifyIpAddrInfo.pu4ObjectId = IfIpAddr;
        SnmpNotifyIpAddrInfo.u4OidLen = sizeof (IfIpAddr) / sizeof (UINT4);
        SnmpNotifyIpAddrInfo.u4SeqNum = 0;
        SnmpNotifyIpAddrInfo.u1RowStatus = FALSE;
        SnmpNotifyIpAddrInfo.pLockPointer = NULL;
        SnmpNotifyIpAddrInfo.pUnLockPointer = NULL;
        SnmpNotifyIpAddrInfo.u4Indices = 1;
        SnmpNotifyIpAddrInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyIpAddrInfo, "%i %p", i4IfMainIndex, 0));

        SnmpNotifyNetMaskInfo.pu4ObjectId = IfIpSubnetMask;
        SnmpNotifyNetMaskInfo.u4OidLen =
            sizeof (IfIpSubnetMask) / sizeof (UINT4);
        SnmpNotifyNetMaskInfo.u4SeqNum = 0;
        SnmpNotifyNetMaskInfo.u1RowStatus = FALSE;
        SnmpNotifyNetMaskInfo.pLockPointer = NULL;
        SnmpNotifyNetMaskInfo.pUnLockPointer = NULL;
        SnmpNotifyNetMaskInfo.u4Indices = 1;
        SnmpNotifyNetMaskInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyNetMaskInfo, "%i %p", i4IfMainIndex, 0));

        SnmpNotifyBcastAddrInfo.pu4ObjectId = IfIpBroadcastAddr;
        SnmpNotifyBcastAddrInfo.u4OidLen =
            sizeof (IfIpBroadcastAddr) / sizeof (UINT4);
        SnmpNotifyBcastAddrInfo.u4SeqNum = 0;
        SnmpNotifyBcastAddrInfo.u1RowStatus = FALSE;
        SnmpNotifyBcastAddrInfo.pLockPointer = NULL;
        SnmpNotifyBcastAddrInfo.pUnLockPointer = NULL;
        SnmpNotifyBcastAddrInfo.u4Indices = 1;
        SnmpNotifyBcastAddrInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyBcastAddrInfo, "%i %p", i4IfMainIndex,
                          0xffffffff));

        if (u1PrevAddrAllocMethod != CFA_IP_ALLOC_POOL)
        {
            if (u1IsAlreadyRegWithIp == CFA_TRUE)
            {
                if (CfaIfmConfigNetworkInterface (CFA_NET_IF_UPD, u4IfIndex,
                                                  0, NULL) != CFA_SUCCESS)
                {
                    CLI_SET_ERR (CFA_CLI_IFM_CONFIG_ERR);
                    return SNMP_FAILURE;
                }
            }
        }
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhSetIfIpAddr
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfIpAddr (INT4 i4IfMainIndex, UINT4 u4SetValIfIpAddr)
{
    UINT4               u4IfIndex;
    UINT1               u1IfType = 0;
    UINT1               u1IsActive = CFA_FALSE;
    UINT1               u1IsAlreadyRegWithIp = CFA_FALSE;
    tIpConfigInfo       IpIfInfo;
    UINT4               u4Flag = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4IpAddr = 0;
    INT4                i4AdminStatus = 0;
    UINT1               u1ChangeInStat = CFA_FALSE;
    INT1                i1RetVal = SNMP_FAILURE;
#ifdef PPP_WANTED
    tIfLayerInfo        LowInterface;
    tIfLayerInfo        HighInterface;
    tPppIpInfo          IpInfo;
    UINT4               u4CurMtu = 0;
#endif /* PPP_WANTED */

    u4IfIndex = (UINT4) i4IfMainIndex;
    /* Null check has to be provided here to avoid crash as MSR does not call
     * Test routine */

    if (CfaValidateCfaIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        if (CFA_IF_ENTRY (u4IfIndex) == NULL)
        {
            return SNMP_FAILURE;
        }
        /* register/update with IP only if RS is active */
        if (CFA_IF_RS (u4IfIndex) == CFA_RS_ACTIVE)
        {
            u1IsActive = CFA_TRUE;
            if (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX)
            {
                u1IsAlreadyRegWithIp = CFA_TRUE;
            }
        }

        if (CfaIpIfGetIfInfo (u4IfIndex, &IpIfInfo) == CFA_FAILURE)
        {
            return SNMP_FAILURE;
        }

        if (u4SetValIfIpAddr == IpIfInfo.u4Addr)
        {
            return SNMP_SUCCESS;
        }
        u4IpAddr = IpIfInfo.u4Addr;
        /*Get Admin Status */
        i4AdminStatus = (INT4) CFA_IF_ADMIN (u4IfIndex);

        if (i4AdminStatus != CFA_IF_DOWN)
        {
            /*Set Admin Status as DOWN */
            u1ChangeInStat = CFA_TRUE;
            CfaSetIfIpv4AddressChange (u4IfIndex, CFA_TRUE);
            CfaSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_DOWN);
        }

        CfaGetIfType (u4IfIndex, &u1IfType);

        switch (u1IfType)
        {
            case CFA_OTHER:
            case CFA_ENET:
            case CFA_LAGG:
            case CFA_LOOPBACK:
#ifdef VPN_WANTED
#ifdef IKE_WANTED
            case CFA_VPNC:
#endif /* IKE_WANTED */
#endif /* VPN_WANTED */
            case CFA_L3SUB_INTF:
            case CFA_L3IPVLAN:
            case CFA_PSEUDO_WIRE:
#ifdef VXLAN_WANTED
            case CFA_VXLAN_NVE:
#endif
#ifdef WGS_WANTED
            case CFA_L2VLAN:
#endif /* WGS_WANTED */
#ifdef PPP_WANTED
            case CFA_PPP:
#endif /* PPP_WANTED */

                u4Flag = CFA_IP_IF_PRIMARY_ADDR;

                MEMSET (&IpIfInfo, 0, sizeof (tIpConfigInfo));
                IpIfInfo.u4Addr = u4SetValIfIpAddr;
                if (CfaIpIfSetIfInfo (u4IfIndex, u4Flag,
                                      &IpIfInfo) == CFA_FAILURE)
                {
                    break;
                }
                if (u1IsActive == CFA_TRUE)
                {
                    if (u1IsAlreadyRegWithIp == CFA_TRUE)
                    {
#ifdef PPP_WANTED
                        if (u1IfType == CFA_PPP)
                        {
                            LowInterface.u1Command = HighInterface.u1Command =
                                CFA_LAYER_IGNORE;
                            IpInfo.u4LocalIpAddr = IpIfInfo.u4Addr;
                            IpInfo.u4PeerIpAddr =
                                CFA_IF_PEER_IPADDR (u4IfIndex);
                            IpInfo.u1SubnetMask = CFA_IF_IPSUBNET (u4IfIndex);

                            if (CfaIpIfValidateIfIpSubnet (u4IfIndex,
                                                           IpIfInfo.u4Addr,
                                                           IpInfo.u1SubnetMask)
                                != CFA_FAILURE)
                            {

                                CfaGetIfMtu (u4IfIndex, &u4CurMtu);
                                if (CfaIfmUpdatePppInterface (u4IfIndex,
                                                              &LowInterface,
                                                              &HighInterface,
                                                              &IpInfo,
                                                              CFA_IF_IDLE_TIMEOUT
                                                              (u4IfIndex),
                                                              u4CurMtu)
                                    != CFA_SUCCESS)
                                {
                                    CLI_SET_ERR (CFA_CLI_IFM_CONFIG_ERR);
                                    break;
                                }
                            }
                        }
#endif
                        if (CfaIfmConfigNetworkInterface
                            (CFA_NET_IF_UPD, u4IfIndex, 0, NULL) != CFA_SUCCESS)
                        {
                            MEMSET (&IpIfInfo, 0, sizeof (tIpConfigInfo));
                            IpIfInfo.u4Addr = u4IpAddr;

                            /* If the CfaIfmConfigNetworkInterface fails revert
                             *  the IP Address */
                            if (CfaIpIfSetIfInfo (u4IfIndex, u4Flag,
                                                  &IpIfInfo) == CFA_FAILURE)
                            {

                                CFA_DBG (CFA_TRC_ALL_TRACK, CFA_IFM,
                                         "In nmhSetIfIpAddr  - Reverting \
                                         the IP address failed\r\n ");
                            }

                            if ((CLI_SUCCESS == CLI_GET_ERR (&u4ErrorCode)) &&
                                (0 == u4ErrorCode))
                            {
                                /* Set CLI Error only when it is not set 
                                 * already */
                                CLI_SET_ERR (CFA_CLI_IFM_CONFIG_ERR);
                            }
                            break;
                        }
                    }
                }
                i1RetVal = SNMP_SUCCESS;

            default:
                break;
        }
        if (u1ChangeInStat == CFA_TRUE)
        {
            CfaSetIfMainAdminStatus ((INT4) u4IfIndex, i4AdminStatus);
            CfaSetIfIpv4AddressChange (u4IfIndex, CFA_FALSE);
        }
        return i1RetVal;
    }
}

/****************************************************************************
 Function    :  nmhSetIfIpSubnetMask
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfIpSubnetMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfIpSubnetMask (INT4 i4IfMainIndex, UINT4 u4SetValIfIpSubnetMask)
{
    UINT4               u4IfIndex;
    UINT1               u1SubnetMask;
    UINT4               u4SubnetMask;
    UINT4               u4BcastAddr;
    UINT1               u1IfType;
    UINT1               u1IsActive = CFA_FALSE;
    UINT1               u1IsAlreadyRegWithIp = CFA_FALSE;
    UINT1               u1Counter = 0;
    tIpConfigInfo       IpIfInfo;
    UINT4               u4Flag = 0;
    INT4                i4AdminStatus = 0;
    UINT1               u1ChangeInStat = CFA_FALSE;
    INT1                i1RetVal = SNMP_FAILURE;
#ifdef PPP_WANTED
    tIfLayerInfo        LowInterface;
    tIfLayerInfo        HighInterface;
    tPppIpInfo          IpInfo;
    UINT4               u4CurMtu;
#endif /* PPP_WANTED */

    u4IfIndex = (UINT4) i4IfMainIndex;

    if (CfaValidateCfaIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        while ((u1Counter <= CFA_MAX_CIDR) &&
               (u4CidrSubnetMask[u1Counter] != (UINT4) u4SetValIfIpSubnetMask))
        {
            ++u1Counter;
        }
        if (u1Counter > CFA_MAX_CIDR)
        {
            return SNMP_FAILURE;
        }

        u1SubnetMask = u1Counter;

        /* Null check has to be provided here to avoid crash as MSR does not call
         * Test routine */
        if (CFA_IF_ENTRY (u4IfIndex) == NULL)
        {
            return SNMP_FAILURE;
        }

        if (CfaIpIfGetIfInfo (u4IfIndex, &IpIfInfo) == CFA_FAILURE)
        {
            return SNMP_FAILURE;
        }

        u4SubnetMask = u4CidrSubnetMask[u1SubnetMask];
        u4BcastAddr = (~u4SubnetMask | IpIfInfo.u4Addr);

        /* register/update with IP only if RS is active */
        if (CFA_IF_RS (u4IfIndex) == CFA_RS_ACTIVE)
        {
            u1IsActive = CFA_TRUE;
            if (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX)
            {
                u1IsAlreadyRegWithIp = CFA_TRUE;
            }
        }

        if (u4SetValIfIpSubnetMask == IpIfInfo.u4NetMask)
        {
            return SNMP_SUCCESS;
        }

        /*Get Admin Status */
        i4AdminStatus = (INT4) CFA_IF_ADMIN (u4IfIndex);

        if (i4AdminStatus != CFA_IF_DOWN)
        {
            /*Set Admin Status as DOWN */
            u1ChangeInStat = CFA_TRUE;
            CfaSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_DOWN);
        }

        CfaGetIfType (u4IfIndex, &u1IfType);

        if (u1IfType == CFA_INVALID_TYPE)
        {
            /* Silent ignore, for invalid port type.
             * Test routine already covered this check.
             * This code will hit only in case of MSR where
             * set is called without test routine.
             * Since user will be confused on getting MSR
             * errors this rare case is silently ignored.
             */
            return SNMP_SUCCESS;
        }

        switch (u1IfType)
        {
            case CFA_OTHER:
            case CFA_ENET:
            case CFA_LAGG:
            case CFA_L3SUB_INTF:
            case CFA_L3IPVLAN:
            case CFA_PSEUDO_WIRE:
#ifdef VXLAN_WANTED
            case CFA_VXLAN_NVE:
#endif
            case CFA_LOOPBACK:
#ifdef VPN_WANTED
#ifdef IKE_WANTED
            case CFA_VPNC:
#endif /* IKE_WANTED */
#endif /* VPN_WANTED */
#ifdef WGS_WANTED
            case CFA_L2VLAN:
#endif /* WGS_WANTED */
#ifdef PPP_WANTED
            case CFA_PPP:
#endif /* WGS_WANTED */

                u4Flag = CFA_IP_IF_NETMASK | CFA_IP_IF_BCASTADDR;

                IpIfInfo.u4NetMask = u4SubnetMask;
                IpIfInfo.u4BcastAddr = u4BcastAddr;

                if (CfaIpIfSetIfInfo ((UINT4) i4IfMainIndex, u4Flag,
                                      &IpIfInfo) != CFA_SUCCESS)
                {
                    break;
                }
                if (u1IsActive == CFA_TRUE)
                {
#ifdef PPP_WANTED
                    if (u1IfType == CFA_PPP)
                    {
                        LowInterface.u1Command = HighInterface.u1Command =
                            CFA_LAYER_IGNORE;
                        IpInfo.u4LocalIpAddr = IpIfInfo.u4Addr;
                        IpInfo.u4PeerIpAddr = CFA_IF_PEER_IPADDR (u4IfIndex);
                        IpInfo.u1SubnetMask = u1SubnetMask;

                        CfaGetIfMtu (u4IfIndex, &u4CurMtu);
                        if (CfaIfmUpdatePppInterface (u4IfIndex, &LowInterface,
                                                      &HighInterface, &IpInfo,
                                                      CFA_IF_IDLE_TIMEOUT
                                                      (u4IfIndex),
                                                      u4CurMtu) != CFA_SUCCESS)
                        {
                            CLI_SET_ERR (CFA_CLI_IFM_CONFIG_ERR);
                            break;
                        }
                    }
#endif
                    if (u1IsAlreadyRegWithIp == CFA_TRUE)
                    {
                        if (CfaIfmConfigNetworkInterface
                            (CFA_NET_IF_UPD, u4IfIndex, 0, NULL) != CFA_SUCCESS)
                        {
                            CLI_SET_ERR (CFA_CLI_IFM_CONFIG_ERR);
                            break;
                        }
                    }
                }
                i1RetVal = SNMP_SUCCESS;
                break;

            default:
                break;

        }
        if (u1ChangeInStat == CFA_TRUE)
        {
            CfaSetIfMainAdminStatus ((INT4) u4IfIndex, i4AdminStatus);
        }
        return i1RetVal;
    }
}

/****************************************************************************
 Function    :  nmhSetIfIpBroadcastAddr
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfIpBroadcastAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfIpBroadcastAddr (INT4 i4IfMainIndex, UINT4 u4SetValIfIpBroadcastAddr)
{
    UINT4               u4IfIndex;
    UINT1               u1IsActive = CFA_FALSE;
    UINT1               u1IsAlreadyRegWithIp = CFA_FALSE;
    tIpConfigInfo       IpIfInfo;
    UINT4               u4Flag = 0;

    u4IfIndex = (UINT4) i4IfMainIndex;
    /* Null check has to be provided here to avoid crash as MSR does not call
     * Test routine */

    if (CfaValidateCfaIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        if (CFA_IF_ENTRY (u4IfIndex) == NULL)
        {
            return SNMP_FAILURE;
        }
        /* register/update with IP only if RS is active */
        if (CFA_IF_RS (u4IfIndex) == CFA_RS_ACTIVE)
        {
            u1IsActive = CFA_TRUE;
            if (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX)
            {
                u1IsAlreadyRegWithIp = CFA_TRUE;
            }
        }

        u4Flag = CFA_IP_IF_BCASTADDR;

        IpIfInfo.u4BcastAddr = (UINT4) u4SetValIfIpBroadcastAddr;

        if (CfaIpIfSetIfInfo ((UINT4) i4IfMainIndex, u4Flag,
                              &IpIfInfo) != CFA_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (u1IsActive == CFA_TRUE)
        {
            if (u1IsAlreadyRegWithIp == CFA_TRUE)
            {
                if (CfaIfmConfigNetworkInterface (CFA_NET_IF_UPD, u4IfIndex,
                                                  0, NULL) != CFA_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
        }

        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhSetIfIpForwardingEnable
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfIpForwardingEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfIpForwardingEnable (INT4 i4IfMainIndex,
                            INT4 i4SetValIfIpForwardingEnable)
{
    UINT4               u4IfIndex;
    UINT4               u4Flag = 0;
    tIpConfigInfo       IpIfInfo;

    u4IfIndex = (UINT4) i4IfMainIndex;

    if (CfaValidateCfaIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Null check has to be provided here to avoid crash as MSR does not call
     * Test routine */
    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }
    else if (CFA_IF_ENTRY (u4IfIndex) == NULL)
    {
        return SNMP_FAILURE;
    }

#ifdef IP_WANTED
    /* notify to IP if interface is registered with IP */
    if (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX)
    {
        if (IpUpdateInterfaceForwardingStatus ((UINT2)
                                               CFA_IF_IPPORT (u4IfIndex),
                                               (UINT1)
                                               i4SetValIfIpForwardingEnable) !=
            IP_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    u4Flag = CFA_IP_IF_FORWARD;

    MEMSET (&IpIfInfo, 0, sizeof (tIpConfigInfo));
    IpIfInfo.u1IpForwardEnable = (UINT1) i4SetValIfIpForwardingEnable;

    if (CfaIpIfSetIfInfo (u4IfIndex, u4Flag, &IpIfInfo) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfIpAddrAllocProtocol
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfIpAddrAllocProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfIpAddrAllocProtocol (INT4 i4IfMainIndex,
                             INT4 i4SetValIfIpAddrAllocProtocol)
{
    UINT4               u4IfIndex;
    UINT4               u4Flag = 0;
    tIpConfigInfo       IpIfInfo;

    /* This is set will not take effect 
     * until port next oper status change (i.e) from down to up */

    if (((UINT4) i4IfMainIndex > CFA_MAX_INTERFACES ()) || (i4IfMainIndex <= 0))
    {
        return SNMP_FAILURE;
    }

    u4IfIndex = (UINT4) i4IfMainIndex;

    if (CfaValidateCfaIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }
    else
    {

        if (CFA_IF_ENTRY_USED (u4IfIndex))
        {
            u4Flag = CFA_IP_IF_ALLOC_PROTO;

            MEMSET (&IpIfInfo, 0, sizeof (tIpConfigInfo));
            IpIfInfo.u1AddrAllocProto = (UINT1) i4SetValIfIpAddrAllocProtocol;
            if (CfaIpIfSetIfInfo (u4IfIndex, u4Flag, &IpIfInfo) == CFA_FAILURE)
            {
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IfIpAddrAllocMethod
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfIpAddrAllocMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfIpAddrAllocMethod (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                              INT4 i4TestValIfIpAddrAllocMethod)
{
    UINT4               u4IfIndex;
    UINT1               u1IfType;
    UINT1               u1BridgedIfaceStatus;
    tIpConfigInfo       IpIfInfo;

    u4IfIndex = (UINT4) i4IfMainIndex;

    if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (CfaIpIfGetIfInfo (u4IfIndex, &IpIfInfo) == CFA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

/* only the manual and negotiation methods are supported currently */
    if (!((i4TestValIfIpAddrAllocMethod == CFA_IP_ALLOC_MAN) ||
          (i4TestValIfIpAddrAllocMethod == CFA_IP_ALLOC_POOL) ||
          (i4TestValIfIpAddrAllocMethod == CFA_IP_ALLOC_NONE) ||
          (i4TestValIfIpAddrAllocMethod == CFA_IP_ALLOC_NEGO)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* IP address allocation method should not be configured for the 
     * bridged interface
     */
    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);
        if (u1BridgedIfaceStatus == CFA_ENABLED)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    CfaGetIfType (u4IfIndex, &u1IfType);
    /* Only mannual allocation method is supported for loopback interface */
    if ((u1IfType == CFA_LOOPBACK) &&
        (i4TestValIfIpAddrAllocMethod != CFA_IP_ALLOC_MAN))
    {
        CLI_SET_ERR (CLI_CFA_LO_ALLOC_METHOD_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfIpAddr
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfIpAddr (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                   UINT4 u4TestValIfIpAddr)
{
    UINT4               u4IfIndex;
    UINT1               u1IfType;
    UINT1               u1BridgedIfaceStatus;
    UINT1               u1AllocMethod;
    UINT4               u4NodeIdIpAddr;
    tIpConfigInfo       IpIfInfo;
#ifdef VRRP_WANTED
    tIPvXAddr           PrevIpAddr;
    tIPvXAddr           CurIpAddr;
    UINT4               u4TempAddr = 0;
#endif

#ifdef VRRP_WANTED
    IPVX_ADDR_CLEAR (&PrevIpAddr);
    IPVX_ADDR_CLEAR (&CurIpAddr);
#endif

    u4IfIndex = (UINT4) i4IfMainIndex;
    if (CfaIfGetIpAllocMethod (u4IfIndex, &u1AllocMethod) == CFA_SUCCESS)
    {
        if ((u1AllocMethod != CFA_IP_ALLOC_MAN) &&
            (u1AllocMethod != CFA_IP_ALLOC_NONE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_CFA_IPADDR_ALLOC_ERR);
            return SNMP_FAILURE;
        }

    }

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_INVALID_IPADDR);
        return SNMP_FAILURE;
    }
    else
    {
        if (CfaIpIfGetIfInfo (u4IfIndex, &IpIfInfo) == CFA_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_CFA_INVALID_IPADDR);
            return SNMP_FAILURE;
        }
        if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_CFA_INVALID_IPADDR);
            return SNMP_FAILURE;
        }

        if (u4TestValIfIpAddr != 0)
        {
            if (CFA_IS_LOOPBACK (u4TestValIfIpAddr))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_CFA_LOOPBACK_IPADDR);
                return SNMP_FAILURE;
            }
            if (!((CFA_IS_ADDR_CLASS_A (u4TestValIfIpAddr) ?
                   (IP_IS_ZERO_NETWORK (u4TestValIfIpAddr) ? 0 : 1) : 0) ||
                  (CFA_IS_ADDR_CLASS_B (u4TestValIfIpAddr)) ||
                  (CFA_IS_ADDR_CLASS_C (u4TestValIfIpAddr)) ||
                  (CFA_IS_ADDR_CLASS_E (u4TestValIfIpAddr))))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_CFA_ZERO_IP_ERR);
                return SNMP_FAILURE;
            }
        }

        /* IP address should not be configured for the bridged interface */
        if (gu4IsIvrEnabled == CFA_ENABLED)
        {
            CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);
            if (u1BridgedIfaceStatus == CFA_ENABLED)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_CFA_IP_BRIDGE_ERR);
                return SNMP_FAILURE;
            }
        }

        CfaGetIfType (u4IfIndex, &u1IfType);

        if (u1IfType != CFA_LOOPBACK)
        {
            /* 127.0.0.0/8 reserved for loop back , should not assign for other interfaces */
            if (CFA_IS_LOOPBACK_RANGE (u4TestValIfIpAddr))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_CFA_LOOPBACK_ERR);
                return SNMP_FAILURE;
            }
        }

        if (u4TestValIfIpAddr == IpIfInfo.u4Addr)
        {
            return SNMP_SUCCESS;
        }

        /* Dont allow configuration of primary address same as that
         * of secondary */
        if (CfaIpIfIsOurInterfaceAddress (u4IfIndex,
                                          u4TestValIfIpAddr) == CFA_SUCCESS)
        {
            CLI_SET_ERR (CFA_CLI_SAME_IP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (u4TestValIfIpAddr != 0)
        {
            /* Validate Primary IP address of node should not be same as peer node
             * OOB Secondary IP address 
             */
            if (IssGetSwitchid () == CFA_NODE0)
            {
                nmhGetIfOOBNode1SecondaryIpAddress (&u4NodeIdIpAddr);
                if (u4NodeIdIpAddr == u4TestValIfIpAddr)
                {
                    CLI_SET_ERR (CFA_CLI_SAME_IP_ERR);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            else
            {
                nmhGetIfOOBNode0SecondaryIpAddress (&u4NodeIdIpAddr);
                if (u4NodeIdIpAddr == u4TestValIfIpAddr)
                {
                    CLI_SET_ERR (CFA_CLI_SAME_IP_ERR);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
        }
        /*To allow the ipaddress to 0.0.0.0 */
        if (u4TestValIfIpAddr == 0)
        {
#ifdef VRRP_WANTED
            u4TempAddr = OSIX_HTONL (IpIfInfo.u4Addr);

            IPVX_ADDR_INIT_FROMV4 (CurIpAddr, u4TempAddr);

            if (VrrpApiValIfIpChange (u4IfIndex, CurIpAddr,
                                      IpIfInfo.u4NetMask) == VRRP_OK)
            {
                CLI_SET_ERR (CLI_CFA_IP_NO_CHG_VRRP_INUSE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (VrrpApiValDecPriority (u4IfIndex,
                                       (UINT1) VRRP_VAL_PRIO_OPER_IP_DEL,
                                       PrevIpAddr, CurIpAddr) == VRRP_NOT_OK)
            {
                CLI_SET_ERR (CLI_CFA_IP_NO_CHG_VRRP_INUSE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
#endif
            return SNMP_SUCCESS;
        }
        /* Validate the IP with Subnet configured, if any */
        if (CfaIpIfValidateIfIpSubnet (u4IfIndex,
                                       u4TestValIfIpAddr,
                                       IpIfInfo.u4NetMask) == CFA_FAILURE)
        {
            CLI_SET_ERR (CLI_CFA_SUBNET_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
#ifdef VRRP_WANTED
        u4TempAddr = OSIX_HTONL (IpIfInfo.u4Addr);
        IPVX_ADDR_INIT_FROMV4 (PrevIpAddr, u4TempAddr);

        if ((IpIfInfo.u4Addr & IpIfInfo.u4NetMask) !=
            (u4TestValIfIpAddr & IpIfInfo.u4NetMask))
        {
            if (VrrpApiValIfIpChange (u4IfIndex, PrevIpAddr,
                                      IpIfInfo.u4NetMask) == VRRP_OK)
            {
                CLI_SET_ERR (CLI_CFA_IP_NO_CHG_VRRP_INUSE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

        }

        u4TempAddr = OSIX_HTONL (u4TestValIfIpAddr);
        IPVX_ADDR_INIT_FROMV4 (CurIpAddr, u4TempAddr);

        if (VrrpApiValDecPriority (u4IfIndex,
                                   (UINT1) VRRP_VAL_PRIO_OPER_IP_MODIFY,
                                   PrevIpAddr, CurIpAddr) == VRRP_NOT_OK)
        {
            CLI_SET_ERR (CLI_CFA_IP_NO_CHG_VRRP_INUSE);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
#endif

        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhTestv2IfIpSubnetMask
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfIpSubnetMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfIpSubnetMask (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                         UINT4 u4TestValIfIpSubnetMask)
{
    UINT4               u4IfIndex;
    UINT1               u1Counter;
    UINT1               u1BridgedIfaceStatus;
    UINT1               u1IfType = 0;
    UINT4               u4BroadCastAddr;
    UINT4               u4NetworkIpAddr;
    tIpConfigInfo       IpIfInfo;

    u4IfIndex = (UINT4) i4IfMainIndex;
    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        if (CfaIpIfGetIfInfo (u4IfIndex, &IpIfInfo) == CFA_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }

        if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }

        CfaGetIfType (u4IfIndex, &u1IfType);

        /* the IfType must have been specified first */
        if (u1IfType == CFA_INVALID_TYPE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }

        /* Subnet mas should not be configured for the bridged interface */

        if (gu4IsIvrEnabled == CFA_ENABLED)
        {
            CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);
            if (u1BridgedIfaceStatus == CFA_ENABLED)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }

        /* The valid subnet address is from 0 to 32 */
        for (u1Counter = 0; u1Counter <= CFA_MAX_CIDR; ++u1Counter)
        {
            if (u4CidrSubnetMask[u1Counter] == u4TestValIfIpSubnetMask)
            {
                break;
            }
        }

        if (u1Counter != 0)
        {
            if ((CFA_CDB_IF_TYPE (u4IfIndex) == CFA_LOOPBACK) &&
                ((u1Counter != CFA_MAX_CIDR)))
            {
                /* Loopback interface mask should be 32 bit mask. */
                CLI_SET_ERR (CLI_CFA_WRONG_SUBNET_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }

        if (u1Counter > CFA_MAX_CIDR)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        /* with VRF_WANTED defined over lapping IP Address  can be configured
         * across different VR.
         */
        if (CfaIpIfValidateIfIpSubnet (u4IfIndex,
                                       IpIfInfo.u4Addr,
                                       u4TestValIfIpSubnetMask) == CFA_FAILURE)
        {
            CLI_SET_ERR (CLI_CFA_SUBNET_ERR);
            return SNMP_FAILURE;
        }

        if (u4TestValIfIpSubnetMask == 0)
        {
            if (IpIfInfo.u4Addr == 0)
            {
                /* Un-numbered interface case */
                return SNMP_SUCCESS;
            }
            else
            {
                /* Valid IP and 0.0.0.0 mask */
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
        else
        {
            if (IpIfInfo.u4Addr == 0)
            {
                /* 0.0.0.0 IP valid mask */
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }

        if (u4TestValIfIpSubnetMask != 0xffffffff)
        {
            /* ipaddress should not be Broadcast Address */
            u4BroadCastAddr = IpIfInfo.u4Addr | (~(u4TestValIfIpSubnetMask));
            if (IpIfInfo.u4Addr == u4BroadCastAddr)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            /* ipaddress should not be Network Address */
            u4NetworkIpAddr = IpIfInfo.u4Addr & (~(u4TestValIfIpSubnetMask));
            if (u4NetworkIpAddr == 0)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhTestv2IfIpBroadcastAddr
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfIpBroadcastAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfIpBroadcastAddr (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                            UINT4 u4TestValIfIpBroadcastAddr)
{
    UINT4               u4IfIndex;
    UINT4               u4BrdCstAddr;
    UINT1               u1BridgedIfaceStatus;
    tIpConfigInfo       IpIfInfo;

    u4IfIndex = (UINT4) i4IfMainIndex;
    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        if (CfaIpIfGetIfInfo (u4IfIndex, &IpIfInfo) == CFA_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }

        if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }

        /* Broadcast address should not be configured for the bridged interface */

        if (gu4IsIvrEnabled == CFA_ENABLED)
        {
            CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);
            if (u1BridgedIfaceStatus == CFA_ENABLED)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }

        if (IpIfInfo.u4Addr == 0)
        {
            return SNMP_SUCCESS;
        }

        if (!((CFA_IS_ADDR_CLASS_A (u4TestValIfIpBroadcastAddr)) ||
              (CFA_IS_ADDR_CLASS_B (u4TestValIfIpBroadcastAddr)) ||
              (CFA_IS_ADDR_CLASS_C (u4TestValIfIpBroadcastAddr)) ||
              (CFA_IS_ADDR_CLASS_E (u4TestValIfIpBroadcastAddr))))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        u4BrdCstAddr = (IpIfInfo.u4Addr | (~(IpIfInfo.u4NetMask)));

        if (u4BrdCstAddr != u4TestValIfIpBroadcastAddr)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhTestv2IfIpForwardingEnable
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfIpForwardingEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfIpForwardingEnable (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                               INT4 i4TestValIfIpForwardingEnable)
{

    UINT4               u4IfIndex = (UINT4) i4IfMainIndex;
    UINT1               u1BridgedIfaceStatus;
    tIpConfigInfo       IpIfInfo;

    if (CfaIpIfGetIfInfo (u4IfIndex, &IpIfInfo) == CFA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (!((i4TestValIfIpForwardingEnable == CFA_DISABLED) ||
          (i4TestValIfIpForwardingEnable == CFA_ENABLED)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((gu4IsIvrEnabled == CFA_ENABLED) &&
        (CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus)
         == CFA_SUCCESS))
    {
        if (u1BridgedIfaceStatus == CFA_ENABLED)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfIpAddrAllocProtocol
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfIpAddrAllocProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
    UINT4               u4IfIndex;
    UINT1               u1IfType;
    UINT1               u1BridgedIfaceStatus;
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfIpAddrAllocProtocol (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                                INT4 i4TestValIfIpAddrAllocProtocol)
{
    UINT4               u4IfIndex = (UINT4) i4IfMainIndex;
    UINT1               u1BridgedIfaceStatus;
    UINT1               u1IfType;
    tIpConfigInfo       IpIfInfo;

    if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (CfaIpIfGetIfInfo (u4IfIndex, &IpIfInfo) == CFA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* only the manual and negotiation methods are supported currently */
    if ((i4TestValIfIpAddrAllocProtocol != CFA_PROTO_RARP) &&
        (i4TestValIfIpAddrAllocProtocol != CFA_PROTO_DHCP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    CfaGetIfType ((UINT4) i4IfMainIndex, &u1IfType);
    if (u1IfType == CFA_LOOPBACK)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_LO_ALLOCPROT_ERR);
        return SNMP_FAILURE;
    }

    /* IP address allocation protocol should not be configured for the 
     * bridged interface
     */
    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);
        if (u1BridgedIfaceStatus == CFA_ENABLED)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : IfSecondaryIpAddressTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIfSecondaryIpAddressTable
 Input       :  The Indices
                IfMainIndex
                IfSecondaryIpAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceIfSecondaryIpAddressTable (INT4 i4IfMainIndex,
                                                   UINT4 u4IfSecondaryIpAddress)
{
    tIpAddrInfo         SecondaryInfo;

    /* This secondary IP address is not configured for the interfaace */
    if (CfaIpIfGetSecondaryAddressInfo ((UINT4) i4IfMainIndex,
                                        u4IfSecondaryIpAddress,
                                        &SecondaryInfo) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIfSecondaryIpAddressTable
 Input       :  The Indices
                IfMainIndex
                IfSecondaryIpAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexIfSecondaryIpAddressTable (INT4 *pi4IfMainIndex,
                                           UINT4 *pu4IfSecondaryIpAddress)
{
    UINT4               u4NextIfIndex = 0;

    if (CfaIpIfGetNextSecondaryIpIndex (0, 0, &u4NextIfIndex,
                                        pu4IfSecondaryIpAddress) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4IfMainIndex = (INT4) u4NextIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIfSecondaryIpAddressTable
 Input       :  The Indices
                IfMainIndex
                nextIfMainIndex
                IfSecondaryIpAddress
                nextIfSecondaryIpAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexIfSecondaryIpAddressTable (INT4 i4IfMainIndex,
                                          INT4 *pi4NextIfMainIndex,
                                          UINT4 u4IfSecondaryIpAddress,
                                          UINT4 *pu4NextIfSecondaryIpAddress)
{
    UINT4               u4NextIfIndex = 0;

    if (CfaIpIfGetNextSecondaryIpIndex ((UINT4) i4IfMainIndex,
                                        u4IfSecondaryIpAddress,
                                        &u4NextIfIndex,
                                        pu4NextIfSecondaryIpAddress) ==
        CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4NextIfMainIndex = (INT4) u4NextIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfSecondaryIpSubnetMask
 Input       :  The Indices
                IfMainIndex
                IfSecondaryIpAddress

                The Object 
                retValIfSecondaryIpSubnetMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfSecondaryIpSubnetMask (INT4 i4IfMainIndex,
                               UINT4 u4IfSecondaryIpAddress,
                               UINT4 *pu4RetValIfSecondaryIpSubnetMask)
{
    UINT4               u4IfIndex = (UINT4) i4IfMainIndex;
    tIpAddrInfo         SecondaryInfo;

    /* Check whether the entry exist or not */
    if (CfaIpIfGetSecondaryAddressInfo (u4IfIndex, u4IfSecondaryIpAddress,
                                        &SecondaryInfo) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIfSecondaryIpSubnetMask =
        u4CidrSubnetMask[SecondaryInfo.u1SubnetMask];
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfSecondaryIpBroadcastAddr
 Input       :  The Indices
                IfMainIndex
                IfSecondaryIpAddress

                The Object 
                retValIfSecondaryIpBroadcastAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfSecondaryIpBroadcastAddr (INT4 i4IfMainIndex,
                                  UINT4 u4IfSecondaryIpAddress,
                                  UINT4 *pu4RetValIfSecondaryIpBroadcastAddr)
{
    UINT4               u4IfIndex = (UINT4) i4IfMainIndex;
    tIpAddrInfo         SecondaryInfo;

    /* Check whether the entry exist or not */
    if (CfaIpIfGetSecondaryAddressInfo (u4IfIndex, u4IfSecondaryIpAddress,
                                        &SecondaryInfo) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIfSecondaryIpBroadcastAddr = SecondaryInfo.u4BcastAddr;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfSecondaryIpRowStatus
 Input       :  The Indices
                IfMainIndex
                IfSecondaryIpAddress

                The Object 
                retValIfSecondaryIpRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfSecondaryIpRowStatus (INT4 i4IfMainIndex,
                              UINT4 u4IfSecondaryIpAddress,
                              INT4 *pi4RetValIfSecondaryIpRowStatus)
{
    UINT4               u4IfIndex = (UINT4) i4IfMainIndex;
    tIpAddrInfo         SecondaryInfo;

    /* Check whether the entry exist or not */
    if (CfaIpIfGetSecondaryAddressInfo (u4IfIndex, u4IfSecondaryIpAddress,
                                        &SecondaryInfo) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIfSecondaryIpRowStatus = SecondaryInfo.i4RowStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfSecondaryIpSubnetMask
 Input       :  The Indices
                IfMainIndex
                IfSecondaryIpAddress

                The Object 
                setValIfSecondaryIpSubnetMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfSecondaryIpSubnetMask (INT4 i4IfMainIndex,
                               UINT4 u4IfSecondaryIpAddress,
                               UINT4 u4SetValIfSecondaryIpSubnetMask)
{
    UINT4               u4IfIndex = (UINT4) i4IfMainIndex;
    tIpAddrInfo         SecondaryInfo;
    UINT1               u1CidrMask = 0;

    MEMSET (&SecondaryInfo, 0, sizeof (tIpAddrInfo));

    if (CfaValidateCfaIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Check whether the entry exist or not */
    if (CfaIpIfGetSecondaryAddressInfo (u4IfIndex, u4IfSecondaryIpAddress,
                                        &SecondaryInfo) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }

    u1CidrMask = CfaGetCidrSubnetMaskIndex (u4SetValIfSecondaryIpSubnetMask);

    if (u1CidrMask == SecondaryInfo.u1SubnetMask)
    {
        return SNMP_SUCCESS;
    }

    SecondaryInfo.u1SubnetMask = u1CidrMask;
    SecondaryInfo.u4Addr = u4IfSecondaryIpAddress;

    if (CfaIpIfSetSecondaryAddressInfo (u4IfIndex, CFA_IP_IF_NETMASK,
                                        &SecondaryInfo) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfSecondaryIpBroadcastAddr
 Input       :  The Indices
                IfMainIndex
                IfSecondaryIpAddress

                The Object 
                setValIfSecondaryIpBroadcastAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfSecondaryIpBroadcastAddr (INT4 i4IfMainIndex,
                                  UINT4 u4IfSecondaryIpAddress,
                                  UINT4 u4SetValIfSecondaryIpBroadcastAddr)
{
    UINT4               u4IfIndex = (UINT4) i4IfMainIndex;
    tIpAddrInfo         SecondaryInfo;

    MEMSET (&SecondaryInfo, 0, sizeof (tIpAddrInfo));

    if (CfaValidateCfaIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Check whether the entry exist or not */
    if (CfaIpIfGetSecondaryAddressInfo (u4IfIndex, u4IfSecondaryIpAddress,
                                        &SecondaryInfo) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (SecondaryInfo.u4BcastAddr == u4SetValIfSecondaryIpBroadcastAddr)
    {
        return SNMP_SUCCESS;
    }

    SecondaryInfo.u4BcastAddr = u4SetValIfSecondaryIpBroadcastAddr;
    SecondaryInfo.u4Addr = u4IfSecondaryIpAddress;

    if (CfaIpIfSetSecondaryAddressInfo (u4IfIndex, CFA_IP_IF_BCASTADDR,
                                        &SecondaryInfo) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfSecondaryIpRowStatus
 Input       :  The Indices
                IfMainIndex
                IfSecondaryIpAddress

                The Object 
                setValIfSecondaryIpRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfSecondaryIpRowStatus (INT4 i4IfMainIndex,
                              UINT4 u4IfSecondaryIpAddress,
                              INT4 i4SetValIfSecondaryIpRowStatus)
{
    tIpAddrInfo         SecondaryInfo;
    UINT4               u4SubNetMask;
    UINT4               u4IfIndex = (UINT4) i4IfMainIndex;
    UINT1               u1CidrMask = 0;
    INT4                i4AdminStatus = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&SecondaryInfo, 0, sizeof (tIpAddrInfo));

    if (CfaValidateCfaIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValIfSecondaryIpRowStatus)
    {
        case CFA_RS_ACTIVE:
        case CFA_RS_NOTINSERVICE:
        case CFA_RS_DESTROY:
            /* Check whether the entry exist or not */
            if (CfaIpIfGetSecondaryAddressInfo
                (u4IfIndex, u4IfSecondaryIpAddress,
                 &SecondaryInfo) == CFA_FAILURE)
            {
                return SNMP_FAILURE;
            }
            if (SecondaryInfo.i4RowStatus == i4SetValIfSecondaryIpRowStatus)
            {
                return SNMP_SUCCESS;
            }
            SecondaryInfo.i4RowStatus = i4SetValIfSecondaryIpRowStatus;
            SecondaryInfo.u4Addr = u4IfSecondaryIpAddress;

            if (CfaIpIfSetSecondaryAddressInfo (u4IfIndex,
                                                CFA_IP_IF_SECONDAY_ROW_STATUS,
                                                &SecondaryInfo) == CFA_SUCCESS)
            {
                i1RetVal = SNMP_SUCCESS;
            }
            return i1RetVal;

        case CFA_RS_CREATEANDGO:
            /*Get Admin Status */
            i4AdminStatus = (INT4) CFA_IF_ADMIN (u4IfIndex);

            if (i4AdminStatus != CFA_IF_DOWN)
            {
                /*Set Admin Status as DOWN */
                CfaSetIfMainAdminStatus ((INT4) u4IfIndex, CFA_IF_DOWN);
            }
            break;

        case CFA_RS_CREATEANDWAIT:

            CfaGetDefaultNetMask (u4IfSecondaryIpAddress, &u4SubNetMask);

            u1CidrMask = CfaGetCidrSubnetMaskIndex (u4SubNetMask);
            SecondaryInfo.u1SubnetMask = u1CidrMask;
            SecondaryInfo.u4Addr = u4IfSecondaryIpAddress;
            SecondaryInfo.u4BcastAddr =
                (~u4SubNetMask | u4IfSecondaryIpAddress);
            SecondaryInfo.i4RowStatus = i4SetValIfSecondaryIpRowStatus;

            if (CfaIpIfSetSecondaryAddressInfo (u4IfIndex,
                                                CFA_IP_IF_SECONDAY_ROW_STATUS,
                                                &SecondaryInfo) == CFA_SUCCESS)
            {
                i1RetVal = SNMP_SUCCESS;
            }
            return i1RetVal;
        default:
            break;
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2IfSecondaryIpSubnetMask
 Input       :  The Indices
                IfMainIndex
                IfSecondaryIpAddress

                The Object 
                testValIfSecondaryIpSubnetMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfSecondaryIpSubnetMask (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                                  UINT4 u4IfSecondaryIpAddress,
                                  UINT4 u4TestValIfSecondaryIpSubnetMask)
{
    UINT4               u4IfIndex = (UINT4) i4IfMainIndex;
    UINT1               u1Counter;
    UINT1               u1IfType;
    UINT1               u1BridgedIfaceStatus;
    UINT4               u4NetworkIpAddr;
    UINT4               u4BroadCastAddr;
    tIpAddrInfo         SecondaryInfo;

    /* This secondary IP address is not configured for the interfaace */
    if (CfaIpIfGetSecondaryAddressInfo (u4IfIndex, u4IfSecondaryIpAddress,
                                        &SecondaryInfo) == CFA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Before setting the value,Row status should be made to not-in-service */
    if (SecondaryInfo.i4RowStatus == CFA_RS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Subnet mas should not be configured for the bridged interface */
    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);
        if (u1BridgedIfaceStatus == CFA_ENABLED)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

    if (u1IfType != CFA_LOOPBACK)
    {
        /* 127.0.0.0/8 reserved for loop back , should not assign for other interfaces */
        if (CFA_IS_LOOPBACK_RANGE (u4IfSecondaryIpAddress))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_CFA_LOOPBACK_ERR);
            return SNMP_FAILURE;
        }
    }

    /* The valid subnet address is from 0 to 32 */
    for (u1Counter = 0; u1Counter <= CFA_MAX_CIDR; ++u1Counter)
    {
        if (u4CidrSubnetMask[u1Counter] == u4TestValIfSecondaryIpSubnetMask)
        {
            break;
        }
    }

    if (u1Counter > CFA_MAX_CIDR)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* with VRF_WANTED defined over lapping IP Address  can be configured 
     * across different VRs. Within each VR, IP address must not be overlapping.
     */
    if (CfaIpIfValidateIfIpSubnet (u4IfIndex,
                                   u4IfSecondaryIpAddress,
                                   u4TestValIfSecondaryIpSubnetMask) ==
        CFA_FAILURE)
    {
        CLI_SET_ERR (CLI_CFA_SUBNET_ERR);
        return SNMP_FAILURE;
    }

    if ((u4TestValIfSecondaryIpSubnetMask != 0xffffffff)
        && (u4TestValIfSecondaryIpSubnetMask != IP_ADDR_31BIT_MASK))
    {
        /* ipaddress should not be Broadcast Address */
        u4BroadCastAddr = u4IfSecondaryIpAddress |
            (~(u4TestValIfSecondaryIpSubnetMask));
        if (u4IfSecondaryIpAddress == u4BroadCastAddr)
        {
            CLI_SET_ERR (CLI_CFA_INVALID_SUBNET);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        /* ipaddress should not be Network Address */
        u4NetworkIpAddr = u4IfSecondaryIpAddress &
            (~(u4TestValIfSecondaryIpSubnetMask));
        if (u4NetworkIpAddr == 0)
        {
            CLI_SET_ERR (CLI_CFA_INVALID_SUBNET);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfSecondaryIpBroadcastAddr
 Input       :  The Indices
                IfMainIndex
                IfSecondaryIpAddress

                The Object 
                testValIfSecondaryIpBroadcastAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfSecondaryIpBroadcastAddr (UINT4 *pu4ErrorCode,
                                     INT4 i4IfMainIndex,
                                     UINT4 u4IfSecondaryIpAddress,
                                     UINT4 u4TestValIfSecondaryIpBroadcastAddr)
{
    UINT4               u4IfIndex;
    UINT4               u4BrdCstAddr;
    UINT1               u1BridgedIfaceStatus;
    tIpAddrInfo         SecondaryInfo;

    u4IfIndex = (UINT4) i4IfMainIndex;

    /* This secondary IP address is not configured for the interfaace */
    if (CfaIpIfGetSecondaryAddressInfo (u4IfIndex, u4IfSecondaryIpAddress,
                                        &SecondaryInfo) == CFA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Before setting the value,Row status should be made to not-in-service */
    if (SecondaryInfo.i4RowStatus == CFA_RS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Broadcast address should not be configured for the bridged interface */
    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);
        if (u1BridgedIfaceStatus == CFA_ENABLED)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (!((CFA_IS_ADDR_CLASS_A (u4TestValIfSecondaryIpBroadcastAddr)) ||
          (CFA_IS_ADDR_CLASS_B (u4TestValIfSecondaryIpBroadcastAddr)) ||
          (CFA_IS_ADDR_CLASS_C (u4TestValIfSecondaryIpBroadcastAddr))))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    u4BrdCstAddr = (u4IfSecondaryIpAddress |
                    (~(u4CidrSubnetMask[SecondaryInfo.u1SubnetMask])));

    if (u4BrdCstAddr != u4TestValIfSecondaryIpBroadcastAddr)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfSecondaryIpRowStatus
 Input       :  The Indices
                IfMainIndex
                IfSecondaryIpAddress

                The Object 
                testValIfSecondaryIpRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfSecondaryIpRowStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4IfMainIndex,
                                 UINT4 u4IfSecondaryIpAddress,
                                 INT4 i4TestValIfSecondaryIpRowStatus)
{
    tIpConfigInfo       IpIfInfo;
    UINT4               u4IfIndex = (UINT4) i4IfMainIndex;
    UINT1               u1BridgedIfaceStatus;
    tIpAddrInfo         SecondaryInfo;
    UINT4               u4SecondaryAddrCount = 0;
#ifdef VRRP_WANTED
    tIPvXAddr           PrevIpAddr;
    tIPvXAddr           CurIpAddr;
    UINT4               u4TempAddr = 0;
#endif

#ifdef VRRP_WANTED
    IPVX_ADDR_CLEAR (&PrevIpAddr);
    IPVX_ADDR_CLEAR (&CurIpAddr);
#endif

    /* Check whether interface entry exists in IP Interface table */
    if (CfaIpIfGetIfInfo (u4IfIndex, &IpIfInfo) == CFA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Configuration of secondary address is not allowed when the
     * address allocation method is not manual */
    if (IpIfInfo.u1AddrAllocMethod != CFA_IP_ALLOC_MAN)
    {
        CLI_SET_ERR (CFA_CLI_ALLOC_METHOD_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Dont allow configuration of secondary address when primary address
     * is not configured */
    if (IpIfInfo.u4Addr == 0)
    {
        CLI_SET_ERR (CFA_CLI_NO_PRIMARY_IP_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Dont allow configuration of secondary address if the same address
     * exists as primary */
    if (IpIfInfo.u4Addr == u4IfSecondaryIpAddress)
    {
        CLI_SET_ERR (CFA_CLI_SAME_IP_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!((CFA_IS_ADDR_CLASS_A (u4IfSecondaryIpAddress)) ||
          (CFA_IS_ADDR_CLASS_B (u4IfSecondaryIpAddress)) ||
          (CFA_IS_ADDR_CLASS_C (u4IfSecondaryIpAddress)) ||
          (u4IfSecondaryIpAddress == 0)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_CFA_ZERO_IP_ERR);
        return SNMP_FAILURE;
    }

    /* IP address should not be configured for the bridged interface */
    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);
        if (u1BridgedIfaceStatus == CFA_ENABLED)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    /* Check whether the IP Address assigned or not  */

    if (i4TestValIfSecondaryIpRowStatus == CFA_RS_DESTROY)
    {
        if (CfaIpIfGetSecondaryAddressInfo (u4IfIndex, u4IfSecondaryIpAddress,
                                            &SecondaryInfo) == CFA_FAILURE)
        {
            CLI_SET_ERR (CLI_CFA_UNASSIGNED_IPADDR_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    /* Check if the DualOob is enabled.
     * If enabled, OOB's secondary ip should be set only through setting value 
     * for IfOOBNode0SecondaryIpAddres or IfOOBNode1SecondaryIpAddres.
     * Creation or destroy of secondary ip entry directly through this object 
     * is not allowed. */
    if ((CfaIsMgmtPort ((UINT4) i4IfMainIndex) == TRUE) &&
        (CfaIsDualOobEnabled () == OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    switch (i4TestValIfSecondaryIpRowStatus)
    {
        case CFA_RS_CREATEANDGO:
        case CFA_RS_CREATEANDWAIT:

            /* Secondary IP address is already configured for the interfaace */
            if (CfaIpIfGetSecondaryAddressInfo (u4IfIndex,
                                                u4IfSecondaryIpAddress,
                                                &SecondaryInfo) == SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            else
            {
                CfaIpIfGetSecondaryAddressCount (u4IfIndex,
                                                 &u4SecondaryAddrCount);
                if (u4SecondaryAddrCount == IP_DEV_MAX_ADDR_PER_IFACE - 1)
                {
                    CLI_SET_ERR (CFA_CLI_MAX_SEC_ERR);
                    *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
                    return SNMP_FAILURE;
                }
            }
            return SNMP_SUCCESS;

        case CFA_RS_ACTIVE:
        case CFA_RS_NOTINSERVICE:
            /* This secondary IP address is not configured for the interfaace */
            if (CfaIpIfGetSecondaryAddressInfo
                (u4IfIndex, u4IfSecondaryIpAddress,
                 &SecondaryInfo) == CFA_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case CFA_RS_DESTROY:
#ifdef VRRP_WANTED
            if (CfaIpIfGetSecondaryAddressInfo
                (u4IfIndex, u4IfSecondaryIpAddress,
                 &SecondaryInfo) == CFA_SUCCESS)
            {
                u4TempAddr = OSIX_HTONL (SecondaryInfo.u4Addr);
                IPVX_ADDR_INIT_FROMV4 (CurIpAddr, u4TempAddr);

                if (VrrpApiValIfIpChange (u4IfIndex,
                                          CurIpAddr,
                                          u4CidrSubnetMask
                                          [SecondaryInfo.u1SubnetMask])
                    == VRRP_OK)
                {
                    CLI_SET_ERR (CLI_CFA_IP_NO_CHG_VRRP_INUSE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;

                }

                if (VrrpApiValDecPriority (u4IfIndex,
                                           (UINT1) VRRP_VAL_PRIO_OPER_IP_DEL,
                                           PrevIpAddr,
                                           CurIpAddr) == VRRP_NOT_OK)
                {
                    CLI_SET_ERR (CLI_CFA_IP_NO_CHG_VRRP_INUSE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
#endif

            return SNMP_SUCCESS;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhDepv2IfSecondaryIpAddressTable
 Input       :  The Indices
                IfMainIndex
                IfSecondaryIpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfSecondaryIpAddressTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IfMainExtTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIfMainExtTable
 Input       :  The Indices
                IfMainIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIfMainExtTable (INT4 i4IfMainIndex)
{
    if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIfMainExtTable
 Input       :  The Indices
                IfMainIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIfMainExtTable (INT4 *pi4IfMainIndex)
{
    if (nmhGetFirstIndexIfMainTable (pi4IfMainIndex) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIfMainExtTable
 Input       :  The Indices
                IfMainIndex
                nextIfMainIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIfMainExtTable (INT4 i4IfMainIndex, INT4 *pi4NextIfMainIndex)
{
    if (nmhGetNextIndexIfMainTable (i4IfMainIndex, pi4NextIfMainIndex) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIfMainExtMacAddress
 Input       :  The Indices
                IfMainIndex
                                                                                                The Object
                retValIfMainExtMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainExtMacAddress (INT4 i4IfMainIndex,
                           tMacAddr * pRetValIfMainExtMacAddress)
{
    UINT4               u4IfIndex;
    tMacAddr            au1MacAddr;

    MEMSET (au1MacAddr, 0, CFA_ENET_ADDR_LEN);
    u4IfIndex = (UINT4) i4IfMainIndex;

    /* Validate the given interface */
    if (nmhValidateIndexInstanceIfMainExtTable (i4IfMainIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /*Get the Mac address for the given port from CFA database */
    CfaGetIfHwAddr (u4IfIndex, au1MacAddr);
    MEMCPY (pRetValIfMainExtMacAddress, au1MacAddr, CFA_ENET_ADDR_LEN);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIfMainExtSysSpecificPortID
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfMainExtSysSpecificPortID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainExtSysSpecificPortID (INT4 i4IfMainIndex,
                                  UINT4 *pu4RetValIfMainExtSysSpecificPortID)
{
    CfaUtilIfGetSysSpecificPortID (i4IfMainIndex,
                                   pu4RetValIfMainExtSysSpecificPortID);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainExtInterfaceType
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfMainExtInterfaceType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainExtInterfaceType (INT4 i4IfMainIndex,
                              INT4 *pi4RetValIfMainExtInterfaceType)
{
    if ((i4IfMainIndex <= 0) ||
        (i4IfMainIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        /* Invalid interface index */
        return SNMP_SUCCESS;
    }
    if (CFA_IF_ENTRY_USED ((UINT4) i4IfMainIndex))
    {
        *pi4RetValIfMainExtInterfaceType =
            CFA_IF_IS_BACKPLANE ((UINT4) i4IfMainIndex);
        return SNMP_SUCCESS;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainExtPortSecState
 Input       :  The Indices
                IfMainIndex

                The Object
                retValIfMainExtPortSecState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainExtPortSecState (INT4 i4IfMainIndex,
                             INT4 *pi4RetValIfMainExtPortSecState)
{
    UINT1               u1PortSecState;

    if (CfaGetIfPortSecState ((UINT4) i4IfMainIndex,
                              &u1PortSecState) == CFA_SUCCESS)
    {
        *pi4RetValIfMainExtPortSecState = (INT4) u1PortSecState;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIfMainExtInPkts
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfMainExtInPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainExtInPkts (INT4 i4IfMainIndex, UINT4 *pu4RetValIfMainExtInPkts)
{
    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
    tCfaIfInfo         *pCfaIfInfo = NULL;
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (i4IfMainIndex);

    if (pCfaIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCfaIfInfo->u4IpIntfStats == CFA_DISABLED)
    {
        *pu4RetValIfMainExtInPkts = 0;
        return SNMP_SUCCESS;
    }
#endif

    u4IfIndex = (UINT4) i4IfMainIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        *pu4RetValIfMainExtInPkts = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    /* If L3 switching is disabled, there will not be any
     *      * hardware routing and hence we can rely on the software
     *           * to give us the correct values of l3 vlan counters*/
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        *pu4RetValIfMainExtInPkts = CFA_IF_GET_IN_PKTS (u4IfIndex);
        return SNMP_SUCCESS;
    }
#endif
    if (u4IfIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG)
    {
        CfaFsHwGetVlanIntfStats (u4IfIndex, (INT1) NP_STAT_IF_IN_PKTS,
                                 pu4RetValIfMainExtInPkts);
    }
    else
    {
        CfaFsHwGetStat (u4IfIndex, (INT1) NP_STAT_IF_IN_PKTS,
                        pu4RetValIfMainExtInPkts);
    }
#else
    *pu4RetValIfMainExtInPkts = CFA_IF_GET_IN_PKTS (u4IfIndex);
#endif
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIfMainExtMacAddress
 Input       :  The Indices
                IfMainIndex
                                                                         
                The Object
                setValIfMainExtMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainExtMacAddress (INT4 i4IfMainIndex,
                           tMacAddr SetValIfMainExtMacAddress)
{
    tMacAddr            au1MacAddr;
    tRcvAddressInfo    *pRcvAddrNode;
    UINT4               u4IfIndex;
    UINT1               u1IfType;

    u4IfIndex = (UINT4) i4IfMainIndex;

    if (CfaValidateCfaIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    MEMSET (au1MacAddr, 0, CFA_ENET_ADDR_LEN);

    /* Get the Macaddress from Cfa data Base */
    CfaGetIfHwAddr (u4IfIndex, au1MacAddr);

    /* Check for the Mac address from CFA is same as input MAC */

    if (MEMCMP (au1MacAddr, SetValIfMainExtMacAddress, CFA_ENET_ADDR_LEN) == 0)
    {
        return SNMP_SUCCESS;
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

    /* The configuration Mac address is allowed only
       for unicast MAC */
    if ((u1IfType == CFA_ENET) || (u1IfType == CFA_LAGG)
        || (u1IfType == CFA_PIP) || (u1IfType == CFA_BRIDGED_INTERFACE)
        || (u1IfType == CFA_OTHER) || (u1IfType == CFA_PSEUDO_WIRE))
    {
#ifdef NPAPI_WANTED
        /* check the NP_Programming according to the SetMacAddr */
        if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
        {
            if (FNP_FAILURE == CfaFsCfaHwSetMacAddr (u4IfIndex,
                                                     SetValIfMainExtMacAddress))
            {
                return SNMP_FAILURE;
            }
        }
#endif /* NPAPI_WANTED */

        CfaSetIfHwAddr (u4IfIndex, SetValIfMainExtMacAddress,
                        CFA_ENET_ADDR_LEN);
    }
    if (u1IfType == CFA_ENET)
    {
        if ((pRcvAddrNode =
             (tRcvAddressInfo *) CFA_IF_RCVADDRTAB_ENTRY (u4IfIndex)) == NULL)
        {
            return SNMP_FAILURE;
        }
        MEMCPY (pRcvAddrNode->au1Address, SetValIfMainExtMacAddress,
                CFA_ENET_ADDR_LEN);
    }

#ifdef LA_WANTED
    if (u1IfType == CFA_LAGG)
    {
        if (LaUpdateActorPortChannelMac ((UINT2) u4IfIndex,
                                         SetValIfMainExtMacAddress) != LA_TRUE)
        {
            return SNMP_FAILURE;
        }
    }
#endif /* LA_WANTED */

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfMainExtSysSpecificPortID
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfMainExtSysSpecificPortID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainExtSysSpecificPortID (INT4 i4IfMainIndex,
                                  UINT4 u4SetValIfMainExtSysSpecificPortID)
{

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    CFA_DS_LOCK ();
    CFA_CDB_IF_SYS_SPECIFIC_PORTID ((UINT4) i4IfMainIndex) =
        u4SetValIfMainExtSysSpecificPortID;
    CFA_DS_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfMainExtInterfaceType
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfMainExtInterfaceType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainExtInterfaceType (INT4 i4IfMainIndex,
                              INT4 i4SetValIfMainExtInterfaceType)
{

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((i4IfMainIndex > 0) &&
        (i4IfMainIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        if (!(CFA_IF_ENTRY_USED ((UINT4) i4IfMainIndex)))
        {
            /* Invalid interface index */
            return SNMP_FAILURE;
        }

        CFA_IF_IS_BACKPLANE ((UINT4) i4IfMainIndex) =
            (UINT1) i4SetValIfMainExtInterfaceType;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfMainExtPortSecState
 Input       :  The Indices
                IfMainIndex

                The Object
                setValIfMainExtPortSecState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainExtPortSecState (INT4 i4IfMainIndex,
                             INT4 i4SetValIfMainExtPortSecState)
{
    UINT2               u2IfIndex = 0;
    UINT1               u1PortSecState = 0;
    UINT1               u1IfType = 0;

    u2IfIndex = (UINT2) i4IfMainIndex;

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* If the interface is active then only the vlan module will know. So if
     * the row status is not active then return failure. */
    if (CFA_IF_RS (u2IfIndex) != CFA_RS_ACTIVE)
    {
        return SNMP_FAILURE;
    }

    CfaGetIfPortSecState ((UINT4) u2IfIndex, &u1PortSecState);

    if (u1PortSecState == (UINT1) i4SetValIfMainExtPortSecState)
    {
        return SNMP_SUCCESS;
    }

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);
    if ((u1IfType == CFA_ENET) || (u1IfType == CFA_LAGG))
    {
        /* Update port state now */
        CfaSetIfPortSecState ((UINT4) u2IfIndex,
                              (UINT1) i4SetValIfMainExtPortSecState);
    }
    else
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IfMainExtMacAddress
 Input       :  The Indices
                IfMainIndex
                                                                    
                The Object
                testValIfMainExtMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainExtMacAddress (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                              tMacAddr TestValIfMainExtMacAddress)
{
    tMacAddr            zeroAddr;
    tMacAddr            BcastMacAddr;
    UINT1               u1IfType;
    UINT4               u4IfIndex;
    UINT4               u4TempIfIndex = 1;
    tMacAddr            au1MacAddr;

    /* Check the mac address to be set is not a complete zero address
       00:00:00:00:00:00 or Multicast or Broadcast Address. */
    MEMSET (zeroAddr, 0, CFA_ENET_ADDR_LEN);
    MEMSET (BcastMacAddr, 0xff, CFA_ENET_ADDR_LEN);
    MEMSET (au1MacAddr, 0, CFA_ENET_ADDR_LEN);

    if ((MEMCMP (TestValIfMainExtMacAddress, zeroAddr,
                 CFA_ENET_ADDR_LEN) == 0) ||
        (CFA_IS_MAC_MULTICAST (TestValIfMainExtMacAddress)) ||
        (MEMCMP (TestValIfMainExtMacAddress, BcastMacAddr,
                 CFA_ENET_ADDR_LEN) == 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* for index validation - because validation is commented in mid-level */
    if (nmhValidateIndexInstanceIfMainExtTable (i4IfMainIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_CFA_INVALID_INTERFACE_MAC_ERR);
        return SNMP_FAILURE;
    }

    u4IfIndex = (UINT4) i4IfMainIndex;
    CFA_CDB_SCAN (u4TempIfIndex, SYS_DEF_MAX_PHYSICAL_INTERFACES,
                  CFA_ALL_IFTYPE)
    {
        CfaGetIfType (u4TempIfIndex, &u1IfType);
        if (u1IfType == CFA_ENET)
        {
            CfaGetIfHwAddr (u4TempIfIndex, au1MacAddr);
            if (MEMCMP
                (au1MacAddr, TestValIfMainExtMacAddress,
                 CFA_ENET_ADDR_LEN) == 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_CFA_DUPLICATE_MACADDR_ERR);
                return SNMP_FAILURE;
            }
        }
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

    /* Check whether the IfType is ENET or LAGG */
    if ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)
        && (u1IfType != CFA_PIP) && (u1IfType != CFA_BRIDGED_INTERFACE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_IFTYPE_MACADDR_ERR);
        return SNMP_FAILURE;
    }

    /* The Admin-Status should be down before setting
       Mac Address */
    if (CFA_IF_ADMIN (u4IfIndex) == CFA_IF_UP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_ADMIN_STATUS_MACADDR_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IfMainExtSysSpecificPortID
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfMainExtSysSpecificPortID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainExtSysSpecificPortID (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                                     UINT4 u4TestValIfMainExtSysSpecificPortID)
{
    UINT4               u4SysSpecificPortID = 0;

    if (CfaUtilIfGetSysSpecificPortID (i4IfMainIndex, &u4SysSpecificPortID) ==
        CFA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* we allow the system-specific portID configuration */
    /* only on bridged interfaces */
    if (CfaIfIsBridgedInterface (i4IfMainIndex) == CFA_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((u4TestValIfMainExtSysSpecificPortID > CFA_IF_MAX_SYS_SPECIFIC_PORTID)
        || (u4TestValIfMainExtSysSpecificPortID <= 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfMainExtInterfaceType
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfMainExtInterfaceType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainExtInterfaceType (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                                 INT4 i4TestValIfMainExtInterfaceType)
{
    /* Only physical ports can be configured as backplane interfaces.
     * */
    if ((i4IfMainIndex <= 0) ||
        (i4IfMainIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_CFA_BACKPLANE_ERR);
        return SNMP_FAILURE;
    }

    if (!CFA_IF_ENTRY_USED ((UINT4) i4IfMainIndex))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIfMainExtInterfaceType != CFA_FRONTPANEL_INTF) &&
        (i4TestValIfMainExtInterfaceType != CFA_BACKPLANE_INTF))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfMainExtPortSecState
 Input       :  The Indices
                IfMainIndex

                The Object
                testValIfMainExtPortSecState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainExtPortSecState (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                                INT4 i4TestValIfMainExtPortSecState)
{
    UINT1               u1IfType = 0;
    UINT2               u2IfIndex = 0;

    u2IfIndex = (UINT2) i4IfMainIndex;

    /* for index validation - because validation is commented in mid-level */
    if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    CfaGetIfType ((UINT4) u2IfIndex, &u1IfType);

    /* the IfType must have been specified first */
    /* PortSecState can be set only for ENET interfaces */
    if ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* the RowStatus should be active first */
    if (CFA_IF_RS (u2IfIndex) != CFA_RS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (CFA_IS_PORT_SEC_STATE_VALID (i4TestValIfMainExtPortSecState)
        == CFA_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IfIpTable
 Input       :  The Indices
                IfMainIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfIpTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2IfMainExtTable
 Input       :  The Indices
                IfMainIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfMainExtTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IfCustOpaqueAttrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIfCustOpaqueAttrTable
 Input       :  The Indices
                IfMainIndex
                IfCustOpaqueAttributeID
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIfCustOpaqueAttrTable (INT4 i4IfMainIndex,
                                               INT4 i4IfCustOpaqueAttributeID)
{

    if ((i4IfMainIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (i4IfMainIndex <= 0) ||
        (CfaIfIsBridgedInterface (i4IfMainIndex) == CFA_FALSE) ||
        (i4IfCustOpaqueAttributeID > CFA_MAX_OPAQUE_ATTRS) ||
        (i4IfCustOpaqueAttributeID <= 0))
    {
        return SNMP_FAILURE;
    }

    if (CfaIsIfEntryProgrammingAllowed ((UINT4) i4IfMainIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (!CFA_IF_OPQ_CONFIGURED
        ((UINT4) i4IfMainIndex, (UINT4) i4IfCustOpaqueAttributeID))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIfCustOpaqueAttrTable
 Input       :  The Indices
                IfMainIndex
                IfCustOpaqueAttributeID
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIfCustOpaqueAttrTable (INT4 *pi4IfMainIndex,
                                       INT4 *pi4IfCustOpaqueAttributeID)
{
    INT4                i4IfIndex = 1;
    INT4                i4OpqAttrId = 0;

    /* Get the First Used Index */
    CFA_CDB_SCAN_WITH_LOCK (i4IfIndex, BRG_MAX_PHY_PLUS_LOG_PORTS,
                            CFA_ALL_IFTYPE)
    {
        /* Flexible ifIndex changes 
         */
        CFA_DS_LOCK ();
        if ((CFA_CDB_IF_TYPE ((UINT4) i4IfIndex) != CFA_ENET) &&
            (CFA_CDB_IF_TYPE ((UINT4) i4IfIndex) != CFA_LAGG))
        {
            CFA_DS_UNLOCK ();
            continue;
        }
        CFA_DS_UNLOCK ();

        if (CfaIfIsBridgedInterface (i4IfIndex) == CFA_TRUE)
        {
            for (i4OpqAttrId = 1; i4OpqAttrId <= CFA_MAX_OPAQUE_ATTRS;
                 i4OpqAttrId++)
            {
                if (CFA_IF_OPQ_CONFIGURED
                    ((UINT4) i4IfIndex, (UINT4) i4OpqAttrId))
                {
                    *pi4IfMainIndex = i4IfIndex;
                    *pi4IfCustOpaqueAttributeID = i4OpqAttrId;
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIfCustOpaqueAttrTable
 Input       :  The Indices
                IfMainIndex
                nextIfMainIndex
                IfCustOpaqueAttributeID
                nextIfCustOpaqueAttributeID
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIfCustOpaqueAttrTable (INT4 i4IfMainIndex,
                                      INT4 *pi4NextIfMainIndex,
                                      INT4 i4IfCustOpaqueAttributeID,
                                      INT4 *pi4NextIfCustOpaqueAttributeID)
{
    INT4                i4IfIndex = 0;
    INT4                i4OpqAttrId = 0;
    INT4                i4OpqAttrCounter = 0;

    i4OpqAttrId = i4IfCustOpaqueAttributeID;

    /* Next Index should always return the next used Index */
    i4IfIndex = i4IfMainIndex;
    CFA_CDB_SCAN_WITH_LOCK (i4IfIndex, BRG_MAX_PHY_PLUS_LOG_PORTS,
                            CFA_ALL_IFTYPE)
    {
        /* Flexible ifIndex changes
         */
        CFA_DS_LOCK ();
        if ((CFA_CDB_IF_TYPE ((UINT4) i4IfIndex) != CFA_ENET) &&
            (CFA_CDB_IF_TYPE ((UINT4) i4IfIndex) != CFA_LAGG))
        {
            CFA_DS_UNLOCK ();
            continue;
        }
        CFA_DS_UNLOCK ();

        if (CfaIfIsBridgedInterface (i4IfIndex) == CFA_TRUE)
        {
            for (i4OpqAttrCounter = (i4OpqAttrId + 1);
                 i4OpqAttrCounter <= CFA_MAX_OPAQUE_ATTRS; i4OpqAttrCounter++)
            {
                if (CFA_IF_OPQ_CONFIGURED
                    ((UINT4) i4IfIndex, (UINT4) i4OpqAttrCounter))
                {
                    *pi4NextIfMainIndex = i4IfIndex;
                    *pi4NextIfCustOpaqueAttributeID = i4OpqAttrCounter;
                    return SNMP_SUCCESS;
                }
            }
        }
        i4OpqAttrId = 0;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIfCustOpaqueAttribute
 Input       :  The Indices
                IfMainIndex
                IfCustOpaqueAttributeID

                The Object 
                retValIfCustOpaqueAttribute
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfCustOpaqueAttribute (INT4 i4IfMainIndex, INT4 i4IfCustOpaqueAttributeID,
                             UINT4 *pu4RetValIfCustOpaqueAttribute)
{
    if (CfaIsIfEntryProgrammingAllowed ((UINT4) i4IfMainIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }
    if (!CFA_IF_OPQ_CONFIGURED
        ((UINT4) i4IfMainIndex, i4IfCustOpaqueAttributeID))
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIfCustOpaqueAttribute =
        CFA_IF_OPQ_ATTR ((UINT4) i4IfMainIndex, i4IfCustOpaqueAttributeID);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfCustOpaqueRowStatus
 Input       :  The Indices
                IfMainIndex
                IfCustOpaqueAttributeID

                The Object 
                retValIfCustOpaqueRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfCustOpaqueRowStatus (INT4 i4IfMainIndex, INT4 i4IfCustOpaqueAttributeID,
                             INT4 *pi4RetValIfCustOpaqueRowStatus)
{

    if (CfaIsIfEntryProgrammingAllowed ((UINT4) i4IfMainIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }
    if (!CFA_IF_OPQ_CONFIGURED
        ((UINT4) i4IfMainIndex, i4IfCustOpaqueAttributeID))
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIfCustOpaqueRowStatus =
        CFA_IF_OPQ_ATTR_RS ((UINT4) i4IfMainIndex, i4IfCustOpaqueAttributeID);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIfCustOpaqueAttribute
 Input       :  The Indices
                IfMainIndex
                IfCustOpaqueAttributeID

                The Object 
                setValIfCustOpaqueAttribute
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfCustOpaqueAttribute (INT4 i4IfMainIndex,
                             INT4 i4IfCustOpaqueAttributeID,
                             UINT4 u4SetValIfCustOpaqueAttribute)
{
#ifdef NPAPI_WANTED
    tHwCustIfParamVal   HwCustIfParamVal;
#endif

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (CfaIsIfEntryProgrammingAllowed ((UINT4) i4IfMainIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }
    if (CFA_IF_OPQ_ATTR ((UINT4) i4IfMainIndex, i4IfCustOpaqueAttributeID) ==
        u4SetValIfCustOpaqueAttribute)
    {
        return SNMP_SUCCESS;
    }
#ifdef NPAPI_WANTED
    MEMSET (&HwCustIfParamVal, 0, sizeof (tHwCustIfParamVal));

    /* if the value is altered in active state ,  give the value to hardware */
    if (CFA_IF_OPQ_ATTR_RS (i4IfMainIndex, i4IfCustOpaqueAttributeID)
        == CFA_RS_ACTIVE)
    {
        HwCustIfParamVal.OpaqueAttrbs.u4AttribID =
            (UINT4) i4IfCustOpaqueAttributeID;
        HwCustIfParamVal.OpaqueAttrbs.u4Attribute =
            u4SetValIfCustOpaqueAttribute;
        if (FsHwSetCustIfParams
            ((UINT4) i4IfMainIndex, NP_CUST_IF_PRM_TYPE_OPQ_ATTR,
             HwCustIfParamVal, NP_ENTRY_ADD) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif
    CFA_IF_OPQ_ATTR ((UINT4) i4IfMainIndex, i4IfCustOpaqueAttributeID) =
        u4SetValIfCustOpaqueAttribute;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfCustOpaqueRowStatus
 Input       :  The Indices
                IfMainIndex
                IfCustOpaqueAttributeID

                The Object 
                setValIfCustOpaqueRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfCustOpaqueRowStatus (INT4 i4IfMainIndex,
                             INT4 i4IfCustOpaqueAttributeID,
                             INT4 i4SetValIfCustOpaqueRowStatus)
{
#ifdef NPAPI_WANTED
    tHwCustIfParamVal   HwCustIfParamVal;

    MEMSET (&HwCustIfParamVal, 0, sizeof (tHwCustIfParamVal));
#endif

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (CfaIsIfEntryProgrammingAllowed ((UINT4) i4IfMainIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }
    switch (i4SetValIfCustOpaqueRowStatus)
    {
        case CFA_RS_CREATEANDWAIT:
            CFA_IF_OPQ_ATTR_RS ((UINT4) i4IfMainIndex,
                                i4IfCustOpaqueAttributeID) = CFA_RS_NOTREADY;
            break;
        case CFA_RS_CREATEANDGO:
            /*If CREATE_AND_GO is given the attribute value will be taken as zero */
#ifdef NPAPI_WANTED
            HwCustIfParamVal.OpaqueAttrbs.u4AttribID =
                (UINT4) i4IfCustOpaqueAttributeID;
            HwCustIfParamVal.OpaqueAttrbs.u4Attribute = 0;
            if (FsHwSetCustIfParams
                ((UINT4) i4IfMainIndex, NP_CUST_IF_PRM_TYPE_OPQ_ATTR,
                 HwCustIfParamVal, NP_ENTRY_ADD) == FNP_FAILURE)
            {
                return SNMP_FAILURE;
            }
#endif
            CFA_IF_OPQ_ATTR_RS ((UINT4) i4IfMainIndex,
                                i4IfCustOpaqueAttributeID) = CFA_RS_ACTIVE;
            break;
        case CFA_RS_ACTIVE:
            if (CFA_IF_OPQ_ATTR_RS
                ((UINT4) i4IfMainIndex,
                 i4IfCustOpaqueAttributeID) == CFA_RS_ACTIVE)
            {
                return SNMP_SUCCESS;
            }
#ifdef NPAPI_WANTED
            HwCustIfParamVal.OpaqueAttrbs.u4AttribID =
                (UINT4) i4IfCustOpaqueAttributeID;
            HwCustIfParamVal.OpaqueAttrbs.u4Attribute =
                CFA_IF_OPQ_ATTR (i4IfMainIndex, i4IfCustOpaqueAttributeID);
            if (FsHwSetCustIfParams
                ((UINT4) i4IfMainIndex, NP_CUST_IF_PRM_TYPE_OPQ_ATTR,
                 HwCustIfParamVal, NP_ENTRY_ADD) == FNP_FAILURE)
            {
                return SNMP_FAILURE;
            }
#endif
            CFA_IF_OPQ_ATTR_RS ((UINT4) i4IfMainIndex,
                                i4IfCustOpaqueAttributeID) = CFA_RS_ACTIVE;
            break;
        case CFA_RS_DESTROY:
            if (!CFA_IF_OPQ_CONFIGURED
                ((UINT4) i4IfMainIndex, i4IfCustOpaqueAttributeID))
            {
                return SNMP_SUCCESS;
            }
#ifdef NPAPI_WANTED
            if (CFA_IF_OPQ_ATTR_RS (i4IfMainIndex, i4IfCustOpaqueAttributeID)
                == CFA_RS_ACTIVE)
            {
                HwCustIfParamVal.OpaqueAttrbs.u4AttribID =
                    (UINT4) i4IfCustOpaqueAttributeID;
                HwCustIfParamVal.OpaqueAttrbs.u4Attribute =
                    CFA_IF_OPQ_ATTR (i4IfMainIndex, i4IfCustOpaqueAttributeID);
                if (FsHwSetCustIfParams
                    ((UINT4) i4IfMainIndex, NP_CUST_IF_PRM_TYPE_OPQ_ATTR,
                     HwCustIfParamVal, NP_ENTRY_DELETE) == FNP_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
#endif
            CFA_IF_OPQ_ATTR ((UINT4) i4IfMainIndex, i4IfCustOpaqueAttributeID) =
                0;
            CFA_IF_OPQ_ATTR_RS ((UINT4) i4IfMainIndex,
                                i4IfCustOpaqueAttributeID) = CFA_RS_INVALID;
            break;
        default:
            break;

    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IfCustOpaqueAttribute
 Input       :  The Indices
                IfMainIndex
                IfCustOpaqueAttributeID

                The Object 
                testValIfCustOpaqueAttribute
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfCustOpaqueAttribute (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                                INT4 i4IfCustOpaqueAttributeID,
                                UINT4 u4TestValIfCustOpaqueAttribute)
{

    UNUSED_PARAM (u4TestValIfCustOpaqueAttribute);

    if ((i4IfMainIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (i4IfMainIndex <= 0) ||
        (i4IfCustOpaqueAttributeID > CFA_MAX_OPAQUE_ATTRS) ||
        (i4IfCustOpaqueAttributeID <= 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (CfaIfIsBridgedInterface (i4IfMainIndex) == CFA_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (!CFA_IF_OPQ_CONFIGURED
        ((UINT4) i4IfMainIndex, i4IfCustOpaqueAttributeID))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfCustOpaqueRowStatus
 Input       :  The Indices
                IfMainIndex
                IfCustOpaqueAttributeID

                The Object 
                testValIfCustOpaqueRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfCustOpaqueRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                                INT4 i4IfCustOpaqueAttributeID,
                                INT4 i4TestValIfCustOpaqueRowStatus)
{

    if ((i4IfMainIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (i4IfMainIndex <= 0) ||
        (i4IfCustOpaqueAttributeID > CFA_MAX_OPAQUE_ATTRS) ||
        (i4IfCustOpaqueAttributeID <= 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (CfaIfIsBridgedInterface (i4IfMainIndex) == CFA_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    switch (i4TestValIfCustOpaqueRowStatus)
    {
        case CFA_RS_CREATEANDGO:
        case CFA_RS_CREATEANDWAIT:
            if (CFA_IF_OPQ_ATTR_RS
                ((UINT4) i4IfMainIndex,
                 i4IfCustOpaqueAttributeID) != CFA_RS_INVALID)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        case CFA_RS_ACTIVE:
            if (!CFA_IF_OPQ_CONFIGURED
                ((UINT4) i4IfMainIndex, i4IfCustOpaqueAttributeID))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        case CFA_RS_NOTINSERVICE:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            break;
        case CFA_RS_DESTROY:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : IfWanTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceIfWanTable
 Input       :  The Indices
                IfMainIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIfWanTable (INT4 i4IfMainIndex)
{
    UNUSED_PARAM (i4IfMainIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIfWanTable
 Input       :  The Indices
                IfMainIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIfWanTable (INT4 *pi4IfMainIndex)
{
    UNUSED_PARAM (pi4IfMainIndex);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIfWanTable
 Input       :  The Indices
                IfMainIndex
                nextIfMainIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIfWanTable (INT4 i4IfMainIndex, INT4 *pi4NextIfMainIndex)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (pi4NextIfMainIndex);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIfWanInterfaceType
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfWanInterfaceType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfWanInterfaceType (INT4 i4IfMainIndex, INT4 *pi4RetValIfWanInterfaceType)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (pi4RetValIfWanInterfaceType);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfWanConnectionType
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfWanConnectionType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfWanConnectionType (INT4 i4IfMainIndex,
                           INT4 *pi4RetValIfWanConnectionType)
{

    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (pi4RetValIfWanConnectionType);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfWanVirtualPathId
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfWanVirtualPathId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfWanVirtualPathId (INT4 i4IfMainIndex, INT4 *pi4RetValIfWanVirtualPathId)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (pi4RetValIfWanVirtualPathId);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfWanVirtualCircuitId
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfWanVirtualCircuitId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfWanVirtualCircuitId (INT4 i4IfMainIndex,
                             INT4 *pi4RetValIfWanVirtualCircuitId)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (pi4RetValIfWanVirtualCircuitId);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfWanPeerMediaAddress
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfWanPeerMediaAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfWanPeerMediaAddress (INT4 i4IfMainIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValIfWanPeerMediaAddress)
{

    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (pRetValIfWanPeerMediaAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfWanSustainedSpeed
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfWanSustainedSpeed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfWanSustainedSpeed (INT4 i4IfMainIndex,
                           INT4 *pi4RetValIfWanSustainedSpeed)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (pi4RetValIfWanSustainedSpeed);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfWanPeakSpeed
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfWanPeakSpeed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfWanPeakSpeed (INT4 i4IfMainIndex, INT4 *pi4RetValIfWanPeakSpeed)
{

    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (pi4RetValIfWanPeakSpeed);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfWanMaxBurstSize
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfWanMaxBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfWanMaxBurstSize (INT4 i4IfMainIndex, INT4 *pi4RetValIfWanMaxBurstSize)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (pi4RetValIfWanMaxBurstSize);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfWanIpQosProfileIndex
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfWanIpQosProfileIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfWanIpQosProfileIndex (INT4 i4IfMainIndex,
                              INT4 *pi4RetValIfWanIpQosProfileIndex)
{

    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (pi4RetValIfWanIpQosProfileIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfWanIdleTimeout
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfWanIdleTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfWanIdleTimeout (INT4 i4IfMainIndex, INT4 *pi4RetValIfWanIdleTimeout)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (pi4RetValIfWanIdleTimeout);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfWanPeerIpAddr
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfWanPeerIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfWanPeerIpAddr (INT4 i4IfMainIndex, UINT4 *pu4RetValIfWanPeerIpAddr)
{

    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (pu4RetValIfWanPeerIpAddr);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfWanRtpHdrComprEnable
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfWanRtpHdrComprEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfWanRtpHdrComprEnable (INT4 i4IfMainIndex,
                              INT4 *pi4RetValIfWanRtpHdrComprEnable)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (pi4RetValIfWanRtpHdrComprEnable);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfWanPersistence
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfWanPersistence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfWanPersistence (INT4 i4IfMainIndex, INT4 *pi4RetValIfWanPersistence)
{

    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (pi4RetValIfWanPersistence);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIfWanConnectionType
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfWanConnectionType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfWanConnectionType (INT4 i4IfMainIndex, INT4 i4SetValIfWanConnectionType)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4SetValIfWanConnectionType);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfWanVirtualPathId
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfWanVirtualPathId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfWanVirtualPathId (INT4 i4IfMainIndex, INT4 i4SetValIfWanVirtualPathId)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4SetValIfWanVirtualPathId);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfWanVirtualCircuitId
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfWanVirtualCircuitId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfWanVirtualCircuitId (INT4 i4IfMainIndex,
                             INT4 i4SetValIfWanVirtualCircuitId)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4SetValIfWanVirtualCircuitId);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfWanPeerMediaAddress
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfWanPeerMediaAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfWanPeerMediaAddress (INT4 i4IfMainIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValIfWanPeerMediaAddress)
{

    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (pSetValIfWanPeerMediaAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfWanSustainedSpeed
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfWanSustainedSpeed
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfWanSustainedSpeed (INT4 i4IfMainIndex, INT4 i4SetValIfWanSustainedSpeed)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4SetValIfWanSustainedSpeed);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfWanPeakSpeed
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfWanPeakSpeed
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfWanPeakSpeed (INT4 i4IfMainIndex, INT4 i4SetValIfWanPeakSpeed)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4SetValIfWanPeakSpeed);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfWanMaxBurstSize
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfWanMaxBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfWanMaxBurstSize (INT4 i4IfMainIndex, INT4 i4SetValIfWanMaxBurstSize)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4SetValIfWanMaxBurstSize);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfWanIpQosProfileIndex
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfWanIpQosProfileIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfWanIpQosProfileIndex (INT4 i4IfMainIndex,
                              INT4 i4SetValIfWanIpQosProfileIndex)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4SetValIfWanIpQosProfileIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfWanIdleTimeout
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfWanIdleTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfWanIdleTimeout (INT4 i4IfMainIndex, INT4 i4SetValIfWanIdleTimeout)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4SetValIfWanIdleTimeout);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfWanPeerIpAddr
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfWanPeerIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfWanPeerIpAddr (INT4 i4IfMainIndex, UINT4 u4SetValIfWanPeerIpAddr)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (u4SetValIfWanPeerIpAddr);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfWanRtpHdrComprEnable
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfWanRtpHdrComprEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfWanRtpHdrComprEnable (INT4 i4IfMainIndex,
                              INT4 i4SetValIfWanRtpHdrComprEnable)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4SetValIfWanRtpHdrComprEnable);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfWanPersistence
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfWanPersistence
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfWanPersistence (INT4 i4IfMainIndex, INT4 i4SetValIfWanPersistence)
{

    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4SetValIfWanPersistence);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IfWanConnectionType
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfWanConnectionType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfWanConnectionType (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                              INT4 i4TestValIfWanConnectionType)
{
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4TestValIfWanConnectionType);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfWanVirtualPathId
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfWanVirtualPathId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfWanVirtualPathId (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                             INT4 i4TestValIfWanVirtualPathId)
{
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4TestValIfWanVirtualPathId);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfWanVirtualCircuitId
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfWanVirtualCircuitId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfWanVirtualCircuitId (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                                INT4 i4TestValIfWanVirtualCircuitId)
{
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4TestValIfWanVirtualCircuitId);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfWanPeerMediaAddress
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfWanPeerMediaAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfWanPeerMediaAddress (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValIfWanPeerMediaAddress)
{
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (pTestValIfWanPeerMediaAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfWanSustainedSpeed
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfWanSustainedSpeed
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfWanSustainedSpeed (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                              INT4 i4TestValIfWanSustainedSpeed)
{
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4TestValIfWanSustainedSpeed);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfWanPeakSpeed
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfWanPeakSpeed
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfWanPeakSpeed (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                         INT4 i4TestValIfWanPeakSpeed)
{
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4TestValIfWanPeakSpeed);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfWanMaxBurstSize
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfWanMaxBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfWanMaxBurstSize (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                            INT4 i4TestValIfWanMaxBurstSize)
{
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4TestValIfWanMaxBurstSize);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfWanIpQosProfileIndex
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfWanIpQosProfileIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfWanIpQosProfileIndex (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                                 INT4 i4TestValIfWanIpQosProfileIndex)
{
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4TestValIfWanIpQosProfileIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfWanIdleTimeout
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfWanIdleTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfWanIdleTimeout (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                           INT4 i4TestValIfWanIdleTimeout)
{
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4TestValIfWanIdleTimeout);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfWanPeerIpAddr
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfWanPeerIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfWanPeerIpAddr (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                          UINT4 u4TestValIfWanPeerIpAddr)
{
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (u4TestValIfWanPeerIpAddr);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfWanRtpHdrComprEnable
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfWanRtpHdrComprEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfWanRtpHdrComprEnable (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                                 INT4 i4TestValIfWanRtpHdrComprEnable)
{
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4TestValIfWanRtpHdrComprEnable);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfWanPersistence
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfWanPersistence
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfWanPersistence (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                           INT4 i4TestValIfWanPersistence)
{
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4TestValIfWanPersistence);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IfWanTable
 Input       :  The Indices
                IfMainIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfWanTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IfAutoCktProfileTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIfAutoCktProfileTable
 Input       :  The Indices
                IfAutoProfileIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIfAutoCktProfileTable (INT4 i4IfAutoProfileIfIndex)
{
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIfAutoCktProfileTable
 Input       :  The Indices
                IfAutoProfileIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIfAutoCktProfileTable (INT4 *pi4IfAutoProfileIfIndex)
{
    UNUSED_PARAM (pi4IfAutoProfileIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIfAutoCktProfileTable
 Input       :  The Indices
                IfAutoProfileIfIndex
                nextIfAutoProfileIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIfAutoCktProfileTable (INT4 i4IfAutoProfileIfIndex,
                                      INT4 *pi4NextIfAutoProfileIfIndex)
{
    UNUSED_PARAM (pi4NextIfAutoProfileIfIndex);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIfAutoProfileIfType
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                retValIfAutoProfileIfType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfAutoProfileIfType (INT4 i4IfAutoProfileIfIndex,
                           INT4 *pi4RetValIfAutoProfileIfType)
{
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    UNUSED_PARAM (pi4RetValIfAutoProfileIfType);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfAutoProfileIpAddrAllocMethod
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                retValIfAutoProfileIpAddrAllocMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfAutoProfileIpAddrAllocMethod (INT4 i4IfAutoProfileIfIndex,
                                      INT4
                                      *pi4RetValIfAutoProfileIpAddrAllocMethod)
{
    UNUSED_PARAM (pi4RetValIfAutoProfileIpAddrAllocMethod);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfAutoProfileDefIpSubnetMask
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                retValIfAutoProfileDefIpSubnetMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfAutoProfileDefIpSubnetMask (INT4 i4IfAutoProfileIfIndex,
                                    UINT4
                                    *pu4RetValIfAutoProfileDefIpSubnetMask)
{
    UNUSED_PARAM (pu4RetValIfAutoProfileDefIpSubnetMask);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfAutoProfileDefIpBroadcastAddr
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                retValIfAutoProfileDefIpBroadcastAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfAutoProfileDefIpBroadcastAddr (INT4 i4IfAutoProfileIfIndex,
                                       UINT4
                                       *pu4RetValIfAutoProfileDefIpBroadcastAddr)
{
    UNUSED_PARAM (pu4RetValIfAutoProfileDefIpBroadcastAddr);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfAutoProfileIdleTimeout
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                retValIfAutoProfileIdleTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfAutoProfileIdleTimeout (INT4 i4IfAutoProfileIfIndex,
                                INT4 *pi4RetValIfAutoProfileIdleTimeout)
{
    UNUSED_PARAM (pi4RetValIfAutoProfileIdleTimeout);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfAutoProfileEncapType
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                retValIfAutoProfileEncapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfAutoProfileEncapType (INT4 i4IfAutoProfileIfIndex,
                              INT4 *pi4RetValIfAutoProfileEncapType)
{
    UNUSED_PARAM (pi4RetValIfAutoProfileEncapType);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfAutoProfileIpQosProfileIndex
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                retValIfAutoProfileIpQosProfileIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfAutoProfileIpQosProfileIndex (INT4 i4IfAutoProfileIfIndex,
                                      INT4
                                      *pi4RetValIfAutoProfileIpQosProfileIndex)
{
    UNUSED_PARAM (pi4RetValIfAutoProfileIpQosProfileIndex);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfAutoProfileRowStatus
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                retValIfAutoProfileRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfAutoProfileRowStatus (INT4 i4IfAutoProfileIfIndex,
                              INT4 *pi4RetValIfAutoProfileRowStatus)
{
    UNUSED_PARAM (pi4RetValIfAutoProfileRowStatus);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIfAutoProfileIpAddrAllocMethod
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                setValIfAutoProfileIpAddrAllocMethod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfAutoProfileIpAddrAllocMethod (INT4 i4IfAutoProfileIfIndex,
                                      INT4
                                      i4SetValIfAutoProfileIpAddrAllocMethod)
{
    UNUSED_PARAM (i4SetValIfAutoProfileIpAddrAllocMethod);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfAutoProfileDefIpSubnetMask
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                setValIfAutoProfileDefIpSubnetMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfAutoProfileDefIpSubnetMask (INT4 i4IfAutoProfileIfIndex,
                                    UINT4 u4SetValIfAutoProfileDefIpSubnetMask)
{
    UNUSED_PARAM (u4SetValIfAutoProfileDefIpSubnetMask);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfAutoProfileDefIpBroadcastAddr
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                setValIfAutoProfileDefIpBroadcastAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfAutoProfileDefIpBroadcastAddr (INT4 i4IfAutoProfileIfIndex,
                                       UINT4
                                       u4SetValIfAutoProfileDefIpBroadcastAddr)
{
    UNUSED_PARAM (u4SetValIfAutoProfileDefIpBroadcastAddr);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfAutoProfileIdleTimeout
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                setValIfAutoProfileIdleTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfAutoProfileIdleTimeout (INT4 i4IfAutoProfileIfIndex,
                                INT4 i4SetValIfAutoProfileIdleTimeout)
{
    UNUSED_PARAM (i4SetValIfAutoProfileIdleTimeout);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfAutoProfileEncapType
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                setValIfAutoProfileEncapType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfAutoProfileEncapType (INT4 i4IfAutoProfileIfIndex,
                              INT4 i4SetValIfAutoProfileEncapType)
{
    UNUSED_PARAM (i4SetValIfAutoProfileEncapType);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfAutoProfileIpQosProfileIndex
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                setValIfAutoProfileIpQosProfileIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfAutoProfileIpQosProfileIndex (INT4 i4IfAutoProfileIfIndex,
                                      INT4
                                      i4SetValIfAutoProfileIpQosProfileIndex)
{
    UNUSED_PARAM (i4SetValIfAutoProfileIpQosProfileIndex);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfAutoProfileRowStatus
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                setValIfAutoProfileRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfAutoProfileRowStatus (INT4 i4IfAutoProfileIfIndex,
                              INT4 i4SetValIfAutoProfileRowStatus)
{
    UNUSED_PARAM (i4SetValIfAutoProfileRowStatus);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IfAutoProfileIpAddrAllocMethod
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                testValIfAutoProfileIpAddrAllocMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfAutoProfileIpAddrAllocMethod (UINT4 *pu4ErrorCode,
                                         INT4 i4IfAutoProfileIfIndex,
                                         INT4
                                         i4TestValIfAutoProfileIpAddrAllocMethod)
{
    UNUSED_PARAM (i4TestValIfAutoProfileIpAddrAllocMethod);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfAutoProfileDefIpSubnetMask
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                testValIfAutoProfileDefIpSubnetMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfAutoProfileDefIpSubnetMask (UINT4 *pu4ErrorCode,
                                       INT4 i4IfAutoProfileIfIndex,
                                       UINT4
                                       u4TestValIfAutoProfileDefIpSubnetMask)
{
    UNUSED_PARAM (u4TestValIfAutoProfileDefIpSubnetMask);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfAutoProfileDefIpBroadcastAddr
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                testValIfAutoProfileDefIpBroadcastAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfAutoProfileDefIpBroadcastAddr (UINT4 *pu4ErrorCode,
                                          INT4 i4IfAutoProfileIfIndex,
                                          UINT4
                                          u4TestValIfAutoProfileDefIpBroadcastAddr)
{
    UNUSED_PARAM (u4TestValIfAutoProfileDefIpBroadcastAddr);
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfAutoProfileIdleTimeout
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                testValIfAutoProfileIdleTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfAutoProfileIdleTimeout (UINT4 *pu4ErrorCode,
                                   INT4 i4IfAutoProfileIfIndex,
                                   INT4 i4TestValIfAutoProfileIdleTimeout)
{
    UNUSED_PARAM (i4TestValIfAutoProfileIdleTimeout);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfAutoProfileEncapType
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                testValIfAutoProfileEncapType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfAutoProfileEncapType (UINT4 *pu4ErrorCode,
                                 INT4 i4IfAutoProfileIfIndex,
                                 INT4 i4TestValIfAutoProfileEncapType)
{
    UNUSED_PARAM (i4TestValIfAutoProfileEncapType);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfAutoProfileIpQosProfileIndex
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                testValIfAutoProfileIpQosProfileIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfAutoProfileIpQosProfileIndex (UINT4 *pu4ErrorCode,
                                         INT4 i4IfAutoProfileIfIndex,
                                         INT4
                                         i4TestValIfAutoProfileIpQosProfileIndex)
{
    UNUSED_PARAM (i4TestValIfAutoProfileIpQosProfileIndex);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfAutoProfileRowStatus
 Input       :  The Indices
                IfAutoProfileIfIndex

                The Object 
                testValIfAutoProfileRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfAutoProfileRowStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4IfAutoProfileIfIndex,
                                 INT4 i4TestValIfAutoProfileRowStatus)
{
    UNUSED_PARAM (i4TestValIfAutoProfileRowStatus);
    UNUSED_PARAM (i4IfAutoProfileIfIndex);
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IfAutoCktProfileTable
 Input       :  The Indices
                IfAutoProfileIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfAutoCktProfileTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IfIvrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIfIvrTable
 Input       :  The Indices
                IfMainIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIfIvrTable (INT4 i4IfMainIndex)
{

    if (((UINT4) i4IfMainIndex > CFA_MAX_INTERFACES ()) || (i4IfMainIndex <= 0))
    {
        return (SNMP_FAILURE);
    }

    if (CfaValidateIfIndex ((UINT4) i4IfMainIndex) == CFA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIfIvrTable
 Input       :  The Indices
                IfMainIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIfIvrTable (INT4 *pi4IfMainIndex)
{
    UINT4               u4Index = 1;

    CFA_CDB_SCAN_WITH_LOCK (u4Index, SYS_DEF_MAX_INTERFACES, CFA_ALL_IFTYPE)
    {
        /* Flexible ifIndex changes
         */
        CFA_DS_LOCK ();
        if (((CFA_CDB_IF_TYPE (u4Index) == CFA_PROP_VIRTUAL_INTERFACE) &&
             (CFA_CDB_IF_SUB_TYPE (u4Index) == CFA_SUBTYPE_SISP_INTERFACE)) ||
            (CFA_CDB_IF_BRIDGE_PORT_TYPE (u4Index) ==
             CFA_VIRTUAL_INSTANCE_PORT))
        {
            CFA_DS_UNLOCK ();
            continue;
        }
        CFA_DS_UNLOCK ();

        if (CfaValidateIfIndex (u4Index) == CFA_SUCCESS)
        {
            *pi4IfMainIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIfIvrTable
 Input       :  The Indices
                IfMainIndex
                nextIfMainIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIfIvrTable (INT4 i4IfMainIndex, INT4 *pi4NextIfMainIndex)
{
    UINT4               u4Counter;

    if ((i4IfMainIndex < 0) || ((UINT4) i4IfMainIndex > SYS_DEF_MAX_INTERFACES))
    {
        return SNMP_FAILURE;
    }

    u4Counter = (UINT4) (i4IfMainIndex + 1);

    CFA_CDB_SCAN_WITH_LOCK (u4Counter, SYS_DEF_MAX_INTERFACES, CFA_ALL_IFTYPE)
    {
        /* Flexible ifIndex changes
         */
        CFA_DS_LOCK ();
        if (((CFA_CDB_IF_TYPE (u4Counter) == CFA_PROP_VIRTUAL_INTERFACE) &&
             (CFA_CDB_IF_SUB_TYPE (u4Counter) == CFA_SUBTYPE_SISP_INTERFACE)) ||
            (CFA_CDB_IF_BRIDGE_PORT_TYPE (u4Counter) ==
             CFA_VIRTUAL_INSTANCE_PORT))
        {
            CFA_DS_UNLOCK ();
            continue;
        }
        CFA_DS_UNLOCK ();

        if (CfaValidateIfIndex (u4Counter) == CFA_SUCCESS)
        {
            *pi4NextIfMainIndex = (INT4) u4Counter;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIfIvrBridgedIface
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfIvrBridgedIface
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfIvrBridgedIface (INT4 i4IfMainIndex, INT4 *pi4RetValIfIvrBridgedIface)
{
    UINT1               u1BridgedIfaceStatus;

    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        CfaGetIfBridgedIfaceStatus ((UINT4) i4IfMainIndex,
                                    &u1BridgedIfaceStatus);

        *pi4RetValIfIvrBridgedIface = (INT4) u1BridgedIfaceStatus;
    }

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIfIvrBridgedIface
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfIvrBridgedIface
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfIvrBridgedIface (INT4 i4IfMainIndex, INT4 i4SetValIfIvrBridgedIface)
{
    UINT1               u1BridgedIfaceStatus = CFA_DISABLED;
    UINT1               u1IfType = 0;
    UINT2               au2ConfPorts[L2IWF_MAX_PORTS_PER_CONTEXT];
    UINT2               u2NumPorts = 0;
    UINT2               u2Index = 0;
    UINT2               u2AggId = 0;
    UINT1               u1AggIfaceStatus = CFA_ENABLED;
    UINT4               u4UfdGroupId = 0;
#ifdef LNXIP4_WANTED
    INT4                i4ProxyArpAdminStatus = 0;
#endif

    MEMSET (au2ConfPorts, 0, sizeof (au2ConfPorts));
    CfaGetIfType ((UINT4) i4IfMainIndex, &u1IfType);

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (gu4IsIvrEnabled == CFA_DISABLED)
    {
        return SNMP_FAILURE;
    }

    /* Null check has to be provided here to avoid crash as MSR does not call
     * Test routine */
    if (CfaIsIfEntryProgrammingAllowed ((UINT4) i4IfMainIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }
    else if (CFA_IF_ENTRY ((UINT2) i4IfMainIndex) == NULL)
    {
        return SNMP_FAILURE;
    }
    if (i4SetValIfIvrBridgedIface == CFA_DISABLED)
    {
        if (CfaGetIfUfdGroupId ((UINT4) i4IfMainIndex, &u4UfdGroupId) !=
            CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ALL_TRACK, CFA_IFM,
                     "In nmhSetIfIvrBridgedIface  - Getting \
                    the UFD Group Id failed\r\n ");
        }
        if (u4UfdGroupId != 0)
        {

            /* Throw an error if user is trying to change the last downlink port
             *  as router port when ufd is in enabled state. */
            if ((CfaUfdCheckLastDownlinkInGroup ((UINT4) i4IfMainIndex) !=
                 OSIX_FALSE) && (CFA_UFD_MODULE_STATUS () == CFA_UFD_ENABLED))
            {
                CLI_SET_ERR (CLI_CFA_UFD_LAST_DOWNLINK_RPORT_ERR);
                return SNMP_FAILURE;
            }
        }

        CfaCleanUpLinkUpPortStatus ((UINT4) i4IfMainIndex);
    }

    CfaGetIfBridgedIfaceStatus ((UINT4) i4IfMainIndex, &u1BridgedIfaceStatus);

    /* Check if the port is a member of port-channel. If so, validate the protocol type */
    if (L2IwfIsPortInPortChannel ((UINT4) i4IfMainIndex) == CFA_SUCCESS)
    {
        L2IwfGetPortChannelForPort ((UINT4) i4IfMainIndex, &u2AggId);
        CfaGetIfBridgedIfaceStatus ((UINT4) u2AggId, &u1AggIfaceStatus);
        if (u1AggIfaceStatus != i4SetValIfIvrBridgedIface)
        {
            CLI_SET_ERR (CFA_CLI_RTR_PORT_AGG_MISMATCH_ERR);
            return SNMP_FAILURE;
        }
    }

    if ((INT4) u1BridgedIfaceStatus == i4SetValIfIvrBridgedIface)
    {
        return SNMP_SUCCESS;
    }
    CfaSetIfBridgedIfaceStatus ((UINT4) i4IfMainIndex,
                                (UINT1) i4SetValIfIvrBridgedIface);

    if (u1IfType == CFA_LAGG)
    {
        if (L2IwfGetConfiguredPortsForPortChannel ((UINT2) i4IfMainIndex,
                                                   au2ConfPorts,
                                                   &u2NumPorts)
            != L2IWF_FAILURE)
        {
            for (u2Index = 0; u2Index < u2NumPorts; u2Index++)
            {
                CfaSetIfBridgedIfaceStatus (au2ConfPorts[u2Index],
                                            (UINT1) i4SetValIfIvrBridgedIface);
                CfaIfmUpdateInterfaceStatus
                    ((UINT4) au2ConfPorts[u2Index],
                     (UINT2) i4SetValIfIvrBridgedIface);
                L2IwfDeleteAndAddPortToPortChannel ((UINT4)
                                                    au2ConfPorts[u2Index],
                                                    (UINT2) i4IfMainIndex);

            }
        }
        if (u2NumPorts > L2IWF_MAX_PORTS_PER_CONTEXT)
        {
            return CFA_FAILURE;
        }
    }
    if (CfaIfmUpdateInterfaceStatus
        ((UINT4) i4IfMainIndex,
         (UINT2) i4SetValIfIvrBridgedIface) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#ifdef LNXIP4_WANTED
    if (LnxIpGetProxyArpAdminStatus (i4IfMainIndex, &i4ProxyArpAdminStatus)
        == NETIPV4_SUCCESS)
    {
        if (LnxIpSetProxyArpAdminStatus (i4IfMainIndex, CFA_PROXY_ARP_DISABLE)
            == NETIPV4_FAILURE)
        {
            CLI_SET_ERR (CLI_CFA_PROXYARP_NOT_DELETED_ERR);
            return SNMP_FAILURE;

        }
    }
#endif

    if (i4SetValIfIvrBridgedIface == CFA_DISABLED)
    {

        if (u4UfdGroupId != 0)
        {
            /* Remove all the members from the UFD if user is changing
             * the last downlink port when ufd is in disabled state */
            if (CfaUfdCheckLastDownlinkInGroup ((UINT4) i4IfMainIndex) !=
                OSIX_FALSE)
            {
                CfaUfdRemoveAllPortsInGroup (u4UfdGroupId);
            }
            else
            {
                /* Remove only the port from the UFD group if the port is not
                 * the last downlink*/
                CfaUfdRemovePortFromGroup ((UINT4) i4IfMainIndex, u4UfdGroupId);
            }
        }
        if (CfaSetIfPortRole ((UINT4) i4IfMainIndex, 0) != CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                     "nmhSetIfIvrBridgedIface - Failure in setting portrole\n");
        }
    }
    else
    {
        if (CfaSetIfPortRole ((UINT4) i4IfMainIndex, CFA_PORT_ROLE_DOWNLINK) !=
            CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                     "nmhSetIfIvrBridgedIface - Failure in setting portrole\n");
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IfIvrBridgedIface
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfIvrBridgedIface
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfIvrBridgedIface (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                            INT4 i4TestValIfIvrBridgedIface)
{

    UINT4               u4IfIndex;
    UINT4               u4ContextId = 0;
#ifdef MPLS_WANTED
    UINT4               u4MplsIfIndex = 0;
#endif
    UINT1               u1IfType;
    UINT1               u1BrgPortType = 0;

#if defined (MPLS_WANTED) || defined (LNXIP4_WANTED)
    UINT1               u1BridgedIfaceStatus = CFA_DISABLED;
#endif

    CfaGetIfBrgPortType ((UINT4) i4IfMainIndex, &u1BrgPortType);

    if (CfaIsParentPort (i4IfMainIndex) == CFA_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_L3SUB_IF_ERR);
        return SNMP_FAILURE;
    }

    if (u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        /* Cannot Change the PortType for S-Channel Interface */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_SBP_ERR);
        return SNMP_FAILURE;
    }

#if defined (MPLS_WANTED) || defined (LNXIP4_WANTED)
    CfaGetIfBridgedIfaceStatus ((UINT4) i4IfMainIndex, &u1BridgedIfaceStatus);
#endif

#ifdef LNXIP4_WANTED
    /*check whether index is available before changing to Bridged interface */
    if ((u1BridgedIfaceStatus == (UINT1) CFA_ENABLED) &&
        (i4TestValIfIvrBridgedIface == (UINT1) CFA_DISABLED))
    {
        if (NetIpv4IsIndexAvailable (CFA_ONE) != SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
#endif

    if (gu4IsIvrEnabled == CFA_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    CfaGetIfType ((UINT4) i4IfMainIndex, &u1IfType);

    if (i4TestValIfIvrBridgedIface == CFA_DISABLED)
    {
        /* Check if the interface is mapped to any switch. If so , don't allow
         * the router port configuration. 
         */
        if (VcmGetL2ModeExt () == VCM_MI_MODE)
        {
            if (u1IfType != CFA_LAGG)
            {
                if (VcmGetContextIdFromCfaIfIndex
                    ((UINT4) i4IfMainIndex, &u4ContextId) == VCM_SUCCESS)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CFA_CLI_RTR_PORT_MAPPED_ERR);
                    return SNMP_FAILURE;
                }
            }
        }
    }

    u4IfIndex = (UINT4) i4IfMainIndex;

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* the interface should be admin down */
    if ((CFA_IF_ADMIN (u4IfIndex) == CFA_IF_UP))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!((i4TestValIfIvrBridgedIface == CFA_DISABLED) ||
          (i4TestValIfIvrBridgedIface == CFA_ENABLED)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

    /* There is no IP entry for these interfaces, 
     * so setting should not be allowed */
    if (u1IfType == CFA_L3IPVLAN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

#ifdef MPLS_WANTED

/* When the interface mode is set to switch port and was previously configured 
 * as a router port and if MPLS interface is stacked over the same then we   
 * should not allow to change the port mode to switchport.
 */

    if ((u1BridgedIfaceStatus == (UINT1) CFA_DISABLED) &&
        (i4TestValIfIvrBridgedIface == (UINT1) CFA_ENABLED))
    {
        if (CfaUtilGetMplsIfFromIfIndex
            (u4IfIndex, &u4MplsIfIndex, FALSE) != CFA_FAILURE)
        {
            CLI_SET_ERR (CLI_CFA_MPLS_INTERFACE_STACK_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        }
    }

#endif

    return (INT1) SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IfIvrTable
 Input       :  The Indices
                IfMainIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfIvrTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetIfSetMgmtVlanList
 Input       :  The Indices

                The Object 
                retValIfSetMgmtVlanList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfSetMgmtVlanList (tSNMP_OCTET_STRING_TYPE * pRetValIfSetMgmtVlanList)
{
#ifdef WGS_WANTED
    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        MEMSET (pRetValIfSetMgmtVlanList->pu1_OctetList, 0,
                CFA_MGMT_VLAN_LIST_SIZE);
        CFA_DS_LOCK ();
        CFA_MGMT_ADD_VLAN_LIST (pRetValIfSetMgmtVlanList->pu1_OctetList,
                                CFA_MGMT_VLANLIST ());
        CFA_DS_UNLOCK ();
        pRetValIfSetMgmtVlanList->i4_Length = CFA_MGMT_VLAN_LIST_SIZE;
        return SNMP_SUCCESS;
    }
    return SNMP_SUCCESS;
#endif
    CFA_DS_LOCK ();
    MEMSET (pRetValIfSetMgmtVlanList->pu1_OctetList, 0,
            CFA_MGMT_VLAN_LIST_SIZE);
    CFA_DS_UNLOCK ();
    pRetValIfSetMgmtVlanList->i4_Length = 1;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfResetMgmtVlanList
 Input       :  The Indices

                The Object 
                retValIfResetMgmtVlanList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfResetMgmtVlanList (tSNMP_OCTET_STRING_TYPE * pRetValIfResetMgmtVlanList)
{
    MEMSET (pRetValIfResetMgmtVlanList->pu1_OctetList, 0,
            CFA_MGMT_VLAN_LIST_SIZE);
    pRetValIfResetMgmtVlanList->i4_Length = 1;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfSecurityBridging
 Input       :  The Indices

                The Object 
                retValIfSecurityBridging
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfSecurityBridging (INT4 *pi4RetValIfSecurityBridging)
{
    *pi4RetValIfSecurityBridging = (INT4) gu4SecModeForBridgedTraffic;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfSecIvrIfIndex
 Input       :  The Indices

                The Object
                retValIfSecIvrIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfSecIvrIfIndex (INT4 *pi4RetValIfSecIvrIfIndex)
{
    *pi4RetValIfSecIvrIfIndex = (INT4) gu4SecIvrIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfSetSecVlanList
 Input       :  The Indices

                The Object 
                retValIfSetSecVlanList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfSetSecVlanList (tSNMP_OCTET_STRING_TYPE * pRetValIfSetSecVlanList)
{
    MEMSET (pRetValIfSetSecVlanList->pu1_OctetList, 0, CFA_SEC_VLAN_LIST_SIZE);

    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        CFA_DS_LOCK ();
        CFA_SEC_ADD_VLAN_LIST (pRetValIfSetSecVlanList->pu1_OctetList,
                               CFA_SEC_VLANLIST ());
        CFA_DS_UNLOCK ();
        pRetValIfSetSecVlanList->i4_Length = CFA_SEC_VLAN_LIST_SIZE;

        if (pRetValIfSetSecVlanList->i4_Length > CFA_MAX_SEC_VLAN_LIST_SIZE)
        {
            pRetValIfSetSecVlanList->i4_Length = CFA_MAX_SEC_VLAN_LIST_SIZE;
        }

        return SNMP_SUCCESS;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfResetSecVlanList
 Input       :  The Indices

                The Object 
                retValIfResetSecVlanList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfResetSecVlanList (tSNMP_OCTET_STRING_TYPE * pRetValIfResetSecVlanList)
{
    UNUSED_PARAM (pRetValIfResetSecVlanList);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetIfSetMgmtVlanList
 Input       :  The Indices

                The Object 
                setValIfSetMgmtVlanList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfSetSecVlanList (tSNMP_OCTET_STRING_TYPE * pSetValIfSetSecVlanList)
{

    UINT1              *pSecVlanList = NULL;
#ifdef NPAPI_WANTED
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1MacAddress[MAC_ADDR_LEN];
    UINT4               u4SecIvrIndex = 0;
    UINT2               u2VlanId = 0;
    BOOL1               bResult = OSIX_FALSE;
#endif /* NPAPI_WANTED */

    pSecVlanList = UtilVlanAllocVlanListSize (sizeof (tSecVlanList));
    if (pSecVlanList == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pSecVlanList, 0, sizeof (tSecVlanList));
    MEMCPY (pSecVlanList, pSetValIfSetSecVlanList->pu1_OctetList,
            pSetValIfSetSecVlanList->i4_Length);

    CFA_DS_LOCK ();
    CFA_SEC_ADD_VLAN_LIST (CFA_SEC_VLANLIST (), pSecVlanList);
    CFA_DS_UNLOCK ();

#ifdef SMOD_WANTED
    SecApiSetSecVlanList (pSecVlanList);
#endif /* SMOD_WANTED */

#ifdef NPAPI_WANTED
    u4SecIvrIndex = CfaGetSecIvrIndex ();
    if (0 == u4SecIvrIndex)
    {
        UtilVlanReleaseVlanListSize (pSecVlanList);
        return SNMP_SUCCESS;
    }

    CfaGetIfName (u4SecIvrIndex, au1IfName);
    CfaGetIfHwAddr (u4SecIvrIndex, au1MacAddress);

    for (u2VlanId = 0; u2VlanId < CFA_MAX_VLAN_ID; u2VlanId++)
    {
        OSIX_BITLIST_IS_BIT_SET (pSecVlanList, u2VlanId,
                                 pSetValIfSetSecVlanList->i4_Length, bResult);
        if (OSIX_TRUE == bResult)
        {
            /* Create L3 Interface in NP for security VLANs, to enable
             * ARP programming for NAT translated IPs.
             */
            CfaFsNpIpv4CreateIpInterface (0, au1IfName,
                                          u4SecIvrIndex, 0, 0, u2VlanId,
                                          au1MacAddress);
        }
    }                            /* End of for */

#endif /* NPAPI_WANTED */
    UtilVlanReleaseVlanListSize (pSecVlanList);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIfResetSecVlanList
 Input       :  The Indices

                The Object 
                setValIfResetSecVlanList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfResetSecVlanList (tSNMP_OCTET_STRING_TYPE * pSetValIfResetSecVlanList)
{
    UINT1              *pSecVlanList = NULL;
#ifdef NPAPI_WANTED
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1MacAddress[MAC_ADDR_LEN];
    UINT4               u4SecIvrIndex = 0;
    UINT2               u2VlanId;
    BOOL1               bResult = OSIX_FALSE;
#endif /* NPAPI_WANTED */

    pSecVlanList = UtilVlanAllocVlanListSize (sizeof (tSecVlanList));
    if (pSecVlanList == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pSecVlanList, 0, sizeof (tSecVlanList));
    MEMCPY (pSecVlanList, pSetValIfResetSecVlanList->pu1_OctetList,
            pSetValIfResetSecVlanList->i4_Length);

    CFA_DS_LOCK ();
    CFA_SEC_REMOVE_VLAN_LIST (CFA_SEC_VLANLIST (), pSecVlanList);
    CFA_DS_UNLOCK ();
#ifdef SMOD_WANTED
    SecApiResetSecVlanList (pSecVlanList);
#endif /* SMOD_WANTED */

#ifdef NPAPI_WANTED
    u4SecIvrIndex = CfaGetSecIvrIndex ();
    if (0 == u4SecIvrIndex)
    {
        UtilVlanReleaseVlanListSize (pSecVlanList);
        return SNMP_SUCCESS;
    }

    CfaGetIfName (u4SecIvrIndex, au1IfName);
    CfaGetIfHwAddr (u4SecIvrIndex, au1MacAddress);

    for (u2VlanId = 0; u2VlanId < CFA_MAX_VLAN_ID; u2VlanId++)
    {
        OSIX_BITLIST_IS_BIT_SET (pSecVlanList, u2VlanId, sizeof (tSecVlanList),
                                 bResult);
        if (OSIX_TRUE == bResult)
        {
            /* Delete the L3 interfaces created in NP by 
             * nmhSetIfSetSecVlanList. 
             */
            CfaFsNpIpv4DeleteIpInterface (0, au1IfName, u4SecIvrIndex,
                                          u2VlanId);
        }
    }                            /* End of for */

#endif /* NPAPI_WANTED */
    UtilVlanReleaseVlanListSize (pSecVlanList);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIfSecurityBridging
 Input       :  The Indices

                The Object
                setValIfSecurityBridging
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfSecurityBridging (INT4 i4SetValIfSecurityBridging)
{
    gu4SecModeForBridgedTraffic = (UINT4) i4SetValIfSecurityBridging;
#ifdef SMOD_WANTED
    SecApiUpdSecBridgingStatus (gu4SecModeForBridgedTraffic);
#endif /* SMOD_WANTED */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfSecIvrIfIndex
 Input       :  The Indices

                The Object
                setValIfSecIvrIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfSecIvrIfIndex (INT4 i4SetValIfSecIvrIfIndex)
{

    gu4SecIvrIfIndex = (UINT4) i4SetValIfSecIvrIfIndex;
#ifdef SMOD_WANTED
    SecApiSetSecIvrIfIndex (i4SetValIfSecIvrIfIndex);
#endif /* SMOD_WANTED */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfSetMgmtVlanList
 Input       :  The Indices

                The Object 
                setValIfSetMgmtVlanList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfSetMgmtVlanList (tSNMP_OCTET_STRING_TYPE * pSetValIfSetMgmtVlanList)
{
#ifdef WGS_WANTED
    UINT1               u1IfOperStatus;
    UINT1               u1TmpOperStatus;
    tMgmtVlanList       MgmtVlanList;
#ifdef NPAPI_WANTED
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    UINT1               u1VlanFlag = 0;
    UINT2               u2VlanId;
#endif

#ifdef KERNEL_WANTED
    tKernMgmtVlanParams KernMgmtVlanParams;
#endif

    MEMSET (MgmtVlanList, 0, sizeof (tMgmtVlanList));
    MEMCPY (MgmtVlanList, pSetValIfSetMgmtVlanList->pu1_OctetList,
            pSetValIfSetMgmtVlanList->i4_Length);

    CFA_DS_LOCK ();
    CFA_MGMT_ADD_VLAN_LIST (CFA_MGMT_VLANLIST (), MgmtVlanList);
    CFA_DS_UNLOCK ();

#ifdef NPAPI_WANTED
    for (u2ByteInd = 0; u2ByteInd < CFA_MGMT_VLAN_LIST_SIZE; u2ByteInd++)
    {
        if (MgmtVlanList[u2ByteInd] != 0)
        {
            u1VlanFlag = MgmtVlanList[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < CFA_MGMT_VLANS_PER_BYTE) && (u1VlanFlag != 0));
                 u2BitIndex++)
            {
                if ((u1VlanFlag & CFA_MGMT_BIT8) != 0)
                {
                    u2VlanId =
                        (UINT2) ((u2ByteInd * CFA_MGMT_VLANS_PER_BYTE) +
                                 u2BitIndex + 1);
#ifdef DX285
                    CFA_NP_ADD_CPUMAC_ENTRY (u2VlanId);
#endif

                }
                u1VlanFlag = (UINT1) (u1VlanFlag << 1);
            }
        }
    }
#endif
#ifdef KERNEL_WANTED
    KernMgmtVlanParams.u4Action = KERN_SET_MGMT_VLAN_LIST;

    MEMCPY (KernMgmtVlanParams.MgmtVlanList, MgmtVlanList,
            sizeof (tMgmtVlanList));

    KAPIUpdateInfo (MGMT_VLAN_IOCTL, (tKernCmdParam *) & KernMgmtVlanParams);

#endif

    /* Get the oper status of the Mgmt interface. If the oper
     * status is not in sync, update the oper status.
     */
    VlanIvrGetVlanIfOperStatus (CFA_MGMT_IF_INDEX, &u1IfOperStatus);

    CfaGetIfOperStatus (CFA_MGMT_IF_INDEX, &u1TmpOperStatus);

    if (u1IfOperStatus != u1TmpOperStatus)
    {
        if ((CfaIfmHandleInterfaceStatusChange (CFA_MGMT_IF_INDEX,
                                                CFA_IF_ADMIN
                                                (CFA_MGMT_IF_INDEX),
                                                u1IfOperStatus,
                                                CFA_IF_LINK_STATUS_CHANGE)) !=
            CFA_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        else
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_SUCCESS;
#endif
    UNUSED_PARAM (*pSetValIfSetMgmtVlanList);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfResetMgmtVlanList
 Input       :  The Indices

                The Object 
                setValIfResetMgmtVlanList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfResetMgmtVlanList (tSNMP_OCTET_STRING_TYPE * pSetValIfResetMgmtVlanList)
{
#ifdef WGS_WANTED
    UINT1               u1IfOperStatus;
    UINT1               u1TmpOperStatus;
    tMgmtVlanList       MgmtVlanList;
#ifdef NPAPI_WANTED
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    UINT2               u2VlanId;
    UINT1               u1VlanFlag = 0;
#endif

#ifdef KERNEL_WANTED
    tKernMgmtVlanParams KernMgmtVlanParams;
#endif

    MEMSET (MgmtVlanList, 0, sizeof (tMgmtVlanList));
    MEMCPY (MgmtVlanList, pSetValIfResetMgmtVlanList->pu1_OctetList,
            pSetValIfResetMgmtVlanList->i4_Length);

    CFA_DS_LOCK ();
    CFA_MGMT_REMOVE_VLAN_LIST (CFA_MGMT_VLANLIST (), MgmtVlanList);
    CFA_DS_UNLOCK ();

#ifdef NPAPI_WANTED
    for (u2ByteInd = 0; u2ByteInd < CFA_MGMT_VLAN_LIST_SIZE; u2ByteInd++)
    {
        if (MgmtVlanList[u2ByteInd] != 0)
        {
            u1VlanFlag = MgmtVlanList[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < CFA_MGMT_VLANS_PER_BYTE) && (u1VlanFlag != 0));
                 u2BitIndex++)
            {
                if ((u1VlanFlag & CFA_MGMT_BIT8) != 0)
                {
                    u2VlanId =
                        (UINT2) ((u2ByteInd * CFA_MGMT_VLANS_PER_BYTE) +
                                 u2BitIndex + 1);
#ifdef DX285
                    CFA_NP_DEL_CPUMAC_ENTRY (u2VlanId);
#endif
                }
                u1VlanFlag = (UINT1) (u1VlanFlag << 1);
            }
        }
    }
#endif

#ifdef KERNEL_WANTED
    KernMgmtVlanParams.u4Action = KERN_RESET_MGMT_VLAN_LIST;

    MEMCPY (KernMgmtVlanParams.MgmtVlanList, MgmtVlanList,
            sizeof (tMgmtVlanList));

    KAPIUpdateInfo (MGMT_VLAN_IOCTL, (tKernCmdParam *) & KernMgmtVlanParams);
#endif

    /* Get the oper status of the Mgmt interface. If the oper
     * status is not in sync, update the oper status.
     */
    VlanIvrGetVlanIfOperStatus (CFA_MGMT_IF_INDEX, &u1IfOperStatus);

    CfaGetIfOperStatus (CFA_MGMT_IF_INDEX, &u1TmpOperStatus);

    if (u1IfOperStatus != u1TmpOperStatus)
    {
        if ((CfaIfmHandleInterfaceStatusChange (CFA_MGMT_IF_INDEX,
                                                CFA_IF_ADMIN
                                                (CFA_MGMT_IF_INDEX),
                                                u1IfOperStatus,
                                                CFA_IF_LINK_STATUS_CHANGE)) !=
            CFA_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        else
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_SUCCESS;
#endif
    UNUSED_PARAM (*pSetValIfResetMgmtVlanList);
    return SNMP_FAILURE;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IfSetMgmtVlanList
 Input       :  The Indices

                The Object 
                testValIfSetMgmtVlanList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfSetMgmtVlanList (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pTestValIfSetMgmtVlanList)
{
#ifdef WGS_WANTED
    tMgmtVlanList       MgmtVlanList;

    if (gu4IsIvrEnabled == CFA_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTestValIfSetMgmtVlanList->i4_Length < 0) ||
        (pTestValIfSetMgmtVlanList->i4_Length > CFA_MGMT_VLAN_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    MEMSET (MgmtVlanList, 0, sizeof (tMgmtVlanList));
    MEMCPY (MgmtVlanList, pTestValIfSetMgmtVlanList->pu1_OctetList,
            pTestValIfSetMgmtVlanList->i4_Length);
    if (CfaMgmtVlanListValid (MgmtVlanList) == CFA_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#endif
    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    UNUSED_PARAM (*pTestValIfSetMgmtVlanList);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfSetSecVlanList
 Input       :  The Indices

                The Object 
                testValIfSetMgmtVlanList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfSetSecVlanList (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pTestValIfSetMgmtVlanList)
{
    UINT4               u4SecMode = CFA_SEC_BRIDGE_DISABLED;
    CfaGetSecurityBridgeMode (&u4SecMode);
    if (u4SecMode == CFA_SEC_BRIDGE_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (gu4IsIvrEnabled == CFA_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTestValIfSetMgmtVlanList->i4_Length < 0) ||
        (pTestValIfSetMgmtVlanList->i4_Length > CFA_SEC_VLAN_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfResetSecVlanList
 Input       :  The Indices

                The Object 
                testValIfResetSecVlanList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfResetSecVlanList (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValIfResetSecVlanList)
{

    UINT4               u4SecMode = CFA_SEC_BRIDGE_DISABLED;
    CfaGetSecurityBridgeMode (&u4SecMode);
    if (u4SecMode == CFA_SEC_BRIDGE_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (gu4IsIvrEnabled == CFA_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTestValIfResetSecVlanList->i4_Length < 0) ||
        (pTestValIfResetSecVlanList->i4_Length > CFA_SEC_VLAN_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfSecurityBridging
 Input       :  The Indices

                The Object
                testValIfSecurityBridging
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfSecurityBridging (UINT4 *pu4ErrorCode,
                             INT4 i4TestValIfSecurityBridging)
{

    INT4                i4FipsOperMode = FIPS_MODE;
    IssCustGetFipsCurrOperMode (&i4FipsOperMode);
    if (i4FipsOperMode == FIPS_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValIfSecurityBridging != CFA_SEC_BRIDGE_DISABLED) &&
        (i4TestValIfSecurityBridging != CFA_SEC_BRIDGE_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfSecIvrIfIndex
 Input       :  The Indices

                The Object
                testValIfSecIvrIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfSecIvrIfIndex (UINT4 *pu4ErrorCode, INT4 i4TestValIfSecIvrIfIndex)
{
    INT4                i4IpPort = CFA_INVALID_INDEX;
    i4IpPort = CFA_IF_IPPORT ((UINT4) i4TestValIfSecIvrIfIndex);
    if (i4IpPort == CFA_INVALID_INDEX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfResetMgmtVlanList
 Input       :  The Indices

                The Object 
                testValIfResetMgmtVlanList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfResetMgmtVlanList (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValIfResetMgmtVlanList)
{
#ifdef WGS_WANTED
    if (gu4IsIvrEnabled == CFA_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTestValIfResetMgmtVlanList->i4_Length < 0) ||
        (pTestValIfResetMgmtVlanList->i4_Length > CFA_MGMT_VLAN_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#endif
    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    UNUSED_PARAM (*pTestValIfResetMgmtVlanList);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IfSetMgmtVlanList
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfSetMgmtVlanList (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2IfResetMgmtVlanList
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfResetMgmtVlanList (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2IfSecurityBridging
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfSecurityBridging (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2IfSetSecVlanList
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfSetSecVlanList (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2IfResetSecVlanList
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfResetSecVlanList (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2IfSecIvrIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfSecIvrIfIndex (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :    nmhGetFfFastForwardingEnable
 Input    :    The Indices

        The Object
        retValFfFastForwardingEnable
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFfFastForwardingEnable (INT4 *pi4RetValFfFastForwardingEnable)
{
    *pi4RetValFfFastForwardingEnable = CFA_DISABLED;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFfCacheSize
 Input       :  The Indices

                The Object
                retValFfCacheSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFfCacheSize (INT4 *pi4RetValFfCacheSize)
{

    UNUSED_PARAM (pi4RetValFfCacheSize);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :    nmhGetFfIpChecksumValidationEnable
 Input    :    The Indices

        The Object
        retValFfIpChecksumValidationEnable
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFfIpChecksumValidationEnable (INT4 *pi4RetValFfIpChecksumValidationEnable)
{

    UNUSED_PARAM (pi4RetValFfIpChecksumValidationEnable);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :    nmhGetFfCachePurgeCount
 Input    :    The Indices

        The Object
        retValFfCachePurgeCount
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFfCachePurgeCount (UINT4 *pu4RetValFfCachePurgeCount)
{

    UNUSED_PARAM (pu4RetValFfCachePurgeCount);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :    nmhGetFfCacheLastPurgeTime
 Input    :    The Indices

        The Object
        retValFfCacheLastPurgeTime
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFfCacheLastPurgeTime (UINT4 *pu4RetValFfCacheLastPurgeTime)
{

    UNUSED_PARAM (pu4RetValFfCacheLastPurgeTime);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :    nmhGetFfStaticEntryInvalidTrapEnable
 Input    :    The Indices

        The Object
        retValFfStaticEntryInvalidTrapEnable
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFfStaticEntryInvalidTrapEnable (INT4
                                      *pi4RetValFfStaticEntryInvalidTrapEnable)
{

    UNUSED_PARAM (pi4RetValFfStaticEntryInvalidTrapEnable);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :    nmhGetFfCurrentStaticEntryInvalidCount
 Input    :    The Indices

        The Object
        retValFfCurrentStaticEntryInvalidCount
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFfCurrentStaticEntryInvalidCount (UINT4
                                        *pu4RetValFfCurrentStaticEntryInvalidCount)
{

    UNUSED_PARAM (pu4RetValFfCurrentStaticEntryInvalidCount);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :    nmhGetFfTotalEntryCount
 Input    :    The Indices

        The Object
        retValFfTotalEntryCount
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFfTotalEntryCount (UINT4 *pu4RetValFfTotalEntryCount)
{
    UNUSED_PARAM (pu4RetValFfTotalEntryCount);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :    nmhGetFfStaticEntryCount
 Input    :    The Indices

        The Object
        retValFfStaticEntryCount
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFfStaticEntryCount (UINT4 *pu4RetValFfStaticEntryCount)
{

    UNUSED_PARAM (pu4RetValFfStaticEntryCount);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :    nmhGetFfTotalPktsFastForwarded
 Input    :    The Indices

        The Object
        retValFfTotalPktsFastForwarded
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFfTotalPktsFastForwarded (UINT4 *pu4RetValFfTotalPktsFastForwarded)
{
    UNUSED_PARAM (pu4RetValFfTotalPktsFastForwarded);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :    nmhSetFfFastForwardingEnable
 Input    :    The Indices

        The Object
        setValFfFastForwardingEnable
 Output    :    The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFfFastForwardingEnable (INT4 i4SetValFfFastForwardingEnable)
{
    UNUSED_PARAM (i4SetValFfFastForwardingEnable);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFfCacheSize
 Input       :  The Indices

                The Object
                setValFfCacheSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFfCacheSize (INT4 i4SetValFfCacheSize)
{
    UNUSED_PARAM (i4SetValFfCacheSize);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :    nmhSetFfIpChecksumValidationEnable
 Input    :    The Indices

        The Object
        setValFfIpChecksumValidationEnable
 Output    :    The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFfIpChecksumValidationEnable (INT4 i4SetValFfIpChecksumValidationEnable)
{

    UNUSED_PARAM (i4SetValFfIpChecksumValidationEnable);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :    nmhSetFfStaticEntryInvalidTrapEnable
 Input    :    The Indices

        The Object
        setValFfStaticEntryInvalidTrapEnable
 Output    :    The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFfStaticEntryInvalidTrapEnable (INT4
                                      i4SetValFfStaticEntryInvalidTrapEnable)
{

    UNUSED_PARAM (i4SetValFfStaticEntryInvalidTrapEnable);
    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :    nmhTestv2FfFastForwardingEnable
 Input    :    The Indices

        The Object
        testValFfFastForwardingEnable
 Output    :    The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
 Error Codes :    The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FfFastForwardingEnable (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFfFastForwardingEnable)
{
    UNUSED_PARAM (i4TestValFfFastForwardingEnable);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FfCacheSize
 Input       :  The Indices

                The Object
                testValFfCacheSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FfCacheSize (UINT4 *pu4ErrorCode, INT4 i4TestValFfCacheSize)
{

    UNUSED_PARAM (i4TestValFfCacheSize);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :    nmhTestv2FfIpChecksumValidationEnable
 Input    :    The Indices

        The Object
        testValFfIpChecksumValidationEnable
 Output    :    The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
 Error Codes :    The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FfIpChecksumValidationEnable (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFfIpChecksumValidationEnable)
{

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    UNUSED_PARAM (i4TestValFfIpChecksumValidationEnable);
    return (SNMP_FAILURE);

}

/****************************************************************************
 Function    :    nmhTestv2FfStaticEntryInvalidTrapEnable
 Input    :    The Indices

        The Object
        testValFfStaticEntryInvalidTrapEnable
 Output    :    The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
 Error Codes :    The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FfStaticEntryInvalidTrapEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FfStaticEntryInvalidTrapEnable (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4TestValFfStaticEntryInvalidTrapEnable)
{

    UNUSED_PARAM (i4TestValFfStaticEntryInvalidTrapEnable);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return (SNMP_FAILURE);

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FfFastForwardingEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FfFastForwardingEnable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FfCacheSize
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FfCacheSize (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FfStaticEntryInvalidTrapEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FfStaticEntryInvalidTrapEnable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FfHostCacheTable. */

/****************************************************************************
 Function    :    nmhValidateIndexInstanceFfHostCacheTable
 Input    :    The Indices
        FfHostCacheDestAddr
 Output    :    The Routines Validates the Given Indices.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFfHostCacheTable (UINT4 u4FfHostCacheDestAddr)
{
    UNUSED_PARAM (u4FfHostCacheDestAddr);

    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :    nmhGetFirstIndexFfHostCacheTable
 Input    :    The Indices
        FfHostCacheDestAddr
 Output    :    The Get First Routines gets the Lexicographicaly
        First Entry from the Table.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexFfHostCacheTable (UINT4 *pu4FfHostCacheDestAddr)
{
    UNUSED_PARAM (pu4FfHostCacheDestAddr);

    return (SNMP_FAILURE);

}

/****************************************************************************
 Function    :    nmhGetNextIndexFfHostCacheTable
 Input    :    The Indices
        FfHostCacheDestAddr
        nextFfHostCacheDestAddr
 Output    :    The Get Next function gets the Next Index for
        the Index Value given in the Index Values. The
        Indices are stored in the next_varname variables.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexFfHostCacheTable (UINT4 u4FfHostCacheDestAddr,
                                 UINT4 *pu4NextFfHostCacheDestAddr)
{
    UNUSED_PARAM (u4FfHostCacheDestAddr);
    UNUSED_PARAM (pu4NextFfHostCacheDestAddr);
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :    nmhGetFfHostCacheNextHopAddr
 Input    :    The Indices
        FfHostCacheDestAddr

        The Object
        retValFfHostCacheNextHopAddr
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFfHostCacheNextHopAddr (UINT4 u4FfHostCacheDestAddr,
                              UINT4 *pu4RetValFfHostCacheNextHopAddr)
{
    UNUSED_PARAM (u4FfHostCacheDestAddr);
    UNUSED_PARAM (pu4RetValFfHostCacheNextHopAddr);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :    nmhGetFfHostCacheIfIndex
 Input    :    The Indices
        FfHostCacheDestAddr

        The Object
        retValFfHostCacheIfIndex
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFfHostCacheIfIndex (UINT4 u4FfHostCacheDestAddr,
                          INT4 *pi4RetValFfHostCacheIfIndex)
{
    UNUSED_PARAM (u4FfHostCacheDestAddr);
    UNUSED_PARAM (pi4RetValFfHostCacheIfIndex);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :    nmhGetFfHostCacheNextHopMediaAddr
 Input    :    The Indices
        FfHostCacheDestAddr

        The Object
        retValFfHostCacheNextHopMediaAddr
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFfHostCacheNextHopMediaAddr (UINT4 u4FfHostCacheDestAddr,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFfHostCacheNextHopMediaAddr)
{
    UNUSED_PARAM (u4FfHostCacheDestAddr);
    UNUSED_PARAM (pRetValFfHostCacheNextHopMediaAddr);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :    nmhGetFfHostCacheHits
 Input    :    The Indices
        FfHostCacheDestAddr

        The Object
        retValFfHostCacheHits
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFfHostCacheHits (UINT4 u4FfHostCacheDestAddr,
                       UINT4 *pu4RetValFfHostCacheHits)
{
    UNUSED_PARAM (u4FfHostCacheDestAddr);
    UNUSED_PARAM (pu4RetValFfHostCacheHits);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :    nmhGetFfHostCacheLastHitTime
 Input    :    The Indices
        FfHostCacheDestAddr

        The Object
        retValFfHostCacheLastHitTime
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFfHostCacheLastHitTime (UINT4 u4FfHostCacheDestAddr,
                              UINT4 *pu4RetValFfHostCacheLastHitTime)
{
    UNUSED_PARAM (u4FfHostCacheDestAddr);
    UNUSED_PARAM (pu4RetValFfHostCacheLastHitTime);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :    nmhGetFfHostCacheEntryType
 Input    :    The Indices
        FfHostCacheDestAddr

        The Object
        retValFfHostCacheEntryType
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFfHostCacheEntryType (UINT4 u4FfHostCacheDestAddr,
                            INT4 *pi4RetValFfHostCacheEntryType)
{
    UNUSED_PARAM (u4FfHostCacheDestAddr);
    UNUSED_PARAM (pi4RetValFfHostCacheEntryType);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :    nmhGetFfHostCacheRowStatus
 Input    :    The Indices
        FfHostCacheDestAddr

        The Object
        retValFfHostCacheRowStatus
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFfHostCacheRowStatus (UINT4 u4FfHostCacheDestAddr,
                            INT4 *pi4RetValFfHostCacheRowStatus)
{
    UNUSED_PARAM (u4FfHostCacheDestAddr);
    UNUSED_PARAM (pi4RetValFfHostCacheRowStatus);
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :    nmhSetFfHostCacheNextHopAddr
 Input    :    The Indices
        FfHostCacheDestAddr

        The Object
        setValFfHostCacheNextHopAddr
 Output    :    The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFfHostCacheNextHopAddr (UINT4 u4FfHostCacheDestAddr,
                              UINT4 u4SetValFfHostCacheNextHopAddr)
{
    UNUSED_PARAM (u4SetValFfHostCacheNextHopAddr);
    UNUSED_PARAM (u4FfHostCacheDestAddr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhSetFfHostCacheIfIndex
 Input    :    The Indices
        FfHostCacheDestAddr

        The Object
        setValFfHostCacheIfIndex
 Output    :    The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFfHostCacheIfIndex (UINT4 u4FfHostCacheDestAddr,
                          INT4 i4SetValFfHostCacheIfIndex)
{
    UNUSED_PARAM (i4SetValFfHostCacheIfIndex);
    UNUSED_PARAM (u4FfHostCacheDestAddr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhSetFfHostCacheNextHopMediaAddr
 Input    :    The Indices
        FfHostCacheDestAddr

        The Object
        setValFfHostCacheNextHopMediaAddr
 Output    :    The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFfHostCacheNextHopMediaAddr (UINT4 u4FfHostCacheDestAddr,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValFfHostCacheNextHopMediaAddr)
{
    UNUSED_PARAM (u4FfHostCacheDestAddr);
    UNUSED_PARAM (pSetValFfHostCacheNextHopMediaAddr);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :    nmhSetFfHostCacheEntryType
 Input    :    The Indices
        FfHostCacheDestAddr

        The Object
        setValFfHostCacheEntryType
 Output    :    The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFfHostCacheEntryType (UINT4 u4FfHostCacheDestAddr,
                            INT4 i4SetValFfHostCacheEntryType)
{
    UNUSED_PARAM (u4FfHostCacheDestAddr);
    UNUSED_PARAM (i4SetValFfHostCacheEntryType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhSetFfHostCacheRowStatus
 Input    :    The Indices
        FfHostCacheDestAddr

        The Object
        setValFfHostCacheRowStatus
 Output    :    The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFfHostCacheRowStatus (UINT4 u4FfHostCacheDestAddr,
                            INT4 i4SetValFfHostCacheRowStatus)
{
    UNUSED_PARAM (u4FfHostCacheDestAddr);
    UNUSED_PARAM (i4SetValFfHostCacheRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/***************************************************************************
 Function    :    nmhTestv2FfHostCacheNextHopAddr
 Input    :    The Indices
        FfHostCacheDestAddr

        The Object
        testValFfHostCacheNextHopAddr
 Output    :    The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
 Error Codes :    The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FfHostCacheNextHopAddr (UINT4 *pu4ErrorCode,
                                 UINT4 u4FfHostCacheDestAddr,
                                 UINT4 u4TestValFfHostCacheNextHopAddr)
{
    UNUSED_PARAM (u4TestValFfHostCacheNextHopAddr);
    UNUSED_PARAM (u4FfHostCacheDestAddr);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :    nmhTestv2FfHostCacheIfIndex
 Input    :    The Indices
        FfHostCacheDestAddr

        The Object
        testValFfHostCacheIfIndex
 Output    :    The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
 Error Codes :    The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FfHostCacheIfIndex (UINT4 *pu4ErrorCode, UINT4 u4FfHostCacheDestAddr,
                             INT4 i4TestValFfHostCacheIfIndex)
{
    UNUSED_PARAM (i4TestValFfHostCacheIfIndex);
    UNUSED_PARAM (u4FfHostCacheDestAddr);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :    nmhTestv2FfHostCacheNextHopMediaAddr
 Input    :    The Indices
        FfHostCacheDestAddr

        The Object
        testValFfHostCacheNextHopMediaAddr
 Output    :    The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
 Error Codes :    The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FfHostCacheNextHopMediaAddr (UINT4 *pu4ErrorCode,
                                      UINT4 u4FfHostCacheDestAddr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValFfHostCacheNextHopMediaAddr)
{
    UNUSED_PARAM (pTestValFfHostCacheNextHopMediaAddr);
    UNUSED_PARAM (u4FfHostCacheDestAddr);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :    nmhTestv2FfHostCacheEntryType
 Input    :    The Indices
        FfHostCacheDestAddr

        The Object
        testValFfHostCacheEntryType
 Output    :    The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
 Error Codes :    The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FfHostCacheEntryType (UINT4 *pu4ErrorCode,
                               UINT4 u4FfHostCacheDestAddr,
                               INT4 i4TestValFfHostCacheEntryType)
{
    UNUSED_PARAM (i4TestValFfHostCacheEntryType);
    UNUSED_PARAM (u4FfHostCacheDestAddr);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :    nmhTestv2FfHostCacheRowStatus
 Input    :    The Indices
        FfHostCacheDestAddr

        The Object
        testValFfHostCacheRowStatus
 Output    :    The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
 Error Codes :    The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FfHostCacheRowStatus (UINT4 *pu4ErrorCode,
                               UINT4 u4FfHostCacheDestAddr,
                               INT4 i4TestValFfHostCacheRowStatus)
{
    UNUSED_PARAM (i4TestValFfHostCacheRowStatus);
    UNUSED_PARAM (u4FfHostCacheDestAddr);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhDepv2FfHostCacheTable
 Input       :  The Indices
                FfHostCacheDestAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FfHostCacheTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/*****************************************************************************/

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :    nmhGetFmMemoryResourceTrapEnable
 Input    :    The Indices

        The Object
        retValFmMemoryResourceTrapEnable
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFmMemoryResourceTrapEnable (INT4 *pi4RetValFmMemoryResourceTrapEnable)
{

    UNUSED_PARAM (pi4RetValFmMemoryResourceTrapEnable);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetFmTimersResourceTrapEnable
 Input    :    The Indices

        The Object
        retValFmTimersResourceTrapEnable
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFmTimersResourceTrapEnable (INT4 *pi4RetValFmTimersResourceTrapEnable)
{

    UNUSED_PARAM (pi4RetValFmTimersResourceTrapEnable);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetFmTracingEnable
 Input    :    The Indices

        The Object
        retValFmTracingEnable
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFmTracingEnable (INT4 *pi4RetValFmTracingEnable)
{

    UNUSED_PARAM (pi4RetValFmTracingEnable);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetFmMemAllocFailCount
 Input    :    The Indices

        The Object
        retValFmMemAllocFailCount
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFmMemAllocFailCount (UINT4 *pu4RetValFmMemAllocFailCount)
{

    UNUSED_PARAM (pu4RetValFmMemAllocFailCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetFmTimerReqFailCount
 Input    :    The Indices

        The Object
        retValFmTimerReqFailCount
 Output    :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFmTimerReqFailCount (UINT4 *pu4RetValFmTimerReqFailCount)
{

    UNUSED_PARAM (pu4RetValFmTimerReqFailCount);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :    nmhSetFmMemoryResourceTrapEnable
 Input    :    The Indices

        The Object
        setValFmMemoryResourceTrapEnable
 Output    :    The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFmMemoryResourceTrapEnable (INT4 i4SetValFmMemoryResourceTrapEnable)
{
    UNUSED_PARAM (i4SetValFmMemoryResourceTrapEnable);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhSetFmTimersResourceTrapEnable
 Input    :    The Indices

        The Object
        setValFmTimersResourceTrapEnable
 Output    :    The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFmTimersResourceTrapEnable (INT4 i4SetValFmTimersResourceTrapEnable)
{

    UNUSED_PARAM (i4SetValFmTimersResourceTrapEnable);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhSetFmTracingEnable
 Input    :    The Indices

        The Object
        setValFmTracingEnable
 Output    :    The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFmTracingEnable (INT4 i4SetValFmTracingEnable)
{

    UNUSED_PARAM (i4SetValFmTracingEnable);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :    nmhTestv2FmMemoryResourceTrapEnable
 Input    :    The Indices

        The Object
        testValFmMemoryResourceTrapEnable
 Output    :    The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
 Error Codes :    The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FmMemoryResourceTrapEnable (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFmMemoryResourceTrapEnable)
{

    UNUSED_PARAM (i4TestValFmMemoryResourceTrapEnable);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :    nmhTestv2FmTimersResourceTrapEnable
 Input    :    The Indices

        The Object
        testValFmTimersResourceTrapEnable
 Output    :    The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
 Error Codes :    The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FmTimersResourceTrapEnable (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFmTimersResourceTrapEnable)
{

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    UNUSED_PARAM (i4TestValFmTimersResourceTrapEnable);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :    nmhTestv2FmTracingEnable
 Input    :    The Indices

        The Object
        testValFmTracingEnable
 Output    :    The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
 Error Codes :    The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FmTracingEnable (UINT4 *pu4ErrorCode, INT4 i4TestValFmTracingEnable)
{
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    UNUSED_PARAM (i4TestValFmTracingEnable);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FmMemoryResourceTrapEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FmMemoryResourceTrapEnable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FmTimersResourceTrapEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FmTimersResourceTrapEnable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FmTracingEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FmTracingEnable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IfCustTLVTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIfCustTLVTable
 Input       :  The Indices
                IfMainIndex
                IfCustTLVType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIfCustTLVTable (INT4 i4IfMainIndex,
                                        UINT4 u4IfCustTLVType)
{
    UINT4               u4IfIndex;

    UNUSED_PARAM (u4IfCustTLVType);

    if ((i4IfMainIndex > BRG_MAX_PHY_PLUS_LOG_PORTS) || (i4IfMainIndex <= 0))
    {
        return (SNMP_FAILURE);
    }

    /*checked for bridged interface */
    if (CfaIfIsBridgedInterface (i4IfMainIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4IfIndex = (UINT4) i4IfMainIndex;

    if (CFA_IF_ENTRY_USED (u4IfIndex))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIfCustTLVTable
 Input       :  The Indices
                IfMainIndex
                IfCustTLVType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIfCustTLVTable (INT4 *pi4IfMainIndex, UINT4 *pu4IfCustTLVType)
{
    tTMO_SLL           *pTlvList = NULL;
    tIfTlvInfoStruct   *pIfTlvNode = NULL;
    UINT4               u4Index = 0;

    /* Scan from the first index
     */
    u4Index = 1;

    /* Get the First Used Index */
    CFA_CDB_SCAN_WITH_LOCK (u4Index, BRG_MAX_PHY_PLUS_LOG_PORTS, CFA_ALL_IFTYPE)
    {
        /* Flexible ifIndex changes 
         */
        CFA_DS_LOCK ();
        if ((CFA_CDB_IF_TYPE (u4Index) != CFA_ENET) &&
            (CFA_CDB_IF_TYPE (u4Index) != CFA_LAGG))
        {
            CFA_DS_UNLOCK ();
            continue;
        }
        CFA_DS_UNLOCK ();

        /* Get the First Used Index */
        if (CFA_IF_ENTRY_USED (u4Index))
        {
            if (CfaIfIsBridgedInterface ((INT4) u4Index) == CFA_FALSE)
            {
                continue;
            }

            /* Get the list for the given port */
            pTlvList = (tTMO_SLL *) & CFA_IF_TLV_INFO (u4Index);

            /* Node is not present then get first of next port */
            if ((pIfTlvNode =
                 (tIfTlvInfoStruct *) TMO_SLL_First (pTlvList)) == NULL)
            {
                continue;
            }
            *pi4IfMainIndex = (INT4) u4Index;
            *pu4IfCustTLVType = pIfTlvNode->u4TlvType;
            break;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    if ((u4Index > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u4Index == 0))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIfCustTLVTable
 Input       :  The Indices
                IfMainIndex
                nextIfMainIndex
                IfCustTLVType
                nextIfCustTLVType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIfCustTLVTable (INT4 i4IfMainIndex,
                               INT4 *pi4NextIfMainIndex,
                               UINT4 u4IfCustTLVType,
                               UINT4 *pu4NextIfCustTLVType)
{
    tTMO_SLL           *pTlvList = NULL;
    tIfTlvInfoStruct   *pIfTlvNode = NULL;
    tIfTlvInfoStruct   *pIfNextTlvNode = NULL;
    UINT4               u4IfIndex;
    UINT4               u4Counter;

    /* Check for port range */
    if ((i4IfMainIndex <= 0) || (i4IfMainIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return SNMP_FAILURE;
    }

    u4IfIndex = (UINT4) i4IfMainIndex;

    /*Get the list for the given port */
    pTlvList = (tTMO_SLL *) & CFA_IF_TLV_INFO (u4IfIndex);

    TMO_SLL_Scan (pTlvList, pIfTlvNode, tIfTlvInfoStruct *)
    {
        if (pIfTlvNode->u4TlvType == u4IfCustTLVType)
        {
            /* Get next for the given Node */
            if ((pIfNextTlvNode = (tIfTlvInfoStruct *) TMO_SLL_Next
                 (pTlvList, &pIfTlvNode->NextNode)) != NULL)
            {
                *pu4NextIfCustTLVType = pIfNextTlvNode->u4TlvType;
                *pi4NextIfMainIndex = i4IfMainIndex;
                return SNMP_SUCCESS;
            }
        }
    }

    /* If no node is present for the given port then get first 
       for the next port */
    u4Counter = (UINT2) (u4IfIndex + 1);
    CFA_CDB_SCAN_WITH_LOCK (u4Counter, BRG_MAX_PHY_PLUS_LOG_PORTS,
                            CFA_ALL_IFTYPE)
    {
        /* Flexible ifIndex changes 
         */
        CFA_DS_LOCK ();
        if ((CFA_CDB_IF_TYPE (u4Counter) != CFA_ENET) &&
            (CFA_CDB_IF_TYPE (u4Counter) != CFA_LAGG))
        {
            CFA_DS_UNLOCK ();
            continue;
        }
        CFA_DS_UNLOCK ();

        if (!CFA_IF_ENTRY_USED (u4Counter))
        {
            continue;
        }

        if (CfaIfIsBridgedInterface ((INT4) u4Counter) == CFA_FALSE)
        {
            continue;
        }

        *pi4NextIfMainIndex = (INT4) u4Counter;

        u4IfIndex = (UINT4) *pi4NextIfMainIndex;

        /*If the Next Node is NULL then access first node of next valid
           Index */
        pTlvList = (tTMO_SLL *) & CFA_IF_TLV_INFO (u4IfIndex);

        pIfTlvNode = (tIfTlvInfoStruct *) TMO_SLL_First (pTlvList);

        if (pIfTlvNode != NULL)
        {
            *pu4NextIfCustTLVType = pIfTlvNode->u4TlvType;
            return SNMP_SUCCESS;
        }
        else                    /*If no node is present for next port then 
                                   continue to next port */
        {
            continue;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIfCustTLVLength
 Input       :  The Indices
                IfMainIndex
                IfCustTLVType

                The Object
                retValIfCustTLVLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfCustTLVLength (INT4 i4IfMainIndex,
                       UINT4 u4IfCustTLVType, UINT4 *pu4RetValIfCustTLVLength)
{
    tTMO_SLL           *pTlvList = NULL;
    tIfTlvInfoStruct   *pIfTlvNode = NULL;
    UINT4               u4Index;

    /*Check for port range */
    if ((i4IfMainIndex <= 0) || (i4IfMainIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return SNMP_FAILURE;
    }

    /* If the interface is not a bridged interface then return */
    if (CfaIfIsBridgedInterface (i4IfMainIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4Index = (UINT4) i4IfMainIndex;

    /* Get the list for the given port */
    pTlvList = (tTMO_SLL *) & CFA_IF_TLV_INFO (u4Index);

    TMO_SLL_Scan (pTlvList, pIfTlvNode, tIfTlvInfoStruct *)
    {
        /* Get the length of the given port and type */
        if (pIfTlvNode->u4TlvType == u4IfCustTLVType)
        {
            *pu4RetValIfCustTLVLength = pIfTlvNode->u4TlvLength;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfCustTLVValue
 Input       :  The Indices
                IfMainIndex
                IfCustTLVType

                The Object
                retValIfCustTLVValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfCustTLVValue (INT4 i4IfMainIndex,
                      UINT4 u4IfCustTLVType,
                      tSNMP_OCTET_STRING_TYPE * pRetValIfCustTLVValue)
{
    tTMO_SLL           *pTlvList = NULL;
    tIfTlvInfoStruct   *pIfTlvNode = NULL;
    UINT4               u4Index;

    /*Check for port range */
    if ((i4IfMainIndex <= 0) || (i4IfMainIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return SNMP_FAILURE;
    }

    /* If the interface is not a bridged interface then return */
    if (CfaIfIsBridgedInterface (i4IfMainIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4Index = (UINT4) i4IfMainIndex;

    /* Get the list for the given port */
    pTlvList = (tTMO_SLL *) & CFA_IF_TLV_INFO (u4Index);

    TMO_SLL_Scan (pTlvList, pIfTlvNode, tIfTlvInfoStruct *)
    {
        /* Get the value of the given port and type */
        if (pIfTlvNode->u4TlvType == u4IfCustTLVType)
        {
            /* If Length is zero, then pIfTlvNode->pu1TlvVlaue will be null.
             * Test routing makes sure that when length is zero then value is 
             * not configured. But in get routine we need to explicitly check
             * for lenght. */
            if (pIfTlvNode->u4TlvLength == 0)
            {
                pRetValIfCustTLVValue->i4_Length = 0;
                return SNMP_SUCCESS;
            }
            MEMSET (pRetValIfCustTLVValue->pu1_OctetList, 0,
                    pIfTlvNode->u4TlvLength);

            MEMCPY (pRetValIfCustTLVValue->pu1_OctetList,
                    pIfTlvNode->pu1TlvVlaue, pIfTlvNode->u4TlvLength);

            pRetValIfCustTLVValue->pu1_OctetList[pIfTlvNode->u4TlvLength]
                = '\0';

            pRetValIfCustTLVValue->i4_Length = (INT4) pIfTlvNode->u4TlvLength;

            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIfCustTLVRowStatus
 Input       :  The Indices
                IfMainIndex
                IfCustTLVType

                The Object
                retValIfCustTLVRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfCustTLVRowStatus (INT4 i4IfMainIndex,
                          UINT4 u4IfCustTLVType,
                          INT4 *pi4RetValIfCustTLVRowStatus)
{
    tTMO_SLL           *pTlvList = NULL;
    tIfTlvInfoStruct   *pIfTlvNode = NULL;
    UINT4               u4Index;

    /* Check for port range */
    if ((i4IfMainIndex <= 0) || (i4IfMainIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return SNMP_FAILURE;
    }

    /* If the interface is not a bridged interface then return */
    if (CfaIfIsBridgedInterface (i4IfMainIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;

    }

    u4Index = (UINT4) i4IfMainIndex;

    /*Get the list for the given port */
    pTlvList = (tTMO_SLL *) & CFA_IF_TLV_INFO (u4Index);

    TMO_SLL_Scan (pTlvList, pIfTlvNode, tIfTlvInfoStruct *)
    {
        /*Get the rowstatus of given port and type */
        if (pIfTlvNode->u4TlvType == u4IfCustTLVType)
        {
            *pi4RetValIfCustTLVRowStatus = pIfTlvNode->i4TlvRowStatus;

            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIfCustTLVLength
 Input       :  The Indices
                IfMainIndex
                IfCustTLVType

                The Object
                setValIfCustTLVLength
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfCustTLVLength (INT4 i4IfMainIndex,
                       UINT4 u4IfCustTLVType, UINT4 u4SetValIfCustTLVLength)
{
    tTMO_SLL           *pTlvList = NULL;
    tIfTlvInfoStruct   *pIfTlvNode = NULL;
    UINT4               u4Index;

    /* Check for port range */
    if ((i4IfMainIndex <= 0) || (i4IfMainIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return SNMP_FAILURE;
    }

    u4Index = (UINT4) i4IfMainIndex;
    /* Null check has to be provided here to avoid crash as MSR does not call
     * Test routine */
    if (CFA_IF_ENTRY (u4Index) == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Get the list for the given port */
    pTlvList = &CFA_IF_TLV_INFO (u4Index);

    /* Scan the list and get the length for the given node */
    TMO_SLL_Scan (pTlvList, pIfTlvNode, tIfTlvInfoStruct *)
    {
        if (pIfTlvNode->u4TlvType == u4IfCustTLVType)
        {
            pIfTlvNode->u4TlvLength = u4SetValIfCustTLVLength;

            /*Allocate buddy memory for the value */
            if ((pIfTlvNode->pu1TlvVlaue =
                 MemBuddyAlloc ((UINT1) CFA_TLV_BUDDY_MEMPOOL,
                                pIfTlvNode->u4TlvLength + 1)) == NULL)
            {
                return SNMP_FAILURE;
            }

            MEMSET (pIfTlvNode->pu1TlvVlaue, 0, pIfTlvNode->u4TlvLength + 1);

            pIfTlvNode->i4TlvRowStatus = CFA_RS_NOTINSERVICE;

            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIfCustTLVValue
 Input       :  The Indices
                IfMainIndex
                IfCustTLVType

                The Object
                setValIfCustTLVValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfCustTLVValue (INT4 i4IfMainIndex,
                      UINT4 u4IfCustTLVType,
                      tSNMP_OCTET_STRING_TYPE * pSetValIfCustTLVValue)
{
    tTMO_SLL           *pTlvList = NULL;
    tIfTlvInfoStruct   *pIfTlvNode = NULL;
    UINT4               u4Index;

    /* Check for port range */
    if ((i4IfMainIndex <= 0) || (i4IfMainIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return SNMP_FAILURE;
    }

    u4Index = (UINT4) i4IfMainIndex;

    /* Null check has to be provided here to avoid crash as MSR does not call
     * Test routine */
    if (CFA_IF_ENTRY (u4Index) == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Get the list for the given port */
    pTlvList = &CFA_IF_TLV_INFO (u4Index);

    /*Scan the list and get the value for the given node */
    TMO_SLL_Scan (pTlvList, pIfTlvNode, tIfTlvInfoStruct *)
    {
        if (pIfTlvNode->u4TlvType == u4IfCustTLVType)
        {
            MEMSET (pIfTlvNode->pu1TlvVlaue, 0,
                    pSetValIfCustTLVValue->i4_Length);

            MEMCPY (pIfTlvNode->pu1TlvVlaue,
                    pSetValIfCustTLVValue->pu1_OctetList,
                    pSetValIfCustTLVValue->i4_Length);
            pIfTlvNode->pu1TlvVlaue[pIfTlvNode->u4TlvLength] = '\0';

            pIfTlvNode->u4TlvLength = (UINT4) pSetValIfCustTLVValue->i4_Length;

            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIfCustTLVRowStatus
 Input       :  The Indices
                IfMainIndex
                IfCustTLVType

                The Object
                setValIfCustTLVRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfCustTLVRowStatus (INT4 i4IfMainIndex,
                          UINT4 u4IfCustTLVType,
                          INT4 i4SetValIfCustTLVRowStatus)
{
    tTMO_SLL           *pTlvList = NULL;
    tIfTlvInfoStruct   *pIfTlvNode = NULL;
    tIfTlvInfoStruct   *pIfFirstTlvNode = NULL;
    tIfTlvInfoStruct   *pIfNextTlvNode = NULL;
#ifdef NPAPI_WANTED
    tHwCustIfParamVal   HwCustIfParamVal;
#endif /*NPAPI_WANTED */
    UINT4               u4Index;
    UINT1               u1Flag = CFA_FALSE;

    /* Check for port range */
    if ((i4IfMainIndex <= 0) || (i4IfMainIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return SNMP_FAILURE;
    }

    u4Index = (UINT4) i4IfMainIndex;
    /* Null check has to be provided here to avoid crash as MSR does not call
     * Test routine */
    if (CFA_IF_ENTRY (u4Index) == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Get the list for the given port */
    pTlvList = (tTMO_SLL *) & CFA_IF_TLV_INFO (u4Index);

    if (i4SetValIfCustTLVRowStatus != CFA_RS_CREATEANDWAIT)
    {
        TMO_SLL_Scan (pTlvList, pIfTlvNode, tIfTlvInfoStruct *)
        {
            if (pIfTlvNode->u4TlvType == u4IfCustTLVType)
            {
                if (pIfTlvNode->i4TlvRowStatus == i4SetValIfCustTLVRowStatus)
                {
                    return SNMP_SUCCESS;
                }

                break;
            }
        }

    }

    switch (i4SetValIfCustTLVRowStatus)
    {
        case CFA_RS_CREATEANDWAIT:

            pIfTlvNode = (tIfTlvInfoStruct *) MemAllocMemBlk (CFA_TLV_MEMPOOL);

            if (pIfTlvNode == NULL)
            {
                return SNMP_FAILURE;
            }

            MEMSET (pIfTlvNode, 0, sizeof (tIfTlvInfoStruct));

            TMO_SLL_Init_Node (&pIfTlvNode->NextNode);

            pIfTlvNode->u4TlvType = u4IfCustTLVType;
            pIfTlvNode->i4TlvRowStatus = CFA_RS_NOTREADY;

            /*Add the nodes in sorted order */
            if ((pIfFirstTlvNode =
                 (tIfTlvInfoStruct *) TMO_SLL_First (pTlvList)) == NULL)
            {
                /* If no node is present then add the node */
                TMO_SLL_Add (pTlvList, &pIfTlvNode->NextNode);
                u1Flag = CFA_TRUE;
            }
            else
            {
                /* If node is present then scan the node and add in  
                 * relevent place */
                TMO_SLL_Scan (pTlvList, pIfFirstTlvNode, tIfTlvInfoStruct *)
                {
                    if (pIfFirstTlvNode->u4TlvType > u4IfCustTLVType)
                    {
                        /* Give type is the smallest one for this port. So 
                         * put this node at the head of linked list.*/
                        TMO_SLL_Insert (pTlvList,
                                        (tTMO_SLL_NODE *) NULL,
                                        &pIfTlvNode->NextNode);
                        u1Flag = CFA_TRUE;
                        break;
                    }

                    pIfNextTlvNode =
                        (tIfTlvInfoStruct *) TMO_SLL_Next (pTlvList,
                                                           (tTMO_SLL_NODE *)
                                                           & (pIfFirstTlvNode->
                                                              NextNode));

                    if (pIfNextTlvNode != NULL)
                    {
                        if ((pIfFirstTlvNode->u4TlvType < u4IfCustTLVType) &&
                            (pIfNextTlvNode->u4TlvType > u4IfCustTLVType))
                        {
                            TMO_SLL_Insert_In_Middle
                                (pTlvList,
                                 &pIfFirstTlvNode->NextNode,
                                 &pIfTlvNode->NextNode,
                                 &pIfNextTlvNode->NextNode);
                            u1Flag = CFA_TRUE;
                            break;

                        }
                    }
                    else
                    {
                        if (pIfFirstTlvNode->u4TlvType < u4IfCustTLVType)
                        {
                            TMO_SLL_Add (pTlvList, &pIfTlvNode->NextNode);
                            u1Flag = CFA_TRUE;
                            break;
                        }

                    }
                }
            }
            if (u1Flag != CFA_TRUE)
            {
                MemReleaseMemBlock (CFA_TLV_MEMPOOL, (UINT1 *) pIfTlvNode);
            }
            break;

        case CFA_RS_ACTIVE:
            if (pIfTlvNode == NULL)
            {
                return SNMP_FAILURE;
            }

            if (pIfTlvNode->i4TlvRowStatus != CFA_RS_NOTINSERVICE)
            {
                return SNMP_FAILURE;
            }

#ifdef NPAPI_WANTED

            HwCustIfParamVal.TLV.u4Type = pIfTlvNode->u4TlvType;

            HwCustIfParamVal.TLV.u4Length = pIfTlvNode->u4TlvLength;

            HwCustIfParamVal.TLV.pu1Value = pIfTlvNode->pu1TlvVlaue;

            if (CfaFsHwSetCustIfParams (u4Index, NP_CUST_IF_PRM_TYPE_TLV,
                                        HwCustIfParamVal, NP_ENTRY_ADD)
                == FNP_FAILURE)
            {
                return SNMP_FAILURE;
            }
#endif /*NPAPI_WANTED */

            pIfTlvNode->i4TlvRowStatus = CFA_RS_ACTIVE;

            break;

        case CFA_RS_DESTROY:
            if (pIfTlvNode == NULL)
            {
                return SNMP_SUCCESS;
            }

            if (pIfTlvNode->i4TlvRowStatus == CFA_RS_ACTIVE)
            {
#ifdef NPAPI_WANTED

                HwCustIfParamVal.TLV.u4Type = pIfTlvNode->u4TlvType;

                HwCustIfParamVal.TLV.u4Length = pIfTlvNode->u4TlvLength;

                HwCustIfParamVal.TLV.pu1Value = pIfTlvNode->pu1TlvVlaue;

                if (CfaFsHwSetCustIfParams (u4Index, NP_CUST_IF_PRM_TYPE_TLV,
                                            HwCustIfParamVal,
                                            NP_ENTRY_DELETE) == FNP_FAILURE)
                {
                    return SNMP_FAILURE;

                }
#endif /*NPAPI_WANTED */
            }

            if (MemBuddyFree ((UINT1) CFA_TLV_BUDDY_MEMPOOL,
                              pIfTlvNode->pu1TlvVlaue) == BUDDY_FAILURE)
            {
                return SNMP_FAILURE;
            }

            TMO_SLL_Delete (pTlvList, &pIfTlvNode->NextNode);

            if (MemReleaseMemBlock (CFA_TLV_MEMPOOL,
                                    (UINT1 *) pIfTlvNode) == MEM_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        default:

            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IfCustTLVLength
 Input       :  The Indices
                IfMainIndex
                IfCustTLVType

                The Object
                testValIfCustTLVLength
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfCustTLVLength (UINT4 *pu4ErrorCode,
                          INT4 i4IfMainIndex,
                          UINT4 u4IfCustTLVType, UINT4 u4TestValIfCustTLVLength)
{
    tTMO_SLL           *pTlvList = NULL;
    tIfTlvInfoStruct   *pIfTlvNode = NULL;
    UINT4               u4Index;

    /*Check for valid length */
    if ((u4TestValIfCustTLVLength <= 0) ||
        (u4TestValIfCustTLVLength > CFA_TLV_MAX_LENGTH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /*Check for port range */
    if ((i4IfMainIndex <= 0) || (i4IfMainIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    /*If the interface is not a briged interface then return Failure */
    if (CfaIfIsBridgedInterface (i4IfMainIndex) == CFA_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4Index = (UINT4) i4IfMainIndex;

    /*Get the list for the given port */
    pTlvList = (tTMO_SLL *) & CFA_IF_TLV_INFO (u4Index);

    TMO_SLL_Scan (pTlvList, pIfTlvNode, tIfTlvInfoStruct *)
    {
        if (pIfTlvNode->u4TlvType == u4IfCustTLVType)
        {
            /*Should not allow to set the length if the rowstatus of the 
               node is ACTIVE */
            if (pIfTlvNode->i4TlvRowStatus == CFA_RS_ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            else
            {
                return SNMP_SUCCESS;
            }
        }
    }

    /* Node is note present. */
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IfCustTLVValue
 Input       :  The Indices
                IfMainIndex
                IfCustTLVType

                The Object
                testValIfCustTLVValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfCustTLVValue (UINT4 *pu4ErrorCode,
                         INT4 i4IfMainIndex,
                         UINT4 u4IfCustTLVType,
                         tSNMP_OCTET_STRING_TYPE * pTestValIfCustTLVValue)
{
    tTMO_SLL           *pTlvList = NULL;
    tIfTlvInfoStruct   *pIfTlvNode = NULL;
    UINT4               u4Index;

    /*Check for port range */
    if ((i4IfMainIndex <= 0) || (i4IfMainIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    /*If the interface is not a briged interface is then return */
    if (CfaIfIsBridgedInterface (i4IfMainIndex) == CFA_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4Index = (UINT4) i4IfMainIndex;

    /*Get the list for the given port */
    pTlvList = (tTMO_SLL *) & CFA_IF_TLV_INFO (u4Index);

    TMO_SLL_Scan (pTlvList, pIfTlvNode, tIfTlvInfoStruct *)
    {
        if (pIfTlvNode->u4TlvType == u4IfCustTLVType)
        {
            /*Should not allow to set the value if the rowstatus of the
             *               node is ACTIVE */
            if (pIfTlvNode->i4TlvRowStatus == CFA_RS_ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CFA_CLI_TLV_ENTRY_ERR);
                return SNMP_FAILURE;
            }
            if ((pIfTlvNode->u4TlvLength == 0) ||
                (pTestValIfCustTLVValue->i4_Length !=
                 (INT4) pIfTlvNode->u4TlvLength))
            {
                CLI_SET_ERR (CFA_CLI_TLV_LENGTH_ERR);
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        }
    }

    /* No node found. */
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IfCustTLVRowStatus
 Input       :  The Indices
                IfMainIndex
                IfCustTLVType

                The Object
                testValIfCustTLVRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfCustTLVRowStatus (UINT4 *pu4ErrorCode,
                             INT4 i4IfMainIndex,
                             UINT4 u4IfCustTLVType,
                             INT4 i4TestValIfCustTLVRowStatus)
{

    tTMO_SLL           *pTlvList = NULL;
    tIfTlvInfoStruct   *pIfTlvNode = NULL;
    UINT4               u4Index;
    UINT4               u4StrLen;

    /* Check for port range */
    if ((i4IfMainIndex <= 0) || (i4IfMainIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    /* If the interface is not a briged interface is then return */
    if (CfaIfIsBridgedInterface (i4IfMainIndex) == CFA_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    u4Index = (UINT4) i4IfMainIndex;
    pTlvList = &CFA_IF_TLV_INFO (u4Index);

    switch (i4TestValIfCustTLVRowStatus)
    {
        case CFA_RS_CREATEANDGO:
        case CFA_RS_NOTINSERVICE:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case CFA_RS_CREATEANDWAIT:
            /*Check for max TLVs per port */
            if (TMO_SLL_Count (pTlvList) >= CFA_MAX_TLVS_PERPORT)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CFA_CLI_MAX_TLV_ERR);

                return SNMP_FAILURE;
            }
            TMO_SLL_Scan (pTlvList, pIfTlvNode, tIfTlvInfoStruct *)
            {
                /* If entry is already there then don't
                 * set the rowstatus as CreateAndWait*/
                if (pIfTlvNode->u4TlvType == u4IfCustTLVType)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            return SNMP_SUCCESS;

        case CFA_RS_ACTIVE:

            TMO_SLL_Scan (pTlvList, pIfTlvNode, tIfTlvInfoStruct *)
        {
            if (pIfTlvNode->u4TlvType == u4IfCustTLVType)
            {
                if (pIfTlvNode->i4TlvRowStatus == i4TestValIfCustTLVRowStatus)
                {
                    return SNMP_SUCCESS;
                }

                /* If length is not set, then don't allow to set the 
                 * rowstatus as active*/
                if (pIfTlvNode->u4TlvLength == 0)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

                /* If the value is not set then do not allow set 
                 * the rowstatus active */
                u4StrLen = STRLEN (pIfTlvNode->pu1TlvVlaue);
                if (pIfTlvNode->u4TlvLength != u4StrLen)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;

                }

                return SNMP_SUCCESS;
            }
        }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case CFA_RS_DESTROY:
            return SNMP_SUCCESS;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
    }
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IfCustTLVTable
 Input       :  The Indices
                IfMainIndex
                IfCustTLVType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfCustTLVTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IfCustOpaqueAttrTable
 Input       :  The Indices
                IfMainIndex
                IfCustOpaqueAttributeID
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfCustOpaqueAttrTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FfIpChecksumValidationEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FfIpChecksumValidationEnable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IfBridgeILanIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIfBridgeILanIfTable 
 Input       :  The Indices
                IfBridgeILanIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIfBridgeILanIfTable (INT4 i4IfIndex)
{
    UINT4               u4IfIndex;

    if ((i4IfIndex > CFA_MAX_ILAN_IF_INDEX) ||
        (i4IfIndex < CFA_MIN_ILAN_IF_INDEX))
    {
        return (SNMP_FAILURE);
    }

    u4IfIndex = (UINT4) i4IfIndex;

    if (CFA_IF_ENTRY_USED (u4IfIndex))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIfBridgeILanIfTable
 Input       :  The Indices
                IfBridgeILanIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIfBridgeILanIfTable (INT4 *pi4IfIndex)
{
    UINT4               u4Index;
    u4Index = CFA_MIN_ILAN_IF_INDEX;
/* Get the First Used Index */
    CFA_CDB_SCAN_WITH_LOCK (u4Index, CFA_MAX_ILAN_IF_INDEX, CFA_ILAN)
    {
        if (CFA_IF_ENTRY_USED (u4Index))
        {
            *pi4IfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* GET_NEXT Routine.  */

/****************************************************************************
 Function    :  nmhGetNextIndexIfBridgeILanIfTable
 Input       :  The Indices
                IfBridgeILanIfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexIfBridgeILanIfTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    UINT4               u4IfIndex;
    UINT4               u4Counter;

    if ((i4IfIndex < CFA_MIN_ILAN_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_ILAN_IF_INDEX))
    {
        return SNMP_FAILURE;
    }

    u4IfIndex = (UINT4) i4IfIndex;
    u4Counter = u4IfIndex + 1;
    /* Next Index should always return the next used Index */
    CFA_CDB_SCAN_WITH_LOCK (u4Counter, CFA_MAX_ILAN_IF_INDEX, CFA_ILAN)
    {
        if (CFA_IF_ENTRY_USED (u4Counter))
        {
            *pi4NextIfIndex = (INT4) u4Counter;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIfBridgeILanIfStatus 
 Input       :  The Indices
                IfBridgeILanIfIndex

                The Object 
                retValIfILanBridgeIfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfBridgeILanIfStatus (INT4 i4IfIndex, INT4 *pi4RetValIfBridgeILanIfStatus)
{
    UINT4               u4IfIndex;
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i4IfIndex < CFA_MIN_ILAN_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_ILAN_IF_INDEX))
    {
        return (i1RetVal);
    }
    u4IfIndex = (UINT4) i4IfIndex;

    if (CFA_IF_ENTRY_USED (u4IfIndex))
    {
        *pi4RetValIfBridgeILanIfStatus = CFA_IF_RS (u4IfIndex);
        i1RetVal = SNMP_SUCCESS;
    }

    return (i1RetVal);
}

/* LOW LEVEL Routines for Table : IfTypeProtoDenyTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIfTypeProtoDenyTable
 Input       :  The Indices
                IfTypeProtoDenyContextId
                IfTypeProtoDenyIfType
                IfTypeProtoDenyBrgPortType
                IfTypeProtoDenyProtocol
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceIfTypeProtoDenyTable (UINT4 u4IfTypeProtoDenyContextId,
                                              INT4 i4IfTypeProtoDenyMainType,
                                              INT4 i4IfTypeProtoDenyBrgPortType,
                                              INT4 i4IfTypeProtoDenyProtocol)
{
    if (CfaValIfTypeProtoDenyTblIndex ((INT4) u4IfTypeProtoDenyContextId,
                                       i4IfTypeProtoDenyMainType,
                                       i4IfTypeProtoDenyBrgPortType,
                                       i4IfTypeProtoDenyProtocol)
        == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIfTypeProtoDenyTable
 Input       :  The Indices
                IfTypeProtoDenyContextId
                IfTypeProtoDenyIfType
                IfTypeProtoDenyBrgPortType
                IfTypeProtoDenyProtocol
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexIfTypeProtoDenyTable (UINT4 *pu4IfTypeProtoDenyContextId,
                                      INT4 *pi4IfTypeProtoDenyMainType,
                                      INT4 *pi4IfTypeProtoDenyBrgPortType,
                                      INT4 *pi4IfTypeProtoDenyProtocol)
{
    return (nmhGetNextIndexIfTypeProtoDenyTable
            (0, pu4IfTypeProtoDenyContextId, 0, pi4IfTypeProtoDenyMainType,
             0, pi4IfTypeProtoDenyBrgPortType, 0, pi4IfTypeProtoDenyProtocol));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIfTypeProtoDenyTable
 Input       :  The Indices
                IfTypeProtoDenyContextId
                nextIfTypeProtoDenyContextId
                IfTypeProtoDenyIfType
                nextIfTypeProtoDenyIfType
                IfTypeProtoDenyBrgPortType
                nextIfTypeProtoDenyBrgPortType
                IfTypeProtoDenyProtocol
                nextIfTypeProtoDenyProtocol
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIfTypeProtoDenyTable (UINT4 u4IfTypeProtoDenyContextId,
                                     UINT4 *pu4NextIfTypeProtoDenyContextId,
                                     INT4 i4IfTypeProtoDenyMainType,
                                     INT4 *pi4NextIfTypeProtoDenyIfType,
                                     INT4 i4IfTypeProtoDenyBrgPortType,
                                     INT4 *pi4NextIfTypeProtoDenyBrgPortType,
                                     INT4 i4IfTypeProtoDenyProtocol,
                                     INT4 *pi4NextIfTypeProtoDenyProtocol)
{
    UINT4               u4ContextId = 0;
    tL2IwfCfaIfTypeDenyProt L2IwfCfaIfTypeDenyProt;

    MEMSET (&L2IwfCfaIfTypeDenyProt, 0, sizeof (tL2IwfCfaIfTypeDenyProt));

    L2IwfCfaIfTypeDenyProt.u4ContextId = u4IfTypeProtoDenyContextId;
    L2IwfCfaIfTypeDenyProt.i4IfType = i4IfTypeProtoDenyMainType;
    L2IwfCfaIfTypeDenyProt.i4BrgPortType = i4IfTypeProtoDenyBrgPortType;
    L2IwfCfaIfTypeDenyProt.i4Protocol = i4IfTypeProtoDenyProtocol;
    L2IwfCfaIfTypeDenyProt.pu4NextContextId = pu4NextIfTypeProtoDenyContextId;
    L2IwfCfaIfTypeDenyProt.pi4NextIfType = pi4NextIfTypeProtoDenyIfType;
    L2IwfCfaIfTypeDenyProt.pi4NextBrgPortType =
        pi4NextIfTypeProtoDenyBrgPortType;
    L2IwfCfaIfTypeDenyProt.pi4NextProtocol = pi4NextIfTypeProtoDenyProtocol;
    L2IwfCfaIfTypeDenyProt.u4Action = L2IWF_CFA_GET_NEXT;

    for (u4ContextId = u4IfTypeProtoDenyContextId;
         u4ContextId < SYS_DEF_MAX_NUM_CONTEXTS; u4ContextId++)
    {
        L2IwfCfaIfTypeDenyProt.u4ContextId = u4ContextId;

        if (L2IwfIfDenyProtocolEntry (&L2IwfCfaIfTypeDenyProt) == L2IWF_SUCCESS)
        {
            return SNMP_SUCCESS;
        }

        L2IwfCfaIfTypeDenyProt.i4BrgPortType = 0;
        L2IwfCfaIfTypeDenyProt.i4Protocol = 0;
        L2IwfCfaIfTypeDenyProt.i4IfType = 0;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIfTypeProtoDenyRowStatus
 Input       :  The Indices
                IfTypeProtoDenyContextId
                IfTypeProtoDenyIfType
                IfTypeProtoDenyBrgPortType
                IfTypeProtoDenyProtocol

                The Object
                retValIfTypeProtoDenyRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfTypeProtoDenyRowStatus (UINT4 u4IfTypeProtoDenyContextId,
                                INT4 i4IfTypeProtoDenyMainType,
                                INT4 i4IfTypeProtoDenyBrgPortType,
                                INT4 i4IfTypeProtoDenyProtocol,
                                INT4 *pi4RetValIfTypeProtoDenyRowStatus)
{
    tL2IwfCfaIfTypeDenyProt L2IwfCfaIfTypeDenyProt;

    MEMSET (&L2IwfCfaIfTypeDenyProt, 0, sizeof (tL2IwfCfaIfTypeDenyProt));

    L2IwfCfaIfTypeDenyProt.u4ContextId = u4IfTypeProtoDenyContextId;
    L2IwfCfaIfTypeDenyProt.i4IfType = i4IfTypeProtoDenyMainType;
    L2IwfCfaIfTypeDenyProt.i4BrgPortType = i4IfTypeProtoDenyBrgPortType;
    L2IwfCfaIfTypeDenyProt.i4Protocol = i4IfTypeProtoDenyProtocol;
    L2IwfCfaIfTypeDenyProt.pi4RowStatus = pi4RetValIfTypeProtoDenyRowStatus;
    L2IwfCfaIfTypeDenyProt.u4Action = L2IWF_CFA_GET_RS;

    if (L2IwfIfDenyProtocolEntry (&L2IwfCfaIfTypeDenyProt) == L2IWF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIfTypeProtoDenyRowStatus
 Input       :  The Indices
                IfTypeProtoDenyContextId
                IfTypeProtoDenyIfType
                IfTypeProtoDenyBrgPortType
                IfTypeProtoDenyProtocol

                The Object
                setValIfTypeProtoDenyRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfTypeProtoDenyRowStatus (UINT4 u4IfTypeProtoDenyContextId,
                                INT4 i4IfTypeProtoDenyMainType,
                                INT4 i4IfTypeProtoDenyBrgPortType,
                                INT4 i4IfTypeProtoDenyProtocol,
                                INT4 i4SetValIfTypeProtoDenyRowStatus)
{
    tL2IwfCfaIfTypeDenyProt L2IwfCfaIfTypeDenyProt;
    tCfaIfInfo          IfInfo;
    UINT4               u4IfIndex = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    MEMSET (&L2IwfCfaIfTypeDenyProt, 0, sizeof (tL2IwfCfaIfTypeDenyProt));

    L2IwfCfaIfTypeDenyProt.u4ContextId = u4IfTypeProtoDenyContextId;
    L2IwfCfaIfTypeDenyProt.i4IfType = i4IfTypeProtoDenyMainType;
    L2IwfCfaIfTypeDenyProt.i4BrgPortType = i4IfTypeProtoDenyBrgPortType;
    L2IwfCfaIfTypeDenyProt.i4Protocol = i4IfTypeProtoDenyProtocol;
    L2IwfCfaIfTypeDenyProt.i4RowStatus = i4SetValIfTypeProtoDenyRowStatus;

    switch (i4SetValIfTypeProtoDenyRowStatus)
    {
        case CFA_RS_CREATEANDGO:

            L2IwfCfaIfTypeDenyProt.u4Action = L2IWF_CFA_SET_RS_CRT;
            break;

        case CFA_RS_DESTROY:

            L2IwfCfaIfTypeDenyProt.u4Action = L2IWF_CFA_SET_RS_DES;
            break;

        case CFA_RS_ACTIVE:
            return SNMP_SUCCESS;

        default:
            return SNMP_FAILURE;
    }

    if (L2IwfIfDenyProtocolEntry (&L2IwfCfaIfTypeDenyProt) == L2IWF_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* The IfType Deny Protocol entry is successfully created or deleted 
     * in L2IWF database. If IfType Deny Protocol entry
     * 
     * 1. Created, then all the ports that are created with IfType and 
     * Bridge Port type needs to be deleted in that particular module for 
     * which the entry is created. So Delete Port indication to the particular
     * module needs to be given here. It is indicated here to L2IWF and 
     * called by L2IWF as per the existing flow.
     *
     * 2. Deleted, then all the ports that have the IfType and Bridge Port type
     * needs to be created in that particular module, where it was deleted.
     * So create port indication is given here to L2IWF and L2IWF calls 
     * Create Port as per the existing flow.
     */

    CFA_CDB_SCAN_WITH_LOCK (u4IfIndex, BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                            CFA_ALL_IFTYPE)
    {
        if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPort) == VCM_FAILURE)
        {
            /* The Port is not mapped to any context */
            continue;
        }

        if (u4ContextId != u4IfTypeProtoDenyContextId)
        {
            /* This port is mapped to some other context, hence continue */
            continue;
        }

        MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
        CfaGetIfInfo (u4IfIndex, &IfInfo);

        if ((IfInfo.u1IfType == (UINT1) i4IfTypeProtoDenyMainType) &&
            (IfInfo.u1BrgPortType == (UINT1) i4IfTypeProtoDenyBrgPortType))
        {
            if (i4SetValIfTypeProtoDenyRowStatus == CFA_RS_CREATEANDGO)
            {
                /* Delete the Port in the particular module */
                L2IwfCfaIfTypeDenyProt.u4IfIndex = u4IfIndex;
                L2IwfCfaIfTypeDenyProt.u4Action = L2IWF_CFA_DEL_PORT;
            }
            else if (i4SetValIfTypeProtoDenyRowStatus == CFA_RS_DESTROY)
            {
                /* Create the Port in the particular module */
                L2IwfCfaIfTypeDenyProt.u4IfIndex = u4IfIndex;
                L2IwfCfaIfTypeDenyProt.u4Action = L2IWF_CFA_CRT_PORT;
            }

            L2IwfCfaIfTypeDenyProt.pIfInfo = &IfInfo;

            if (L2IwfIfDenyProtocolEntry (&L2IwfCfaIfTypeDenyProt) ==
                L2IWF_FAILURE)
            {
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IfTypeProtoDenyRowStatus
 Input       :  The Indices
                IfTypeProtoDenyContextId
                IfTypeProtoDenyIfType
                IfTypeProtoDenyBrgPortType
                IfTypeProtoDenyProtocol

                The Object
                testValIfTypeProtoDenyRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfTypeProtoDenyRowStatus (UINT4 *pu4ErrorCode,
                                   UINT4 u4IfTypeProtoDenyContextId,
                                   INT4 i4IfTypeProtoDenyMainType,
                                   INT4 i4IfTypeProtoDenyBrgPortType,
                                   INT4 i4IfTypeProtoDenyProtocol,
                                   INT4 i4TestValIfTypeProtoDenyRowStatus)
{
    tL2IwfCfaIfTypeDenyProt L2IwfCfaIfTypeDenyProt;
    INT4                i4RowStatus = 0;

    MEMSET (&L2IwfCfaIfTypeDenyProt, 0, sizeof (tL2IwfCfaIfTypeDenyProt));

    L2IwfCfaIfTypeDenyProt.u4ContextId = u4IfTypeProtoDenyContextId;
    L2IwfCfaIfTypeDenyProt.i4IfType = i4IfTypeProtoDenyMainType;
    L2IwfCfaIfTypeDenyProt.i4BrgPortType = i4IfTypeProtoDenyBrgPortType;
    L2IwfCfaIfTypeDenyProt.i4Protocol = i4IfTypeProtoDenyProtocol;
    L2IwfCfaIfTypeDenyProt.pi4RowStatus = &i4RowStatus;
    L2IwfCfaIfTypeDenyProt.u4Action = L2IWF_CFA_GET_RS;

    if ((i4TestValIfTypeProtoDenyRowStatus != CFA_RS_CREATEANDGO) &&
        (i4TestValIfTypeProtoDenyRowStatus != CFA_RS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (CfaValIfTypeProtoDenyTblIndex ((INT4) u4IfTypeProtoDenyContextId,
                                       i4IfTypeProtoDenyMainType,
                                       i4IfTypeProtoDenyBrgPortType,
                                       i4IfTypeProtoDenyProtocol)
        == CFA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValIfTypeProtoDenyRowStatus == CFA_RS_CREATEANDGO)
    {
        if (L2IwfIfDenyProtocolEntry (&L2IwfCfaIfTypeDenyProt) == L2IWF_FAILURE)
        {
            return SNMP_SUCCESS;
        }

        if (i4RowStatus == CFA_RS_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2IfTypeProtoDenyTable
Input       :  The Indices
IfTypeProtoDenyContextId
IfTypeProtoDenyIfType
IfTypeProtoDenyBrgPortType
IfTypeProtoDenyProtocol
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2IfTypeProtoDenyTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfIpDestMacAddress
 Input       :  The Indices
                IfMainIndex
                                                                    
                The Object
                testValIfMainExtMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfIpDestMacAddress (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                             tMacAddr TestValIfIpDestMacAddress)
{
    tMacAddr            zeroAddr;
    tMacAddr            BcastMacAddr;
    UINT1               u1IfType;
    UINT4               u4IfIndex;
    tIpConfigInfo       IpIfInfo;

    /* Check the mac address to be set is not a complete zero address
       00:00:00:00:00:00 or Broadcast Address. */
    MEMSET (zeroAddr, 0, CFA_ENET_ADDR_LEN);
    MEMSET (BcastMacAddr, 0xff, CFA_ENET_ADDR_LEN);

    if ((MEMCMP (TestValIfIpDestMacAddress, zeroAddr,
                 CFA_ENET_ADDR_LEN) == 0) ||
        (MEMCMP (TestValIfIpDestMacAddress, BcastMacAddr,
                 CFA_ENET_ADDR_LEN) == 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* If the mac address is multicast, currently only the reserved address 
     * 01:00:5E:90:00:00 for MPLS-TP for use over point-to-point links 
     * is allowed.*/
    if ((CFA_IS_MAC_MULTICAST (TestValIfIpDestMacAddress)) &&
        (CFA_IS_MAC_UNNUM_MULTICAST (TestValIfIpDestMacAddress) != 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* for index validation - because validation is commented in mid-level */
    if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_CFA_INVALID_INTERFACE_MAC_ERR);
        return SNMP_FAILURE;
    }

    u4IfIndex = (UINT4) i4IfMainIndex;
    CfaGetIfType (u4IfIndex, &u1IfType);

    if ((u1IfType != CFA_ENET) && (u1IfType != CFA_L3IPVLAN))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_IFTYPE_MACADDR_ERR);
        return SNMP_FAILURE;
    }
    if (CfaIpIfGetIfInfo (u4IfIndex, &IpIfInfo) == CFA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_CFA_INVALID_INTERFACE_MAC_ERR);
        return SNMP_FAILURE;
    }
    if (IpIfInfo.u4Addr != 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_INVALID_IP_UNNUM_PEER_MAC);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfIpDestMacAddress
 Input       :  The Indices
                IfMainIndex
                                                                         
                The Object
                setValIfIpDestMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfIpDestMacAddress (INT4 i4IfMainIndex, tMacAddr SetValIfIpDestMacAddress)
{
    tMacAddr            au1MacAddr;
    UINT4               u4IfIndex;

    MEMSET (au1MacAddr, 0, CFA_ENET_ADDR_LEN);
    u4IfIndex = (UINT4) i4IfMainIndex;

    /* Get the Macaddress from Cfa data Base */
    CfaGetIfUnnumPeerMac (u4IfIndex, au1MacAddr);

    /* Check for the Mac address from CFA is same as input MAC */

    if (MEMCMP (au1MacAddr, SetValIfIpDestMacAddress, CFA_ENET_ADDR_LEN) == 0)
    {
        return SNMP_SUCCESS;
    }

    CfaSetIfUnnumPeerMac (u4IfIndex, SetValIfIpDestMacAddress,
                          CFA_ENET_ADDR_LEN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfIpDestMacAddress
 Input       :  The Indices
                IfMainIndex
                                                                                                The Object
                retValIfIpDestMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfIpDestMacAddress (INT4 i4IfMainIndex,
                          tMacAddr * pRetValIfIpDestMacAddress)
{
    UINT4               u4IfIndex;
    tMacAddr            au1MacAddr;

    MEMSET (au1MacAddr, 0, CFA_ENET_ADDR_LEN);
    u4IfIndex = (UINT4) i4IfMainIndex;

    /* Validate the given interface */
    if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_CFA_INVALID_INTERFACE_MAC_ERR);
        return SNMP_FAILURE;
    }

    /*Get the Mac address for the given port from CFA database */
    CfaGetIfUnnumPeerMac (u4IfIndex, au1MacAddr);
    MEMCPY (pRetValIfIpDestMacAddress, au1MacAddr, CFA_ENET_ADDR_LEN);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IfIpUnnumAssocIfIPAddr
 Input       :  The Indices
                IfMainIndex
                                                                    
                The Object
                testValIfMainExtMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfIpUnnumAssocIPIf (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                             UINT4 u4TestValIfIpUnnumAssocIPIf)
{
    tIpConfigInfo       IpIfInfo;
    UINT1               u1IfType = 0;
    UINT1               u1IfMainType = 0;

    CfaGetIfType (u4TestValIfIpUnnumAssocIPIf, &u1IfType);
    CfaGetIfType ((UINT4) i4IfMainIndex, &u1IfMainType);
    if (u1IfMainType == CFA_PPP)
    {
        if (CfaValidateIfIndex ((UINT4) i4IfMainIndex) == CFA_FAILURE)
        {
            CLI_SET_ERR (CLI_CFA_PPP_NOT_STACKED_ERR);
            return SNMP_FAILURE;
        }
    }
    if ((u1IfType != CFA_ENET)
        && (u1IfType != CFA_L3IPVLAN) && (u1IfType != CFA_LOOPBACK))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_CFA_INVALID_IP_UNNUM_ASSOC_IF);
        return SNMP_FAILURE;

    }
    MEMSET (&IpIfInfo, 0, sizeof (tIpConfigInfo));
    if (CfaIpIfGetIfInfo ((UINT4) i4IfMainIndex, &IpIfInfo) == CFA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_CFA_INVALID_IP_UNNUM_ASSOC_IF);
        return SNMP_FAILURE;
    }
    if (IpIfInfo.u4Addr != 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_INVALID_IP_UNNUM_ASSOC_IF);
        return SNMP_FAILURE;
    }

    MEMSET (&IpIfInfo, 0, sizeof (tIpConfigInfo));
    if (CfaIpIfGetIfInfo (u4TestValIfIpUnnumAssocIPIf, &IpIfInfo) ==
        CFA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_CFA_INVALID_IP_UNNUM_ASSOC_IF);
        return SNMP_FAILURE;
    }
    if (IpIfInfo.u4Addr == 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_INVALID_IP_UNNUM_ASSOC_IF);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfIpUnnumAssocIPIf
 Input       :  The Indices
                IfMainIndex
                                                                         
                The Object
                setValIfIpDestMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfIpUnnumAssocIPIf (INT4 i4IfMainIndex, UINT4 u4SetValIfIpUnnumAssocIPIf)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
#ifdef PPP_WANTED
    UINT1               u1IfType = 0;
    tIpConfigInfo       IpIfInfo;
    tIfLayerInfo        LowInterface;
    tIfLayerInfo        HighInterface;
    tPppIpInfo          IpInfo;
    UINT4               u4CurMtu = 0;

    MEMSET (&IpIfInfo, 0, sizeof (tIpConfigInfo));
    CfaGetIfType ((UINT4) i4IfMainIndex, &u1IfType);

    if (u1IfType == CFA_PPP)
    {

        CfaIpIfGetIfInfo (u4SetValIfIpUnnumAssocIPIf, &IpIfInfo);

        LowInterface.u1Command = HighInterface.u1Command = CFA_LAYER_IGNORE;
        IpInfo.u4LocalIpAddr = IpIfInfo.u4Addr;
        IpInfo.u4PeerIpAddr = CFA_IF_PEER_IPADDR ((UINT4) i4IfMainIndex);
        IpInfo.u1SubnetMask = (UINT1) IpIfInfo.u4NetMask;

        CfaGetIfMtu ((UINT4) i4IfMainIndex, &u4CurMtu);
        if (CfaIfmUpdatePppInterface ((UINT4) i4IfMainIndex, &LowInterface,
                                      &HighInterface, &IpInfo,
                                      CFA_IF_IDLE_TIMEOUT
                                      ((UINT4) i4IfMainIndex),
                                      u4CurMtu) != CFA_SUCCESS)
        {
            CLI_SET_ERR (CFA_CLI_IFM_CONFIG_ERR);
            return SNMP_FAILURE;
        }

    }
#endif /* PPP_WANTED */

    CfaSetIfUnnumAssocIPIf ((UINT4) i4IfMainIndex, u4SetValIfIpUnnumAssocIPIf);
    SnmpNotifyInfo.pu4ObjectId = IfIpUnnumAssocIPIf;
    SnmpNotifyInfo.u4OidLen = sizeof (IfIpUnnumAssocIPIf) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u", i4IfMainIndex,
                      u4SetValIfIpUnnumAssocIPIf));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfIpUnnumAssocIPIf
 Input       :  The Indices
                IfMainIndex
                                                                                                The Object
                retValIfIpDestMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfIpUnnumAssocIPIf (INT4 i4IfMainIndex,
                          UINT4 *pu4RetValIfIpUnnumAssocIPIf)
{
    CfaGetIfUnnumAssocIPIf ((UINT4) i4IfMainIndex, pu4RetValIfIpUnnumAssocIPIf);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 *  Function    :  nmhGetIfmDebug
 *   Input       :  The Indices the Object
 *                                   retValIfmDebug
 *                                    Output      :  The Get Low Lev Routine Take the Indices &
 *                                                    store the Value requested in the Return val.
 *                                                     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *                                                     ****************************************************************************/
INT1
nmhGetIfmDebug (UINT4 *pu4RetValIfmDebug)
{
    *pu4RetValIfmDebug = CFA_GLB_TRC ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 *  Function    :  nmhSetIfmDebug
 *   Input       :  The Indices
 *
 *                   The Object
 *                                   setValIfmDebug
 *                                    Output      :  The Set Low Lev Routine Take the Indices &
 *                                                    Sets the Value accordingly.
 *                                                     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *                                                     ****************************************************************************/
INT1
nmhSetIfmDebug (UINT4 u4SetValIfmDebug)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;

    CFA_GLB_TRC () = u4SetValIfmDebug;

    /* Sending Trigger to MSR */
    SnmpNotifyInfo.pu4ObjectId = IfmDebug;
    SnmpNotifyInfo.u4OidLen = sizeof (IfmDebug) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = 0;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u", u4SetValIfmDebug));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 *  Function    :  nmhTestv2IfmDebug
 *   Input       :  The Indices
 *
 *                   The Object
 *                                   testValIfmDebug
 *                                    Output      :  The Test Low Lev Routine Take the Indices &
 *                                                    Test whether that Value is Valid Input for Set.
 *                                                                    Stores the value of error code in the Return val
 *                                                                     Error Codes :  The following error codes are to be returned
 *                                                                                     SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                                                                                                     SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                                                                                                                     SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                                                                                                                                     SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                                                                                                                                                     SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *                                                                                                                                                      Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *                                                                                                                                                      ****************************************************************************/
INT1
nmhTestv2IfmDebug (UINT4 *pu4ErrorCode, UINT4 u4TestValIfmDebug)
{
#ifdef TRACE_WANTED
    if (u4TestValIfmDebug > CFA_TRC_ALL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_CFA_TRC_LEVEL_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_CFA_TRC_LEVEL_ERR);
    UNUSED_PARAM (u4TestValIfmDebug);
    return SNMP_FAILURE;
#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 *  Function    :  nmhDepv2IfmDebug
 *   Output      :  The Dependency Low Lev Routine Take the Indices &
 *                   check whether dependency is met or not.
 *                                   Stores the value of error code in the Return val
 *                                    Error Codes :  The following error codes are to be returned
 *                                                    SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                                                                    SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                                                                                    SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                                                                                                    SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                                                                                                                    SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *                                                                                                                     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *                                                                                                                     ****************************************************************************/
INT1
nmhDepv2IfmDebug (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IfIvrMappingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIfIvrMappingTable
 Input       :  The Indices
                IfMainIndex
                IfIvrMappingVlan
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIfIvrMappingTable (INT4 i4IfMainIndex,
                                           INT4 i4IfIvrMappingVlan)
{
    if ((i4IfMainIndex < CFA_MIN_IVR_IF_INDEX) ||
        (i4IfMainIndex > CFA_MAX_IVR_IF_INDEX))
    {
        return SNMP_FAILURE;
    }

    if ((i4IfIvrMappingVlan < VLAN_MIN_VLAN_ID) ||
        (i4IfIvrMappingVlan > VLAN_MAX_VLAN_ID))
    {
        return SNMP_FAILURE;
    }

    if (CfaIvrMapGetInfo ((UINT4) i4IfMainIndex, (tVlanId) i4IfIvrMappingVlan,
                          NULL) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIfIvrMappingTable
 Input       :  The Indices
                IfMainIndex
                IfIvrMappingVlan
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIfIvrMappingTable (INT4 *pi4IfMainIndex,
                                   INT4 *pi4IfIvrMappingVlan)
{
    return (nmhGetNextIndexIfIvrMappingTable (0, pi4IfMainIndex, 0,
                                              pi4IfIvrMappingVlan));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIfIvrMappingTable
 Input       :  The Indices
                IfMainIndex
                nextIfMainIndex
                IfIvrMappingVlan
                nextIfIvrMappingVlan
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIfIvrMappingTable (INT4 i4IfMainIndex,
                                  INT4 *pi4NextIfMainIndex,
                                  INT4 i4IfIvrMappingVlan,
                                  INT4 *pi4NextIfIvrMappingVlan)
{
    if (CfaIvrMapGetNextInfo
        ((UINT4) i4IfMainIndex, (tVlanId) i4IfIvrMappingVlan,
         pi4NextIfMainIndex, pi4NextIfIvrMappingVlan) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIfIvrMappingRowStatus
 Input       :  The Indices
                IfMainIndex
                IfIvrMappingVlan

                The Object 
                retValIfIvrMappingRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfIvrMappingRowStatus (INT4 i4IfMainIndex, INT4 i4IfIvrMappingVlan,
                             INT4 *pi4RetValIfIvrMappingRowStatus)
{
    tCfaVlanToIvrMapInfo PvlanEntry;

    /* Check whether the entry exist or not */
    if (CfaIvrMapGetInfo ((UINT4) i4IfMainIndex, (tVlanId) i4IfIvrMappingVlan,
                          &PvlanEntry) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIfIvrMappingRowStatus = CFA_RS_ACTIVE;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIfIvrMappingRowStatus
 Input       :  The Indices
                IfMainIndex
                IfIvrMappingVlan

                The Object 
                setValIfIvrMappingRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfIvrMappingRowStatus (INT4 i4IfMainIndex, INT4 i4IfIvrMappingVlan,
                             INT4 i4SetValIfIvrMappingRowStatus)
{
    if ((i4IfMainIndex < CFA_MIN_IVR_IF_INDEX) ||
        (i4IfMainIndex > CFA_MAX_IVR_IF_INDEX))
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValIfIvrMappingRowStatus)
    {
        case CFA_RS_DESTROY:

            if (CfaIvrMapDeleteInfo ((UINT4) i4IfMainIndex,
                                     (UINT4) i4IfIvrMappingVlan) == CFA_FAILURE)
            {
                return SNMP_FAILURE;
            }

            break;
        case CFA_RS_CREATEANDGO:

            if (CfaIvrMapSetInfo
                ((UINT4) i4IfMainIndex,
                 (tVlanId) i4IfIvrMappingVlan) == CFA_FAILURE)
            {
                return SNMP_FAILURE;
            }
        default:
            break;
    }

    /* silently ignore the NotInService/CreateAndWait/Active cases */
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IfIvrMappingRowStatus
 Input       :  The Indices
                IfMainIndex
                IfIvrMappingVlan

                The Object 
                testValIfIvrMappingRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfIvrMappingRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                                INT4 i4IfIvrMappingVlan,
                                INT4 i4TestValIfIvrMappingRowStatus)
{
    INT4                i4Result;
    tCfaVlanToIvrMapInfo PvlanEntry;
    UINT1               u1BrgPortType = 0;

    CfaGetIfBrgPortType ((UINT4) i4IfMainIndex, &u1BrgPortType);

    if (u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        /* Private Vlan Mapping is not applicable for
         * S-Channel Interface */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_SBP_ERR);
        return SNMP_FAILURE;
    }

    if ((i4IfMainIndex < CFA_MIN_IVR_IF_INDEX) ||
        (i4IfMainIndex > CFA_MAX_IVR_IF_INDEX))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4IfIvrMappingVlan < VLAN_MIN_VLAN_ID) ||
        (i4IfIvrMappingVlan > VLAN_MAX_VLAN_ID))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (CfaValidateIfIndex ((UINT4) i4IfMainIndex) == CFA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i4Result =
        CfaIvrMapGetInfo ((UINT4) i4IfMainIndex, (tVlanId) i4IfIvrMappingVlan,
                          &PvlanEntry);

    switch (i4TestValIfIvrMappingRowStatus)
    {
        case CFA_RS_CREATEANDGO:
            if (i4Result == CFA_SUCCESS)
            {
                CLI_SET_ERR (CLI_CFA_IVR_VLAN_ALREADY_EXISTS_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            break;
        case CFA_RS_DESTROY:
            if (i4Result == CFA_FAILURE)
            {
                CLI_SET_ERR (CLI_CFA_IVR_VLAN_NOT_FOUND_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        case CFA_RS_ACTIVE:
        case CFA_RS_CREATEANDWAIT:
        case CFA_RS_NOTINSERVICE:
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IfIvrMappingTable
 Input       :  The Indices
                IfMainIndex
                IfIvrMappingVlan
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfIvrMappingTable (UINT4 *pu4ErrorCode,
                           tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainDesc
 Input       :  The Indices
                IfMainIndex

                The Object
                retValIfMainDesc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetIfMainDesc (INT4 i4IfMainIndex,
                  tSNMP_OCTET_STRING_TYPE * pRetValIfMainDesc)
{
    pRetValIfMainDesc->i4_Length
        = (INT4) STRLEN (CFA_CDB_IF_DESCRIPTION ((UINT4) i4IfMainIndex));
    MEMCPY (pRetValIfMainDesc->pu1_OctetList,
            CFA_CDB_IF_DESCRIPTION ((UINT4) i4IfMainIndex),
            pRetValIfMainDesc->i4_Length);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIfMainDesc
 Input       :  The Indices
                IfMainIndex

                The Object
                setValIfMainDesc
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetIfMainDesc (INT4 i4IfMainIndex,
                  tSNMP_OCTET_STRING_TYPE * pSetValIfMainDesc)
{

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    MEMSET (CFA_CDB_IF_DESCRIPTION ((UINT4) i4IfMainIndex), 0,
            CFA_MAX_INTF_DESC_LEN);

    MEMCPY (CFA_CDB_IF_DESCRIPTION ((UINT4) i4IfMainIndex),
            pSetValIfMainDesc->pu1_OctetList, pSetValIfMainDesc->i4_Length);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IfMainDesc
 Input       :  The Indices
                IfMainIndex

                The Object
                testValIfMainDesc
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2IfMainDesc (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                     tSNMP_OCTET_STRING_TYPE * pTestValIfMainDesc)
{
    if (CfaValidateIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        CLI_SET_ERR (CLI_CFA_INVALID_INDEX);
        return SNMP_FAILURE;
    }

    if (pTestValIfMainDesc->i4_Length >= CFA_MAX_INTF_DESC_LEN)
    {
        CLI_SET_ERR (CLI_CFA_INVALID_DESCRIPTION);
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainStorageType
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfMainStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainStorageType (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainStorageType)
{

    *pi4RetValIfMainStorageType =
        CFA_CDB_IF_STORAGE_TYPE ((UINT4) i4IfMainIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfMainStorageType
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfMainStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainStorageType (INT4 i4IfMainIndex, INT4 i4SetValIfMainStorageType)
{
    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    CfaSetIfMainStorageType ((UINT4) i4IfMainIndex, i4SetValIfMainStorageType);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfMainStorageType
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfMainStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainStorageType (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                            INT4 i4TestValIfMainStorageType)
{
    UINT4               u4IfIndex;

    u4IfIndex = (UINT4) i4IfMainIndex;

    if (CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        /* for index validation - because validation is commented in mid-level */
        if (CfaValidateIfMainTableIndex (i4IfMainIndex) != CFA_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }

        if (!(i4TestValIfMainStorageType == CFA_STORAGE_TYPE_VOLATILE ||
              i4TestValIfMainStorageType == CFA_STORAGE_TYPE_NONVOLATILE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IfAvailableIndexTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIfAvailableIndexTable
 Input       :  The Indices
                IfType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIfAvailableIndexTable (INT4 i4IfType)
{

    switch (i4IfType)
    {
        case CFA_ENET:
            break;

        case CFA_PPP:
            break;

        case CFA_LOOPBACK:
            break;

        case CFA_TUNNEL:
            break;

        case CFA_L3IPVLAN:
            break;

        case CFA_MPLS_TUNNEL:
            break;

        case CFA_LAGG:
            break;

        case CFA_MPLS:
            break;

        case CFA_TELINK:
            break;

        case CFA_BRIDGED_INTERFACE:
            break;

        case CFA_PSEUDO_WIRE:
            break;

        case CFA_ILAN:
            break;

        case CFA_PIP:
            break;

        case CFA_VPNC:
            break;

        case CFA_VXLAN_NVE:
            break;

        case CFA_TAP:
            break;

        default:
            /* unknown interface type */
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIfAvailableIndexTable
 Input       :  The Indices
                IfType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIfAvailableIndexTable (INT4 *pi4IfType)
{
    *pi4IfType = 0;

    if (nmhGetNextIndexIfAvailableIndexTable (*pi4IfType,
                                              pi4IfType) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIfAvailableIndexTable
 Input       :  The Indices
                IfType
                nextIfType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIfAvailableIndexTable (INT4 i4IfType, INT4 *pi4NextIfType)
{
    INT4                i4IfAvailableFreeIndex = -1;

    switch (i4IfType)
    {
        case 0:
            /* for ethernet interface */
            *pi4NextIfType = CFA_ENET;
            if ((CfaGetFreeInterfaceIndex
                 ((UINT4 *) &i4IfAvailableFreeIndex,
                  (UINT4) *pi4NextIfType) != OSIX_FAILURE) &&
                (i4IfAvailableFreeIndex != 0))
            {
                CFA_DS_LOCK ();
                CFA_CDB_IF_DB_SET_INTF_VALID (*
                                              ((UINT4 *)
                                               &i4IfAvailableFreeIndex),
                                              CFA_FALSE);
                CFA_DS_UNLOCK ();

                break;
            }
        case CFA_ENET:
            /* for ethernet interface */

#ifdef PPP_WANTED
            *pi4NextIfType = CFA_PPP;
#else
            *pi4NextIfType = CFA_LOOPBACK;
#endif

            if ((CfaGetFreeInterfaceIndex
                 ((UINT4 *) &i4IfAvailableFreeIndex,
                  (UINT4) *pi4NextIfType) != OSIX_FAILURE) &&
                (i4IfAvailableFreeIndex != 0))
            {
                CFA_DS_LOCK ();
                CFA_CDB_IF_DB_SET_INTF_VALID (*
                                              ((UINT4 *)
                                               &i4IfAvailableFreeIndex),
                                              CFA_FALSE);
                CFA_DS_UNLOCK ();

                break;
            }

        case CFA_PPP:
            /* For PPP interface */
            *pi4NextIfType = CFA_LOOPBACK;
            if ((CfaGetFreeInterfaceIndex
                 ((UINT4 *) &i4IfAvailableFreeIndex,
                  (UINT4) *pi4NextIfType) != OSIX_FAILURE) &&
                (i4IfAvailableFreeIndex != 0))
            {
                CFA_DS_LOCK ();
                CFA_CDB_IF_DB_SET_INTF_VALID (*
                                              ((UINT4 *)
                                               &i4IfAvailableFreeIndex),
                                              CFA_FALSE);
                CFA_DS_UNLOCK ();

                break;
            }

        case CFA_LOOPBACK:
            /* For loopback interface */
            *pi4NextIfType = CFA_TUNNEL;

            if ((CfaGetFreeInterfaceIndex
                 ((UINT4 *) &i4IfAvailableFreeIndex,
                  (UINT4) *pi4NextIfType) != OSIX_FAILURE) &&
                (i4IfAvailableFreeIndex != 0))
            {
                CFA_DS_LOCK ();
                CFA_CDB_IF_DB_SET_INTF_VALID (*
                                              ((UINT4 *)
                                               &i4IfAvailableFreeIndex),
                                              CFA_FALSE);
                CFA_DS_UNLOCK ();

                break;
            }

        case CFA_TUNNEL:
            /* for IP Tunnel interface */

            *pi4NextIfType = CFA_L3IPVLAN;

            if ((CfaGetFreeInterfaceIndex
                 ((UINT4 *) &i4IfAvailableFreeIndex,
                  (UINT4) *pi4NextIfType) != OSIX_FAILURE) &&
                (i4IfAvailableFreeIndex != 0))
            {
                CFA_DS_LOCK ();
                CFA_CDB_IF_DB_SET_INTF_VALID (*
                                              ((UINT4 *)
                                               &i4IfAvailableFreeIndex),
                                              CFA_FALSE);
                CFA_DS_UNLOCK ();

                break;
            }

        case CFA_L3IPVLAN:
            /* for IVR interface */
            *pi4NextIfType = CFA_MPLS_TUNNEL;
            if ((CfaGetFreeInterfaceIndex
                 ((UINT4 *) &i4IfAvailableFreeIndex,
                  (UINT4) *pi4NextIfType) != OSIX_FAILURE) &&
                (i4IfAvailableFreeIndex != 0))
            {
                CFA_DS_LOCK ();
                CFA_CDB_IF_DB_SET_INTF_VALID (*
                                              ((UINT4 *)
                                               &i4IfAvailableFreeIndex),
                                              CFA_FALSE);
                CFA_DS_UNLOCK ();

                break;
            }

        case CFA_MPLS_TUNNEL:
            /* for MPLS Tunnel interface */
            *pi4NextIfType = CFA_LAGG;
            if ((CfaGetFreeInterfaceIndex
                 ((UINT4 *) &i4IfAvailableFreeIndex,
                  (UINT4) *pi4NextIfType) != OSIX_FAILURE) &&
                (i4IfAvailableFreeIndex != 0))
            {
                CFA_DS_LOCK ();
                CFA_CDB_IF_DB_SET_INTF_VALID (*
                                              ((UINT4 *)
                                               &i4IfAvailableFreeIndex),
                                              CFA_FALSE);
                CFA_DS_UNLOCK ();

                break;
            }

        case CFA_LAGG:
            /* for portchannel interface */
            *pi4NextIfType = CFA_MPLS;
            if ((CfaGetFreeInterfaceIndex
                 ((UINT4 *) &i4IfAvailableFreeIndex,
                  (UINT4) *pi4NextIfType) != OSIX_FAILURE) &&
                (i4IfAvailableFreeIndex != 0))
            {
                CFA_DS_LOCK ();
                CFA_CDB_IF_DB_SET_INTF_VALID (*
                                              ((UINT4 *)
                                               &i4IfAvailableFreeIndex),
                                              CFA_FALSE);
                CFA_DS_UNLOCK ();

                break;
            }

        case CFA_MPLS:
            /* for MPLS interface */
            *pi4NextIfType = CFA_TELINK;
            if ((CfaGetFreeInterfaceIndex
                 ((UINT4 *) &i4IfAvailableFreeIndex,
                  (UINT4) *pi4NextIfType) != OSIX_FAILURE) &&
                (i4IfAvailableFreeIndex != 0))
            {
                CFA_DS_LOCK ();
                CFA_CDB_IF_DB_SET_INTF_VALID (*
                                              ((UINT4 *)
                                               &i4IfAvailableFreeIndex),
                                              CFA_FALSE);
                CFA_DS_UNLOCK ();

                break;
            }

        case CFA_TELINK:
            /* For TLM Interface */
            *pi4NextIfType = CFA_BRIDGED_INTERFACE;
            if ((CfaGetFreeInterfaceIndex
                 ((UINT4 *) &i4IfAvailableFreeIndex,
                  (UINT4) *pi4NextIfType) != OSIX_FAILURE) &&
                (i4IfAvailableFreeIndex != 0))
            {
                CFA_DS_LOCK ();
                CFA_CDB_IF_DB_SET_INTF_VALID (*
                                              ((UINT4 *)
                                               &i4IfAvailableFreeIndex),
                                              CFA_FALSE);
                CFA_DS_UNLOCK ();

                break;
            }

        case CFA_BRIDGED_INTERFACE:

            /* For internal interface */
            *pi4NextIfType = CFA_PSEUDO_WIRE;
            if ((CfaGetFreeInterfaceIndex
                 ((UINT4 *) &i4IfAvailableFreeIndex,
                  (UINT4) *pi4NextIfType) != OSIX_FAILURE) &&
                (i4IfAvailableFreeIndex != 0))
            {
                CFA_DS_LOCK ();
                CFA_CDB_IF_DB_SET_INTF_VALID (*
                                              ((UINT4 *)
                                               &i4IfAvailableFreeIndex),
                                              CFA_FALSE);
                CFA_DS_UNLOCK ();

                break;
            }

        case CFA_PSEUDO_WIRE:
            /* For Pseudowire Interface */
            *pi4NextIfType = CFA_ILAN;
            if ((CfaGetFreeInterfaceIndex
                 ((UINT4 *) &i4IfAvailableFreeIndex,
                  (UINT4) *pi4NextIfType) != OSIX_FAILURE) &&
                (i4IfAvailableFreeIndex != 0))
            {
                CFA_DS_LOCK ();
                CFA_CDB_IF_DB_SET_INTF_VALID (*
                                              ((UINT4 *)
                                               &i4IfAvailableFreeIndex),
                                              CFA_FALSE);
                CFA_DS_UNLOCK ();

                break;
            }

        case CFA_ILAN:
            /* For ilan interface */
            *pi4NextIfType = CFA_PIP;
            if ((CfaGetFreeInterfaceIndex
                 ((UINT4 *) &i4IfAvailableFreeIndex,
                  (UINT4) *pi4NextIfType) != OSIX_FAILURE) &&
                (i4IfAvailableFreeIndex != 0))
            {
                CFA_DS_LOCK ();
                CFA_CDB_IF_DB_SET_INTF_VALID (*
                                              ((UINT4 *)
                                               &i4IfAvailableFreeIndex),
                                              CFA_FALSE);
                CFA_DS_UNLOCK ();

                break;
            }

        case CFA_PIP:

            *pi4NextIfType = CFA_VPNC;
            if ((CfaGetFreeInterfaceIndex
                 ((UINT4 *) &i4IfAvailableFreeIndex,
                  (UINT4) *pi4NextIfType) != OSIX_FAILURE) &&
                (i4IfAvailableFreeIndex != 0))
            {
                CFA_DS_LOCK ();
                CFA_CDB_IF_DB_SET_INTF_VALID (*
                                              ((UINT4 *)
                                               &i4IfAvailableFreeIndex),
                                              CFA_FALSE);
                CFA_DS_UNLOCK ();

                break;
            }

        case CFA_VPNC:
            /* For VPN dynamic interface */
            *pi4NextIfType = -1;
            return SNMP_FAILURE;
            break;

        default:
            return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIfAvailableFreeIndex
 Input       :  The Indices
                IfType

                The Object 
                retValIfAvailableFreeIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfAvailableFreeIndex (INT4 i4IfType, INT4 *pi4RetValIfAvailableFreeIndex)
{

    if (CfaGetFreeInterfaceIndex
        ((UINT4 *) pi4RetValIfAvailableFreeIndex,
         (UINT4) i4IfType) == OSIX_FAILURE)
    {

        return SNMP_FAILURE;
    }

    CFA_DS_LOCK ();
    CFA_CDB_IF_DB_SET_INTF_VALID (*((UINT4 *) pi4RetValIfAvailableFreeIndex),
                                  CFA_FALSE);
    CFA_DS_UNLOCK ();

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IfHCErrorTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIfHCErrorTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIfHCErrorTable (INT4 i4IfIndex)
{
    UINT4               u4IfIndex = 0;

    if (((UINT4) i4IfIndex > CFA_MAX_INTERFACES ()) || (i4IfIndex <= 0))
    {
        return (SNMP_FAILURE);
    }

    u4IfIndex = (UINT4) i4IfIndex;

    if (((CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_TRUE) &&
         (CFA_IF_ENTRY_USED (u4IfIndex))) ||
        ((CfaIsIfEntryProgrammingAllowed (u4IfIndex) == CFA_FALSE) &&
         (CfaValidateCfaIfIndex (u4IfIndex) == CFA_SUCCESS)))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIfHCErrorTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIfHCErrorTable (INT4 *pi4IfIndex)
{
    UINT4               u4Index = 1;

    CFA_CDB_SCAN_WITH_LOCK (u4Index, (CFA_MAX_INTERFACES ()), CFA_ALL_IFTYPE)
    {
        if (((CfaIsIfEntryProgrammingAllowed (u4Index) == CFA_TRUE) &&
             (CFA_IF_ENTRY_USED (u4Index))) ||
            ((CfaIsIfEntryProgrammingAllowed (u4Index) == CFA_FALSE) &&
             (CfaValidateCfaIfIndex (u4Index) == CFA_SUCCESS)))
        {
            *pi4IfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIfHCErrorTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIfHCErrorTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{

    UINT4               u4IfIndex;
    UINT4               u4Index;

    if (i4IfIndex < 0 || (UINT4) i4IfIndex > CFA_MAX_INTERFACES ())
    {
        return SNMP_FAILURE;
    }
    u4IfIndex = (UINT4) i4IfIndex;

    u4Index = u4IfIndex + 1;
    CFA_CDB_SCAN_WITH_LOCK (u4Index, (CFA_MAX_INTERFACES ()), CFA_ALL_IFTYPE)
    {
        if (((CfaIsIfEntryProgrammingAllowed (u4Index) == CFA_TRUE) &&
             (CFA_IF_ENTRY_USED (u4Index))) ||
            ((CfaIsIfEntryProgrammingAllowed (u4Index) == CFA_FALSE) &&
             (CfaValidateCfaIfIndex (u4Index) == CFA_SUCCESS)))
        {
            *pi4NextIfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIfHCInDiscards
 Input       :  The Indices
                IfIndex

                The Object 
                retValIfHCInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfHCInDiscards (INT4 i4IfIndex,
                      tSNMP_COUNTER64_TYPE * pu8RetValIfHCInDiscards)
{
    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        pu8RetValIfHCInDiscards->msn = 0;
        pu8RetValIfHCInDiscards->lsn = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        pu8RetValIfHCInDiscards->msn = 0;
        pu8RetValIfHCInDiscards->lsn = 0;
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat64 (u4IfIndex, NP_STAT_IF_HC_IN_DISCARDS,
                      pu8RetValIfHCInDiscards);
#else
    pu8RetValIfHCInDiscards->msn = 0;
    pu8RetValIfHCInDiscards->lsn = 0;
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIfHCInErrors
 Input       :  The Indices
                IfIndex

                The Object 
                retValIfHCInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfHCInErrors (INT4 i4IfIndex,
                    tSNMP_COUNTER64_TYPE * pu8RetValIfHCInErrors)
{
    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        pu8RetValIfHCInErrors->msn = 0;
        pu8RetValIfHCInErrors->lsn = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        pu8RetValIfHCInErrors->msn = 0;
        pu8RetValIfHCInErrors->lsn = 0;
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat64 (u4IfIndex, NP_STAT_IF_HC_IN_ERRORS,
                      pu8RetValIfHCInErrors);
#else
    pu8RetValIfHCInErrors->msn = 0;
    pu8RetValIfHCInErrors->lsn = 0;
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIfHCInUnknownProtos
 Input       :  The Indices
                IfIndex

                The Object 
                retValIfHCInUnknownProtos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfHCInUnknownProtos (INT4 i4IfIndex,
                           tSNMP_COUNTER64_TYPE * pu8RetValIfHCInUnknownProtos)
{
    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        pu8RetValIfHCInUnknownProtos->msn = 0;
        pu8RetValIfHCInUnknownProtos->lsn = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        pu8RetValIfHCInUnknownProtos->msn = 0;
        pu8RetValIfHCInUnknownProtos->lsn = 0;
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat64 (u4IfIndex, NP_STAT_IF_HC_IN_UNKNOWN_PROTOS,
                      pu8RetValIfHCInUnknownProtos);
#else
    pu8RetValIfHCInUnknownProtos->msn = 0;
    pu8RetValIfHCInUnknownProtos->lsn = 0;
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIfHCOutDiscards
 Input       :  The Indices
                IfIndex

                The Object 
                retValIfHCOutDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfHCOutDiscards (INT4 i4IfIndex,
                       tSNMP_COUNTER64_TYPE * pu8RetValIfHCOutDiscards)
{
    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        pu8RetValIfHCOutDiscards->msn = 0;
        pu8RetValIfHCOutDiscards->lsn = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        pu8RetValIfHCOutDiscards->msn = 0;
        pu8RetValIfHCOutDiscards->lsn = 0;
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat64 (u4IfIndex, NP_STAT_IF_HC_OUT_DISCARDS,
                      pu8RetValIfHCOutDiscards);
#else
    pu8RetValIfHCOutDiscards->msn = 0;
    pu8RetValIfHCOutDiscards->lsn = 0;
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIfHCOutErrors
 Input       :  The Indices
                IfIndex

                The Object 
                retValIfHCOutErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfHCOutErrors (INT4 i4IfIndex,
                     tSNMP_COUNTER64_TYPE * pu8RetValIfHCOutErrors)
{
    UINT4               u4IfIndex;
#ifdef NPAPI_WANTED
    UINT1               u1IfType;
#endif
    u4IfIndex = (UINT4) i4IfIndex;

    if (CfaIsSispInterface (u4IfIndex) == CFA_TRUE)
    {
        pu8RetValIfHCOutErrors->msn = 0;
        pu8RetValIfHCOutErrors->lsn = 0;
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    CfaGetIfType (u4IfIndex, &u1IfType);
#ifndef L3_SWITCHING_WANTED
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG)))
    {
        pu8RetValIfHCOutErrors->msn = 0;
        pu8RetValIfHCOutErrors->lsn = 0;
        return SNMP_SUCCESS;
    }
#endif
    CfaFsHwGetStat64 (u4IfIndex, NP_STAT_IF_HC_IN_OUT_ERRORS,
                      pu8RetValIfHCOutErrors);
#else
    pu8RetValIfHCOutErrors->msn = 0;
    pu8RetValIfHCOutErrors->lsn = 0;
#endif
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : FsPacketAnalyserTable. */

/***************************************************************************
  Functin    :  nmhValidateIndexInstanceFsPacketAnalyserTable
 Input       :  The Indices
                FsPacketAnalyserIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPacketAnalyserTable (UINT4 u4FsPacketAnalyserIndex)
{
    if (u4FsPacketAnalyserIndex < CFA_MAX_PKT_GEN_ENTRIES)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPacketAnalyserTable
 Input       :  The Indices
                FsPacketAnalyserIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPacketAnalyserTable (UINT4 *pu4FsPacketAnalyserIndex)
{
    tCfaPktRxInfo      *pCfaPktRxInfo = NULL;
    UINT4               u4Index = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    for (u4Index = 0; u4Index < CFA_MAX_PKT_GEN_ENTRIES; u4Index++)
    {
        pCfaPktRxInfo = &gpCfaPktRxInfoList[u4Index];
        if (pCfaPktRxInfo->u4RowStatus != 0)
        {
            *pu4FsPacketAnalyserIndex = pCfaPktRxInfo->u4Index;
            i1RetVal = SNMP_SUCCESS;
            break;
        }
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPacketAnalyserTable
 Input       :  The Indices
                FsPacketAnalyserIndex
                nextFsPacketAnalyserIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPacketAnalyserTable (UINT4 u4FsPacketAnalyserIndex,
                                      UINT4 *pu4NextFsPacketAnalyserIndex)
{
    tCfaPktRxInfo      *pCfaPktRxInfo = NULL;
    UINT4               u4Index = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    for (u4Index = u4FsPacketAnalyserIndex + 1;
         u4Index < CFA_MAX_PKT_GEN_ENTRIES; u4Index++)
    {
        pCfaPktRxInfo = &gpCfaPktRxInfoList[u4Index];
        if (pCfaPktRxInfo->u4RowStatus != 0)
        {
            *pu4NextFsPacketAnalyserIndex = pCfaPktRxInfo->u4Index;
            i1RetVal = SNMP_SUCCESS;
            break;
        }
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPacketAnalyserWatchValue
 Input       :  The Indices
                FsPacketAnalyserIndex

                The Object 
                retValFsPacketAnalyserWatchValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPacketAnalyserWatchValue (UINT4 u4FsPacketAnalyserIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsPacketAnalyserWatchValue)
{
    tCfaPktRxInfo      *pCfaPktRxInfo = NULL;
    pCfaPktRxInfo = &gpCfaPktRxInfoList[u4FsPacketAnalyserIndex];
    if (pCfaPktRxInfo->u4RowStatus != ACTIVE)
    {
        return SNMP_FAILURE;
    }

    pRetValFsPacketAnalyserWatchValue->pu1_OctetList = pCfaPktRxInfo->au1Val;
    pRetValFsPacketAnalyserWatchValue->i4_Length = (INT4) pCfaPktRxInfo->u4Len;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPacketAnalyserWatchMask
 Input       :  The Indices
                FsPacketAnalyserIndex

                The Object 
                retValFsPacketAnalyserWatchMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPacketAnalyserWatchMask (UINT4 u4FsPacketAnalyserIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsPacketAnalyserWatchMask)
{
    tCfaPktRxInfo      *pCfaPktRxInfo = NULL;
    pCfaPktRxInfo = &gpCfaPktRxInfoList[u4FsPacketAnalyserIndex];
    if (pCfaPktRxInfo->u4RowStatus == 0)
    {
        return SNMP_FAILURE;
    }

    pRetValFsPacketAnalyserWatchMask->pu1_OctetList = pCfaPktRxInfo->au1Mask;
    pRetValFsPacketAnalyserWatchMask->i4_Length = (INT4) pCfaPktRxInfo->u4Len;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPacketAnalyserWatchPorts
 Input       :  The Indices
                FsPacketAnalyserIndex

                The Object 
                retValFsPacketAnalyserWatchPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPacketAnalyserWatchPorts (UINT4 u4FsPacketAnalyserIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsPacketAnalyserWatchPorts)
{
    tCfaPktRxInfo      *pCfaPktRxInfo = NULL;
    pCfaPktRxInfo = &gpCfaPktRxInfoList[u4FsPacketAnalyserIndex];
    if (pCfaPktRxInfo->u4RowStatus == 0)
    {
        return SNMP_FAILURE;
    }

    pRetValFsPacketAnalyserWatchPorts->pu1_OctetList =
        pCfaPktRxInfo->au1RcvPorts;
    pRetValFsPacketAnalyserWatchPorts->i4_Length = BRG_PORT_LIST_SIZE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPacketAnalyserMatchPorts
 Input       :  The Indices
                FsPacketAnalyserIndex

                The Object 
                retValFsPacketAnalyserMatchPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPacketAnalyserMatchPorts (UINT4 u4FsPacketAnalyserIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsPacketAnalyserMatchPorts)
{
    tCfaPktRxInfo      *pCfaPktRxInfo = NULL;
    pCfaPktRxInfo = &gpCfaPktRxInfoList[u4FsPacketAnalyserIndex];
    if (pCfaPktRxInfo->u4RowStatus == 0)
    {
        return SNMP_FAILURE;
    }

    pRetValFsPacketAnalyserMatchPorts->pu1_OctetList =
        pCfaPktRxInfo->au1MatchPorts;
    pRetValFsPacketAnalyserMatchPorts->i4_Length = BRG_PORT_LIST_SIZE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPacketAnalyserCounter
 Input       :  The Indices
                FsPacketAnalyserIndex

                The Object 
                retValFsPacketAnalyserCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPacketAnalyserCounter (UINT4 u4FsPacketAnalyserIndex,
                               UINT4 *pu4RetValFsPacketAnalyserCounter)
{
    tCfaPktRxInfo      *pCfaPktRxInfo = NULL;
    pCfaPktRxInfo = &gpCfaPktRxInfoList[u4FsPacketAnalyserIndex];
    if (pCfaPktRxInfo->u4RowStatus == 0)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsPacketAnalyserCounter = pCfaPktRxInfo->u4Cnt;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPacketAnalyserTime
 Input       :  The Indices
                FsPacketAnalyserIndex

                The Object 
                retValFsPacketAnalyserTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPacketAnalyserTime (UINT4 u4FsPacketAnalyserIndex,
                            UINT4 *pu4RetValFsPacketAnalyserTime)
{
    tCfaPktRxInfo      *pCfaPktRxInfo = NULL;
    pCfaPktRxInfo = &gpCfaPktRxInfoList[u4FsPacketAnalyserIndex];
    if (pCfaPktRxInfo->u4RowStatus == 0)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsPacketAnalyserTime = pCfaPktRxInfo->u4LastPktRcvTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPacketAnalyserCreateTime
 Input       :  The Indices
                FsPacketAnalyserIndex

                The Object 
                retValFsPacketAnalyserCreateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPacketAnalyserCreateTime (UINT4 u4FsPacketAnalyserIndex,
                                  UINT4 *pu4RetValFsPacketAnalyserCreateTime)
{
    tCfaPktRxInfo      *pCfaPktRxInfo = NULL;
    pCfaPktRxInfo = &gpCfaPktRxInfoList[u4FsPacketAnalyserIndex];
    if (pCfaPktRxInfo->u4RowStatus == 0)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsPacketAnalyserCreateTime = pCfaPktRxInfo->u4CreateTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPacketAnalyserStatus
 Input       :  The Indices
                FsPacketAnalyserIndex

                The Object 
                retValFsPacketAnalyserStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPacketAnalyserStatus (UINT4 u4FsPacketAnalyserIndex,
                              INT4 *pi4RetValFsPacketAnalyserStatus)
{
    tCfaPktRxInfo      *pCfaPktRxInfo = NULL;
    pCfaPktRxInfo = &gpCfaPktRxInfoList[u4FsPacketAnalyserIndex];
    if (pCfaPktRxInfo->u4RowStatus == 0)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPacketAnalyserStatus = (INT4) pCfaPktRxInfo->u4RowStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsPacketAnalyserWatchValue
 Input       :  The Indices
                FsPacketAnalyserIndex

                The Object 
                setValFsPacketAnalyserWatchValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPacketAnalyserWatchValue (UINT4 u4FsPacketAnalyserIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValFsPacketAnalyserWatchValue)
{
    tCfaPktRxInfo      *pCfaPktRxInfo = NULL;
    pCfaPktRxInfo = &gpCfaPktRxInfoList[u4FsPacketAnalyserIndex];
    MEMCPY (pCfaPktRxInfo->au1Val,
            pSetValFsPacketAnalyserWatchValue->pu1_OctetList,
            pSetValFsPacketAnalyserWatchValue->i4_Length);
    pCfaPktRxInfo->u4Len = (UINT4) pSetValFsPacketAnalyserWatchValue->i4_Length;
    pCfaPktRxInfo->u1IsValueSet = 1;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPacketAnalyserWatchMask
 Input       :  The Indices
                FsPacketAnalyserIndex

                The Object 
                setValFsPacketAnalyserWatchMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPacketAnalyserWatchMask (UINT4 u4FsPacketAnalyserIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsPacketAnalyserWatchMask)
{
    tCfaPktRxInfo      *pCfaPktRxInfo = NULL;
    pCfaPktRxInfo = &gpCfaPktRxInfoList[u4FsPacketAnalyserIndex];
    MEMSET (pCfaPktRxInfo->au1Mask, 0xff, 1600);
    MEMCPY (pCfaPktRxInfo->au1Mask,
            pSetValFsPacketAnalyserWatchMask->pu1_OctetList,
            pSetValFsPacketAnalyserWatchMask->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPacketAnalyserWatchPorts
 Input       :  The Indices
                FsPacketAnalyserIndex

                The Object 
                setValFsPacketAnalyserWatchPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPacketAnalyserWatchPorts (UINT4 u4FsPacketAnalyserIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValFsPacketAnalyserWatchPorts)
{
    tCfaPktRxInfo      *pCfaPktRxInfo = NULL;
    pCfaPktRxInfo = &gpCfaPktRxInfoList[u4FsPacketAnalyserIndex];
    MEMCPY (pCfaPktRxInfo->au1RcvPorts,
            pSetValFsPacketAnalyserWatchPorts->pu1_OctetList,
            pSetValFsPacketAnalyserWatchPorts->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPacketAnalyserStatus
 Input       :  The Indices
                FsPacketAnalyserIndex

                The Object 
                setValFsPacketAnalyserStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPacketAnalyserStatus (UINT4 u4FsPacketAnalyserIndex,
                              INT4 i4SetValFsPacketAnalyserStatus)
{
    tCfaPktRxInfo      *pCfaPktRxInfo = NULL;
    tOsixSysTime        SysTime;

    pCfaPktRxInfo = &gpCfaPktRxInfoList[u4FsPacketAnalyserIndex];
    switch (i4SetValFsPacketAnalyserStatus)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
            pCfaPktRxInfo->u4RowStatus = CREATE_AND_WAIT;
            MEMSET (pCfaPktRxInfo->au1Mask, 0xff, 1600);
            MEMSET (pCfaPktRxInfo->au1RcvPorts, 0xff, BRG_PORT_LIST_SIZE);
            OsixGetSysTime (&SysTime);
            pCfaPktRxInfo->u4CreateTime = (UINT4) SysTime;
            pCfaPktRxInfo->u4Index = u4FsPacketAnalyserIndex;
            break;
        case ACTIVE:
            pCfaPktRxInfo->u4RowStatus = ACTIVE;
            break;
        case NOT_IN_SERVICE:
        case NOT_READY:
            pCfaPktRxInfo->u4RowStatus = u4FsPacketAnalyserIndex;
            break;
        case DESTROY:
            MEMSET (pCfaPktRxInfo, 0, sizeof (tCfaPktRxInfo));
            break;
        default:
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPacketAnalyserWatchValue
 Input       :  The Indices
                FsPacketAnalyserIndex

                The Object 
                testValFsPacketAnalyserWatchValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPacketAnalyserWatchValue (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsPacketAnalyserIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFsPacketAnalyserWatchValue)
{
    tCfaPktRxInfo      *pCfaPktRxInfo = NULL;
    pCfaPktRxInfo = &gpCfaPktRxInfoList[u4FsPacketAnalyserIndex];

    if (pCfaPktRxInfo->u4RowStatus == 0)
    {
        return SNMP_FAILURE;
    }
    if (pTestValFsPacketAnalyserWatchValue->i4_Length > 1600)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPacketAnalyserWatchMask
 Input       :  The Indices
                FsPacketAnalyserIndex

                The Object 
                testValFsPacketAnalyserWatchMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPacketAnalyserWatchMask (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsPacketAnalyserIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsPacketAnalyserWatchMask)
{
    tCfaPktRxInfo      *pCfaPktRxInfo = NULL;
    pCfaPktRxInfo = &gpCfaPktRxInfoList[u4FsPacketAnalyserIndex];

    if (pCfaPktRxInfo->u4RowStatus == 0)
    {
        return SNMP_FAILURE;
    }
    if (pTestValFsPacketAnalyserWatchMask->i4_Length > 1600)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPacketAnalyserWatchPorts
 Input       :  The Indices
                FsPacketAnalyserIndex

                The Object 
                testValFsPacketAnalyserWatchPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPacketAnalyserWatchPorts (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsPacketAnalyserIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFsPacketAnalyserWatchPorts)
{
    tCfaPktRxInfo      *pCfaPktRxInfo = NULL;
    pCfaPktRxInfo = &gpCfaPktRxInfoList[u4FsPacketAnalyserIndex];

    if (pCfaPktRxInfo->u4RowStatus == 0)
    {
        return SNMP_FAILURE;
    }
    if (pTestValFsPacketAnalyserWatchPorts->i4_Length > BRG_PORT_LIST_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPacketAnalyserStatus
 Input       :  The Indices
                FsPacketAnalyserIndex

                The Object 
                testValFsPacketAnalyserStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPacketAnalyserStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsPacketAnalyserIndex,
                                 INT4 i4TestValFsPacketAnalyserStatus)
{
    tCfaPktRxInfo      *pCfaPktRxInfo = NULL;
    if (i4TestValFsPacketAnalyserStatus < ACTIVE ||
        i4TestValFsPacketAnalyserStatus > DESTROY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pCfaPktRxInfo = &gpCfaPktRxInfoList[u4FsPacketAnalyserIndex];
    if ((i4TestValFsPacketAnalyserStatus == CREATE_AND_WAIT)
        || (i4TestValFsPacketAnalyserStatus == CREATE_AND_GO))
    {
        if (pCfaPktRxInfo->u4RowStatus != 0)
        {
            return SNMP_FAILURE;
        }
    }
    else if (i4TestValFsPacketAnalyserStatus == ACTIVE)
    {
        if (pCfaPktRxInfo->u4RowStatus == 0)
        {
            return SNMP_FAILURE;
        }
        if (pCfaPktRxInfo->u1IsValueSet == 0)
        {
            return SNMP_FAILURE;
        }
    }
    else if ((i4TestValFsPacketAnalyserStatus == NOT_IN_SERVICE)
             || (i4TestValFsPacketAnalyserStatus == NOT_READY)
             || (i4TestValFsPacketAnalyserStatus == DESTROY))
    {
        if (pCfaPktRxInfo->u4RowStatus == 0)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPacketAnalyserTable
 Input       :  The Indices
                FsPacketAnalyserIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPacketAnalyserTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPacketTransmitterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPacketTransmitterTable
 Input       :  The Indices
                FsPacketTransmitterIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsPacketTransmitterTable
    (UINT4 u4FsPacketTransmitterIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;
    if (u4FsPacketTransmitterIndex < CFA_MAX_PKT_GEN_ENTRIES)
    {
        i1RetVal = SNMP_SUCCESS;
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPacketTransmitterTable
 Input       :  The Indices
                FsPacketTransmitterIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsPacketTransmitterTable
    (UINT4 *pu4FsPacketTransmitterIndex)
{
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;
    UINT4               u4Index = 0;
    UINT1               u1RetVal = SNMP_FAILURE;
    for (u4Index = 0; u4Index < CFA_MAX_PKT_GEN_ENTRIES; u4Index++)
    {
        pCfaPktTxInfo = &gpCfaPktTxInfoList[u4Index];
        if (pCfaPktTxInfo->u4RowStatus != 0)
        {
            *pu4FsPacketTransmitterIndex = pCfaPktTxInfo->u4Index;
            u1RetVal = SNMP_SUCCESS;
            break;
        }
    }
    return ((INT1) u1RetVal);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPacketTransmitterTable
 Input       :  The Indices
                FsPacketTransmitterIndex
                nextFsPacketTransmitterIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPacketTransmitterTable (UINT4 u4FsPacketTransmitterIndex,
                                         UINT4 *pu4NextFsPacketTransmitterIndex)
{
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;
    UINT4               u4Index = 0;
    UINT1               u1RetVal = SNMP_FAILURE;
    for (u4Index = u4FsPacketTransmitterIndex + 1;
         u4Index < CFA_MAX_PKT_GEN_ENTRIES; u4Index++)
    {
        pCfaPktTxInfo = &gpCfaPktTxInfoList[u4Index];
        if (pCfaPktTxInfo->u4RowStatus != 0)
        {
            *pu4NextFsPacketTransmitterIndex = pCfaPktTxInfo->u4Index;
            u1RetVal = SNMP_SUCCESS;
            break;
        }
    }
    return ((INT1) u1RetVal);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPacketTransmitterValue
 Input       :  The Indices
                FsPacketTransmitterIndex

                The Object 
                retValFsPacketTransmitterValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPacketTransmitterValue (UINT4 u4FsPacketTransmitterIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsPacketTransmitterValue)
{
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;
    pCfaPktTxInfo = &gpCfaPktTxInfoList[u4FsPacketTransmitterIndex];
    if (pCfaPktTxInfo->u4RowStatus != ACTIVE)
    {
        return SNMP_FAILURE;
    }

    pRetValFsPacketTransmitterValue->pu1_OctetList =
        pCfaPktTxInfo->au1PktGenVal;
    pRetValFsPacketTransmitterValue->i4_Length = (INT4) pCfaPktTxInfo->u4Length;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPacketTransmitterPort
 Input       :  The Indices
                FsPacketTransmitterIndex

                The Object 
                retValFsPacketTransmitterPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPacketTransmitterPort (UINT4 u4FsPacketTransmitterIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsPacketTransmitterPort)
{
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;
    pCfaPktTxInfo = &gpCfaPktTxInfoList[u4FsPacketTransmitterIndex];
    if (pCfaPktTxInfo->u4RowStatus == 0)
    {
        return SNMP_FAILURE;
    }

    pRetValFsPacketTransmitterPort->pu1_OctetList = pCfaPktTxInfo->au1Ports;
    pRetValFsPacketTransmitterPort->i4_Length = BRG_PORT_LIST_SIZE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPacketTransmitterInterval
 Input       :  The Indices
                FsPacketTransmitterIndex

                The Object 
                retValFsPacketTransmitterInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPacketTransmitterInterval (UINT4 u4FsPacketTransmitterIndex,
                                   UINT4 *pu4RetValFsPacketTransmitterInterval)
{
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;
    pCfaPktTxInfo = &gpCfaPktTxInfoList[u4FsPacketTransmitterIndex];
    if (pCfaPktTxInfo->u4RowStatus == 0)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsPacketTransmitterInterval = pCfaPktTxInfo->u4Interval;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPacketTransmitterCount
 Input       :  The Indices
                FsPacketTransmitterIndex

                The Object 
                retValFsPacketTransmitterCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPacketTransmitterCount (UINT4 u4FsPacketTransmitterIndex,
                                UINT4 *pu4RetValFsPacketTransmitterCount)
{
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;
    pCfaPktTxInfo = &gpCfaPktTxInfoList[u4FsPacketTransmitterIndex];
    if (pCfaPktTxInfo->u4RowStatus == 0)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsPacketTransmitterCount = pCfaPktTxInfo->u4PktCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPacketTransmitterStatus
 Input       :  The Indices
                FsPacketTransmitterIndex

                The Object 
                retValFsPacketTransmitterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPacketTransmitterStatus (UINT4 u4FsPacketTransmitterIndex,
                                 INT4 *pi4RetValFsPacketTransmitterStatus)
{
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;
    pCfaPktTxInfo = &gpCfaPktTxInfoList[u4FsPacketTransmitterIndex];
    *pi4RetValFsPacketTransmitterStatus = 0;
    if (pCfaPktTxInfo->u4RowStatus == 0)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsPacketTransmitterStatus = (INT4) pCfaPktTxInfo->u4RowStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPacketTransmitterValue
 Input       :  The Indices
                FsPacketTransmitterIndex

                The Object 
                setValFsPacketTransmitterValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPacketTransmitterValue (UINT4 u4FsPacketTransmitterIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsPacketTransmitterValue)
{
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;
    pCfaPktTxInfo = &gpCfaPktTxInfoList[u4FsPacketTransmitterIndex];
    MEMCPY (pCfaPktTxInfo->au1PktGenVal,
            pSetValFsPacketTransmitterValue->pu1_OctetList,
            pSetValFsPacketTransmitterValue->i4_Length);
    pCfaPktTxInfo->u4Length =
        (UINT4) pSetValFsPacketTransmitterValue->i4_Length;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPacketTransmitterPort
 Input       :  The Indices
                FsPacketTransmitterIndex

                The Object 
                setValFsPacketTransmitterPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPacketTransmitterPort (UINT4 u4FsPacketTransmitterIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsPacketTransmitterPort)
{
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;
    pCfaPktTxInfo = &gpCfaPktTxInfoList[u4FsPacketTransmitterIndex];
    MEMCPY (pCfaPktTxInfo->au1Ports,
            pSetValFsPacketTransmitterPort->pu1_OctetList,
            pSetValFsPacketTransmitterPort->i4_Length);
    pCfaPktTxInfo->u1IsPortSet = 1;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPacketTransmitterInterval
 Input       :  The Indices
                FsPacketTransmitterIndex

                The Object 
                setValFsPacketTransmitterInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPacketTransmitterInterval (UINT4 u4FsPacketTransmitterIndex,
                                   UINT4 u4SetValFsPacketTransmitterInterval)
{
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;
    pCfaPktTxInfo = &gpCfaPktTxInfoList[u4FsPacketTransmitterIndex];
    pCfaPktTxInfo->u4Interval = u4SetValFsPacketTransmitterInterval;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPacketTransmitterCount
 Input       :  The Indices
                FsPacketTransmitterIndex

                The Object 
                setValFsPacketTransmitterCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPacketTransmitterCount (UINT4 u4FsPacketTransmitterIndex,
                                UINT4 u4SetValFsPacketTransmitterCount)
{
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;
    pCfaPktTxInfo = &gpCfaPktTxInfoList[u4FsPacketTransmitterIndex];
    pCfaPktTxInfo->u4PktCount = u4SetValFsPacketTransmitterCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPacketTransmitterStatus
 Input       :  The Indices
                FsPacketTransmitterIndex

                The Object 
                setValFsPacketTransmitterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPacketTransmitterStatus (UINT4 u4FsPacketTransmitterIndex,
                                 INT4 i4SetValFsPacketTransmitterStatus)
{
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;
    tPktgenQueInfo     *pPktgenQueInfo = NULL;

    pCfaPktTxInfo = &gpCfaPktTxInfoList[u4FsPacketTransmitterIndex];
    switch (i4SetValFsPacketTransmitterStatus)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
            pCfaPktTxInfo->u4RowStatus =
                (UINT4) i4SetValFsPacketTransmitterStatus;
            pCfaPktTxInfo->u4PktCount = 1;
            pCfaPktTxInfo->u4Interval = 1;
            pCfaPktTxInfo->u4Index = u4FsPacketTransmitterIndex;
            break;
        case ACTIVE:
            pCfaPktTxInfo->u4RowStatus = ACTIVE;

            pPktgenQueInfo = MemAllocMemBlk (CFA_PKT_GEN_QUE_POOL_ID);

            if (pPktgenQueInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            pPktgenQueInfo->u4Index = pCfaPktTxInfo->u4Index;
            pPktgenQueInfo->u4Action = CFA_PKTGEN_START;
            if (OsixQueSend (CFA_PKT_GEN_TX_QID, (UINT1 *) &pPktgenQueInfo,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (CFA_PKT_GEN_QUE_POOL_ID,
                                    (UINT1 *) pPktgenQueInfo);
                return SNMP_FAILURE;
            }
            if (OsixSendEvent (SELF, (const UINT1 *) CFA_TASK_NAME,
                               CFA_PKT_GEN_TX_EVENT) != OSIX_SUCCESS)
            {

                MemReleaseMemBlock (CFA_PKT_GEN_QUE_POOL_ID,
                                    (UINT1 *) pPktgenQueInfo);
                return SNMP_FAILURE;
            }
            break;
        case NOT_IN_SERVICE:
        case NOT_READY:
            pCfaPktTxInfo->u4RowStatus =
                (UINT4) i4SetValFsPacketTransmitterStatus;
            break;
        case DESTROY:
            pPktgenQueInfo = MemAllocMemBlk (CFA_PKT_GEN_QUE_POOL_ID);
            if (pPktgenQueInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            MEMSET (pPktgenQueInfo, 0, sizeof (tPktgenQueInfo));
            pPktgenQueInfo->u4Index = pCfaPktTxInfo->u4Index;
            pPktgenQueInfo->u4Action = CFA_PKTGEN_STOP;
            if (OsixQueSend (CFA_PKT_GEN_TX_QID, (UINT1 *) &pPktgenQueInfo,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (CFA_PKT_GEN_QUE_POOL_ID,
                                    (UINT1 *) pPktgenQueInfo);
                return SNMP_FAILURE;
            }
            if (OsixSendEvent (SELF, (const UINT1 *) CFA_TASK_NAME,
                               CFA_PKT_GEN_TX_EVENT) != OSIX_SUCCESS)
            {

                MemReleaseMemBlock (CFA_PKT_GEN_QUE_POOL_ID,
                                    (UINT1 *) pPktgenQueInfo);
                return SNMP_FAILURE;
            }
            break;
        default:
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPacketTransmitterValue
 Input       :  The Indices
                FsPacketTransmitterIndex

                The Object 
                testValFsPacketTransmitterValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPacketTransmitterValue (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsPacketTransmitterIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsPacketTransmitterValue)
{
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;
    pCfaPktTxInfo = &gpCfaPktTxInfoList[u4FsPacketTransmitterIndex];

    if (pCfaPktTxInfo->u4RowStatus == 0)
    {
        return SNMP_FAILURE;
    }
    if (pTestValFsPacketTransmitterValue->i4_Length > 1600)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPacketTransmitterPort
 Input       :  The Indices
                FsPacketTransmitterIndex

                The Object 
                testValFsPacketTransmitterPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPacketTransmitterPort (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsPacketTransmitterIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsPacketTransmitterPort)
{
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;
    pCfaPktTxInfo = &gpCfaPktTxInfoList[u4FsPacketTransmitterIndex];

    if (pCfaPktTxInfo->u4RowStatus == 0)
    {
        return SNMP_FAILURE;
    }
    if (pTestValFsPacketTransmitterPort->i4_Length > BRG_PORT_LIST_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPacketTransmitterInterval
 Input       :  The Indices
                FsPacketTransmitterIndex

                The Object 
                testValFsPacketTransmitterInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPacketTransmitterInterval (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsPacketTransmitterIndex,
                                      UINT4
                                      u4TestValFsPacketTransmitterInterval)
{
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;
    pCfaPktTxInfo = &gpCfaPktTxInfoList[u4FsPacketTransmitterIndex];

    if (pCfaPktTxInfo->u4RowStatus == 0)
    {
        return SNMP_FAILURE;
    }
    if (u4TestValFsPacketTransmitterInterval == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPacketTransmitterCount
 Input       :  The Indices
                FsPacketTransmitterIndex

                The Object 
                testValFsPacketTransmitterCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPacketTransmitterCount (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsPacketTransmitterIndex,
                                   UINT4 u4TestValFsPacketTransmitterCount)
{
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;
    pCfaPktTxInfo = &gpCfaPktTxInfoList[u4FsPacketTransmitterIndex];

    if (pCfaPktTxInfo->u4RowStatus == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u4TestValFsPacketTransmitterCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPacketTransmitterStatus
 Input       :  The Indices
                FsPacketTransmitterIndex

                The Object 
                testValFsPacketTransmitterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPacketTransmitterStatus (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsPacketTransmitterIndex,
                                    INT4 i4TestValFsPacketTransmitterStatus)
{
    tCfaPktTxInfo      *pCfaPktTxInfo = NULL;

    if (i4TestValFsPacketTransmitterStatus < ACTIVE ||
        i4TestValFsPacketTransmitterStatus > DESTROY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pCfaPktTxInfo = &gpCfaPktTxInfoList[u4FsPacketTransmitterIndex];
    if ((i4TestValFsPacketTransmitterStatus == CREATE_AND_WAIT)
        || (i4TestValFsPacketTransmitterStatus == CREATE_AND_GO))
    {
        if (pCfaPktTxInfo->u4RowStatus != 0)
        {
            return SNMP_FAILURE;
        }
    }
    else if (i4TestValFsPacketTransmitterStatus == ACTIVE)
    {
        if (pCfaPktTxInfo->u4RowStatus == 0)
        {
            return SNMP_FAILURE;
        }
        if (pCfaPktTxInfo->u4Length == 0)
        {
            return SNMP_FAILURE;
        }
        if (pCfaPktTxInfo->u1IsPortSet == 0)
        {
            return SNMP_FAILURE;
        }
    }
    else if ((i4TestValFsPacketTransmitterStatus == NOT_IN_SERVICE)
             || (i4TestValFsPacketTransmitterStatus == NOT_READY)
             || (i4TestValFsPacketTransmitterStatus == DESTROY))
    {
        if (pCfaPktTxInfo->u4RowStatus == 0)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPacketTransmitterTable
 Input       :  The Indices
                FsPacketTransmitterIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPacketTransmitterTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IfACTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIfACTable
 Input       :  The Indices
                IfMainIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIfACTable (INT4 i4IfMainIndex)
{
    /* This should be modified for AC interface specific */
    return (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIfACTable
 Input       :  The Indices
                IfMainIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIfACTable (INT4 *pi4IfMainIndex)
{
    /* This should be modified for AC interface specific */
    UINT4               u4Index = CFA_MIN_AC_IF_INDEX;

    /* Get the First Used Index */
    CFA_CDB_SCAN_WITH_LOCK (u4Index, BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                            CFA_PROP_VIRTUAL_INTERFACE)
    {
        if (CfaIsIfEntryProgrammingAllowed (u4Index) == CFA_FALSE)
        {
            CFA_DS_LOCK ();
            if (CFA_CDB_IS_INTF_VALID (u4Index) == CFA_TRUE)
            {
                CFA_DS_UNLOCK ();
                *pi4IfMainIndex = (INT4) u4Index;
                return SNMP_SUCCESS;
            }
            CFA_DS_UNLOCK ();
        }
        else
        {
            if (CFA_IF_ENTRY_USED (u4Index))
            {
                *pi4IfMainIndex = (INT4) u4Index;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIfACTable
 Input       :  The Indices
                IfMainIndex
                nextIfMainIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIfACTable (INT4 i4IfMainIndex, INT4 *pi4NextIfMainIndex)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4Counter = 0;

    if ((i4IfMainIndex < CFA_MIN_AC_IF_INDEX) ||
        ((UINT4) i4IfMainIndex > BRG_MAX_PHY_PLUS_LAG_INT_PORTS))
    {
        return SNMP_FAILURE;
    }

    u4IfIndex = (UINT4) i4IfMainIndex;
    u4Counter = u4IfIndex + 1;

    /* Next Index should always return the next used Index */
    CFA_CDB_SCAN_WITH_LOCK (u4Counter, BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                            CFA_PROP_VIRTUAL_INTERFACE)
    {

        if (CfaIsIfEntryProgrammingAllowed (u4Counter) == CFA_FALSE)
        {
            CFA_DS_LOCK ();
            if (CFA_CDB_IS_INTF_VALID (u4Counter) == CFA_TRUE)
            {
                CFA_DS_UNLOCK ();
                *pi4NextIfMainIndex = (INT4) u4Counter;
                return SNMP_SUCCESS;
            }
            CFA_DS_UNLOCK ();
        }
        else
        {
            if (CFA_IF_ENTRY_USED (u4Counter))
            {
                *pi4NextIfMainIndex = (INT4) u4Counter;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIfACPortIdentifier
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfACPortIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfACPortIdentifier (INT4 i4IfMainIndex, INT4 *pi4RetValIfACPortIdentifier)
{
    UINT4               u4ACPort = 0;

    if (CfaGetIfACPortIdentifier ((UINT4) i4IfMainIndex, &u4ACPort)
        == CFA_SUCCESS)
    {
        *pi4RetValIfACPortIdentifier = (INT4) u4ACPort;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIfACCustomerVlan
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfACCustomerVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfACCustomerVlan (INT4 i4IfMainIndex, INT4 *pi4RetValIfACCustomerVlan)
{
    UINT2               u2ACVlan = 0;

    if (CfaGetIfACVlan ((UINT4) i4IfMainIndex, &u2ACVlan) == CFA_SUCCESS)
    {
        *pi4RetValIfACCustomerVlan = (INT4) u2ACVlan;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIfACPortIdentifier
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfACPortIdentifier
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfACPortIdentifier (INT4 i4IfMainIndex, INT4 i4SetValIfACPortIdentifier)
{
    UINT1               au1HwAddr[CFA_ENET_ADDR_LEN];
    UINT1               u1OperStatus = CFA_IF_DOWN;

    /* Attachment circuit interface's MAC address is same as that of 
     * underlying physical interface's MAC address. Hence taking the 
     * physical interface's MAC address and populating it for 
     * Attachment circuit interface's MAC field.
     */
    MEMSET (au1HwAddr, 0, CFA_ENET_ADDR_LEN);

    if (CfaGetIfHwAddr ((UINT4) i4SetValIfACPortIdentifier, au1HwAddr)
        == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (CfaGetIfOperStatus ((UINT4) i4SetValIfACPortIdentifier, &u1OperStatus)
        == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }

    CfaSetIfHwAddr ((UINT4) i4IfMainIndex, au1HwAddr, CFA_ENET_ADDR_LEN);

    /* When mapping physical port on an AC interface, the oper status of 
     * AC interface will be set. This oper status is taken from the oper 
     * status of the physical port.
     */
    if (CfaSetIfACPortIdentifier ((UINT4) i4IfMainIndex,
                                  (UINT4) i4SetValIfACPortIdentifier) ==
        CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (CfaInterfaceStatusChangeIndication ((UINT4) i4IfMainIndex, u1OperStatus)
        == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfACCustomerVlan
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfACCustomerVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfACCustomerVlan (INT4 i4IfMainIndex, INT4 i4SetValIfACCustomerVlan)
{
    if (CfaSetIfACVlan ((UINT4) i4IfMainIndex,
                        (UINT2) i4SetValIfACCustomerVlan) == CFA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IfACPortIdentifier
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfACPortIdentifier
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfACPortIdentifier (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                             INT4 i4TestValIfACPortIdentifier)
{
    UINT1               u1IfType = 0;
    /* for index validation - because validation is commented in mid-level */
    if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    CfaGetIfType ((UINT4) i4IfMainIndex, &u1IfType);

    /* the IfType must have been specified first */
    if ((u1IfType == CFA_INVALID_TYPE))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* the interface should be admin down */
    if ((CFA_IF_ADMIN ((UINT4) i4IfMainIndex) == CFA_IF_UP))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_ADMIN_STATUS_ERR);
        return SNMP_FAILURE;
    }
    /* physical interface presence validation */

    if (u1IfType != CFA_PROP_VIRTUAL_INTERFACE)
    {
        /* Can be set only for attachment circuit interfaces */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceIfMainTable (i4TestValIfACPortIdentifier)
        != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    CfaGetIfType ((UINT4) i4TestValIfACPortIdentifier, &u1IfType);

    if (u1IfType != CFA_ENET)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfACCustomerVlan
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfACCustomerVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfACCustomerVlan (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                           INT4 i4TestValIfACCustomerVlan)
{
    UINT1               u1IfType = 0;

    /* for index validation - because validation is commented in mid-level */
    if (nmhValidateIndexInstanceIfMainTable (i4IfMainIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    CfaGetIfType ((UINT4) i4IfMainIndex, &u1IfType);

    /* the IfType must have been specified first */
    if (u1IfType == CFA_INVALID_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (u1IfType != CFA_PROP_VIRTUAL_INTERFACE)
    {
        /* Can be set only for attachment circuit interfaces */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* the interface should be admin down */
    if ((CFA_IF_ADMIN ((UINT4) i4IfMainIndex) == CFA_IF_UP))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_ADMIN_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if (i4TestValIfACCustomerVlan != 0)
    {
        /* Customer vlan identifier. */
        if ((i4TestValIfACCustomerVlan < CFA_MIN_CUSTOMER_VLAN_ID) ||
            (i4TestValIfACCustomerVlan > CFA_MAX_CUSTOMER_VLAN_ID))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IfACTable
 Input       :  The Indices
                IfMainIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfACTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainExtSubType
 Input       :  The Indices
                IfMainIndex

                The Object
                retValIfMainExtSubType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainExtSubType (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainExtSubType)
{
    UINT1               u1IfSubType = 0;

    if (CfaGetIfSubType ((UINT4) i4IfMainIndex, &u1IfSubType) == CFA_SUCCESS)
    {
        *pi4RetValIfMainExtSubType = (INT4) u1IfSubType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfMainExtSubType
 Input       :  The Indices
                IfMainIndex

                The Object
                setValIfMainExtSubType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainExtSubType (INT4 i4IfMainIndex, INT4 i4SetValIfMainExtSubType)
{
    UINT1               u1IfMainType = 0;
    UINT1               u1RetValIfMainExtSubType = CFA_SUBTYPE_SISP_INTERFACE;

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    CfaGetIfType ((UINT4) i4IfMainIndex, &u1IfMainType);

    CfaGetIfSubType ((UINT4) i4IfMainIndex, &u1RetValIfMainExtSubType);
    if ((u1IfMainType == CFA_PROP_VIRTUAL_INTERFACE) ||
        (i4SetValIfMainExtSubType == CFA_SUBTYPE_OPENFLOW_INTERFACE) ||
        (u1RetValIfMainExtSubType == CFA_SUBTYPE_OPENFLOW_INTERFACE))
    {
        if ((CfaIfmUpdateExtSubType
             ((UINT4) i4IfMainIndex,
              (UINT2) i4SetValIfMainExtSubType, TRUE)) != CFA_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (CfaSetIfSubType
            ((UINT4) i4IfMainIndex,
             (UINT1) i4SetValIfMainExtSubType) == CFA_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IfMainExtSubType
 Input       :  The Indices
                IfMainIndex

                The Object
                testValIfMainExtSubType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainExtSubType (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                           INT4 i4TestValIfMainExtSubType)
{
    UINT1               u1IfType = 0;
#ifdef OPENFLOW_WANTED
    UINT4               u4Context = 0;
#endif
    UINT1               u1RetValIfMainExtSubType = CFA_SUBTYPE_SISP_INTERFACE;

    if (CfaValidateIfMainTableIndex (i4IfMainIndex) != CFA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    CfaGetIfType ((UINT4) i4IfMainIndex, &u1IfType);

    CfaGetIfSubType ((UINT4) i4IfMainIndex, &u1RetValIfMainExtSubType);

    if ((i4TestValIfMainExtSubType != CFA_SUBTYPE_OPENFLOW_INTERFACE) &&
        (u1RetValIfMainExtSubType == CFA_SUBTYPE_OPENFLOW_INTERFACE))
    {
#ifdef OPENFLOW_WANTED
        if ((OfcGetContextIdFromIfIndex ((UINT4) i4IfMainIndex, &u4Context) ==
             OFC_SUCCESS) && (u4Context != OFC_INVALID_CONTEXT))
        {
            CLI_SET_ERR (CLI_CFA_OPENFLOW_INTERFACE_MAP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
#endif
    }

    if ((i4TestValIfMainExtSubType == CFA_SUBTYPE_OPENFLOW_INTERFACE) ||
        (u1RetValIfMainExtSubType == CFA_SUBTYPE_OPENFLOW_INTERFACE))
    {
        return SNMP_SUCCESS;
    }

    if (u1IfType != CFA_PROP_VIRTUAL_INTERFACE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValIfMainExtSubType != CFA_SUBTYPE_AC_INTERFACE) &&
        (i4TestValIfMainExtSubType != CFA_SUBTYPE_SISP_INTERFACE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfIpIntfStatsEnable
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfIpIntfStatsEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfIpIntfStatsEnable (INT4 i4IfMainIndex,
                           INT4 *pi4RetValIfIpIntfStatsEnable)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry ((UINT4) i4IfMainIndex);
    if (pCfaIfInfo != NULL)
    {
        *pi4RetValIfIpIntfStatsEnable = (INT4) pCfaIfInfo->u4IpIntfStats;
    }
    CFA_DS_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfIpIntfStatsEnable
 Input       :  The Indices
                IfMainIndex

                The Object 
                setValIfIpIntfStatsEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfIpIntfStatsEnable (INT4 i4IfMainIndex, INT4 i4SetValIfIpIntfStatsEnable)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
#ifdef NPAPI_WANTED
    UINT4               u4Value;
    UINT1               u1RetVal;
#endif

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry ((UINT4) i4IfMainIndex);

    if (pCfaIfInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCfaIfInfo->u4IpIntfStats == (UINT4) i4SetValIfIpIntfStatsEnable)
    {
        return SNMP_SUCCESS;
    }
#ifdef NPAPI_WANTED
    if (i4SetValIfIpIntfStatsEnable == CFA_ENABLED)
    {
        u1RetVal = CfaFsHwGetVlanIntfStats (i4IfMainIndex,
                                            CFA_L3_VLAN_COUNTER_ENABLE,
                                            &u4Value);

        if (u1RetVal == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u1RetVal = CfaFsHwGetVlanIntfStats (i4IfMainIndex,
                                            CFA_L3_VLAN_COUNTER_DISABLE,
                                            &u4Value);

        if (u1RetVal == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }

    }
#endif

    pCfaIfInfo->u4IpIntfStats = (UINT4) i4SetValIfIpIntfStatsEnable;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfIpIntfStatsEnable
 Input       :  The Indices
                IfMainIndex

                The Object 
                testValIfIpIntfStatsEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfIpIntfStatsEnable (UINT4 *pu4ErrorCode,
                              INT4 i4IfMainIndex,
                              INT4 i4TestValIfIpIntfStatsEnable)
{
    if ((i4IfMainIndex < 0) ||
        ((UINT4) i4IfMainIndex > BRG_MAX_PHY_PLUS_LAG_INT_PORTS))
    {
        return SNMP_FAILURE;
    }
    if ((i4TestValIfIpIntfStatsEnable != CFA_DISABLED) &&
        (i4TestValIfIpIntfStatsEnable != CFA_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfIpPortVlanId
 Input       :  The Indices
                IfMainIndex
 
                The Object
                retValIfIpPortVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfIpPortVlanId (INT4 i4IfMainIndex, INT4 *pi4RetValIfIpPortVlanId)
{
    if (CfaGetIfIpPortVlanId ((UINT4) i4IfMainIndex, pi4RetValIfIpPortVlanId) ==
        CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IfIpPortVlanId
 Input       :  The Indices
                IfMainIndex
 
                The Object
                testValIfIpPortVlanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfIpPortVlanId (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                         INT4 i4TestValIfIpPortVlanId)
{
    INT4                i4PortVlanId = 0;
    UINT4               u4ContextId = 0;
    UINT1               u1BridgedIfaceStatus = CFA_DISABLED;

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        /* Handles Invalid Index */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_INVALID_INDEX);
        return SNMP_FAILURE;
    }

    /* Check Whether the port is oper-up or not */

    if ((CFA_IF_ADMIN ((UINT4) i4IfMainIndex) == CFA_IF_UP))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_ADMIN_STATUS_PORT_VID_ERR);
        return SNMP_FAILURE;
    }

    /* Check whether the port is router-port */
    CfaGetIfBridgedIfaceStatus ((UINT4) i4IfMainIndex, &u1BridgedIfaceStatus);
    if (u1BridgedIfaceStatus == CFA_ENABLED)
    {
        /* If the Interface is a bridged InterfacE (Non Router Port) */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Check Whether the PortVlanId is allowed Vlan range */
    if ((i4TestValIfIpPortVlanId < 0) ||
        (i4TestValIfIpPortVlanId > VLAN_MAX_VLAN_ID))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_RESV_VLAN_ERR);
        return SNMP_FAILURE;
    }

    /* Check Whether the PortVlanId specified is already being used by Vlan */

    for (u4ContextId = 0; u4ContextId < SYS_DEF_MAX_NUM_CONTEXTS; u4ContextId++)
    {
        /* Go through all context and find if the PortVlanId specified
         *  has already been used by l2 vlan */

        if (OSIX_TRUE ==
            L2IwfMiIsVlanActive (u4ContextId,
                                 (tVlanId) i4TestValIfIpPortVlanId))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_CFA_PORT_VLAN_IN_USE_VLAN);
            return SNMP_FAILURE;
        }

    }

    /* Error is not thrown when the same Port VLAN ID is configured again
     *  for this interface */

    CfaGetIfIpPortVlanId ((UINT4) i4IfMainIndex, &i4PortVlanId);
    if (i4TestValIfIpPortVlanId == i4PortVlanId)
    {
        return SNMP_SUCCESS;
    }

    if (i4TestValIfIpPortVlanId != 0)
    {
        if (CfaIsPortVlanExists ((UINT2) i4TestValIfIpPortVlanId) ==
            OSIX_SUCCESS)
        {

            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_CFA_PORT_VLAN_USED_ERR);
            return SNMP_FAILURE;

        }

        if (CfaIsInterfaceVlanExists ((UINT2) i4TestValIfIpPortVlanId) ==
            CFA_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_CFA_PORT_VLAN_IN_USE_VLAN);
            return SNMP_FAILURE;

        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIfIpPortVlanId
 Input       :  The Indices
                IfMainIndex
 
                The Object
                setValIfIpPortVlanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfIpPortVlanId (INT4 i4IfMainIndex, INT4 i4SetValIfIpPortVlanId)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1MacAddress[MAC_ADDR_LEN];

    tIpConfigInfo       IpConfigInfo;
#ifdef NPAPI_WANTED
    tFsNpL3IfInfo       FsNpL3IfInfo;
#ifdef LNXIP4_WANTED
    UINT2               KnetIfVlanId = 0;
#endif
#endif
    UINT4               u4ContextId = 0;
    INT4                i4PortVlanId = 0;

#ifdef NPAPI_WANTED
    MEMSET (&FsNpL3IfInfo, 0, sizeof (tFsNpL3IfInfo));
#endif
    MEMSET (&IpConfigInfo, 0, sizeof (tIpConfigInfo));
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1MacAddress, 0, MAC_ADDR_LEN);

    /* Returns Success when the same Port VLAN ID is re-configured 
     * on this interface */
    CfaGetIfIpPortVlanId ((UINT4) i4IfMainIndex, &i4PortVlanId);
    if (i4SetValIfIpPortVlanId == i4PortVlanId)
    {
        return SNMP_SUCCESS;
    }

    if (CFA_IF_ENTRY_USED ((UINT4) i4IfMainIndex))
    {
        if (VcmGetContextIdFromCfaIfIndex ((UINT4) i4IfMainIndex, &u4ContextId)
            == VCM_FAILURE)
        {
            CLI_SET_ERR (CLI_CFA_INVALID_INDEX);
            return SNMP_FAILURE;
        }

        if (CfaIpIfGetIfInfo ((UINT4) i4IfMainIndex, &IpConfigInfo) !=
            CFA_SUCCESS)
        {
            CLI_SET_ERR (CLI_CFA_INVALID_INDEX);
            return SNMP_FAILURE;
        }

        CfaGetIfName ((UINT4) i4IfMainIndex, au1IfName);

        CfaGetIfHwAddr ((UINT4) i4IfMainIndex, au1MacAddress);

        IpConfigInfo.u2PortVlanId = (UINT2) i4SetValIfIpPortVlanId;
#ifdef NPAPI_WANTED
        FsNpL3IfInfo.u4VrId = u4ContextId;
        FsNpL3IfInfo.u4IfIndex = i4IfMainIndex;
        FsNpL3IfInfo.u4IpAddr = IpConfigInfo.u4Addr;
        FsNpL3IfInfo.u4IpSubnet = IpConfigInfo.u4NetMask;
        FsNpL3IfInfo.u2PortVlanId = i4SetValIfIpPortVlanId;
        MEMCPY (FsNpL3IfInfo.au1IfName, au1IfName, CFA_MAX_PORT_NAME_LENGTH);
        MEMCPY (FsNpL3IfInfo.au1MacAddr, au1MacAddress, MAC_ADDR_LEN);

        if (FNP_FAILURE == CfaFsNpIpv4L3IpInterface (L3_HW_MODIFY_IF_INFO,
                                                     &FsNpL3IfInfo))
        {
            return SNMP_FAILURE;
        }
#ifdef LNXIP4_WANTED
        if (ISS_HW_SUPPORTED ==
            IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT))
        {
            /* Knet interface name is assigned dynamically based on
             * the created dummy Vlan Id and the name is set
             * to the correspondig interface index */
            KnetIfVlanId = FsNpL3IfInfo.u2PortVlanId;
            SNPRINTF ((CHR1 *) FsNpL3IfInfo.au1IfName,
                      CFA_MAX_PORT_NAME_LENGTH, "vlan%d", KnetIfVlanId);
            CfaGddSetLnxIntfnameForPort ((UINT4) i4IfMainIndex,
                                         FsNpL3IfInfo.au1IfName);
        }
#endif /* LNXIP4_WANTED */
        IpConfigInfo.u2PortVlanId = (UINT2) FsNpL3IfInfo.u2PortVlanId;
#endif /* NPAPI_WANTED */

        if (CFA_FAILURE ==
            CfaIpIfSetIfInfo ((UINT4) i4IfMainIndex, CFA_IP_IF_PORTVID,
                              &IpConfigInfo))
        {
            CLI_SET_ERR (CLI_CFA_INVALID_INDEX);
            return SNMP_FAILURE;
        }
#ifdef LNXIP4_WANTED
        /* In case of Kernel Network support, when the Port VLAN id of 
         * Router port is changed, the interface will be deleted and 
         * recreated with new interface name corresponding to the new 
         * dummy VLAN id. So IP address and other parameters for that
         * interface will be lost.Hence reconfigured again here.
         */
        if (ISS_HW_SUPPORTED ==
            IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT))
        {
            if (CfaIvrDeleteIpAddressForPortVlanId
                ((UINT4) i4IfMainIndex, IpConfigInfo.u4Addr) == CLI_FAILURE)
            {
                return CLI_FAILURE;
            }
            /* Setting IP address after changing dummy vlan id */
            CfaIvrSetIpAddressForPortVlanId ((UINT4) i4IfMainIndex,
                                             IpConfigInfo.u4Addr,
                                             IpConfigInfo.u4NetMask);
        }
#endif
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainUfdOperStatus
 Input       :  The Indices
                IfMainIndex

                The Object
                retValIfMainUfdOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainUfdOperStatus (INT4 i4IfMainIndex,
                           INT4 *pi4RetValIfMainUfdOperStatus)
{
    UINT1               u1UfdOperStatus = 0;

    *pi4RetValIfMainUfdOperStatus = 0;
    if (CfaGetIfUfdOperStatus ((UINT4) i4IfMainIndex, &u1UfdOperStatus)
        == CFA_SUCCESS)
    {
        *pi4RetValIfMainUfdOperStatus = (INT4) u1UfdOperStatus;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainPortRole
 Input       :  The Indices
                IfMainIndex

                The Object
                retValIfMainPortRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainPortRole (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainPortRole)
{
    UINT1               u1PortRole = 0;

    *pi4RetValIfMainPortRole = 0;
    if (CfaGetIfPortRole ((UINT4) i4IfMainIndex, &u1PortRole) == CFA_SUCCESS)
    {
        *pi4RetValIfMainPortRole = (INT4) u1PortRole;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainUfdGroupId
 Input       :  The Indices
                IfMainIndex

                The Object
                retValIfMainUfdGroupId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainUfdGroupId (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainUfdGroupId)
{
    UINT4               u4UfdGroupId = 0;

    *pi4RetValIfMainUfdGroupId = 0;
    if (CfaGetIfUfdGroupId ((UINT4) i4IfMainIndex, &u4UfdGroupId) ==
        CFA_SUCCESS)
    {
        *pi4RetValIfMainUfdGroupId = (INT4) u4UfdGroupId;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainUfdDownlinkDisabledCount
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfMainUfdDownlinkDisabledCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainUfdDownlinkDisabledCount (INT4 i4IfMainIndex,
                                      UINT4
                                      *pu4RetValIfMainUfdDownlinkDisabledCount)
{
    UINT4               u4UfdDownlinkDisabledCount = 0;

    *pu4RetValIfMainUfdDownlinkDisabledCount = 0;
    if (CfaGetIfUfdDownlinkDisabledCount ((UINT4) i4IfMainIndex,
                                          &u4UfdDownlinkDisabledCount) ==
        CFA_SUCCESS)
    {
        *pu4RetValIfMainUfdDownlinkDisabledCount = u4UfdDownlinkDisabledCount;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainUfdDownlinkEnabledCount
 Input       :  The Indices
                IfMainIndex

                The Object 
                retValIfMainUfdDownlinkEnabledCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainUfdDownlinkEnabledCount (INT4 i4IfMainIndex,
                                     UINT4
                                     *pu4RetValIfMainUfdDownlinkEnabledCount)
{
    UINT4               u4UfdDownlinkEnabledCount = 0;

    *pu4RetValIfMainUfdDownlinkEnabledCount = 0;
    if (CfaGetIfUfdDownlinkEnabledCount ((UINT4) i4IfMainIndex,
                                         &u4UfdDownlinkEnabledCount) ==
        CFA_SUCCESS)
    {
        *pu4RetValIfMainUfdDownlinkEnabledCount = u4UfdDownlinkEnabledCount;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainDesigUplinkStatus
 Input       :  The Indices
                IfMainIndex

                The Object
                retValIfMainDesigUplinkStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainDesigUplinkStatus (INT4 i4IfMainIndex,
                               INT4 *pi4RetValIfMainDesigUplinkStatus)
{
    UINT1               u1DesigUplinkStatus = 0;

    if (CfaGetIfDesigUplinkStatus ((UINT4) i4IfMainIndex, &u1DesigUplinkStatus)
        == CFA_SUCCESS)
    {
        *pi4RetValIfMainDesigUplinkStatus = (INT4) u1DesigUplinkStatus;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainEncapDot1qVlanId
 Input       :  The Indices
                IfMainIndex

                The Object
                retValIfMainEncapDot1qVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.                                             Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainEncapDot1qVlanId (INT4 i4IfMainIndex,
                              INT4 *pi4RetValIfMainEncapDot1qVlanId)
{
    UINT2               u2VlanId = 0;

    /* Get the VLAN Id configured for the Subinterface */
    if (CfaGetIfIvrVlanId ((UINT4) i4IfMainIndex, &u2VlanId) == CFA_SUCCESS)
    {
        *pi4RetValIfMainEncapDot1qVlanId = (INT4) u2VlanId;
    }

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIfMainPortRole
 Input       :  The Indices
                IfMainIndex

                The Object
                setValIfMainPortRole
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainPortRole (INT4 i4IfMainIndex, INT4 i4SetValIfMainPortRole)
{
    UINT1               u1PortRole = 0;
    UINT1               u1ModifyShRules = CFA_FALSE;
    UINT1               u1DownlinkCount = 0;
    UINT1               u1UplinkCount = 0;
    UINT1               u1BridgedIfaceStatus = CFA_DISABLED;
    UINT1               u1IfType = 0;
    UINT4               u4IfIndex = 0;

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (CfaIsIfEntryProgrammingAllowed ((UINT4) i4IfMainIndex) == CFA_TRUE)
    {
        if (CFA_IF_ENTRY ((UINT4) i4IfMainIndex) == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
    if (CfaGetIfPortRole ((UINT4) i4IfMainIndex, &u1PortRole) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u1PortRole == (UINT1) i4SetValIfMainPortRole)
    {
        return SNMP_SUCCESS;
    }

    /* Flush the Split-Horizon entries in the system */

    if (CFA_SH_MODULE_STATUS == CFA_SH_ENABLED)
    {
        for (u4IfIndex = 1; u4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES;
             u4IfIndex++)
        {
            CfaGetIfType (u4IfIndex, &u1IfType);
            CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);

            if ((u1IfType == CFA_ENET)
                && (u1BridgedIfaceStatus != CFA_DISABLED))
            {

                if (CfaGetIfPortRole (u4IfIndex, &u1PortRole) != CFA_SUCCESS)
                {
                    continue;
                }
                if (u4IfIndex == (UINT4) i4IfMainIndex)
                {
                    u1PortRole = (UINT1) i4SetValIfMainPortRole;
                }

                if (u1PortRole == CFA_PORT_ROLE_DOWNLINK)
                {
                    u1DownlinkCount++;
                }

                else if (u1PortRole == CFA_PORT_ROLE_UPLINK)
                {
                    u1UplinkCount++;
                }
            }

        }
        if ((u1DownlinkCount == 0) || (u1UplinkCount == 0))
        {
            /* At least one uplink and downlink port must be present
             * for split-horizon */
            CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                     "Minimum of one Uplink and Downlink Ports"
                     "should be present while SH is running in the System\n");
            return SNMP_FAILURE;
        }
        else
        {
            /* Flush the Split-Horizon Table Entries */
            if (CfaShDisable (CFA_SH_DISABLED) == CFA_SUCCESS)
            {
                u1ModifyShRules = CFA_TRUE;
            }

        }

    }

    CfaGetIfType ((UINT4) i4IfMainIndex, &u1IfType);
    CfaGetIfBridgedIfaceStatus ((UINT4) i4IfMainIndex, &u1BridgedIfaceStatus);

    if ((u1IfType == CFA_LAGG) ||
        ((u1IfType == CFA_ENET) && (u1BridgedIfaceStatus != CFA_DISABLED)))
    {
        if (CfaIfChangePortRole
            ((UINT4) i4IfMainIndex,
             (UINT1) i4SetValIfMainPortRole) != CFA_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    /* If Change of Port role is successful , 
     * Reconfigure split-horizon rules */
    if (u1ModifyShRules == CFA_TRUE)
    {
        if (CfaShEnable (CFA_SH_ENABLED) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_UFD,
                      "Reconfiguration of Split-Horizon rules failed"
                      "For the IfIndex %d\n", i4IfMainIndex);
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfMainDesigUplinkStatus
 Input       :  The Indices
                IfMainIndex

                The Object
                setValIfMainDesigUplinkStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainDesigUplinkStatus (INT4 i4IfMainIndex,
                               INT4 i4SetValIfMainDesigUplinkStatus)
{
    UINT1               u1DesigUplinkStatus = 0;
    UINT1               u1ModifyShRules = CFA_FALSE;

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (CfaIsIfEntryProgrammingAllowed ((UINT4) i4IfMainIndex) == CFA_TRUE)
    {
        if (CFA_IF_ENTRY ((UINT4) i4IfMainIndex) == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
    if (CfaGetIfDesigUplinkStatus ((UINT4) i4IfMainIndex, &u1DesigUplinkStatus)
        != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u1DesigUplinkStatus == (UINT1) i4SetValIfMainDesigUplinkStatus)
    {
        return SNMP_SUCCESS;
    }
    /* Flush and reconfigure Split-Horizon rules , since there is a change in the role
     * of Designated uplink Port */

    if (CFA_SH_MODULE_STATUS == CFA_SH_ENABLED)
    {
        /* Flush the SH rules in The System */
        if (CfaShDisable (CFA_SH_DISABLED) == CFA_SUCCESS)
        {
            u1ModifyShRules = CFA_TRUE;
        }
    }

    if (CfaSetIfDesigUplinkStatus
        ((UINT4) i4IfMainIndex,
         (UINT1) i4SetValIfMainDesigUplinkStatus) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (u1ModifyShRules == CFA_TRUE)
    {
        if (CfaShEnable (CFA_SH_ENABLED) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_UFD,
                      "Enabling SH rules Failed while changing "
                      "the port role of For the IfIndex %d\n", i4IfMainIndex);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfMainEncapDot1qVlanId
 Input       :  The Indices
                IfMainIndex

                The Object
                setValIfMainEncapDot1qVlanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainEncapDot1qVlanId (INT4 i4IfMainIndex,
                              INT4 i4SetValIfMainEncapDot1qVlanId)
{
    UINT1               u1Command = 0;
    UINT2               u2VlanId = 0;
#ifdef NPAPI_WANTED
    UINT1               au1MacAddress[CFA_ENET_ADDR_LEN];
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    INT4                i4RetVal = FNP_FAILURE;
    UINT4               u4ParentIfIndex = 0;
#ifdef VCM_WANTED
    UINT4               u4ContextId = 0;
#endif
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    CfaGetIfName ((UINT4) i4IfMainIndex, au1IfName);

    MEMSET (au1MacAddress, 0, CFA_ENET_ADDR_LEN);
    CfaGetIfHwAddr ((UINT4) i4IfMainIndex, au1MacAddress);
#endif

    if (CfaIsL3SubIfIndex ((UINT4) i4IfMainIndex) == CFA_FALSE)
    {
        return SNMP_FAILURE;

    }

    if (i4SetValIfMainEncapDot1qVlanId == 0)
    {
        u1Command = CFA_DISABLED;
    }
    else if (i4SetValIfMainEncapDot1qVlanId > 0)
    {
        u1Command = CFA_ENABLED;
    }
    else
    {
        u1Command = CFA_NONE;
    }

    switch (u1Command)
    {
        case CFA_ENABLED:
        {
            /* Set the Layer 3 VLAN ID to the Subinterface */
            CfaSetIfIvrVlanId ((UINT4) i4IfMainIndex,
                               (UINT2) i4SetValIfMainEncapDot1qVlanId);

            /* for linux vlan interface  do not set gIntfVlanBitList */
            if (CfaIsLinuxVlanIntf ((UINT4) i4IfMainIndex) == FALSE)
            {
                /* Set this vlan as an interface Vlan */
                CfaCdbAddToIntfVlanList ((UINT2)
                                         i4SetValIfMainEncapDot1qVlanId);
            }

            /* Sets the Encapsulation dot1q status as TRUE */
            CFA_CDB_SET_ENCAP_STATUS ((UINT4) i4IfMainIndex, CFA_TRUE);

            /* Notify interface status change notification */
            CfaInterfaceStatusChangeIndication ((UINT4) i4IfMainIndex,
                                                CFA_IF_UP);
#ifdef NPAPI_WANTED

#ifdef VCM_WANTED
            VcmGetContextIdFromCfaIfIndex ((UINT4) i4IfMainIndex, &u4ContextId);
#endif
            CfaGetL3SubIfParentIndex ((UINT4) i4IfMainIndex, &u4ParentIfIndex);
            if ((CfaIsMgmtPort ((UINT4) i4IfMainIndex) == FALSE) &&
                (CfaIsLinuxVlanIntf ((UINT4) i4IfMainIndex) == FALSE))
            {
                if (CFA_NODE_STATUS () == CFA_NODE_ACTIVE)
                {
                    i4RetVal =
                        CfaFsNpIpv4CreateL3SubInterface (u4ContextId,
                                                         au1IfName,
                                                         (UINT4) i4IfMainIndex,
                                                         0, 0,
                                                         (UINT2)
                                                         i4SetValIfMainEncapDot1qVlanId,
                                                         au1MacAddress,
                                                         u4ParentIfIndex);
                    if (i4RetVal == FNP_FAILURE)
                    {
                        return CFA_FAILURE;
                    }

                }
            }
#endif
        }
            break;

        case CFA_DISABLED:
        {
            /* Get the VLAN Id configured for the Subinterface */
            CfaGetIfIvrVlanId ((UINT4) i4IfMainIndex, &u2VlanId);

            /* Remove the VLAND Id from the IntfVlan List */
            CfaCdbRemoveFromIntfVlanList (u2VlanId);

            CFA_CDB_IF_VLANID_OF_IVR ((UINT4) i4IfMainIndex) = 0;

            /* Set the encap status to be CFA_FALSE */
            CFA_CDB_SET_ENCAP_STATUS ((UINT4) i4IfMainIndex, CFA_FALSE);

            /* Notify down indication to the interface */
            CfaInterfaceStatusChangeIndication ((UINT4) i4IfMainIndex,
                                                CFA_IF_DOWN);
#ifdef NPAPI_WANTED
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            CfaGetIfName ((UINT4) i4IfMainIndex, au1IfName);

            CfaGetL3SubIfParentIndex ((UINT4) i4IfMainIndex, &u4ParentIfIndex);

            i4RetVal = CfaFsNpIpv4UpdateIpInterfaceStatus (0, au1IfName,
                                                           (UINT4)
                                                           i4IfMainIndex, 0, 0,
                                                           u2VlanId, 0,
                                                           CFA_IF_DOWN);
            if (FNP_FAILURE == i4RetVal)
            {
                return CFA_FAILURE;
            }
            i4RetVal = CfaFsNpIpv4DeleteL3SubInterface ((UINT4) i4IfMainIndex);

            if (FNP_FAILURE == i4RetVal)
            {
                return CFA_FAILURE;
            }

#endif
        }
            break;
        default:
            break;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfMainPortRole
 Input       :  The Indices
                IfMainIndex

                The Object
                testValIfMainPortRole
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainPortRole (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                         INT4 i4TestValIfMainPortRole)
{
    UINT1               u1BrgPortType = 0;

    CfaGetIfBrgPortType ((UINT4) i4IfMainIndex, &u1BrgPortType);

    /* S-Channel Interface cannot be assigned any Port-Role */
    if (u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_SBP_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValIfMainPortRole != CFA_PORT_ROLE_UPLINK) &&
        (i4TestValIfMainPortRole != CFA_PORT_ROLE_DOWNLINK))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4IfMainIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfMainDesigUplinkStatus
 Input       :  The Indices
                IfMainIndex

                The Object
                testValIfMainDesigUplinkStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainDesigUplinkStatus (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                                  INT4 i4TestValIfMainDesigUplinkStatus)
{
    if ((i4TestValIfMainDesigUplinkStatus != CFA_ENABLED) &&
        (i4TestValIfMainDesigUplinkStatus != CFA_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4IfMainIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfMainEncapDot1qVlanId
 Input       :  The Indices
                IfMainIndex

                The Object
                testValIfMainEncapDot1qVlanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainEncapDot1qVlanId (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                                 INT4 i4TestValIfMainEncapDot1qVlanId)
{
    INT4                i4Status = 0;

    if (CfaIsL3SubIfIndex ((UINT4) i4IfMainIndex) == CFA_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIfMainEncapDot1qVlanId < 1) ||
        (i4TestValIfMainEncapDot1qVlanId > VLAN_MAX_VLAN_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check if configuring VLAN id is overlapping with anyother subinterface 
     * belonging to same parent */
    if (CfaIsVlanOverLapping
        ((UINT4) i4IfMainIndex,
         (UINT2) i4TestValIfMainEncapDot1qVlanId) == CFA_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_CFA_VLAN_OVERLAPS);
        return SNMP_FAILURE;
    }
    CFA_CDB_GET_ENCAP_STATUS (i4IfMainIndex, i4Status);

    if (CFA_TRUE == i4Status)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_CFA_ENCAP_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIfUfdSystemControl
 Input       :  The Indices

                The Object
                retValIfUfdSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfUfdSystemControl (INT4 *pi4RetValIfUfdSystemControl)
{
    *pi4RetValIfUfdSystemControl = (INT4) CFA_UFD_SYSTEM_CONTROL ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfUfdModuleStatus
 Input       :  The Indices

                The Object
                retValIfUfdModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfUfdModuleStatus (INT4 *pi4RetValIfUfdModuleStatus)
{
    *pi4RetValIfUfdModuleStatus = (INT4) CFA_UFD_MODULE_STATUS ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIfUfdSystemControl
 Input       :  The Indices

                The Object
                setValIfUfdSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfUfdSystemControl (INT4 i4SetValIfUfdSystemControl)
{
/*
     * If the value is same no need to set it again.
     * */
    if (CFA_UFD_SYSTEM_CONTROL () != (UINT4) i4SetValIfUfdSystemControl)
    {
        if (i4SetValIfUfdSystemControl == CFA_UFD_START)
        {
            if (CfaUfdStart () != OSIX_SUCCESS)
            {
                return SNMP_FAILURE;
            }
        }
        else
        {
            CfaUfdShutdown ();
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfUfdModuleStatus
 Input       :  The Indices

                The Object
                setValIfUfdModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfUfdModuleStatus (INT4 i4SetValIfUfdModuleStatus)
{
    if ((CFA_UFD_SYSTEM_CONTROL () != CFA_UFD_START) &&
        (i4SetValIfUfdModuleStatus == CFA_UFD_ENABLED))
    {
        CLI_SET_ERR (CLI_CFA_UFD_SYS_CONTROL_ERR);
        return SNMP_FAILURE;
    }

    if (CFA_UFD_MODULE_STATUS () != (UINT4) i4SetValIfUfdModuleStatus)
    {
        if (i4SetValIfUfdModuleStatus == CFA_UFD_ENABLED)
        {
            CfaUfdEnable ();
        }
        else
        {
            CfaUfdDisable ();
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IfUfdSystemControl
 Input       :  The Indices

                The Object
                testValIfUfdSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfUfdSystemControl (UINT4 *pu4ErrorCode,
                             INT4 i4TestValIfUfdSystemControl)
{
    if (((UINT4) i4TestValIfUfdSystemControl != CFA_UFD_START) &&
        ((UINT4) i4TestValIfUfdSystemControl != CFA_UFD_SHUTDOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfUfdModuleStatus
 Input       :  The Indices

                The Object
                testValIfUfdModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfUfdModuleStatus (UINT4 *pu4ErrorCode,
                            INT4 i4TestValIfUfdModuleStatus)
{

    if ((i4TestValIfUfdModuleStatus == CFA_UFD_ENABLED) ||
        (i4TestValIfUfdModuleStatus == CFA_UFD_DISABLED))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IfUfdSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfUfdSystemControl (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2IfUfdModuleStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfUfdModuleStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIfUfdGroupTable (INT4 i4IfUfdGroupId)
{
    tUfdGroupInfo      *pUfdGroupInfoEntry = NULL;

    if (i4IfUfdGroupId <= 0)
    {
        return SNMP_FAILURE;
    }

    pUfdGroupInfoEntry = CfaIfUfdGroupInfoDbGet ((UINT4) i4IfUfdGroupId);
    if (pUfdGroupInfoEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIfUfdGroupTable
 Input       :  The Indices
                IfUfdGroupId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIfUfdGroupTable (INT4 *pi4IfUfdGroupId)
{
    UINT4               u4IfUfdGroupId;

    if (CfaGetFirstUfdGroup (&u4IfUfdGroupId) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4IfUfdGroupId = (INT4) u4IfUfdGroupId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIfUfdGroupTable
 Input       :  The Indices
                IfUfdGroupId
                nextIfUfdGroupId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIfUfdGroupTable (INT4 i4IfUfdGroupId, INT4 *pi4NextIfUfdGroupId)
{
    UINT4               u4NextUfdGroupId = 0;

    if (CfaGetNextUfdGroup ((UINT4) i4IfUfdGroupId, &u4NextUfdGroupId)
        == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4NextIfUfdGroupId = (INT4) u4NextUfdGroupId;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIfUfdGroupName
 Input       :  The Indices
                IfUfdGroupId

                The Object
                retValIfUfdGroupName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfUfdGroupName (INT4 i4IfUfdGroupId,
                      tSNMP_OCTET_STRING_TYPE * pRetValIfUfdGroupName)
{
    tUfdGroupInfo      *pUfdGroupInfoEntry = NULL;

    pUfdGroupInfoEntry = CfaIfUfdGroupInfoDbGet ((UINT4) i4IfUfdGroupId);
    if (pUfdGroupInfoEntry != NULL)
    {
        pRetValIfUfdGroupName->i4_Length =
            (INT4) STRLEN (pUfdGroupInfoEntry->au1UfdGroupName);
        if (pRetValIfUfdGroupName->i4_Length != 0)
        {
            MEMCPY (pRetValIfUfdGroupName->pu1_OctetList,
                    pUfdGroupInfoEntry->au1UfdGroupName,
                    pRetValIfUfdGroupName->i4_Length);
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfUfdGroupStatus
 Input       :  The Indices
                IfUfdGroupId

                The Object
                retValIfUfdGroupStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfUfdGroupStatus (INT4 i4IfUfdGroupId, INT4 *pi4RetValIfUfdGroupStatus)
{
    UINT1               u1UfdGroupStatus = 0;

    if (CfaUfdGetGroupStatus ((UINT4) i4IfUfdGroupId, &u1UfdGroupStatus)
        == CFA_SUCCESS)
    {
        *pi4RetValIfUfdGroupStatus = (INT4) u1UfdGroupStatus;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfUfdGroupUplinkPorts
 Input       :  The Indices
                IfUfdGroupId

                The Object 
                retValIfUfdGroupUplinkPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfUfdGroupUplinkPorts (INT4 i4IfUfdGroupId,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValIfUfdGroupUplinkPorts)
{
    tUfdGroupInfo      *pUfdGroupInfo = NULL;

    pUfdGroupInfo = CfaIfUfdGroupInfoDbGet ((UINT4) i4IfUfdGroupId);

    if (pUfdGroupInfo != NULL)
    {
        pRetValIfUfdGroupUplinkPorts->i4_Length =
            (INT4) sizeof (pUfdGroupInfo->UfdGroupUplinkPortList);
        MEMCPY (pRetValIfUfdGroupUplinkPorts->pu1_OctetList,
                pUfdGroupInfo->UfdGroupUplinkPortList,
                pRetValIfUfdGroupUplinkPorts->i4_Length);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfUfdGroupDownlinkPorts
 Input       :  The Indices
                IfUfdGroupId

                The Object 
                retValIfUfdGroupDownlinkPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfUfdGroupDownlinkPorts (INT4 i4IfUfdGroupId,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValIfUfdGroupDownlinkPorts)
{
    tUfdGroupInfo      *pUfdGroupInfo = NULL;

    pUfdGroupInfo = CfaIfUfdGroupInfoDbGet ((UINT4) i4IfUfdGroupId);

    if (pUfdGroupInfo != NULL)
    {
        pRetValIfUfdGroupDownlinkPorts->i4_Length =
            (INT4) sizeof (pUfdGroupInfo->UfdGroupDownlinkPortList);
        MEMCPY (pRetValIfUfdGroupDownlinkPorts->pu1_OctetList,
                pUfdGroupInfo->UfdGroupDownlinkPortList,
                pRetValIfUfdGroupDownlinkPorts->i4_Length);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfUfdGroupDesigUplinkPort
 Input       :  The Indices
                IfUfdGroupId

                The Object 
                retValIfUfdGroupDesigUplinkPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfUfdGroupDesigUplinkPort (INT4 i4IfUfdGroupId,
                                 INT4 *pi4RetValIfUfdGroupDesigUplinkPort)
{
    tUfdGroupInfo      *pUfdGroupInfo = NULL;

    pUfdGroupInfo = CfaIfUfdGroupInfoDbGet ((UINT4) i4IfUfdGroupId);

    if (pUfdGroupInfo != NULL)
    {
        *pi4RetValIfUfdGroupDesigUplinkPort =
            (INT4) pUfdGroupInfo->u4UfdGroupDesigUplinkPort;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfUfdGroupUplinkCount
 Input       :  The Indices
                IfUfdGroupId

                The Object
                retValIfUfdGroupUplinkCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfUfdGroupUplinkCount (INT4 i4IfUfdGroupId,
                             INT4 *pi4RetValIfUfdGroupUplinkCount)
{
    tUfdGroupInfo      *pUfdGroupInfo = NULL;

    pUfdGroupInfo = CfaIfUfdGroupInfoDbGet ((UINT4) i4IfUfdGroupId);

    if (pUfdGroupInfo != NULL)
    {
        *pi4RetValIfUfdGroupUplinkCount =
            (INT4) (pUfdGroupInfo->u4UfdUplinkCount);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfUfdGroupDownlinkCount
 Input       :  The Indices
                IfUfdGroupId

                The Object
                retValIfUfdGroupDownlinkCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfUfdGroupDownlinkCount (INT4 i4IfUfdGroupId,
                               INT4 *pi4RetValIfUfdGroupDownlinkCount)
{
    tUfdGroupInfo      *pUfdGroupInfo = NULL;

    pUfdGroupInfo = CfaIfUfdGroupInfoDbGet ((UINT4) i4IfUfdGroupId);

    if (pUfdGroupInfo != NULL)
    {
        *pi4RetValIfUfdGroupDownlinkCount =
            (INT4) (pUfdGroupInfo->u4UfdDownlinkCount);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfUfdGroupRowStatus
 Input       :  The Indices
                IfUfdGroupId

                The Object
                retValIfUfdGroupRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfUfdGroupRowStatus (INT4 i4IfUfdGroupId,
                           INT4 *pi4RetValIfUfdGroupRowStatus)
{
    tUfdGroupInfo      *pUfdGroupInfo = NULL;

    pUfdGroupInfo = CfaIfUfdGroupInfoDbGet ((UINT4) i4IfUfdGroupId);
    if (pUfdGroupInfo != NULL)
    {
        *pi4RetValIfUfdGroupRowStatus = (INT4) pUfdGroupInfo->u4UfdRowStatus;
    }
    else
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetValIfUfdGroupNameIfUfdGeroupName
 Input       :  The Indices
                IfUfdGroupId

                The Object
                setValIfUfdGroupName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfUfdGroupName (INT4 i4IfUfdGroupId,
                      tSNMP_OCTET_STRING_TYPE * pSetValIfUfdGroupName)
{
    tUfdGroupInfo      *pUfdGroupInfo = NULL;

    pUfdGroupInfo = CfaIfUfdGroupInfoDbGet ((UINT4) i4IfUfdGroupId);
    if (pUfdGroupInfo != NULL)
    {
        MEMSET (pUfdGroupInfo->au1UfdGroupName, 0, CFA_UFD_GROUP_MAX_NAME_LEN);
        MEMCPY (pUfdGroupInfo->au1UfdGroupName,
                pSetValIfUfdGroupName->pu1_OctetList,
                pSetValIfUfdGroupName->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfUfdGroupUplinkPorts
 Input       :  The Indices
                IfUfdGroupId

                The Object 
                setValIfUfdGroupUplinkPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfUfdGroupUplinkPorts (INT4 i4IfUfdGroupId,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValIfUfdGroupUplinkPorts)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4UfdGetGroupId = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;
    UINT1               u1IfPortRole = 0;
    for (u2ByteIndex = 0; u2ByteIndex < BRG_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = pSetValIfUfdGroupUplinkPorts->pu1_OctetList[u2ByteIndex];
        for (u2BitIndex = 0; u2BitIndex < BITS_PER_BYTE; u2BitIndex++)
        {
            u4IfIndex =
                (UINT4) ((u2ByteIndex * BITS_PER_BYTE) + u2BitIndex + 1);
            if (CfaGetIfPortRole (u4IfIndex, &u1IfPortRole) != CFA_SUCCESS)
            {
                /* Need not care  for ports other than uplink */
                u1PortFlag = (UINT1) (u1PortFlag << 1);
                continue;
            }
            if (u1IfPortRole != CFA_PORT_ROLE_UPLINK)
            {
                /* Need not care  for ports other than uplink */
                u1PortFlag = (UINT1) (u1PortFlag << 1);
                continue;
            }

            if ((u1PortFlag & INTERNAL_BIT8) != 0)
            {
                /* Bit set as 1 ; Port has to be added */
                /* NOTE : In case of any invalid interface, 
                 * failure should be returned in nmhTest Validation function */
                if (CfaGetIfUfdGroupId (u4IfIndex, &u4UfdGetGroupId) ==
                    CFA_SUCCESS)
                {
                    /* This check filters out ports that already
                     * belong to this group*/

                    if (u4UfdGetGroupId == (UINT4) i4IfUfdGroupId)
                    {
                        u1PortFlag = (UINT1) (u1PortFlag << 1);
                        continue;
                    }

                    if (CfaUfdAddPortToGroup (u4IfIndex,
                                              (UINT4) i4IfUfdGroupId) !=
                        CFA_SUCCESS)
                    {
                        CLI_SET_ERR (CLI_CFA_UFD_PORT_ADD_ERR);
                        return SNMP_FAILURE;
                    }
                }

            }
            else
            {
                /* Bit set as 0 ; Port has to be deleted */
                /* This check filters out ports that doesn't 
                 * belong to this group*/

                if (CfaGetIfUfdGroupId (u4IfIndex, &u4UfdGetGroupId) ==
                    CFA_SUCCESS)
                {
                    /* This check filters out ports that doesn't 
                     * belong to this group*/

                    if (u4UfdGetGroupId != (UINT4) i4IfUfdGroupId)
                    {
                        u1PortFlag = (UINT1) (u1PortFlag << 1);
                        continue;
                    }
                    if (CfaUfdRemovePortFromGroup (u4IfIndex,
                                                   (UINT4) i4IfUfdGroupId) !=
                        CFA_SUCCESS)
                    {
                        CLI_SET_ERR (CLI_CFA_UFD_PORT_DEL_ERR);
                        return SNMP_FAILURE;
                    }
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfUfdGroupDownlinkPorts
 Input       :  The Indices
                IfUfdGroupId

                The Object 
                setValIfUfdGroupDownlinkPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfUfdGroupDownlinkPorts (INT4 i4IfUfdGroupId,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValIfUfdGroupDownlinkPorts)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4UfdGetGroupId = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;
    UINT1               u1IfPortRole = 0;
    for (u2ByteIndex = 0; u2ByteIndex < BRG_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = pSetValIfUfdGroupDownlinkPorts->pu1_OctetList[u2ByteIndex];
        for (u2BitIndex = 0; u2BitIndex < BITS_PER_BYTE; u2BitIndex++)
        {
            u4IfIndex =
                (UINT4) ((u2ByteIndex * BITS_PER_BYTE) + u2BitIndex + 1);
            if (CfaGetIfPortRole (u4IfIndex, &u1IfPortRole) != CFA_SUCCESS)
            {
                /* Need not care for ports other than downlink */
                u1PortFlag = (UINT1) (u1PortFlag << 1);
                continue;
            }
            if (u1IfPortRole != CFA_PORT_ROLE_DOWNLINK)
            {
                /* Need not care  for ports other than downlink */
                u1PortFlag = (UINT1) (u1PortFlag << 1);
                continue;
            }
            if ((u1PortFlag & INTERNAL_BIT8) != 0)
            {
                /* Bit set as 1 ; Port has to be added */
                if (CfaGetIfUfdGroupId (u4IfIndex, &u4UfdGetGroupId) ==
                    CFA_SUCCESS)
                {
                    /* This check filters out ports that already
                     *                      * belong to this group*/

                    if (u4UfdGetGroupId == (UINT4) i4IfUfdGroupId)
                    {
                        u1PortFlag = (UINT1) (u1PortFlag << 1);
                        continue;
                    }

                    if (CfaUfdAddPortToGroup (u4IfIndex,
                                              (UINT4) i4IfUfdGroupId) !=
                        CFA_SUCCESS)
                    {
                        CLI_SET_ERR (CLI_CFA_UFD_PORT_ADD_ERR);
                        return SNMP_FAILURE;
                    }
                }

            }
            else
            {
                /* Bit set as 0 ; Port has to be deleted */

                /* This function call will eliminate invalid interfaces */
                if (CfaGetIfUfdGroupId (u4IfIndex, &u4UfdGetGroupId) ==
                    CFA_SUCCESS)
                {
                    /* This check filters out ports that doesn't 
                     * belong to this group*/
                    if (u4UfdGetGroupId != (UINT4) i4IfUfdGroupId)
                    {
                        u1PortFlag = (UINT1) (u1PortFlag << 1);
                        continue;
                    }

                    if (CfaUfdRemovePortFromGroup (u4IfIndex,
                                                   (UINT4) i4IfUfdGroupId) !=
                        CFA_SUCCESS)
                    {
                        CLI_SET_ERR (CLI_CFA_UFD_PORT_DEL_ERR);
                        return SNMP_FAILURE;
                    }
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfUfdGroupDesigUplinkPort
 Input       :  The Indices
                IfUfdGroupId

                The Object 
                setValIfUfdGroupDesigUplinkPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfUfdGroupDesigUplinkPort (INT4 i4IfUfdGroupId,
                                 INT4 i4SetValIfUfdGroupDesigUplinkPort)
{
    tUfdGroupInfo      *pUfdGroupInfo = NULL;

    pUfdGroupInfo = CfaIfUfdGroupInfoDbGet ((UINT4) i4IfUfdGroupId);
    if (pUfdGroupInfo != NULL)
    {
        pUfdGroupInfo->u4UfdGroupDesigUplinkPort =
            (UINT4) i4SetValIfUfdGroupDesigUplinkPort;
        return SNMP_SUCCESS;
    }
    if (CFA_SH_MODULE_STATUS == CFA_SH_ENABLED)
    {
        /* Flush the Split-Horizon port entry table entries */
        if (CfaShDisable (CFA_SH_DISABLED) == CFA_SUCCESS)
        {
            if (CfaShEnable (CFA_SH_ENABLED) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_UFD,
                          "reConfiguration of Split-Horizon Rules Failed"
                          "for change of Desig Port %d\n",
                          i4SetValIfUfdGroupDesigUplinkPort);
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIfMainUfdGroupId
 Input       :  The Indices
                IfMainIndex

                The Object
                setValIfMainUfdGroupId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainUfdGroupId (INT4 i4IfMainIndex, INT4 i4SetValIfMainUfdGroupId)
{
    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (CfaIsIfEntryProgrammingAllowed ((UINT4) i4IfMainIndex) == CFA_TRUE)
    {
        if (CFA_IF_ENTRY ((UINT4) i4IfMainIndex) == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }

    if (i4SetValIfMainUfdGroupId != 0)
    {
        if (CfaUfdAddPortToGroup ((UINT4) i4IfMainIndex,
                                  (UINT4) i4SetValIfMainUfdGroupId) !=
            CFA_SUCCESS)
        {
            CLI_SET_ERR (CLI_CFA_UFD_PORT_ADD_ERR);
            return SNMP_FAILURE;
        }

    }
    else
    {
        if (CfaUfdRemovePortFromGroup ((UINT4) i4IfMainIndex,
                                       (UINT4) i4SetValIfMainUfdGroupId) !=
            CFA_SUCCESS)
        {
            CLI_SET_ERR (CLI_CFA_UFD_PORT_DEL_ERR);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfMainUfdGroupId
 Input       :  The Indices
                IfMainIndex

                The Object
                testValIfMainUfdGroupId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainUfdGroupId (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                           INT4 i4TestValIfMainUfdGroupId)
{
    INT4                i4PortRole = 0;
    INT4                i4IfUfdGroupDownlinkCount = 0;

    /* Check if port is an OOB port */
    if ((CFA_MGMT_PORT == TRUE) && (i4IfMainIndex == CFA_OOB_MGMT_IFINDEX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return CFA_FAILURE;
    }

    /* If this is a uplink port,
     * Check whether downlink count is zero or not */
    if (nmhGetIfMainPortRole (i4IfMainIndex, &i4PortRole) != SNMP_SUCCESS)
    {
        /* Invalid port */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (nmhGetIfUfdGroupDownlinkCount (i4TestValIfMainUfdGroupId,
                                       &i4IfUfdGroupDownlinkCount) !=
        SNMP_SUCCESS)
    {
        /* Invalid group */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4PortRole == CFA_PORT_ROLE_UPLINK) &&
        (i4IfUfdGroupDownlinkCount == 0))
    {
        /* Cannot add uplink port to group with no downlinks */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfUfdGroupRowStatus
 Input       :  The Indices
                IfUfdGroupId

                The Object
                setValIfUfdGroupRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfUfdGroupRowStatus (INT4 i4IfUfdGroupId,
                           INT4 i4SetValIfUfdGroupRowStatus)
{
    tUfdGroupInfo      *pUfdGroupInfo = NULL;
    UINT4               u4UfdGroupCount = 0;

    pUfdGroupInfo = CfaIfUfdGroupInfoDbGet ((UINT4) i4IfUfdGroupId);
    if (pUfdGroupInfo != NULL)
    {
        if (pUfdGroupInfo->u4UfdRowStatus ==
            (UINT4) i4SetValIfUfdGroupRowStatus)
        {
            return SNMP_SUCCESS;
        }
        if ((i4SetValIfUfdGroupRowStatus == CREATE_AND_WAIT) ||
            (i4SetValIfUfdGroupRowStatus == CREATE_AND_GO))
        {
            /*This UFD group entry is already present.So Creation not allowed */
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (((UINT4) i4SetValIfUfdGroupRowStatus != CREATE_AND_WAIT) &&
            ((UINT4) i4SetValIfUfdGroupRowStatus != CREATE_AND_GO))
        {
            /* This UFD group entry is not present.So execpt creation, other
             * settings are not allowed. */
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValIfUfdGroupRowStatus)
    {
        case ACTIVE:
        case NOT_READY:
        case NOT_IN_SERVICE:
            pUfdGroupInfo->u4UfdRowStatus = (UINT4) i4SetValIfUfdGroupRowStatus;
            break;

        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
            CfaUfdGetGroupCount (&u4UfdGroupCount);
            if (u4UfdGroupCount >= CFA_UFD_MAX_GROUP)
            {
                CLI_SET_ERR (CLI_CFA_UFD_GROUP_MAX_LIMIT_REACHED_ERR);
                return SNMP_FAILURE;
            }

            if (CfaUfdCreateGroup (i4IfUfdGroupId) == CFA_FAILURE)
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_UFD, "UFD group creation is failed"
                          "for GroupId  %d\n", i4IfUfdGroupId);
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:
            CfaUfdDeleteGroup (i4IfUfdGroupId);
            break;

        default:
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2IfUfdGroupName
 Input       :  The Indices
                IfUfdGroupId

                The Object
                testValIfUfdGroupName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfUfdGroupName (UINT4 *pu4ErrorCode, INT4 i4IfUfdGroupId,
                         tSNMP_OCTET_STRING_TYPE * pTestValIfUfdGroupName)
{
    if (CFA_UFD_SYSTEM_CONTROL () != CFA_UFD_START)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_CFA_UFD_SYS_CONTROL_ERR);
        return SNMP_FAILURE;
    }
    if (pTestValIfUfdGroupName->i4_Length > CFA_UFD_GROUP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (CfaUfdCheckForAlpha (pTestValIfUfdGroupName->pu1_OctetList)
        != CFA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (CfaUfdCheckGroupNameUnique
        (pTestValIfUfdGroupName->pu1_OctetList,
         (UINT4) i4IfUfdGroupId) != CFA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4IfUfdGroupId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfUfdGroupRowStatus
 Input       :  The Indices
                IfUfdGroupId

                The Object
                testValIfUfdGroupRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfUfdGroupRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfUfdGroupId,
                              INT4 i4TestValIfUfdGroupRowStatus)
{
    tUfdGroupInfo      *pUfdGroupInfo = NULL;

    if (CFA_UFD_SYSTEM_CONTROL () != CFA_UFD_START)
    {
        CLI_SET_ERR (CLI_CFA_UFD_SYS_CONTROL_ERR);
        return SNMP_FAILURE;
    }
    if (CFA_UFD_MODULE_STATUS () != CFA_UFD_ENABLED)
    {
        CLI_SET_ERR (CLI_CFA_UFD_MODULE_STATUS_ERR);
        return SNMP_FAILURE;
    }

    pUfdGroupInfo = CfaIfUfdGroupInfoDbGet ((UINT4) i4IfUfdGroupId);
    switch (i4TestValIfUfdGroupRowStatus)
    {
        case ACTIVE:
        case NOT_IN_SERVICE:
        case NOT_READY:
        case DESTROY:
            if (pUfdGroupInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
            if (pUfdGroupInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfUfdGroupUplinkPorts
 Input       :  The Indices
                IfUfdGroupId

                The Object 
                testValIfUfdGroupUplinkPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfUfdGroupUplinkPorts (UINT4 *pu4ErrorCode, INT4 i4IfUfdGroupId,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValIfUfdGroupUplinkPorts)
{
    tPortList          *pUplinkPorts = NULL;
    tPortList          *pDownlinkPorts = NULL;

    /* Check if the port role is uplink */
    pUplinkPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
    if (pUplinkPorts == NULL)
    {
        return SNMP_FAILURE;
    }

    pDownlinkPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
    if (pDownlinkPorts == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
        return SNMP_FAILURE;
    }

    MEMSET (pUplinkPorts, 0, sizeof (tPortList));
    MEMSET (pDownlinkPorts, 0, sizeof (tPortList));

    CfaUfdSeperatePortlistOnPortRole (pTestValIfUfdGroupUplinkPorts->
                                      pu1_OctetList, pUplinkPorts,
                                      pDownlinkPorts);

    /* Check if there is any downlink ports set */
    if (CfaUfdCountPortlistSetBits (*pDownlinkPorts) != 0)
    {
        FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
        FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
        /* This means some downlink ports are set */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
    FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);

    /* Utility to check portlist */
    if (CfaUfdValidateAddPortList (pTestValIfUfdGroupUplinkPorts->pu1_OctetList,
                                   (UINT4) i4IfUfdGroupId) != CFA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfUfdGroupDownlinkPorts
 Input       :  The Indices
                IfUfdGroupId

                The Object 
                testValIfUfdGroupDownlinkPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfUfdGroupDownlinkPorts (UINT4 *pu4ErrorCode, INT4 i4IfUfdGroupId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValIfUfdGroupDownlinkPorts)
{
    tPortList          *pUplinkPorts = NULL;
    tPortList          *pDownlinkPorts = NULL;

    /* Check if the port role is uplink */
    pUplinkPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pUplinkPorts == NULL)
    {
        return SNMP_FAILURE;
    }

    pDownlinkPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pDownlinkPorts == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
        return SNMP_FAILURE;
    }
    MEMSET (pUplinkPorts, 0, sizeof (tPortList));
    MEMSET (pDownlinkPorts, 0, sizeof (tPortList));

    CfaUfdSeperatePortlistOnPortRole (pTestValIfUfdGroupDownlinkPorts->
                                      pu1_OctetList, pUplinkPorts,
                                      pDownlinkPorts);

    /* Check if there is any uplink ports set */
    if (CfaUfdCountPortlistSetBits (*pUplinkPorts) != 0)
    {
        FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
        FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);
        /* This means some uplink ports are set */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    FsUtilReleaseBitList ((UINT1 *) pUplinkPorts);
    FsUtilReleaseBitList ((UINT1 *) pDownlinkPorts);

    /* Utility to check portlist */
    if (CfaUfdValidateAddPortList
        (pTestValIfUfdGroupDownlinkPorts->pu1_OctetList,
         (UINT4) i4IfUfdGroupId) != CFA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (CfaUfdValidateRemovePortList
        (pTestValIfUfdGroupDownlinkPorts->pu1_OctetList,
         (UINT4) i4IfUfdGroupId) != CFA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfUfdGroupDesigUplinkPort
 Input       :  The Indices
                IfUfdGroupId

                The Object 
                testValIfUfdGroupDesigUplinkPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfUfdGroupDesigUplinkPort (UINT4 *pu4ErrorCode, INT4 i4IfUfdGroupId,
                                    INT4 i4TestValIfUfdGroupDesigUplinkPort)
{
    INT4                i4RetValIfMainPortRole = 0;
    INT4                i4RetValIfMainUfdGroupId = 0;

    /* Check the port is an uplink port */
    if (nmhGetIfMainPortRole (i4TestValIfUfdGroupDesigUplinkPort,
                              &i4RetValIfMainPortRole) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4RetValIfMainPortRole != CFA_PORT_ROLE_UPLINK)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Check the port is an uplink port is in the group */
    if (nmhGetIfMainUfdGroupId (i4TestValIfUfdGroupDesigUplinkPort,
                                &i4RetValIfMainUfdGroupId) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (i4RetValIfMainUfdGroupId != i4IfUfdGroupId)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4IfUfdGroupId);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2IfUfdGroupTable
 Input       :  The Indices
                IfUfdGroupId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfUfdGroupTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfSplitHorizonSysControl
 Input       :  The Indices

                The Object
                retValIfSplitHorizonSysControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfSplitHorizonSysControl (INT4 *pi4RetValIfSplitHorizonSysControl)
{
    *pi4RetValIfSplitHorizonSysControl = (INT4) CFA_SH_SYSTEM_CONTROL;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfSplitHorizonModStatus
 Input       :  The Indices

                The Object
                retValIfSplitHorizonModStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfSplitHorizonModStatus (INT4 *pi4RetValIfSplitHorizonModStatus)
{
    *pi4RetValIfSplitHorizonModStatus = (INT4) CFA_SH_MODULE_STATUS;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfSplitHorizonSysControl
 Input       :  The Indices

                The Object
                setValIfSplitHorizonSysControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetIfSplitHorizonSysControl (INT4 i4SetValIfSplitHorizonSysControl)
{
    if (CFA_SH_SYSTEM_CONTROL != (UINT4) i4SetValIfSplitHorizonSysControl)
    {
        if (i4SetValIfSplitHorizonSysControl == CFA_SH_START)
        {
            CfaShStart ();
        }
        else
        {
            if (CfaShShutdown () != CFA_SUCCESS)
            {
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIfSplitHorizonModStatus
 Input       :  The Indices

                The Object
                setValIfSplitHorizonModStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetIfSplitHorizonModStatus (INT4 i4SetValIfSplitHorizonModStatus)
{
    if (CFA_SH_MODULE_STATUS != (UINT4) i4SetValIfSplitHorizonModStatus)
    {
        if (i4SetValIfSplitHorizonModStatus == CFA_SH_ENABLED)
        {
            if (CfaShEnable (i4SetValIfSplitHorizonModStatus) != CFA_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                         "Split-Horizon configuration Failed - while enabling\n");
                return SNMP_FAILURE;
            }
        }
        else
        {
            if (CfaShDisable (i4SetValIfSplitHorizonModStatus) != CFA_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                         "Split-Horizon configuration Failed - while disabling\n");
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfSplitHorizonSysControl
 Input       :  The Indices

                The Object
                testValIfSplitHorizonSysControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2IfSplitHorizonSysControl (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValIfSplitHorizonSysControl)
{
    if ((i4TestValIfSplitHorizonSysControl != CFA_SH_START) &&
        (i4TestValIfSplitHorizonSysControl != CFA_SH_SHUTDOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfSplitHorizonModStatus
 Input       :  The Indices

                The Object
                testValIfSplitHorizonModStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2IfSplitHorizonModStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValIfSplitHorizonModStatus)
{
    if ((CFA_SH_MODULE_STATUS == CFA_SH_ENABLED) &&
        (CFA_SH_SYSTEM_CONTROL != CFA_SH_START))
    {
        CLI_SET_ERR (CLI_CFA_SH_SYS_CONTROL_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValIfSplitHorizonModStatus != CFA_SH_ENABLED) &&
        (i4TestValIfSplitHorizonModStatus != CFA_SH_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2IfSplitHorizonSysControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhDepv2IfSplitHorizonSysControl (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2IfSplitHorizonModStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhDepv2IfSplitHorizonModStatus (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainExtLinkUpEnabledStatus
 Input       :  The Indices
                IfMainIndex

                The Object
                retValIfMainExtLinkUpEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainExtLinkUpEnabledStatus (INT4 i4IfMainIndex,
                                    INT4 *pi4RetValIfMainExtLinkUpEnabledStatus)
{
    UINT2               u2EnableStatus = CFA_LINKUP_DELAY_DISABLED;
    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    CfaGetIfLinkUpDelayStatus ((UINT4) i4IfMainIndex, &u2EnableStatus);
    *pi4RetValIfMainExtLinkUpEnabledStatus = (INT4) u2EnableStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainExtLinkUpDelayTimer
 Input       :  The Indices
                IfMainIndex

                The Object
                retValIfMainExtLinkUpDelayTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainExtLinkUpDelayTimer (INT4 i4IfMainIndex,
                                 UINT4 *pu4RetValIfMainExtLinkUpDelayTimer)
{
    UINT2               u2DelayTimer = CFA_LINKUP_DELAY_DEFAULT_TIMER;

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    CfaGetIfLinkUpDelayTimer ((UINT4) i4IfMainIndex, &u2DelayTimer);
    *pu4RetValIfMainExtLinkUpDelayTimer = (INT4) u2DelayTimer;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfMainExtLinkUpRemainingTime
 Input       :  The Indices
                IfMainIndex

                The Object
                retValIfMainExtLinkUpRemainingTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfMainExtLinkUpRemainingTime (INT4 i4IfMainIndex,
                                    UINT4
                                    *pu4RetValIfMainExtLinkUpRemainingTime)
{

    UINT2               u2RemainingTime = 0;
    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    CfaGetIfLinkUpDelayRemainingTime ((UINT4) i4IfMainIndex, &u2RemainingTime);
    *pu4RetValIfMainExtLinkUpRemainingTime = (INT4) u2RemainingTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfMainExtLinkUpEnabledStatus
 Input       :  The Indices
                IfMainIndex

                The Object
                setValIfMainExtLinkUpEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainExtLinkUpEnabledStatus (INT4 i4IfMainIndex,
                                    INT4 i4SetValIfMainExtLinkUpEnabledStatus)
{

    UINT2               u2EnableStatus = 0;

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (CfaGetIfLinkUpDelayStatus ((UINT4) i4IfMainIndex,
                                   &u2EnableStatus) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (u2EnableStatus == i4SetValIfMainExtLinkUpEnabledStatus)
    {
        return SNMP_SUCCESS;
    }
    if (CfaSetIfLinkUpDelayStatus ((UINT4) i4IfMainIndex,
                                   (UINT4) i4SetValIfMainExtLinkUpEnabledStatus)
        != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (i4SetValIfMainExtLinkUpEnabledStatus == CFA_LINKUP_DELAY_ENABLED)
    {
        if (CfaSetIfLinkUpDelayTimer ((UINT4) i4IfMainIndex,
                                      CFA_LINKUP_DELAY_DEFAULT_TIMER) !=
            CFA_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        CfaCleanUpLinkUpPortStatus ((UINT4) i4IfMainIndex);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfMainExtLinkUpDelayTimer
 Input       :  The Indices
                IfMainIndex

                The Object
                setValIfMainExtLinkUpDelayTimer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfMainExtLinkUpDelayTimer (INT4 i4IfMainIndex,
                                 UINT4 u4SetValIfMainExtLinkUpDelayTimer)
{

    UINT2               u2DelayTimer = 0;

    if (CfaValidateCfaIfIndex ((UINT4) i4IfMainIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (CfaGetIfLinkUpDelayTimer ((UINT4) i4IfMainIndex,
                                  &u2DelayTimer) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (u2DelayTimer == u4SetValIfMainExtLinkUpDelayTimer)
    {
        return SNMP_SUCCESS;
    }
    CfaLinkUpDelayTmrProcessRemainingTime ((UINT4) i4IfMainIndex,
                                           u4SetValIfMainExtLinkUpDelayTimer);
    if (CfaSetIfLinkUpDelayTimer
        ((UINT4) i4IfMainIndex,
         u4SetValIfMainExtLinkUpDelayTimer) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IfMainExtLinkUpEnabledStatus
 Input       :  The Indices
                IfMainIndex

                The Object
                testValIfMainExtLinkUpEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainExtLinkUpEnabledStatus (UINT4 *pu4ErrorCode,
                                       INT4 i4IfMainIndex,
                                       INT4
                                       i4TestValIfMainExtLinkUpEnabledStatus)
{

    UINT1               u1BridgedIfaceStatus = 0;
    UINT1               u1BrgPortType = 0;

    if (CfaValidateIfMainTableIndex (i4IfMainIndex) != CFA_SUCCESS)
    {
        CLI_SET_ERR (CLI_CFA_INVALID_INDEX);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    CfaGetIfBrgPortType ((UINT4) i4IfMainIndex, &u1BrgPortType);

    /* link-up cannot be enabled in s-channel */
    if (u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CFA_SBP_ERR);
        return SNMP_FAILURE;
    }

    if (((UINT4) i4IfMainIndex > CFA_PHYS_NUM ()) || (i4IfMainIndex <= 0))
    {
        if ((UINT4) i4IfMainIndex > CFA_PHYS_NUM ())
        {
            CLI_SET_ERR (CLI_CFA_LINKUP_PORT_CHANNEL_ERR);
        }
        else
        {
            CLI_SET_ERR (CLI_CFA_INVALID_INDEX);
        }
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    CfaGetIfBridgedIfaceStatus ((UINT4) i4IfMainIndex, &u1BridgedIfaceStatus);
    if (u1BridgedIfaceStatus == CFA_DISABLED)
    {
        CLI_SET_ERR (CLI_CFA_LINKUP_ROUTER_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValIfMainExtLinkUpEnabledStatus != CFA_LINKUP_DELAY_ENABLED) &&
        (i4TestValIfMainExtLinkUpEnabledStatus != CFA_LINKUP_DELAY_DISABLED))
    {
        CLI_SET_ERR (CLI_CFA_LINKUP_DELAY_SYSTEM_SET_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (CFA_LNKUP_SYSTEM_CONTROL () != CFA_LINKUP_DELAY_START)
    {
        CLI_SET_ERR (CLI_CFA_LINKUP_DELAY_SYSTEM_STATUS_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfMainExtLinkUpDelayTimer
 Input       :  The Indices
                IfMainIndex

                The Object
                testValIfMainExtLinkUpDelayTimer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfMainExtLinkUpDelayTimer (UINT4 *pu4ErrorCode,
                                    INT4 i4IfMainIndex,
                                    UINT4 u4TestValIfMainExtLinkUpDelayTimer)
{
    UINT2               u2EnableStatus = 0;

    if (CfaValidateIfMainTableIndex (i4IfMainIndex) != CFA_SUCCESS)
    {
        CLI_SET_ERR (CLI_CFA_INVALID_INDEX);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((u4TestValIfMainExtLinkUpDelayTimer == 0) ||
        (u4TestValIfMainExtLinkUpDelayTimer > CFA_LINKUP_DELAY_MAX_TIMER))
    {
        CLI_SET_ERR (CLI_CFA_LINKUP_DELAY_TIMER_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (CFA_LNKUP_SYSTEM_CONTROL () != CFA_LINKUP_DELAY_START)
    {
        CLI_SET_ERR (CLI_CFA_LINKUP_DELAY_SYSTEM_STATUS_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    CfaGetIfLinkUpDelayStatus ((UINT4) i4IfMainIndex, &u2EnableStatus);
    if (u2EnableStatus != CFA_LINKUP_DELAY_ENABLED)
    {
        CLI_SET_ERR (CLI_CFA_LINKUP_DELAY_PORT_STATUS_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIfLinkUpEnabledStatus
 Input       :  The Indices

                The Object
                retValIfLinkUpEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfLinkUpEnabledStatus (INT4 *pi4RetValIfLinkUpEnabledStatus)
{
    *pi4RetValIfLinkUpEnabledStatus = (INT4) CFA_LNKUP_SYSTEM_CONTROL ();

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIfLinkUpEnabledStatus
 Input       :  The Indices

                The Object
                setValIfLinkUpEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfLinkUpEnabledStatus (INT4 i4SetValIfLinkUpEnabledStatus)
{
    /* If the value is same no need to set it again */
    if (CFA_LNKUP_SYSTEM_CONTROL () != (UINT4) i4SetValIfLinkUpEnabledStatus)
    {
        CFA_LNKUP_SYSTEM_CONTROL () = i4SetValIfLinkUpEnabledStatus;
    }
    if (i4SetValIfLinkUpEnabledStatus == CFA_LINKUP_DELAY_SHUTDOWN)
    {
        CfaCleanUpLinkUpModuleStatus (i4SetValIfLinkUpEnabledStatus);
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IfLinkUpEnabledStatus
 Input       :  The Indices

                The Object
                testValIfLinkUpEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfLinkUpEnabledStatus (UINT4 *pu4ErrorCode,
                                INT4 i4TestValIfLinkUpEnabledStatus)
{
    if ((i4TestValIfLinkUpEnabledStatus != CFA_LINKUP_DELAY_START) &&
        (i4TestValIfLinkUpEnabledStatus != CFA_LINKUP_DELAY_SHUTDOWN))
    {
        CLI_SET_ERR (CLI_CFA_LINKUP_DELAY_SYSTEM_SET_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IfLinkUpEnabledStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfLinkUpEnabledStatus (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfOOBNode0SecondaryIpAddress
 Input       :  The Indices

                The Object 
                retValIfOOBNode0SecondaryIpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfOOBNode0SecondaryIpAddress (UINT4
                                    *pu4RetValIfOOBNode0SecondaryIpAddress)
{
    *pu4RetValIfOOBNode0SecondaryIpAddress = gu4IfOOBNode0SecondaryIpAddress;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfOOBNode0SecondaryIpMask
 Input       :  The Indices

                The Object 
                retValIfOOBNode0SecondaryIpMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfOOBNode0SecondaryIpMask (UINT4 *pu4RetValIfOOBNode0SecondaryIpMask)
{
    *pu4RetValIfOOBNode0SecondaryIpMask = gu4IfOOBNode0SecondaryIpMask;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfOOBNode1SecondaryIpAddress
 Input       :  The Indices

                The Object 
                retValIfOOBNode1SecondaryIpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfOOBNode1SecondaryIpAddress (UINT4
                                    *pu4RetValIfOOBNode1SecondaryIpAddress)
{
    *pu4RetValIfOOBNode1SecondaryIpAddress = gu4IfOOBNode1SecondaryIpAddress;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIfOOBNode1SecondaryIpMask
 Input       :  The Indices

                The Object 
                retValIfOOBNode1SecondaryIpMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIfOOBNode1SecondaryIpMask (UINT4 *pu4RetValIfOOBNode1SecondaryIpMask)
{
    *pu4RetValIfOOBNode1SecondaryIpMask = gu4IfOOBNode1SecondaryIpMask;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfOOBNode0SecondaryIpAddress
 Input       :  The Indices

                The Object 
                setValIfOOBNode0SecondaryIpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfOOBNode0SecondaryIpAddress (UINT4 u4SetValIfOOBNode0SecondaryIpAddress)
{
    if (IssGetSwitchid () == CFA_NODE0)
    {
        if (CfaIfSetOOBLocalNodeSecondaryIpAddress
            (u4SetValIfOOBNode0SecondaryIpAddress, 0) == CFA_FAILURE)
        {
            return SNMP_FAILURE;
        }
        /*Set the Secondary OOB IP Address to the NodeId File */
        IssSetSecIpAddrForNodeId (u4SetValIfOOBNode0SecondaryIpAddress);
    }

    /* Update the global variable */
    gu4IfOOBNode0SecondaryIpAddress = u4SetValIfOOBNode0SecondaryIpAddress;

    if (u4SetValIfOOBNode0SecondaryIpAddress == 0)
    {
        /* Deletion of row. Reset Ip Mask too */
        gu4IfOOBNode0SecondaryIpMask = 0;
    }
    else
    {

        /* Incase the user didnt configure the mask before setting the ip 
         * address assign default mask */
        if (gu4IfOOBNode0SecondaryIpMask == 0)
        {
            CfaGetDefaultNetMask (gu4IfOOBNode0SecondaryIpAddress,
                                  &gu4IfOOBNode0SecondaryIpMask);
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfOOBNode0SecondaryIpMask
 Input       :  The Indices

                The Object 
                setValIfOOBNode0SecondaryIpMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfOOBNode0SecondaryIpMask (UINT4 u4SetValIfOOBNode0SecondaryIpMask)
{
    /* Check whether IP address is assigned, if not dont allow Mask  */
    if (gu4IfOOBNode0SecondaryIpAddress == 0)
    {
        return SNMP_FAILURE;
    }

    if (IssGetSwitchid () == CFA_NODE0)
    {
        if (CfaIfSetOOBLocalNodeSecondaryIpAddress
            (gu4IfOOBNode0SecondaryIpAddress,
             u4SetValIfOOBNode0SecondaryIpMask) == CFA_FAILURE)
        {
            return SNMP_FAILURE;
        }
        /* Set the Secondary OOB IP Mask to the NodeId File */
        IssSetSecSubnetMaskForNodeId (u4SetValIfOOBNode0SecondaryIpMask);
    }

    /* Update the global variable */
    gu4IfOOBNode0SecondaryIpMask = u4SetValIfOOBNode0SecondaryIpMask;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfOOBNode1SecondaryIpAddress
 Input       :  The Indices

                The Object 
                setValIfOOBNode1SecondaryIpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfOOBNode1SecondaryIpAddress (UINT4 u4SetValIfOOBNode1SecondaryIpAddress)
{
    if (IssGetSwitchid () == CFA_NODE1)
    {
        if (CfaIfSetOOBLocalNodeSecondaryIpAddress
            (u4SetValIfOOBNode1SecondaryIpAddress, 0) == CFA_FAILURE)
        {
            return SNMP_FAILURE;
        }
        /* Set the Secondary OOB IP Address to the NodeId File */
        IssSetSecIpAddrForNodeId (u4SetValIfOOBNode1SecondaryIpAddress);
    }

    /* Update the global variable */
    gu4IfOOBNode1SecondaryIpAddress = u4SetValIfOOBNode1SecondaryIpAddress;

    if (u4SetValIfOOBNode1SecondaryIpAddress == 0)
    {
        /* Deletion of row. Reset Ip Mask too */
        gu4IfOOBNode1SecondaryIpMask = 0;
    }
    else
    {

        /* Incase the user didnt configure the mask before setting the ip 
         * address assign default mask */
        if (gu4IfOOBNode1SecondaryIpMask == 0)
        {
            CfaGetDefaultNetMask (gu4IfOOBNode1SecondaryIpAddress,
                                  &gu4IfOOBNode1SecondaryIpMask);
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIfOOBNode1SecondaryIpMask
 Input       :  The Indices

                The Object 
                setValIfOOBNode1SecondaryIpMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIfOOBNode1SecondaryIpMask (UINT4 u4SetValIfOOBNode1SecondaryIpMask)
{
    /* Check whether IP address is assigned, if not dont allow Mask  */
    if (gu4IfOOBNode1SecondaryIpAddress == 0)
    {
        return SNMP_FAILURE;
    }

    if (IssGetSwitchid () == CFA_NODE1)
    {
        if (CfaIfSetOOBLocalNodeSecondaryIpAddress
            (gu4IfOOBNode1SecondaryIpAddress,
             u4SetValIfOOBNode1SecondaryIpMask) == CFA_FAILURE)
        {
            return SNMP_FAILURE;
        }
        /* Set the Secondary OOB IP Mask to the NodeId File */
        IssSetSecSubnetMaskForNodeId (u4SetValIfOOBNode1SecondaryIpMask);
    }

    /* Update the global variable */
    gu4IfOOBNode1SecondaryIpMask = u4SetValIfOOBNode1SecondaryIpMask;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfOOBNode0SecondaryIpAddress
 Input       :  The Indices

                The Object 
                testValIfOOBNode0SecondaryIpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfOOBNode0SecondaryIpAddress (UINT4 *pu4ErrorCode,
                                       UINT4
                                       u4TestValIfOOBNode0SecondaryIpAddress)
{
    UINT4               u4IfOOBIpAddress = 0;
    /* Check if the Secondary Ip belongs to valid class of ip addresses.
     * If it is set as zero, the secondary ip address row is to be deleted.*/
    if (u4TestValIfOOBNode0SecondaryIpAddress != 0)
    {
        nmhGetIfOOBNode1SecondaryIpAddress (&u4IfOOBIpAddress);
        /* validate Node0 should not have the same IP address of Node1 */
        if (u4IfOOBIpAddress == u4TestValIfOOBNode0SecondaryIpAddress)
        {
            CLI_SET_ERR (CFA_CLI_SAME_IP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        /* Validate Primary should not have the same IP address */
        if (CfaIfGetIpAddress (CFA_OOB_MGMT_IFINDEX, &u4IfOOBIpAddress) ==
            CFA_SUCCESS)
        {
            if (u4IfOOBIpAddress == u4TestValIfOOBNode0SecondaryIpAddress)
            {
                CLI_SET_ERR (CFA_CLI_SAME_IP_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
        if (!(CFA_IS_ADDR_CLASS_A (u4TestValIfOOBNode0SecondaryIpAddress) ||
              (CFA_IS_ADDR_CLASS_B (u4TestValIfOOBNode0SecondaryIpAddress)) ||
              (CFA_IS_ADDR_CLASS_C (u4TestValIfOOBNode0SecondaryIpAddress))))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfOOBNode0SecondaryIpMask
 Input       :  The Indices

                The Object 
                testValIfOOBNode0SecondaryIpMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfOOBNode0SecondaryIpMask (UINT4 *pu4ErrorCode,
                                    UINT4 u4TestValIfOOBNode0SecondaryIpMask)
{
    UINT1               u1Counter = 0;

    /* Ip mask should not be zero */
    if (u4TestValIfOOBNode0SecondaryIpMask == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* The valid subnet address is from 0 to 32 */
    for (u1Counter = 0; u1Counter <= CFA_MAX_CIDR; ++u1Counter)
    {
        if (u4CidrSubnetMask[u1Counter] == u4TestValIfOOBNode0SecondaryIpMask)
        {
            break;
        }
    }
    if (u1Counter > CFA_MAX_CIDR)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfOOBNode1SecondaryIpAddress
 Input       :  The Indices

                The Object 
                testValIfOOBNode1SecondaryIpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfOOBNode1SecondaryIpAddress (UINT4 *pu4ErrorCode,
                                       UINT4
                                       u4TestValIfOOBNode1SecondaryIpAddress)
{
    /* Check if the Secondary Ip belongs to valid class of ip addresses.
     * If it is set as zero, the secondary ip address row is to be deleted.*/

    UINT4               u4IfOOBIpAddress = 0;

    if (u4TestValIfOOBNode1SecondaryIpAddress != 0)
    {
        nmhGetIfOOBNode0SecondaryIpAddress (&u4IfOOBIpAddress);
        /* validate Node0 should not have the same IP address of Node1 */
        if (u4IfOOBIpAddress == u4TestValIfOOBNode1SecondaryIpAddress)
        {
            CLI_SET_ERR (CFA_CLI_SAME_IP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        /* Validate Primary should not have the same IP address */
        if (CfaIfGetIpAddress (CFA_OOB_MGMT_IFINDEX, &u4IfOOBIpAddress) ==
            CFA_SUCCESS)
        {
            if (u4IfOOBIpAddress == u4TestValIfOOBNode1SecondaryIpAddress)
            {
                CLI_SET_ERR (CFA_CLI_SAME_IP_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
        if (!(CFA_IS_ADDR_CLASS_A (u4TestValIfOOBNode1SecondaryIpAddress) ||
              (CFA_IS_ADDR_CLASS_B (u4TestValIfOOBNode1SecondaryIpAddress)) ||
              (CFA_IS_ADDR_CLASS_C (u4TestValIfOOBNode1SecondaryIpAddress))))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IfOOBNode1SecondaryIpMask
 Input       :  The Indices

                The Object 
                testValIfOOBNode1SecondaryIpMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IfOOBNode1SecondaryIpMask (UINT4 *pu4ErrorCode,
                                    UINT4 u4TestValIfOOBNode1SecondaryIpMask)
{
    UINT1               u1Counter = 0;

    /* Ip mask should not be zero */
    if (u4TestValIfOOBNode1SecondaryIpMask == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* The valid subnet address is from 0 to 32 */
    for (u1Counter = 0; u1Counter <= CFA_MAX_CIDR; ++u1Counter)
    {
        if (u4CidrSubnetMask[u1Counter] == u4TestValIfOOBNode1SecondaryIpMask)
        {
            break;
        }
    }
    if (u1Counter > CFA_MAX_CIDR)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2IfOOBNode0SecondaryIpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfOOBNode0SecondaryIpAddress (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2IfOOBNode0SecondaryIpMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfOOBNode0SecondaryIpMask (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2IfOOBNode1SecondaryIpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfOOBNode1SecondaryIpAddress (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2IfOOBNode1SecondaryIpMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IfOOBNode1SecondaryIpMask (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
