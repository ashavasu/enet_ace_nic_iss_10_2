/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: iwfmain.c,v 1.190 2018/01/08 12:28:33 siva Exp $
 *
 * Description:This file contains the routines for the     
 *             Inter-Working Functionality Module of the CFA.  
 *             These routines are mostly used in the data path 
 *             for encapsulation/de-encapsulation of packets   
 *             over the interfaces. This file contains     
 *             routines for the Enet interfaces.        
 *
 *******************************************************************/
#include "cfainc.h"
#include "lnxip.h"
#ifdef PPP_WANTED
#include "pppoe.h"
#endif /* PPP_WANTED */
#ifdef OPENFLOW_WANTED
#include "ofcl.h"
#endif /* OPENFLOW_WANTED */

extern INT4         arp_extract_hdr (t_ARP_PKT *, tCRU_BUF_CHAIN_HEADER *);

/*****************************************************************************
 *
 *    Function Name        : CfaIwfEnetInit
 *
 *    Description        : This function performs the initialisation of
 *                the Enet IWF structures for all Enet
 *                interfaces. The pre-formed Enet headers
 *                are formed for IP, ARP and RARP for EnetV2
 *                encapsulation. Currently, Enet SNAP
 *                encapsulation packets are not transmitted.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gIfGlobal (Interface's global struct),
 *                IfTable (gapIfTable) IWF structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) IWF structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
INT4
CfaIwfEnetInit (UINT4 u4IfIndex)
{
    UINT1               au1IfHwAddr[CFA_ENET_ADDR_LEN];
    UINT2               u2EtherType;

    CFA_DBG (CFA_TRC_ALL, CFA_IWF, "Entering CfaIwfEnetInit \n");

/* first we initialise the Enet IWF struct for the Enet interfaces. it is
expected that these structs are allocated first during the Init of the
IfEntry for Enet. */
    CfaGetIfHwAddr (u4IfIndex, au1IfHwAddr);

/* formation of IP Enet & SNAP Frame */
    MEMCPY (CFA_ENET_SRCADDR (&CFA_ENET_IWF_V2_IPHDR (u4IfIndex)),
            au1IfHwAddr, CFA_ENET_ADDR_LEN);

    u2EtherType = CFA_ENET_IPV4;
    CFA_ENET_V2_ETYPE (&CFA_ENET_IWF_V2_IPHDR (u4IfIndex)) =
        OSIX_HTONS (u2EtherType);
    CfaGetIfHwAddr (u4IfIndex, au1IfHwAddr);
    MEMCPY (CFA_ENET_SRCADDR (&CFA_ENET_IWF_SNAP_IPHDR (u4IfIndex)),
            au1IfHwAddr, CFA_ENET_ADDR_LEN);

    CFA_ENET_SNAP_ETYPE (&CFA_ENET_IWF_SNAP_IPHDR (u4IfIndex)) =
        OSIX_HTONS (u2EtherType);
    CFA_ENET_SNAP_DSAP (&CFA_ENET_IWF_SNAP_IPHDR (u4IfIndex)) =
        CFA_LLC_SNAP_SAP;
    CFA_ENET_SNAP_SSAP (&CFA_ENET_IWF_SNAP_IPHDR (u4IfIndex)) =
        CFA_LLC_SNAP_SAP;
    CFA_ENET_SNAP_CNTRL (&CFA_ENET_IWF_SNAP_IPHDR (u4IfIndex)) =
        CFA_LLC_CONTROL_UI;
    CFA_ENET_SNAP_OUI1 (&CFA_ENET_IWF_SNAP_IPHDR (u4IfIndex)) = 0x00;
    CFA_ENET_SNAP_OUI2 (&CFA_ENET_IWF_SNAP_IPHDR (u4IfIndex)) = 0x00;
    CFA_ENET_SNAP_OUI3 (&CFA_ENET_IWF_SNAP_IPHDR (u4IfIndex)) = 0x00;

#ifdef IP6_WANTED
    CfaGetIfHwAddr (u4IfIndex, au1IfHwAddr);
    MEMCPY (CFA_ENET_SRCADDR (&CFA_ENET_IWF_V2_IP6HDR (u4IfIndex)),
            au1IfHwAddr, CFA_ENET_ADDR_LEN);

    u2EtherType = CFA_ENET_IPV6;
    CFA_ENET_V2_ETYPE (&CFA_ENET_IWF_V2_IP6HDR (u4IfIndex)) =
        OSIX_HTONS (u2EtherType);

#endif /* IP6_WANTED */

/* formation of ARP Enet & SNAP Frame */
    CfaGetIfHwAddr (u4IfIndex, au1IfHwAddr);
    MEMCPY (CFA_ENET_SRCADDR (&CFA_ENET_IWF_V2_ARPHDR (u4IfIndex)),
            au1IfHwAddr, CFA_ENET_ADDR_LEN);

    u2EtherType = CFA_ENET_ARP;
    CFA_ENET_V2_ETYPE (&CFA_ENET_IWF_V2_ARPHDR (u4IfIndex)) =
        OSIX_HTONS (u2EtherType);
    CfaGetIfHwAddr (u4IfIndex, au1IfHwAddr);
    MEMCPY (CFA_ENET_SRCADDR (&CFA_ENET_IWF_SNAP_ARPHDR (u4IfIndex)),
            au1IfHwAddr, CFA_ENET_ADDR_LEN);

    CFA_ENET_SNAP_ETYPE (&CFA_ENET_IWF_SNAP_ARPHDR (u4IfIndex)) =
        OSIX_HTONS (u2EtherType);
    CFA_ENET_SNAP_DSAP (&CFA_ENET_IWF_SNAP_ARPHDR (u4IfIndex)) =
        CFA_LLC_SNAP_SAP;
    CFA_ENET_SNAP_SSAP (&CFA_ENET_IWF_SNAP_ARPHDR (u4IfIndex)) =
        CFA_LLC_SNAP_SAP;
    CFA_ENET_SNAP_CNTRL (&CFA_ENET_IWF_SNAP_ARPHDR (u4IfIndex)) =
        CFA_LLC_CONTROL_UI;
    CFA_ENET_SNAP_OUI1 (&CFA_ENET_IWF_SNAP_ARPHDR (u4IfIndex)) = 0x00;
    CFA_ENET_SNAP_OUI2 (&CFA_ENET_IWF_SNAP_ARPHDR (u4IfIndex)) = 0x00;
    CFA_ENET_SNAP_OUI3 (&CFA_ENET_IWF_SNAP_ARPHDR (u4IfIndex)) = 0x00;

/* formation of RARP Enet & SNAP Frame */
    CfaGetIfHwAddr (u4IfIndex, au1IfHwAddr);
    MEMCPY (CFA_ENET_SRCADDR (&CFA_ENET_IWF_V2_RARPHDR (u4IfIndex)),
            au1IfHwAddr, CFA_ENET_ADDR_LEN);

    u2EtherType = CFA_ENET_RARP;
    CFA_ENET_V2_ETYPE (&CFA_ENET_IWF_V2_RARPHDR (u4IfIndex)) =
        OSIX_HTONS (u2EtherType);
    CfaGetIfHwAddr (u4IfIndex, au1IfHwAddr);
    MEMCPY (CFA_ENET_SRCADDR (&CFA_ENET_IWF_SNAP_RARPHDR (u4IfIndex)),
            au1IfHwAddr, CFA_ENET_ADDR_LEN);

    CFA_ENET_SNAP_ETYPE (&CFA_ENET_IWF_SNAP_RARPHDR (u4IfIndex)) =
        OSIX_HTONS (u2EtherType);
    CFA_ENET_SNAP_DSAP (&CFA_ENET_IWF_SNAP_RARPHDR (u4IfIndex)) =
        CFA_LLC_SNAP_SAP;
    CFA_ENET_SNAP_SSAP (&CFA_ENET_IWF_SNAP_RARPHDR (u4IfIndex)) =
        CFA_LLC_SNAP_SAP;
    CFA_ENET_SNAP_CNTRL (&CFA_ENET_IWF_SNAP_RARPHDR (u4IfIndex)) =
        CFA_LLC_CONTROL_UI;
    CFA_ENET_SNAP_OUI1 (&CFA_ENET_IWF_SNAP_RARPHDR (u4IfIndex)) = 0x00;
    CFA_ENET_SNAP_OUI2 (&CFA_ENET_IWF_SNAP_RARPHDR (u4IfIndex)) = 0x00;
    CFA_ENET_SNAP_OUI3 (&CFA_ENET_IWF_SNAP_RARPHDR (u4IfIndex)) = 0x00;

/* formation of BPDU pre-formed header */
    CfaGetIfHwAddr (u4IfIndex, au1IfHwAddr);
    MEMCPY (CFA_ENET_SRCADDR (&CFA_ENET_IWF_LLC_BPDUHDR (u4IfIndex)),
            au1IfHwAddr, CFA_ENET_ADDR_LEN);

    CFA_ENET_LLC_DSAP (&CFA_ENET_IWF_LLC_BPDUHDR (u4IfIndex)) =
        CFA_LLC_STAP_SAP;
    CFA_ENET_LLC_SSAP (&CFA_ENET_IWF_LLC_BPDUHDR (u4IfIndex)) =
        CFA_LLC_STAP_SAP;
    CFA_ENET_LLC_CNTRL (&CFA_ENET_IWF_LLC_BPDUHDR (u4IfIndex)) =
        CFA_LLC_CONTROL_UI;

#ifdef ISIS_WANTED
/* formation of OSI pre-formed header */
    CfaGetIfHwAddr ((UINT4) u4IfIndex, au1IfHwAddr);
    MEMCPY (CFA_ENET_OSI_SRCADDR (&CFA_ENET_IWF_LLC_OSIHDR (u4IfIndex)),
            au1IfHwAddr, CFA_ENET_ADDR_LEN);
    CFA_ENET_LLC_DSAP (&CFA_ENET_IWF_LLC_OSIHDR (u4IfIndex)) =
        CFA_LLC_ROUTED_SAP;
    CFA_ENET_LLC_SSAP (&CFA_ENET_IWF_LLC_OSIHDR (u4IfIndex)) =
        CFA_LLC_ROUTED_SAP;
    CFA_ENET_LLC_CNTRL (&CFA_ENET_IWF_LLC_OSIHDR (u4IfIndex)) =
        CFA_LLC_CONTROL_UI;
#endif

    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_IWF,
             "Success in CfaIwfEnetInit - Enet IWF Data Structure Ready.\n");

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaIwfEnetShutdown
 *
 *    Description        : This function performs the shutdown of
 *                the Enet IWF Module of CFA. This
 *                routine should be called during the shutdown
 *                of CFA. Currently it is a DUMMY - kept for
 *                future use.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
VOID
CfaIwfEnetShutdown (VOID)
{

/* Do nothing  */
    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_IWF, "In CfaIwfEnetShutdown.\n");

}

/*************************************************************************
 *    Function Name        : CfaIwfPswInit
 *
 *    Description          : This function performs the initialisation of
 *                           the PSW IWF structures for all pseudo wire
 *                           interfaces.
 *       
 *    INPUT                : u4IfIndex
 *    OUTPUT               : None.
 *    Return Value         : CFA_SUCCESS
 *
 ************************************************************************/

INT4
CfaIwfPswInit (UINT4 u4IfIndex)
{
    UINT1               au1IfHwAddr[CFA_ENET_ADDR_LEN];
    UINT2               u2EtherType = 0;

    CFA_DBG (CFA_TRC_ALL, CFA_IWF, "Entering CfaIwfPswInit \n");

    MEMSET (au1IfHwAddr, 0, sizeof (CFA_ENET_ADDR_LEN));

    /* first we initialise the Psw IWF struct for the Psw interfaces. it is
     * expected that these structs are allocated first during the Init of the
     * IfEntry for Enet. */

    CfaGetIfHwAddr (u4IfIndex, au1IfHwAddr);

    /* formation of BPDU pre-formed header */
    MEMCPY (CFA_ENET_SRCADDR (&CFA_ENET_IWF_LLC_BPDUHDR (u4IfIndex)),
            au1IfHwAddr, CFA_ENET_ADDR_LEN);

    u2EtherType = CFA_ENET_IPV4;
    CFA_ENET_V2_ETYPE (&CFA_ENET_IWF_V2_IPHDR (u4IfIndex)) =
        OSIX_HTONS (u2EtherType);
    CfaGetIfHwAddr (u4IfIndex, au1IfHwAddr);
    MEMCPY (CFA_ENET_SRCADDR (&CFA_ENET_IWF_SNAP_IPHDR (u4IfIndex)),
            au1IfHwAddr, CFA_ENET_ADDR_LEN);

    CFA_ENET_LLC_DSAP (&CFA_ENET_IWF_LLC_BPDUHDR (u4IfIndex)) =
        CFA_LLC_STAP_SAP;
    CFA_ENET_LLC_SSAP (&CFA_ENET_IWF_LLC_BPDUHDR (u4IfIndex)) =
        CFA_LLC_STAP_SAP;
    CFA_ENET_LLC_CNTRL (&CFA_ENET_IWF_LLC_BPDUHDR (u4IfIndex)) =
        CFA_LLC_CONTROL_UI;

#ifdef IP6_WANTED
    MEMCPY (CFA_ENET_SRCADDR (&CFA_ENET_IWF_V2_IP6HDR (u4IfIndex)),
            au1IfHwAddr, CFA_ENET_ADDR_LEN);

    u2EtherType = CFA_ENET_IPV6;
    CFA_ENET_V2_ETYPE (&CFA_ENET_IWF_V2_IP6HDR (u4IfIndex)) =
        OSIX_HTONS (u2EtherType);

#endif /* IP6_WANTED */

    MEMCPY (CFA_ENET_SRCADDR (&CFA_ENET_IWF_V2_ARPHDR (u4IfIndex)),
            au1IfHwAddr, CFA_ENET_ADDR_LEN);

    u2EtherType = CFA_ENET_ARP;
    CFA_ENET_V2_ETYPE (&CFA_ENET_IWF_V2_ARPHDR (u4IfIndex)) =
        OSIX_HTONS (u2EtherType);
    MEMCPY (CFA_ENET_SRCADDR (&CFA_ENET_IWF_SNAP_ARPHDR (u4IfIndex)),
            au1IfHwAddr, CFA_ENET_ADDR_LEN);

#ifdef ISIS_WANTED
    /* formation of OSI pre-formed header */
    CfaGetIfHwAddr ((UINT4) u4IfIndex, au1IfHwAddr);
    MEMCPY (CFA_ENET_OSI_SRCADDR (&CFA_ENET_IWF_LLC_OSIHDR (u4IfIndex)),
            au1IfHwAddr, CFA_ENET_ADDR_LEN);
    CFA_ENET_LLC_DSAP (&CFA_ENET_IWF_LLC_OSIHDR (u4IfIndex)) =
        CFA_LLC_ROUTED_SAP;
    CFA_ENET_LLC_SSAP (&CFA_ENET_IWF_LLC_OSIHDR (u4IfIndex)) =
        CFA_LLC_ROUTED_SAP;
    CFA_ENET_LLC_CNTRL (&CFA_ENET_IWF_LLC_OSIHDR (u4IfIndex)) =
        CFA_LLC_CONTROL_UI;
#endif

    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_IWF,
             "Success in CfaIwfPswInit - Psw IWF Data Structure Ready.\n");

    return CFA_SUCCESS;
}

/*************************************************************************
 *  Function Name        : CfaIwfACInit
 *
 *  Description          : This function performs the initialisation of
 *                          the AC IWF structures for all pseudo wire
 *                           interfaces.
 *
 *  INPUT                : u4IfIndex
 *  OUTPUT               : None.
 *  Return Value         : CFA_SUCCESS
 *    
 *************************************************************************/
INT4
CfaIwfACInit (UINT4 u4IfIndex)
{
    UINT1               au1IfHwAddr[CFA_ENET_ADDR_LEN];

    CFA_DBG (CFA_TRC_ALL, CFA_IWF, "Entering CfaIwfPswInit \n");

    MEMSET (au1IfHwAddr, 0, sizeof (CFA_ENET_ADDR_LEN));

    /* first we initialise the Psw IWF struct for the Psw interfaces. it is
     *      * expected that these structs are allocated first during the Init of the
     *           * IfEntry for Enet. */

    CfaGetIfHwAddr (u4IfIndex, au1IfHwAddr);

    /* formation of BPDU pre-formed header */
    MEMCPY (CFA_ENET_SRCADDR (&CFA_ENET_IWF_LLC_BPDUHDR (u4IfIndex)),
            au1IfHwAddr, CFA_ENET_ADDR_LEN);

    CFA_ENET_LLC_DSAP (&CFA_ENET_IWF_LLC_BPDUHDR (u4IfIndex)) =
        CFA_LLC_STAP_SAP;
    CFA_ENET_LLC_SSAP (&CFA_ENET_IWF_LLC_BPDUHDR (u4IfIndex)) =
        CFA_LLC_STAP_SAP;
    CFA_ENET_LLC_CNTRL (&CFA_ENET_IWF_LLC_BPDUHDR (u4IfIndex)) =
        CFA_LLC_CONTROL_UI;

#ifdef ISIS_WANTED
    /* formation of OSI pre-formed header */
    CfaGetIfHwAddr ((UINT4) u4IfIndex, au1IfHwAddr);
    MEMCPY (CFA_ENET_OSI_SRCADDR (&CFA_ENET_IWF_LLC_OSIHDR (u4IfIndex)),
            au1IfHwAddr, CFA_ENET_ADDR_LEN);
    CFA_ENET_LLC_DSAP (&CFA_ENET_IWF_LLC_OSIHDR (u4IfIndex)) =
        CFA_LLC_ROUTED_SAP;
    CFA_ENET_LLC_SSAP (&CFA_ENET_IWF_LLC_OSIHDR (u4IfIndex)) =
        CFA_LLC_ROUTED_SAP;
    CFA_ENET_LLC_CNTRL (&CFA_ENET_IWF_LLC_OSIHDR (u4IfIndex)) =
        CFA_LLC_CONTROL_UI;
#endif
    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_IWF,
             "Success in CfaIwfPswInit - Psw IWF Data Structure Ready.\n");

    return CFA_SUCCESS;
}

/******************************************************************************
 *    Function Name        : CfaIwfPswShutdown
 *
 *    Description          : This function performs the shutdown of
 *                           the Psw IWF Module of CFA. This
 *                           routine should be called during the shutdown
 *                           of CFA. Currently it is a DUMMY - kept for
 *                           future use.
 *    
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *     Exceptions or Operating
 *     System Error Handling    : None.
 *
 *     Use of Recursion         : None.
 *
 *     Returns                  : None.
 ********************************************************************************/
VOID
CfaIwfPswShutdown (VOID)
{
    /* Do nothing  */
    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_IWF, "In CfaIwfPswShutdown.\n");
}

/*****************************************************************************
 *
 *    Function Name        : CfaIwfEnetProcessRxFrame
 *
 *    Description        : This function processes incoming frames on
 *                Enet interfaces. Both Enet V2 and Enet SNAP
 *                encapsulations are supported. The frame is
 *                send to the appropriate module/routine for
 *                processing.
 *
 *    Input(s)            : tCRU_BUF_CHAIN_HEADER *pBuf,
 *                UINT4 u4IfIndex,
 *                UINT4 u4PktSize.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if frame is processed
 *                succeessfully, otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIwfEnetProcessRxFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                          UINT4 u4PktSize, UINT2 u2Protocol,
                          UINT1 u1EnetEncapType)
{
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
    tEnetSnapHeader    *pSnapEthHdr;
    tCfaBackPlaneParams BackPlaneParams;
    tPktHandleInfo      PktHandleInfo;
    tEnetV2Header      *pEthHdr;
    tSecIntfInfo        SecIntfInfo;
#if defined (WLC_WANTED) || defined (WTP_WANTED)
#ifdef LNXIP4_WANTED
#ifdef NAT_WANTED
    t_ARP_PKT           Arp_pkt;
    UINT4               u4IpAddress = 0;
    INT1                i1IsNatIpEntry = FALSE;
    INT1                i1Status = 0;
    INT1                i1NatPolicyStatus = 0;
#endif
#endif
#endif
#ifdef ISIS_WANTED
    UINT2               u2LlcHeader;
    UINT1               u1Protocol = 0;
#endif
    UINT4               u4VlanIfIndex = CFA_INVALID_INDEX;
#ifdef IP6_WANTED
    UINT2               u2LenOrType = 0;
#endif
    UINT1               u1LinkFrameType;
    UINT1               u1EnetHeaderSize = 0;
    INT4                i4RetValue;
#if defined (ARP_WANTED) && (defined (IP_WANTED) || defined (LNXIP4_WANTED))
    tArpQMsg            ArpQMsg;
#endif
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
#ifdef UNIQUE_VLAN_MAC_WANTED
    tCRU_BUF_CHAIN_HEADER *pDupBuffer = NULL;
#endif
    tVlanIfaceVlanId    VlanId = 0;
#ifdef VRRP_WANTED
    INT4                i4OperVrId = 0;
    tIpConfigInfo       IpInfInfo;
#endif
    UINT1               u1BridgedIfaceStatus = CFA_DISABLED;
    tCfaRegInfo         CfaInfo;
#ifdef ISS_WANTED
#ifndef KERNEL_WANTED
    UINT1               u1L3Proto = 0;
    UINT2               u2L4DstPort = 0;
    UINT4               u4SrcIp = 0;
    BOOL1               bOOBFlag = OSIX_FALSE;
#endif /*KERNEL_WANTED */
#endif /*ISS_WANTED */
#ifdef MPLS_WANTED
    UINT4               u4MplsIfIndex = 0;
#endif
    UINT2               u2PortAuthMode = PNAC_PORT_AUTHMODE_PORTBASED;
    UINT2               u2PortAuthControl = PNAC_PORTCNTRL_FORCEAUTHORIZED;
    UINT1               u1PortPnacStatus = OSIX_ENABLED;
    UINT1               u1IfEtherType;
#if ((defined (RMON2_WANTED)) && (defined (NPAPI_WANTED)))
    UINT1              *pData;
    tPktHdrInfo         PktHdrInfo;
    tMacAddr            SwitchMac;
#endif
    UINT4               u4LclIfIndex = u4IfIndex;    /* Added this local 
                                                       variable to avoid the 
                                                       overwriting using u2AggId
                                                       while checking for Ports */

    UINT2               u2AggId = 0;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT2               u2VlanProt = 0;
    UINT2               u2VlanId = 0;
    UINT1               u1FrameType = BRG_DATA_FRAME;
    UINT1               u1IfType = 0;
    BOOL1               bSecVerdict = 0;
    UINT1               au1OobMac[ETHERNET_ADDR_SIZE];
    BOOL1               bProcessFlag = CFA_TRUE;
    BOOL1               bIsVrrpEnabled = FALSE;
    BOOL1               bRportFlag = FALSE;
    UINT1               u1NetworkType = 0;
    UINT1               pu1EoamTunnelStatus = 0;
#ifdef SYNCE_WANTED
    tCRU_BUF_CHAIN_HEADER *pDupCruBuf = NULL;
#endif
#ifdef BCMX_WANTED
    UINT1               u1InterfaceType = 0;
#endif
#ifdef HDLC_WANTED
    UINT4               u4HdlcIndex = 0;
#endif
#if ((defined LNXIP4_WANTED) && (defined IGMP_WANTED))
    tIpParms           *pIpParms;
    t_IP_HEADER        *pIpHdr = NULL;
    t_IP_HEADER         TmpIpHdr;
    UINT4               u4IfIpPortNum = 0;
    tCRU_INTERFACE      IfId;
    UINT1               u1Offset = 0;

    MEMSET (&TmpIpHdr, 0, sizeof (t_IP_HEADER));
    MEMSET (&IfId, 0, sizeof (tCRU_INTERFACE));
#endif

    bIsVrrpEnabled = FALSE;
    if (u4IfIndex >= CFA_MIN_TAP_IF_INDEX)
        CFA_DBG1 (CFA_TRC_ALL, CFA_IWF,
                  "Entering CfaIwfEnetProcessRxFrame for interface %d.\n",
                  u4IfIndex);
    CFA_DBG1 (CFA_TRC_ALL, CFA_IWF,
              "Flag to check VRRP enabling %d.\n", bIsVrrpEnabled);

    MEMSET (&CfaInfo, 0, sizeof (tCfaRegInfo));
    MEMSET (&BackPlaneParams, 0, sizeof (tCfaBackPlaneParams));
    MEMSET (au1OobMac, 0, ETHERNET_ADDR_SIZE);
    MEMSET (&PktHandleInfo, 0, sizeof (tPktHandleInfo));
    MEMSET (&SecIntfInfo, 0, sizeof (tSecIntfInfo));

    PktHandleInfo.u4ModuleId = CFA_GET_PROTOCOL (pBuf);

#ifdef HDLC_WANTED
    if (CFA_SUCCESS == CfaValidateHdlcPacket (pBuf, u4PktSize, &u4HdlcIndex))
    {
#ifdef PPP_WANTED
        if (CfaIwfPppHandleIncomingPkt (pBuf, u4HdlcIndex, u4PktSize,
                                        CFA_PPPOHDLC_FRAME, CFA_ENCAP_OTHER)
            != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                      "Unable to process the Incoming packet "
                      "over Interface %d...\r\n", u4IfIndex);
            return CFA_FAILURE;

        }
        return CFA_SUCCESS;
#endif
    }
#endif
    CfaGetIfNwType (u4IfIndex, &u1NetworkType);

    if (CfaValidateRxPacket (pBuf, u4IfIndex, u4PktSize, &PktHandleInfo,
                             &bProcessFlag) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                  "Unable to process the Incoming packet "
                  "over Interface %d...\r\n", u4IfIndex);
        return CFA_FAILURE;
    }

    /* Function to check whether destination MAC in packet matches
       with the OTHER protocol MAC address */
    i4RetVal = VlanCheckIsTunnelEnabledForOtherMac (u4IfIndex, pBuf);
    if (i4RetVal == VLAN_TUNNEL_PROTOCOL_TUNNEL)
    {
        /* Forward to l2iwf for tunneling */
        L2IwfWrHandleIncomingLayer2Pkt (pBuf, &PktHandleInfo, BRG_DATA_FRAME);
        return CFA_SUCCESS;
    }
    else if (i4RetVal == VLAN_TUNNEL_PROTOCOL_DISCARD)
    {
        /* Discard the packet */
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                  "Exiting CfaIwfEnetProcessRxFrame"
                  " on interface %d - Discard is enabled\n", u4IfIndex);
        CFA_IF_SET_IN_DISCARD (u4IfIndex);
        return CFA_FAILURE;
    }

    /* Call a function to analyse the received packets against the 
     * packet analyser patterns configured */
    CfaPktUtlAnalysePkt (pBuf, u4IfIndex, u4PktSize);

    /* If the bProcessFlag set to CFA_FALSE Packet should not be
     * processed further*/
    if (bProcessFlag == CFA_FALSE)
    {
        return CFA_SUCCESS;
    }

    u1LinkFrameType = PktHandleInfo.u1LinkFrameType;

    pEthHdr = &PktHandleInfo.EnetHdr;

    if (PktHandleInfo.u2EtherProto == CFA_ENET_PTP)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2VlanProt,
                                   CFA_VLAN_TAG_OFFSET, 2);
        if (u2VlanProt == VLAN_PROTOCOL_ID)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2VlanId,
                                       VLAN_TAG_VLANID_OFFSET, 2);
            CfaInfo.CfaPktInfo.VlanId = u2VlanId;
        }
        /* Send the packet to PTP module. */
        CfaInfo.u4IfIndex = u4IfIndex;
        CfaInfo.CfaPktInfo.pBuf = pBuf;
        MEMCPY (CfaInfo.CfaPktInfo.au1DstMac, pEthHdr->au1DstAddr,
                CFA_ENET_ADDR_LEN);

        if (CfaIssFillProtoInfoAndNotifyPkt (&CfaInfo,
                                             PktHandleInfo.u2EtherProto)
            == CFA_FAILURE)
        {
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                      "Unable to provide Packet to PTP module..\r\n",
                      u4IfIndex);
            CFA_IF_SET_IN_DISCARD (u4IfIndex);
            return CFA_FAILURE;
        }

        return CFA_SUCCESS;
    }
/* we now determine the type of Encapsulation - we support EnetV2 and LLC-SNAP.
we also find the type of the packet. */
    if (CFA_ENET_IS_TYPE (PktHandleInfo.u2EtherProto))
    {
        u1EnetEncapType = CFA_ENCAP_ENETV2;
        u2Protocol = PktHandleInfo.u2EtherProto;
        u1EnetHeaderSize = CFA_ENET_V2_HEADER_SIZE;
/* we have already extracted the Enet Header */
    }                            /* end of Enet V2 encap */
    else
    {
#ifdef ISIS_WANTED
        /* Get the LLC header to see SNAP is present */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2LlcHeader,
                                   (CFA_ENET_LLC_HEADER_SIZE +
                                    PktHandleInfo.u4VlanOffset -
                                    CFA_VLAN_TAG_OFFSET - 3), 2);
        if (u2LlcHeader == 0xFEFE)
        {
            /* the packet is LLC-NLPID - get the NLPID */
            CRU_BUF_Copy_FromBufChain (pBuf, &u1Protocol,
                                       CFA_ENET_LLC_HEADER_SIZE +
                                       PktHandleInfo.u4VlanOffset -
                                       CFA_VLAN_TAG_OFFSET, 1);
            u2Protocol = (UINT2) u1Protocol;
        }
        else
#endif /* ISIS_WANTED */
        {

/* the packet may be LLC-SNAP encapsulated */
            u1EnetEncapType = CFA_ENCAP_LLC_SNAP;
            u1EnetHeaderSize = CFA_ENET_SNAP_HEADER_SIZE;
            pSnapEthHdr =
                (tEnetSnapHeader *) (VOID *) CRU_BMC_Get_DataPointer (pBuf);

            if ((pSnapEthHdr->u1DstLSap == CFA_LLC_STAP_SAP) &&
                (pSnapEthHdr->u1SrcLSap == CFA_LLC_STAP_SAP))
            {
                /* Since VLAN module expects the control pkts with LLC header,
                 * header should not be stripped at this point
                 */
                L2IwfWrHandleIncomingLayer2Pkt (pBuf, &PktHandleInfo,
                                                BRG_CNTL_FRAME);

                CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
                          "In L2IwfWrHandleIncomingLayer2Pkt (interface %d)\n",
                          u4IfIndex);

                return (CFA_SUCCESS);
            }

/* check for LLC control frame first - we expect only LLC in SNAP now */
            if (pSnapEthHdr->u1Control == CFA_LLC_CONTROL_UI)
            {

/* check for presence of SNAP which may carry IP/ARP/RARP after LLC */
                if ((pSnapEthHdr->u1DstLSap == CFA_LLC_SNAP_SAP) ||
                    (pSnapEthHdr->u1SrcLSap == CFA_LLC_SNAP_SAP) ||
                    (pSnapEthHdr->u1Oui1 == 0x00) ||
                    (pSnapEthHdr->u1Oui2 == 0x00)
                    || (pSnapEthHdr->u1Oui3 == 0x00))
                {
/* determine which protocol is being carried */
                    u2Protocol = OSIX_NTOHS (pSnapEthHdr->u2ProtocolType);
                }                /* end of SNAP framing */
            }                    /* end of LLC framing */
        }                        /* end of Enet LLC_SNAP encap */

    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
              "In CfaIwfEnetProcessRxFrame - Successfully Recd Frame on interface %d.\n",
              u4IfIndex);
    CFA_DBG3 (CFA_TRC_ALL_TRACK, CFA_IWF,
              "In CfaIwfEnetProcessRxFrame - Encap %d, Type %d and Protocol %x.\n",
              u1EnetEncapType, u1LinkFrameType, u2Protocol);

#ifdef BCMX_WANTED
    /* Ethertype needs to be overwritten based on bridgemode for IP packets received on cpu port */
    if ((CfaCheckIfLocalMac (pEthHdr, u4IfIndex) == CFA_SUCCESS) &&
        (u2Protocol == CFA_ENET_IPV4) &&
        (CfaGetIfBrgPortType (u4IfIndex, &u1InterfaceType) == CFA_SUCCESS))
    {
        if ((u1InterfaceType == CFA_PROVIDER_NETWORK_PORT) &&
            (CfaOvrwriteEtherTypeBasedOnBridgeMode (pBuf) == CFA_FAILURE))
        {
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                      "Unable to Overwrite Ether Type "
                      "over Interface %d...\r\n", u4IfIndex);
            return CFA_FAILURE;
        }
    }
#endif
    CFA_DUMP (CFA_TRC_ENET_PKT_DUMP, pBuf);

/* finally we send the packet to the respective module - it would be either
IP (i.e. FFM) or ARP (i.e. for ARP & RARP both). Here we are assuming right
now that only IP will run over Enet - there should be eventually a check
here for PPP as well and PPP packets should be sent to PPP. Similar mechanism
should be implemented on the Tx side and pre-formed header for PPP should
also be kept ready. */

/* check if this interface is registered with IP */
    if (gu4IsIvrEnabled == CFA_DISABLED)
    {
        if (CFA_IF_IPPORT (u4IfIndex) == CFA_INVALID_INDEX)
        {

            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                      "Exiting CfaIwfEnetProcessRxFrame - interface %d not reg with IP - FAILURE\n",
                      u4IfIndex);

            CFA_IF_SET_IN_DISCARD (u4IfIndex);
            return (CFA_FAILURE);    /* buffer is released by GDD */
        }
    }
    switch (u2Protocol)
    {
        case CFA_ENET_IPV4:

            /* IP source guard validation before IP packet
             * processing */
            i4RetValue = CfaDoIPSrcGuardValidation (pBuf, u4IfIndex, pEthHdr);

            if (i4RetValue == CFA_FAILURE)
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                          CFA_IWF, "Exiting CfaIwfEnetProcessRxFrame"
                          "IP source guard validation for interface %d-"
                          "FAILS\n", u4IfIndex);

                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                return (CFA_FAILURE);    /* buffer is released by GDD */
            }

/* if Ip FWD is disabled and if the dest MAC addr is not of the local port then
 * send the IP frame to bridge. Otherwise send the frame for normal IP 
 * processing */
            if (gu4IsIvrEnabled == CFA_DISABLED)
            {
                if ((gu1IpForwardingEnable == CFA_FALSE) &&
                    (CfaCheckIfLocalMac (pEthHdr, u4IfIndex) != CFA_SUCCESS))
                {
                    if (CfaDuplicateCruBuffer (pBuf, &pDupBuf, u4PktSize) ==
                        CFA_SUCCESS)
                    {

                        if (L2IwfWrHandleIncomingLayer2Pkt
                            (pDupBuf, &PktHandleInfo,
                             BRG_DATA_FRAME) != CFA_SUCCESS)
                        {
                            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                                      CFA_IWF,
                                      "Exiting CfaIwfEnetProcessRxFrame - "
                                      "L2IwfWrHandleIncomingLayer2Pkt for "
                                      "interface %d - returned FAILURE\n",
                                      u4IfIndex);
                            return (CFA_FAILURE);    /* buffer is released by GDD */
                        }

                        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
                                  "In CfaIwfEnetProcessRxFrame - Sent IP Pkt to Bridge on interface %d.\n",
                                  u4IfIndex);
                    }
                    else
                    {
                        CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                                 "Error In CfaIwfEnetProcessRxFrame - Making DUP for sending to Bridge - FAIL.\n");
                    }
                }
                u4VlanIfIndex = u4IfIndex;    /* for VRRP and VLAN enabled case */
            }
            else
            {

                CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);
                if (u1BridgedIfaceStatus == CFA_DISABLED)
                {

                    /*for a L3 port-channel */
                    if (L2IwfIsPortInPortChannel (u4IfIndex) == CFA_SUCCESS)
                    {
                        if (L2IwfGetPortChannelForPort (u4IfIndex, &u2AggId) ==
                            L2IWF_SUCCESS)
                        {
                            u4IfIndex = u2AggId;
                            bRportFlag = TRUE;
                        }
                    }
                }

#ifdef UNIQUE_VLAN_MAC_WANTED
                if (u1BridgedIfaceStatus == CFA_ENABLED)
                {
                    /* buffer is duplicated to fetch u4VlanIfIndex */
                    if (CfaDuplicateCruBuffer (pBuf, &pDupBuffer, u4PktSize) ==
                        CFA_SUCCESS)
                    {

                        u4VlanIfIndex = CfaGetVlanIfIndexAndUntagFrame
                            (u4IfIndex, pDupBuffer, &VlanId);
                        /* Release the DupBuffer after fetching
                         * the u4VlanIfIndex */
                        CRU_BUF_Release_MsgBufChain (pDupBuffer, FALSE);
                    }
                    else
                    {
                        CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                                 "Error In CfaIwfEnetProcessRxFrame - Making DUP Buffer - FAILED.\n");
                    }
                }
                else
                {
                    u4VlanIfIndex = u4IfIndex;

                }
#if ((defined (RMON2_WANTED)) && (defined (NPAPI_WANTED)))
                pData = CRU_BUF_GetDataPtr (pBuf->pFirstValidDataDesc);

                MEMSET (&PktHdrInfo, 0, sizeof (tPktHdrInfo));

                PktHdrInfo.PktHdr.u4IfIndex = u4LclIfIndex;
                PktHdrInfo.PktHdr.u4VlanIfIndex = u4VlanIfIndex;
                PktHdrInfo.PktHdr.u2VlanId = VlanId;
                PktHdrInfo.u4PktSize = u4PktSize;

                if (CfaIwfRmon2UpdatePktHdr (pData, &PktHdrInfo.PktHdr) ==
                    CFA_SUCCESS)
                {
                    if (Rmon2MainUpdateTables (&PktHdrInfo) != CFA_SUCCESS)
                    {
                        CFA_DBG (CFA_TRC_ALL, CFA_GDD,
                                 "Rmon2MainUpdateTables returns Failure\r\n");
                    }
                }
                else
                {
                    CFA_DBG (CFA_TRC_ALL, CFA_GDD,
                             "In CfaGddProcessRecvEvent - \
                failed on invoking CfaIwfRmon2UpdatePktHdr \r\n");
                }
                CfaGetSysMacAddress (SwitchMac);

                if (!
                    ((CFA_IS_ENET_MAC_BCAST (PktHdrInfo.PktHdr.DstMACAddress)
                      || CFA_IS_ENET_MAC_MCAST (PktHdrInfo.PktHdr.
                                                DstMACAddress)))
                    &&
                    ((MEMCMP
                      (PktHdrInfo.PktHdr.DstMACAddress, SwitchMac,
                       sizeof (tMacAddr)) != 0)))
                {
#ifdef VRRP_WANTED
                    /*For Vrrp, Packet will contain virtual mac. */
                    /*So we should check whether a vrrp interface */
                    /* exists with this mac */
                    if (VrrpGetVrIdFromMacAddr (PktHdrInfo.PktHdr.DstMACAddress,
                                                &i4OperVrId) != VRRP_OK)
#endif /* VRRP_WANTED */
                    {
                        CFA_IF_SET_IN_DISCARD (u4LclIfIndex);
                        return CFA_FAILURE;
                    }
                }
#endif
#else
#if ((defined (RMON2_WANTED)) && (defined (NPAPI_WANTED)))
                pData = CRU_BUF_GetDataPtr (pBuf->pFirstValidDataDesc);

                MEMSET (&PktHdrInfo, 0, sizeof (tPktHdrInfo));
                if (u1BridgedIfaceStatus == CFA_ENABLED)
                {
                    if (L2IwfGetPortChannelForPort (u4LclIfIndex, &u2AggId) ==
                        L2IWF_SUCCESS)
                    {
                        u4LclIfIndex = (UINT4) u2AggId;
                    }
                    VlanIdentifyVlanIdAndUntagFrame (RMON_MODULE,
                                                     pBuf, u4LclIfIndex,
                                                     &PktHdrInfo.PktHdr.
                                                     u2VlanId);
                    u4VlanIfIndex =
                        CfaGetVlanInterfaceIndex (PktHdrInfo.PktHdr.u2VlanId);
                }
                else
                {
                    /*for a L3 port-channel */
                    u4VlanIfIndex = u4IfIndex;
                    u4LclIfIndex = u4IfIndex;
                }

                PktHdrInfo.PktHdr.u4IfIndex = u4LclIfIndex;
                PktHdrInfo.PktHdr.u4VlanIfIndex = u4VlanIfIndex;
                PktHdrInfo.u4PktSize = u4PktSize;

                if (CfaIwfRmon2UpdatePktHdr (pData, &PktHdrInfo.PktHdr) ==
                    CFA_SUCCESS)
                {
                    if (Rmon2MainUpdateTables (&PktHdrInfo) != CFA_SUCCESS)
                    {
                        CFA_DBG (CFA_TRC_ALL, CFA_GDD,
                                 "Rmon2MainUpdateTables returns Failure\r\n");
                    }
                }
                else
                {
                    CFA_DBG (CFA_TRC_ALL, CFA_GDD,
                             "In CfaGddProcessRecvEvent - \
                failed on invoking CfaIwfRmon2UpdatePktHdr \r\n");
                }
                CfaGetIfHwAddr (u4VlanIfIndex, SwitchMac);

                if (!
                    ((CFA_IS_ENET_MAC_BCAST (PktHdrInfo.PktHdr.DstMACAddress)
                      || CFA_IS_ENET_MAC_MCAST (PktHdrInfo.PktHdr.
                                                DstMACAddress)))
                    &&
                    ((MEMCMP
                      (PktHdrInfo.PktHdr.DstMACAddress, SwitchMac,
                       sizeof (tMacAddr)) != 0)))
                {
                    if ((CfaIsMgmtPort (PktHdrInfo.PktHdr.u4IfIndex)
                         == TRUE) ||
                        (CfaIsLinuxVlanIntf (PktHdrInfo.PktHdr.u4IfIndex)
                         == TRUE))
                    {
                        CfaGetIfHwAddr (PktHdrInfo.PktHdr.u4IfIndex, au1OobMac);
                        if (MEMCMP (PktHdrInfo.PktHdr.DstMACAddress,
                                    au1OobMac, sizeof (tMacAddr)) != 0)
                        {
#ifdef VRRP_WANTED
                            /*For Vrrp, Packet will contain virtual mac. */
                            /*So we should check whether a vrrp interface */
                            /* exists with this mac */
                            if (VrrpGetVrIdFromMacAddr
                                (PktHdrInfo.PktHdr.DstMACAddress,
                                 &i4OperVrId) != VRRP_OK)
#endif /* VRRP_WANTED */
                            {
                                CFA_IF_SET_IN_DISCARD (u4LclIfIndex);
                                return CFA_FAILURE;
                            }
                        }
                    }
                }
#endif
                UNUSED_PARAM (u4LclIfIndex);
                if (u1BridgedIfaceStatus == CFA_ENABLED)
                {
                    /* 
                     * For giving the packets to L2iwf, retrive the vlan
                     * tag in the packet without stripping the packet as 
                     * the vlan tag header will be stripped by the individual 
                     * L2 protocols. For L3 protocols, the packet will be
                     * stripped before handing over the packets to the IP
                     * module.
                     */
                    u4VlanIfIndex = (UINT4) CfaGetVlanIfIndex
                        (u4IfIndex, pBuf, &VlanId);
#if ((defined (VXLAN_WANTED)) && (defined (NPAPI_WANTED)))
                    if ((u4VlanIfIndex == CFA_INVALID_INDEX) && (VlanId == 0))
                    {
                        if (VxlanPortCheckVniVlanMapAndUpdateNveDb (pBuf) ==
                            VXLAN_SUCCESS)
                        {
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                            return CFA_SUCCESS;
                        }

                    }
#endif
                }
                else
                {
                    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
                    {
                        CfaGetEthernetType (u4IfIndex, &u1IfEtherType);
                        if (u1IfEtherType == CFA_STACK_ENET)
                        {
                            u4VlanIfIndex = (UINT4) CfaGetVlanInterfaceIndex
                                (CFA_DEFAULT_STACK_VLAN_ID);
                        }
                        else
                        {
                            u4VlanIfIndex = u4IfIndex;
                        }
                    }
                    else
                    {
                        u4VlanIfIndex = u4IfIndex;
                    }
                }
#endif
            }
            if (u4VlanIfIndex != CFA_INVALID_INDEX)
            {
                if (CFA_IF_IPPORT (u4VlanIfIndex) == CFA_INVALID_INDEX)
                {
                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                              "Exiting CfaIwfEnetProcessRxFrame - Protocol IP - interface %d not reg with IP - FAILURE\n",
                              u4IfIndex);

                    CFA_IF_SET_IN_DISCARD (u4IfIndex);
                    return (CFA_FAILURE);    /* buffer is released by GDD */
                }
            }
            if ((CfaCheckIsLocalMac (pEthHdr, u4IfIndex,
                                     (UINT2) u4VlanIfIndex) != CFA_SUCCESS) &&
                (u1BridgedIfaceStatus == CFA_DISABLED))
            {
                /* IGMP control packet recived on router port should not be
                 * processed */
#if (!defined (IGMP_WANTED))
                if (PktHandleInfo.u1IpProto == L2_IGMP_CONTROL_PKT)
                {
                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                              "Exiting CfaIwfEnetProcessRxFrame - IGMP control packet received on router port - interface %d - FAILURE\n",
                              u4IfIndex);
                    return (CFA_FAILURE);    /* buffer is released by GDD */
                }
#endif
            }

            if ((CfaCheckIsLocalMac (pEthHdr, u4IfIndex,
                                     (UINT2) u4VlanIfIndex) != CFA_SUCCESS) &&
                (u1BridgedIfaceStatus == CFA_ENABLED))
            {
#if (defined LNXIP4_WANTED) && (defined (IGMP_WANTED))    /* IGMP_WANTED */
                /* Below packet delivery to IGMP is special case for 
                   receiving packets with 0.0.0.0 */
                if (PktHandleInfo.u1IpProto == L2_IGMP_CONTROL_PKT)
                {
                    if (CfaDuplicateCruBuffer (pBuf, &pDupBuf, u4PktSize) ==
                        CFA_SUCCESS)
                    {
                        if (PktHandleInfo.u4VlanOffset ==
                            CFA_VLAN_TAGGED_HEADER_SIZE)
                        {
                            u1Offset = CFA_ENET_V2_HEADER_SIZE + 4;

                        }
                        else
                        {
                            u1Offset = CFA_ENET_V2_HEADER_SIZE;
                        }
                        if (CRU_BUF_Move_ValidOffset (pDupBuf, u1Offset) ==
                            CRU_SUCCESS)
                        {

                            pIpParms = (tIpParms *) (&pDupBuf->ModuleData);
                            u4IfIpPortNum =
                                (UINT4) CFA_IF_IPPORT (u4VlanIfIndex);
                            pIpParms->u2Port = (UINT2) u4IfIpPortNum;
                            CfaGetIfBridgedIfaceStatus (u4IfIndex,
                                                        &u1BridgedIfaceStatus);
                            /* #if IGMP_WANTED cases, strip the vlan tag from the buffer
                             * if the packet is received on the physical ports. */
                            if (u1BridgedIfaceStatus == CFA_ENABLED)
                            {
                                u4VlanIfIndex =
                                    (UINT4)
                                    CfaGetVlanIfIndexAndUntagFrame (u4IfIndex,
                                                                    pDupBuf,
                                                                    &VlanId);
                            }
                            pIpHdr =
                                (t_IP_HEADER *) (VOID *)
                                CRU_BUF_Get_DataPtr_IfLinear (pDupBuf, 0,
                                                              sizeof
                                                              (t_IP_HEADER));

                            if (pIpHdr == NULL)
                            {
                                /* The header is not contiguous in the buffer */
                                pIpHdr = &TmpIpHdr;
                                /* Copy the header */
                                CRU_BUF_Copy_FromBufChain (pDupBuf,
                                                           (UINT1 *) pIpHdr, 0,
                                                           sizeof
                                                           (t_IP_HEADER));
                            }
                            if (pIpHdr->u4Src == 0)
                            {
                                IgmpHandleIncomingPkt (pDupBuf, 0, 0, IfId, 0);
                            }
                            else
                            {
                                /*DupBuff Released here since delivering to IGMP not required */
                                CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
                                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                                          CFA_IWF,
                                          "IN CfaIwfEnetProcessRxFrame - "
                                          "Delivering to IGMP Module is failed for "
                                          "interface %d since SrcAddr is not 0.0.0.0\n",
                                          u4IfIndex);

                            }
                        }
                        else
                        {
                            CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
                            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                                      CFA_IWF,
                                      "IN CfaIwfEnetProcessRxFrame - "
                                      "Delivering to IGMP Module is failed for "
                                      "interface %d EthdHdr stripping failed\n",
                                      u4IfIndex);

                        }
                    }
                    else
                    {
                        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                                  CFA_IWF,
                                  "IN CfaIwfEnetProcessRxFrame - "
                                  "Delivering to IGMP Module is failed for "
                                  "interface %d since Buffer copy failed\n",
                                  u4IfIndex);
                    }
                }
#endif
#if ((defined (IGS_WANTED)) && (!defined (IGMP_WANTED)))    /* ISS_METRO_WANTED */
                if (PktHandleInfo.u1IpProto == L2_IGMP_CONTROL_PKT)
                {
                    /* Updating pBuf's MODULE_DATA Here */
                    /*CRU_BUF_Set_U2Reserved(pBuf,L2_IGMP_CONTROL_PKT); */

                    if (L2IwfWrHandleIncomingLayer2Pkt
                        (pBuf, &PktHandleInfo, BRG_IGMP_FRAME) != CFA_SUCCESS)
                    {
                        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                                  CFA_IWF,
                                  "Exiting CfaIwfEnetProcessRxFrame - "
                                  "L2IwfWrHandleIncomingLayer2Pkt for "
                                  "interface %d - returned FAILURE\n",
                                  u4IfIndex);
                        return (CFA_FAILURE);    /* buffer is released by GDD */
                    }

                }
                else
#endif
                {
                    if (CfaDuplicateCruBuffer (pBuf, &pDupBuf, u4PktSize) ==
                        CFA_SUCCESS)
                    {
                        MEMCPY (SEC_GET_MODULE_DATA_PTR (pDupBuf),
                                SEC_GET_MODULE_DATA_PTR (pBuf),
                                sizeof (tSecModuleData));
                        if (L2IwfWrHandleIncomingLayer2Pkt
                            (pDupBuf, &PktHandleInfo,
                             BRG_DATA_FRAME) != CFA_SUCCESS)
                        {
                            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                                      CFA_IWF,
                                      "Exiting CfaIwfEnetProcessRxFrame - "
                                      "L2IwfWrHandleIncomingLayer2Pkt for "
                                      "interface %d - returned FAILURE\n",
                                      u4IfIndex);
                            return (CFA_FAILURE);    /* buffer is released by GDD */
                        }

                        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
                                  "In CfaIwfEnetProcessRxFrame - Sent IP Pkt to Bridge on interface %d.\n",
                                  u4IfIndex);
                    }
                    else
                    {
                        CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                                 "Error In CfaIwfEnetProcessRxFrame - Making DUP for sending to Bridge - FAIL.\n");
                    }
                }
            }
            else if (u1BridgedIfaceStatus == CFA_ENABLED)
            {
                L2IwfGetPortPnacAuthMode (u4IfIndex, &u2PortAuthMode);

                L2IwfGetPortPnacAuthControl (u4IfIndex, &u2PortAuthControl);

                L2IwfGetPortPnacPaeStatus (u4IfIndex, &u1PortPnacStatus);

                /* Check whether PNAC is enabled on a port */
                if (u1PortPnacStatus == OSIX_ENABLED)
                {
                    if ((u2PortAuthMode == PNAC_PORT_AUTHMODE_PORTBASED) ||
                        ((u2PortAuthMode == PNAC_PORT_AUTHMODE_MACBASED) &&
                         (u2PortAuthControl != PNAC_PORTCNTRL_AUTO)))
                    {
                        if (L2IwfIsPortAuthorized (u4IfIndex) == L2IWF_FAILURE)
                        {
                            CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                                     "Error In CfaIwfEnetProcessRxFrame"
                                     "- Port Unauthorized.\n");
                            return (CFA_FAILURE);
                        }
                    }
                    else
                    {
                        if (L2IwfIsSrcMacAuthorized (pEthHdr->au1SrcAddr)
                            == L2IWF_FAILURE)
                        {
                            CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                                     "Error In CfaIwfEnetProcessRxFrame"
                                     "-Received Source Mac Unauthorized.\n");
                            return (CFA_FAILURE);
                        }
                    }
                }

                /* PNAC disabled on a port. Proceed frame processing */
            }

            if (u1LinkFrameType == CFA_LINK_UCAST)
            {
                CFA_IF_SET_IN_UCAST (u4IfIndex);
/* scan the Rcv Address Table to see if the frame is addressed to local host
if not then drop the packet - dont send to IP */
                if (CfaIwfCheckIsLocalEnetFrame
                    (u4IfIndex, u4VlanIfIndex,
                     (UINT1 *) pEthHdr) != CFA_SUCCESS)
                {
#ifdef VRRP_WANTED
                    /*For Vrrp, Packet will contain virtual mac. */
                    /*So we should check whether a vrrp interface */
                    /* exists with this mac */
                    if (VrrpGetVrIdFromMacAddr (pEthHdr->au1DstAddr,
                                                &i4OperVrId) != VRRP_OK)
#endif /* VRRP_WANTED */
                    {
                        CFA_IF_SET_IN_DISCARD (u4IfIndex);
                        return CFA_FAILURE;    /* buffer is released by GDD */
                    }
                }
            }                    /* end of processing for Bridge */

            /* 
             * For Metro package, it is not necessary to give the
             * IGMP Control packet to the L3 protocols. So it 
             * is avoided to optimise IGMP packet handling path. 
             */

#if ((defined (IGS_WANTED)) && (!defined (IGMP_WANTED)))    /* ISS_METRO_WANTED */
            if (PktHandleInfo.u1IpProto != L2_IGMP_CONTROL_PKT)
            {
#endif
                /* Apply security on the frames received over WAN interface. 
                 */
                if ((CFA_LINK_UCAST == u1LinkFrameType) && (bRportFlag == FALSE)
#ifdef OPENFLOW_WANTED            /* skip security check for OpenFlow tunnel */
                    && (OfcHandleOpenflowSwModeProcess (&u4IfIndex, pBuf) !=
                        OFC_SUCCESS)
#endif /* OPENFLOW_WANTED */
                    )
                {
                    if (u1NetworkType == CFA_NETWORK_TYPE_WAN)
                    {
                        SecIntfInfo.u4PhyIfIndex = u4IfIndex;
                        SecIntfInfo.u4VlanIfIndex = u4VlanIfIndex;

                        i4RetVal =
                            (INT4) SecProcessFrame (pBuf, &SecIntfInfo,
                                                    SEC_INBOUND, &bSecVerdict);
                        if ((OSIX_SUCCESS == i4RetVal)
                            && (SEC_STOLEN == bSecVerdict))
                        {
                            /* Indicates that the frame is stolen for security 
                             * processing. 
                             */
                            return CFA_SUCCESS;
                        }
                        if (OSIX_FAILURE == i4RetVal)
                        {
                            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                                      CFA_IWF,
                                      "Exiting CfaIwfEnetProcessRxFrame"
                                      "Security Processing for interface %d-"
                                      "FAILS\n", u4IfIndex);

                            CFA_IF_SET_IN_DISCARD (u4IfIndex);
                            return (CFA_FAILURE);    /* buffer is released by GDD */
                        }
                    }
                }

                /* #if IGMP_WANTED cases, strip the vlan tag from the buffer 
                 * if the packet is received on the physical ports. */
                if (u1BridgedIfaceStatus == CFA_ENABLED)
                {
                    u4VlanIfIndex = (UINT4) CfaGetVlanIfIndexAndUntagFrame
                        (u4IfIndex, pBuf, &VlanId);
                }
/* Remove Enet/LLC-SNAP header */

#ifdef VRRP_WANTED
                if ((VrrpEnabledOnInterface ((INT4) u4VlanIfIndex) == VRRP_OK)
                    && (u1LinkFrameType == CFA_LINK_UCAST)
                    && (VrrpGetVrIdFromMacAddr (pEthHdr->au1DstAddr,
                                                &i4OperVrId) == VRRP_OK))
                {
#ifndef NPAPI_WANTED
                    /* Dropping of packet which is destined to Backup node
                     * is skipped when MC-LAG is enabled on interface */
                    if (CfaLaApiIsMemberofMcLag (u4IfIndex) != OSIX_SUCCESS)
                    {
                        /* The packet is addressed to Virtual Mac Address */
                        if ((VrrpGetState (u4VlanIfIndex, i4OperVrId,
                                           IPVX_ADDR_FMLY_IPV4)) ==
                            BACKUP_STATE)
                        {
                            /*for backup drop all unicast packets which 
                               are addressed for Virtual MAC Address */
                            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                                      CFA_IWF,
                                      "Error in CfaIwfEnetProcessRxFrame - VRRP in backup state %d.\n",
                                      u4IfIndex);
                            CFA_IF_SET_IN_ERR (u4IfIndex);
                            if (CFA_DEF_INTERFACES_IN_SYSTEM > u4VlanIfIndex)
                            {
                                CFA_IF_SET_IN_ERR (u4VlanIfIndex);
                            }
                            return (CFA_FAILURE);
                        }
                    }
#endif
                }
                else
                {
#endif /* VRRP_WANTED */

                    /* - If the pkt's  MAC address is not addressed to us and if 
                     * pkt is neither broadcast or multicast, drop the pkt
                     * - If IVR is enabled, the MAC address to be compared against 
                     * the packet's MAC address should be the VLAN interface's 
                     * MAC address.
                     */
                    if (CfaCheckIsLocalMac (pEthHdr, u4IfIndex,
                                            (UINT2) u4VlanIfIndex) !=
                        CFA_SUCCESS)
                    {
                        if ((u1LinkFrameType != CFA_LINK_MCAST)
                            && (u1LinkFrameType != CFA_LINK_BCAST))
                        {
                            CFA_IF_SET_IN_DISCARD (u4IfIndex);
                            return (CFA_FAILURE);
                        }
                    }
#ifdef VRRP_WANTED
                }
#endif
                CfaGetIfaceType (u4IfIndex, &u1IfType);

                if ((u1BridgedIfaceStatus == CFA_DISABLED) &&
                    (u1IfType == CFA_PSEUDO_WIRE) &&
                    (CFA_IS_VLAN_PRESENT
                     (pBuf->pFirstValidDataDesc->pu1_FirstValidByte) ==
                     CFA_TRUE))
                {
                    u1EnetHeaderSize =
                        (UINT1) (u1EnetHeaderSize + VLAN_TAG_PID_LEN);
                }
                u4PktSize = u4PktSize - u1EnetHeaderSize;
                if (CRU_BUF_Move_ValidOffset (pBuf, u1EnetHeaderSize) !=
                    CRU_SUCCESS)
                {
                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                              "Error in CfaIwfEnetProcessRxFrame - Cant Strip Frame on interface %d.\n",
                              u4VlanIfIndex);
                    CFA_IF_SET_IN_ERR (u4VlanIfIndex);
                    return (CFA_FAILURE);    /* buffer is released by GDD */
                }

                if (gu4IsIvrEnabled == CFA_ENABLED)
                {
                    if (u4VlanIfIndex != CFA_INVALID_INDEX)
                    {
#ifdef ISS_WANTED
#ifndef KERNEL_WANTED
                        if (CfaIsMgmtPort (u4IfIndex) == FALSE)
                        {
                            bOOBFlag = OSIX_FALSE;
                        }
                        else
                        {
                            bOOBFlag = OSIX_TRUE;
                        }

                        /* Parse the recieved packet and get info */
                        CfaGetL3L4Info (pBuf, &u4SrcIp, &u1L3Proto,
                                        &u2L4DstPort);
                        /* Apply filter  for the management packets only
                         * when IPAuthMgr is  applied at user space*/
                        if (IssApplyFilter ((UINT2) u4IfIndex, VlanId, u4SrcIp,
                                            u1L3Proto, u2L4DstPort,
                                            bOOBFlag) == ISS_FAILURE)
                        {
                            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                                      CFA_IWF,
                                      "Exiting CfaIwfEnetProcessRxFrame - Pkt recvd on Port %d dropped on IP Authorization Failure.\n",
                                      u4IfIndex);

                            CFA_IF_SET_IN_DISCARD (u4IfIndex);

                            return (CFA_FAILURE);
                        }
#endif /*KERNEL_WANTED */
#endif /*ISS_WANTED */

                        CFA_IF_SET_IN_OCTETS (u4VlanIfIndex, u4PktSize);
                        i4RetValue =
                            CfaProcessIncomingIpPkt (pBuf, u4VlanIfIndex,
                                                     u1LinkFrameType, pEthHdr);
                    }
                    else
                        i4RetValue = CFA_FAILURE;
                }
                else
                {
                    i4RetValue = CfaProcessIncomingIpPkt (pBuf, u4IfIndex,
                                                          u1LinkFrameType,
                                                          pEthHdr);
                }

/* send the packet to IP packet handler */
                if (i4RetValue != CFA_SUCCESS)
                {

                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                              "Exiting CfaIwfEnetProcessRxFrame - IP Pkt on interface %d - FAILURE\n",
                              u4IfIndex);

                    CFA_IF_SET_IN_DISCARD (u4IfIndex);
                    if (u4VlanIfIndex != CFA_INVALID_INDEX)
                    {
                        CFA_IF_SET_IN_DISCARD (u4VlanIfIndex);
                    }
                    return (CFA_FAILURE);    /* buffer is released by GDD */
                }
#if ((defined (IGS_WANTED)) && (!defined (IGMP_WANTED)))    /* ISS_METRO_WANTED */
            }

#endif /* ISS_METRO_WANTED */
            break;

        case CFA_ENET_SLOW_PROTOCOL:
            if (EoamApiIsOAMPDU (pBuf) == OSIX_TRUE)
            {
                L2IwfGetEoamTunnelStatus (u4IfIndex, &pu1EoamTunnelStatus);
                if (pu1EoamTunnelStatus == VLAN_TUNNEL_PROTOCOL_PEER)
                {
                    /* Send the OAMPDU to EOAM sublayer */
                    if (EoamApiEnqIncomingPdu (pBuf, u4IfIndex) == OSIX_FAILURE)
                    {
                        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                                  "Exiting CfaIwfEnetProcessRxFrame - EOAM PDU"
                                  " on interface %d - FAILURE\n", u4IfIndex);

                        CFA_IF_SET_IN_DISCARD (u4IfIndex);
                        return CFA_FAILURE;
                    }
                }
                else if (pu1EoamTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    /* Forward to l2iwf for tunneling */
                    L2IwfWrHandleIncomingLayer2Pkt (pBuf, &PktHandleInfo,
                                                    BRG_DATA_FRAME);
                    break;
                }
                else if (pu1EoamTunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    /* Discard the packet */
                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                              "Exiting CfaIwfEnetProcessRxFrame - EOAM PDU"
                              " on interface %d - Discard is enabled \n",
                              u4IfIndex);
#ifdef VLAN_WANTED
                    VlanIncrRxDiscardCounters (u4IfIndex, L2_PROTO_EOAM);
#endif
                    CFA_IF_SET_IN_DISCARD (u4IfIndex);
                    return CFA_FAILURE;
                }
            }
#ifdef SYNCE_WANTED
            else if (SynceApiIsSyncePDU (pBuf) == OSIX_TRUE)
            {
                /* duplicate the CRU buffer */
                pDupCruBuf = CRU_BUF_Duplicate_BufChain (pBuf);
                if (pDupCruBuf != NULL)
                {
                    /* Send the PDU to SyncE module */
                    if (SynceApiEnqIncomingPdu
                        (pDupCruBuf, (UINT2) u4PktSize,
                         u4IfIndex) == OSIX_FAILURE)
                    {
                        CRU_BUF_Release_MsgBufChain (pDupCruBuf, OSIX_FALSE);
                        pDupCruBuf = NULL;
                        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                                  "Exiting CfaIwfEnetProcessRxFrame - SYNCE PDU"
                                  " on interface %d - FAILURE\n", u4IfIndex);

                        CFA_IF_SET_IN_DISCARD (u4IfIndex);
                        return CFA_FAILURE;
                    }
                }
                CRU_BUF_Release_MsgBufChain (pBuf, OSIX_FALSE);
            }
#endif
            else
            {
                /* LA control packet is given to L2Iwf module. */
                L2IwfWrHandleIncomingLayer2Pkt (pBuf, &PktHandleInfo,
                                                BRG_DATA_FRAME);
            }
            break;

        case CFA_ENET_ARP:
        case CFA_ENET_RARP:

/* if Ip FWD is disabled send a copy of ARP/RARP frame to bridge first. 
The bridge will make
DUPs and flood over ports. Each time a pkt is released in GDD the reference
count will be decremented. We then send the packet to ARP task after stripping
the Enet Header */
            if (gu4IsIvrEnabled == CFA_DISABLED)
            {
                if ((gu1IpForwardingEnable == CFA_FALSE))
                {
                    if (CfaDuplicateCruBuffer (pBuf, &pDupBuf, u4PktSize) ==
                        CFA_SUCCESS)
                    {
                        if (L2IwfWrHandleIncomingLayer2Pkt
                            (pDupBuf, &PktHandleInfo,
                             BRG_DATA_FRAME) != CFA_SUCCESS)
                        {
                            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                                      CFA_IWF,
                                      "Exiting CfaIwfEnetProcessRxFrame - "
                                      "L2IwfWrHandleIncomingLayer2Pkt for "
                                      "interface %d - returned FAILURE\n",
                                      u4IfIndex);
                            return (CFA_FAILURE);    /* buffer is released by GDD */
                        }
                        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
                                  "In CfaIwfEnetProcessRxFrame - Sent ARP/RARP Pkt to Bridge on interface %d.\n",
                                  u4IfIndex);
                    }
                    else
                    {
                        CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                                 "Error In CfaIwfEnetProcessRxFrame - Making DUP for sending to Bridge - FAIL.\n");
                    }
                }
            }
            else
            {
                CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);
                if (u1BridgedIfaceStatus == CFA_ENABLED)
                {
                    if (CfaDuplicateCruBuffer (pBuf, &pDupBuf, u4PktSize) ==
                        CFA_SUCCESS)
                    {
                        if (L2IwfWrHandleIncomingLayer2Pkt
                            (pDupBuf, &PktHandleInfo,
                             BRG_DATA_FRAME) != CFA_SUCCESS)
                        {
                            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                                      CFA_IWF,
                                      "Exiting CfaIwfEnetProcessRxFrame - "
                                      "L2IwfWrHandleIncomingLayer2Pkt for "
                                      "interface %d - returned FAILURE\n",
                                      u4IfIndex);
                            return (CFA_FAILURE);    /* buffer is released by GDD */
                        }
                        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
                                  "In CfaIwfEnetProcessRxFrame - Sent ARP/RARP Pkt to Bridge on interface %d.\n",
                                  u4IfIndex);
                    }
                    else
                    {
                        CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                                 "Error In CfaIwfEnetProcessRxFrame - Making DUP for sending to Bridge - FAIL.\n");
                    }
                }
            }
            /* end of sending to bridge */
            if (gu4IsIvrEnabled == CFA_ENABLED)
            {
                CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);
                if (u1BridgedIfaceStatus == CFA_ENABLED)
                {
                    u4VlanIfIndex = (UINT2) CfaGetVlanIfIndexAndUntagFrame
                        (u4IfIndex, pBuf, &VlanId);
                }
                else
                {
                    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
                    {
                        CfaGetEthernetType (u4IfIndex, &u1IfEtherType);
                        if (u1IfEtherType == CFA_STACK_ENET)
                        {
                            u4VlanIfIndex = CfaGetVlanInterfaceIndex
                                (CFA_DEFAULT_STACK_VLAN_ID);
                        }
                        else
                        {
                            u4VlanIfIndex = u4IfIndex;
                        }
                    }
                    else
                    {
                        /*for a L3 port-channel */
                        if (L2IwfIsPortInPortChannel (u4IfIndex) == CFA_SUCCESS)
                        {
                            if (L2IwfGetPortChannelForPort (u4IfIndex, &u2AggId)
                                == L2IWF_SUCCESS)
                            {
                                u4VlanIfIndex = u2AggId;
                                u4LclIfIndex = u2AggId;
                            }
                            else
                            {
                                u4VlanIfIndex = u4IfIndex;
                            }
                        }
                        else
                        {
                            u4VlanIfIndex = u4IfIndex;
                        }

                    }
                }
            }
            else                /* Added for VRRP */
            {
                u4VlanIfIndex = u4IfIndex;
            }

            if (u4VlanIfIndex != CFA_INVALID_INDEX)
            {
                if (CFA_IF_IPPORT (u4VlanIfIndex) == CFA_INVALID_INDEX)
                {

                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                              "Exiting CfaIwfEnetProcessRxFrame - Protocol ARP - interface %d not reg with IP - FAILURE\n",
                              u4IfIndex);
                    CFA_IF_SET_IN_DISCARD (u4IfIndex);
                    return (CFA_FAILURE);    /* buffer is released by GDD */
                }
            }
#ifdef LNXIP4_WANTED
#ifndef NAT_WANTED
            if (u4VlanIfIndex != CFA_INVALID_INDEX)
            {
                CfaHandleInBoundPktToLnxIpTap (u4VlanIfIndex, pBuf, NULL,
                                               IP_ID);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return CFA_SUCCESS;
            }
#endif
#endif
#if ((defined (RMON2_WANTED)) && (defined (NPAPI_WANTED)))
            pData = CRU_BUF_GetDataPtr (pBuf->pFirstValidDataDesc);

            MEMSET (&PktHdrInfo, 0, sizeof (tPktHdrInfo));

            PktHdrInfo.PktHdr.u4IfIndex = u4LclIfIndex;
            PktHdrInfo.PktHdr.u4VlanIfIndex = u4VlanIfIndex;
            PktHdrInfo.PktHdr.u2VlanId = VlanId;
            PktHdrInfo.u4PktSize = u4PktSize;

            if (CfaIwfRmon2UpdatePktHdr (pData, &PktHdrInfo.PktHdr) ==
                CFA_SUCCESS)
            {
                if (Rmon2MainUpdateTables (&PktHdrInfo) != CFA_SUCCESS)
                {
                    CFA_DBG (CFA_TRC_ALL, CFA_GDD,
                             "Rmon2MainUpdateTables returns Failure\r\n");
                }
            }
            else
            {
                CFA_DBG (CFA_TRC_ALL, CFA_GDD, "In CfaGddProcessRecvEvent - \
                failed on invoking CfaIwfRmon2UpdatePktHdr \r\n");
            }
            CfaGetIfHwAddr (u4VlanIfIndex, SwitchMac);

            if (!((CFA_IS_ENET_MAC_BCAST (PktHdrInfo.PktHdr.DstMACAddress) ||
                   CFA_IS_ENET_MAC_MCAST (PktHdrInfo.PktHdr.DstMACAddress))) &&
                (MEMCMP (PktHdrInfo.PktHdr.DstMACAddress,
                         SwitchMac, sizeof (tMacAddr)) != 0))
            {
                if ((CfaIsMgmtPort (PktHdrInfo.PktHdr.u4IfIndex)
                     == TRUE) ||
                    (CfaIsLinuxVlanIntf (PktHdrInfo.PktHdr.u4IfIndex) == TRUE))
                {
                    CfaGetIfHwAddr (PktHdrInfo.PktHdr.u4IfIndex, au1OobMac);
                    if (MEMCMP (PktHdrInfo.PktHdr.DstMACAddress,
                                au1OobMac, sizeof (tMacAddr)) != 0)
                    {
#ifdef VRRP_WANTED
                        /*For Vrrp, Packet will contain virtual mac. */
                        /*So we should check whether a vrrp interface */
                        /* exists with this mac */
                        if (VrrpGetVrIdFromMacAddr
                            (PktHdrInfo.PktHdr.DstMACAddress,
                             &i4OperVrId) != VRRP_OK)
#endif /* VRRP_WANTED */
                        {
                            CFA_IF_SET_IN_DISCARD (u4LclIfIndex);
                            return CFA_FAILURE;
                        }
                    }
                }
            }
#endif

/* Remove Enet header */
            u4PktSize = u4PktSize - u1EnetHeaderSize;
            if (CRU_BUF_Move_ValidOffset (pBuf, u1EnetHeaderSize) !=
                CRU_SUCCESS)
            {

                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Error in CfaIwfEnetProcessRxFrame - Cant Strip Frame on interface %d.\n",
                          u4IfIndex);
                CFA_IF_SET_IN_ERR (u4IfIndex);
                if (CFA_DEF_INTERFACES_IN_SYSTEM > u4VlanIfIndex)
                {
                    CFA_IF_SET_IN_ERR (u4VlanIfIndex);
                }
                return (CFA_FAILURE);    /* buffer is released by GDD */
            }

/* send the packet to the ARP/RARP task */
#if defined (ARP_WANTED) && (defined (IP_WANTED) || defined (LNXIP4_WANTED))
#ifdef VRRP_WANTED

            /* If VRRP is enabled on an interface then packets are processed
             * based on whether IP Forwarding is enabled/disabled.
             */

            if (VrrpEnabledOnInterface ((INT4) u4VlanIfIndex) == VRRP_OK)
            {
                if ((CfaIpIfGetIfInfo ((UINT4) u4VlanIfIndex,
                                       &IpInfInfo) != CFA_SUCCESS)
                    || (IpInfInfo.u1IpForwardEnable == CFA_DISABLED))
                {
                    /* CRU_BUF_Release_MsgBufChain(pBuf,FALSE); */
                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                              "Exiting CfaIwfEnetProcessRxFrame - ARP/RARP Pkt on Ip-disabled interface %d - FAILURE\n",
                              u4IfIndex);
                    CFA_IF_SET_IN_DISCARD (u4IfIndex);
                    CFA_IF_SET_IN_DISCARD (u4VlanIfIndex);
                    return (CFA_FAILURE);
                }
            }
#endif /* VRRP_WANTED */

#if defined (WLC_WANTED) || defined (WTP_WANTED)
#ifdef LNXIP4_WANTED
#ifdef NAT_WANTED
            if (CFA_CHECK_IF_NAT_ENABLE (u4IfIndex) == NAT_ENABLE)
            {
                if (arp_extract_hdr (&Arp_pkt, pBuf) == ARP_SUCCESS)
                {
                    u4IpAddress = NatSearchStaticTable (Arp_pkt.u4Tproto_addr,
                                                        NAT_INBOUND, u4IfIndex);
                    if (u4IpAddress == 0)
                    {
                        i1Status =
                            NatSearchGlobalIpAddrTable (Arp_pkt.u4Tproto_addr,
                                                        u4IfIndex);
                        if (NAT_SUCCESS != i1Status)
                        {
                            i1NatPolicyStatus =
                                NatApiSearchPolicyTable (Arp_pkt.u4Tproto_addr);
                        }
                    }

                    if ((u4IpAddress != 0) || (i1Status == NAT_SUCCESS) ||
                        (NAT_SUCCESS == i1NatPolicyStatus))
                    {
                        i1IsNatIpEntry = TRUE;
                    }
                }
            }
            if (i1IsNatIpEntry != TRUE)
#endif
            {
                if (u4VlanIfIndex != CFA_INVALID_INDEX)
                {
                    CfaHandleInBoundPktToLnxIpTap (u4VlanIfIndex, pBuf, NULL,
                                                   IP_ID);
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    return CFA_SUCCESS;
                }
            }
#endif
#endif

            MEMSET (&ArpQMsg, 0, sizeof (tArpQMsg));
            ArpQMsg.u4MsgType = FROM_CFA_TO_ARP;
            ArpQMsg.u2Protocol = u2Protocol;
            ArpQMsg.u4EncapType = (UINT4) u1EnetEncapType;
            ArpQMsg.pBuf = pBuf;

            if (gu4IsIvrEnabled == CFA_ENABLED)
            {
                if (u4VlanIfIndex != CFA_INVALID_INDEX)
                {

                    CFA_IF_SET_IN_OCTETS (u4VlanIfIndex, u4PktSize);
                    ArpQMsg.u2Port = (UINT2) CFA_IF_IPPORT (u4VlanIfIndex);
                    i4RetValue = ArpEnqueuePkt (&ArpQMsg);
                }
                else
                    i4RetValue = CFA_FAILURE;
            }
            else
            {
                ArpQMsg.u2Port = (UINT2) CFA_IF_IPPORT (u4IfIndex);
                i4RetValue = ArpEnqueuePkt (&ArpQMsg);

            }
            if (i4RetValue != ARP_SUCCESS)
            {
                CFA_IF_SET_IN_DISCARD (u4IfIndex);

                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Exiting CfaIwfEnetProcessRxFrame - ARP/RARP Pkt on interface %d - FAILURE\n",
                          u4IfIndex);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                if (u4VlanIfIndex != CFA_INVALID_INDEX)
                {
                    CFA_IF_SET_IN_DISCARD (u4VlanIfIndex);
                }
                return (CFA_FAILURE);    /* buffer is released by GDD */
            }
#else
            /* pBuf is not used. Returning FAILURE will invoke the release of 
             * the buffer in calling place. */
            return (CFA_FAILURE);
#endif /* ARP_WANTED && (IP_WANTED || LNXIP4_WANTED) */
            break;

#ifdef IP6_WANTED
        case CFA_ENET_IPV6:
            /* if Ip FWD is disabled and if the dest MAC addr is not of the local port 
             * then send the IP6 frame to bridge. Otherwise send the frame for normal 
             * IP6 processing */
            if (gu4IsIvrEnabled == CFA_DISABLED)
            {
                if ((gu1IpForwardingEnable == CFA_FALSE) &&
                    (CfaCheckIfLocalMac (pEthHdr, u4IfIndex) != CFA_SUCCESS))
                {
                    if (CfaDuplicateCruBuffer (pBuf, &pDupBuf, u4PktSize) ==
                        CFA_SUCCESS)
                    {
                        if (L2IwfWrHandleIncomingLayer2Pkt
                            (pDupBuf, &PktHandleInfo,
                             BRG_DATA_FRAME) != CFA_SUCCESS)
                        {
                            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                                      CFA_IWF,
                                      "Exiting CfaIwfEnetProcessRxFrame - "
                                      "L2IwfWrHandleIncomingLayer2Pkt for "
                                      "interface %d - returned FAILURE\n",
                                      u4IfIndex);
                            return (CFA_FAILURE);    /* buffer is released by GDD */
                        }

                        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
                                  "In CfaIwfEnetProcessRxFrame - \
                                  Sent IP Pkt to Bridge on interface %d.\n", u4IfIndex);
                    }

                    else
                    {
                        CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                                 "Error In CfaIwfEnetProcessRxFrame - Making DUP for sending to Bridge - FAIL.\n");
                    }
                }
                u4VlanIfIndex = u4IfIndex;    /* for VRRP and VLAN enabled case */
            }
            else
            {
                /*for a L3 port-channel */
                if (L2IwfIsPortInPortChannel (u4IfIndex) == CFA_SUCCESS)
                {
                    if (L2IwfGetPortChannelForPort (u4IfIndex, &u2AggId) ==
                        L2IWF_SUCCESS)
                    {
                        u4IfIndex = u2AggId;
                    }
                }

                CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);
                if (u1BridgedIfaceStatus == CFA_ENABLED)
                {

                    u4VlanIfIndex = (UINT4) CfaGetVlanIfIndex
                        (u4IfIndex, pBuf, &VlanId);
                }
                else
                {
                    u4VlanIfIndex = u4IfIndex;
                }

                if ((CfaCheckIsLocalMac (pEthHdr, u4IfIndex,
                                         (UINT2) u4VlanIfIndex) != CFA_SUCCESS)
                    && (u1BridgedIfaceStatus == CFA_ENABLED))
                {
                    if (CfaDuplicateCruBuffer (pBuf, &pDupBuf, u4PktSize) ==
                        CFA_SUCCESS)
                    {
                        if (L2IwfWrHandleIncomingLayer2Pkt
                            (pDupBuf, &PktHandleInfo,
                             BRG_DATA_FRAME) != CFA_SUCCESS)
                        {
                            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                                      CFA_IWF,
                                      "Exiting CfaIwfEnetProcessRxFrame - "
                                      "L2IwfWrHandleIncomingLayer2Pkt for "
                                      "interface %d - FAILURE\n", u4IfIndex);
                            return (CFA_FAILURE);    /* buffer is released by GDD */
                        }

                        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
                                  "In CfaIwfEnetProcessRxFrame - Sent IP Pkt to\
                                  Bridge on interface %d.\n", u4IfIndex);
                    }
                    else
                    {
                        CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                                 "Error In CfaIwfEnetProcessRxFrame - Making DUP for sending to Bridge - FAIL.\n");
                    }
                }
                if (u1LinkFrameType == CFA_LINK_UCAST)
                {
                    /* scan the Rcv Address Table to see if the frame is addressed to local host
                       if not then drop the packet - dont send to IP */
                    if (CfaIwfCheckIsLocalEnetFrame
                        (u4IfIndex, u4VlanIfIndex,
                         (UINT1 *) pEthHdr) != CFA_SUCCESS)
                    {
#ifdef VRRP_WANTED
                        /*For Vrrp, Packet will contain virtual mac. */
                        /*So we should check whether a vrrp interface */
                        /* exists with this mac */
                        if (VrrpGetVrIdFromMacAddr (pEthHdr->au1DstAddr,
                                                    &i4OperVrId) != VRRP_OK)
#endif /* VRRP_WANTED */
                        {
                            CFA_IF_SET_IN_DISCARD (u4IfIndex);
                            return CFA_FAILURE;    /* buffer is released by GDD */
                        }
                    }
                }
                /* Apply security on the frames received */
                if (CFA_LINK_UCAST == u1LinkFrameType)
                {
                    if (u1NetworkType == CFA_NETWORK_TYPE_WAN)
                    {
                        SecIntfInfo.u4PhyIfIndex = u4IfIndex;
                        SecIntfInfo.u4VlanIfIndex = u4VlanIfIndex;

                        i4RetVal =
                            (INT4) SecProcessFrame (pBuf, &SecIntfInfo,
                                                    SEC_INBOUND, &bSecVerdict);
                        if ((OSIX_SUCCESS == i4RetVal)
                            && (SEC_STOLEN == bSecVerdict))
                        {
                            /* Indicates that the frame is stolen for security 
                             * processing. 
                             */
                            return CFA_SUCCESS;
                        }
                        if (OSIX_FAILURE == i4RetVal)
                        {
                            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                                      CFA_IWF,
                                      "Exiting CfaIwfEnetProcessRxFrame"
                                      "Security Processing for interface %d-"
                                      "FAILS\n", u4VlanIfIndex);

                            return (CFA_FAILURE);    /* buffer is released by GDD */
                        }
                    }
                }

                if (u1BridgedIfaceStatus == CFA_ENABLED)
                {
                    u4VlanIfIndex =
                        (UINT4) CfaGetVlanIfIndexAndUntagFrame
                        (u4IfIndex, pBuf, &VlanId);
                }
            }

#if ((defined (RMON2_WANTED)) && (defined (NPAPI_WANTED)))
            pData = CRU_BUF_GetDataPtr (pBuf->pFirstValidDataDesc);

            MEMSET (&PktHdrInfo, 0, sizeof (tPktHdrInfo));

            PktHdrInfo.PktHdr.u4IfIndex = u4LclIfIndex;
            PktHdrInfo.PktHdr.u4VlanIfIndex = u4VlanIfIndex;
            PktHdrInfo.PktHdr.u2VlanId = VlanId;
            PktHdrInfo.u4PktSize = u4PktSize;

            if (CfaIwfRmon2UpdatePktHdr (pData, &PktHdrInfo.PktHdr) ==
                CFA_SUCCESS)
            {
                if (Rmon2MainUpdateTables (&PktHdrInfo) != CFA_SUCCESS)
                {
                    CFA_DBG (CFA_TRC_ALL, CFA_GDD,
                             "Rmon2MainUpdateTables returns Failure\r\n");
                }
            }
            else
            {
                CFA_DBG (CFA_TRC_ALL, CFA_GDD, "In CfaGddProcessRecvEvent - \
                    failed on invoking CfaIwfRmon2UpdatePktHdr \r\n");
            }
#endif

#ifdef VRRP_WANTED
            if ((VrrpEnabledOnInterface ((INT4) u4VlanIfIndex) == VRRP_OK)
                && (VrrpGetVrIdFromMacAddr (pEthHdr->au1DstAddr,
                                            &i4OperVrId) == VRRP_OK))
            {
                bIsVrrpEnabled = TRUE;

                /* The packet is addressed to Virtual Mac Address */
                if ((VrrpGetState (u4VlanIfIndex, i4OperVrId,
                                   IPVX_ADDR_FMLY_IPV6)) == BACKUP_STATE)
                {
                    /*for backup drop all unicast packets which 
                     * are addressed for Virtual MAC Address */
                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                              CFA_IWF,
                              "Error in CfaIwfEnetProcessRxFrame - "
                              "VRRP in backup state %d.\n", u4IfIndex);
                    CFA_IF_SET_IN_ERR (u4IfIndex);
                    if (CFA_DEF_INTERFACES_IN_SYSTEM > u4VlanIfIndex)
                    {
                        CFA_IF_SET_IN_ERR (u4VlanIfIndex);
                    }
                    return (CFA_FAILURE);
                }
            }

#endif /* VRRP_WANTED */

            /* If the Received Packet == Broadcast or multcast or one of our MAC address then skip
             * this if condition otherwise drop the packet */
            if ((bIsVrrpEnabled == FALSE) &&
                (u1LinkFrameType != CFA_LINK_BCAST)
                && (!(CFA_IS_ENET_IPV6_MAC_MCAST (pEthHdr)))
                && (CfaCheckIsLocalMac (pEthHdr, u4IfIndex,
                                        (UINT2) u4VlanIfIndex) != CFA_SUCCESS))
            {
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                return (CFA_FAILURE);
            }

            u4PktSize = u4PktSize - u1EnetHeaderSize;
            if (CRU_BUF_Move_ValidOffset (pBuf, u1EnetHeaderSize) !=
                CRU_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Error in CfaIwfEnetProcessRxFrame - Cant Strip Frame\
                           on interface %d.\n", u4IfIndex);
                CFA_IF_SET_IN_ERR (u4IfIndex);
                return (CFA_FAILURE);    /* buffer is released by GDD */
            }

            if ((gu4IsIvrEnabled == CFA_ENABLED) &&
                (u4VlanIfIndex != CFA_INVALID_INDEX))
            {
                CfaInfo.u4IfIndex = u4VlanIfIndex;
#ifdef MPLS_IPV6_WANTED
                i4RetVal = CfaProcessIncomingIpv6Pkt (pBuf, u4VlanIfIndex,
                                                      u1LinkFrameType, pEthHdr);
                if (CFA_FAILURE == i4RetVal)
                {
                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                              "Exiting CfaIwfEnetProcessRxFrame - IP Pkt on interface %d - FAILURE\n",
                              u4IfIndex);
                    CFA_IF_SET_IN_DISCARD (u4IfIndex);
                    if (u4VlanIfIndex != CFA_INVALID_INDEX)
                    {
                        CFA_IF_SET_IN_DISCARD (u4VlanIfIndex);
                    }
                    return CFA_FAILURE;
                }
                else if (CFA_SUCCESS == i4RetVal)
                {
                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                              "Exiting CfaIwfEnetProcessRxFrame - MPLS Pkt on interface %d - SUCCESS\n",
                              u4IfIndex);
                    CFA_IF_SET_IN_DISCARD (u4IfIndex);
                    if (u4VlanIfIndex != CFA_INVALID_INDEX)
                    {
                        CFA_IF_SET_IN_DISCARD (u4VlanIfIndex);
                    }
                    return CFA_SUCCESS;
                }
                else if (CFA_NOTIFYV6 == i4RetVal)
#endif
                {
                    /* Send the received pkt to HL */
                    u2LenOrType = CFA_ENET_IPV6;
                    CfaInfo.CfaPktInfo.pBuf = pBuf;
                    MEMCPY (CfaInfo.CfaPktInfo.au1DstMac,
                            pEthHdr->au1DstAddr, CFA_ENET_ADDR_LEN);
                    MEMCPY (&CfaInfo.CfaPktInfo.EthHdr,
                            pEthHdr, sizeof (tEnetV2Header));

                    if (CfaNotifyIfPktRcvd (u2LenOrType, &CfaInfo) !=
                        CFA_SUCCESS)
                    {
                        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                                  "Exiting CfaIwfEnetProcessRxFrame - IP6 Pkt on interface %d - FAILURE\n",
                                  u4IfIndex);

                        CFA_IF_SET_IN_DISCARD (u4IfIndex);
                        return CFA_FAILURE;
                    }
#ifdef MPLS_IPV6_WANTED
                    else
                    {
                        CFA_DBG (CFA_TRC_ERROR, CFA_FFM,
                                 "Successfully delivered the message to IPV6 : "
                                 "Exiting CfaProcessIncomingIpv6Pkt \n");
                        return CFA_SUCCESS;
                    }
#endif
                }
            }
            else
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Exiting CfaIwfEnetProcessRxFrame - IP Pkt on interface %d - FAILURE\n",
                          u4IfIndex);

                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                return CFA_FAILURE;
            }
            break;
#endif /* IP6_WANTED */

        case CFA_PPPOE_SESSION_FRAME:
            if (CFA_LINK_UCAST == u1LinkFrameType)
            {
                SecIntfInfo.u4PhyIfIndex = u4IfIndex;
                SecIntfInfo.u4VlanIfIndex = 0;

                i4RetVal =
                    (INT4) SecProcessFrame (pBuf, &SecIntfInfo, SEC_INBOUND,
                                            &bSecVerdict);
                if ((OSIX_SUCCESS == i4RetVal) && (SEC_STOLEN == bSecVerdict))
                {
                    /* Indicates that the frame is stolen for security 
                     * processing. 
                     */
                    return CFA_SUCCESS;
                }
                if (OSIX_FAILURE == i4RetVal)
                {
                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK,
                              CFA_IWF, "Exiting CfaIwfEnetProcessRxFrame"
                              "Security Processing for interface %d-"
                              "FAILS\n", u4IfIndex);

                    CFA_IF_SET_IN_DISCARD (u4IfIndex);
                    return (CFA_FAILURE);    /* buffer is released by GDD */
                }
            }
#ifdef PPP_WANTED
            if (PPPHandlePPPoEPkt (pBuf, (UINT2) u4PktSize, (UINT2) u4IfIndex)
                == FAILURE)
            {
                return CFA_FAILURE;
            }
            return CFA_SUCCESS;
#endif /* PPP_WANTED */
            break;
        case CFA_PPPOE_DISCOVERY_FRAME:
#ifdef PPP_WANTED
            if (PPPHandlePPPoEPkt (pBuf, (UINT2) u4PktSize, (UINT2) u4IfIndex)
                == FAILURE)
            {
                return CFA_FAILURE;
            }
            return CFA_SUCCESS;
#endif /* PPP_WANTED */
            break;

#ifdef MPLS_WANTED
        case CFA_ENET_MPLS:

            if (gu4IsIvrEnabled == CFA_ENABLED)
            {
                CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);
                if (u1BridgedIfaceStatus == CFA_ENABLED)
                {
                    u4VlanIfIndex = (UINT2) CfaGetVlanIfIndexAndUntagFrame
                        (u4IfIndex, pBuf, &VlanId);
                    if (CfaCheckIsLocalMac (pEthHdr, u4IfIndex,
                                            (UINT2) u4VlanIfIndex) !=
                        CFA_SUCCESS)
                    {
                        if ((u1LinkFrameType != CFA_LINK_MCAST)
                            && (u1LinkFrameType != CFA_LINK_BCAST))
                        {
                            CFA_IF_SET_IN_DISCARD (u4IfIndex);
                            return (CFA_FAILURE);
                        }
                    }
                }
#ifndef MPLS_LNXIP4_TEST_WANTED
                else
                {
                    if (CfaCheckIfLocalMac (pEthHdr, u4IfIndex) != CFA_SUCCESS)
                    {
                        if ((u1LinkFrameType != CFA_LINK_MCAST)
                            && (u1LinkFrameType != CFA_LINK_BCAST))
                        {
                            CFA_IF_SET_IN_DISCARD (u4IfIndex);
                            return (CFA_FAILURE);
                        }
                    }
                    u4VlanIfIndex = u4IfIndex;
                }
#endif
            }
#ifdef MPLS_LNXIP4_TEST_WANTED
            if (u1BridgedIfaceStatus == CFA_DISABLED)
            {
                u4VlanIfIndex = u4IfIndex;
            }
#endif

            /* CFA_LOCK () needs to be taken inside CfaUtilGetMplsIfFromIfIndex 
             * incase of linux mode */
            if ((u4VlanIfIndex == CFA_INVALID_INDEX) ||
                (CfaUtilGetMplsIfFromIfIndex ((UINT4) u4VlanIfIndex,
                                              &u4MplsIfIndex,
                                              FALSE) == CFA_FAILURE))
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "CfaIwfEnetProcessRxFrame -E- MPLS If not enabled"
                          "on %d - FAILURE\n", u4VlanIfIndex);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                return CFA_FAILURE;
            }
            if (CfaValidateIfIndex (u4IfIndex) == CFA_FAILURE)
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Exiting CfaIwfEnetProcessRxFrame - "
                          "Invalid Vlan Interface index (%d) - FAILED\n",
                          u4IfIndex);
                return CFA_FAILURE;
            }

            if (MplsPktHandleFromCfa (pBuf, (UINT2) u4VlanIfIndex, u4PktSize) !=
                MPLS_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "CfaIwfEnetProcessRxFrame -E- MPLS Pkt Processing "
                          "on interface %d - FAILURE\n", u4IfIndex);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                return CFA_FAILURE;
            }
            break;
#endif
#ifdef ISIS_WANTED
        case CFA_NLPID_ISIS:
        case CFA_NLPID_ESIS:
            if (IssGetModuleSystemControl (ISIS_MODULE_ID) == MODULE_START)
            {
                if (gu4IsIvrEnabled == CFA_ENABLED)
                {
                    CfaGetIfBridgedIfaceStatus (u4IfIndex,
                                                &u1BridgedIfaceStatus);
                    if (u1BridgedIfaceStatus == CFA_ENABLED)
                    {
                        u4VlanIfIndex = CfaGetVlanIfIndexAndUntagFrame
                            (u4IfIndex, pBuf, &VlanId);
                    }
                    else
                    {
                        if (L2IwfIsPortInPortChannel (u4IfIndex) ==
                            L2IWF_SUCCESS)
                        {
                            L2IwfGetPortChannelForPort (u4IfIndex, &u2AggId);
                            u4VlanIfIndex = u2AggId;
                        }

                        else
                        {
                            u4VlanIfIndex = u4IfIndex;
                        }
                    }
                    if ((u4VlanIfIndex == CFA_INVALID_INDEX)
                        || (CFA_CDB_IF_OPER (u4VlanIfIndex) != CFA_IF_UP))
                    {
                        CFA_DBG (CFA_TRC_ALL, CFA_IWF,
                                 "Rxed IP Packet for Unknown Vlan Interface\n");
                        CFA_IF_SET_IN_ERR (u4IfIndex);
                        return (CFA_FAILURE);    /* buffer is released by GDD */

                    }
                }
                else
                {
                    u4VlanIfIndex = (UINT4) u4IfIndex;
                }

                /* send the packet to ISIS packet handler */
                if (IsisDlliRecv (pBuf, (UINT2) u4VlanIfIndex) != ISIS_SUCCESS)
                {

                    CFA_DBG1 (CFA_TRC_ALL, CFA_IWF,
                              "Exiting CfaIwfEnetProcessRxFrame - IsIs Pkt on interface %d - FAILURE\n",
                              u4IfIndex);

                    CFA_IF_SET_IN_DISCARD (u4IfIndex);
                    return (CFA_FAILURE);    /* buffer is released by GDD */
                }
            }
            else
            {
                CFA_DBG1 (CFA_TRC_ALL, CFA_IWF,
                          "Exiting CfaIwfEnetProcessRxFrame - IsIs Pkt on interface %d - FAILURE\n",
                          u4IfIndex);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                return (CFA_FAILURE);    /* buffer is released by GDD */
            }

            break;
#endif /* ISIS_WANTED */

        default:
            CFA_IF_SET_IN_UNK (u4IfIndex);

            /* all unknown packets are sent to bridge */
            /* LA and PNAC control packets fall under this category */
            CFA_DBG2 (CFA_TRC_ALL_TRACK, CFA_IWF,
                      "In CfaIwfEnetProcessRxFrame - Sent UNK Protocol %x Frame on interface %d to Bridge\n",
                      u2Protocol, u4IfIndex);

            if ((u2Protocol == MRP_MVRP_ETHER_TYPE) ||
                (u2Protocol == MRP_MMRP_ETHER_TYPE))
            {
                u1FrameType = BRG_MRP_FRAME;
            }

            if (CFA_IF_IS_BACKPLANE (u4IfIndex) != CFA_BACKPLANE_INTF)
            {
                L2IwfWrHandleIncomingLayer2Pkt (pBuf, &PktHandleInfo,
                                                u1FrameType);
                /* Nothing needs to be done for normal interfaces. Hence
                 * breaking here. 
                 * */
                break;
            }

            /* Back plane interface. Extract the necessary params
             * and then indicate L2IWF
             * */
            switch (u1FrameType)
            {
                case BRG_MRP_FRAME:

                    /* MRP Frame. Extract the context Identifier from it */
                    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *)
                                               &(BackPlaneParams.u4ContextId),
                                               MRP_CONTEXT_ID_OFFSET,
                                               MRP_CONTEXT_ID_LEN);
                    BackPlaneParams.u4ContextId =
                        OSIX_NTOHS (BackPlaneParams.u4ContextId);

                    if (VcmIsL2VcExist (BackPlaneParams.u4ContextId) !=
                        VCM_TRUE)
                    {
                        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
                                  "Invalid Ctxt identifier received over backplane "
                                  "interface %d.\n", u4IfIndex);
                        return CFA_FAILURE;
                    }

                    pCruBuf = CfaCopyFrmPduForBackPlaneIntf (pBuf,
                                                             MRP_CONTEXT_ID_OFFSET,
                                                             MRP_CONTEXT_ID_LEN);
                    break;

                default:
                    /* nothing needs to be done here */
                    break;
            }

            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            /* pCruBuf will be released by the corresponding module
             * */
            if (pCruBuf == NULL)
            {
                return CFA_FAILURE;
            }
            L2IwfHandleIncominPktOnBackPlane (pCruBuf, &BackPlaneParams,
                                              u4IfIndex, u1FrameType);
            break;
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
              "Exiting CfaIwfEnetProcessRxFrame with SUCCESS for interface %d.\n",
              u4IfIndex);

    return (CFA_SUCCESS);
}

#ifdef WTP_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaIwfWssProcessRxFrame
 *
 *    Description        : This function processes incoming frames on
 *                Radio interfaces. The frame is send to the MAC Handler for
 *                further processing.
 *
 *    Input(s)            : tCRU_BUF_CHAIN_HEADER *pBuf,
 *                UINT4 u4IfIndex,
 *                UINT4 u4PktSize.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if frame is processed
 *                succeessfully, otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIwfWssProcessRxFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                         UINT4 u4PktSize)
{
    unApHdlrMsgStruct   CapwapMsgStruct;
    UNUSED_PARAM (u4PktSize);

    CFA_DBG1 (CFA_TRC_ALL, CFA_IWF,
              "Entering CfaIwfWssProcessRxFrame for interface %d.\n",
              u4IfIndex);

    MEMSET (&CapwapMsgStruct, 0, sizeof (unApHdlrMsgStruct));

    if (pBuf == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                  "Null Packet received %d \r\n", u4IfIndex);
        return OSIX_FAILURE;

    }

    /* Fill the MAC Handler message structure */
    CapwapMsgStruct.ApHdlrQueueReq.u4MsgType = APHDLR_CFA_RX_MSG;
    CapwapMsgStruct.ApHdlrQueueReq.pRcvBuf = pBuf;

    /* Invoke the WSSIF module to send the packet to MAC Handler module */
    if (WssIfProcessApHdlrMsg (WSS_APHDLR_CFA_QUEUE_REQ, &CapwapMsgStruct) !=
        OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return CFA_FAILURE;
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
              "Exiting CfaIwfWssProcessRxFrame with SUCCESS for interface %d.\n",
              u4IfIndex);

    return CFA_SUCCESS;
}
#endif

/*****************************************************************************
 *
 *    Function Name        : CfaDuplicateCruBuffer
 *
 *    Description        : This function makes a copy the CRU buffer by making
 *                use of the linear buffer and the FSAP calls.
 *
 *    Input(s)            : tCRU_BUF_CHAIN_HEADER *pBuf,
 *                        tCRU_BUF_CHAIN_HEADER *pDupBuf,
 *                          UINT4 u4PktSize.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if frame is processed
 *                succeessfully, otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaDuplicateCruBuffer (tCRU_BUF_CHAIN_HEADER * pBuf,
                       tCRU_BUF_CHAIN_HEADER ** pDupBuf, UINT4 u4PktSize)
{
    UINT1              *pu1DataBuf = NULL;

    CFA_DBG (CFA_TRC_ALL, CFA_IWF, "Entering CfaDuplicateCruBuffer.\n");

    /* Allocate the Buffer chain for the Duplicate buffer */
    if (((*pDupBuf) = CRU_BUF_Allocate_MsgBufChain ((u4PktSize +
                                                     CFA_MAX_FRAME_HEADER_SIZE),
                                                    CFA_MAX_FRAME_HEADER_SIZE))
        == NULL)
    {
        return CFA_FAILURE;
    }
    else
    {
        /* Allocate memory for the linear buffer */
        if ((pu1DataBuf = MemAllocMemBlk (gCfaDriverMtuPoolId)) == NULL)
        {
            CRU_BUF_Release_MsgBufChain ((*pDupBuf), FALSE);
            return CFA_FAILURE;
        }
        else
        {
            /* Copy from the pBuf to the Linear buffer */
            if (CRU_BUF_Copy_FromBufChain (pBuf, pu1DataBuf, 0, u4PktSize)
                == CFA_FAILURE)
            {
                CRU_BUF_Release_MsgBufChain ((*pDupBuf), FALSE);
                MemReleaseMemBlock (gCfaDriverMtuPoolId, (UINT1 *) pu1DataBuf);
                return CFA_FAILURE;
            }
            else
            {
                /* Copy from linear buffer to the CRU buffer */
                if (CRU_BUF_Copy_OverBufChain
                    ((*pDupBuf), pu1DataBuf, 0, u4PktSize) == CFA_FAILURE)
                {
                    CRU_BUF_Release_MsgBufChain ((*pDupBuf), FALSE);
                    MemReleaseMemBlock (gCfaDriverMtuPoolId,
                                        (UINT1 *) pu1DataBuf);
                    return CFA_FAILURE;
                }
            }
        }
    }

    MemReleaseMemBlock (gCfaDriverMtuPoolId, (UINT1 *) pu1DataBuf);
    CFA_DBG (CFA_TRC_ALL, CFA_IWF, "Exiting CfaDuplicateCruBuffer.\n");
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaIwfEnetProcessTxFrame
 *
 *    Description        : This function processes outgoing frames on
 *                Enet interfaces. Only Enet V2 encapsulation
 *                is currently supported. Support for SNAP is
 *                to be added. The frame is send to the GDD
 *                routine for processing.
 *
 *    Input(s)            : tCRU_BUF_CHAIN_HEADER *pBuf,
 *                UINT4 u4IfIndex,
 *                UINT1 *au1DestHwAddr.
 *                UINT4 u4PktSize.
 *                UINT2 u2Protocol.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if frame is processed
 *                succeessfully, otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIwfEnetProcessTxFrame (tCRU_BUF_CHAIN_HEADER * pCruBuf, UINT4 u4IfIndex,
                          UINT1 *au1DestHwAddr, UINT4 u4PktSize,
                          UINT2 u2Protocol, UINT1 u1EncapType)
{
    tSecIntfInfo        SecIntfInfo;
    INT4                i4RetVal = CFA_SUCCESS;
    UINT4               u4Dest = 0;
    UINT4               u4PhyIfIndex = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4UapIfIndex = 0;
    UINT2               u2SVID = 0;
    UINT1               u1EnetHeaderSize = 0;
    UINT2               u2TempSize;
    UINT2               u2PortIndex = 0;    /* L3 port-channel */
    tEnetSnapHeader    *pSnapHeader = NULL;
    tEnetLlcHeader     *pLlcHeader;
    tCfaBackPlaneParams *pBackPlaneParams = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tCfaIfInfo          IfInfo;
    UINT1               u1LinkFrameType;
    UINT1               u1VlanTagCheck = CFA_TRUE;

    UINT1               au1TempDestHwAddr[CFA_ENET_ADDR_LEN];
    UINT1               u1IfType = 0;

#if defined (MPLS_WANTED) || defined (OPENFLOW_WANTED)
    tEnetV2Header       EnetV2Header;
#endif /* MPLS_WANTED or OPENFLOW_WANTED */

#ifdef VRRP_WANTED
    UINT1               au1SrcVrrpMac[CFA_ENET_ADDR_LEN] =
        { 0x00, 0x00, 0x5E, 0x00, 0x01, 0x00 };
    UINT1               au1DestVrrpMac[CFA_ENET_ADDR_LEN] =
        { 0x01, 0x00, 0x5E, 0x00, 0x00, 0x12 };
    UINT1               au1DestVrrpIpv6Mac[CFA_ENET_ADDR_LEN] =
        { 0x33, 0x33, 0x00, 0x00, 0x00, 0x12 };
    UINT1               au1SrcVrrpIpv6Mac[CFA_ENET_ADDR_LEN] =
        { 0x00, 0x00, 0x5E, 0x00, 0x02, 0x00 };
    UINT1               au1DestMacAddr[CFA_ENET_ADDR_LEN];
    UINT1               u1VrId = 0;
#endif /* VRRP_WANTED */
    UINT1               au1IfHwAddr[ETHERNET_ADDR_SIZE];
#ifdef NPAPI_WANTED
#ifdef PPP_WANTED
    tPPPoEHeader        PPPoEHeader;
#endif /* PPP_WANTED */
#endif /* NPAPI_WANTED */
    BOOL1               bSecVerdict = SEC_NOT_STOLEN;

    UINT1               u1IsSecWantedForBridgedTraffic = OSIX_FALSE;
    UINT1               u1CruBufFlag = OSIX_FALSE;
    UINT1               u1SubType = 0;
    UINT1               u1NetworkType = 0;
#ifdef OPENFLOW_WANTED
    UINT4               u4SrcIpAddr = ZERO;
    UINT4               u4DstIpAddr = ZERO;
    UINT4               u4SrcIpOff =
        (ARP_STANDARD_HDR_SIZE + CFA_ENET_ADDR_LEN);
    UINT4               u4DstIpOff = (ARP_PACKET_LEN - ARP_MAX_PROTO_ADDR_LEN);
#endif /* OPENFLOW_WANTED */

    MEMSET (&SecIntfInfo, 0, sizeof (tSecIntfInfo));

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        /* Memory should be freed wherever it is called */
        return CFA_FAILURE;
    }
    /* Inbound Frame is received here after security processing.
     * Frame should be posted back again to CFA for routing. 
     */
    CfaGetIfNwType (u4IfIndex, &u1NetworkType);

    if (SEC_TO_CFA == CFA_GET_COMMAND (pCruBuf))
    {
        /* post the buffer to CFA queue */
        i4RetVal = (INT4) OsixQueSend (CFA_PACKET_QID,
                                       (UINT1 *) &pCruBuf, OSIX_DEF_MSG_LEN);
        if (i4RetVal != OSIX_SUCCESS)
        {
            return CFA_FAILURE;
        }

        /* trigger the interuppt event for CFA */
        OsixEvtSend (CFA_TASK_ID, CFA_PACKET_ARRIVAL_EVENT);
        return CFA_SUCCESS;
    }

    if (IfInfo.u1IfType == CFA_VPNC)
    {
        SecIntfInfo.u4VpncIfIndex = u4IfIndex;
        u1EncapType = CFA_ENCAP_ENETV2;
        if (VpnGetAssoIntfForVpnc (pCruBuf, &u4IfIndex, &u4Dest) == VPN_FAILURE)
        {
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                      "Exiting CfaIwfEnetProcessTxFrame - Fail to get "
                      "associated interface for VPNC interface %d - "
                      "FAILURE\n", u4IfIndex);
            return CFA_FAILURE;
        }
        if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
        {
            /* Memory should be freed wherever it is called */
            return CFA_FAILURE;
        }
        /* ARP is resolved for WAN interface, when packets are received 
         * through VPNC interface */
        if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId) ==
            VCM_FAILURE)
        {
            return CFA_FAILURE;
        }

        if (ArpResolveInCxt (u4Dest, (INT1 *) au1DestHwAddr,
                             &u1EncapType, u4ContextId) == ARP_SUCCESS)
        {
            CFA_SET_DEST_MEDIA_ADDR (pCruBuf, au1DestHwAddr);
        }
    }
    if ((IfInfo.u1IfType == CFA_LAGG) && (IfInfo.u1BridgedIface != CFA_ENABLED))
    {
        if (L2IwfGetPotentialTxPortForAgg ((UINT2) u4IfIndex, &u2PortIndex) ==
            LA_SUCCESS)
        {
            /* send through the active port of the portchannel */
            u4IfIndex = (UINT4) u2PortIndex;
        }
        if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
        {
            return CFA_FAILURE;
        }

    }

    CFA_DBG1 (CFA_TRC_ALL, CFA_IWF,
              "Entering CfaIwfEnetProcessTxFrame for interface %d.\n",
              u4IfIndex);

    /* If the interface is a backplane interface, then get the backplane
     * params from CFA Q and push it into the PDU */
    if (CFA_IF_IS_BACKPLANE (u4IfIndex) == CFA_BACKPLANE_INTF)
    {
        if (OsixQueRecv (CFA_PACKET_PARAMS_QID, (UINT1 *) &pBackPlaneParams,
                         OSIX_DEF_MSG_LEN, 0) != OSIX_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ALL, CFA_IWF,
                      "Not able to receive Backplane parameters received "
                      "over interface %d.\n", u4IfIndex);
            return CFA_FAILURE;
        }

        pBuf = CfaCopyOverPduForBackPlaneIntf (pCruBuf, pBackPlaneParams,
                                               MRP_CONTEXT_ID_OFFSET,
                                               MRP_CONTEXT_ID_LEN);
        if (pBuf == NULL)
        {
            return CFA_FAILURE;
        }
        u1CruBufFlag = OSIX_TRUE;
    }
    else
    {
        pBuf = pCruBuf;
    }

    if (IfInfo.u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        /* Get SVID and UAP If Index */
        if (VlanApiGetSChInfoFromSChIfIndex (u4IfIndex, &u4UapIfIndex,
                                             &u2SVID) == CFA_FAILURE)
        {
            if (u1CruBufFlag == OSIX_TRUE)
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            }
            return CFA_FAILURE;
        }
        /* Resetting the UAP IfIndex to transmit out the pkt over physical
         * interface. */
        u4IfIndex = u4UapIfIndex;
        /* Add S-Tag on the frame */
        if (VlanAddOuterTagInFrame (pBuf, u2SVID, 0, u4IfIndex) != VLAN_FORWARD)
        {
            if (u1CruBufFlag == OSIX_TRUE)
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            }
            return CFA_FAILURE;
        }
    }
#ifdef OPENFLOW_WANTED
    /*
     * OpenFlow Hybrid implementation uses user configured Tunnel
     * to acheive communication between OpenFlow and ISS (L2/L3)
     */
    if (IfInfo.u1IfType == CFA_TUNNEL)
    {
        /* lift the packet for Openflow hybrid processing */
        if (OfcHandleOpenflowSwModeProcess (&u4IfIndex, pBuf) == OFC_SUCCESS)
        {
            /*
             * while the ENCAP type is only ENETV2 we support,
             * the protocol can be only ARP or IPv4 
             */
            if (!((u2Protocol == CFA_ENET_IPV4) ||
                  (u2Protocol == CFA_ENET_ARP)))
            {
                if (u1CruBufFlag == OSIX_TRUE)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                }
                return CFA_FAILURE;
            }
            MEMSET (&EnetV2Header, ZERO, sizeof (EnetV2Header));

            EnetV2Header.u2LenOrType = OSIX_HTONS (u2Protocol);
            MEMCPY (EnetV2Header.au1DstAddr, au1DestHwAddr, CFA_ENET_ADDR_LEN);

            /* Fill in the Switch Mac as Source */
            CfaGetSysMacAddress (EnetV2Header.au1SrcAddr);

            if (u2Protocol == CFA_ENET_ARP)
            {
                CRU_BUF_Copy_OverBufChain (pBuf, EnetV2Header.au1SrcAddr,
                                           ARP_STANDARD_HDR_SIZE,
                                           CFA_ENET_ADDR_LEN);
                /*
                 * Get the IP Address of matching Interface in order
                 * to fill in the SrcIp which will not be filled otherwise
                 */
                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4SrcIpAddr,
                                           u4SrcIpOff, ARP_MAX_PROTO_ADDR_LEN);
                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4DstIpAddr,
                                           u4DstIpOff, ARP_MAX_PROTO_ADDR_LEN);

                u4SrcIpAddr = OSIX_NTOHL (u4SrcIpAddr);
                u4DstIpAddr = OSIX_NTOHL (u4DstIpAddr);
                if (u4SrcIpAddr == ZERO)
                {
                    if (CfaIpIfGetSrcAddress (u4DstIpAddr, &u4SrcIpAddr) ==
                        CFA_FAILURE)
                    {
                        if (u1CruBufFlag == OSIX_TRUE)
                        {
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        }
                        return CFA_FAILURE;
                    }
                    u4SrcIpAddr = OSIX_HTONL (u4SrcIpAddr);
                    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4SrcIpAddr,
                                               u4SrcIpOff,
                                               ARP_MAX_PROTO_ADDR_LEN);
                }
            }
            i4RetVal = CRU_BUF_Prepend_BufChain (pBuf, (UINT1 *) &EnetV2Header,
                                                 CFA_ENET_V2_HEADER_SIZE);
            if (i4RetVal != CRU_SUCCESS)
            {
                if (u1CruBufFlag == OSIX_TRUE)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                }
                return CFA_FAILURE;
            }

            if (OfcHybridHandlePktFromIss (u4IfIndex, pBuf) == OFC_SUCCESS)
            {
                return CFA_SUCCESS;
            }
            return CFA_FAILURE;
        }

        if (u1CruBufFlag == OSIX_TRUE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        }
        return CFA_FAILURE;
    }
#endif /* OPENFLOW_WANTED */

    /* bridged data packets come with CFA_ENCAP_NONE = 0 - we dont have
     * to do any encap for them */

    switch (u1EncapType)
    {
        case CFA_ENCAP_ENETV2:
            u1EnetHeaderSize = CFA_ENET_V2_HEADER_SIZE;
            break;

        case CFA_ENCAP_LLC_SNAP:
            u1EnetHeaderSize = CFA_ENET_SNAP_HEADER_SIZE;
            break;

        case CFA_ENCAP_LLC:
            u1EnetHeaderSize = CFA_ENET_LLC_HEADER_SIZE;
            break;

        case CFA_ENCAP_NLPID:
            u1EnetHeaderSize = CFA_ENET_LLC_HEADER_SIZE;
            break;
        default:
            break;
    }

/* now prepend the pre-formed header on the basis of the protocol and 
encapsulation type */

    if ((u1EncapType == CFA_ENCAP_ENETV2) || (u1EncapType == CFA_ENCAP_OTHER))
    {
        switch (u2Protocol)
        {

            case CFA_ENET_IPV4:
                if (CRU_BUF_Prepend_BufChain (pBuf,
                                              (UINT1 *)
                                              &CFA_ENET_IWF_V2_IPHDR
                                              (u4IfIndex),
                                              CFA_ENET_V2_HEADER_SIZE) !=
                    CRU_SUCCESS)
                {
                    i4RetVal = CFA_FAILURE;
                }

                break;

            case CFA_ENET_ARP:
                if (CRU_BUF_Prepend_BufChain (pBuf,
                                              (UINT1 *) &CFA_ENET_IWF_V2_ARPHDR
                                              (u4IfIndex),
                                              CFA_ENET_V2_HEADER_SIZE) !=
                    CRU_SUCCESS)
                {
                    i4RetVal = CFA_FAILURE;
                }
                break;
#ifdef IP6_WANTED
            case CFA_ENET_IPV6:
                if (CRU_BUF_Prepend_BufChain (pBuf,
                                              (UINT1 *)
                                              &CFA_ENET_IWF_V2_IP6HDR
                                              (u4IfIndex),
                                              CFA_ENET_V2_HEADER_SIZE) !=
                    CRU_SUCCESS)
                {
                    i4RetVal = CFA_FAILURE;
                }
                break;
#endif /* IP6_WANTED  */

            case CFA_ENET_RARP:
                if (CRU_BUF_Prepend_BufChain (pBuf,
                                              (UINT1 *) &CFA_ENET_IWF_V2_RARPHDR
                                              (u4IfIndex),
                                              CFA_ENET_V2_HEADER_SIZE) !=
                    CRU_SUCCESS)
                {
                    i4RetVal = CFA_FAILURE;
                }
                break;

#ifdef MPLS_WANTED
            case CFA_ENET_MPLS:

                MEMCPY (&EnetV2Header, &CFA_ENET_IWF_V2_IPHDR (u4IfIndex),
                        sizeof (tEnetV2Header));
                EnetV2Header.u2LenOrType = OSIX_HTONS (CFA_ENET_MPLS);
                CFA_GET_DEST_MEDIA_ADDR (pBuf, EnetV2Header.au1DstAddr);
                if (CRU_BUF_Prepend_BufChain (pBuf, (UINT1 *) &EnetV2Header,
                                              CFA_ENET_V2_HEADER_SIZE) !=
                    CRU_SUCCESS)
                {
                    i4RetVal = CFA_FAILURE;
                }
                break;
#endif /* MPLS_WANTED */

#ifdef PPP_WANTED
            case CFA_PPPOE_SESSION_FRAME:
            case CFA_PPPOE_DISCOVERY_FRAME:
                /* Since for PPPoE the packet comes with the encapsulation the
                 * Ethernet header length has to be reset */
#ifdef NPAPI_WANTED
                PPPoEExtractHeader (pBuf, &PPPoEHeader);
#endif /* NPAPI_WANTED */
                u1EnetHeaderSize = 0;
                break;
#endif /* PPP_WANTED */

            default:
                i4RetVal = CFA_FAILURE;
                break;
        }                        /* end if enet switch */
    }                            /* end of enet encap */

    if (u1EncapType == CFA_ENCAP_LLC_SNAP)
    {
        switch (u2Protocol)
        {

            case CFA_ENET_IPV4:
                if (CRU_BUF_Prepend_BufChain (pBuf,
                                              (UINT1 *) &CFA_ENET_IWF_SNAP_IPHDR
                                              (u4IfIndex),
                                              CFA_ENET_SNAP_HEADER_SIZE) !=
                    CRU_SUCCESS)
                {
                    i4RetVal = CFA_FAILURE;
                }
                break;

            case CFA_ENET_ARP:
                if (CRU_BUF_Prepend_BufChain (pBuf,
                                              (UINT1 *)
                                              &CFA_ENET_IWF_SNAP_ARPHDR
                                              (u4IfIndex),
                                              CFA_ENET_SNAP_HEADER_SIZE) !=
                    CRU_SUCCESS)
                {
                    i4RetVal = CFA_FAILURE;
                }
                break;

            case CFA_ENET_RARP:
                if (CRU_BUF_Prepend_BufChain (pBuf,
                                              (UINT1 *)
                                              &CFA_ENET_IWF_SNAP_RARPHDR
                                              (u4IfIndex),
                                              CFA_ENET_SNAP_HEADER_SIZE) !=
                    CRU_SUCCESS)
                {
                    i4RetVal = CFA_FAILURE;
                }
                break;

            default:
                i4RetVal = CFA_FAILURE;
                break;
        }                        /* end of switch case */

/* now update the size of the frame in the buffer */
        u2TempSize = (UINT2) u4PktSize;
        pSnapHeader = (tEnetSnapHeader *) (VOID *)
            CRU_BMC_Get_DataPointer (pBuf);
        pSnapHeader->u2LenOrType = OSIX_HTONS (u2TempSize);

    }                            /* end of encap checking */

    if (u1EncapType == CFA_ENCAP_LLC)
    {
        switch (u2Protocol)
        {

            case CFA_PROT_BPDU:
                if (CRU_BUF_Prepend_BufChain (pBuf,
                                              (UINT1 *)
                                              &CFA_ENET_IWF_LLC_BPDUHDR
                                              (u4IfIndex),
                                              CFA_ENET_LLC_HEADER_SIZE) !=
                    CRU_SUCCESS)
                {
                    i4RetVal = CFA_FAILURE;
                }
                break;
            default:
                break;

        }

        /*
         * The 'u4PktSize' getting passed is the size of the BPDUs only
         * The length in the type/length field of the mac frame, is
         * size of the mac client data following the type/length field
         * (ie) BPDU length + LLC header size
         */
        u2TempSize = (UINT2) (u4PktSize + CFA_LLC_HEADER_SIZE);
        pLlcHeader = (tEnetLlcHeader *) (VOID *) CRU_BMC_Get_DataPointer (pBuf);
        u2TempSize = OSIX_HTONS (u2TempSize);
        MEMCPY (&pLlcHeader->u2LenOrType, &u2TempSize, 2);
    }

#ifdef ISIS_WANTED
    if (u1EncapType == CFA_ENCAP_NLPID)
    {
        switch (u2Protocol)
        {
            case CFA_NLPID_ISIS:
                pLlcHeader =
                    (tEnetLlcHeader *) (VOID *) CRU_BMC_Get_DataPointer (pBuf);
                break;

            default:
                i4RetVal = CFA_FAILURE;
                break;
        }
    }
#endif

/* check is encap was successful */
    if (i4RetVal == CFA_FAILURE)
    {
        CFA_IF_SET_OUT_ERR (u4IfIndex);

        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                  "Exiting CfaIwfEnetProcessTxFrame - Prepend fail for interface %d - FAILURE\n",
                  u4IfIndex);

        /* pBuf would have been newly allocated. Free The same here.
         * Since we are returning failure, pCruBuff will be freed by this
         * calling routine. */
        if (u1CruBufFlag == OSIX_TRUE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        }
        return (CFA_FAILURE);    /* buf released by FFM or GDD-incoming */
    }
    else
    {
        /* now copy the destination address provided */
        /* if bridge is present then we have to handle bridged data also - dest
         * address already available in the frame */
        if (u1EncapType != CFA_ENCAP_NONE)
        {
#ifdef ISIS_WANTED
            if (u1EncapType != CFA_ENCAP_NLPID)
#endif
            {
                CRU_BUF_Copy_OverBufChain (pBuf, au1DestHwAddr, 0,
                                           CFA_ENET_ADDR_LEN);
            }

        }
        else
        {

            if (au1DestHwAddr == NULL)
                au1DestHwAddr = au1TempDestHwAddr;
            /* get the destination address for updation of counters */
            CRU_BUF_Copy_FromBufChain (pBuf, au1DestHwAddr,
                                       0, CFA_ENET_ADDR_LEN);
        }

        CFA_DBG2 (CFA_TRC_ALL_TRACK, CFA_IWF,
                  "In CfaIwfEnetProcessTxFrame - Sending Frame on interface %d Protocol %d.\n",
                  u4IfIndex, u2Protocol);

        CFA_DUMP (CFA_TRC_ENET_PKT_DUMP, pBuf);

/* we determine the type of Enet Frame */
        if (CFA_IS_ENET_MAC_BCAST (au1DestHwAddr))
        {
            u1LinkFrameType = CFA_LINK_BCAST;
            CFA_IF_SET_OUT_BCAST (u4IfIndex);
        }
        else if (CFA_IS_ENET_MAC_MCAST (au1DestHwAddr))
        {
            u1LinkFrameType = CFA_LINK_MCAST;
            CFA_IF_SET_OUT_MCAST (u4IfIndex);
        }
        else
        {
            u1LinkFrameType = CFA_LINK_UCAST;
            CFA_IF_SET_OUT_UCAST (u4IfIndex);
        }

/* if bridge is present then we have to handle bridged data also */

        if (u1EncapType != CFA_ENCAP_NONE)
        {
#ifdef ISIS_WANTED
            if (u1EncapType != CFA_ENCAP_NLPID)
#endif
            {

/* Packet size is increased as buffer has been prepended
 * for Ethernet header.*/
                u4PktSize += u1EnetHeaderSize;
            }
        }

        if ((IfInfo.u1IfType == CFA_L3IPVLAN) ||
            (IfInfo.u1IfType == CFA_L3SUB_INTF) ||
            ((IfInfo.u1IfType == CFA_PSEUDO_WIRE)
             && (IfInfo.u1BridgedIface == CFA_DISABLED)) ||
            ((IfInfo.u1IfType == CFA_ENET)
             && (IfInfo.u1BridgedIface == CFA_DISABLED)) ||
            ((IfInfo.u1IfType == CFA_LAGG)
             && (IfInfo.u1BridgedIface == CFA_DISABLED)) ||
            ((IfInfo.u1IfType == CFA_OTHER)
             && (IfInfo.u1BridgedIface == CFA_DISABLED))
#ifdef WGS_WANTED
            || (IfInfo.u1IfType == CFA_L2VLAN)
#endif
            )
        {
            CfaGetIfHwAddr (u4IfIndex, au1IfHwAddr);

            /* Copy the source MAC address before giving the packet to VLAN 
             * since it is coming from IP/ARP */
            CRU_BUF_Copy_OverBufChain (pBuf, au1IfHwAddr, CFA_ENET_ADDR_LEN,
                                       CFA_ENET_ADDR_LEN);
#ifdef VRRP_WANTED

            CRU_BUF_Copy_FromBufChain (pBuf, au1DestMacAddr,
                                       0, CFA_ENET_ADDR_LEN);

            /*Check whether this is a VRRP packet, if yes needs to replace the
             * Source mac address. Destination mac for VRRP packets will be
             * 01:00:5e:00:00:12*/
            if (MEMCMP (au1DestMacAddr, au1DestVrrpMac, CFA_ENET_ADDR_LEN) == 0)
            {
                CRU_BUF_Copy_FromBufChain (pBuf, &u1VrId,
                                           CFA_VRRP_VRID_OFFSET, 1);
                au1SrcVrrpMac[CFA_ENET_ADDR_LEN - 1] = u1VrId;
                CRU_BUF_Copy_OverBufChain (pBuf, au1SrcVrrpMac,
                                           CFA_ENET_ADDR_LEN,
                                           CFA_ENET_ADDR_LEN);
            }
            else if (MEMCMP (au1DestMacAddr, au1DestVrrpIpv6Mac,
                             CFA_ENET_ADDR_LEN) == 0)
            {
                CRU_BUF_Copy_FromBufChain (pBuf, &u1VrId,
                                           CFA_VRRP_IPV6_VRID_OFFSET, 1);
                au1SrcVrrpIpv6Mac[CFA_ENET_ADDR_LEN - 1] = u1VrId;
                CRU_BUF_Copy_OverBufChain (pBuf, au1SrcVrrpIpv6Mac,
                                           CFA_ENET_ADDR_LEN,
                                           CFA_ENET_ADDR_LEN);
            }
#endif /* VRRP_WANTED */
        }
        /* send the packet to the GDD module for transmission - dont 
         * return Failure even if GDDWrite fails, as the buf will be release
         * by the GDD in any case. */

        if (u2Protocol == CFA_PROT_BPDU)
        {
            /* This packet is sent by RSTP, LA etc.. where packets are not 
             * tagged. In such packets, we need not check for VLAN Tag */
            u1VlanTagCheck = CFA_FALSE;
        }

        /* Apply security on the frames sent out on WAN interface.
         */
        /* Bridged Packets Over Wan Interface should be Secured */
        /* User can configure the Set of VLAN's on which the Bridged
           Packets should be secured */
        /* Security Processing is done only if the port is a Tagged Member Port
           of any of the configured VLAN's */
        CfaChkSecForOutBoundBridgedFrame (pBuf, u4IfIndex,
                                          &u1IsSecWantedForBridgedTraffic);
        if (u1IsSecWantedForBridgedTraffic == OSIX_TRUE)
        {
            SecIntfInfo.u4VlanIfIndex = CfaGetSecIvrIndex ();
        }

        /* Apply security processing in the following scenarios. 
         * (i)Unicast IPV4/IPV6/PPP Traffic via router interfaces/non-security 
         *      IVR interfaces.     
         * (ii) Unicast Traffic via security IVR interface, properly qualified by 
         *     CfaCheckSecForOutboundTraffic.
         */
        if ((((CfaGetSecIvrIndex () != u4IfIndex) &&
              ((CFA_ENET_IPV4 == u2Protocol) ||
               (CFA_ENET_IPV6 == u2Protocol) ||
               (CFA_PPPOE_SESSION_FRAME == u2Protocol))) ||
             (u1IsSecWantedForBridgedTraffic == OSIX_TRUE)) &&
            (CFA_LINK_UCAST == u1LinkFrameType))
        {
            if (u1NetworkType == CFA_NETWORK_TYPE_WAN)
            {
                SecIntfInfo.u4PhyIfIndex = u4IfIndex;

                i4RetVal =
                    (INT4) SecProcessFrame (pBuf, &SecIntfInfo, SEC_OUTBOUND,
                                            &bSecVerdict);
                if ((OSIX_SUCCESS == i4RetVal) && (SEC_STOLEN == bSecVerdict))
                {
                    /* The frame is stolen for security processing.
                     * IPSec takes care of writing the frames out on the 
                     * interface. Buffer memory will not be freed here.
                     */
                    return CFA_SUCCESS;
                }

                if (OSIX_FAILURE == i4RetVal)
                {
                    CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
                    return CFA_SUCCESS;
                }
            }
        }

        u4PktSize = IPSEC_BUF_GetValidBytes (pBuf);
        CfaGetIfType (u4IfIndex, &u1IfType);
        CfaGetIfSubType (u4IfIndex, &u1SubType);

        if ((u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
            (u1SubType == CFA_SUBTYPE_AC_INTERFACE))
        {
            CfaGetIfACPortIdentifier (u4IfIndex, &u4PhyIfIndex);
            u4IfIndex = u4PhyIfIndex;
        }
#ifdef MPLS_WANTED
        /*Add for pseudowire l2 visibility */
        CfaGetIfType (u4IfIndex, &u1IfType);
        if (u1IfType == CFA_PSEUDO_WIRE)
        {
            if (CfaIwfMplsProcessOutGoingL2Pkt (pBuf, u4IfIndex, 0,
                                                CFA_LINK_UCAST) == MPLS_SUCCESS)
            {
                /*Updating OutOctet information for Pseudowire */
                CFA_IF_SET_OUT_OCTETS (u4IfIndex, u4PktSize);
                return CFA_SUCCESS;
            }
            return CFA_FAILURE;
        }
#endif

#ifdef VXLAN_WANTED
        CfaGetIfType (u4IfIndex, &u1IfType);
        if (u1IfType == CFA_VXLAN_NVE)
        {
            if (VxlanProcessL2Pkt (pBuf, u4IfIndex) == OSIX_SUCCESS)
            {
                /*Updating OutOctet information for Pseudowire */
                CFA_IF_SET_OUT_OCTETS (u4IfIndex, u4PktSize);
                return CFA_SUCCESS;
            }
            return CFA_FAILURE;
        }
#endif

#ifdef VCPEMGR_WANTED
        CfaGetIfType (u4IfIndex, &u1IfType);
        if (u1IfType == CFA_TAP)
        {
            if (CfaGddTunTapWrite (pBuf, u4IfIndex) == OSIX_SUCCESS)
            {
                /*Updating OutOctet information for Pseudowire */
                CFA_IF_SET_OUT_OCTETS (u4IfIndex, u4PktSize);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Exiting CfaIwfEnetProcessTxFrame - write on "
                          "TAP interface %d - SUCCESS\n", u4IfIndex);
                return CFA_SUCCESS;
            }
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                     "Exiting CfaIwfEnetProcessTxFrame - "
                     "CfaGddTunTapWrite failed\n");
            return CFA_FAILURE;
        }
#endif

        if (CfaGddWrite (pBuf, u4IfIndex, u4PktSize, u1VlanTagCheck, &IfInfo) !=
            CFA_SUCCESS)
        {

            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                      "Exiting CfaIwfEnetProcessTxFrame - GDD send fail on "
                      "interface %d - FAILURE\n", u4IfIndex);
        }
        else
        {
            CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
                      "Exiting CfaIwfEnetProcessTxFrame - write on "
                      "interface %d - SUCCESS\n", u4IfIndex);
        }

        /* pBuf will be freed by the above function. The original buffer pCruBuf
         * needs to be freed */
        if (CFA_IF_IS_BACKPLANE (u4IfIndex) == CFA_BACKPLANE_INTF)
        {
            CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
        }
        return (CFA_SUCCESS);
    }
}

/*****************************************************************************
 *
 *    Function Name        : CfaIwfEnetFormEnetMcastAddr
 *
 *    Description        : This function converts the multicast IP
 *                address to the Mulicast Enet.
 *
 *    Input(s)            : UINT4 u4IpAddr,
 *
 *    Output(s)            : UINT1 *au1HwAddr.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
VOID
CfaIwfEnetFormEnetMcastAddr (UINT4 u4IpAddr, UINT1 *au1HwAddr)
{

    UINT1               au1EnetMcastTemplate[CFA_ENET_ADDR_LEN] =
        { 0x01, 0x00, 0x5e, 0x00, 0x00, 0x00 };

/* we expect au1HwAddr to be of size 6 bytes */
    MEMCPY (au1HwAddr, au1EnetMcastTemplate, CFA_ENET_ADDR_LEN);

/* copy 23 bits low bits from IP to Enet */
    u4IpAddr = OSIX_HTONL (u4IpAddr);
    au1HwAddr[3] = *((UINT1 *) &u4IpAddr + 1) & 0x7;
    au1HwAddr[4] = *((UINT1 *) &u4IpAddr + 2);
    au1HwAddr[5] = *((UINT1 *) &u4IpAddr + 3);

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
              "In CfaIwfEnetFormEnetMcastAddr for IP Address %d\n", u4IpAddr);

}

/*****************************************************************************
 *
 *    Function Name        : CfaIwfCheckLocalEnetFrame
 *
 *    Description        : This function converts the multicast IP
 *                address to the Mulicast Enet.
 *
 *    Input(s)            : UINT4 u4IpAddr,
 *
 *    Output(s)            : UINT1 *au1HwAddr.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
INT4
CfaIwfCheckLocalEnetFrame (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
    tTMO_SLL           *pIfRcvAddrTab = NULL;
    UINT1               au1DefMacAddr[CFA_ENET_ADDR_LEN];
    UINT1               u1IfEtherType;
    tRcvAddressInfo    *pScanNode = NULL;

    /* If MGMT_PORT then get the OOB MacAddr and do compare */
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        (CfaIsLinuxVlanIntf (u4IfIndex) == TRUE))
    {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
        CfaGddOobGetOsHwAddr (u4IfIndex, au1DefMacAddr);
#endif
    }
    else
    {
        /* Get the Default Mac Read from issnvram.txt */
        if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
        {
            /* The stack interface uses the stack mac address
             * which is different from the system mac address*/

            CfaGetEthernetType (u4IfIndex, &u1IfEtherType);

            if (u1IfEtherType == CFA_STACK_ENET)
            {
                CfaGetIfHwAddr (CFA_DEFAULT_STACK_IFINDEX, au1DefMacAddr);
            }
            else
            {
                CfaGetSysMacAddress (au1DefMacAddr);
            }
        }
        else
        {
            CfaGetSysMacAddress (au1DefMacAddr);
        }
    }

    if (MEMCMP (au1HwAddr, au1DefMacAddr, CFA_ENET_ADDR_LEN) == 0)
    {
        return CFA_SUCCESS;
    }

    pIfRcvAddrTab = &CFA_IF_RCVADDRTAB (u4IfIndex);

/* If not found an entry means return NULL */
    if (pIfRcvAddrTab == NULL)
        return CFA_FAILURE;

    /* Added the offset CFA_ENET_ADDR_LEN to au1HwAddr in the MEMCMP function */
/* scan the list the find the node */
    TMO_SLL_Scan (pIfRcvAddrTab, pScanNode, tRcvAddressInfo *)
    {
        if (MEMCMP (pScanNode->au1Address, au1HwAddr, CFA_ENET_ADDR_LEN) == 0)
/* found an entry, return Success*/
            return CFA_SUCCESS;
    }                            /* end of SLL scan */

/* No entry found, return Failure */
    return CFA_FAILURE;
}

#ifdef BRIDGE_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaIwfEnetDeEncapBridgeFrame
 *
 *    Description        : This function converts the multicast IP
 *                address to the Mulicast Enet.
 *
 *    Input(s)            : UINT4 u4IpAddr,
 *
 *    Output(s)            : UINT1 *au1HwAddr.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
INT4
CfaIwfEnetDeEncapBridgeFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                              UINT2 *pu2Protocol)
{
    tEnetV2Header      *pEthHdr;
    tEnetSnapHeader    *pSnapEthHdr;
    UINT2               u2LenOrType;

    if (CRU_BUF_Move_ValidOffset (pBuf, CFA_BRIDGE_HEADER_SIZE) != CRU_SUCCESS)
    {
        CFA_IF_SET_IN_ERR (u4IfIndex);

        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                  "Error in CfaIwfEnetDeEncapBridgeFrame - Cant Strip Frame on interface %d.\n",
                  u4IfIndex);

        return (CFA_FAILURE);    /* buffer is released by GDD */
    }

/* first we extract the Enet Frame Header - we assume it to be Enet by 
default. It is assumed that the Header is located linearly in the
buffer since Enet is the lower most interface layer */
    pEthHdr = (tEnetV2Header *) CRU_BMC_Get_DataPointer (pBuf);
    u2LenOrType = OSIX_NTOHS (pEthHdr->u2LenOrType);

/* we now determine the type of Encapsulation - we support EnetV2 and LLC-SNAP.
we also find the type of the packet. */
    if (CFA_ENET_IS_TYPE (u2LenOrType))
    {
        *pu2Protocol = u2LenOrType;
    }                            /* end of Enet V2 encap */
    else
    {
/* the packet may be LLC-SNAP encapsulated */
        pSnapEthHdr = (tEnetSnapHeader *) CRU_BMC_Get_DataPointer (pBuf);

/* check if the PDU is for STAP task and if STAP is enabled */
        if ((pSnapEthHdr->u1DstLSap == CFA_LLC_STAP_SAP) &&
            (pSnapEthHdr->u1SrcLSap == CFA_LLC_STAP_SAP) &&
            (CFA_IS_BRIDGE_ENABLED))
        {

/* strip the Enet and LLC header */
            if (CRU_BUF_Move_ValidOffset (pBuf, CFA_ENET_LLC_HEADER_SIZE)
                != CRU_SUCCESS)
            {
                CFA_IF_SET_IN_ERR (u4IfIndex);

                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Error in CfaIwfEnetDeEncapBridgeFrame - Cant Strip Frame on interface %d.\n",
                          u4IfIndex);

                return (CFA_FAILURE);    /* buffer is released by GDD */
            }

/* return - specify protocol as STAP BPDU */
            *pu2Protocol = CFA_PROT_BPDU;
            CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
                      "In CfaIwfEnetDeEncapBridgeFrame - recd STAP packet on interface %d.\n",
                      u4IfIndex);
            return (CFA_SUCCESS);
        }

/* check for LLC control frame first - we expect only LLC in SNAP now */
        if (pSnapEthHdr->u1Control == CFA_LLC_CONTROL_UI)
        {

/* check for presence of SNAP which may carry IP/ARP/RARP after LLC */
            if ((pSnapEthHdr->u1DstLSap == CFA_LLC_SNAP_SAP) ||
                (pSnapEthHdr->u1SrcLSap == CFA_LLC_SNAP_SAP) ||
                (pSnapEthHdr->u1Oui1 == 0x00) ||
                (pSnapEthHdr->u1Oui2 == 0x00) || (pSnapEthHdr->u1Oui3 == 0x00))
            {
/* determine which protocol is being carried */
                *pu2Protocol = OSIX_NTOHS (pSnapEthHdr->u2ProtocolType);
            }                    /* end of SNAP framing */
        }                        /* end of LLC framing */
    }                            /* end of Enet LLC_SNAP encap */

    CFA_DBG2 (CFA_TRC_ALL_TRACK, CFA_IWF,
              "In CfaIwfEnetDeEncapBridgeFrame - Received Frame on iface %d, proto %x.\n",
              u4IfIndex, *pu2Protocol);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaIwfEnetEncapBridgeFrame
 *
 *    Description        : This function converts the multicast IP
 *                address to the Mulicast Enet.
 *
 *    Input(s)            : UINT4 u4IpAddr,
 *
 *    Output(s)            : UINT1 *au1HwAddr.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
INT4
CfaIwfEnetEncapBridgeFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                            UINT4 *pu4PktSize)
{
    tBridgeHeader       BridgeHeader;

    CFA_BRIDGE_FLAGS (&BridgeHeader) = CFA_BFLAGS;
    CFA_BRIDGE_MAC_TYPE (&BridgeHeader) = CFA_BMAC_TYPE;

    if (CRU_BUF_Prepend_BufChain
        (pBuf, (UINT1 *) &BridgeHeader, CFA_BRIDGE_HEADER_SIZE) != CRU_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                  "Exiting CfaIwfEnetEncapBridgeFrame - Prepend fail for interface %d - FAILURE\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

    (*pu4PktSize) += CFA_BRIDGE_HEADER_SIZE;

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
              "In CfaIwfEnetEncapBridgeFrame - form STAP BPDU encap for pkt on iface %d\n",
              u4IfIndex);
    return CFA_SUCCESS;
}

#endif /* BRIDGE_WANTED */

#ifdef RMON_WANTED
/*****************************************************************************
 *                                                                         *    
 *    Function Name        : CfaIwfRmonUpdateTables                        *    
 *                                                                         *
 *    Description          : This function updates the etherSatistics Table,*
 *                           Host Table and Matrix Table                   *
 *                                                                         *    
 *    Input(s)             : UINT1 *pu1DataBuf, UINT4 u4IfIndex,           *
 *                 UINT4 u4PktSize                                         *    
 *    Output(s)            : CFA_SUCCESS or CFA_FAILURE                    *
 *                                                                         *
 *    Global Variables Referred : t_FRAME_PARTICULARS                      *
 *                                                                         *
 *    Global Variables Modified : None.                                    *
 *                                                                         *
 *    Exceptions or Operating                                              *
 *    System Error Handling     : None.                                    *
 *                                                                         *
 *    Use of Recursion          : None.                                    *
 *                                                                         *
 *    Returns                   : None.                                    *
 *                                                                         *
 *****************************************************************************/

INT4
CfaIwfRmonUpdateTables (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    tFrameParticulars   FrameParticulars;

    MEMSET (&FrameParticulars, 0, sizeof (tFrameParticulars));

    CFA_DBG (CFA_TRC_ERROR, CFA_IWF, " CfaIwfRmonUpdateTables - Entry \n");
   /** check received buffer contains Data   **/
    if (pu1DataBuf == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                 "In CfaIwfRmonUpdateTables - no data received \n");
        return CFA_FAILURE;
    }
   /** updating the frame particulars **/
    FrameParticulars.u2Length = (UINT2) u4PktSize;
    FrameParticulars.u1DataSource = (UINT1) u4IfIndex;
    FrameParticulars.u4Type = (UINT4) PORT_INDEX_TYPE;

   /** log Frame Particulars */
    CFA_DBG2 (CFA_TRC_ALL, CFA_IWF,
              "In CfaIwfRmonUpdateTables - frame particulars updated are DataSource -%d , Length - %d \n",
              u4IfIndex, u4PktSize);

    if (RmonUpdateFrameParticulars (pu1DataBuf, &FrameParticulars)
        == CFA_SUCCESS)
    {
        if (RmonUpdateStatisticsTable (pu1DataBuf, &FrameParticulars)
            != CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                     "In CfaIwfRmonUpdateTables - EtherStatistics Table not updated \n");
        }
        RmonUpdateHostModuleTables (&FrameParticulars);

        if (RmonUpdateMatrixTables (&FrameParticulars) == FALSE)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                     "In CfaIwfRmonUpdateTables - Matrix Table Not updated \n");

        }
        return CFA_SUCCESS;
    }
    else
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                 "In CfaIwfRmonUpdateTables - Frame particulars cannot be updated \n");
        return (CFA_FAILURE);
    }

}
#endif /* RMON_WANTED */

#ifdef ISIS_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaIwfHandleOutgoingIsisPkt
 *
 *    Description        : This function processes outgoing frames of IsIs
 *                                                                                  *    Input(s)            : tCRU_BUF_CHAIN_HEADER *pBuf,
 *                          UINT4 u4IfIndex,
 *                          UINT4 u4PktSize.
 *                          UINT2 u2Protocol.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if frame is processed
 *                succeessfully, otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIwfHandleOutgoingIsisPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                             UINT4 u4IfIndex, UINT4 u4PktSize, UINT2 u2Protocol)
{
    UINT1               u1IfType;
    UINT1               u1BridgedIfStatus = CFA_DISABLED;
    UINT1               au1DestHwAddr[CFA_ENET_ADDR_LEN];

    CFA_GET_DEST_MEDIA_ADDR (pBuf, au1DestHwAddr);

/* based on the interface type, send the pkt to the respective IWF
routines */

    CfaGetIfType (u4IfIndex, &u1IfType);
    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfStatus);
    switch (u1IfType)
    {
        case CFA_ENET:
        case CFA_IFPWTYPE:
        case CFA_L3IPVLAN:
            if (CfaIwfEnetProcessTxFrame
                (pBuf, u4IfIndex, au1DestHwAddr, u4PktSize,
                 u2Protocol, CFA_ENCAP_NLPID) != CFA_SUCCESS)
            {
                /* release of the pkt in IWF/GDD */
                CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                         "Eth pkt from IsIs dispatch to IWF fail.\n");
                return (CFA_FAILURE);
            }
            break;

        case CFA_LAGG:
            if (u1BridgedIfStatus == CFA_DISABLED)
            {
                if (CfaIwfEnetProcessTxFrame
                    (pBuf, u4IfIndex, au1DestHwAddr, u4PktSize,
                     u2Protocol, CFA_ENCAP_NLPID) != CFA_SUCCESS)
                {
                    /* release of the pkt in IWF/GDD */
                    CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                             "Eth pkt from IsIs dispatch to IWF fail.\n");
                    return (CFA_FAILURE);
                }
            }
            break;

#ifdef PPP_WANTED
        case CFA_PPP:
        case CFA_MP:
            if (CfaIwfPppHandleOutgoingPkt
                (pBuf, u4IfIndex, u4PktSize, u2Protocol) != CFA_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                         "PPP pkt from IsIs dispatch to " "IWF fail.\n");
                return (CFA_FAILURE);
            }
            break;
#endif
        default:
            CFA_DBG (CFA_TRC_ERROR, CFA_IWF,
                     "Lower layer interface is not  "
                     "Ethernet or PPP or AtmVc \n");
            return (CFA_FAILURE);
            break;
    }
    return (CFA_SUCCESS);        /* we dont release buf here */
}

#endif /* ISIS_WANTED */

INT4
CfaIwfCheckIsLocalEnetFrame (UINT4 u4IfIndex, UINT4 u4VlanIfIndex,
                             UINT1 *pau1HwAddr)
{
    tTMO_SLL           *pIfRcvAddrTab = NULL;
    UINT1               au1DefMacAddr[CFA_ENET_ADDR_LEN];
    tRcvAddressInfo    *pScanNode = NULL;
    UINT1               u1DefRouterIdx = CFA_FALSE;
    UINT1               u1IfType = 0;
    INT4                i4IfValid = CFA_FALSE;

    MEMSET (au1DefMacAddr, 0, CFA_ENET_ADDR_LEN);

    /* If MGMT_PORT then get the OOB MacAddr and do compare */
    if ((CfaIsMgmtPort (u4IfIndex) == TRUE) ||
        (CfaIsLinuxVlanIntf (u4IfIndex) == TRUE))
    {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
        CfaGddOobGetOsHwAddr (u4IfIndex, au1DefMacAddr);
#endif
    }
    else
    {
        if ((u4VlanIfIndex > CFA_MAX_INTERFACES ()) || (u4VlanIfIndex == 0))
        {
            return CFA_FAILURE;
        }

        CFA_DS_LOCK ();
        u1IfType = CFA_CDB_IF_TYPE (u4VlanIfIndex);
        CFA_CDB_CHECK_IF_TYPE (u1IfType, u1DefRouterIdx);
        if (CFA_FALSE == u1DefRouterIdx)
        {
            CFA_CDB_GET_ENET_IWF_LOCAL_MAC (u4VlanIfIndex, au1DefMacAddr);
        }
        else
        {
            i4IfValid = CFA_CDB_IS_INTF_VALID (CFA_DEFAULT_ROUTER_IFINDEX);
            if (i4IfValid == CFA_TRUE)
            {
                CFA_CDB_GET_ENET_IWF_LOCAL_MAC (CFA_DEFAULT_ROUTER_IFINDEX,
                                                au1DefMacAddr);
            }
        }
        CFA_DS_UNLOCK ();
    }

    if (MEMCMP (pau1HwAddr, au1DefMacAddr, CFA_ENET_ADDR_LEN) == 0)
    {
        return CFA_SUCCESS;
    }

    pIfRcvAddrTab = &CFA_IF_RCVADDRTAB (u4IfIndex);

/* If not found an entry means return NULL */
    if (pIfRcvAddrTab == NULL)
        return CFA_FAILURE;

    /* Added the offset CFA_ENET_ADDR_LEN to pau1HwAddr in the MEMCMP function */
/* scan the list the find the node */
    TMO_SLL_Scan (pIfRcvAddrTab, pScanNode, tRcvAddressInfo *)
    {
        if (MEMCMP (pScanNode->au1Address, pau1HwAddr, CFA_ENET_ADDR_LEN) == 0)
/* found an entry, return Success*/
            return CFA_SUCCESS;
    }                            /* end of SLL scan */

/* No entry found, return Failure */
    return CFA_FAILURE;
}

#if ((defined (RMON2_WANTED)) && (defined (NPAPI_WANTED)))
/************************************************************************
 *    FUNCTION NAME    : CfaIwfRmon2UpdatePktHdr                        *
 *                                                                      *
 *    DESCRIPTION      :                                                *
 *                                                                      *
 *    INPUT            :                                                *
 *                                                                      *
 *    OUTPUT           : None                                           *
 *                                                                      *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                      *
 ************************************************************************/
PUBLIC INT4
CfaIwfRmon2UpdatePktHdr (UINT1 *pu1DataBuf, tPktHeader * pPktHdr)
{
    UINT1              *pFrame = NULL, *pTcpHdr = NULL;
    UINT1               u1IpHdrLen = 0;
    UINT2               u2Ip6TrafficClass = 0;

    pFrame = pu1DataBuf;

    /* Copy Destination MAC */
    MEMCPY (pPktHdr->DstMACAddress, pFrame, RMON2_MAC_ADDRESS_LEN);

    /* Move Frame to Source MAC and Copy Source MAC */
    pFrame += RMON2_MAC_ADDRESS_LEN;

    MEMCPY (pPktHdr->SrcMACAddress, pFrame, RMON2_MAC_ADDRESS_LEN);

    /* Move Frame to EtherType and Copy EtherType */
    pFrame += RMON2_MAC_ADDRESS_LEN;

    MEMCPY (&pPktHdr->u2EtherType, pFrame, RMON2_ETH_TYPE_LEN);
    pPktHdr->u2EtherType = OSIX_NTOHS (pPktHdr->u2EtherType);

    switch (pPktHdr->u2EtherType)
    {
        case RMON2_OUTER_VLAN_ID:
            pFrame += (RMON2_VLAN_HEADER_LEN * 2);
            MEMCPY (&pPktHdr->u2EtherType, pFrame, RMON2_ETH_TYPE_LEN);
            pPktHdr->u2EtherType = OSIX_NTOHS (pPktHdr->u2EtherType);
            break;
        case RMON2_INNER_VLAN_ID:
            pFrame += RMON2_VLAN_HEADER_LEN;
            MEMCPY (&pPktHdr->u2EtherType, pFrame, RMON2_ETH_TYPE_LEN);
            pPktHdr->u2EtherType = OSIX_NTOHS (pPktHdr->u2EtherType);
            break;
        default:
            break;
    }
    /* Move Frame to IP Header */
    pFrame += RMON2_ETH_TYPE_LEN;

    switch (pPktHdr->u2EtherType)
    {
        case RMON2_ENET_IPV4:
        {
            MEMCPY (&u1IpHdrLen, pFrame, RMON2_IPV4_VER_LEN);

            u1IpHdrLen &= 0x0f;
            pTcpHdr = pFrame + (u1IpHdrLen * 4);

            pPktHdr->u1IpVersion = RMON2_IPV4_VERSION;

            /* Move Frame to IP Header - DSCP Value */
            pFrame += RMON2_IPV4_VER_LEN;

            MEMCPY (&pPktHdr->u1DSCP, pFrame, RMON2_IPV4_DSCP_LEN);
            pPktHdr->u1DSCP >>= 2;

            /* Move Frame to IP Header - L4 Protocol */
            pFrame += RMON2_IPV4_PROTO_FROM_DSCP_OFFSET;

            MEMCPY (&pPktHdr->u1IpProtocol, pFrame, RMON2_IPV4_PROTO_LEN);

            /* Move Frame to IP Header - Source IP */
            pFrame += RMON2_IPV4_SIP_FROM_PROTO_OFFSET;

            MEMCPY (&pPktHdr->SrcIp.u4_addr[3], pFrame, RMON2_IPV4_ADDR_LEN);
            pPktHdr->SrcIp.u4_addr[3] = OSIX_NTOHL (pPktHdr->SrcIp.u4_addr[3]);

            /* Move Frame to IP Header - Destination IP */
            pFrame += RMON2_IPV4_ADDR_LEN;

            MEMCPY (&pPktHdr->DstIp.u4_addr[3], pFrame, RMON2_IPV4_ADDR_LEN);
            pPktHdr->DstIp.u4_addr[3] = OSIX_NTOHL (pPktHdr->DstIp.u4_addr[3]);

            /* For ICMP packets, do not process L4 layer */
            if (pPktHdr->u1IpProtocol == RMON2_ENET_ICMP)
            {
                return OSIX_SUCCESS;
            }
            else if ((pPktHdr->u1IpProtocol != RMON2_IP_PROTO_TCP) &&
                     (pPktHdr->u1IpProtocol != RMON2_IP_PROTO_UDP))
            {
                return OSIX_FAILURE;
            }

            /* Copy Source Port */
            MEMCPY (&pPktHdr->u2SrcPort, pTcpHdr, RMON2_L4PORT_LEN);
            pPktHdr->u2SrcPort = OSIX_NTOHS (pPktHdr->u2SrcPort);

            /* Copy Destination Port */
            pTcpHdr += RMON2_L4PORT_LEN;

            MEMCPY (&pPktHdr->u2DstPort, pTcpHdr, RMON2_L4PORT_LEN);
            pPktHdr->u2DstPort = OSIX_NTOHS (pPktHdr->u2DstPort);

            return OSIX_SUCCESS;
        }
        case RMON2_ENET_IPV6:
        {
            pPktHdr->u1IpVersion = RMON2_IPV6_VERSION;

            /* Copy Traffic Class */
            MEMCPY (&u2Ip6TrafficClass, pFrame, RMON2_IPV6_TC_LEN);
            u2Ip6TrafficClass = OSIX_NTOHS (u2Ip6TrafficClass);

            /* Since Traffic Class values are 8 bits long,
             * Extract it from Short value */
            u2Ip6TrafficClass = u2Ip6TrafficClass & RMON2_IPV6_DSCP;

            /* Discard last 4 bits from Traffic Class */
            u2Ip6TrafficClass = u2Ip6TrafficClass >> 4;

            pPktHdr->u1DSCP = (UINT1) u2Ip6TrafficClass;
            /* Discard ECN Bits from DSCP */
            pPktHdr->u1DSCP = pPktHdr->u1DSCP >> 2;

            /* Move Frame to IPV6 Header - Next Header (L4 Protocol) */
            pFrame += RMON2_IPV6_NEXT_HEADER_OFFSET;

            MEMCPY (&pPktHdr->u1IpProtocol, pFrame, RMON2_IPV4_PROTO_LEN);

            if ((pPktHdr->u1IpProtocol != RMON2_IP_PROTO_TCP) &&
                (pPktHdr->u1IpProtocol != RMON2_IP_PROTO_UDP))
            {
                return OSIX_FAILURE;
            }

            /* Move Frame to IPV6 Source Address */
            pFrame += RMON2_IPV6_SIP_FROM_PROTO_OFFSET;

            MEMCPY (&pPktHdr->SrcIp, pFrame, RMON2_IPV6_ADDR_LEN);
            pPktHdr->SrcIp.u4_addr[0] = OSIX_NTOHL (pPktHdr->SrcIp.u4_addr[0]);
            pPktHdr->SrcIp.u4_addr[1] = OSIX_NTOHL (pPktHdr->SrcIp.u4_addr[1]);
            pPktHdr->SrcIp.u4_addr[2] = OSIX_NTOHL (pPktHdr->SrcIp.u4_addr[2]);
            pPktHdr->SrcIp.u4_addr[3] = OSIX_NTOHL (pPktHdr->SrcIp.u4_addr[3]);

            /* Move Frame to IPV6 Destination Address */
            pFrame += RMON2_IPV6_ADDR_LEN;

            MEMCPY (&pPktHdr->DstIp, pFrame, RMON2_IPV6_ADDR_LEN);
            pPktHdr->DstIp.u4_addr[0] = OSIX_NTOHL (pPktHdr->DstIp.u4_addr[0]);
            pPktHdr->DstIp.u4_addr[1] = OSIX_NTOHL (pPktHdr->DstIp.u4_addr[1]);
            pPktHdr->DstIp.u4_addr[2] = OSIX_NTOHL (pPktHdr->DstIp.u4_addr[2]);
            pPktHdr->DstIp.u4_addr[3] = OSIX_NTOHL (pPktHdr->DstIp.u4_addr[3]);

            /* Move to TCP / UDP header */
            pFrame += RMON2_IPV6_ADDR_LEN;

            /* Copy Source Port */
            MEMCPY (&pPktHdr->u2SrcPort, pFrame, RMON2_L4PORT_LEN);
            pPktHdr->u2SrcPort = OSIX_NTOHS (pPktHdr->u2SrcPort);

            /* Copy Destination Port */
            pFrame += RMON2_L4PORT_LEN;

            MEMCPY (&pPktHdr->u2DstPort, pFrame, RMON2_L4PORT_LEN);
            pPktHdr->u2DstPort = OSIX_NTOHS (pPktHdr->u2DstPort);

            return OSIX_SUCCESS;
        }
        default:
            return OSIX_FAILURE;
    }
}
#endif /* RMON2_WANTED */

/*****************************************************************************
 * Name               : CfaCopyFrmPduForBackPlaneIntf
 *
 * Description        : This utility is used to copy the data present in the 
 *                      given CRU buffer to a new CRU buffer by skipping the
 *                      data present in the buffer pointed by u4Offset. 
 *                      u4OffsetSize provides the number of bytes that needs to
 *                      be skipped while copying the data.
 *
 * Input(s)           : pSrcCruBuf - Pointer to source CRU-Buffer
 *
 * Output(s)          : None
 *
 * Return Value(s)    : tCRU_BUF_CHAIN_HEADER * (Pointer to copied CRU buffer)
 *
 *****************************************************************************/
tCRU_BUF_CHAIN_HEADER *
CfaCopyFrmPduForBackPlaneIntf (tCRU_BUF_CHAIN_HEADER * pSrcCruBuf,
                               UINT4 u4OffSet, UINT4 u4OffSetSize)
{
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
    UINT4               u4BufSize = 0;
    UINT1              *pu1Frame = NULL;

    u4BufSize = CRU_BUF_Get_ChainValidByteCount (pSrcCruBuf);
    if (u4BufSize == 0)
    {
        return NULL;
    }
    /* Allocate for srcbuffer size - no. of bytes to be avoided */
    pCruBuf = CRU_BUF_Allocate_MsgBufChain ((u4BufSize - u4OffSetSize), 0);
    if (pCruBuf == NULL)
    {
        return NULL;
    }
    pu1Frame = CRU_BUF_Get_DataPtr_IfLinear (pSrcCruBuf, 0, u4BufSize);
    if (pu1Frame == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
        return NULL;
    }
    /* First copy upto the offset value provided */
    CRU_BUF_Copy_OverBufChain (pCruBuf, pu1Frame, 0, u4OffSet);

    /* Now copy the remaining bytes skipping the value provided in the 
     * u4Offset */
    if ((u4OffSet + u4OffSetSize) > u4BufSize)
    {
        CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
        /* To avoid klocwork warning */
        return NULL;
    }
    CRU_BUF_Copy_OverBufChain (pCruBuf, (pu1Frame + u4OffSet + u4OffSetSize),
                               u4OffSet,
                               (u4BufSize - (u4OffSet + u4OffSetSize)));
    return pCruBuf;
}

/*****************************************************************************
 * Name               : CfaCopyOverPduForBackPlaneIntf
 *
 * Description        : This utility is used to insert the data present in the
 *                      pBackPlaneParams into the given CRU buffer. A new CRU
 *                      buffer is created and the data is inserted into the 
 *                      corresponding position.                      
 *
 * Input(s)           : pSrcCruBuf - Pointer to source CRU-Buffer
 *
 * Output(s)          : None
 *
 * Return Value(s)    : tCRU_BUF_CHAIN_HEADER * (Pointer to copied CRU buffer)
 *
 *****************************************************************************/
tCRU_BUF_CHAIN_HEADER *
CfaCopyOverPduForBackPlaneIntf (tCRU_BUF_CHAIN_HEADER * pSrcCruBuf,
                                tCfaBackPlaneParams * pBackPlaneParams,
                                UINT4 u4OffSet, UINT4 u4OffSetSize)
{
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
    UINT4               u4BufSize = 0;
    UINT1              *pu1Frame = NULL;

    u4BufSize = CRU_BUF_Get_ChainValidByteCount (pSrcCruBuf);
    if (u4BufSize == 0)
    {
        return NULL;
    }
    /* Allocate for srcbuffer size - no. of bytes to be avoided */
    pCruBuf = CRU_BUF_Allocate_MsgBufChain ((u4BufSize + u4OffSetSize), 0);
    if (pCruBuf == NULL)
    {
        return NULL;
    }
    pu1Frame = CRU_BUF_Get_DataPtr_IfLinear (pSrcCruBuf, 0, u4BufSize);
    if (pu1Frame == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
        return NULL;
    }
    /* First copy upto the offset value provided */
    CRU_BUF_Copy_OverBufChain (pCruBuf, pu1Frame, 0, u4OffSet);

    /* Now copy the given value in the offset */
    pBackPlaneParams->u4ContextId = OSIX_HTONS (pBackPlaneParams->u4ContextId);

    CRU_BUF_Copy_OverBufChain (pCruBuf, (UINT1 *)
                               &(pBackPlaneParams->u4ContextId),
                               u4OffSet, u4OffSetSize);

    /* Now copy the remaining bytes present in the existing frame */
    CRU_BUF_Copy_OverBufChain (pCruBuf, (pu1Frame + u4OffSet),
                               (u4OffSet + u4OffSetSize),
                               (u4BufSize - u4OffSet));
    return pCruBuf;
}

/*****************************************************************************
 * Name               : CfaValidateRxPacket
 *
 * Description        : This function is called on packet reception to validate
 *                      the packet and extract the header information.
 * 
 * Input(s)           : pBuf - Packet Buffer
 *                      u4IfIndex - Interface on which packet is received
 *                      u4PktSize - Size of the packet
 *
 * Output(s)          : pPktHandleInfo - Contains the packet information for 
 *                      packet handling 
 *                      pbProcessFlag - Flag indicates the packet processing
 *                      needs to be continued or not. if set to CFA_TRUE it 
 *                      should process packet. Other wise the packet should
 *                      not be processed further.
 *
 * Return Value(s)    : CFA_SUCCESS / CFA_FAILURE 
 *
 *****************************************************************************/

INT4
CfaValidateRxPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                     UINT4 u4PktSize, tPktHandleInfo * pPktHandleInfo,
                     BOOL1 * pbProcessFlag)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4VlanOffset = CFA_VLAN_TAG_OFFSET;
    INT4                i4EoamRetVal;
    UINT1               u1NetworkType;
    UINT1               u1IfType = CFA_ENET_UNKNOWN;
    UINT4               u4TempIf = CFA_NONE;
    BOOL1               bIsPppPkt = CFA_FALSE;

    *pbProcessFlag = CFA_TRUE;
    /* Get the Interface information once and for all.
     * This should be passed onto subsequent functions to 
     * avoid locks.*/
    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        *pbProcessFlag = CFA_FALSE;
        CFA_DBG1 (CFA_TRC_ALL, CFA_IWF,
                  "Get interface info for Interface %d failed.\n", u4IfIndex);
        return (CFA_FAILURE);    /* buffer is released by GDD */
    }

    if (IfInfo.i4Active != CFA_TRUE)
    {
        *pbProcessFlag = CFA_FALSE;
        CFA_DBG1 (CFA_TRC_ALL, CFA_IWF,
                  "Exiting CfaValidateRxPacket: Interface %d - Invalid.\n",
                  u4IfIndex);
        return CFA_FAILURE;
    }

    /* Fill the interface related parameters */
    pPktHandleInfo->u4IfIndex = u4IfIndex;
    pPktHandleInfo->u1IfType = IfInfo.u1IfType;

    CfaGetIfNwType (u4IfIndex, &u1NetworkType);

    if (CfaGetHLIfFromLLIf (u4IfIndex, &u4TempIf, &u1IfType) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_IWF,
                  "CfaGetHLIfFromLLIf for Interface %d failed\n", u4IfIndex);
    }

    if ((u1IfType != CFA_PPP) && (u1NetworkType == CFA_NETWORK_TYPE_LAN))
    {
        if (VcmGetContextInfoFromIfIndex
            (u4IfIndex, &pPktHandleInfo->u4ContextId,
             &pPktHandleInfo->u2LocalPortId) != VCM_SUCCESS)
        {
            *pbProcessFlag = CFA_FALSE;
            CFA_DBG1 (CFA_TRC_ALL, CFA_IWF,
                      "VcmGetContextInfoFromIfIndex for Interface %d failed\n",
                      u4IfIndex);
            return CFA_FAILURE;
        }
    }

    i4EoamRetVal = EoamMuxParActOnRx (u4IfIndex, pBuf, u4PktSize);

    if (i4EoamRetVal == EOAM_DISCARD)
    {
        *pbProcessFlag = CFA_FALSE;
        /* Discard the received the frame. Returning FAILURE
         * will release the buffer ptr in the calling place. */
        CFA_IF_SET_IN_DISCARD (u4IfIndex);
        return CFA_FAILURE;
    }
    else if ((i4EoamRetVal == EOAM_LOOPBACK) ||
             (i4EoamRetVal == EOAM_REMOTE_LB))
    {
        /* EOAM_LOOPBACK and EOAM_REMOTE_LB cases the packet is consumed
         * by the EOAM. So the packet should not be processes further.
         * Set the pbProcessFlag to CFA_FALSE */
        *pbProcessFlag = CFA_FALSE;
        /* EOAM has already processed the frame. */
        CFA_IF_SET_IN_UCAST (u4IfIndex);
        return CFA_SUCCESS;
    }

    /* first we extract the Enet Frame Header - we assume it to be Enet by
       default. It is assumed that the Header is located linearly in the
       buffer since Enet is the lower most interface layer */
    MEMSET (&pPktHandleInfo->EnetHdr, 0, CFA_ENET_V2_HEADER_SIZE);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &pPktHandleInfo->EnetHdr, 0,
                               CFA_ENET_V2_HEADER_SIZE);

    pPktHandleInfo->u2EtherProto =
        OSIX_NTOHS (pPktHandleInfo->EnetHdr.u2LenOrType);

    if (IfInfo.u1BridgedIface == CFA_ENABLED)
    {
        if (VlanGetTagLenInFrame (pBuf, u4IfIndex, &u4VlanOffset)
            == VLAN_FAILURE)
        {
            if (EoamApiIsOAMPDU (pBuf) == OSIX_FALSE)
            {
                *pbProcessFlag = CFA_FALSE;
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                return (CFA_FAILURE);
            }
        }

        if (u4VlanOffset > CFA_VLAN_TAG_OFFSET)
        {
            /* Vlan Tag is present, copy the Ethernet Protocol with
             *  the new offset */
            CRU_BUF_Copy_FromBufChain (pBuf,
                                       (UINT1 *) &pPktHandleInfo->u2EtherProto,
                                       u4VlanOffset, CFA_ENET_TYPE_OR_LEN);

            pPktHandleInfo->u2EtherProto =
                OSIX_NTOHS (pPktHandleInfo->u2EtherProto);
        }
    }
    else
    {
        /*Added the API CfaGetHLIfFromLLIf for getting Higher Layer Interface 
           from Lower Layer Interface */

        if (CfaGetHLIfFromLLIf (u4IfIndex, &u4TempIf, &u1IfType) == CFA_SUCCESS)
        {
            /*Checking Interface Type for PPP while receiving the PPP packet with both
             *VLAN and non-VLAN packet */

            if ((IfInfo.u1IfType == CFA_ENET) && (u1IfType == CFA_PPP))
            {
                bIsPppPkt = CFA_TRUE;
            }
        }

        if ((IfInfo.u1IfType == CFA_PSEUDO_WIRE) || (bIsPppPkt == CFA_TRUE))
        {
            if (pPktHandleInfo->u2EtherProto == CFA_VLAN_PROTOCOL_ID)
            {
                /* Vlan Tag is present, copy the Ethernet Protocol with
                 * the new offset */
                CRU_BUF_Copy_FromBufChain (pBuf,
                                           (UINT1 *) &pPktHandleInfo->
                                           u2EtherProto,
                                           CFA_VLAN_TAGGED_HEADER_SIZE,
                                           CFA_VLAN_PROTOCOL_SIZE);

                pPktHandleInfo->u2EtherProto =
                    OSIX_NTOHS (pPktHandleInfo->u2EtherProto);
            }
        }
    }

    /* Set the Vlan OfSet to the Packet Info */
    pPktHandleInfo->u4VlanOffset = u4VlanOffset;

    if (pPktHandleInfo->u2EtherProto == L2_ENET_IP)
    {
        /* Extracting IP header from the packet to identify the
         * protocol type.
         */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &pPktHandleInfo->u1IpProto,
                                   u4VlanOffset + VLAN_TYPE_OR_LEN_SIZE +
                                   CFA_IP_PROTO_OFFSET_IN_IPHDR,
                                   sizeof (UINT1));

    }

    if ((CfaIsMacAllZeros (pPktHandleInfo->EnetHdr.au1SrcAddr) == CFA_TRUE)
        || (CfaIsMacAllZeros (pPktHandleInfo->EnetHdr.au1DstAddr) == CFA_TRUE))
    {
        *pbProcessFlag = CFA_FALSE;
        CFA_IF_SET_IN_DISCARD (u4IfIndex);
        return (CFA_FAILURE);
    }

    if (CFA_IS_ENET_MAC_BCAST (&pPktHandleInfo->EnetHdr))
    {
        pPktHandleInfo->u1LinkFrameType = CFA_LINK_BCAST;
    }
    else if (CFA_IS_ENET_MAC_MCAST (&pPktHandleInfo->EnetHdr))
    {
        pPktHandleInfo->u1LinkFrameType = CFA_LINK_MCAST;
    }
    else
    {
        pPktHandleInfo->u1LinkFrameType = CFA_LINK_UCAST;
    }
    if (pPktHandleInfo->u2EtherProto == CFA_ENET_IPV6)
    {
        if (CFA_IS_ENET_MAC_BCAST (&pPktHandleInfo->EnetHdr))
        {
            pPktHandleInfo->u1LinkFrameType = CFA_LINK_BCAST;
            CFA_IF_SET_IN_BCAST (u4IfIndex);
        }
        else if (CFA_IS_ENET_IPV6_MAC_MCAST (&pPktHandleInfo->EnetHdr))
        {
            pPktHandleInfo->u1LinkFrameType = CFA_LINK_MCAST;
            CFA_IF_SET_IN_MCAST (u4IfIndex);
        }
        else
        {
            pPktHandleInfo->u1LinkFrameType = CFA_LINK_UCAST;
            CFA_IF_SET_IN_UCAST (u4IfIndex);
        }
    }

    return (CFA_SUCCESS);
}

/*****************************************************************************
 * Name               : CfaIwfMainACValidate
 *
 *Description        : This function is called to validate the port and cvlan 
 *                     of the attachment circuit.
 *
 *Input(s)           :  u4PhyIfIndex - Interface id
 *                      u2VlanId  - Vlan id
 *
 *Output(s)          : pu4ACIfIndex - attchment circuit interface id
*****************************************************************************/
INT4
CfaIwfMainACValidate (UINT4 u4PhyIndex, tVlanId u2VlanId, UINT4 *pu4ACIfIndex)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4Index = 0;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    if ((u4PhyIndex > CFA_MAX_INTERFACES ()) || (u4PhyIndex == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in - CfaIwfMainACValidate "
                  "Invalid Interface Index - %d\n", u4PhyIndex);
        return CFA_FAILURE;
    }

    for (u4Index = CFA_MIN_AC_IF_INDEX; u4Index <= CFA_MAX_AC_IF_INDEX;
         u4Index++)
    {
        if (CfaGetIfInfo (u4Index, &IfInfo) == CFA_SUCCESS)
        {
            if ((IfInfo.u4ACPort == u4PhyIndex)
                && (IfInfo.u2ACVlan == u2VlanId))
            {
                *pu4ACIfIndex = u4Index;
                return CFA_SUCCESS;
            }
        }

    }
    CFA_DBG2 (CFA_TRC_ALL, CFA_IWF,
              "Error in - CfaIwfMainACValidate "
              "Getting AC Interface for physical index  %d and vlan %d failed\n",
              u4PhyIndex, u2VlanId);
    return (CFA_FAILURE);
}

#ifdef BCMX_WANTED
/*****************************************************************************
 * Name               : CfaOvrwriteEtherTypeBasedOnBridgeMode
 *
 * Description        : This function is called when an IP packet is received. 
 *                      It is used to change the ethertype of the received packet,
 *                      if the current bridge mode is PROVIDER BRIDGE.
 * 
 * Input(s)           : pBuf - Incoming Packet Buffer
 *
 * Output(s)          : pBuf - Modified Packet Buffer 
 *
 * Return Value(s)    : CFA_SUCCESS / CFA_FAILURE 
 *
 *****************************************************************************/

INT4
CfaOvrwriteEtherTypeBasedOnBridgeMode (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4BridgeMode = 0;
    UINT2               u2VlanProt = 0;

    if (L2IwfGetBridgeMode (L2IWF_DEFAULT_CONTEXT, &u4BridgeMode) ==
        L2IWF_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaOvrwriteEtherTypeBasedOnBridgeMode- "
                 "L2IWF-GetBridgeMode -  FAILURE.\n");
        return (CFA_FAILURE);
    }
    /* Packet Modification is not needed for Customer Bridge mode as by default,
     * 0x8100 ethertype is set when packet is trapped to CPU port in BCM. 
     * Hence, it is done only for Provider Bridge mode, as packet trapped to CPU port in BCM, 
     * requires 0x88a8 ethertype for Provider Bridge mode. */
    if ((u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE) ||
        (u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE))
    {
        /* Vlan Ethertype is copied from packet Buffer */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2VlanProt,
                                   CFA_VLAN_TAG_OFFSET, VLAN_ETHER_TYPE_LEN);
        if (u2VlanProt == VLAN_PROTOCOL_ID)
        {
            /* Ethertype is modified and copied to packet buffer */
            u2VlanProt = VLAN_PROVIDER_PROTOCOL_ID;
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2VlanProt,
                                       CFA_VLAN_TAG_OFFSET,
                                       VLAN_ETHER_TYPE_LEN);
        }
    }
    return (CFA_SUCCESS);
}
#endif

#ifdef WTP_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaIwfWssProcessTxFrame
 *
 *    Description        : This function processes outgoing frames on
 *                         radio interfaces. 
 *
 *    Input(s)            : tCRU_BUF_CHAIN_HEADER *pBuf,
 *                UINT4 u4IfIndex,
 *                UINT1 *au1DestHwAddr.
 *                UINT4 u4PktSize.
 *                UINT2 u2Protocol.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if frame is processed
 *                succeessfully, otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIwfWssProcessTxFrame (tCRU_BUF_CHAIN_HEADER * pCruBuf, UINT4 u4IfIndex,
                         UINT4 u4PktSize)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tCfaIfInfo          IfInfo;
    UINT1               u1VlanTagCheck = CFA_FALSE;

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        /* Memory should be freed wherever it is called */
        return CFA_FAILURE;
    }

    if (pCruBuf == NULL)
    {
        return CFA_FAILURE;
    }
    pBuf = pCruBuf;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IWF,
              "Entering CfaIwfWssProcessTxFrame for interface %d.\n",
              u4IfIndex);

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    if (CfaGddWrite (pBuf, u4IfIndex, u4PktSize, u1VlanTagCheck, &IfInfo) !=
        CFA_SUCCESS)
    {

        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                  "Exiting CfaIwfWssProcessTxFrame - GDD send fail on "
                  "interface %d - FAILURE\n", u4IfIndex);
    }
    else
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IWF,
                  "Exiting CfaIwfWssProcessTxFrame - write on "
                  "interface %d - SUCCESS\n", u4IfIndex);
    }

    return (CFA_SUCCESS);
}
#endif
#ifdef HDLC_WANTED
/*****************************************************************************
 * Name               : CfaValidateHdlcPacket
 *
 * Description        : This function is called on packet reception to validate
 *                      the packet and extract the header information.
 * 
 * Input(s)           : pBuf - Packet Buffer
 *                      u4PktSize - Size of the packet
 *
 * Output(s)          : *pu4HdlcIndex - HDLC index from packet buffer.
 *
 * Return Value(s)    : CFA_SUCCESS / CFA_FAILURE 
 *
 *****************************************************************************/
INT4
CfaValidateHdlcPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4PktSize,
                       UINT4 *pu4HdlcIndex)
{
    UINT1               u1IfType = 0;
    UINT1              *pu1InterfaceIndex = NULL;
    UINT1              *pu1HdlcHdr = NULL;

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

    pu1HdlcHdr = CRU_BUF_Get_DataPtr_IfLinear (pBuf, 12, 2);

    if ((pu1HdlcHdr[0] == 0xff) && (pu1HdlcHdr[1] == 0x3))
    {
        pu1InterfaceIndex = CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0, 12);
        CfaGetInterfaceIndexFromName (pu1InterfaceIndex, pu4HdlcIndex);
        CfaGetIfType (*pu4HdlcIndex, &u1IfType);

        if (u1IfType == CFA_HDLC)
        {
            if (CRU_BUF_Move_ValidOffset (pBuf, 12) != CRU_SUCCESS)
            {
                return CFA_FAILURE;
            }
            u4PktSize -= 12;
            return CFA_SUCCESS;
        }
        else
        {
            return CFA_FAILURE;
        }
    }
    return (CFA_FAILURE);
}
#endif /* HDLC_WANTED */
