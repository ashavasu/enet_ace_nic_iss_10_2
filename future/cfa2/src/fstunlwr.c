
/* $Id: fstunlwr.c,v 1.8 2014/05/12 10:40:03 siva Exp $*/
# include  "lr.h"
# include "cfa.h"
# include  "fssnmp.h"
# include  "fstunllw.h"
# include  "fstunlwr.h"
# include  "fstunldb.h"

INT4
GetNextIndexFsTunlIfTable (tSnmpIndex * pFirstMultiIndex,
                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsTunlIfTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsTunlIfTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

#ifdef SNMP_2_WANTED
VOID
RegisterFSTUNL ()
{
#ifdef SNMP_3_WANTED
    SNMPRegisterMibWithContextIdAndLock (&fstunlOID, &fstunlEntry,
                                         CfaLock, CfaUnlock,
                                         CfaUtilSetTnlCxt,
                                         CfaUtilResetTnlCxt,
                                         SNMP_MSR_TGR_FALSE);
#endif /* SNMP_3_WANTED */
    SNMPAddSysorEntry (&fstunlOID, (const UINT1 *) "fstunl");
}
#endif /*SNMP_2_WANTED */

INT4
FsTunlIfAddressTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
FsTunlIfLocalInetAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[1].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
FsTunlIfRemoteInetAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[2].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[2].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
FsTunlIfEncapsMethodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[3].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
FsTunlIfConfigIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[4].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
FsTunlIfHopLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTunlIfHopLimit (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiIndex->pIndex[2].pOctetStrValue,
                                    pMultiIndex->pIndex[3].i4_SLongValue,
                                    pMultiIndex->pIndex[4].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FsTunlIfSecurityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTunlIfSecurity (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiIndex->pIndex[2].pOctetStrValue,
                                    pMultiIndex->pIndex[3].i4_SLongValue,
                                    pMultiIndex->pIndex[4].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FsTunlIfTOSGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTunlIfTOS (pMultiIndex->pIndex[0].i4_SLongValue,
                               pMultiIndex->pIndex[1].pOctetStrValue,
                               pMultiIndex->pIndex[2].pOctetStrValue,
                               pMultiIndex->pIndex[3].i4_SLongValue,
                               pMultiIndex->pIndex[4].i4_SLongValue,
                               &(pMultiData->i4_SLongValue)));

}

INT4
FsTunlIfFlowLabelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTunlIfFlowLabel (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiIndex->pIndex[2].pOctetStrValue,
                                     pMultiIndex->pIndex[3].i4_SLongValue,
                                     pMultiIndex->pIndex[4].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsTunlIfMTUGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTunlIfMTU (pMultiIndex->pIndex[0].i4_SLongValue,
                               pMultiIndex->pIndex[1].pOctetStrValue,
                               pMultiIndex->pIndex[2].pOctetStrValue,
                               pMultiIndex->pIndex[3].i4_SLongValue,
                               pMultiIndex->pIndex[4].i4_SLongValue,
                               &(pMultiData->i4_SLongValue)));

}

INT4
FsTunlIfDirFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTunlIfDirFlag (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiIndex->pIndex[2].pOctetStrValue,
                                   pMultiIndex->pIndex[3].i4_SLongValue,
                                   pMultiIndex->pIndex[4].i4_SLongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsTunlIfDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTunlIfDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiIndex->pIndex[2].pOctetStrValue,
                                     pMultiIndex->pIndex[3].i4_SLongValue,
                                     pMultiIndex->pIndex[4].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsTunlIfEncaplmtGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTunlIfEncaplmt (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiIndex->pIndex[2].pOctetStrValue,
                                    pMultiIndex->pIndex[3].i4_SLongValue,
                                    pMultiIndex->pIndex[4].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FsTunlIfEncapOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTunlIfEncapOption (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       pMultiIndex->pIndex[3].i4_SLongValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsTunlIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTunlIfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].pOctetStrValue,
                                 pMultiIndex->pIndex[2].pOctetStrValue,
                                 pMultiIndex->pIndex[3].i4_SLongValue,
                                 pMultiIndex->pIndex[4].i4_SLongValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FsTunlIfAliasGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTunlIfAlias (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].pOctetStrValue,
                                 pMultiIndex->pIndex[2].pOctetStrValue,
                                 pMultiIndex->pIndex[3].i4_SLongValue,
                                 pMultiIndex->pIndex[4].i4_SLongValue,
                                 pMultiData->pOctetStrValue));

}

INT4
FsTunlIfCksumFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTunlIfCksumFlag (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiIndex->pIndex[2].pOctetStrValue,
                                     pMultiIndex->pIndex[3].i4_SLongValue,
                                     pMultiIndex->pIndex[4].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsTunlIfPmtuFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTunlIfPmtuFlag (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiIndex->pIndex[2].pOctetStrValue,
                                    pMultiIndex->pIndex[3].i4_SLongValue,
                                    pMultiIndex->pIndex[4].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FsTunlIfStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTunlIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTunlIfStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  pMultiIndex->pIndex[2].pOctetStrValue,
                                  pMultiIndex->pIndex[3].i4_SLongValue,
                                  pMultiIndex->pIndex[4].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsTunlIfHopLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTunlIfHopLimit (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiIndex->pIndex[2].pOctetStrValue,
                                    pMultiIndex->pIndex[3].i4_SLongValue,
                                    pMultiIndex->pIndex[4].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FsTunlIfTOSSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTunlIfTOS (pMultiIndex->pIndex[0].i4_SLongValue,
                               pMultiIndex->pIndex[1].pOctetStrValue,
                               pMultiIndex->pIndex[2].pOctetStrValue,
                               pMultiIndex->pIndex[3].i4_SLongValue,
                               pMultiIndex->pIndex[4].i4_SLongValue,
                               pMultiData->i4_SLongValue));

}

INT4
FsTunlIfFlowLabelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTunlIfFlowLabel (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiIndex->pIndex[2].pOctetStrValue,
                                     pMultiIndex->pIndex[3].i4_SLongValue,
                                     pMultiIndex->pIndex[4].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsTunlIfDirFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTunlIfDirFlag (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiIndex->pIndex[2].pOctetStrValue,
                                   pMultiIndex->pIndex[3].i4_SLongValue,
                                   pMultiIndex->pIndex[4].i4_SLongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsTunlIfDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTunlIfDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiIndex->pIndex[2].pOctetStrValue,
                                     pMultiIndex->pIndex[3].i4_SLongValue,
                                     pMultiIndex->pIndex[4].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsTunlIfEncaplmtSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTunlIfEncaplmt (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiIndex->pIndex[2].pOctetStrValue,
                                    pMultiIndex->pIndex[3].i4_SLongValue,
                                    pMultiIndex->pIndex[4].i4_SLongValue,
                                    pMultiData->u4_ULongValue));

}

INT4
FsTunlIfEncapOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTunlIfEncapOption (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       pMultiIndex->pIndex[3].i4_SLongValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsTunlIfAliasSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTunlIfAlias (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].pOctetStrValue,
                                 pMultiIndex->pIndex[2].pOctetStrValue,
                                 pMultiIndex->pIndex[3].i4_SLongValue,
                                 pMultiIndex->pIndex[4].i4_SLongValue,
                                 pMultiData->pOctetStrValue));

}

INT4
FsTunlIfCksumFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTunlIfCksumFlag (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiIndex->pIndex[2].pOctetStrValue,
                                     pMultiIndex->pIndex[3].i4_SLongValue,
                                     pMultiIndex->pIndex[4].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsTunlIfPmtuFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTunlIfPmtuFlag (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiIndex->pIndex[2].pOctetStrValue,
                                    pMultiIndex->pIndex[3].i4_SLongValue,
                                    pMultiIndex->pIndex[4].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FsTunlIfStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTunlIfStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  pMultiIndex->pIndex[2].pOctetStrValue,
                                  pMultiIndex->pIndex[3].i4_SLongValue,
                                  pMultiIndex->pIndex[4].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsTunlIfHopLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsTunlIfHopLimit (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       pMultiIndex->pIndex[3].i4_SLongValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsTunlIfTOSTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    return (nmhTestv2FsTunlIfTOS (pu4Error,
                                  pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  pMultiIndex->pIndex[2].pOctetStrValue,
                                  pMultiIndex->pIndex[3].i4_SLongValue,
                                  pMultiIndex->pIndex[4].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsTunlIfFlowLabelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsTunlIfFlowLabel (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].pOctetStrValue,
                                        pMultiIndex->pIndex[3].i4_SLongValue,
                                        pMultiIndex->pIndex[4].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsTunlIfDirFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsTunlIfDirFlag (pu4Error,
                                      pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiIndex->pIndex[2].pOctetStrValue,
                                      pMultiIndex->pIndex[3].i4_SLongValue,
                                      pMultiIndex->pIndex[4].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsTunlIfDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsTunlIfDirection (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].pOctetStrValue,
                                        pMultiIndex->pIndex[3].i4_SLongValue,
                                        pMultiIndex->pIndex[4].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsTunlIfEncaplmtTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsTunlIfEncaplmt (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       pMultiIndex->pIndex[3].i4_SLongValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
FsTunlIfEncapOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsTunlIfEncapOption (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiIndex->pIndex[2].pOctetStrValue,
                                          pMultiIndex->pIndex[3].i4_SLongValue,
                                          pMultiIndex->pIndex[4].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsTunlIfAliasTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2FsTunlIfAlias (pu4Error,
                                    pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiIndex->pIndex[2].pOctetStrValue,
                                    pMultiIndex->pIndex[3].i4_SLongValue,
                                    pMultiIndex->pIndex[4].i4_SLongValue,
                                    pMultiData->pOctetStrValue));

}

INT4
FsTunlIfCksumFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsTunlIfCksumFlag (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].pOctetStrValue,
                                        pMultiIndex->pIndex[3].i4_SLongValue,
                                        pMultiIndex->pIndex[4].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsTunlIfPmtuFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsTunlIfPmtuFlag (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       pMultiIndex->pIndex[3].i4_SLongValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsTunlIfStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsTunlIfStatus (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiIndex->pIndex[2].pOctetStrValue,
                                     pMultiIndex->pIndex[3].i4_SLongValue,
                                     pMultiIndex->pIndex[4].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsTunlIfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsTunlIfTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
