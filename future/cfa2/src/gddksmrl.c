/****************************************************************************/
/* Copyright (C) 2011 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2011                                          */
/*                                                                           */
/* $Id: gddksmrl.c,v 1.6 2015/07/16 10:51:34 siva Exp $                                                  */
/*                                                                           */
/*  FILE NAME             : gddksmrl.c                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : Packet transmission and reception in the kernel  */
/*  MODULE NAME           : Security Module                                  */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE :                                                  */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains Marvell target specific       */
/*                          functions used by kernel                         */
/*****************************************************************************/
#ifndef _GDDKSEC_MRVL_C_
#define _GDDKSEC_MRVL_C_

#include "cfa.h"
#include "secmod.h"
#include "secgen.h"
#include "gddksec.h"
#include "gddksmrl.h"
#include "sectrc.h"
#include "secdec.h"


UINT1 gau1DSATag[CFA_DSA_TAG_LEN];

/*****************************************************************************
 *
 *    Function Name     : GddKSecMoveOffSet
 *
 *    Description       : This functin moves the Offset based on target.
 *                        Making DSA tag invisible in case of marvell boards
 *
 *    Input(s)          : pSkb - The packet buffer
 *
 *    Output(s)         : pSkb - Modifies the offset
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            : CFA_SUCCESS / CFA_FAILURE.
 *
 *****************************************************************************/
INT4
GddKSecMoveOffSet (tSkb *pSkb)
{
    /* For Marvel boards move the pointer forward 
     * to make IP header visible.
     * */
    UINT4               u4SrcIp = 0;
    UINT4               u4DstIp = 0;
    SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecMoveOffSet:"
            "Entering function GddKSecMoveOffSet !!!\r\n");


    u4SrcIp = *((UINT4 *) (pSkb->data + IP_PKT_OFF_SRC));
    u4DstIp = *((UINT4 *) (pSkb->data + IP_PKT_OFF_DEST));
 
    u4SrcIp = OSIX_NTOHL (u4SrcIp);
    u4DstIp = OSIX_NTOHL (u4DstIp);

    /* Do not strip packets with src/dest IP 127.0.0.1 (snort and ISS IPC) */
    if ((u4SrcIp != INADDR_LOOPBACK) && (u4DstIp != INADDR_LOOPBACK))
    {
        
    pSkb->data += CFA_DSA_TAG_OFFSET;
    pSkb->len -= CFA_DSA_TAG_OFFSET;
    }

    SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecMoveOffSet:"
            "Exiting function GddKSecMoveOffSet !!!\r\n");

    return CFA_SUCCESS;
}
/*****************************************************************************
 *
 *    Function Name     : nf_register_hook
 *
 *    Description       : This function is used in case of Marvell as nf_register_hook
 *                        is not defined in the Linux kernel
 *                      
 *
 *    Input(s)          : nf_hook_ops
 *
 *    Output(s)         : int
 *             
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            : CFA_SUCCESS / CFA_FAILURE.
 *
 *****************************************************************************/

int nf_register_hook(struct nf_hook_ops *reg)
{
        return 0;
}

/*****************************************************************************
 *
 *    Function Name     : GddKSecPortGetDirection
 *
 *    Description       : This function is used to parse target specific tag,
 *                        retrieve ifindex, vlanid and remove the tag.
 *
 *    Input(s)          : pBuf - The packet buffer
 *
 *    Output(s)         : pBuf - Modifies the packet with specific tag removed.
 *                        pu1Direction - The direction of the packet.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            : CFA_SUCCESS / CFA_FAILURE.
 *
 *****************************************************************************/
INT4
GddKSecPortGetDirection (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1Direction, tNetDevice * pNetDev)
{
    UINT2               u2DsaIfIndex = 0;
    UINT2               u2DsaVlanId = 0;
    UINT2               u2IsTrunk = 0;
    UINT2               u2TrunkId = 0;
    UINT2               u2AggIndex = 0;

    UINT1               u1NwType = CFA_NETWORK_TYPE_LAN;
    UINT1               au1Buf[SEC_MODULE_DATA_SIZE];
    UINT1               u1Offset = 0;

    UNUSED_PARAM(pNetDev);
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecPortGetDirection:"
                "Entering function GddKSecPortGetDirection !!!\r\n");

    MEMSET(pu1Direction, 0, sizeof(UINT1));

    /*
       MAC1------MAC2-----DSA W0-----DSA W1------IP header 
              ^
              ^
       W0[19:23] ==> Port DSA IfIndex  
       W0[18]    ==> Incase of trunk port ,this bit is 1
       If W0[18] is true then W1[29:30] ==> Trunk Id
       W1[29:30] W0[19:23] ==> CPSS Link Aggegator index 
     */

    /* In order to move the data pointer to the start of the MAC header ,move 4 bytes before */
    pBuf->pSkb->data -= CFA_DSA_TAG_OFFSET;
    pBuf->pSkb->len += CFA_DSA_TAG_OFFSET;

    /* Extract W0[19:23] from the DSA tag */
    u2DsaIfIndex =  OSIX_NTOHS (*((UINT1 *) ((VOID *) (pBuf->pSkb->data) +
                    CFA_DSA_TAG_PORT_OFFSET)));

    u2DsaIfIndex = u2DsaIfIndex>>CFA_DSA_PORT_BITSHIFT; /* 11 */ /* remove extra bits */

    /* Extract VLAN ID from the DSA Tag */ 
    u2DsaVlanId =   OSIX_NTOHS (*
            ((UINT2 *) ((VOID *) (pBuf->pSkb->data) +
                CFA_DSA_TAG_VLAN_OFFSET)));

    u2DsaVlanId = (CFA_VLAN_VID_MASK & u2DsaVlanId); /* remove extra bits */

    /* Extract one bit W0[18]=> isTrunk from the DSA tag */
    u2IsTrunk =  OSIX_NTOHS (*
            ((UINT1 *) ((VOID *) (pBuf->pSkb->data) + 13)));


    u2IsTrunk = u2IsTrunk>>15; /*To remove extra bits */

    if(u2IsTrunk == CFA_TRUE)
    {
        SEC_TRC_ARG2 (SEC_CONTROL_PLANE_TRC, "GddKSecPortGetDirection:"
                "Packet Received is from LAGG interface u2DsaIfIndex %d u2DsaVlanId %s !!!\r\n",u2DsaIfIndex,u2DsaVlanId);
        /*
           MAC1------MAC2-----DSA W0----DSA W1----IP header
           ^                        
           Now make the data pointer point the Word1 
           MAC1 + MAC2+ DSA W0 = 6 +6 +4 = 16bytes  */

        pBuf->pSkb->data += 16;
        pBuf->pSkb->len -= 16;

        /* Extract two bits W1[29:30] Trunk Id from the DSA tag */
        u2TrunkId =  OSIX_NTOHS (*
                ((UINT1 *) ((VOID *) (pBuf->pSkb->data))));

        u2TrunkId =  u2TrunkId>>8; 
        /* Right shift 13 to remove extra bits ,left shift 5 to perform OR with W0[19:23]
           (13 right shift  - 5 left shift)  = 8 right shift */


        /* Example LAGG Index configured = 29 , CPS AGG index = 56
           Packet received with following DSA tag information 
           To retrive the CPSS AGG index from the DSA TAG , OR operation is performed 
           between W1[29:30] and W0[19:23] 

           W1                                     W0      
           ======================================================================       
           30       29     23       22     21      20      19      
           TrunkID[6:0]    bit 6   bit 5   bit 4   bit 3   bit 2   bit 1   bit 0       
           ======================================================================
           Value Rxed 
           in DSA Tag       0        1      1       1       0       0       0   In Decimal ==>56 
           ======================================================================
           */
        u2DsaIfIndex = u2DsaIfIndex | u2TrunkId;

        /*The  gaSecWanIfInfo [] / gaSecLanIfInfo [] arrays are updated for Layer3 VLAN interface only.
         * In case of router port, the physical interface index is same as the Layer interface index.
         * Whereas in case of IVR, there can be many physical interfaces that are part of the  IVR VLAN.
         * Hence interface network type is also maintained per physical interface.
         */
        /*Computing the Cfa Agg Index from the CPSS Agg index */
        u2AggIndex  = (((u2DsaIfIndex -
                        SECURITY_LA_TRUNK_ID_START_INDEX) / 2) + 1) +
            SECURITY_SYS_DEF_MAX_PHYSICAL_INTERFACES;

        u2DsaIfIndex = u2AggIndex;

        /* Move the data pointer back to its original position */
        pBuf->pSkb->data -= 16;
        pBuf->pSkb->len += 16;
    }
    else
    {
        /* Increment so that port is mapped correctly, this is based on XCAT NPAPI mapping */
        u2DsaIfIndex++;
            SEC_TRC_ARG1 (SEC_CONTROL_PLANE_TRC, "GddKSecPortGetDirection:"
                    "Updated the u2DsaIfIndex according to port mapping u2DsaIfIndex %d !!!\r\n",u2DsaIfIndex);
    }

    if (SecUtilGetPhyIfNwType(u2DsaIfIndex, &u1NwType) == OSIX_SUCCESS)
    {   
        if (u1NwType == CFA_NETWORK_TYPE_WAN)
        {
            *pu1Direction = SEC_INBOUND;
            SEC_SET_DIRECTION(pBuf, *pu1Direction);
            SEC_TRC_ARG1 (SEC_CONTROL_PLANE_TRC, "GddKSecPortGetDirection:"
                    "Updated the direction as SEC_INBOUND In Packet u2DsaIfIndex %d !!!\r\n",u2DsaIfIndex);
        }
        else
        {
            *pu1Direction = SEC_OUTBOUND;
            SEC_SET_DIRECTION(pBuf, *pu1Direction);
            SEC_TRC_ARG1 (SEC_CONTROL_PLANE_TRC, "GddKSecPortGetDirection:"
                    "Updated the direction as SEC_OUTBOUND In Packet u2DsaIfIndex %d !!!\r\n",u2DsaIfIndex);
        }
    }
    /* Prepare VLAN tag for Sending out */
    u2DsaVlanId = OSIX_HTONS (u2DsaVlanId);

    /* Remove the DSA tag here
     * 1. Move Src,Dest Mac to local variable au1Buf.
     * 2. Remove first 20 bytes ( Src Mac + Dst Mac + DSA tag)
     * 3. Append Vlan tag to local variable au1Buf
     * 4. Prepend au1Buf to pBuf (packet buffer)
     */
    if (CRU_FAILURE == CRU_BUF_Copy_FromBufChain (pBuf, au1Buf, 0,
                CFA_VLAN_TAG_OFFSET))
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecPortGetDirection:"
                "Failed to Copy from Buffer !!!\r\n");
        return CFA_FAILURE;
    }

    if (CRU_FAILURE == CRU_BUF_Move_ValidOffset (pBuf,
                (CFA_DSA_TAG_START + CFA_DSA_TAG_LEN)))
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecPortGetDirection:"
                "Failed to Move Valid Offset !!!\r\n");
        return CFA_FAILURE;
    }

    /* Append VLAN Tag used for security processing */
    u1Offset = CFA_VLAN_TAG_OFFSET;
    au1Buf[u1Offset] = CFA_VLAN_PROTOCOL_ID_BYTE_1;
    u1Offset++;
    au1Buf[u1Offset] = CFA_VLAN_PROTOCOL_ID_BYTE_2;
    u1Offset++;
    MEMCPY(&au1Buf[u1Offset],&u2DsaVlanId,sizeof(u2DsaVlanId));
    u1Offset += sizeof(u2DsaVlanId);
    /* u1Offset will now hold length of au1Buf */

    /* Prepend au1Buf to pBuf */
    if (CRU_FAILURE == CRU_BUF_Prepend_BufChain (pBuf, au1Buf, u1Offset))
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecPortGetDirection:"
                "CRU_BUF_Prepend_BufChain Failed !!!\r\n");
        return CFA_FAILURE;
    }

    /* Update the Out Tag which will be used for transmitting the packet */
    GddKSecUpdateOutTag(*pu1Direction, u2DsaVlanId);
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecPortGetDirection:"
                "Exiting Function GddKSecUpdateOutTag !!!\r\n");

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name     : GddKSecUpdateOutTag
 *
 *    Description       : This function updates the Tag required for sending 
 *                        out the packet
 *                        
 *                        
 *
 *    Input(s)          : u1Direction - Direction of the packet.(INBOUND/OUTBOUND)
 *                        u2DsaVlanId - VlanId for the packet.
 *
 *    Output(s)         : None.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : gau1DSATag
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            : CFA_SUCCESS / CFA_FAILURE.
 *
 *****************************************************************************/
INT4
GddKSecUpdateOutTag(UINT1 u1Direction, UINT2 u2DsaVlanId)
{

      SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecUpdateOutTag:"
               "Entering function GddKSecUpdateOutTag !!!\r\n");

    MEMSET(gau1DSATag, 0, (sizeof(UINT1)*CFA_DSA_TAG_LEN));

    /* Generate out DSA tag used in packet retransmission
     * DSA Tag Breakdown:
     * Consists of 8 bytes (2 words)
     * Word 0 [31:0] Word 1 [31:0]
     * Note: Reserved means it must be set as 0.
     * (bit position) -> use
     * Word0(first 4 bytes)
     * 31:30 -> TagCommand
     * 29    -> Tagged/Untagged
     * 28:24 -> SrcDev
     * 23:19 -> SrcPort/SrcTrunk
     * 18    -> SrcIsTrunk
     * 17    -> Reserved (UNUSED)
     * 16    -> CFI (UNUSED)
     * 15:13 -> UP (UNUSED)
     * 12    -> Extend (UNUSED)
     * 11:0  -> Vlan ID of packet
     *
     * Word 1 (2nd 4 bytes)
     * 31    -> Extend (UNUSED)
     * 30    -> SrcTrunk[6]/Reserved
     * 29    -> SrcTrunk[5]/SrcPort[5]
     * 28    -> EgressFilter Registered
     * 27    -> DropOnSource
     * 26    -> PacketIsLooped
     * 25    -> Routed
     * 24:20 -> Packet Src ID
     * 19:13 -> QoSProfile
     * 12    -> use_vidx(use vlanid)
     * if use_vidx is true
     * 11:0  -> VlanID[11:0]
     * else
     * 11    -> Reserved
     * 10:5  -> TrgPort
     * 4:0   -> TrgDev
     * */
    gau1DSATag[0] = CFA_DSA_OUT_CPU_CODE;
    /* get port to send to and convert to DSA tag port */
    gau1DSATag[1] = CONVERT_CPSS_PORT_TO_DSA(SECURITY_CASCD_OUTBOUND_PORT);

    if (SEC_OUTBOUND == u1Direction)
    {
      SEC_TRC_ARG1 (SEC_CONTROL_PLANE_TRC, "GddKSecUpdateOutTag:"
               "Update the VlanId in DSA tag for OUTBOUND Packet u2DsaVlanId %d !!!\r\n",u2DsaVlanId);
        MEMCPY(&gau1DSATag[2], &u2DsaVlanId, sizeof(u2DsaVlanId));
    }
    else
    {
        /* Set the Vlan ID to DEFAULT VLAN ID */
        gau1DSATag[2] = CFA_DSA_DEFAULT_VLAN_BYTE_1;
        gau1DSATag[3] = CFA_DSA_DEFAULT_VLAN_BYTE_2;
    }
    gau1DSATag[2] |= CFA_DSA_OUT_INFO_1;

    gau1DSATag[4] = CFA_DSA_OUT_INFO_2;
    gau1DSATag[5] = CFA_DSA_OUT_INFO_3;

    if (SEC_OUTBOUND == u1Direction)
    {    
      SEC_TRC_ARG1 (SEC_CONTROL_PLANE_TRC, "GddKSecUpdateOutTag:"
               "Update the VlanId in DSA tag for OUTBOUND Packet u2DsaVlanId %d !!!\r\n",u2DsaVlanId);
        MEMCPY(&gau1DSATag[6], &u2DsaVlanId, sizeof(u2DsaVlanId));
    }
    else
    {
        /* Set the Vlan ID to DEFAULT VLAN ID */
        gau1DSATag[6] = CFA_DSA_DEFAULT_VLAN_BYTE_1;
        gau1DSATag[7] = CFA_DSA_DEFAULT_VLAN_BYTE_2;
    }
    gau1DSATag[6] |= CFA_DSA_OUT_INFO_4;


      SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecUpdateOutTag:"
               "Exiting Function GddKSecUpdateOutTag !!!\r\n");
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name     : GddKSecAddSpecificTag
 *
 *    Description       : This function adds the specific tag based on 
 *                        the target used. In case of marvell adds DSA tag.
 *                        gau1DSATag IS CLEARED IN THIS FUNCTION.
 *                        
 *
 *    Input(s)          : pBuf - The packet buffer
 *
 *    Output(s)         : pBuf - Adds specific tag to the buffer.
 *
 *    Global Variables Referred : gau1DSATag
 *
 *    Global Variables Modified : gau1DSATag
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            : CFA_SUCCESS / CFA_FAILURE.
 *
 *****************************************************************************/
INT4
GddKSecAddSpecificTag (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               au1Buf[CFA_ENET_V2_HEADER_SIZE];

      SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecAddSpecificTag:"
               "Entering Function GddKSecAddSpecificTag !!!\r\n");

    /* ADD DSA tag before sending the packet
     * To add DSA tag
     * 1.Copy the SrcMac,DstMac (first 12 bytes)
     * 2.Remove the Ethernet header from frame (first 14 bytes)
     * 3.Prepend the DSA tag (stored in gau1DsaTag)
     * 4.Prepend the SrcMac,DstMac (Vlan is not needed)
     */
    /* Copy srcmac, dst mac to aut1Buf */
    if (CRU_FAILURE == CRU_BUF_Copy_FromBufChain (pBuf, au1Buf, 0,
                                          (CFA_ENET_V2_HEADER_SIZE - CFA_VLAN_PROTOCOL_SIZE)))
    {
      SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecAddSpecificTag:"
               "Failed to Copy from Buffer !!!\r\n");
        return CFA_FAILURE;
    }
    
    /* Delete ethernet header from pBuf */
    if (CRU_FAILURE == CRU_BUF_Move_ValidOffset (pBuf, CFA_ENET_V2_HEADER_SIZE + CFA_VLAN_PROTOCOL_SIZE))
    {
      SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecAddSpecificTag:"
               "Failed to Move Valid Offset !!!\r\n");
      return CFA_FAILURE;
    }

    /* Prepend DSA tag to pBuf */
    if (CRU_FAILURE == CRU_BUF_Prepend_BufChain (pBuf, gau1DSATag, CFA_DSA_TAG_LEN))
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecAddSpecificTag:"
                "Failed to Prepend DSA tag to buffer !!!\r\n");
        return CFA_FAILURE;
    }
    /* Used the gau1DsaTag, now clear it */
    MEMSET(gau1DSATag, 0, (sizeof(UINT1)*CFA_DSA_TAG_LEN));

    /* Prepend au1Buf(src mac, dst mac) to pBuf */
    if (CRU_FAILURE == CRU_BUF_Prepend_BufChain (pBuf, au1Buf, 
                                           (CFA_ENET_V2_HEADER_SIZE - CFA_VLAN_PROTOCOL_SIZE)))
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecAddSpecificTag:"
                "Failed to Prepend au1Buf(srcmac,dstmac) to buffer !!!\r\n");
        return CFA_FAILURE;
    }
      SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecAddSpecificTag:"
               "Exiting Function GddKSecAddSpecificTag !!!\r\n");
    return CFA_SUCCESS;
}
#endif
