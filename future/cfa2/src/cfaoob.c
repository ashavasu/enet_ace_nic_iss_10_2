/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfaoob.c,v 1.42 2016/03/06 10:07:09 siva Exp $ 
 *
 * Description: This file contains the portable routines related to
 *              OOB Interface Configurations. OOB Interface  is a 
 *              System Interface dedicated for Management Operations.
 *              It need to be handled  specific to the  underlying 
 *              Operating System.This files contains code for OOB 
 *              Interface Support in LINUX and VX_WORKS 
 *******************************************************************/
#include "cfainc.h"

#if defined (IP_WANTED) || defined (LNXIP4_WANTED) || defined (BSDCOMP_SLI_WANTED)
#include "selutil.h"
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#if defined (OS_PTHREADS) || defined (OS_CPSS_MAIN_OS)
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#endif
#include <fcntl.h>
#endif /* IP_WANTED || LNXIP4_WANTED */

/* Linux Ipv6 addr structure for OOB linux programming. */
#if defined (IP6_WANTED) && (defined (CLI_LNXIP_WANTED) || defined (SNMP_LNXIP_WANTED))
struct in6_ifreq {
   struct in6_addr ifr6_addr;
   unsigned int       ifr6_prefixlen;
   int     ifr6_ifindex;
};
#endif


#ifdef LNXIP4_WANTED
/******************************************************************************
 * Function           : CfaGddOobGetOsHwAddr 
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : au1HwAddr - Hw addr of the interface.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function gets the hardware address of the 
 *                      Out Of Band Port (i.e. Linux ethernet port). 
 ******************************************************************************/
INT4
CfaGddOobGetOsHwAddr (UINT4 u4IfIndex, UINT1 *pu1HwAddr)
{
    struct ifreq        ifr;
    INT4                i4SockId = -1;

    MEMSET (&ifr, 0, sizeof (struct ifreq));

    i4SockId = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_ALL));

    STRCPY (ifr.ifr_name, CFA_GDD_PORT_NAME (u4IfIndex));

    if (ioctl (i4SockId, SIOCGIFHWADDR, &ifr) < 0)
    {
        perror ("CfaGddOobGetOsHwAddr: ioctl failed\r\n");
        close (i4SockId);
        return CFA_FAILURE;
    }

    close (i4SockId);
    MEMCPY (pu1HwAddr, ifr.ifr_hwaddr.sa_data, CFA_ENET_ADDR_LEN);

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobOpen 
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function opens the Out of band port.
 ******************************************************************************/
INT4
CfaGddOobOpen (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);

    /* In Linux IP, OOB port would have been created 
     * during bootup. So the interface is not created here. */

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobConfigPort
 * Input(s)           : u4IfIndex - Interface index.
 *                      u1ConfigOption -
 *                      pConfigParam -
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE.
 * Action             : This function is used for configuration of linux
 *                      ethernet port.
 ******************************************************************************/
INT4
CfaGddOobConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1ConfigOption);

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobClose 
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function deleted the Out Of Band Port 
 ******************************************************************************/
INT4
CfaGddOobClose (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);

    /* In LinuxIp, OOB port is a physical port and cannot be deleted. */

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobRead 
 * Input(s)           : u4IfIndex - Interface index.
 *                      pu1DataBuf - Buf read
 *                      pu4PktSize - Size of the read pkt.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function reads the data from the linux 
 *                      ethernet port.
 ******************************************************************************/
INT4
CfaGddOobRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4PktSize);

    /* LinuxIP takes handles packet processing for OOB port */

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobWrite 
 * Input(s)           : u4IfIndex - Interface index.
 *                      pu1DataBuf - Buf to be sent out.
 *                      u4PktSize - size of the pkt to be sent out
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function writes the data to ethernet driver. 
 ******************************************************************************/
INT4
CfaGddOobWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PktSize);

    /* LinuxIp handles packet transmission for OOB port */

    return CFA_SUCCESS;
}
#else /* LNXIP4_WANTED is undefined */

#if defined (IP_WANTED) || defined (BSDCOMP_SLI_WANTED)

#if defined (OS_PTHREADS) || defined (OS_CPSS_MAIN_OS)

/******************************************************************************
 * Function           : CfaGddProcessOobDataRcvdEvent 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None 
 * Action             : This function Receives the Packet from OOB  Interface
 *                      and deliver it to Higher Layer.
 ******************************************************************************/
INT4
CfaGddProcessOobDataRcvdEvent (VOID)
{
#if !defined(CLI_LNXIP_WANTED) && !defined(SNMP_LNXIP_WANTED)
    UINT1              *pu1DataBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT2               u4IfIndex = CFA_OOB_MGMT_IFINDEX;
    UINT4               u4PktSize;
    UINT1               u1IfType;
    UINT1               u1OperStatus;

    /* If the OperStatus is down,Socket should have been closed.
       Ignore the Event for data reception on OOB Interface */
    CfaGetIfOperStatus ((UINT4) u4IfIndex, &u1OperStatus);
    if (u1OperStatus == CFA_IF_DOWN)
    {
        return (CFA_FAILURE);
    }

    /* Allocate Buffer for Receiving Data from Socket */
    if ((pu1DataBuf = MemAllocMemBlk (gCfaDriverMtuPoolId)) == NULL)
    {

        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaGddProcessRecvEvent - " "No Memory - FAILURE.\n");
        /* Add the OOB Interface Socket Descriptor to Select 
         * task for Packet Reception*/
        SelAddFd (CFA_GDD_PORT_DESC (u4IfIndex), CfaOobDataRcvd);
        return (CFA_FAILURE);
    }

    CFA_LOCK ();
    /* Read all the data from the interface */
    while ((CFA_GDD_READ_FNPTR (u4IfIndex))
           (pu1DataBuf, u4IfIndex, &u4PktSize) != CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_GDD,
                  "In CfaGddProcessRecvEvent - got data from interface %d.\n",
                  u4IfIndex);

        CFA_DBG1 (CFA_TRC_ALL, CFA_GDD,
                  "In CfaGddProcessRecvEvent - got %d bytes.\n", u4PktSize);

        /* allocate CRU Buf */
        if ((pBuf = CRU_BUF_Allocate_MsgBufChain ((u4PktSize +
                                                   CFA_MAX_FRAME_HEADER_SIZE),
                                                  CFA_MAX_FRAME_HEADER_SIZE)) ==
            NULL)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaGddProcessRecvEvent - "
                     "No CRU Buf Available - FAILURE.\n");

            CFA_IF_SET_IN_DISCARD (u4IfIndex);
            MemReleaseMemBlock (gCfaDriverMtuPoolId, (UINT1 *) pu1DataBuf);
            CFA_UNLOCK ();
            /* Add the OOB Interface Socket Descriptor to Select 
             * task for Packet Reception*/
            SelAddFd (CFA_GDD_PORT_DESC (u4IfIndex), CfaOobDataRcvd);
            return CFA_FAILURE;
        }

        /* copy data from linear buffer into CRU buffer - it is expected that the
           higher layers will fill the CRU interface struct if required */
        if (CRU_BUF_Copy_OverBufChain (pBuf, pu1DataBuf, 0, u4PktSize) ==
            CRU_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaGddProcessRecvEvent - "
                     "unable to copy to CRU Buf- FAILURE.\n");

            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CFA_IF_SET_IN_DISCARD (u4IfIndex);
            continue;
        }

        /* Check for Higher layer.If not found. Release buffer */
        if (CFA_GDD_HL_RX_FNPTR (u4IfIndex) == NULL)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CFA_IF_SET_IN_DISCARD (u4IfIndex);
            continue;
        }

        if (CfaGetIfType ((UINT4) u4IfIndex, &u1IfType) != CFA_SUCCESS)
        {
            CFA_IF_SET_IN_DISCARD (u4IfIndex);
            MemReleaseMemBlock (gCfaDriverMtuPoolId, (UINT1 *) pu1DataBuf);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CFA_UNLOCK ();
            return CFA_FAILURE;
        }

        /* send the buffer to the higher layer */
        if ((CFA_GDD_HL_RX_FNPTR (u4IfIndex))
            (pBuf, u4IfIndex, u4PktSize, u1IfType,
             CFA_ENCAP_NONE) != CFA_SUCCESS)
        {
            /* release buffer which were not successfully sent to 
             * higher layer */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        }
    }

    /* free the linear buffer allocated */
    MemReleaseMemBlock (gCfaDriverMtuPoolId, (UINT1 *) pu1DataBuf);

    CFA_UNLOCK ();

    /* Add the OOB Interface Socket Descriptor to Select 
     * task for Packet Reception*/
    SelAddFd (CFA_GDD_PORT_DESC (u4IfIndex), CfaOobDataRcvd);
#endif
    return (CFA_SUCCESS);
}

/******************************************************************************
 * Function           : CfaOobDataRcvd 
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function sends Event to CFA Task upon Packet 
 *                      arrival on OOB Interface 
 ******************************************************************************/
VOID
CfaOobDataRcvd (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);
    if (CFA_INITIALISED != TRUE)
    {
        return;
    }
    /* Send an event to CFA task as if packet is received on Socket */
    OsixEvtSend (CFA_TASK_ID, CFA_OOB_DATA_RCVD_EVENT);
}

/******************************************************************************
 * Function           : CfaGddOobGetOsHwAddr 
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : au1HwAddr - Hw addr of the interface.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function get the hardware address of the 
 *                      Linux ethernet port. 
 ******************************************************************************/
INT4
CfaGddOobGetOsHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
    struct ifreq        ifr;    /* Interface structure */
    INT4                i4CommandStatus;

#if defined(CLI_LNXIP_WANTED) || defined(SNMP_LNXIP_WANTED)
    INT4 i4PortDesc = CFA_INVALID_DESC;
    /* Get the OOB Interface Name in Issnvram.txt */
    /* and copy it into  GDD Structure */
    STRNCPY (CFA_GDD_PORT_NAME (u4IfIndex), IssGetInterfaceFromNvRam (),
             STRLEN (IssGetInterfaceFromNvRam ()));
    CFA_GDD_PORT_NAME (u4IfIndex)[STRLEN (IssGetInterfaceFromNvRam ())] = '\0';

    if ((i4PortDesc = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_ALL))) < 0)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_GDD, "Error in opening OOB port!\n");
        return CFA_FAILURE;
    }

    CFA_GDD_PORT_DESC (u4IfIndex) = i4PortDesc;

#else
    /* If Socket is not opened for the port, open it */
    if (CFA_GDD_PORT_DESC (u4IfIndex) == CFA_INVALID_DESC)
    {
        if (CfaGddOobOpen (u4IfIndex) == CFA_FAILURE)
        {
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                      "Error in Opening Port \r\n", u4IfIndex);
            return (CFA_FAILURE);
        }
    }
#endif

    /* Invoke the IOCTL call on the socket to get Mac Address  */
    /* To avoid klowork warning, the sizeof(ifr.ifr_name) is used.
     * */
    STRNCPY (ifr.ifr_name, CFA_GDD_PORT_NAME (u4IfIndex),
             sizeof (ifr.ifr_name));
    if ((i4CommandStatus =
         ioctl (CFA_GDD_PORT_DESC (u4IfIndex), SIOCGIFHWADDR, &ifr)) < 0)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddOobGetOsHwAddr - "
                  "IOCTL on Interface %u failed - FAILURE.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    MEMCPY (au1HwAddr, ifr.ifr_hwaddr.sa_data, CFA_ENET_ADDR_LEN);

#if defined(CLI_LNXIP_WANTED) || defined(SNMP_LNXIP_WANTED)
    /* Close the Opened Socket */
    if (close (CFA_GDD_PORT_DESC (u4IfIndex)) < 0)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Error in Closing the Socket \r\n ");
        return (CFA_FAILURE);
    }
    CFA_GDD_PORT_DESC (u4IfIndex) = CFA_INVALID_DESC;
#endif

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_GDD,
              "In CfaGddOobGetOsHwAddr - "
              "Got Address of interface %u - SUCCESS.\n", u4IfIndex);

    return CFA_SUCCESS;

}

/******************************************************************************
 * Function           : CfaGddOobOpen 
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function opens the a Linux ethernet interface
 *                      and enables promiscuous mode
 ******************************************************************************/
INT4
CfaGddOobOpen (UINT4 u4IfIndex)
{
#if !defined(CLI_LNXIP_WANTED) && !defined(SNMP_LNXIP_WANTED)
    struct sockaddr_ll  Enet;
    struct ifreq        ifr;
    INT4                i4PortDesc = 0;

    MEMSET (&Enet, 0, sizeof (Enet));
    MEMSET (&ifr, 0, sizeof (ifr));

    /* If Socket is opened already opened,don't open
     * it again */
    if (CFA_GDD_PORT_DESC (u4IfIndex) != CFA_INVALID_DESC)
    {
        return CFA_SUCCESS;
    }
    /* Get the OOB Interface Name in Issnvram.txt */
    /* and copy it into  GDD Structure */
    STRNCPY (CFA_GDD_PORT_NAME (u4IfIndex), IssGetInterfaceFromNvRam (),
             MEM_MAX_BYTES((STRLEN (IssGetInterfaceFromNvRam ())),STRLEN (CFA_GDD_PORT_NAME (u4IfIndex))));
    CFA_GDD_PORT_NAME (u4IfIndex)[STRLEN (IssGetInterfaceFromNvRam ())] = '\0';

    if ((i4PortDesc = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_ALL))) < 0)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_GDD, "Error in opening OOB port!\n");
        return CFA_FAILURE;
    }

    CFA_GDD_PORT_DESC (u4IfIndex) = i4PortDesc;

    SNPRINTF (ifr.ifr_name, IFNAMSIZ, "%s", CFA_GDD_PORT_NAME (u4IfIndex));
    if (ioctl (i4PortDesc, SIOCGIFINDEX, (char *) &ifr) < 0)
    {
        perror ("Interface Index Get Failed");
        close (i4PortDesc);
        return CFA_FAILURE;
    }

    Enet.sll_family = AF_PACKET;
    Enet.sll_ifindex = ifr.ifr_ifindex;

    if (bind (i4PortDesc, (struct sockaddr *) &Enet, sizeof (Enet)) < 0)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_GDD, "Bind Failed for OOB port!\n");
        close (i4PortDesc);
        return CFA_FAILURE;
    }

    /* set options for non-blocking mode */
    if (fcntl (i4PortDesc, F_SETFL, O_NONBLOCK) < 0)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_GDD,
                 "Setting ETH SOCK option for OOB port!\n");
        close (i4PortDesc);
        return CFA_FAILURE;
    }

    /* Enable Promiscous Mode */
    CfaGddOobConfigPort (u4IfIndex, CFA_ENET_EN_PROMISC);

    KW_FALSEPOSITIVE_FIX3 (i4PortDesc);
#else
    /* In case of LINUX IP support over OOB, no need
     * to open RAW socket over OOB port. */
    UNUSED_PARAM (u4IfIndex);
#endif

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobConfigPort
 * Input(s)           : u4IfIndex - Interface index.
 *                      u1ConfigOption -
 *                      pConfigParam -
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE.
 * Action             : This function is used for configuration of linux
 *                      ethernet ports.
 ******************************************************************************/
INT4
CfaGddOobConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption)
{

#if !defined(CLI_LNXIP_WANTED) && !defined(SNMP_LNXIP_WANTED)
    struct ifreq        ifr;    /* Interface structure */
    INT4                i4CommandStatus;
#else
    tIpConfigInfo       IpConfigInfo;
    UINT4               u4Flag = 0;
    UINT4               u4SecIpAddr = 0;
    UINT4               u4SecIpMask = 0;
#endif
    INT4                i4RetVal = CFA_SUCCESS;

    CFA_DBG2 (CFA_TRC_ALL, CFA_GDD,
              "Entering CfaGddOobConfigPort for interface %d and config %d.\n",
              u4IfIndex, u1ConfigOption);

#if !defined(CLI_LNXIP_WANTED) && !defined(SNMP_LNXIP_WANTED)
    /* Check whether Interface Structures are Initialized */
    if ((!CFA_IF_ENTRY_USED (u4IfIndex)) || (!CFA_GDD_ENTRY_USED (u4IfIndex)))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                  "Error in CfaGddOobConfigPort - un-United interface %d.\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

    /* Dont configure for ports which are not registered - we dont have
       socket/file descriptors for them */
    if (CFA_GDD_REGSTAT (u4IfIndex) == CFA_INVALID)
    {

        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddOobConfigPort - "
                  "Interface %d not Registered - FAILURE.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    switch (u1ConfigOption)
    {

        case CFA_ENET_EN_PROMISC:
            /* Get the eth interface flags */
            STRNCPY (ifr.ifr_name, CFA_GDD_PORT_NAME (u4IfIndex),
                     sizeof (ifr.ifr_name));
            if ((i4CommandStatus =
                 ioctl (CFA_GDD_PORT_DESC (u4IfIndex), SIOCGIFFLAGS, &ifr)) < 0)
            {
                CFA_DBG (ALL_FAILURE_TRC, CFA_GDD,
                         "Getting interface flags Failed for OOB port!\n");
                i4RetVal = CFA_FAILURE;
                break;
            }

            /* set the IFF_PROMISC flag to enter promiscuous mode and 
             * set eth interface flags */
            ifr.ifr_flags = ifr.ifr_flags | IFF_PROMISC;
            if ((i4CommandStatus = ioctl (CFA_GDD_PORT_DESC (u4IfIndex),
                                          SIOCSIFFLAGS, &ifr)) < 0)
            {
                CFA_DBG (ALL_FAILURE_TRC, CFA_GDD,
                         "Setting promiscuous mode Failed for OOB port!\n");
                i4RetVal = CFA_FAILURE;
            }
            else
            {
                CFA_IF_PROMISC (u4IfIndex) = CFA_ENABLED;
            }
            break;

        case CFA_ENET_DIS_PROMIS:
            /* Get eth interface flags */
            STRNCPY (ifr.ifr_name, CFA_GDD_PORT_NAME (u4IfIndex),
                     sizeof (ifr.ifr_name));
            if ((i4CommandStatus =
                 ioctl (CFA_GDD_PORT_DESC (u4IfIndex), SIOCGIFFLAGS, &ifr)) < 0)
            {
                CFA_DBG (ALL_FAILURE_TRC, CFA_GDD,
                         "Getting interface flags Failed for OOB port!\n");
                i4RetVal = CFA_FAILURE;
                break;
            }

            /* reset the IFF_PROMISC flag to disable promiscuous mode and 
             * set eth interface flags */
            ifr.ifr_flags = ifr.ifr_flags & (~IFF_PROMISC);
            /* To avoid pkt queing when interface admin status is down */
            if ((i4CommandStatus = ioctl (CFA_GDD_PORT_DESC (u4IfIndex),
                                          SIOCSIFFLAGS, &ifr)) < 0)
            {
                CFA_DBG (ALL_FAILURE_TRC, CFA_GDD,
                         "Setting promiscuous mode Failed for OOB port!\n");
                i4RetVal = CFA_FAILURE;
            }
            else
            {
                CFA_IF_PROMISC (u4IfIndex) = CFA_DISABLED;
            }
            break;

        default:
            i4RetVal = CFA_FAILURE;
            break;
    }
#else
#ifdef RM_WANTED
    /* When OOB interface status change is being handled in case of active node.
     * During IF_DOWN of OOB, interface ip in linux is cleared and 
     * configured again on receiving IF_UP indication for OOB interface. 
     * In case of standby node, no linux interface for OOB will be present. */
    if (RmGetNodeState() == RM_ACTIVE)
    {
#endif
    MEMSET (&IpConfigInfo, 0, sizeof(tIpConfigInfo));
    if (u1ConfigOption == CFA_IF_UP)
    {
        CfaIpIfGetIfInfo (CFA_DEFAULT_ROUTER_IFINDEX, &IpConfigInfo);
        u4Flag = CFA_IP_IF_ALLOC_PROTO | CFA_IP_IF_ALLOC_METHOD |
            CFA_IP_IF_PRIMARY_ADDR | CFA_IP_IF_NETMASK |
            CFA_IP_IF_FORWARD | CFA_IP_IF_BCASTADDR;
        i4RetVal = CfaIpUpdateInterfaceParams (CFA_CDB_IF_ALIAS (u4IfIndex), 
                                   u4Flag, &IpConfigInfo);
#ifdef IP6_WANTED
        CfaGetIfIp6Addr (CFA_DEFAULT_ROUTER_IFINDEX, &IpConfigInfo);
        CfaLinuxIpUpdateIpv6Addr (CFA_DEFAULT_ROUTER_IFINDEX, &IpConfigInfo.Ip6Addr,
                        (INT4) IpConfigInfo.u1PrefixLen, OSIX_TRUE);
#endif
    }
    else if (u1ConfigOption == CFA_IF_DOWN)
    {
        u4Flag = CFA_IP_IF_NOFORWARD;
#ifdef IP6_WANTED
        CfaLinuxIpUpdateIpv6Addr (CFA_DEFAULT_ROUTER_IFINDEX, &IpConfigInfo.Ip6Addr, 
                         (INT4) IpConfigInfo.u1PrefixLen, OSIX_FALSE);
#endif
        i4RetVal = CfaIpUpdateInterfaceParams (CFA_CDB_IF_ALIAS (u4IfIndex), 
                         u4Flag, NULL);
    }
#ifdef RM_WANTED
    }
    if (u1ConfigOption == CFA_IF_UP)
    {
        u4SecIpAddr = IssGetSecIpAddrFromNodeId ();
        u4SecIpMask = IssGetSecSubnetMaskFromNodeId ();
        if (u4SecIpAddr != 0)
        {
            /* Create OOB Secondary Interface in Linux */
            CfaIfSetOOBLocalNodeSecondaryIpAddress(u4SecIpAddr, u4SecIpMask);
        }
    }
#endif /* RM_WANTED */
#endif

    if (i4RetVal == CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_GDD,
                  "In CfaGddOobConfigPort - Configured interface %d - SUCCESS.\n",
                  u4IfIndex);
    }
    else
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "In CfaGddOobConfigPort - Configuration of "
                  "interface %d - FAILURE.\n", u4IfIndex);
    }

    return (i4RetVal);
}

/******************************************************************************
 * Function           : CfaGddOobClose 
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function disables promiscuos mode of the 
 *                      Linux ethernet port. 
 ******************************************************************************/
INT4
CfaGddOobClose (UINT4 u4IfIndex)
{
#if !defined(CLI_LNXIP_WANTED) && !defined(SNMP_LNXIP_WANTED)
    /* No Socket Descriptor to close */
    if (CFA_GDD_PORT_DESC (u4IfIndex) == CFA_INVALID_DESC)
    {
        return CFA_SUCCESS;
    }

    /* Disable the promiscuous mode of the port */
    CfaGddOobConfigPort (u4IfIndex, CFA_ENET_DIS_PROMIS);

    /* Remove the FD added with Select Task */
    SelRemoveFd (CFA_GDD_PORT_DESC (u4IfIndex));

    /* Close the Opened Socket */
    if (close (CFA_GDD_PORT_DESC (u4IfIndex)) < 0)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Error in Closing the Socket \r\n ");
        return (CFA_FAILURE);
    }

    CFA_GDD_PORT_DESC (u4IfIndex) = CFA_INVALID_DESC;
#else
    UNUSED_PARAM (u4IfIndex);
#endif

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobRead 
 * Input(s)           : u4IfIndex - Interface index.
 *                      pu1DataBuf - Buf read
 *                      pu4PktSize - Size of the read pkt.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function reads the data from the linux 
 *                      ethernet port.
 ******************************************************************************/
INT4
CfaGddOobRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
#if !defined(CLI_LNXIP_WANTED) && !defined(SNMP_LNXIP_WANTED)
    struct sockaddr_ll  Enet;
    struct sockaddr    *pPort;
    int                 i4FromAddrLen = 0;
    INT4                i4ReadBytes;
    tEnetV2Header      *pEthHdr;
    tEnetSnapHeader    *pSnapEthHdr;
    INT2                i2IpTotalLen = 0;
    UINT2               u2Protocol = 0xFF;
    UINT2               u2LenOrType;
    UINT4               u4ExtraBytes = 0;
    UINT1               u1EnetHeaderSize;
    struct ifreq        ifr;

    MEMSET (&Enet, 0, sizeof (Enet));
    MEMSET (&ifr, 0, sizeof (ifr));

    SNPRINTF (ifr.ifr_name, IFNAMSIZ, "%s", CFA_GDD_PORT_NAME (u4IfIndex));
    if (ioctl (CFA_GDD_PORT_DESC (u4IfIndex), SIOCGIFINDEX, (char *) &ifr) < 0)
    {
        perror ("Interface Index Get Failed");
        close (CFA_GDD_PORT_DESC (u4IfIndex));
        return CFA_FAILURE;
    }

    Enet.sll_family = AF_PACKET;
    Enet.sll_ifindex = ifr.ifr_ifindex;

    i4FromAddrLen = sizeof (Enet);

    pPort = (struct sockaddr *) &Enet;

    /* Receive the Packet From OOB Port */
    if ((i4ReadBytes = recvfrom (CFA_GDD_PORT_DESC (u4IfIndex),
                                 (VOID *) pu1DataBuf, CFA_MAX_DRIVER_MTU, 0,
                                 (struct sockaddr *) pPort,
                                 (socklen_t *) & i4FromAddrLen)) <= 0)
    {
        return CFA_FAILURE;
    }

    CFA_IF_SET_IN_OCTETS (u4IfIndex, (UINT4) i4ReadBytes);
    pEthHdr = (tEnetV2Header *) (VOID *) pu1DataBuf;
    u2LenOrType = OSIX_NTOHS (pEthHdr->u2LenOrType);

    if (CFA_ENET_IS_TYPE (u2LenOrType))
    {
        u2Protocol = u2LenOrType;
        u1EnetHeaderSize = CFA_ENET_V2_HEADER_SIZE;
    }
    else
    {
        pSnapEthHdr = (tEnetSnapHeader *) (VOID *) pu1DataBuf;
        u1EnetHeaderSize = CFA_ENET_SNAP_HEADER_SIZE;

        /* check for LLC control frame first - we expect only LLC in SNAP now */
        if (pSnapEthHdr->u1Control == CFA_LLC_CONTROL_UI)
        {
            /* Check for presence of SNAP which may carry IP/ARP/RARP 
             * after LLC */
            if ((pSnapEthHdr->u1DstLSap == CFA_LLC_SNAP_SAP) ||
                (pSnapEthHdr->u1SrcLSap == CFA_LLC_SNAP_SAP) ||
                (pSnapEthHdr->u1Oui1 == 0x00) ||
                (pSnapEthHdr->u1Oui2 == 0x00) || (pSnapEthHdr->u1Oui3 == 0x00))
            {
                /* determine which protocol is being carried */
                u2Protocol = OSIX_NTOHS (pSnapEthHdr->u2ProtocolType);
            }

        }

    }

    if (u2Protocol == CFA_ENET_IPV4)
    {
        i2IpTotalLen = (INT2) (OSIX_NTOHS (*(INT2 *) (VOID *) (pu1DataBuf +
                                                               u1EnetHeaderSize
                                                               +
                                                               IP_PKT_OFF_LEN)));

        if ((i2IpTotalLen + u1EnetHeaderSize) > i4ReadBytes)
        {
            return CFA_FAILURE;
        }

        u4ExtraBytes =
            (UINT4) (i4ReadBytes - (i2IpTotalLen + u1EnetHeaderSize));
    }
#ifdef IP6_WANTED
    else if (u2Protocol == CFA_ENET_IPV6)
    {
        i2IpTotalLen = (INT2) (OSIX_NTOHS (*(INT2 *) (VOID *) (pu1DataBuf +
                                                               u1EnetHeaderSize
                                                               +
                                                               IPV6_OFF_PAYLOAD_LEN)));

        if ((i2IpTotalLen + u1EnetHeaderSize + IPV6_HEADER_LEN) > i4ReadBytes)
        {
            return CFA_FAILURE;
        }

        u4ExtraBytes =
            (UINT4) (i4ReadBytes - (i2IpTotalLen + u1EnetHeaderSize +
                                    IPV6_HEADER_LEN));
    }
#endif /*IP6_WANTED */

    (*pu4PktSize) = (UINT4) i4ReadBytes - u4ExtraBytes;
#else
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4PktSize);
#endif
    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobWrite 
 * Input(s)           : u4IfIndex - Interface index.
 *                      pu1DataBuf - Buf to be sent out.
 *                      u4PktSize - size of the pkt to be sent out
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function writes the data to ethernet driver. 
 ******************************************************************************/
INT4
CfaGddOobWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
#if !defined(CLI_LNXIP_WANTED) && !defined(SNMP_LNXIP_WANTED)
    struct sockaddr_ll  Enet;
    UINT4               u4ToAddrLen;
    INT4                i4WrittenBytes;
    struct ifreq        ifr;

    MEMSET (&Enet, 0, sizeof (Enet));
    MEMSET (&ifr, 0, sizeof (ifr));

    SNPRINTF (ifr.ifr_name, IFNAMSIZ, "%s", CFA_GDD_PORT_NAME (u4IfIndex));
    if (ioctl (CFA_GDD_PORT_DESC (u4IfIndex), SIOCGIFINDEX, (char *) &ifr) < 0)
    {
        PRINTF ("\r\nInterface Index Get Failed\r\n");
        close (CFA_GDD_PORT_DESC (u4IfIndex));
        return CFA_FAILURE;
    }

    Enet.sll_family = AF_PACKET;
    Enet.sll_ifindex = ifr.ifr_ifindex;
    u4ToAddrLen = sizeof (Enet);

    /* Send the Packet Over the OOB Interface through the socket Opened */
    if ((i4WrittenBytes =
         sendto (CFA_GDD_PORT_DESC (u4IfIndex), (VOID *) pu1DataBuf, u4PktSize,
                 0, (struct sockaddr *) &Enet, (socklen_t) u4ToAddrLen)) < 0)
    {
        PRINTF ("\n\nInterface Sendto Failed\n\n");
        return CFA_FAILURE;
    }

    if (i4WrittenBytes != (INT4) u4PktSize)
        return CFA_FAILURE;
#else
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PktSize);
#endif
    return CFA_SUCCESS;

}

#endif /*OS_PTHREADS || OS_CPSS_MAIN_OS */

#if defined (OS_VXWORKS)        /*Vxworks Defined */

/* In VxWorks,OOB Interface uses  MUX Libraries */
#include <muxLib.h>

#define FCS_EXTRA_BYTES        4
#define MUX_SUCCESS            OK

/* Queue for Packets received on OOB Interface */
#define   CFA_PKT_OOB_QUEUE        "CFAOOB"
#define   CFA_OOB_PKT_QUEUE_DEPTH   20

tOsixQId            CfaOobQId;
/* Decriptor for the OOB iInterface Binded with VxWorks System */
VOID               *gpCookieDesc = NULL;
UINT4               u4Spare = 0;

BOOL CfaSendPktToMux PROTO ((VOID *pCookiePtr, UINT4 u4PktType,
                             M_BLK_ID pMuxBuf, LL_HDR_INFO * pLinkHdrInfo,
                             VOID *pSpare));

BOOL CfaRcvPktFromMux PROTO ((VOID *pCookiePtr, UINT4 u4PktType,
                              M_BLK_ID pMuxBuf, LL_HDR_INFO * pLinkHdrInfo,
                              VOID *pSpare));
M_BLK_ID CfaGddNetMblkFromBufCopy PROTO ((UINT1 *pu1DataBuf,
                                          UINT4 u4IfIndex, UINT4 u4PktSize));

VOID                CfaGetVxIfIndexAndAliasForIfName (UINT1 *pu1IfName,
                                                      UINT4 *pu4Index,
                                                      UINT1 *pu1IfAlias);

/******************************************************************************
 * Function           : CfaGddProcessOobDataRcvdEvent 
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : au1HwAddr - Hw addr of the interface.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function Reads the Buffer from the data received on OOB
 *                      Interface Queue and deliver it to Higher Layer.
 ******************************************************************************/
INT4
CfaGddProcessOobDataRcvdEvent (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pCruBuf;
    UINT4               u4IfIndex;
    UINT4               u4PktSize;
    UINT1               u1IfType;

    while (OsixQueRecv (CfaOobQId, (UINT1 *) &pCruBuf,
                        OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        CFA_LOCK ();
        /* Extract the IfIndex and Packet size */
        u4IfIndex = pCruBuf->ModuleData.InterfaceId.u4IfIndex;
        u4PktSize = pCruBuf->pFirstValidDataDesc->u4_ValidByteCount;

        if (CfaGetIfType (u4IfIndex, &u1IfType) == CFA_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                     "In CfaMain Pkt received in interrupt mode failed to deliver to HL.\r\n");
        }
        else
        {
            /* Handover the Packet to Receive module */
            if ((CFA_GDD_HL_RX_FNPTR ((UINT2) u4IfIndex))
                (pCruBuf, (UINT2) u4IfIndex, u4PktSize, u1IfType,
                 CFA_ENCAP_NONE) != CFA_SUCCESS)
            {
                /* Release buffer which were not successfully sent to
                 * higher layer */
                CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "In CfaMain Pkt received in interrupt mode failed to deliver to HL.\n");
            }
            else
            {
                /* Increment after successful handling of packet */
                CFA_IF_SET_IN_OCTETS ((UINT2) u4IfIndex, u4PktSize);
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "In CfaMain Pkt received in interrupt mode delivered to HL.\n");
            }
        }
        CFA_UNLOCK ();
    }
    return (CFA_SUCCESS);
}

/******************************************************************************
 * Function           : CfaGddOobGetOsHwAddr 
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : au1HwAddr - Hw addr of the interface.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function get the hardware address of the 
 *                      VxWorks System Interface. 
 ******************************************************************************/
INT4
CfaGddOobGetOsHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
    UINT1               au1Addr[CFA_ENET_ADDR_LEN];

    /* If OOB Port is not opened,Open it */
    if (gpCookieDesc == NULL)
    {
        CfaGddOobOpen (u4IfIndex);
    }

    /* Get the H/w Address using the Mux Library Fn */
    if (muxIoctl (gpCookieDesc, EIOCGADDR, (INT1 *) au1Addr) != MUX_SUCCESS)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_GDD,
                 "Getting the Hardware Address Failed for OOB port!\n");
        return (CFA_FAILURE);
    }

    MEMCPY ((VOID *) au1HwAddr, (VOID *) au1Addr, CFA_ENET_ADDR_LEN);

    return (CFA_SUCCESS);

}

/******************************************************************************
 * Function           : CfaGddOobOpen 
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function Opens the VxWorks System Interface for
 *                      OOB and register CallBack for receiving the Packet.  
 ******************************************************************************/
INT4
CfaGddOobOpen (UINT4 u4IfIndex)
{
    UINT1               au1IfAlias[10];

    /* If VxWorks System Interface  is created for OOB, return */
    if (gpCookieDesc != NULL)
    {
        UtlTrcPrint ("CfaGddMuxOpen ... Valid COOKIE_DESC Found!\n");
        return (CFA_SUCCESS);
    }

    u4Spare = (UINT4) u4IfIndex;

    /* Get the OOB Interface Name in Issnvram.txt */
    /* and copy it into  GDD Structure */
    STRCPY (CFA_GDD_PORT_NAME (u4IfIndex), IssGetInterfaceFromNvRam ());

    CfaGetVxIfIndexAndAliasForIfName (CFA_GDD_PORT_NAME (u4IfIndex),
                                      &u4IfIndex, au1IfAlias);

    /* Bind to the Interface  read from issnvram.txt */
    if ((gpCookieDesc = muxBind ((INT1 *) au1IfAlias, u4IfIndex,
                                 (FUNCPTR) CfaRcvPktFromMux, NULL,
                                 NULL, NULL, MUX_PROTO_PROMISC, "MuxCfa",
                                 (VOID *) &u4Spare)) == NULL)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_GDD, "Bind Failed for OOB port!\n");
        return CFA_FAILURE;
    }

    /* Enable Promiscuous mode */
    muxIoctl (gpCookieDesc, (int) EIOCSFLAGS, (void *) MUX_PROTO_PROMISC);

    /* Crate a Queue to receive Packets from OOB Interface */
    if (OsixQueCrt ((UINT1 *) CFA_PKT_OOB_QUEUE, OSIX_MAX_Q_MSG_LEN,
                    CFA_OOB_PKT_QUEUE_DEPTH, &CfaOobQId) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaInit - OOB Queue Creation Fail -"
                 "FAILURE.\n");
        if (muxUnbind ((INT4 *) gpCookieDesc, MUX_PROTO_PROMISC,
                       CfaRcvPktFromMux) != MUX_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                     "Error in UnBinding Device \r\n ");
        }
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobConfigPort 
 * Input(s)           : u4IfIndex - Interface index.
 *                      u1ConfigOption - Option to Configure
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : Enable/Disable Promiscous mode on the VxWorks 
                        System interface Opened for OOB Port  
 ******************************************************************************/

INT4
CfaGddOobConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption)
{
    UNUSED_PARAM (u4IfIndex);
    /* Check whether VxSystem Interface Opened */
    if (gpCookieDesc == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "No Valid Cookie Descriptor \r\n ");
        return (CFA_FAILURE);
    }
    if (u1ConfigOption == CFA_ENET_EN_PROMISC)
    {
        /* Enable Promiscuous mode */
        muxIoctl (gpCookieDesc, (int) EIOCSFLAGS, (void *) MUX_PROTO_PROMISC);
    }
    else if (u1ConfigOption == CFA_ENET_DIS_PROMIS)
    {
        return CFA_SUCCESS;
    }
    else
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Invalid Option to Configure \r\n ");
        return (CFA_FAILURE);
    }
    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobClose 
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : Closes the VxWorks System interface Opened for OOB Port  
 ******************************************************************************/
INT4
CfaGddOobClose (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    /* If No VxSystem Interface Opened,return */
    if (gpCookieDesc == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "No Valid Cookie Descriptor \r\n ");
        return (CFA_SUCCESS);
    }

    if (muxUnbind ((INT4 *) gpCookieDesc, MUX_PROTO_PROMISC,
                   CfaRcvPktFromMux) != MUX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Error in Unbinding Device \r\n ");
        return (CFA_FAILURE);
    }

    /* Delete the Queue created for packets received  on OOB Interface */
    OsixQueDel (CfaOobQId);

    u4Spare = 0;

    gpCookieDesc = NULL;

    return (CFA_SUCCESS);

}

/******************************************************************************
 * Function           : CfaRcvPktFromMux 
 * Input(s)           : u4IfIndex - Interface index.
 *                      pu1DataBuf - Buf read
 *                      pu4PktSize - Size of the read pkt.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function reads the data from the VxWorks Driver 
 *                      ethernet port.
 ******************************************************************************/
BOOL
CfaRcvPktFromMux (VOID *pCookiePtr, UINT4 u4PktType, M_BLK_ID pMuxBuf,
                  LL_HDR_INFO * pLinkHdrInfo, VOID *pSpare)
{

    UINT1              *pu1DataBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4PktSize;
    UINT4               u4Len;

    UNUSED_PARAM (pCookiePtr);
    UNUSED_PARAM (u4PktType);
    UNUSED_PARAM (pLinkHdrInfo);
    UNUSED_PARAM (pSpare);

    CFA_DBG (CFA_TRC_ALL, CFA_GDD, "CfaRcvPktFromMux: Pkt Arrived: \n");

    if (CFA_INITIALISED != TRUE)
    {
        CFA_IF_SET_IN_DISCARD (CFA_OOB_MGMT_IFINDEX);
        CFA_DBG (ALL_FAILURE_TRC, CFA_GDD,
                 "CfaRcvPktFromMux: CFA is not initialized!\n");
        netMblkClChainFree (pMuxBuf);
        return FALSE;
    }
    u4PktSize = pMuxBuf->mBlkPktHdr.len;

    pu1DataBuf = MemAllocMemBlk (gCfaPktPoolId);

    if (pu1DataBuf == NULL)
    {
        CFA_IF_SET_IN_DISCARD (CFA_OOB_MGMT_IFINDEX);
        CFA_DBG (ALL_FAILURE_TRC, CFA_GDD,
                 "CfaRcvPktFromMux: Linear Buffer Allocation - FAILURE!\n");
        netMblkClChainFree (pMuxBuf);
        return FALSE;
    }

    /* Copy data from MuxBuffer to Data Buffer */
    if ((u4Len =
         netMblkToBufCopy (pMuxBuf, (INT1 *) pu1DataBuf, NULL)) != u4PktSize)
    {
        MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1DataBuf);
        netMblkClChainFree (pMuxBuf);
        CFA_IF_SET_IN_DISCARD (CFA_OOB_MGMT_IFINDEX);
        CFA_DBG (ALL_FAILURE_TRC, CFA_GDD,
                 "CfaRcvPktFromMux: mBuf To Linear Buffer Copy - FAILURE!\n");
        return FALSE;
    }

    /* Free the NET Buffer */
    netMblkClChainFree (pMuxBuf);

    /* Allocate CRU Buffer and copy data in Linear Buffer into it */
    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0)) == NULL)
    {
        MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1DataBuf);
        CFA_IF_SET_IN_DISCARD (CFA_OOB_MGMT_IFINDEX);
        CFA_DBG (ALL_FAILURE_TRC, CFA_GDD,
                 "CfaRcvPktFromMux: CRU Buffer Allocation - FAILURE!\n");
        return FALSE;
    }

    if (CRU_BUF_Copy_OverBufChain (pBuf, pu1DataBuf, 0,
                                   u4PktSize) != CRU_SUCCESS)
    {
        MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1DataBuf);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        CFA_IF_SET_IN_DISCARD (CFA_OOB_MGMT_IFINDEX);
        CFA_DBG (ALL_FAILURE_TRC, CFA_GDD,
                 "CfaRcvPktFromMux: CRU Buffer Copy BufChain - FAILURE!\n");
        return FALSE;
    }

    pBuf->ModuleData.InterfaceId.u4IfIndex = CFA_OOB_MGMT_IFINDEX;
    pBuf->pFirstValidDataDesc->u4_ValidByteCount = u4PktSize;

    /* Free the Linear Buffer as CRU_BUFFER is ready at this point of time */
    MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1DataBuf);

    /* Send the Message to Queue for OOB */
    if (OsixQueSend (CfaOobQId, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN) == OSIX_SUCCESS)

    {
        /* send the event to the CFA task */
        OsixEvtSend (CFA_TASK_ID, CFA_OOB_DATA_RCVD_EVENT);
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        CFA_DBG (ALL_FAILURE_TRC, CFA_GDD,
                 "CfaRcvPktFromMux: Enqueuing to CFA Q - FAILURE!\n");
        return FALSE;
    }

    return TRUE;
}

/******************************************************************************
 * Function           : CfaGddOobRead 
 * Input(s)           : u4IfIndex - Interface index.
 *                      pu1DataBuf - Buf read
 *                      pu4PktSize - Size of the read pkt.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function is not needed in VxWorks.
 ******************************************************************************/
INT4
CfaGddOobRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4PktSize);
    /* Packet will be received through Interupt in VxWorks */
    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobWrite 
 * Input(s)           : u4IfIndex - Interface index.
 *                      pu1DataBuf - Buf to be sent out.
 *                      u4PktSize - size of the pkt to be sent out
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function writes the data to VxWorks ethernet driver. 
 ******************************************************************************/
INT4
CfaGddOobWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    M_BLK_ID            pNetBuf = NULL;

    CFA_DBG (CFA_TRC_ALL, CFA_GDD, " Entering CfaGddMuxWrite \n");

    if (u4IfIndex == 0)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_GDD,
                 "u4IfIndex is zero - CfaGddMuxWrite Failure!\n");
        return CFA_FAILURE;
    }

    if ((pNetBuf = CfaGddNetMblkFromBufCopy (pu1DataBuf, u4IfIndex, u4PktSize))
        == NULL)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_GDD,
                 "-E- CfaGddMuxWrite: CfaGddNetMblkFromBufCopy Failure!\r\n");
        return CFA_FAILURE;
    }

    if (muxSend ((INT4 *) gpCookieDesc, pNetBuf) != MUX_SUCCESS)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_GDD,
                 "Error while Sending. Device not found(ENETDOWN) \r\n");
        netMblkClChainFree (pNetBuf);
        return (CFA_FAILURE);
    }

    return (CFA_SUCCESS);
}

/******************************************************************************
 * Function           : CfaGddNetMblkFromBufCopy 
 * Input(s)           : u4IfIndex - Interface index.
 *                      pu1DataBuf - Buf to be sent out.
 *                      u4PktSize - size of the pkt to be sent out
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function Copies the data received on OOB Interface
 *                      into Buffer 
 ******************************************************************************/

M_BLK_ID
CfaGddNetMblkFromBufCopy (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    NET_POOL_ID         pNetPool;
    END_OBJ            *pEnd;
    M_BLK_ID            pMbuf = NULL;
    M_BLK_ID            pNextMbuf = NULL;
    M_BLK_ID            pCurMbuf = NULL;
    UINT4               u4ThisPktLen;
    UINT1               u1FirstCopy;
    UINT1               au1IfAlias[10];

    /* Reference Pointer to the linear Buffer,as it's required to move 
     * the pointer,we need to have a reference pointer, otherwise,
     * the caller cann't free the linear buffer from where it's
     * originated */
    UINT1              *pu1Data = pu1DataBuf;

    CfaGetVxIfIndexAndAliasForIfName (CFA_GDD_PORT_NAME (u4IfIndex),
                                      &u4IfIndex, au1IfAlias);

    /* Get the Device (END_OBJ) Structure for retrieval of the Driver Net Pool
     * to construct mBlk-clBlk Net buffer */
    pEnd = endFindByName ((INT1 *) au1IfAlias, u4IfIndex);

    /* When there is no such device OR the Pkt size is less than or equal
     * to zero, then no need to construct a Net buffer */
    if (pEnd == NULL || u4PktSize <= 0)
    {
        return NULL;
    }

    pNetPool = pEnd->pNetPool;

    /* Get the MBLK-CL Construct from the NetPool 
     * It's assumed that enough buffer will always
     * get allocated when the call to netTupleGet
     * is made, if not available, this function wil
     * return failure, so in such case, we won't handoff
     * the packet, simply discarded
     */
    pMbuf = netTupleGet (pNetPool, pNetPool->clSizeMax, M_DONTWAIT,
                         MT_DATA, TRUE);

    if (pMbuf == NULL)
    {
        UtlTrcPrint
            ("-E- CfaGddNetMblkFromBufCopy - pMbuf - netTupleGet failed to allocate\n");
        return NULL;
    }

    /* Some Physical devices demands us to reserve
     * space for stuffing FCS */
    if ((u4PktSize + FCS_EXTRA_BYTES) < pNetPool->clSizeMax)
    {
        MEMCPY (pMbuf->mBlkHdr.mData, pu1Data, u4PktSize);
        pMbuf->mBlkHdr.mLen = u4PktSize;
    }
    else
    {
        /* For the very first time, no need
         * to allocate mBuf, as it's already done.
         * So to keep track this, u1FirstCopy is set 
         */
        u1FirstCopy = 1;
        while (u4PktSize > 0)
        {
            /* Determine the length of the current pkt
             * At any point of time, the Pkt can hold 
             * only Maximum of the Cluster Size Bytes.
             * 
             * Some Physical devices demands us to reserve
             * space for stuffing FCS, so 4 bytes were
             * reserved.
             */
            u4ThisPktLen = (u4PktSize + FCS_EXTRA_BYTES) < pNetPool->clSizeMax ?
                u4PktSize : (pNetPool->clSizeMax - FCS_EXTRA_BYTES);

            if (u1FirstCopy)
            {
                MEMCPY (pMbuf->mBlkHdr.mData, pu1Data, u4ThisPktLen);
                pMbuf->mBlkHdr.mLen = u4ThisPktLen;
                /* Reset this flag  so that next
                 * time onwards, it can allocate new mBuf
                 * for copying the remaining data
                 */
                u1FirstCopy = 0;
                /* Set Reference to pMbuf for further chaining */
                pCurMbuf = pMbuf;
            }
            else
            {
                /* Get the MBLK-CL Construct from the NetPool 
                 * It's assumed that enough buffer will always
                 * get allocated when the call to netTupleGet
                 * is made, if not available, this function wil
                 * return failure, so in such case, we won't handoff
                 * the packet, simply discarded
                 */
                pNextMbuf = NULL;
                pNextMbuf =
                    netTupleGet (pNetPool, pNetPool->clSizeMax, M_DONTWAIT,
                                 MT_DATA, TRUE);
                if (pNextMbuf == NULL)
                {
                    netMblkClChainFree (pMbuf);
                    return NULL;
                }
                MEMCPY (pNextMbuf->mBlkHdr.mData, pu1Data, u4ThisPktLen);
                pNextMbuf->mBlkHdr.mLen = u4ThisPktLen;

                /* Form the Chain */
                pCurMbuf->mBlkHdr.mNext = pNextMbuf;

                /* Update pu1Data (linear Buffer), 
                 * PktSize, pCurMbuf
                 */
                pCurMbuf = pNextMbuf;
            }

            /*
             * Update u4PktSize and Move pointer by u4ThisPktLen
             * for pu1Data for successive iterations
             */
            u4PktSize -= u4ThisPktLen;
            pu1Data += u4ThisPktLen;
        }                        /* more bytes remaining to copy */
    }

    /* Calculate the Total Length of the Chained
     * mBuffer*/
    pNextMbuf = pMbuf;
    pMbuf->mBlkPktHdr.len = 0;

    /* Traverse thru' mBuf till end to calculate the Total Pkt Len */
    while (pNextMbuf != NULL)
    {
        pMbuf->mBlkPktHdr.len += pNextMbuf->mBlkHdr.mLen;
        pNextMbuf = pNextMbuf->mBlkHdr.mNext;
    }

    pMbuf->mBlkHdr.mFlags |= M_PKTHDR;

    return pMbuf;
}

/******************************************************************************
 * Function           : CfaGetVxIfIndexFromIfName 
 * Input(s)           : pu1IfName
 * Output(s)          : None.
 * Returns            : IfIndex 
 * Action             : This function returns IfIndex from Name. 
 *                       ex. If the Interface name is bc10,it returns 10 as IFindex
 *                                                     "bc" as IfAlias
 ******************************************************************************/
VOID
CfaGetVxIfIndexAndAliasForIfName (UINT1 *pu1IfName, UINT4 *pu4Index,
                                  UINT1 *pu1IfAlias)
{
    UINT4               u4Count = 0;
    UINT1               au1IfAlias[10];

    while (ISDIGIT (pu1IfName[u4Count]) == 0)
    {
        au1IfAlias[u4Count] = pu1IfName[u4Count];
        u4Count++;
    }
    au1IfAlias[u4Count] = '\0';

    *pu4Index = ATOI (&pu1IfName[u4Count]);

    MEMCPY ((VOID *) pu1IfAlias, (VOID *) au1IfAlias, STRLEN (au1IfAlias) + 1);

    return;
}

#endif /* OS_VXWORKS */

#if defined (OS_QNX)

/******************************************************************************
 * Function           : CfaGddOobGetOsHwAddr
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : au1HwAddr - Hw addr of the interface.
 * Returns            : CFA_SUCCESS/CFA_FAILURE.
 * Action             : This function gets the hardware address of the
 *                      Out Of Band Port (i.e. Linux ethernet port).
 *******************************************************************************/
INT4
CfaGddOobGetOsHwAddr (UINT4 u4IfIndex, UINT1 *pu1HwAddr)
{
    UNUSED_PARAM (pu1HwAddr);
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobOpen
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE.
 * Action             : This function opens the Out of band port.
 ******************************************************************************/
INT4
CfaGddOobOpen (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);

    /* In Linux IP, OOB port would have been created
     * during bootup. So the interface is not created here. */

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobConfigPort
 * Input(s)           : u4IfIndex - Interface index.
 *                      u1ConfigOption -
 *                      pConfigParam -
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE.
 * Action             : This function is used for configuration of linux
 *                      ethernet port.
 ******************************************************************************/
INT4
CfaGddOobConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1ConfigOption);

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobClose 
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function deleted the Out Of Band Port 
 ******************************************************************************/
INT4
CfaGddOobClose (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);

    /* In LinuxIp, OOB port is a physical port and cannot be deleted. */

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobRead 
 * Input(s)           : u4IfIndex - Interface index.
 *                      pu1DataBuf - Buf read
 *                      pu4PktSize - Size of the read pkt.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function reads the data from the linux 
 *                      ethernet port.
 ******************************************************************************/
INT4
CfaGddOobRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4PktSize);

    /* LinuxIP takes handles packet processing for OOB port */

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobWrite 
 * Input(s)           : u4IfIndex - Interface index.
 *                      pu1DataBuf - Buf to be sent out.
 *                      u4PktSize - size of the pkt to be sent out
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function writes the data to ethernet driver. 
 ******************************************************************************/
INT4
CfaGddOobWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PktSize);

    /* LinuxIp handles packet transmission for OOB port */

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddProcessOobDataRcvdEvent 
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : au1HwAddr - Hw addr of the interface.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function Reads the Buffer from the data received on OOB
 *                      Interface Queue and deliver it to Higher Layer.
 ******************************************************************************/
INT4
CfaGddProcessOobDataRcvdEvent (VOID)
{
    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaOobDataRcvd 
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function sends Event to CFA Task upon Packet 
 *                      arrival on OOB Interface 
 ******************************************************************************/
VOID
CfaOobDataRcvd (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);
}

#endif /*OS_QNX */

#if defined(CLI_LNXIP_WANTED) || defined(SNMP_LNXIP_WANTED)
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : CfaIpUpdateInterfaceParams                       */
/*                                                                           */
/*    Description         : Updates an IP interface parameters in            */
/*                          Linux kernel.                                    */
/*                                                                           */
/*    Input(s)            : u4IfIndex   -  IfIndex of OOB Device             */
/*                          pIpPortParams     - A structure containing       */
/*                          Ip address, Subnet mask, Broadcast addr.         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : CFA_SUCCESS / CFA_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
CfaIpUpdateInterfaceParams (UINT1 *pu1IntfName, UINT4 u4Flag,
                            tIpConfigInfo * pIpPortParams)
{
    /* It is assumed that all the ioctl calls for setting the IP address,
     * Netmask, Broadcast address and MTU should succeed. The input parameters
     * to this function should have been validated at the calling place.
     * Hence cleanup on failure of a set has not been done .*/

    struct ifreq        if_req;
    struct sockaddr_in *sinptr;
    INT4                i4socketid = -1;
    INT4                i4Err = -1;

    CFA_DBG2 (CFA_TRC_ALL, CFA_GDD,
              "Entering CfaIpUpdateInterfaceParams for interface %s and u4Flag 0x%x.\n",
              pu1IntfName, u4Flag);
    /* Setting the IP address */

    i4socketid = socket (PF_PACKET, SOCK_RAW, OSIX_HTONS (ETH_P_ALL));

    if (i4socketid < 0)
    {
        perror ("socket fail");
        return CFA_FAILURE;
    }

    if (pIpPortParams != NULL)
    {
        if (u4Flag & CFA_IP_IF_PRIMARY_ADDR)
        {
            MEMSET (&if_req, 0, sizeof (if_req));
            STRCPY (if_req.ifr_ifrn.ifrn_name, pu1IntfName);

            if_req.ifr_ifru.ifru_addr.sa_family = AF_INET;

            sinptr = (struct sockaddr_in *) (VOID *) &if_req.ifr_addr;
            sinptr->sin_family = AF_INET;
            sinptr->sin_addr.s_addr = OSIX_HTONL (pIpPortParams->u4Addr);
            sinptr->sin_port = 0;
            i4Err = ioctl (i4socketid, SIOCSIFADDR, &if_req);

            if (i4Err < 0)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaIpUpdateInterfaceParams - "
                     "unable to set primary address- FAILURE.\n");
                close (i4socketid);
                i4socketid = -1;
                return CFA_FAILURE;
            }
        }


        /* Setting the netmask */

        if (u4Flag & CFA_IP_IF_NETMASK)
        {
            MEMSET (&if_req, 0, sizeof (if_req));

            STRCPY (if_req.ifr_name, pu1IntfName);

            sinptr = (struct sockaddr_in *) (VOID *) &if_req.ifr_netmask;
            sinptr->sin_family = AF_INET;
            sinptr->sin_addr.s_addr = OSIX_HTONL (pIpPortParams->u4NetMask);
            sinptr->sin_port = 0;
            i4Err = ioctl (i4socketid, SIOCSIFNETMASK, &if_req);

            if (i4Err < 0)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaIpUpdateInterfaceParams - "
                     "unable to set net mask- FAILURE.\n");
                close (i4socketid);
                i4socketid = -1;
                return CFA_FAILURE;
            }
        }

        /* Setting the broadcast address */
        if (u4Flag & CFA_IP_IF_BCASTADDR)
        {
            MEMSET (&if_req, 0, sizeof (if_req));

            STRCPY (if_req.ifr_name, pu1IntfName);

            sinptr = (struct sockaddr_in *) (VOID *) &if_req.ifr_broadaddr;
            sinptr->sin_family = AF_INET;
            sinptr->sin_addr.s_addr = OSIX_HTONL (pIpPortParams->u4BcastAddr);
            sinptr->sin_port = 0;
            i4Err = ioctl (i4socketid, SIOCSIFBRDADDR, &if_req);

            if (i4Err < 0)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaIpUpdateInterfaceParams - "
                     "unable to set bcast addr- FAILURE.\n");
                close (i4socketid);
                i4socketid = -1;
                return CFA_FAILURE;
            }
        }
    }

    /* Setting the interface UP */

    if (u4Flag & CFA_IP_IF_FORWARD)
    {
        MEMSET (&if_req, 0, sizeof (if_req));
        STRCPY (if_req.ifr_name, pu1IntfName);

        if ((i4Err = ioctl (i4socketid, SIOCGIFFLAGS, &if_req)) < 0)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaIpUpdateInterfaceParams - "
                 "unable to get interface flags- FAILURE.\n");
            close (i4socketid);
            i4socketid = -1;
            return CFA_FAILURE;
        }

        if_req.ifr_flags = if_req.ifr_flags | IFF_UP | IFF_RUNNING;
        i4Err = ioctl (i4socketid, SIOCSIFFLAGS, &if_req);
        if (i4Err < 0)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaIpUpdateInterfaceParams - "
                 "unable to set interface UP- FAILURE.\n");
            close (i4socketid);
            i4socketid = -1;
            return CFA_FAILURE;
        }
    }

    /* Setting the interface DOWN */
    if (u4Flag & CFA_IP_IF_NOFORWARD)
    {
        MEMSET (&if_req, 0, sizeof (if_req));
        STRCPY (if_req.ifr_name, pu1IntfName);

        if ((i4Err = ioctl (i4socketid, SIOCGIFFLAGS, &if_req)) < 0)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaIpUpdateInterfaceParams - "
                 "unable to get interface flags- FAILURE.\n");
            close (i4socketid);
            i4socketid = -1;
            return CFA_FAILURE;
        }

        if_req.ifr_flags = (if_req.ifr_flags & ~(IFF_RUNNING));
        if_req.ifr_flags = (if_req.ifr_flags & ~(IFF_UP));

        i4Err = ioctl (i4socketid, SIOCSIFFLAGS, &if_req);
        if (i4Err < 0)
        {
            /* perror is not a failure here, since making an ethernet
             * interface down without inet address will throw error
             * which is a false positive. */
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaIpUpdateInterfaceParams - "
                 "unable to set interface DOWN- FAILURE.\n");
            close (i4socketid);
            i4socketid = -1;
            return CFA_FAILURE;
        }
    }

    close (i4socketid);
    i4socketid = -1;

    return CFA_SUCCESS;
}
#endif

#endif /*IP_WANTED || BSDCOMP_SLI_WANTED */

#endif /* LNXIP4_WANTED */

#ifndef IP_WANTED
#ifndef LNXIP4_WANTED
#ifndef BSDCOMP_SLI_WANTED
/******************************************************************************
 * Function           : CfaGddOobGetOsHwAddr 
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : au1HwAddr - Hw addr of the interface.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function gets the hardware address of the 
 *                      Out Of Band Port (i.e. Linux ethernet port). 
 ******************************************************************************/
INT4
CfaGddOobGetOsHwAddr (UINT4 u4IfIndex, UINT1 *pu1HwAddr)
{
    UNUSED_PARAM (pu1HwAddr);
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobOpen 
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function opens the Out of band port.
 ******************************************************************************/
INT4
CfaGddOobOpen (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);

    /* In Linux IP, OOB port would have been created 
     * during bootup. So the interface is not created here. */

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobConfigPort
 * Input(s)           : u4IfIndex - Interface index.
 *                      u1ConfigOption -
 *                      pConfigParam -
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE.
 * Action             : This function is used for configuration of linux
 *                      ethernet port.
 ******************************************************************************/
INT4
CfaGddOobConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1ConfigOption);

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobClose 
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function deleted the Out Of Band Port 
 ******************************************************************************/
INT4
CfaGddOobClose (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);

    /* In LinuxIp, OOB port is a physical port and cannot be deleted. */

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobRead 
 * Input(s)           : u4IfIndex - Interface index.
 *                      pu1DataBuf - Buf read
 *                      pu4PktSize - Size of the read pkt.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function reads the data from the linux 
 *                      ethernet port.
 ******************************************************************************/
INT4
CfaGddOobRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4PktSize);

    /* LinuxIP takes handles packet processing for OOB port */

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddOobWrite 
 * Input(s)           : u4IfIndex - Interface index.
 *                      pu1DataBuf - Buf to be sent out.
 *                      u4PktSize - size of the pkt to be sent out
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function writes the data to ethernet driver. 
 ******************************************************************************/
INT4
CfaGddOobWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PktSize);

    /* LinuxIp handles packet transmission for OOB port */

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGddProcessOobDataRcvdEvent 
 * Input(s)           : u4IfIndex - Interface index.
 * Output(s)          : au1HwAddr - Hw addr of the interface.
 * Returns            : CFA_SUCCESS/CFA_FAILURE. 
 * Action             : This function Reads the Buffer from the data received on OOB
 *                      Interface Queue and deliver it to Higher Layer.
 ******************************************************************************/
INT4
CfaGddProcessOobDataRcvdEvent (VOID)
{
    return CFA_SUCCESS;
}

#endif
#endif
#endif

#if defined (IP6_WANTED) && (defined (CLI_LNXIP_WANTED) || defined (SNMP_LNXIP_WANTED)) 
/******************************************************************************
 * FUNCTION    : CfaLinuxIpUpdateIpv6Addr
 * DESCRIPTION : Routine to set an IPv6 address on an interface in Linux IP
 * INPUTS      : u4IfIndex   -interface index for which address
 *                            has to be added/deleted
 *               pIp6Addr   - IPv6 address to add/delete
 *               i4PrefixLen - Prefix Len to set
 *               i4Command  - OSIX_TRUE/OSIX_FALSE
 * OUTPUTS     : None
 * RETURNS     : CFA_SUCCESS
 *               CFA_FAILURE
 ******************************************************************************/
INT4
CfaLinuxIpUpdateIpv6Addr (UINT4 u4IfIndex, tIp6Addr * pIp6Addr,
                          INT4 i4PrefixLen, INT4 i4Command)
{
    INT4                i4RetVal = 0;
    struct in6_ifreq    ifr6;
    INT4                i4SockFd = -1;

    CFA_DBG2 (CFA_TRC_ALL, CFA_GDD,
              "Entering CfaLinuxIpUpdateIpv6Addr for interface %d and i4Command %d.\n",
              u4IfIndex, i4Command);

#ifdef RM_WANTED
    if ((RmGetNodeState() != RM_ACTIVE) &&
        (i4Command == OSIX_TRUE))
    {
       CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
            "Error in CfaLinuxIpUpdateIpv6Addr - "
            "linux programming of ipv6 addr not allowed in standby.\n");
        return CFA_SUCCESS;
    }
#endif

    if ((pIp6Addr == NULL) ||
        (i4PrefixLen == 0))
    {
       CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
            "Error in CfaLinuxIpUpdateIpv6Addr - "
            "Received Invalid ipv6 addr or Prefix length.\n");
        return CFA_FAILURE;
    }

    MEMSET (&ifr6, 0, sizeof (ifr6));

    MEMCPY (&ifr6.ifr6_addr, pIp6Addr, 16);
    ifr6.ifr6_ifindex = CfaGetIfIpPort ((UINT2) u4IfIndex);

    i4SockFd =  socket (AF_INET6, SOCK_DGRAM, IPPROTO_IP);
    if (i4SockFd == -1)
    {
       CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
            "Error in CfaLinuxIpUpdateIpv6Addr - "
            "socket creation FAILURE.\n");
        return CFA_FAILURE;
    }

    ifr6.ifr6_prefixlen = i4PrefixLen;
    if (i4Command == OSIX_TRUE)
    {
	    i4RetVal = ioctl (i4SockFd, SIOCDIFADDR, &ifr6);
	    i4RetVal = ioctl (i4SockFd, SIOCSIFADDR, &ifr6);
    }
    else
    {
	    i4RetVal = ioctl (i4SockFd, SIOCDIFADDR, &ifr6);
    }

    if (i4RetVal < 0)
    {
        close (i4SockFd);
        i4SockFd = -1;
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
             "Error in CfaLinuxIpUpdateIpv6Addr - "
             "ioctl programming for ipv6 addr of OOB index %d - FAILURE.\n", u4IfIndex);
        return CFA_FAILURE;
    }

    close (i4SockFd);
    i4SockFd = -1;
    CFA_DBG2 (CFA_TRC_ALL, CFA_GDD,
              "Exiting CfaLinuxIpUpdateIpv6Addr for interface %d and i4Command %d.- Success\n",
              u4IfIndex, i4Command);
    return CFA_SUCCESS;
}
#endif /* IP6_WANTED || CLI_LNXIP_WANTED */
