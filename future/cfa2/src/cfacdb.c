/*$Id: cfacdb.c,v 1.102 2016/12/28 12:14:15 siva Exp $*/
#define _CFACDB_C_

#include "cfainc.h"

tOsixSemId          gCfaCdbSemId;
/*****************************************************************************/
/* Function Name      : CfaCdbInit                                           */
/*                                                                           */
/* Description        : This function allocates memory and initialises the   */
/*                      common database                                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : Cfa Common Database                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCdbInit (VOID)
{
    UINT4               u4Index;

    if (OsixCreateSem (CFA_CDB_SEMAPHORE, 1, 0, &gCfaCdbSemId) != OSIX_SUCCESS)
    {
        return CFA_FAILURE;
    }

    CfaIfUtlCreateCdbMemPool ();

    CfaIfUtlCreateMauMemPool ();

    /* Initialize the IfType's list to NULL */

    CfaIfUtlIfTypeDbInit (CFA_CDB_IFTYPE_REQ);

    CfaIfUtlCreateIfDb (CFA_CDB_IFDB_REQ);

    /* Initialize the Iftype to IfIndex map */

    /* Flexible IfIndex changes: ===
     * to maintain Ethernet type to IfIndex mapping.
     * This is needed since in MI solution, during
     * initialization itself the ethernet type for
     * physical interfaces is set. Once array is changed to
     * dynamic data structute since the node is not available,
     * (in MI the interfaces are not created by default) we
     * need to maintain a separate mapping to maintain
     * the current initialization behavior */

    MEMSET (gau1EthTypeIndexMap, 0, ISS_MAX_PORTS);

    CFA_DS_LOCK ();

    for (u4Index = 1; u4Index <= MAX_CFA_DEF_INTF_IN_SYS; u4Index++)
    {
        CFA_CDB_IF_DB_SET_INTF_ACTIVE (u4Index, CFA_FALSE);
        CFA_CDB_IF_DB_SET_IFINDEX (u4Index);
    }

    MEMSET (gIntfVlanBitList, 0, VLAN_LIST_SIZE);

    if (CfaIvrMapInit () == CFA_FAILURE)
    {
        CFA_DS_UNLOCK ();

        return CFA_FAILURE;
    }

    CFA_DS_UNLOCK ();

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaCdbDeInit                                         */
/*                                                                           */
/* Description        : This function releases the memory allocated for the  */
/*                      common database                                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : Cfa Common Database                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCdbDeInit (VOID)
{
    if (gCfaCdbSemId != 0)
    {
        OsixSemDel (gCfaCdbSemId);
    }
    if (CfaIvrMapDeInit () == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }

    CfaIfUtlDestroyIfDb (CFA_CDB_IFDB_REQ);

    /* Initialize the IfType's list */

    CfaIfUtlIfTypeDbInit (CFA_CDB_IFTYPE_REQ);

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaCdbRemoveFromIntfVlanList                         */
/*                                                                           */
/* Description        : This function resets the given Vlan in the interface */
/*                      VLAN bitlist.                                        */
/*                                                                           */
/* Input(s)           : u2IfIvrVlanId: Interface VLAN Id                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCdbRemoveFromIntfVlanList (UINT2 u2IfIvrVlanId)
{
    CFA_DS_LOCK ();
    OSIX_BITLIST_RESET_BIT (gIntfVlanBitList, u2IfIvrVlanId, VLAN_LIST_SIZE);
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaCdbAddToIntfVlanList                              */
/*                                                                           */
/* Description        : This function sets the given Vlan in the interface   */
/*                      VLAN bitlist.                                        */
/*                                                                           */
/* Input(s)           : u2IfIvrVlanId: Interface VLAN Id                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCdbAddToIntfVlanList (UINT2 u2IfIvrVlanId)
{
    CFA_DS_LOCK ();
    OSIX_BITLIST_SET_BIT (gIntfVlanBitList, u2IfIvrVlanId, VLAN_LIST_SIZE);
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfValidStatus                                  */
/*                                                                           */
/* Description        : This function sets the status of the interface       */
/*                                                                           */
/* Input(s)           : u4IfIndex: Interface Index                           */
/*                      i4Status :Status to be set                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetIfValidStatus (UINT4 u4IfIndex, INT4 i4Status)
{
    CFA_DS_LOCK ();
    CFA_CDB_SET_INTF_VALID (u4IfIndex, i4Status);
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************
*
*    Function Name        : CfaValidateCfaIfIndex
*
*    Description        : This function Checks whether the interface exists
*    with this Interface index.
*
*    Input(s)            :u4IfIndex:Interface Index 
*
*    Output(s)            : None.
*
*    Global Variables Referred : gapIfTable
*
*    Exceptions or Operating
*    System Error Handling    : None.
*
*    Use of Recursion        : None.
*
*    Returns           : CFA_SUCCESS if interface exists else CFA_FAILURE 
*
*    NOTE              : This function is NOT an exported API and hence
*                        MUST NOT be used by other modules.
*****************************************************************************/

INT4
CfaValidateCfaIfIndex (UINT4 u4IfIndex)
{
    INT4                i4Status = CFA_FAILURE;

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        i4Status = CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();

    return i4Status;
}

/*****************************************************************************
 *
 *    Function Name       : CfaSetIfSpeed
 *
 *    Description         : This function sets the interface speed.   
 *
 *    Input(s)            : u4IfIndex - interface index of the interface whose
 *                                      speed needs to be set.
 *                          u4Speed   - Speed to be set.             
 *
 *    Output(s)           : None.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS.  
 *****************************************************************************/
INT4
CfaSetIfSpeed (UINT4 u4IfIndex, UINT4 u4Speed)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    CFA_CDB_IF_SPEED (u4IfIndex) = u4Speed;
    CFA_DS_UNLOCK ();
    CfaCopyPhyPortPropToLogicalInterface (u4IfIndex);
#ifdef L2RED_WANTED
    CfaSyncPortInformation (u4IfIndex, CFA_SYNC_PORT_SPEED, u4Speed);
#endif

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetIfSpeed
 *
 *    Description         : This function returns the interface speed.   
 *
 *    Input(s)            : u4IfIndex - interface index of the interface whose
 *                                      speed needs to be set.
 *
 *    Output(s)           : pu4IfSpeed - Pointer to Speed .             
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS/CFA_FAILURE.  
 *****************************************************************************/
INT4
CfaGetIfSpeed (UINT4 u4IfIndex, UINT4 *pu4IfSpeed)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu4IfSpeed = CFA_CDB_IF_SPEED (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaSetIfHighSpeed
 *
 *    Description         : This function sets the interface highspeed.   
 *
 *    Input(s)            : u4IfIndex - interface index of the interface whose
 *                                      speed needs to be set.
 *                          u4HighSpeed   - Speed to be set.             
 *
 *    Output(s)           : None.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS.  
 *****************************************************************************/
INT4
CfaSetIfHighSpeed (UINT4 u4IfIndex, UINT4 u4HighSpeed)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;

    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    CFA_CDB_IF_HIGHSPEED (u4IfIndex) = u4HighSpeed;
    CFA_DS_UNLOCK ();
    CfaCopyPhyPortPropToLogicalInterface (u4IfIndex);
#ifdef L2RED_WANTED
    CfaSyncPortInformation (u4IfIndex, CFA_SYNC_PORT_HIGH_SPEED, u4HighSpeed);
#endif
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetIfHighSpeed
 *
 *    Description         : This function returns the interface speed.   
 *
 *    Input(s)            : u4IfIndex - interface index of the interface whose
 *                                      speed needs to be set.
 *
 *    Output(s)           : pu4IfHighSpeed - Pointer to HighSpeed .             
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS/CFA_FAILURE.  
 *****************************************************************************/
INT4
CfaGetIfHighSpeed (UINT4 u4IfIndex, UINT4 *pu4IfHighSpeed)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu4IfHighSpeed = CFA_CDB_IF_HIGHSPEED (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfOperStatus                                   */
/*                                                                           */
/* Description        : This function sets the oper-status of the interface  */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      u1OperStatus:Oper Status to be set                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfOperStatus (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tVlanEvbSbpArray SbpArray;
    tCfaIfInfo       *pCfaIfInfo = NULL;
    UINT1            u1BrgPortType = 0;
    UINT1            u1LoopIndex   = 0;
    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    CFA_CDB_IF_OPER (u4IfIndex) = u1OperStatus;
    CFA_DS_UNLOCK ();

    /* When changing the UAP port up to down . update for all SCh 
     * will happen in CfaSetIfOperStatus . 
     * since CfaVlanApiEvbGetSbpPortsOnUap gives the CONFIRMED state entries.
     */
    CfaGetIfBrgPortType (u4IfIndex, &u1BrgPortType);
    if ((u1BrgPortType == CFA_UPLINK_ACCESS_PORT) &&
            (MIB_RESTORE_IN_PROGRESS != gi4MibResStatus))
    {
        MEMSET (&SbpArray, 0, sizeof (tVlanEvbSbpArray));
        CfaVlanApiEvbGetSbpPortsOnUap (u4IfIndex,  &SbpArray);
        while (u1LoopIndex < VLAN_EVB_MAX_SBP_PER_UAP)
        {
            if (SbpArray.au4SbpArray[u1LoopIndex] == 0)
            {
                /* No more SBPs are present on this interface */
                break;
            }
            CFA_DS_LOCK ();
            pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry 
                (SbpArray.au4SbpArray[u1LoopIndex]);

            if (NULL != pCfaIfInfo)
            {
                CFA_CDB_IF_OPER (SbpArray.au4SbpArray[u1LoopIndex]) = 
                    u1OperStatus;
            }
            CFA_DS_UNLOCK ();
            u1LoopIndex++;
        }
    }
    CfaCopyPhyPortPropToLogicalInterface (u4IfIndex);
#ifdef L2RED_WANTED
    CfaSyncPortInformation (u4IfIndex, CFA_SYNC_PORT_OPER_STATUS, u1OperStatus);
#endif
#if defined(IP_WANTED) && (defined(CLI_LNXIP_WANTED) || defined(SNMP_LNXIP_WANTED))
    if(CfaIsMgmtPort (u4IfIndex) == TRUE)
    {
        CfaGddOobConfigPort (u4IfIndex, u1OperStatus);
    }
#endif
#ifdef LNXIP4_WANTED
    CfaGddConfigPort (u4IfIndex, u1OperStatus, NULL);
#endif
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfType                                         */
/*                                                                           */
/* Description        : This function gets the interface type                */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu1IfType:Pointer to the Interface type.             */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfType (UINT4 u4IfIndex, UINT1 *pu1IfType)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu1IfType = CFA_CDB_IF_TYPE (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfSubType                                      */
/*                                                                           */
/* Description        : This function gets the interface Sub type            */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu1IfType:Pointer to the Interface type.             */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfSubType (UINT4 u4IfIndex, UINT1 *pu1IfType)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu1IfType = CFA_CDB_IF_SUB_TYPE (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfType                                         */
/*                                                                           */
/* Description        : This function sets the interface type                */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                      u1IfType :Interface type to be set.                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfType (UINT4 u4IfIndex, UINT1 u1IfType)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    CFA_DS_UNLOCK ();

    /* Update the IfType database table based on the current interface type
     * and then set the ifType */
    CfaIfUtlIfTypeDbUpdate (u4IfIndex, u1IfType, CFA_CDB_IFTYPE_REQ);

    CFA_DS_LOCK ();
    CFA_CDB_IF_TYPE (u4IfIndex) = u1IfType;
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfSubType                                      */
/*                                                                           */
/* Description        : This function sets the interface Subtype             */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                      u1IfSubType :Interface type to be set.               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfSubType (UINT4 u4IfIndex, UINT1 u1IfSubType)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_IF_SUB_TYPE (u4IfIndex) = u1IfSubType;
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetEthernetType                                   */
/*                                                                           */
/* Description        : This function gets the ethernet type                 */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu1EthernetType:Pointer to the ethernet type.        */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetEthernetType (UINT4 u4IfIndex, UINT1 *pu1EthernetType)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    /* Check if the node is present, if not then return ethernet type
     * from array. This flow will hit from CfaGetIfIndex and
     * CfaGetSlotLocalPortNum */

    pCfaIfInfo = (tCfaIfInfo *) CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL != pCfaIfInfo)
    {
        *pu1EthernetType = CFA_CDB_ETHERNET_TYPE (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    else
    {
        if (u4IfIndex > 0 && u4IfIndex <= ISS_MAX_PORTS)
        {
            /* Flexible IfIndex changes: ===
             * to maintain Ethernet type to IfIndex mapping.
             * This is needed since in MI solution, during
             * initialization itself the ethernet type for
             * physical interfaces is set. Once array is changed to
             * dynamic data structute since the node is not available,
             * (in MI the interfaces are not created by default) we
             * need to maintain a separate mapping to maintain
             * the current initialization behavior */

            *pu1EthernetType = gau1EthTypeIndexMap[u4IfIndex - 1];
            CFA_DS_UNLOCK ();
            return CFA_SUCCESS;
        }
        else
        {
            CFA_DS_UNLOCK ();
            return CFA_FAILURE;
        }
    }
}

/*****************************************************************************/
/* Function Name      : CfaSetEthernetType                                   */
/*                                                                           */
/* Description        : This function sets the ethernet type                 */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                      u1EthernetType :Ethernet type to be set.             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetEthernetType (UINT4 u4IfIndex, UINT1 u1EthernetType)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;

    /* Flexible IfIndex changes: ===
     * to maintain Ethernet type to IfIndex mapping.
     * This is needed since in MI solution, during
     * initialization itself the ethernet type for
     * physical interfaces is set. Once array is changed to
     * dynamic data structute since the node is not available,
     * (in MI the interfaces are not created by default) we
     * need to maintain a separate mapping to maintain
     * the current initialization behavior */

    if (u4IfIndex > 0 && u4IfIndex <= ISS_MAX_PORTS)
    {
        gau1EthTypeIndexMap[u4IfIndex - 1] = u1EthernetType;
    }

    CFA_DS_LOCK ();

    /* Check if the node is present and then only update */

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL != pCfaIfInfo)
    {
        CFA_CDB_ETHERNET_TYPE (u4IfIndex) = u1EthernetType;
        CFA_DS_UNLOCK ();
#ifdef L2RED_WANTED
        CfaSyncPortInformation (u4IfIndex, CFA_SYNC_PORT_ETHER_TYPE,
                                u1EthernetType);
#endif
        return CFA_SUCCESS;
    }
    CFA_DS_UNLOCK ();

    return CFA_FAILURE;

}

/*****************************************************************************/
/* Function Name      : CfaGetIfHwAddr                                       */
/*                                                                           */
/* Description        : This function gets the MacAddress of the interface   */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu1HwAddr:Pointer to Mac Address                     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfHwAddr (UINT4 u4IfIndex, UINT1 *pu1HwAddr)
{
    INT4                i4IfValidStatus = CFA_FALSE;
    UINT1               u1DefRouterIdx = CFA_FALSE;
    UINT1               u1IfType = 0;

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        u1IfType = CFA_CDB_IF_TYPE (u4IfIndex);
        CFA_CDB_CHECK_IF_TYPE (u1IfType, u1DefRouterIdx);
        if (CFA_FALSE == u1DefRouterIdx)
        {
            CFA_CDB_GET_ENET_IWF_LOCAL_MAC (u4IfIndex, pu1HwAddr);
        }
        else
        {
            i4IfValidStatus =
                CFA_CDB_IS_INTF_VALID (CFA_DEFAULT_ROUTER_IFINDEX);
            if (i4IfValidStatus == CFA_TRUE)
            {
                CFA_CDB_GET_ENET_IWF_LOCAL_MAC (CFA_DEFAULT_ROUTER_IFINDEX,
                                                pu1HwAddr);
            }
        }
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfIpPort                                       */
/*                                                                           */
/* Description        : This function gets the IP port of the interface      */
/*                                                                           */
/* Input(s)           : u4IfIndex:  Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Ip port number                                       */
/*****************************************************************************/
INT4
CfaGetIfIpPort (UINT4 u4IfIndex)
{
    INT4                i4IpPort = CFA_INVALID_INDEX;

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return i4IpPort;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        i4IpPort = CFA_CDB_IF_IP_IPPORT (u4IfIndex);
    }
    CFA_DS_UNLOCK ();

    return i4IpPort;
}

#ifdef LNXIP4_WANTED
UINT4
CfaGetIdxMgrPort (UINT4 u4IfIndex)
{
        UINT4            u4IdxMgrPort = 0;
        if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
        {
                return u4IdxMgrPort;
        }
        CFA_DS_LOCK ();
        if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
        {
                u4IdxMgrPort = CFA_CDB_IDX_MGR_PORT (u4IfIndex);
        }
        CFA_DS_UNLOCK ();
        return u4IdxMgrPort;
}
#endif
/*****************************************************************************/
/* Function Name      : CfaGetIfInterface                                    */
/*                                                                           */
/* Description        : This function gets the CFA interface for the IP Port */
/*                                                                           */
/* Input(s)           : u4Port:  IP Port                                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA IF Index                                         */
/*****************************************************************************/

UINT4 CfaGetIfInterface (UINT4 u4Port)
{
     UINT4 u4LocPort = 0;
     UINT4 u4CfaIfIndex = CFA_INVALID_INDEX;
     for(u4LocPort=1; u4LocPort <= CFA_MAX_IVR_IF_INDEX; u4LocPort++)
     {
       CFA_DS_LOCK ();
       if(CFA_CDB_IF_IP_IPPORT(u4LocPort) == (INT4)u4Port)
       {
               CFA_DS_UNLOCK ();
               u4CfaIfIndex = u4LocPort;
               return u4CfaIfIndex;
       }
       CFA_DS_UNLOCK ();
     }
     return u4CfaIfIndex;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfHwAddr                                       */
/*                                                                           */
/* Description        : This function sets the MacAddress of the interface   */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      pu1HwAddr:Pointer to Mac Address                     */
/*                      u2Length :Length of the Mac address                  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfHwAddr (UINT4 u4IfIndex, UINT1 *pu1HwAddr, UINT2 u2Length)
{
    tCfaIfInfo         *pCfaInfo = NULL;
    UINT1               u1DefRouterIdx = CFA_FALSE;
    UINT1               u1IfType = 0;
    INT4                i4IfValid = CFA_FALSE;

    CFA_DS_LOCK ();

    pCfaInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    u1IfType = CFA_CDB_IF_TYPE (u4IfIndex);
    CFA_CDB_CHECK_IF_TYPE (u1IfType, u1DefRouterIdx);
    if (CFA_FALSE == u1DefRouterIdx)
    {
        CFA_CDB_SET_ENET_IWF_LOCAL_MAC (u4IfIndex, pu1HwAddr,
                                        (MEM_MAX_BYTES
                                         (u2Length, CFA_ENET_ADDR_LEN)));
    }
    else
    {
        i4IfValid = CFA_CDB_IS_INTF_VALID (CFA_DEFAULT_ROUTER_IFINDEX);
        if (i4IfValid == CFA_TRUE)
        {
            CFA_CDB_SET_ENET_IWF_LOCAL_MAC (CFA_DEFAULT_ROUTER_IFINDEX,
                                            pu1HwAddr,
                                            (MEM_MAX_BYTES
                                             (u2Length, CFA_ENET_ADDR_LEN)));
        }
    }
    CFA_DS_UNLOCK ();
    CfaCopyPhyPortPropToLogicalInterface (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfName                                         */
/*                                                                           */
/* Description        : This function gets the interface name from the given */
/*                      interface index.                                     */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : au1IfName:Pointer to the interface name.             */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfName (UINT4 u4IfIndex, UINT1 *au1IfName)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        STRNCPY (au1IfName, CFA_CDB_IF_ALIAS (u4IfIndex), STRLEN(CFA_CDB_IF_ALIAS (u4IfIndex)));
	au1IfName[STRLEN(CFA_CDB_IF_ALIAS (u4IfIndex))] = '\0';
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfName                                         */
/*                                                                           */
/* Description        : This function sets the interface name from the given */
/*                      interface index.                                     */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      au1IfName:Pointer to the interface name.             */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaSetIfName (UINT4 u4IfIndex, UINT1 *au1IfName)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    STRNCPY (CFA_CDB_IF_ALIAS (u4IfIndex),
             au1IfName, CFA_MAX_PORT_NAME_LENGTH - 1);
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfAlias                                        */
/*                                                                           */
/* Description        : This function gets the interface alias name from the */
/*                      given interface index.                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : au1IfAlias:Pointer to the interface alias name.      */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfAlias (UINT4 u4IfIndex, UINT1 *au1IfAlias)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        STRNCPY (au1IfAlias, CFA_CDB_IF_ALIAS_NAME (u4IfIndex), STRLEN(CFA_CDB_IF_ALIAS_NAME (u4IfIndex)));
	au1IfAlias[STRLEN(CFA_CDB_IF_ALIAS_NAME (u4IfIndex))] = '\0';
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfAlias                                        */
/*                                                                           */
/* Description        : This function sets the interface alias name from the */
/*                      given interface index.                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      au1IfAlias:Pointer to the interface alias name.      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaSetIfAlias (UINT4 u4IfIndex, UINT1 *au1IfAlias)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    STRNCPY (CFA_CDB_IF_ALIAS_NAME (u4IfIndex),
             au1IfAlias, CFA_MAX_IFALIAS_LENGTH - 1);
    CFA_DS_UNLOCK ();
    /* notify change to LLDP */
    if ((CfaIsSispInterface (u4IfIndex) == CFA_FALSE) && 
            (CFA_CDB_IS_INTF_ACTIVE (u4IfIndex) == CFA_TRUE))
    {
        LldpApiNotifyIfAlias (u4IfIndex, au1IfAlias);
    }

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetPortName                                       */
/*                                                                           */
/* Description        : This function gets the Port name (Fe/Gi/Hg/Xe)
*/
/*                      from the given Index.                                 */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu1IfName:Pointer to the interface name.             */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetPortName (UINT4 u4IfIndex, UINT1 *pu1IfName)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        STRNCPY (pu1IfName, CFA_CDB_PORT_NAME (u4IfIndex), STRLEN(CFA_CDB_PORT_NAME (u4IfIndex)));
	pu1IfName[STRLEN(CFA_CDB_PORT_NAME (u4IfIndex))] = '\0';
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaSetPortName                                       */
/*                                                                           */
/* Description        : This function sets the Port Name                     */
/*                      (ie Fe/Gi/Hg/Xe) for the given Index.                */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      pu1IfName:Pointer to the interface name.             */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/

/* Note : This function needs to be called whenever there is a change in 
 *        the port type, ie from Hg to Xe or vice versa, so that Cfa table
 *        gets updated */
INT4
CfaSetPortName (UINT4 u4IfIndex, UINT1 *pu1IfName)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    STRNCPY (CFA_CDB_PORT_NAME (u4IfIndex),
             pu1IfName, CFA_MAX_PORT_NAME_LENGTH - 1);
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfMtu                                          */
/*                                                                           */
/* Description        : This function sets MTU of the interface              */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      u4IfMtu : MTU of the interface                       */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfMtu (UINT4 u4IfIndex, UINT4 u4IfMtu)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_IF_MTU (u4IfIndex) = u4IfMtu;
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfIpv4AddressChange                            */
/*                                                                           */
/* Description        : This function sets u1Ipv4AddressChange flag of the   */
/*                      interface                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      u1Ipv4AddressChange Flag of the interface            */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfIpv4AddressChange (UINT4 u4IfIndex, UINT1 u1Ipv4AddressChange)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_IF_IP_CHANGE (u4IfIndex) = u1Ipv4AddressChange;
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : CfaSetIfIpPort                                       */
/*                                                                           */
/* Description        : This function sets IP Port Number for an interface   */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      i4IpPort: IP Port Number Value                       */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfIpPort (UINT4 u4IfIndex, INT4 i4IpPort)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_IF_IP_IPPORT (u4IfIndex) = i4IpPort;
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfMtu                                          */
/*                                                                           */
/* Description        : This function gets MTU of the interface              */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu4IfMtu : Pointer to MTU of the interface.          */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfMtu (UINT4 u4IfIndex, UINT4 *pu4IfMtu)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu4IfMtu = CFA_CDB_IF_MTU (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfIpv4AddressChange                            */
/*                                                                           */
/* Description        : This function gets Ipv4AddressChange Flag of the     */
/*                      interface                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu1Ipv4AddressChange : Pointer to u1Ipv4AddressChange*/
/*                      of the interface                                     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfIpv4AddressChange (UINT4 u4IfIndex, UINT1 *pu1Ipv4AddressChange)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu1Ipv4AddressChange = CFA_CDB_IF_IP_CHANGE (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}
/*****************************************************************************/
/* Function Name      : CfaSetIfBrgPortType                                  */
/*                                                                           */
/* Description        : This function sets bridge port type for the given    */
/*                      interface.                                           */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      u1BrgPortType - Bridge port type for the interface.  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfBrgPortType (UINT4 u4IfIndex, UINT1 u1BrgPortType)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;

    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_IF_BRIDGE_PORT_TYPE (u4IfIndex) = u1BrgPortType;
    CFA_DS_UNLOCK ();
    /* No need to explicitly invoke CfaCopyPhyPortPropToLogicalInterface
     * here. The nmh set routine for bridge port type explictly takes care
     * of sync between the physical and corresponding SISP routines
     * */
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfBrgPortType                                  */
/*                                                                           */
/* Description        : This function gets bridge port type for the given    */
/*                      interface.                                           */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu1BrgPortType - Pointer to bridge port type for the */
/*                                       given port type.                    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfBrgPortType (UINT4 u4IfIndex, UINT1 *pu1BrgPortType)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    UINT1               u1InterfaceType = 0;

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu1BrgPortType = CFA_CDB_IF_BRIDGE_PORT_TYPE (u4IfIndex);
        CFA_DS_UNLOCK ();
        if (*pu1BrgPortType == CFA_CNP_STAGGED_PORT)
        {
            if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                              &u2LocalPortId) == VCM_FAILURE)
            {
                return CFA_FAILURE;
            }
            if (L2IwfGetInterfaceType (u4ContextId, &u1InterfaceType) ==
                L2IWF_FAILURE)
            {
                return CFA_FAILURE;
            }
            if (u1InterfaceType == VLAN_C_INTERFACE_TYPE)
            {
                *pu1BrgPortType = CFA_CNP_CTAGGED_PORT;
            }
        }
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfIvrVlanId                                    */
/*                                                                           */
/* Description        : This function gets the VlanId for                    */
/*                      the given IVRinterface                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu2IfIvrVlanId:Pointer to the Ivr Interface Index    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfIvrVlanId (UINT4 u4IfIndex, UINT2 *pu2IfIvrVlanId)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu2IfIvrVlanId = CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfIvrVlanId                                    */
/*                                                                           */
/* Description        : This function sets the VlanId for                    */
/*                      the given IVRinterface                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      u2IfIvrVlanId:VlanId of  Ivr Interface               */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfIvrVlanId (UINT4 u4IfIndex, UINT2 u2IfIvrVlanId)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex) = u2IfIvrVlanId;
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfLoopbackId                                   */
/*                                                                           */
/* Description        : This function sets the LoopbackId for                */
/*                      the given Loopback interface                         */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      u2IfLoopbackId:Loopback Id of Interface               */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfLoopbackId (UINT4 u4IfIndex, UINT2 u2IfLoopbackId)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex) = u2IfLoopbackId;
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfDuplexStatus                                 */
/*                                                                           */
/* Description        : This function gets the Link Duplex status            */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pi4DuplexStatus:Pointer to the duplex status         */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfDuplexStatus (UINT4 u4IfIndex, INT4 *pi4DuplexStatus)
{
    if (CfaValidateCfaIfIndex (u4IfIndex) == CFA_SUCCESS)
    {
        CFA_DS_LOCK ();
        *pi4DuplexStatus = (INT4) CFA_CDB_IF_DUPLEXSTATUS (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfDuplexStatus                                 */
/*                                                                           */
/* Description        : This function sets the Link Duplex status            */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      i4DuplexStatus: duplex status  to be set             */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfDuplexStatus (UINT4 u4IfIndex, INT4 i4DuplexStatus)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_IF_DUPLEXSTATUS (u4IfIndex) = (UINT1) i4DuplexStatus;
    CFA_DS_UNLOCK ();
    CfaCopyPhyPortPropToLogicalInterface (u4IfIndex);
#ifdef L2RED_WANTED
    CfaSyncPortInformation (u4IfIndex, CFA_SYNC_PORT_DUPLEX_STATUS,
                            (UINT4) i4DuplexStatus);
#endif
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfAutoNegStatus                                */
/*                                                                           */
/* Description        : This function sets the Link Auto Negotiation status  */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      i4Status: AutoNeg status  to be set                   */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfAutoNegStatus (UINT4 u4IfIndex, INT4 i4Status)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;

    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_IF_AUTONEGSTATUS (u4IfIndex) = (UINT1) i4Status;
    CFA_DS_UNLOCK ();
#ifdef L2RED_WANTED
    CfaSyncPortInformation (u4IfIndex, CFA_SYNC_PORT_AUTO_NEG_STATUS,
                            (UINT4) i4Status);
#endif

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfAutoNegSupport                               */
/*                                                                           */
/* Description        : This function sets the Link Auto Negotiation support */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      i4Status: AutoNeg support to be set                  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfAutoNegSupport (UINT4 u4IfIndex, INT4 i4Status)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;

    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    CFA_CDB_IF_AUTONEGSUPPORT (u4IfIndex) = (UINT1) i4Status;
    CFA_DS_UNLOCK ();
#ifdef L2RED_WANTED
    CfaSyncPortInformation (u4IfIndex, CFA_SYNC_PORT_AUTO_NEG_SUPPORT,
                            (UINT4) i4Status);
#endif

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfAdvtCapability                               */
/*                                                                           */
/* Description        : This function sets the capabilities advertised by    */
/*                      the link                                             */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                     u2AdvtCapability - Advertised capability to be set    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfAdvtCapability (UINT4 u4IfIndex, UINT2 u2AdvtCapability)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;

    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_IF_ADVT_CAPABILITY (u4IfIndex) = u2AdvtCapability;
    CFA_DS_UNLOCK ();
#ifdef L2RED_WANTED
    CfaSyncPortInformation (u4IfIndex, CFA_SYNC_PORT_ADV_CAP, u2AdvtCapability);
#endif

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfOperMauType                                  */
/*                                                                           */
/* Description        : This function sets the operational MAU type of the   */
/*                      link                                                 */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      u2OperMauType: Oper Mau Type to be set               */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfOperMauType (UINT4 u4IfIndex, UINT2 u2OperMauType)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;

    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    CFA_CDB_IF_OPER_MAU_TYPE (u4IfIndex) = u2OperMauType;
    CFA_DS_UNLOCK ();
#ifdef L2RED_WANTED
    CfaSyncPortInformation (u4IfIndex, CFA_SYNC_PORT_OPER_MAU_TYPE,
                            u2OperMauType);
#endif

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfBridgedIfaceStatus                           */
/*                                                                           */
/* Description        : This function gets the bridged interface status      */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : pu1Status:Pointer to the  status.                    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfBridgedIfaceStatus (UINT4 u4IfIndex, UINT1 *pu1Status)
{
    *pu1Status = CFA_DISABLED;

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu1Status = (UINT1) CFA_CDB_IF_BRIDGED_IFACE_STATUS (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();

    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfActiveStatus                                 */
/*                                                                           */
/* Description        : This function sets the interface Active status       */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                      i4Status: status to be set                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfActiveStatus (UINT4 u4IfIndex, INT4 i4Status)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;

    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_SET_INTF_ACTIVE (u4IfIndex, i4Status);
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfInternalStatus                               */
/*                                                                           */
/* Description        : This function sets the interface internal status     */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                      i4Status: status to be set                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfInternalStatus (UINT4 u4IfIndex, INT4 i4Status)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;

    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    CFA_CDB_IS_INTF_INTERNAL (u4IfIndex) = i4Status;
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaCheckIfLocalMac                                   */
/*                                                                           */
/* Description        : This function checks whether the given Mac address   */
/*                      is self mac address                                  */
/*                                                                           */
/* Input(s)           : pHwAddr:Mac Address.                                 */
/*                      u4IfIndex :Interface Index                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaCheckIfLocalMac (tEnetV2Header * pHwAddr, UINT4 u4IfIndex)
{
    UINT1               au1SysMac[CFA_ENET_ADDR_LEN];
    UINT1               u1IfEtherType;
    INT4                i4IfValidStatus = CFA_FALSE;
    UINT1               u1DefRouterIdx = CFA_FALSE;
    UINT1               u1IfType = 0;
    UINT1               au1HwAddr[CFA_ENET_ADDR_LEN];

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* The stack interface uses the stack mac address
         * which is different from the system mac address*/

        CfaGetEthernetType (u4IfIndex, &u1IfEtherType);

        if (u1IfEtherType == CFA_STACK_ENET)
        {
            CfaGetIfHwAddr (CFA_DEFAULT_STACK_IFINDEX, au1SysMac);
        }
        else
        {
            CfaGetSysMacAddress (au1SysMac);
        }
    }
    else
    {
        CfaGetSysMacAddress (au1SysMac);
    }
    CFA_DS_LOCK ();

    MEMSET (au1HwAddr, 0, CFA_ENET_ADDR_LEN);
    u1IfType = CFA_CDB_IF_TYPE (u4IfIndex);
    CFA_CDB_CHECK_IF_TYPE (u1IfType, u1DefRouterIdx);
    if (CFA_FALSE == u1DefRouterIdx)
    {
        CFA_CDB_GET_ENET_IWF_LOCAL_MAC (u4IfIndex, au1HwAddr);
    }
    else
    {
        i4IfValidStatus = CFA_CDB_IS_INTF_VALID (CFA_DEFAULT_ROUTER_IFINDEX);
        if (i4IfValidStatus == CFA_TRUE)
        {
            CFA_CDB_GET_ENET_IWF_LOCAL_MAC (CFA_DEFAULT_ROUTER_IFINDEX,
                                            au1HwAddr);
        }
    }

    if (((MEMCMP (pHwAddr, au1HwAddr,
                  CFA_ENET_ADDR_LEN) == 0) ||
         (MEMCMP (pHwAddr, au1SysMac, CFA_ENET_ADDR_LEN) == 0)))
    {

        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    else
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name      : CfaCheckIsLocalMac                                   */
/*                                                                           */
/* Description        : This function checks whether the given Mac address   */
/*                      is self mac address                                  */
/*                                                                           */
/* Input(s)           : pHwAddr:Mac Address.                                 */
/*                      u4IfIndex :Interface Index                           */
/*                      u2VlanIfIndex :VlanInterface Index                   */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gaCfaCommonIfInfo                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaCheckIsLocalMac (tEnetV2Header * pHwAddr, UINT4 u4IfIndex,
                    UINT2 u2VlanIfIndex)
{
    UINT4               u4MaxVlanIfIndex = 0;

    UINT1               u1DefRouterIdx = CFA_FALSE;
    UINT1               u1IfType = 0;
    UINT1               au1HwAddr[CFA_ENET_ADDR_LEN];
    INT4                i4IfValidStatus = CFA_FALSE;

#ifdef WLC_WANTED
    u4MaxVlanIfIndex = SYS_DEF_NUM_PHYSICAL_INTERFACES +
        LA_MAX_AGG_INTF + IP_DEV_MAX_L3VLAN_INTF +
        SYS_DEF_MAX_TUNL_IFACES + SYS_DEF_MAX_WSS_IFACES;
#else

    u4MaxVlanIfIndex = SYS_DEF_NUM_PHYSICAL_INTERFACES +
        LA_MAX_AGG_INTF + IP_DEV_MAX_L3VLAN_INTF + SYS_DEF_MAX_TUNL_IFACES;
#endif

    MEMSET (au1HwAddr, 0, CFA_ENET_ADDR_LEN);

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }
    else
    {
        CFA_DS_LOCK ();
        u1IfType = CFA_CDB_IF_TYPE (u4IfIndex);
        CFA_CDB_CHECK_IF_TYPE (u1IfType, u1DefRouterIdx);
        if (CFA_FALSE == u1DefRouterIdx)
        {
            CFA_CDB_GET_ENET_IWF_LOCAL_MAC (u4IfIndex, au1HwAddr);
        }
        else
        {
            i4IfValidStatus =
                CFA_CDB_IS_INTF_VALID (CFA_DEFAULT_ROUTER_IFINDEX);

            if (i4IfValidStatus == CFA_TRUE)
            {
                CFA_CDB_GET_ENET_IWF_LOCAL_MAC (CFA_DEFAULT_ROUTER_IFINDEX,
                                                au1HwAddr);
            }
        }

        if (MEMCMP (pHwAddr, au1HwAddr, CFA_ENET_ADDR_LEN) == 0)
        {
            CFA_DS_UNLOCK ();
            return CFA_SUCCESS;
        }
    }

    if (((u2VlanIfIndex > u4MaxVlanIfIndex) || (u2VlanIfIndex == 0) )&& 
         (CfaIsL3SubIfIndex (u2VlanIfIndex) == CFA_FALSE))
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    else
    {
        u1IfType = CFA_CDB_IF_TYPE (u2VlanIfIndex);
        CFA_CDB_CHECK_IF_TYPE (u1IfType, u1DefRouterIdx);
        if (CFA_FALSE == u1DefRouterIdx)
        {
            CFA_CDB_GET_ENET_IWF_LOCAL_MAC (u2VlanIfIndex, au1HwAddr);
        }
        else
        {
            i4IfValidStatus =
                CFA_CDB_IS_INTF_VALID (CFA_DEFAULT_ROUTER_IFINDEX);
            if (i4IfValidStatus == CFA_TRUE)
            {
                CFA_CDB_GET_ENET_IWF_LOCAL_MAC (CFA_DEFAULT_ROUTER_IFINDEX,
                                                au1HwAddr);
            }
        }

        if (MEMCMP (pHwAddr, au1HwAddr, CFA_ENET_ADDR_LEN) == 0)
        {
            CFA_DS_UNLOCK ();
            return CFA_SUCCESS;
        }
    }
    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaInitCdbInfo                                       */
/*                                                                           */
/* Description        : This function initialises the fields in the common   */
/*                      data base interface entry.                           */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : Cfa Common Database                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
CfaInitCdbInfo (UINT4 u4IfIndex)
{
    CFA_DS_LOCK ();

    CFA_CDB_SET_INTF_VALID (u4IfIndex, CFA_FALSE);
    CFA_CDB_SET_INTF_ACTIVE (u4IfIndex, CFA_FALSE);
    CFA_CDB_IS_INTF_INTERNAL (u4IfIndex) = CFA_FALSE;
    CFA_CDB_IF_OPER (u4IfIndex) = CFA_IF_DOWN;
    CFA_CDB_IF_ROW_STATUS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CFA_CDB_IF_MTU (u4IfIndex) = 0;
    CFA_CDB_IF_SPEED (u4IfIndex) = 0;
    CFA_CDB_IF_SYS_SPECIFIC_PORTID (u4IfIndex) = 0;
    STRNCPY (CFA_CDB_IF_ALIAS (u4IfIndex), "", STRLEN(""));
    CFA_CDB_IF_ALIAS (u4IfIndex)[STRLEN("")] = '\0';
    STRNCPY (CFA_CDB_PORT_NAME (u4IfIndex), "", STRLEN(""));
    CFA_CDB_PORT_NAME (u4IfIndex)[STRLEN("")] = '\0';
    STRNCPY (CFA_CDB_IF_ALIAS_NAME (u4IfIndex), "", STRLEN(""));
    CFA_CDB_IF_ALIAS_NAME (u4IfIndex)[STRLEN("")] = '\0';
    MEMSET (CFA_CDB_IF_ENTRY (u4IfIndex)->au1MacAddr, 0, CFA_ENET_ADDR_LEN);
    CFA_CDB_IF_ENTRY (u4IfIndex)->u4UnnumAssocIPIf = 0;
    MEMSET (CFA_CDB_IF_ENTRY (u4IfIndex)->au1PeerMacAddr, 0, CFA_ENET_ADDR_LEN);
    CFA_CDB_IF_TYPE (u4IfIndex) = CFA_INVALID_TYPE;
    CFA_CDB_IF_DUPLEXSTATUS (u4IfIndex) = 0;
    CFA_CDB_IF_VLANID_OF_IVR (u4IfIndex) = 0;
    /* In H/w Autonegotiation is on by default. So in software enable
     * Tx & Rx Flow Control */
    CFA_CDB_IF_PAUSE_ADMIN_MODE (u4IfIndex) = PAUSE_ENABLED_BOTH;

    /* Initialize the EOAM parameters */
    CFA_CDB_IF_EOAM_STATUS (u4IfIndex) = EOAM_DISABLED;
    CFA_CDB_IF_EOAM_MUX_STATE (u4IfIndex) = EOAM_FWD;
    CFA_CDB_IF_EOAM_PAR_STATE (u4IfIndex) = EOAM_FWD;
    CFA_CDB_IF_EOAM_UNIDIR_SUPP (u4IfIndex) = EOAM_DISABLED;
    CFA_CDB_IF_EOAM_LINK_FAULT (u4IfIndex) = OSIX_FALSE;
    CFA_CDB_IF_EOAM_REMOTE_LB (u4IfIndex) = EOAM_DISABLED;
    CFA_CDB_IF_PORT_SEC_STATE (u4IfIndex) = CFA_PORT_STATE_TRUSTED;
    CFA_CDB_IF_IP_IPPORT (u4IfIndex) = CFA_INVALID_INDEX;
    CFA_CDB_IF_ENTRY (u4IfIndex)->IfMauEntry.pIfMauInfo = NULL;
    CFA_CDB_IF_STORAGE_TYPE (u4IfIndex) = CFA_STORAGE_TYPE_NONVOLATILE;

    /* clear the statistics structure */
    MEMSET (&(CFA_CDB_IF_COUNTERS (u4IfIndex)), 0, sizeof (tIfCountersStruct));

    CFA_DS_UNLOCK ();

    return;
}

/*****************************************************************************/
/* Function Name      : CfaCommonDSLock                                      */
/*                                                                           */
/* Description        : This function is used to take the CFA  mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      CFA common database structure                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS or CFA_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCommonDSLock (VOID)
{
    if (OsixSemTake (gCfaCdbSemId) != OSIX_SUCCESS)
    {
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaCommonDSUnLock                                    */
/*                                                                           */
/* Description        : This function is used to give the CFA mutual         */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structure by the protocol task.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS or CFA_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCommonDSUnLock (VOID)
{
    OsixSemGive (gCfaCdbSemId);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaTunnelDSLock                                      */
/*                                                                           */
/* Description        : This function is used to take the CFAT  mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      CFA Tunnel If Table data structure                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS or CFA_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
CfaTunnelDSLock (VOID)
{
    if (OsixSemTake (gCfaTnlSemId) != OSIX_SUCCESS)
    {
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaTunnelDSUnLock                                    */
/*                                                                           */
/* Description        : This function is used to give the CFA mutual         */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      Tunnel If Table data structure.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS or CFA_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
CfaTunnelDSUnLock (VOID)
{
    if (OsixSemGive (gCfaTnlSemId) != OSIX_SUCCESS)
    {
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaIfGetInOctets                                     */
/*                                                                           */
/* Description        : This function gets the number of incoming octets.    */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of Octets.                                    */
/*****************************************************************************/

/* Exportable APIs for accessing Interface Statistics */
UINT4
CfaIfGetInOctets (UINT4 u4IfIndex)
{
    UINT4               u4IfInOctets;
    CFA_DS_LOCK ();
    u4IfInOctets = CFA_CDB_IF_COUNTERS (u4IfIndex).u4InOctets;
    CFA_DS_UNLOCK ();
    return (u4IfInOctets);
}

/*****************************************************************************/
/* Function Name      : CfaIfGetInPkts                                       */
/*                                                                           */
/* Description        : This function gets the number of incoming packets.   */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of Octets.                                    */
/*****************************************************************************/

/* Exportable APIs for accessing Interface Statistics */
UINT4
CfaIfGetInPkts (UINT4 u4IfIndex)
{
    UINT4               u4IfInPkts = 0;
    CFA_DS_LOCK ();
    u4IfInPkts = CFA_CDB_IF_COUNTERS (u4IfIndex).u4InUcastPkts
        + CFA_CDB_IF_COUNTERS (u4IfIndex).u4InMulticastPkts
        + CFA_CDB_IF_COUNTERS (u4IfIndex).u4InBroadcastPkts;
    CFA_DS_UNLOCK ();
    return (u4IfInPkts);

}

/*****************************************************************************/
/* Function Name      : CfaIfGetInUcast                                      */
/*                                                                           */
/* Description        : This function gets the number of incoming            */
/*                      unicast packets.                                     */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of Unicast packets.                           */
/*****************************************************************************/
UINT4
CfaIfGetInUcast (UINT4 u4IfIndex)
{
    UINT4               u4IfInUcastPkts;
    CFA_DS_LOCK ();
    u4IfInUcastPkts = CFA_CDB_IF_COUNTERS (u4IfIndex).u4InUcastPkts;
    CFA_DS_UNLOCK ();
    return (u4IfInUcastPkts);
}

/*****************************************************************************/
/* Function Name      : CfaIfGetInNUOctets                                   */
/*                                                                           */
/* Description        : This function gets the sum of number of incoming     */
/*                      multicast packets and broadcast packets.             */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Sum of number of multicast and braodcast packets.    */
/*****************************************************************************/
UINT4
CfaIfGetInNUcast (UINT4 u4IfIndex)
{
    UINT4               u4NUcast = 0;

    CFA_DS_LOCK ();
    u4NUcast = CFA_CDB_IF_COUNTERS (u4IfIndex).u4InMulticastPkts
        + CFA_CDB_IF_COUNTERS (u4IfIndex).u4InBroadcastPkts;
    CFA_DS_UNLOCK ();
    return (u4NUcast);
}

/*****************************************************************************/
/* Function Name      : CfaIfGetInMCast                                      */
/*                                                                           */
/* Description        : This function gets the number of incoming            */
/*                      multicast packets.                                   */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of Multicast packets.                         */
/*****************************************************************************/
UINT4
CfaIfGetInMCast (UINT4 u4IfIndex)
{
    UINT4               u4IfInMcastPkts;
    CFA_DS_LOCK ();
    u4IfInMcastPkts = CFA_CDB_IF_COUNTERS (u4IfIndex).u4InMulticastPkts;
    CFA_DS_UNLOCK ();
    return (u4IfInMcastPkts);
}

/*****************************************************************************/
/* Function Name      : CfaIfGetInBCast                                      */
/*                                                                           */
/* Description        : This function gets the number of incoming            */
/*                      broadcast packets.                                   */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of broadcast packets.                         */
/*****************************************************************************/
UINT4
CfaIfGetInBCast (UINT4 u4IfIndex)
{
    UINT4               u4IfInBroadcastPkts;
    CFA_DS_LOCK ();
    u4IfInBroadcastPkts = CFA_CDB_IF_COUNTERS (u4IfIndex).u4InBroadcastPkts;
    CFA_DS_UNLOCK ();
    return (u4IfInBroadcastPkts);
}

/*****************************************************************************/
/* Function Name      : CfaIfGetInHcOctets                                   */
/*                                                                           */
/* Description        : This function gets the number of incoming high octets*/
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of high Octets.                               */
/*****************************************************************************/
UINT4
CfaIfGetInHcOctets (UINT4 u4IfIndex)
{
    UINT4               u4IfHighInOctets;
    CFA_DS_LOCK ();
    u4IfHighInOctets = CFA_CDB_IF_COUNTERS (u4IfIndex).u4HighInOctets;
    CFA_DS_UNLOCK ();
    return (u4IfHighInOctets);
}

/*****************************************************************************/
/* Function Name      : CfaIfGetInDiscard                                    */
/*                                                                           */
/* Description        : This function gets the number of incoming packets    */
/*                      discarded.                                           */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of discarded packets.                         */
/*****************************************************************************/
UINT4
CfaIfGetInDiscard (UINT4 u4IfIndex)
{
    UINT4               u4IfInDiscards;
    CFA_DS_LOCK ();
    u4IfInDiscards = CFA_CDB_IF_COUNTERS (u4IfIndex).u4InDiscards;
    CFA_DS_UNLOCK ();
    return (u4IfInDiscards);
}

/*****************************************************************************/
/* Function Name      : CfaIfGetInErr                                        */
/*                                                                           */
/* Description        : This function gets the number of incoming            */
/*                      error packets .                                      */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of error packets                              */
/*****************************************************************************/
UINT4
CfaIfGetInErr (UINT4 u4IfIndex)
{
    UINT4               u4IfInErrors;
    CFA_DS_LOCK ();
    u4IfInErrors = CFA_CDB_IF_COUNTERS (u4IfIndex).u4InErrors;
    CFA_DS_UNLOCK ();
    return (u4IfInErrors);
}

/*****************************************************************************/
/* Function Name      : CfaIfGetInUnk                                        */
/*                                                                           */
/* Description        : This function gets the number of incoming unknown    */
/*                      protocol packets.                                    */
/*                      Before calling the function the ifIndex entry should */
/*                                                                           */
/* Input(s)           : u4IfIndex : Interface index.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of unknown protocol packets.                  */
/*****************************************************************************/
UINT4
CfaIfGetInUnk (UINT4 u4IfIndex)
{
    UINT4               u4IfInUnknownProtos;
    CFA_DS_LOCK ();
    u4IfInUnknownProtos = CFA_CDB_IF_COUNTERS (u4IfIndex).u4InUnknownProtos;
    CFA_DS_UNLOCK ();
    return (u4IfInUnknownProtos);
}

/*****************************************************************************/
/* Function Name      : CfaIfGetOutOctets                                    */
/*                                                                           */
/* Description        : This function gets the number of outgoing octets.    */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of outgoing Octets.                           */
/*****************************************************************************/
UINT4
CfaIfGetOutOctets (UINT4 u4IfIndex)
{
    UINT4               u4IfOutOctets;
    CFA_DS_LOCK ();
    u4IfOutOctets = CFA_CDB_IF_COUNTERS (u4IfIndex).u4OutOctets;
    CFA_DS_UNLOCK ();
    return (u4IfOutOctets);
}

/*****************************************************************************/
/* Function Name      : CfaIfGetOutUCast                                     */
/*                                                                           */
/* Description        : This function gets the number of outgoing            */
/*                      unicast packets.                                     */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of Outgoing unicast packets.                   */
/*****************************************************************************/
UINT4
CfaIfGetOutUCast (UINT4 u4IfIndex)
{
    UINT4               u4IfOutUcastPkts;
    CFA_DS_LOCK ();
    u4IfOutUcastPkts = CFA_CDB_IF_COUNTERS (u4IfIndex).u4OutUcastPkts;
    CFA_DS_UNLOCK ();
    return (u4IfOutUcastPkts);
}

/*****************************************************************************/
/* Function Name      : CfaIfGetOutNUCast                                    */
/* Description        : This function gets the sum of number of outgoing     */
/*                      multicast packets and broadcast packets.             */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of outgoing multicast packets and             */
/*                      broadcast packets.                                   */
/*****************************************************************************/
UINT4
CfaIfGetOutNUCast (UINT4 u4IfIndex)
{
    UINT4               u4OutNUCast = 0;
    CFA_DS_LOCK ();
    u4OutNUCast = CFA_CDB_IF_COUNTERS (u4IfIndex).u4OutMulticastPkts
        + CFA_CDB_IF_COUNTERS (u4IfIndex).u4OutBroadcastPkts;
    CFA_DS_UNLOCK ();
    return (u4OutNUCast);
}

/*****************************************************************************/
/* Function Name      : CfaIfGetOutMCast                                     */
/*                                                                           */
/* Description        : This function gets the number of outgoing multicast. */
/*                      packets.                                             */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of Outgoing Multicast packets                 */
/*****************************************************************************/
UINT4
CfaIfGetOutMCast (UINT4 u4IfIndex)
{
    UINT4               u4IfOutMulticastPkts;
    CFA_DS_LOCK ();
    u4IfOutMulticastPkts = CFA_CDB_IF_COUNTERS (u4IfIndex).u4OutMulticastPkts;
    CFA_DS_UNLOCK ();
    return (u4IfOutMulticastPkts);
}

/*****************************************************************************/
/* Function Name      : CfaIfGetOutBCast                                     */
/*                                                                           */
/* Description        : This function gets the number of outgoing broadcast  */
/*                      packets .                                            */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of outgoing broadcast packets.                */
/*****************************************************************************/
UINT4
CfaIfGetOutBCast (UINT4 u4IfIndex)
{
    UINT4               u4IfOutBroadcastPkts;
    CFA_DS_LOCK ();
    u4IfOutBroadcastPkts = CFA_CDB_IF_COUNTERS (u4IfIndex).u4OutBroadcastPkts;
    CFA_DS_UNLOCK ();
    return (u4IfOutBroadcastPkts);
}

/*****************************************************************************/
/* Function Name      : CfaIfGetOutHcOctets                                  */
/*                                                                           */
/* Description        : This function gets the number of outgoing high octets*/
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of outgoing high Octets.                      */
/*****************************************************************************/
UINT4
CfaIfGetOutHcOctets (UINT4 u4IfIndex)
{
    UINT4               u4IfHighOutOctets;
    CFA_DS_LOCK ();
    u4IfHighOutOctets = CFA_CDB_IF_COUNTERS (u4IfIndex).u4HighOutOctets;
    CFA_DS_UNLOCK ();
    return (u4IfHighOutOctets);
}

/*****************************************************************************/
/* Function Name      : CfaIfGetOutDiscard                                   */
/*                                                                           */
/* Description        : This function gets the number of outgoing packets    */
/*                      discarded.                                           */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of outgoing octets discarded.                 */
/*****************************************************************************/
UINT4
CfaIfGetOutDiscard (UINT4 u4IfIndex)
{
    UINT4               u4IfOutDiscards;
    CFA_DS_LOCK ();
    u4IfOutDiscards = CFA_CDB_IF_COUNTERS (u4IfIndex).u4OutDiscards;
    CFA_DS_UNLOCK ();
    return (u4IfOutDiscards);
}

/*****************************************************************************/
/* Function Name      : CfaIfGetOutErr                                       */
/*                                                                           */
/* Description        : This function gets the number outgoing error packets */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of outgoing error packets.                    */
/*****************************************************************************/
UINT4
CfaIfGetOutErr (UINT4 u4IfIndex)
{
    UINT4               u4IfOutErrors;

    CFA_DS_LOCK ();
    u4IfOutErrors = CFA_CDB_IF_COUNTERS (u4IfIndex).u4OutErrors;
    CFA_DS_UNLOCK ();
    return (u4IfOutErrors);
}

/*****************************************************************************/
/* Function Name      : CfaIfSetInOctets                                     */
/*                                                                           */
/* Description        : This function sets the number of incoming octets.    */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                      u4InOctets:Counter for incoming octets               */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
/* Exportable APIs for Updating Interface Statistics */
VOID
CfaIfSetInOctets (UINT4 u4IfIndex, UINT4 u4InOctets)
{
    CFA_DS_LOCK ();
    CFA_CDB_IF_COUNTERS (u4IfIndex).u4InOctets += u4InOctets;
    CFA_DS_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIfSetInUcast                                      */
/*                                                                           */
/* Description        : This function increments the number of incoming      */
/*                      unicast packets.                                     */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
CfaIfSetInUCast (UINT4 u4IfIndex)
{
    CFA_DS_LOCK ();
    ++CFA_CDB_IF_COUNTERS (u4IfIndex).u4InUcastPkts;
    CFA_DS_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIfSetInDiscard                                    */
/*                                                                           */
/* Description        : This function increments the number of               */
/*                      incoming packets discarded                           */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
CfaIfSetInDiscard (UINT4 u4IfIndex)
{
    CFA_DS_LOCK ();
    ++CFA_CDB_IF_COUNTERS (u4IfIndex).u4InDiscards;
    CFA_DS_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIfSetInErr                                        */
/*                                                                           */
/* Description        : This function increments the number of incoming      */
/*                      error packets .                                      */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
CfaIfSetInErr (UINT4 u4IfIndex)
{
    CFA_DS_LOCK ();
    ++CFA_CDB_IF_COUNTERS (u4IfIndex).u4InErrors;
    CFA_DS_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIfSetInUnk                                        */
/*                                                                           */
/* Description        : This function increments the number of               */
/*                      incoming unknown protocol packets.                   */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex : Interface index.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
CfaIfSetInUnk (UINT4 u4IfIndex)
{
    CFA_DS_LOCK ();
    ++CFA_CDB_IF_COUNTERS (u4IfIndex).u4InUnknownProtos;
    CFA_DS_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIfSetInMCast                                      */
/*                                                                           */
/* Description        : This function increments the number of incoming      */
/*                      multicast packets.                                   */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CfaIfSetInMCast (UINT4 u4IfIndex)
{
    CFA_DS_LOCK ();
    ++CFA_CDB_IF_COUNTERS (u4IfIndex).u4InMulticastPkts;
    CFA_DS_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIfSetInBCast                                      */
/*                                                                           */
/* Description        : This function increments the number of incoming      */
/*                      broadcast packets.                                   */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CfaIfSetInBCast (UINT4 u4IfIndex)
{
    CFA_DS_LOCK ();
    ++CFA_CDB_IF_COUNTERS (u4IfIndex).u4InBroadcastPkts;
    CFA_DS_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIfSetInHcOctets                                   */
/*                                                                           */
/* Description        : This function sets the number of incoming high octets*/
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                      u4Octets :Number of High octets                      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CfaIfSetInHCOctets (UINT4 u4IfIndex, UINT4 u4Octets)
{
    CfaIfmUpdateHighCounters (u4IfIndex, u4Octets, CFA_INCOMING);
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIfIncrInHcOctets                                  */
/*                                                                           */
/* Description        : This function increments the number of               */
/*                      incoming high octets                                 */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CfaIfIncrInHCOctets (UINT4 u4IfIndex)
{
    CFA_DS_LOCK ();
    ++CFA_CDB_IF_COUNTERS (u4IfIndex).u4HighInOctets;
    CFA_DS_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIfSetOutOctets                                    */
/*                                                                           */
/* Description        : This function sets the number of outgoing octets.    */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                      u4Octets :Number of outgoing octets                  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                     .                           */
/*****************************************************************************/
VOID
CfaIfSetOutOctets (UINT4 u4IfIndex, UINT4 u4Octets)
{
    CFA_DS_LOCK ();
    CFA_CDB_IF_COUNTERS (u4IfIndex).u4OutOctets += u4Octets;
    CFA_DS_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIfSetOutUCast                                     */
/*                                                                           */
/* Description        : This function increments the number of outgoing      */
/*                      unicast packets.                                     */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CfaIfSetOutUCast (UINT4 u4IfIndex)
{
    CFA_DS_LOCK ();
    ++CFA_CDB_IF_COUNTERS (u4IfIndex).u4OutUcastPkts;
    CFA_DS_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIfSetOutDiscard                                   */
/*                                                                           */
/* Description        : This function increments the number of outgoing      */
/*                      packets discarded.                                   */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CfaIfSetOutDiscard (UINT4 u4IfIndex)
{
    CFA_DS_LOCK ();
    ++CFA_CDB_IF_COUNTERS (u4IfIndex).u4OutDiscards;
    CFA_DS_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIfSetOutErr                                       */
/*                                                                           */
/* Description        : This function increments the number of outgoing      */
/*                      error packets                                        */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CfaIfSetOutErr (UINT4 u4IfIndex)
{
    CFA_DS_LOCK ();
    ++CFA_CDB_IF_COUNTERS (u4IfIndex).u4OutErrors;
    CFA_DS_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIfSetOutMCast                                     */
/*                                                                           */
/* Description        : This function increments the number of outgoing      */
/*                      multicast packets.                                   */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CfaIfSetOutMCast (UINT4 u4IfIndex)
{
    CFA_DS_LOCK ();
    ++CFA_CDB_IF_COUNTERS (u4IfIndex).u4OutMulticastPkts;
    CFA_DS_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIfSetOutBCast                                     */
/*                                                                           */
/* Description        : This function increments the number of outgoing      */
/*                      broadcast  packets .                                 */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
CfaIfSetOutBCast (UINT4 u4IfIndex)
{
    CFA_DS_LOCK ();
    ++CFA_CDB_IF_COUNTERS (u4IfIndex).u4OutBroadcastPkts;
    CFA_DS_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIfSetOutHcOctets                                  */
/*                                                                           */
/* Description        : This function sets the number of outgoing high octets*/
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                      u4Octets :Number of High outgoing octets             */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CfaIfSetOutHCOctets (UINT4 u4IfIndex, UINT4 u4Octets)
{
    CfaIfmUpdateHighCounters (u4IfIndex, u4Octets, CFA_OUTGOING);
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIfIncrOutHcOctets                                 */
/*                                                                           */
/* Description        : This function increments the number of               */
/*                      outgoing high octets                                 */
/*                      Before calling the function the ifIndex entry should */
/*                      exist.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CfaIfIncrOutHCOctets (UINT4 u4IfIndex)
{
    CFA_DS_LOCK ();
    ++CFA_CDB_IF_COUNTERS (u4IfIndex).u4HighOutOctets;
    CFA_DS_UNLOCK ();
    return;
}

/* Scan thru' the TnlIfTbl to find whether the entry is already present */
/******************************************************************************
 * Function           : CfaGetTnlInCxt
 * Input(s)           : pTunlCxt - Tunnel Context Pointer
 *                      u4AddrType  - Ipv4/ Ipv6
 *                      u4SrcIpAddr - Source IP address
 *                      u4DestIpAddr - Dest IP address
 *                      pu1SrcIp6Addr - Source IP6 Address
 *                      pu1DestIp6Addr - Dest Ip6 Address
 *                      u4EncapsMethod 
 *                      u4ConfigId
 * Output(s)          : pNode - Matching TnlIfEntry 
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This routine scans the TnlIfTbl to find the matching 
 *                      TnlEntry.
 ******************************************************************************/
INT4
CfaGetTnlInCxt (tCfaTnlCxt * pTunlCxt, UINT4 u4AddrType, UINT4 u4SrcIpAddr,
                UINT4 u4DestIpAddr, UINT1 *pu1SrcIp6Addr, UINT1 *pu1DestIp6Addr,
                UINT4 u4EncapsMethod, UINT4 u4ConfigId, tTnlIfNode * pNode)
{
    tTnlIfNode         *pTnlIfNode = NULL;

    CFA_TNL_DS_LOCK ();
    TMO_SLL_Scan (&(pTunlCxt->TnlIfSLL), pTnlIfNode, tTnlIfNode *)
    {
        if ((pTnlIfNode->u4AddrType == u4AddrType) &&
            (pTnlIfNode->u4EncapsMethod == u4EncapsMethod) &&
            (pTnlIfNode->u4ConfigId == u4ConfigId) &&
            (pTnlIfNode->u4ContextId == pTunlCxt->u4ContextId))
        {
            if (u4AddrType == ADDR_TYPE_IPV4)
            {
                if ((pTnlIfNode->LocalAddr.Ip4TnlAddr == u4SrcIpAddr) &&
                    (pTnlIfNode->RemoteAddr.Ip4TnlAddr == u4DestIpAddr))
                {
                    break;
                }
            }
            else if (u4AddrType == ADDR_TYPE_IPV6)
            {
                if ((MEMCMP (&(pTnlIfNode->LocalAddr.Ip6TnlAddr),
                             pu1SrcIp6Addr, IP6ADDR_SIZE) == 0) &&
                    (MEMCMP (&(pTnlIfNode->RemoteAddr.Ip6TnlAddr),
                             pu1DestIp6Addr, IP6ADDR_SIZE) == 0))
                {
                    break;
                }
            }
        }
    }
    if (pTnlIfNode != NULL)
    {
        MEMCPY (pNode, pTnlIfNode, sizeof (tTnlIfNode));
        CFA_TNL_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaFindTnlEntryFromIfIndex
 * Input                u4IfIndex - Index of the interface
 * Output(s)          : TnlIfEntry
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This routine scans the TnlIfTbl to find the matching 
 *                      TnlEntry with the specified IfIndex.
 ******************************************************************************/
INT4
CfaFindTnlEntryFromIfIndex (UINT4 u4IfIndex, tTnlIfEntry * pTnlIfEntry)
{
    tTnlIfNode         *pNode = NULL;
    tTnlIfNode         *pTnlIfNode = NULL;
    UINT4               u4ContextId = 0;
    tCfaTnlCxt         *pTunlCxt = NULL;

    if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId) == VCM_FAILURE)
    {
        pTnlIfEntry = NULL;
        return CFA_FAILURE;
    }
    if ((pTunlCxt = CfaTnlGetCxt (u4ContextId)) == NULL)
    {
        pTnlIfEntry = NULL;
        return CFA_FAILURE;
    }
    CFA_TNL_DS_LOCK ();
    TMO_SLL_Scan (&(pTunlCxt->TnlIfSLL), pTnlIfNode, tTnlIfNode *)
    {
        if ((pTnlIfNode->u4IfIndex == u4IfIndex) &&
            (pTnlIfNode->u4ContextId == pTunlCxt->u4ContextId))
        {
            pNode = pTnlIfNode;
            break;
        }
    }
    if (pNode != NULL)
    {
        CfaTnlCopyEntry (pTnlIfEntry, pNode);
        CFA_TNL_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    pTnlIfEntry = NULL;
    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaFindTnlEntryFromContextId
 * Input                u4ContextId - virtual Context Identifier
 *                      u4IfIndex - Interface index 
 * Output(s)          : TnlIfEntry - tunnel entry which is matched with ifindex 
 *                                   in the given context.   
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This routine scans the TnlIfTbl to find the matching 
 *                      TnlEntry with the specified IfIndex.
 ******************************************************************************/
INT4
CfaFindTnlEntryFromContextId (UINT4 u4ContextId, UINT4 u4IfIndex,
                              tTnlIfEntry * pTnlIfEntry)
{
    tTnlIfNode         *pNode = NULL;
    tTnlIfNode         *pTnlIfNode = NULL;
    tCfaTnlCxt         *pTunlCxt = NULL;

    if ((pTunlCxt = CfaTnlGetCxt (u4ContextId)) == NULL)
    {
        pTnlIfEntry = NULL;
        return CFA_FAILURE;
    }
    CFA_TNL_DS_LOCK ();
    TMO_SLL_Scan (&(pTunlCxt->TnlIfSLL), pTnlIfNode, tTnlIfNode *)
    {
        if ((pTnlIfNode->u4IfIndex == u4IfIndex) &&
            (pTnlIfNode->u4ContextId == pTunlCxt->u4ContextId))
        {
            pNode = pTnlIfNode;
            break;
        }
    }
    if (pNode != NULL)
    {
        CfaTnlCopyEntry (pTnlIfEntry, pNode);
        CFA_TNL_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    pTnlIfEntry = NULL;
    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaTnlFindEntryInCxt
 * Input(s)           : u4ContextId - Tunnel Context Id
 *                      pTunlCxt - Tunnel context pointer
 *                      u4AddrType - type of addr (IPV4/IPV6)
 *                      pTnlSrcAddr - source addr of the tunnel
 *                      pTnlDestAddr - dest addr of the tunnel
 * Output(s)          : pTnlIfEntry
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This routine scans the TnlIfTbl to find the matching 
 *                      TnlEntry with the specified addr type, src addr &
 *                      dest addr. 
 ******************************************************************************/
INT4
CfaTnlFindEntryInCxt (UINT4 u4ContextId,
                      UINT4 u4AddrType, tTnlIpAddr * pTnlSrcAddr,
                      tTnlIpAddr * pTnlDestAddr, UINT1 u1Direction,
                      tTnlIfEntry * pTnlIfEntry)
{
    tTnlIfNode         *pNode = NULL;
    tTnlIfNode         *pTnlIfNode = NULL;
    tCfaTnlCxt         *pTunlCxt = NULL;

    if ((pTunlCxt = CfaTnlGetCxt (u4ContextId)) == NULL)
    {
        pTnlIfEntry = NULL;
        return CFA_FAILURE;
    }

    if ((pTnlSrcAddr == NULL) && (pTnlDestAddr == NULL))
    {
        return CFA_FAILURE;
    }

    if ((u4AddrType != ADDR_TYPE_IPV4) && (u4AddrType != ADDR_TYPE_IPV6))
    {
        return CFA_FAILURE;
    }
    CFA_TNL_DS_LOCK ();
    TMO_SLL_Scan (&(pTunlCxt->TnlIfSLL), pTnlIfNode, tTnlIfNode *)
    {
        if ((u4AddrType == ADDR_TYPE_IPV4) &&
            (pTunlCxt->u4ContextId == pTnlIfNode->u4ContextId))
        {
            if (pTnlSrcAddr != NULL)
            {
                if (pTnlIfNode->LocalAddr.Ip4TnlAddr == pTnlSrcAddr->Ip4TnlAddr)
                {
                    if (pTnlDestAddr != NULL)
                    {
                        /* Both TnlSrc & TnlDest are specified */
                        if (pTnlIfNode->RemoteAddr.Ip4TnlAddr ==
                            pTnlDestAddr->Ip4TnlAddr)
                        {
                            if ((u1Direction != 0) &&
                                (pTnlIfNode->u1DirFlag == TNL_UNIDIRECTIONAL))
                            {
                                /* Direction is specified, for uni-directional 
                                 * look for matching direction */
                                if (pTnlIfNode->u1Direction == u1Direction)
                                {
                                    pNode = pTnlIfNode;
                                    break;
                                }
                            }
                            else
                            {
                                /* Direction is not mentioned, 
                                 * look for bi-directional entry */
                                pNode = pTnlIfNode;
                                break;
                            }
                        }
                    }
                    else        /* pTnlDestAddr == NULL */
                    {
                        if ((u1Direction != 0) &&
                            (pTnlIfNode->u1DirFlag == TNL_UNIDIRECTIONAL))
                        {
                            /* Direction is specified, 
                             * look for matching direction */
                            if (pTnlIfNode->u1Direction == u1Direction)
                            {
                                pNode = pTnlIfNode;
                                break;
                            }
                        }
                        else
                        {
                            /* Direction is not mentioned, 
                             * look for bi-directional entry */
                            pNode = pTnlIfNode;
                            break;
                        }
                    }
                }
            }
            else                /* pTnlSrcAddr == NULL */
            {
                if (pTnlIfNode->RemoteAddr.Ip4TnlAddr ==
                    pTnlDestAddr->Ip4TnlAddr)
                {
                    if ((u1Direction != 0) &&
                        (pTnlIfNode->u1DirFlag == TNL_UNIDIRECTIONAL))
                    {
                        /* Direction is specified, 
                         * look for matching direction */
                        if (pTnlIfNode->u1Direction == u1Direction)
                        {
                            pNode = pTnlIfNode;
                            break;
                        }
                    }
                    else
                    {
                        /* Direction is not mentioned, 
                         * look for bi-directional entry */
                        pNode = pTnlIfNode;
                        break;
                    }
                }
            }
        }                        /* end of ADDR_TYPE_V4 */
        else if ((u4AddrType == ADDR_TYPE_IPV6) &&
                 (pTunlCxt->u4ContextId == pTnlIfNode->u4ContextId))
        {
            if (pTnlSrcAddr != NULL)
            {
                if (MEMCMP (&(pTnlIfNode->RemoteAddr.Ip6TnlAddr),
                            pTnlSrcAddr->Ip6TnlAddr, IP6ADDR_SIZE) == 0)
                {
                    if (pTnlDestAddr != NULL)
                    {
                        /* Both TnlSrc & TnlDest are specified */
                        if (MEMCMP (&(pTnlIfNode->RemoteAddr.Ip6TnlAddr),
                                    pTnlDestAddr->Ip6TnlAddr,
                                    IP6ADDR_SIZE) == 0)
                        {
                            pNode = pTnlIfNode;
                            break;
                        }
                    }
                    else        /* pTnlDestAddr == NULL */
                    {
                        pNode = pTnlIfNode;
                        break;
                    }
                }
            }
            else                /* pTnlSrcAddr == NULL */
            {
                if (MEMCMP (&(pTnlIfNode->RemoteAddr.Ip6TnlAddr),
                            pTnlDestAddr->Ip6TnlAddr, IP6ADDR_SIZE) == 0)
                {
                    pNode = pTnlIfNode;
                    break;
                }
            }
        }
    }
    if (pNode != NULL)
    {
        CfaTnlCopyEntry (pTnlIfEntry, pNode);
        CFA_TNL_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    pTnlIfEntry = NULL;
    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaCreateAndInitTnlIfEntryInCxt
 * Input(s)           : pTunlCxt - Tunnel context pointer  
 *                      pTnlIfEntry - Tnl entry to be created  
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This routine creates a new SLL node in TnlIfTbl 
 *                      and initializes all default values in the node.
 ******************************************************************************/
INT4
CfaCreateAndInitTnlIfEntryInCxt (tCfaTnlCxt * pTunlCxt,
                                 tTnlIfEntry * pTnlIfEntry)
{
    tTnlIfNode         *pTnlIfNode = NULL;    /* Node to be inserted */
    tTnlIfNode         *pPrevNode = NULL;
    tTnlIfNode         *pScanNode = NULL;
    UINT4               u4PhyIfIndex = 0;

    /* allocate for the tunnel table entry */
    if ((pTnlIfNode = (tTnlIfNode *)
         MemAllocMemBlk (gCfaTnlGblInfo.CfaTnlMemPoolId)) == NULL)
    {
        return CFA_FAILURE;
    }

    MEMSET (pTnlIfNode, 0, sizeof (tTnlIfNode));

    TMO_SLL_Init_Node (&pTnlIfNode->Next);

    pTnlIfNode->u4ContextId = pTunlCxt->u4ContextId;

    MEMCPY (pTnlIfNode->au1IfAlias, pTnlIfEntry->au1IfAlias,
            CFA_MAX_PORT_NAME_LENGTH);
    pTnlIfNode->u4AddrType = pTnlIfEntry->u4AddrType;
    if (pTnlIfEntry->u4AddrType == ADDR_TYPE_IPV4)
    {
        pTnlIfNode->LocalAddr.Ip4TnlAddr = pTnlIfEntry->LocalAddr.Ip4TnlAddr;
        pTnlIfNode->RemoteAddr.Ip4TnlAddr = pTnlIfEntry->RemoteAddr.Ip4TnlAddr;
        if (CfaIpIfGetIfIndexFromIpAddressInCxt (pTunlCxt->u4ContextId,
                                                 pTnlIfEntry->LocalAddr.
                                                 Ip4TnlAddr,
                                                 &u4PhyIfIndex) == CFA_FAILURE)
        {
            MemReleaseMemBlock (gCfaTnlGblInfo.CfaTnlMemPoolId,
                                (UINT1 *) pTnlIfNode);
            return CFA_FAILURE;
        }
        pTnlIfNode->u4PhyIfIndex = u4PhyIfIndex;
    }
    else if (pTnlIfEntry->u4AddrType == ADDR_TYPE_IPV6)
    {
        MEMCPY (pTnlIfNode->LocalAddr.Ip6TnlAddr,
                pTnlIfEntry->LocalAddr.Ip6TnlAddr, IP6ADDR_SIZE);
        MEMCPY (pTnlIfNode->RemoteAddr.Ip6TnlAddr,
                pTnlIfEntry->RemoteAddr.Ip6TnlAddr, IP6ADDR_SIZE);
    }
    pTnlIfNode->u4EncapsMethod = pTnlIfEntry->u4EncapsMethod;
    pTnlIfNode->u4ConfigId = pTnlIfEntry->u4ConfigId;

    /* option not configurable - not supported currently */
    pTnlIfNode->u4EncapLmt = 0xFF;

    pTnlIfNode->i4FlowLabel = 0;

    /* 0 indicates that the TTL is copied from the payload's header */
    pTnlIfNode->u2HopLimit = 0;

    /* -1 indicates that TOS bits are coped from payload's hdr */
    pTnlIfNode->i2TOS = (INT2) -1;

    /* set default values */
    pTnlIfNode->u1Security = TNL_SECURITY_NONE;
    pTnlIfNode->u1DirFlag = TNL_BIDIRECTIONAL;
    pTnlIfNode->u1Direction = TNL_OUTGOING;
    pTnlIfNode->u1EncapOption = TNL_ENCAP_DISABLE;
    pTnlIfNode->bCksumFlag = TNL_CKSUM_DISABLE;
    pTnlIfNode->bPmtuFlag = TNL_PMTU_DISABLE;
    pTnlIfNode->i4PathMtuSock = CFA_INVALID_DESC;

    CfaGetIfMtu (u4PhyIfIndex, &(pTnlIfNode->u4Mtu));
    /* path-mtu-discovery is disabled by default, set the outgoing link MTU */
    if (pTnlIfNode->u4EncapsMethod == TNL_TYPE_GRE)
    {
        pTnlIfNode->u4Mtu = pTnlIfNode->u4Mtu - IP_HDR_LEN - MAX_GRE_HDR_LEN;
    }
    else
    {
        pTnlIfNode->u4Mtu = pTnlIfNode->u4Mtu - IP_HDR_LEN;
    }

    pTnlIfNode->u1RowStatus = CFA_RS_NOTINSERVICE;
    CFA_TNL_DS_LOCK ();
    TMO_SLL_Scan (&(pTunlCxt->TnlIfSLL), pScanNode, tTnlIfNode *)
    {
        if ((pScanNode->u4AddrType > pTnlIfNode->u4AddrType) ||
            MEMCMP (&(pScanNode->LocalAddr),
                    &(pTnlIfNode->LocalAddr), sizeof (tTnlIpAddr)) > 0 ||
            MEMCMP (&(pScanNode->RemoteAddr),
                    &(pTnlIfNode->RemoteAddr), sizeof (tTnlIpAddr)) > 0 ||
            (pScanNode->u4EncapsMethod > pTnlIfNode->u4EncapsMethod) ||
            (pScanNode->u4ConfigId > pTnlIfNode->u4ConfigId))

        {
            break;
        }

        else
        {
            pPrevNode = pScanNode;
        }
    }

    if (pPrevNode == NULL)
    {
        /* Node to be inserted in first position */
        TMO_SLL_Insert (&(pTunlCxt->TnlIfSLL), NULL,
                        (tTMO_SLL_NODE *) & (pTnlIfNode->Next));
    }
    else if (pScanNode == NULL)
    {
        /* Node to be inserted in last position */
        TMO_SLL_Insert (&(pTunlCxt->TnlIfSLL),
                        (tTMO_SLL_NODE *) & (pPrevNode->Next),
                        (tTMO_SLL_NODE *) & (pTnlIfNode->Next));
    }
    else
    {
        /* Node to be inserted in middle */
        TMO_SLL_Insert_In_Middle (&(pTunlCxt->TnlIfSLL),
                                  (tTMO_SLL_NODE *) & (pPrevNode->Next),
                                  (tTMO_SLL_NODE *) & (pTnlIfNode->Next),
                                  (tTMO_SLL_NODE *) & (pScanNode->Next));
    }
    CFA_TNL_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaDeleteTnlIfEntryInCxt
 * Input(s)           : pTunlCxt - Tunnel context pointer 
 *                    : u4IfIndex - Index of the interface
 * Output(s)          : None. 
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This routine scans the TnlIfTbl, finds the matching 
 *                      TnlEntry and deletes the entry.
 ******************************************************************************/
INT4
CfaDeleteTnlIfEntryInCxt (tCfaTnlCxt * pTunlCxt, UINT4 u4IfIndex)
{
    tTnlIfNode         *pTnlIfNode = NULL;

    if (pTunlCxt == NULL)
    {
        return CFA_FAILURE;
    }

    CFA_TNL_DS_LOCK ();
    TMO_SLL_Scan (&(pTunlCxt->TnlIfSLL), pTnlIfNode, tTnlIfNode *)
    {
        if (pTnlIfNode->u4IfIndex == u4IfIndex)
        {
            TMO_SLL_Delete (&(pTunlCxt->TnlIfSLL),
                            (tTMO_SLL_NODE *) & (pTnlIfNode->Next));

            if (MemReleaseMemBlock (gCfaTnlGblInfo.CfaTnlMemPoolId,
                                    (UINT1 *) pTnlIfNode) != MEM_SUCCESS)
            {
                CFA_TNL_DS_UNLOCK ();
                return CFA_FAILURE;
            }
            else
            {
                pTnlIfNode = NULL;
            }
            CFA_TNL_DS_UNLOCK ();
            return CFA_SUCCESS;
        }
    }
    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaTnlPhyIfDelete
 * Input(s)           : None. 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : Physical interface associated with Tnl interface has
 *                      been deleted. So the oper status of the Tnl interface
 *                      should be made down. 
 ******************************************************************************/
INT4
CfaTnlPhyIfDelete (tCfaRegInfo * pCfaInfo)
{
    tTnlIfNode         *pTnlIfNode = NULL;
    UINT4               u4TnlIfIndex;
    UINT4               u4ContextId = 0;
    INT4                i4RetVal = CFA_SUCCESS;
    UINT1               u1TnlIfOperSts = 0;
    tCfaTnlCxt         *pTunlCxt = NULL;

    if (VcmGetContextIdFromCfaIfIndex (pCfaInfo->u4IfIndex,
                                       &u4ContextId) == VCM_FAILURE)
    {
        return (CFA_FAILURE);
    }

    CFA_TNL_DS_LOCK ();
    if ((pTunlCxt = CfaTnlGetCxt (u4ContextId)) == NULL)
    {
        i4RetVal = CFA_FAILURE;
        CFA_TNL_DS_UNLOCK ();
        return (i4RetVal);
    }
    TMO_SLL_Scan (&(pTunlCxt->TnlIfSLL), pTnlIfNode, tTnlIfNode *)
    {
        if ((pTnlIfNode->u1RowStatus == CFA_RS_ACTIVE) &&
            (pTnlIfNode->u4PhyIfIndex == pCfaInfo->u4IfIndex))
        {
            /* matching phyIf associated with TnlIf found. */
            u1TnlIfOperSts = CFA_IF_DOWN;
            /* change the oper sts of the Tnl interface */
            u4TnlIfIndex = pTnlIfNode->u4IfIndex;
            if (CfaInterfaceStatusChangeIndication (u4TnlIfIndex,
                                                    u1TnlIfOperSts)
                == CFA_FAILURE)
            {
                i4RetVal = CFA_FAILURE;
            }
            /* Physical Interface is deleted. Means the tunnel is no more
             * attached to this Physical interface. Reset the physical
             * If index. */
            pTnlIfNode->u4PhyIfIndex = 0;
        }
    }
    CFA_TNL_DS_UNLOCK ();
    return (i4RetVal);
}

/******************************************************************************
 * Function           : CfaTnlPhyIfUpdate
 * Input(s)           : None. 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : physical interface params associated with tunnel
 *                      interface is modified. Make the TnlIf oper DOWN.
 ******************************************************************************/
INT4
CfaTnlPhyIfUpdate (tCfaRegInfo * pCfaInfo)
{
    tTnlIfNode         *pTnlIfNode = NULL;
    UINT4               u4TnlIfIndex;
    UINT4               u4ContextId = 0;
    INT4                i4RetVal = CFA_SUCCESS;
    UINT1               u1TnlIfOperSts = 0;
    UINT4               u4IfMtu;
    tCfaTnlCxt         *pTunlCxt = NULL;

    if (VcmGetContextIdFromCfaIfIndex (pCfaInfo->u4IfIndex,
                                       &u4ContextId) == VCM_FAILURE)
    {
        return (CFA_FAILURE);
    }

    CFA_TNL_DS_LOCK ();
    if ((pTunlCxt = CfaTnlGetCxt (u4ContextId)) == NULL)
    {
        i4RetVal = CFA_FAILURE;
        CFA_TNL_DS_UNLOCK ();
        return (i4RetVal);
    }
    TMO_SLL_Scan (&(pTunlCxt->TnlIfSLL), pTnlIfNode, tTnlIfNode *)
    {
        u1TnlIfOperSts = 0;
        if (pTnlIfNode->u1RowStatus == CFA_RS_ACTIVE)
        {
            if (pTnlIfNode->u4PhyIfIndex == pCfaInfo->u4IfIndex)
            {
                CfaGetIfMtu (pCfaInfo->u4IfIndex, &u4IfMtu);
                /* matching phyIf associated with TnlIf found. */
                if (pTnlIfNode->u4AddrType == ADDR_TYPE_IPV4)
                {
                    if (CfaIpIfIsOurInterfaceAddress (pCfaInfo->u4IfIndex,
                                                      pTnlIfNode->LocalAddr.
                                                      Ip4TnlAddr) ==
                        CFA_FAILURE)
                    {
                        /* IpAddr of phyIf is modified. */
                        u1TnlIfOperSts = CFA_IF_DOWN;
                        /* Physical Address is changed. Means the tunnel is
                         * no more attached to this Physical interface. Reset
                         * the physical If index. */
                        pTnlIfNode->u4PhyIfIndex = 0;
                    }
                    else if ((pTnlIfNode->u4EncapsMethod == TNL_TYPE_GRE) &&
                             (pTnlIfNode->u4Mtu !=
                              (u4IfMtu - IP_HDR_LEN - MAX_GRE_HDR_LEN)))
                    {
                        /* MTU of phyIf is modified */
                        CfaInterfaceMtuChangeIndication
                            (pTnlIfNode->u4IfIndex,
                             (u4IfMtu - IP_HDR_LEN - MAX_GRE_HDR_LEN));
                    }
                    else if ((pTnlIfNode->u4EncapsMethod != TNL_TYPE_GRE) &&
                             (pTnlIfNode->u4Mtu != (u4IfMtu - IP_HDR_LEN)))
                    {
                        /* MTU of phyIf is modified */
                        CfaInterfaceMtuChangeIndication
                            (pTnlIfNode->u4IfIndex, (u4IfMtu - IP_HDR_LEN));
                    }
                    else
                    {
                        /* May be Change in Tunnel Interface Status. */
                        CfaGetIfOperStatus (pCfaInfo->u4IfIndex,
                                            &u1TnlIfOperSts);
                    }
                }
            }
            else
            {
                if ((pTnlIfNode->u4PhyIfIndex == 0) &&
                    (pTnlIfNode->u4AddrType == ADDR_TYPE_IPV4) &&
                    (CfaIpIfIsOurInterfaceAddress (pCfaInfo->u4IfIndex,
                                                   pTnlIfNode->LocalAddr.
                                                   Ip4TnlAddr) == CFA_SUCCESS))
                {
                    /* IpAddr of phyIf is modified. */
                    CfaGetIfOperStatus (pCfaInfo->u4IfIndex, &u1TnlIfOperSts);
                    /* Tunnel is associated with this physical interface
                     * address */
                    pTnlIfNode->u4PhyIfIndex = pCfaInfo->u4IfIndex;
                }
            }
        }

        if (u1TnlIfOperSts != 0)
        {
            /* change the oper sts of the Tnl interface */
            u4TnlIfIndex = pTnlIfNode->u4IfIndex;
            if (CfaInterfaceStatusChangeIndication (u4TnlIfIndex,
                                                    u1TnlIfOperSts)
                == CFA_FAILURE)
            {
                i4RetVal = CFA_FAILURE;
            }
        }
    }
    CFA_TNL_DS_UNLOCK ();
    return (i4RetVal);
}

/******************************************************************************
 * Function           : CfaTnlPhyIfOperStChg
 * Input(s)           : None. 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : OperSts of the phyIf associated with TnlIf is modified.
 *                      Change the TnlIf oper sts accordingly.             
 ******************************************************************************/
INT4
CfaTnlPhyIfOperStChg (tCfaRegInfo * pCfaInfo)
{
    tTnlIfNode         *pTnlIfNode = NULL;
    INT4                i4RetVal = CFA_SUCCESS;
    UINT4               u4TnlIfIndex;
    UINT4               u4ContextId = 0;
    UINT1               u1TnlIfOperSts = 0;
    tCfaTnlCxt         *pTunlCxt = NULL;

    if (VcmGetContextIdFromCfaIfIndex (pCfaInfo->u4IfIndex,
                                       &u4ContextId) == VCM_FAILURE)
    {
        return (CFA_FAILURE);
    }

    CFA_TNL_DS_LOCK ();
    if ((pTunlCxt = CfaTnlGetCxt (u4ContextId)) == NULL)
    {
        i4RetVal = CFA_FAILURE;
        CFA_TNL_DS_UNLOCK ();
        return (i4RetVal);
    }
    TMO_SLL_Scan (&(pTunlCxt->TnlIfSLL), pTnlIfNode, tTnlIfNode *)
    {
        if ((pTnlIfNode->u1RowStatus == CFA_RS_ACTIVE) &&
            (pTnlIfNode->u4PhyIfIndex == pCfaInfo->u4IfIndex))
        {
            if (CfaIpIfIsOurInterfaceAddress (pCfaInfo->u4IfIndex,
                                              pTnlIfNode->LocalAddr.
                                              Ip4TnlAddr) == CFA_SUCCESS)
            {
                /* matching phyIf associated with TnlIf found. */
                u1TnlIfOperSts = (UINT1) pCfaInfo->CfaIntfInfo.u1IfOperStatus;
                /* change the oper sts of the Tnl interface */
                u4TnlIfIndex = pTnlIfNode->u4IfIndex;
                if (CfaInterfaceStatusChangeIndication
                    (u4TnlIfIndex, u1TnlIfOperSts) == CFA_FAILURE)
                {
                    i4RetVal = CFA_FAILURE;
                }
            }
        }
    }
    CFA_TNL_DS_UNLOCK ();
    return (i4RetVal);
}

/******************************************************************************
 * Function           : CfaTnlMgrRtChgHdlr
 * Input(s)           : pNetIpv4RtInfo - Route info struct  
 *                      u1CmdType - command action
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This is the call-back function registered to 
 *                      receive route change notification from Ipv4. 
 ******************************************************************************/
VOID
CfaTnlMgrRtChgHdlr (tNetIpv4RtInfo * pNetIpv4RtInfo, tNetIpv4RtInfo * pNetIpv4RtInfo1, UINT1 u1CmdType)
{
    tTnlIfNode         *pTnlIfNode = NULL;
    UINT4               u4IfIndex = 0;
    UINT1               u1IfOperStatus;
    UINT1               u1OperSts = CFA_IF_UNK;
    UINT1               u1OperStChg = FALSE;
    tCfaTnlCxt         *pTunlCxt = NULL;

    if ((pTunlCxt = CfaTnlGetCxt (pNetIpv4RtInfo->u4ContextId)) == NULL)
    {
        /* No change is route status. */
        return;
    }

    switch (u1CmdType)
    {
        case NETIPV4_DELETE_ROUTE:
            /* check for reachability of tnl dest when a route is deleted */
            CFA_TNL_DS_LOCK ();
            TMO_SLL_Scan (&(pTunlCxt->TnlIfSLL), pTnlIfNode, tTnlIfNode *)
            {
                /* Tnl remote addr is known only for configured tunnels 
                 * (GRE/IPV6IP). */
                if ((pTnlIfNode->u4AddrType == ADDR_TYPE_IPV4) &&
                    ((pTnlIfNode->u4EncapsMethod == TNL_TYPE_GRE) ||
                     (pTnlIfNode->u4EncapsMethod == TNL_TYPE_IPV6IP)) &&
                    (pTnlIfNode->RemoteAddr.Ip4TnlAddr ==
                     pNetIpv4RtInfo->u4DestNet))
                {
                    /* TnlDestAddr is not reachable. OperSts should be 
                     * made DOWN. */
                    u4IfIndex = pTnlIfNode->u4IfIndex;
                    u1OperSts = CFA_IF_DOWN;

                    CfaGetIfOperStatus (u4IfIndex, &u1IfOperStatus);

                    if (u1IfOperStatus != u1OperSts)
                    {
                        /* Change in OperSts to be handled */
                        u1OperStChg = TRUE;
                    }
                }
            }
            CFA_TNL_DS_UNLOCK ();
            break;

        case NETIPV4_ADD_ROUTE:
            /* check for reachability of tnl dest when a route is added */
            CFA_TNL_DS_LOCK ();
            TMO_SLL_Scan (&(pTunlCxt->TnlIfSLL), pTnlIfNode, tTnlIfNode *)
            {
                /* Tnl remote addr is known only for configured tunnels 
                 * (GRE/IPV6IP). */
                if ((pTnlIfNode->u4AddrType == ADDR_TYPE_IPV4) &&
                    ((pTnlIfNode->u4EncapsMethod == TNL_TYPE_GRE) ||
                     (pTnlIfNode->u4EncapsMethod == TNL_TYPE_IPV6IP)) &&
                    (pTnlIfNode->RemoteAddr.Ip4TnlAddr ==
                     pNetIpv4RtInfo->u4DestNet))
                {
                    /* TnlDestAddr is reachable. OperSts should be made UP */
                    u4IfIndex = pTnlIfNode->u4IfIndex;
                    u1OperSts = CFA_IF_UP;
                    CfaGetIfOperStatus (u4IfIndex, &u1IfOperStatus);
                    if (u1IfOperStatus != u1OperSts)
                    {
                        /* Change in OperSts to be handled */
                        u1OperStChg = TRUE;
                    }
                }
            }
            CFA_TNL_DS_UNLOCK ();
            break;

case NETIPV4_MODIFY_ROUTE:
            CFA_TNL_DS_LOCK ();
            TMO_SLL_Scan (&(pTunlCxt->TnlIfSLL), pTnlIfNode, tTnlIfNode *)
            {
                /* Tnl remote addr is known only for configured tunnels
                 * (GRE/IPV6IP). */
                if ((pTnlIfNode->u4AddrType == ADDR_TYPE_IPV4) &&
                        ((pTnlIfNode->u4EncapsMethod == TNL_TYPE_GRE) ||
                         (pTnlIfNode->u4EncapsMethod == TNL_TYPE_IPV6IP)) &&
                        (pTnlIfNode->RemoteAddr.Ip4TnlAddr ==
                         pNetIpv4RtInfo1->u4DestNet))
                {
                    /* TnlDestAddr is not reachable. OperSts should be 
                     * made DOWN. */
                    u4IfIndex = pTnlIfNode->u4IfIndex;
                    u1OperSts = CFA_IF_DOWN;

                    CfaGetIfOperStatus (u4IfIndex, &u1IfOperStatus);

                    if (u1IfOperStatus != u1OperSts)
                    {
                        /* Change in OperSts to be handled */
                        CfaInterfaceStatusChangeIndication (u4IfIndex, u1OperSts);
                    }
                }

            }
            pTnlIfNode = NULL;
            TMO_SLL_Scan (&(pTunlCxt->TnlIfSLL), pTnlIfNode, tTnlIfNode *)
            {
                /* Tnl remote addr is known only for configured tunnels 
                 * (GRE/IPV6IP). */
                if ((pTnlIfNode->u4AddrType == ADDR_TYPE_IPV4) &&
                        ((pTnlIfNode->u4EncapsMethod == TNL_TYPE_GRE) ||
                         (pTnlIfNode->u4EncapsMethod == TNL_TYPE_IPV6IP)) &&
                        (pTnlIfNode->RemoteAddr.Ip4TnlAddr ==
                         pNetIpv4RtInfo->u4DestNet))
                {
                    /* TnlDestAddr is reachable. OperSts should be made UP */
                    u4IfIndex = pTnlIfNode->u4IfIndex;
                    u1OperSts = CFA_IF_UP;
                    CfaGetIfOperStatus (u4IfIndex, &u1IfOperStatus);
                    if (u1IfOperStatus != u1OperSts)
                    {
                        /* Change in OperSts to be handled */
                        CfaInterfaceStatusChangeIndication (u4IfIndex, u1OperSts);
                    }
                }
            }
            CFA_TNL_DS_UNLOCK ();
            break;

        default:
            u1OperStChg = FALSE;
            break;
    }

    if (u1OperStChg == TRUE)
    {
        /* Change the OperSts of the interface based on RtChg 
         * (addition/deletion) */
        CfaInterfaceStatusChangeIndication (u4IfIndex, u1OperSts);
    }

    return;
}

/******************************************************************************
 * Function           : CfaTnlMgrNotifyIpv4PMtuChgInCxt
 * Input(s)           : pTnlIfEntry - TnlEntry
 * Input(s)           : 
 *                      i4SdIndex : Socket desriptor identifying a 
 *                                   particular entry in the tunnel table.
 *                      u2Pmtu    : Changed Pmtu value.

 * Output(s)          : None. 
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This function is called from SLI, which inturn will 
 *                      called from FutureIP when ever there is a change in 
 *                      Path Mtu for the tunnel destination.
 ******************************************************************************/
VOID
CfaTnlMgrNotifyIpv4PMtuChgInCxt (UINT4 u4ContextId, INT4 i4SdIndex,
                                 UINT2 u2Pmtu)
{
    tCfaTnlCxt         *pTunlCxtEntry = NULL;
    tTnlIfNode         *pTnlIfNode = NULL;

    CFA_TNL_DS_LOCK ();
    pTunlCxtEntry = CfaUtilGetCxtEntryFromCxtId (u4ContextId);
    if (pTunlCxtEntry == NULL)
    {
        CFA_TNL_DS_UNLOCK ();
        return;
    }

    TMO_SLL_Scan (&(pTunlCxtEntry->TnlIfSLL), pTnlIfNode, tTnlIfNode *)
    {
        if (pTnlIfNode->i4PathMtuSock == i4SdIndex)
        {
            if (pTnlIfNode->u4Mtu != u2Pmtu)
            {
                CfaInterfaceMtuChangeIndication (pTnlIfNode->u4IfIndex, u2Pmtu);
            }
            break;
        }
    }
    CFA_TNL_DS_UNLOCK ();
    return;
}

/******************************************************************************
 * Function           : CfaTnlGetFirstEntryInCxt
 * Input(s)           : pTunlCxt - Tunnel Context
 * Output(s)          : pi4FsTunlIfAddressType - IPv4/IPv6
 *                      pFsTunlIfLocalInetAddress - Source Address
 *                      pFsTunlIfRemoteInetAddress - Dest Address
 *                      pi4FsTunlIfEncapsMethod - Encapsulation Method
 *                      pi4FsTunlIfConfigID - Config Id of the tunnel
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This fetches the first entry of the tunnel table. 
 ******************************************************************************/

INT4
CfaTnlGetFirstEntryInCxt (tCfaTnlCxt * pTunlCxt,
                          INT4 *pi4FsTunlIfAddressType,
                          tSNMP_OCTET_STRING_TYPE *
                          pFsTunlIfLocalInetAddress,
                          tSNMP_OCTET_STRING_TYPE *
                          pFsTunlIfRemoteInetAddress,
                          INT4 *pi4FsTunlIfEncapsMethod,
                          INT4 *pi4FsTunlIfConfigID)
{

    tTnlIfNode         *pTnlIfNode = NULL;

    CFA_TNL_DS_LOCK ();

    pTnlIfNode = (tTnlIfNode *) TMO_SLL_First (&(pTunlCxt->TnlIfSLL));

    if (pTnlIfNode != NULL)
    {
        *pi4FsTunlIfAddressType = (INT4) pTnlIfNode->u4AddrType;
        if (*pi4FsTunlIfAddressType == ADDR_TYPE_IPV4)
        {
            pFsTunlIfLocalInetAddress->i4_Length = sizeof (UINT4);
            PTR_ASSIGN4 (pFsTunlIfLocalInetAddress->pu1_OctetList,
                         pTnlIfNode->LocalAddr.Ip4TnlAddr);
            pFsTunlIfRemoteInetAddress->i4_Length = sizeof (UINT4);
            PTR_ASSIGN4 (pFsTunlIfRemoteInetAddress->pu1_OctetList,
                         pTnlIfNode->RemoteAddr.Ip4TnlAddr);
        }
        else if (*pi4FsTunlIfAddressType == ADDR_TYPE_IPV6)
        {
            pFsTunlIfLocalInetAddress->i4_Length = sizeof (tIp6Addr);
            pFsTunlIfLocalInetAddress->pu1_OctetList =
                pTnlIfNode->LocalAddr.Ip6TnlAddr;
            pFsTunlIfRemoteInetAddress->i4_Length = sizeof (tIp6Addr);
            pFsTunlIfRemoteInetAddress->pu1_OctetList =
                pTnlIfNode->RemoteAddr.Ip6TnlAddr;
        }

        *pi4FsTunlIfEncapsMethod = (INT4) pTnlIfNode->u4EncapsMethod;
        *pi4FsTunlIfConfigID = (INT4) pTnlIfNode->u4ConfigId;
        CFA_TNL_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaTnlGetNextEntryInCxt
 * Input(s)           : pTunlCxt - Tunnel Context pointer
 *                      i4FsTunlIfAddressType
 *                      pFsTunlIfLocalInetAddress
 *                      pFsTunlIfRemoteInetAddress
 *                      i4FsTunlIfEncapsMethod
 *                      i4FsTunlIfConfigID
 * Output(s)          : pi4NextFsTunlIfAddressType - IPv4/IPv6
 *                      pNextFsTunlIfLocalInetAddress - Source Address
 *                      pNextFsTunlIfRemoteInetAddress - Dest Address
 *                      pi4NextFsTunlIfEncapsMethod - Encapsulation Method
 *                      pi4NextFsTunlIfConfigID - Config Id of the tunnel
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This fetches the Next entry to the input
 *                      entry from the tunnel table. 
 ******************************************************************************/
INT4
CfaTnlGetNextEntryInCxt (tCfaTnlCxt * pTunlCxt,
                         INT4 i4FsTunlIfAddressType,
                         INT4 *pi4NextFsTunlIfAddressType,
                         tSNMP_OCTET_STRING_TYPE *
                         pFsTunlIfLocalInetAddress,
                         tSNMP_OCTET_STRING_TYPE *
                         pNextFsTunlIfLocalInetAddress,
                         tSNMP_OCTET_STRING_TYPE *
                         pFsTunlIfRemoteInetAddress,
                         tSNMP_OCTET_STRING_TYPE *
                         pNextFsTunlIfRemoteInetAddress,
                         INT4 i4FsTunlIfEncapsMethod,
                         INT4 *pi4NextFsTunlIfEncapsMethod,
                         INT4 i4FsTunlIfConfigID, INT4 *pi4NextFsTunlIfConfigID)
{
    UINT4               u4AddrType;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4DestIpAddr = 0;
    UINT1               au1SrcIp6Addr[16];
    UINT1               au1DestIp6Addr[16];
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;
    tTnlIfNode         *pTnlIfNode = NULL;
    tTnlIfNode         *pNode = NULL;
    tTnlIfNode         *pNextTnlEntry = NULL;

    u4AddrType = (UINT4) i4FsTunlIfAddressType;
    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        PTR_FETCH4 (u4SrcIpAddr, pFsTunlIfLocalInetAddress->pu1_OctetList);
        PTR_FETCH4 (u4DestIpAddr, pFsTunlIfRemoteInetAddress->pu1_OctetList);
    }
    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        MEMCPY (au1SrcIp6Addr, pFsTunlIfLocalInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
        MEMCPY (au1DestIp6Addr, pFsTunlIfRemoteInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
    }
    u4EncapsMethod = (UINT4) i4FsTunlIfEncapsMethod;
    u4ConfigId = (UINT4) i4FsTunlIfConfigID;

    CFA_TNL_DS_LOCK ();
    TMO_SLL_Scan (&(pTunlCxt->TnlIfSLL), pTnlIfNode, tTnlIfNode *)
    {
        if ((pTnlIfNode->u4AddrType == u4AddrType) &&
            (pTnlIfNode->u4EncapsMethod == u4EncapsMethod) &&
            (pTnlIfNode->u4ConfigId == u4ConfigId))
        {
            if (u4AddrType == ADDR_TYPE_IPV4)
            {
                if ((pTnlIfNode->LocalAddr.Ip4TnlAddr == u4SrcIpAddr) &&
                    (pTnlIfNode->RemoteAddr.Ip4TnlAddr == u4DestIpAddr))
                {
                    pNode = pTnlIfNode;
                    break;
                }
            }
            else if (u4AddrType == ADDR_TYPE_IPV6)
            {
                if ((MEMCMP (&(pTnlIfNode->RemoteAddr.Ip6TnlAddr),
                             au1SrcIp6Addr, IP6ADDR_SIZE) == 0) &&
                    (MEMCMP (&(pTnlIfNode->RemoteAddr.Ip6TnlAddr),
                             au1DestIp6Addr, IP6ADDR_SIZE) == 0))
                {
                    pNode = pTnlIfNode;
                    break;
                }
            }
        }
    }

    if (pNode != NULL)
    {
        pNextTnlEntry = (tTnlIfNode *) TMO_SLL_Next (&(pTunlCxt->TnlIfSLL),
                                                     &pNode->Next);
        if (pNextTnlEntry == NULL)
        {
            CFA_TNL_DS_UNLOCK ();
            return CFA_FAILURE;
        }
    }
    else
    {
        CFA_TNL_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    *pi4NextFsTunlIfAddressType = (INT4) pNextTnlEntry->u4AddrType;
    if (*pi4NextFsTunlIfAddressType == ADDR_TYPE_IPV4)
    {
        pNextFsTunlIfLocalInetAddress->i4_Length = sizeof (UINT4);
        PTR_ASSIGN4 (pNextFsTunlIfLocalInetAddress->pu1_OctetList,
                     pNextTnlEntry->LocalAddr.Ip4TnlAddr);
        pNextFsTunlIfRemoteInetAddress->i4_Length = sizeof (UINT4);
        PTR_ASSIGN4 (pNextFsTunlIfRemoteInetAddress->pu1_OctetList,
                     pNextTnlEntry->RemoteAddr.Ip4TnlAddr);
    }
    else if (*pi4NextFsTunlIfAddressType == ADDR_TYPE_IPV6)
    {
        pNextFsTunlIfLocalInetAddress->i4_Length = sizeof (tIp6Addr);
        pNextFsTunlIfLocalInetAddress->pu1_OctetList =
            pNextTnlEntry->LocalAddr.Ip6TnlAddr;
        pNextFsTunlIfRemoteInetAddress->i4_Length = sizeof (tIp6Addr);
        pNextFsTunlIfRemoteInetAddress->pu1_OctetList =
            pNextTnlEntry->RemoteAddr.Ip6TnlAddr;
    }

    *pi4NextFsTunlIfEncapsMethod = (INT4) pNextTnlEntry->u4EncapsMethod;
    *pi4NextFsTunlIfConfigID = (INT4) pNextTnlEntry->u4ConfigId;

    CFA_TNL_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaSnmpGetMatchingAutoTnlEntryInCxt
 * Input(s)           : pTunlCxt - Tunnel Context Pointer     
 *                      u4EncapsMethod - Encapsulation Method 
 *                      pTunlIfLocalAddress - Source Address
 * Output(s)          : pNode - pointer to the matching tunnel node
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This returns the tunnel node matching the src address
 *                      and encapsulation method 
 ******************************************************************************/
INT4
CfaSnmpGetMatchingAutoTnlEntryInCxt (tCfaTnlCxt * pTunlCxt,
                                     UINT4 u4EncapsMethod,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTunlIfLocalAddress, tTnlIfNode * pNode)
{
    tTnlIfNode         *pTnlIfNode = NULL;
    UINT4               u4AddrType;
    UINT1               au1SrcIp6Addr[16];
    UINT4               u4SrcIpAddr;
    /* EncapType may be ISATAP, COMPAT or 6TO4.
     * A router can have only one tunnel entry for encap types :
     * COMPAT, 6to4 . For encap type ISATAP, we shall have multiple
     * tunnel entries. But the src should be different for each encap type */

    CFA_TNL_DS_LOCK ();
    TMO_SLL_Scan (&(pTunlCxt->TnlIfSLL), pTnlIfNode, tTnlIfNode *)
    {
        if ((u4EncapsMethod != TNL_TYPE_ISATAP) &&
            (pTnlIfNode->u4EncapsMethod == u4EncapsMethod))
        {
            break;
        }
        else
        {
            u4AddrType = pTnlIfNode->u4AddrType;
            if (u4AddrType == ADDR_TYPE_IPV4)
            {
                PTR_FETCH4 (u4SrcIpAddr, pTunlIfLocalAddress->pu1_OctetList);

                if (pTnlIfNode->LocalAddr.Ip4TnlAddr == u4SrcIpAddr)
                {
                    break;
                }
            }
            else if (u4AddrType == ADDR_TYPE_IPV6)
            {
                MEMCPY (au1SrcIp6Addr,
                        pTunlIfLocalAddress->pu1_OctetList,
                        pTunlIfLocalAddress->i4_Length);
                if (MEMCMP (pTnlIfNode->LocalAddr.Ip6TnlAddr,
                            au1SrcIp6Addr, sizeof (au1SrcIp6Addr)) == 0)
                {
                    break;
                }
            }
        }
    }
    if (pTnlIfNode != NULL)
    {
        MEMCPY (pNode, pTnlIfNode, sizeof (tTnlIfNode));
        CFA_TNL_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaTnlCheckSourceAddrInCxt
 * Input(s)           : pTunlCxt - Tunnel Context Pointer    
 *                      pTunlIfLocalAddress - Source Address
 * Output(s)          : None
 * Returns            : pointer to the matching tunnel node
 * Action             : This returns the tunnel node matching the src address 
 ******************************************************************************/
tTnlIfNode         *
CfaTnlCheckSourceAddrInCxt (tCfaTnlCxt * pTunlCxt,
                            tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress)
{
    tTnlIfNode         *pTnlIfEntry = NULL;
    UINT1               au1SrcIp6Addr[16];
    UINT4               u4SrcIpAddr;

    CFA_TNL_DS_LOCK ();
    TMO_SLL_Scan (&(pTunlCxt->TnlIfSLL), pTnlIfEntry, tTnlIfNode *)
    {
        if ((pTnlIfEntry->u4EncapsMethod == TNL_TYPE_ISATAP) ||
            (pTnlIfEntry->u4EncapsMethod == TNL_TYPE_COMPAT) ||
#ifdef OPENFLOW_WANTED
            (pTnlIfEntry->u4EncapsMethod == TNL_TYPE_OPENFLOW) ||
#endif /* OPENFLOW_WANTED */
            (pTnlIfEntry->u4EncapsMethod == TNL_TYPE_SIXTOFOUR))
        {
            if (pTnlIfEntry->u4AddrType == ADDR_TYPE_IPV4)
            {
                PTR_FETCH4 (u4SrcIpAddr,
                            pFsTunlIfLocalInetAddress->pu1_OctetList);
                if (pTnlIfEntry->LocalAddr.Ip4TnlAddr == u4SrcIpAddr)
                {
                    break;
                }
            }
            else if (pTnlIfEntry->u4AddrType == ADDR_TYPE_IPV6)
            {
                MEMCPY (au1SrcIp6Addr,
                        pFsTunlIfLocalInetAddress->
                        pu1_OctetList, pFsTunlIfLocalInetAddress->i4_Length);
                if (MEMCMP (pTnlIfEntry->LocalAddr.Ip6TnlAddr,
                            au1SrcIp6Addr, sizeof (au1SrcIp6Addr)) == 0)
                {
                    break;
                }
            }
        }
    }
    CFA_TNL_DS_UNLOCK ();
    return (pTnlIfEntry);
}

/******************************************************************************
 * Function           : CfaSetIfTnlEntry
 * Input(s)           : u4Index - If Table Index
 *                      pTnlIfNode - pointer to be set  
 * Output(s)          : None
 * Returns            : None
 * Action             : This sets the If Table Tnl pointer variable 
 ******************************************************************************/
VOID
CfaSetIfTnlEntry (UINT4 u4Index, tTnlIfNode * pTnlIfNode)
{

    CFA_TNL_DS_LOCK ();
    CFA_TNL_ENTRY (u4Index) = pTnlIfNode;
    CFA_TNL_DS_UNLOCK ();
}

/******************************************************************************
 * Function           : CfaGetIfTnlEntry
 * Input(s)           : u4Index - If Table Index
 * Output(s)          : None
 * Returns            : pointer to the tunnel node 
 * Action             : This gets the If Table Tnl pointer variable 
 ******************************************************************************/
tTnlIfNode         *
CfaGetIfTnlEntry (UINT4 u4Index)
{
    tTnlIfNode         *pNode;

    CFA_TNL_DS_LOCK ();
    pNode = CFA_TNL_ENTRY (u4Index);
    CFA_TNL_DS_UNLOCK ();
    return (pNode);
}

/******************************************************************************
 * Function           : CfaSetTnlIfAliasInCxt
 * Input(s)           : pTunlCxt - Tunnel Context Pointer    
 *                   :  i4FsTunlIfAddressType - IPv4/IPv6
 *                      pFsTunlIfLocalInetAddress - Source Address
 *                      pFsTunlIfRemoteInetAddress - Dest Address
 *                      i4FsTunlIfEncapsMethod - Encapsulation Method
 *                      i4FsTunlIfConfigID - Config Id of the tunnel
 *                      pSetValFsTunlIfAlias - Tunnel Alias
 * Output(s)          : None                      
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This configures the tunnel alias. 
 ******************************************************************************/
INT4
CfaSetTnlIfAliasInCxt (tCfaTnlCxt * pTunlCxt, INT4 i4FsTunlIfAddressType,
                       tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                       tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                       INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                       tSNMP_OCTET_STRING_TYPE * pSetValFsTunlIfAlias)
{

    UINT4               u4AddrType;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4DestIpAddr = 0;
    UINT1               au1SrcIp6Addr[16];
    UINT1               au1DestIp6Addr[16];
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;
    tTnlIfNode         *pTnlIfNode = NULL;

    u4AddrType = (UINT4) i4FsTunlIfAddressType;
    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        PTR_FETCH4 (u4SrcIpAddr, pFsTunlIfLocalInetAddress->pu1_OctetList);
        PTR_FETCH4 (u4DestIpAddr, pFsTunlIfRemoteInetAddress->pu1_OctetList);
    }
    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        MEMCPY (au1SrcIp6Addr, pFsTunlIfLocalInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
        MEMCPY (au1DestIp6Addr, pFsTunlIfRemoteInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
    }
    u4EncapsMethod = (UINT4) i4FsTunlIfEncapsMethod;
    u4ConfigId = (UINT4) i4FsTunlIfConfigID;

    CFA_TNL_DS_LOCK ();
    pTnlIfNode = CfaGetTnlNodeInCxt (pTunlCxt, u4AddrType, u4SrcIpAddr,
                                     u4DestIpAddr, au1SrcIp6Addr,
                                     au1DestIp6Addr, u4EncapsMethod,
                                     u4ConfigId);

    if (pTnlIfNode != NULL)
    {
        MEMSET (pTnlIfNode->au1IfAlias, 0, CFA_MAX_PORT_NAME_LENGTH);
        MEMCPY (pTnlIfNode->au1IfAlias, pSetValFsTunlIfAlias->pu1_OctetList,
                pSetValFsTunlIfAlias->i4_Length);
        CFA_TNL_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaSetTnlIfIndexInCxt
 * Input(s)           : pTunlCxt - Tunnel Context Pointer    
 *                     i4FsTunlIfAddressType - IPv4/IPv6
 *                      pFsTunlIfLocalInetAddress - Source Address
 *                      pFsTunlIfRemoteInetAddress - Dest Address
 *                      i4FsTunlIfEncapsMethod - Encapsulation Method
 *                      i4FsTunlIfConfigID - Config Id of the tunnel
 *                      u4Index - Tunnel Index
 * Output(s)           : None
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This configures the tunnel If Index. 
 ******************************************************************************/
INT4
CfaSetTnlIfIndexInCxt (tCfaTnlCxt * pTunlCxt,
                       INT4 i4FsTunlIfAddressType,
                       tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                       tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                       INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                       UINT4 u4Index)
{

    UINT4               u4AddrType;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4DestIpAddr = 0;
    UINT1               au1SrcIp6Addr[16];
    UINT1               au1DestIp6Addr[16];
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;
    tTnlIfNode         *pTnlIfNode = NULL;

    u4AddrType = (UINT4) i4FsTunlIfAddressType;
    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        PTR_FETCH4 (u4SrcIpAddr, pFsTunlIfLocalInetAddress->pu1_OctetList);
        PTR_FETCH4 (u4DestIpAddr, pFsTunlIfRemoteInetAddress->pu1_OctetList);
    }
    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        MEMCPY (au1SrcIp6Addr, pFsTunlIfLocalInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
        MEMCPY (au1DestIp6Addr, pFsTunlIfRemoteInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
    }
    u4EncapsMethod = (UINT4) i4FsTunlIfEncapsMethod;
    u4ConfigId = (UINT4) i4FsTunlIfConfigID;

    CFA_TNL_DS_LOCK ();
    pTnlIfNode = CfaGetTnlNodeInCxt (pTunlCxt, u4AddrType, u4SrcIpAddr,
                                     u4DestIpAddr, au1SrcIp6Addr,
                                     au1DestIp6Addr, u4EncapsMethod,
                                     u4ConfigId);

    if (pTnlIfNode != NULL)
    {
        pTnlIfNode->u4IfIndex = u4Index;
        CFA_TNL_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaSetTnlIfHopLimitInCxt
 * Input(s)           : pTunlCxt - Tunnel Context Pointer    
 *                     i4FsTunlIfAddressType - IPv4/IPv6
 *                      pFsTunlIfLocalInetAddress - Source Address
 *                      pFsTunlIfRemoteInetAddress - Dest Address
 *                      i4FsTunlIfEncapsMethod - Encapsulation Method
 *                      i4FsTunlIfConfigID - Config Id of the tunnel
 *                      i4SetValFsTunlIfHopLimit - Tunnel HopLimit
 * Output(s)           : None
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This configures the tunnel If HopLimit. 
 ******************************************************************************/
INT4
CfaSetTnlIfHopLimitInCxt (tCfaTnlCxt * pTunlCxt, INT4 i4FsTunlIfAddressType,
                          tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                          tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                          INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                          INT4 i4SetValFsTunlIfHopLimit)
{
    UINT4               u4AddrType;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4DestIpAddr = 0;
    UINT1               au1SrcIp6Addr[16];
    UINT1               au1DestIp6Addr[16];
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;
    tTnlIfNode         *pTnlIfNode = NULL;
    tCfaRegInfo         CfaInfo;

    u4AddrType = (UINT4) i4FsTunlIfAddressType;
    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        PTR_FETCH4 (u4SrcIpAddr, pFsTunlIfLocalInetAddress->pu1_OctetList);
        PTR_FETCH4 (u4DestIpAddr, pFsTunlIfRemoteInetAddress->pu1_OctetList);
    }
    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        MEMCPY (au1SrcIp6Addr, pFsTunlIfLocalInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
        MEMCPY (au1DestIp6Addr, pFsTunlIfRemoteInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
    }
    u4EncapsMethod = (UINT4) i4FsTunlIfEncapsMethod;
    u4ConfigId = (UINT4) i4FsTunlIfConfigID;

    CFA_TNL_DS_LOCK ();
    pTnlIfNode = CfaGetTnlNodeInCxt (pTunlCxt, u4AddrType, u4SrcIpAddr,
                                     u4DestIpAddr, au1SrcIp6Addr,
                                     au1DestIp6Addr, u4EncapsMethod,
                                     u4ConfigId);

    if (pTnlIfNode != NULL)
    {
        pTnlIfNode->u2HopLimit = (UINT2) i4SetValFsTunlIfHopLimit;
        /* Notify tunnel interface parameter updation to all registered HLs */
        MEMSET (&CfaInfo, 0, sizeof (tCfaRegInfo));
        CfaInfo.u4IfIndex = pTnlIfNode->u4IfIndex;
        CfaInfo.CfaTnlIfInfo.u4TnlMask = TNL_HOP_LIMIT_CHG;
        CfaInfo.CfaTnlIfInfo.u4TnlHopLimit = pTnlIfNode->u2HopLimit;
        CFA_TNL_DS_UNLOCK ();
        CfaNotifyTnlIfUpdate (&CfaInfo);
        return CFA_SUCCESS;
    }
    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaSetTnlIfTOSInCxt
 * Input(s)           : pTunlCxt - Tunnel Context Pointer    
 *                     i4FsTunlIfAddressType - IPv4/IPv6
 *                      pFsTunlIfLocalInetAddress - Source Address
 *                      pFsTunlIfRemoteInetAddress - Dest Address
 *                      i4FsTunlIfEncapsMethod - Encapsulation Method
 *                      i4FsTunlIfConfigID - Config Id of the tunnel
 *                      i4SetValFsTunlIfTOS - Tunnel Type of Service
 * Output(s)           : None
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This configures the tunnel If TOS. 
 ******************************************************************************/
INT4
CfaSetTnlIfTOSInCxt (tCfaTnlCxt * pTunlCxt, INT4 i4FsTunlIfAddressType,
                     tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                     tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                     INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                     INT4 i4SetValFsTunlIfTOS)
{
    UINT4               u4AddrType;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4DestIpAddr = 0;
    UINT1               au1SrcIp6Addr[16];
    UINT1               au1DestIp6Addr[16];
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;
    tTnlIfNode         *pTnlIfNode = NULL;

    u4AddrType = (UINT4) i4FsTunlIfAddressType;
    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        PTR_FETCH4 (u4SrcIpAddr, pFsTunlIfLocalInetAddress->pu1_OctetList);
        PTR_FETCH4 (u4DestIpAddr, pFsTunlIfRemoteInetAddress->pu1_OctetList);
    }
    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        MEMCPY (au1SrcIp6Addr, pFsTunlIfLocalInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
        MEMCPY (au1DestIp6Addr, pFsTunlIfRemoteInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
    }
    u4EncapsMethod = (UINT4) i4FsTunlIfEncapsMethod;
    u4ConfigId = (UINT4) i4FsTunlIfConfigID;

    CFA_TNL_DS_LOCK ();
    pTnlIfNode = CfaGetTnlNodeInCxt (pTunlCxt, u4AddrType, u4SrcIpAddr,
                                     u4DestIpAddr, au1SrcIp6Addr,
                                     au1DestIp6Addr, u4EncapsMethod,
                                     u4ConfigId);

    if (pTnlIfNode != NULL)
    {
        pTnlIfNode->i2TOS = (INT2) i4SetValFsTunlIfTOS;
        CFA_TNL_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaSetTnlIfFlowLabelInCxt
 * Input(s)           : pTunlCxt - Tunnel Context Pointer    
 *                     i4FsTunlIfAddressType - IPv4/IPv6
 *                      pFsTunlIfLocalInetAddress - Source Address
 *                      pFsTunlIfRemoteInetAddress - Dest Address
 *                      i4FsTunlIfEncapsMethod - Encapsulation Method
 *                      i4FsTunlIfConfigID - Config Id of the tunnel
 *                      i4SetValFsTunlIfFlowLabel - Tunnel FlowLabel
 * Output(s)           : None
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This configures the tunnel If FlowLabel. 
 ******************************************************************************/
INT4
CfaSetTnlIfFlowLabelInCxt (tCfaTnlCxt * pTunlCxt, INT4 i4FsTunlIfAddressType,
                           tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                           tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                           INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                           INT4 i4SetValFsTunlIfFlowLabel)
{
    UINT4               u4AddrType;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4DestIpAddr = 0;
    UINT1               au1SrcIp6Addr[16];
    UINT1               au1DestIp6Addr[16];
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;
    tTnlIfNode         *pTnlIfNode = NULL;

    u4AddrType = (UINT4) i4FsTunlIfAddressType;
    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        PTR_FETCH4 (u4SrcIpAddr, pFsTunlIfLocalInetAddress->pu1_OctetList);
        PTR_FETCH4 (u4DestIpAddr, pFsTunlIfRemoteInetAddress->pu1_OctetList);
    }
    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        MEMCPY (au1SrcIp6Addr, pFsTunlIfLocalInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
        MEMCPY (au1DestIp6Addr, pFsTunlIfRemoteInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
    }
    u4EncapsMethod = (UINT4) i4FsTunlIfEncapsMethod;
    u4ConfigId = (UINT4) i4FsTunlIfConfigID;

    CFA_TNL_DS_LOCK ();
    pTnlIfNode = CfaGetTnlNodeInCxt (pTunlCxt, u4AddrType, u4SrcIpAddr,
                                     u4DestIpAddr, au1SrcIp6Addr,
                                     au1DestIp6Addr, u4EncapsMethod,
                                     u4ConfigId);

    if (pTnlIfNode != NULL)
    {
        pTnlIfNode->i4FlowLabel = i4SetValFsTunlIfFlowLabel;
        CFA_TNL_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaSetTnlIfDirFlagInCxt
 * Input(s)           : pTunlCxt - Tunnel Context Pointer    
 *                     i4FsTunlIfAddressType - IPv4/IPv6
 *                      pFsTunlIfLocalInetAddress - Source Address
 *                      pFsTunlIfRemoteInetAddress - Dest Address
 *                      i4FsTunlIfEncapsMethod - Encapsulation Method
 *                      i4FsTunlIfConfigID - Config Id of the tunnel
 *                      i4SetValFsTunlIfDirFlag - Tunnel Direction flag
 * Output(s)           : None
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This configures the tunnel direction flag. 
 ******************************************************************************/
INT4
CfaSetTnlIfDirFlagInCxt (tCfaTnlCxt * pTunlCxt, INT4 i4FsTunlIfAddressType,
                         tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                         tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                         INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                         INT4 i4SetValFsTunlIfDirFlag)
{
    UINT4               u4AddrType;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4DestIpAddr = 0;
    UINT1               au1SrcIp6Addr[16];
    UINT1               au1DestIp6Addr[16];
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;
    tTnlIfNode         *pTnlIfNode = NULL;
    tCfaRegInfo         CfaInfo;

    u4AddrType = (UINT4) i4FsTunlIfAddressType;
    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        PTR_FETCH4 (u4SrcIpAddr, pFsTunlIfLocalInetAddress->pu1_OctetList);
        PTR_FETCH4 (u4DestIpAddr, pFsTunlIfRemoteInetAddress->pu1_OctetList);
    }
    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        MEMCPY (au1SrcIp6Addr, pFsTunlIfLocalInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
        MEMCPY (au1DestIp6Addr, pFsTunlIfRemoteInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
    }
    u4EncapsMethod = (UINT4) i4FsTunlIfEncapsMethod;
    u4ConfigId = (UINT4) i4FsTunlIfConfigID;

    CFA_TNL_DS_LOCK ();
    pTnlIfNode = CfaGetTnlNodeInCxt (pTunlCxt, u4AddrType, u4SrcIpAddr,
                                     u4DestIpAddr, au1SrcIp6Addr,
                                     au1DestIp6Addr, u4EncapsMethod,
                                     u4ConfigId);

    if (pTnlIfNode != NULL)
    {
        pTnlIfNode->u1DirFlag = (UINT1) i4SetValFsTunlIfDirFlag;
        /* Notify tunnel interface parameter updation to all registered HLs */
        MEMSET (&CfaInfo, 0, sizeof (tCfaRegInfo));
        CfaInfo.u4IfIndex = pTnlIfNode->u4IfIndex;
        CfaInfo.CfaTnlIfInfo.u4TnlMask = TNL_DIR_FLAG_CHG;
        CfaInfo.CfaTnlIfInfo.u4TnlDirFlag = pTnlIfNode->u1DirFlag;
        CFA_TNL_DS_UNLOCK ();
        CfaNotifyTnlIfUpdate (&CfaInfo);
        return CFA_SUCCESS;
    }
    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaSetTnlIfDirectionInCxt
 * Input(s)           : pTunlCxt - Tunnel Context Pointer    
 *                     i4FsTunlIfAddressType - IPv4/IPv6
 *                      pFsTunlIfLocalInetAddress - Source Address
 *                      pFsTunlIfRemoteInetAddress - Dest Address
 *                      i4FsTunlIfEncapsMethod - Encapsulation Method
 *                      i4FsTunlIfConfigID - Config Id of the tunnel
 *                      i4SetValFsTunlIfDirection - Tunnel Direction 
 * Output(s)           : None
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This configures the tunnel direction. 
 ******************************************************************************/
INT4
CfaSetTnlIfDirectionInCxt (tCfaTnlCxt * pTunlCxt, INT4 i4FsTunlIfAddressType,
                           tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                           tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                           INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                           INT4 i4SetValFsTunlIfDirection)
{
    UINT4               u4AddrType;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4DestIpAddr = 0;
    UINT1               au1SrcIp6Addr[16];
    UINT1               au1DestIp6Addr[16];
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;
    tTnlIfNode         *pTnlIfNode = NULL;
    tCfaRegInfo         CfaInfo;

    u4AddrType = (UINT4) i4FsTunlIfAddressType;
    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        PTR_FETCH4 (u4SrcIpAddr, pFsTunlIfLocalInetAddress->pu1_OctetList);
        PTR_FETCH4 (u4DestIpAddr, pFsTunlIfRemoteInetAddress->pu1_OctetList);
    }
    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        MEMCPY (au1SrcIp6Addr, pFsTunlIfLocalInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
        MEMCPY (au1DestIp6Addr, pFsTunlIfRemoteInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
    }
    u4EncapsMethod = (UINT4) i4FsTunlIfEncapsMethod;
    u4ConfigId = (UINT4) i4FsTunlIfConfigID;

    CFA_TNL_DS_LOCK ();
    pTnlIfNode = CfaGetTnlNodeInCxt (pTunlCxt, u4AddrType, u4SrcIpAddr,
                                     u4DestIpAddr, au1SrcIp6Addr,
                                     au1DestIp6Addr, u4EncapsMethod,
                                     u4ConfigId);

    if (pTnlIfNode != NULL)
    {
        pTnlIfNode->u1Direction = (UINT1) i4SetValFsTunlIfDirection;
        /* Notify tunnel interface parameter updation to all registered HLs */
        MEMSET (&CfaInfo, 0, sizeof (tCfaRegInfo));
        CfaInfo.u4IfIndex = pTnlIfNode->u4IfIndex;
        CfaInfo.CfaTnlIfInfo.u4TnlMask = TNL_DIRECTION_CHG;
        CfaInfo.CfaTnlIfInfo.u4TnlDir = pTnlIfNode->u1Direction;
        CFA_TNL_DS_UNLOCK ();
        CfaNotifyTnlIfUpdate (&CfaInfo);
        return CFA_SUCCESS;
    }
    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaSetTnlIfEncapLimitInCxt
 * Input(s)           : pTunlCxt - Tunnel Context Pointer    
 *                     i4FsTunlIfAddressType - IPv4/IPv6
 *                      pFsTunlIfLocalInetAddress - Source Address
 *                      pFsTunlIfRemoteInetAddress - Dest Address
 *                      i4FsTunlIfEncapsMethod - Encapsulation Method
 *                      i4FsTunlIfConfigID - Config Id of the tunnel
 *                      u4SetValFsTunlIfEncaplmt - Tunnel Encapsulation limit 
 * Output(s)           : None
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This configures the tunnel Encapsulation limit. 
 ******************************************************************************/
INT4
CfaSetTnlIfEncapLimitInCxt (tCfaTnlCxt * pTunlCxt, INT4 i4FsTunlIfAddressType,
                            tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                            tSNMP_OCTET_STRING_TYPE *
                            pFsTunlIfRemoteInetAddress,
                            INT4 i4FsTunlIfEncapsMethod,
                            INT4 i4FsTunlIfConfigID,
                            UINT4 u4SetValFsTunlIfEncaplmt)
{
    UINT4               u4AddrType;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4DestIpAddr = 0;
    UINT1               au1SrcIp6Addr[16];
    UINT1               au1DestIp6Addr[16];
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;
    tTnlIfNode         *pTnlIfNode = NULL;
    tCfaRegInfo         CfaInfo;

    u4AddrType = (UINT4) i4FsTunlIfAddressType;
    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        PTR_FETCH4 (u4SrcIpAddr, pFsTunlIfLocalInetAddress->pu1_OctetList);
        PTR_FETCH4 (u4DestIpAddr, pFsTunlIfRemoteInetAddress->pu1_OctetList);
    }
    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        MEMCPY (au1SrcIp6Addr, pFsTunlIfLocalInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
        MEMCPY (au1DestIp6Addr, pFsTunlIfRemoteInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
    }
    u4EncapsMethod = (UINT4) i4FsTunlIfEncapsMethod;
    u4ConfigId = (UINT4) i4FsTunlIfConfigID;

    CFA_TNL_DS_LOCK ();
    pTnlIfNode = CfaGetTnlNodeInCxt (pTunlCxt, u4AddrType, u4SrcIpAddr,
                                     u4DestIpAddr, au1SrcIp6Addr,
                                     au1DestIp6Addr, u4EncapsMethod,
                                     u4ConfigId);

    if (pTnlIfNode != NULL)
    {
        pTnlIfNode->u4EncapLmt = u4SetValFsTunlIfEncaplmt;
        /* Notify tunnel interface parameter updation to all registered HLs */
        MEMSET (&CfaInfo, 0, sizeof (tCfaRegInfo));
        CfaInfo.u4IfIndex = pTnlIfNode->u4IfIndex;
        CfaInfo.CfaTnlIfInfo.u4TnlMask = TNL_ENCAP_LIMIT_CHG;
        CfaInfo.CfaTnlIfInfo.u4TnlEncapLmt = pTnlIfNode->u1EncapOption;
        CFA_TNL_DS_UNLOCK ();
        CfaNotifyTnlIfUpdate (&CfaInfo);
        return CFA_SUCCESS;
    }
    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaSetTnlIfEncapOptionInCxt
 * Input(s)           : pTunlCxt - Tunnel Context Pointer    
 *                     i4FsTunlIfAddressType - IPv4/IPv6
 *                      pFsTunlIfLocalInetAddress - Source Address
 *                      pFsTunlIfRemoteInetAddress - Dest Address
 *                      i4FsTunlIfEncapsMethod - Encapsulation Method
 *                      i4FsTunlIfConfigID - Config Id of the tunnel
 *                      i4SetValFsTunlIfEncapOption - Tunnel Encapsulation 
 *                          option 
 * Output(s)           : None
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This configures the tunnel Encapsulation option. 
 ******************************************************************************/
INT4
CfaSetTnlIfEncapOptionInCxt (tCfaTnlCxt * pTunlCxt, INT4 i4FsTunlIfAddressType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsTunlIfLocalInetAddress,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsTunlIfRemoteInetAddress,
                             INT4 i4FsTunlIfEncapsMethod,
                             INT4 i4FsTunlIfConfigID,
                             INT4 i4SetValFsTunlIfEncapOption)
{
    UINT4               u4AddrType;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4DestIpAddr = 0;
    UINT1               au1SrcIp6Addr[16];
    UINT1               au1DestIp6Addr[16];
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;
    tTnlIfNode         *pTnlIfNode = NULL;
    tCfaRegInfo         CfaInfo;

    u4AddrType = (UINT4) i4FsTunlIfAddressType;
    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        PTR_FETCH4 (u4SrcIpAddr, pFsTunlIfLocalInetAddress->pu1_OctetList);
        PTR_FETCH4 (u4DestIpAddr, pFsTunlIfRemoteInetAddress->pu1_OctetList);
    }
    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        MEMCPY (au1SrcIp6Addr, pFsTunlIfLocalInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
        MEMCPY (au1DestIp6Addr, pFsTunlIfRemoteInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
    }
    u4EncapsMethod = (UINT4) i4FsTunlIfEncapsMethod;
    u4ConfigId = (UINT4) i4FsTunlIfConfigID;

    CFA_TNL_DS_LOCK ();
    pTnlIfNode = CfaGetTnlNodeInCxt (pTunlCxt, u4AddrType, u4SrcIpAddr,
                                     u4DestIpAddr, au1SrcIp6Addr,
                                     au1DestIp6Addr, u4EncapsMethod,
                                     u4ConfigId);

    if (pTnlIfNode != NULL)
    {
        pTnlIfNode->u1EncapOption = (UINT1) i4SetValFsTunlIfEncapOption;
        /* Notify tunnel interface parameter updation to all registered HLs */
        MEMSET (&CfaInfo, 0, sizeof (tCfaRegInfo));
        CfaInfo.u4IfIndex = pTnlIfNode->u4IfIndex;
        CfaInfo.CfaTnlIfInfo.u4TnlMask = TNL_ENCAP_OPT_CHG;
        CfaInfo.CfaTnlIfInfo.u4TnlEncapOpt = pTnlIfNode->u1EncapOption;
        CFA_TNL_DS_UNLOCK ();
        CfaNotifyTnlIfUpdate (&CfaInfo);
        return CFA_SUCCESS;
    }
    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaSetTnlIfCksumFlagInCxt
 * Input(s)           : pTunlCxt - Tunnel Context Pointer    
 *                     i4FsTunlIfAddressType - IPv4/IPv6
 *                      pFsTunlIfLocalInetAddress - Source Address
 *                      pFsTunlIfRemoteInetAddress - Dest Address
 *                      i4FsTunlIfEncapsMethod - Encapsulation Method
 *                      i4FsTunlIfConfigID - Config Id of the tunnel
 *                      i4SetValFsTunlIfCksumFlag - Tunnel Checksum flag
 * Output(s)           : None
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This configures the tunnel Checksum flag.
 ******************************************************************************/
INT4
CfaSetTnlIfCksumFlagInCxt (tCfaTnlCxt * pTunlCxt, INT4 i4FsTunlIfAddressType,
                           tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                           tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                           INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                           INT4 i4SetValFsTunlIfCksumFlag)
{
    UINT4               u4AddrType;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4DestIpAddr = 0;
    UINT1               au1SrcIp6Addr[16];
    UINT1               au1DestIp6Addr[16];
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;
    tTnlIfNode         *pTnlIfNode = NULL;

    u4AddrType = (UINT4) i4FsTunlIfAddressType;
    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        PTR_FETCH4 (u4SrcIpAddr, pFsTunlIfLocalInetAddress->pu1_OctetList);
        PTR_FETCH4 (u4DestIpAddr, pFsTunlIfRemoteInetAddress->pu1_OctetList);
    }
    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        MEMCPY (au1SrcIp6Addr, pFsTunlIfLocalInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
        MEMCPY (au1DestIp6Addr, pFsTunlIfRemoteInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
    }
    u4EncapsMethod = (UINT4) i4FsTunlIfEncapsMethod;
    u4ConfigId = (UINT4) i4FsTunlIfConfigID;

    CFA_TNL_DS_LOCK ();
    pTnlIfNode = CfaGetTnlNodeInCxt (pTunlCxt, u4AddrType, u4SrcIpAddr,
                                     u4DestIpAddr, au1SrcIp6Addr,
                                     au1DestIp6Addr, u4EncapsMethod,
                                     u4ConfigId);

    if (pTnlIfNode != NULL)
    {
        pTnlIfNode->bCksumFlag = (UINT1) i4SetValFsTunlIfCksumFlag;
        CFA_TNL_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaSetTnlIfPmtuFlagInCxt
 * Input(s)           : pTunlCxt - Tunnel Context Pointer    
 *                     i4FsTunlIfAddressType - IPv4/IPv6
 *                      pFsTunlIfLocalInetAddress - Source Address
 *                      pFsTunlIfRemoteInetAddress - Dest Address
 *                      i4FsTunlIfEncapsMethod - Encapsulation Method
 *                      i4FsTunlIfConfigID - Config Id of the tunnel
 *                      i4SetValFsTunlIfPmtuFlag - Tunnel PMTU flag
 * Output(s)           : None
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This configures the tunnel PMTU flag.
 ******************************************************************************/
INT4
CfaSetTnlIfPmtuFlagInCxt (tCfaTnlCxt * pTunlCxt, INT4 i4FsTunlIfAddressType,
                          tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                          tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                          INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                          INT4 i4SetValFsTunlIfPmtuFlag)
{
    UINT4               u4AddrType;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4DestIpAddr = 0;
    UINT1               au1SrcIp6Addr[16];
    UINT1               au1DestIp6Addr[16];
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;
    UINT4               u4Index = 0;
    UINT4               u4Mtu = 0;
    tTnlIfNode         *pTnlIfNode = NULL;
    tCfaIfInfo          CfaIfInfo;

    u4AddrType = (UINT4) i4FsTunlIfAddressType;
    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        PTR_FETCH4 (u4SrcIpAddr, pFsTunlIfLocalInetAddress->pu1_OctetList);
        PTR_FETCH4 (u4DestIpAddr, pFsTunlIfRemoteInetAddress->pu1_OctetList);
    }
    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        MEMCPY (au1SrcIp6Addr, pFsTunlIfLocalInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
        MEMCPY (au1DestIp6Addr, pFsTunlIfRemoteInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
    }
    u4EncapsMethod = (UINT4) i4FsTunlIfEncapsMethod;
    u4ConfigId = (UINT4) i4FsTunlIfConfigID;

    CFA_TNL_DS_LOCK ();
    pTnlIfNode = CfaGetTnlNodeInCxt (pTunlCxt, u4AddrType, u4SrcIpAddr,
                                     u4DestIpAddr, au1SrcIp6Addr,
                                     au1DestIp6Addr, u4EncapsMethod,
                                     u4ConfigId);

    if (pTnlIfNode != NULL)
    {
        pTnlIfNode->bPmtuFlag = (UINT1) i4SetValFsTunlIfPmtuFlag;
        if (CfaGetIfInfo (pTnlIfNode->u4PhyIfIndex, &CfaIfInfo) == CFA_FAILURE)
        {
            CFA_TNL_DS_UNLOCK ();
            return CFA_FAILURE;
        }

        /* set path MTU based on the configured flag */
        if (pTnlIfNode->bPmtuFlag == TNL_PMTU_ENABLE)
        {
            /* path-mtu-discovery enabled, disvocer the MTU */
            if (CfaTnlMgrPmtuRegWithV4 (pTnlIfNode) == CFA_FAILURE)
            {
                CFA_TNL_DS_UNLOCK ();
                return CFA_FAILURE;
            }
            CFA_TNL_DS_UNLOCK ();
        }
        else
        {
            /* De-register PMTU Discovery */
            CfaTnlMgrPmtuDeregWithV4 (pTnlIfNode);

            /* path-mtu-discovery disabled, use the outgoing link MTU */
            pTnlIfNode->u4Mtu = CfaIfInfo.u4IfMtu - IP_HDR_LEN;

            u4Index = pTnlIfNode->u4IfIndex;
            u4Mtu = pTnlIfNode->u4Mtu;
            CFA_TNL_DS_UNLOCK ();

            /* Update MTU in ifMainTable, so that notification will be sent
             * to HLs */

            if ((CfaValidateCfaIfIndex (u4Index) != CFA_FAILURE) &&
                (u4Index <= SYS_DEF_MAX_INTERFACES))
            {
                nmhSetIfMainMtu ((INT4) u4Index, (INT4) u4Mtu);
            }
        }
        return CFA_SUCCESS;
    }
    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaSetTnlIfActiveStatusInCxt
 * Input(s)           : pTunlCxt - Tunnel Context Pointer    
 *                     i4FsTunlIfAddressType - IPv4/IPv6
 *                      pFsTunlIfLocalInetAddress - Source Address
 *                      pFsTunlIfRemoteInetAddress - Dest Address
 *                      i4FsTunlIfEncapsMethod - Encapsulation Method
 *                      i4FsTunlIfConfigID - Config Id of the tunnel
 * Output(s)           : None
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This configures the tunnel If Entry to ACTIVE.
 ******************************************************************************/
INT4
CfaSetTnlIfActiveStatusInCxt (tCfaTnlCxt * pTunlCxt, INT4 i4FsTunlIfAddressType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsTunlIfLocalInetAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsTunlIfRemoteInetAddress,
                              INT4 i4FsTunlIfEncapsMethod,
                              INT4 i4FsTunlIfConfigID)
{
    UINT4               u4AddrType;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4DestIpAddr = 0;
    UINT1               au1SrcIp6Addr[16];
    UINT1               au1DestIp6Addr[16];
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;
    UINT4               u4RetVal = SNMP_SUCCESS;
    tTnlIfNode         *pTnlIfNode = NULL;
    tSNMP_OCTET_STRING_TYPE *IfAlias;
    tCfaRegInfo         CfaInfo;

    u4AddrType = (UINT4) i4FsTunlIfAddressType;
    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        PTR_FETCH4 (u4SrcIpAddr, pFsTunlIfLocalInetAddress->pu1_OctetList);
        PTR_FETCH4 (u4DestIpAddr, pFsTunlIfRemoteInetAddress->pu1_OctetList);
    }
    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        MEMCPY (au1SrcIp6Addr, pFsTunlIfLocalInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
        MEMCPY (au1DestIp6Addr, pFsTunlIfRemoteInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
    }
    u4EncapsMethod = (UINT4) i4FsTunlIfEncapsMethod;
    u4ConfigId = (UINT4) i4FsTunlIfConfigID;

    CFA_TNL_DS_LOCK ();
    pTnlIfNode = CfaGetTnlNodeInCxt (pTunlCxt, u4AddrType, u4SrcIpAddr,
                                     u4DestIpAddr, au1SrcIp6Addr,
                                     au1DestIp6Addr, u4EncapsMethod,
                                     u4ConfigId);

    if (pTnlIfNode != NULL)
    {
        if (pTnlIfNode->u1RowStatus == CFA_RS_ACTIVE)
        {
            /* Row status is already active */
            CFA_TNL_DS_UNLOCK ();
            return CFA_SUCCESS;
        }

        if (pTnlIfNode->u1RowStatus == CFA_RS_NOTINSERVICE)
        {
            /* IfAlias is a mandatory arg for activating the row */
            IfAlias = allocmem_octetstring (CFA_MAX_PORT_NAME_LENGTH);
            CFA_TNL_DS_UNLOCK ();

            if (IfAlias == NULL)
            {
                return CFA_FAILURE;
            }

            if (nmhGetFsTunlIfAlias (i4FsTunlIfAddressType,
                                     pFsTunlIfLocalInetAddress,
                                     pFsTunlIfRemoteInetAddress,
                                     i4FsTunlIfEncapsMethod,
                                     i4FsTunlIfConfigID,
                                     IfAlias) != SNMP_SUCCESS)
            {
                u4RetVal = SNMP_FAILURE;
            }

            free_octetstring (IfAlias);

            if (u4RetVal == SNMP_FAILURE)
            {
                return CFA_FAILURE;
            }

            /* Store the tunnel info in gapIfTable */
            CfaSetIfTnlEntry (pTnlIfNode->u4IfIndex, pTnlIfNode);
            CFA_TNL_DS_LOCK ();
            /* Also update the MTU value in gapIfTable table */
            CfaSetIfMtu (pTnlIfNode->u4IfIndex, pTnlIfNode->u4Mtu);

            /* create the stack entry and set the stack status as ACTIVE. */
            if (CfaIfmAddStackEntry (pTnlIfNode->u4IfIndex, CFA_LOW,
                                     pTnlIfNode->u4PhyIfIndex,
                                     CFA_RS_ACTIVE) != CFA_SUCCESS)
            {
                CFA_TNL_DS_UNLOCK ();
                return CFA_FAILURE;
            }

            /* activate the row of TnlTbl */
            pTnlIfNode->u1RowStatus = CFA_RS_ACTIVE;

            /* TnlMgr registers with NetIpV4 for the very first time 
             * to receive the IP tunnel packets */
            if (pTunlCxt->u1TunlIfExists == FALSE)
            {
                CfaTnlMgrRegWithIp4InCxt (pTunlCxt->u4ContextId);
                pTunlCxt->u1TunlIfExists = TRUE;
            }

            /* Notify tunnel interface parameter updation to all 
             * registered HLs */
            MEMSET (&CfaInfo, 0, sizeof (tCfaRegInfo));
            CfaInfo.u4IfIndex = pTnlIfNode->u4IfIndex;
            CfaInfo.CfaTnlIfInfo.u4TnlMask = TNL_DIR_FLAG_CHG |
                TNL_DIRECTION_CHG | TNL_ENCAP_OPT_CHG |
                TNL_ENCAP_LIMIT_CHG | TNL_ENCAP_ADDR_CHG;
            CfaInfo.CfaTnlIfInfo.u4TnlDirFlag = pTnlIfNode->u1DirFlag;
            CfaInfo.CfaTnlIfInfo.u4TnlDir = pTnlIfNode->u1Direction;
            CfaInfo.CfaTnlIfInfo.u4TnlEncapLmt = pTnlIfNode->u4EncapLmt;
            CfaInfo.CfaTnlIfInfo.u4TnlEncapOpt = pTnlIfNode->u1EncapOption;
            CfaInfo.CfaTnlIfInfo.u4LocalAddr = pTnlIfNode->LocalAddr.Ip4TnlAddr;
            CfaInfo.CfaTnlIfInfo.u4RemoteAddr =
                pTnlIfNode->RemoteAddr.Ip4TnlAddr;
            CfaInfo.CfaTnlIfInfo.u4TnlType = pTnlIfNode->u4EncapsMethod;
            CfaInfo.CfaTnlIfInfo.u4TnlHopLimit = pTnlIfNode->u2HopLimit;

            CFA_TNL_DS_UNLOCK ();
            CfaNotifyTnlIfUpdate (&CfaInfo);
#ifdef IP_WANTED
            Ip4TnlIfUpdateNotifyFromCfa (CfaInfo.u4IfIndex,
                                         &CfaInfo.CfaTnlIfInfo);
#endif
            return CFA_SUCCESS;
        }
    }
    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function           : CfaSetTnlIfNotInServiceStatusInCxt
 * Input(s)           : pTunlCxt - Tunnel Context Pointer    
 *                     i4FsTunlIfAddressType - IPv4/IPv6
 *                      pFsTunlIfLocalInetAddress - Source Address
 *                      pFsTunlIfRemoteInetAddress - Dest Address
 *                      i4FsTunlIfEncapsMethod - Encapsulation Method
 *                      i4FsTunlIfConfigID - Config Id of the tunnel
 * Output(s)           : None
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This configures the tunnel If Entry to NOT_IN_SERVICE.
 ******************************************************************************/
INT4
CfaSetTnlIfNotInServiceStatusInCxt (tCfaTnlCxt * pTunlCxt,
                                    INT4 i4FsTunlIfAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsTunlIfLocalInetAddress,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsTunlIfRemoteInetAddress,
                                    INT4 i4FsTunlIfEncapsMethod,
                                    INT4 i4FsTunlIfConfigID)
{
    UINT4               u4AddrType;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4DestIpAddr = 0;
    UINT1               au1SrcIp6Addr[16];
    UINT1               au1DestIp6Addr[16];
    UINT4               u4EncapsMethod;
    UINT4               u4ConfigId;
    tTnlIfNode         *pTnlIfNode = NULL;

    u4AddrType = (UINT4) i4FsTunlIfAddressType;
    if (u4AddrType == ADDR_TYPE_IPV4)
    {
        PTR_FETCH4 (u4SrcIpAddr, pFsTunlIfLocalInetAddress->pu1_OctetList);
        PTR_FETCH4 (u4DestIpAddr, pFsTunlIfRemoteInetAddress->pu1_OctetList);
    }
    else if (u4AddrType == ADDR_TYPE_IPV6)
    {
        MEMCPY (au1SrcIp6Addr, pFsTunlIfLocalInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
        MEMCPY (au1DestIp6Addr, pFsTunlIfRemoteInetAddress->pu1_OctetList,
                pFsTunlIfRemoteInetAddress->i4_Length);
    }
    u4EncapsMethod = (UINT4) i4FsTunlIfEncapsMethod;
    u4ConfigId = (UINT4) i4FsTunlIfConfigID;

    CFA_TNL_DS_LOCK ();
    pTnlIfNode = CfaGetTnlNodeInCxt (pTunlCxt, u4AddrType, u4SrcIpAddr,
                                     u4DestIpAddr, au1SrcIp6Addr,
                                     au1DestIp6Addr, u4EncapsMethod,
                                     u4ConfigId);

    if (pTnlIfNode != NULL)
    {
        pTnlIfNode->u1RowStatus = CFA_RS_NOTINSERVICE;
        CFA_TNL_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    CFA_TNL_DS_UNLOCK ();
    return CFA_FAILURE;
}

/******************************************************************************
 * Function       : CfaCdbGetFirstIvrIfInfo 
 *
 * Description    : This function will return the first valid L3_IPVLAN interface
 *                  index and their related interface parameters 
 *                  1. OperStatus
 *                  2. VlanId
 *                  3. MacAddress.
 *
 * Input(s)       : None.
 *
 * Output(s)      : pu4IfIndex - Interface index of first valid L3_IPVLAN interface.
 *                  pIfInfo - Interface Info structure. 
 *
 * Returns        : CFA_SUCCESS/FAILURE
 *
 ******************************************************************************/

INT4
CfaCdbGetFirstIvrIfInfo (UINT4 *pu4IfIndex, tCfaIfInfo * pIfInfo)
{
    INT4                i4IfValid = CFA_FALSE;
    UINT1               u1DefRouterIdx = CFA_FALSE;
    UINT1               u1IfType = 0;
    UINT4               u4Index;
    UINT4               u4StartIndex = CFA_MIN_IVR_IF_INDEX;
    UINT4               u4EndIfIndex = CFA_MAX_IVR_IF_INDEX;

    CFA_DS_LOCK ();

    u4Index = u4StartIndex;
    CFA_CDB_SCAN (u4Index, u4EndIfIndex, CFA_L3IPVLAN)
    {
        if (CFA_CDB_IS_INTF_VALID (u4Index) == CFA_TRUE)
        {
            *pu4IfIndex = u4Index;
            pIfInfo->u1IfOperStatus = CFA_CDB_IF_OPER (u4Index);
            pIfInfo->u2VlanId = CFA_CDB_IF_VLANID_OF_IVR (u4Index);

            u1IfType = CFA_CDB_IF_TYPE ((UINT4) u4Index);
            CFA_CDB_CHECK_IF_TYPE (u1IfType, u1DefRouterIdx);
            if (CFA_FALSE == u1DefRouterIdx)
            {
                CFA_CDB_GET_ENET_IWF_LOCAL_MAC (u4Index, pIfInfo->au1MacAddr);
            }
            else
            {
                i4IfValid = CFA_CDB_IS_INTF_VALID (CFA_DEFAULT_ROUTER_IFINDEX);
                if (i4IfValid == CFA_TRUE)
                {
                    CFA_CDB_GET_ENET_IWF_LOCAL_MAC
                        (CFA_DEFAULT_ROUTER_IFINDEX, pIfInfo->au1MacAddr);
                }

            }

            CFA_DS_UNLOCK ();
            return (CFA_SUCCESS);
        }
    }

    CFA_DS_UNLOCK ();
    return (CFA_FAILURE);
}

/******************************************************************************
 * Function       : CfaCdbGetNextIvrIfInfo 
 *
 * Description    : This function will return the next valid L3_IPVLAN interface
 *                  index and their related interface parameters 
 *                  1. OperStatus
 *                  2. VlanId
 *                  3. MacAddress.
 *
 * Input(s)       : u4IfIndex
 *
 * Output(s)      : pIfInfo - Interface Info structure. 
 *                  pu4NextIndex - Interface index of next valid L3_IPVLAN interface.
 *
 * Returns        : CFA_SUCCESS/FAILURE
 *
 ******************************************************************************/

INT4
CfaCdbGetNextIvrIfInfo (UINT4 u4IfIndex, UINT4 *pu4NextIndex,
                        tCfaIfInfo * pIfInfo)
{
    INT4                i4Index;
    INT4                i4StartIndex = (INT4) u4IfIndex + 1;
    INT4                i4IfValid = CFA_FALSE;
    UINT1               u1IfType = 0;
    UINT1               u1DefRouterIdx = CFA_FALSE;

    CFA_DS_LOCK ();

    i4Index = i4StartIndex;
    CFA_CDB_SCAN (i4Index, (CFA_MAX_IVR_IF_INDEX), CFA_L3IPVLAN)
    {
        if (CFA_CDB_IS_INTF_VALID ((UINT4) i4Index) == CFA_TRUE)
        {
            *pu4NextIndex = (UINT4) i4Index;
            pIfInfo->u1IfOperStatus = CFA_CDB_IF_OPER ((UINT4) i4Index);
            pIfInfo->u2VlanId = CFA_CDB_IF_VLANID_OF_IVR ((UINT4) i4Index);

            u1IfType = CFA_CDB_IF_TYPE ((UINT4) i4Index);
            CFA_CDB_CHECK_IF_TYPE (u1IfType, u1DefRouterIdx);
            if (CFA_FALSE == u1DefRouterIdx)
            {
                CFA_CDB_GET_ENET_IWF_LOCAL_MAC ((UINT4) i4Index,
                                                pIfInfo->au1MacAddr);
            }
            else
            {
                i4IfValid = CFA_CDB_IS_INTF_VALID (CFA_DEFAULT_ROUTER_IFINDEX);
                if (i4IfValid == CFA_TRUE)
                {
                    CFA_CDB_GET_ENET_IWF_LOCAL_MAC
                        (CFA_DEFAULT_ROUTER_IFINDEX, pIfInfo->au1MacAddr);
                }

            }

            CFA_DS_UNLOCK ();
            return (CFA_SUCCESS);
        }
    }

    CFA_DS_UNLOCK ();
    return (CFA_FAILURE);
}

#ifdef MPLS_WANTED
/******************************************************************************
 * Function       : CfaCdbGetFirstMplsIfInfo 
 *
 * Description    : This function will return the first valid MPLS or MPLS Tunnel interface
 *                  index and their related interface parameters 
 *                  1. OperStatus
 *                  2. VlanId
 *                  3. MacAddress.
 *
 * Input(s)       : None.
 *
 * Output(s)      : pu4IfIndex - Interface index of first valid MPLS or MPLS Tunnel interface.
 *                  pIfInfo - Interface Info structure. 
 *
 * Returns        : CFA_SUCCESS/FAILURE
 *
 ******************************************************************************/

INT4
CfaCdbGetFirstMplsIfInfo (UINT4 *pu4IfIndex, tCfaIfInfo * pIfInfo)
{
    UINT4               u4Index;
    UINT4               u4StartIndex = 0;
    UINT4               u4EndIndex = 0;
    UINT4               u4MaxTnlInterface = 0;
    UINT4               u4IfRange = 0;
    INT4                i4IfValid = CFA_FALSE;
    UINT1               u1DefRouterIdx = CFA_FALSE;
    UINT1               u1IfType = 0;
    UINT1               u1IfLoopCount = 0;

    CFA_DS_LOCK ();

    u4StartIndex = (CFA_MAX_IVR_IF_INDEX + SYS_DEF_MAX_TUNL_IFACES + 1);
    u4EndIndex = (CFA_MAX_IVR_IF_INDEX + SYS_DEF_MAX_TUNL_IFACES +
                  SYS_DEF_MAX_MPLS_IFACES + SYS_DEF_MAX_MPLS_TNL_IFACES);
    u4MaxTnlInterface = SYS_DEF_MAX_MPLS_TNL_IFACES;
    u4IfRange = u4EndIndex - u4MaxTnlInterface;

    u4Index = u4StartIndex;

    /* Flexible ifIndex change: Loop twice, once for MPLS interfaces
     * and then for MPLS tunnel interfaces
     */

    for (u1IfType = CFA_MPLS, u1IfLoopCount = 0; u1IfLoopCount < 2;
         u1IfType = CFA_MPLS_TUNNEL, u4IfRange = u4EndIndex, u1IfLoopCount++)
    {
        CFA_CDB_SCAN (u4Index, u4IfRange, u1IfType)
        {
            if (CFA_CDB_IS_INTF_VALID (u4Index) == CFA_TRUE)
            {
                *pu4IfIndex = u4Index;
                pIfInfo->u1IfOperStatus = CFA_CDB_IF_OPER (u4Index);
                pIfInfo->u2VlanId = CFA_CDB_IF_VLANID_OF_IVR (u4Index);
                u1IfType = CFA_CDB_IF_TYPE (u4Index);
                CFA_CDB_CHECK_IF_TYPE (u1IfType, u1DefRouterIdx);
                if (CFA_FALSE == u1DefRouterIdx)
                {
                    CFA_CDB_GET_ENET_IWF_LOCAL_MAC (u4Index,
                                                    pIfInfo->au1MacAddr);
                }
                else
                {
                    i4IfValid =
                        CFA_CDB_IS_INTF_VALID (CFA_DEFAULT_ROUTER_IFINDEX);
                    if (i4IfValid == CFA_TRUE)
                    {
                        CFA_CDB_GET_ENET_IWF_LOCAL_MAC
                            (CFA_DEFAULT_ROUTER_IFINDEX, pIfInfo->au1MacAddr);
                    }
                }

                CFA_DS_UNLOCK ();
                return (CFA_SUCCESS);
            }
        }
        u4Index = u4StartIndex;
    }

    CFA_DS_UNLOCK ();
    return (CFA_FAILURE);
}

/******************************************************************************
 * Function       : CfaCdbGetNextMplsIfInfo 
 *
 * Description    : This function will return the next valid MPLS or MPLS Tunnel interface
 *                  index and their related interface parameters 
 *                  1. OperStatus
 *                  2. VlanId
 *                  3. MacAddress.
 *
 * Input(s)       : u4IfIndex
 *
 * Output(s)      : pIfInfo - Interface Info structure. 
 *                  pu4NextIndex - Interface index of next valid MPLS or MPLS Tunnel interface.
 *
 * Returns        : CFA_SUCCESS/FAILURE
 *
 ******************************************************************************/

INT4
CfaCdbGetNextMplsIfInfo (UINT4 u4IfIndex, UINT4 *pu4NextIndex,
                         tCfaIfInfo * pIfInfo)
{
    UINT4               u4Index;
    UINT4               u4StartIndex = 0;
    UINT4               u4EndIndex = 0;
    UINT4               u4IfRange = 0;
    UINT4               u4MaxTnlInterface = 0;
    INT4                i4IfValid = CFA_FALSE;
    UINT1               u1IfType = 0;

    UINT1               u1IfLoopCount = 0;
    UINT1               u1DefRouterIdx = CFA_FALSE;

    CFA_DS_LOCK ();

    u4StartIndex = u4IfIndex + 1;
    u4EndIndex = (CFA_MAX_IVR_IF_INDEX + SYS_DEF_MAX_TUNL_IFACES +
                  SYS_DEF_MAX_MPLS_IFACES + SYS_DEF_MAX_MPLS_TNL_IFACES);

    u4MaxTnlInterface = SYS_DEF_MAX_MPLS_TNL_IFACES;
    u4IfRange = u4EndIndex - u4MaxTnlInterface;

    u4Index = u4StartIndex;

    for (u1IfType = CFA_MPLS; u1IfLoopCount < 2; u1IfType = CFA_MPLS_TUNNEL,
         u4IfRange = u4EndIndex, u1IfLoopCount++)
    {
        CFA_CDB_SCAN (u4Index, u4IfRange, u1IfType)
        {

            if (CFA_CDB_IS_INTF_VALID (u4Index) == CFA_TRUE)
            {
                *pu4NextIndex = u4Index;
                pIfInfo->u1IfOperStatus = CFA_CDB_IF_OPER (u4Index);
                pIfInfo->u2VlanId = CFA_CDB_IF_VLANID_OF_IVR (u4Index);

                u1IfType = CFA_CDB_IF_TYPE (u4Index);
                CFA_CDB_CHECK_IF_TYPE (u1IfType, u1DefRouterIdx);
                if (CFA_FALSE == u1DefRouterIdx)
                {
                    CFA_CDB_GET_ENET_IWF_LOCAL_MAC (u4Index,
                                                    pIfInfo->au1MacAddr);
                }
                else
                {
                    i4IfValid =
                        CFA_CDB_IS_INTF_VALID (CFA_DEFAULT_ROUTER_IFINDEX);
                    if (i4IfValid == CFA_TRUE)
                    {
                        CFA_CDB_GET_ENET_IWF_LOCAL_MAC
                            (CFA_DEFAULT_ROUTER_IFINDEX, pIfInfo->au1MacAddr);

                    }
                }

                CFA_DS_UNLOCK ();
                return (CFA_SUCCESS);
            }
        }
        u4Index = u4StartIndex;
    }

    CFA_DS_UNLOCK ();
    return (CFA_FAILURE);
}

#endif /* MPLS_WANTED */
/*****************************************************************************
 *
 *    Function Name       : CfaSetCdbPortAdminStatus
 *
 *    Description         : This function updates the interface admin status
 *                          in the CDB database 
 *
 *    Input(s)            : u4IfIndex - interface index of the interface whose
 *                                      speed needs to be set.
 *                          u1AdminStatus - Port admin status           
 *
 *    Output(s)           : None.
 *
 *    Returns             : CFA_SUCCESS.  
 *****************************************************************************/
INT4
CfaSetCdbPortAdminStatus (UINT4 u4IfIndex, UINT1 u1AdminStatus)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;

    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_IF_ADMIN_STATUS (u4IfIndex) = u1AdminStatus;
    CFA_DS_UNLOCK ();

    CfaCopyPhyPortPropToLogicalInterface (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetCdbPortAdminStatus
 *
 *    Description         : This function returns the interface admin status
 *                          in the CDB database 
 *
 *    Input(s)            : u4IfIndex - interface index of the interface whose
 *                                      AdminStatus needs to be get.
 *                          u1AdminStatus - Port admin status           
 *
 *    Output(s)           : None.
 *
 *    Returns             : CFA_SUCCESS.  
 *****************************************************************************/
INT4
CfaGetCdbPortAdminStatus (UINT4 u4IfIndex, UINT1 *pu1AdminStatus)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;

    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    *pu1AdminStatus = CFA_CDB_IF_ADMIN_STATUS (u4IfIndex);
    CFA_DS_UNLOCK ();

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaSetCdbRowStatus
 *
 *    Description         : This function updates the interface row status
 *                          in the CDB database 
 *
 *    Input(s)            : u4IfIndex - interface index of the interface whose
 *                                      row status needs to be set.
 *                          u1RowStatus - Port row status           
 *
 *    Output(s)           : None.
 *
 *    Returns             : CFA_SUCCESS.  
 *****************************************************************************/
INT4
CfaSetCdbRowStatus (UINT4 u4IfIndex, UINT1 u1RowStatus)
{
    CFA_DS_LOCK ();
    CFA_CDB_IF_ROW_STATUS (u4IfIndex) = u1RowStatus;
    CFA_DS_UNLOCK ();

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfRowStatus                                    */
/*                                                                           */
/* Description        : This function gets the interface row status          */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu1RowStatus:Pointer to the row status               */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfRowStatus (UINT4 u4IfIndex, UINT1 *pu1RowStatus)
{
    if (CfaValidateCfaIfIndex (u4IfIndex) == CFA_SUCCESS)
    {
        CFA_DS_LOCK ();
        *pu1RowStatus = CFA_CDB_IF_ROW_STATUS (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfPortSecState                                 */
/*                                                                           */
/* Description        : This function sets port state for the given interface*/
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                      u1PortSecState - Bridge port state for the interface */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfPortSecState (UINT4 u4IfIndex, UINT1 u1PortSecState)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;

    CFA_DS_LOCK ();

    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_IF_PORT_SEC_STATE (u4IfIndex) = u1PortSecState;
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetIfWanType
 *
 *    Description         : This function returns the interface WAN type.   
 *
 *    Input(s)            : u4IfIndex - interface index
 *
 *    Output(s)           : pu1IfWanType - Pointer to network type.             
 *
 *    Returns             : CFA_SUCCESS/CFA_FAILURE.  
 *****************************************************************************/
INT4
CfaGetIfWanType (UINT4 u4IfIndex, UINT1 *pu1IfWanType)
{
    if ((u4IfIndex > SYS_MAX_INTERFACES) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu1IfWanType = CFA_CDB_IF_WAN_TYPE (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetIfNwType
 *
 *    Description         : This function returns the interface Network type.   
 *
 *    Input(s)            : u4IfIndex - interface index
 *
 *    Output(s)           : pu1IfNwType - Pointer to network type.             
 *
 *    Returns             : CFA_SUCCESS/CFA_FAILURE.  
 *****************************************************************************/
INT4
CfaGetIfNwType (UINT4 u4IfIndex, UINT1 *pu1IfNwType)
{
    if ((u4IfIndex > SYS_MAX_INTERFACES) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu1IfNwType = CFA_CDB_IF_NW_TYPE (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaSetIfWanType
 *
 *    Description         : This function sets the WAN type as private or 
 *                          public
 *
 *    Input(s)            : u4IfIndex - interface index
 *                          u1WanType.             
 *
 *    Output(s)           : None.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS.  
 *****************************************************************************/
INT4
CfaSetIfWanType (UINT4 u4IfIndex, UINT1 u1WanType)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
#ifdef NPAPI_WANTED
    tIfWanInfo          CfaWanInfo;
    MEMSET (&CfaWanInfo, 0, sizeof (tIfWanInfo));
#endif /* NPAPI_WANTED */

    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_IF_WAN_TYPE (u4IfIndex) = u1WanType;
    CFA_DS_UNLOCK ();

#ifdef NPAPI_WANTED
    CfaWanInfo.u4IfIndex = u4IfIndex;
    if (CFA_FAILURE == CfaGetIfHwAddr (u4IfIndex, CfaWanInfo.MacAddr))
    {
        return CFA_FAILURE;
    }
    if (u1WanType == CFA_WAN_TYPE_PUBLIC)
    {
        CfaWanInfo.u1Opcode = CFA_SET_TO_WAN;
    }
    else if (u1WanType == CFA_WAN_TYPE_PRIVATE)
    {
        CfaWanInfo.u1Opcode = CFA_RESET_TO_LAN;
    }
    /* check the NP_Programming */
    if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
    {
        if (CfaFsCfaHwSetWanTye (&CfaWanInfo) == FNP_FAILURE)
        {
            return CFA_FAILURE;
        }
    }
#endif /* NPAPI_WANTED */
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaSetIfNwType
 *
 *    Description         : This function sets the Network type as LAN or 
 *                          WAN
 *
 *    Input(s)            : u4IfIndex - interface index
 *                          u1NwType.             
 *
 *    Output(s)           : None.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS.  
 *****************************************************************************/
INT4
CfaSetIfNwType (UINT4 u4IfIndex, UINT1 u1NwType)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_IF_NW_TYPE (u4IfIndex) = u1NwType;
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfUnnumPeerMac                                 */
/*                                                                           */
/* Description        : This function gets the Peer MacAddress of the unnumbered 
            interface   */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu1MacAddr:Pointer to Mac Address                     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfUnnumPeerMac (UINT4 u4IfIndex, UINT1 *pu1MacAddr)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        MEMCPY (pu1MacAddr, CFA_CDB_ENET_IWF_UNNUM_PEER_MAC (u4IfIndex),
                CFA_ENET_ADDR_LEN);

        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfUnnumPeerMac                                   */
/*                                                                           */
/* Description        : This function sets the Peer MacAddress of the interface   */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      pu1MacAddr:Pointer to Mac Address                     */
/*                      u2Length :Length of the Mac address                  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfUnnumPeerMac (UINT4 u4IfIndex, UINT1 *pu1MacAddr, UINT2 u2Length)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }

    MEMCPY (CFA_CDB_ENET_IWF_UNNUM_PEER_MAC (u4IfIndex), pu1MacAddr,
            MEM_MAX_BYTES (u2Length, CFA_ENET_ADDR_LEN));
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfUnnumAssocIPIf                               */
/*                                                                           */
/* Description        : This function sets the Assossiated interface id      */
/*                      for unnumbered interface                             */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      u4AssocIfId                                          */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfUnnumAssocIPIf (UINT4 u4IfIndex, UINT4 u4AssocIfId)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);
    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_IF_ENTRY (u4IfIndex)->u4UnnumAssocIPIf = u4AssocIfId;
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfUnnumAssocIPIf                               */
/*                                                                           */
/* Description        : This function gets Assosiated interface id           */
/*                       of unnumbered interface                             */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/* Output(s)          : pu4AssocIfId:Pointer to Assossiate interface id      */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/

INT4
CfaGetIfUnnumAssocIPIf (UINT4 u4IfIndex, UINT4 *pu4AssocIfId)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu4AssocIfId = CFA_CDB_IF_ENTRY (u4IfIndex)->u4UnnumAssocIPIf;
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaCdbSetIfMauStatus                                 */
/*                                                                           */
/* Description        : This function sets the Mau Status for the Interface  */
/*                                                                           */
/* Input(s)           : u4MauIfIndex : Interface index                       */
/*                      i4MauStatus  : Mau Status                            */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaCdbSetIfMauStatus (UINT4 u4MauIfIndex, INT4 i4MauStatus)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4MauIfIndex);

    if (NULL != pCfaIfInfo)
    {
        if (CFA_CDB_IF_MAU_ENTRY_VALID (pCfaIfInfo))
        {
            CFA_CDB_IF_MAU_STATUS (pCfaIfInfo) = i4MauStatus;
            CFA_DS_UNLOCK ();
            return CFA_SUCCESS;
        }
    }

    CFA_DBG (CFA_TRC_ERROR, CFA_IF, "Error in setting MAU Status.\n");
    CFA_DS_UNLOCK ();
    return CFA_FAILURE;

}

/*****************************************************************************/
/* Function Name      : CfaCdbSetIfMauDefaultType                            */
/*                                                                           */
/* Description        : This function sets the Mau type for the Interface    */
/*                                                                           */
/* Input(s)           : u4MauIfIndex : Interface index                       */
/*                      u4MauType    : Mau Type                              */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaCdbSetIfMauDefaultType (UINT4 u4MauIfIndex, UINT4 u4MauType)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4MauIfIndex);

    if (NULL != pCfaIfInfo)
    {
        if (CFA_CDB_IF_MAU_ENTRY_VALID (pCfaIfInfo))
        {
            CFA_CDB_IF_MAU_DEF_TYPE (pCfaIfInfo) = u4MauType;
            CFA_DS_UNLOCK ();
            return CFA_SUCCESS;
        }
    }

    CFA_DBG (CFA_TRC_ERROR, CFA_IF, "Error in setting MAU Type.\n");
    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaCdbSetIfMauAutoNegAdminStatus                     */
/*                                                                           */
/* Description        : This function sets the AutoNegotiation status       */
/*                                                                           */
/* Input(s)           : u4MauIfIndex : Interface index                       */
/*                      i4ANStatus   : ifMauAutoNegAdminStatus               */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaCdbSetIfMauAutoNegAdminStatus (UINT4 u4MauIfIndex, INT4 i4ANStatus)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4MauIfIndex);

    if (NULL != pCfaIfInfo)
    {
        if (CFA_CDB_IF_MAU_ENTRY_VALID (pCfaIfInfo))
        {
            CFA_CDB_IF_MAU_ANSTATUS (pCfaIfInfo) = i4ANStatus;
            CFA_DS_UNLOCK ();
            return CFA_SUCCESS;
        }
    }

    CFA_DBG (CFA_TRC_ERROR, CFA_IF,
             "Error in setting MauAutoNegAdminStatus.\n");
    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaCdbSetIfMauAutoNegRestart                         */
/*                                                                           */
/* Description        : This function sets the Auto Negotiation restart      */
/*                      status                                               */
/*                                                                           */
/* Input(s)           : u4MauIfIndex : Interface index                       */
/*                      i4ANRestart  : ifMauAutoNegRestart                   */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaCdbSetIfMauAutoNegRestart (UINT4 u4MauIfIndex, INT4 i4ANRestart)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4MauIfIndex);

    if (NULL != pCfaIfInfo)
    {
        if (CFA_CDB_IF_MAU_ENTRY_VALID (pCfaIfInfo))
        {
            CFA_CDB_IF_MAU_ANRESTART (pCfaIfInfo) = i4ANRestart;
            CFA_DS_UNLOCK ();
            return CFA_SUCCESS;
        }
    }

    CFA_DBG (CFA_TRC_ERROR, CFA_IF, "Error in setting MauAutoNegRestart.\n");
    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaCdbSetIfMauAutoNegCapAdvtBits                     */
/*                                                                           */
/* Description        : This function sets the capabilities advertised       */
/*                      by the Auto negotiation entity                       */
/*                                                                           */
/* Input(s)           : u4MauIfIndex : Interface index                       */
/*                      i4ANCapAdvtBits : ifMauAutoNegCapAdvertisedBits      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaCdbSetIfMauAutoNegCapAdvtBits (UINT4 u4MauIfIndex, INT4 i4ANCapAdvtBits)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4MauIfIndex);

    if (NULL != pCfaIfInfo)
    {
        if (CFA_CDB_IF_MAU_ENTRY_VALID (pCfaIfInfo))
        {
            CFA_CDB_IF_MAU_ANCAPADVTBITS (pCfaIfInfo) = i4ANCapAdvtBits;
            CFA_DS_UNLOCK ();
            return CFA_SUCCESS;
        }
    }

    CFA_DBG (CFA_TRC_ERROR, CFA_IF,
             "Error in setting MauAutoNegCapAdvtBits.\n");
    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaCdbSetIfMauAutoNegRemFaultAdvt                    */
/*                                                                           */
/* Description        : This function sets the value fault indication and    */
/*                      advertises on the next auto-negotiation              */
/*                                                                           */
/* Input(s)           : u4MauIfIndex : Interface index                       */
/*                      i4ANRemFltAdvt : ifMauAutoNegRemoteFaultAdvertised   */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaCdbSetIfMauAutoNegRemFaultAdvt (UINT4 u4MauIfIndex, INT4 i4ANRemFltAdvt)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4MauIfIndex);

    if (NULL != pCfaIfInfo)
    {
        if (CFA_CDB_IF_MAU_ENTRY_VALID (pCfaIfInfo))
        {
            CFA_CDB_IF_MAU_ANREMFLTADVT (pCfaIfInfo) = i4ANRemFltAdvt;
            CFA_DS_UNLOCK ();
            return CFA_SUCCESS;
        }
    }

    CFA_DBG (CFA_TRC_ERROR, CFA_IF,
             "Error in setting MauAutoNegRemFaultAdvt.\n");
    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaCdbGetIfMauEntry                              */
/*                                                                           */
/* Description        : This function return configured Auto negoitiation    */
/*                          values for MAU entries                           */
/*                                                                           */
/* Input(s)           : u4MauIfIndex : Interface index                       */
/*                                                                           */
/* Output(s)          : pIfMauEntry - configured Auto negoitiation           */
/*                          values for MAU entries                           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : pIfMauEntry -  configured Auto negoitiation          */
/*                          values for MAU entries                           */
/*****************************************************************************/
tIfMauStruct       *
CfaCdbGetIfMauEntry (UINT4 u4MauIfIndex, UINT4 u4IfMauIndex)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    tIfMauStruct       *pIfMauEntry = NULL;

    UNUSED_PARAM (u4IfMauIndex);
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4MauIfIndex);

    if (NULL != pCfaIfInfo)
    {
        if (CFA_CDB_IF_MAU_ENTRY_VALID (pCfaIfInfo))
        {
            pIfMauEntry = CFA_CDB_IF_MAU_ENTRY (pCfaIfInfo);
        }
    }

    CFA_DS_UNLOCK ();
    return pIfMauEntry;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfACPortIdentifier                             */
/*                                                                           */
/* Description        : This function gets underlying physical interface     */
/*                      for the attachment circuit interface.                */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index for AC                     */
/*                                                                           */
/* Output(s)          : pu4IfACPort : Pointer to physical interface.         */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfACPortIdentifier (UINT4 u4IfIndex, UINT4 *pu4IfACPort)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu4IfACPort = CFA_CDB_IF_AC_PORT (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfACVlan                                       */
/*                                                                           */
/* Description        : This function gets the customer vlan identifier      */
/*                      for the attachment circuit interface.                */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index for AC                     */
/*                                                                           */
/* Output(s)          : pu2IfACVlan: Pointer to customer vlan id.            */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfACVlan (UINT4 u4IfIndex, UINT2 *pu2IfACVlan)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu2IfACVlan = CFA_CDB_IF_AC_VLAN (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfACPortIdentifier                             */
/*                                                                           */
/* Description        : This function sets physical interface for AC iface.  */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index of AC interface            */
/*                      u4IfACPort : Physical interface                      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfACPortIdentifier (UINT4 u4IfIndex, UINT4 u4IfACPort)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_IF_AC_PORT (u4IfIndex) = u4IfACPort;
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfACVlan                                       */
/*                                                                           */
/* Description        : This function sets customer vlan id for AC iface.    */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index of AC interface            */
/*                      u2IfACvlan : Customer vlan id                        */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfACVlan (UINT4 u4IfIndex, UINT2 u2IfACvlan)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_IF_AC_VLAN (u4IfIndex) = u2IfACvlan;
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfLinkUpDelayStatus                            */
/*                                                                           */
/* Description        : This function gets the interface LinkUp delay status */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index for LinkUp Delay           */
/*                      pu2LinkUpDelayStatus: LinkUp Delay status            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfLinkUpDelayStatus (UINT4 u4IfIndex, UINT2 *pu2LinkUpDelayStatus)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu2LinkUpDelayStatus =  CFA_CDB_LINKUP_DELAY_STATUS (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}


/*****************************************************************************/
/* Function Name      : CfaSetIfLinkUpDelayStatus                            */
/*                                                                           */
/* Description        : This function sets LinkUp delay Status               */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index of Link Up Delay           */
/*                      u4LinkUpDelayStatus  link up delay status            */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfLinkUpDelayStatus (UINT4 u4IfIndex, UINT4 u4LinkUpDelayStatus)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_LINKUP_DELAY_STATUS (u4IfIndex) = (UINT1) u4LinkUpDelayStatus;
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : CfaGetIfLinkUpDelayTimer                             */
/*                                                                           */
/* Description        : This function sets LinkUp Delay Timer                */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index of Link Up Delay           */
/*                      pu2LinkUpDelayTimer  link up delay timer value       */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/

INT4
CfaGetIfLinkUpDelayTimer (UINT4 u4IfIndex, UINT2 *pu2LinkUpDelayTimer)
{
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        *pu2LinkUpDelayTimer = (UINT2) CFA_CDB_LINKUP_DELAY_TIMER (u4IfIndex);
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }

    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}
/*****************************************************************************/
/* Function Name      : CfaSetIfLinkUpDelayTimer                             */
/*                                                                           */
/* Description        : This function set the LinkUp Delay Timer             */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index of Link Up Delay           */
/*                      u2LinkUpDelayTimer   link up delay timer value       */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfLinkUpDelayTimer (UINT4 u4IfIndex, UINT4 u2LinkUpDelayTimer)
{
    tCfaIfInfo         *pCfaIfInfo = NULL;
    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4IfIndex);

    if (NULL == pCfaIfInfo)
    {
        CFA_DS_UNLOCK ();
        return CFA_FAILURE;
    }
    CFA_CDB_LINKUP_DELAY_TIMER (u4IfIndex) = u2LinkUpDelayTimer;
    CFA_DS_UNLOCK ();
    return CFA_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : CfaGetIfLinkUpDelayRemainingTime                     */
/*                                                                           */
/* Description        : This function gets the remaining time of interface   */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index of LinkUp Delay            */
/*                      pu2LinkUpDelayRemainingTime: LinkUp delay            */
/*                      Remaining time                                       */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/

INT4
CfaGetIfLinkUpDelayRemainingTime (UINT4 u4IfIndex,
                                  UINT2 *pu2LinkUpDelayRemainingTime)
{

    tCfaIfInfo         *pCfaIfInfo = NULL;
    UINT4               u4RemainingTime = CFA_LINKUP_ZERO;

    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        return CFA_FAILURE;
    }

    CFA_DS_LOCK ();
    pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry(u4IfIndex);
    CFA_DS_UNLOCK ();

    if (CFA_CDB_IS_INTF_VALID (u4IfIndex) == CFA_TRUE)
    {
        TmrGetRemainingTime (CFA_LINKUP_DELAY_TMR_LIST,
           &(pCfaIfInfo->IfLinkUpDelay.PeriodicTmrNode.TmrBlk.TimerNode),
           &u4RemainingTime);
        /*convert the time from milliseconds to seconds*/
        u4RemainingTime = (u4RemainingTime/100);
        *pu2LinkUpDelayRemainingTime = (UINT2) u4RemainingTime;
        pCfaIfInfo->IfLinkUpDelay.u4LinkUpRemainingTime = u4RemainingTime;
        CFA_DS_UNLOCK ();
        return CFA_SUCCESS;
    }
    CFA_DS_UNLOCK ();
    return CFA_FAILURE;
}
