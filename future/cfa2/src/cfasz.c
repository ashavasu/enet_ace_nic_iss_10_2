/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfasz.c,v 1.3 2013/11/29 11:04:11 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _CFASZ_C
#include "cfainc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
CfaSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < CFA_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsCFASizingParams[i4SizingId].u4StructSize,
                                     FsCFASizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(CFAMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            CfaSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
CfaSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsCFASizingParams);
    IssSzRegisterModulePoolId (pu1ModName, CFAMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
CfaSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < CFA_MAX_SIZING_ID; i4SizingId++)
    {
        if (CFAMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (CFAMemPoolIds[i4SizingId]);
            CFAMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
