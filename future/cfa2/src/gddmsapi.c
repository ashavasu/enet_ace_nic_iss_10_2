/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: gddmsapi.c,v 1.22 2017/05/23 14:24:58 siva Exp $
 *
 *******************************************************************/

#include "ms.h"
#include "cfainc.h"
#include "size.h"
#include "gddapi.h"
#include <sys/select.h>
#include <signal.h>
#include "ofcl.h"
#ifdef FSB_WANTED
#include "fsb.h"
#endif
#define NSL_MAX_PORTS  SYS_DEF_MAX_PHYSICAL_INTERFACES    /* max number of ports for switch         */
#define NSL_ADDR_LEN    6        /* we support only ethernet MAC addresses */
#define NSL_DOWN        0        /* flag to indicate port state            */
#define NSL_UP          1        /* flag to indicate port state            */

#define DUMMY_VARIABLE(x) ((x)=(x))

#ifndef PACK_REQUIRED
#pragma pack(1)
#endif
typedef struct _NslPort
{
    UINT4               u4IfIndex;
    CHR1                PortName[20];    /* Assume no more than 999 ports per switch */
    UINT1               u1PortState;    /* connected to network or not connected   */
    UINT1               u1AppState;    /* whether CFA has registered on interface */
    UINT1               au1Rsvd[3];    /* to align on 4-byte word boundary         */
}
tNslPort;
tNslPort            gaNslPort[NSL_MAX_PORTS];
#ifndef PACK_REQUIRED
#pragma pack()
#endif

#ifdef  WLC_WANTED
extern INT4         VlanGetContextInfoFromIfIndex (UINT4 u4IfIndex,
                                                   UINT4 *pu4ContextId,
                                                   UINT2 *pu2LocalPort);
extern INT4         VlanClassifyFrame (tCRU_BUF_CHAIN_DESC * pFrame,
                                       UINT2 u2EtherType, UINT2 u2Port);
extern INT4         VlanSelectContext (UINT4 u4ContextId);
extern INT4         VlanReleaseContext (VOID);
extern INT4         WlcHdlrEnqueCtrlPkts (unWlcHdlrMsgStruct *);
#endif

typedef struct _NslMapPort
{
    UINT4               u4IfIndex;
    CHR1                PortName[24];    /* Assume no more than 999 ports per switch */
}
tNslMapPort;
tNslMapPort         gaIfAliasMap[NSL_MAX_PORTS];

void                NslSockClose (int signum);
static INT4         NslReadConf (CHR1 * pConfFile);
static INT4         isSwitch (CHR1 * pLine);
static INT4         isMac (CHR1 * pLine);
static INT4         isPktSrv (CHR1 * pLine);
static VOID         NslHandlePktSrvCtrlMsg (UINT1 *pData);
static VOID         NslBufDump (UINT1 *pBuf, UINT1 *pTitle, INT4 u4Len);
static INT4         NslReadTopology (VOID);
static INT4         NslReadTopFile (CHR1 * pc1TopfFile);
static INT4         NslAddNetwork (CHR1 * pu1Network);
static INT4         NslAddLinkToNetwork (CHR1 * pu1Network, INT4 i4Switch,
                                         INT4 i4Port);
static INT4         NslNetworkExists (CHR1 * pu1Network);
static INT4         NslAddInterfaceToNetwork (CHR1 * pu1IfName,
                                              CHR1 * pu1Network);
static INT4         NslInterfaceExists (CHR1 * pu1IfName);
static INT4         NslFindLinkNetwork (INT2 i2Port);
static INT4         NslOpenExtLinks (VOID);
static INT4         NslOpenEnetSocket (struct network *pNetwork);
static INT4         NslExtSend (UINT1 *pu1Buf, UINT4 u4Len, INT4 i4Sock,
                                INT4 i4IfIndex);
static INT4         NslFindRxPort (INT4 i4NetworkId);
static VOID         CfaProcessRxPacket (tCRU_BUF_CHAIN_HEADER ** pBuf,
                                        UINT1 *pData, UINT2 u2RxSw,
                                        UINT2 u2RxPort, INT2 i2PktLen);
static CHR1        *gpConfFilename = "../pktsrv.conf";
static UINT1        gu1EmbeddedMs = FALSE;
static INT4         gi4MaxSocketId = -1;
static fd_set       allFd;
INT4                NslGetPortFromIfIndex (UINT4 u4IfIndex);

INT4                gNslSock = 0;    /* Socket for all packets                  */
                                  /* Initialize with invalid value           */
INT4                gi4SingleMacAddr = -1;    /* if 0: the whole switch has one MAC      */
                                  /* otherwise: each port has a MAC of its   */
                                  /* own                                     */
UINT1               gau1HwAddr[NSL_MAX_PORTS][NSL_ADDR_LEN];
                                  /* Switch ethernet address. For simulation */
                                  /* Each switch has one and only one MAC    */
                                  /* Address used by all ports of the switch */
UINT4               gu2NslSw = 0;    /* Switch number for simulation. Every     */
                                  /* switch is assigned a unique number. The */
                                  /* number is carried in packets between    */
                                  /* switch and packet server.               */

UINT2               gu2NslBaseUdpPort = 8000;
UINT2               gu2NslUdpPort = 0;    /* UDP port number for server connection.  */
                                  /* This is derived from the switch number  */
                                  /* and base port number. Switch 1 uses base */
                                  /* port number + 1, switch 2 uses base port */
                                  /* number + 2, .... and so on.             */
                                  /* The server uses the base port itself.   */

UINT4               gu4NslPktServerIpAddr = 0;    /* IP address for server messages          */

/* Buffer to send frames - global to avoid allocating for each packet. */
UINT1               gau1TxBuf[CFA_MAX_DRIVER_MTU + CFA_MAX_FRAME_HEADER_SIZE];
INT4                CfaProcessPacket (tCRU_BUF_CHAIN_HEADER *, UINT4, UINT4);

/* defines for debugging and traces at physical level */
UINT4               gu4NslDebug = 0;
#define NSL_DEBUG_CTRL_MSGS        (0x00000001)
#define NSL_DEBUG_PKT_EVENT        (0x00000002)
#define NSL_DEBUG_PKT_DUMP         (0x00000004)

#define IS_NSL_DEBUG_CTRL_MSGS(x)  ((x) & NSL_DEBUG_CTRL_MSGS)
#define IS_NSL_DEBUG_PKT_EVENT(x)  ((x) & NSL_DEBUG_PKT_EVENT)
#define IS_NSL_DEBUG_PKT_DUMP(x)   ((x) & NSL_DEBUG_PKT_DUMP)
#define MAX_LINE_LENGTH 160
INT4                CfaGddWanicOpen (UINT4 u4IfIndex);
INT4                CfaGddWanicClose (UINT4 u4IfIndex);
INT4                CfaGddWanicWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex,
                                      UINT4 u4PktSize);
INT4                CfaGddWanicRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex,
                                     UINT4 *pu4PktSize);
INT4                CfaGddAsyncOpen (UINT4 u4IfIndex);
INT4                CfaGddAsyncClose (UINT4 u4IfIndex);
INT4                CfaGddAsyncWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex,
                                      UINT4 u4PktSize);
INT4                CfaGddAsyncRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex,
                                     UINT4 *pu4PktSize);
INT4                CfaGddAtmVcOpen (UINT4 u4IfIndex);
INT4                CfaGddAtmVcClose (UINT4 u4IfIndex);
INT4                CfaGddAtmVcRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex,
                                     UINT4 *pu4PktSize);
INT4                CfaGddAtmVcWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex,
                                      UINT4 u4PktSize);

static int
is_blankline (char *line)
{
    while (*line && isspace (*line))
        line++;
    if (*line == 0)
        return 1;

    return 0;
}
static int
is_comment (char *line)
{
    while (isspace (*line))
        line++;
    if (*line == '#')
        return 1;
    return 0;
}
static char        *
is_ipaddress (char *line)
{
    return (strstr (line, "server ipaddress"));
}
static char        *
is_interface (char *line)
{
    return (strstr (line, "interface"));
}
static char        *
is_network (char *line)
{
    return (strstr (line, "network"));
}
static char        *
tolowerstr (char *str)
{
    char               *tmp = str;

    if (!str)
        return NULL;

    while (*str)
    {
        *str = tolower (*str);
        str++;
    }
    return tmp;
}
static INT4
isSwitch (CHR1 * pLine)
{
    CHR1               *pc1Ptr = NULL;
    pc1Ptr = ((char *) strstr (pLine, "switch"));

    if (pc1Ptr == NULL)
    {
        return 0;
    }
    else
    {
        return 1;
    }

}

static INT4
isMac (CHR1 * pLine)
{
    CHR1               *pc1Ptr = NULL;
    pc1Ptr = ((char *) strstr (pLine, "mac"));

    if (pc1Ptr == NULL)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

static INT4
isInterfaceMac (CHR1 * pLine)
{
    CHR1               *pc1Ptr = NULL;
    pc1Ptr = ((char *) strstr (pLine, "mac port"));

    if (pc1Ptr == NULL)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

static INT4
isPktSrv (CHR1 * pLine)
{
    CHR1               *pc1Ptr = NULL;
    pc1Ptr = ((char *) strstr (pLine, "packet server ip"));

    if (pc1Ptr == NULL)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

static INT4
NslReadConf (CHR1 * pConfFile)
{
    FILE               *fp;
    UINT4               au4HwAddr[NSL_ADDR_LEN], u4Tmp;
    CHR1                au1PktServerIp[20];
    CHR1                au1Line[80];

    if ((fp = fopen (pConfFile, "r")) == NULL)
    {
        printf ("ERROR: Cannot open config file %s for reading\n", pConfFile);
        return (CFA_FAILURE);
    }

    MEMSET (gau1HwAddr, 0, NSL_ADDR_LEN * NSL_MAX_PORTS);
    gu4NslPktServerIpAddr = 0;
    gu2NslSw = 0;

    MEMSET (au1Line, '\0', 80);
    while (fgets (au1Line, 80, fp))
    {
        if (is_blankline (au1Line))
        {
            continue;
        }
        if (is_comment (au1Line))
        {
            continue;
        }

        if (isSwitch (au1Line))
        {
            sscanf (au1Line, "switch %d\n", (int *) &u4Tmp);
            gu2NslSw = (UINT2) u4Tmp;
            continue;
        }

        if (isInterfaceMac (au1Line))
        {
            INT4                portId;

            if (gi4SingleMacAddr == 0)
            {
                /* if we reach here it means that the switch MAC has
                 * already been specified. We cannot have both the per port
                 * MAC configuration and MAC Switch configuration appearing
                 * together.
                 */
                printf ("NSL INIT: Cannot specify per port MAC when "
                        "switch MAC is already configured\n");
                return (CFA_FAILURE);
            }
            sscanf (au1Line, "mac port %d %2x:%2x:%2x:%2x:%2x:%2x",
                    (int *) &portId,
                    (unsigned int *) &au4HwAddr[0],
                    (unsigned int *) &au4HwAddr[1],
                    (unsigned int *) &au4HwAddr[2],
                    (unsigned int *) &au4HwAddr[3],
                    (unsigned int *) &au4HwAddr[4],
                    (unsigned int *) &au4HwAddr[5]);

            if ((portId < 1) || (portId > NSL_MAX_PORTS))
            {
                printf ("NSL INIT: Port ID must be between 1 and %d\n",
                        NSL_MAX_PORTS);
                return (CFA_FAILURE);
            }

            portId--;

            gau1HwAddr[portId][0] = (UINT1) au4HwAddr[0];
            gau1HwAddr[portId][1] = (UINT1) au4HwAddr[1];
            gau1HwAddr[portId][2] = (UINT1) au4HwAddr[2];
            gau1HwAddr[portId][3] = (UINT1) au4HwAddr[3];
            gau1HwAddr[portId][4] = (UINT1) au4HwAddr[4];
            gau1HwAddr[portId][5] = (UINT1) au4HwAddr[5];

            if (gi4SingleMacAddr == -1)
            {
                gi4SingleMacAddr = 1;
            }
            else
            {
                gi4SingleMacAddr++;
            }
            continue;
        }

        if (isMac (au1Line))
        {
            if ((gi4SingleMacAddr != 0) && (gi4SingleMacAddr != -1))
            {
                /* if we reach here it means that the mac addresses for
                 * ports have been specified on a per port basis. We cannot
                 * have both the per port MAC configuration and MAC Switch
                 * configuration appearing together.
                 */
                printf ("NSL INIT: Cannot specify switch MAC, when per "
                        "port MAC is already configured\n");
                return (CFA_FAILURE);
            }
            sscanf (au1Line, "mac %2x:%2x:%2x:%2x:%2x:%2x",
                    (unsigned int *) &au4HwAddr[0],
                    (unsigned int *) &au4HwAddr[1],
                    (unsigned int *) &au4HwAddr[2],
                    (unsigned int *) &au4HwAddr[3],
                    (unsigned int *) &au4HwAddr[4],
                    (unsigned int *) &au4HwAddr[5]);
            gau1HwAddr[0][0] = (UINT1) au4HwAddr[0];
            gau1HwAddr[0][1] = (UINT1) au4HwAddr[1];
            gau1HwAddr[0][2] = (UINT1) au4HwAddr[2];
            gau1HwAddr[0][3] = (UINT1) au4HwAddr[3];
            gau1HwAddr[0][4] = (UINT1) au4HwAddr[4];
            gau1HwAddr[0][5] = (UINT1) au4HwAddr[5];

            gi4SingleMacAddr = 0;    /* only single MAC for the whole switch */

            continue;
        }

        if (isPktSrv (au1Line))
        {
            sscanf (au1Line, "packet server ip %s\n", au1PktServerIp);
            gu4NslPktServerIpAddr = (UINT4) inet_addr (au1PktServerIp);
            continue;
        }
    }
    fclose (fp);

    if ((gu2NslSw == 0) || (gu4NslPktServerIpAddr == 0))
    {
        printf ("ERROR: %s file does not specify either switch number or "
                "packet server IP address\n", pConfFile);
        return (CFA_FAILURE);
    }

    return (CFA_SUCCESS);
}

static CHR1         ConfigFile[] = "nsl.conf";

void
NslSockClose (int signum)
{
    INT4                i4Index = 0;
    signum = 0;
    if (gNslSock != 0)
    {
        if (close (gNslSock) != 0)
        {
            perror ("Nsl Socket Close Error");
        }
        fflush (stdout);
        gNslSock = 0;
    }

    FD_ZERO (&allFd);

    for (i4Index = 0; i4Index < MAX_NETWORKS; i4Index++)
    {
        if (Networks[i4Index].networkName[0] != 0)
        {
            if (!Networks[i4Index].LinkExt.ifname[0])
                continue;

            close (Networks[i4Index].LinkExt.sock);
        }
    }
    UNUSED_PARAM (signum);
}

INT4
CfaGddInit (VOID)
{
    UINT4               i;
    struct sockaddr_in  NslSockAddr;
    CHR1               *pu1EmbdMs = NULL;

    /* Get the environmental variable "MS" */
    pu1EmbdMs = getenv ("MS");

    /* If the env variable MS="MS_EMBEDDED", then set the Global flag to TRUE */
    if ((pu1EmbdMs != NULL) && (STRCMP (pu1EmbdMs, "MS_EMBEDDED") == 0))
    {
        gu1EmbeddedMs = TRUE;
    }

    if (NslReadConf (ConfigFile) != CFA_SUCCESS)
    {
        return (CFA_FAILURE);
    }

    /* Open socket for connection to the packet server. All packets */
    /* from a switch go to the packet server which sends it to the  */
    /* correct switch or switches. Similarly all packets to this    */
    /* switch come from the packet server. After opening, bind the  */
    /* socket to the UDP port for this switch (calculated based on  */
    /* the switch number and the base UDP port) using the global IP */
    /* address for all switches in the simulated network.           */
    if ((gNslSock = socket (AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror ("Error in opening port\n");
        return (CFA_FAILURE);
    }

    gu2NslUdpPort = (UINT2) (gu2NslBaseUdpPort + gu2NslSw);
    NslSockAddr.sin_family = AF_INET;
    NslSockAddr.sin_port = htons (gu2NslUdpPort);
    MEMCPY ((char *) &NslSockAddr.sin_addr.s_addr,
            (char *) &gu4NslPktServerIpAddr, 4);
    if (bind (gNslSock, (struct sockaddr *) &NslSockAddr,
              sizeof (NslSockAddr)) < 0)
    {
        perror ("UDP socket bind failure");
        return (CFA_FAILURE);
    }

    /* set options for non-blocking mode */
    if (fcntl (gNslSock, F_SETFL, O_NONBLOCK) < 0)
    {
        perror ("Failure in setting socket non-blocking option\n");
        close (gNslSock);
        return (CFA_FAILURE);
    }

    for (i = 0; i < NSL_MAX_PORTS; i++)
    {
        gaNslPort[i].u4IfIndex = -1;
        gaNslPort[i].u1AppState = NSL_DOWN;
        gaNslPort[i].u1PortState = NSL_DOWN;
        /* let Slot numbering start from 1 against actual physical port eth0 */
        SPRINTF ((CHR1 *) (gaNslPort[i].PortName), "eth%d", ((int) i));
    }

    for (i = 0; i < NSL_MAX_PORTS; i++)
    {
        gaNslPort[i].u4IfIndex = -1;
        gaNslPort[i].u1AppState = NSL_DOWN;
        gaNslPort[i].u1PortState = NSL_DOWN;
        /* let Slot numbering start from 1 against actual physical port eth0 */
        SPRINTF ((CHR1 *) (gaIfAliasMap[i].PortName), "Slot0/%d",
                 ((int) i + 1));
    }
    signal (SIGINT, NslSockClose);

    /* When Embedded MS is enabled, read the topology file and update the 
     * networks and links informations */
    if (gu1EmbeddedMs == TRUE)
    {
        if (NslReadTopology () != CFA_SUCCESS)
        {
            return CFA_FAILURE;
        }
    }
    return (CFA_SUCCESS);
}

static VOID
NslHandlePktSrvCtrlMsg (UINT1 *pData)
{
    struct ctrlMsg     *pCtrlmsg = (struct ctrlMsg *) pData;

    /* control messages from packet server */
#define PKTSRV_CTRL_SET_DEBUG   (10)

    switch (pCtrlmsg->code)
    {
        case PKTSRV_CTRL_SET_DEBUG:
            MEMCPY (&gu4NslDebug, pData, 4);
            printf ("NSL: CTRL: NSL debug now is 0x%8X\n", (int) gu4NslDebug);
            break;

        case CTRL_MSG_LINK_STATUS:
        {
            struct linkStatus  *linkStatus;
            int                 u2RxSw, u2RxPort, status;
            int                 i4IfIndex = 0;

            linkStatus = &pCtrlmsg->data.linkStatus;

            u2RxSw = linkStatus->s;
            u2RxPort = linkStatus->p;
            status = linkStatus->status;

            if (IS_NSL_DEBUG_CTRL_MSGS (gu4NslDebug))
            {
                printf ("NSL: CTRL: port %s; sw: %d, port: %d\n",
                        (status == NSL_DOWN ? "DOWN" : "UP"), u2RxSw, u2RxPort);
            }
            gaNslPort[u2RxPort - 1].u1PortState = status;

            if ((i4IfIndex = (int) gaNslPort[u2RxPort - 1].u4IfIndex) < 0)
            {
                break;
            }

            if (CFA_IF_ADMIN (i4IfIndex) == CFA_IF_UP)
            {
                CfaIfmHandleInterfaceStatusChange (i4IfIndex, CFA_IF_UP,
                                                   (status ? CFA_IP_UP :
                                                    CFA_IF_DOWN),
                                                   CFA_IF_LINK_STATUS_CHANGE);
            }

            break;
        }

        default:
            if (IS_NSL_DEBUG_CTRL_MSGS (gu4NslDebug))
            {
                printf ("NSL: CTRL: INVALID control message (%d)\n",
                        pCtrlmsg->code);
            }
            break;

    }
}

VOID
NslBufDump (UINT1 *pBuf, UINT1 *pTitle, INT4 i4Len)
{
    INT4                i4Tmp;

    if (!pBuf)
        return;

    printf ("\n%s\n", pTitle);
    for (i4Tmp = 0; i4Tmp < i4Len; i4Tmp++)
    {
        if (i4Tmp)
        {
            if (!(i4Tmp % 24))
            {
                printf ("\n");
            }
            else
            {
                if (!(i4Tmp % 8))
                {
                    printf ("  ");
                }
                else
                {
                    if (i4Tmp)
                    {
                        printf (" ");
                    }
                }
            }
        }
        printf ("%02X", pBuf[i4Tmp]);
    }
    printf ("\n");
}

VOID
CfaGddShutdown (VOID)
{
    INT4                i;

    close (gNslSock);
    gNslSock = -1;
    for (i = 0; i < NSL_MAX_PORTS; i++)
    {
        gaNslPort[i].u4IfIndex = i + 1;
        gaNslPort[i].u1AppState = NSL_DOWN;
        gaNslPort[i].u1PortState = NSL_DOWN;
        gaNslPort[i].PortName[0] = 0;
    }
    FD_ZERO (&allFd);

    for (i = 0; i < MAX_NETWORKS; i++)
    {
        if (Networks[i].networkName[0] != 0)
        {
            if (!Networks[i].LinkExt.ifname[0])
                continue;

            close (Networks[i].LinkExt.sock);
        }
    }
}

INT4
CfaGddSetGddType (UINT4 u4IfIndex)
{
    UINT1               u1IfType;
    /* Accept anything; but we support only ethernet */
    CfaGetIfType (u4IfIndex, &u1IfType);
    CFA_GDD_TYPE (u4IfIndex) = u1IfType;
    return (CFA_SUCCESS);
}

INT4
CfaGddLinuxFixForWanPipe (UINT4 u4IfIndex)
{
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    return (CFA_SUCCESS);
}

VOID
CfaGddOsRemoveIfFromList (UINT4 u4IfIndex)
{
    /* unused function, we don't maintain any polling */
    /* table because we have only one real socket.    */
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
}

VOID
CfaGddOsAddIfToList (UINT4 u4IfIndex)
{
    /* unused function, we don't maintain any polling */
    /* table because we have only one real socket.    */
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
}

INT4
CfaGddOsProcessRecvEvent (VOID)
{
    struct sockaddr_in  FromAddr;
    INT4                i4RetVal, i4AddrLen;
    UINT1              *pData;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    fd_set              readFds;
    UINT2               u2RxPort, u2RxSw;
    INT2                i2PktLen;
    struct timeval      tv;
    INT4                i4NetworkId = 0;

    /* - Select to check if socket has any data to read
       - If select fails, stop loop
       - Check if our socket descriptor shows data present
       - If no data present stop loop
       - If buffer pointer null, allocate a buffer; if not use
       allocated buffer
       - Call function to read in the packet
       - If read fails stop loop
       - Call function to validate packet and get ifIndex, source
       and destination mac-address
       - If packet valid, call function to process packet further
       - If packet invalid, loop further to check for more packets
       - When loop is finished, if buffer pointer not null, free
       it and return
     */
    while (1)
    {
        FD_ZERO (&readFds);
        if (gNslSock > gi4MaxSocketId)
        {
            gi4MaxSocketId = gNslSock;
        }
        FD_SET (gNslSock, &allFd);
        memcpy (&readFds, &allFd, sizeof (fd_set));

        tv.tv_sec = 0;
        tv.tv_usec = 0;
        i4RetVal = select ((gi4MaxSocketId + 1), &readFds, NULL, NULL, &tv);

        if (i4RetVal <= 0)
        {
            break;                /* No data to read or some error */
        }

        if (FD_ISSET (gNslSock, &readFds))
        {
            /* Socket has data. Get buffer if needed to read packet in to. */
            if (pBuf == NULL)
            {
                if ((pBuf = CRU_BUF_Allocate_MsgBufChain (1520, 0)) == NULL)
                {
                    break;
                }
            }

            /* We don't check return value here. We assume buffer is linear. */
            pData = CRU_BUF_GetDataPtr (pBuf->pFirstValidDataDesc);

            MEMSET (&FromAddr, 0, sizeof (FromAddr));
            i4AddrLen = sizeof (FromAddr);

            if ((i2PktLen =
                 recvfrom (gNslSock, (VOID *) pData,
                           CFA_MAX_DRIVER_MTU + CFA_MAX_FRAME_HEADER_SIZE + 4,
                           0, (struct sockaddr *) &FromAddr,
                           (socklen_t *) & i4AddrLen)) <= 0)
            {
                break;
            }

            if (i2PktLen > 1518)
            {
                CFA_DBG1 (CFA_TRC_IP_PKT_DUMP, CFA_GDD,
                          "GDDNSL: pktsize %d\n", i2PktLen);
            }
            pBuf->pFirstValidDataDesc->u4_ValidByteCount += i2PktLen;
            pBuf->pFirstValidDataDesc->u4_FreeByteCount -= i2PktLen;

            MEMCPY (&u2RxSw, pData, 2);
            MEMCPY (&u2RxPort, (pData + 2), 2);

            i2PktLen -= 4;
            CRU_BUF_Move_ValidOffset (pBuf, 4);

            CfaProcessRxPacket (&pBuf, pData, u2RxSw, u2RxPort, i2PktLen);
        }

        /* Only When Embedded MS is enabled */
        if (gu1EmbeddedMs == TRUE)
        {
            /* Fetch all the external sockets and check if Data has been received or not */
            for (i4NetworkId = 0; i4NetworkId < MAX_NETWORKS; i4NetworkId++)
            {
                if ((Networks[i4NetworkId].LinkExt.ifname[0]) &&
                    (Networks[i4NetworkId].LinkExt.st) &&
                    (Networks[i4NetworkId].LinkExt.sock >= 0))
                {
                    if (FD_ISSET (Networks[i4NetworkId].LinkExt.sock, &readFds))
                    {
                        if (pBuf == NULL)
                        {
                            if ((pBuf =
                                 CRU_BUF_Allocate_MsgBufChain (1520,
                                                               0)) == NULL)
                            {
                                break;
                            }
                        }

                        pData = CRU_BUF_GetDataPtr (pBuf->pFirstValidDataDesc);

                        i2PktLen = recv (Networks[i4NetworkId].LinkExt.sock,
                                         (void *) pData,
                                         CFA_MAX_DRIVER_MTU +
                                         CFA_MAX_FRAME_HEADER_SIZE, 0);

                        if (i2PktLen < 0)
                        {
                            break;
                        }
                        pBuf->pFirstValidDataDesc->u4_ValidByteCount +=
                            i2PktLen;
                        pBuf->pFirstValidDataDesc->u4_FreeByteCount -= i2PktLen;

                        /* Find the Rx port */
                        u2RxPort = NslFindRxPort (i4NetworkId);
                        u2RxSw = gu2NslSw;

                        CfaProcessRxPacket (&pBuf, pData, u2RxSw, u2RxPort,
                                            i2PktLen);
                    }
                }
            }
        }
    }

    if (pBuf != NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }
    return (CFA_SUCCESS);
}

INT4
CfaGddConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption, VOID *pConfigParam)
{
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    DUMMY_VARIABLE (pConfigParam);    /* avoid compiler warning */
    DUMMY_VARIABLE (u1ConfigOption);    /* avoid compiler warning */

    if ((u1ConfigOption == CFA_ENET_EN_PROMISC) ||
        (u1ConfigOption == CFA_ENET_DIS_PROMIS))
        return (CFA_SUCCESS);
    else
        return (CFA_FAILURE);
}

INT4
CfaGddGetOsHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
    UINT2               u2Port;
    UINT1               u1IfType;

    /* The name of the function is misleading. For ethernet */
    /* this function actually opens the port and registers. */
    /* If interface already open/registered return address */
    if (gi4SingleMacAddr == 0)
    {
        /* single MAC for whole switch */
        MEMCPY (au1HwAddr, gau1HwAddr[0], NSL_ADDR_LEN);
    }

    CfaGetIfaceType (u4IfIndex, &u1IfType);
    if ((u1IfType == CFA_PSEUDO_WIRE) || (u1IfType == CFA_PPP))
    {
        if (CfaGetPswHwAddr (u4IfIndex, au1HwAddr) != CFA_SUCCESS)
        {
            return CFA_FAILURE;
        }
        return CFA_SUCCESS;
    }

    /* new interface being created by CFA */
    /* check if NSL is configured with an interface by that name */
    /* NSL interfaces MUST have names like eth0, eth1, eth2,...  */
    for (u2Port = 0; u2Port < NSL_MAX_PORTS; u2Port++)
    {
        if (STRCMP (CFA_GDD_PORT_NAME (u4IfIndex),
                    gaIfAliasMap[u2Port].PortName) == 0)

        {
            /* Found the NSL interface. Register CFA on interface */
            /* and also store array index as GDD port descriptor. */
            gaNslPort[u2Port].u1AppState = NSL_UP;
            gaNslPort[u2Port].u1PortState = NSL_UP;
            gaNslPort[u2Port].u4IfIndex = (UINT4) u4IfIndex;
            CFA_GDD_PORT_DESC (u4IfIndex) = u2Port;

            if (gi4SingleMacAddr != 0)
            {
                /* mac address per port */
                MEMCPY (au1HwAddr, gau1HwAddr[u4IfIndex - 1], NSL_ADDR_LEN);
            }

            return (CFA_SUCCESS);
        }
    }
    return (CFA_SUCCESS);
}

INT4
CfaGddLinuxEthSockOpen (UINT4 u4IfIndex)
{
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    return (CFA_SUCCESS);
}

INT4
CfaGddEthOpen (UINT4 u4IfIndex)
{
    UINT2               u2Port;

    /* Nothing to do; port opened in CfaGddGetOsHwAddr() call */
    for (u2Port = 0; u2Port < NSL_MAX_PORTS; u2Port++)
    {
        if (u4IfIndex == (UINT2) gaNslPort[u2Port].u4IfIndex)
        {
            /* Found the NSL interface. Register CFA on interface */
            /* and also store array index as GDD port descriptor. */
            gaNslPort[u2Port].u1AppState = NSL_UP;
            gaNslPort[u2Port].u4IfIndex = (UINT4) u4IfIndex;
            CFA_GDD_PORT_DESC (u4IfIndex) = u2Port;
            return (CFA_SUCCESS);
        }
    }
    return (CFA_SUCCESS);
}

INT4
CfaGddEthClose (UINT4 u4IfIndex)
{
    UINT2               u2Port;

    /* Scan NSL port array, match input ifIndex and close */
    for (u2Port = 0; u2Port < NSL_MAX_PORTS; u2Port++)
    {
        if ((gaNslPort[u2Port].u4IfIndex) == (UINT4) u4IfIndex)
        {
            CFA_GDD_PORT_DESC (u4IfIndex) = CFA_INVALID_DESC;    /* invalid port */
            gaNslPort[u2Port].u1AppState = NSL_DOWN;
            return (CFA_SUCCESS);
        }
    }
    return (CFA_FAILURE);        /* interface does not exist */
}

INT4
CfaGddEthWrite (UINT1 *pu1Buf, UINT4 u4IfIndex, UINT4 u4Size)
{
    struct sockaddr_in  ToAddr;
    INT4                i4Bytes;
    INT2                i2Port;
    INT4                i4NetworkId = -1;
    INT4                i4LinkId = 0;
    INT2                i2DestSwitch = 0;
    INT2                i2DestPort = 0;
    struct Link        *pLink = NULL;
    UINT1               u1SubType = CFA_SUBTYPE_SISP_INTERFACE;

    if (u4Size > 1522)
    {
        return (CFA_FAILURE);
    }

    i2Port = NslGetPortFromIfIndex (u4IfIndex);

    if (i2Port < 0)
    {
        return (CFA_FAILURE);
    }

    if (gaNslPort[i2Port].u1PortState != NSL_UP)
        /*(gaNslPort[i2Port].u1AppState != NSL_UP)) */
    {
        return (CFA_FAILURE);
    }

    if (IS_NSL_DEBUG_PKT_DUMP (gu4NslDebug))
    {
        CHR1                Tmp[80];

        sprintf (Tmp, "NSL: PktDump: Tx sw: %d, port: %d, ifIndex: %d"
                 "Size: %d", (int) gu2NslSw, (int) i2Port,
                 (int) u4IfIndex, (int) u4Size);
        NslBufDump (pu1Buf, (UINT1 *) Tmp, u4Size);
    }
    else
    {
        if (IS_NSL_DEBUG_PKT_EVENT (gu4NslDebug))
        {
            printf ("NSL: PktEv: Tx sw: %d, port: %d, ifIndex: %d, "
                    "Size: %d\n", (int) gu2NslSw, (int) i2Port,
                    (int) u4IfIndex, (int) u4Size);
        }
    }

    i2Port++;
    MEMCPY ((UINT1 *) (&(gau1TxBuf[4])), pu1Buf, u4Size);

    ToAddr.sin_family = AF_INET;
    MEMCPY ((CHR1 *) (&(ToAddr.sin_addr.s_addr)),
            (CHR1 *) & (gu4NslPktServerIpAddr), 4);

    if (gu1EmbeddedMs == FALSE)
    {
        MEMCPY ((CHR1 *) & gau1TxBuf[0], &gu2NslSw, 2);
        MEMCPY ((CHR1 *) & gau1TxBuf[2], &i2Port, 2);
        ToAddr.sin_port = htons (gu2NslBaseUdpPort);
        i4Bytes = sendto (gNslSock, (void *) gau1TxBuf, (u4Size + 4),
                          0, (struct sockaddr *) &ToAddr, sizeof (ToAddr));
        if (i4Bytes == (INT4) (u4Size + 4))
        {
            CfaGetIfSubType ((UINT4) u4IfIndex, &u1SubType);

            if (u1SubType == CFA_SUBTYPE_OPENFLOW_INTERFACE)
            {
                CFA_IF_SET_OUT_UCAST (u4IfIndex);
                CFA_IF_SET_OUT_OCTETS (u4IfIndex, u4Size);
            }
            return (CFA_SUCCESS);
        }
    }
    /* Only when Embebbed MS is enabled */
    else
    {
        if ((i4NetworkId = NslFindLinkNetwork (i2Port)) >= 0)
        {
            for (i4LinkId = 0; i4LinkId < MAX_LINKS; i4LinkId++)
            {
                pLink = &Networks[i4NetworkId].Links[i4LinkId];
                if (pLink->st && pLink->fl)
                {
                    /* do not transmit packet back to the sender */
                    if ((pLink->ss == (INT4) gu2NslSw) && (pLink->sp == i2Port))
                    {
                        continue;
                    }

                    i2DestSwitch = pLink->ss;
                    i2DestPort = pLink->sp;
                    MEMCPY ((CHR1 *) & gau1TxBuf[0], &i2DestSwitch, 2);
                    MEMCPY ((CHR1 *) & gau1TxBuf[2], &i2DestPort, 2);
                    ToAddr.sin_port = htons (gu2NslBaseUdpPort + i2DestSwitch);
                    i4Bytes =
                        sendto (gNslSock, (void *) gau1TxBuf, (u4Size + 4), 0,
                                (struct sockaddr *) &ToAddr, sizeof (ToAddr));
                    if (i4Bytes != (INT4) (u4Size + 4))
                    {
                        perror ("sendto");
                    }
                }
            }

            /* Transmit the packet to external interfaces also if any */
            if ((Networks[i4NetworkId].LinkExt.ifname[0]) &&
                (Networks[i4NetworkId].LinkExt.sock >= 0))
            {
                i4Bytes =
                    NslExtSend ((gau1TxBuf + 4), u4Size,
                                Networks[i4NetworkId].LinkExt.sock,
                                Networks[i4NetworkId].LinkExt.ifindex);
                if (i4Bytes != (INT4) u4Size)
                {
                    perror ("sendto");
                }
            }

            CfaGetIfSubType ((UINT4) u4IfIndex, &u1SubType);

            if (u1SubType == CFA_SUBTYPE_OPENFLOW_INTERFACE)
            {
                CFA_IF_SET_OUT_UCAST (u4IfIndex);
                CFA_IF_SET_OUT_OCTETS (u4IfIndex, u4Size);
            }
            return CFA_SUCCESS;
        }
    }
    return (CFA_FAILURE);
}

INT4
CfaGddEthWriteWithPri (UINT1 *pData, UINT4 u4IfIndex, UINT4 u4PktLen,
                       UINT1 u1Priority)
{
    if (CfaGddEthWrite (pData, u4IfIndex, u4PktLen) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }
    DUMMY_VARIABLE (u1Priority);
    return CFA_SUCCESS;
}

INT4
CfaGddEthRead (UINT1 *pData, UINT4 u4IfIndex, UINT4 *pu4PktLen)
{
    /* Read and process is done in CfaGddOsProcsssRecvEvent. */
    /* Nothing to do here. Just avoid compiler warnings.     */
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    DUMMY_VARIABLE (pData);        /* avoid compiler warning */
    DUMMY_VARIABLE (pu4PktLen);    /* avoid compiler warning */
    return CFA_FAILURE;
}

INT4
CfaGddWanicOpen (UINT4 u4IfIndex)
{
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    return CFA_FAILURE;
}

INT4
CfaGddWanicClose (UINT4 u4IfIndex)
{
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    return CFA_FAILURE;
}

INT4
CfaGddWanicWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    DUMMY_VARIABLE (pu1DataBuf);    /* avoid compiler warning */
    DUMMY_VARIABLE (u4PktSize);    /* avoid compiler warning */
    return CFA_FAILURE;
}

INT4
CfaGddWanicRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    DUMMY_VARIABLE (pu1DataBuf);    /* avoid compiler warning */
    DUMMY_VARIABLE (pu4PktSize);    /* avoid compiler warning */
    return CFA_FAILURE;
}

INT4
CfaGddAsyncOpen (UINT4 u4IfIndex)
{
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    return CFA_FAILURE;
}

INT4
CfaGddAsyncClose (UINT4 u4IfIndex)
{
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    return CFA_FAILURE;
}

INT4
CfaGddAsyncWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    DUMMY_VARIABLE (pu1DataBuf);    /* avoid compiler warning */
    DUMMY_VARIABLE (u4PktSize);    /* avoid compiler warning */
    return CFA_FAILURE;
}

INT4
CfaGddAsyncRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    DUMMY_VARIABLE (pu1DataBuf);    /* avoid compiler warning */
    DUMMY_VARIABLE (pu4PktSize);    /* avoid compiler warning */
    return CFA_FAILURE;
}

INT4
CfaGddAtmVcOpen (UINT4 u4IfIndex)
{
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    return CFA_FAILURE;
}

INT4
CfaGddAtmVcClose (UINT4 u4IfIndex)
{
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    return CFA_FAILURE;
}

INT4
CfaGddAtmVcRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    DUMMY_VARIABLE (pu1DataBuf);    /* avoid compiler warning */
    DUMMY_VARIABLE (pu4PktSize);    /* avoid compiler warning */
    return CFA_FAILURE;
}

INT4
CfaGddAtmVcWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    DUMMY_VARIABLE (u4IfIndex);    /* avoid compiler warning */
    DUMMY_VARIABLE (pu1DataBuf);    /* avoid compiler warning */
    DUMMY_VARIABLE (u4PktSize);    /* avoid compiler warning */
    return CFA_FAILURE;
}

INT4
CfaProcessPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex, UINT4 u4PktLen)
{

    UINT1               u1IfType;
#ifdef FSB_WANTED
    UINT2               u2VlanId = 0;
#endif

    /* Check if higher layer registered. */
    if (CFA_GDD_HL_RX_FNPTR ((UINT2) u4IfIndex) == NULL)
    {
        CFA_IF_SET_IN_DISCARD (u4IfIndex);    /* No higher layer */
        return (CFA_FAILURE);
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

#ifdef FSB_WANTED
    if (FsbProcessFsbFrame (pBuf, u4IfIndex, u2VlanId) == FSB_SUCCESS)
    {
        return CFA_SUCCESS;
    }
#endif

    /* If higher layer registered, give packet to it. */
    if ((CFA_GDD_HL_RX_FNPTR (u4IfIndex))
        (pBuf, u4IfIndex, u4PktLen, u1IfType, CFA_ENCAP_NONE) != CFA_SUCCESS)
    {
        return (CFA_FAILURE);
    }
    return (CFA_SUCCESS);
}

UINT1
CfaGddGetLinkStatus (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_IF_UP;
}

INT4
NslGetPortFromIfIndex (UINT4 u4IfIndex)
{
    INT4                i4Port;

    /* Scan NSL interface array, match input ifIndex and validate */
    for (i4Port = 0; i4Port < NSL_MAX_PORTS; i4Port++)
    {
        if (gaNslPort[i4Port].u4IfIndex == (UINT4) u4IfIndex)
        {
            return i4Port;
        }
    }

    return -1;
}

/*****************************************************************************
 *
 *    Function Name       :CfaGddTxPktOnVlanMemberPorts 
 *
 *    Description         :This routine transmits the given Frame on the member
 *                         ports of the given Vlan.
 *
 *    Input(s)            : u4L2ContextId - L2 Context Id to which VlanId is 
 *                                          associated
 *                        : pu1DataBuf  - Pointer to the frame buffer 
 *                          u4PktSize   - Size of the frame 
 *                          VlanId      - Vlan on which the frame is to be 
 *                                        transmitted
 *                          bBcast      - Whether this frame is to be 
 *                                        broadcasted  
 *
 *    Output(s)           : None
 *
 *    Global Variables Referred : None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/ CFA_FAILURE.
 *****************************************************************************/

INT4
CfaGddTxPktOnVlanMemberPortsInCxt (UINT4 u4L2ContextId,
                                   UINT1 *pu1DataBuf, tVlanId VlanId,
                                   BOOL1 bBcast, UINT4 u4PktSize)
{
    tPortList          *pTagPorts = NULL;
    tPortList          *pUnTagPorts = NULL;
    tMacAddr            DestAddr;
    tCfaVlanInfo        VlanInfo;
    UINT4               u4BridgeMode;
#ifdef NPAPI_WANTED
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2EtherType = 0;
    UINT1               u1PortFlag;
#endif
    UINT4               u4OutPort;
    BOOL1               bIsTag;
    INT4                i4RetVal;
    UINT1              *pu1TempBuf = NULL;
    UINT1               TempDataBuf[CFA_ENET_MIN_UNTAGGED_FRAME_SIZE];
    UINT4               u4MinFrameMtu = 0;
    INT4                i4RetStat = CFA_SUCCESS;
    UINT1               u1PaddingDone = CFA_FALSE;
    MEMCPY (DestAddr, pu1DataBuf, CFA_ENET_ADDR_LEN);

    MEMSET (TempDataBuf, 0, sizeof (TempDataBuf));
    VlanInfo.u2VlanId = VlanId;
#ifdef NPAPI_WANTED
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
#ifdef MBSM_WANTED
        /* If the packet belongs to STACK IVR, then send the
         * packet via stack port using ATP*/
        if (VlanId == CFA_DEFAULT_STACK_VLAN_ID)
        {
            if (CfaMbsmNpTxOnStackInterface (pu1DataBuf, u4PktSize) ==
                FNP_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaUtilTxPktOnVlanMemberPorts - "
                         "Unable to tx on stack interface.\n");
                return CFA_FAILURE;
            }
            return CFA_SUCCESS;
        }
#endif
    }
#endif

    pTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        return CFA_FAILURE;
    }

    pUnTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pUnTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        return CFA_FAILURE;
    }

    MEMSET (*pTagPorts, 0, sizeof (tPortList));
    MEMSET (*pUnTagPorts, 0, sizeof (tPortList));
    i4RetVal = VlanIvrGetTxPortOrPortListInCxt (u4L2ContextId,
                                                DestAddr, VlanId, bBcast,
                                                &u4OutPort, &bIsTag, *pTagPorts,
                                                *pUnTagPorts);

    if (i4RetVal == VLAN_FORWARD)
    {
        /*ASSUMPTION: The Packet reached here will be untagged always.
         * Since bcoz this thread is called only for the outgoing packet
         * from IP.*/

        u4MinFrameMtu = CFA_ENET_MIN_UNTAGGED_FRAME_SIZE;

        if (u4PktSize < u4MinFrameMtu)
        {
            pu1TempBuf = pu1DataBuf;
            MEMCPY (TempDataBuf, pu1DataBuf, u4PktSize);
            pu1DataBuf = TempDataBuf;
            u4PktSize = u4MinFrameMtu;
            u1PaddingDone = CFA_TRUE;
        }

        CfaTxFillVlanInfo (DestAddr, u4OutPort,
                           bIsTag, *pTagPorts, *pUnTagPorts, &VlanInfo);
        if (L2IwfGetBridgeMode (L2IWF_DEFAULT_CONTEXT, &u4BridgeMode) ==
            L2IWF_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaGddWrite - "
                     "L2IWF-GetBridgeMode -  FAILURE.\n");
            i4RetStat = CFA_FAILURE;
        }
        else
        {
#ifdef NPAPI_WANTED
            if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
            {
                if ((u4BridgeMode == L2IWF_CUSTOMER_BRIDGE_MODE) ||
                    (u4BridgeMode == L2IWF_PROVIDER_BRIDGE_MODE))
                {
                    if (CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo)
                        != FNP_SUCCESS)
                    {
                        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                 "Error in CfaGddWrite - "
                                 "Unsuccessful Driver Write -  FAILURE.\n");
                        i4RetStat = CFA_FAILURE;
                    }
                }
                else if ((u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE) ||
                         (u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE))
                {
                    if (u4OutPort != VLAN_INVALID_PORT)
                    {
                        if (CfaHwL3VlanIntfWrite
                            (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                        {
                            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                     "Error in CfaGddWrite - "
                                     "Unsuccessful Driver Write -  FAILURE.\n");
                            i4RetStat = CFA_FAILURE;
                        }
                    }
                    else
                    {
                        for (u2ByteInd = 0; u2ByteInd < BRG_PORT_LIST_SIZE;
                             u2ByteInd++)
                        {
                            if ((*pTagPorts)[u2ByteInd] == 0)
                            {
                                continue;
                            }

                            u1PortFlag = (*pTagPorts)[u2ByteInd];

                            for (u2BitIndex = 0;
                                 ((u2BitIndex < BITS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
                            {
                                if ((u1PortFlag & 0x80) != 0)
                                {
                                    VlanInfo.u2PktType = CFA_NP_KNOWN_UCAST_PKT;

                                    VlanInfo.unPortInfo.TxUcastPort.u2TxPort =
                                        (UINT2) ((u2ByteInd * BITS_PER_BYTE) +
                                                 u2BitIndex + 1);

                                    u4OutPort =
                                        VlanInfo.unPortInfo.TxUcastPort.
                                        u2TxPort;

#ifdef VLAN_WANTED
                                    VlanGetPortEtherType (u4OutPort,
                                                          &u2EtherType);
#endif
                                    VlanInfo.unPortInfo.TxUcastPort.
                                        u2EtherType = u2EtherType;

                                    VlanInfo.unPortInfo.TxUcastPort.u1Tag =
                                        CFA_NP_TAGGED;

                                    CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize,
                                                          VlanInfo);
                                }
                                u1PortFlag = (UINT1) (u1PortFlag << 1);
                            }
                        }
                        MEMSET (VlanInfo.unPortInfo.TxPorts.pTagPorts, 0,
                                sizeof (tPortList));
                        /* 
                         * In 802.1ad Bridges, for untagged ports, the tag ethertype 
                         * need not be filled in based on the port ethertype. 
                         * Hence calling the API for packet transmission on L3 
                         * interfaces directly.
                         */
                        if (CfaHwL3VlanIntfWrite
                            (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                        {
                            i4RetStat = CFA_FAILURE;
                        }
                    }
                }
            }
#else
            if (CfaL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo) !=
                CFA_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddWrite - "
                         "Unsuccessful Driver Write -  FAILURE.\n");
                i4RetStat = CFA_FAILURE;

            }
#endif
        }
        if (u1PaddingDone == CFA_TRUE)
        {
            pu1DataBuf = pu1TempBuf;
        }
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pUnTagPorts));
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        return i4RetStat;
    }

    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pUnTagPorts));
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pTagPorts));
    FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
    FsUtilReleaseBitList ((UINT1 *) pTagPorts);
    return CFA_FAILURE;
}

/*****************************************************************************
*
*    Function Name        : NslReadTopology
*
*    Description          : This function is to read the topolgy file and do the
*                           necessary initializations
*
*    Input(s)             : None
*
*    Output(s)            : None
*
*    Returns              : CFA_SUCCESS/CFA_FAILURE
*****************************************************************************/
static INT4
NslReadTopology (VOID)
{
    MEMSET (&Networks[0], 0, sizeof (Networks));

    if (NslReadTopFile (CONF_FILENAME) != CFA_SUCCESS)
    {
        return CFA_FAILURE;
    }
    if (NslOpenExtLinks () != CFA_SUCCESS)
    {
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
*
*    Function Name        : NslReadTopFile
*
*    Description          : This function is to read the topolgy file and update the
*                           network and link informations to the database
*
*    Input(s)             : pc1TopfFile - Topology file
*
*    Output(s)            : None
*
*    Returns              : CFA_SUCCESS/CFA_FAILURE
*****************************************************************************/
static INT4
NslReadTopFile (CHR1 * pc1TopfFile)
{
    FILE               *fp = NULL;
    INT4                i4Switch = 0;
    INT4                i4Port = 0;
    CHR1                au1Line[MAX_LINE_LENGTH];
    CHR1                au1CurrentNetwork[NETWORK_NAMELEN];
    CHR1                au1IfName[IFNAME_LEN];
    UINT1               u1LineNum = 0;

    MEMSET (au1Line, 0, MAX_LINE_LENGTH);
    MEMSET (au1CurrentNetwork, 0, NETWORK_NAMELEN);
    MEMSET (au1IfName, 0, IFNAME_LEN);
    fp = fopen (pc1TopfFile, "r");

    if (!fp)
    {
        return CFA_FAILURE;;
    }

    while (fgets (au1Line, 160, fp))
    {
        u1LineNum++;

        tolowerstr (au1Line);

        if (is_blankline (au1Line))
        {
            continue;
        }
        if (is_comment (au1Line))
        {
            continue;
        }
        if (is_ipaddress (au1Line))
        {
            continue;
        }
        if (is_network (au1Line))
        {
            sscanf (au1Line, "network %s", au1CurrentNetwork);
            NslAddNetwork (au1CurrentNetwork);
            continue;
        }
        if (is_interface (au1Line))
        {
            sscanf (au1Line, "interface %s", au1IfName);
            NslAddInterfaceToNetwork (au1IfName, au1CurrentNetwork);
            continue;
        }
        if (sscanf (au1Line, "%d %d", &i4Switch, &i4Port) == 2)
        {
            /* switch number cannot be more than MAX_RTRS*sizeof(uchar), as
             * the bitmap that is used to store the ipaddress, would overflow.
             * Check for this condition here.
             */
            if (((UINT4) i4Switch > MAX_SWITCH_NUMBER)
                || (UINT4) (i4Switch < 1))
            {
                fprintf (stderr, "ERROR:%d: the switch number must be "
                         "between 1 and %d\n", u1LineNum,
                         (INT4) MAX_SWITCH_NUMBER);
                return CFA_FAILURE;
            }
            NslAddLinkToNetwork (au1CurrentNetwork, i4Switch, i4Port);
            continue;
        }

        fprintf (stderr, "ERROR:%d: unable to parse: %s \n", u1LineNum,
                 au1Line);
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
*
*    Function Name        : NslAddNetwork
*
*    Description          : This function is to add the new network information 
*                           present in the topology
*
*    Input(s)             : pu1Network - Network name
*
*    Output(s)            : None
*
*    Returns              : 0 if SUCCESS/-1 if FAILURE
*****************************************************************************/
static INT4
NslAddNetwork (CHR1 * pu1Network)
{
    INT4                i4Index = 0;

    if (NslNetworkExists (pu1Network) >= 0)
    {
        return 0;
    }

    for (i4Index = 0; i4Index < MAX_NETWORKS; i4Index++)
    {
        if (Networks[i4Index].networkName[0] == 0)
        {
            memset (&Networks[i4Index], 0, sizeof (Networks[i4Index]));
            strcpy (Networks[i4Index].networkName, pu1Network);
            return 0;
        }
    }
    return -1;
}

/*****************************************************************************
*
*    Function Name        : NslNetworkExists
*
*    Description          : This function is to check whether a particular
*                           network information is already present or not
*
*    Input(s)             : pu1Network - Network name
*
*    Output(s)            : None
*
*    Returns              : Valid Network Id if SUCCESS/-1 if FAILURE
*****************************************************************************/
static INT4
NslNetworkExists (CHR1 * pu1Network)
{
    INT4                i4Index = 0;

    for (i4Index = 0; i4Index < MAX_NETWORKS; i4Index++)
    {
        if (strcmp (Networks[i4Index].networkName, pu1Network) == 0)
        {
            return i4Index;
        }
    }
    return -1;
}

/*****************************************************************************
*
*    Function Name        : NslAddLinkToNetwork
*
*    Description          : This function is to add the new link information 
*                           to a particular network present in the topolgy
*
*    Input(s)             : pu1Network - Network name
*                           i4Switch   - Swicth number
*                           i4Port     - Port number
*
*    Output(s)            : None
*
*    Returns              : 0 if SUCCESS/-1 if FAILURE
*****************************************************************************/
static INT4
NslAddLinkToNetwork (CHR1 * pu1Network, INT4 i4Switch, INT4 i4Port)
{
    INT4                i4NetworkId = -1;
    INT4                i4Index = 0;
    struct Link        *pLinks = NULL;

    if ((i4NetworkId = NslNetworkExists (pu1Network)) < 0)
    {
        return -1;
    }

    Networks[i4NetworkId].noOfLinks++;
    pLinks = Networks[i4NetworkId].Links;
    for (i4Index = 0; i4Index < MAX_LINKS; i4Index++)
    {
        if (pLinks[i4Index].fl == 0)
        {
            pLinks[i4Index].ss = i4Switch;
            pLinks[i4Index].sp = i4Port;
            pLinks[i4Index].fl = 1;    /* flag as used */
            pLinks[i4Index].st = LINK_STATUS_UP;    /*     admin up */
            return 1;
        }
    }
    return 0;
}

/*****************************************************************************
*
*    Function Name        : NslAddInterfaceToNetwork
*
*    Description          : This function is to add the external interface to the
*                           network information 
*
*    Input(s)             : pu1IfName - Interface name
*                           pu1Network - Network name
*
*    Output(s)            : None
*
*    Returns              : 0 if SUCCESS/-1 if FAILURE
*****************************************************************************/
static INT4
NslAddInterfaceToNetwork (CHR1 * pu1IfName, CHR1 * pu1Network)
{
    INT4                i4Index = 0;
    INT4                i4NetworkId = -1;
    struct LinkExt     *pExtLink = NULL;

    if ((i4NetworkId = NslNetworkExists (pu1Network)) < 0)
    {
        printf ("-E-: Cannot find network %s while adding interface %s\n",
                pu1Network, pu1IfName);
        return -1;
    }

    pExtLink = &(Networks[i4NetworkId].LinkExt);

    if ((i4Index = NslInterfaceExists (pu1IfName)) >= 0)
    {
        printf ("-E-: Interface %s already part of network %s\n",
                pu1IfName, Networks[i4Index].networkName);
        return -1;
    }

    strcpy (pExtLink->ifname, pu1IfName);
    pExtLink->st = 1;            /*     admin up */
    pExtLink->sock = -1;
    return 0;
}

/*****************************************************************************
*
*    Function Name        : NslInterfaceExists
*
*    Description          : This function is to check whether the external interface 
*                           already exists or not
*
*    Input(s)             : pu1IfName - Interface name
*
*    Output(s)            : None
*
*    Returns              : Valid Network Id if SUCCESS/-1 if FAILURE
*****************************************************************************/
static INT4
NslInterfaceExists (CHR1 * pu1IfName)
{
    INT4                i4Index = 0;

    for (i4Index = 0; i4Index < MAX_NETWORKS; i4Index++)
    {
        if (Networks[i4Index].networkName[0])
        {
            if (strcmp (Networks[i4Index].LinkExt.ifname, pu1IfName) == 0)
            {
                return i4Index;
            }
        }
    }
    return -1;
}

/*****************************************************************************
*
*    Function Name        : NslFindLinkNetwork
*
*    Description          : This function is to find the network from the 
*                           Rx port and switch number
*
*    Input(s)             : i2Port - Rx Port
*
*    Output(s)            : None
*
*    Returns              : Valid Network Id if SUCCESS/-1 if FAILURE
*****************************************************************************/
static INT4
NslFindLinkNetwork (INT2 i2Port)
{
    INT4                i4NetworkId = 0;
    INT4                i4LinkId = 0;
    struct Link        *pLink = NULL;

    for (i4NetworkId = 0; i4NetworkId < MAX_NETWORKS; i4NetworkId++)
    {
        if (Networks[i4NetworkId].networkName[0])
        {
            for (i4LinkId = 0; i4LinkId < MAX_LINKS; i4LinkId++)
            {
                pLink = &Networks[i4NetworkId].Links[i4LinkId];
                if (pLink->st && pLink->fl && (pLink->ss == (INT4) gu2NslSw)
                    && (pLink->sp == i2Port))
                {
                    return i4NetworkId;
                }
            }
        }
    }
    return -1;
}

/*****************************************************************************
*
*    Function Name        : NslOpenExtLinks
*
*    Description          : This function is to identify and trigger the new RAW socket 
*                           creation for a particular network in the topology
*
*    Input(s)             : None
*
*    Output(s)            : None
*
*    Returns              : CFA_SUCCESS/CFA_FAILURE
*****************************************************************************/
static INT4
NslOpenExtLinks (VOID)
{
    INT4                i4Index = 0;

    FD_ZERO (&allFd);

    for (i4Index = 0; i4Index < MAX_NETWORKS; i4Index++)
    {
        if (Networks[i4Index].networkName[0] != 0)
        {
            if (Networks[i4Index].LinkExt.ifname[0] == 0)
            {
                continue;
            }

            NslOpenEnetSocket (&Networks[i4Index]);

            if (Networks[i4Index].LinkExt.sock >= 0)
            {
                FD_SET (Networks[i4Index].LinkExt.sock, &allFd);
                if (Networks[i4Index].LinkExt.sock > gi4MaxSocketId)
                {
                    gi4MaxSocketId = Networks[i4Index].LinkExt.sock;
                }
            }
            else
            {
                PRINTF ("-E-: Cannot open socket for network %s (%s)\n",
                        Networks[i4Index].networkName,
                        Networks[i4Index].LinkExt.ifname);
                return CFA_FAILURE;
            }
        }
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
*
*    Function Name        : NslOpenEnetSocket
*
*    Description          : This function is to open a new RAW socket and bind it 
*                           to the each external interface present in the topology file
*
*    Input(s)             : pNetwork - Pointer to the network information
*
*    Output(s)            : None
*
*    Returns              : Valid Sock Descriptor if SUCCESS/if FAILURE -1
*****************************************************************************/
static INT4
NslOpenEnetSocket (struct network *pNetwork)
{
    struct sockaddr_ll  Enet;
    INT4                i4Sock;
    INT4                i4RetVal;
    struct ifreq        ifr;    /* struct which contains status of interface */
    INT4                i4CommandStatus;
    CHR1               *pEthName = pNetwork->LinkExt.ifname;

    MEMSET (&Enet, 0, sizeof (struct sockaddr_ll));
    MEMSET (&ifr, 0, sizeof (struct ifreq));

    if ((i4RetVal = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_ALL))) < 0)
    {
        perror ("Error in opening port\n");
        return i4RetVal;
    }

    i4Sock = i4RetVal;

    /* get ethX interface ifindex */
    strcpy (ifr.ifr_name, pEthName);
    if ((i4CommandStatus = ioctl (i4Sock, SIOCGIFINDEX, &ifr)) < 0)
    {
        perror ("Error in getting interface flags\n");
        close (i4Sock);
        return -1;
    }
    Enet.sll_family = AF_PACKET;

    pNetwork->LinkExt.ifindex = Enet.sll_ifindex = ifr.ifr_ifindex;

    if ((i4RetVal = bind (i4Sock, (struct sockaddr *) &Enet,
                          sizeof (struct sockaddr_ll))) < 0)
    {
        perror ("Bind error\n");
        close (i4Sock);
        return i4RetVal;
    }
    /* set options for non-blocking mode */
    if ((i4RetVal = fcntl (i4Sock, F_SETFL, O_NONBLOCK)) < 0)
    {
        perror ("Failure in setting ETH SOCK option\n");
        close (i4Sock);
        return i4RetVal;
    }

    /* get ethX interface flags */
    strcpy (ifr.ifr_name, pEthName);
    if ((i4CommandStatus = ioctl (i4Sock, SIOCGIFFLAGS, &ifr)) < 0)
    {
        perror ("Error in getting interface flags\n");
        close (i4Sock);
        return -1;
    }
    /* set the IFF_PROMISC flag to enter promiscuous mode and set ethX
     * interface flags */
    ifr.ifr_flags = ifr.ifr_flags | IFF_PROMISC;
    ifr.ifr_flags = ifr.ifr_flags | IFF_UP;
    if ((i4CommandStatus = ioctl (i4Sock, SIOCSIFFLAGS, &ifr)) < 0)
    {
        perror ("Error in setting promisc mode\n");
        close (i4Sock);
        return -1;
    }

    pNetwork->LinkExt.sock = i4Sock;
    return i4Sock;
}

/*****************************************************************************
*
*    Function Name        : NslExtSend
*
*    Description          : This function is to transmit the packets to the
*                           external interfaces present in the topology file
*
*    Input(s)             : pu1Buf - Pointer to the packet buffer
*                           u4Len  - length of the packet
*                           i4Sock - Socket ID in which the pkt has to be tx
*                           i4IfIndex - Interface index through which the pkt
*                                       has to be transmitted
*
*    Output(s)            : None
*
*    Returns              : No of bytes written if SUCCESS/-1 if FAILURE
*****************************************************************************/
static INT4
NslExtSend (UINT1 *pu1Buf, UINT4 u4Len, INT4 i4Sock, INT4 i4IfIndex)
{
    struct sockaddr_ll  Enet;
    UINT4               u4WrittenBytes = 0;

    memset (&Enet, 0, sizeof (Enet));
    Enet.sll_family = AF_PACKET;
    Enet.sll_ifindex = i4IfIndex;

    if ((u4WrittenBytes = sendto (i4Sock, (void *) pu1Buf, u4Len,
                                  0, (struct sockaddr *) &Enet,
                                  (socklen_t) sizeof (Enet))) != u4Len)
    {
        perror ("MS_ExtSend: sendto");
        return -1;
    }
    return u4WrittenBytes;
}

/*****************************************************************************
*
*    Function Name        : NslFindRxPort
*
*    Description          : This function is to find the port on which the packet
*                           is received
*
*    Input(s)             : i4NetworkId - Network Id
*
*    Output(s)            : None
*
*    Returns              : RX-Port if SUCCESS/Zero if FAILURE
*****************************************************************************/
static INT4
NslFindRxPort (INT4 i4NetworkId)
{
    INT4                i4LinkId = 0;
    struct Link        *pLink = NULL;

    for (i4LinkId = 0; i4LinkId < MAX_LINKS; i4LinkId++)
    {
        pLink = &Networks[i4NetworkId].Links[i4LinkId];
        if (pLink->st && pLink->fl && (pLink->ss == (INT4) gu2NslSw))
        {
            return pLink->sp;
        }
    }
    return 0;
}

/*****************************************************************************
*
*    Function Name        : CfaProcessRxPacket
*
*    Description          : This function is to process the received packet
*
*    Input(s)             : ppBuf - Double pointer to the CRU-BUFFER
*                           pData  - Pointer to the starting point of valid Data
*                           u2RxSw - Rx Switch
*                           u2RxPort - Rx Port
*                           i2PktLen - Packet length
*
*    Output(s)            : None
*
*    Returns              : None
*****************************************************************************/
static VOID
CfaProcessRxPacket (tCRU_BUF_CHAIN_HEADER ** ppBuf, UINT1 *pData,
                    UINT2 u2RxSw, UINT2 u2RxPort, INT2 i2PktLen)
{
    UINT4               u4IfIndex = 0;
    tEnetV2Header      *pHdr = NULL;
    tEnetSnapHeader    *pSnapHdr = NULL;
    UINT2               u2Protocol = 0;
    UINT2               u2LenOrType = 0;
    INT2                i2HdrLen = 0;
    INT2                i2IpPktLen = 0;
    UINT4               u4PktLen = 0;
    UINT1               u1SubType = CFA_SUBTYPE_SISP_INTERFACE;
#ifdef OPENFLOW_WANTED
    UINT4               u4VlanIndex = 0;
#endif

    if (IS_NSL_DEBUG_PKT_EVENT (gu4NslDebug) &&
        (!IS_NSL_DEBUG_PKT_DUMP (gu4NslDebug)))
    {
        u4IfIndex = (UINT4) (gaNslPort[u2RxPort - 1].u4IfIndex);
        printf ("NSL: PktEv: Rx sw: %d, port: %d, ifIndex: %d, Size: %d\n",
                (int) u2RxSw, (int) (u2RxPort - 1), (int) u4IfIndex,
                (int) i2PktLen);
    }

    if ((u2RxPort == 0) && (u2RxSw == 0))
    {
        /* this is error; control messages are handled only at the
         * packet server
         */
        NslHandlePktSrvCtrlMsg (pData);
        CRU_BUF_Release_MsgBufChain (*ppBuf, FALSE);
        *ppBuf = NULL;
        return;

    }

    pData += 4;
    if ((u2RxPort > NSL_MAX_PORTS) || (u2RxSw != gu2NslSw) || (u2RxPort == 0))
    {
        CRU_BUF_Release_MsgBufChain (*ppBuf, FALSE);
        *ppBuf = NULL;
        return;
    }

    u2RxPort--;
    u4IfIndex = (UINT4) (gaNslPort[u2RxPort].u4IfIndex);

    if ((gaNslPort[u2RxPort].u1PortState == NSL_DOWN) ||
        (gaNslPort[u2RxPort].u1AppState == NSL_DOWN))
    {
        if (IS_NSL_DEBUG_PKT_EVENT (gu4NslDebug))
        {
            printf ("    i/f down; discarding packet\n");
        }
        CRU_BUF_Release_MsgBufChain (*ppBuf, FALSE);
        *ppBuf = NULL;
        return;
    }
    /* check if the interface exists */
    if ((CFA_IF_ENTRY (u4IfIndex) == NULL) ||
        (CFA_IF_RS (u4IfIndex) != CFA_RS_ACTIVE))
    {
        if (IS_NSL_DEBUG_PKT_EVENT (gu4NslDebug))
        {
            printf ("    no i/f; discarding packet\n");
        }
        CRU_BUF_Release_MsgBufChain (*ppBuf, FALSE);
        *ppBuf = NULL;
        return;
    }
    pHdr = (tEnetV2Header *) (pData);
    u2LenOrType = OSIX_NTOHS (pHdr->u2LenOrType);
    if (CFA_ENET_IS_TYPE (u2LenOrType))
    {
        u2Protocol = u2LenOrType;
        i2HdrLen = CFA_ENET_V2_HEADER_SIZE;
    }
    else
    {
        pSnapHdr = (tEnetSnapHeader *) pData;
        i2HdrLen = CFA_ENET_SNAP_HEADER_SIZE;

        /* check for LLC control frame first - we expect only LLC in SNAP now */
        if (pSnapHdr->u1Control == CFA_LLC_CONTROL_UI)
        {
            /*check for presence of SNAP which may carry IP/ARP/RARP after LLC */
            if ((pSnapHdr->u1DstLSap == CFA_LLC_SNAP_SAP) ||
                (pSnapHdr->u1SrcLSap == CFA_LLC_SNAP_SAP) ||
                (pSnapHdr->u1Oui1 == 0x00) ||
                (pSnapHdr->u1Oui2 == 0x00) || (pSnapHdr->u1Oui3 == 0x00))
            {
                /* determine which protocol is being carried */
                u2Protocol = OSIX_NTOHS (pSnapHdr->u2ProtocolType);
            }                    /* end of SNAP framing */
        }                        /* end of LLC framing */
    }

    /* u2Protocol = 0xFF; */
    if (u2Protocol == CFA_ENET_IPV4)
    {
        i2IpPktLen = (INT2) *(INT2 *) (pData + i2HdrLen + IP_PKT_OFF_LEN);
        i2IpPktLen = OSIX_NTOHS (i2IpPktLen);
        u4PktLen = (UINT4) (i2IpPktLen + i2HdrLen);
    }
#ifdef IP6_WANTED
    else if (u2Protocol == CFA_ENET_IPV6)
    {
        i2IpPktLen = (INT2) *(INT2 *) (pData + i2HdrLen + IPV6_OFF_PAYLOAD_LEN);
        i2IpPktLen = OSIX_NTOHS (i2IpPktLen);
        u4PktLen = (UINT4) (i2IpPktLen + i2HdrLen + IPV6_HEADER_LEN);
    }
#endif /* IP6_WANTED */
    else
    {
        u4PktLen = i2PktLen;    /* leaving out the simulation header */
    }

    CFA_IF_SET_IN_OCTETS ((UINT2) u4IfIndex, u4PktLen);
#ifdef RMON_WANTED
    /* RMON code should process only if SW_FWD enabled.         */
    /* Update rmon tables. Return value doesn't matter.         */
    /* Routine should be called only for ethernet interfaces.   */
    /* We avoid checking because we support only ethernet.      */
    CfaIwfRmonUpdateTables (pData, (UINT2) u4IfIndex, u4PktLen);
#endif /* RMON_WANTED */
    if (IS_NSL_DEBUG_PKT_DUMP (gu4NslDebug))
    {
        CHR1                Tmp[80];

        sprintf (Tmp, "NSL: PktDump: Rx Sw: %d, port: %d, ifIndex: %d\n",
                 (int) u2RxSw, (int) u2RxPort, (int) u4IfIndex);
        NslBufDump (pData, (UINT1 *) Tmp, u4PktLen);
    }

#ifdef OPENFLOW_WANTED
    if (OfcHandleVlanPkt (*ppBuf, &u4VlanIndex) == OSIX_SUCCESS)
    {
        if (OpenflowNotifyPktRecv (u4VlanIndex, *ppBuf) == OFC_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (*ppBuf, FALSE);
        }
        *ppBuf = NULL;
        return;
    }
#endif /* OPENFLOW_WANTED */

    CfaGetIfSubType ((UINT4) u4IfIndex, &u1SubType);
    if (u1SubType == CFA_SUBTYPE_OPENFLOW_INTERFACE)
    {
        CFA_IF_SET_IN_OCTETS (u4IfIndex, u4PktLen);
        CFA_IF_SET_IN_UCAST (u4IfIndex);
#ifdef OPENFLOW_WANTED
        /* 
         * Make the openflow ports visble to the ISS context  when the Client context 
         * associated with the IfIndex is in fail stand alone mode.Else process the 
         * packets through the Openflow Pipeline Process.
         */
        if (OfcHandleOpenflowSwModeProcess (&u4IfIndex, *ppBuf) != OFC_SUCCESS)
        {
            if (OpenflowNotifyPktRecv (u4IfIndex, *ppBuf) == OFC_FAILURE)
            {
                CfaIfSetInDiscard (u4IfIndex);
                CRU_BUF_Release_MsgBufChain (*ppBuf, FALSE);
            }
        }
        else
        {
            if (CfaProcessPacket (*ppBuf, u4IfIndex, u4PktLen) != CFA_SUCCESS)
            {
                CRU_BUF_Release_MsgBufChain (*ppBuf, FALSE);
            }
        }
#endif /* OPENFLOW_WANTED */
    }
    else
    {
        if (CfaProcessPacket (*ppBuf, u4IfIndex, u4PktLen) != CFA_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (*ppBuf, FALSE);
        }
    }
    *ppBuf = NULL;

    return;
}

#ifdef WLC_WANTED
INT4
CfaGddWssOpen (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

INT4
CfaGddWssClose (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddWssWrite
 *
 *    Description        : Writes the data to the ethernet driver
 *                         by constructing CAPWAP messages and posts from
 *                         CFA to WSS
 *
 *    Input(s)            : pu1DataBuf - Pointer to the linear buffer.
 *                          u4IfIndex - MIB-2 interface index
 *                          u4PktSize - Size of the buffer.
 *
 *    Output(s)            : None.
 *
 *    Returns            : CFA_SUCCESS if write succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaGddWssWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    UINT1               u1OperStatus = 0;
    unWlcHdlrMsgStruct *pcapwapMsgStruct = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    UINT2               u2EtherType = 0;

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0);
    if (pBuf == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaGddWssWrite: Unable to Allocate CRU Buffer \n");
        return CFA_FAILURE;
    }

    if (CRU_BUF_Copy_OverBufChain (pBuf, pu1DataBuf, 0, u4PktSize) ==
        CRU_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaGddWssWrite: Unable to copy to CRU Buffer \n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return CFA_FAILURE;
    }

    CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
    if (u1OperStatus != CFA_IF_UP)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaGddWssWrite: The VAP interface is not Operationally UP\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return CFA_FAILURE;
    }

    /*Vlan tag is added when the packet is sent frmo WSS to CFA */
    /*Similarly Untag the frame before giving the packet to WSS */
    VLAN_LOCK ();
    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPort) !=
        VLAN_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaGddWssWrite: VlanGetContextInfoFromIfIndex FAILED\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        VLAN_UNLOCK ();
        return VLAN_FAILURE;

    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaGddWssWrite: VlanSelectContext FAILED\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    /*Fetch the etherType of the packet .Ethertype is present after Dest Mac + Src Mac */

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2EtherType,
                               (CFA_ENET_ADDR_LEN * 2), sizeof (u2EtherType));

    u2EtherType = OSIX_NTOHS (u2EtherType);

    if ((VlanClassifyFrame (pBuf, u2EtherType, u2LocalPort) != VLAN_UNTAGGED))
    {
        VlanUnTagFrame (pBuf);
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    pcapwapMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    MEMSET (pcapwapMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    pcapwapMsgStruct->WlcHdlrQueueReq.pRcvBuf = pBuf;
    pcapwapMsgStruct->WlcHdlrQueueReq.u2SessId = u4IfIndex;
    pcapwapMsgStruct->WlcHdlrQueueReq.u4MsgType = WLCHDLR_CFA_RX_MSG;

    /* Invoke the WSSIF module to send the packet to MAC Handler module */
    /* Posting the packet from CFA to WSS for sending Tx capwap message */
    if (WlcHdlrEnqueCtrlPkts (pcapwapMsgStruct) != OSIX_SUCCESS)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_L2,
                 "L2 Packet processing in WSS failed for VAP Index\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pcapwapMsgStruct);
        return CFA_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pcapwapMsgStruct);
    return CFA_SUCCESS;

}

INT4
CfaGddWssRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4PktSize);
    return CFA_SUCCESS;
}
#endif
