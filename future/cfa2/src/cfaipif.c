/*
 *  $Id: cfaipif.c,v 1.80 2018/02/02 09:48:08 siva Exp $
 */

#include "cfainc.h"
#include "rtm.h"
#ifdef LNXIP4_WANTED
#include "lnxip.h"
#endif
#include "ipvx.h"

extern UINT4        IfSecondaryIpRowStatus[12];
PRIVATE INT4        CfaIpIfUtilRBTFreeNodeFn (tRBElem * pRBElem, UINT4 u4Arg);

#ifdef LNXIP4_WANTED
/*****************************************************************************
*    Function Name            : CfaHandleInBoundPktToLnxIpTap
*
*    Description              : This Collects IP Packets and Post them to LNX
                                TAP Module 
*    Input(s)                 : Cfa Iff Idx, IP Packet Buffer and Ptr To  
*                               Ethernet Header, IPv4 or IPv6 
*
*    Output(s)                : None.
*    
*    Returns                  : NONE 
*****************************************************************************/
VOID
CfaHandleInBoundPktToLnxIpTap (UINT4 u4IfIdx, tCRU_BUF_CHAIN_HEADER * pBuf,
                               tEnetV2Header * pEthHdr, UINT1 u1Flag)
{
    LnxIpPostPktToTap (u4IfIdx, pBuf, pEthHdr, u1Flag);
    return;
}

/*****************************************************************************
*    Function Name            : CfaHandleOutBoundPktFromLnxIpTap
*
*    Description              : This Collects IP Packets and Passes them to
                                IWF Module 
*    Input(s)                 : Ptr to Pkt Buf, Cfa Iff Idx, Lenth of Packet  
*
*    Output(s)                : None.
*    
*    Returns                  : NONE 
*****************************************************************************/

VOID
CfaHandleOutBoundPktFromLnxIpTap (tCRU_BUF_CHAIN_HEADER * pBuf, INT4 i4If,
                                  UINT4 u4Size)
{
    UINT1               u1IfType;

    CfaGetIfType (i4If, &u1IfType);

    if (u1IfType == CFA_PPP)
    {
	CRU_BUF_Move_ValidOffset (pBuf, CFA_ENET_V2_HEADER_SIZE);	
    }

    if (CfaProcessOutgoingIpPktForIwf (pBuf, i4If, u4Size) != CFA_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }
    return;
}
#endif

#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
PRIVATE VOID        CfaIpIfCreateorDeleteSecondaryAddress (UINT4 u4IfIndex,
                                                           tIpAddrInfo *
                                                           pSecIpInfo,
                                                           UINT1 u1Flag);
#endif /* IP_WANTED || LNXIP4_WANTED */
/*****************************************************************************
*    Function Name            : CfaIpIfCreateIpIfTable
*
*    Description              : This function creates the IP Interface Tree
                                and allocates memory for nodes containing IP 
*                               inforrmation 
*    Input(s)                  : None
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : gIpIfInfo
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfCreateIpIfTable ()
{

    /* Create the RB tree that holds the IP information of
     * the interface */
    gIpIfInfo.pIpIfTable = RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tIpIfRecord, RBNode), CfaIpIfCompareInterfaceIndex);

    if (gIpIfInfo.pIpIfTable == NULL)
    {
        return CFA_FAILURE;
    }

    gIpIfInfo.IpIntfPoolId =
        CFAMemPoolIds[MAX_CFA_IPIF_RECORDS_IN_SYSTEM_SIZING_ID];
    gIpIfInfo.SecondaryIpPool =
        CFAMemPoolIds[MAX_CFA_IPADDR_INFO_IN_SYSTEM_SIZING_ID];

    /* Create Semaphore for protecting IP interface table */
    if (OsixSemCrt (CFA_IP_IF_TBL_SEMAPHORE, &gIpIfInfo.SemId) != OSIX_SUCCESS)
    {
        return CFA_FAILURE;
    }
    OsixSemGive (gIpIfInfo.SemId);

    return CFA_SUCCESS;
}

/*****************************************************************************
*    Function Name            : CfaIpIfCompareInterfaceIndex
*
*    Description              : Comparision function used by IP Interface tree
*    
*    Input(s)                  : None
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfCompareInterfaceIndex (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT4               u4IfIndex1 = ((tIpIfRecord *) pRBElem1)->u4IfIndex;
    UINT4               u4IfIndex2 = ((tIpIfRecord *) pRBElem2)->u4IfIndex;

    if (u4IfIndex1 < u4IfIndex2)
    {
        return -1;
    }
    if (u4IfIndex1 > u4IfIndex2)
    {
        return 1;
    }
    return 0;
}

/*****************************************************************************
*    Function Name            : CfaIpIfDestroyIpIfTable
*
*    Description              : This function deletes the IP Interface Tree
                                and frees the nodes containing IP inforrmation 
*    Input(s)                  : None
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : gIpIfInfo
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfDestroyIpIfTable ()
{
    /* Delete all the interface infomration available in the Tree */
    if (gIpIfInfo.pIpIfTable != NULL)
    {
        RBTreeDestroy (gIpIfInfo.pIpIfTable, CfaIpIfUtilRBTFreeNodeFn, 0);
        gIpIfInfo.pIpIfTable = NULL;
    }

    /* Delete the semaphore created */
    if (gIpIfInfo.SemId != 0)
    {
        OsixSemDel (gIpIfInfo.SemId);
        gIpIfInfo.SemId = 0;
    }
    return CFA_SUCCESS;
}

/******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CfaIpIfUtilRBTFreeNodeFn 
 *                                                                          
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the RBTree
 *                                                                          
 *    INPUT            : pRBElem - pointer to the node to be freed. 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *                                                                          
*******************************************************************************/
PRIVATE INT4
CfaIpIfUtilRBTFreeNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    tIpIfRecord        *pIpIntf = NULL;
    tIpAddrInfo        *pSecondaryIpInfo;

    UNUSED_PARAM (u4Arg);

    /* Scan the secondary Address list and delete them */
    if (pRBElem != NULL)
    {
        pIpIntf = (tIpIfRecord *) pRBElem;

        while ((pSecondaryIpInfo = (tIpAddrInfo *)
                TMO_SLL_First (&(pIpIntf->SecondaryIpList))) != NULL)
        {
            TMO_SLL_Delete (&(pIpIntf->SecondaryIpList),
                            &pSecondaryIpInfo->NextNode);

            MemReleaseMemBlock (gIpIfInfo.SecondaryIpPool,
                                (UINT1 *) pSecondaryIpInfo);
        }

        MemReleaseMemBlock (gIpIfInfo.IpIntfPoolId, (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
*    Function Name            : CfaIpIfCreateIpInterface
*
*    Description              :  Creates IP interface node for the specified 
*                                interface
*    
*    Input(s)                  : u4CfaIfIndex - CFA Interface Index
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfCreateIpInterface (UINT4 u4CfaIfIndex)
{
    tIpIfRecord        *pIpIntf;
    INT4                i4RetVal = CFA_FAILURE;
    tIpIfRecord         IpIntfInfo;

    CFA_IPIFTBL_LOCK ();

    /* Get the IP interface node from RBTree */
    IpIntfInfo.u4IfIndex = u4CfaIfIndex;
    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
    if (pIpIntf == NULL)
    {
        /* Allocate the IP interface node from MemPool */
        if ((pIpIntf = (tIpIfRecord *) MemAllocMemBlk (gIpIfInfo.IpIntfPoolId))
            != NULL)
        {
            /* Initialize the interface node */
            MEMSET (pIpIntf, 0, sizeof (tIpIfRecord));
            pIpIntf->u4IfIndex = u4CfaIfIndex;

            UTL_SLL_Init (&(pIpIntf->SecondaryIpList),
                          FSAP_OFFSETOF (tIpAddrInfo, NextNode));
            /* Add the interface node to RB Tree */
            if (RBTreeAdd (gIpIfInfo.pIpIfTable, pIpIntf) == RB_SUCCESS)
            {
                i4RetVal = CFA_SUCCESS;
            }

            if (i4RetVal == CFA_FAILURE)
            {
                MemReleaseMemBlock (gIpIfInfo.IpIntfPoolId, (UINT1 *) pIpIntf);
            }
        }
    }
    else
    {
        i4RetVal = CFA_SUCCESS;
    }

    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
*    Function Name            : CfaIpIfDeleteIpInterface
*
*    Description              :  Delete the IP interface node for the specified 
*                                interface
*    
*    Input(s)                  : u4CfaIfIndex - CFA Interface Index
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfDeleteIpInterface (UINT4 u4CfaIfIndex)
{
    INT4                i4RetVal = CFA_FAILURE;
    tIpIfRecord         IpIntfInfo;
    tIpIfRecord        *pIpIntf;
    tIpAddrInfo        *pSecondaryIpInfo;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    CFA_IPIFTBL_LOCK ();

    /* Get the IP interface node from RBTree */
    IpIntfInfo.u4IfIndex = u4CfaIfIndex;
    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);

    if (pIpIntf != NULL)
    {
        /* Delete the interface node to RB Tree */
        RBTreeRemove (gIpIfInfo.pIpIfTable, pIpIntf);

        /* If any secondary address information available,Delete them */
        while ((pSecondaryIpInfo = (tIpAddrInfo *)
                TMO_SLL_First (&(pIpIntf->SecondaryIpList))) != NULL)
        {
            SnmpNotifyInfo.pu4ObjectId = IfSecondaryIpRowStatus;
            SnmpNotifyInfo.u4OidLen =
                sizeof (IfSecondaryIpRowStatus) / sizeof (UINT4);
            SnmpNotifyInfo.u4SeqNum = 0;
            SnmpNotifyInfo.u1RowStatus = TRUE;
            SnmpNotifyInfo.pLockPointer = NULL;
            SnmpNotifyInfo.pUnLockPointer = NULL;
            SnmpNotifyInfo.u4Indices = 2;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i", (INT4) u4CfaIfIndex,
                              pSecondaryIpInfo->u4Addr, CFA_RS_DESTROY));

            TMO_SLL_Delete (&(pIpIntf->SecondaryIpList),
                            &pSecondaryIpInfo->NextNode);

            MemReleaseMemBlock (gIpIfInfo.SecondaryIpPool,
                                (UINT1 *) pSecondaryIpInfo);
        }

        MemReleaseMemBlock (gIpIfInfo.IpIntfPoolId, (UINT1 *) pIpIntf);
        i4RetVal = CFA_SUCCESS;
    }
    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
*    Function Name            : CfaIpIfSetInfo
*
*    Description              :  Set the IP related parameters for a interface
*    
*    Input(s)                  : u4IfIndex - CFA interface Index 
*                                pIpInfo   - IP information to be set 
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfSetIfInfo (UINT4 u4IfIndex, UINT4 u4Flag, tIpConfigInfo * pIpInfo)
{
    UINT4               u4OldIpAddr;
    UINT4               u4OldNetMask;
    UINT4               u4ContextId;
    INT4                i4RetVal = CFA_SUCCESS;
    UINT1               u1NetChange = FALSE;
    tVlanId             u2VlanId = 0;
    tIpIfRecord         IpIntfInfo;
    tIpIfRecord        *pIpIntf = NULL;
    tIpAddrInfo        *pSecondaryIpInfo = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    tRtmRouteUpdateInfo IpRtInfo;
#endif
#if defined(NPAPI_WANTED) && !defined(NPSIM_WANTED)
    UINT1               au1MacAddress[MAC_ADDR_LEN];
    MEMSET (au1MacAddress, 0, MAC_ADDR_LEN);
#endif

    CFA_IPIFTBL_LOCK ();

    /* Get the IP interface node from RBTree */
    IpIntfInfo.u4IfIndex = u4IfIndex;

    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
    if (pIpIntf == NULL)
    {
        CFA_IPIFTBL_UNLOCK ();
        return CFA_FAILURE;
    }

    /* Protocol to be used for acquiring dynamic ip address */
    if (u4Flag & CFA_IP_IF_ALLOC_PROTO)
    {
        pIpIntf->u1AddrAllocProto = pIpInfo->u1AddrAllocProto;
    }

    /* Address allocation method (MANUAL/DYNAMIC) */
    if (u4Flag & CFA_IP_IF_ALLOC_METHOD)
    {
        pIpIntf->u1AddrAllocMethod = pIpInfo->u1AddrAllocMethod;
        /* When the mode is changed to dynamic, Delete all the secondary 
         * address configured over the interface .As the allocation
         * method is set after putting rowstatus NOT_IN_SERVICE,
         * interface should have been deleted from IP*/
        if (pIpIntf->u1AddrAllocMethod == CFA_IP_ALLOC_POOL)
        {
            while ((pSecondaryIpInfo = (tIpAddrInfo *)
                    TMO_SLL_First (&(pIpIntf->SecondaryIpList))) != NULL)
            {
                SnmpNotifyInfo.pu4ObjectId = IfSecondaryIpRowStatus;
                SnmpNotifyInfo.u4OidLen =
                    sizeof (IfSecondaryIpRowStatus) / sizeof (UINT4);
                SnmpNotifyInfo.u4SeqNum = 0;
                SnmpNotifyInfo.u1RowStatus = TRUE;
                SnmpNotifyInfo.pLockPointer = NULL;
                SnmpNotifyInfo.pUnLockPointer = NULL;
                SnmpNotifyInfo.u4Indices = 2;
                SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i", (INT4) u4IfIndex,
                                  pSecondaryIpInfo->u4Addr, CFA_RS_DESTROY));
                TMO_SLL_Delete (&(pIpIntf->SecondaryIpList),
                                &pSecondaryIpInfo->NextNode);
                MemReleaseMemBlock (gIpIfInfo.SecondaryIpPool,
                                    (UINT1 *) pSecondaryIpInfo);
            }
        }
    }

    u4OldIpAddr = pIpIntf->u4IpAddr;
    u4OldNetMask = u4CidrSubnetMask[pIpIntf->u1SubnetMask];

    if (u4Flag & CFA_IP_IF_PRIMARY_ADDR)
    {
        /* Primary IP address of the interface */
        if (pIpIntf->u4IpAddr != pIpInfo->u4Addr)
        {
            pIpIntf->u4IpAddr = pIpInfo->u4Addr;
            pIpIntf->u1AddrType = IPVX_ADDR_TYPE_UNICAST;
            pIpIntf->u1SecAddrFlag = IPVX_TWO;
            pIpIntf->u1IpStatus = IPVX_ADDR_STATUS_PREFERRED;
            pIpIntf->u4IpLastChanged = OsixGetSysUpTime ();
            pIpIntf->u1IpStorageType = IPVX_STORAGE_TYPE_VOLATILE;
            pIpIntf->u1Origin = IPVX_ADDR_PREFIX_ORIGIN_MANUAL;
            pIpIntf->u1AddrRowStatus = ACTIVE;
            pIpIntf->u1PrefixRowStatus = ACTIVE;
            u1NetChange = TRUE;

            if (CFA_SUCCESS == CfaGetVlanId (u4IfIndex, &u2VlanId))
            {
                if (u2VlanId == IssGetDefaultVlanIdFromNvRam ())
                {
                    CFA_IPIFTBL_UNLOCK ();
                    SYS_LOG_MSG ((SYSLOG_NOTICE_LEVEL, gu4CfaSysLogId,
                                  "IP Address change in Default vlan interface."));
                    CFA_IPIFTBL_LOCK ();
                }
            }

        }

    }

    /* NetMask associated with primary ipaddress of the interface */
    if (u4Flag & CFA_IP_IF_NETMASK)
    {
        if (u4OldNetMask != pIpInfo->u4NetMask)
        {
            pIpIntf->u1SubnetMask = CfaGetCidrSubnetMaskIndex
                (pIpInfo->u4NetMask);
            u1NetChange = TRUE;
            if ((CFA_CDB_IF_TYPE (u4IfIndex) == CFA_PPP) && 
                (CFA_CDB_IF_ENTRY (u4IfIndex)->u4UnnumAssocIPIf != 0))
            {
                pIpIntf->u1SubnetMask = 8;
            }
        }
    }

#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
#if defined(CLI_LNXIP_WANTED) || defined(SNMP_LINXIP_WANTED)
#ifdef RM_WANTED
    if (RmGetNodeState () == RM_ACTIVE)
    {
#endif
        CfaIpUpdateInterfaceParams (CFA_CDB_IF_ALIAS (u4IfIndex), u4Flag,
                                    pIpInfo);
#ifdef RM_WANTED
    }
#endif
#endif
    /* If there is any route change,Notify RTM about the route change */
    if (u1NetChange == TRUE)
    {
        /* If there is no interface net is same as that of old Net,
         * delete the route from RTM */

        if ((u4OldIpAddr != 0) && (CfaIpIfIsLocalNetOnIntf (pIpIntf,
                                                            (u4OldIpAddr &
                                                             u4OldNetMask),
                                                            u4OldNetMask) ==
                                   FALSE))
        {
            if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId)
                == VCM_FAILURE)
            {
                CFA_IPIFTBL_UNLOCK ();
                return CFA_FAILURE;
            }
            MEMSET (&IpRtInfo, 0, sizeof (tRtmRouteUpdateInfo));
            IpRtInfo.u4ContextId = u4ContextId;
            IpRtInfo.u4NewIpAddr = u4OldIpAddr;
            IpRtInfo.u4NewNetMask = u4OldNetMask;
            IpRtInfo.u4IfIndex = (UINT4) CFA_IF_IPPORT ((UINT2) u4IfIndex);
            RtmIntfNotifyRoute (RTM_INTF_ROUTE_DEL_MSG, &IpRtInfo);
#if 0 
#if defined(NPAPI_WANTED) && !defined(NPSIM_WANTED)
            CfaGetIfHwAddr (u4IfIndex, au1MacAddress);
            /*the interface is not created/modify int this call  */
            CfaFsNpIpv4ModifyIpInterface (u4ContextId, CFA_ZERO,
                                          CFA_IP_INTERFACE_TYPE, u4OldIpAddr,
                                          CFA_ZERO, CFA_ZERO, au1MacAddress);
#endif
#endif
        }
        if ((pIpIntf->u4IpAddr != 0) && (pIpIntf->u1SubnetMask != 0))
        {
            if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId)
                == VCM_FAILURE)
            {
                CFA_IPIFTBL_UNLOCK ();
                return CFA_FAILURE;
            }

            MEMSET (&IpRtInfo, 0, sizeof (tRtmRouteUpdateInfo));
            IpRtInfo.u4ContextId = u4ContextId;
            IpRtInfo.u4NewIpAddr = pIpIntf->u4IpAddr;
            IpRtInfo.u4NewNetMask = u4CidrSubnetMask[pIpIntf->u1SubnetMask];
            IpRtInfo.u4IfIndex = (UINT4) CFA_IF_IPPORT ((UINT2) u4IfIndex);
            RtmIntfNotifyRoute (RTM_INTF_ROUTE_ADD_MSG, &IpRtInfo);

#if 0 
#if defined(NPAPI_WANTED) && !defined(NPSIM_WANTED)
            CfaGetIfHwAddr (u4IfIndex, au1MacAddress);
            /* this doesnt create a L3 Interface in Hardware */
            CfaFsNpIpv4CreateIpInterface (u4ContextId, CFA_ZERO,
                                          CFA_IP_INTERFACE_TYPE,
                                          pIpIntf->u4IpAddr, CFA_ZERO, CFA_ZERO,
                                          au1MacAddress);
#endif
#endif
        }
    }
#endif

    /* Broadcast address associated with the interface */
    if (u4Flag & CFA_IP_IF_BCASTADDR)
    {
        pIpIntf->u4BcastAddr = pIpInfo->u4BcastAddr;
    }
    else
    {
        pIpIntf->u4BcastAddr = (~(u4CidrSubnetMask[pIpIntf->u1SubnetMask])
                                | pIpIntf->u4IpAddr);
    }

    /* Forwarding status of the  interface */
    if (u4Flag & CFA_IP_IF_FORWARD)
    {
        pIpIntf->u1IpForwardEnable = pIpInfo->u1IpForwardEnable;
    }

    /* Port-VlanId associated with the router port */
    if (u4Flag & CFA_IP_IF_PORTVID)
    {
        pIpIntf->u2PortVlanId = (UINT2) pIpInfo->u2PortVlanId;
    }

    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
*    Function Name            : CfaIpIfGetIfInfo
*
*    Description              :  Providesthe IP related parameters for a interface
*    
*    Input(s)                  : u4IfIndex -Interface Index
*
*    Output(s)                 : pIpInfo - Interface information filled
*    
*    Global Variables Referred : gIpIfInfo.pIpIfTable
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfGetIfInfo (UINT4 u4IfIndex, tIpConfigInfo * pIpInfo)
{
    INT4                i4RetVal = CFA_FAILURE;
    tIpIfRecord         IpIntfInfo;
    tIpIfRecord        *pIpIntf;

    CFA_IPIFTBL_LOCK ();

    /* Get the IP interface node from RBTree */
    IpIntfInfo.u4IfIndex = u4IfIndex;
    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);

    if (pIpIntf != NULL)
    {
        /* Fill all the ip information associated with the interface */
        pIpInfo->u4Addr = pIpIntf->u4IpAddr;
        pIpInfo->u4NetMask = u4CidrSubnetMask[pIpIntf->u1SubnetMask];
        pIpInfo->u4BcastAddr = pIpIntf->u4BcastAddr;
        pIpInfo->u1AddrAllocMethod = pIpIntf->u1AddrAllocMethod;
        pIpInfo->u1AddrAllocProto = pIpIntf->u1AddrAllocProto;
        pIpInfo->u1IpForwardEnable = pIpIntf->u1IpForwardEnable;
        pIpInfo->u2PortVlanId = pIpIntf->u2PortVlanId;
        i4RetVal = CFA_SUCCESS;
    }

    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
*    Function Name            :  CfaIpIfSetSecondaryAddressInfoWr
*
*    Description              :  Configures the Secondary address information
*    
*    Input(s)                  : u4IfIndex - Interface Index
*                                u4IpAddr  - Secondary IP Address
*                                u4NetMask - Net Mask
*                                u4Flag    - BitMask for the set of parameters
*                                            to be set
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfSetSecondaryAddressInfoWr (UINT4 u4IfIndex, UINT4 u4IpAddr,
                                  UINT4 u4NetMask, UINT4 u4Flag)
{
    tIpAddrInfo         IpAddrInfo;

    MEMSET (&IpAddrInfo, 0, sizeof (tIpAddrInfo));

    IpAddrInfo.u4Addr = u4IpAddr;
    IpAddrInfo.u1SubnetMask = CfaGetCidrSubnetMaskIndex (u4NetMask);
    IpAddrInfo.u1AllocMethod = CFA_IP_IF_VIRTUAL;

    if (u4Flag == CREATE_AND_GO)
    {
        u4Flag = (CFA_IP_IF_SECONDAY_ROW_STATUS | CFA_IP_IF_NETMASK);
        IpAddrInfo.i4RowStatus = CFA_RS_CREATEANDGO;
    }
    else if (u4Flag == DESTROY)
    {
        u4Flag = CFA_IP_IF_SECONDAY_ROW_STATUS;
        IpAddrInfo.i4RowStatus = CFA_RS_DESTROY;
    }

    if (CfaIpIfSetSecondaryAddressInfo (u4IfIndex, u4Flag, &IpAddrInfo)
        == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
*    Function Name            :  CfaIpIfSetSecondaryAddressInfo
*
*    Description              :  Configures the Secondary address information
*    
*    Input(s)                  : u4IfIndex -Interface Index
*                                u4Flag  - BitMask for the set of parameters
*                                to be set
*                                tIpAddrInfo - Secondary address information
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfSetSecondaryAddressInfo (UINT4 u4IfIndex, UINT4 u4Flag,
                                tIpAddrInfo * pIpAddrInfo)
{
    UINT2               u2AliasIndex = 0;
    UINT4               u4ContextId;
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    UINT4               u4NetMask = 0;
    tRtmRouteUpdateInfo IpRtInfo;
#endif
    tIpIfRecord         IpIntfInfo;
    tIpIfRecord        *pIpIntf = NULL;
    tIpAddrInfo        *pSecondaryIpInfo = NULL;
    tIpAddrInfo        *pPrevNode = NULL;

    CFA_IPIFTBL_LOCK ();

    /* Get the IP interface node from RBTree */
    IpIntfInfo.u4IfIndex = u4IfIndex;
    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
    if (pIpIntf == NULL)
    {
        CFA_IPIFTBL_UNLOCK ();
        return CFA_FAILURE;
    }

    if ((u4Flag & CFA_IP_IF_SECONDAY_ROW_STATUS) &&
        ((pIpAddrInfo->i4RowStatus == CFA_RS_CREATEANDGO) ||
         (pIpAddrInfo->i4RowStatus == CFA_RS_CREATEANDWAIT)))
    {
        /*Request for create secondary ip entry. No need to scan. */
    }
    else
    {
        /* Scan the secondary address and  get the appropriate
         * secondary address record */
        TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecondaryIpInfo,
                      tIpAddrInfo *)
        {
            if (pSecondaryIpInfo->u4Addr == pIpAddrInfo->u4Addr)
            {
                break;
            }
        }

        if (pSecondaryIpInfo == NULL)
        {
            CFA_IPIFTBL_UNLOCK ();
            return CFA_FAILURE;
        }
    }
    /* NetMask associated with the secondary address */
    if (pSecondaryIpInfo != NULL)
    {
        if (u4Flag & CFA_IP_IF_NETMASK)
        {
            pSecondaryIpInfo->u1SubnetMask = pIpAddrInfo->u1SubnetMask;
        }

        /* Broadcast address associated with the secondary address */
        if (u4Flag & CFA_IP_IF_BCASTADDR)
        {
            pSecondaryIpInfo->u4BcastAddr = pIpAddrInfo->u4BcastAddr;
        }
    }

    /* Process the configuration of RowStatus */
    if (u4Flag & CFA_IP_IF_SECONDAY_ROW_STATUS)
    {
        if ((pIpAddrInfo->i4RowStatus == CFA_RS_CREATEANDGO) ||
            (pIpAddrInfo->i4RowStatus == CFA_RS_CREATEANDWAIT))
        {
            /* Scan the secondary address list and get the alias index to
             * be used */
            TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecondaryIpInfo,
                          tIpAddrInfo *)
            {
                if (pSecondaryIpInfo->u4Addr < pIpAddrInfo->u4Addr)
                {
                    pPrevNode = pSecondaryIpInfo;
                }
                if (pSecondaryIpInfo->u2AliasIndex > u2AliasIndex)
                {
                    u2AliasIndex = pSecondaryIpInfo->u2AliasIndex;
                }
                if (pSecondaryIpInfo->u4Addr == pIpAddrInfo->u4Addr)
                {
                    /* Skip secondary IP insertion if it is already present */
                    CFA_IPIFTBL_UNLOCK ();
                    return CFA_SUCCESS;
                }
            }
            if ((pSecondaryIpInfo =
                 (tIpAddrInfo *)
                 MemAllocMemBlk (gIpIfInfo.SecondaryIpPool)) == NULL)
            {
                CFA_IPIFTBL_UNLOCK ();
                return CFA_FAILURE;
            }

            MEMSET (pSecondaryIpInfo, 0, sizeof (tIpAddrInfo));
            pSecondaryIpInfo->u4Addr = pIpAddrInfo->u4Addr;
            pSecondaryIpInfo->u2AliasIndex = (UINT2) (u2AliasIndex + 1);

            /* Add the node to the Secondary IP List */
            TMO_SLL_Insert (&(pIpIntf->SecondaryIpList),
                            &pPrevNode->NextNode, &pSecondaryIpInfo->NextNode);

            if (pIpAddrInfo->u1AllocMethod == CFA_IP_IF_VIRTUAL)
            {
                pSecondaryIpInfo->u1AllocMethod = CFA_IP_IF_VIRTUAL;
            }
            else
            {
                pSecondaryIpInfo->u1AllocMethod = CFA_IP_IF_MANUAL;
            }

            if (pIpAddrInfo->i4RowStatus == CFA_RS_CREATEANDGO)
            {
                pSecondaryIpInfo->u1SubnetMask = pIpAddrInfo->u1SubnetMask;
                pSecondaryIpInfo->u4BcastAddr = pIpAddrInfo->u4BcastAddr;
                pIpAddrInfo->i4RowStatus = CFA_RS_ACTIVE;
            }
            else
            {
                pSecondaryIpInfo->i4RowStatus = CFA_RS_NOTREADY;
            }
        }
        if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId)
            == VCM_FAILURE)
        {
            CFA_IPIFTBL_UNLOCK ();
            return CFA_FAILURE;
        }
        if (pIpAddrInfo->i4RowStatus == CFA_RS_ACTIVE)
        {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
            if ((pSecondaryIpInfo->u4Addr != 0) &&
                (pSecondaryIpInfo->u1SubnetMask != 0))
            {
                u4NetMask = u4CidrSubnetMask[pSecondaryIpInfo->u1SubnetMask];
                /* Overlapping Ip address can be configured on the 
                 * single interface(ex 12.0.0.1,12.0.0.2,..).But only
                 * a single route associated wih them.So check
                 * whether any route already exists on the interface.If exists
                 * Dont notify RTM again */
                if (CfaIpIfIsLocalNetOnIntf (pIpIntf,
                                             pSecondaryIpInfo->
                                             u4Addr & u4NetMask,
                                             u4NetMask) == FALSE)
                {
                    MEMSET (&IpRtInfo, 0, sizeof (tRtmRouteUpdateInfo));
                    IpRtInfo.u4ContextId = u4ContextId;
                    IpRtInfo.u4NewIpAddr = pSecondaryIpInfo->u4Addr;
                    IpRtInfo.u4NewNetMask = u4NetMask;
                    IpRtInfo.u4IfIndex = (UINT4)
                        CFA_IF_IPPORT ((UINT2) u4IfIndex);
                    RtmIntfNotifyRoute (RTM_INTF_ROUTE_ADD_MSG, &IpRtInfo);
                }
                CfaIpIfCreateorDeleteSecondaryAddress (u4IfIndex,
                                                       pSecondaryIpInfo,
                                                       CFA_NET_IF_NEW);
            }
#endif /* IP_WANTED || LNXIP4_WANTED */
            pSecondaryIpInfo->i4RowStatus = pIpAddrInfo->i4RowStatus;
        }
        if ((pIpAddrInfo->i4RowStatus == CFA_RS_DESTROY) ||
            (pIpAddrInfo->i4RowStatus == CFA_RS_NOTINSERVICE))
        {
            pSecondaryIpInfo->i4RowStatus = pIpAddrInfo->i4RowStatus;

#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
            if ((pSecondaryIpInfo->u4Addr != 0) &&
                (pSecondaryIpInfo->u1SubnetMask != 0))
            {
                u4NetMask = u4CidrSubnetMask[pSecondaryIpInfo->u1SubnetMask];
                /* Overlapping Ip address can be configured on the 
                 * single interface(ex 12.0.0.1,12.0.0.2,..).But only
                 * a single route associated wih them.If there is only
                 * one route exists on the network,Notify RTM to delete it.
                 * Else Dont notify*/
                if (CfaIpIfIsLocalNetOnIntf (pIpIntf,
                                             pSecondaryIpInfo->
                                             u4Addr & u4NetMask,
                                             u4NetMask) == FALSE)
                {
                    MEMSET (&IpRtInfo, 0, sizeof (tRtmRouteUpdateInfo));
                    IpRtInfo.u4ContextId = u4ContextId;
                    IpRtInfo.u4NewIpAddr = pSecondaryIpInfo->u4Addr;
                    IpRtInfo.u4NewNetMask =
                        u4CidrSubnetMask[pSecondaryIpInfo->u1SubnetMask];
                    IpRtInfo.u4IfIndex = (UINT4)
                        CFA_IF_IPPORT ((UINT2) u4IfIndex);
                    RtmIntfNotifyRoute (RTM_INTF_ROUTE_DEL_MSG, &IpRtInfo);
                }
                CfaIpIfCreateorDeleteSecondaryAddress (u4IfIndex,
                                                       pSecondaryIpInfo,
                                                       CFA_NET_IF_DEL);
            }
#endif /* IP_WANTED || LNXIP4_WANTED */
        }
        if (pSecondaryIpInfo->i4RowStatus == CFA_RS_DESTROY)
        {
            /* Delete the secondary address from the List */
            TMO_SLL_Delete (&(pIpIntf->SecondaryIpList),
                            &pSecondaryIpInfo->NextNode);
            MemReleaseMemBlock (gIpIfInfo.SecondaryIpPool,
                                (UINT1 *) pSecondaryIpInfo);
        }
    }

    CFA_IPIFTBL_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************
*    Function Name            :  CfaIpIfGetSecondaryAddressInfo
*
*    Description              :  Provides the information associated with 
*                                the secondary IP address
*    
*    Input(s)                  : u4IfIndex - Interface Index
*                                u4SecIpAddress - Secondary IP address
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : gIpIfInfo.pIpIfTable
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfGetSecondaryAddressInfo (UINT4 u4IfIndex,
                                UINT4 u4SecIpAddress, tIpAddrInfo * pIpAddrInfo)
{
    tIpIfRecord        *pIpIntf;
    tIpIfRecord         IpIntfInfo;
    tIpAddrInfo        *pSecIpInfo;
    INT4                i4RetVal = CFA_FAILURE;

    CFA_IPIFTBL_LOCK ();

    IpIntfInfo.u4IfIndex = u4IfIndex;
    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
    if (pIpIntf != NULL)
    {
        TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo, tIpAddrInfo *)
        {
            if (pSecIpInfo->u4Addr == u4SecIpAddress)
            {
                MEMCPY (pIpAddrInfo, pSecIpInfo, sizeof (tIpAddrInfo));
                i4RetVal = CFA_SUCCESS;
                break;
            }
        }
    }

    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
 * *    Function Name            :  CfaIpIfIsSecondaryAddress
 * *
 * *    Description              :  Checks whether the given IPv4 address is a
 * *                                secondary address or not
 * *
 * *    Input(s)                  : u4IfIndex - Interface Index
 * *                                u4SecIpAddress - IP address
 * *
 * *    Output(s)                 : None.
 * *
 * *    Global Variables Referred : gIpIfInfo.pIpIfTable
 * *
 * *    Returns                   : CFA_SUCCESS/CFA_FAILURE
 * *****************************************************************************/

INT4
CfaIpIfIsSecondaryAddress (UINT4 u4IfIndex, UINT4 u4IpAddress)
{
    tIpIfRecord        *pIpIntf;
    tIpIfRecord         IpIntfInfo;
    tIpAddrInfo        *pSecIpInfo;
    INT4                i4RetVal = CFA_FAILURE;

    CFA_IPIFTBL_LOCK ();

    IpIntfInfo.u4IfIndex = u4IfIndex;
    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
    if (pIpIntf != NULL)
    {
        TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo, tIpAddrInfo *)
        {
            if (pSecIpInfo->u4Addr == u4IpAddress)
            {
                i4RetVal = CFA_SUCCESS;
                break;
            }
        }
    }

    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
*    Function Name            :  CfaIpIfHandleSecondaryAddress
*
*    Description              :  On creation ,this function make the secondary
*                                address configured on the interface visible 
*                                to IP.
*                                On deletion, delete the secondary address 
*                                visible to IP
*    
*    Input(s)                  : u4IfIndex - Interface Index
*                                u1Flag - Indicates the operation is 
*                                                            addition/deletion
*
*    Output(s)                 : NONE 
*    
*    Global Variables Referred : gIpIfInfo.pIpIfTable
*
*    Returns                   : None 
*****************************************************************************/
VOID
CfaIpIfHandleSecondaryAddress (UINT4 u4IfIndex, UINT1 u1Flag)
{
    tIpIfRecord         IpIntfInfo;
    tIpIfRecord        *pIpIntf;
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    tIpAddrInfo        *pSecIpInfo;
    tNetIpv4RtInfo      NetRtInfo;
#endif
    UINT4               u4RowStatus = 0;
    UINT4               u4ContextId = 0;
    UINT1               u1CmdType = 0;
    UINT1               u1OperStatus = CFA_IF_DOWN;
    INT4                i4RetValue = 0;

    UNUSED_PARAM (i4RetValue);

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);

    CFA_IPIFTBL_LOCK ();
    IpIntfInfo.u4IfIndex = u4IfIndex;
    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);

    if (u1Flag == CFA_NET_IF_NEW)
    {
        CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
        if (u1OperStatus == CFA_IF_UP)
        {
            u4RowStatus = IPFWD_ACTIVE;
        }
        else
        {
            u4RowStatus = IPFWD_NOT_READY;
        }
        u1CmdType = NETIPV4_ADD_ROUTE;
    }
    else if (u1Flag == CFA_NET_IF_DEL)
    {
        u4RowStatus = IPFWD_DESTROY;
        u1CmdType = NETIPV4_DELETE_ROUTE;
    }

#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    if (pIpIntf != NULL)
    {
        if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId)
            == VCM_FAILURE)
        {
            CFA_IPIFTBL_UNLOCK ();
            return;
        }
        if (pIpIntf->u4IpAddr != 0)
        {
            /* Update RTM for the Routes */
            MEMSET (&NetRtInfo, 0, sizeof (tNetIpv4RtInfo));
            NetRtInfo.u4DestNet = pIpIntf->u4IpAddr &
                u4CidrSubnetMask[pIpIntf->u1SubnetMask];
            NetRtInfo.u4DestMask = u4CidrSubnetMask[pIpIntf->u1SubnetMask];
            NetRtInfo.u4ContextId = u4ContextId;
            NetRtInfo.u4NextHop = 0;
            NetRtInfo.u4RtIfIndx = (UINT4) CFA_IF_IPPORT ((UINT2) u4IfIndex);
            NetRtInfo.u2RtType = (UINT2) FSIP_LOCAL;
            NetRtInfo.u4RtNxtHopAs = 0;
            NetRtInfo.u2RtProto = CIDR_LOCAL_ID;
            NetRtInfo.u4RowStatus = u4RowStatus;
            SET_BIT_FOR_CHANGED_PARAM (NetRtInfo.u2ChgBit, IP_BIT_STATUS);

            CFA_IPIFTBL_UNLOCK ();
            i4RetValue = NetIpv4LeakRoute (u1CmdType, &NetRtInfo);
            CFA_IPIFTBL_LOCK ();
        }

        TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo, tIpAddrInfo *)
        {
            if (pSecIpInfo->i4RowStatus != CFA_RS_ACTIVE)
            {
                continue;
            }
            MEMSET (&NetRtInfo, 0, sizeof (tNetIpv4RtInfo));
            NetRtInfo.u4DestNet = pSecIpInfo->u4Addr &
                u4CidrSubnetMask[pSecIpInfo->u1SubnetMask];
            NetRtInfo.u4DestMask = u4CidrSubnetMask[pSecIpInfo->u1SubnetMask];
            NetRtInfo.u4ContextId = u4ContextId;
            NetRtInfo.u4NextHop = 0;
            NetRtInfo.u4RtIfIndx = (UINT4) CFA_IF_IPPORT ((UINT2) u4IfIndex);
            NetRtInfo.u2RtType = (UINT2) FSIP_LOCAL;
            NetRtInfo.u4RtNxtHopAs = 0;
            NetRtInfo.u2RtProto = CIDR_LOCAL_ID;
            NetRtInfo.u4RowStatus = u4RowStatus;
            SET_BIT_FOR_CHANGED_PARAM (NetRtInfo.u2ChgBit, IP_BIT_STATUS);

            CFA_IPIFTBL_UNLOCK ();
            i4RetValue = NetIpv4LeakRoute (u1CmdType, &NetRtInfo);
            CFA_IPIFTBL_LOCK ();
            CfaIpIfCreateorDeleteSecondaryAddress (u4IfIndex,
                                                   pSecIpInfo, u1Flag);
        }
    }
#endif /* IP_WANTED || LNXIP4_WANTED */

    CFA_IPIFTBL_UNLOCK ();
}

#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
/*****************************************************************************
*    Function Name            :  CfaIpIfCreateorDeleteSecondaryAddress
*
*    Description              :  This function acts on the given secondary 
*                                address information
*    
*    Input(s)                  : u4IfIndex - Interface Index
*                                pSecIpInfo - Secondary address information
*                                u1Flag - Indicates the operation is 
*                                                            addition/deletion
*
*    Output(s)                 : NONE 
*    
*    Global Variables Referred : gIpIfInfo.pIpIfTable
*
*    Returns                   : None 
*****************************************************************************/
VOID
CfaIpIfCreateorDeleteSecondaryAddress (UINT4 u4IfIndex,
                                       tIpAddrInfo * pSecIpInfo, UINT1 u1Flag)
{
    UINT1               au1AliasName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1IfName[CFA_MAX_DESCR_LENGTH];
#ifdef LNXIP4_WANTED
    tIpConfigInfo       IpInfo;
    UINT4               u4Flag = 0;
#endif
#ifdef NPAPI_WANTED
    tVlanId             u2VlanId = 0;
    UINT4               u4ContextId = 0;
    UINT1               au1MacAddress[MAC_ADDR_LEN];
#endif
    t_IFACE_PARAMS      IfParams;
/*updating info for secondary IP Address that is created/deteled*/
    MEMSET (&IfParams, 0, sizeof (t_IFACE_PARAMS));
    MEMSET (au1AliasName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1IfName, 0, CFA_MAX_DESCR_LENGTH);
    CfaGetIfName (u4IfIndex, au1AliasName);
    SPRINTF ((CHR1 *) au1IfName, "%s:%d", au1AliasName,
             pSecIpInfo->u2AliasIndex);
    MEMCPY (IfParams.au1IfName, au1IfName, CFA_MAX_PORT_NAME_LENGTH);
    IfParams.u2Port = (UINT2) CfaGetIfIpPort (u4IfIndex);
    IfParams.u4IpAddr = pSecIpInfo->u4Addr;
    IfParams.u4SubnetMask = (UINT4) pSecIpInfo->u1SubnetMask;
    IfParams.u4BcastAddr = pSecIpInfo->u4BcastAddr;
    IfParams.u2IfIndex = (UINT2) u4IfIndex;

    if (u1Flag == CFA_NET_IF_NEW)
    {

#ifdef LNXIP4_WANTED
        u4Flag = CFA_IP_IF_SECONDARY_ADDR_ADD;
        IpInfo.u4Addr = pSecIpInfo->u4Addr;
        IpInfo.u4NetMask = u4CidrSubnetMask[pSecIpInfo->u1SubnetMask];
        IpInfo.u4BcastAddr = pSecIpInfo->u4BcastAddr;
        LnxIpUpdateSecondaryIpInfo (au1IfName, &IpInfo, u4Flag);
#endif
#ifdef NPAPI_WANTED
        CfaGetIfHwAddr (u4IfIndex, au1MacAddress);
        VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
        if (CfaGetIfIvrVlanId (u4IfIndex, &u2VlanId) == CFA_SUCCESS)
        {
            CfaFsNpIpv4CreateIpInterface (u4ContextId, au1IfName, u4IfIndex,
                                          pSecIpInfo->u4Addr,
                                          u4CidrSubnetMask[pSecIpInfo->
                                                           u1SubnetMask],
                                          u2VlanId, au1MacAddress);
        }
#endif
        IpHandleInterfaceMsgFromCfa (&IfParams, IP_SEC_CREATE);
    }
    else if (u1Flag == CFA_NET_IF_DEL)
    {
        if (ISS_HW_SUPPORTED !=
            IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT))
        {
#ifdef LNXIP4_WANTED
            u4Flag = CFA_IP_IF_SECONDARY_ADDR_DEL;
            LnxIpUpdateSecondaryIpInfo (au1IfName, &IpInfo, u4Flag);
#endif
        }
#ifdef NPAPI_WANTED
        VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
        if (CfaGetIfIvrVlanId (u4IfIndex, &u2VlanId) == CFA_SUCCESS)
        {
            CfaFsNpIpv4DeleteSecIpInterface (u4ContextId, au1IfName, u4IfIndex,
                                             pSecIpInfo->u4Addr, u2VlanId);
        }

#endif
        IpHandleInterfaceMsgFromCfa (&IfParams, IP_SEC_DELETE);
    }
}

#endif /* IP_WANTED || LNXIP4_WANTED */
/*****************************************************************************
*    Function Name            :  CfaIpIfIsLocalNetOnIntf
*
*    Description              :  Checks whether any local net already exists on
*                                the interface. This function should be called
*                                after taking the Lock
*    
*    Input(s)                  : pIpIntf - IP Interface node in RB Tree
*                                u4IpNet - Network associated with Interface
*
*    Output(s)                 : NONE 
*    
*    Global Variables Referred : gIpIfInfo.pIpIfTable
*
*    Returns                   : TRUE/FALSE 
*****************************************************************************/
UINT1
CfaIpIfIsLocalNetOnIntf (tIpIfRecord * pIpIntf, UINT4 u4IpNet,
                         UINT4 u4SubnetMask)
{
    tIpAddrInfo        *pSecIpInfo = NULL;
    UINT4               u4NetMask;

    u4NetMask = u4CidrSubnetMask[pIpIntf->u1SubnetMask];
    if ((((pIpIntf->u4IpAddr) & u4NetMask) == u4IpNet) &&
        (u4SubnetMask == u4NetMask))
    {
        return TRUE;
    }

    TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo, tIpAddrInfo *)
    {
        u4NetMask = u4CidrSubnetMask[pSecIpInfo->u1SubnetMask];

        /* Consider only the active entries */
        if (pSecIpInfo->i4RowStatus != CFA_RS_ACTIVE)
        {
            continue;
        }
        /* If the given network is same as interface IP net,
         * return TRUE */
        if (((pSecIpInfo->u4Addr & u4NetMask) == u4IpNet) &&
            (u4SubnetMask == u4NetMask))
        {
            return TRUE;
        }
    }
    return FALSE;
}

/*****************************************************************************
*    Function Name            :  CfaIpIfGetNextSecondaryIpIndex
*
*    Description              :  Provides the lexicographical higher secondary 
*                                IP address configured.This function is used
*                                for performing get next operation on secondary
*                                address table
*    
*    Input(s)                  : u4IfIndex - Interface Index
*                                u4SecIpAddress - Secondary IP address
*
*    Output(s)                 : None 
*    
*    Global Variables Referred : gIpIfInfo.pIpIfTable
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfGetNextSecondaryIpIndex (UINT4 u4IfIndex,
                                UINT4 u4SecIpAddress, UINT4 *pu4NextIndex,
                                UINT4 *pu4NextSecIpAddress)
{
    INT4                i4RetVal = CFA_FAILURE;
    UINT1               u1Found = FALSE;
    tIpIfRecord        *pIpIntf;
    tIpIfRecord         IpIntfInfo;
    tIpAddrInfo        *pSecIpInfo;

    CFA_IPIFTBL_LOCK ();

    IpIntfInfo.u4IfIndex = u4IfIndex;
    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);

    if (pIpIntf != NULL)
    {
        TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo, tIpAddrInfo *)
        {
            if (pSecIpInfo->u4Addr > u4SecIpAddress)
            {
                *pu4NextIndex = u4IfIndex;
                *pu4NextSecIpAddress = pSecIpInfo->u4Addr;
                u1Found = TRUE;
                break;
            }
        }
    }

    if (u1Found == FALSE)
    {
        IpIntfInfo.u4IfIndex = u4IfIndex;

        while ((pIpIntf = (tIpIfRecord *) RBTreeGetNext (gIpIfInfo.pIpIfTable,
                                                         &IpIntfInfo,
                                                         NULL)) != NULL)
        {
            pSecIpInfo = (tIpAddrInfo *)
                TMO_SLL_First (&(pIpIntf->SecondaryIpList));
            if (pSecIpInfo != NULL)
            {
                *pu4NextIndex = pIpIntf->u4IfIndex;
                *pu4NextSecIpAddress = pSecIpInfo->u4Addr;
                u1Found = TRUE;
                break;
            }
            IpIntfInfo.u4IfIndex = pIpIntf->u4IfIndex;
        }
    }
    if (u1Found == TRUE)
    {
        i4RetVal = CFA_SUCCESS;
    }
    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
 *
 *    Function Name       : CfaIpIfValidateIfIpSubnet
 *
 *    Description         : This function validates the subnet entered for 
 *                          a particular interface.
 *
 *    Input(s)            : Interface index and Netmask.
 *
 *
 *    Output(s)           :None. 
 *
 *    Global Variables Referred : gIpIfInfo
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE.
 *****************************************************************************/

INT4
CfaIpIfValidateIfIpSubnet (UINT4 u4CurrIfIndex, UINT4 u4CurrIpAddr,
                           UINT4 u4CurrNetMask)
{
    tIpIfRecord         DummyIfNode;
    tIpIfRecord        *pIpIntf = NULL;
    tIpAddrInfo        *pSecIpInfo = NULL;
    INT4                i4RetVal = CFA_SUCCESS;
    UINT4               u4Var = 0;
    UINT4               u4Addr;
    UINT4               u4NetMask = 0;
    UINT4               u4ContextId = 0;
    UINT4               au4IfIndex[IPIF_MAX_LOGICAL_IFACES + 1];
    UINT1               u1IfNetMask = 0;

    MEMSET (au4IfIndex, 0, (sizeof (UINT1) * IPIF_MAX_LOGICAL_IFACES + 1));

    /* IP address need to be unique within each context. Across context, IP 
     * address can be overlapping. 
     * Get the list of interfaces mapped to the context to which this interface
     * is also mapped. Then check if the address is overlapping.
     */

    VcmGetContextIdFromCfaIfIndex (u4CurrIfIndex, &u4ContextId);
    VcmGetCxtIpIfaceList (u4ContextId, au4IfIndex);
    if (au4IfIndex[0] == 0)
    {
        return CFA_SUCCESS;
    }

    CFA_IPIFTBL_LOCK ();
    for (u4Var = 1; ((u4Var <= au4IfIndex[0]) &&
                     (u4Var < IPIF_MAX_LOGICAL_IFACES + 1)); u4Var++)
    {
        DummyIfNode.u4IfIndex = au4IfIndex[u4Var];
        pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &DummyIfNode);
        if (NULL == pIpIntf)
        {
            continue;
        }

        /* Check with the primary ip address of the interface */
        u1IfNetMask = pIpIntf->u1SubnetMask;
        u4Addr = pIpIntf->u4IpAddr;

        if (u4Addr != 0)
        {
            u4NetMask = u4CidrSubnetMask[u1IfNetMask];

            if (u4CurrIfIndex != pIpIntf->u4IfIndex)
            {
                if ((u4CurrIpAddr == u4Addr)
                    || ((u4CurrIpAddr & u4CurrNetMask) == (u4NetMask & u4Addr)))
                {
                    i4RetVal = CFA_FAILURE;
                    break;
                }
            }
        }
        /* Check whether address overlaps with any of the secondary 
         * address */
        TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo, tIpAddrInfo *)
        {
            u4Addr = pSecIpInfo->u4Addr;
            u1IfNetMask = pSecIpInfo->u1SubnetMask;
            u4NetMask = u4CidrSubnetMask[u1IfNetMask];

            if (u4CurrIfIndex != pIpIntf->u4IfIndex)
            {
                if ((u4CurrIpAddr & u4CurrNetMask) == (u4NetMask & u4Addr))
                {
                    CFA_IPIFTBL_UNLOCK ();
                    return CFA_FAILURE;
                }

            }
        }
    }
    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
*    Function Name            :  CfaIpIfIsOurInterfaceAddress
*
*    Description              :  Verifies  whether the interface is having the
*                                given IP address or not
*    
*    Input(s)                  : u4CfaIfIndex - CFA Interface Index
*                                u4IpAddress  - IP Address that needs to be
*                                checked against interface IP addresses.
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : gIpIfInfo.pIpIfTable
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfIsOurInterfaceAddress (UINT4 u4CfaIfIndex, UINT4 u4IpAddress)
{
    INT4                i4RetVal = CFA_FAILURE;
    tIpIfRecord         IpIntfInfo;
    tIpIfRecord        *pIpIntf;
    tIpAddrInfo        *pSecIpInfo;

    CFA_IPIFTBL_LOCK ();
    IpIntfInfo.u4IfIndex = u4CfaIfIndex;
    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);

    /* Consider the interface information only if you have valid
     * IP interface associated with the interface */
    if ((pIpIntf != NULL) &&
        (CFA_IF_IPPORT ((UINT2) u4CfaIfIndex) != CFA_INVALID_INDEX))
    {
        if (pIpIntf->u4IpAddr == u4IpAddress)
        {
            i4RetVal = CFA_SUCCESS;
        }
        else
        {
            TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo,
                          tIpAddrInfo *)
            {
                if ((pSecIpInfo->u4Addr == u4IpAddress) &&
                    (pSecIpInfo->i4RowStatus == CFA_RS_ACTIVE))
                {
                    i4RetVal = CFA_SUCCESS;
                    break;
                }
            }
        }
    }
    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
*    Function Name            : CfaIpIfIsOurAddress
*
*    Description              :  Verifes whether the IP address belongs to any 
*                                of the interface exists in the system
*    
*    Input(s)                  : u4IpAddress - Address to be verified against
*                                the interface ip address
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfIsOurAddress (UINT4 u4IpAddress)
{
    return (CfaIpIfIsOurAddressInCxt (VCM_DEFAULT_CONTEXT, u4IpAddress));
}

/*****************************************************************************
*    Function Name            : CfaIpIfIsOurAddressInCxt
*
*    Description              :  Verifes whether the IP address belongs to any 
*                                of the interface mapped to the specified contextm
*    
*    Input(s)                  : u4ContextId - context id
*                                u4IpAddress - Address to be verified against
*                                              the interface ip address
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfIsOurAddressInCxt (UINT4 u4ContextId, UINT4 u4IpAddress)
{
    tIpIfRecord        *pIpIntf;
    tIpAddrInfo        *pSecIpInfo;
    INT4                i4RetVal = CFA_FAILURE;
    UINT4               u4IfCxtId = 0;

    CFA_IPIFTBL_LOCK ();
    /* Get the first IP interface node from RBTree */
    pIpIntf = RBTreeGetFirst (gIpIfInfo.pIpIfTable);

    if (pIpIntf != NULL)
    {
        do
        {
            /* A Valid IP interface associated with the given
             * Interface */
            if (CFA_IF_IPPORT ((UINT2) pIpIntf->u4IfIndex) != CFA_INVALID_INDEX)
            {
                VcmGetContextIdFromCfaIfIndex (pIpIntf->u4IfIndex, &u4IfCxtId);
                if (u4IfCxtId == u4ContextId)
                {
                    if (pIpIntf->u4IpAddr == u4IpAddress)
                    {
                        i4RetVal = CFA_SUCCESS;
                        break;
                    }

                    TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo,
                                  tIpAddrInfo *)
                    {
                        if ((pSecIpInfo->u4Addr == u4IpAddress) &&
                            (pSecIpInfo->i4RowStatus == CFA_RS_ACTIVE))
                        {
                            i4RetVal = CFA_SUCCESS;
                            break;
                        }
                    }
                    if (i4RetVal == CFA_SUCCESS)
                    {
                        break;
                    }
                }
            }
            pIpIntf = (tIpIfRecord *) RBTreeGetNext (gIpIfInfo.pIpIfTable,
                                                     (tRBElem *) pIpIntf, NULL);
        }
        while (pIpIntf != NULL);
    }
    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
*    Function Name            : CfaIpIfGetIpAddressInfo
*
*    Description              :  Provides the netmask, broadcast address and 
*                                interface index associated with the given IP 
*                                Address in default context.
*    
*    Input(s)                  : u4IpAddress - IP address for which the 
*                                interface index to be derived
*                                pu4CfaIfIndex - In few cases, this argument
*                                is overloaded with input interface index also
*                                for usage in the utility.
*    
*    Output(s)                 :  NetMask, Broadcast address, Interface index.
*    
*    Global Variables Referred : gIpIfInfo.pIpIfTable
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfGetIpAddressInfo (UINT4 u4IpAddress, UINT4 *pu4NetMask,
                         UINT4 *pu4BcastAddr, UINT4 *pu4CfaIfIndex)
{

    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
    tCfaIfInfo          CfaIfInfo;
    INT4                i4RetVal = CFA_FAILURE;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    /* if the index is filled then ensure correct contextid is used */
    if (*pu4CfaIfIndex != 0)
    {
        i4RetVal = CfaGetIfInfo (*pu4CfaIfIndex, &CfaIfInfo);

        /* Since openflow interfaces are not registered with VCM, skip the context fetch.
         * use the default context itself 
         */
        if (CfaIfInfo.u1IfSubType != CFA_SUBTYPE_OPENFLOW_INTERFACE)
        {
            if (VcmGetContextIdFromCfaIfIndex (*pu4CfaIfIndex, &u4ContextId)
                == VCM_FAILURE)
            {
                return CFA_FAILURE;
            }
        }
    }
    UNUSED_PARAM (i4RetVal);

    return (CfaIpIfGetIpAddressInfoInCxt (u4ContextId, u4IpAddress,
                                          pu4NetMask, pu4BcastAddr,
                                          pu4CfaIfIndex));

}

/*****************************************************************************
*    Function Name            : CfaIpIfGetIpAddressInfoInCxt
*
*    Description              :  Provides the netmask, broadcast address and 
*                                interface index associated with the given IP 
*                                Address in the specified context
*    
*    Input(s)                  : u4ContextId - Context id
*                                u4IpAddress - IP address for which the 
*                                interface index to be derived
*    
*    Output(s)                 :  NetMask, Broadcast address, Interface index.
*    
*    Global Variables Referred : gIpIfInfo.pIpIfTable
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfGetIpAddressInfoInCxt (UINT4 u4ContextId, UINT4 u4IpAddress,
                              UINT4 *pu4NetMask, UINT4 *pu4BcastAddr,
                              UINT4 *pu4CfaIfIndex)
{
    tIpIfRecord        *pIpIntf;
    tIpAddrInfo        *pSecIpInfo;
    INT4                i4RetVal = CFA_FAILURE;
    UINT4               u4IfCxtId = 0;

    CFA_IPIFTBL_LOCK ();

    /* Get the IP interface node from RBTree */
    pIpIntf = RBTreeGetFirst (gIpIfInfo.pIpIfTable);
    if (pIpIntf != NULL)
    {
        do
        {
            /* A Valid IP interface associated with the given
             * Interface */
            if (CFA_IF_IPPORT ((UINT4) pIpIntf->u4IfIndex) != CFA_INVALID_INDEX)
            {
                VcmGetContextIdFromCfaIfIndex (pIpIntf->u4IfIndex, &u4IfCxtId);
                if (u4IfCxtId != u4ContextId)
                {
                    pIpIntf =
                        (tIpIfRecord *) RBTreeGetNext (gIpIfInfo.pIpIfTable,
                                                       (tRBElem *) pIpIntf,
                                                       NULL);
                    continue;
                }

                /* Check whether IP address exists as primary IP address */
                if (pIpIntf->u4IpAddr == u4IpAddress)
                {
                    *pu4CfaIfIndex = pIpIntf->u4IfIndex;
                    *pu4NetMask = u4CidrSubnetMask[pIpIntf->u1SubnetMask];
                    *pu4BcastAddr = pIpIntf->u4BcastAddr;
                    i4RetVal = CFA_SUCCESS;
                    break;
                }
                /* Check adddress exists as secondary address */
                TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo,
                              tIpAddrInfo *)
                {
                    if (pSecIpInfo->u4Addr == u4IpAddress)
                    {
                        *pu4CfaIfIndex = pIpIntf->u4IfIndex;
                        *pu4NetMask =
                            u4CidrSubnetMask[pSecIpInfo->u1SubnetMask];
                        *pu4BcastAddr = pSecIpInfo->u4BcastAddr;
                        i4RetVal = CFA_SUCCESS;
                        break;
                    }
                }
                if (i4RetVal == CFA_SUCCESS)
                {
                    break;
                }
            }
            pIpIntf = (tIpIfRecord *) RBTreeGetNext (gIpIfInfo.pIpIfTable,
                                                     (tRBElem *) pIpIntf, NULL);
        }
        while (pIpIntf != NULL);
    }

    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
*    Function Name            : CfaIpIfGetIfIndexFromIpAddress
*
*    Description              :  Provides the interface having the given IP 
*                                Address
*    
*    Input(s)                  : u4IpAddress - IP address for which the 
*                                interface index to be derived
*    
*    Output(s)                 :  Interface index.
*    
*    Global Variables Referred : gIpIfInfo.pIpIfTable
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfGetIfIndexFromIpAddress (UINT4 u4IpAddress, UINT4 *pu4CfaIfIndex)
{
    return (CfaIpIfGetIfIndexFromIpAddressInCxt (VCM_DEFAULT_CONTEXT,
                                                 u4IpAddress, pu4CfaIfIndex));
}

/*****************************************************************************
*    Function Name            : CfaIpIfGetIfIndexFromIpAddressInCxt
*
*    Description              :  Provides the interface having the given IP 
*                                Address which is mapped to the specified context
*    
*    Input(s)                  : u4ContextId - The context id 
*                                u4IpAddress - IP address for which the 
*                                interface index to be derived
*    
*    Output(s)                 :  Interface index.
*    
*    Global Variables Referred : gIpIfInfo.pIpIfTable
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfGetIfIndexFromIpAddressInCxt (UINT4 u4ContextId, UINT4 u4IpAddress,
                                     UINT4 *pu4CfaIfIndex)
{
    tIpIfRecord        *pIpIntf;
    tIpAddrInfo        *pSecIpInfo;
    INT4                i4RetVal = CFA_FAILURE;
    UINT4               u4IfCxtId = 0;

    CFA_IPIFTBL_LOCK ();

    /* Get the IP interface node from RBTree */
    pIpIntf = RBTreeGetFirst (gIpIfInfo.pIpIfTable);
    if (pIpIntf != NULL)
    {
        do
        {
            /* A Valid IP interface associated with the given
             * Interface */
            if (CFA_IF_IPPORT ((UINT2) pIpIntf->u4IfIndex) != CFA_INVALID_INDEX)
            {
                /* Check if the interface is mapped to the specified context. */
                VcmGetContextIdFromCfaIfIndex (pIpIntf->u4IfIndex, &u4IfCxtId);
                if (u4IfCxtId != u4ContextId)
                {
                    pIpIntf = (tIpIfRecord *) RBTreeGetNext
                        (gIpIfInfo.pIpIfTable, (tRBElem *) pIpIntf, NULL);
                    continue;
                }

                /* Check whether IP address exists as primary IP address */
                if (pIpIntf->u4IpAddr == u4IpAddress)
                {
                    *pu4CfaIfIndex = pIpIntf->u4IfIndex;
                    i4RetVal = CFA_SUCCESS;
                    break;
                }
                /* Check adddress exists as secondary address */
                TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo,
                              tIpAddrInfo *)
                {
                    if (pSecIpInfo->u4Addr == u4IpAddress)
                    {
                        *pu4CfaIfIndex = pIpIntf->u4IfIndex;
                        i4RetVal = CFA_SUCCESS;
                        break;
                    }
                }
                if (i4RetVal == CFA_SUCCESS)
                {
                    break;
                }
            }
            pIpIntf = (tIpIfRecord *) RBTreeGetNext (gIpIfInfo.pIpIfTable,
                                                     (tRBElem *) pIpIntf, NULL);
        }
        while (pIpIntf != NULL);
    }

    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
*    Function Name            :  CfaIpIfGetSrcAddress
*
*    Description              :  It is used to get the source ip address for
*                                the given destination
*    
*    Input(s)                  : u4Dest  - Destination for which source ip
*                                          address to be got
*
*    Output(s)                 : *pu4Src - Source IP address to be used. 
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE
*****************************************************************************/
INT4
CfaIpIfGetSrcAddress (UINT4 u4Dest, UINT4 *pu4Src)
{
    UINT4               u4NetMask;
    UINT4               u4SrcToUse = 0;
    INT4                i4RetVal = CFA_FAILURE;
    tIpIfRecord        *pIpIntf;
    tIpAddrInfo        *pSecIpInfo;

    CFA_IPIFTBL_LOCK ();

    /* Get the IP interface node from RBTree */
    pIpIntf = RBTreeGetFirst (gIpIfInfo.pIpIfTable);
    if (pIpIntf == NULL)
    {
        CFA_IPIFTBL_UNLOCK ();
        return i4RetVal;
    }

    do
    {
        /* A Valid IP interface associated with the given
         * Interface */
        if (CFA_IF_IPPORT ((UINT2) pIpIntf->u4IfIndex) != CFA_INVALID_INDEX)
        {
            u4NetMask = u4CidrSubnetMask[pIpIntf->u1SubnetMask];

            /* Find a interface that can be used for reaching destination.
             * If the interface ip is having the longest match as compared
             * to the previously selected source ip,select it */
            if (((pIpIntf->u4IpAddr) & u4NetMask) == (u4Dest & u4NetMask))
            {
                if (pIpIntf->u4IpAddr > u4SrcToUse)
                {
                    u4SrcToUse = pIpIntf->u4IpAddr;
                }
            }
            TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo,
                          tIpAddrInfo *)
            {
                if (pSecIpInfo->i4RowStatus != CFA_RS_ACTIVE)
                {
                    continue;
                }
                u4NetMask = u4CidrSubnetMask[pSecIpInfo->u1SubnetMask];
                if (((pSecIpInfo->u4Addr) & u4NetMask) == (u4Dest & u4NetMask))
                {
                    if (pSecIpInfo->u4Addr > u4SrcToUse)
                    {
                        u4SrcToUse = pSecIpInfo->u4Addr;
                    }
                }
            }
        }
        pIpIntf = (tIpIfRecord *) RBTreeGetNext (gIpIfInfo.pIpIfTable,
                                                 (tRBElem *) pIpIntf, NULL);
    }
    while (pIpIntf != NULL);

    if (u4SrcToUse != 0)
    {
        i4RetVal = CFA_SUCCESS;
    }
    *pu4Src = u4SrcToUse;

    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
*    Function Name            : CfaIpIfGetSrcAddressOnInterface
*
*    Description              :  It is used to get the source ip address for
*                                the given destination over the given interface
*    
*    Input(s)                  : u4CfaIfIndex - CFA interface Index
*                                u4Dest-  Destination for which source ip
*                                                            address to be got
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_FAILURE/CFA_SUCCESS
*****************************************************************************/
INT4
CfaIpIfGetSrcAddressOnInterface (UINT4 u4CfaIfIndex,
                                 UINT4 u4Dest, UINT4 *pu4Src)
{
    UINT4               u4NetMask;
    UINT4               u4SrcToUse = 0;
    tIpIfRecord        *pIpIntf;
    tIpIfRecord         IpIntfInfo;
    tIpAddrInfo        *pSecIpInfo;

    CFA_IPIFTBL_LOCK ();

    IpIntfInfo.u4IfIndex = u4CfaIfIndex;
    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);

    if ((pIpIntf == NULL) || (pIpIntf->u2IpPortNum == CFA_INVALID_INDEX))
    {
        CFA_IPIFTBL_UNLOCK ();
        return CFA_FAILURE;
    }

    /* If the interface ip is having the longest match as compared
     * to the previously selected source ip,select it */
    u4NetMask = u4CidrSubnetMask[pIpIntf->u1SubnetMask];
    if (((pIpIntf->u4IpAddr) & u4NetMask) == (u4Dest & u4NetMask))
    {
        if (pIpIntf->u4IpAddr > u4SrcToUse)
        {
            u4SrcToUse = pIpIntf->u4IpAddr;
        }
    }
    TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo, tIpAddrInfo *)
    {
        if (pSecIpInfo->i4RowStatus != CFA_RS_ACTIVE)
        {
            continue;
        }
        u4NetMask = u4CidrSubnetMask[pSecIpInfo->u1SubnetMask];
        if (((pSecIpInfo->u4Addr) & u4NetMask) == (u4Dest & u4NetMask))
        {
            if (pSecIpInfo->u4Addr > u4SrcToUse)
            {
                u4SrcToUse = pSecIpInfo->u4Addr;
            }
        }
    }
    if (u4SrcToUse == 0)
    {
        u4SrcToUse = pIpIntf->u4IpAddr;
    }
    *pu4Src = u4SrcToUse;
    CFA_IPIFTBL_UNLOCK ();
    return CFA_SUCCESS;
}

/*****************************************************************************
*    Function Name            : CfaIpIfIsLocalBroadcastInCxt
*
*    Description              :  Verifes whether the IP address belongs to any 
*                                of the interface's broadcast address which are
*                                mapped to the specified context
*    
*    Input(s)                  : u4ContextId - context id
*                                u4IpAddress - Address to be verified against
*                                the interface broadcast address
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : None
*
*    Returns                   :  CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfIsLocalBroadcastInCxt (UINT4 u4ContextId, UINT4 u4IpAddress)
{
    tIpIfRecord        *pIpIntf;
    tIpAddrInfo        *pSecIpInfo;
    INT4                i4RetVal = CFA_FAILURE;
    UINT4               u4IfCxtId = 0;
    UINT4               u4DefMask = 0;
    CFA_IPIFTBL_LOCK ();

    /* Get the first IP interface node from RBTree */
    pIpIntf = RBTreeGetFirst (gIpIfInfo.pIpIfTable);
    if (pIpIntf != NULL)
    {
        do
        {
            /* A Valid IP interface associated with the given
             * Interface */
            if (CFA_IF_IPPORT ((UINT2) pIpIntf->u4IfIndex) != CFA_INVALID_INDEX)
            {
                VcmGetContextIdFromCfaIfIndex (pIpIntf->u4IfIndex, &u4IfCxtId);
                if (u4IfCxtId == u4ContextId)
                {
                    CfaGetDefaultNetMask (u4IpAddress, &u4DefMask);
                    if (((pIpIntf->u4BcastAddr == u4IpAddress) ||
                         ((~(u4DefMask) | (pIpIntf->u4BcastAddr)) ==
                          u4IpAddress))
                        && (pIpIntf->u1SubnetMask != CFA_31BIT_MASK))
                    {
                        i4RetVal = CFA_SUCCESS;
                        break;
                    }

                    TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo,
                                  tIpAddrInfo *)
                    {
                        if ((((pSecIpInfo->i4RowStatus == CFA_RS_ACTIVE)) &&
                             (pSecIpInfo->u4BcastAddr == u4IpAddress)) &&
                            (pSecIpInfo->u1SubnetMask != CFA_31BIT_MASK))
                        {
                            i4RetVal = CFA_SUCCESS;
                            break;
                        }
                    }
                }
            }
            pIpIntf = (tIpIfRecord *) RBTreeGetNext (gIpIfInfo.pIpIfTable,
                                                     (tRBElem *) pIpIntf, NULL);
        }
        while (pIpIntf != NULL);
    }
    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
*    Function Name            : CfaIpIfCheckInterfaceBcastAddr
*
*    Description              :  Verifes whether the IP address belongs to  
*                                the interface's broadcast address
*    
*    Input(s)                  : u4IpAddress - Address to be verified against
*                                the interface broadcast addresses
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfCheckInterfaceBcastAddr (UINT4 u4CfaIfIndex, UINT4 u4IpAddress)
{
    INT4                i4RetVal = CFA_FAILURE;
    tIpIfRecord         IpIntfInfo;
    tIpIfRecord        *pIpIntf;
    tIpAddrInfo        *pSecIpInfo;
    UINT4               u4DefMask = 0;

    CFA_IPIFTBL_LOCK ();
    IpIntfInfo.u4IfIndex = u4CfaIfIndex;
    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);

    if ((pIpIntf != NULL) &&
        (CFA_IF_IPPORT ((UINT2) pIpIntf->u4IfIndex) != CFA_INVALID_INDEX))
    {
        if (pIpIntf->u4BcastAddr != u4IpAddress)
        {
            CfaGetDefaultNetMask (u4IpAddress, &u4DefMask);
            if (((~(u4DefMask) | (pIpIntf->u4BcastAddr)) == u4IpAddress) &&
                (pIpIntf->u1SubnetMask != CFA_31BIT_MASK))
            {
                i4RetVal = CFA_SUCCESS;
            }
            else
            {
                TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo,
                              tIpAddrInfo *)
                {
                    if (((pSecIpInfo->i4RowStatus == CFA_RS_ACTIVE)) &&
                        (pSecIpInfo->u4BcastAddr == u4IpAddress))
                    {
                        i4RetVal = CFA_SUCCESS;
                        break;
                    }
                }
            }
        }
        else
        {
            i4RetVal = CFA_SUCCESS;
        }
    }
    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
*    Function Name            : CfaIpIfIsLocalNet
*
*    Description              :  Verifes whether the IP address belongs to any 
*                                of the interface's network mapped to default
*                                context.
*    
*    Input(s)                  : u4IpAddress - Address to be verified against
*                                the interface broadcast address
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE
*****************************************************************************/
INT4
CfaIpIfIsLocalNet (UINT4 u4IpAddress)
{
    return (CfaIpIfIsLocalNetInCxt (VCM_DEFAULT_CONTEXT, u4IpAddress));
}

/*****************************************************************************
*    Function Name            : CfaIpIfIsLocalNetInCxt
*
*    Description              :  Verifes whether the IP address belongs to any 
*                                of the interface's network in the specified
*                                context
*    
*    Input(s)                  : u4ContextId - the context identifier
*                                u4IpAddress - Address to be verified against
*                                the interface broadcast address
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE
*****************************************************************************/
INT4
CfaIpIfIsLocalNetInCxt (UINT4 u4ContextId, UINT4 u4IpAddress)
{
    tIpIfRecord        *pIpIntf;
    tIpAddrInfo        *pSecIpInfo;
    INT4                i4RetVal = CFA_FAILURE;
    UINT4               u4IfCxtId = 0;
    UINT4               u4NetMask;

    CFA_IPIFTBL_LOCK ();

    /* Get the first IP interface node from RBTree */
    pIpIntf = RBTreeGetFirst (gIpIfInfo.pIpIfTable);
    if (pIpIntf != NULL)
    {
        do
        {
            /* A Valid IP interface associated with the given
             * Interface */
            if (CFA_IF_IPPORT ((UINT2) pIpIntf->u4IfIndex) != CFA_INVALID_INDEX)
            {

                VcmGetContextIdFromCfaIfIndex (pIpIntf->u4IfIndex, &u4IfCxtId);
                if (u4IfCxtId != u4ContextId)
                {
                    pIpIntf = (tIpIfRecord *) RBTreeGetNext
                        (gIpIfInfo.pIpIfTable, (tRBElem *) pIpIntf, NULL);
                    continue;
                }
                u4NetMask = u4CidrSubnetMask[pIpIntf->u1SubnetMask];
                if ((((pIpIntf->u4IpAddr) & u4NetMask) ==
                     (u4IpAddress & u4NetMask)) && (pIpIntf->u4IpAddr != 0))
                {
                    i4RetVal = CFA_SUCCESS;
                    break;
                }

                TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo,
                              tIpAddrInfo *)
                {
                    if (pSecIpInfo->i4RowStatus != CFA_RS_ACTIVE)
                    {
                        continue;
                    }
                    u4NetMask = u4CidrSubnetMask[pSecIpInfo->u1SubnetMask];
                    if ((((pSecIpInfo->u4Addr) & u4NetMask) ==
                         (u4IpAddress & u4NetMask)) && (pIpIntf->u4IpAddr != 0))
                    {
                        i4RetVal = CFA_SUCCESS;
                        break;
                    }
                }
                if (i4RetVal == CFA_SUCCESS)
                {
                    break;
                }
            }
            pIpIntf = (tIpIfRecord *) RBTreeGetNext (gIpIfInfo.pIpIfTable,
                                                     (tRBElem *) pIpIntf, NULL);
        }
        while (pIpIntf != NULL);
    }
    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
*    Function Name            : CfaIpIfIsLocalNetOnInterface
*
*    Description              :  Verifes whether the IP address belongs to
*                                the interface's local net
*    
*    Input(s)                  :u4CfaIfIndex - CFA interface index 
*                               u4IpAddress - Address to be verified against
*                                the interface local net
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfIsLocalNetOnInterface (UINT4 u4CfaIfIndex, UINT4 u4IpAddress)
{
    UINT4               u4NetMask;
    INT4                i4RetVal = CFA_FAILURE;
    tIpIfRecord         IpIntfInfo;
    tIpIfRecord        *pIpIntf = NULL;
    tIpAddrInfo        *pSecIpInfo = NULL;

    CFA_IPIFTBL_LOCK ();
    IpIntfInfo.u4IfIndex = u4CfaIfIndex;
    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);

    if ((pIpIntf != NULL) &&
        (CFA_IF_IPPORT ((UINT2) pIpIntf->u4IfIndex) != CFA_INVALID_INDEX))
    {
        u4NetMask = u4CidrSubnetMask[pIpIntf->u1SubnetMask];
        if (((pIpIntf->u4IpAddr) & u4NetMask) == (u4IpAddress & u4NetMask))
        {
            i4RetVal = CFA_SUCCESS;
        }
        if (i4RetVal != CFA_SUCCESS)
        {
            TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo,
                          tIpAddrInfo *)
            {
                if (pSecIpInfo->i4RowStatus != CFA_RS_ACTIVE)
                {
                    continue;
                }
                if (((pSecIpInfo->u4Addr) & u4NetMask)
                    == (u4IpAddress & u4NetMask))
                {
                    i4RetVal = CFA_SUCCESS;
                    break;
                }
            }
        }
    }
    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
*    Function Name            :  CfaIpIfIsSecLocalNetOnInterface
*
*    Description              :  Verifes whether the Secondary IP address is configured
*                                on the interface
*    
*    Input(s)                  :u4CfaIfIndex - CFA interface index 
*                               u4IpAddress - Secondary Address to be verified against
*                                the interface local net
*                               pu4NetMask - Network Mask
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfIsSecLocalNetOnInterface (UINT4 u4CfaIfIndex, UINT4 u4IpAddress,
                                 UINT4 *pu4NetMask)
{
    UINT4               u4NetMask;
    INT4                i4RetVal = CFA_FAILURE;
    tIpIfRecord         IpIntfInfo;
    tIpIfRecord        *pIpIntf = NULL;
    tIpAddrInfo        *pSecIpInfo = NULL;

    CFA_IPIFTBL_LOCK ();
    IpIntfInfo.u4IfIndex = u4CfaIfIndex;
    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);

    if ((pIpIntf != NULL) &&
        (CFA_IF_IPPORT ((UINT2) pIpIntf->u4IfIndex) != CFA_INVALID_INDEX))
    {
        u4NetMask = u4CidrSubnetMask[pIpIntf->u1SubnetMask];
        TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo, tIpAddrInfo *)
        {
            if (pSecIpInfo->i4RowStatus != CFA_RS_ACTIVE)
            {
                continue;
            }
            if (((pSecIpInfo->u4Addr) & u4NetMask) == (u4IpAddress & u4NetMask))
            {
                *pu4NetMask = u4CidrSubnetMask[pSecIpInfo->u1SubnetMask];
                i4RetVal = CFA_SUCCESS;
                break;
            }
        }
    }
    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
*    Function Name            : CfaIpIfIsIpValidOnLocalNetIf
*
*    Description              : Verifes whether the IP address is a valid IP
*                               on the local interface or not.
*                               
*                               The following IP addresses are invalid in a
*                               subnet.
*                               1. Network address in the subnet
*                               2. Broadcast address in the subnet.
*                               
*    
*    Input(s)                  : u4CfaIfIndex - CFA interface index 
*                                u4IpAddress  - Address to be verified
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfIsIpValidOnLocalNetIf (UINT4 u4CfaIfIndex, UINT4 u4IpAddress)
{
    UINT4               u4NetMask;
    INT4                i4RetVal = CFA_SUCCESS;
    tIpIfRecord         IpIntfInfo;
    tIpIfRecord        *pIpIntf = NULL;
    tIpAddrInfo        *pSecIpInfo = NULL;

    CFA_IPIFTBL_LOCK ();

    IpIntfInfo.u4IfIndex = u4CfaIfIndex;
    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);

    if ((pIpIntf != NULL) &&
        (CFA_IF_IPPORT ((UINT2) pIpIntf->u4IfIndex) != CFA_INVALID_INDEX))
    {
        u4NetMask = u4CidrSubnetMask[pIpIntf->u1SubnetMask];

        /* Check for Network address */
        if ((pIpIntf->u4IpAddr & u4NetMask) == u4IpAddress)
        {
            i4RetVal = CFA_FAILURE;
        }

        /* Check for Braodcast address */
        if ((i4RetVal == CFA_SUCCESS) && (pIpIntf->u4BcastAddr == u4IpAddress))
        {
            i4RetVal = CFA_FAILURE;
        }

        /* Check if IP part of the subnet */
        if ((i4RetVal == CFA_SUCCESS) &&
            ((pIpIntf->u4IpAddr & u4NetMask) != (u4IpAddress & u4NetMask)))
        {
            i4RetVal = CFA_FAILURE;
        }

        /* Validate IP in secondary list only if validation has failed in
         * primary */
        if (i4RetVal == CFA_FAILURE)
        {
            TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo,
                          tIpAddrInfo *)
            {
                if (pSecIpInfo->i4RowStatus != CFA_RS_ACTIVE)
                {
                    continue;
                }

                i4RetVal = CFA_SUCCESS;

                u4NetMask = u4CidrSubnetMask[pSecIpInfo->u1SubnetMask];

                /* Check for Network address */
                if ((pSecIpInfo->u4Addr & u4NetMask) == u4IpAddress)
                {
                    i4RetVal = CFA_FAILURE;
                }

                /* Check for Broadcast address */
                if ((i4RetVal == CFA_SUCCESS) &&
                    (pSecIpInfo->u4BcastAddr == u4IpAddress))
                {
                    i4RetVal = CFA_FAILURE;
                }

                /* Check if IP is valid in subnet of secondary IP address */
                if ((i4RetVal == CFA_SUCCESS) &&
                    ((pSecIpInfo->u4Addr & u4NetMask) !=
                     (u4IpAddress & u4NetMask)))
                {
                    i4RetVal = CFA_FAILURE;
                }

                if (i4RetVal == CFA_SUCCESS)
                {
                    break;
                }
            }
        }
    }

    CFA_IPIFTBL_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************
*    Function Name            : CfaIpIfGetHighestIpAddr
*
*    Description              :  Provides the highest ip address available in
*                                in the system for interfaces mapped to default
*                                context
*    
    Input(s)                  : None
*
*    Output(s)                 : pu4IpAddress.
*    
*    Global Variables Referred : None
*
*    Returns                   : None
*****************************************************************************/
VOID
CfaIpIfGetHighestIpAddr (UINT4 *pu4IpAddress)
{

    CfaIpIfGetHighestIpAddrInCxt (VCM_DEFAULT_CONTEXT, pu4IpAddress);
}

/*****************************************************************************
*    Function Name            : CfaIpIfGetHighestIpAddrInCxt
*
*    Description              :  Provides the highest ip address available in
*                                in the system for the interfaces mapped to the
*                                specified context
*    
    Input(s)                  : None
*
*    Output(s)                 : None.
*    
*    Global Variables Referred : None
*
*    Returns                   : None
*****************************************************************************/
VOID
CfaIpIfGetHighestIpAddrInCxt (UINT4 u4ContextId, UINT4 *pu4IpAddress)
{
    tIpIfRecord        *pIpIntf;
    tIpAddrInfo        *pSecIpInfo;
    UINT4               u4HighestIpAddr = 0;
    UINT4               u4IfCxtId = 0;

    CFA_IPIFTBL_LOCK ();

    /* Get the first IP interface node from RBTree */
    pIpIntf = RBTreeGetFirst (gIpIfInfo.pIpIfTable);
    if (pIpIntf != NULL)
    {
        do
        {
            /* A Valid IP interface associated with the given
             * Interface */
            if (CFA_IF_IPPORT ((UINT2) pIpIntf->u4IfIndex) != CFA_INVALID_INDEX)
            {
                VcmGetContextIdFromCfaIfIndex (pIpIntf->u4IfIndex, &u4IfCxtId);
                if (u4IfCxtId == u4ContextId)
                {
                    if (pIpIntf->u4IpAddr > u4HighestIpAddr)
                    {
                        u4HighestIpAddr = pIpIntf->u4IpAddr;
                    }

                    TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo,
                                  tIpAddrInfo *)
                    {
                        if ((pSecIpInfo->i4RowStatus == CFA_RS_ACTIVE) &&
                            (pSecIpInfo->u4Addr > u4HighestIpAddr))
                        {
                            u4HighestIpAddr = pSecIpInfo->u4Addr;
                        }
                    }
                }
            }
            pIpIntf = (tIpIfRecord *) RBTreeGetNext (gIpIfInfo.pIpIfTable,
                                                     (tRBElem *) pIpIntf, NULL);
        }
        while (pIpIntf != NULL);
    }
    *pu4IpAddress = u4HighestIpAddr;
    CFA_IPIFTBL_UNLOCK ();
}

/*****************************************************************************
*    Function Name            : CfaIpIfGetNextIpAddr
*
*    Description              :  Get the next highest address in the system.This
*                                function is used for doing getnext on
*                                ipAddrTable.
*    Input(s)                  :u4IpAddress - IP Address for which next IP 
*                               address is requested
*
*    Output(s)                 : NextIP address.
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfGetNextIpAddr (UINT4 u4IpAddress, UINT4 *pu4NextIpAddress)
{
    return (CfaIpIfGetNextIpAddrInCxt (VCM_DEFAULT_CONTEXT, u4IpAddress,
                                       pu4NextIpAddress));
}

/*****************************************************************************
*    Function Name            : CfaIpIfGetNextIpAddrInCxt
*
*    Description              :  Get the next highest address in the system in
*                                the specified context. This function is used 
*                                for doing getnext on
*                                ipAddrTable.
*    Input(s)                  :u4ContextId - the context identifier
*                               u4IpAddress - IP Address for which next IP 
*                               address is requested
*
*    Output(s)                 : NextIP address.
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfGetNextIpAddrInCxt (UINT4 u4ContextId, UINT4 u4IpAddress,
                           UINT4 *pu4NextIpAddress)
{
    tIpIfRecord        *pIpIntf;
    INT4                i4RetVal = CFA_FAILURE;
    UINT4               u4NextIpAddr = u4IpAddress;
    UINT4               u4IfCxtId = 0;
    UINT1               u1Found = FALSE;

    CFA_IPIFTBL_LOCK ();

    /* Get the first IP interface node from RBTree */
    pIpIntf = RBTreeGetFirst (gIpIfInfo.pIpIfTable);
    if (pIpIntf != NULL)
    {
        do
        {
            /* A Valid IP interface associated with the given
             * Interface */
            if (CFA_IF_IPPORT ((UINT2) pIpIntf->u4IfIndex) != CFA_INVALID_INDEX)
            {
                if (VcmGetContextIdFromCfaIfIndex
                    (pIpIntf->u4IfIndex, &u4IfCxtId) == VCM_FAILURE)
                {
                    return CFA_FAILURE;
                }

                /* Check if the interface is mapped to the specified context. If
                 * not continue with the next interface.
                 */
                if (u4ContextId != u4IfCxtId)
                {
                    pIpIntf = (tIpIfRecord *) RBTreeGetNext
                        (gIpIfInfo.pIpIfTable, (tRBElem *) pIpIntf, NULL);
                    continue;
                }
                if (u1Found == FALSE)
                {
                    if (pIpIntf->u4IpAddr > u4IpAddress)
                    {
                        u4NextIpAddr = pIpIntf->u4IpAddr;
                        u1Found = TRUE;
                    }
                }
                else
                {
                    if ((pIpIntf->u4IpAddr > u4IpAddress) &&
                        (pIpIntf->u4IpAddr < u4NextIpAddr))
                    {
                        u4NextIpAddr = pIpIntf->u4IpAddr;
                    }
                }
            }
            pIpIntf = (tIpIfRecord *) RBTreeGetNext (gIpIfInfo.pIpIfTable,
                                                     (tRBElem *) pIpIntf, NULL);
        }
        while (pIpIntf != NULL);
    }
    if (u1Found == TRUE)
    {
        *pu4NextIpAddress = u4NextIpAddr;
        i4RetVal = CFA_SUCCESS;
    }
    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
*    Function Name            : CfaIpIfGetNextSecondaryAddress
*
*    Description              :  Get the next Secondary address on the interface
*    
*    Input(s)                  :u4CfaIfIndex - CFA interface index 
*                               u4IpAddress -  Interface IP address for which 
*                               the next IP address is sougght
*
*    Output(s)                 : NextIP address and its Mask.
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfGetNextSecondaryAddress (UINT4 u4CfaIfIndex, UINT4 u4IpAddress,
                                UINT4 *pu4NextIpAddress, UINT4 *pu4NetMask)
{
    INT4                i4RetVal = CFA_FAILURE;
    tIpIfRecord         IpIntfInfo;
    tIpIfRecord        *pIpIntf = NULL;
    tIpAddrInfo        *pSecIpInfo = NULL;

    CFA_IPIFTBL_LOCK ();

    IpIntfInfo.u4IfIndex = u4CfaIfIndex;
    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
    if (pIpIntf != NULL)
    {
        TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo, tIpAddrInfo *)
        {
            if (pSecIpInfo->i4RowStatus != CFA_RS_ACTIVE)
            {
                continue;
            }

            if (pSecIpInfo->u1AllocMethod == CFA_IP_IF_VIRTUAL)
            {
                continue;
            }

            /* If the getnext is requested with primary ip address,first
             * secondary address on the interface will be provided */
            if ((u4IpAddress == pIpIntf->u4IpAddr) ||
                (pSecIpInfo->u4Addr > u4IpAddress))
            {
                *pu4NextIpAddress = pSecIpInfo->u4Addr;
                *pu4NetMask = u4CidrSubnetMask[pSecIpInfo->u1SubnetMask];
                i4RetVal = CFA_SUCCESS;
                break;
            }
        }
    }
    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
*    Function Name            : CfaIpIfGetNextIndexIpIfTable
*
*    Description              :  Get the next IP interface Index
*    
*    Input(s)                  : u4CfaIfIndex - CFA interface index 
*
*    Output(s)                 : Next IP interface Index
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
INT4
CfaIpIfGetNextIndexIpIfTable (UINT4 u4CfaIfIndex, UINT4 *pu4NextIfIndex)
{
    INT4                i4RetVal = CFA_FAILURE;
    tIpIfRecord        *pIpIntf = NULL;
    tIpIfRecord         IpIntfInfo;
    UINT1               u1BridgedIfaceStatus = 0;

    CFA_IPIFTBL_LOCK ();

    IpIntfInfo.u4IfIndex = u4CfaIfIndex;
    do
    {
        pIpIntf = RBTreeGetNext (gIpIfInfo.pIpIfTable, &IpIntfInfo, NULL);

        if (pIpIntf != NULL)
        {
            if (CfaGetIfBridgedIfaceStatus (pIpIntf->u4IfIndex,
                                            &u1BridgedIfaceStatus) ==
                CFA_FAILURE)
            {
                CFA_IPIFTBL_UNLOCK ();
                return i4RetVal;
            }
            IpIntfInfo.u4IfIndex = pIpIntf->u4IfIndex;
        }
    }
    while (u1BridgedIfaceStatus == CFA_ENABLED && pIpIntf != NULL);

    if (pIpIntf != NULL)
    {
        *pu4NextIfIndex = pIpIntf->u4IfIndex;
        i4RetVal = CFA_SUCCESS;
    }
    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
*    Function Name            :  CfaIpIfGetSecondaryAddressCount
*
*    Description              :  Get the no of secondary address available on
*                                the interface
*    
*    Input(s)                  : u4IfIndex - CFA interface index 
*
*    Output(s)                 : No of secondary addresses available on the
*                                interface
*    
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE 
*****************************************************************************/
VOID
CfaIpIfGetSecondaryAddressCount (UINT4 u4IfIndex, UINT4 *pu4Count)
{
    tIpIfRecord        *pIpIntf = NULL;
    tIpIfRecord         IpIntfInfo;
    UINT4               u4SecondaryAddrCnt = 0;

    CFA_IPIFTBL_LOCK ();

    IpIntfInfo.u4IfIndex = u4IfIndex;
    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);

    if (pIpIntf != NULL)
    {
        u4SecondaryAddrCnt = TMO_SLL_Count (&(pIpIntf->SecondaryIpList));
    }
    CFA_IPIFTBL_UNLOCK ();

    *pu4Count = u4SecondaryAddrCnt;
}

/*************************************************************************
 * Function           : CfaIfGetIpAllocProto 
 * 
 * Input(s)           : u4IfIndex -CFA Interface Index
 *
 * Output(s)          : pu1IpAllocProto - Dynamic Allocation protocol filled.
 *
 * Returns            : CFA_SUCCESS/CFA_FAILURE 
 *
 * Action             : Get the allocation protocol configured over the 
 *                      interface 
 * ***************************************************************************/
INT4
CfaIfGetIpAllocProto (UINT4 u4IfIndex, UINT1 *pu1IpAllocProto)
{
    tIpConfigInfo       IpIfInfo;

    if (CfaIpIfGetIfInfo (u4IfIndex, &IpIfInfo) == CFA_SUCCESS)
    {
        *pu1IpAllocProto = IpIfInfo.u1AddrAllocProto;
        return CFA_SUCCESS;
    }
    return CFA_FAILURE;
}

/*************************************************************************
 * Function           : CfaIfGetIpAllocMethod 
 * 
 * Input(s)           : u4IfIndex - CFA Interface index 
 *
 * Output(s)          : pu1IpAllocMethod - Allocation method filled.
 *
 * Returns            : CFA_SUCCESS/CFA_FAILURE 
 *
 * Action             : Get the allocation method for the interface  
*****************************************************************************/
INT4
CfaIfGetIpAllocMethod (UINT4 u4IfIndex, UINT1 *pu1IpAllocMethod)
{
    tIpConfigInfo       IpIfInfo;

    if (CfaIpIfGetIfInfo (u4IfIndex, &IpIfInfo) == CFA_SUCCESS)
    {
        *pu1IpAllocMethod = IpIfInfo.u1AddrAllocMethod;
        return CFA_SUCCESS;
    }
    return CFA_FAILURE;
}

/*************************************************************************
 * Function           : CfaIfGetIpAddress
 * 
 * Input(s)           : u4IfIndex - CFA Interface index 
 *
 * Output(s)          : pu1IpAddress - Primary Ip address of the index
 *
 * Returns            : CFA_SUCCESS/CFA_FAILURE 
 *
 ****************************************************************************/
INT4
CfaIfGetIpAddress (UINT4 u4IfIndex, UINT4 *pu1IpAddress)
{
    tIpConfigInfo       IpIfInfo;

    if (CfaIpIfGetIfInfo (u4IfIndex, &IpIfInfo) == CFA_SUCCESS)
    {
        *pu1IpAddress = IpIfInfo.u4Addr;
        return CFA_SUCCESS;
    }
    return CFA_FAILURE;
}

/*************************************************************************
 * Function           : CfaIpIfTblLock 
 * 
 * Input(s)           : None.  
 *
 * Output(s)          : None.
 *
 * Returns            : CFA_SUCCESS/CFA_FAILURE 
 *
 * Action             : Takes a task Sempaphore  
*****************************************************************************/

INT4
CfaIpIfTblLock (VOID)
{
    if (OsixSemTake (gIpIfInfo.SemId) != OSIX_SUCCESS)
    {
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

/*************************************************************************
 * Function           : CfaIpIfTblUnlock 
 * 
 * Input(s)           : None.  
 *
 * Output(s)          : None.
 *
 * Returns            : CFA_SUCCESS/CFA_FAILURE 
 *
 * Action             : Takes a task Sempaphore  
*****************************************************************************/
INT4
CfaIpIfTblUnlock (VOID)
{
    OsixSemGive (gIpIfInfo.SemId);
    return CFA_SUCCESS;
}

/*****************************************************************************
 * *    Function Name            : CfaIpIfGetIfIndexFromHostIpAddressInCxt
 * *
 * *    Description              :  Provides the Host  having the given IP
 * *                                Address which is mapped to the specified context
 * *
 * *    Input(s)                  : u4ContextId - The context id
 * *                                u4IpAddress - IP address for which the
 * *                                interface index to be derived
 * *
 * *    Output(s)                 :  Interface index.
 * *
 * *    Global Variables Referred : gIpIfInfo.pIpIfTable
 * *
 * *    Returns                   : CFA_SUCCESS/CFA_FAILURE
 * *****************************************************************************/
INT4
CfaIpIfGetIfIndexFromHostIpAddressInCxt (UINT4 u4ContextId, UINT4 u4IpAddress,
                                         UINT4 *pu4CfaIfIndex)
{
    tIpIfRecord        *pIpIntf;
    tIpAddrInfo        *pSecIpInfo;
    INT4                i4RetVal = CFA_FAILURE;
    UINT4               u4IfCxtId = 0;
    UINT4               u4NetMask;

    CFA_IPIFTBL_LOCK ();

    /* Get the IP interface node from RBTree */
    pIpIntf = RBTreeGetFirst (gIpIfInfo.pIpIfTable);
    if (pIpIntf != NULL)
    {
        do
        {
            /* A Valid IP interface associated with the given
             *              * Interface */
            if (CFA_IF_IPPORT ((UINT2) pIpIntf->u4IfIndex) != CFA_INVALID_INDEX)
            {
                /* Check if the interface is mapped to the specified context. */
                VcmGetContextIdFromCfaIfIndex (pIpIntf->u4IfIndex, &u4IfCxtId);
                if (u4IfCxtId != u4ContextId)
                {
                    pIpIntf = (tIpIfRecord *) RBTreeGetNext
                        (gIpIfInfo.pIpIfTable, (tRBElem *) pIpIntf, NULL);
                    continue;
                }

                /* Check whether IP address exists as primary IP address */

                u4NetMask = u4CidrSubnetMask[pIpIntf->u1SubnetMask];

                if ((u4NetMask != 0) && (((pIpIntf->u4IpAddr) & u4NetMask) ==
                                         (u4IpAddress & u4NetMask)))

                {
                    *pu4CfaIfIndex = pIpIntf->u4IfIndex;
                    i4RetVal = CFA_SUCCESS;
                    break;
                }
                /* Check adddress exists as secondary address */
                TMO_SLL_Scan (&(pIpIntf->SecondaryIpList), pSecIpInfo,
                              tIpAddrInfo *)
                {
                    if ((u4NetMask != 0)
                        && (((pSecIpInfo->u4Addr) & u4NetMask) ==
                            (u4IpAddress & u4NetMask)))
                    {
                        *pu4CfaIfIndex = pIpIntf->u4IfIndex;
                        i4RetVal = CFA_SUCCESS;
                        break;
                    }
                }
                if (i4RetVal == CFA_SUCCESS)
                {
                    break;
                }
            }
            pIpIntf = (tIpIfRecord *) RBTreeGetNext (gIpIfInfo.pIpIfTable,
                                                     (tRBElem *) pIpIntf, NULL);
        }
        while (pIpIntf != NULL);
    }

    CFA_IPIFTBL_UNLOCK ();
    return i4RetVal;
}

 /*****************************************************************************
 *    Function Name            : CfaIpIfCheckLoopbackAddr
 *
 *    Description              :  Verifes whether the IP address belongs to
 *                                the interface's loopback address
 *
 *    Input(s)                  : u4IpAddress - Address to be verified against
 *                                the interface loopback addresses
 *
 *    Output(s)                 : None.
 *
 *    Global Variables Referred : None
 *
 *    Returns                   : CFA_SUCCESS/CFA_FAILURE
 *****************************************************************************/
INT4
CfaIpIfCheckLoopbackAddr (UINT4 u4IpAddress)
{
    UINT4               u4CfaIfIndex = 0;

    if (CfaIpIfGetIfIndexFromIpAddress (u4IpAddress, &u4CfaIfIndex) ==
        CFA_SUCCESS)
    {
        if (CfaIsLoopBackIntf (u4CfaIfIndex) == TRUE)
        {
            return CFA_SUCCESS;
        }
        else
        {
            return CFA_FAILURE;
        }
    }
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaIsInterfaceVlanExists                             */
/*                                                                           */
/* Description        : This function function is used to check whether the  */
/*                      the VLAN ID passed is already used by l3 VLAN        */
/*                                                                           */
/* Input(s)           : u2PortVlanId :VLAN ID which is to be checked         */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaIsInterfaceVlanExists (UINT2 u2PortVlanId)
{
    tIpIfRecord        *pIpIntf = NULL;
    UINT2               u2VlanId = 0;

    CFA_IPIFTBL_LOCK ();
    pIpIntf = RBTreeGetFirst (gIpIfInfo.pIpIfTable);

    while (pIpIntf != NULL)
    {
        /* Check whether PortVlan is already in use for Interface VLAN */
        if (CFA_SUCCESS == CfaGetVlanId (pIpIntf->u4IfIndex, &u2VlanId))
        {
            if (u2VlanId == u2PortVlanId)
            {
                CFA_IPIFTBL_UNLOCK ();
                return CFA_SUCCESS;
            }
        }

        pIpIntf = RBTreeGetNext (gIpIfInfo.pIpIfTable,
                                 (tRBElem *) pIpIntf, NULL);
    }                            /* End of while */

    CFA_IPIFTBL_UNLOCK ();
    return CFA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfIpPortVlanId                                 */
/*                                                                           */
/* Description        : This function gets the PortVlanId of the Routerport */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      pi4PortVlanId:Pointer to the PortVlanId              */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/

INT4
CfaGetIfIpPortVlanId (UINT4 u4IfIndex, INT4 *pi4PortVlanId)
{
    tIpIfRecord         IpIntfInfo;
    tIpIfRecord        *pIpIntf = NULL;

    MEMSET (&IpIntfInfo, 0, sizeof (tIpIfRecord));

    CFA_IPIFTBL_LOCK ();
    IpIntfInfo.u4IfIndex = u4IfIndex;

    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);
    if (pIpIntf != NULL)
    {
        /* Port-VlanId Associated with the Interface */
        *pi4PortVlanId = (INT4) pIpIntf->u2PortVlanId;
        CFA_IPIFTBL_UNLOCK ();
        return CFA_SUCCESS;

    }
    CFA_IPIFTBL_UNLOCK ();
    return CFA_FAILURE;
}
