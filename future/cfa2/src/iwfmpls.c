/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: iwfmpls.c,v 1.14 2012/02/13 12:12:08 siva Exp $
*
* Description:This file contains the IWF routines for MPLS.
*
*******************************************************************/
#ifdef MPLS_WANTED

#include "cfainc.h"

/*****************************************************************************
 *    Function Name             : CfaIwfMplsHandleOutGoingPkt
 *
 *    Description               : This function sends the mpls packet from  
 *                                mpls to the corresponding layer
 *
 *    Input(s)                  : PBuf - Pointer to the CRU Buffer.
 *                                MIB-2 ifIndex of the driver port.
 *                                u4PktSize - Size of the buffer to be TX
 *
 *    Output(s)                 : None.
 *
 *    Global Variables Referred : gapIfTable.
 *
 *    Global Variables Modified : None.
 *
 *    Returns                   : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIwfMplsHandleOutGoingPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                             UINT4 u4MplsIfIndex, UINT4 u4PktSize)
{
    UINT2               u2Protocol;
    UINT1               au1DstMac[CFA_ENET_ADDR_LEN];
    UINT1               u1EncapType;

    CFA_GET_DEST_MEDIA_ADDR (pBuf, au1DstMac);
    u1EncapType = CFA_GET_ENCAP_TYPE (pBuf);
    u2Protocol = CFA_GET_PROTOCOL (pBuf);
    if (CfaIwfEnetProcessTxFrame (pBuf, (UINT2) u4MplsIfIndex, au1DstMac,
                                  u4PktSize,
                                  u2Protocol, u1EncapType) != CFA_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return (CFA_FAILURE);
    }
    CFA_DBG1 (BUFFER_TRC | CFA_TRC_ALL_TRACK | CFA_TRC_ERROR, CFA_IWF,
              "CfaIwfMplsHandleOutGoingPkt - "
              "write to LL on iface %d - SUCCESS.\n", u4MplsIfIndex);

    return (CFA_SUCCESS);
}

/* ************************************************************************* *
 *  Function Name   : CfaIwfMplsProcessOutGoingL2Pkt                         *
 *  Description     : Called from CFA task to post packet to MPLS            *
 *  Input           : pBuf - Packet Buffer                                   *
 *                    u4IfIndex - Incoming interface                         *
 *                    u4VfId - PW index if the u1PktType is UCAST            *
 *                             VPLS instance index if the u1PktType is BCAST *
 *                    u1PktType - CFA_LINK_UCAST / CFA_LINK_BCAST            *
 *  Output          : NONE                                                   *
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS                            *
 * ************************************************************************* */
INT4
CfaIwfMplsProcessOutGoingL2Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                                UINT4 u4VfId, UINT1 u1PktType)
{
    return (MplsProcessL2Pkt (pBuf, u4IfIndex, u4VfId, u1PktType));
}

#endif /* MPLS_WANTED */
