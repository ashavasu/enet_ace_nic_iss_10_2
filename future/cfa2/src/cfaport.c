/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfaport.c,v 1.114 2018/01/05 09:57:07 siva Exp $
 *
 * Description:This file contains the routines of CFA modules  
 *             which need to be ported when moving to a    
 *             different target or while integrating with    
 *             other products like IP and PPP/MP. Along with   
 *             this file and its associated header file, the   
 *             GDD Module may also need modifications.     
 *
 *******************************************************************/
#include "cfainc.h"

/*****************************************************************************
 *
 *    Function Name        : CfaIfmDefaultInterfaceInit
 *
 *    Description        : This function initialises the default router 
 *                         interface. Currently this is eth0 for LINUX.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure.
 *               
 *    Global Variables Modified : gapIfTable (Interface table) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if parsing of config succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmDefaultInterfaceInit (VOID)
{
    tCfaDefPortParams   CfaDefPortParams;
    UINT1               au1DefaultVlanInterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1IfType = 0;

    CFA_DBG (CFA_TRC_ALL, CFA_IFM, "Entering CfaIfmDefaultInterfaceInit.\n");

    if ((CFA_MGMT_PORT == TRUE) || (CFA_DEFAULT_IFINDEX_SUPPORTED))
    {
        CfaGetDefaultPortParams (&CfaDefPortParams);

        if (CfaIfmCreateAndInitIfEntry (CFA_DEFAULT_ROUTER_IFINDEX, CFA_ENET,
                                        CfaDefPortParams.au1IntfName) !=
            CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                     "Error in CfaIfmDefaultInterfaceInit - "
                     "interface creation - FAILURE.\n");
            return (CFA_FAILURE);
        }

        CFA_IF_RS (CFA_DEFAULT_ROUTER_IFINDEX) = CFA_RS_ACTIVE;

        CfaSetIfActiveStatus (CFA_DEFAULT_ROUTER_IFINDEX, CFA_TRUE);
        CfaSetCdbRowStatus (CFA_DEFAULT_ROUTER_IFINDEX, CFA_RS_ACTIVE);

        if (CfaIsMgmtPort (CFA_DEFAULT_ROUTER_IFINDEX) == FALSE)
        {
            /* Creating the interface mapping in VCM */
            VcmCfaCreateIfaceMapping (L2IWF_DEFAULT_CONTEXT,
                                      CFA_DEFAULT_ROUTER_IFINDEX);
        }
        /* Default port creation will be indicated to L2 when all the other
         * ports are created in the system.
         */
    }

    if ((CFA_MGMT_PORT == TRUE) || (gu4IsIvrEnabled == CFA_DISABLED))
    {
        CFA_DBG (CFA_TRC_ALL_TRACK, CFA_IFM,
                 "Exiting CfaIfmDefaultInterfaceInit - "
                 "Default interface init - SUCCESS.\n");
        return CFA_SUCCESS;
    }

    /* Default IP Interface (Vlan Interface) is created only when the system 
     * does not have OOB mgmt port. */

    MEMSET (au1DefaultVlanInterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);

#ifdef WGS_WANTED

    u1IfType = CFA_L2VLAN;

    STRCPY (au1DefaultVlanInterfaceName, CFA_DEFAULT_MGMT_INTERFACE);

#else

    u1IfType = CFA_L3IPVLAN;

    SPRINTF ((CHR1 *) au1DefaultVlanInterfaceName, "%s%u",
             CFA_DEFAULT_VLAN_INTERFACE, CFA_DEFAULT_VLAN_ID);

#endif /* WGS_WANTED */

    if ((IssGetRestoreFlagFromNvRam () != ISS_CONFIG_RESTORE) ||
        (IssGetRestoreOptionFromNvRam () == ISS_CONFIG_REMOTE_RESTORE))
    {
        if (CfaIfmCreateAndInitIfEntry (CFA_DEFAULT_ROUTER_VLAN_IFINDEX,
                                        u1IfType,
                                        au1DefaultVlanInterfaceName) !=
            CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                     "Error in CfaIfmDefaultInterfaceInit - "
                     "interface creation - FAILURE.\n");
            return (CFA_FAILURE);
        }

        CfaSetIfAlias (CFA_DEFAULT_ROUTER_VLAN_IFINDEX,
                       au1DefaultVlanInterfaceName);

        /* make Rowstatus active */
        CFA_IF_RS (CFA_DEFAULT_ROUTER_VLAN_IFINDEX) = CFA_RS_ACTIVE;

        CfaSetIfActiveStatus (CFA_DEFAULT_ROUTER_VLAN_IFINDEX, CFA_TRUE);
        CfaSetCdbRowStatus (CFA_DEFAULT_ROUTER_VLAN_IFINDEX, CFA_RS_ACTIVE);
    }

    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_IFM,
             "Exiting CfaIfmDefaultInterfaceInit - "
             "Default interface init - SUCCESS.\n");
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /*Allocating an entry for STACK IVR in interface table */
        MEMSET (au1DefaultVlanInterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);
        SPRINTF ((CHR1 *) au1DefaultVlanInterfaceName, "%s%u",
                 CFA_DEFAULT_VLAN_INTERFACE, CFA_DEFAULT_STACK_VLAN_ID);

        if (CfaIfmCreateAndInitIfEntry (CFA_DEFAULT_STACK_IFINDEX,
                                        CFA_L3IPVLAN,
                                        au1DefaultVlanInterfaceName) !=
            CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                     "Error in CfaIfmDefaultInterfaceInit - "
                     "interface creation - FAILURE.\n");
            return (CFA_FAILURE);
        }
        CfaSetIfAlias (CFA_DEFAULT_STACK_IFINDEX, au1DefaultVlanInterfaceName);
        CFA_IF_RS (CFA_DEFAULT_STACK_IFINDEX) = CFA_RS_ACTIVE;
        CfaSetIfActiveStatus (CFA_DEFAULT_STACK_IFINDEX, CFA_TRUE);
    }

    return (CFA_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : CfaRmEnqChkSumMsgToRm                                */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
CfaRmEnqChkSumMsgToRm (UINT2 u2AppId, UINT2 u2ChkSum)
{
#ifdef RM_WANTED
    if ((RmEnqChkSumMsgToRmFromApp (u2AppId, u2ChkSum)) == RM_FAILURE)
    {
        return CFA_FAILURE;
    }
#else
    UNUSED_PARAM (u2AppId);
    UNUSED_PARAM (u2ChkSum);
#endif
    return CFA_SUCCESS;
}

#ifdef IKE_WANTED

/*****************************************************************************
 *
 *    Function Name        : CfaHandlePacketFromIke
 *
 *    Description        : Receives data from the IKE module and enqueues the 
 *                         message to the CFA task. 
 *
 *    Input(s)            : pBuf - Pointer to the CRU buffer 
 *                                 containing the packet.
 *
 *    Output(s)            : None.
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE.
 *****************************************************************************/
INT4
CfaHandlePacketFromIke (tCRU_BUF_CHAIN_HEADER * pBuf)
{

    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandlePktFromIke - " "CFA is not initialised.\n");
        return (CFA_FAILURE);
    }
    if (OsixQueSend (CFA_PACKET_QID, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandlePktFromIke - "
                 "Pkt Enqueue to CFA FAIL.\n");
        return (CFA_FAILURE);
    }

    /* send an explicit event to the task */

    if (OsixEvtSend (CFA_TASK_ID, CFA_PACKET_ARRIVAL_EVENT) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandlePktFromIke - event send to CFA FAIL.\n");
        return (CFA_FAILURE);
    }
    return (CFA_SUCCESS);

}

#endif /* IKE_WANTED */
/*****************************************************************************
 *
 *    Function Name        : CfaHandlePktFromIp
 *
 *    Description        : Receives the outgoing packets from IP. 
 *                         If the Fast forwarding and Host Cache add flag is 
 *                         enabled, it checks whether an entry is available 
 *                         in the Host Cache Table with this destination address. 
 *                         It the entry is not avaliable then it adds an entry 
 *                         in the HCT and transmits the packet on the outgoing
 *                         interface. If the fastforwarding is not enabled or 
 *                         the Host Cache Add flag s not enabled it 
 *                         just transmits 
 *                         the packet on the outgoing interface.
 *
 *    Input(s)            : PBuf - Pointer to the CRU buffer
 *                                 containing the IP packet.
 *                          U2IfIndex - Outgoing interface index
 *                          U4NextHopIpAddr - Next Hop Ip Addr given by IP.
 *                          U1HctAddFlag - Indicates whether an 
 *                                         entry is to be added 
 *                          in the Host Cache Table.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gFfConfigInfo, gapIfTable.
 *
 *    Global Variables Modified : gFfConfigInfo.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE.
 *****************************************************************************/
INT4
CfaHandlePktFromIp (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                    UINT4 u4NextHopIpAddr, UINT1 *au1MediaAddr,
                    UINT2 u2Protocol, UINT1 u1PktType, UINT1 u1EncapType)
{
    UINT1               au1UserData[CFA_MAX_USER_DATA_LEN];

    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                 "Error in CfaHandlePktFromIp - CFA is not initialised.\n");
        return (CFA_FAILURE);
    }
/* check if the IfIndex is valid */
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                  "Error in CfaHandlePktFromIp - invalid interface %d.\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

    if (!CFA_IF_ENTRY_USED ((UINT2) u4IfIndex))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                  "Error in CfaHandlePktFromIp - unused interface %d.\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

/* the RARP check is required as during Init we run RARP before registering 
with IP */
#ifdef IP_WANTED
    if ((CFA_IF_IPPORT (u4IfIndex) == CFA_INVALID_INDEX) &&
        (u2Protocol != CFA_ENET_RARP))

    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                  "Error in CfaHandlePktFromIp - interface %d not used by IP.\n",
                  u4IfIndex);
        return CFA_FAILURE;
    }
#endif

    CFA_SET_IFINDEX (pBuf, (UINT2) u4IfIndex);
    CFA_SET_PKT_TYPE (pBuf, u1PktType);
    CFA_SET_DEST_MEDIA_ADDR (pBuf, au1MediaAddr);
    CFA_SET_ENCAP_TYPE (pBuf, u1EncapType);
    MEMCPY (au1UserData, &u4NextHopIpAddr, sizeof (UINT4));
    CFA_SET_NH_IP_ADDR (pBuf, au1UserData);
    CFA_SET_PROTOCOL (pBuf, u2Protocol);

    if (u2Protocol == CFA_ENET_IPV4)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "In CfaHandlePktFromIp - recd IP Pkt on interface %d.\n",
                  u4IfIndex);
        CFA_SET_COMMAND (pBuf, IP_TO_CFA);
        CFA_DUMP (CFA_TRC_IP_PKT_DUMP, pBuf);
    }
#ifdef IP6_WANTED
    else if (u2Protocol == CFA_ENET_IPV6)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "In CfaHandlePktFromIp - recd IP6 Pkt on interface %d.\n",
                  u4IfIndex);
        CFA_SET_COMMAND (pBuf, IP6_TO_CFA);
        CFA_DUMP (CFA_TRC_IP_PKT_DUMP, pBuf);
    }
#endif /* IP6_WANTED */
    else
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "In CfaHandlePktFromIp - recd ARP/RARP Pkt on interface %d.\n",
                  u4IfIndex);
        CFA_SET_COMMAND (pBuf, ARP_TO_CFA);
        CFA_DUMP (CFA_TRC_ARP_PKT_DUMP, pBuf);
    }

    if (OsixQueSend (CFA_PACKET_QID, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaHandlePktFromIp - "
                  "Pkt Enqueue to CFA on interface %d FAIL.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

/* send an explicit event to the task */
    OsixEvtSend (CFA_TASK_ID, CFA_PACKET_ARRIVAL_EVENT);

    CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
              "Exiting CfaHandlePktFromIp - "
              "Pkt Enqueue to CFA on interface %d SUCCESS.\n", u4IfIndex);
    return (CFA_SUCCESS);

}

/*****************************************************************************
 *
 *    Function Name        : CfaNotifyIpUp
 *
 *    Description        : This API sends an event to the CFA task indicating 
 *                         the completion of the Initialization of the IP modules. 
 *                         This is used for synchronization between CFA and other 
 *                         higher layer tasks.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gFfConfigInfo.
 *
 *    Global Variables Modified : gFfConfigInfo.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE.
 *****************************************************************************/
INT4
CfaNotifyIpUp (VOID)
{
    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaNotifyIpUp - CFA is not initialised.\n");
        return (CFA_FAILURE);
    }
    /* SELF may be changed in dual processor */
    OsixEvtSend (CFA_TASK_ID, CFA_IP_UP_EVENT);

    CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
             "In CfaNotifyIpUp - event send to IP SUCCESS.\n");
    return (CFA_SUCCESS);

}

#ifdef MPLS_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaHandlePktFromMpls
 *
 *    Description        : Receives data from the Mpls module and enqueues the 
 *                         message to the CFA task.
 *                         The API handles data in both, 
 *                         incoming (from lower layer to higher layer - IP) and 
 *                         outgoing (for transmitting out over the lower layer)
 *
 *    Input(s)            : pBuf - the pointer to the data buffer
 *                          to be transmitted
 *                          u4IfIndex - the MIB-2 ifIndex assigned 
 *                          to the interface.
 *                          u1Direction - the direction of the packet transfer 
 *                          u1PktType - type of the pkt UCAST/BCAST/MCAST
 *
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gFfConfigInfo, gapIfTable.
 *
 *    Global Variables Modified : gFfConfigInfo.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                   : CFA_SUCCESS/CFA_FAILURE.
 *****************************************************************************/
INT4
CfaHandlePktFromMpls (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                      UINT1 u1Direction, UINT1 *pu1MediaAddr, UINT1 u1PktType)
{

    UINT4               u4Reserved2 = 0;
    UINT4               u4Reserved3 = 0;

#ifdef PPP_WANTED
    UINT4               u4PktSize;
#endif
#ifdef MPLS_IPV6_WANTED
    tCfaRegInfo         CfaInfo;
#ifdef IP6_WANTED
    UINT2               u2LenOrType = 0;
#endif
#endif

    switch (u1Direction)
    {
        case MPLS_TO_CFA:        /* Ingress/LSR */
            /* Outgoing pkt(labelled) from mpls, so enque it to cfa */
            if (CFA_INITIALISED != TRUE)
            {
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "Error In CfaHandlePktFromMpls - "
                         "CFA is not initialised\n");
                return (CFA_FAILURE);
            }
            u4Reserved3 = pBuf->ModuleData.u4Reserved3;
            u4Reserved2 = pBuf->ModuleData.u4Reserved2;
            GET_MODULE_DATA_PTR (pBuf)->au1Reserved[0] =
                u4Reserved3 & 0x000000ff;
            GET_MODULE_DATA_PTR (pBuf)->au1Reserved[1] =
                u4Reserved2 & 0x000000ff;

            CFA_SET_COMMAND (pBuf, MPLS_TO_CFA);
            CFA_SET_IFINDEX (pBuf, (UINT2) u4IfIndex);
            CFA_SET_PKT_TYPE (pBuf, u1PktType);
            if (NULL != pu1MediaAddr)
            {
                CFA_SET_DEST_MEDIA_ADDR (pBuf, pu1MediaAddr);
            }
            CFA_SET_ENCAP_TYPE (pBuf, CFA_ENCAP_ENETV2);
            CFA_SET_PROTOCOL (pBuf, CFA_ENET_MPLS);
            if (OsixQueSend (CFA_PACKET_QID, (UINT1 *) &pBuf,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                          "Error In CfaHandlePktFromMpls - "
                          "Pkt Enqueue to CFA on interface %d FAIL.\n",
                          u4IfIndex);
                return (CFA_FAILURE);
            }
            OsixEvtSend (CFA_TASK_ID, CFA_PACKET_ARRIVAL_EVENT);
            break;

        case L2PKT_MPLS_TO_CFA:
            /* For pseudowire visibility for L2 modules -
             * CFA receives the packet from MPLS after removing
             * pseudowire label. This Packet is destined L2
             * modules */
            if (CFA_INITIALISED != TRUE)
            {
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "Error In CfaHandlePktFromMpls - "
                         "CFA is not initialised\n");
                return (CFA_FAILURE);
            }
            CFA_SET_COMMAND (pBuf, L2PKT_MPLS_TO_CFA);
            CFA_SET_IFINDEX (pBuf, (UINT2) u4IfIndex);
            if (OsixQueSend (CFA_PACKET_QID, (UINT1 *) &pBuf,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                          "Error In CfaHandlePktFromMpls - "
                          "Pkt Enqueue to CFA on interface %d FAIL.\n",
                          u4IfIndex);
                return (CFA_FAILURE);
            }
            OsixEvtSend (CFA_TASK_ID, CFA_PACKET_ARRIVAL_EVENT);
            break;

        case RPTE_TO_CFA:        /* Ingress/LSR */
            /* Outgoing pkt(labelled) from mpls, so enque it to cfa */
            if (CFA_INITIALISED != TRUE)
            {
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "Error In CfaHandlePktFromMpls - "
                         "CFA is not initialised.\n");
                return (CFA_FAILURE);
            }
            CFA_SET_COMMAND (pBuf, MPLS_TO_CFA);
            CFA_SET_IFINDEX (pBuf, (UINT2) u4IfIndex);
            CFA_SET_PKT_TYPE (pBuf, u1PktType);
            if (NULL != pu1MediaAddr)
            {
                CFA_SET_DEST_MEDIA_ADDR (pBuf, pu1MediaAddr);
            }
            CFA_SET_ENCAP_TYPE (pBuf, CFA_ENCAP_ENETV2);
            CFA_SET_PROTOCOL (pBuf, IP_PROTOCOL);
            if (OsixQueSend (CFA_PACKET_QID, (UINT1 *) &pBuf,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                          "Error In CfaHandlePktFromMpls - "
                          "Pkt Enqueue to CFA on interface %d FAIL.\n",
                          u4IfIndex);
                return (CFA_FAILURE);
            }
            OsixEvtSend (CFA_TASK_ID, CFA_PACKET_ARRIVAL_EVENT);
            break;
/* MPLS_IPv6 add start*/
#ifdef MPLS_IPV6_WANTED
        case IPV6_MPLS_TO_L3:
            MEMSET (&CfaInfo, 0, sizeof (tCfaRegInfo));
            u2LenOrType = CFA_ENET_IPV6;
            CfaInfo.u4IfIndex = u4IfIndex;
            CfaInfo.CfaPktInfo.pBuf = pBuf;
            if (CfaNotifyIfPktRcvd (u2LenOrType, &CfaInfo) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
                          "Exiting CfaIwfEnetProcessRxFrame - IP6 Pkt on interface %d - FAILURE\n",
                          u4IfIndex);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                return CFA_FAILURE;
            }
            else
            {
                CFA_DBG (CFA_TRC_ERROR, CFA_FFM,
                         "Successfully delivered the message to IPV6 : "
                         "Exiting CfaProcessIncomingIpv6Pkt \n");
                return CFA_SUCCESS;
            }
#endif
/* MPLS_IPv6 add end*/
        case MPLS_TO_L3:        /* Egress */
#ifdef IP_WANTED
            if (IpHandlePktFromCfa (pBuf,
                                    (UINT2) (CFA_IF_IPPORT ((UINT2) u4IfIndex)),
                                    u1PktType) != IP_SUCCESS)
            {
                return CFA_FAILURE;
            }
#endif
            break;

        case MPLS_TO_L2:        /* Egress */
            VlanL2VpnFwdMplsPktOnPorts (pBuf,    /* context */
                                        (UINT4) CRU_BUF_Get_U4Reserved1 (pBuf),
                                        (tVlanId)
                                        CRU_BUF_Get_U4Reserved2 (pBuf),
                                        /*vlan */
                                        (UINT1) CRU_BUF_Get_U2Reserved (pBuf),    /*pri */
                                        (UINT1) CRU_BUF_Get_U4Reserved3 (pBuf),    /*mode */
                                        u4IfIndex);

            break;

        case PPP_TO_CFA:
#ifdef PPP_WANTED

            u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

            CfaIwfPppHandleOutgoingPkt (pBuf, u4IfIndex, u4PktSize,
                                        CFA_IPV4_PID);
#else /* PPP_WANTED */
            return (CFA_FAILURE);
#endif /* PPP */

            break;

#ifdef MPLS_L3VPN_WANTED
        case MPLSL3VPN_TO_CFA:    /* Ingress/LSR */
            /* Outgoing pkt(labelled) from mplsl3vpn, so enque it to cfa */
            if (CFA_INITIALISED != TRUE)
            {
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "Error In CfaHandlePktFromMpls - "
                         "CFA is not initialised\n");
                return (CFA_FAILURE);
            }
            CFA_SET_COMMAND (pBuf, MPLSL3VPN_TO_CFA);
            CFA_SET_IFINDEX (pBuf, (UINT2) u4IfIndex);
            CFA_SET_PKT_TYPE (pBuf, u1PktType);
            if (NULL != pu1MediaAddr)
            {
                CFA_SET_DEST_MEDIA_ADDR (pBuf, pu1MediaAddr);
            }
            CFA_SET_ENCAP_TYPE (pBuf, CFA_ENCAP_ENETV2);
            CFA_SET_PROTOCOL (pBuf, CFA_ENET_IPV4);
            if (OsixQueSend (CFA_PACKET_QID, (UINT1 *) &pBuf,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                          "Error In CfaHandlePktFromMpls - "
                          "Pkt Enqueue to CFA on interface %d FAIL.\n",
                          u4IfIndex);
                return (CFA_FAILURE);
            }
            OsixEvtSend (CFA_TASK_ID, CFA_PACKET_ARRIVAL_EVENT);
            break;
#endif
        default:
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "-E- Wrong direction from MPLS");
    }

    CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
              "Exiting CfaHandlePktFromMpls -"
              "Pkt Enqueue to CFA on interface %d SUCCESS.\n", u4IfIndex);
    return (CFA_SUCCESS);

}
#endif /* MPLS_WANTED */

#ifdef ISIS_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaHandlePktFromIsIs
 *
 *    Description        : Receives data from the IsIs module and enqueues the 
 *                         message to the CFA task for Tx out over the lower 
 *                         layer.
 *
 *    Input(s)            : pBuf - the pointer to the data buffer
 *                          to be transmitted
 *                          u2IfIndex - the MIB-2 ifIndex assigned 
 *                          to the interface.
 *                          u2Protocol - type of protocol
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gFfConfigInfo, gapIfTable.
 *
 *    Global Variables Modified : gFfConfigInfo.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaHandlePktFromIsIs (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2IfIndex,
                      UINT2 u2Protocol)
{
    /* Validate the IfIndex */
    if (CfaValidateCfaIfIndex (u2IfIndex) == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error in - CfaHandlePktFromIsIs "
                 "Invalid Interface Index - FAIL\n");
        return CFA_FAILURE;
    }

    CFA_SET_IFINDEX (pBuf, u2IfIndex);
    CFA_SET_COMMAND (pBuf, ISIS_TO_CFA);
    CFA_SET_PROTOCOL (pBuf, u2Protocol);

    /* enqueue the packet to CFA task */
    if (OsixQueSend (CFA_PACKET_QID, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaHandlePktFromIsIs - "
                  "Pkt Enqueue to CFA on interface %d FAIL.\n", u2IfIndex);
        return (CFA_FAILURE);
    }

    /* send an explicit event to the task */
    if (OsixEvtSend (CFA_TASK_ID, CFA_PACKET_ARRIVAL_EVENT) != OSIX_SUCCESS)
    {
        /* dont return failure as buffer has already been enqueued */
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandlePktFromIsIs - event send to CFA FAIL.\n");
    }

    CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
              "Exiting CfaHandlePktFromIsIs -"
              "Pkt Enqueue to CFA on interface %d SUCCESS.\n", u2IfIndex);
    return (CFA_SUCCESS);
}
#endif /* ISIS_WANTED */

/*****************************************************************************
 *
 *    Function Name        : CfaHandleIpForwardingStatusUpdate
 *
 *    Description        : Provided to IP to notify any change 
 *                         in its ipForwarding MIB-2 object to CFA. 
 *                         This information is required for enabling
 *                         /disabling fast-forwarding and bridging.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gu1IpForwardingEnable.
 *
 *    Global Variables Modified : gu1IpForwardingEnable.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None
 *****************************************************************************/
VOID
CfaHandleIpForwardingStatusUpdate (UINT1 u1IpForwardingStatus)
{

/* check if the u1IpForwardingStatus is valid */
    if ((u1IpForwardingStatus == CFA_TRUE) ||
        (u1IpForwardingStatus == CFA_FALSE))
    {

        gu1IpForwardingEnable = u1IpForwardingStatus;
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "In CfaHandleIpForwardingStatusUpdate -i"
                  "updating IP Forwarding Status - %d.\n",
                  u1IpForwardingStatus);
    }
    else
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                  "Error in CfaHandleIpForwardingStatusUpdate -i"
                  "invalid status %d.\n", u1IpForwardingStatus);
    }
}

/*****************************************************************************
 *
 *    Function Name        : CfaEnablePromiscuousMode
 *
 *    Description        : This function sets the interface to promiscuous mode.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : SNMP_SUCCESS/SNMP_FAILURE
 *****************************************************************************/

INT1
CfaEnablePromiscuousMode (INT4 i4Index, INT4 i4Value)
{
    return (nmhSetIfPromiscuousMode (i4Index, i4Value));
}

#ifdef PPP_WANTED
#ifdef __STDC__
INT4
CfaTxPPPoEPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
               UINT1 *au1MediaAddr, UINT2 u2Protocol)
#else /* __STDC__ */
INT4
CfaTxPPPoEPkt (pBuf, u4IfIndex, au1MediaAddr, u2Protocol)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     UINT4               u4IfIndex;
     UINT1              *au1MediaAddr;
     UINT2               u2Protocol;
#endif /* __STDC__ */
{
    if (!CFA_IF_ENTRY_USED (u4IfIndex))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                  "Error in CfaHandlePktFromPpp - unused interface %d.\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_SET_COMMAND (pBuf, PPPOE_PKT_OUT);
    CFA_SET_IFINDEX (pBuf, u4IfIndex);
    CFA_SET_DEST_MEDIA_ADDR (pBuf, au1MediaAddr);
    CFA_SET_PROTOCOL (pBuf, u2Protocol);

    /* enqueue the packet to CFA task */
    if (OsixSendToQ (SELF, (const UINT1 *) CFA_PACKET_QUEUE, pBuf,
                     OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaTxPPPoEPkt - "
                  "Pkt Enqueue to CFA on interface %d FAIL.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    /* send an explicit event to the task */
    if (OsixSendEvent (SELF, (const UINT1 *) CFA_TASK_NAME,
                       CFA_PACKET_ARRIVAL_EVENT) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaTxPPPoEPkt - event send to CFA FAIL.\n");
        /* dont return failure as buffer has already been enqueued */
    }

    return (CFA_SUCCESS);

}
#endif /* PPP_WANTED */

 /*  Added code for configuring address and parameters obtained via DHCP */

/*****************************************************************************
 *
 *    Function Name        : CfaDynamicInterfaceIpConfig
 *
 *    Description        : This API is provided for configuration of IP address and 
 *                         related parameters for any interface when obtained
 *                         dynamically ( via DHCP / BOOTP /PPP )
 *
 *    Input(s)            : u4IfIndex - Interface index of the default 
 *                                      interface.
 *                          u4IpAddress - IP Address of the default interface.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gFfConfigInfo, gapIfTable.
 *
 *    Global Variables Modified : gFfConfigInfo, gapIfTable, gu1IpForwardingEnable.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if succeeds, otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaDynamicInterfaceIpConfig (UINT4 u4IfIndex, tIpInterfaceParams IpParams)
{
    UINT1               u1CidrMaskIndex;
    UINT1               u1IfType;

    UINT4               u4Flag = 0;
    tIpConfigInfo       IpInfo;
    UINT4               u4IfSpeed;
    UINT1               u1IfNwType;
    UINT1               u1WanType;
    UINT4               u4CidrMask;
    UINT4               u4CurMtu;
    tNetIpv4RtInfo      NetIpv4RtInfo;
    UINT1               u1CmdType;
    tStackInfoStruct   *pNode = NULL;
    UINT1               u1IpAllocMethod = 0;
#ifdef PPP_WANTED
    UINT4               u4IfIpUnnumAssocIPIf = 0;
    INT4                i4RetValPPPoEMode = 0;
    UINT1               u1PhyIfType = 0;
#endif

    /* check if the IfIndex is valid */
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                  "Error in CfaDynamicInterfaceConfig - "
                  "invalid interface %d.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    if (!CFA_IF_ENTRY_USED (u4IfIndex))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                  "Error in CfaDynamicInterfaceConfig - unused interface %d.\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

    CfaGetIfType ((UINT4) u4IfIndex, &u1IfType);
    MEMSET (&IpInfo, 0, sizeof (tIpConfigInfo));
    if (CfaIfGetIpAllocMethod (u4IfIndex, &u1IpAllocMethod) == CFA_FAILURE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "CfaDynamicInterfaceIpConfig(): "
                 "Get IP Alloc Method Failed !!!\n");
        return (CFA_FAILURE);
    }

    switch (IpParams.u4StatusFlag)
    {
        case CFA_IP_ACQUIRED:

            if (CFA_PPP == u1IfType)
            {
                /* The server does not give the subnet mask for
                 * the PPP interface. So it has to be calculated separately */

                /* If the IP address has been changed, then we assign
                 * a default subnet mask */
                if (IpParams.u4IpAddress != CFA_IF_IPADDR (u4IfIndex))
                {
                    /* If Allocation method is Manual and not dynamic,
                     * Subnet would already present along with Ip Address and
                     * need not be calculated.
                     */
                    if (u1IpAllocMethod != CFA_IP_ALLOC_MAN)
                    {
                        /* The default subnet mask is calculated according to
                         * the class of the IP address */
                        if (CFA_IS_ADDR_CLASS_A (IpParams.u4IpAddress))
                            IpParams.u4SubnetMask = 0xff000000;    /* 255.0.0.0 */
                        else if (CFA_IS_ADDR_CLASS_B (IpParams.u4IpAddress))
                            IpParams.u4SubnetMask = 0xffff0000;    /* 255.255.0.0 */
                        else if (CFA_IS_ADDR_CLASS_C (IpParams.u4IpAddress))
                            IpParams.u4SubnetMask = 0xffffff00;    /* 255.255.0.0 */
                        else
                        {
                            /* Invalid IP addr - This is not expected to happen */
                            CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                                      "Error in CfaIfmHandlePppConfigUpdate - "
                                      "invalid IP Addr - interface %lu.\n",
                                      u4IfIndex);
                            return (CFA_FAILURE);
                        }
                    }
                }
                else
                {
                    /* The same ip address is obtained from the Server.
                     * Reuse the subnet mask already stored.
                     */
                    u4CidrMask = CFA_IF_IPSUBNET (u4IfIndex);
                    IpParams.u4SubnetMask = u4CidrSubnetMask[u4CidrMask];
                }
                /* Update the MTU for the PPP/MP interface */
                CfaGetIfMtu (u4IfIndex, &u4CurMtu);
                CFA_IF_CONFIG_MTU (u4IfIndex) = u4CurMtu;
                CfaSetIfMtu (u4IfIndex, IpParams.u4Mtu);
            }
            else
            {
                if (IpParams.u4IpAddress != 0)
                {
                    /* Validate the subnet mask.
                     * Same subnet address shouldn't be allowed for multiple
                     * interfaces */
                    if (CFA_FAILURE
                        == CfaIpIfValidateIfIpSubnet (u4IfIndex,
                                                      IpParams.u4IpAddress,
                                                      IpParams.u4SubnetMask))
                    {
                        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                 "In CfaDynamicInterfaceConfig -FAIL "
                                 "same subnet address shouldn't be configured "
                                 "for multiple interfaces.\n");
                        return (CFA_FAILURE);
                    }
                }
            }

            if ((u1IfType != CFA_ENET) && (u1IfType != CFA_L3IPVLAN)
                && (u1IfType != CFA_PPP))
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                          "Error in CfaDynamicInterfaceConfig - "
                          "interface %d not ENET/L3IPVLAN/PPP\n", u4IfIndex);
                return (CFA_FAILURE);
            }

            CfaGetIfSpeed (u4IfIndex, &u4IfSpeed);
            CfaGetIfNwType (u4IfIndex, &u1IfNwType);
            CfaGetIfWanType (u4IfIndex, &u1WanType);

            u1CidrMaskIndex = CfaGetCidrSubnetMaskIndex (IpParams.u4SubnetMask);
#ifdef PPP_WANTED
            if (CFA_PPP == u1IfType)
            {
                nmhGetIfIpUnnumAssocIPIf ((INT4) u4IfIndex,
                                          &u4IfIpUnnumAssocIPIf);

                if (u4IfIpUnnumAssocIPIf != 0)
                {
                    IpInfo.u4Addr = 0;
                    IpInfo.u4NetMask = 0;
                }
                else
                {
                    IpInfo.u4Addr = IpParams.u4IpAddress;
                    IpInfo.u4NetMask = IpParams.u4SubnetMask;
                }
            }
            else
#endif
            {
                IpInfo.u4Addr = IpParams.u4IpAddress;
                IpInfo.u4NetMask = IpParams.u4SubnetMask;
            }
            u4Flag = CFA_IP_IF_PRIMARY_ADDR | CFA_IP_IF_NETMASK;

            pNode = (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
            if (pNode == NULL)
            {
                return (CFA_FAILURE);
            }

            if (CFA_IF_ENTRY_REG_WITH_IP (u4IfIndex, pNode))
            {
                if (CfaIfmConfigNetworkInterface
                    (CFA_NET_IF_UPD, u4IfIndex, u4Flag, &IpInfo) != CFA_SUCCESS)
                {
                    CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                             "CfaDynamicInterfaceIpConfig(): "
                             "IP updation failed !!!\n");
                    return (CFA_FAILURE);
                }
            }
            else
            {
                CFA_IF_IPADDR (u4IfIndex) = IpParams.u4IpAddress;
                CFA_IF_IPSUBNET (u4IfIndex) = u1CidrMaskIndex;
            }

            /* Default route needs to be added only for
             * Public WAN interfaces */

            MEMSET (&NetIpv4RtInfo, 0, sizeof (tNetIpv4RtInfo));
            NetIpv4RtInfo.u4NextHop = 0;
            NetIpv4RtInfo.u4RtIfIndx = (UINT4) CfaGetIfIpPort (u4IfIndex);
            NetIpv4RtInfo.u2RtType = CIDR_REMOTE_ROUTE_TYPE;
            NetIpv4RtInfo.u4RtNxtHopAs = 0;
            NetIpv4RtInfo.u2RtProto = CIDR_STATIC_ID;
            NetIpv4RtInfo.i4Metric1 = 1;
            NetIpv4RtInfo.u4RowStatus = ACTIVE;

            if ((u1IfNwType == CFA_NETWORK_TYPE_WAN) &&
                (u1WanType == CFA_WAN_TYPE_PUBLIC))
            {
                /* Metric calculation from speed.
                 * REFERENCE:  OSPF RFC 1850
                 */

                if (u4IfSpeed == CFA_32BIT_MAX)
                {
                    /* For all speeds greater than 100 mbps the metric is 1 */
                    NetIpv4RtInfo.i4Metric1 = 1;
                }
                else
                {
                    NetIpv4RtInfo.i4Metric1 =
                        (INT4) ((100000000 / u4IfSpeed) + 1);
                }
                /* Store the value for the deletion purpose,
                 * where the next hop address is updated with
                 * a different network */
                CFA_IF_DEF_ROUTE_METRIC (u4IfIndex) =
                    (INT2) (NetIpv4RtInfo.i4Metric1);

                /* Addition of routes */
                u1CmdType = NETIPV4_ADD_ROUTE;

#ifdef PPP_WANTED
                if ((CFA_PPP == u1IfType) && (NetIpv4RtInfo.u4DestNet == 0))
                {
                    CfaGetIfType (CFA_IF_STACK_IFINDEX
                                  (CFA_IF_STACK_LOW_ENTRY (u4IfIndex)),
                                  &u1PhyIfType);
                    if (u1PhyIfType == CFA_HDLC)
                    {
                        CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                                  "Route addition is not done for HDLC interface %lu.\n",
                                  u4IfIndex);
                        break;
                    }
                    else
                    {
                        if (PppGetPeerIpAddress
                            (u4IfIndex,
                             &NetIpv4RtInfo.u4NextHop) == CLI_FAILURE)
                        {
                            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                     "Error in Setting Default route for PPP interface\n");
                            return (CFA_FAILURE);
                        }
                    }
                }
#endif
                if (CFA_FAILURE ==
                    NetIpv4LeakRoute (u1CmdType, &(NetIpv4RtInfo)))
                {
                    CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                              "Route addition failed for interface %lu.\n",
                              u4IfIndex);
                }

                CFA_IF_NEGO_PEER_IPADDR (u4IfIndex) = NetIpv4RtInfo.u4NextHop;

            }
            break;

        case CFA_IP_RELEASED:

            CfaGetIfNwType (u4IfIndex, &u1IfNwType);
            CfaGetIfWanType (u4IfIndex, &u1WanType);

            /* Default route needs to be deleted for
             * Public WAN interfaces */

            if ((u1IfNwType == CFA_NETWORK_TYPE_WAN)
                && (u1WanType == CFA_WAN_TYPE_PUBLIC))
            {                    /* PPP interfaces become operationally down every time                                            * the session is closed.
                                 * Hence the default route would have already been deleted                                        * for PPP interfaces. */

                if ((CFA_PPP != u1IfType))
                {
                    if (CFA_FAILURE == CfaIpUpdateHostRoute (u4IfIndex,
                                                             CFA_IF_NEGO_PEER_IPADDR
                                                             (u4IfIndex),
                                                             CFA_IF_DEF_ROUTE_METRIC
                                                             (u4IfIndex),
                                                             CFA_NET_IF_DEL))
                    {
                        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                  "Error in CfaIfmNotifyInterfaceStatus "
                                  "for interface %d Default route deletion - "
                                  "Failure\n", u4IfIndex);
                    }
                }
                CFA_IF_NEGO_PEER_IPADDR (u4IfIndex) = 0;
            }
#ifdef PPP_WANTED
            if ((u1IfType == CFA_PPP))
            {
                /* MTU is also negotiated in case of PPP links.
                 * Hence reset it to the configured values */
                CfaSetIfMtu (u4IfIndex, CFA_IF_CONFIG_MTU (u4IfIndex));

                if (u1IpAllocMethod != CFA_IP_ALLOC_MAN)
                {
                    IpInfo.u4Addr = 0;
                    IpInfo.u4NetMask = 0;
                    u4Flag = CFA_IP_IF_PRIMARY_ADDR | CFA_IP_IF_NETMASK;
                }

                /* PPP Server interface should be Re-Initalized when PPP 
                 * client interface became operationally down every time*/
                PppGetPPPoEMode (&i4RetValPPPoEMode);
                if (PPPOE_SERVER == i4RetValPPPoEMode)
                {
                    CfaPppInitForIfType (u4IfIndex);
                }
            }
            else
#endif
            {
                IpInfo.u4Addr = 0;
                IpInfo.u4NetMask = 0;
                u4Flag = CFA_IP_IF_PRIMARY_ADDR | CFA_IP_IF_NETMASK;
            }
            /* Update IP with the new address and MTU */
            if (CfaIfmConfigNetworkInterface
                (CFA_NET_IF_UPD, u4IfIndex, u4Flag, &IpInfo) == CFA_FAILURE)
            {
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "CfaDynamicInterfaceIpConfig(): "
                         "IP updation failed !!!\n");
                return (CFA_FAILURE);
            }

            break;
        default:
            break;
    }

    /* 
     * When IVR is enabled, the VLAN interface type should also
     * be allowed as the default interface.
     */
    if (gu4IsIvrEnabled == CFA_DISABLED)
    {
        if (u1IfType != CFA_ENET)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                      "Error in CfaDynamicInterfaceConfig - "
                      "interface %d not Enet.\n", u4IfIndex);
            return (CFA_FAILURE);
        }
    }
    else
    {
        if ((u1IfType != CFA_ENET) && (u1IfType != CFA_L3IPVLAN)
            && (u1IfType != CFA_PPP)
#ifdef WGS_WANTED
            && (u1IfType != CFA_L2VLAN)
#endif /*WGS_WANTED */
            )

        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                      "Error in CfaDynamicInterfaceConfig - "
                      "interface %d not Enet.\n", u4IfIndex);
            return (CFA_FAILURE);
        }
    }

    /* For PPP already IP notification is done */
    if (u1IfType != CFA_PPP)
    {
        if (IpParams.u4IpAddress != 0)
        {
            /* Same subnet address shouldn't be allowed for multiple 
             * interfaces */
            if (CfaIpIfValidateIfIpSubnet (u4IfIndex, IpParams.u4IpAddress,
                                           IpParams.u4SubnetMask) ==
                CFA_FAILURE)
            {
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "In CfaDynamicInterfaceConfig -FAIL "
                         "same subnet address shouldn't be configured for"
                         " multiple interfaces.\n");
                return (CFA_FAILURE);
            }
        }

        IpInfo.u4Addr = IpParams.u4IpAddress;
        IpInfo.u4NetMask = IpParams.u4SubnetMask;
        u4Flag = CFA_IP_IF_PRIMARY_ADDR | CFA_IP_IF_NETMASK;

        if (CfaIfmConfigNetworkInterface (CFA_NET_IF_UPD,
                                          u4IfIndex, u4Flag,
                                          &IpInfo) != CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                     "In CfaProcessIpInitComplete register def"
                     "interface with IP(BOOTP) - FAIL.\n");
            return (CFA_FAILURE);
        }
    }
#ifdef SNTP_WANTED
    /* Send an event to SNTP Task. On Receiving the event
     * SNTP will trigger auto discovery.
     * */

    SntpIpInterfaceUpdateEvent ();

#endif

    return CFA_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name        : CfaGetDefaultPortParams
 *
 *    Description        : This function gets the Default Port Params.              
 *
 *    Input(s)            : None                                         
 *
 *    Output(s)            : pCfaDefPortParams - Port Parameters.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.                                            
 *****************************************************************************/

VOID
CfaGetDefaultPortParams (tCfaDefPortParams * pCfaDefPortParams)
{
    pCfaDefPortParams->u4IpAddress = IssGetIpAddrFromNvRam ();
    pCfaDefPortParams->u4SubnetMask = IssGetSubnetMaskFromNvRam ();
    IssGetIp6AddrFromNvRam (&(pCfaDefPortParams->localIp6Addr));
    pCfaDefPortParams->u1PrefixLen = IssGetIp6PrefixLenFromNvRam ();
    pCfaDefPortParams->u4ConfigMode = IssGetConfigModeFromNvRam ();
    pCfaDefPortParams->u4IpAddrAllocProto = IssGetIpAddrAllocProtoFromNvRam ();
    /* Read default device name from the nvram */
    STRNCPY ((INT1 *) &(pCfaDefPortParams->au1IntfName),
             IssGetInterfaceFromNvRam (),
             MEM_MAX_BYTES (STRLEN (IssGetInterfaceFromNvRam ()),
                            CFA_MAX_PORT_NAME_LENGTH));
    pCfaDefPortParams->au1IntfName[STRLEN (IssGetInterfaceFromNvRam ())] = '\0';

}

/*****************************************************************************/
/* Function Name      : CfaVcmSispGetSispPortsOfPhysicalPort                 */
/*                                                                           */
/* Description        : This function used to get the SISP port information  */
/*                      of a particual physical port. This API will return   */
/*                      the logical ports in IfIndex or LocalPort based on   */
/*                      the u1RetLocalPorts variable. paSispPort array will  */
/*                      be typecasted to UINT4 if the return value is to be  */
/*                      in IfIndex; will be typecasted to UINT2, if the      */
/*                      return value is to be in LocalPort.                  */
/*                                                                           */
/*                      This API can be used to get the number of SISP ports */
/*                      existing for the particular physical port. When      */
/*                      paSispPorts is NULL, SISP port count alone will be   */
/*                      returned.                                            */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex   - Physical or port channel port Id.   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
CfaVcmSispGetSispPortsOfPhysicalPort (UINT4 u4PhyIfIndex, UINT1 u1RetLocalPorts,
                                      VOID *paSispPorts, UINT4 *pu4PortCount)
{
    return (VcmSispGetSispPortsInfoOfPhysicalPort (u4PhyIfIndex,
                                                   u1RetLocalPorts,
                                                   paSispPorts, pu4PortCount));
}

#ifdef PPP_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaHandlePktFromPpp
 *
 *    Description        : Receives data from the PPP module and enqueues the
 *                         message to the CFA task.
 *                         The API handles data in both,
 *                         incoming (from lower layer to higher layer - IP) and
 *                         outgoing (for transmitting out over the lower layer). *                         The data buffer should contain only the PPP packet
 *                         if "PPP bypass" feature is enabled and otherwise
 *                         should contains the complete frame
 *                         (in case of sending the packet out on
 *                         HDLC interface, includes the 0xff03 header).
 *
 *    Input(s)            : pBuf - the pointer to the data buffer
 *                          to be transmitted
 *                          u2IfIndex - the MIB-2 ifIndex assigned
 *                          to the interface.
 *                          u1Direction - the direction of the packet transfer
 *                          u2Protocol - the protocol
 *                          (the standard PPP protocol ID)
 *                          of the PPP PDU in the buffer.
 *
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gFfConfigInfo, gapIfTable.
 *
 *    Global Variables Modified : gFfConfigInfo.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE.
 *****************************************************************************/
#ifdef __STDC__
INT4
CfaHandlePktFromPpp (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                     UINT1 u1Direction, UINT2 u2Protocol)
#else /*  __STDC__ */
INT4
CfaHandlePktFromPpp (pBuf, u4IfIndex, u1Direction, u2Protocol)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     UINT4               u4IfIndex;
     UINT1               u1Direction;
     UINT2               u2Protocol;
#endif /*  __STDC__ */
{

#ifdef PPP_BYPASS_WANTED
    UINT4               u4PktSize;
#endif /* PPP_BYPASS_WANTED */
    UINT1               u1CurType;

    /* check if the IfIndex is valid */
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                  "Error in CfaHandlePktFromPpp - invalid interface %d.\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

    CfaGetIfType (u4IfIndex, &u1CurType);

    if ((!CFA_IF_ENTRY_USED (u4IfIndex)) || ((u1CurType != CFA_PPP)))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                  "Error in CfaHandlePktFromPpp - unused interface %d.\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

    if (u1Direction == CFA_INCOMING)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "In CfaHandlePktFromPpp - recd PPP Pkt from interface %d.\n",
                  u4IfIndex);
        CFA_DUMP (CFA_TRC_IP_PKT_DUMP, pBuf);

#ifdef PPP_BYPASS_WANTED
        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

        /* handle incoming packets from PPP - to be sent to HL .i.e IP - no need to
           enqueue to CFA task */
        CfaIwfPppHandleIncomingPktFromPpp (pBuf,
                                           u4IfIndex, u4PktSize, u2Protocol);

        return (CFA_SUCCESS);
#else /* PPP_BYPASS_WANTED */
        CFA_SET_COMMAND (pBuf, PPP_TO_CFA_IN);
        CFA_SET_IFINDEX (pBuf, u4IfIndex);
        CFA_SET_PROTOCOL (pBuf, u2Protocol);
#endif /* PPP_BYPASS_WANTED */

    }                            /* end of incoming packet */
    else
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "In CfaHandlePktFromPpp - "
                  "recd PPP Pkt for Tx on interface %d.\n", u4IfIndex);
        CFA_SET_IFINDEX (pBuf, u4IfIndex);
        CFA_SET_COMMAND (pBuf, PPP_TO_CFA_OUT);
        CFA_DUMP (CFA_TRC_PPP_PKT_DUMP, pBuf);
        CFA_SET_PROTOCOL (pBuf, u2Protocol);
    }                            /* end of outgoing packet */

    /* enqueue the packet to CFA task */
    if (OsixSendToQ (SELF, (const UINT1 *) CFA_PACKET_QUEUE, pBuf,
                     OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaHandlePktFromPpp - "
                  "Pkt Enqueue to CFA on interface %d FAIL.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    /* send an explicit event to the task */
    if (OsixSendEvent (SELF, (const UINT1 *) CFA_TASK_NAME,
                       CFA_PACKET_ARRIVAL_EVENT) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandlePktFromPpp - event send to CFA FAIL.\n");
        /* dont return failure as buffer has already been enqueued */
    }

    return (CFA_SUCCESS);

}
#endif /* PPP_WANTED */

/*****************************************************************************
 *
 *    Function Name        : CfaHandlePktFromSec
 *
 *    Description        : Receives data from the Security module and enqueues 
 *                         the message to the CFA task.
 *
 *    Input(s)            : pBuf - the pointer to the data buffer
 *                          to be transmitted
 *                          u2IfIndex - the MIB-2 ifIndex assigned
 *                          to the interface.
 *                          u1Direction - the direction of the packet transfer
 *                          u2Protocol - the protocol
 *                          (the standard PPP protocol ID)
 *                          of the PPP PDU in the buffer.
 *
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gFfConfigInfo, gapIfTable.
 *
 *    Global Variables Modified : gFfConfigInfo.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaHandlePktFromSec (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    INT1                i1RetVal = OSIX_FAILURE;

    /* post the buffer to CFA queue */
    i1RetVal =
        (INT1) (OsixQueSend
                (CFA_PACKET_QID, (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN));
    if (i1RetVal != OSIX_SUCCESS)
    {
        return CFA_FAILURE;
    }

    /* trigger the interuppt event for CFA */
    OsixEvtSend (CFA_TASK_ID, CFA_PACKET_ARRIVAL_EVENT);

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaHandlePktFromSecKern
 *
 *    Description        : Receives data from the Kernel Security module and enqueues 
 *                         the message to the CFA task.
 *
 *    Input(s)            : pBuf - the pointer to the data buffer
 *                          to be transmitted
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if Queue Send succeeds,
 *                otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaHandlePktFromSecKern (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    INT1                i1RetVal = OSIX_FAILURE;

#ifdef CFA_INTERRUPT_MODE
    pBuf->ModuleData.InterfaceId.u4IfIndex = SEC_GET_IFINDEX (pBuf);
    /* post the buffer to CFA queue */
    i1RetVal =
        OsixQueSend (CFA_PACKET_MUX_QID, (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN);
    if (i1RetVal != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return CFA_FAILURE;
    }
    /* trigger the interuppt event for CFA */
    OsixEvtSend (CFA_TASK_ID, CFA_GDD_INTERRUPT_EVENT);
#else
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    UNUSED_PARAM (i1RetVal);
#endif
    return CFA_SUCCESS;
}

#ifdef OPENFLOW_WANTED
/******************************************************************************
 * Function Name : CfaHandlePktFromOfc
 * Description   : Receives data from the Openflow module and enqueues 
 *                 the message to the CFA task.
 * Input         : pBuf - the pointer to the data buffer
 *                        to be transmitted
 * Output        : None.
 * Returns       : CFA_SUCCESS if Queue Send succeeds,
 *                 otherwise CFA_FAILURE.
 ******************************************************************************/
INT4
CfaHandlePktFromOfc (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    INT1                i1RetVal = OSIX_FAILURE;

    /* post the buffer to CFA queue */
    i1RetVal = (INT1) (OsixQueSend
                       (CFA_PACKET_QID, (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN));
    if (i1RetVal != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandlePktFromOfc - "
                 "Pkt Enqueue to CFA FAIL.\n");
        return CFA_FAILURE;
    }

    /* trigger the interuppt event for CFA */
    OsixEvtSend (CFA_TASK_ID, CFA_PACKET_ARRIVAL_EVENT);
    return CFA_SUCCESS;
}
#endif /* OPENFLOW_WANTED */

#ifdef WTP_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaHandlePacketFromWss
 *
 *    Description        : Receives data from the WSS module and enqueues the 
 *                         message to the CFA task. 
 *
 *    Input(s)            : pBuf - Pointer to the CRU buffer 
 *                                 containing the packet.
 *
 *    Output(s)            : None.
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE.
 *****************************************************************************/
INT4
CfaHandlePacketFromWss (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex)
{

    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandlePacketFromWss - "
                 "CFA is not initialised.\n");
        return (CFA_FAILURE);
    }

    CFA_SET_COMMAND (pBuf, WSS_CFA_TX_DOT11_PKT);
    CFA_SET_IFINDEX (pBuf, u4IfIndex);

    if (OsixQueSend (CFA_PACKET_QID, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandlePacketFromWss - "
                 "Pkt Enqueue to CFA FAIL.\n");
        return (CFA_FAILURE);
    }

    /* send an explicit event to the task */

    if (OsixEvtSend (CFA_TASK_ID, CFA_PACKET_ARRIVAL_EVENT) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandlePacketFromWss - event send to CFA FAIL.\n");
        return (CFA_FAILURE);
    }
    return (CFA_SUCCESS);

}
#endif /* WTP_WANTED */
#ifdef VXLAN_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaHandlePktFromVxlan
 *
 *    Description        : Receives data from the VXLAN module and enqueues the
 *                         message to the CFA task.
 *
 *    Input(s)            : pBuf - the pointer to the data buffer
 *                          to be transmitted
 *                          u4IfIndex - the MIB-2 ifIndex assigned
 *                          to the interface.
 *
 *    Output(s)            : None.
 *
 *    Returns                   : CFA_SUCCESS/CFA_FAILURE.
 *****************************************************************************/

INT4
CfaHandlePktFromVxlan (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                       UINT1 u1Direction, UINT1 u1PktType)
{
    if (CFA_INITIALISED != TRUE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaHandlePktFromVxlan - "
                 "CFA is not initialised\n");
        return (CFA_FAILURE);
    }
    CFA_SET_COMMAND (pBuf, L2PKT_VXLAN_TO_CFA);
    CFA_SET_IFINDEX (pBuf, (UINT2) u4IfIndex);
    if (OsixQueSend (CFA_PACKET_QID, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                  "Error In CfaHandlePktFromVxlan - "
                  "Pkt Enqueue to CFA on interface %d FAIL.\n", u4IfIndex);
        return (CFA_FAILURE);
    }
    OsixEvtSend (CFA_TASK_ID, CFA_PACKET_ARRIVAL_EVENT);
    CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
              "Exiting CfaHandlePktFromVxlan -"
              "Pkt Enqueue to CFA on interface %d SUCCESS.\n", u4IfIndex);
    UNUSED_PARAM (u1Direction);
    UNUSED_PARAM (u1PktType);
    return (CFA_SUCCESS);
}
#endif
/*****************************************************************************/
/*  Function Name   : CfaIcchApiGetIcclIpInterface                              */
/*  Description     : This Function fetches the ICCH IP interface            */
/*  Input(s)        : None                                                   */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ICCH interface  from iccl.conf            */
/*****************************************************************************/
CHR1               *
CfaIcchApiGetIcclIpInterface (UINT2 u2Index)
{
#ifdef ICCH_WANTED
    return (IcchApiGetIcclIpInterface (u2Index));
#else
    UNUSED_PARAM (u2Index);
    return NULL;
#endif
}

/*****************************************************************************/
/*  Function Name   : CfaIcchApiGetIcclVlanId                                */
/*  Description     : This Function fetches the ICCH VlanId               */
/*  Input(s)        : None                                                   */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ICCH Vlan Id from iccl.conf               */
/*****************************************************************************/
UINT4
CfaIcchApiGetIcclVlanId (UINT2 u2Index)
{
#ifdef ICCH_WANTED
    return (IcchApiGetIcclVlanId (u2Index));
#else
    UNUSED_PARAM (u2Index);
    return 0;
#endif
}

/*****************************************************************************/
/* Function Name      : CfaIcchSetIcclIfIndex                                */
/*                                                                           */
/* Description        : This function is used to set the ICCL interface      */
/*                      index.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE or OSIX_FALSE                              */
/*                                                                           */
/*****************************************************************************/
INT1
CfaIcchSetIcclIfIndex (UINT4 u4IfIndex)
{
#ifdef ICCH_WANTED
    return (IcchSetIcclIfIndex (u4IfIndex));
#else
    UNUSED_PARAM (u4IfIndex);
    return OSIX_FALSE;
#endif
}

/*****************************************************************************/
/*  Function Name   : CfaIcchApiGetIcclIpMask                                */
/*  Description     : This Function fetches the ICCH IP Mask                 */
/*  Input(s)        : None                                                   */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ICCH IP Mask from iccl.conf               */
/*****************************************************************************/
UINT4
CfaIcchApiGetIcclIpMask (UINT2 u2Index)
{
#ifdef ICCH_WANTED
    return (IcchApiGetIcclIpMask (u2Index));
#else
    UNUSED_PARAM (u2Index);
    return CFA_INVALID_SUBNET_MASK;
#endif
}

/*****************************************************************************/
/*  Function Name   : CfaIcchApiGetIcclIpAddr                                */
/*  Description     : This Function fetches the ICCH IP Address              */
/*  Input(s)        : None                                                   */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ICCH IP Addresss from iccl.conf           */
/*****************************************************************************/
UINT4
CfaIcchApiGetIcclIpAddr (UINT2 u2Index)
{
#ifdef ICCH_WANTED
    return (IcchApiGetIcclIpAddr (u2Index));
#else
    UNUSED_PARAM (u2Index);
    return CFA_INVALID_IP_ADDR;
#endif
}

/*****************************************************************************/
/* Function Name      : CfaIcchSetIcclL3IfIndex                              */
/*                                                                           */
/* Description        : This function is used to fetch the ICCL interface    */
/*                      index.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
CfaIcchSetIcclL3IfIndex (UINT4 u4IfIndex)
{
#ifdef ICCH_WANTED
    IcchSetIcclL3IfIndex (u4IfIndex);
#else
    UNUSED_PARAM (u4IfIndex);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIcchGetIcclIfIndex                                 */
/*                                                                           */
/* Description        : This function is used to fetch the ICCL interface    */
/*                      index.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
CfaIcchGetIcclIfIndex (UINT4 *pu4IfIndex)
{
#ifdef ICCH_WANTED
    IcchGetIcclIfIndex (pu4IfIndex);
#else
    UNUSED_PARAM (pu4IfIndex);
#endif
    return;
}

/********************************************************************************
 *    Function Name      : CfaHbIcchPostHBPktToIcch
 *
 *    Description        : This function is invoked whenever ICCH HB packet is 
 *                         received in the driver. This will take care of 
 *                         posting the packet in CruBuffer format to ICCH
 *
 *    Input(s)           : pBuf - pointer to the ICCH HB Data
 *                           
 *                            
 *
 *    Output(s)          : None
 *
 *    Returns            : OSIX_SUCCESS / OSIX_FAILURE
 ********************************************************************************/

INT4
CfaHbIcchPostHBPktToIcch (tCRU_BUF_CHAIN_HEADER * pBuf)
{
#ifdef ICCH_WANTED
    return (HbIcchPostHBPktToIcch (pBuf));
#else
    UNUSED_PARAM (pBuf);
    return OSIX_FAILURE;
#endif
}

/*****************************************************************************/
/*  Function Name   : CfaIcchIsIcclVlan                                      */
/*  Description     : This function checks whether the VLAN ID is ICCL VLAN  */
/*  Input(s)        : u2VlanId - VLAN Identifier                             */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  gIcchSessionInfo                          */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  OSIX_SUCCESS or OSIX_FAILURE              */
/*****************************************************************************/
UINT4
CfaIcchIsIcclVlan (UINT2 u2VlanId)
{
#ifdef ICCH_WANTED
    return (IcchIsIcclVlan (u2VlanId));
#else
    UNUSED_PARAM (u2VlanId);
    return OSIX_FAILURE;
#endif
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : CfaVlanApiEvbGetSbpPortsOnUap                    */
/*                                                                           */
/*    Description         : This function retrieves the SBP ports present on */
/*                          the UAP.                                         */
/*                                                                           */
/*    Input(s)            : u4UapIfIndex - UAP If Index.                     */
/*                                                                           */
/*    Output(s)           : pSbpArray    - SBP ports.                        */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
CfaVlanApiEvbGetSbpPortsOnUap (UINT4 u4UapIfIndex, tVlanEvbSbpArray * pSbpArray)
{
    return (VlanApiEvbGetSbpPortsOnUap (u4UapIfIndex, pSbpArray));
}

/*****************************************************************************/
/* Function Name      : CfaLaApiIsMemberofMcLag                              */
/*                                                                           */
/* Description        : This routine checks whether the port is a member     */
/*                      of MC-LAG enabled port-channel.                      */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS or OSIX_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
CfaLaApiIsMemberofMcLag (UINT4 u4IfIndex)
{
#ifdef LA_WANTED
    return (LaApiIsMemberofMcLag (u4IfIndex));
#else
    UNUSED_PARAM (u4IfIndex);
#endif
    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CfaApiIcchGetIcclL3IfIndex                           */
/*                                                                           */
/* Description        : This routine is used to fetch ICCL IVR index         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
CfaApiIcchGetIcclL3IfIndex (UINT4 *pu4IfIndex)
{
#ifdef ICCH_WANTED
    IcchGetIcclL3IfIndex (pu4IfIndex);
#else
    UNUSED_PARAM (pu4IfIndex);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : CfaIsIcchNextIpAddress                               */
/*                                                                           */
/* Description        : This routine is used to check the IP interface is    */
/*             configured as ICCL IP                                         */
/*                                                                           */
/* Input(s)           : u4IpAddr - IP address                                */
/*                      u4NetMask - subnet mask                              */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/ OSIX_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
UINT4
CfaIsIcchNextIpAddress (UINT4 u4IpAddr, UINT4 u4NetMask)
{
#ifdef ICCH_WANTED
    return (IsIcchNextIpAddress (u4IpAddr, u4NetMask));
#else
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (u4NetMask);
    return OSIX_FAILURE;
#endif
}

/*****************************************************************************/
/* Function Name      : CfaVlanIsMemberPort                                  */
/*                                                                           */
/* Description        : This routine is used to check whether the port is    */
/*                      Member port of the given VLAN                        */
/*                                                                           */
/* Input(s)           : u4VlanId  - VLAN ID                                */
/*                      u2Port    - InterfaceIndex                              */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/ OSIX_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
CfaVlanIsMemberPort (UINT4 u4VlanId, UINT4 u4Index)
{
    if (VlanCheckMemberPort (0, u4Index, u4VlanId) == VLAN_SUCCESS)
    {
        return CFA_SUCCESS;
    }
    else
    {
        return CFA_FAILURE;
    }
}
