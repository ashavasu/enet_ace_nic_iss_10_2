/*****************************************************************************/
/* Copyright (C) 2011 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2011                                          */
/*                                                                           */
/* $Id: gddksec.c,v 1.30 2018/01/08 12:28:33 siva Exp $                       */
/*                                                                           */
/*  FILE NAME             : gddksec.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : Packet transmission and reception in the kernel  */
/*  MODULE NAME           : Security Module                                  */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 16 Dec 2010                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains handlers for packet           */
/*  transmission and reception                                               */
/*                          in the kernel                                    */
/*****************************************************************************/
#ifndef _GDDKSEC_C_
#define _GDDKSEC_C_

#include "l2iwf.h"
#include "iss.h"
#include "cfa.h"
#include "arsec.h"
#include "secmod.h"
#include "secgen.h"
#include "seckgen.h"
#include "seckextn.h"
#include "gddksec.h"
#include "sectrc.h"
#include "secdec.h"
#include <linux/netfilter.h>    /*For nf_hook_ops structure */
#include <linux/netfilter_ipv4.h>

#if defined(BCMX_WANTED)
#include "gddksbcm.h"
#elif defined(XCAT)
#include "gddksmrl.h"
#else
#elif defined(XCAT3)
#include "gddksmrl.h"
#else
#include "gddkslnx.h"
#endif

#define CFA_ARCH_FSIP 1
#define CFA_ARCH_LINUXIP 2
#define CFA_ETH_HDR_LEN 14

/* The following global variable is used to differentiate the process
 * of receiving the packet in kernel in Linux Host architecture 
 * (LINUXIP-LINUX kernel) from the traditional implementation for 
 * FSIP-LINUX Kernel  architecture */

UINT1               gu1PktRecv = CFA_ARCH_FSIP;

/* This is the structure used to register our nethook function */
static struct nf_hook_ops pre_nfho;
static struct nf_hook_ops post_nfho;

/*****************************************************************************
 *
 *    Function Name        : pre_hook_func 
 *
 *    Description          : This is the pre-routing hook function which has 
 *                           been registered  with the netfilter pre-routing
 *                           hook. This function will be called when a packet
 *                           with destination MAC address destined to the
 *                           interface of the machine. 
 *
 *    Input(s)            : hooknum - Hook Number 
 *                          pSkb    - Buffer containing the packet.
 *                          in      - Dev through which the packet came in.
 *                          out     - Dev through which the packet goes out.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : NF_ACCEPT/NF_STOLEN
 *
 *****************************************************************************/

/* hook function*/
unsigned int
pre_hook_func (unsigned int hooknum,
               struct sk_buff *pSkb,
               const struct net_device *in,
               const struct net_device *out, int (*okfn) (struct sk_buff *))
{

    UINT4               u4DestAddr = 0;
    UINT4               u4SrcAddr = 0;
    struct sk_buff     *sb = pSkb;
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
    tSkb               *new_skb = NULL;
    struct vlan_ethhdr *vlan = NULL;
    UINT1               u1Direction = 0;
    UINT4               u4IfIndex = 0;
    UINT1               u1MsgType = 0;
    UINT2               u2Proto = 0;
    UINT2               u2OspfProto = 0;
    UINT4               u4IpHdrOffset = 0;
    UINT4               u4SrcIp = 0;
    UINT4               u4DstIp = 0;
    UINT4               u4PktSize = 0;
    UINT2               u2Protocol = 0;
    INT4                i4headroom = 0;
    INT4                i4IfLen = 0;
    UINT1               au1DstMac[6];
    tSecIfIpAddrInfo   *pSecIfIpAddrNode = NULL;
    struct in_device   *out_dev = NULL;
    struct in_ifaddr   *if_info = NULL;
    struct ethhdr      *eh;
    UINT4               u4Index = 0;
    UINT4               u4WanInterfaceIp = 0;
    UINT1               u1WanInterfaceFound = 0;

    if (gu1PktRecv == CFA_ARCH_FSIP)
    {

        UNUSED_PARAM (hooknum);
        UNUSED_PARAM (out);
        UNUSED_PARAM (in);
        UNUSED_PARAM (okfn);

        u4SrcAddr = ip_hdr (pSkb)->saddr;
        u4DestAddr = ip_hdr (pSkb)->daddr;

        u4SrcAddr = OSIX_NTOHL (u4SrcAddr);
        u4DestAddr = OSIX_NTOHL (u4DestAddr);

        /* Allow only packets with 127.0.0.1 src/dest IP.
         * These packets are used for snort and ISS IPC 
         * communication */

        if ((u4SrcAddr == INADDR_LOOPBACK) && (u4DestAddr == INADDR_LOOPBACK))
        {
            return NF_ACCEPT;
        }
        return NF_DROP;
    }
    else
    {

        for (u4Index = 0; u4Index < SYS_MAX_WAN_INTERFACES; u4Index++)
        {
            i4IfLen = strlen (gaSecWanIfInfo[u4Index].au1InfName);

            if (i4IfLen < strlen (in->name))
            {
                i4IfLen = strlen (in->name);
            }

            if (memcmp (in->name, gaSecWanIfInfo[u4Index].au1InfName, i4IfLen)
                == 0)
            {
                u1WanInterfaceFound = 1;
                break;
            }

        }

        if (u1WanInterfaceFound)
        {
            u4WanInterfaceIp = gaSecWanIfInfo[u4Index].u4IpAddr;

            eh = (struct ethhdr *) pSkb->data;
            u2Proto = OSIX_NTOHS (eh->h_proto);

            /* Untagged packets starting with IP header || Tagged packets starting with
             * the VLAN TPID/TCID info */
            if (((*(pSkb->data)) == CFA_IP_VER_IHL_MASK)
                || (u2Proto == CFA_ENET_IPV4) || (u2Proto == CFA_PPPOE_SESSION)
                || (u2Proto == CFA_ENET_ARP))
            {
                /* Get the IP Header offset. If untagged offset will be 0.
                 * If tagged packet offset will be 4 (VLAN-ID + Protocol) */

                u4IpHdrOffset =
                    ((*(pSkb->data)) ==
                     CFA_IP_VER_IHL_MASK) ? 0 : sizeof (UINT4);
                u4SrcIp =
                    *((UINT4 *) (pSkb->data + u4IpHdrOffset + IP_PKT_OFF_SRC));
                u4DstIp =
                    *((UINT4 *) (pSkb->data + u4IpHdrOffset + IP_PKT_OFF_DEST));

                u4SrcIp = OSIX_NTOHL (u4SrcIp);
                u4DstIp = OSIX_NTOHL (u4DstIp);

                /* Do not strip packets with src/dest IP 127.0.0.1 (snort and ISS IPC) */
                if ((u4SrcIp != INADDR_LOOPBACK)
                    && (u4DstIp != INADDR_LOOPBACK))
                {
                    SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
                             "This is a Valid Packet Strip The Header Before Processing !!!\r\n");
                    pSkb->data -= CFA_ENET_V2_HEADER_SIZE;
                    pSkb->len += CFA_ENET_V2_HEADER_SIZE;
                }
                else
                {

                    SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
                             "Drop packets with source and destination"
                             "as LoopBack address !!!\r\n");

                    return NF_ACCEPT;
                }
            }

            else if (((*(pSkb->data)) == CFA_IPV6_VER_PRIORITY_MASK)
                     || (u2Proto == CFA_ENET_IPV6))
            {
                pSkb->data -= CFA_ENET_V2_HEADER_SIZE;
                pSkb->len += CFA_ENET_V2_HEADER_SIZE;
            }

            if ((pCruBuf = CRU_BUF_Allocate_ChainDesc ()) == NULL)
            {
                SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
                         "Buffer allocation failed !!!\r\n");

                return NF_ACCEPT;
            }

            pCruBuf->pSkb = pSkb;

            /* Through WAN interface IP , we are trying to fetch the ifIndex of the
             * WAN interface, so that we can update the ifIndex in the CRU Buf */

            pSecIfIpAddrNode =
                SecUtilGetIfIpAddrEntry (u4WanInterfaceIp, SEC_UCAST_IP);

            if (pSecIfIpAddrNode == NULL)
            {
                skb_set_mac_header (pCruBuf->pSkb, 0);
                skb_set_network_header (pCruBuf->pSkb, CFA_ETH_HDR_LEN);
                skb_pull (pCruBuf->pSkb, CFA_ETH_HDR_LEN);
                pCruBuf->pSkb = NULL;
                CRU_BUF_Release_ChainDesc (pCruBuf);
                return NF_ACCEPT;
            }

            /* Packet from WAN */
            u1Direction = SEC_INBOUND;
            u4IfIndex = pSecIfIpAddrNode->i4IfIndex;

            if (u4IfIndex == 0)
            {
                skb_set_mac_header (pCruBuf->pSkb, 0);
                skb_set_network_header (pCruBuf->pSkb, CFA_ETH_HDR_LEN);
                skb_pull (pCruBuf->pSkb, CFA_ETH_HDR_LEN);
                pCruBuf->pSkb = NULL;
                CRU_BUF_Release_ChainDesc (pCruBuf);

                SEC_TRC_ARG1 (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
                              "Not a IfIndex %d!!!\r\n", u4IfIndex);
                return NF_ACCEPT;
            }

            u1MsgType = SEC_MSG_TYPE_ETH_FRAME;

            SEC_SET_MSGTYPE (pCruBuf, u1MsgType);
            SEC_SET_IFINDEX (pCruBuf, u4IfIndex);
            SEC_SET_DIRECTION (pCruBuf, u1Direction);

            if (CfaIwfEnetProcessRxFrame
                (pCruBuf, u4IfIndex, u4PktSize, u2Protocol,
                 u1Direction) == CFA_SUCCESS)
            {
                skb_set_mac_header (pCruBuf->pSkb, 0);
                skb_set_network_header (pCruBuf->pSkb, CFA_ETH_HDR_LEN);
                skb_pull (pCruBuf->pSkb, CFA_ETH_HDR_LEN);
                pCruBuf->pSkb = NULL;
                CRU_BUF_Release_ChainDesc (pCruBuf);
                return NF_ACCEPT;

            }

            SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
                     "Exiting Function CfaProcessSkb !!!\r\n");
            return NF_STOLEN;
        }

        return NF_ACCEPT;
    }

}

/*****************************************************************************
 *
 *    Function Name        : post_hook_func 
 *
 *    Description          : This is the post-routing hook function which has 
 *                           been registered  with the netfilter post-routing
 *                           hook. This function will be called when a packet
 *                           goes out via a particular interface.
 *
 *    Input(s)            : hooknum - Hook Number 
 *                          pSkb    - Buffer containing the packet.
 *                          in      - Dev through which the packet came in.
 *                          out     - Dev through which the packet goes out.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : NF_ACCEPT/NF_STOLEN
 *
 *****************************************************************************/

UINT4
post_hook_func (UINT4 u4hooknum,
                struct sk_buff *pSkb,
                const struct net_device *in,
                const struct net_device *out, INT4 (*okfn) (struct sk_buff *))
{

    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
    tSkb               *new_skb = NULL;
    struct vlan_ethhdr *vlan = NULL;
    UINT1               u1Direction = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Index = 0;
    UINT1               u1MsgType = 0;
    UINT2               u2Proto = 0;
    UINT2               u2OspfProto = 0;
    UINT4               u4IpHdrOffset = 0;
    UINT4               u4SrcIp = 0;
    UINT4               u4DstIp = 0;
    UINT4               u4PktSize = 0;
    UINT2               u2Protocol = 0;
    UINT1               u1Flag = 0;
    INT4                i4headroom = 0;
    tSecIfIpAddrInfo   *pSecIfIpAddrNode = NULL;
    struct in_device   *out_dev = NULL;
    struct in_ifaddr   *if_info = NULL;
    struct ethhdr      *eh;
    struct iphdr       *ipfh = NULL;
    UINT4               u4WanInterfaceIp = 0;
    UINT1               au1DstMac[6];
    UINT1               u1WanInterfaceFound = 0;
    INT4                i4IfLen = 0;

    for (u4Index = 0; u4Index < SYS_MAX_WAN_INTERFACES; u4Index++)
    {
        i4IfLen = strlen (gaSecWanIfInfo[u4Index].au1InfName);

        if (i4IfLen < strlen (out->name))
        {
            i4IfLen = strlen (out->name);
        }

        if (memcmp (out->name, gaSecWanIfInfo[u4Index].au1InfName, i4IfLen) ==
            0)
        {
            u1WanInterfaceFound = 1;
            break;
        }

    }

    if (u1WanInterfaceFound)
    {
        u4WanInterfaceIp = gaSecWanIfInfo[u4Index].u4IpAddr;

        ipfh = (struct iphdr *) ip_hdr (pSkb);
        u4SrcIp = OSIX_NTOHL (ipfh->saddr);

        /* If the packet is originated from the ISS, then no need to give it to
         * NAT for translation */

        if (u4SrcIp == u4WanInterfaceIp)
        {
            return NF_ACCEPT;
        }

        eh = (struct ethhdr *) eth_hdr (pSkb);
        u2Proto = OSIX_NTOHS (eh->h_proto);

        /* Untagged packets starting with IP header || Tagged packets starting with
         * the VLAN TPID/TCID info */
        if (((*(pSkb->data)) == CFA_IP_VER_IHL_MASK)
            || (u2Proto == CFA_ENET_IPV4) || (u2Proto == CFA_PPPOE_SESSION)
            || (u2Proto == CFA_ENET_ARP))
        {
            /* Get the IP Header offset. If untagged offset will be 0.
             * If tagged packet offset will be 4 (VLAN-ID + Protocol) */

            u4IpHdrOffset =
                ((*(pSkb->data)) == CFA_IP_VER_IHL_MASK) ? 0 : sizeof (UINT4);
            u4SrcIp =
                *((UINT4 *) (pSkb->data + u4IpHdrOffset + IP_PKT_OFF_SRC));
            u4DstIp =
                *((UINT4 *) (pSkb->data + u4IpHdrOffset + IP_PKT_OFF_DEST));

            u4SrcIp = OSIX_NTOHL (u4SrcIp);
            u4DstIp = OSIX_NTOHL (u4DstIp);

            /* Do not strip packets with src/dest IP 127.0.0.1 (snort and ISS IPC) */
            if ((u4SrcIp != INADDR_LOOPBACK) && (u4DstIp != INADDR_LOOPBACK))
            {
                SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
                         "This is a Valid Packet Strip The Header Before Processing !!!\r\n");
                pSkb->data -= CFA_ENET_V2_HEADER_SIZE;
                pSkb->len += CFA_ENET_V2_HEADER_SIZE;
            }
            else
            {
                SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
                         "Drop packets with source and destination as LoopBack address !!!\r\n");
                return NF_ACCEPT;
            }
        }

        else if (((*(pSkb->data)) == CFA_IPV6_VER_PRIORITY_MASK)
                 || (u2Proto == CFA_ENET_IPV6))
        {
            pSkb->data -= CFA_ENET_V2_HEADER_SIZE;
            pSkb->len += CFA_ENET_V2_HEADER_SIZE;
        }

        if ((pCruBuf = CRU_BUF_Allocate_ChainDesc ()) == NULL)
        {
            SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
                     "Buffer allocation failed !!!\r\n");

            return NF_ACCEPT;

        }
        pCruBuf->pSkb = pSkb;

        /* Through WAN interface IP , we are trying to fetch the ifIndex of the
         * WAN interface, so that we can update the ifIndex in the CRU Buf */

        pSecIfIpAddrNode =
            SecUtilGetIfIpAddrEntry (u4WanInterfaceIp, SEC_UCAST_IP);

        if (pSecIfIpAddrNode == NULL)
        {
            skb_set_mac_header (pCruBuf->pSkb, 0);
            skb_set_network_header (pCruBuf->pSkb, CFA_ETH_HDR_LEN);
            skb_pull (pCruBuf->pSkb, CFA_ETH_HDR_LEN);
            pCruBuf->pSkb = NULL;
            CRU_BUF_Release_ChainDesc (pCruBuf);
            return NF_ACCEPT;
        }

        /* Packet to WAN */
        u1Direction = SEC_OUTBOUND;
        u4IfIndex = pSecIfIpAddrNode->i4IfIndex;

        if (u4IfIndex == 0)
        {
            skb_set_mac_header (pCruBuf->pSkb, 0);
            skb_set_network_header (pCruBuf->pSkb, CFA_ETH_HDR_LEN);
            skb_pull (pCruBuf->pSkb, CFA_ETH_HDR_LEN);
            pCruBuf->pSkb = NULL;
            CRU_BUF_Release_ChainDesc (pCruBuf);

            SEC_TRC_ARG1 (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
                          "Not a valid IfIndex %d!!!\r\n", u4IfIndex);
            return NF_ACCEPT;
        }

        u1MsgType = SEC_MSG_TYPE_ETH_FRAME;

        SEC_SET_MSGTYPE (pCruBuf, u1MsgType);
        SEC_SET_IFINDEX (pCruBuf, u4IfIndex);
        SEC_SET_DIRECTION (pCruBuf, u1Direction);

        if (CfaIwfEnetProcessRxFrame (pCruBuf, u4IfIndex, u4PktSize, u2Protocol,
                                      u1Direction) == CFA_SUCCESS)
        {
            skb_set_mac_header (pCruBuf->pSkb, 0);
            skb_set_network_header (pCruBuf->pSkb, CFA_ETH_HDR_LEN);
            skb_pull (pCruBuf->pSkb, CFA_ETH_HDR_LEN);
            pCruBuf->pSkb = NULL;
            CRU_BUF_Release_ChainDesc (pCruBuf);
            return NF_ACCEPT;
        }

        SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
                 "Exiting Function CfaProcessSkb !!!\r\n");

        return NF_STOLEN;
    }

    return NF_ACCEPT;

}

tNetDevice         *pDefXmitDev = NULL;
tNetDevice         *apDefXmitDev[2];
static tPktType     IpFrameType = {
    .type = __constant_htons (ETH_P_ALL),
    .func = CfaProcessSkb,
};

/*****************************************************************************
 *
 *    Function Name        : CfaProcessSkb
 *
 *    Description          : This is the callback function which was registered 
 *                           with device add pack. This function will be called 
 *                           when the IP packet is received in the kernel.
 *
 *    Input(s)            : pSkb - The packet
 *                          pNetDev - The Device on which the packet is received
 *                          pPktType - The Packet Type
 *                          pOrgDev - The original netdevice
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : NET_RX_DROP/NET_RX_SUCCESS
 *
 *****************************************************************************/

INT4
CfaProcessSkb (tSkb * pSkb, tNetDevice * pNetDev,
               tPktType * pPktType, tNetDevice * pOrgDev)
{
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
    UINT1               u1Direction = 0;
    UINT4               u4IfIndex = 0;
    UINT1               u1MsgType = 0;
    UINT2               u2Proto = 0;
    UINT2               u2OspfProto = 0;
    UINT4               u4IpHdrOffset = 0;
    UINT4               u4SrcIp = 0;
    UINT4               u4DstIp = 0;
    UINT4               u4PktSize = 0;
    UINT2               u2Protocol = 0;

    UNUSED_PARAM (pNetDev);
    UNUSED_PARAM (pPktType);
    UNUSED_PARAM (pOrgDev);
    SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
             "Entering Function CfaProcessSkb !!!\r\n");

    if (gu1PktRecv == CFA_ARCH_FSIP)
    {

        /* For the outgoing packet security is not applied hence returning
           as soon as packet is received from dev_add_pack */

        if (pSkb->pkt_type == PACKET_OUTGOING)
        {
            SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
                     "Packet Type is PACKET_OUTGOING return NET_RX_SUCCESS !!!\r\n");
            kfree_skb (pSkb);
            return NET_RX_SUCCESS;
        }

        /* Move the offset based on target specific packet format */
        if (GddKSecMoveOffSet (pSkb) == CFA_FAILURE)
        {
            SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
                     "GddKSecMoveOffSet failed !!!\r\n");
            kfree_skb (pSkb);
            return NET_RX_DROP;
        }

        /* Get Protocol */
        u2Proto = OSIX_NTOHS (*(UINT2 *) (pSkb->data + sizeof (UINT2)));
        u2OspfProto = OSIX_NTOHS (*(UINT1 *) (pSkb->data + sizeof (UINT2) +
                                              (OSPF_PROTO_BYTE *
                                               sizeof (UINT1))));

        /* Drop all ARP packets and OSPF packets */
        if ((u2Proto == CFA_ENET_ARP) || (u2OspfProto == CFA_ENET_OSPF))
        {
            SEC_TRC_ARG1 (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
                          "Incoming packet is a ARP or multicast packet %d !!!\r\n",
                          u2Proto);
            kfree_skb (pSkb);
            return NET_RX_DROP;
        }

        /* Untagged packets starting with IP header || Tagged packets starting with
         * the VLAN TPID/TCID info */
        if (((*(pSkb->data)) == CFA_IP_VER_IHL_MASK)
            || (u2Proto == CFA_ENET_IPV4) || (u2Proto == CFA_PPPOE_SESSION)
            || (u2Proto == CFA_ENET_ARP))
        {
            /* Get the IP Header offset. If untagged offset will be 0.
             * If tagged packet offset will be 4 (VLAN-ID + Protocol) */

            u4IpHdrOffset =
                ((*(pSkb->data)) == CFA_IP_VER_IHL_MASK) ? 0 : sizeof (UINT4);
            u4SrcIp =
                *((UINT4 *) (pSkb->data + u4IpHdrOffset + IP_PKT_OFF_SRC));
            u4DstIp =
                *((UINT4 *) (pSkb->data + u4IpHdrOffset + IP_PKT_OFF_DEST));

            u4SrcIp = OSIX_NTOHL (u4SrcIp);
            u4DstIp = OSIX_NTOHL (u4DstIp);

            /* Do not strip packets with src/dest IP 127.0.0.1 (snort and ISS IPC) */
            if ((u4SrcIp != INADDR_LOOPBACK) && (u4DstIp != INADDR_LOOPBACK))
            {
                SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
                         "This is a Valid Packet Strip The Header Before Processing !!!\r\n");
                pSkb->data -= CFA_ENET_V2_HEADER_SIZE;
                pSkb->len += CFA_ENET_V2_HEADER_SIZE;
            }
            else
            {
                SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
                         "Drop packets with source and destination as LoopBack address !!!\r\n");
                kfree_skb (pSkb);
                return NET_RX_DROP;
            }
        }
        else if (((*(pSkb->data)) == CFA_IPV6_VER_PRIORITY_MASK)
                 || (u2Proto == CFA_ENET_IPV6))
        {
            pSkb->data -= CFA_ENET_V2_HEADER_SIZE;
            pSkb->len += CFA_ENET_V2_HEADER_SIZE;
        }

        if ((pCruBuf = CRU_BUF_Allocate_ChainDesc ()) == NULL)
        {
            SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
                     "Buffer allocation failed !!!\r\n");
            kfree_skb (pSkb);
            return NET_RX_DROP;
        }
        pCruBuf->pSkb = pSkb;

        /* Memory release for failure returned by this function call
         * should be handled in the GddKSecGetIntAndDir function itself and
         * not in this calling place.
         */
        if (GddKSecGetIntAndDir (pCruBuf, &u1Direction, &u4IfIndex, pNetDev) ==
            CFA_FAILURE)
        {
            SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
                     "Unable  to get proper direction and ifindex !!!\r\n");
            return NET_RX_DROP;
        }

        if ((u1Direction == 0) || (u4IfIndex == 0))
        {
            CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
            SEC_TRC_ARG2 (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
                          "Not a valid direction %d or IfIndex %d!!!\r\n",
                          u1Direction, u4IfIndex);
            return NET_RX_DROP;
        }
        u1MsgType = SEC_MSG_TYPE_ETH_FRAME;

        SEC_SET_MSGTYPE (pCruBuf, u1MsgType);
        SEC_SET_IFINDEX (pCruBuf, u4IfIndex);
        SEC_SET_DIRECTION (pCruBuf, u1Direction);

        CfaIwfEnetProcessRxFrame (pCruBuf, u4IfIndex, u4PktSize, u2Protocol,
                                  u1Direction);

        SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaProcessSkb:"
                 "Exiting Function CfaProcessSkb !!!\r\n");
        return NET_RX_DROP;

    }
    else
    {
        /* In Linux IP architecture, NAT translation would be handled in 
         * PRE-ROUTING & POST-ROUTING HOOK.*/

        dev_kfree_skb_any (pSkb);
        return NET_RX_SUCCESS;

    }

}

/*****************************************************************************
 *
 *    Function Name        : CfaGddInit
 *
 *    Description        : This function registers the callback function with 
 *                         device add pack
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : CFA_SUCCESS if initialisation succeeds,
 *                             otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddInit (VOID)
{

    /* Fill in hook structure */
    pre_nfho.hook = pre_hook_func;    /* Handler function */
    pre_nfho.hooknum = NF_INET_PRE_ROUTING;    /* First hook for IPv4 */
    pre_nfho.pf = PF_INET;
    pre_nfho.priority = NF_IP_PRI_FIRST;    /* Make our function first */

    /*In case of Marvell platform the function is locally defined in gddksmrl.c since nf_register_hook is not defined in the Linux kernel */

    nf_register_hook (&pre_nfho);

    if (gu1PktRecv == CFA_ARCH_FSIP)
    {

#if defined (LINUX_KERNEL_2_6_28VER) || defined (LINUX_KERNEL_2_6_32VER) \
    || defined (LINUX_KERNEL_2_6_25VER) || defined (LINUX_KERNEL_2_6_27VER) \
    || defined (LINUX_KERNEL_3_3_4VER)
        pDefXmitDev = dev_get_by_name (&init_net, SEC_DEF_XMIT_DEV);
        apDefXmitDev[0] = dev_get_by_name (&init_net, SEC_DEF_BOND_DEV_0);
        apDefXmitDev[1] = dev_get_by_name (&init_net, SEC_DEF_BOND_DEV_1);
#else
        pDefXmitDev = dev_get_by_name ((const CHR1 *) SEC_DEF_XMIT_DEV);
        apDefXmitDev[0] = dev_get_by_name ((const CHR1 *) SEC_DEF_BOND_DEV_0);
        apDefXmitDev[1] = dev_get_by_name ((const CHR1 *) SEC_DEF_BOND_DEV_1);
#endif

        if ((pDefXmitDev == NULL) ||
            (apDefXmitDev[0] == NULL) || (apDefXmitDev[1] == NULL))
        {
            SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaGddInit:"
                     " Failed to get the device Name !!!\r\n");
            return CFA_FAILURE;
        }

        dev_add_pack (&IpFrameType);
    }
    else
    {

        /* Reistering the Post Routing Hook */
        /* Fill in hook structure */
        post_nfho.hook = post_hook_func;    /* Handler function */
        post_nfho.hooknum = NF_INET_POST_ROUTING;    /* First hook for IPv4 */
        post_nfho.pf = PF_INET;
        post_nfho.priority = NF_IP_PRI_FIRST;    /* Make our function first */

        nf_register_hook (&post_nfho);
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddDeInit
 *
 *    Description        : This function deregisters the callback function with 
 *                         device add pack
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                             otherwise CFA_FAILURE.
 *
 *****************************************************************************/

VOID
CfaGddDeInit (VOID)
{
    if (gu1PktRecv == CFA_ARCH_FSIP)
    {
        dev_remove_pack (&IpFrameType);
    }
    else
    {

        /*Un-Registering the Pre-Routing Hook */
        nf_unregister_hook (&pre_nfho);
        /*Un-Registering the Post-Routing Hook */
        nf_unregister_hook (&post_nfho);

    }

    return;
}

/*****************************************************************************
 *
 *    Function Name        : CfaIwfEnetProcessRxFrame
 *
 *    Description          : This function calls the SecProcessFrame to apply the 
 *                           security. After applying security the packet will be 
 *                           posted to CFA if the packet is destined to itself 
 *                           otherwise the packet will be transmitted using dev 
 *                           queue transmit.                         
 *
 *    Input(s)            : pCruBuf - The packet
 *                          u4IfIndex - The interface index on which the packet is received
 *                          u4PktSize - Size of the packet
 *                          u2Protocol - Protocol
 *                          u1Direction - Direction of packet Inbound/Outbound
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS / CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIwfEnetProcessRxFrame (tCRU_BUF_CHAIN_HEADER * pCruBuf, UINT4 u4IfIndex,
                          UINT4 u4PktSize, UINT2 u2Protocol, UINT1 u1Direction)
{
    tSecIntfInfo        SecIntfInfo;
    UINT4               u4PppIndex = 0;
    INT4                i4RetVal = OSIX_SUCCESS;
    BOOL1               bSecVerdict = SEC_NOT_STOLEN;

    UNUSED_PARAM (u4PktSize);
    UNUSED_PARAM (u2Protocol);

    MEMSET (&SecIntfInfo, 0, sizeof (tSecIntfInfo));
    SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaIwfEnetProcessRxFrame:"
             "Entering Function CfaIwfEnetProcessRxFrame !!!\r\n");

    if (SEC_OUTBOUND == u1Direction)
    {
        /* Add PPP header to the packet, if the packet is SEC_OUTBOUND and 
         * Index is stacked with PPP Index.
         */
        if (OSIX_SUCCESS ==
            SecUtilGetPppIdxFromPhyIfdx (u4IfIndex, &u4PppIndex))
        {
            SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaIwfEnetProcessRxFrame:"
                     " Add PPP header to the packet  !!!\r\n");
            SecUtilAddPppHdrToPkt (pCruBuf, u4IfIndex);
        }
    }

    SecIntfInfo.u4PhyIfIndex = u4IfIndex;
    i4RetVal =
        SecProcessFrame (pCruBuf, &SecIntfInfo, u1Direction, &bSecVerdict);

    if ((OSIX_FAILURE == i4RetVal) ||
        ((OSIX_SUCCESS == i4RetVal) && (SEC_STOLEN != bSecVerdict)))
    {
        if (i4RetVal == OSIX_FAILURE)
        {
            SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaIwfEnetProcessRxFrame:"
                     " Failed to apply the security !!!\r\n");
            i4RetVal = CFA_FAILURE;
        }
        else
        {
            SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaIwfEnetProcessRxFrame:"
                     " security processing completed !!!\r\n");
            i4RetVal = CFA_SUCCESS;
        }
        if (gu1PktRecv != CFA_ARCH_FSIP)
        {
            pCruBuf->pSkb = NULL;
        }

        CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
    }
    SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaIwfEnetProcessRxFrame:"
             "Exiting Function CfaIwfEnetProcessRxFrame !!!\r\n");
    return i4RetVal;
}

/*****************************************************************************
 *
 *    Function Name     : GddKSecGetIntAndDir
 *
 *    Description       : This function is used to get the interface and 
 *                        direction using the vlan Id of the packet.
 *
 *    Input(s)          : pBuf - The packet buffer
 *
 *    Output(s)         : pu1Direction - The direction of the packet.
 *                        u4IfIndex - Interface index on which the packet is 
 *                           received.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS / CFA_FAILURE.
 *
 *****************************************************************************/
INT4
GddKSecGetIntAndDir (tCRU_BUF_CHAIN_HEADER * pBuf,
                     UINT1 *pu1Direction, UINT4 *pu4IfIndex,
                     tNetDevice * pNetDev)
{
    INT4                u4SecIvrIndex = 0;
    UINT2               u2TPID = ISS_ZERO_ENTRY;    /*Tag Protocol Identifier */
    UINT2               u2TCI = ISS_ZERO_ENTRY;    /*Tag Control Identifier */
    UINT2               u2VlanId = ISS_ZERO_ENTRY;
    UINT1               u1NwType = CFA_NETWORK_TYPE_LAN;

    SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecGetIntAndDir:"
             "Entering Function GddKSecGetIntAndDir !!!\r\n");

    /* Get direction based on the packet */
    if (CFA_FAILURE == GddKSecPortGetDirection (pBuf, pu1Direction, pNetDev))
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecGetIntAndDir:"
                 "Failed to get the direction !!!\r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return CFA_FAILURE;
    }
    /* Extract the Tag Protocol Identifier from the frame */
    u2TPID =
        OSIX_NTOHS (*
                    ((UINT2 *) ((VOID *) (pBuf->pSkb->data) +
                                CFA_VLAN_TAG_OFFSET)));

    if (u2TPID == CFA_VLAN_PROTOCOL_ID)
    {
        /* Vlan tag is present, so extract the control identifier in Vlan tag */

        u2TCI = OSIX_NTOHS (*((UINT2 *) ((VOID *) (pBuf->pSkb->data) +
                                         (CFA_VLAN_TAG_OFFSET +
                                          sizeof (UINT2)))));
        /* Tag Control Identifier u2TCI contains 3bits priority, 1 canonical form
         * bit and 12 bits Vlan ID */

        /* Extracting the canonical bit */

        /* As canonical bit is unused in most of the ethernet frames, the
         * underlying hardware uses this bit to convey the direction information
         * to the CPU */

        /* Extracting the Vlan ID */
        u2VlanId = u2TCI & CFA_VLAN_VID_MASK;

        if (SecUtilGetIfIndexFromVlanId (u2VlanId, pu4IfIndex) == OSIX_SUCCESS)
        {
            if (SecUtilGetIfNwType (*pu4IfIndex, &u1NwType) == OSIX_SUCCESS)
            {
                /* Security processing is done only if packet from or to WAN 
                   interface */
                if (u1NwType == CFA_NETWORK_TYPE_WAN)
                {
                    /* Multicast packets that are duplicated to kernel are dropped */

                    if ((CFA_IS_ENET_MAC_MCAST (pBuf->pSkb->data) == CFA_TRUE)
                        || (CFA_IS_ENET_MAC_BCAST (pBuf->pSkb->data) ==
                            CFA_TRUE)
                        || (CFA_IS_ENET_IPV6_MAC_MCAST (pBuf->pSkb->data) ==
                            CFA_TRUE))
                    {
                        SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecGetIntAndDir:"
                                 "Multicast packets that are duplicated to kernel are dropped !!!\r\n");
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        return CFA_FAILURE;
                    }
                    SEC_TRC_ARG1 (SEC_CONTROL_PLANE_TRC, "GddKSecGetIntAndDir:"
                                  "Successfully Identified Network Type As WAN for the interface %d !!!\r\n",
                                  *pu4IfIndex);
                    return CFA_SUCCESS;
                }

            }
            SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecGetIntAndDir:"
                     "SecUtilGetIfIndexFromVlanId: Identified Interface Index from VlanId !!!\r\n");
        }
        else
        {
            /* (i) security for bridged packets is enabled. */
            if (SecGetBrigdingStatus () == CFA_ENABLED)
            {
                /* a. If the incoming vlan is member of security VLAN list. */
                if (SecIsMemberOfSecVlanList (u2VlanId) == OSIX_TRUE)
                {
                    u4SecIvrIndex = SecGetSecIvrIndex ();
                    if (u4SecIvrIndex != CFA_INVALID_INDEX)
                    {
                        /* The arp packets switched over bridged WAN interfaces should 
                         * not be processed. 
                         */
                        if (*
                            ((UINT2 *) (pBuf->pSkb->data +
                                        VLAN_TAGGED_HEADER_SIZE)) ==
                            CFA_ENET_ARP)
                        {
                            if (dev_queue_xmit (pBuf->pSkb) != NET_XMIT_SUCCESS)
                            {
                                SEC_TRC (SEC_CONTROL_PLANE_TRC,
                                         "GddKSecGetIntAndDir:"
                                         "Unable to transmit the packet !!!\r\n");
                                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                                return CFA_FAILURE;
                            }
                            SEC_TRC (SEC_CONTROL_PLANE_TRC,
                                     "GddKSecGetIntAndDir:"
                                     "The arp packets switched over bridged WAN interfaces should not be processed. !!!\r\n");
                            CRU_BUF_Release_ChainDesc (pBuf);
                            return CFA_FAILURE;
                        }

                        /* 1. A valid security IVR exists. return the same as the 
                         *    interface index for security processing. 
                         */
                        SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecGetIntAndDir:"
                                 "Valid Security IVR Index Exist. !!!\r\n");
                        *pu4IfIndex = u4SecIvrIndex;
                        return CFA_SUCCESS;
                    }
                }

                /* In all other scenarios, write back the frame without security 
                 * processing. 
                 */
                if (dev_queue_xmit (pBuf->pSkb) != NET_XMIT_SUCCESS)
                {
                    SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKsecIntAndDir:"
                             " Failed to transmit the packet - Security IVR does not \
                            exist for bridged packet!!!\r\n");
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    return CFA_FAILURE;
                }
                SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecGetIntAndDir:"
                         "Unable to find security IVR index!!!\r\n");
                CRU_BUF_Release_ChainDesc (pBuf);
                return CFA_FAILURE;
            }
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return CFA_FAILURE;
        }
    }
    SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecGetIntAndDir:"
             "Not a Vlan tagged packet!!!\r\n");
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    SEC_TRC (SEC_CONTROL_PLANE_TRC, "GddKSecGetIntAndDir:"
             "Exiting Function GddKSecGetIntAndDir !!!\r\n");
    return CFA_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name     : CfaIwfEnetProcessTxFrame
 *
 *    Description       : This function is used to forward the packet 
 *                        from the kernel using devqueue transmit.
 *
 *    Input(s)              : pCRUBuf - The packet buffer
 *                            au1DestHwAddr - Destination hardware address 
 *                            of the packet to be transmitted.
 *                            u4PktSize - The size of the packet.
 *                            u2Protocol - The protocol of the packet.
 *                            u1EncapType - The encapsulation type of the packet.
 *
 *    Output(s)            : u1Direction - The direction of the packet.
 *                            u4IfIndex - Interface index on which the packet 
 *                            to be transmitted.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS / CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaIwfEnetProcessTxFrame (tCRU_BUF_CHAIN_HEADER * pCRUBuf, UINT4 u4IfIndex,
                          UINT1 *au1DestHwAddr, UINT4 u4PktSize,
                          UINT2 u2Protocol, UINT1 u1EncapType)
{
    tSecModuleData     *pSecModuleData = NULL;
    UINT1               au1Buf[SEC_MODULE_DATA_SIZE];
    UINT1              *pu1Buf = NULL;
    UINT4               u4Offset = CFA_VLAN_TAG_OFFSET;
    UINT4               u4Size = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2Proto = 0;
    UINT1               u1IfType = 0;
    UINT4               u4PppIndex = 0;
    UINT1               u1Direction = 0;
    UINT4               u4Index = 0;
    struct iphdr       *ipfh = NULL;
    UINT2               u2CheckSum = 0;

    UNUSED_PARAM (au1DestHwAddr);
    UNUSED_PARAM (u1EncapType);
    UNUSED_PARAM (u4PktSize);

    u4IfIndex = SEC_GET_IFINDEX (pCRUBuf);
    u1Direction = SEC_GET_DIRECTION (pCRUBuf);

    SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaIwfEnetProcessTxFrame:"
             "Entering Function CfaIwfEnetProcessTxFrame !!!\r\n");

    if (SEC_INBOUND == u1Direction)
    {
        /* Strip PPP header from the packet, if the packet is SEC_INBOUND and 
         * Index is stacked with PPP Index.
         */
        if (OSIX_SUCCESS ==
            SecUtilGetPppIdxFromPhyIfdx (u4IfIndex, &u4PppIndex))
        {
            if (OSIX_SUCCESS != SecUtilStripPppHdrFromBuf (pCRUBuf, u4IfIndex))
            {
                SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaIwfProcessTxFrame:"
                         "Unable to strip PPP Header from CRU Buffer !!!\r\n");
                return CFA_FAILURE;
            }
        }
    }

    if (NULL ==
        (pSecModuleData = (tSecModuleData *) CRU_BUF_Get_ModuleData (pCRUBuf)))
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaIwfProcessTxFrame:"
                 "Unable to Get module Data From The Packet !!!\r\n");
        return CFA_FAILURE;
    }

    if (gu1PktRecv == CFA_ARCH_FSIP)
    {

/* For all the non-tagged frames sent, tag them with the 
     * dummy vlan id associated with the interface index. 
     */
        if (CFA_VLAN_PROTOCOL_ID != pSecModuleData->u2LenOrType)
        {
            /* Save the first CFA_ENET_V2_HEADER_SIZE(14) bytes.
             * in the local array au1Buf.
             */
            MEMSET (au1Buf, 0, SEC_MODULE_DATA_SIZE);
            pu1Buf = au1Buf;
            if (CRU_FAILURE == CRU_BUF_Copy_FromBufChain (pCRUBuf, pu1Buf, 0,
                                                          CFA_VLAN_TAG_OFFSET))
            {
                SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaIwfProcessTxFrame:"
                         "Failed to Copy ethernet header to array !!!\r\n");
                return CFA_FAILURE;
            }

            /* Append 0x81 00 to the au1Buf array. 
             */
            *((UINT2 *) ((VOID *) &au1Buf[u4Offset])) =
                OSIX_HTONS (CFA_VLAN_PROTOCOL_ID);
            u4Offset += sizeof (UINT2);

            /* Fetch the dummy vlan ID associated with the 
             * interface index. 
             */
            SecUtilGetIfTypeFromPhyIndex (u4IfIndex, &u1IfType);
            if (u1IfType == CFA_PPP)
            {
                if (OSIX_FAILURE ==
                    SecUtilGetVlanIdFromPhyIfIndex (u4IfIndex, &u2VlanId))
                {
                    SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaIwfProcessTxFrame:"
                             "Failed to fetch the dummy vlan id !!!\r\n");
                    return CFA_FAILURE;
                }
            }
            else
            {
                if (OSIX_FAILURE ==
                    SecUtilGetVlanIdFromIfIndex (u4IfIndex, &u2VlanId))
                {
                    SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaIwfProcessTxFrame:"
                             "Failed to fetch the dummy vlan id !!!\r\n");
                    return CFA_FAILURE;
                }
            }

            /* Append the vlan id (2 bytes) to the au1Buf array. */
            *((UINT2 *) ((VOID *) &au1Buf[u4Offset])) = OSIX_HTONS (u2VlanId);
            u4Offset += sizeof (UINT2);

            /* Copy the protocol id from the Tx frame at 12th offset. 
             * and append the same to au1Buf array.
             */
            if (CRU_FAILURE == CRU_BUF_Copy_FromBufChain (pCRUBuf,
                                                          (UINT1 *) &u2Proto,
                                                          CFA_VLAN_TAG_OFFSET,
                                                          sizeof (UINT2)))
            {
                SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaIwfProcessTxFrame:"
                         "Failed to Copy Protocol from Tx frame!!!\r\n");
                return CFA_FAILURE;
            }

            *((UINT2 *) ((VOID *) &au1Buf[u4Offset])) = (u2Proto);
            u4Offset += sizeof (UINT2);

            /* Update the number of bytes. */
            u4Size = u4Offset;

            /* Move the valid offset bytes to 14 bytes forward. */
            if (CRU_BUF_Move_ValidOffset (pCRUBuf, CFA_ENET_V2_HEADER_SIZE) !=
                CRU_SUCCESS)
            {
                SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaIwfProcessTxFrame:"
                         "Failed to Move Valid Offset !!!\r\n");
                return CFA_FAILURE;
            }

            /* Now prepend the tagged header information saved in au1Buf
             * to the Tx frame.  
             * DestMAC-SourceMAC-0x81 0x00- VLANID - ProtocolID- Packet.
             *   [6]     [6]        [2]      [2]        [2]
             */
            if (CRU_FAILURE ==
                CRU_BUF_Prepend_BufChain (pCRUBuf, au1Buf, u4Size))
            {
                SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaIwfProcessTxFrame:"
                         "Failed to Copy tagged information to the TX frame !!!\r\n");
                return CFA_FAILURE;
            }

        }

        /* Add the Specific tag based on hw to the packet */
        if (CFA_FAILURE == GddKSecAddSpecificTag (pCRUBuf))
        {
            SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaIwfProcessTxFrame:"
                     "Failed to Append Specific Tag !!!\r\n");
            return CFA_FAILURE;
        }

        if (pCRUBuf->pSkb->dev == apDefXmitDev[1])
        {
            pCRUBuf->pSkb->dev = apDefXmitDev[0];
        }
        else
        {
            pCRUBuf->pSkb->protocol = u2Protocol;
            pCRUBuf->pSkb->dev = apDefXmitDev[1];
        }

        skb_set_mac_header (pCRUBuf->pSkb, 0);
        skb_set_network_header (pCRUBuf->pSkb, 0);
        skb_set_transport_header (pCRUBuf->pSkb, 0);

        if (dev_queue_xmit (pCRUBuf->pSkb) != NET_XMIT_SUCCESS)
        {
            SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaIwfEnetProcessTxFrame:"
                     " Failed to transmit the packet !!!\r\n");
            return CFA_FAILURE;
        }

        SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaIwfEnetProcessTxFrame:"
                 "Packet Transmitted Successfully !!!\r\n");
        CRU_BUF_Release_ChainDesc (pCRUBuf);
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaIwfEnetProcessTxFrame:"
                 "Exiting Function CfaIwfEnetProcessTxFrame !!!\r\n");
        return CFA_SUCCESS;
    }
    else
    {
        /* Mac Header and Network Header of the packet is properly updated */
        skb_set_mac_header (pCRUBuf->pSkb, 0);
        skb_set_network_header (pCRUBuf->pSkb, CFA_ETH_HDR_LEN);
        return CFA_SUCCESS;
    }
}

/*****************************************************************************
 *
 *    Function Name     : CfaHandlePktFromSec
 *
 *    Description       : This function is used to send the packet to 
 *                        CFA in user space.
 *
 *    Input(s)          : pBuf - The packet buffer
 *
 *    Output(s)         : None
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS / CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaHandlePktFromSec (tCRU_BUF_CHAIN_HEADER * pBuf)
{

    INT1                i1RetVal = OSIX_SUCCESS;
    tSecQueMsg          SecQueMsg;
    tCRU_BUF_CHAIN_HEADER *pNewBuf = NULL;

    MEMSET (&SecQueMsg, 0, sizeof (tSecQueMsg));
    SecQueMsg.u1MsgType = SEC_PKT_TO_CFA_FROM_KERNEL;
    SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaHandlePktFromSec:"
             "Entering Function CfaHandlePktFromSec !!!\r\n");

    pNewBuf = SEC_CRU_BUF_Duplicate_BufChain (pBuf);
    if (pNewBuf == NULL)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaHandlePktFromSec:"
                 " Failed to Duplicate CRU buffer !!!\r\n");
        return CFA_FAILURE;
    }
    if (CRU_BUF_Prepend_BufChain (pNewBuf, (UINT1 *)
                                  SEC_GET_MODULE_DATA_PTR (pBuf),
                                  sizeof (tSecModuleData)) == CRU_FAILURE)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaHandlePktFromSec:"
                 " Failed to prepend the module data !!!\r\n");
        SEC_CRU_BUF_Release_MsgBufChain (pNewBuf, FALSE);
        return CFA_FAILURE;
    }

    if (CRU_BUF_Prepend_BufChain (pNewBuf, (UINT1 *) &SecQueMsg,
                                  sizeof (tSecQueMsg)) == CRU_FAILURE)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaHandlePktFromSec:"
                 " Failed to prepend the SecQue Message !!!\r\n");
        SEC_CRU_BUF_Release_MsgBufChain (pNewBuf, FALSE);
        return CFA_FAILURE;
    }
    /* post the buffer to CFA queue */
    i1RetVal = OsixQueSend (gu4SecReadModIdx,
                            (UINT1 *) &pNewBuf, OSIX_DEF_MSG_LEN);

    if (i1RetVal != OSIX_SUCCESS)
    {
        SEC_CRU_BUF_Release_MsgBufChain (pNewBuf, FALSE);
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaHandlePktFromSec:"
                 " Failed to send the packet to CFA in user space!!!\r\n");
        return CFA_FAILURE;
    }
    /*This should not be released since we are giving NF_ACCEPT
     * on successful reverse translation for the packets destined to ISS
     * machine this would cause kernel panic.*/

    if (gu1PktRecv == CFA_ARCH_FSIP)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }
    SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaHandlePktFromSec:"
             "Exiting Function CfaHandlePktFromSec !!!\r\n");
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name     : CfaGddWrite
 *
 *    Description       : This function is used to transmit the packet 
 *                        out from the kernel.
 *
 *    Input(s)          : pBuf - The packet buffer
 *                        u4IfIndex - The interface index on which the packet 
 *                        needs to be sent 
 *                        u4PktSize -  The size of the packet
 *                        u1VlanTagCheck - Flag to check the vlan tag
 *                        pIfInfo - Interface info.
 *
 *    Output(s)            : None
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS / CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddWrite (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
             UINT4 u4PktSize, UINT1 u1VlanTagCheck, tCfaIfInfo * pIfInfo)
{
    UNUSED_PARAM (u1VlanTagCheck);
    UNUSED_PARAM (pIfInfo);

    if (CfaIwfEnetProcessTxFrame (pBuf, u4IfIndex, NULL, u4PktSize, 0, 0) !=
        CFA_SUCCESS)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaGddWrite:"
                 " Failed to transmit the packet out!!!\r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return CFA_FAILURE;
    }
    SEC_TRC (SEC_CONTROL_PLANE_TRC, "CfaGddWrite:"
             " Packet transmitted Successfully !!!\r\n");
    return CFA_SUCCESS;
}
#endif
