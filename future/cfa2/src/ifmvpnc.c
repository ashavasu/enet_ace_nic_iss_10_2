/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ifmvpnc.c,v 1.8 2014/04/16 13:03:48 siva Exp $
 *
 * Description: This file contains interface management routines 
 *              for VPN Client
 *
 *******************************************************************/
#ifndef _CFA2_VPNC_C_
#define _CFA2_VPNC_C_

#include "cfainc.h"

#define   CFA_VPNC_DEF_MTU            1500
#define   CFA_VPNC_SPEED              1000000000
#define   CFA_VPNC_DESCR             "VPN Client Interface"
#define   CFA_VPNC_NAME_PREFIX       "vpnc"
#define   CFA_VPNC_NAME_LEN          ((STRLEN(CFA_VPNC_NAME_PREFIX)) + 1)
#define   CFA_VPNC_IF_COUNT          gu1NumofVpncInterface

UINT1               gu1NumofVpncInterface = 0;    /* keep track of number of vpnc interface */

INT4                CfaIfmDeRegisterVpncInterfaceFromIp (UINT2 u2IfIndex);

PRIVATE UINT4
 
 
 
CfaVpnAddDelRoute (UINT2 u2IfIndex, UINT4 u4IpRouteDest, UINT4 u4IpRouteMask,
                   UINT1 u1Status);
PRIVATE INT4
 
 
   CfaSetVpncIpAddress (UINT4 u4IfIndex, UINT4 u4IpAddr, UINT4 u4IpSubnetMask);

PRIVATE INT4
       CfaSetVpncInterfaceAdminStatus (UINT4 u4IfIndex, UINT4 u4AdminStatus);

PRIVATE UINT4       CfaVpnIfVpncInterfaceExists (UINT4 *pu4Index);

PRIVATE BOOL1       CfaIfmIsVpncInterface (UINT4 u4IfIndex);

/*****************************************************************************
 *
 *    Function Name             : CfaIfmCreateDynamicVpncInterface
 *
 *    Description               : This function creates and initialises a new
 *                                VPN Client ifEntry. It enables NAT on the
 *                                vpnc interface. It also sets the vpnc interface
 *                                as External in firewall, so that attacks are
 *                                prevented
 *  
 *    Input(s)                  : u4DynamicVpncAddr - IP address for VPN Client
 *                                u4SubnetMask - Subnet mask for VPN Client
 *                                u4GatewayAddr - Next hop IP address
 *
 *    Output(s)                 : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if initialisation succeeds;
 *                                CFA_FAILURE, otherwise.
 *
 *****************************************************************************/
INT4
CfaIfmCreateDynamicVpncInterface (UINT4 u4DynamicVpncAddr,
                                  UINT4 u4SubnetMask, UINT4 u4GatewayAddr,
                                  UINT4 u4IpRouteDest, UINT4 u4IpRouteMask)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4Flag = 0;
    UINT2               u2IfIndex = 0;
    UINT1               au1NullString[2] = "";
    UINT1               u1Counter = 0;
    UINT1               u1SubnetMask = 0;
    tIpConfigInfo       IpConfigInfo;
    tCfaRegInfo         CfaInfo;

    UNUSED_PARAM (u4GatewayAddr);

    MEMSET (&IpConfigInfo, 0, sizeof (tIpConfigInfo));
    MEMSET (&CfaInfo, 0, sizeof (tCfaRegInfo));

    if (u4DynamicVpncAddr == 0)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Cannot create VPNC interface. IP address 0.0.0.0 is "
                 "invalid\n");
    }

    /* Check if a 'vpnc' interface already exists. If so update the
     * existing interface */

    if (CfaVpnIfVpncInterfaceExists (&u4IfIndex) == CFA_SUCCESS)
    {
        /* Shutdown the interface (make admin status down), populate 
         * the new IP address for VPN Client interface (vpnc) and 
         * then make the admin status of the interface as UP.
         */

        if (CfaSetVpncInterfaceAdminStatus (u4IfIndex, CFA_IF_DOWN) ==
            CFA_FAILURE)
        {
            return CFA_FAILURE;
        }

        if (CfaSetVpncIpAddress (u4IfIndex, u4DynamicVpncAddr,
                                 u4SubnetMask) == CFA_FAILURE)
        {
            return CFA_FAILURE;
        }

        if (CfaSetVpncInterfaceAdminStatus (u4IfIndex, CFA_IF_UP) ==
            CFA_FAILURE)
        {
            return CFA_FAILURE;
        }
        return ((INT4) u4IfIndex);
    }
    /* Get a free index from the CFA index pool */
    if (CfaGetFreeInterfaceIndex (&u4IfIndex, CFA_VPNC) == OSIX_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Cannot create VPNC interface. No free index available.\n");
        return (CFA_FAILURE);
    }

    u2IfIndex = (UINT2) u4IfIndex;
    /* Create an entry for VPN client interface */
    if (CfaIfmCreateAndInitIfEntry (u2IfIndex, CFA_VPNC,
                                    au1NullString) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Cannot create VPNC interface %d. Initialization failed.\n",
                  u2IfIndex);
        return (CFA_FAILURE);
    }
    /* Set the IP address obtained dynamically */
    IpConfigInfo.u4Addr = u4DynamicVpncAddr;

    /* Calculate the subnet mask length value */
    while (u4CidrSubnetMask[u1Counter] != u4SubnetMask)
    {
        ++u1Counter;
    }
    u1SubnetMask = u1Counter;
    u4SubnetMask = u4CidrSubnetMask[u1SubnetMask];
    /* Set the Subnet mask for the VPN client interface */
    IpConfigInfo.u4NetMask = u4SubnetMask;

    /* Set the broadcast address for the VPN client interface */
    IpConfigInfo.u4BcastAddr = (~u4SubnetMask | u4DynamicVpncAddr);

    u4Flag = CFA_IP_IF_PRIMARY_ADDR | CFA_IP_IF_NETMASK | CFA_IP_IF_BCASTADDR;

    /* we register the interface with IP */
    if (CfaIfmConfigNetworkInterface (CFA_NET_IF_NEW, u2IfIndex,
                                      u4Flag, &IpConfigInfo) != CFA_SUCCESS)
    {
        /* Unsuccessful with IP registration */
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmCreateDynamicVpncInterface - "
                  "IP Registration fail for vpnc interface %d\n", u2IfIndex);
        /* Do cleanup of VPN client interface entries */
        CfaIfmDeleteVpncInterface (u2IfIndex, CFA_TRUE);
        return (CFA_FAILURE);
    }
    /* Set the Network Type as WAN and Wan Type as Public */
    CfaSetIfNwType (u2IfIndex, CFA_NETWORK_TYPE_WAN);
    CfaSetIfWanType (u2IfIndex, CFA_WAN_TYPE_PUBLIC);
    /* Update the admin status */
    if (CfaIfmHandleInterfaceStatusChange (u2IfIndex,
                                           CFA_IF_UP,
                                           CFA_IF_UP, CFA_FALSE) != CFA_SUCCESS)
    {
        /* Unsuccessful Status updation */
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Update admin status of Vpn client interface failed \n");
        return CFA_FAILURE;
    }

    if (CfaVpnAddDelRoute (u2IfIndex, u4IpRouteDest,
                           u4IpRouteMask, CFA_NET_IF_NEW) != CFA_SUCCESS)
    {
        /* Unsuccessful Status updation */
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Add route for the VPN tarffic failed \n");
        return CFA_FAILURE;
    }

#if defined (SMOD_WANTED)
    /* Again Updating Interface status to make interface NAT status ACTIVE */
    CfaInfo.u4IfIndex = u4IfIndex;
    CfaInfo.CfaIntfInfo.u1IfType = CFA_VPNC;
    CfaInfo.CfaIntfInfo.u4IfMtu = CFA_VPNC_DEF_MTU;
    CfaInfo.CfaIntfInfo.u4IfSpeed = CFA_VPNC_SPEED;
    CfaInfo.CfaIntfInfo.u1WanType = CFA_WAN_TYPE_PUBLIC;
    CfaInfo.CfaIntfInfo.u1NwType = CFA_NETWORK_TYPE_WAN;
    CfaGetIfOperStatus (u4IfIndex, &(CfaInfo.CfaIntfInfo.u1IfOperStatus));
    CfaGetIfName (u4IfIndex, CfaInfo.CfaIntfInfo.au1IfName);

    CfaGetIfHwAddr (u4IfIndex, CfaInfo.CfaIntfInfo.au1MacAddr);
    CfaNotifyIfUpdate (&CfaInfo);
#endif /* SMOD_WANTED */

    /* Dynamic creation of VPN client interface is success */
    return ((INT4) u2IfIndex);

}

/*****************************************************************************
 *
 *    Function Name             : CfaIfmInitVpncIfEntry
 *
 *    Description               : This function initialises the VPNC ifEntries.
 *  
 *    Input(s)                  : UINT2 u2IfIndex
 *                                UINT1 *pu1PortName
 *
 *    Output(s)                 : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if initialisation succeeds;
 *                                CFA_FAILURE, otherwise.
 *
 *****************************************************************************/
INT4
CfaIfmInitVpncIfEntry (UINT4 u4IfIndex, UINT1 *pu1PortName)
{

    UINT1               au1PortName[CFA_MAX_PORT_NAME_LENGTH];
    tIpConfigInfo       IpCfgInfo;
    UINT4               u4Flag = 0;

    MEMSET (au1PortName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&IpCfgInfo, 0, sizeof (tIpConfigInfo));

    /* allocate for the IP config entry in the ifTable */
    if (CfaIpIfCreateIpInterface (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitVpncIfEntry - "
                  "IP Entry Mempool alloc fail for vpn interface %d.\n",
                  u4IfIndex);
        CfaSetIfType (u4IfIndex, CFA_INVALID_TYPE);
        /* Do cleanup of VPN client interface entries */
        CfaIfmDeleteVpncInterface (u4IfIndex, CFA_TRUE);

        return (CFA_FAILURE);
    }

    /* Initialise the IP config entry */
    u4Flag = CFA_IP_IF_FORWARD | CFA_IP_IF_ALLOC_METHOD;
    IpCfgInfo.u1IpForwardEnable = CFA_ENABLED;
    IpCfgInfo.u1AddrAllocMethod = CFA_IP_ALLOC_MAN;
    if (CfaIpIfSetIfInfo (u4IfIndex, u4Flag, &IpCfgInfo) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Setting IP Param for Interface %d failed\n", u4IfIndex);
        return CFA_FAILURE;
    }
    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_OTHER;
    CfaSetIfMtu (u4IfIndex, CFA_VPNC_DEF_MTU);
    CfaSetIfSpeed (u4IfIndex, CFA_VPNC_SPEED);
    CfaSetIfType (u4IfIndex, CFA_VPNC);
    CfaSetIfNwType (u4IfIndex, CFA_NETWORK_TYPE_WAN);
    CfaSetIfWanType (u4IfIndex, CFA_WAN_TYPE_PUBLIC);
    CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_DISABLED);

    /* Set the default alias name only if the alias is not set */
    if (*pu1PortName == 0)
    {
        STRCPY (CFA_IF_DESCR (u4IfIndex), CFA_VPNC_DESCR);
        SPRINTF ((CHR1 *) au1PortName, "%s%d", CFA_VPNC_NAME_PREFIX,
                 CFA_VPNC_IF_COUNT);
        CfaSetIfName (u4IfIndex, au1PortName);
    }

/* there is no RCV addr table for VPNC - so we dont add any nodes */
    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

/* Set Row status of VPN client interface and its associated
 * stack entries */
    CFA_IF_RS (u4IfIndex) = CFA_RS_ACTIVE;
    CfaSetIfActiveStatus (u4IfIndex, CFA_TRUE);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name             : CfaIfmDeleteVpncInterface
 *
 *    Description               : This function is for de-registration of
 *                                VPNC interface
 *
 *    Input(s)                  : u2IfIndex - Interface Index of vpnc
 *                                u1DelIfEntry - Flag to delete vpnc
 *
 *    Output(s)                 : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if deletion succeeds;
 *                                CFA_FAILURE, otherwise .
 *
 ****************************************************************************/
INT4
CfaIfmDeleteVpncInterface (UINT4 u4IfIndex, UINT1 u1DelIfEntry)
{

    UINT4               u4VpncIndex = 0;
    tStackInfoStruct   *pNode = NULL;
    tStackInfoStruct   *pScanNode = NULL;

    /* Check if the Interface is a VPN client interface before
     * deletion; If the interface is not a 'vpnc' interface
     * return failure */

    if (CfaVpnIfVpncInterfaceExists (&u4VpncIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmDeleteVpncInterface - "
                  "vpnc Interface does not exist %d\n", u4IfIndex);
        return (CFA_SUCCESS);
    }

    /* Assigning the acquired Index of the VPN client interface */
    u4IfIndex = u4VpncIndex;

    /* first we de-register the interface from IP or higher layer if it was
       registered earlier */
    pScanNode =
        (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY ((UINT2) u4IfIndex);
    CFA_CHECK_IF_NULL (pScanNode, CFA_FAILURE);

    if (CFA_IF_STACK_STATUS (pScanNode) == CFA_RS_ACTIVE)
    {

        pNode = (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
        if (pNode == NULL)
        {
            return (CFA_FAILURE);
        }
        if (CFA_IF_ENTRY_REG_WITH_IP (u4IfIndex, pNode))
        {
            /* this interface is registered with IP */
            if (CfaIfmConfigNetworkInterface (CFA_NET_IF_DEL, u4IfIndex, 0, 0)
                != CFA_SUCCESS)
            {
                /* unsuccessful in de-registering from IP - we cant delete the interface.
                   we dont expect this to fail - just for handling unforeseen exceptions. */
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                          "Error in CfaIfmDeleteVpncInterface - "
                          "IP De-reg fail for interface %d\n", u4IfIndex);
                return (CFA_FAILURE);
            }                    /* End of Config netwrk interface */
        }                        /* end of interface was registered with IP */

        /* the higher interface was invalidated - so we just delete the stack entry */
        pScanNode =
            (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY ((UINT2) u4IfIndex);
        CFA_CHECK_IF_NULL (pScanNode, CFA_FAILURE);

        CfaIfmDeleteStackEntry (u4IfIndex, CFA_HIGH,
                                CFA_IF_STACK_IFINDEX (pScanNode));

    }                            /* end of interface was registered to some higher module */

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteVpncInterface - "
              "Un-reg with HL success - interface %d\n", u4IfIndex);

    /* now we de-register all the lower interfaces and delete the stack entries
       for lower layers */

    /* scan all the VCs and update the stack table */
    pScanNode = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY (u4IfIndex);
    CFA_CHECK_IF_NULL (pScanNode, CFA_FAILURE);

    while (CFA_IF_STACK_IFINDEX (pScanNode) != CFA_NONE)
    {
        /* we now delete this VC from the stack table over this VPNC interface */
        CfaIfmDeleteStackEntry (u4IfIndex, CFA_LOW,
                                CFA_IF_STACK_IFINDEX (pScanNode));
    }                            /* end of while looop */

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteVpncInterface - "
              "Un-reg with LL success - interface %d\n", u4IfIndex);

    /* check whether the entry is to be deleted from IfTable itself */
    if (u1DelIfEntry != CFA_TRUE)
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "In CfaIfmDeletVpncInterface - "
                  "disabling success - interface %d\n", u4IfIndex);

        return (CFA_SUCCESS);
    }

    /* delete the interface from the IfTable */
    if (CfaIfmDeleteIfEntry (u4IfIndex, CFA_VPNC) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeleteVpncInterface - "
                  "ifEntry delete fail for interface %d\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteVpncInterface - "
              "Full Delete success - interface %d\n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name             : CfaIfmDeRegisterVpncInterfaceFromIp
 *
 *    Description               : This function is used for de-registration of
 *                                VPNC interface with IP.
 *
 *    Input(s)                  : UINT2 u2IfIndex
 *
 *    Output(s)                 : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if deregistration succeeds;
 *                                CFA_FAILURE, otherwise.
 *
 *****************************************************************************/
INT4
CfaIfmDeRegisterVpncInterfaceFromIp (UINT2 u2IfIndex)
{

    tStackInfoStruct   *pNode = NULL;
    tStackInfoStruct   *pScanNode = NULL;

/* first make the interface down */
    if (CfaIfmHandleInterfaceStatusChange (u2IfIndex, CFA_IF_DOWN,
                                           CFA_IF_DOWN,
                                           CFA_FALSE) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmDeRegisterVpncInterfaceFromIp !\n"
                  "Making VPNC interface %d DOWN - FAIL\n", u2IfIndex);
        return CFA_FAILURE;
    }

    pScanNode = (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u2IfIndex);
    CFA_CHECK_IF_NULL (pScanNode, CFA_FAILURE);

/* De-register the interface from IP registered earlier */
    if (CFA_IF_STACK_STATUS (pScanNode) == CFA_RS_ACTIVE)
    {
        pNode = (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u2IfIndex);
        if (pNode == NULL)
        {
            return (CFA_FAILURE);
        }

        if (CFA_IF_ENTRY_REG_WITH_IP (u2IfIndex, pNode))
        {
/* this interface is registered with IP */
            if (CfaIfmConfigNetworkInterface (CFA_NET_IF_DEL, u2IfIndex, 0, 0)
                != CFA_SUCCESS)
            {
                /* unsuccessful in de-registering from IP - we cant delete the interface.
                   we dont expect this to fail - just for handling unforeseen exceptions. */
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                          "Error in CfaIfmDeleteVpncInterface - "
                          "IP De-reg fail for interface %d\n", u2IfIndex);
                return (CFA_FAILURE);
            }                    /* End of Config netwrk interface */
        }                        /* end of interface was registered with IP */
    }                            /* end of interface was registered to some higher module */

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteVpncInterface - "
              "Un-reg with LL success - interface %d\n", u2IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaVpnAddDelRoute                                  */
/*                                                                           */
/*     DESCRIPTION      : This function adds a route for the vpnc interface  */
/*     INPUT            : u2IfIndex - Index of the vpnc interface            */
/*                        u4IpRouteDest - Destination Ip Address             */
/*                        u4IpRouteMask - Mask                               */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

UINT4
CfaVpnAddDelRoute (UINT2 u2IfIndex, UINT4 u4IpRouteDest, UINT4 u4IpRouteMask,
                   UINT1 u1Status)
{
    tNetIpv4RtInfo      NetRtInfo;
    INT4                i4RetVal = IP_FAILURE;
    UINT2               u2Port = 0;
    UINT1               u1CmdType = 0;

    MEMSET (&NetRtInfo, 0, sizeof (tNetIpv4RtInfo));
    u2Port = (UINT2) (CfaGetIfIpPort (u2IfIndex));

    /* Add the Route with IpPort Number */
    NetRtInfo.u4RtIfIndx = u2Port;
    NetRtInfo.u4NextHop = 0x0;
    NetRtInfo.u2RtType = IPROUTE_LOCAL_TYPE;    /* remote destination ???? */
    NetRtInfo.u2RtProto = CIDR_STATIC_ID;
    NetRtInfo.u4DestNet = u4IpRouteDest;
    NetRtInfo.u4DestMask = u4IpRouteMask;
    NetRtInfo.u4RtNxtHopAs = 0;
    NetRtInfo.u4RtAge = OsixGetSysUpTime ();

    if (u1Status == CFA_NET_IF_DEL)
    {
        NetRtInfo.u4RowStatus = (UINT4) IPFWD_DESTROY;
        u1CmdType = NETIPV4_DELETE_ROUTE;
    }
    else if (u1Status == CFA_NET_IF_NEW)
    {
        NetRtInfo.u4RowStatus = (UINT4) IPFWD_ACTIVE;
        u1CmdType = NETIPV4_ADD_ROUTE;
    }

    i4RetVal = NetIpv4LeakRoute (u1CmdType, &NetRtInfo);
    if (i4RetVal == NETIPV4_FAILURE)
    {
        return ((UINT4) CFA_FAILURE);
    }
    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaVpnIfVpncInterfaceExists                        */
/*                                                                           */
/*     DESCRIPTION      : This function checks if a vpnc interface exists    */
/*                        and returns the interface index of the existing    */
/*                        interface.                                         */
/*     INPUT            : u4IfIndex - Index of the vpnc interface            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
UINT4
CfaVpnIfVpncInterfaceExists (UINT4 *pu4Index)
{
    UINT4               u4Index = 1;

    CFA_DS_LOCK ();

    /* Scan all the interfaces and check if a vpnc interface
     * exist. If a vpnc interface exists, return success;else
     * failure */

    CFA_CDB_SCAN (u4Index, SYS_MAX_INTERFACES, CFA_ALL_IFTYPE)
    {
        if (CFA_CDB_IS_INTF_VALID (u4Index) == CFA_FALSE)
        {
            continue;
        }

        if (CfaIfmIsVpncInterface (u4Index) == TRUE)
        {
            *pu4Index = u4Index;
            CFA_DS_UNLOCK ();
            return (CFA_SUCCESS);
        }
    }
    CFA_DS_UNLOCK ();
    return ((UINT4) CFA_FAILURE);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetVpncInterfaceAdminStatus                     */
/*                                                                           */
/*     DESCRIPTION      : This function set the admin status of  the         */
/*                        vpnc interface                                     */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
/*                        u4AdminStatus - Status                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetVpncInterfaceAdminStatus (UINT4 u4IfIndex, UINT4 u4AdminStatus)
{
    UINT4               u4ErrCode;

    if (nmhTestv2IfAdminStatus (&u4ErrCode, u4IfIndex, u4AdminStatus)
        == SNMP_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_MAIN,
                  "In CfaSetVpncInterfaceAdminStatus - "
                  "Failure while setting admin status for"
                  "- interface %d\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    if (nmhSetIfAdminStatus (u4IfIndex, u4AdminStatus) == SNMP_FAILURE)
    {
        return (CFA_FAILURE);
    }

    return (CFA_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaSetVpncIpAddress                                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the IP address of an VPN client */
/*                         interface                                         */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
/*                        u4IpAddr, u4SubnetMask - IPAddress, Subnet Mask    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SUCCESS/FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaSetVpncIpAddress (UINT4 u4IfIndex, UINT4 u4IpAddr, UINT4 u4IpSubnetMask)
{
    UINT4               u4ErrCode;
    UINT4               u4BcastAddr;
    UINT4               u4PrevIpAddr;
    UINT4               u4PrevSubnetMask;
    UINT4               u4PrevBcastAddr;

    nmhGetIfIpAddr (u4IfIndex, &u4PrevIpAddr);
    nmhGetIfIpSubnetMask (u4IfIndex, &u4PrevSubnetMask);
    nmhGetIfIpBroadcastAddr (u4IfIndex, &u4PrevBcastAddr);

    /* Test Ip addr */
    if (nmhTestv2IfIpAddr (&u4ErrCode, u4IfIndex, u4IpAddr) == SNMP_FAILURE)
    {
        return (CFA_FAILURE);
    }

    if (nmhSetIfIpAddr (u4IfIndex, u4IpAddr) == SNMP_FAILURE)
    {
        return (CFA_FAILURE);
    }
    /* Test Sub net mask */
    if (nmhTestv2IfIpSubnetMask (&u4ErrCode, u4IfIndex,
                                 u4IpSubnetMask) == SNMP_FAILURE)
    {
        return (CFA_FAILURE);
    }

    if (nmhSetIfIpSubnetMask (u4IfIndex, u4IpSubnetMask) == SNMP_FAILURE)
    {
        nmhSetIfIpAddr (u4IfIndex, u4PrevIpAddr);
        return (CFA_FAILURE);
    }

    u4BcastAddr = u4IpAddr | (~(u4IpSubnetMask));

    if (nmhTestv2IfIpBroadcastAddr (&u4ErrCode, u4IfIndex,
                                    u4BcastAddr) == SNMP_FAILURE)
    {
        nmhSetIfIpAddr (u4IfIndex, u4PrevIpAddr);
        nmhSetIfIpSubnetMask (u4IfIndex, u4PrevSubnetMask);
        return (CFA_FAILURE);
    }

    if (nmhSetIfIpBroadcastAddr (u4IfIndex, u4BcastAddr) == SNMP_FAILURE)
    {
        return (CFA_FAILURE);
    }
    return (CFA_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaIfmIsVpncInterface                              */
/*                                                                           */
/*     DESCRIPTION      : This function checks if the given interface is a   */
/*                        vpn client interface or not.                       */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface                 */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : TRUE/FALSE                                         */
/*****************************************************************************/
BOOL1
CfaIfmIsVpncInterface (UINT4 u4IfIndex)
{
    if ((CFA_CDB_IF_TYPE (u4IfIndex) == CFA_VPNC) &&
        ((STRNCMP (CFA_CDB_IF_ALIAS (u4IfIndex), CFA_VPNC_NAME_PREFIX,
                   (CFA_VPNC_NAME_LEN - 1))) == 0))
    {
        return TRUE;
    }
    return FALSE;
}
#endif /*** END OF FILE: ifmvpnc.c ***/
