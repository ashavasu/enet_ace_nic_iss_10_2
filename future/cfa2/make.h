# 
# $Id: make.h,v 1.30
#
#
#!/bin/csh
#Copyright (C) 2006 Aricent Inc . All Rights Reserved
#
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 22/08/2000                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the FutureCFA          |
# |                            product.                                      |
# |                                                                          |
# +--------------------------------------------------------------------------+
#
#     CHANGE RECORD :
# +--------------------------------------------------------------------------+
# | VERSION | AUTHOR/    | DESCRIPTION OF CHANGE                             |
# |         | DATE       |                                                   |
# +---------|------------|---------------------------------------------------+
# |   1     | RSR        | Creation of makefile                              |
# |         | 25/08/2000 |                                                   |
# +--------------------------------------------------------------------------+


# Set the PROJ_BASE_DIR as the directory where you untar the project files
PROJECT_NAME  = FutureCFA
PROJECT_BASE_DIR = ${BASE_DIR}/cfa2
PROJECT_SOURCE_DIR = ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR = ${PROJECT_BASE_DIR}/obj
#if ((defined (WLC_WANTED)) || (defined (WTP_WANTED)))
WSS_BASE_DIR = ${BASE_DIR}/wss
#endif

# Specify the project level compilation switches here
PROJECT_COMPILATION_SWITCHES = -U_BSD_SOURCE \
    -UCFA_DEBUG -UPORT_MAC_WANTED -UUNIQUE_VLAN_MAC_WANTED \

# CPSS 3.1 chg - check OS_LINUX_PTHREADS
ifeq ($(TARGET_ASIC), DX285)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif

ifeq ($(TARGET_ASIC), DX5128)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif

ifeq ($(TARGET_ASIC), DX167)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif

ifeq ($(TARGET_ASIC), XCAT)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif

ifeq ($(TARGET_ASIC), XCAT3)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif

ifeq ($(TARGET_ASIC), ALTERA)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif

ifeq ($(TARGET_ASIC), LION)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif

ifeq ($(TARGET_ASIC), LION_DB)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif

ifeq ($(TARGET_ASIC), FULCRUM)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif

ifeq ($(TARGET_ASIC), PETRA)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif

ifeq ($(TARGET_ASIC), QCA)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE 
endif

ifeq ($(TARGET_ASIC), WASP)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif

ifeq ($(TARGET_ASIC), LNXWIRELESS)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif

ifeq ($(TARGET_ASIC), QORIQ)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif

ifeq ($(TARGET_ASIC), VTSS_SERVAL_1)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif

ifeq ($(TARGET_ASIC), VTSS_SERVAL_2)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif

ifeq ($(TARGET_OS), OS_LINUX_PTHREADS)
ifeq ($(TARGET_ASIC), BCMX)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif
ifeq ($(TARGET_ASIC), SWITCHCORE)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif
endif

ifeq ($(TARGET_OS), OS_QNX)
ifneq (${TARGET_ASIC}, NONE)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif
endif

ifeq (${TARGET_ASIC}, NPSIM)
ifeq (${MBSM}, YES)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif
endif

ifeq ($(TARGET_OS), OS_VXWORKS)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif

ifeq ($(TARGET_OS), OS_VX2PTH_WRAP)
PROJECT_COMPILATION_SWITCHES += -DCFA_INTERRUPT_MODE
endif


PROJECT_FINAL_INCLUDES_DIRS = -I$(PROJECT_INCLUDE_DIR) \
     $(COMMON_INCLUDE_DIRS)

PROJECT_FINAL_INCLUDE_FILES = $(PROJECT_INCLUDE_FILES)

PROJECT_FINAL_INCLUDES_DIRS += -I ${ISS_COMMON_DIR}/system/inc \
                               -I ${ISS_EXT_DIR}/inc

PROJECT_DEPENDENCIES = $(COMMON_DEPENDENCIES) \
    $(PROJECT_BASE_DIR)/Makefile \
    $(PROJECT_BASE_DIR)/make.h


