#
# Copyright (C) 2007 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                                  |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 10 November 2007                              |
# |                                                                          |
# |   DESCRIPTION            : This file contains all the warning options    |
# |                            that are used for building this module        |
# +--------------------------------------------------------------------------+

#include the LR make.h and make.rule
include ../../LR/make.h
include ../../LR/make.rule

#Project base directories
PROJECT_NAME			= FutureIPDB
PROJECT_BASE_DIR		= ${BASE_DIR}/cfa2/ipdb
PROJECT_SOURCE_DIR	= $(PROJECT_BASE_DIR)/src
PROJECT_INCLUDE_DIR	= $(PROJECT_BASE_DIR)/inc
PROJECT_OBJECT_DIR	= $(PROJECT_BASE_DIR)/obj

SNMP_INC_DIR            = ${BASE_DIR}/inc/snmp
COMN_INC_DIR            = ${BASE_DIR}/inc
FSAP_INC_DIR            = ${BASE_DIR}/fsap2/tmo

#Project compilation switches
PROJECT_COMPILATION_SWITCHES	=

PROJECT_FINAL_INCLUDES_DIRS     = -I$(PROJECT_INCLUDE_DIR) \
                                        $(COMMON_INCLUDE_DIRS) \
                                        -I$(FSAP_INC_DIR)

#Project final include files
PROJECT_FINAL_INCLUDE_FILES   = $(PROJECT_INCLUDE_FILES)

#dependencies
PROJECT_DEPENDENCIES        =  	$(COMMON_DEPENDENCIES) \
											$(PROJECT_FINAL_INCLUDE_FILES) \
											$(PROJECT_BASE_DIR)/Makefile \
											$(PROJECT_BASE_DIR)/make.h
