/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: ipdbutil.c,v 1.15 2014/02/24 11:40:26 siva Exp $                */
/*****************************************************************************/
/*    FILE  NAME            : ipdbutil.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : IP Binding Database management                 */
/*    MODULE NAME           : Ip Binding Database management util module     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains util functions for          */
/*                            IP Binding Database management module          */
/*---------------------------------------------------------------------------*/

#include "ipdbinc.h"
#include "ipdbextn.h"

/*  Data buffer for duplicating data information */
PRIVATE UINT1       gau1IpdbDataBuffer[IPDB_MAX_MTU] = { IPDB_ZERO };

/*****************************************************************************/
/* Function Name      : IpDbUtilGetBindEntryByIp                             */
/*                                                                           */
/* Description        : This function is to get the binding entry based on   */
/*                      IP Address                                           */
/*                                                                           */
/* Input(s)           : u4HostIp - Host Ip Address                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Pointer to Binding entry / NULL                      */
/*****************************************************************************/
tIpDbBindingEntry  *
IpDbUtilGetBindEntryByIp (UINT4 u4HostIp)
{
    tIpDbBindingEntry  *pIpDbBindingEntry = NULL;
    tIpDbBindingEntry   IpDbBindingEntry;

    MEMSET (&IpDbBindingEntry, IPDB_ZERO, sizeof (tIpDbBindingEntry));

    IpDbBindingEntry.u4HostIp = u4HostIp;

    /* Get the entry from RBTree */
    pIpDbBindingEntry = (tIpDbBindingEntry *)
        RBTreeGet (IPDB_IP_BINDING_RBTREE, (tRBElem *) & IpDbBindingEntry);

    return pIpDbBindingEntry;
}

/*****************************************************************************/
/* Function Name      : IpDbUtilGetBindEntryByIp                             */
/*                                                                           */
/* Description        : This function is to get the binding entry based on   */
/*                      IP Address                                           */
/*                                                                           */
/* Input(s)           : u4HostIp - Host Ip Address                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Pointer to Binding entry / NULL                      */
/*****************************************************************************/
INT4
IpDbIsGatewayIp (tIpDbBindingEntry * pIpDbBindingEntry, UINT4 u4DestIp)
{
    tIpDbGatewayEntry  *pGatewayNode = NULL;

    TMO_SLL_Scan (&(pIpDbBindingEntry->NetworkList), pGatewayNode,
                  tIpDbGatewayEntry *)
    {
        if (pGatewayNode != NULL)
        {
            if (pGatewayNode->u4GatewayIp == u4DestIp)
            {
                return IPDB_SUCCESS;
            }
        }
    }

    return IPDB_FAILURE;
}

/*****************************************************************************/
/* Function Name      : IpDbUtilGetPortForIp                                 */
/*                                                                           */
/* Description        : This function is to get the Port to which the Host   */
/*                      with given IP address is attached to                 */
/*                                                                           */
/* Input(s)           : u4HostIp - Host Ip Address                           */
/*                                                                           */
/* Output(s)          : pu4Port - Port to which the host is attached         */
/*                                                                           */
/* Return Value(s)    : IPDB_SUCCESS/IPDB_FAILURE                            */
/*****************************************************************************/
INT4
IpDbUtilGetPortForIp (UINT4 u4HostIp, UINT4 *pu4Port)
{
    tIpDbBindingEntry  *pIpDbBindingEntry = NULL;

    /* Get the entry, and if entry does not enist, return failure */
    pIpDbBindingEntry = IpDbUtilGetBindEntryByIp (u4HostIp);

    if (pIpDbBindingEntry == NULL)
    {
        *pu4Port = IPDB_ZERO;
        return IPDB_FAILURE;
    }

    *pu4Port = pIpDbBindingEntry->u4InIfIndex;
    return IPDB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IpDbUtilTakeLock                                     */
/*                                                                           */
/* Description        : This function is to take the protocol lock for the   */
/*                      module                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/
INT4
IpDbUtilTakeLock (VOID)
{
    if (IPDB_TAKE_SEM (IPDB_ZERO, IPDB_SEM_NAME, IPDB_ZERO, IPDB_ZERO)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IpDbUtilReleaseLock                                  */
/*                                                                           */
/* Description        : This function is to release the protocol lock taken  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/
INT4
IpDbUtilReleaseLock (VOID)
{
    IPDB_GIVE_SEM (IPDB_ZERO, IPDB_SEM_NAME);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IpdbUtilRegisterFsIpdbMib                            */
/*                                                                           */
/* Description        : This function is to register the protocol MIB        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IpdbUtilRegisterFsIpdbMib (VOID)
{
    RegisterFSMIIP ();

    /* Registering SI mibs in case of SI alone. */
    if (IpDbUtilGetVcmSystemModeExt (IPDB_PROTOCOL_ID) == VCM_SI_MODE)
    {
        RegisterFSIPDB ();
    }
    return;
}

/*****************************************************************************/
/* Function Name      : IpdbUtilRBTreeStaticCmp                              */
/*                                                                           */
/* Description        : This function is used for comparing two entries of   */
/*                      RBTree for static                                    */
/*                                                                           */
/* Input(s)           : pRBElem1 - First Entry                               */
/*                      pRBElem2 - second Entry                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when entry 1 is Less/Greater than entry 2    */
/*                      0    -> when entries elements are Equal              */
/*****************************************************************************/
INT4
IpdbUtilRBTreeStaticCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    INT4                i4RetVal = IPDB_ZERO;
    tIpDbStaticBindingEntry *pIpDbStaticBindingEntry1 = NULL;
    tIpDbStaticBindingEntry *pIpDbStaticBindingEntry2 = NULL;

    pIpDbStaticBindingEntry1 = ((tIpDbStaticBindingEntry *) pRBElem1);
    pIpDbStaticBindingEntry2 = ((tIpDbStaticBindingEntry *) pRBElem2);

    if (pIpDbStaticBindingEntry1->u4ContextId <
        pIpDbStaticBindingEntry2->u4ContextId)
    {
        return IPDB_MINUS_ONE;
    }
    if (pIpDbStaticBindingEntry1->u4ContextId >
        pIpDbStaticBindingEntry2->u4ContextId)
    {
        return IPDB_ONE;
    }

    if (pIpDbStaticBindingEntry1->VlanId < pIpDbStaticBindingEntry2->VlanId)
    {
        return IPDB_MINUS_ONE;
    }
    if (pIpDbStaticBindingEntry1->VlanId > pIpDbStaticBindingEntry2->VlanId)
    {
        return IPDB_ONE;
    }

    i4RetVal = MEMCMP (pIpDbStaticBindingEntry1->HostMac,
                       pIpDbStaticBindingEntry2->HostMac, sizeof (tMacAddr));

    if (i4RetVal < IPDB_ZERO)
    {
        return IPDB_MINUS_ONE;
    }
    if (i4RetVal > IPDB_ZERO)
    {
        return IPDB_ONE;
    }
    return IPDB_ZERO;
}

/*****************************************************************************/
/* Function Name      : IpdbUtilMacRBTreeBindCmp                             */
/*                                                                           */
/* Description        : This function is used for comparing two entries of   */
/*                      Mac based RBTree for binding entries                 */
/*                                                                           */
/* Input(s)           : pRBElem1 - First Entry                               */
/*                      pRBElem2 - second Entry                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when entry 1 is Less/Greater than entry 2    */
/*                      0    -> when entries elements are Equal              */
/*****************************************************************************/
INT4
IpdbUtilMacRBTreeBindCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    INT4                i4RetVal = IPDB_ZERO;
    tIpDbBindingEntry  *pIpDbBindingEntry1 = NULL;
    tIpDbBindingEntry  *pIpDbBindingEntry2 = NULL;

    pIpDbBindingEntry1 = ((tIpDbBindingEntry *) pRBElem1);
    pIpDbBindingEntry2 = ((tIpDbBindingEntry *) pRBElem2);

    if (pIpDbBindingEntry1->u4ContextId < pIpDbBindingEntry2->u4ContextId)
    {
        return IPDB_MINUS_ONE;
    }
    if (pIpDbBindingEntry1->u4ContextId > pIpDbBindingEntry2->u4ContextId)
    {
        return IPDB_ONE;
    }

    if ((pIpDbBindingEntry1->VlanId) < (pIpDbBindingEntry2->VlanId))
    {
        return IPDB_MINUS_ONE;
    }
    if ((pIpDbBindingEntry1->VlanId) > (pIpDbBindingEntry2->VlanId))
    {
        return IPDB_ONE;
    }

    i4RetVal = MEMCMP ((pIpDbBindingEntry1->HostMac),
                       (pIpDbBindingEntry2->HostMac), sizeof (tMacAddr));

    if (i4RetVal < IPDB_ZERO)
    {
        return IPDB_MINUS_ONE;
    }
    if (i4RetVal > IPDB_ZERO)
    {
        return IPDB_ONE;
    }
    return IPDB_ZERO;
}

/*****************************************************************************/
/* Function Name      : IpdbUtilIpRBTreeBindCmp                              */
/*                                                                           */
/* Description        : This function is used for comparing two entries of   */
/*                      Ip based RBTree for binding entries                  */
/*                                                                           */
/* Input(s)           : pIpDbBindingEntryOne - First Entry                   */
/*                      pIpDbBindingEntryTwo - second Entry                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when entry 1 is Less/Greater than entry 2    */
/*                      0    -> when entries elements are Equal              */
/*****************************************************************************/
INT4
IpdbUtilIpRBTreeBindCmp (tRBElem * pIpDbBindingEntryOne,
                         tRBElem * pIpDbBindingEntryTwo)
{

    if (((tIpDbBindingEntry *) pIpDbBindingEntryOne)->u4HostIp <
        ((tIpDbBindingEntry *) pIpDbBindingEntryTwo)->u4HostIp)
    {
        return IPDB_MINUS_ONE;
    }
    if (((tIpDbBindingEntry *) pIpDbBindingEntryOne)->u4HostIp >
        ((tIpDbBindingEntry *) pIpDbBindingEntryTwo)->u4HostIp)
    {
        return IPDB_ONE;
    }

    return IPDB_ZERO;
}

/*****************************************************************************/
/* Function Name      : IpdbUtilRBTreeIntfEntryCmp                           */
/*                                                                           */
/* Description        : This function is used for comparing two entries of   */
/*                      RBTree for interface entries                         */
/*                                                                           */
/* Input(s)           : pRBElem1 - First Entry                               */
/*                      pRBElem2 - second Entry                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when entry 1 is Less/Greater than entry 2    */
/*                      0    -> when entries elements are Equal              */
/*****************************************************************************/
INT4
IpdbUtilRBTreeIntfEntryCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tIpDbIfaceEntry    *pIpDbIfaceEntry1 = NULL;
    tIpDbIfaceEntry    *pIpDbIfaceEntry2 = NULL;

    pIpDbIfaceEntry1 = ((tIpDbIfaceEntry *) pRBElem1);
    pIpDbIfaceEntry2 = ((tIpDbIfaceEntry *) pRBElem2);

    if ((pIpDbIfaceEntry1->u4ContextId) < (pIpDbIfaceEntry2->u4ContextId))
    {
        return IPDB_MINUS_ONE;
    }
    if ((pIpDbIfaceEntry1->u4ContextId) > (pIpDbIfaceEntry2->u4ContextId))
    {
        return IPDB_ONE;
    }

    if ((pIpDbIfaceEntry1->VlanId) < (pIpDbIfaceEntry2->VlanId))
    {
        return IPDB_MINUS_ONE;
    }
    if ((pIpDbIfaceEntry1->VlanId) > (pIpDbIfaceEntry2->VlanId))
    {
        return IPDB_ONE;
    }
    return IPDB_ZERO;
}

/*****************************************************************************/
/* Function Name      : IpdbUtilRBTreePortCtrlEntryCmp                       */
/*                                                                           */
/* Description        : This function is used for comparing two entries of   */
/*                      RBTree for port table entries                        */
/*                                                                           */
/* Input(s)           : pIpDbPortCtrlEntry1 - First Entry                    */
/*                      pIpDbPortCtrlEntry2 - second Entry                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when entry 1 is Less/Greater than entry 2    */
/*                      0    -> when entries elements are Equal              */
/*****************************************************************************/
INT4
IpdbUtilRBTreePortCtrlEntryCmp (tRBElem * pIpDbPortCtrlEntry1,
                                tRBElem * pIpDbPortCtrlEntry2)
{
    if ((((tPortCtrlEntry *) pIpDbPortCtrlEntry1)->u4IfIndex) <
        (((tPortCtrlEntry *) pIpDbPortCtrlEntry2)->u4IfIndex))
    {
        return IPDB_MINUS_ONE;
    }
    if ((((tPortCtrlEntry *) pIpDbPortCtrlEntry1)->u4IfIndex) >
        (((tPortCtrlEntry *) pIpDbPortCtrlEntry2)->u4IfIndex))
    {
        return IPDB_ONE;
    }
    return IPDB_ZERO;
}

/*****************************************************************************/
/* Function Name      : IpdbUtilRbTreeStaticFree                             */
/*                                                                           */
/* Description        : This routine is used for releasing the Memory        */
/*                      allocated for RbTree Node for Static binding.        */
/*                                                                           */
/* Input(s)           : pIpDbStaticBindingEntry - Pointer to Static binding  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IpdbUtilRbTreeStaticFree (tIpDbStaticBindingEntry * pIpDbStaticBindingEntry)
{
    if (pIpDbStaticBindingEntry != NULL)
    {
        IPDB_RELEASE_MEM_BLOCK (IPDB_STATIC_MEMPOOL_ID,
                                pIpDbStaticBindingEntry);
    }
}

/*****************************************************************************/
/* Function Name      : IpdbUtilMacRbTreeBindingFree                         */
/*                                                                           */
/* Description        : This routine is used for releasing the Memory        */
/*                      allocated for RbTree Node for binding.               */
/*                                                                           */
/* Input(s)           : pIpDbBindingEntry - Pointer to binding entry         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IpdbUtilMacRbTreeBindingFree (tIpDbBindingEntry * pIpDbBindingEntry)
{
    tIpDbGatewayEntry  *pIpDbGatewayEntry = NULL;

    if (pIpDbBindingEntry != NULL)
    {
        /* Delete all the Gateway entries */
        while ((pIpDbGatewayEntry = (tIpDbGatewayEntry *)
                TMO_SLL_Get (&(pIpDbBindingEntry->NetworkList))) != NULL)
        {
            TMO_SLL_Delete (&(pIpDbBindingEntry->NetworkList),
                            &(pIpDbGatewayEntry->GatewayNode));

            IPDB_RELEASE_MEM_BLOCK (IPDB_GW_POOL_ID, pIpDbGatewayEntry);
        }
        IPDB_RELEASE_MEM_BLOCK (IPDB_BINDING_MEMPOOL_ID, pIpDbBindingEntry);
    }
}

/*****************************************************************************/
/* Function Name      : IpdbUtilIpRbTreeBindingFree                          */
/*                                                                           */
/* Description        : This routine is used for releasing the Memory        */
/*                      allocated for RbTree Node for binding.               */
/*                                                                           */
/* Input(s)           : pIpDbBindingEntry - Pointer to binding entry         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IpdbUtilIpRbTreeBindingFree (tIpDbBindingEntry * pIpDbBindingEntry)
{
    /* Nothing to do here, as this is just a replica of Mac based RBTree */
    UNUSED_PARAM (pIpDbBindingEntry);
    return;
}

/*****************************************************************************/
/* Function Name      : IpdbUtilRbTreeIntfFree                               */
/*                                                                           */
/* Description        : This routine is used for releasing the Memory        */
/*                      allocated for RbTree Node for interface entry        */
/*                                                                           */
/* Input(s)           : pIpDbIfaceEntry - Pointer to interface entry         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IpdbUtilRbTreeIntfFree (tIpDbIfaceEntry * pIpDbIfaceEntry)
{
    if (pIpDbIfaceEntry != NULL)
    {
        IPDB_RELEASE_MEM_BLOCK (IPDB_INTF_POOL_ID, pIpDbIfaceEntry);
    }
}

/*****************************************************************************/
/* Function Name      : IpdbUtilRbTreePortCtrlFree                           */
/*                                                                           */
/* Description        : This routine is used for releasing the Memory        */
/*                      allocated for RbTree Node for port control entry     */
/*                                                                           */
/* Input(s)           : pIpDbIfaceEntry - Pointer to interface entry         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IpdbUtilRbTreePortCtrlFree (tPortCtrlEntry * pPortCtrlEntry)
{
    if (pPortCtrlEntry != NULL)
    {
        IPDB_RELEASE_MEM_BLOCK (IPDB_PORTCTRL_POOL_ID, pPortCtrlEntry);
    }
}

/*****************************************************************************/
/* Function Name      : IpdbUtilDuplicateBuffer                              */
/*                                                                           */
/* Description        : This routine is used to duplicate the given CRU      */
/*                      buffer                                               */
/*                                                                           */
/* Input(s)           : pSrcBuf - CRU Buffer to be duplicated                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Pointer to the duplicated buffer                     */
/*****************************************************************************/
tCRU_BUF_CHAIN_HEADER *
IpdbUtilDuplicateBuf (tCRU_BUF_CHAIN_HEADER * pSrcBuf)
{
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    UINT4               u4PktSize = IPDB_ZERO;

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pSrcBuf);

    pDupBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, IPDB_ZERO);

    if (pDupBuf == NULL)
    {
        return NULL;
    }

    MEMSET (gau1IpdbDataBuffer, IPDB_ZERO, u4PktSize);

    CRU_BUF_Copy_FromBufChain (pSrcBuf, (UINT1 *) gau1IpdbDataBuffer,
                               IPDB_ZERO, u4PktSize);
    CRU_BUF_Copy_OverBufChain (pDupBuf, (UINT1 *) gau1IpdbDataBuffer,
                               IPDB_ZERO, u4PktSize);

    return pDupBuf;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IpdbUtilIsIPDBEntryPresent                       */
/*                                                                           */
/*    Description         : This function is used to find whether IPDB entry */
/*                          is present or not for the incoming values        */
/*                                                                           */
/*    Input(s)            : u4ContextID - Context Identifier                 */
/*                          VlanId      - VLAN ID                            */
/*                          HostMac     - Host MAC address                   */
/*                          u4HostIp    - Host IP address                    */
/*                          u4InPort    - The port through which packet came */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : IPDB_TRUE / IPDB_FALSE                           */
/*                                                                           */
/*****************************************************************************/
INT4
IpdbUtilIsIPDBEntryPresent (UINT4 u4ContextID, tVlanId VlanId,
                            tMacAddr HostMac, UINT4 u4HostIp, UINT4 u4InPort)
{
    tRBElem            *pRBElem = NULL;
    tIpDbBindingEntry  *pIpDbBindingEntry = NULL;
    tIpDbBindingEntry   IpDbBindingEntry;

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_ENTRY, IPDB_MODULE_NAME,
                   "IpdbApiIsIPDBEntryPresent: Entering the function %s \r\n",
                   __FUNCTION__);

    /* This API should have IPDB_LOCK.
     * Intentionally not used for the following reasons:
     * Since the common DB shared by IPDB, L2DS, VLAN is not present in the
     * L2IWF module, there is a possibility for deadlock between IPDB and
     * VLAN module. Revisit required, while doing the enhancement
     * (Moving IPDB Database to L2IWF).
     */
    MEMSET (&IpDbBindingEntry, IPDB_ZERO, sizeof (tIpDbBindingEntry));

    IpDbBindingEntry.u4ContextId = u4ContextID;
    IpDbBindingEntry.VlanId = VlanId;
    MEMCPY (IpDbBindingEntry.HostMac, HostMac, sizeof (tMacAddr));

    if ((pRBElem = RBTreeGet (IPDB_MAC_BINDING_RBTREE,
                              (tRBElem *) & IpDbBindingEntry)) == NULL)
    {
        IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                       "IpdbApiIsIPDBEntryPresent: Entry is not present. "
                       "Exiting the function %s \r\n", __FUNCTION__);
        return IPDB_FALSE;
    }

    pIpDbBindingEntry = (tIpDbBindingEntry *) pRBElem;

    if ((u4InPort != IPDB_DB_INTF_INDEX (pIpDbBindingEntry)) ||
        (u4HostIp != IPDB_DB_HOST_IP (pIpDbBindingEntry)))
    {
        IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                       "IpdbApiIsIPDBEntryPresent: Entry does not matches "
                       "Port-no or Host-IP-Address.Exiting function %s \r\n",
                       __FUNCTION__);
        return IPDB_FALSE;
    }

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "IpdbApiIsIPDBEntryPresent: Entry is present. "
                   "Exiting the function %s \r\n", __FUNCTION__);

    return IPDB_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IpDbUtilCreatePortCtrlEntry                      */
/*                                                                           */
/*    Description         : To create a port control entry.                  */
/*                                                                           */
/*    Input(s)            : u4IfIndex -  Interface index                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : IPDB_SUCCESS / IPDB_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
IpDbUtilCreatePortCtrlEntry (UINT4 u4IfIndex)
{
    tPortCtrlEntry     *pPortCtrlEntry = NULL;

    IpDbGetPortCtrlEntry (u4IfIndex, &pPortCtrlEntry);

    if (pPortCtrlEntry != NULL)
    {
        return IPDB_FAILURE;
    }

    if ((pPortCtrlEntry = (tPortCtrlEntry *)
         IPDB_ALLOC_MEM_BLOCK (IPDB_PORTCTRL_POOL_ID)) == NULL)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Memory allocation for pPortCtrlEntry failed.\r\n");
        return IPDB_FAILURE;
    }

    MEMSET (pPortCtrlEntry, IPDB_ZERO, sizeof (tPortCtrlEntry));

    pPortCtrlEntry->u4IfIndex = u4IfIndex;
    pPortCtrlEntry->u1DownstreamArpBcastStatus = IPDB_ARP_ALLOW;
    pPortCtrlEntry->u1IpsgStatus = IPDB_IPSG_DISABLE;

    /* Add the entry to RBTree */
    if (RBTreeAdd (IPDB_PORTCTRL_RBTREE, pPortCtrlEntry) != RB_SUCCESS)
    {
        IPDB_RELEASE_MEM_BLOCK (IPDB_PORTCTRL_POOL_ID, pPortCtrlEntry);

        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Failed to add port control entry to RBTree\r\n");
        return IPDB_FAILURE;
    }
    return IPDB_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IpDbUtilDeletePortCtrlEntry                      */
/*                                                                           */
/*    Description         : To delete a port control entry.                  */
/*                                                                           */
/*    Input(s)            : u4IfIndex -  Interface index                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : IPDB_SUCCESS / IPDB_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
IpDbUtilDeletePortCtrlEntry (UINT4 u4IfIndex)
{
    tPortCtrlEntry     *pPortCtrlEntry = NULL;

    IpDbGetPortCtrlEntry (u4IfIndex, &pPortCtrlEntry);

    if (pPortCtrlEntry == NULL)
    {
        return IPDB_FAILURE;
    }

    /* Remove the entry from RBTree */
    RBTreeRemove (IPDB_PORTCTRL_RBTREE, pPortCtrlEntry);

    IPDB_RELEASE_MEM_BLOCK (IPDB_PORTCTRL_POOL_ID, pPortCtrlEntry);

    return IPDB_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IpDbUtilSetContext                                         */
/*                                                                           */
/* Description  : This function sets the current IPDB context ID with        */
/*                the given u4CxtId.                                         */
/*                                                                           */
/* Input        : u4CxtId     - Context Id                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IPDB_SUCCESS / IPDB_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IpDbUtilSetContext (UINT4 u4CxtId)
{
    if (IpDbUtilGetVcmSystemMode (IPDB_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if ((IpDbUtilVcmIsVcExist (u4CxtId) == IPDB_FALSE))
        {
            return IPDB_FAILURE;
        }

        IPDB_CURR_CONTEXT_ID = u4CxtId;
    }
    return IPDB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IpDbUtilResetContext                                       */
/*                                                                           */
/* Description  : This function resets the current IPDB context ID           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IpDbUtilResetContext ()
{
    if (IpDbUtilGetVcmSystemMode (IPDB_PROTOCOL_ID) == VCM_MI_MODE)
    {
        IPDB_CURR_CONTEXT_ID = IPDB_INVALID_CXT_ID;
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpDbUtilGetVcmSystemMode                           */
/*                                                                           */
/*     DESCRIPTION      : This function calls the VCM Module to get the      */
/*                        mode of the system (SI / MI).                      */
/*                                                                           */
/*     INPUT            : u2ProtocolId - Protocol Identifier                 */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VCM_MI_MODE / VCM_SI_MODE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IpDbUtilGetVcmSystemMode (UINT2 u2ProtocolId)
{
    return (VcmGetSystemMode (u2ProtocolId));
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpDbUtilGetVcmSystemModeExt                        */
/*                                                                           */
/*     DESCRIPTION      : This function calls the VCM Module to get the      */
/*                        mode of the system (SI / MI).                      */
/*                                                                           */
/*     INPUT            : u2ProtocolId - Protocol Identifier                 */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VCM_MI_MODE / VCM_SI_MODE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IpDbUtilGetVcmSystemModeExt (UINT2 u2ProtocolId)
{
    return (VcmGetSystemModeExt (u2ProtocolId));
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpDbUtilVcmIsVcExist                               */
/*                                                                           */
/*     DESCRIPTION      : This function calls the VCM Module to check        */
/*                        whether the given context is present or not.       */
/*                                                                           */
/*     INPUT            : u4CxtId - Context Identifier                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : IPDB_TRUE / IPDB_FALSE                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IpDbUtilVcmIsVcExist (UINT4 u4CxtId)
{
    if (IpDbUtilGetVcmSystemMode (IPDB_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (VcmIsL2VcExist (u4CxtId) == VCM_FALSE)
        {
            return IPDB_FALSE;
        }
    }
    else
    {
        if (u4CxtId != IPDB_DEFAULT_CONTEXT)
        {
            return IPDB_FALSE;
        }
    }
    return IPDB_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpDbUtilGetNextActiveCtxt                          */
/*                                                                           */
/*     DESCRIPTION      : This function calls the VCM Module to return       */
/*                        next active context of a particular context ID.    */
/*                                                                           */
/*     INPUT            : u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : pu4NextContextId - Next Context Identifier         */
/*                                                                           */
/*     RETURNS          : IPDB_SUCCESS / IPDB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IpDbUtilGetNextActiveCtxt (UINT4 u4ContextId, UINT4 *pu4NextContextId)
{
    if (VcmGetNextActiveL2Context (u4ContextId, pu4NextContextId)
        == VCM_FAILURE)
    {
        return IPDB_FAILURE;
    }
    return IPDB_SUCCESS;
}

/*****************************************************************************
 * FUNCTION NAME    : IpDbVcmIsSwitchExist
 *
 * DESCRIPTION      : Routine used to get the context Id for the Alias Name
 *
 * INPUT            : pu1Alias - Context Name
 *
 * OUTPUT           : pu4VcNum - Context Identifier
 *
 * RETURNS          : NONE
 *
 * ***************************************************************************/

PUBLIC INT4
IpDbVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4VcNum)
{
    return (VcmIsSwitchExist (pu1Alias, pu4VcNum));
}

/*                                                                           */
/***************************** END OF FILE ***********************************/
/*                                                                           */
