/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssystlw.c,v 1.6 2014/02/24 11:40:26 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "ipdbinc.h"
# include  "ipdbextn.h"

/* LOW LEVEL Routines for Table : FsSystemPortCtrlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsSystemPortCtrlTable
 Input       :  The Indices
                FsSystemPortCtrlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsSystemPortCtrlTable (INT4 i4FsSystemPortCtrlIndex)
{
    tPortCtrlEntry     *pPortCtrlEntry = NULL;

    if (IPDB_IS_PORT_VALID (i4FsSystemPortCtrlIndex) == IPDB_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (IpDbGetPortCtrlEntry ((UINT4) i4FsSystemPortCtrlIndex, &pPortCtrlEntry)
        == IPDB_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsSystemPortCtrlTable
 Input       :  The Indices
                FsSystemPortCtrlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsSystemPortCtrlTable (INT4 *pi4FsSystemPortCtrlIndex)
{
    return (nmhGetNextIndexFsSystemPortCtrlTable (IPDB_ZERO,
                                                  pi4FsSystemPortCtrlIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsSystemPortCtrlTable
 Input       :  The Indices
                FsSystemPortCtrlIndex
                nextFsSystemPortCtrlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsSystemPortCtrlTable (INT4 i4FsSystemPortCtrlIndex,
                                      INT4 *pi4NextFsSystemPortCtrlIndex)
{
    tPortCtrlEntry     *pPortCtrlEntry = NULL;
    tPortCtrlEntry      PortCtrlEntry;

    MEMSET (&PortCtrlEntry, IPDB_ZERO, sizeof (tPortCtrlEntry));

    PortCtrlEntry.u4IfIndex = i4FsSystemPortCtrlIndex;

    pPortCtrlEntry =
        (tPortCtrlEntry *) RBTreeGetNext (IPDB_PORTCTRL_RBTREE,
                                          (tPortCtrlEntry *) & PortCtrlEntry,
                                          NULL);

    if (pPortCtrlEntry != NULL)
    {
        *pi4NextFsSystemPortCtrlIndex = pPortCtrlEntry->u4IfIndex;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSystemPortCtrlDownstreamArpBcast
 Input       :  The Indices
                FsSystemPortCtrlIndex

                The Object 
                retValFsSystemPortCtrlDownstreamArpBcast
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSystemPortCtrlDownstreamArpBcast (INT4 i4FsSystemPortCtrlIndex,
                                          INT4
                                          *pi4RetValFsSystemPortCtrlDownstreamArpBcast)
{
    tPortCtrlEntry     *pPortCtrlEntry = NULL;

    if (IPDB_IS_PORT_VALID (i4FsSystemPortCtrlIndex) == IPDB_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (IpDbGetPortCtrlEntry ((UINT4) i4FsSystemPortCtrlIndex, &pPortCtrlEntry)
        == IPDB_SUCCESS)
    {
        *pi4RetValFsSystemPortCtrlDownstreamArpBcast
            = pPortCtrlEntry->u1DownstreamArpBcastStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsSystemPortCtrlArpPktsForwarded
 Input       :  The Indices
                FsSystemPortCtrlIndex

                The Object 
                retValFsSystemPortCtrlArpPktsForwarded
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSystemPortCtrlArpPktsForwarded (INT4 i4FsSystemPortCtrlIndex,
                                        UINT4
                                        *pu4RetValFsSystemPortCtrlArpPktsForwarded)
{
    tPortCtrlEntry     *pPortCtrlEntry = NULL;

    if (IPDB_IS_PORT_VALID (i4FsSystemPortCtrlIndex) == IPDB_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (IpDbGetPortCtrlEntry ((UINT4) i4FsSystemPortCtrlIndex, &pPortCtrlEntry)
        == IPDB_SUCCESS)
    {
        *pu4RetValFsSystemPortCtrlArpPktsForwarded
            = pPortCtrlEntry->u4ArpPktsForwarded;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsSystemPortCtrlArpPktsDropped
 Input       :  The Indices
                FsSystemPortCtrlIndex

                The Object 
                retValFsSystemPortCtrlArpPktsDropped
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSystemPortCtrlArpPktsDropped (INT4 i4FsSystemPortCtrlIndex,
                                      UINT4
                                      *pu4RetValFsSystemPortCtrlArpPktsDropped)
{
    tPortCtrlEntry     *pPortCtrlEntry = NULL;

    if (IPDB_IS_PORT_VALID (i4FsSystemPortCtrlIndex) == IPDB_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (IpDbGetPortCtrlEntry ((UINT4) i4FsSystemPortCtrlIndex, &pPortCtrlEntry)
        == IPDB_SUCCESS)
    {
        *pu4RetValFsSystemPortCtrlArpPktsDropped
            = pPortCtrlEntry->u4ArpPktsDropped;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSystemPortCtrlDownstreamArpBcast
 Input       :  The Indices
                FsSystemPortCtrlIndex

                The Object 
                setValFsSystemPortCtrlDownstreamArpBcast
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSystemPortCtrlDownstreamArpBcast (INT4 i4FsSystemPortCtrlIndex,
                                          INT4
                                          i4SetValFsSystemPortCtrlDownstreamArpBcast)
{
    tPortCtrlEntry     *pPortCtrlEntry = NULL;

    if (IpDbGetPortCtrlEntry ((UINT4) i4FsSystemPortCtrlIndex, &pPortCtrlEntry)
        == IPDB_SUCCESS)
    {
        pPortCtrlEntry->u1DownstreamArpBcastStatus
            = (UINT1) i4SetValFsSystemPortCtrlDownstreamArpBcast;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSystemPortCtrlDownstreamArpBcast
 Input       :  The Indices
                FsSystemPortCtrlIndex

                The Object 
                testValFsSystemPortCtrlDownstreamArpBcast
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSystemPortCtrlDownstreamArpBcast (UINT4 *pu4ErrorCode,
                                             INT4 i4FsSystemPortCtrlIndex,
                                             INT4
                                             i4TestValFsSystemPortCtrlDownstreamArpBcast)
{
    tPortCtrlEntry     *pPortCtrlEntry = NULL;

    if (IPDB_IS_PORT_VALID (i4FsSystemPortCtrlIndex) == IPDB_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IPDB_INVALID_PORT_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsSystemPortCtrlDownstreamArpBcast != IPDB_ARP_ALLOW) &&
        (i4TestValFsSystemPortCtrlDownstreamArpBcast != IPDB_ARP_DROP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IPDB_INVALID_VALUE_ERR);
        return SNMP_FAILURE;
    }

    /* If the Rowstatus of the entry is Active, Entry cannot be modified */
    if (IpDbGetPortCtrlEntry ((UINT4) i4FsSystemPortCtrlIndex, &pPortCtrlEntry)
        != IPDB_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IPDB_NO_ENTRY_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSystemPortCtrlTable
 Input       :  The Indices
                FsSystemPortCtrlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSystemPortCtrlTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsSystemVlanCtrlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsSystemVlanCtrlTable
 Input       :  The Indices
                FsSystemVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsSystemVlanCtrlTable (INT4 i4FsSystemVlanIndex)
{
    tVlanCtrlEntry     *pVlanCtrlEntry = NULL;

    if ((IPDB_IS_VLAN_VALID (i4FsSystemVlanIndex)) == IPDB_FALSE)
    {
        return SNMP_FAILURE;
    }

    if ((IpDbGetVlanCtrlEntry ((tVlanId) i4FsSystemVlanIndex, &pVlanCtrlEntry))
        != IPDB_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsSystemVlanCtrlTable
 Input       :  The Indices
                FsSystemVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsSystemVlanCtrlTable (INT4 *pi4FsSystemVlanIndex)
{
    return (nmhGetNextIndexFsSystemVlanCtrlTable (IPDB_ZERO,
                                                  pi4FsSystemVlanIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsSystemVlanCtrlTable
 Input       :  The Indices
                FsSystemVlanIndex
                nextFsSystemVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsSystemVlanCtrlTable (INT4 i4FsSystemVlanIndex,
                                      INT4 *pi4NextFsSystemVlanIndex)
{
    tVlanCtrlEntry     *pVlanCtrlEntry = NULL;
    UINT4               u4VlanId = IPDB_ZERO;

    for (u4VlanId = (UINT4) (i4FsSystemVlanIndex + IPDB_ONE);
         u4VlanId <= VLAN_DEV_MAX_VLAN_ID; u4VlanId++)
    {
        if ((IpDbGetVlanCtrlEntry ((tVlanId) u4VlanId, &pVlanCtrlEntry))
            == IPDB_SUCCESS)
        {
            *pi4NextFsSystemVlanIndex = (INT4) u4VlanId;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSystemVlanMacForceFwdStatus
 Input       :  The Indices
                FsSystemVlanIndex

                The Object 
                retValFsSystemVlanMacForceFwdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSystemVlanMacForceFwdStatus (INT4 i4FsSystemVlanIndex,
                                     INT4
                                     *pi4RetValFsSystemVlanMacForceFwdStatus)
{
    tVlanCtrlEntry     *pVlanCtrlEntry = NULL;

    if ((IPDB_IS_VLAN_VALID (i4FsSystemVlanIndex)) == IPDB_FALSE)
    {
        return SNMP_FAILURE;
    }

    if ((IpDbGetVlanCtrlEntry ((tVlanId) i4FsSystemVlanIndex, &pVlanCtrlEntry))
        == IPDB_SUCCESS)
    {
        *pi4RetValFsSystemVlanMacForceFwdStatus =
            (INT4) pVlanCtrlEntry->u1MacForceFwdStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsSystemVlanRowStatus
 Input       :  The Indices
                FsSystemVlanIndex

                The Object 
                retValFsSystemVlanRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSystemVlanRowStatus (INT4 i4FsSystemVlanIndex,
                             INT4 *pi4RetValFsSystemVlanRowStatus)
{
    tVlanCtrlEntry     *pVlanCtrlEntry = NULL;

    if ((IPDB_IS_VLAN_VALID (i4FsSystemVlanIndex)) == IPDB_FALSE)
    {
        return SNMP_FAILURE;
    }

    if ((IpDbGetVlanCtrlEntry ((tVlanId) i4FsSystemVlanIndex, &pVlanCtrlEntry))
        == IPDB_SUCCESS)
    {
        *pi4RetValFsSystemVlanRowStatus = (INT4) pVlanCtrlEntry->u1RowStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSystemVlanMacForceFwdStatus
 Input       :  The Indices
                FsSystemVlanIndex

                The Object 
                setValFsSystemVlanMacForceFwdStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSystemVlanMacForceFwdStatus (INT4 i4FsSystemVlanIndex,
                                     INT4 i4SetValFsSystemVlanMacForceFwdStatus)
{
    tVlanCtrlEntry     *pVlanCtrlEntry = NULL;

    if ((IpDbGetVlanCtrlEntry ((tVlanId) i4FsSystemVlanIndex, &pVlanCtrlEntry))
        == IPDB_SUCCESS)
    {
        pVlanCtrlEntry->u1MacForceFwdStatus =
            (UINT1) i4SetValFsSystemVlanMacForceFwdStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsSystemVlanRowStatus
 Input       :  The Indices
                FsSystemVlanIndex

                The Object 
                setValFsSystemVlanRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSystemVlanRowStatus (INT4 i4FsSystemVlanIndex,
                             INT4 i4SetValFsSystemVlanRowStatus)
{
    tVlanCtrlEntry     *pVlanCtrlEntry = NULL;

    if ((IpDbGetVlanCtrlEntry ((tVlanId) i4FsSystemVlanIndex, &pVlanCtrlEntry))
        == IPDB_SUCCESS)
    {
        if (pVlanCtrlEntry->u1RowStatus ==
            (UINT4) i4SetValFsSystemVlanRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }

    switch (i4SetValFsSystemVlanRowStatus)
    {
        case IPDB_CREATE_AND_GO:
            /*Intentional fall through. IPDB_CREATE_AND_WAIT used for MSR. */
        case IPDB_CREATE_AND_WAIT:

            if (pVlanCtrlEntry != NULL)
            {
                return SNMP_FAILURE;
            }

            if ((pVlanCtrlEntry = (tVlanCtrlEntry *)
                 IPDB_ALLOC_MEM_BLOCK (IPDB_VLANCTRL_POOL_ID)) == NULL)
            {
                IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                          "Memory allocation for pVlanCtrlEntry failed.\r\n");
                return SNMP_FAILURE;
            }

            MEMSET (pVlanCtrlEntry, IPDB_ZERO, sizeof (tVlanCtrlEntry));
            TMO_DLL_Init_Node (&(pVlanCtrlEntry->VlanCtrlDllNode));

            pVlanCtrlEntry->VlanId = (tVlanId) i4FsSystemVlanIndex;
            pVlanCtrlEntry->u1MacForceFwdStatus = IPDB_DISABLED;

            TMO_DLL_Add (&(gIpDbGlobalInfo.VlanCtrlDllList),
                         &(pVlanCtrlEntry->VlanCtrlDllNode));
            if (i4SetValFsSystemVlanRowStatus == IPDB_CREATE_AND_GO)
            {
                pVlanCtrlEntry->u1RowStatus = IPDB_ACTIVE;
            }
            else
            {
                pVlanCtrlEntry->u1RowStatus = IPDB_NOT_IN_SERVICE;
            }
            break;

        case IPDB_ACTIVE:

            if (pVlanCtrlEntry == NULL)
            {
                return SNMP_FAILURE;
            }

            pVlanCtrlEntry->u1RowStatus = IPDB_ACTIVE;
            break;

        case IPDB_NOT_IN_SERVICE:

            if (pVlanCtrlEntry == NULL)
            {
                return SNMP_FAILURE;
            }

            pVlanCtrlEntry->u1RowStatus = IPDB_NOT_IN_SERVICE;
            break;

        case IPDB_DESTROY:

            if (pVlanCtrlEntry == NULL)
            {
                return SNMP_FAILURE;
            }

            TMO_DLL_Delete (&(gIpDbGlobalInfo.VlanCtrlDllList),
                            &(pVlanCtrlEntry->VlanCtrlDllNode));

            MemReleaseMemBlock (IPDB_PORTCTRL_POOL_ID,
                                (UINT1 *) pVlanCtrlEntry);

            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSystemVlanMacForceFwdStatus
 Input       :  The Indices
                FsSystemVlanIndex

                The Object 
                testValFsSystemVlanMacForceFwdStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSystemVlanMacForceFwdStatus (UINT4 *pu4ErrorCode,
                                        INT4 i4FsSystemVlanIndex,
                                        INT4
                                        i4TestValFsSystemVlanMacForceFwdStatus)
{
    tVlanCtrlEntry     *pVlanCtrlEntry = NULL;

    if ((IPDB_IS_VLAN_VALID (i4FsSystemVlanIndex)) == IPDB_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IPDB_INVALID_VALUE_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsSystemVlanMacForceFwdStatus != IPDB_ENABLED) &&
        (i4TestValFsSystemVlanMacForceFwdStatus != IPDB_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IPDB_INVALID_VALUE_ERR);
        return SNMP_FAILURE;
    }

    if ((IpDbGetVlanCtrlEntry ((tVlanId) i4FsSystemVlanIndex, &pVlanCtrlEntry))
        != IPDB_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IPDB_NO_ENTRY_ERR);
        return SNMP_FAILURE;
    }

    if (pVlanCtrlEntry->u1RowStatus == IPDB_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSystemVlanRowStatus
 Input       :  The Indices
                FsSystemVlanIndex

                The Object 
                testValFsSystemVlanRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSystemVlanRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsSystemVlanIndex,
                                INT4 i4TestValFsSystemVlanRowStatus)
{
    tVlanCtrlEntry     *pVlanCtrlEntry = NULL;

    if ((IPDB_IS_VLAN_VALID (i4FsSystemVlanIndex)) == IPDB_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IPDB_INVALID_VALUE_ERR);
        return SNMP_FAILURE;
    }

    if ((L2IwfIsVlanActive ((tVlanId) i4FsSystemVlanIndex)) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IPDB_VLAN_NOT_ACTIVE_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsSystemVlanRowStatus < IPDB_ACTIVE) ||
        (i4TestValFsSystemVlanRowStatus > IPDB_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IPDB_INVALID_VALUE_ERR);
        return SNMP_FAILURE;
    }

    IpDbGetVlanCtrlEntry ((tVlanId) i4FsSystemVlanIndex, &pVlanCtrlEntry);

    switch (i4TestValFsSystemVlanRowStatus)
    {
        case IPDB_CREATE_AND_GO:
            /*Intentional fall through. IPDB_CREATE_AND_WAIT used for MSR. */
        case IPDB_CREATE_AND_WAIT:

            if (pVlanCtrlEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case IPDB_NOT_IN_SERVICE:

            if (pVlanCtrlEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_IPDB_NO_ENTRY_ERR);
                return SNMP_FAILURE;
            }
            if (pVlanCtrlEntry->u1RowStatus == IPDB_NOT_READY)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case IPDB_DESTROY:

            if (pVlanCtrlEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_IPDB_NO_ENTRY_ERR);
                return SNMP_FAILURE;
            }
            break;

        case IPDB_ACTIVE:

            if (pVlanCtrlEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_IPDB_NO_ENTRY_ERR);
                return SNMP_FAILURE;
            }
            if (pVlanCtrlEntry->u1RowStatus == IPDB_NOT_READY)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSystemVlanCtrlTable
 Input       :  The Indices
                FsSystemVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSystemVlanCtrlTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
