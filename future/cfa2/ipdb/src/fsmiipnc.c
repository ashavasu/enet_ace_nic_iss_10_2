/* $Id: fsmiipnc.c,v 1.1 2016/06/18 11:43:28 siva Exp $
    ISS Wrapper module
    module ARICENT-MIIPDB-MIB

 */
# include  "fssnmp.h"
# include  "fsmiipnc.h"
# include  "ipdbinc.h"

/********************************************************************
* FUNCTION NcFsMIIpDbNoOfBindingsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbNoOfBindingsGet (
                UINT4 *pu4FsMIIpDbNoOfBindings )
{

    INT1 i1RetVal;
    i1RetVal = nmhGetFsMIIpDbNoOfBindings(
                 pu4FsMIIpDbNoOfBindings );

    return i1RetVal;


} /* NcFsMIIpDbNoOfBindingsGet */

/********************************************************************
* FUNCTION NcFsMIIpDbNoOfStaticBindingsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbNoOfStaticBindingsGet (
                UINT4 *pu4FsMIIpDbNoOfStaticBindings )
{

    INT1 i1RetVal;
    i1RetVal = nmhGetFsMIIpDbNoOfStaticBindings(
                 pu4FsMIIpDbNoOfStaticBindings );

    return i1RetVal;


} /* NcFsMIIpDbNoOfStaticBindingsGet */

/********************************************************************
* FUNCTION NcFsMIIpDbNoOfDHCPBindingsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbNoOfDHCPBindingsGet (
                UINT4 *pu4FsMIIpDbNoOfDHCPBindings )
{

    INT1 i1RetVal;
    i1RetVal = nmhGetFsMIIpDbNoOfDHCPBindings(
                 pu4FsMIIpDbNoOfDHCPBindings );

    return i1RetVal;


} /* NcFsMIIpDbNoOfDHCPBindingsGet */

/********************************************************************
* FUNCTION NcFsMIIpDbNoOfPPPBindingsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbNoOfPPPBindingsGet (
                UINT4 *pu4FsMIIpDbNoOfPPPBindings )
{

    INT1 i1RetVal;
    i1RetVal = nmhGetFsMIIpDbNoOfPPPBindings(
                 pu4FsMIIpDbNoOfPPPBindings );

    return i1RetVal;


} /* NcFsMIIpDbNoOfPPPBindingsGet */

/********************************************************************
* FUNCTION NcFsMIIpDbTraceLevelSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbTraceLevelSet (
                INT4 i4FsMIIpDbTraceLevel )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetFsMIIpDbTraceLevel(
                i4FsMIIpDbTraceLevel);

    return i1RetVal;


} /* NcFsMIIpDbTraceLevelSet */

/********************************************************************
* FUNCTION NcFsMIIpDbTraceLevelTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbTraceLevelTest (UINT4 *pu4Error,
                INT4 i4FsMIIpDbTraceLevel )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2FsMIIpDbTraceLevel(pu4Error,
                i4FsMIIpDbTraceLevel);

    return i1RetVal;


} /* NcFsMIIpDbTraceLevelTest */

/********************************************************************
* FUNCTION NcFsMIIpDbStaticHostIpSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbStaticHostIpSet (
                INT4 i4FsMIIpDbContextId,
                INT4 i4FsMIIpDbStaticHostVlanId,
                UINT1 *pFsMIIpDbStaticHostMac,
                UINT4 pFsMIIpDbStaticHostIp )
{

    INT1 i1RetVal;
    /*VXLAN_INET_HTONL(pFsMIIpDbStaticHostIp);*/
    i1RetVal = nmhSetFsMIIpDbStaticHostIp(
                 i4FsMIIpDbContextId,
                 i4FsMIIpDbStaticHostVlanId,
                 pFsMIIpDbStaticHostMac,
                pFsMIIpDbStaticHostIp);

    return i1RetVal;


} /* NcFsMIIpDbStaticHostIpSet */

/********************************************************************
* FUNCTION NcFsMIIpDbStaticHostIpTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbStaticHostIpTest (UINT4 *pu4Error,
                INT4 i4FsMIIpDbContextId,
                INT4 i4FsMIIpDbStaticHostVlanId,
                UINT1 *pFsMIIpDbStaticHostMac,
                UINT4 pFsMIIpDbStaticHostIp )
{

    INT1 i1RetVal;
    /*VXLAN_INET_HTONL(pFsMIIpDbStaticHostIp);*/
    i1RetVal = nmhTestv2FsMIIpDbStaticHostIp(pu4Error,
                 i4FsMIIpDbContextId,
                 i4FsMIIpDbStaticHostVlanId,
                 pFsMIIpDbStaticHostMac,
                pFsMIIpDbStaticHostIp);

    return i1RetVal;


} /* NcFsMIIpDbStaticHostIpTest */

/********************************************************************
* FUNCTION NcFsMIIpDbStaticInIfIndexSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbStaticInIfIndexSet (
                INT4 i4FsMIIpDbContextId,
                INT4 i4FsMIIpDbStaticHostVlanId,
                UINT1 *pFsMIIpDbStaticHostMac,
                INT4 i4FsMIIpDbStaticInIfIndex )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetFsMIIpDbStaticInIfIndex(
                 i4FsMIIpDbContextId,
                 i4FsMIIpDbStaticHostVlanId,
                 pFsMIIpDbStaticHostMac,
                i4FsMIIpDbStaticInIfIndex);

    return i1RetVal;


} /* NcFsMIIpDbStaticInIfIndexSet */

/********************************************************************
* FUNCTION NcFsMIIpDbStaticInIfIndexTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbStaticInIfIndexTest (UINT4 *pu4Error,
                INT4 i4FsMIIpDbContextId,
                INT4 i4FsMIIpDbStaticHostVlanId,
                UINT1 *pFsMIIpDbStaticHostMac,
                INT4 i4FsMIIpDbStaticInIfIndex )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2FsMIIpDbStaticInIfIndex(pu4Error,
                 i4FsMIIpDbContextId,
                 i4FsMIIpDbStaticHostVlanId,
                 pFsMIIpDbStaticHostMac,
                i4FsMIIpDbStaticInIfIndex);

    return i1RetVal;


} /* NcFsMIIpDbStaticInIfIndexTest */

/********************************************************************
* FUNCTION NcFsMIIpDbStaticGatewaySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbStaticGatewaySet (
                INT4 i4FsMIIpDbContextId,
                INT4 i4FsMIIpDbStaticHostVlanId,
                UINT1 *pFsMIIpDbStaticHostMac,
                UINT4 pFsMIIpDbStaticGateway )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetFsMIIpDbStaticGateway(
                 i4FsMIIpDbContextId,
                 i4FsMIIpDbStaticHostVlanId,
                 pFsMIIpDbStaticHostMac,
                pFsMIIpDbStaticGateway);

    return i1RetVal;


} /* NcFsMIIpDbStaticGatewaySet */

/********************************************************************
* FUNCTION NcFsMIIpDbStaticGatewayTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbStaticGatewayTest (UINT4 *pu4Error,
                INT4 i4FsMIIpDbContextId,
                INT4 i4FsMIIpDbStaticHostVlanId,
                UINT1 *pFsMIIpDbStaticHostMac,
                UINT4 pFsMIIpDbStaticGateway )
{

    INT1 i1RetVal;
    /*VXLAN_INET_HTONL(pFsMIIpDbStaticGateway);*/
    i1RetVal = nmhTestv2FsMIIpDbStaticGateway(pu4Error,
                 i4FsMIIpDbContextId,
                 i4FsMIIpDbStaticHostVlanId,
                 pFsMIIpDbStaticHostMac,
                pFsMIIpDbStaticGateway);

    return i1RetVal;


} /* NcFsMIIpDbStaticGatewayTest */

/********************************************************************
* FUNCTION NcFsMIIpDbStaticBindingStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbStaticBindingStatusSet (
                INT4 i4FsMIIpDbContextId,
                INT4 i4FsMIIpDbStaticHostVlanId,
                UINT1 *pFsMIIpDbStaticHostMac,
                INT4 i4FsMIIpDbStaticBindingStatus )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetFsMIIpDbStaticBindingStatus(
                 i4FsMIIpDbContextId,
                 i4FsMIIpDbStaticHostVlanId,
                 pFsMIIpDbStaticHostMac,
                i4FsMIIpDbStaticBindingStatus);

    return i1RetVal;


} /* NcFsMIIpDbStaticBindingStatusSet */

/********************************************************************
* FUNCTION NcFsMIIpDbStaticBindingStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbStaticBindingStatusTest (UINT4 *pu4Error,
                INT4 i4FsMIIpDbContextId,
                INT4 i4FsMIIpDbStaticHostVlanId,
                UINT1 *pFsMIIpDbStaticHostMac,
                INT4 i4FsMIIpDbStaticBindingStatus )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2FsMIIpDbStaticBindingStatus(pu4Error,
                 i4FsMIIpDbContextId,
                 i4FsMIIpDbStaticHostVlanId,
                 pFsMIIpDbStaticHostMac,
                i4FsMIIpDbStaticBindingStatus);

    return i1RetVal;


} /* NcFsMIIpDbStaticBindingStatusTest */

/********************************************************************
* FUNCTION NcFsMIIpDbHostBindingTypeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbHostBindingTypeGet (
                INT4 i4FsMIIpDbHostContextId,
                INT4 i4FsMIIpDbHostVlanId,
                UINT1 *pFsMIIpDbHostMac,
                INT4 *pi4FsMIIpDbHostBindingType )
{

    INT1 i1RetVal;
    tMacAddr FsMIIpDbHostMac;
    IpdbPortMacToStr (pFsMIIpDbHostMac,FsMIIpDbHostMac);

    if (nmhValidateIndexInstanceFsMIIpDbBindingTable(
                 i4FsMIIpDbHostContextId,
                 i4FsMIIpDbHostVlanId,
                 FsMIIpDbHostMac) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMIIpDbHostBindingType(
                 i4FsMIIpDbHostContextId,
                 i4FsMIIpDbHostVlanId,
                 FsMIIpDbHostMac,
                 pi4FsMIIpDbHostBindingType );

    return i1RetVal;


} /* NcFsMIIpDbHostBindingTypeGet */

/********************************************************************
* FUNCTION NcFsMIIpDbHostIpGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbHostIpGet (
                INT4 i4FsMIIpDbHostContextId,
                INT4 i4FsMIIpDbHostVlanId,
                UINT1 *pFsMIIpDbHostMac,
                UINT4 *pFsMIIpDbHostIp )
{

    INT1 i1RetVal;
    tMacAddr FsMIIpDbHostMac;
    IpdbPortMacToStr (pFsMIIpDbHostMac,FsMIIpDbHostMac); 

    if (nmhValidateIndexInstanceFsMIIpDbBindingTable(
                 i4FsMIIpDbHostContextId,
                 i4FsMIIpDbHostVlanId,
                 FsMIIpDbHostMac) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMIIpDbHostIp(
                 i4FsMIIpDbHostContextId,
                 i4FsMIIpDbHostVlanId,
                 FsMIIpDbHostMac,
                 pFsMIIpDbHostIp );

    return i1RetVal;


} /* NcFsMIIpDbHostIpGet */

/********************************************************************
* FUNCTION NcFsMIIpDbHostInIfIndexGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbHostInIfIndexGet (
                INT4 i4FsMIIpDbHostContextId,
                INT4 i4FsMIIpDbHostVlanId,
                UINT1 *pFsMIIpDbHostMac,
                INT4 *pi4FsMIIpDbHostInIfIndex )
{

    INT1 i1RetVal;
    tMacAddr FsMIIpDbHostMac;
    IpdbPortMacToStr (pFsMIIpDbHostMac,FsMIIpDbHostMac);

    if (nmhValidateIndexInstanceFsMIIpDbBindingTable(
                 i4FsMIIpDbHostContextId,
                 i4FsMIIpDbHostVlanId,
                 FsMIIpDbHostMac) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMIIpDbHostInIfIndex(
                 i4FsMIIpDbHostContextId,
                 i4FsMIIpDbHostVlanId,
                 FsMIIpDbHostMac,
                 pi4FsMIIpDbHostInIfIndex );

    return i1RetVal;


} /* NcFsMIIpDbHostInIfIndexGet */

/********************************************************************
* FUNCTION NcFsMIIpDbHostRemLeaseTimeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbHostRemLeaseTimeGet (
                INT4 i4FsMIIpDbHostContextId,
                INT4 i4FsMIIpDbHostVlanId,
                UINT1 *pFsMIIpDbHostMac,
                INT4 *pi4FsMIIpDbHostRemLeaseTime )
{

    INT1 i1RetVal;
    tMacAddr FsMIIpDbHostMac;
    IpdbPortMacToStr (pFsMIIpDbHostMac,FsMIIpDbHostMac);

    if (nmhValidateIndexInstanceFsMIIpDbBindingTable(
                 i4FsMIIpDbHostContextId,
                 i4FsMIIpDbHostVlanId,
                 FsMIIpDbHostMac) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMIIpDbHostRemLeaseTime(
                 i4FsMIIpDbHostContextId,
                 i4FsMIIpDbHostVlanId,
                 FsMIIpDbHostMac,
                 pi4FsMIIpDbHostRemLeaseTime );

    return i1RetVal;


} /* NcFsMIIpDbHostRemLeaseTimeGet */

/********************************************************************
* FUNCTION NcFsMIIpDbHostBindingIDGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbHostBindingIDGet (
                INT4 i4FsMIIpDbHostContextId,
                INT4 i4FsMIIpDbHostVlanId,
                UINT1 *pFsMIIpDbHostMac,
                UINT4 *pu4FsMIIpDbHostBindingID )
{

    INT1 i1RetVal;
    tMacAddr FsMIIpDbHostMac;
    IpdbPortMacToStr (pFsMIIpDbHostMac,FsMIIpDbHostMac);

    if (nmhValidateIndexInstanceFsMIIpDbBindingTable(
                 i4FsMIIpDbHostContextId,
                 i4FsMIIpDbHostVlanId,
                 FsMIIpDbHostMac) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMIIpDbHostBindingID(
                 i4FsMIIpDbHostContextId,
                 i4FsMIIpDbHostVlanId,
                 FsMIIpDbHostMac,
                 pu4FsMIIpDbHostBindingID );

    return i1RetVal;


} /* NcFsMIIpDbHostBindingIDGet */

/********************************************************************
* FUNCTION NcFsMIIpDbGatewayIpModeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbGatewayIpModeGet (
                INT4 i4FsMIIpDbHostContextId,
                UINT1 *FsMIIpDbHostMac,
                INT4 i4FsMIIpDbHostVlanId,
                UINT4 pFsMIIpDbGatewayNetwork,
                UINT4 pFsMIIpDbGatewayNetMask,
                UINT4 pFsMIIpDbGatewayIp,
                INT4 *pi4FsMIIpDbGatewayIpMode )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceFsMIIpDbGatewayIpTable(
                 i4FsMIIpDbHostContextId,
                 FsMIIpDbHostMac,
                 i4FsMIIpDbHostVlanId,
                 pFsMIIpDbGatewayNetwork,
                 pFsMIIpDbGatewayNetMask,
                 pFsMIIpDbGatewayIp) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMIIpDbGatewayIpMode(
                 i4FsMIIpDbHostContextId,
                 FsMIIpDbHostMac,
                 i4FsMIIpDbHostVlanId,
                 pFsMIIpDbGatewayNetwork,
                 pFsMIIpDbGatewayNetMask,
                 pFsMIIpDbGatewayIp,
                 pi4FsMIIpDbGatewayIpMode );

    return i1RetVal;


} /* NcFsMIIpDbGatewayIpModeGet */

/********************************************************************
* FUNCTION NcFsMIIpDbIntfNoOfVlanBindingsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbIntfNoOfVlanBindingsGet (
                INT4 i4FsMIIpDbIntfContextId,
                INT4 i4FsMIIpDbIntfVlanId,
                UINT4 *pu4FsMIIpDbIntfNoOfVlanBindings )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceFsMIIpDbInterfaceTable(
                 i4FsMIIpDbIntfContextId,
                 i4FsMIIpDbIntfVlanId) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMIIpDbIntfNoOfVlanBindings(
                 i4FsMIIpDbIntfContextId,
                 i4FsMIIpDbIntfVlanId,
                 pu4FsMIIpDbIntfNoOfVlanBindings );

    return i1RetVal;


} /* NcFsMIIpDbIntfNoOfVlanBindingsGet */

/********************************************************************
* FUNCTION NcFsMIIpDbIntfNoOfVlanStaticBindingsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbIntfNoOfVlanStaticBindingsGet (
                INT4 i4FsMIIpDbIntfContextId,
                INT4 i4FsMIIpDbIntfVlanId,
                UINT4 *pu4FsMIIpDbIntfNoOfVlanStaticBindings )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceFsMIIpDbInterfaceTable(
                 i4FsMIIpDbIntfContextId,
                 i4FsMIIpDbIntfVlanId) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMIIpDbIntfNoOfVlanStaticBindings(
                 i4FsMIIpDbIntfContextId,
                 i4FsMIIpDbIntfVlanId,
                 pu4FsMIIpDbIntfNoOfVlanStaticBindings );

    return i1RetVal;


} /* NcFsMIIpDbIntfNoOfVlanStaticBindingsGet */

/********************************************************************
* FUNCTION NcFsMIIpDbIntfNoOfVlanDHCPBindingsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbIntfNoOfVlanDHCPBindingsGet (
                INT4 i4FsMIIpDbIntfContextId,
                INT4 i4FsMIIpDbIntfVlanId,
                UINT4 *pu4FsMIIpDbIntfNoOfVlanDHCPBindings )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceFsMIIpDbInterfaceTable(
                 i4FsMIIpDbIntfContextId,
                 i4FsMIIpDbIntfVlanId) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMIIpDbIntfNoOfVlanDHCPBindings(
                 i4FsMIIpDbIntfContextId,
                 i4FsMIIpDbIntfVlanId,
                 pu4FsMIIpDbIntfNoOfVlanDHCPBindings );

    return i1RetVal;


} /* NcFsMIIpDbIntfNoOfVlanDHCPBindingsGet */

/********************************************************************
* FUNCTION NcFsMIIpDbIntfNoOfVlanPPPBindingsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbIntfNoOfVlanPPPBindingsGet (
                INT4 i4FsMIIpDbIntfContextId,
                INT4 i4FsMIIpDbIntfVlanId,
                UINT4 *pu4FsMIIpDbIntfNoOfVlanPPPBindings )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceFsMIIpDbInterfaceTable(
                 i4FsMIIpDbIntfContextId,
                 i4FsMIIpDbIntfVlanId) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMIIpDbIntfNoOfVlanPPPBindings(
                 i4FsMIIpDbIntfContextId,
                 i4FsMIIpDbIntfVlanId,
                 pu4FsMIIpDbIntfNoOfVlanPPPBindings );

    return i1RetVal;


} /* NcFsMIIpDbIntfNoOfVlanPPPBindingsGet */

/********************************************************************
* FUNCTION NcFsMIIpDbSrcGuardStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbSrcGuardStatusSet (
                INT4 i4FsMIIpDbSrcGuardIndex,
                INT4 i4FsMIIpDbSrcGuardStatus )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetFsMIIpDbSrcGuardStatus(
                 i4FsMIIpDbSrcGuardIndex,
                i4FsMIIpDbSrcGuardStatus);

    return i1RetVal;


} /* NcFsMIIpDbSrcGuardStatusSet */

/********************************************************************
* FUNCTION NcFsMIIpDbSrcGuardStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIIpDbSrcGuardStatusTest (UINT4 *pu4Error,
                INT4 i4FsMIIpDbSrcGuardIndex,
                INT4 i4FsMIIpDbSrcGuardStatus )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2FsMIIpDbSrcGuardStatus(pu4Error,
                 i4FsMIIpDbSrcGuardIndex,
                i4FsMIIpDbSrcGuardStatus);

    return i1RetVal;


} /* NcFsMIIpDbSrcGuardStatusTest */

/* END i_ARICENT_MIIPDB_MIB.c */
