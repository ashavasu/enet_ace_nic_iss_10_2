/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: ipdbport.c,v 1.4 2010/10/18 06:27:10 prabuc Exp $                */
/*****************************************************************************/
/*    FILE  NAME            : ipdbport.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : IP Binding Database management                 */
/*    MODULE NAME           : Ip Binding Database management Porting Module  */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the porting funtions        */
/*                            for IP Binding Database management             */
/*---------------------------------------------------------------------------*/

#include "ipdbinc.h"
#include "ipdbextn.h"

/*****************************************************************************/
/* Function Name      : IpdbPortVlanDelete                                   */
/*                                                                           */
/* Description        : This function is to indicate the IP Binding Database */
/*                      management Module about VLAN Deletion                */
/*                                                                           */
/* Input(s)           : u4ContextID - Context Identifier                     */
/*                      VlanId      - VLAN Identifier                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IpdbPortVlanDelete (UINT4 u4ContextID, tVlanId VlanId)
{
    tIpDbQMsg          *pIpDbQMsg = NULL;

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_ENTRY, IPDB_MODULE_NAME,
                   "Entering the function %s \r\n", __FUNCTION__);

    IPDB_TRC_ARG2 (IPDB_TRC_FLAG, IPDB_FN_ARGS, IPDB_MODULE_NAME,
                   "The arguments to the function %s are VlanId %d \r\n",
                   __FUNCTION__, VlanId);

    if ((pIpDbQMsg = (tIpDbQMsg *)
         IPDB_ALLOC_MEM_BLOCK (IPDB_Q_POOL_ID)) == NULL)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Allocation of memory for Q message failed\r\n");
        return;
    }

    pIpDbQMsg->u4EventType = IPDB_VLAN_INTERFACE_EVENT;
    pIpDbQMsg->u4ContextId = u4ContextID;
    pIpDbQMsg->VlanId = VlanId;

    /* Send the Messageto queue */
    if (IPDB_SEND_TO_QUEUE (IPDB_QUEUE_ID, (UINT1 *) &pIpDbQMsg,
                            OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Failed to send the message to Q\r\n");
        IPDB_RELEASE_MEM_BLOCK (IPDB_Q_POOL_ID, pIpDbQMsg);
        return;
    }

    /* Send an event to IPDB Task, indicating VLAN deletion */
    if (IPDB_SEND_EVENT (IPDB_ZERO, IPDB_TASK_NAME, IPDB_EVENT_ARRIVED)
        != OSIX_SUCCESS)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Sending the port deletion event to IPDB failed\r\n");
        return;
    }
    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);
    return;
}

/*****************************************************************************/
/* Function Name      : IpdbPortConvertMacToStr                              */
/*                                                                           */
/* Description        : This function is to convert Mac to string            */
/*                                                                           */
/* Input(s)           : MacAddress - Mac Address                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Pointer to the Mac address string                    */
/*****************************************************************************/
UINT1              *
IpdbPortConvertMacToStr (tMacAddr MacAddress)
{
    MEMSET (gau1MacString, IPDB_ZERO, IPDB_MAC_STR_LEN);
    CliMacToStr (MacAddress, gau1MacString);

    return gau1MacString;
}

/*****************************************************************************/
/* Function Name      : IpdbPortMacToStr                                     */
/*                                                                           */
/* Description        : This function is to convert Mac to string            */
/*                                                                           */
/* Input(s)           : MacAddress - Mac Address                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IPDB_SUCCESS/IPDB_FAILURE                            */
/*****************************************************************************/
INT4
IpdbPortMacToStr (UINT1 *pu1String, tMacAddr MacAddr)
{
    StrToMac (pu1String, MacAddr);
    return IPDB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IpdbPortGetIfName                                    */
/*                                                                           */
/* Description        : This function is to get the interface name of given  */
/*                      port                                                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pi1NameStr - Interface index                         */
/*                                                                           */
/* Return Value(s)    : IPDB_SUCCESS/IPDB_FAILURE                            */
/*****************************************************************************/
INT4
IpdbPortGetIfName (UINT4 u4IfIndex, INT1 *pi1NameStr)
{
    CfaCliConfGetIfName (u4IfIndex, pi1NameStr);
    return IPDB_SUCCESS;
}

/*                                                                           */
/***************************** END OF FILE ***********************************/
/*                                                                           */
