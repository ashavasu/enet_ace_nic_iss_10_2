/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipdbsz.c,v 1.3 2013/11/29 11:04:11 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _IPDBSZ_C
#include "ipdbinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
IpdbSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < IPDB_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsIPDBSizingParams[i4SizingId].u4StructSize,
                              FsIPDBSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(IPDBMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            IpdbSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
IpdbSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsIPDBSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, IPDBMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
IpdbSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < IPDB_MAX_SIZING_ID; i4SizingId++)
    {
        if (IPDBMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (IPDBMemPoolIds[i4SizingId]);
            IPDBMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
