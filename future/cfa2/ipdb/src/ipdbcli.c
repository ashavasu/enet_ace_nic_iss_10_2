/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: ipdbcli.c,v 1.18 2015/02/20 12:16:38 siva Exp $                     */
/*****************************************************************************/
/*    FILE  NAME            : ipdbcli.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : IP Binding Database management                 */
/*    MODULE NAME           : IP Binding Database management Cli Module      */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains CLI SET/GET/TEST and        */
/*                            GETNEXT routines for the MIB objects           */
/*                            specified in fsipdb.mib funtions               */
/*                            for IP Binding Database management Module      */
/*---------------------------------------------------------------------------*/
#ifndef __IPDBCLI_C__
#define __IPDBCLI_C__

#include "ipdbinc.h"
#include "ipdbextn.h"
/*****************************************************************************/
/* Function Name      : cli_process_ipdb_cmd                                 */
/*                                                                           */
/* Description        : This function takes in variable no. of arguments     */
/*                      and process the commands for IP Binding Database     */
/*                      management Cli module as defined in ipdbcmd.def      */
/*                                                                           */
/* Input(s)           : CliHandle -  CLIHandler                              */
/*                      u4Command -  Command Identifier                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
cli_process_ipdb_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    tMacAddr            MacAddr = { IPDB_ZERO };
    UINT4              *args[CLI_MAX_ARGS] = { IPDB_ZERO };
    INT4                i4IpDbVlanId = IPDB_ZERO;
    INT4                i4VlanId = IPDB_ZERO;
    UINT4               u4Inst = IPDB_ZERO;
    UINT4               u4IfIndex = IPDB_ZERO;
    INT4                i4RetStat = CLI_SUCCESS;
    INT4                i4Trace = IPDB_ZERO;
    INT4                i4ShowType = IPDB_ZERO;
    INT4                i4Type = IPDB_ZERO;
    UINT4               u4ErrCode = IPDB_ZERO;
    INT1                i1argno = IPDB_ZERO;
    UINT4               u4ContextId = IPDB_ZERO;
    INT4                i4Args = IPDB_ZERO;

    CliRegisterLock (CliHandle, IPDB_LOCK, IPDB_UNLOCK);
    IPDB_LOCK ();

    if (IpDbCliSelectContextOnMode
        (CliHandle, u4Command, &u4ContextId) == IPDB_FAILURE)
    {
        IPDB_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* third argument is always interface name/index */
    u4Inst = va_arg (ap, UINT4);

    if (u4Inst != IPDB_ZERO)
    {
        u4IfIndex = u4Inst;
    }
    /* Walk through the rest of the arguments and store in args array,
     * till the number of arguments reaches CLI_MAX_ARGS */
    while (IPDB_ONE)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);

        if (i1argno == CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);
    CLI_SET_ERR (IPDB_ZERO);

    switch (u4Command)
    {
        case IPDB_BINDING_SHOW:
        {
            if (args[IPDB_ONE] != NULL)
            {
                i4IpDbVlanId = *(INT4 *) (args[IPDB_ONE]);
            }
            i4ShowType = CLI_PTR_TO_I4 (args[IPDB_TWO]);

            if ((i4ShowType == IPDB_SHOW_STATIC) ||
                (i4ShowType == IPDB_SHOW_DHCP) ||
                (i4ShowType == IPDB_SHOW_PPP) || (i4ShowType == IPDB_SHOW_ALL))
            {
                i4RetStat =
                    IpdbCliShowBindingDatabase
                    (CliHandle, (UINT1 *) args[IPDB_ZERO],
                     i4IpDbVlanId, i4ShowType);
            }
            else
            {
                i4RetStat = CLI_FAILURE;
            }

            break;
        }

        case IPDB_STATIC_BINDING_ADD:
        {
            i4IpDbVlanId = *(INT4 *) (args[IPDB_ONE]);
            IpdbPortMacToStr ((UINT1 *) args[IPDB_ZERO], MacAddr);
            i4RetStat =
                IpdbCliStaticBindingAdd (CliHandle, u4IfIndex, MacAddr,
                                         i4IpDbVlanId, *(args[IPDB_TWO]),
                                         *(args[IPDB_THREE]));

            break;
        }
        case IPDB_STATIC_BINDING_DEL:
        {
            i4IpDbVlanId = *(INT4 *) (args[IPDB_ONE]);
            IpdbPortMacToStr ((UINT1 *) args[IPDB_ZERO], MacAddr);

            i4RetStat =
                IpdbCliDeleteStaticBindingEntry (CliHandle,
                                                 i4IpDbVlanId, MacAddr);
            break;
        }

        case IPDB_ARP_MAC_FORCE_FWD:
        {
            i4VlanId = (INT4) CLI_GET_VLANID ();

            if (i4VlanId == CLI_ERROR)
            {
                i4RetStat = CLI_FAILURE;
               break;
            }

            i4RetStat =
                IpdbCliSetArpInfo (CliHandle, IPDB_ZERO,
                                   (UINT2) i4VlanId, IPDB_ARP_MAC_FORCE_FWD,
                                   (UINT1) (CLI_PTR_TO_U4 (args[IPDB_ZERO])));
            break;
        }

        case IPDB_ARP_DOWNSTREAM_BCAST:
        {
            u4IfIndex = CLI_GET_IFINDEX ();

            i4RetStat =
                IpdbCliSetArpInfo (CliHandle, u4IfIndex, IPDB_ZERO,
                                   IPDB_ARP_DOWNSTREAM_BCAST,
                                   (UINT1) (CLI_PTR_TO_U4 (args[IPDB_ZERO])));
            break;
        }

        case IPDB_SHOW_ARP_SPOOF:
        {
            i4RetStat = IpdbCliShowArpSpoofing (CliHandle, u4IfIndex);
            break;
        }

        case IPDB_SHOW_MAC_FORCE_FWD:
        {
            if ((args[IPDB_ZERO]) != NULL)
            {
                i4VlanId = (INT4) *(args[IPDB_ZERO]);
            }
            else
            {
                i4VlanId = IPDB_ZERO;
            }

            i4RetStat = IpdbCliShowMacForceFwd (CliHandle, i4VlanId);
            break;
        }

        case IPDB_TRACE_ENABLE:
        {
            if (args[1] != NULL)
            {
                i4Args = CLI_PTR_TO_I4 (args[1]);
                IpdbCliSetDebugLevel (CliHandle, i4Args);
            }
            i4Trace = CLI_PTR_TO_I4 (args[IPDB_ZERO]);
            i4RetStat = IpdbCliSetTrace (i4Trace, IPDB_ENABLED);
            break;
        }

        case IPDB_TRACE_DISABLE:
        {
            i4Trace = CLI_PTR_TO_I4 (args[IPDB_ZERO]);
            i4RetStat = IpdbCliSetTrace (i4Trace, IPDB_DISABLED);
            break;
        }

        case IPDB_SHOW_BINDING_COUNT:
        {
            if (args[IPDB_ONE] != NULL)
            {
                i4VlanId = *(INT4 *) (args[IPDB_ONE]);
            }
            if (args[IPDB_TWO] != NULL)
            {
                i4Type = CLI_PTR_TO_I4 (args[IPDB_TWO]);
            }
            i4RetStat =
                IpdbCliShowBindingCount (CliHandle, (UINT1 *)
                                         args[IPDB_ZERO], i4VlanId, i4Type);
            break;
        }

        case IPDB_ENABLE_IP_SRC_GUARD:

            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStat =
                IpdbCliSetIpSrcGuardStatus (CliHandle, u4IfIndex,
                                            (CLI_PTR_TO_I4 (args[IPDB_ZERO])));
            break;

        case IPDB_DISABLE_IP_SRC_GUARD:

            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStat = IpdbCliSetIpSrcGuardStatus (CliHandle, u4IfIndex,
                                                    IPDB_IPSG_DISABLE);
            break;

        case IPDB_SHOW_IP_SRC_GUARD:

            i4RetStat = IpdbCliShowIpSrcGuardStatus (CliHandle, u4IfIndex);
            break;

        default:
        {
            CliPrintf (CliHandle, "\r%% Unknown command \r\n");
            IpDbUtilResetContext ();
            IPDB_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
        }
    }

    IpDbUtilResetContext ();

    if ((i4RetStat == CLI_FAILURE) && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > IPDB_ZERO) && (u4ErrCode < CLI_IPDB_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", gaIpdbCliErrorString[u4ErrCode]);
        }
        CLI_SET_ERR (IPDB_ZERO);
    }
    CLI_SET_CMD_STATUS (i4RetStat);

    IPDB_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  IpdbCliSetDebugLevel
* Description :
* Input       :  CliHandle, i4CliDebugLevel
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
IpdbCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel)
{

    IPDB_TRC_LVL = 0;

    UNUSED_PARAM (CliHandle);

    if (i4CliDebugLevel == DEBUG_DEBUG_LEVEL)
    {
        IPDB_TRC_LVL = IPDB_FN_ENTRY | IPDB_FN_EXIT |
            IPDB_FAIL_TRC | IPDB_DBG_TRC | IPDB_FN_ARGS;

    }
    else if (i4CliDebugLevel == DEBUG_INFO_LEVEL)
    {
        IPDB_TRC_LVL = IPDB_FN_ENTRY | IPDB_FN_EXIT | IPDB_DBG_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_NOTICE_LEVEL)
    {
        IPDB_TRC_LVL = IPDB_FN_ENTRY | IPDB_FN_EXIT | IPDB_DBG_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_WARN_LEVEL)
    {
        IPDB_TRC_LVL = IPDB_FAIL_TRC | IPDB_DBG_TRC | IPDB_FN_ARGS;
    }
    else if (i4CliDebugLevel == DEBUG_ERROR_LEVEL)
    {
        IPDB_TRC_LVL = IPDB_FAIL_TRC;
    }
    else if ((i4CliDebugLevel == DEBUG_CRITICAL_LEVEL)
             || (i4CliDebugLevel == DEBUG_ALERT_LEVEL)
             || (i4CliDebugLevel == DEBUG_EMERG_LEVEL))
    {
        IPDB_TRC_LVL = IPDB_FAIL_TRC | IPDB_DBG_TRC | IPDB_FN_ARGS;
    }
    else if (i4CliDebugLevel == DEBUG_DEF_LVL_FLAG)
    {
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IpdbCliStaticBindingAdd                              */
/*                                                                           */
/* Description        : This function  configures Static Binding Information */
/*                                                                           */
/* Input(s)           : CliHandle     - CLIHandler                           */
/*                      i4IpdbVlanId  - Vlan Identifier                      */
/*                      u4PortId      - Identifies PortLocation              */
/*                      HostMac       - HostMacAddress                       */
/*                      u4HostIp      - HostIpAddress                        */
/*                      u4GatewayIp   - Gateway Ip Address                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_FAILURE/CLI_SUCCESS                              */
/*****************************************************************************/
INT4
IpdbCliStaticBindingAdd (tCliHandle CliHandle, UINT4 u4PortId, tMacAddr HostMac,
                         INT4 i4IpdbVlanId, UINT4 u4HostIp, UINT4 u4GatewayIp)
{
    UINT4               u4ErrorCode = IPDB_ZERO;
    UINT4               u4IpDbStaticBindingStatus = IPDB_ZERO;

    if (nmhGetFsIpDbStaticBindingStatus (i4IpdbVlanId, HostMac,
                                         (INT4 *) &u4IpDbStaticBindingStatus) ==
        SNMP_SUCCESS)
    {
        if (nmhTestv2FsIpDbStaticBindingStatus (&u4ErrorCode, i4IpdbVlanId,
                                                HostMac,
                                                IPDB_NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Unable to create static entry \r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsIpDbStaticBindingStatus (i4IpdbVlanId, HostMac,
                                             IPDB_NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsIpDbStaticBindingStatus (&u4ErrorCode, i4IpdbVlanId,
                                                HostMac,
                                                IPDB_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Unable to create static entry \r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsIpDbStaticBindingStatus (i4IpdbVlanId, HostMac,
                                             IPDB_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsIpDbStaticHostIp (&u4ErrorCode, i4IpdbVlanId, HostMac,
                                     u4HostIp) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Invalid value for Host Ip Address\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsIpDbStaticHostIp (i4IpdbVlanId, HostMac, u4HostIp) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsIpDbStaticGateway (&u4ErrorCode, i4IpdbVlanId,
                                      HostMac, u4GatewayIp) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Invalid Gateway Ip Address\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsIpDbStaticGateway (i4IpdbVlanId, HostMac, u4GatewayIp) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsIpDbStaticInIfIndex (&u4ErrorCode, i4IpdbVlanId,
                                        HostMac, u4PortId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Invalid Port Id\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsIpDbStaticInIfIndex (i4IpdbVlanId, HostMac, u4PortId) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsIpDbStaticBindingStatus (&u4ErrorCode, i4IpdbVlanId,
                                            HostMac, IPDB_ACTIVE) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Invalid value for Rowstatus \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsIpDbStaticBindingStatus (i4IpdbVlanId, HostMac,
                                         IPDB_ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "%% Cannot create the static entry over the dynamically learnt entries for the same VLAN \r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : IpdbCliDeleteStaticBindingEntry                      */
/*                                                                           */
/* Description        : This function deletes Static Binding Information     */
/*                                                                           */
/* Input(s)           : CliHandle    -  CLIHandler                           */
/*                      i4IpdbVlanId -  Vlan Identifier                      */
/*                      HostMac      -  HostMacAddress                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_FAILURE/CLI_SUCCESS                              */
/*****************************************************************************/
INT4
IpdbCliDeleteStaticBindingEntry (tCliHandle CliHandle, INT4 i4IpdbVlanId,
                                 tMacAddr HostMac)
{
    UINT4               u4ErrorCode = IPDB_ZERO;
    UINT4               u4IpDbStaticBindingStatus = IPDB_ZERO;

    if (nmhGetFsIpDbStaticBindingStatus (i4IpdbVlanId, HostMac,
                                         (INT4 *) &u4IpDbStaticBindingStatus)
        != SNMP_SUCCESS)
    {

        CliPrintf (CliHandle, "%% Entry does not exist to delete\r\n");
        return CLI_SUCCESS;
    }

    if (nmhValidateIndexInstanceFsIpDbStaticBindingTable (i4IpdbVlanId,
                                                          HostMac) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsIpDbStaticBindingStatus (&u4ErrorCode, i4IpdbVlanId,
                                            HostMac, IPDB_DESTROY) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Invalid value for RowStatus\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsIpDbStaticBindingStatus (i4IpdbVlanId, HostMac,
                                         IPDB_DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IpdbCliSetArpInfo                                    */
/*                                                                           */
/* Description        : This function configures the given status in the     */
/*                     MacForceFwd or DownstreamArpBcast variable            */
/*                                                                           */
/* Input(s)           : CliHandle    -  CLIHandler                           */
/*                      u4IfIndex    -  Port Identifier                      */
/*                      u1SetParam   -  Flag used to specify whether to      */
/*                                      configure MacForceFwd or             */
/*                                      DownstreamArpBcast                   */
/*                      u1Status     -  Status to be configured              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_FAILURE/CLI_SUCCESS                              */
/*****************************************************************************/
INT4
IpdbCliSetArpInfo (tCliHandle CliHandle, UINT4 u4IfIndex, tVlanId VlanId,
                   UINT1 u1SetParam, UINT1 u1Status)
{
    tVlanCtrlEntry     *pVlanCtrlEntry = NULL;
    tPortCtrlEntry     *pPortCtrlEntry = NULL;
    UINT4               u4ErrorCode = IPDB_ZERO;

    if (VlanId != IPDB_ZERO)
    {
        IpDbGetVlanCtrlEntry (VlanId, &pVlanCtrlEntry);
    }
    else if (u4IfIndex != IPDB_ZERO)
    {
        IpDbGetPortCtrlEntry (u4IfIndex, &pPortCtrlEntry);
    }

    if (u1SetParam == IPDB_ARP_MAC_FORCE_FWD)
    {
        if (pVlanCtrlEntry == NULL)
        {
            if (nmhTestv2FsSystemVlanRowStatus (&u4ErrorCode, (INT4) VlanId,
                                                IPDB_CREATE_AND_GO)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsSystemVlanRowStatus ((INT4) VlanId,
                                             IPDB_CREATE_AND_GO) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }

        if (nmhTestv2FsSystemVlanRowStatus (&u4ErrorCode, (INT4) VlanId,
                                            IPDB_NOT_IN_SERVICE)
            != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsSystemVlanRowStatus ((INT4) VlanId,
                                         IPDB_NOT_IN_SERVICE) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2FsSystemVlanMacForceFwdStatus (&u4ErrorCode, (INT4) VlanId,
                                                    (INT4) u1Status) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsSystemVlanMacForceFwdStatus ((INT4) VlanId,
                                                 (INT4) u1Status) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2FsSystemVlanRowStatus (&u4ErrorCode, (INT4) VlanId,
                                            IPDB_ACTIVE) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsSystemVlanRowStatus ((INT4) VlanId,
                                         IPDB_ACTIVE) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        return CLI_SUCCESS;
    }
    else if (u1SetParam == IPDB_ARP_DOWNSTREAM_BCAST)
    {
        if (pPortCtrlEntry != NULL)
        {
            if (nmhTestv2FsSystemPortCtrlDownstreamArpBcast
                (&u4ErrorCode, (INT4) u4IfIndex,
                 (INT4) u1Status) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsSystemPortCtrlDownstreamArpBcast
                ((INT4) u4IfIndex, (INT4) u1Status) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

/*****************************************************************************/
/* Function Name      : IpdbCliShowArpSpoofing                               */
/*                                                                           */
/* Description        : This function used to show the Arp parameters.       */
/*                                                                           */
/* Input(s)           : CliHandle    -  CLIHandler                           */
/*                      u4IfIndex    -  Port Identifier                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_FAILURE/CLI_SUCCESS                              */
/*****************************************************************************/
INT4
IpdbCliShowArpSpoofing (tCliHandle CliHandle, UINT4 u4PortId)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4NextPort = IPDB_ZERO;
    UINT4               u4CurrentPort = IPDB_ZERO;
    UINT4               u4PagingStatus = IPDB_ZERO;
    INT4                i4Status = IPDB_ZERO;
    UINT4               u4Counter = IPDB_ZERO;
    UINT1               u1isShowAll = TRUE;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH] = { IPDB_ZERO };

    CliPrintf (CliHandle, "\r\nArp Spoofing table \r\n");

    CliPrintf (CliHandle, "------------------ \r\n");

    CliPrintf (CliHandle,
               "Port       Downstream Bcast   Arp forwarded"
               "   Arp dropped \r\n");

    CliPrintf (CliHandle,
               "----       ----------------   -------------"
               "   ----------- \r\n");

    if (u4PortId == IPDB_ZERO)
    {
        /* Show for all the Ports */
        i4RetVal = nmhGetFirstIndexFsSystemPortCtrlTable ((INT4 *) &u4NextPort);
    }
    else
    {
        /* 
         * Show for a specific Port. 
         *
         * Setting the Current indices to the values to the previous 
         * element such that the Get Next operation will fetch the next 
         * element in the lexicographic order.
         */
        u4CurrentPort = u4PortId - IPDB_ONE;

        i4RetVal
            = nmhGetNextIndexFsSystemPortCtrlTable (u4CurrentPort,
                                                    (INT4 *) &u4NextPort);
    }

    if (i4RetVal == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if (u4PortId != IPDB_ZERO)
        {
            /* Show for a specific Port */
            if (u4NextPort > u4PortId)
            {
                /* 
                 * Done with the show of the required port and hence
                 * terminating the loop.
                 */
                break;
            }
        }

        CfaCliGetIfName (u4NextPort, (INT1 *) au1NameStr);
        CliPrintf (CliHandle, "%-15s", au1NameStr);

        nmhGetFsSystemPortCtrlDownstreamArpBcast (u4NextPort, &i4Status);

        if (i4Status == IPDB_ARP_ALLOW)
        {
            CliPrintf (CliHandle, "%-20s", "allow");
        }
        else
        {
            CliPrintf (CliHandle, "%-20s", "drop");
        }

        nmhGetFsSystemPortCtrlArpPktsForwarded (u4NextPort, &u4Counter);
        CliPrintf (CliHandle, "%-15d", u4Counter);

        nmhGetFsSystemPortCtrlArpPktsDropped (u4NextPort, &u4Counter);
        u4PagingStatus = CliPrintf (CliHandle, "%d\r\n", u4Counter);

        u4CurrentPort = u4NextPort;

        if (nmhGetNextIndexFsSystemPortCtrlTable (u4CurrentPort,
                                                  (INT4 *) &u4NextPort) ==
            SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt, no more print required, exit */
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll == TRUE);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      :  IpdbCliShowMacForceFwd                              */
/*                                                                           */
/* Description        :  This function shows                                 */
/*                       Mac force forwarding status                         */
/*                                                                           */
/* Input(s)           :  CliHandle   -  CLIHandler                           */
/*                       i4VlanId    -  VlanId                               */
/*                                                                           */
/* Output(s)          :  None                                                */
/*                                                                           */
/* Return Value(s)    :  CLI_SUCCESS/CLI_FAILURE                             */
/*****************************************************************************/

INT4
IpdbCliShowMacForceFwd (tCliHandle CliHandle, INT4 i4VlanId)
{
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4CurrVlanId = IPDB_ZERO;
    INT4                i4IpdbVlanId = IPDB_ZERO;
    INT4                i4MacForceFwd = IPDB_ZERO;

    /* Checking whether Vlan Id is Zero or not, If it is Not Equal to zero *
     * then displays all the information about that Particular Vlan Id..   *
     * otherwise Displays all the Vlan Information By Using GetFirst and   *
     * GetNext  nmh Routines                                               */
    if (i4VlanId != IPDB_ZERO)
    {
        if (nmhGetFsSystemVlanMacForceFwdStatus (i4VlanId, &i4MacForceFwd)
            == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n Vlan Mac force forwarding status \r\n");
            CliPrintf (CliHandle, " --------------------------------\r\n");

            /* Calling the function to get the Structure information   *
             * about the particular Vlan                               */
            CliPrintf (CliHandle, "VLAN                   : %ld\r\n", i4VlanId);

            if (i4MacForceFwd == IPDB_ENABLED)
            {
                CliPrintf (CliHandle, "Mac force forwarding   : %s\r\n",
                           "Enabled");
            }
            else if (i4MacForceFwd == IPDB_DISABLED)
            {
                CliPrintf (CliHandle, "Mac force forwarding   : %s\r\n",
                           "Disabled");
            }
        }
        else
        {
            CliPrintf (CliHandle, "\r% No entries to display \r\n");
        }

    }
    else
    {
        /* Getting the First Entry From the Table */
        if (nmhGetFirstIndexFsSystemVlanCtrlTable (&i4IpdbVlanId)
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r% No entries to display \r\n");
            return CLI_SUCCESS;
        }

        CliPrintf (CliHandle, "\r\n Vlan Mac force forwarding status \r\n");
        CliPrintf (CliHandle, " --------------------------------\r\n");

        do
        {
            if (nmhGetFsSystemVlanMacForceFwdStatus
                (i4IpdbVlanId, &i4MacForceFwd) == SNMP_SUCCESS)
            {
                /* Calling the function to get the Structure information   *
                 * about the particular Vlan                               */
                CliPrintf (CliHandle, "VLAN                   : %ld\r\n",
                           i4IpdbVlanId);

                if (i4MacForceFwd == IPDB_ENABLED)
                {
                    CliPrintf (CliHandle, "Mac force forwarding   : %s\r\n",
                               "Enabled");
                }
                else if (i4MacForceFwd == IPDB_DISABLED)
                {
                    CliPrintf (CliHandle, "Mac force forwarding   : %s\r\n",
                               "Disabled");
                }
                u4PagingStatus = CliPrintf (CliHandle, "\r\n");
            }

            if ((u4PagingStatus == CLI_FAILURE))
            {
                /* User pressed 'q' at more prompt, *
                 * no more print required, exit */
                break;
            }

            i4CurrVlanId = i4IpdbVlanId;
            /* Getting the Next Entry From the Table */
        }
        while (nmhGetNextIndexFsSystemVlanCtrlTable (i4CurrVlanId,
                                                     &i4IpdbVlanId) ==
               SNMP_SUCCESS);

    }                            /*End of Else */

    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : IpdbCliShowBindingDatabase                           */
/*                                                                           */
/* Description        : This function Shows Binding Database Information     */
/*                                                                           */
/* Input(s)           : CliHandle    -  CLIHandler                           */
/*                      pu1CxtName   -  Context Name                         */
/*                      i4IpdbVlanId -  Vlan Identifier                      */
/*                      i4ShowType   - Identifies Static/PPP/DHCP            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_FAILURE/CLI_SUCCESS                              */
/*****************************************************************************/
INT4
IpdbCliShowBindingDatabase (tCliHandle CliHandle, UINT1 *pu1CxtName,
                            INT4 i4IpdbVlanId, INT4 i4ShowType)
{
    tMacAddr            IpdbNextHostMac = { IPDB_ZERO };
    tMacAddr            IpdbGetHostMac = { IPDB_ZERO };
    INT4                i4IpdbNextVlanId = IPDB_ZERO;
    INT4                i4IpdbGetVlanId = IPDB_ZERO;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4ContextId = IPDB_ZERO;
    UINT4               u4ContextId = IPDB_ZERO;
    INT4                i4NextCxtId = IPDB_ZERO;
    UINT1               u1CxtFlag = IPDB_FALSE;
    UINT1               u1VlanFlag = IPDB_FALSE;
    INT4                i4CliContextId = IPDB_ZERO;
    INT4                i4RetValue = 0;
    UINT1               u1IsShowAll = IPDB_TRUE;

    UNUSED_PARAM (i4RetValue);

    CliPrintf (CliHandle, "\r\n                            "
               "Host Binding Information\r\n");
    CliPrintf (CliHandle, "\r                            "
               "------------------------\r\n");
    if (pu1CxtName != NULL)
    {
        /* If Switch-name is given then get the Context-Id */
        if (IpDbVcmIsSwitchExist (pu1CxtName, &u4ContextId) != VCM_TRUE)
        {
            CliPrintf (CliHandle, "\r%% Switch %s Does not exist.\r\n",
                       pu1CxtName);
            return CLI_FAILURE;
        }
        i4CliContextId = (INT4) u4ContextId;

        /*Set the Context Flag */
        u1CxtFlag = IPDB_TRUE;
    }
    if (i4IpdbVlanId != IPDB_ZERO)
    {
        /*Set the Vlan Flag */
        u1VlanFlag = IPDB_TRUE;
    }

    /*Get the IP binding data base information from the IP Binding Table */
    if (nmhGetFirstIndexFsMIIpDbBindingTable (&i4ContextId, &i4IpdbGetVlanId,
                                              &IpdbGetHostMac) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        /*Select the Context */
        i4RetValue = IpDbUtilSetContext (i4ContextId);
        /*Check for both the Vlan and Context Flag is set */
        if ((u1CxtFlag == IPDB_TRUE) && (u1VlanFlag == IPDB_TRUE))
        {
            /*Get the Binding Information if Vlan and Context is Given 
             *Through CLI.
             *1)Check for the Context-Id present in the IP Binding table  
             *and the Context-Id which is give through CLI are Same.
             *2)Check for the Vlan-Id present in the IP Binding table and 
             *the Vlan-Id which is give through CLI are Same.
             *If both are same then print the binding Information*/

            if ((i4CliContextId == i4ContextId) &&
                (i4IpdbVlanId == i4IpdbGetVlanId))
            {
                if (i4ContextId != i4NextCxtId)
                {
                    IpdbPrintContextInfo (CliHandle, i4CliContextId);
                }
                i4NextCxtId = i4ContextId;
                i4IpdbNextVlanId = i4IpdbGetVlanId;
                MEMCPY (IpdbNextHostMac, IpdbGetHostMac, sizeof (tMacAddr));

                IpdbCliPrintBindingInfo (CliHandle, i4IpdbGetVlanId,
                                         IpdbGetHostMac, i4ShowType,
                                         &u4PagingStatus);
                if (nmhGetNextIndexFsMIIpDbBindingTable
                    (i4NextCxtId, &i4ContextId, i4IpdbNextVlanId,
                     &i4IpdbGetVlanId, IpdbNextHostMac, &IpdbGetHostMac)
                    == SNMP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    return CLI_SUCCESS;
                }
            }
        }
        /*Check weather the Context Flag is set or not */
        else if (u1CxtFlag == IPDB_TRUE)
        {
            /* If the Context-Id Given Through CLI and Context-Id in the
             * binding table matches then Get the Binding Information.*/
            if (i4CliContextId == i4ContextId)
            {
                if (i4ContextId != i4NextCxtId)
                {
                    IpdbPrintContextInfo (CliHandle, i4CliContextId);
                }
                IpdbCliPrintBindingInfo (CliHandle, i4IpdbGetVlanId,
                                         IpdbGetHostMac, i4ShowType,
                                         &u4PagingStatus);
                i4NextCxtId = i4ContextId;
                i4IpdbNextVlanId = i4IpdbGetVlanId;
                MEMCPY (IpdbNextHostMac, IpdbGetHostMac, sizeof (tMacAddr));
                if (nmhGetNextIndexFsMIIpDbBindingTable
                    (i4NextCxtId, &i4ContextId, i4IpdbNextVlanId,
                     &i4IpdbGetVlanId, IpdbNextHostMac, &IpdbGetHostMac)
                    == SNMP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    return CLI_SUCCESS;
                }
            }

        }
        /*Check weather the Vlan Flag is set or not */
        else if (u1VlanFlag == IPDB_TRUE)
        {
            /* If the Vlan-Id Given Through CLI and Vlan-Id in the
             * binding table matches then Get the Binding Information.*/
            if (i4IpdbVlanId == i4IpdbGetVlanId)
            {
                if (i4ContextId != i4NextCxtId)
                {
                    IpdbPrintContextInfo (CliHandle, i4ContextId);
                }
                i4NextCxtId = i4ContextId;
                i4IpdbNextVlanId = i4IpdbGetVlanId;
                MEMCPY (IpdbNextHostMac, IpdbGetHostMac, sizeof (tMacAddr));

                IpdbCliPrintBindingInfo (CliHandle, i4IpdbGetVlanId,
                                         IpdbGetHostMac, i4ShowType,
                                         &u4PagingStatus);
                if (nmhGetNextIndexFsMIIpDbBindingTable
                    (i4NextCxtId, &i4ContextId, i4IpdbNextVlanId,
                     &i4IpdbGetVlanId, IpdbNextHostMac, &IpdbGetHostMac)
                    == SNMP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    return CLI_SUCCESS;
                }
            }
        }
        else
        {
            if (i4ContextId != i4NextCxtId)
            {
                IpdbPrintContextInfo (CliHandle, i4ContextId);
            }
            /*Print the Binding Information */
            IpdbCliPrintBindingInfo (CliHandle, i4IpdbGetVlanId,
                                     IpdbGetHostMac, i4ShowType,
                                     &u4PagingStatus);
        }
        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        /*Resetting the Context-Id */
        IpDbUtilResetContext ();
        i4NextCxtId = i4ContextId;
        i4IpdbNextVlanId = i4IpdbGetVlanId;
        MEMCPY (IpdbNextHostMac, IpdbGetHostMac, sizeof (tMacAddr));
        /*Get the Next Binding Information form the IP Binding Table */
        if (nmhGetNextIndexFsMIIpDbBindingTable
            (i4NextCxtId, &i4ContextId, i4IpdbNextVlanId, &i4IpdbGetVlanId,
             IpdbNextHostMac, &IpdbGetHostMac) != SNMP_SUCCESS)
        {
            u1IsShowAll = IPDB_FALSE;
        }
    }
    while (u1IsShowAll);
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : IpdbPrintContextInfo                                 */
/*                                                                           */
/* Description        : This function displays Cotext  Information           */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLIHandler                          */
/*                      i4ContextId   -  Identifier                          */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
IpdbPrintContextInfo (tCliHandle CliHandle, INT4 i4ContextId)
{
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN] = { IPDB_ZERO };
    /*Get the Context Name with respect to the Context-Id */
    VcmGetAliasName (i4ContextId, au1ContextName);
    CliPrintf (CliHandle, "\r\nSwitch: ");
    CliPrintf (CliHandle, " %s\n\n", au1ContextName);
    CliPrintf (CliHandle, "-------  -------\r\n");
    CliPrintf (CliHandle, "VLAN      HostMac           HostIP   "
               "      Port        GatewayIP    Type ");
    CliPrintf (CliHandle, "\r\n----  -------------------  ----------"
               "   ----------    ----------    ------\r\n");
    return;
}

/*****************************************************************************/
/* Function Name      : IpdbCliPrintBindingInfo                              */
/*                                                                           */
/* Description        : This function displays Binding Information           */
/*                       for particular Vlan                                 */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLIHandler                          */
/*                      i4IpdbVlanId  -  Vlan Identifier                     */
/*                      HostMac       -  Host Mac Address                    */
/*                      i4BindingType - Show type                            */
/*                                                                           */
/* Output(s)          : pu4PagingStatus - Paging status                      */
/*                                                                           */
/* Return Value(s)    : CLI_FAILURE/CLI_SUCCESS                              */
/*****************************************************************************/
INT4
IpdbCliPrintBindingInfo (tCliHandle CliHandle, INT4 i4IpdbVlanId,
                         tMacAddr HostMac, INT4 i4BindingType,
                         UINT4 *pu4PagingStatus)
{
    tMacAddr            IpdbHostMac = { IPDB_ZERO };
    UINT1               au1Type[IPDB_BIND_TYPE_LEN] = { IPDB_ZERO };
    UINT1               au1NameStr[IPDB_MAX_PORT_NAME_LENGTH] = { IPDB_ZERO };
    UINT1               au1HostMacAddr[IPDB_MAC_STR_LEN] = { IPDB_ZERO };
    CHR1                ac1IpString[IPDB_IP_STR_LEN] = { IPDB_ZERO };
    CHR1                ac1GwString[IPDB_IP_STR_LEN] = { IPDB_ZERO };
    UINT4               u4IpdbHostBindingType = IPDB_ZERO;
    UINT4               u4IpdbInIfIndex = IPDB_ZERO;
    UINT4               u4IpdbHostIp = IPDB_ZERO;
    UINT4               u4IpDbGatewayNetwork = IPDB_ZERO;
    UINT4               u4IpDbGatewayNetMask = IPDB_ZERO;
    UINT4               u4IpDbGatewayIp = IPDB_ZERO;
    INT4                i4VlanId = IPDB_ZERO;
    CHR1               *pc1IpString = NULL;
    CHR1               *pc1GwString = NULL;

    pc1IpString = (CHR1 *) & ac1IpString[IPDB_ZERO];
    pc1GwString = (CHR1 *) & ac1GwString[IPDB_ZERO];
    *pu4PagingStatus = CLI_SUCCESS;

    if (nmhGetFsIpDbHostBindingType (i4IpdbVlanId, HostMac,
                                     (INT4 *) &u4IpdbHostBindingType) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    switch (i4BindingType)
    {
        case IPDB_SHOW_STATIC:
        {
            if (u4IpdbHostBindingType != IPDB_STATIC_BINDING)
            {
                return CLI_SUCCESS;
            }
            break;
        }
        case IPDB_SHOW_DHCP:
        {
            if (u4IpdbHostBindingType != IPDB_DHCP_BINDING)
            {
                return CLI_SUCCESS;
            }
            break;
        }
        case IPDB_SHOW_PPP:
        {
            if (u4IpdbHostBindingType != IPDB_PPP_BINDING)
            {
                return CLI_SUCCESS;
            }
            break;
        }
        case IPDB_SHOW_ALL:
        {
            break;
        }
        default:
        {
            break;
        }
    }

    if (u4IpdbHostBindingType == IPDB_PPP_BINDING)
    {
        STRNCPY (au1Type, "ppp", STRLEN("ppp"));
	au1Type[STRLEN("ppp")] = '\0';
    }

    if (u4IpdbHostBindingType == IPDB_DHCP_BINDING)
    {
        STRNCPY (au1Type, "dhcp", STRLEN("dhcp"));
	au1Type[STRLEN("dhcp")] = '\0';
    }

    if (u4IpdbHostBindingType == IPDB_STATIC_BINDING)
    {
        STRNCPY (au1Type, "static", STRLEN("static"));
	au1Type[STRLEN("static")] = '\0';
    }

    if (nmhGetNextIndexFsIpDbGatewayIpTable (HostMac, &IpdbHostMac,
                                             i4IpdbVlanId, &i4VlanId, IPDB_ZERO,
                                             &u4IpDbGatewayNetwork, IPDB_ZERO,
                                             &u4IpDbGatewayNetMask, IPDB_ZERO,
                                             &u4IpDbGatewayIp) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsIpDbHostIp (i4IpdbVlanId, HostMac, &u4IpdbHostIp) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsIpDbHostInIfIndex (i4IpdbVlanId, HostMac,
                                   (INT4 *) &u4IpdbInIfIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    PrintMacAddress (HostMac, au1HostMacAddr);
    CLI_CONVERT_IPADDR_TO_STR (pc1IpString, u4IpdbHostIp);
    CfaCliGetIfName (u4IpdbInIfIndex, (INT1 *) au1NameStr);

    CliPrintf (CliHandle, "%-4d %-17s %-15s %-11s ",
               i4IpdbVlanId, au1HostMacAddr, pc1IpString, au1NameStr);
    CLI_CONVERT_IPADDR_TO_STR (pc1GwString, u4IpDbGatewayIp);

    *pu4PagingStatus =
        CliPrintf (CliHandle, "%-12s %-3s\r\n", pc1GwString, au1Type);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      :  IpdbShowRunningConfig                               */
/*                                                                           */
/* Description        :  This function displays the                          */
/*                       Ipdb Static Binding Configurations                  */
/*                                                                           */
/* Input(s)           :  CliHandle - Handle to the CLI Context               */
/*                                                                           */
/* Output(s)          :  None                                                */
/*                                                                           */
/* Return Value(s)    :  CLI_FAILURE/CLI SUCCESS                             */
/*****************************************************************************/
INT4
IpdbShowRunningConfig (tCliHandle CliHandle, UINT4 u4Index, UINT4 u4Module)
{

    CliRegisterLock (CliHandle, IPDB_LOCK, IPDB_UNLOCK);
    IPDB_LOCK ();

    IssIpdbShowDebugging (CliHandle);

    if ((u4Module == ISS_INTERFACE_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_IPDB_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        IpdbShowRunningConfigIntfDetails (CliHandle, u4Index);
    }

    if ((u4Module == ISS_IPDB_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        IpdbShowRunningConfigGetId (CliHandle);
    }

    if ((u4Module == ISS_VLAN_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_IPDB_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        IpdbShowRunningConfigVlanDetails (CliHandle, u4Index);
    }

    IPDB_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      :  IpdbShowRunningConfigIntfDetails                    */
/*                                                                           */
/* Description        :  This function displays current configuration        */
/*                                                                           */
/* Input(s)           :  CliHandle  - Handle to the  CLI context             */
/*                                                                           */
/* Output(s)          :  None                                                */
/*                                                                           */
/* Return Value(s)    :  CLI_FAILURE/CLI SUCCESS                             */
/*****************************************************************************/
INT4
IpdbShowRunningConfigIntfDetails (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    INT4                i4IfIndex = IPDB_ZERO;
    INT4                i4CurrIndex = IPDB_ZERO;
    INT4                i4Status = IPDB_ZERO;
    INT4                i4IpsgStatus = IPDB_ZERO;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4Flag = IPDB_ZERO;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH] = { IPDB_ZERO };

    if (u4IfIndex != IPDB_ZERO)
    {
        u4Flag = IPDB_ONE;
        i4IfIndex = (INT4) u4IfIndex;
    }
    else
    {
        if (nmhGetFirstIndexFsSystemPortCtrlTable (&i4IfIndex) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    do
    {
        if (nmhGetFsSystemPortCtrlDownstreamArpBcast (i4IfIndex, &i4Status)
            == SNMP_SUCCESS)
        {
            if (i4Status == IPDB_ARP_DROP)
            {
                CfaCliConfGetIfName ((UINT4) i4IfIndex, (INT1 *) au1NameStr);
                CliPrintf (CliHandle, "interface %s\r\n", au1NameStr);

                CliPrintf (CliHandle, "downstream arp-bcast drop\r\n");
                u4PagingStatus = CliPrintf (CliHandle, "!\r\n");
            }

            if ((u4PagingStatus == CLI_FAILURE))
            {
                /* User pressed 'q' at more prompt, *
                 *                  * no more print required, exit *
                 *                                   */
                break;
            }
        }
        if (nmhGetFsIpDbSrcGuardStatus (i4IfIndex, &i4IpsgStatus)
            == SNMP_SUCCESS)
        {
            if ((i4IpsgStatus == IPDB_IPSG_IP_MODE) ||
                (i4IpsgStatus == IPDB_IPSG_IP_MAC_MODE))
            {
                CfaCliConfGetIfName ((UINT4) i4IfIndex, (INT1 *) au1NameStr);
                CliPrintf (CliHandle, "interface %s\r\n", au1NameStr);

                if (i4IpsgStatus == IPDB_IPSG_IP_MODE)
                {
                    CliPrintf (CliHandle, "ip verify source\r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "ip verify source port-security\r\n");
                }
                u4PagingStatus = CliPrintf (CliHandle, "!\r\n");
            }

            if ((u4PagingStatus == CLI_FAILURE))
            {
                /* User pressed 'q' at more prompt, *
                 *                  * no more print required, exit *
                 *                                   */
                break;
            }
        }

        i4CurrIndex = i4IfIndex;
        /* Getting the Next Entry From the Table */
    }
    while ((u4Flag == IPDB_ZERO) &&
           (nmhGetNextIndexFsSystemPortCtrlTable (i4CurrIndex, &i4IfIndex)
            == SNMP_SUCCESS));

    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function Name      :  IpdbShowRunningConfigVlanDetails                    */
/*                                                                           */
/* Description        :  This function displays current configuration        */
/*                                                                           */
/* Input(s)           :  CliHandle  - Handle to the  CLI context             */
/*                                                                           */
/* Output(s)          :  None                                                */
/*                                                                           */
/* Return Value(s)    :  CLI_FAILURE/CLI SUCCESS                             */
/*****************************************************************************/
INT4
IpdbShowRunningConfigVlanDetails (tCliHandle CliHandle, UINT4 u4Index)
{
    INT4                i4VlanId = IPDB_ZERO;
    INT4                i4CurrVlanId = IPDB_ZERO;
    INT4                i4MacForceFwd = IPDB_ZERO;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4Flag = IPDB_ZERO;

    if (u4Index != IPDB_ZERO)
    {
        u4Flag = IPDB_ONE;
        i4VlanId = (INT4) u4Index;
    }
    else
    {
        if (nmhGetFirstIndexFsSystemVlanCtrlTable (&i4VlanId) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    do
    {
        if (nmhGetFsSystemVlanMacForceFwdStatus (i4VlanId, &i4MacForceFwd)
            == SNMP_SUCCESS)
        {
            if (i4MacForceFwd == IPDB_ENABLED)
            {
                CliPrintf (CliHandle, "vlan %d \r\n", i4VlanId);
                CliPrintf (CliHandle, "mac force forward enable\r\n");
                u4PagingStatus = CliPrintf (CliHandle, "!\r\n");

                if ((u4PagingStatus == CLI_FAILURE))
                {
                    /* User pressed 'q' at more prompt, *
                     * no more print required, exit *
                     */
                    break;
                }
            }
        }

        i4CurrVlanId = i4VlanId;
        /* Getting the Next Entry From the Table */
    }
    while ((u4Flag == IPDB_ZERO) &&
           (nmhGetNextIndexFsSystemVlanCtrlTable (i4CurrVlanId, &i4VlanId)
            == SNMP_SUCCESS));

    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function Name      :  IpdbShowRunningConfigGetId                          */
/*                                                                           */
/* Description        :  This function displays current configuration        */
/*                                                                           */
/* Input(s)           :  CliHandle  - Handle to the  CLI context             */
/*                                                                           */
/* Output(s)          :  None                                                */
/*                                                                           */
/* Return Value(s)    :  CLI_FAILURE/CLI SUCCESS                             */
/*****************************************************************************/
INT4
IpdbShowRunningConfigGetId (tCliHandle CliHandle)
{
    tMacAddr            IpdbHostMac = { IPDB_ZERO };
    tMacAddr            IpdbCurrHostMac = { IPDB_ZERO };
    INT4                i4IpdbVlanId = IPDB_ZERO;
    INT4                i4IpdbCurrVlanId = IPDB_ZERO;
    UINT4               u4PagingStatus = CLI_SUCCESS;

    if (nmhGetFirstIndexFsIpDbStaticBindingTable (&i4IpdbVlanId, &IpdbHostMac)
        == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if (IpdbShowRunningConfigDetails (CliHandle, i4IpdbVlanId,
                                          IpdbHostMac) == CLI_SUCCESS)
        {
            u4PagingStatus = CliPrintf (CliHandle, "!\r\n");

            if ((u4PagingStatus == CLI_FAILURE))
            {
                /* User pressed 'q' at more prompt, *
                 * no more print required, exit *
                 */
                break;
            }
        }

        i4IpdbCurrVlanId = i4IpdbVlanId;
        MEMCPY (IpdbCurrHostMac, IpdbHostMac, sizeof (tMacAddr));
        /* Getting the Next Entry From the Table */
    }
    while (nmhGetNextIndexFsIpDbStaticBindingTable (i4IpdbCurrVlanId,
                                                    &i4IpdbVlanId,
                                                    IpdbCurrHostMac,
                                                    &IpdbHostMac)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      :  IpdbShowRunningConfigDetails                        */
/*                                                                           */
/* Description        :  This function displays static binding               */
/*                       Configurations in IPDB binding Database             */
/*                                                                           */
/* Input(s)           :  CliHandle   -  CLIHandler                           */
/*                       i4IpdbVlanId    -  VlanId                           */
/*                       IpdbHostMac     -  HostMac Address                  */
/*                                                                           */
/* Output(s)          :  None                                                */
/*                                                                           */
/* Return Value(s)    :  CLI_SUCCESS/CLI_FAILURE                             */
/*****************************************************************************/
INT4
IpdbShowRunningConfigDetails (tCliHandle CliHandle, INT4 i4IpdbVlanId,
                              tMacAddr IpdbHostMac)
{
    CHR1                ac1IpString[IPDB_IP_STR_LEN] = { IPDB_ZERO };
    CHR1                ac1GwString[IPDB_IP_STR_LEN] = { IPDB_ZERO };
    INT4                i4IpdbBindingStatus = IPDB_ZERO;
    INT4                i4IpdbInIfIndex = IPDB_ZERO;
    UINT4               u4IpdbHostIp = IPDB_ZERO;
    UINT4               u4IpdbGatewayIp = IPDB_ZERO;
    UINT1               au1HostMacAddr[IPDB_MAC_STR_LEN] = { IPDB_ZERO };
    UINT1               au1NameStr[IPDB_MAX_PORT_NAME_LENGTH] = { IPDB_ZERO };
    CHR1               *pc1IpString = NULL;
    CHR1               *pc1GwString = NULL;

    pc1IpString = (CHR1 *) & ac1IpString[IPDB_ZERO];
    pc1GwString = (CHR1 *) & ac1GwString[IPDB_ZERO];

    if (nmhGetFsIpDbStaticBindingStatus (i4IpdbVlanId, IpdbHostMac,
                                         &i4IpdbBindingStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsIpDbStaticHostIp (i4IpdbVlanId, IpdbHostMac, &u4IpdbHostIp) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsIpDbStaticInIfIndex (i4IpdbVlanId, IpdbHostMac,
                                     &i4IpdbInIfIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsIpDbStaticGateway (i4IpdbVlanId, IpdbHostMac,
                                   &u4IpdbGatewayIp) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4IpdbBindingStatus == IPDB_ACTIVE)
    {
        CLI_CONVERT_IPADDR_TO_STR (pc1IpString, u4IpdbHostIp);

        IpdbPortGetIfName ((UINT4) i4IpdbInIfIndex, (INT1 *) au1NameStr);

        PrintMacAddress (IpdbHostMac, au1HostMacAddr);

        CliPrintf (CliHandle, "ip source binding %s vlan %d %s ",
                   au1HostMacAddr, i4IpdbVlanId, pc1IpString);

        CLI_CONVERT_IPADDR_TO_STR (pc1GwString, u4IpdbGatewayIp);

        CliPrintf (CliHandle, "interface %s "
                   "gateway %s \r\n", au1NameStr, pc1GwString);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IpdbCliSetTrace                                      */
/*                                                                           */
/* Description        : This function This function will enable/disable trace*/
/*                                                                           */
/* Input(s)           : i4TraceValue- Trace Flag                             */
/*                                                                           */
/* Output(s)          : Update the Global Structure                          */
/*                                                                           */
/* Return Value(s)    : CLI_FAILURE/CLI_SUCCESS                              */
/*****************************************************************************/
INT4
IpdbCliSetTrace (INT4 i4TraceValue, UINT1 u1Action)
{
    UINT4               u4ErrCode = SNMP_ERR_NO_ERROR;
    INT4                i4CurrTrcLevel = IPDB_ZERO;
    INT4                i4NewTrcLevel = IPDB_ZERO;

    nmhGetFsIpDbTraceLevel (&i4CurrTrcLevel);

    if (u1Action == IPDB_ENABLED)
    {
        i4NewTrcLevel = i4TraceValue;
    }
    else
    {
        i4NewTrcLevel = (i4CurrTrcLevel & (~i4TraceValue));
    }

    if (nmhTestv2FsIpDbTraceLevel (&u4ErrCode, (UINT4) i4NewTrcLevel)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetFsIpDbTraceLevel ((UINT4) i4NewTrcLevel);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IssIpdbShowDebugging                               */
/*                                                                           */
/*     DESCRIPTION      : This function prints IPDB Debug level              */
/*                        for interfaces                                     */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
IssIpdbShowDebugging (tCliHandle CliHandle)
{
    INT4                i4TraceLevel = IPDB_ZERO;

    nmhGetFsMIIpDbTraceLevel (&i4TraceLevel);

    if (i4TraceLevel == IPDB_ZERO)
    {
        return;
    }

    CliPrintf (CliHandle, "\r\n end");
    if (i4TraceLevel & IPDB_FN_ENTRY)
    {
        CliPrintf (CliHandle, "\r\n debug ip binding database entry");
    }
    if (i4TraceLevel & IPDB_FN_EXIT)
    {
        CliPrintf (CliHandle, "\r\n debug ip binding database exit");
    }
    if (i4TraceLevel & IPDB_DBG_TRC)
    {
        CliPrintf (CliHandle, "\r\n debug ip binding database debug");
    }
    if (i4TraceLevel & IPDB_FAIL_TRC)
    {
        CliPrintf (CliHandle, "\r\n debug ip binding database fail");
    }
    CliPrintf (CliHandle, "\n!\r\n");
    return;
}

/*****************************************************************************/
/* Function Name      : IpdbCliShowBindingCount                              */
/*                                                                           */
/* Description        : This function Shows Total no.of Bindings in the vlan */
/*                                                                           */
/* Input(s)           : CliHandle    -  CLIHandler                           */
/*                      pu1CxtName   -  Context Name                         */
/*                      i4IpdbVlanId -  Vlan Identifier                      */
/*                      i4Type       -  Specifies Global/Vlan statistics     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_FAILURE/CLI_SUCCESS                              */
/*****************************************************************************/
INT4
IpdbCliShowBindingCount (tCliHandle CliHandle, UINT1 *pu1CxtName,
                         INT4 i4IpdbVlanId, INT4 i4Type)
{
    INT4                i4IpdbGetVlanId = IPDB_ZERO;
    INT4                i4IpdbNextVlanId = IPDB_ZERO;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4ContextId = IPDB_ZERO;
    UINT1               u1CxtFlag = IPDB_FALSE;
    UINT1               u1VlanFlag = IPDB_FALSE;
    INT4                i4CliCxtId = IPDB_ZERO;
    INT4                i4IpdbNextCxtId = IPDB_ZERO;
    INT4                i4IpdbGetCxtId = IPDB_ZERO;
    INT4                i4RetValue = 0;

    UNUSED_PARAM (i4RetValue);

    if (pu1CxtName != NULL)
    {
        /* If Switch-name is given then get the Context-Id */
        if (IpDbVcmIsSwitchExist (pu1CxtName, &u4ContextId) != VCM_TRUE)
        {
            CliPrintf (CliHandle, "\r%% Switch %s Does not exist.\r\n",
                       pu1CxtName);
            return CLI_FAILURE;
        }
        i4CliCxtId = (UINT4) u4ContextId;
        /*Set the Context Flag */
        u1CxtFlag = IPDB_TRUE;
    }
    if (i4IpdbVlanId != IPDB_ZERO)
    {
        /*Set the Vlan Flag */
        u1VlanFlag = IPDB_TRUE;
    }
    if (i4Type != IPDB_ZERO)
    {
        /* Calling the function to get the Global Binding information
         * of Vlan statistics.*/
        IpdbCliShowGlobalBindingCount (CliHandle);
    }
    else
    {
        CliPrintf (CliHandle, "\r\n                            "
                   " VLAN Binding count Information\r\n");
        CliPrintf (CliHandle, "                           "
                   "--------------------------------\r\n");
        /*Get the first Entry of the IPDB Interface table */
        if (nmhGetFirstIndexFsMIIpDbInterfaceTable
            (&i4IpdbGetCxtId, &i4IpdbGetVlanId) != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }

        do
        {
            /*Select the Context */
            i4RetValue = IpDbUtilSetContext (i4IpdbGetCxtId);
            /*Check weather the Vlan Flag is set or not */
            if (u1VlanFlag == IPDB_TRUE)
            {
                /* If the Vlan-Id Given Through CLI and Vlan-Id in the
                 * binding table matches then Get the Binding Information.*/
                if (i4IpdbVlanId == i4IpdbGetVlanId)
                {
                    /* Calling the function to get the Statistics information
                     * about the particular Vlan.*/
                    IpdbCliPrintVlanBinding (CliHandle, i4IpdbGetCxtId,
                                             i4IpdbVlanId, &u4PagingStatus);
                }
            }
            /*Check weather the Context Flag is set or not */
            else if (u1CxtFlag == IPDB_TRUE)
            {
                /* If the Context-Id Given Through CLI and Context-Id in the
                 * binding table matches then Get the Binding Information.*/
                if (i4CliCxtId == i4IpdbGetCxtId)
                {
                    /* Calling the function to get the Statistics information
                     * about the particular Vlan for the selected context.*/
                    IpdbCliPrintVlanBinding (CliHandle, i4IpdbGetCxtId,
                                             i4IpdbGetVlanId, &u4PagingStatus);
                }
            }
            else
            {
                /* Calling the function to get the Statistics information
                 * about the all binding tables.*/
                IpdbCliPrintVlanBinding (CliHandle, i4IpdbGetCxtId,
                                         i4IpdbGetVlanId, &u4PagingStatus);

            }
            if (u4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt, *
                 * no more print required, exit *
                 */
                break;
            }
            /*Release the Context */
            IpDbUtilResetContext ();
            i4IpdbNextCxtId = i4IpdbGetCxtId;
            i4IpdbNextVlanId = i4IpdbGetVlanId;
            /* Getting the Next Entry From the Table */
        }
        while (nmhGetNextIndexFsMIIpDbInterfaceTable
               (i4IpdbNextCxtId, &i4IpdbGetCxtId, i4IpdbNextVlanId,
                &i4IpdbGetVlanId) == SNMP_SUCCESS);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IpdbCliPrintVlanBinding                              */
/*                                                                           */
/* Description        : This function displays Total no.of Bindings          */
/*                       for particular Vlan                                 */
/*                                                                           */
/* Input(s)           : CliHandle -  CLIHandler                              */
/*                      i4IpdbVlanId -  Vlan Identifier                      */
/*                      i4ContextId   -  Context Identifier                  */
/*                                                                           */
/* Output(s)          : pu4PagingStatus - Paging status                      */
/*                                                                           */
/* Return Value(s)    : CLI_FAILURE/CLI_SUCCESS                              */
/*****************************************************************************/
INT4
IpdbCliPrintVlanBinding (tCliHandle CliHandle, INT4 i4ContextId,
                         INT4 i4IpdbVlanId, UINT4 *pu4PagingStatus)
{
    UINT4               u4IpDbIntfNoOfVlanBindings = IPDB_ZERO;
    UINT4               u4IpDbIntfNoOfVlanStaticBindings = IPDB_ZERO;
    UINT4               u4IpDbIntfNoOfVlanDHCPBindings = IPDB_ZERO;
    UINT4               u4IpDbIntfNoOfVlanPPPBindings = IPDB_ZERO;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN] = { IPDB_ZERO };

    if (nmhGetFsIpDbIntfNoOfVlanBindings (i4IpdbVlanId,
                                          &u4IpDbIntfNoOfVlanBindings) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsIpDbIntfNoOfVlanStaticBindings
        (i4IpdbVlanId, &u4IpDbIntfNoOfVlanStaticBindings) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsIpDbIntfNoOfVlanDHCPBindings (i4IpdbVlanId,
                                              &u4IpDbIntfNoOfVlanDHCPBindings)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsIpDbIntfNoOfVlanPPPBindings (i4IpdbVlanId,
                                             &u4IpDbIntfNoOfVlanPPPBindings) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /*Get the Context Name with respect to the Context-Id */
    VcmGetAliasName (i4ContextId, au1ContextName);

    CliPrintf (CliHandle, "\r\nSwitch : %s\r\n\n", au1ContextName);

    CliPrintf (CliHandle, "VLAN  : %ld\r\n", i4IpdbVlanId);

    CliPrintf (CliHandle, "Number of Bindings         : %ld\r\n",
               u4IpDbIntfNoOfVlanBindings);

    CliPrintf (CliHandle, "Number of Static Bindings  : %ld\r\n",
               u4IpDbIntfNoOfVlanStaticBindings);

    CliPrintf (CliHandle, "Number of DHCP Bindings    : %ld\r\n",
               u4IpDbIntfNoOfVlanDHCPBindings);

    CliPrintf (CliHandle, "Number of PPP Bindings     : %ld\r\n",
               u4IpDbIntfNoOfVlanPPPBindings);

    *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IpdbCliShowGlobalBindingCount                        */
/*                                                                           */
/* Description        : This function Shows Total no.of Bindings in the vlan */
/*                                                                           */
/* Input(s)           : CliHandle    -  CLIHandler                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_FAILURE/CLI_SUCCESS                              */
/*****************************************************************************/
INT4
IpdbCliShowGlobalBindingCount (tCliHandle CliHandle)
{
    UINT4               u4IpdbNoOfBindings = IPDB_ZERO;
    UINT4               u4IpdbNoOfStaticBindings = IPDB_ZERO;
    UINT4               u4IpdbNoOfDHCPBindings = IPDB_ZERO;
    UINT4               u4IpdbNoOfPPPBindings = IPDB_ZERO;

    CliPrintf (CliHandle, "\r\n                            "
               "Global Binding Count Information\r\n");
    CliPrintf (CliHandle, "                            "
               "--------------------------------\r\n");

    if (nmhGetFsIpDbNoOfBindings (&u4IpdbNoOfBindings) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsIpDbNoOfStaticBindings (&u4IpdbNoOfStaticBindings) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsIpDbNoOfDHCPBindings (&u4IpdbNoOfDHCPBindings) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsIpDbNoOfPPPBindings (&u4IpdbNoOfPPPBindings) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "Number of Bindings        : %ld\r\n",
               u4IpdbNoOfBindings);

    CliPrintf (CliHandle, "Number of Static Bindings : %ld\r\n",
               u4IpdbNoOfStaticBindings);

    CliPrintf (CliHandle, "Number of DHCP Bindings   : %ld\r\n",
               u4IpdbNoOfDHCPBindings);

    CliPrintf (CliHandle, "Number of PPP Bindings    : %ld\r\n",
               u4IpdbNoOfPPPBindings);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpdbCliSetIpSrcGuardStatus                         */
/*                                                                           */
/*     DESCRIPTION      : This function is used to configure the IP source   */
/*                        guard status on a particular interface index.      */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        u4IfIndex - Interface index                        */
/*                        i4IpsgStatus - IP source guard status              */
/*                                           IPDB_IPSG_DISABLE /             */
/*                                           IPDB_IPSG_IP_MODE /             */
/*                                           IPDB_IPSG_IP_MAC_MODE           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
IpdbCliSetIpSrcGuardStatus (tCliHandle CliHandle, UINT4 u4IfIndex,
                            INT4 i4IpsgStatus)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    if (nmhTestv2FsIpDbSrcGuardStatus (&u4ErrorCode, u4IfIndex, i4IpsgStatus)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set IP source guard status\r\n");
        return CLI_FAILURE;
    }

    nmhSetFsIpDbSrcGuardStatus (u4IfIndex, i4IpsgStatus);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpdbCliShowIpSrcGuardStatus                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays the IP source guard status  */
/*                        for a particular interface index (or) for all      */
/*                        interfaces.                                        */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        u4IfIndex - Interface index                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
IpdbCliShowIpSrcGuardStatus (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    INT4                i4IfIndex = IPDB_ZERO;
    INT4                i4NextIfIndex = IPDB_ZERO;
    INT4                i4RetVal = SNMP_FAILURE;

    CliPrintf (CliHandle, "\r\nInterface          IP Source guard Status");
    CliPrintf (CliHandle, "\r\n---------          ----------------------\r\n");

    if (u4IfIndex == IPDB_ZERO)
    {
        if (nmhGetFirstIndexFsIpDbSrcGuardConfigTable (&i4IfIndex)
            == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }

        do
        {
            IpdbCliShowIpsgStatus (CliHandle, (UINT4) i4IfIndex);

            i4RetVal =
                nmhGetNextIndexFsIpDbSrcGuardConfigTable (i4IfIndex,
                                                          &i4NextIfIndex);

            i4IfIndex = i4NextIfIndex;

        }
        while (i4RetVal == SNMP_SUCCESS);
    }
    else
    {
        if (nmhValidateIndexInstanceFsIpDbSrcGuardConfigTable
            ((INT4) u4IfIndex) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Entry doesn't exists \r\n");
            return CLI_FAILURE;
        }
        IpdbCliShowIpsgStatus (CliHandle, u4IfIndex);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpdbCliShowIpsgStatus                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays the IP source guard status  */
/*                        for a particular interface index.                  */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        u4IfIndex - Interface index                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
IpdbCliShowIpsgStatus (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    INT1               *piIfName = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { IPDB_ZERO };
    INT4                i4IpsgStatus = IPDB_ZERO;

    nmhGetFsIpDbSrcGuardStatus (u4IfIndex, &i4IpsgStatus);

    piIfName = (INT1 *) &au1IfName[IPDB_ZERO];
    CfaCliGetIfName (u4IfIndex, piIfName);

    if (i4IpsgStatus == IPDB_IPSG_DISABLE)
    {
        CliPrintf (CliHandle, "%-13s Disable", piIfName);
    }
    else if (i4IpsgStatus == IPDB_IPSG_IP_MODE)
    {
        CliPrintf (CliHandle, "%-13s ip", piIfName);
    }
    else if (i4IpsgStatus == IPDB_IPSG_IP_MAC_MODE)
    {
        CliPrintf (CliHandle, "%-13s ip-mac", piIfName);
    }
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IpDbCliSelectContextOnMode                       */
/*                                                                           */
/*    Description         : This function is used to check the Mode of       */
/*                          the command and also if it a Config Mode         */
/*                          command it will do SelectContext for the         */
/*                          Context.                                         */
/*                                                                           */
/*    Input(s)            : u4Cmd - CLI Command.                             */
/*                                                                           */
/*    Output(s)           : CliHandle - Contains error messages.             */
/*                          pu4ContextId - Context Id.                       */
/*                                                                           */
/*    Returns            : IPDB_SUCCESS/IPDB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IpDbCliSelectContextOnMode (tCliHandle CliHandle, UINT4 u4Cmd,
                            UINT4 *pu4Context)
{
    UINT4               u4ContextId = IPDB_ZERO;

    /* For debug commands the context-name will be not be applicable as 
     * they are global configuration and per context based configuration */
    if ((u4Cmd != IPDB_STATIC_BINDING_ADD) &&
        (u4Cmd != IPDB_STATIC_BINDING_DEL))
    {
        return IPDB_SUCCESS;
    }

    *pu4Context = IPDB_DEFAULT_CONTEXT;

    /* Get the Context-Id from the pCliContext structure */
    u4ContextId = CLI_GET_CXT_ID ();

    /* Check if the command is a switch mode command */
    if (u4ContextId != IPDB_INVALID_CXT_ID)
    {
        /* Switch-mode Command */
        *pu4Context = u4ContextId;
    }

    if (IpDbUtilSetContext (u4ContextId) == IPDB_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Virtual Switch.\r\n ");
        return IPDB_FAILURE;
    }

    return IPDB_SUCCESS;
}

#endif /*_CFACLI_C */
/*                                                                           */
/***************************** END OF FILE ***********************************/
/*                                                                           */
