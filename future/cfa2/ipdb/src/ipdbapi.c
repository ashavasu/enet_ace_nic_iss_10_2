/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: ipdbapi.c,v 1.8 2014/07/05 11:19:29 siva Exp $                 */
/*****************************************************************************/
/*    FILE  NAME            : ipdbapi.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : IP Binding Database management                 */
/*    MODULE NAME           : Ip Binding Database management API Module      */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains APIs provided by the        */
/*                            IP Binding Database management module          */
/*---------------------------------------------------------------------------*/

#include "ipdbinc.h"
#include "ipdbextn.h"

/*****************************************************************************/
/* Function Name      : IpdbApiUpdateBindingEntry                            */
/*                                                                           */
/* Description        : This function creates a binding entry, (if it is not */
/*                      there; If the entry is already there, it updates the */
/*                      entry, with new values provided) or deletes the      */
/*                      binding entry                                        */
/*                                                                           */
/* Input(s)           : pIpDbEntry - Pointer to the binding entry info.      */
/*                      u4NoOfGWEntries - Number of gateways alloed for host */
/*                      pIpDbGateway - Gateway IP entries                    */
/*                      u1Status - CREATE/DELETE                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IPDB_SUCCESS/IPDB_FAILURE                            */
/*****************************************************************************/
INT4
IpdbApiUpdateBindingEntry (tIpDbEntry * pIpDbEntry, UINT4 u4NoOfGWEntries,
                           tIpDbGateway * pIpDbGateway, UINT1 u1Status)
{
    /* ASSUMPTION: As of now, we are handling only default gateway. so 
     * u4NoOfGWEntries is always assumed to be one */

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_ENTRY, IPDB_MODULE_NAME,
                   "IpdbApiUpdateBindingEntry: Entering the function %s \r\n",
                   __FUNCTION__);

    IPDB_LOCK ();

    if (u1Status == IPDB_CREATE_ENTRY)
    {
        /* If the request is CREATE, create the entry or update the already 
         * existing entry */
        if (IpdbProcUpdateBindingEntry (pIpDbEntry, u4NoOfGWEntries,
                                        pIpDbGateway) != IPDB_SUCCESS)
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "IpdbApiUpdateBindingEntry: Failed to create/update "
                      "entry  \r\n");

            IPDB_UNLOCK ();
            return IPDB_FAILURE;
        }
    }
    if (u1Status == IPDB_DELETE_ENTRY)
    {
        /* As an extra security, Incoming interface index and/or Host IP 
         * can be passed. If it is passed, the entry will be deleted,
         * only after the verification against the InIndex field in the 
         * binding entry */

        /* If the request is DELETE, delete the entry */
        if (IpdbProcDeleteBindingEntry (pIpDbEntry->u4ContextId,
                                        pIpDbEntry->HostMac,
                                        pIpDbEntry->VlanId,
                                        pIpDbEntry->u4InIfIndex,
                                        pIpDbEntry->u4HostIp,
                                        pIpDbEntry->u1BindingType)
            != IPDB_SUCCESS)
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "IpdbApiUpdateBindingEntry: Failed to delete entry  "
                      "\r\n");

            IPDB_UNLOCK ();
            return IPDB_FAILURE;
        }
    }

    IPDB_UNLOCK ();

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "IpdbApiUpdateBindingEntry: Exiting the function %s \r\n",
                   __FUNCTION__);

    return IPDB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IpdbApiDeleteBindingEntries                          */
/*                                                                           */
/* Description        : This function is used to delete the binding entries  */
/*                      as a bunch. If VlanId is 0, it will delete all the   */
/*                      binding entries of the corresponding module          */
/*                                                                           */
/* Input(s)           : u1BindingType - The module owning the Binding        */
/*                      u4ContextID   - Context Identifier                   */
/*                      VlanId - VLAN Identifier                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IPDB_SUCCESS/IPDB_FAILURE                            */
/*****************************************************************************/
INT4
IpdbApiDeleteBindingEntries (UINT1 u1BindingType, UINT4 u4ContextID,
                             tVlanId VlanId)
{
    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_ENTRY, IPDB_MODULE_NAME,
                   "IpdbApiDeleteBindingEntries: Entering the function %s \r\n",
                   __FUNCTION__);

    IPDB_LOCK ();

    /* Delete the entries */
    if (IpdbProcDeleteEntries (u1BindingType, u4ContextID, VlanId)
        != IPDB_SUCCESS)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "IpdbApiDeleteBindingEntries: Error while deleting the "
                  "binding entries \r\n");

        IPDB_UNLOCK ();
        return IPDB_FAILURE;
    }

    IPDB_UNLOCK ();

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "IpdbApiDeleteBindingEntries: Exiting the function %s \r\n",
                   __FUNCTION__);

    return IPDB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IpdbCreatePort                                   */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever any port is created by */
/*                          mgmt module                                      */
/*                                                                           */
/*    Input(s)            : u2IfIndex    - The Number of the Port            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : IPDB_SUCCESS                                      */
/*                         IPDB_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
IpdbCreatePort (UINT4 u4IfIndex)
{
    INT4                i4RetVal = IPDB_FAILURE;

    i4RetVal = IpdbPostCfgMessage (u4IfIndex, IPDB_PORT_CREATE_EVENT);

    if (i4RetVal == IPDB_SUCCESS)
    {
        L2_SYNC_TAKE_SEM ();
    }

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IpdbDeletePort                                   */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever any port is Deleted by */
/*                          mgmt module                                      */
/*                                                                           */
/*    Input(s)            : u2IfIndex    - The Number of the Port            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : IPDB_SUCCESS                                      */
/*                         IPDB_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
IpdbDeletePort (UINT4 u4IfIndex)
{
    INT4                i4RetVal = IPDB_FAILURE;

    i4RetVal = IpdbPostCfgMessage (u4IfIndex, IPDB_PORT_DELETE_EVENT);

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IpdbApiProcessPktWithPortList                    */
/*                                                                           */
/*    Description         : This function is used to send out a packet on    */
/*                          a given portlist                                 */
/*                                                                           */
/*    Input(s)            : pBuf       - The Packet to be sent out           */
/*                          u4ContextId - Context ID                         */
/*                          VlanTag    - VLAN tag informations of the Packet */
/*                          u4InPort   - The port through which packet came  */
/*                          DstMacAddr - Destination MAC Address             */
/*                          SrcMacAddr - Source MAC Address                  */
/*                          PortList   - List of the outgoing port           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : IPDB_SUCCESS/IPDB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
IpdbApiProcessPktWithPortList (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ContextId,
                               tVlanTag VlanTag, UINT4 u4InPort,
                               tMacAddr DstMacAddr, tMacAddr SrcMacAddr,
                               tPortList PortList)
{
    tPortList          *pFwdPortList = NULL;
    UINT1              *pOutPortList = NULL;
    tPortList          *pOutPortListL2Iwf = NULL;
    tCRU_BUF_CHAIN_DESC *pDupBuf = NULL;
    INT4                i4Result = IPDB_ZERO;
    INT4                i4RetVal = IPDB_ZERO;
    INT4                i4ByteIndex = IPDB_MINUS_ONE;
    UINT4               u4IfIndex = IPDB_ONE;
    UINT1               u1Result = IPDB_ZERO;
    pFwdPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pFwdPortList == NULL)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "IpdbApiProcessPktWithPortList: "
                  "Error in allocating memory for pFwdPortList\r\n");
        return IPDB_FAILURE;
    }
    MEMSET (*pFwdPortList, 0, sizeof (tPortList));

    pOutPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pOutPortList == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pFwdPortList);
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "IpdbApiProcessPktWithPortList: "
                  "Error in allocating memory for pOutPortList\r\n");
        return IPDB_FAILURE;
    }
    MEMSET (pOutPortList, 0, sizeof (tLocalPortList));

    /* Get the OutPortList information from the VLAN */
    i4Result = VlanGetFwdPortList (u4ContextId, (UINT2) u4InPort,
                                   SrcMacAddr, DstMacAddr,
                                   VlanTag.OuterVlanTag.u2VlanId, pOutPortList);

    /* As of now, multicast packets are not handled in this function */
    if ((IPDB_IS_MCASTADDR (DstMacAddr) == IPDB_TRUE) &&
        (IPDB_IS_BCASTADDR (DstMacAddr) == IPDB_FALSE))
    {
        FsUtilReleaseBitList ((UINT1 *) pFwdPortList);
        UtilPlstReleaseLocalPortList (pOutPortList);
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "IpdbApiProcessPktWithPortList: multicast packets are not "
                  "handled in this function \r\n");
        return IPDB_FAILURE;
    }

    if (IPDB_IS_BCASTADDR (DstMacAddr) == IPDB_TRUE)
    {
        /* If the packet is a broadcast packet and the given portlist is not 
         * empty, use that portlist to forward packets out. If the given 
         * portlist is empty then use the portlist updated by 
         * VlanGetFwdPortList function */
        if (i4Result != VLAN_SUCCESS)
        {
            FsUtilReleaseBitList ((UINT1 *) pFwdPortList);
            UtilPlstReleaseLocalPortList (pOutPortList);
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "The passed portlist is null, and the VLAN module "
                      "failed to get the out portlist for Bcast Pkt\r\n");
            return IPDB_FAILURE;
        }
        else if (PortList != NULL)
        {
            MEMCPY (*pFwdPortList, PortList, sizeof (tPortList));
        }
        else                    /* If Port List is empty and VlanGetFwdPortList was successful */
        {
            MEMCPY (*pFwdPortList, pOutPortList, sizeof (tPortList));
        }
    }
    else
    {
        /* If the packet is a unicast packet, use the portlist returned by the 
         * VlanGetFwdPortList function (which corresponds to a static entry or 
         * an FDB entry). If that fails, use the given portlist. */
        if (i4Result != VLAN_SUCCESS)
        {
            FsUtilReleaseBitList ((UINT1 *) pFwdPortList);
            UtilPlstReleaseLocalPortList (pOutPortList);

            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "The passed portlist is null, and the VLAN module "
                      "failed to get the out portlist for Ucast pkt\r\n");
            return IPDB_FAILURE;
        }
        else if (i4Result == VLAN_SUCCESS)
        {
            IPDB_IS_NULL_PORT_LIST ((pOutPortList), sizeof (tPortList),
                                    u1Result);
            if (u1Result == IPDB_TRUE)
            {
                pOutPortListL2Iwf =
                    (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

                if (pOutPortListL2Iwf == NULL)
                {
                    IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                              "IpdbApiProcessPktWithPortList: "
                              "Error in allocating memory for pFwdPortList\r\n");
                    FsUtilReleaseBitList ((UINT1 *) pFwdPortList);
                    UtilPlstReleaseLocalPortList (pOutPortList);
                    return IPDB_FAILURE;
                }
                MEMSET (*pOutPortListL2Iwf, 0, sizeof (tPortList));

                /* Since the Portlist is not given from the IPDB,
                 * and since the VlanGetFwdPortList failed, packets
                 * should be forwarded to all the vlan member ports.*/
                L2IwfGetVlanEgressPorts (VlanTag.OuterVlanTag.u2VlanId,
                                         *pOutPortListL2Iwf);

                MEMCPY (*pFwdPortList, *pOutPortListL2Iwf, sizeof (tPortList));
                FsUtilReleaseBitList ((UINT1 *) pOutPortListL2Iwf);
            }
            else
            {
                MEMCPY (*pFwdPortList, pOutPortList, sizeof (tPortList));
            }
        }
        else                    /*PortList != NULL */
        {
            MEMCPY (*pFwdPortList, PortList, sizeof (tPortList));
        }
    }

    /* Exclude the InPort */
    OSIX_BITLIST_RESET_BIT ((*pFwdPortList), u4InPort, sizeof (tPortList));

    /* Scan through the portlist and send the packet out on the ports. */
    while (u4IfIndex <= (sizeof (tPortList) * BITS_PER_BYTE))
    {
        /* If the bitmap contain 8 concecutive zeros, starting from position
         * zero of the byte, then entire byte will be zero, and there is no
         * need for looping through it */
        if (((u4IfIndex - IPDB_ONE) % BITS_PER_BYTE) == IPDB_ZERO)
        {
            i4ByteIndex++;
        }

        if ((*pFwdPortList)[i4ByteIndex] == IPDB_ZERO)
        {
            u4IfIndex = u4IfIndex + BITS_PER_BYTE;
            continue;
        }

        OSIX_BITLIST_IS_BIT_SET ((*pFwdPortList),
                                 u4IfIndex, sizeof (tPortList) - IPDB_ONE,
                                 u1Result);

        if (u1Result == OSIX_TRUE)
        {
            /* Do Egress filtering */
            i4RetVal = VlanApiDoEgressFiltering ((UINT2) u4IfIndex,
                                                 VlanTag.OuterVlanTag.u2VlanId,
                                                 DstMacAddr);
            if (i4RetVal == VLAN_SUCCESS)
            {
                /* If the packet is allowed, duplicate the buffer and 
                 * send the packet out. */
                pDupBuf = IpdbUtilDuplicateBuf (pBuf);

                if (pDupBuf != NULL)
                {
                    VlanTransmitCfmFrame (pDupBuf, u4ContextId,
                                          (UINT2) u4IfIndex, VlanTag,
                                          VLAN_CFM_FRAME);
                }
            }
        }
        u4IfIndex++;
    }

    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    FsUtilReleaseBitList ((UINT1 *) pFwdPortList);
    UtilPlstReleaseLocalPortList (pOutPortList);

    return IPDB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IpdbApiDoIpSrcGuardValidation                    */
/*                                                                           */
/*    Description         : This function is used for IP source guard        */
/*                          validation.                                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context ID                         */
/*                          VlanId     - VLAN ID                             */
/*                          HostMac    - Host MAC address                    */
/*                          u4HostIp   - Host IP address                     */
/*                          u4InPort   - The port through which packet came  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : IPDB_SUCCESS/IPDB_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
IpdbApiDoIpSrcGuardValidation (UINT4 u4ContextId, tVlanId VlanId,
                               tMacAddr HostMac, UINT4 u4HostIp, UINT4 u4InPort)
{
    tPortCtrlEntry     *pPortCtrlEntry = NULL;

    IPDB_TRC (IPDB_TRC_FLAG, IPDB_FN_ENTRY, IPDB_MODULE_NAME,
              "IpdbApiDoIpSrcGuardValidation: Entering the function\r\n");

    IPDB_LOCK ();

    IpDbGetPortCtrlEntry (u4InPort, &pPortCtrlEntry);

    if (pPortCtrlEntry == NULL)
    {
        IPDB_UNLOCK ();
        IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                       "IpdbApiDoIpSrcGuardValidation: Getting the port "
                       "entry of port %d failed.\r\n", u4InPort);
        return OSIX_SUCCESS;
    }

    if (pPortCtrlEntry->u1IpsgStatus == IPDB_IPSG_IP_MAC_MODE)
    {
        if (IpdbUtilIsIPDBEntryPresent (u4ContextId, VlanId, HostMac,
                                        u4HostIp, u4InPort) == IPDB_FALSE)
        {
            IPDB_UNLOCK ();
            IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                           "IpdbApiDoIpSrcGuardValidation: IP Source guard "
                           "validation fails for port : %d\r\n", u4InPort);
            return OSIX_FAILURE;
        }
    }

    IPDB_UNLOCK ();

    IPDB_TRC (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
              "IpdbApiDoIpSrcGuardValidation: Exiting the function \r\n");

    return OSIX_SUCCESS;
}

/*                                                                           */
/***************************** END OF FILE ***********************************/
/*                                                                           */
