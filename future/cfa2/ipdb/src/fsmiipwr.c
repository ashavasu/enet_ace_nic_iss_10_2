# include  "lr.h" 
# include  "fssnmp.h" 
# include  "fsmiiplw.h"
# include  "fsmiipwr.h"
# include  "fsmiipdb.h"


VOID RegisterFSMIIP ()
{
	SNMPRegisterMib (&fsmiipOID, &fsmiipEntry, SNMP_MSR_TGR_FALSE);
	SNMPAddSysorEntry (&fsmiipOID, (const UINT1 *) "fsmiipdb");
}



VOID UnRegisterFSMIIP ()
{
	SNMPUnRegisterMib (&fsmiipOID, &fsmiipEntry);
	SNMPDelSysorEntry (&fsmiipOID, (const UINT1 *) "fsmiipdb");
}

INT4 FsMIIpDbNoOfBindingsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsMIIpDbNoOfBindings(&(pMultiData->u4_ULongValue)));
}
INT4 FsMIIpDbNoOfStaticBindingsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsMIIpDbNoOfStaticBindings(&(pMultiData->u4_ULongValue)));
}
INT4 FsMIIpDbNoOfDHCPBindingsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsMIIpDbNoOfDHCPBindings(&(pMultiData->u4_ULongValue)));
}
INT4 FsMIIpDbNoOfPPPBindingsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsMIIpDbNoOfPPPBindings(&(pMultiData->u4_ULongValue)));
}
INT4 FsMIIpDbTraceLevelGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsMIIpDbTraceLevel(&(pMultiData->i4_SLongValue)));
}
INT4 FsMIIpDbTraceLevelSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsMIIpDbTraceLevel(pMultiData->i4_SLongValue));
}


INT4 FsMIIpDbTraceLevelTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsMIIpDbTraceLevel(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsMIIpDbTraceLevelDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsMIIpDbTraceLevel(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsMIIpDbStaticBindingTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsMIIpDbStaticBindingTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			(tMacAddr *)pNextMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsMIIpDbStaticBindingTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			*(tMacAddr *)pFirstMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList,
			(tMacAddr *)pNextMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
		pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
	return SNMP_SUCCESS;
}
INT4 FsMIIpDbStaticHostIpGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIIpDbStaticBindingTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIIpDbStaticHostIp(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIIpDbStaticInIfIndexGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIIpDbStaticBindingTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIIpDbStaticInIfIndex(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIIpDbStaticGatewayGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIIpDbStaticBindingTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIIpDbStaticGateway(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIIpDbStaticBindingStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIIpDbStaticBindingTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIIpDbStaticBindingStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIIpDbStaticHostIpSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMIIpDbStaticHostIp(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
		pMultiData->u4_ULongValue));

}

INT4 FsMIIpDbStaticInIfIndexSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMIIpDbStaticInIfIndex(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
		pMultiData->i4_SLongValue));

}

INT4 FsMIIpDbStaticGatewaySet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMIIpDbStaticGateway(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
		pMultiData->u4_ULongValue));

}

INT4 FsMIIpDbStaticBindingStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMIIpDbStaticBindingStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
		pMultiData->i4_SLongValue));

}

INT4 FsMIIpDbStaticHostIpTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMIIpDbStaticHostIp(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
		pMultiData->u4_ULongValue));

}

INT4 FsMIIpDbStaticInIfIndexTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMIIpDbStaticInIfIndex(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
		pMultiData->i4_SLongValue));

}

INT4 FsMIIpDbStaticGatewayTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMIIpDbStaticGateway(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
		pMultiData->u4_ULongValue));

}

INT4 FsMIIpDbStaticBindingStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMIIpDbStaticBindingStatus(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
		pMultiData->i4_SLongValue));

}

INT4 FsMIIpDbStaticBindingTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsMIIpDbStaticBindingTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsMIIpDbBindingTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsMIIpDbBindingTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			(tMacAddr *)pNextMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsMIIpDbBindingTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			*(tMacAddr *)pFirstMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList,
			(tMacAddr *)pNextMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
		pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
	return SNMP_SUCCESS;
}
INT4 FsMIIpDbHostBindingTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIIpDbBindingTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIIpDbHostBindingType(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIIpDbHostIpGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIIpDbBindingTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIIpDbHostIp(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIIpDbHostInIfIndexGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIIpDbBindingTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIIpDbHostInIfIndex(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIIpDbHostRemLeaseTimeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIIpDbBindingTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIIpDbHostRemLeaseTime(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIIpDbHostBindingIDGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIIpDbBindingTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIIpDbHostBindingID(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
		&(pMultiData->u4_ULongValue)));

}

INT4 GetNextIndexFsMIIpDbGatewayIpTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsMIIpDbGatewayIpTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			(tMacAddr *)pNextMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue),
			&(pNextMultiIndex->pIndex[3].u4_ULongValue),
			&(pNextMultiIndex->pIndex[4].u4_ULongValue),
			&(pNextMultiIndex->pIndex[5].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsMIIpDbGatewayIpTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			*(tMacAddr *)pFirstMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList,
			(tMacAddr *)pNextMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList,
			pFirstMultiIndex->pIndex[2].i4_SLongValue,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue),
			pFirstMultiIndex->pIndex[3].u4_ULongValue,
			&(pNextMultiIndex->pIndex[3].u4_ULongValue),
			pFirstMultiIndex->pIndex[4].u4_ULongValue,
			&(pNextMultiIndex->pIndex[4].u4_ULongValue),
			pFirstMultiIndex->pIndex[5].u4_ULongValue,
			&(pNextMultiIndex->pIndex[5].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
		pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
	return SNMP_SUCCESS;
}
INT4 FsMIIpDbGatewayIpModeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIIpDbGatewayIpTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList),
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].u4_ULongValue,
		pMultiIndex->pIndex[4].u4_ULongValue,
		pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIIpDbGatewayIpMode(
		pMultiIndex->pIndex[0].i4_SLongValue,
		(*(tMacAddr *)pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList),
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].u4_ULongValue,
		pMultiIndex->pIndex[4].u4_ULongValue,
		pMultiIndex->pIndex[5].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}

INT4 GetNextIndexFsMIIpDbInterfaceTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsMIIpDbInterfaceTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsMIIpDbInterfaceTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsMIIpDbIntfNoOfVlanBindingsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIIpDbInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIIpDbIntfNoOfVlanBindings(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIIpDbIntfNoOfVlanStaticBindingsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIIpDbInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIIpDbIntfNoOfVlanStaticBindings(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIIpDbIntfNoOfVlanDHCPBindingsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIIpDbInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIIpDbIntfNoOfVlanDHCPBindings(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMIIpDbIntfNoOfVlanPPPBindingsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIIpDbInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIIpDbIntfNoOfVlanPPPBindings(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}

INT4 GetNextIndexFsMIIpDbSrcGuardConfigTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsMIIpDbSrcGuardConfigTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsMIIpDbSrcGuardConfigTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsMIIpDbSrcGuardStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMIIpDbSrcGuardConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMIIpDbSrcGuardStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMIIpDbSrcGuardStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMIIpDbSrcGuardStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIIpDbSrcGuardStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMIIpDbSrcGuardStatus(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMIIpDbSrcGuardConfigTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsMIIpDbSrcGuardConfigTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

