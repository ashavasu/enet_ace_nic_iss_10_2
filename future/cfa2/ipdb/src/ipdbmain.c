/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: ipdbmain.c,v 1.11 2015/10/05 12:21:35 siva Exp $                    */
/*****************************************************************************/
/*    FILE  NAME            : ipdbmain.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : IP Binding Database management                 */
/*    MODULE NAME           : Ip Binding Database management Main Module     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains init, deinit funtions       */
/*                            for IP Binding Database management             */
/*---------------------------------------------------------------------------*/

#include "ipdbinc.h"
#include "ipdbglob.h"

/*****************************************************************************/
/* Function Name      : IPDBMain                                             */
/*                                                                           */
/* Description        : This function is the main entry point function for   */
/*                      the IPDB MGMT task                                   */
/*                                                                           */
/* Input(s)           : pi1Param - unused                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IPDBMain (INT1 *pi1Param)
{
    UINT4               u4EventsRecvd = IPDB_ZERO;

    UNUSED_PARAM (pi1Param);
    IPDB_TRC_FLAG = IPDB_TRC_NONE;

    if (IpdbInit () == IPDB_FAILURE)
    {
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    IPDB_CURR_CONTEXT_ID = IPDB_DEFAULT_CONTEXT;
#ifdef SNMP_2_WANTED
    /* Register the IPDB Mib */
    IpdbUtilRegisterFsIpdbMib ();
#endif

    lrInitComplete (OSIX_SUCCESS);

    while (IPDB_TRUE)
    {
        if (IPDB_RECEIVE_EVENT ((IPDB_EVENT_ARRIVED | IPDB_TIMER_EXP_EVENT),
                                IPDB_EVENT_WAIT_FLAG, IPDB_ZERO,
                                &u4EventsRecvd) == OSIX_SUCCESS)
        {
            if ((u4EventsRecvd & IPDB_EVENT_ARRIVED) != IPDB_ZERO)
            {
                IpdbMainProcessEvent ();
            }

            if ((u4EventsRecvd & IPDB_TIMER_EXP_EVENT) != IPDB_ZERO)
            {
                IpdbMainProcessTmrEvent ();
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : IpdbInit                                             */
/*                                                                           */
/* Description        : This function is used for initializing               */
/*                      the IPDB MGMT task                                   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IPDB_SUCCESS/IPDB_FAILURE                            */
/*****************************************************************************/
INT4
IpdbInit (VOID)
{
    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_ENTRY, IPDB_MODULE_NAME,
                   "Entering the function %s \r\n", __FUNCTION__);

    /* Create semaphore for mutual exclusion */
    if (IPDB_CREATE_SEM (IPDB_SEM_NAME, IPDB_SEM_COUNT, IPDB_SEM_FLAGS,
                         &(IPDB_SEM_ID)) != OSIX_SUCCESS)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Creation of IPDB semaphore failed.\r\n");

        return IPDB_FAILURE;
    }
    /* Create Queue For handling Events */
    if (IPDB_CREATE_QUEUE (IPDB_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                           IPDB_QUEUE_DEPTH, &(IPDB_QUEUE_ID)) != OSIX_SUCCESS)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Creation of Queue for IPDB failed.\r\n");

        IpdbDeInit ();
        return IPDB_FAILURE;
    }

    /* Creating memory pools */
    if (IpdbSizingMemCreateMemPools () == IPDB_FAILURE)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Creation of Memory pools failed.\r\n");
        IpdbDeInit ();
        return IPDB_FAILURE;
    }

    /*Assigning mempool id's */
    IpdbAssignMempoolIds ();

    /* Create RBTree for static binding entries */
    IPDB_STATIC_RBTREE = IPDB_CREATE_RBTREE (IPDB_ZERO,
                                             IpdbUtilRBTreeStaticCmp);
    if (IPDB_STATIC_RBTREE == NULL)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Creation of RBTree for Static Binding Entries failed.\r\n");

        IpdbDeInit ();
        return IPDB_FAILURE;
    }

    /* Create RBTree based on Mac address for binding entries */
    IPDB_MAC_BINDING_RBTREE = IPDB_CREATE_RBTREE (IPDB_MAC_BIND_RBTREE_OFFSET,
                                                  IpdbUtilMacRBTreeBindCmp);
    if (IPDB_MAC_BINDING_RBTREE == NULL)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Creation of RBTree for Binding Entries failed.\r\n");

        IpdbDeInit ();
        return IPDB_FAILURE;
    }

    /* Create RBTree based on IP Address for binding entries */
    IPDB_IP_BINDING_RBTREE = IPDB_CREATE_RBTREE (IPDB_IP_BIND_RBTREE_OFFSET,
                                                 IpdbUtilIpRBTreeBindCmp);
    if (IPDB_IP_BINDING_RBTREE == NULL)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Creation of RBTree for Binding Entries failed.\r\n");

        IpdbDeInit ();
        return IPDB_FAILURE;
    }

    /* Create RBTree for interface entries */
    IPDB_INTF_RBTREE = IPDB_CREATE_RBTREE (IPDB_ZERO,
                                           IpdbUtilRBTreeIntfEntryCmp);
    if (IPDB_INTF_RBTREE == NULL)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Creation of RBTree for Interface Entries failed.\r\n");

        IpdbDeInit ();
        return IPDB_FAILURE;

    }

    /* Create RBTree for port control entries  */
    IPDB_PORTCTRL_RBTREE =
        IPDB_CREATE_RBTREE (IPDB_ZERO, IpdbUtilRBTreePortCtrlEntryCmp);

    if (IPDB_PORTCTRL_RBTREE == NULL)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Creation of RBTree for port control table failed \r\n");
        IpdbDeInit ();
        return IPDB_FAILURE;
    }

    /* DLL for VlanCtrl Table */
    TMO_DLL_Init (&IPDB_VLANCTRL_DLL_LIST);

    /* Create the timer list for IPDB task */
    if (TmrCreateTimerList (IPDB_TASK_NAME, IPDB_TIMER_EXP_EVENT, NULL,
                            &(IPDB_TIMER_LIST)) != TMR_SUCCESS)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Creation of IPDB Timer list failed.\r\n");

        IpdbDeInit ();
        return IPDB_FAILURE;
    }

    /* Used to create the port entries for all the ports */
    if (IpdbGetAllPorts () == IPDB_FAILURE)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Creation of Ports failed.\r\n");
        IpdbDeInit ();
        return IPDB_FAILURE;
    }

    gu1IsIpdbInitialised = IPDB_TRUE;

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);
    return IPDB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IpdbDeInit                                           */
/*                                                                           */
/* Description        : This function is  Deinit                             */
/*                      the IPDB MGMT task                                   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IpdbDeInit (VOID)
{
    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_ENTRY, IPDB_MODULE_NAME,
                   "Entering the function %s \r\n", __FUNCTION__);

    /* Delete the Semaphore created for IPDB task */
    if (IPDB_SEM_ID != IPDB_ZERO)
    {
        IPDB_DELETE_SEM (IPDB_ZERO, IPDB_SEM_NAME);
    }

    /* Delete the queue created for IPDB task */
    if (IPDB_QUEUE_ID != IPDB_ZERO)
    {
        IPDB_DELETE_QUEUE (IPDB_QUEUE_ID);
    }

    /* Deleting all the memory pools */
    IpdbSizingMemDeleteMemPools ();

    /* Delete all the data structures created for IPDB task */

    if (IPDB_STATIC_RBTREE != NULL)
    {
        IPDB_DELETE_RBTREE (IPDB_STATIC_RBTREE,
                            (tRBKeyFreeFn) IpdbUtilRbTreeStaticFree, IPDB_ZERO);
    }

    if (IPDB_MAC_BINDING_RBTREE != NULL)
    {
        IPDB_DELETE_RBTREE (IPDB_MAC_BINDING_RBTREE,
                            (tRBKeyFreeFn) IpdbUtilMacRbTreeBindingFree,
                            IPDB_ZERO);
    }

    if (IPDB_IP_BINDING_RBTREE != NULL)
    {
        IPDB_DELETE_RBTREE (IPDB_IP_BINDING_RBTREE,
                            (tRBKeyFreeFn) IpdbUtilIpRbTreeBindingFree,
                            IPDB_ZERO);
    }

    if (IPDB_INTF_RBTREE != NULL)
    {
        IPDB_DELETE_RBTREE (IPDB_INTF_RBTREE,
                            (tRBKeyFreeFn) IpdbUtilRbTreeIntfFree, IPDB_ZERO);
    }

    if (IPDB_PORTCTRL_RBTREE != NULL)
    {
        IPDB_DELETE_RBTREE (IPDB_PORTCTRL_RBTREE,
                            (tRBKeyFreeFn) IpdbUtilRbTreePortCtrlFree,
                            IPDB_ZERO);
    }

    /* Delete the Timerlist created for the IPDB task */
    if (IPDB_TIMER_LIST != IPDB_ZERO)
    {
        TmrDeleteTimerList (IPDB_TIMER_LIST);
    }

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IpdbGetAllPorts                                  */
/*                                                                           */
/*    Description         : This function scans through the L2IWF port table */
/*                          and issues create port indications.              */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : IPDB_SUCCESS/IPDB_FAILURE.                        */
/*                                                                           */
/*****************************************************************************/
INT4
IpdbGetAllPorts (VOID)
{
    UINT4               u4IfIndex = IPDB_ZERO;
    UINT2               u2PrevPort = IPDB_ZERO;
    UINT2               u2LocalPort = IPDB_ZERO;

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_ENTRY, IPDB_MODULE_NAME,
                   "Entering the function %s \r\n", __FUNCTION__);

    u2PrevPort = IPDB_ZERO;

    while (L2IwfGetNextValidPortForContext (IPDB_DEFAULT_CONTEXT,
                                            u2PrevPort, &u2LocalPort,
                                            &u4IfIndex) == L2IWF_SUCCESS)
    {
        if (IPDB_IS_PORT_CHANNEL (u4IfIndex) == IPDB_TRUE)
        {
            u2PrevPort = u2LocalPort;
            continue;
        }

        if (L2IwfIsPortInPortChannel (u4IfIndex) == L2IWF_SUCCESS)
        {
            u2PrevPort = u2LocalPort;
            continue;
        }

        if (IpDbUtilCreatePortCtrlEntry (u4IfIndex) == IPDB_FAILURE)
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "Addition of pPortCtrlEntry failed.\r\n");
            return IPDB_FAILURE;
        }

        u2PrevPort = u2LocalPort;
    }

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);
    return IPDB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IpdbMainProcessEvent                                 */
/*                                                                           */
/* Description        : This function handles the events.                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
IpdbMainProcessEvent (VOID)
{
    tIpDbQMsg          *pIpDbQMsg = NULL;
    tOsixMsg           *pOsixMsg = NULL;
    tVlanTag            VlanTag;

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_ENTRY, IPDB_MODULE_NAME,
                   "Entering the function %s \r\n", __FUNCTION__);

    MEMSET (&VlanTag, IPDB_ZERO, sizeof (tVlanTag));

    /* Take the lock, before processing the event */
    if (IPDB_LOCK () == SNMP_FAILURE)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Taking the protocol lock failed.\r\n");
        return;
    }

    /* Receive the event from the queue */
    while (IPDB_RECV_FROM_QUEUE (IPDB_QUEUE_ID, (UINT1 *) &pOsixMsg,
                                 OSIX_DEF_MSG_LEN, IPDB_ZERO) == OSIX_SUCCESS)
    {
        pIpDbQMsg = (tIpDbQMsg *) pOsixMsg;
        MEMCPY (&VlanTag, &(pIpDbQMsg->VlanTag), sizeof (tVlanTag));

        if (pIpDbQMsg->u4EventType == IPDB_VLAN_INTERFACE_EVENT)
        {
            /* The event indicates the deletion of a VLAN */
            /* Delete all the static bindings for that VLAN. 
             * DHCP Bindings should be deleted through L2DHCP Snooping module 
             * PPP Bindings should be deleted through PPPoE Snooping module */
            if (IpdbProcDeleteEntries (IPDB_STATIC_BINDING,
                                       pIpDbQMsg->u4ContextId,
                                       pIpDbQMsg->VlanId) != IPDB_SUCCESS)
            {
                IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                               "Error while deleting the binding entries on "
                               "VLAN %d \r\n", pIpDbQMsg->VlanId);
            }
            /* Delete the interface entry for that VLAN */
            if (IpdbIntfDeleteEntry (pIpDbQMsg->u4ContextId, pIpDbQMsg->VlanId)
                != IPDB_SUCCESS)
            {
                IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                               "Error while deleting the VLAN %d entry \r\n",
                               pIpDbQMsg->VlanId);
            }
        }
        else if (pIpDbQMsg->u4EventType == IPDB_ARP_PKT_ARRIVAL_EVENT)
        {
            /* This Event indicates the arrival of ARP packet */
            if (IpdbHandleArpPacket (pIpDbQMsg->pInQMsg, L2IWF_DEFAULT_CONTEXT,
                                     pIpDbQMsg->u4InPort,
                                     VlanTag) != IPDB_SUCCESS)
            {
                IPDB_TRC_ARG2 (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                               "Error in handling the ARP packet for VLAN %d"
                               "in Port %d\r\n", VlanTag.OuterVlanTag.u2VlanId,
                               pIpDbQMsg->u4InPort);
            }
        }
        else if ((pIpDbQMsg->u4EventType == IPDB_PORT_CREATE_EVENT)
                 || (pIpDbQMsg->u4EventType == IPDB_PORT_DELETE_EVENT))
        {
            IpdbProcessCfgMsgEvent (pIpDbQMsg);
        }

        /* Release the Q message */
        IPDB_RELEASE_MEM_BLOCK (IPDB_Q_POOL_ID, pIpDbQMsg);
    }

    /* Release the lock */
    IPDB_UNLOCK ();

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);
    return;
}

/*****************************************************************************/
/* Function Name      : IpdbMainProcessTmrEvent                              */
/*                                                                           */
/* Description        : This function handles the events.                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
IpdbMainProcessTmrEvent (VOID)
{
    tIpDbTimerEntry    *pExpiredTmr = NULL;
    tIpDbBindingEntry  *pIpDbBindingEntry = NULL;
    INT2                i2OffSet = IPDB_ZERO;
    UINT1               u1TimerId = IPDB_ZERO;

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_ENTRY, IPDB_MODULE_NAME,
                   "Entering the function %s \r\n", __FUNCTION__);

    /* Take the lock, before processing the event */
    if (IPDB_LOCK () == SNMP_FAILURE)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Taking the protocol lock failed.\r\n");
        return;
    }

    /* Get the expired timer from the list */
    while ((pExpiredTmr = (tIpDbTimerEntry *)
            TmrGetNextExpiredTimer (IPDB_TIMER_LIST)) != NULL)
    {
        /* Get the timer id */
        u1TimerId = ((tIpDbTimerEntry *) pExpiredTmr)->TmrBlk.u1TimerId;

        if (u1TimerId == IPDB_BINDING_TIMER)
        {
            /* Get the offset and hence get the Binding entry */
            i2OffSet = (INT2) IPDB_BINDING_TMR_OFFSET;

            pIpDbBindingEntry = (tIpDbBindingEntry *) (VOID *)
                ((UINT1 *) pExpiredTmr - i2OffSet);

            pIpDbBindingEntry->LeaseTimerNode.u1Status = IPDB_DISABLED;

            /* ASSUMPTION: For static entry there is no timer. For applying 
             * timer deletion for static entry, we shoul call the function
             * IpdbProcChangeStaticEntry with DESTROY */

            /* Delete the Binding entry */
            if (IpdbProcDeleteBindingEntry (pIpDbBindingEntry->u4ContextId,
                                            pIpDbBindingEntry->HostMac,
                                            pIpDbBindingEntry->VlanId,
                                            IPDB_ZERO, IPDB_ZERO,
                                            pIpDbBindingEntry->u1BindingType)
                != IPDB_SUCCESS)
            {
                IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                          "Failure in deleting binding entry \r\n");
            }
            pIpDbBindingEntry = NULL;
        }
        u1TimerId = IPDB_ZERO;
    }
    /* Release the lock */
    IPDB_UNLOCK ();

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);
    return;
}

/*****************************************************************************/
/* Function Name      : IpdbProcessCfgMsgEvent                               */
/*                                                                           */
/* Description        : This function handles the config msg events.         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
IpdbProcessCfgMsgEvent (tIpDbQMsg * pIpDbQMsg)
{
    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_ENTRY, IPDB_MODULE_NAME,
                   "Entering the function %s \r\n", __FUNCTION__);

    if (pIpDbQMsg->u4EventType == IPDB_PORT_CREATE_EVENT)
    {
        if (IPDB_IS_PORT_CHANNEL (pIpDbQMsg->u4InPort) == IPDB_TRUE)
        {
            L2_SYNC_GIVE_SEM ();
            return;
        }

        if (IpDbUtilCreatePortCtrlEntry (pIpDbQMsg->u4InPort) == IPDB_FAILURE)
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "Addition of pPortCtrlEntry failed.\r\n");
            L2_SYNC_GIVE_SEM ();
            return;
        }

        if (DcsApiAddPortEntry (pIpDbQMsg->u4InPort) == IPDB_FAILURE)
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "Adding created port into DcsPortCtrlEntry function "
                      "failed.\r\n");
            L2_SYNC_GIVE_SEM ();
            return;
        }
        /* Release the L2 synch sem */
        L2_SYNC_GIVE_SEM ();

    }
    else if (pIpDbQMsg->u4EventType == IPDB_PORT_DELETE_EVENT)
    {
        if (IpDbUtilDeletePortCtrlEntry (pIpDbQMsg->u4InPort) == IPDB_FAILURE)
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "Deletion of pPortCtrlEntry failed.\r\n");
            return;
        }

        if (DcsApiDeletePortEntry (pIpDbQMsg->u4InPort) == IPDB_FAILURE)
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "Deleting created port from DcsPortCtrlEntry function "
                      "failed.\r\n");
            return;
        }
    }

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IpdbPostCfgMessage                               */
/*                                                                           */
/*    Description         : Posts the message to Vlan Config Q.              */
/*                                                                           */
/*    Input(s)            : u1MsgType -  Meesage Type                        */
/*                          u2Port    -  Port Index.                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : IPDB_SUCCESS / IPDB_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
IpdbPostCfgMessage (UINT4 u4IfIndex, UINT4 u4MsgType)
{
    tIpDbQMsg          *pIpDbQMsg = NULL;

    IPDB_TRC_ARG2 (IPDB_TRC_FLAG, IPDB_FN_ARGS, IPDB_MODULE_NAME,
                   "Entering the function %s with arguments - InPort"
                   " %d,\r\n", __FUNCTION__, u4IfIndex);

    if (gu1IsIpdbInitialised != IPDB_TRUE)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "IPDB module not initialized\r\n");
        return IPDB_FAILURE;
    }

    if ((pIpDbQMsg = (tIpDbQMsg *)
         IPDB_ALLOC_MEM_BLOCK (IPDB_Q_POOL_ID)) == NULL)
    {
        IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                       "Allocation of memory for Q message failed - pool ID"
                       " %d \r\n", IPDB_Q_POOL_ID);
        return IPDB_FAILURE;
    }

    MEMSET (pIpDbQMsg, IPDB_ZERO, sizeof (tIpDbQMsg));

    pIpDbQMsg->u4EventType = u4MsgType;
    pIpDbQMsg->u4InPort = u4IfIndex;

    /* Send the Messageto queue */
    if (IPDB_SEND_TO_QUEUE (IPDB_QUEUE_ID, (UINT1 *) &pIpDbQMsg,
                            OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Failed to send the message to Q\r\n");
        IPDB_RELEASE_MEM_BLOCK (IPDB_Q_POOL_ID, pIpDbQMsg);
        return IPDB_FAILURE;
    }

    /* Send an event to IPDB Task, indicating VLAN deletion */
    if (IPDB_SEND_EVENT (IPDB_ZERO, IPDB_TASK_NAME, IPDB_EVENT_ARRIVED)
        != OSIX_SUCCESS)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Sending the config msg event to IPDB failed\r\n");
        return IPDB_FAILURE;
    }

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);
    return IPDB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IpdbAssignMempoolIds                                   */
/*                                                                           */
/* Description        : This function is used to assign respective mempool   */
/*                       ID's                                                */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
IpdbAssignMempoolIds (VOID)
{
    /*tIpDbQMsg */
    IPDB_Q_POOL_ID = IPDBMemPoolIds[MAX_IPDB_Q_MESG_SIZING_ID];

    /*tIpDbStaticBindingEntry */
    IPDB_STATIC_MEMPOOL_ID =
        IPDBMemPoolIds[MAX_IPDB_STATIC_BINDING_ENTRIES_SIZING_ID];

    /*tIpDbBindingEntry */
    IPDB_BINDING_MEMPOOL_ID =
        IPDBMemPoolIds[MAX_IPDB_BINDING_ENTRIES_SIZING_ID];

    /*tIpDbIfaceEntry */
    IPDB_INTF_POOL_ID = IPDBMemPoolIds[MAX_IPDB_IFACE_ENTRIES_SIZING_ID];

    /*tIpDbGatewayEntry */
    IPDB_GW_POOL_ID = IPDBMemPoolIds[MAX_IPDB_GATEWAY_ENTRIES_SIZING_ID];

    /*tPortCtrlEntry */
    IPDB_PORTCTRL_POOL_ID =
        IPDBMemPoolIds[MAX_IPDB_PORT_CTRL_ENTRIES_SIZING_ID];

    /*tVlanCtrlEntry */
    IPDB_VLANCTRL_POOL_ID =
        IPDBMemPoolIds[MAX_IPDB_VLAN_CTRL_ENTRIES_SIZING_ID];
}

/*                                                                           */
/***************************** END OF FILE ***********************************/
/*                                                                           */
