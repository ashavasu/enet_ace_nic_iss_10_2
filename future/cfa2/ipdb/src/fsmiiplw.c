/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmiiplw.c,v 1.2 2010/08/09 13:23:16 prabuc Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "ipdbinc.h"
# include  "fssnmp.h"
# include  "ipdbextn.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpDbNoOfBindings
 Input       :  The Indices

                The Object 
                retValFsMIIpDbNoOfBindings
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpDbNoOfBindings (UINT4 *pu4RetValFsMIIpDbNoOfBindings)
{
    return (nmhGetFsIpDbNoOfBindings (pu4RetValFsMIIpDbNoOfBindings));
}

/****************************************************************************
 Function    :  nmhGetFsMIIpDbNoOfStaticBindings
 Input       :  The Indices

                The Object 
                retValFsMIIpDbNoOfStaticBindings
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpDbNoOfStaticBindings (UINT4 *pu4RetValFsMIIpDbNoOfStaticBindings)
{
    return
        (nmhGetFsIpDbNoOfStaticBindings (pu4RetValFsMIIpDbNoOfStaticBindings));
}

/****************************************************************************
 Function    :  nmhGetFsMIIpDbNoOfDHCPBindings
 Input       :  The Indices

                The Object 
                retValFsMIIpDbNoOfDHCPBindings
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpDbNoOfDHCPBindings (UINT4 *pu4RetValFsMIIpDbNoOfDHCPBindings)
{
    return (nmhGetFsIpDbNoOfDHCPBindings (pu4RetValFsMIIpDbNoOfDHCPBindings));
}

/****************************************************************************
 Function    :  nmhGetFsMIIpDbNoOfPPPBindings
 Input       :  The Indices

                The Object 
                retValFsMIIpDbNoOfPPPBindings
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpDbNoOfPPPBindings (UINT4 *pu4RetValFsMIIpDbNoOfPPPBindings)
{
    return (nmhGetFsIpDbNoOfPPPBindings (pu4RetValFsMIIpDbNoOfPPPBindings));
}

/****************************************************************************
 Function    :  nmhGetFsMIIpDbTraceLevel
 Input       :  The Indices

                The Object 
                retValFsMIIpDbTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpDbTraceLevel (INT4 *pi4RetValFsMIIpDbTraceLevel)
{
    return (nmhGetFsIpDbTraceLevel (pi4RetValFsMIIpDbTraceLevel));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpDbTraceLevel
 Input       :  The Indices

                The Object 
                setValFsMIIpDbTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpDbTraceLevel (INT4 i4SetValFsMIIpDbTraceLevel)
{
    return (nmhSetFsIpDbTraceLevel (i4SetValFsMIIpDbTraceLevel));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpDbTraceLevel
 Input       :  The Indices

                The Object 
                testValFsMIIpDbTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpDbTraceLevel (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsMIIpDbTraceLevel)
{
    return (nmhTestv2FsIpDbTraceLevel (pu4ErrorCode,
                                       i4TestValFsMIIpDbTraceLevel));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpDbTraceLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIIpDbTraceLevel (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIIpDbStaticBindingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpDbStaticBindingTable
 Input       :  The Indices
                FsMIIpDbContextId
                FsMIIpDbStaticHostVlanId
                FsMIIpDbStaticHostMac
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpDbStaticBindingTable (INT4 i4FsMIIpDbContextId,
                                                    INT4
                                                    i4FsMIIpDbStaticHostVlanId,
                                                    tMacAddr
                                                    FsMIIpDbStaticHostMac)
{
    if ((i4FsMIIpDbContextId < IPDB_DEFAULT_CONTEXT) ||
        (i4FsMIIpDbContextId >= IPDB_MAX_CONTEXTS))
    {
        return SNMP_FAILURE;
    }

    if (IpDbUtilVcmIsVcExist (i4FsMIIpDbContextId) == IPDB_FALSE)
    {
        return SNMP_FAILURE;
    }

    return (nmhValidateIndexInstanceFsIpDbStaticBindingTable
            (i4FsMIIpDbStaticHostVlanId, FsMIIpDbStaticHostMac));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpDbStaticBindingTable
 Input       :  The Indices
                FsMIIpDbContextId
                FsMIIpDbStaticHostVlanId
                FsMIIpDbStaticHostMac
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpDbStaticBindingTable (INT4 *pi4FsMIIpDbContextId,
                                            INT4 *pi4FsMIIpDbStaticHostVlanId,
                                            tMacAddr * pFsMIIpDbStaticHostMac)
{
    UINT4               u4ContextId = IPDB_DEFAULT_CONTEXT;

    do
    {
        if (IpDbUtilSetContext (u4ContextId) == IPDB_FAILURE)
        {
            return SNMP_FAILURE;
        }

        *pi4FsMIIpDbContextId = u4ContextId;

        if (nmhGetFirstIndexFsIpDbStaticBindingTable
            (pi4FsMIIpDbStaticHostVlanId, pFsMIIpDbStaticHostMac)
            == SNMP_SUCCESS)
        {
            IpDbUtilResetContext ();
            return SNMP_SUCCESS;
        }
        IpDbUtilResetContext ();
    }
    while (IpDbUtilGetNextActiveCtxt (*pi4FsMIIpDbContextId, &u4ContextId)
           == IPDB_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpDbStaticBindingTable
 Input       :  The Indices
                FsMIIpDbContextId
                nextFsMIIpDbContextId
                FsMIIpDbStaticHostVlanId
                nextFsMIIpDbStaticHostVlanId
                FsMIIpDbStaticHostMac
                nextFsMIIpDbStaticHostMac
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpDbStaticBindingTable (INT4 i4FsMIIpDbContextId,
                                           INT4 *pi4NextFsMIIpDbContextId,
                                           INT4 i4FsMIIpDbStaticHostVlanId,
                                           INT4
                                           *pi4NextFsMIIpDbStaticHostVlanId,
                                           tMacAddr FsMIIpDbStaticHostMac,
                                           tMacAddr *
                                           pNextFsMIIpDbStaticHostMac)
{
    UINT4               u4ContextId = IPDB_DEFAULT_CONTEXT;

    if (IpDbUtilSetContext (i4FsMIIpDbContextId) == IPDB_SUCCESS)
    {
        if (nmhGetNextIndexFsIpDbStaticBindingTable
            (i4FsMIIpDbStaticHostVlanId, pi4NextFsMIIpDbStaticHostVlanId,
             FsMIIpDbStaticHostMac, pNextFsMIIpDbStaticHostMac) == SNMP_SUCCESS)
        {
            *pi4NextFsMIIpDbContextId = i4FsMIIpDbContextId;
            IpDbUtilResetContext ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        IpDbUtilResetContext ();

        if (IpDbUtilGetNextActiveCtxt ((UINT4) i4FsMIIpDbContextId,
                                       &u4ContextId) == IPDB_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (IpDbUtilSetContext (u4ContextId) == IPDB_FAILURE)
        {
            return SNMP_FAILURE;
        }

        i4FsMIIpDbContextId = (INT4) u4ContextId;
        *pi4NextFsMIIpDbContextId = (INT4) u4ContextId;
    }
    while (nmhGetFirstIndexFsIpDbStaticBindingTable
           (pi4NextFsMIIpDbStaticHostVlanId, pNextFsMIIpDbStaticHostMac)
           != SNMP_SUCCESS);

    IpDbUtilResetContext ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpDbStaticHostIp
 Input       :  The Indices
                FsMIIpDbContextId
                FsMIIpDbStaticHostVlanId
                FsMIIpDbStaticHostMac

                The Object 
                retValFsMIIpDbStaticHostIp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpDbStaticHostIp (INT4 i4FsMIIpDbContextId,
                            INT4 i4FsMIIpDbStaticHostVlanId,
                            tMacAddr FsMIIpDbStaticHostMac,
                            UINT4 *pu4RetValFsMIIpDbStaticHostIp)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue =
        nmhGetFsIpDbStaticHostIp (i4FsMIIpDbStaticHostVlanId,
                                  FsMIIpDbStaticHostMac,
                                  pu4RetValFsMIIpDbStaticHostIp);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpDbStaticInIfIndex
 Input       :  The Indices
                FsMIIpDbContextId
                FsMIIpDbStaticHostVlanId
                FsMIIpDbStaticHostMac

                The Object 
                retValFsMIIpDbStaticInIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpDbStaticInIfIndex (INT4 i4FsMIIpDbContextId,
                               INT4 i4FsMIIpDbStaticHostVlanId,
                               tMacAddr FsMIIpDbStaticHostMac,
                               INT4 *pi4RetValFsMIIpDbStaticInIfIndex)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue =
        nmhGetFsIpDbStaticInIfIndex (i4FsMIIpDbStaticHostVlanId,
                                     FsMIIpDbStaticHostMac,
                                     pi4RetValFsMIIpDbStaticInIfIndex);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpDbStaticGateway
 Input       :  The Indices
                FsMIIpDbContextId
                FsMIIpDbStaticHostVlanId
                FsMIIpDbStaticHostMac

                The Object 
                retValFsMIIpDbStaticGateway
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpDbStaticGateway (INT4 i4FsMIIpDbContextId,
                             INT4 i4FsMIIpDbStaticHostVlanId,
                             tMacAddr FsMIIpDbStaticHostMac,
                             UINT4 *pu4RetValFsMIIpDbStaticGateway)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue =
        nmhGetFsIpDbStaticGateway (i4FsMIIpDbStaticHostVlanId,
                                   FsMIIpDbStaticHostMac,
                                   pu4RetValFsMIIpDbStaticGateway);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpDbStaticBindingStatus
 Input       :  The Indices
                FsMIIpDbContextId
                FsMIIpDbStaticHostVlanId
                FsMIIpDbStaticHostMac

                The Object 
                retValFsMIIpDbStaticBindingStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpDbStaticBindingStatus (INT4 i4FsMIIpDbContextId,
                                   INT4 i4FsMIIpDbStaticHostVlanId,
                                   tMacAddr FsMIIpDbStaticHostMac,
                                   INT4 *pi4RetValFsMIIpDbStaticBindingStatus)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue =
        nmhGetFsIpDbStaticBindingStatus (i4FsMIIpDbStaticHostVlanId,
                                         FsMIIpDbStaticHostMac,
                                         pi4RetValFsMIIpDbStaticBindingStatus);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpDbStaticHostIp
 Input       :  The Indices
                FsMIIpDbContextId
                FsMIIpDbStaticHostVlanId
                FsMIIpDbStaticHostMac

                The Object 
                setValFsMIIpDbStaticHostIp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpDbStaticHostIp (INT4 i4FsMIIpDbContextId,
                            INT4 i4FsMIIpDbStaticHostVlanId,
                            tMacAddr FsMIIpDbStaticHostMac,
                            UINT4 u4SetValFsMIIpDbStaticHostIp)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue =
        nmhSetFsIpDbStaticHostIp (i4FsMIIpDbStaticHostVlanId,
                                  FsMIIpDbStaticHostMac,
                                  u4SetValFsMIIpDbStaticHostIp);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpDbStaticInIfIndex
 Input       :  The Indices
                FsMIIpDbContextId
                FsMIIpDbStaticHostVlanId
                FsMIIpDbStaticHostMac

                The Object 
                setValFsMIIpDbStaticInIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpDbStaticInIfIndex (INT4 i4FsMIIpDbContextId,
                               INT4 i4FsMIIpDbStaticHostVlanId,
                               tMacAddr FsMIIpDbStaticHostMac,
                               INT4 i4SetValFsMIIpDbStaticInIfIndex)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue =
        nmhSetFsIpDbStaticInIfIndex (i4FsMIIpDbStaticHostVlanId,
                                     FsMIIpDbStaticHostMac,
                                     i4SetValFsMIIpDbStaticInIfIndex);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpDbStaticGateway
 Input       :  The Indices
                FsMIIpDbContextId
                FsMIIpDbStaticHostVlanId
                FsMIIpDbStaticHostMac

                The Object 
                setValFsMIIpDbStaticGateway
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpDbStaticGateway (INT4 i4FsMIIpDbContextId,
                             INT4 i4FsMIIpDbStaticHostVlanId,
                             tMacAddr FsMIIpDbStaticHostMac,
                             UINT4 u4SetValFsMIIpDbStaticGateway)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue =
        nmhSetFsIpDbStaticGateway (i4FsMIIpDbStaticHostVlanId,
                                   FsMIIpDbStaticHostMac,
                                   u4SetValFsMIIpDbStaticGateway);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/****************************************************************************
 Function    :  nmhSetFsMIIpDbStaticBindingStatus
 Input       :  The Indices
                FsMIIpDbContextId
                FsMIIpDbStaticHostVlanId
                FsMIIpDbStaticHostMac

                The Object 
                setValFsMIIpDbStaticBindingStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpDbStaticBindingStatus (INT4 i4FsMIIpDbContextId,
                                   INT4 i4FsMIIpDbStaticHostVlanId,
                                   tMacAddr FsMIIpDbStaticHostMac,
                                   INT4 i4SetValFsMIIpDbStaticBindingStatus)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue =
        nmhSetFsIpDbStaticBindingStatus (i4FsMIIpDbStaticHostVlanId,
                                         FsMIIpDbStaticHostMac,
                                         i4SetValFsMIIpDbStaticBindingStatus);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpDbStaticHostIp
 Input       :  The Indices
                FsMIIpDbContextId
                FsMIIpDbStaticHostVlanId
                FsMIIpDbStaticHostMac

                The Object 
                testValFsMIIpDbStaticHostIp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpDbStaticHostIp (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIIpDbContextId,
                               INT4 i4FsMIIpDbStaticHostVlanId,
                               tMacAddr FsMIIpDbStaticHostMac,
                               UINT4 u4TestValFsMIIpDbStaticHostIp)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue =
        nmhTestv2FsIpDbStaticHostIp (pu4ErrorCode, i4FsMIIpDbStaticHostVlanId,
                                     FsMIIpDbStaticHostMac,
                                     u4TestValFsMIIpDbStaticHostIp);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpDbStaticInIfIndex
 Input       :  The Indices
                FsMIIpDbContextId
                FsMIIpDbStaticHostVlanId
                FsMIIpDbStaticHostMac

                The Object 
                testValFsMIIpDbStaticInIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpDbStaticInIfIndex (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIIpDbContextId,
                                  INT4 i4FsMIIpDbStaticHostVlanId,
                                  tMacAddr FsMIIpDbStaticHostMac,
                                  INT4 i4TestValFsMIIpDbStaticInIfIndex)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue =
        nmhTestv2FsIpDbStaticInIfIndex (pu4ErrorCode,
                                        i4FsMIIpDbStaticHostVlanId,
                                        FsMIIpDbStaticHostMac,
                                        i4TestValFsMIIpDbStaticInIfIndex);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpDbStaticGateway
 Input       :  The Indices
                FsMIIpDbContextId
                FsMIIpDbStaticHostVlanId
                FsMIIpDbStaticHostMac

                The Object 
                testValFsMIIpDbStaticGateway
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpDbStaticGateway (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIIpDbContextId,
                                INT4 i4FsMIIpDbStaticHostVlanId,
                                tMacAddr FsMIIpDbStaticHostMac,
                                UINT4 u4TestValFsMIIpDbStaticGateway)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue =
        nmhTestv2FsIpDbStaticGateway (pu4ErrorCode,
                                      i4FsMIIpDbStaticHostVlanId,
                                      FsMIIpDbStaticHostMac,
                                      u4TestValFsMIIpDbStaticGateway);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIIpDbStaticBindingStatus
 Input       :  The Indices
                FsMIIpDbContextId
                FsMIIpDbStaticHostVlanId
                FsMIIpDbStaticHostMac

                The Object 
                testValFsMIIpDbStaticBindingStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpDbStaticBindingStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIIpDbContextId,
                                      INT4 i4FsMIIpDbStaticHostVlanId,
                                      tMacAddr FsMIIpDbStaticHostMac,
                                      INT4 i4TestValFsMIIpDbStaticBindingStatus)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue = nmhTestv2FsIpDbStaticBindingStatus
        (pu4ErrorCode, i4FsMIIpDbStaticHostVlanId,
         FsMIIpDbStaticHostMac, i4TestValFsMIIpDbStaticBindingStatus);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpDbStaticBindingTable
 Input       :  The Indices
                FsMIIpDbContextId
                FsMIIpDbStaticHostVlanId
                FsMIIpDbStaticHostMac
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIIpDbStaticBindingTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIIpDbBindingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpDbBindingTable
 Input       :  The Indices
                FsMIIpDbHostContextId
                FsMIIpDbHostVlanId
                FsMIIpDbHostMac
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpDbBindingTable (INT4 i4FsMIIpDbHostContextId,
                                              INT4 i4FsMIIpDbHostVlanId,
                                              tMacAddr FsMIIpDbHostMac)
{
    if ((i4FsMIIpDbHostContextId < IPDB_DEFAULT_CONTEXT) ||
        (i4FsMIIpDbHostContextId >= IPDB_MAX_CONTEXTS))
    {
        return SNMP_FAILURE;
    }

    if (IpDbUtilVcmIsVcExist (i4FsMIIpDbHostContextId) == IPDB_FALSE)
    {
        return SNMP_FAILURE;
    }

    return (nmhValidateIndexInstanceFsIpDbBindingTable
            (i4FsMIIpDbHostVlanId, FsMIIpDbHostMac));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpDbBindingTable
 Input       :  The Indices
                FsMIIpDbHostContextId
                FsMIIpDbHostVlanId
                FsMIIpDbHostMac
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpDbBindingTable (INT4 *pi4FsMIIpDbHostContextId,
                                      INT4 *pi4FsMIIpDbHostVlanId,
                                      tMacAddr * pFsMIIpDbHostMac)
{
    UINT4               u4ContextId = IPDB_DEFAULT_CONTEXT;

    do
    {
        if (IpDbUtilSetContext (u4ContextId) == IPDB_FAILURE)
        {
            return SNMP_FAILURE;
        }

        *pi4FsMIIpDbHostContextId = u4ContextId;

        if (nmhGetFirstIndexFsIpDbBindingTable
            (pi4FsMIIpDbHostVlanId, pFsMIIpDbHostMac) == SNMP_SUCCESS)
        {
            IpDbUtilResetContext ();
            return SNMP_SUCCESS;
        }
        IpDbUtilResetContext ();
    }
    while (IpDbUtilGetNextActiveCtxt (*pi4FsMIIpDbHostContextId, &u4ContextId)
           == IPDB_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpDbBindingTable
 Input       :  The Indices
                FsMIIpDbHostContextId
                nextFsMIIpDbHostContextId
                FsMIIpDbHostVlanId
                nextFsMIIpDbHostVlanId
                FsMIIpDbHostMac
                nextFsMIIpDbHostMac
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpDbBindingTable (INT4 i4FsMIIpDbHostContextId,
                                     INT4 *pi4NextFsMIIpDbHostContextId,
                                     INT4 i4FsMIIpDbHostVlanId,
                                     INT4 *pi4NextFsMIIpDbHostVlanId,
                                     tMacAddr FsMIIpDbHostMac,
                                     tMacAddr * pNextFsMIIpDbHostMac)
{
    UINT4               u4ContextId = IPDB_DEFAULT_CONTEXT;

    if (IpDbUtilSetContext (i4FsMIIpDbHostContextId) == IPDB_SUCCESS)
    {
        if (nmhGetNextIndexFsIpDbBindingTable
            (i4FsMIIpDbHostVlanId, pi4NextFsMIIpDbHostVlanId,
             FsMIIpDbHostMac, pNextFsMIIpDbHostMac) == SNMP_SUCCESS)
        {
            *pi4NextFsMIIpDbHostContextId = i4FsMIIpDbHostContextId;
            IpDbUtilResetContext ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        IpDbUtilResetContext ();

        if (IpDbUtilGetNextActiveCtxt (i4FsMIIpDbHostContextId, &u4ContextId)
            == IPDB_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (IpDbUtilSetContext (u4ContextId) == IPDB_FAILURE)
        {
            return SNMP_FAILURE;
        }

        i4FsMIIpDbHostContextId = (INT4) u4ContextId;
        *pi4NextFsMIIpDbHostContextId = (INT4) u4ContextId;
    }
    while (nmhGetFirstIndexFsIpDbBindingTable
           (pi4NextFsMIIpDbHostVlanId, pNextFsMIIpDbHostMac) != SNMP_SUCCESS);

    IpDbUtilResetContext ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpDbHostBindingType
 Input       :  The Indices
                FsMIIpDbHostContextId
                FsMIIpDbHostVlanId
                FsMIIpDbHostMac

                The Object 
                retValFsMIIpDbHostBindingType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpDbHostBindingType (INT4 i4FsMIIpDbHostContextId,
                               INT4 i4FsMIIpDbHostVlanId,
                               tMacAddr FsMIIpDbHostMac,
                               INT4 *pi4RetValFsMIIpDbHostBindingType)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbHostContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue =
        nmhGetFsIpDbHostBindingType (i4FsMIIpDbHostVlanId,
                                     FsMIIpDbHostMac,
                                     pi4RetValFsMIIpDbHostBindingType);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpDbHostIp
 Input       :  The Indices
                FsMIIpDbHostContextId
                FsMIIpDbHostVlanId
                FsMIIpDbHostMac

                The Object 
                retValFsMIIpDbHostIp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpDbHostIp (INT4 i4FsMIIpDbHostContextId,
                      INT4 i4FsMIIpDbHostVlanId,
                      tMacAddr FsMIIpDbHostMac, UINT4 *pu4RetValFsMIIpDbHostIp)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbHostContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue =
        nmhGetFsIpDbHostIp (i4FsMIIpDbHostVlanId,
                            FsMIIpDbHostMac, pu4RetValFsMIIpDbHostIp);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpDbHostInIfIndex
 Input       :  The Indices
                FsMIIpDbHostContextId
                FsMIIpDbHostVlanId
                FsMIIpDbHostMac

                The Object 
                retValFsMIIpDbHostInIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpDbHostInIfIndex (INT4 i4FsMIIpDbHostContextId,
                             INT4 i4FsMIIpDbHostVlanId,
                             tMacAddr FsMIIpDbHostMac,
                             INT4 *pi4RetValFsMIIpDbHostInIfIndex)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbHostContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue =
        nmhGetFsIpDbHostInIfIndex (i4FsMIIpDbHostVlanId,
                                   FsMIIpDbHostMac,
                                   pi4RetValFsMIIpDbHostInIfIndex);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpDbHostRemLeaseTime
 Input       :  The Indices
                FsMIIpDbHostContextId
                FsMIIpDbHostVlanId
                FsMIIpDbHostMac

                The Object 
                retValFsMIIpDbHostRemLeaseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpDbHostRemLeaseTime (INT4 i4FsMIIpDbHostContextId,
                                INT4 i4FsMIIpDbHostVlanId,
                                tMacAddr FsMIIpDbHostMac,
                                INT4 *pi4RetValFsMIIpDbHostRemLeaseTime)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbHostContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue =
        nmhGetFsIpDbHostRemLeaseTime (i4FsMIIpDbHostVlanId,
                                      FsMIIpDbHostMac,
                                      pi4RetValFsMIIpDbHostRemLeaseTime);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpDbHostBindingID
 Input       :  The Indices
                FsMIIpDbHostContextId
                FsMIIpDbHostVlanId
                FsMIIpDbHostMac

                The Object 
                retValFsMIIpDbHostBindingID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpDbHostBindingID (INT4 i4FsMIIpDbHostContextId,
                             INT4 i4FsMIIpDbHostVlanId,
                             tMacAddr FsMIIpDbHostMac,
                             UINT4 *pu4RetValFsMIIpDbHostBindingID)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbHostContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue =
        nmhGetFsIpDbHostBindingID (i4FsMIIpDbHostVlanId,
                                   FsMIIpDbHostMac,
                                   pu4RetValFsMIIpDbHostBindingID);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/* LOW LEVEL Routines for Table : FsMIIpDbGatewayIpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpDbGatewayIpTable
 Input       :  The Indices
                FsMIIpDbHostContextId
                FsMIIpDbHostMac
                FsMIIpDbHostVlanId
                FsMIIpDbGatewayNetwork
                FsMIIpDbGatewayNetMask
                FsMIIpDbGatewayIp
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpDbGatewayIpTable (INT4 i4FsMIIpDbHostContextId,
                                                tMacAddr FsMIIpDbHostMac,
                                                INT4 i4FsMIIpDbHostVlanId,
                                                UINT4 u4FsMIIpDbGatewayNetwork,
                                                UINT4 u4FsMIIpDbGatewayNetMask,
                                                UINT4 u4FsMIIpDbGatewayIp)
{
    if ((i4FsMIIpDbHostContextId < IPDB_DEFAULT_CONTEXT) ||
        (i4FsMIIpDbHostContextId >= IPDB_MAX_CONTEXTS))
    {
        return SNMP_FAILURE;
    }

    if (IpDbUtilVcmIsVcExist (i4FsMIIpDbHostContextId) == IPDB_FALSE)
    {
        return SNMP_FAILURE;
    }

    return (nmhValidateIndexInstanceFsIpDbGatewayIpTable
            (FsMIIpDbHostMac, i4FsMIIpDbHostVlanId, u4FsMIIpDbGatewayNetwork,
             u4FsMIIpDbGatewayNetMask, u4FsMIIpDbGatewayIp));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpDbGatewayIpTable
 Input       :  The Indices
                FsMIIpDbHostContextId
                FsMIIpDbHostMac
                FsMIIpDbHostVlanId
                FsMIIpDbGatewayNetwork
                FsMIIpDbGatewayNetMask
                FsMIIpDbGatewayIp
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpDbGatewayIpTable (INT4 *pi4FsMIIpDbHostContextId,
                                        tMacAddr * pFsMIIpDbHostMac,
                                        INT4 *pi4FsMIIpDbHostVlanId,
                                        UINT4 *pu4FsMIIpDbGatewayNetwork,
                                        UINT4 *pu4FsMIIpDbGatewayNetMask,
                                        UINT4 *pu4FsMIIpDbGatewayIp)
{
    UINT4               u4ContextId = IPDB_DEFAULT_CONTEXT;

    do
    {
        if (IpDbUtilSetContext (u4ContextId) == IPDB_FAILURE)
        {
            return SNMP_FAILURE;
        }

        *pi4FsMIIpDbHostContextId = u4ContextId;

        if (nmhGetFirstIndexFsIpDbGatewayIpTable
            (pFsMIIpDbHostMac, pi4FsMIIpDbHostVlanId,
             pu4FsMIIpDbGatewayNetwork, pu4FsMIIpDbGatewayNetMask,
             pu4FsMIIpDbGatewayIp) == SNMP_SUCCESS)
        {
            IpDbUtilResetContext ();
            return SNMP_SUCCESS;
        }
        IpDbUtilResetContext ();
    }
    while (IpDbUtilGetNextActiveCtxt (*pi4FsMIIpDbHostContextId, &u4ContextId)
           == IPDB_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpDbGatewayIpTable
 Input       :  The Indices
                FsMIIpDbHostContextId
                nextFsMIIpDbHostContextId
                FsMIIpDbHostMac
                nextFsMIIpDbHostMac
                FsMIIpDbHostVlanId
                nextFsMIIpDbHostVlanId
                FsMIIpDbGatewayNetwork
                nextFsMIIpDbGatewayNetwork
                FsMIIpDbGatewayNetMask
                nextFsMIIpDbGatewayNetMask
                FsMIIpDbGatewayIp
                nextFsMIIpDbGatewayIp
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpDbGatewayIpTable (INT4 i4FsMIIpDbHostContextId,
                                       INT4 *pi4NextFsMIIpDbHostContextId,
                                       tMacAddr FsMIIpDbHostMac,
                                       tMacAddr * pNextFsMIIpDbHostMac,
                                       INT4 i4FsMIIpDbHostVlanId,
                                       INT4 *pi4NextFsMIIpDbHostVlanId,
                                       UINT4 u4FsMIIpDbGatewayNetwork,
                                       UINT4 *pu4NextFsMIIpDbGatewayNetwork,
                                       UINT4 u4FsMIIpDbGatewayNetMask,
                                       UINT4 *pu4NextFsMIIpDbGatewayNetMask,
                                       UINT4 u4FsMIIpDbGatewayIp,
                                       UINT4 *pu4NextFsMIIpDbGatewayIp)
{
    UINT4               u4ContextId = IPDB_DEFAULT_CONTEXT;

    if (IpDbUtilSetContext (i4FsMIIpDbHostContextId) == IPDB_SUCCESS)
    {
        if (nmhGetNextIndexFsIpDbGatewayIpTable
            (FsMIIpDbHostMac, pNextFsMIIpDbHostMac,
             i4FsMIIpDbHostVlanId, pi4NextFsMIIpDbHostVlanId,
             u4FsMIIpDbGatewayNetwork, pu4NextFsMIIpDbGatewayNetwork,
             u4FsMIIpDbGatewayNetMask, pu4NextFsMIIpDbGatewayNetMask,
             u4FsMIIpDbGatewayIp, pu4NextFsMIIpDbGatewayIp) == SNMP_SUCCESS)
        {
            *pi4NextFsMIIpDbHostContextId = i4FsMIIpDbHostContextId;
            IpDbUtilResetContext ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        IpDbUtilResetContext ();

        if (IpDbUtilGetNextActiveCtxt ((UINT4) i4FsMIIpDbHostContextId,
                                       &u4ContextId) == IPDB_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (IpDbUtilSetContext (u4ContextId) == IPDB_FAILURE)
        {
            return SNMP_FAILURE;
        }

        i4FsMIIpDbHostContextId = (INT4) u4ContextId;
        *pi4NextFsMIIpDbHostContextId = (INT4) u4ContextId;
    }
    while (nmhGetFirstIndexFsIpDbGatewayIpTable
           (pNextFsMIIpDbHostMac, pi4NextFsMIIpDbHostVlanId,
            pu4NextFsMIIpDbGatewayNetwork, pu4NextFsMIIpDbGatewayNetMask,
            pu4NextFsMIIpDbGatewayIp) != SNMP_SUCCESS);

    IpDbUtilResetContext ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpDbGatewayIpMode
 Input       :  The Indices
                FsMIIpDbHostContextId
                FsMIIpDbHostMac
                FsMIIpDbHostVlanId
                FsMIIpDbGatewayNetwork
                FsMIIpDbGatewayNetMask
                FsMIIpDbGatewayIp

                The Object 
                retValFsMIIpDbGatewayIpMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpDbGatewayIpMode (INT4 i4FsMIIpDbHostContextId,
                             tMacAddr FsMIIpDbHostMac,
                             INT4 i4FsMIIpDbHostVlanId,
                             UINT4 u4FsMIIpDbGatewayNetwork,
                             UINT4 u4FsMIIpDbGatewayNetMask,
                             UINT4 u4FsMIIpDbGatewayIp,
                             INT4 *pi4RetValFsMIIpDbGatewayIpMode)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbHostContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue =
        nmhGetFsIpDbGatewayIpMode (FsMIIpDbHostMac,
                                   i4FsMIIpDbHostVlanId,
                                   u4FsMIIpDbGatewayNetwork,
                                   u4FsMIIpDbGatewayNetMask,
                                   u4FsMIIpDbGatewayIp,
                                   pi4RetValFsMIIpDbGatewayIpMode);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/* LOW LEVEL Routines for Table : FsMIIpDbInterfaceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpDbInterfaceTable
 Input       :  The Indices
                FsMIIpDbIntfContextId
                FsMIIpDbIntfVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpDbInterfaceTable (INT4 i4FsMIIpDbIntfContextId,
                                                INT4 i4FsMIIpDbIntfVlanId)
{
    if ((i4FsMIIpDbIntfContextId < IPDB_DEFAULT_CONTEXT) ||
        (i4FsMIIpDbIntfContextId >= IPDB_MAX_CONTEXTS))
    {
        return SNMP_FAILURE;
    }

    if (IpDbUtilVcmIsVcExist (i4FsMIIpDbIntfContextId) == IPDB_FALSE)
    {
        return SNMP_FAILURE;
    }

    return (nmhValidateIndexInstanceFsIpDbInterfaceTable
            (i4FsMIIpDbIntfVlanId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpDbInterfaceTable
 Input       :  The Indices
                FsMIIpDbIntfContextId
                FsMIIpDbIntfVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpDbInterfaceTable (INT4 *pi4FsMIIpDbIntfContextId,
                                        INT4 *pi4FsMIIpDbIntfVlanId)
{
    UINT4               u4ContextId = IPDB_DEFAULT_CONTEXT;

    do
    {
        if (IpDbUtilSetContext (u4ContextId) == IPDB_FAILURE)
        {
            return SNMP_FAILURE;
        }

        *pi4FsMIIpDbIntfContextId = u4ContextId;

        if (nmhGetFirstIndexFsIpDbInterfaceTable
            (pi4FsMIIpDbIntfVlanId) == SNMP_SUCCESS)
        {
            IpDbUtilResetContext ();
            return SNMP_SUCCESS;
        }
        IpDbUtilResetContext ();
    }
    while (IpDbUtilGetNextActiveCtxt (*pi4FsMIIpDbIntfContextId, &u4ContextId)
           == IPDB_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpDbInterfaceTable
 Input       :  The Indices
                FsMIIpDbIntfContextId
                nextFsMIIpDbIntfContextId
                FsMIIpDbIntfVlanId
                nextFsMIIpDbIntfVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpDbInterfaceTable (INT4 i4FsMIIpDbIntfContextId,
                                       INT4 *pi4NextFsMIIpDbIntfContextId,
                                       INT4 i4FsMIIpDbIntfVlanId,
                                       INT4 *pi4NextFsMIIpDbIntfVlanId)
{
    UINT4               u4ContextId = IPDB_DEFAULT_CONTEXT;

    if (IpDbUtilSetContext (i4FsMIIpDbIntfContextId) == IPDB_SUCCESS)
    {
        if (nmhGetNextIndexFsIpDbInterfaceTable
            (i4FsMIIpDbIntfVlanId, pi4NextFsMIIpDbIntfVlanId) == SNMP_SUCCESS)
        {
            *pi4NextFsMIIpDbIntfContextId = i4FsMIIpDbIntfContextId;
            IpDbUtilResetContext ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        IpDbUtilResetContext ();

        if (IpDbUtilGetNextActiveCtxt ((UINT4) i4FsMIIpDbIntfContextId,
                                       &u4ContextId) == IPDB_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (IpDbUtilSetContext (u4ContextId) == IPDB_FAILURE)
        {
            return SNMP_FAILURE;
        }

        i4FsMIIpDbIntfContextId = (INT4) u4ContextId;
        *pi4NextFsMIIpDbIntfContextId = (INT4) u4ContextId;
    }
    while (nmhGetFirstIndexFsIpDbInterfaceTable
           (pi4NextFsMIIpDbIntfVlanId) != SNMP_SUCCESS);

    IpDbUtilResetContext ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpDbIntfNoOfVlanBindings
 Input       :  The Indices
                FsMIIpDbIntfContextId
                FsMIIpDbIntfVlanId

                The Object 
                retValFsMIIpDbIntfNoOfVlanBindings
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpDbIntfNoOfVlanBindings (INT4 i4FsMIIpDbIntfContextId,
                                    INT4 i4FsMIIpDbIntfVlanId,
                                    UINT4
                                    *pu4RetValFsMIIpDbIntfNoOfVlanBindings)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbIntfContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue = nmhGetFsIpDbIntfNoOfVlanBindings
        (i4FsMIIpDbIntfVlanId, pu4RetValFsMIIpDbIntfNoOfVlanBindings);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpDbIntfNoOfVlanStaticBindings
 Input       :  The Indices
                FsMIIpDbIntfContextId
                FsMIIpDbIntfVlanId

                The Object 
                retValFsMIIpDbIntfNoOfVlanStaticBindings
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsMIIpDbIntfNoOfVlanStaticBindings
    (INT4 i4FsMIIpDbIntfContextId, INT4 i4FsMIIpDbIntfVlanId,
     UINT4 *pu4RetValFsMIIpDbIntfNoOfVlanStaticBindings)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbIntfContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue = nmhGetFsIpDbIntfNoOfVlanStaticBindings
        (i4FsMIIpDbIntfVlanId, pu4RetValFsMIIpDbIntfNoOfVlanStaticBindings);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpDbIntfNoOfVlanDHCPBindings
 Input       :  The Indices
                FsMIIpDbIntfContextId
                FsMIIpDbIntfVlanId

                The Object 
                retValFsMIIpDbIntfNoOfVlanDHCPBindings
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsMIIpDbIntfNoOfVlanDHCPBindings
    (INT4 i4FsMIIpDbIntfContextId, INT4 i4FsMIIpDbIntfVlanId,
     UINT4 *pu4RetValFsMIIpDbIntfNoOfVlanDHCPBindings)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbIntfContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue = nmhGetFsIpDbIntfNoOfVlanDHCPBindings
        (i4FsMIIpDbIntfVlanId, pu4RetValFsMIIpDbIntfNoOfVlanDHCPBindings);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/****************************************************************************
 Function    :  nmhGetFsMIIpDbIntfNoOfVlanPPPBindings
 Input       :  The Indices
                FsMIIpDbIntfContextId
                FsMIIpDbIntfVlanId

                The Object 
                retValFsMIIpDbIntfNoOfVlanPPPBindings
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsMIIpDbIntfNoOfVlanPPPBindings
    (INT4 i4FsMIIpDbIntfContextId, INT4 i4FsMIIpDbIntfVlanId,
     UINT4 *pu4RetValFsMIIpDbIntfNoOfVlanPPPBindings)
{
    INT1                i1RetValue = SNMP_FAILURE;

    if (IpDbUtilSetContext (i4FsMIIpDbIntfContextId) == IPDB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetValue = nmhGetFsIpDbIntfNoOfVlanPPPBindings
        (i4FsMIIpDbIntfVlanId, pu4RetValFsMIIpDbIntfNoOfVlanPPPBindings);

    IpDbUtilResetContext ();

    return i1RetValue;
}

/* LOW LEVEL Routines for Table : FsMIIpDbSrcGuardConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIIpDbSrcGuardConfigTable
 Input       :  The Indices
                FsMIIpDbSrcGuardIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIIpDbSrcGuardConfigTable (INT4
                                                     i4FsMIIpDbSrcGuardIndex)
{
    return (nmhValidateIndexInstanceFsIpDbSrcGuardConfigTable
            (i4FsMIIpDbSrcGuardIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIIpDbSrcGuardConfigTable
 Input       :  The Indices
                FsMIIpDbSrcGuardIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIIpDbSrcGuardConfigTable (INT4 *pi4FsMIIpDbSrcGuardIndex)
{
    return (nmhGetFirstIndexFsIpDbSrcGuardConfigTable
            (pi4FsMIIpDbSrcGuardIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIIpDbSrcGuardConfigTable
 Input       :  The Indices
                FsMIIpDbSrcGuardIndex
                nextFsMIIpDbSrcGuardIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIIpDbSrcGuardConfigTable (INT4 i4FsMIIpDbSrcGuardIndex,
                                            INT4 *pi4NextFsMIIpDbSrcGuardIndex)
{
    return (nmhGetNextIndexFsIpDbSrcGuardConfigTable
            (i4FsMIIpDbSrcGuardIndex, pi4NextFsMIIpDbSrcGuardIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIIpDbSrcGuardStatus
 Input       :  The Indices
                FsMIIpDbSrcGuardIndex

                The Object 
                retValFsMIIpDbSrcGuardStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIIpDbSrcGuardStatus (INT4 i4FsMIIpDbSrcGuardIndex,
                              INT4 *pi4RetValFsMIIpDbSrcGuardStatus)
{
    return (nmhGetFsIpDbSrcGuardStatus (i4FsMIIpDbSrcGuardIndex,
                                        pi4RetValFsMIIpDbSrcGuardStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIIpDbSrcGuardStatus
 Input       :  The Indices
                FsMIIpDbSrcGuardIndex

                The Object 
                setValFsMIIpDbSrcGuardStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIIpDbSrcGuardStatus (INT4 i4FsMIIpDbSrcGuardIndex,
                              INT4 i4SetValFsMIIpDbSrcGuardStatus)
{
    return (nmhSetFsIpDbSrcGuardStatus (i4FsMIIpDbSrcGuardIndex,
                                        i4SetValFsMIIpDbSrcGuardStatus));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIIpDbSrcGuardStatus
 Input       :  The Indices
                FsMIIpDbSrcGuardIndex

                The Object 
                testValFsMIIpDbSrcGuardStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIIpDbSrcGuardStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIIpDbSrcGuardIndex,
                                 INT4 i4TestValFsMIIpDbSrcGuardStatus)
{
    return (nmhTestv2FsIpDbSrcGuardStatus (pu4ErrorCode,
                                           i4FsMIIpDbSrcGuardIndex,
                                           i4TestValFsMIIpDbSrcGuardStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIIpDbSrcGuardConfigTable
 Input       :  The Indices
                FsMIIpDbSrcGuardIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIIpDbSrcGuardConfigTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
