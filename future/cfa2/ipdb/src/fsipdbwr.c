# include  "lr.h"
# include  "fssnmp.h"
# include  "fsipdblw.h"
# include  "fsipdbwr.h"
# include  "fsipdbdb.h"

VOID
RegisterFSIPDB ()
{
    SNMPRegisterMib (&fsipdbOID, &fsipdbEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsipdbOID, (const UINT1 *) "fsipdb");
}

VOID
UnRegisterFSIPDB ()
{
    SNMPUnRegisterMib (&fsipdbOID, &fsipdbEntry);
    SNMPDelSysorEntry (&fsipdbOID, (const UINT1 *) "fsipdb");
}

INT4
FsIpDbNoOfBindingsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIpDbNoOfBindings (&(pMultiData->u4_ULongValue)));
}

INT4
FsIpDbNoOfStaticBindingsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIpDbNoOfStaticBindings (&(pMultiData->u4_ULongValue)));
}

INT4
FsIpDbNoOfDHCPBindingsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIpDbNoOfDHCPBindings (&(pMultiData->u4_ULongValue)));
}

INT4
FsIpDbNoOfPPPBindingsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIpDbNoOfPPPBindings (&(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexFsIpDbStaticBindingTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIpDbStaticBindingTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIpDbStaticBindingTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FsIpDbStaticHostIpGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIpDbStaticBindingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIpDbStaticHostIp (pMultiIndex->pIndex[0].i4_SLongValue,
                                      (*(tMacAddr *) pMultiIndex->pIndex[1].
                                       pOctetStrValue->pu1_OctetList),
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsIpDbStaticInIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIpDbStaticBindingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIpDbStaticInIfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                         (*(tMacAddr *) pMultiIndex->pIndex[1].
                                          pOctetStrValue->pu1_OctetList),
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsIpDbStaticGatewayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIpDbStaticBindingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIpDbStaticGateway (pMultiIndex->pIndex[0].i4_SLongValue,
                                       (*(tMacAddr *) pMultiIndex->pIndex[1].
                                        pOctetStrValue->pu1_OctetList),
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsIpDbStaticBindingStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIpDbStaticBindingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIpDbStaticBindingStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
              pu1_OctetList), &(pMultiData->i4_SLongValue)));

}

INT4
FsIpDbStaticHostIpSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIpDbStaticHostIp (pMultiIndex->pIndex[0].i4_SLongValue,
                                      (*(tMacAddr *) pMultiIndex->pIndex[1].
                                       pOctetStrValue->pu1_OctetList),
                                      pMultiData->u4_ULongValue));

}

INT4
FsIpDbStaticInIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIpDbStaticInIfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                         (*(tMacAddr *) pMultiIndex->pIndex[1].
                                          pOctetStrValue->pu1_OctetList),
                                         pMultiData->i4_SLongValue));

}

INT4
FsIpDbStaticGatewaySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIpDbStaticGateway (pMultiIndex->pIndex[0].i4_SLongValue,
                                       (*(tMacAddr *) pMultiIndex->pIndex[1].
                                        pOctetStrValue->pu1_OctetList),
                                       pMultiData->u4_ULongValue));

}

INT4
FsIpDbStaticBindingStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIpDbStaticBindingStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
              pu1_OctetList), pMultiData->i4_SLongValue));

}

INT4
FsIpDbStaticHostIpTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsIpDbStaticHostIp (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         (*(tMacAddr *) pMultiIndex->pIndex[1].
                                          pOctetStrValue->pu1_OctetList),
                                         pMultiData->u4_ULongValue));

}

INT4
FsIpDbStaticInIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsIpDbStaticInIfIndex (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            (*(tMacAddr *) pMultiIndex->
                                             pIndex[1].pOctetStrValue->
                                             pu1_OctetList),
                                            pMultiData->i4_SLongValue));

}

INT4
FsIpDbStaticGatewayTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsIpDbStaticGateway (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          (*(tMacAddr *) pMultiIndex->pIndex[1].
                                           pOctetStrValue->pu1_OctetList),
                                          pMultiData->u4_ULongValue));

}

INT4
FsIpDbStaticBindingStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsIpDbStaticBindingStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                (*(tMacAddr *) pMultiIndex->
                                                 pIndex[1].pOctetStrValue->
                                                 pu1_OctetList),
                                                pMultiData->i4_SLongValue));

}

INT4
FsIpDbStaticBindingTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIpDbStaticBindingTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsIpDbBindingTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIpDbBindingTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIpDbBindingTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FsIpDbHostBindingTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIpDbBindingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIpDbHostBindingType (pMultiIndex->pIndex[0].i4_SLongValue,
                                         (*(tMacAddr *) pMultiIndex->pIndex[1].
                                          pOctetStrValue->pu1_OctetList),
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsIpDbHostIpGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIpDbBindingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIpDbHostIp (pMultiIndex->pIndex[0].i4_SLongValue,
                                (*(tMacAddr *) pMultiIndex->pIndex[1].
                                 pOctetStrValue->pu1_OctetList),
                                &(pMultiData->u4_ULongValue)));

}

INT4
FsIpDbHostInIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIpDbBindingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIpDbHostInIfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                       (*(tMacAddr *) pMultiIndex->pIndex[1].
                                        pOctetStrValue->pu1_OctetList),
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsIpDbHostRemLeaseTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIpDbBindingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIpDbHostRemLeaseTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                          (*(tMacAddr *) pMultiIndex->pIndex[1].
                                           pOctetStrValue->pu1_OctetList),
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsIpDbHostBindingIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIpDbBindingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIpDbHostBindingID (pMultiIndex->pIndex[0].i4_SLongValue,
                                       (*(tMacAddr *) pMultiIndex->pIndex[1].
                                        pOctetStrValue->pu1_OctetList),
                                       &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexFsIpDbGatewayIpTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIpDbGatewayIpTable ((tMacAddr *) pNextMultiIndex->
                                                  pIndex[0].pOctetStrValue->
                                                  pu1_OctetList,
                                                  &(pNextMultiIndex->pIndex[1].
                                                    i4_SLongValue),
                                                  &(pNextMultiIndex->pIndex[2].
                                                    u4_ULongValue),
                                                  &(pNextMultiIndex->pIndex[3].
                                                    u4_ULongValue),
                                                  &(pNextMultiIndex->pIndex[4].
                                                    u4_ULongValue)) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIpDbGatewayIpTable
            (*(tMacAddr *) pFirstMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList, pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FsIpDbGatewayIpModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIpDbGatewayIpTable ((*(tMacAddr *)
                                                       pMultiIndex->pIndex[0].
                                                       pOctetStrValue->
                                                       pu1_OctetList),
                                                      pMultiIndex->pIndex[1].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[2].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[3].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[4].
                                                      u4_ULongValue) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIpDbGatewayIpMode ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                        pOctetStrValue->pu1_OctetList),
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiIndex->pIndex[4].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFsIpDbInterfaceTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIpDbInterfaceTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIpDbInterfaceTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsIpDbIntfNoOfVlanBindingsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIpDbInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIpDbIntfNoOfVlanBindings
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIpDbIntfNoOfVlanStaticBindingsGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIpDbInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIpDbIntfNoOfVlanStaticBindings
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIpDbIntfNoOfVlanDHCPBindingsGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIpDbInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIpDbIntfNoOfVlanDHCPBindings
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIpDbIntfNoOfVlanPPPBindingsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIpDbInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIpDbIntfNoOfVlanPPPBindings
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexFsIpDbSrcGuardConfigTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIpDbSrcGuardConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIpDbSrcGuardConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsIpDbSrcGuardStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIpDbSrcGuardConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIpDbSrcGuardStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsIpDbSrcGuardStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIpDbSrcGuardStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsIpDbSrcGuardStatusTest (UINT4 *pu4Error,
                          tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsIpDbSrcGuardStatus (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsIpDbSrcGuardConfigTableDep (UINT4 *pu4Error,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIpDbSrcGuardConfigTable (pu4Error, pSnmpIndexList,
                                               pSnmpvarbinds));
}

INT4
FsIpDbTraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIpDbTraceLevel (&(pMultiData->i4_SLongValue)));
}

INT4
FsIpDbTraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsIpDbTraceLevel (pMultiData->i4_SLongValue));
}

INT4
FsIpDbTraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsIpDbTraceLevel (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsIpDbTraceLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIpDbTraceLevel (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
