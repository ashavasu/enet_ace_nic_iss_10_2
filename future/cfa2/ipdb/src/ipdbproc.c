/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: ipdbproc.c,v 1.8 2013/12/16 15:27:56 siva Exp $                */
/*****************************************************************************/
/*    FILE  NAME            : ipdbproc.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : IP Binding Database management                 */
/*    MODULE NAME           : Ip Binding Database management processing      */
/*                            module                                         */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains processing functions for    */
/*                            IP Binding Database management module          */
/*---------------------------------------------------------------------------*/

#include "ipdbinc.h"
#include "ipdbextn.h"

/*****************************************************************************/
/* Function Name      : IpdbProcCreateStaticEntry                            */
/*                                                                           */
/* Description        : This function is to create a static binding entry    */
/*                      Warning: There is no check whether the entry exist or*/
/*                      not. That check have to be done before calling this  */
/*                      function                                             */
/*                                                                           */
/* Input(s)           : u4ContextId - Context identifier                     */
/*                      HostMac     - Host MAC Address                       */
/*                      VlanId      - VLAN Identifier                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NULL/Ponter to the entry created.                    */
/*****************************************************************************/
tIpDbStaticBindingEntry *
IpdbProcCreateStaticEntry (UINT4 u4ContextId, tMacAddr HostMac, tVlanId VlanId)
{
    tIpDbStaticBindingEntry *pIpDbStaticBindingEntry = NULL;

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_ENTRY, IPDB_MODULE_NAME,
                   "Entering the function %s \r\n", __FUNCTION__);

    IPDB_TRC_ARG3 (IPDB_TRC_FLAG, IPDB_FN_ARGS, IPDB_MODULE_NAME,
                   "The arguments to the function %s are HostMac %s, VlanId %d"
                   "\r\n", __FUNCTION__, IPDB_MAC_TO_STR (HostMac), VlanId);

    if ((pIpDbStaticBindingEntry = (tIpDbStaticBindingEntry *)
         IPDB_ALLOC_MEM_BLOCK (IPDB_STATIC_MEMPOOL_ID)) == NULL)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Failed to allocate memory for static entry\r\n");
        return NULL;
    }

    MEMSET (pIpDbStaticBindingEntry, IPDB_ZERO,
            sizeof (tIpDbStaticBindingEntry));

    pIpDbStaticBindingEntry->u4ContextId = u4ContextId;
    MEMCPY (pIpDbStaticBindingEntry->HostMac, HostMac, sizeof (tMacAddr));
    pIpDbStaticBindingEntry->VlanId = VlanId;
    pIpDbStaticBindingEntry->u1RowStatus = IPDB_NOT_READY;

    /* Add the entry to RBTree */
    if (RBTreeAdd (IPDB_STATIC_RBTREE, pIpDbStaticBindingEntry) != RB_SUCCESS)
    {
        IPDB_RELEASE_MEM_BLOCK (IPDB_STATIC_MEMPOOL_ID,
                                pIpDbStaticBindingEntry);
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Failed to add the static entry to RBTree\r\n");
        return NULL;
    }
    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);

    return pIpDbStaticBindingEntry;
}

/*****************************************************************************/
/* Function Name      : IpdbProcCreateBindingEntry                           */
/*                                                                           */
/* Description        : This function is to create a IP binding entry        */
/*                      Warning: There is no check whether the entry exist or*/
/*                      not. That check have to be done before calling this  */
/*                      function                                             */
/*                                                                           */
/* Input(s)           : HostMac       - Host MAC Address                     */
/*                      VlanId        - VLAN Identifier                      */
/*                      u4HostIp      - Host IP Address                      */
/*                      u1BindingType - Type of binding                      */
/*                      u4ContextID   - Context Identifier                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NULL/Ponter to the entry created.                    */
/*****************************************************************************/
tIpDbBindingEntry  *
IpdbProcCreateBindingEntry (UINT4 u4ContextID, tMacAddr HostMac,
                            tVlanId VlanId, UINT4 u4HostIp,
                            UINT4 u4IfIndex, UINT1 u1BindingType)
{
    tIpDbBindingEntry  *pIpDbBindingEntry = NULL;
    tIpDbIfaceEntry    *pIpDbIfaceEntry = NULL;
    tAclFilterInfo      AclFilterInfo;

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_ENTRY, IPDB_MODULE_NAME,
                   "Entering the function %s \r\n", __FUNCTION__);

    IPDB_TRC_ARG4 (IPDB_TRC_FLAG, IPDB_FN_ARGS, IPDB_MODULE_NAME,
                   "The arguments to the function %s are HostMac %s\n, VlanId "
                   "%d,Binding type %d \r\n", __FUNCTION__,
                   IPDB_MAC_TO_STR (HostMac), VlanId, u1BindingType);

    MEMSET (&AclFilterInfo, 0x0, sizeof (tAclFilterInfo));

    /* Check whether the entry already exists or not */
    pIpDbIfaceEntry = IpdbIntfGetEntry (u4ContextID, VlanId);
    if (pIpDbIfaceEntry == NULL)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Failed to get the interface entry\r\n");

        return NULL;
    }

    if ((pIpDbBindingEntry = (tIpDbBindingEntry *)
         IPDB_ALLOC_MEM_BLOCK (IPDB_BINDING_MEMPOOL_ID)) == NULL)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Failed to allocate memory for binding entry\r\n");
        return NULL;
    }

    MEMSET (pIpDbBindingEntry, IPDB_ZERO, sizeof (tIpDbBindingEntry));

    pIpDbBindingEntry->u4ContextId = u4ContextID;
    MEMCPY (pIpDbBindingEntry->HostMac, HostMac, sizeof (tMacAddr));
    pIpDbBindingEntry->VlanId = VlanId;
    pIpDbBindingEntry->u4HostIp = u4HostIp;
    pIpDbBindingEntry->u4InIfIndex = u4IfIndex;
    pIpDbBindingEntry->u1BindingType = u1BindingType;

    pIpDbBindingEntry->LeaseTimerNode.u1Status = IPDB_DISABLED;

    /* Initialize the gateway entries SLL */
    TMO_SLL_Init (&(pIpDbBindingEntry->NetworkList));

    /* Add the entry to RBTree */
    if (RBTreeAdd (IPDB_MAC_BINDING_RBTREE, pIpDbBindingEntry) != RB_SUCCESS)
    {
        IPDB_RELEASE_MEM_BLOCK (IPDB_BINDING_MEMPOOL_ID, pIpDbBindingEntry);

        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Failed to add the binding entry to RBTree\r\n");
        return NULL;
    }

    /* To avoid the creation of 2 static IP binding entry with same
     * IP address, but with MAC address / VLAN ID
     if (RBTreeAdd (IPDB_IP_BINDING_RBTREE, pIpDbBindingEntry) != RB_SUCCESS)
     {
     RBTreeRemove (IPDB_MAC_BINDING_RBTREE, pIpDbBindingEntry);
     IPDB_RELEASE_MEM_BLOCK (IPDB_BINDING_MEMPOOL_ID, pIpDbBindingEntry);

     IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
     "Failed to add the binding entry to RBTree\r\n");
     return NULL;
     }
     */

    MEMCPY (AclFilterInfo.HostMac, HostMac, MAC_ADDR_LEN);
    AclFilterInfo.u2VlanId = VlanId;
    AclFilterInfo.u4HostIp = u4HostIp;
    AclFilterInfo.u1ProtocolId = ISS_PROTO_ANY;
    AclFilterInfo.u1Action = ISS_ALLOW;
    AclFilterInfo.u1Priority = IPDB_ISS_DEFAULT_PRIORITY;
    AclFilterInfo.u1FilterType = ISS_UDBFILTER;
    OSIX_BITLIST_SET_BIT (AclFilterInfo.PortList, u4IfIndex,
                          sizeof (tPortList));

    if (IssACLCreateFilter (&AclFilterInfo,
                            &(pIpDbBindingEntry->u4L2FilterId),
                            &(pIpDbBindingEntry->u4L3FilterId),
                            &(pIpDbBindingEntry->u4UDBFilterId)) == ISS_FAILURE)
    {
        RBTreeRemove (IPDB_MAC_BINDING_RBTREE, pIpDbBindingEntry);
        IPDB_RELEASE_MEM_BLOCK (IPDB_BINDING_MEMPOOL_ID, pIpDbBindingEntry);

        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Failed to add the binding entry to ACL table\r\n");
        return NULL;
    }

    /* Update the Global and interface specific statistics */
    IPDB_GLOB_INCR_BINDINGS;
    IPDB_INTF_INCR_BINDINGS (pIpDbIfaceEntry);

    if (u1BindingType == IPDB_STATIC_BINDING)
    {
        IPDB_GLOB_INCR_STAT_BINDINGS;
        IPDB_INTF_INCR_STAT_BINDS (pIpDbIfaceEntry);
    }
    else if (u1BindingType == IPDB_DHCP_BINDING)
    {
        IPDB_GLOB_INCR_DHCP_BINDINGS;
        IPDB_INTF_INCR_DHCP_BINDS (pIpDbIfaceEntry);
    }
    else if (u1BindingType == IPDB_PPP_BINDING)
    {
        IPDB_GLOB_INCR_PPP_BINDINGS;
        IPDB_INTF_INCR_PPP_BINDS (pIpDbIfaceEntry);
    }

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);

    return pIpDbBindingEntry;
}

/*****************************************************************************/
/* Function Name      : IpdbProcDeleteBindingEntry                           */
/*                                                                           */
/* Description        : This function is to delete a IP binding entry        */
/*                                                                           */
/* Input(s)           : u4ContextID   - Context Identifier                   */
/*                      HostMac       - Host MAC Address                     */
/*                      VlanId        - VLAN Identifier                      */
/*                      u4InIfIndex   - Incoming interface index             */
/*                      u4HostIp      - Host IP address                      */
/*                      u1BindingType - Type of binding                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IPDB_SUCCESS/IPDB_FAILURE                            */
/*****************************************************************************/
INT4
IpdbProcDeleteBindingEntry (UINT4 u4ContextID, tMacAddr HostMac,
                            tVlanId VlanId, UINT4 u4InIfIndex, UINT4 u4HostIp,
                            UINT1 u1BindingType)
{
    tIpDbBindingEntry   IpDbBindingEntry;
    tIpDbBindingEntry  *pIpDbBindingEntry = NULL;
    tIpDbGatewayEntry  *pIpDbGatewayEntry = NULL;
    tIpDbIfaceEntry    *pIpDbIfaceEntry = NULL;
    INT4                i4RetVal = IPDB_SUCCESS;

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_ENTRY, IPDB_MODULE_NAME,
                   "Entering the function %s \r\n", __FUNCTION__);

    IPDB_TRC_ARG5 (IPDB_TRC_FLAG, IPDB_FN_ARGS, IPDB_MODULE_NAME,
                   "The arguments to the function %s are HostMac %s\n, "
                   "VlanId %d, InIndex %d, Binding type %d \r\n",
                   __FUNCTION__, IPDB_MAC_TO_STR (HostMac), VlanId, u4InIfIndex,
                   u1BindingType);

    MEMSET (&IpDbBindingEntry, IPDB_ZERO, sizeof (tIpDbBindingEntry));

    IpDbBindingEntry.u4ContextId = u4ContextID;
    MEMCPY (IpDbBindingEntry.HostMac, HostMac, sizeof (tMacAddr));
    IpDbBindingEntry.VlanId = VlanId;

    /* get the entry from RBTree */
    pIpDbBindingEntry = (tIpDbBindingEntry *)
        RBTreeGet (IPDB_MAC_BINDING_RBTREE, &IpDbBindingEntry);

    if (pIpDbBindingEntry == NULL)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "The binding entry does not exist\r\n");
        return IPDB_SUCCESS;
    }

    /* If the incoming interface index is given, validate it against the value 
     * in the existing record */
    if (u4InIfIndex != IPDB_ZERO)
    {
        if (pIpDbBindingEntry->u4InIfIndex != u4InIfIndex)
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "exiting because the incoming index does not match\r\n");
            return IPDB_FAILURE;
        }
    }

    /* If the Host IP is given, validate it against the value in the existing 
     * record */
    if (u4HostIp != IPDB_ZERO)
    {
        if (pIpDbBindingEntry->u4HostIp != u4HostIp)
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "exiting because the Binding IDs does not match\r\n");
            return IPDB_FAILURE;
        }
    }

    /* If the binding Type is given, validate it against the value in the existing
     * record */
    if (u1BindingType != IPDB_ZERO)
    {
        if (pIpDbBindingEntry->u1BindingType != u1BindingType)
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "exiting because the Binding Types does not match\r\n");
            return IPDB_FAILURE;
        }
    }

    /* Update the global and interface specific statistics */
    IPDB_GLOB_DECR_BINDINGS;
    if (pIpDbBindingEntry->u1BindingType == IPDB_STATIC_BINDING)
    {
        IPDB_GLOB_DECR_STAT_BINDINGS;
    }
    else if (pIpDbBindingEntry->u1BindingType == IPDB_DHCP_BINDING)
    {
        IPDB_GLOB_DECR_DHCP_BINDINGS;
    }
    else if (pIpDbBindingEntry->u1BindingType == IPDB_PPP_BINDING)
    {
        IPDB_GLOB_DECR_PPP_BINDINGS;
    }

    pIpDbIfaceEntry = IpdbIntfCheckIfEntryExist (u4ContextID, VlanId);
    if (pIpDbIfaceEntry != NULL)
    {
        IPDB_INTF_DECR_BINDINGS (pIpDbIfaceEntry);

        if (pIpDbBindingEntry->u1BindingType == IPDB_STATIC_BINDING)
        {
            IPDB_INTF_DECR_STAT_BINDS (pIpDbIfaceEntry);
        }
        else if (pIpDbBindingEntry->u1BindingType == IPDB_DHCP_BINDING)
        {
            IPDB_INTF_DECR_DHCP_BINDS (pIpDbIfaceEntry);
        }
        else if (pIpDbBindingEntry->u1BindingType == IPDB_PPP_BINDING)
        {
            IPDB_INTF_DECR_PPP_BINDS (pIpDbIfaceEntry);
        }
    }

    /* Delete all the Gateway entries */
    while ((pIpDbGatewayEntry = (tIpDbGatewayEntry *)
            TMO_SLL_Get (&(pIpDbBindingEntry->NetworkList))) != NULL)
    {
        TMO_SLL_Delete (&(pIpDbBindingEntry->NetworkList),
                        &(pIpDbGatewayEntry->GatewayNode));

        IPDB_RELEASE_MEM_BLOCK (IPDB_GW_POOL_ID, pIpDbGatewayEntry);
    }

    /* If the Lease timer is running, stop it */
    if (pIpDbBindingEntry->LeaseTimerNode.u1Status == IPDB_ENABLED)
    {
        TmrStop (IPDB_TIMER_LIST, &(pIpDbBindingEntry->LeaseTimerNode.TmrBlk));
        pIpDbBindingEntry->LeaseTimerNode.u1Status = IPDB_DISABLED;
    }

    IssACLDeleteFilter (ISS_UDBFILTER, pIpDbBindingEntry->u4L2FilterId,
                        pIpDbBindingEntry->u4L3FilterId,
                        pIpDbBindingEntry->u4UDBFilterId);

    /* Remove the entry from RBTree */
    if (RBTreeRemove (IPDB_MAC_BINDING_RBTREE, pIpDbBindingEntry) != RB_SUCCESS)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Failed to remove the entry from RBTree\r\n");
        i4RetVal = IPDB_FAILURE;
    }

    /* To avoid the creation of 2 static IP binding entry with same
     * IP address, but with MAC address / VLAN ID
     if (RBTreeRemove (IPDB_IP_BINDING_RBTREE, pIpDbBindingEntry) != RB_SUCCESS)
     {
     IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
     "Failed to remove the entry from RBTree\r\n");
     i4RetVal = IPDB_FAILURE;
     }
     */

    IPDB_RELEASE_MEM_BLOCK (IPDB_BINDING_MEMPOOL_ID, pIpDbBindingEntry);

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : IpdbProcChangeStaticEntry                            */
/*                                                                           */
/* Description        : This function is to update the rowstatus of any      */
/*                      static binding entry                                 */
/*                                                                           */
/* Input(s)           : u4ContextID   - Context Identifier                   */
/*                      HostMac       - Host MAC Address                     */
/*                      VlanId        - VLAN Identifier                      */
/*                      u1Status      - New rowstatus                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IPDB_SUCCESS/IPDB_FAILURE                            */
/*****************************************************************************/
INT4
IpdbProcChangeStaticEntry (UINT4 u4ContextID, tMacAddr HostMac,
                           tVlanId VlanId, UINT1 u1Status)
{
    tIpDbStaticBindingEntry IpDbStaticBindingEntry;
    tIpDbStaticBindingEntry *pIpDbStaticBindingEntry = NULL;
    tIpDbGatewayEntry  *pIpDbGatewayEntry = NULL;
    tIpDbBindingEntry  *pIpDbBindingEntry = NULL;
    INT4                i4RetStat = IPDB_SUCCESS;

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_ENTRY, IPDB_MODULE_NAME,
                   "Entering the function %s \r\n", __FUNCTION__);

    IPDB_TRC_ARG5 (IPDB_TRC_FLAG, IPDB_FN_ARGS, IPDB_MODULE_NAME,
                   "The arguments to the function %s are HostMac %s\n, VlanId "
                   "%d, rowstatus %d and Context ID %d \r\n", __FUNCTION__,
                   IPDB_MAC_TO_STR (HostMac), VlanId, u1Status, u4ContextID);

    MEMSET (&IpDbStaticBindingEntry, IPDB_ZERO,
            sizeof (tIpDbStaticBindingEntry));

    IpDbStaticBindingEntry.u4ContextId = u4ContextID;
    IpDbStaticBindingEntry.VlanId = VlanId;
    MEMCPY (&(IpDbStaticBindingEntry.HostMac), HostMac, sizeof (tMacAddr));

    /* Get the static entry from RBTree */
    pIpDbStaticBindingEntry = (tIpDbStaticBindingEntry *)
        RBTreeGet (IPDB_STATIC_RBTREE, &IpDbStaticBindingEntry);

    if ((u1Status == IPDB_CREATE_AND_WAIT) || (u1Status == IPDB_CREATE_AND_GO))
    {
        /* if the new status is CREATE_AND_WAIT or CREATE_AND_GO, and if the 
         * entry already exist, return failure */
        if (pIpDbStaticBindingEntry != NULL)
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "Entry already exists\r\n");
            return IPDB_FAILURE;
        }

        /* Create the static entry; the rowstatus will be set as not-ready as
         * other fields like ip-address and gateway needs to be set */
        pIpDbStaticBindingEntry =
            IpdbProcCreateStaticEntry (u4ContextID, HostMac, VlanId);

        if (pIpDbStaticBindingEntry == NULL)
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "Failed to create the binding entry\r\n");
            return IPDB_FAILURE;
        }
        return IPDB_SUCCESS;
    }

    /* If the new rowstatus is not CREATE_AND_WAIT and CREATE_AND_GO, and if 
     * the entry does not exist, return failure */
    if (pIpDbStaticBindingEntry == NULL)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "The static binding entry does not exist\r\n");
        return IPDB_FAILURE;
    }

    /* If the existing status is the same, return success */
    if (u1Status == pIpDbStaticBindingEntry->u1RowStatus)
    {
        IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                       "Exiting the function %s \r\n", __FUNCTION__);

        return IPDB_SUCCESS;
    }

    if (u1Status == IPDB_ACTIVE)
    {
        /* If the new rowstatus is ACTIVE, and if IP-address, GatewayIP, and
         * incoming port fields are not set, return failure */
        if ((pIpDbStaticBindingEntry->u4HostIp == IPDB_ZERO) ||
            (pIpDbStaticBindingEntry->u4InIfIndex == IPDB_ZERO))
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "Necessary fields are not filled; hence cannot make"
                      " the entry active\r\n");
            pIpDbStaticBindingEntry->u1RowStatus = IPDB_NOT_READY;
            return IPDB_FAILURE;
        }

        /* once, the static entry is activated, add it to the binding table */
        pIpDbBindingEntry =
            IpdbProcCreateBindingEntry (u4ContextID, HostMac, VlanId,
                                        pIpDbStaticBindingEntry->u4HostIp,
                                        pIpDbStaticBindingEntry->u4InIfIndex,
                                        IPDB_STATIC_BINDING);
        if (pIpDbBindingEntry == NULL)
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "Failed to create the binding entry\r\n");
            return IPDB_FAILURE;
        }

        if ((pIpDbGatewayEntry = (tIpDbGatewayEntry *)
             IPDB_ALLOC_MEM_BLOCK (IPDB_GW_POOL_ID)) == NULL)
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "Failed to allocate memory for gateway entries\r\n");
            return IPDB_FAILURE;
        }

        MEMSET (pIpDbGatewayEntry, IPDB_ZERO, sizeof (tIpDbGatewayEntry));

        TMO_SLL_Init_Node (&(pIpDbGatewayEntry->GatewayNode));
        pIpDbGatewayEntry->u4GatewayIp = pIpDbStaticBindingEntry->u4GatewayIp;

        TMO_SLL_Add (&(pIpDbBindingEntry->NetworkList),
                     &(pIpDbGatewayEntry->GatewayNode));

        pIpDbStaticBindingEntry->u1RowStatus = IPDB_ACTIVE;

        return IPDB_SUCCESS;
    }

    if (u1Status == IPDB_DESTROY)
    {
        if (pIpDbStaticBindingEntry->u1RowStatus == IPDB_ACTIVE)
        {
            /* If the new rowstatus is DESTROY, and the existing rowstatus is 
             * ACTIVE, then delete the entry first from binding table */
            if (IpdbProcDeleteBindingEntry (u4ContextID, HostMac, VlanId,
                                            IPDB_ZERO, IPDB_ZERO,
                                            IPDB_STATIC_BINDING)
                != IPDB_SUCCESS)
            {
                IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                          "Failed to remove the binding entry\r\n");
                i4RetStat = IPDB_FAILURE;
            }
        }

        /* Remove the static binding entry from RBTree and free its memory */
        if (RBTreeRemove (IPDB_STATIC_RBTREE, pIpDbStaticBindingEntry) !=
            RB_SUCCESS)
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "Failed to remove the entry from RBTree\r\n");
            i4RetStat = IPDB_FAILURE;
        }

        IPDB_RELEASE_MEM_BLOCK (IPDB_STATIC_MEMPOOL_ID,
                                pIpDbStaticBindingEntry);
        return i4RetStat;
    }

    if (u1Status == IPDB_NOT_IN_SERVICE)
    {
        if (pIpDbStaticBindingEntry->u1RowStatus == IPDB_ACTIVE)
        {
            /* If the new rowstatus is NOT_IN_SERVICE, and the existing 
             * rowstatus is ACTIVE, then delete the entry first from 
             * binding table */
            if (IpdbProcDeleteBindingEntry (u4ContextID, HostMac, VlanId,
                                            IPDB_ZERO, IPDB_ZERO,
                                            IPDB_STATIC_BINDING)
                != IPDB_SUCCESS)
            {
                IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                          "Failed to remove the binding entry\r\n");

                i4RetStat = IPDB_FAILURE;
            }
        }
        pIpDbStaticBindingEntry->u1RowStatus = u1Status;
        return i4RetStat;
    }

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);

    return IPDB_FAILURE;
}

/*****************************************************************************/
/* Function Name      : IpdbProcUpdateBindingEntry                           */
/*                                                                           */
/* Description        : This function is to update a binding entry, based on */
/*                      the new informations given                           */
/*                      warning: For static entry updation, call the function*/
/*                      IpdbProcChangeStaticEntry                            */
/*                                                                           */
/* Input(s)           : pIpDbEntry - Pointer to the binding entry info.      */
/*                      u4NoOfGWEntries - Number of gateways alloed for host */
/*                      pIpDbGateway - Gateway IP entries                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IPDB_SUCCESS/IPDB_FAILURE                            */
/*****************************************************************************/
INT4
IpdbProcUpdateBindingEntry (tIpDbEntry * pIpDbEntry, UINT4 u4NoOfGWEntries,
                            tIpDbGateway * pIpDbGateway)
{
    tIpDbBindingEntry   IpDbBindingEntry;
    tIpDbBindingEntry  *pIpDbBindingEntry = NULL;
    tIpDbGatewayEntry  *pIpDbGatewayEntry = NULL;
    tAclFilterInfo      AclFilterInfo;

    /* As of now, we are handling only default gateway. so u4NoOfGWEntries is
     * always assumed to be one */

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_ENTRY, IPDB_MODULE_NAME,
                   "Entering the function %s \r\n", __FUNCTION__);

    MEMSET (&IpDbBindingEntry, IPDB_ZERO, sizeof (tIpDbBindingEntry));
    MEMSET (&AclFilterInfo, 0x0, sizeof (tAclFilterInfo));

    if (pIpDbEntry->u1BindingType == IPDB_STATIC_BINDING)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "This function will not handle static entries\r\n");
        return IPDB_FAILURE;
    }

    IpDbBindingEntry.u4ContextId = pIpDbEntry->u4ContextId;
    MEMCPY (IpDbBindingEntry.HostMac, pIpDbEntry->HostMac,
            sizeof (tMacAddress));
    IpDbBindingEntry.VlanId = pIpDbEntry->VlanId;

    /* Get the binding entry */
    pIpDbBindingEntry = (tIpDbBindingEntry *)
        RBTreeGet (IPDB_MAC_BINDING_RBTREE, &IpDbBindingEntry);

    if (pIpDbBindingEntry == NULL)
    {
        if (u4NoOfGWEntries == IPDB_ZERO)
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "No.of Gateway Entries Should be atleast one\r\n");
            return IPDB_FAILURE;
        }

        if ((pIpDbEntry->u4HostIp == IPDB_ZERO) ||
            (pIpDbEntry->u4InIfIndex == IPDB_ZERO) ||
            ((pIpDbEntry->u1BindingType != IPDB_PPP_BINDING) &&
             (pIpDbEntry->u1BindingType != IPDB_DHCP_BINDING)))
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "Invalid arguments passed \r\n");

            return IPDB_FAILURE;
        }

        /* If the binding entry is not there, create one */
        pIpDbBindingEntry =
            IpdbProcCreateBindingEntry (pIpDbEntry->u4ContextId,
                                        pIpDbEntry->HostMac,
                                        pIpDbEntry->VlanId,
                                        pIpDbEntry->u4HostIp,
                                        pIpDbEntry->u4InIfIndex,
                                        pIpDbEntry->u1BindingType);
        if (pIpDbBindingEntry == NULL)
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "Failed to create the binding entry\r\n");

            return IPDB_FAILURE;
        }

        pIpDbBindingEntry->u4BindingId = pIpDbEntry->u4BindingId;

        if ((pIpDbGatewayEntry = (tIpDbGatewayEntry *)
             IPDB_ALLOC_MEM_BLOCK (IPDB_GW_POOL_ID)) == NULL)
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "Failed to allocate memory for the gateway entry\r\n");

            return IPDB_FAILURE;
        }

        MEMSET (pIpDbGatewayEntry, IPDB_ZERO, sizeof (tIpDbGatewayEntry));

        TMO_SLL_Init_Node (&(pIpDbGatewayEntry->GatewayNode));

        /* To be changed while handling multiple gateways */
        pIpDbGatewayEntry->u4GatewayIp = pIpDbGateway->u4GatewayIp;

        TMO_SLL_Add (&(pIpDbBindingEntry->NetworkList),
                     &(pIpDbGatewayEntry->GatewayNode));

        if (pIpDbEntry->u4LeaseTime != IPDB_ZERO)
        {
            /* Start the lease timer */
            TmrStart (IPDB_TIMER_LIST,
                      &(pIpDbBindingEntry->LeaseTimerNode.TmrBlk),
                      IPDB_BINDING_TIMER, pIpDbEntry->u4LeaseTime, IPDB_ZERO);

            pIpDbBindingEntry->LeaseTimerNode.u1Status = IPDB_ENABLED;
        }
    }
    else
    {
        /* The entry is already there, so update it eith the new
         * informations given */
        if (pIpDbEntry->u4HostIp != IPDB_ZERO)
        {
            pIpDbBindingEntry->u4HostIp = pIpDbEntry->u4HostIp;
        }
        if (pIpDbEntry->u4InIfIndex != IPDB_ZERO)
        {
            pIpDbBindingEntry->u4InIfIndex = pIpDbEntry->u4InIfIndex;
        }
        if (pIpDbEntry->u4BindingId != IPDB_ZERO)
        {
            pIpDbBindingEntry->u4BindingId = pIpDbEntry->u4BindingId;
        }
        if (pIpDbEntry->u1BindingType != IPDB_ZERO)
        {
            pIpDbBindingEntry->u1BindingType = pIpDbEntry->u1BindingType;
        }
        if (u4NoOfGWEntries != IPDB_ZERO)
        {
            /* ASSUMPTION: There will be only one Gateway per binding entry. 
               To be changed while handling multiple gateways */
            if (pIpDbGateway->u4GatewayIp != IPDB_ZERO)
            {
                pIpDbGatewayEntry = (tIpDbGatewayEntry *)
                    TMO_SLL_First (&(pIpDbBindingEntry->NetworkList));

                if (pIpDbGatewayEntry == NULL)
                {
                    if ((pIpDbGatewayEntry = (tIpDbGatewayEntry *)
                         IPDB_ALLOC_MEM_BLOCK (IPDB_GW_POOL_ID)) == NULL)
                    {
                        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC,
                                  IPDB_MODULE_NAME, "Failed to allocate memory"
                                  "for the gateway entry\r\n");

                        return IPDB_FAILURE;
                    }

                    MEMSET (pIpDbGatewayEntry, IPDB_ZERO,
                            sizeof (tIpDbGatewayEntry));

                    TMO_SLL_Init_Node (&(pIpDbGatewayEntry->GatewayNode));

                    pIpDbGatewayEntry->u4GatewayIp = pIpDbGateway->u4GatewayIp;

                    TMO_SLL_Add (&(pIpDbBindingEntry->NetworkList),
                                 &(pIpDbGatewayEntry->GatewayNode));
                }
                else
                {
                    /* To be changed while handling multiple gateways */
                    pIpDbGatewayEntry->u4GatewayIp = pIpDbGateway->u4GatewayIp;
                }
            }
        }

        /* While updation of IPDb entry, delete the ACL entry 
         * and create the entry again with the updated information */
        IssACLDeleteFilter (ISS_UDBFILTER, pIpDbBindingEntry->u4L2FilterId,
                            pIpDbBindingEntry->u4L3FilterId,
                            pIpDbBindingEntry->u4UDBFilterId);

        MEMCPY (AclFilterInfo.HostMac, pIpDbBindingEntry->HostMac,
                MAC_ADDR_LEN);
        AclFilterInfo.u2VlanId = pIpDbBindingEntry->VlanId;
        AclFilterInfo.u4HostIp = pIpDbBindingEntry->u4HostIp;
        AclFilterInfo.u1ProtocolId = ISS_PROTO_ANY;
        AclFilterInfo.u1Action = ISS_ALLOW;
        AclFilterInfo.u1Priority = IPDB_ISS_DEFAULT_PRIORITY;
        AclFilterInfo.u1FilterType = ISS_UDBFILTER;
        OSIX_BITLIST_SET_BIT (AclFilterInfo.PortList,
                              pIpDbBindingEntry->u4InIfIndex,
                              sizeof (tPortList));

        pIpDbBindingEntry->u4L2FilterId = IPDB_ZERO;
        pIpDbBindingEntry->u4L3FilterId = IPDB_ZERO;
        pIpDbBindingEntry->u4UDBFilterId = IPDB_ZERO;

        if (IssACLCreateFilter (&AclFilterInfo,
                                &(pIpDbBindingEntry->u4L2FilterId),
                                &(pIpDbBindingEntry->u4L3FilterId),
                                &(pIpDbBindingEntry->u4UDBFilterId))
            == ISS_FAILURE)
        {
            RBTreeRemove (IPDB_MAC_BINDING_RBTREE, pIpDbBindingEntry);
            IPDB_RELEASE_MEM_BLOCK (IPDB_BINDING_MEMPOOL_ID, pIpDbBindingEntry);
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "Failed to add the binding entry to ACL table\r\n");
            return IPDB_FAILURE;
        }

        if (pIpDbEntry->u4LeaseTime != IPDB_ZERO)
        {
            /* Start the timer if a lease time value is given */
            if (pIpDbBindingEntry->LeaseTimerNode.u1Status == IPDB_ENABLED)
            {
                /* If the timer is already running, stop it first */
                TmrStop (IPDB_TIMER_LIST,
                         &(pIpDbBindingEntry->LeaseTimerNode.TmrBlk));
            }
            TmrStart (IPDB_TIMER_LIST,
                      &(pIpDbBindingEntry->LeaseTimerNode.TmrBlk),
                      IPDB_BINDING_TIMER, pIpDbEntry->u4LeaseTime, IPDB_ZERO);

            pIpDbBindingEntry->LeaseTimerNode.u1Status = IPDB_ENABLED;
        }
    }

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);
    return IPDB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IpdbProcDeleteEntries                                */
/*                                                                           */
/* Description        : This function is to delete binding entries as a      */
/*                      bunch.                                               */
/*                                                                           */
/* Input(s)           : u1BindingType - Type of binding                      */
/*                      u4ContextID   - Context Identifier                   */
/*                      VlanId - VLAN Identifier                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IPDB_SUCCESS/IPDB_FAILURE                            */
/*****************************************************************************/
INT4
IpdbProcDeleteEntries (UINT1 u1BindingType, UINT4 u4ContextID, tVlanId VlanId)
{
    tIpDbStaticBindingEntry *pIpDbStaticBindingEntry = NULL;
    tIpDbBindingEntry  *pIpDbBindingEntry = NULL;
    tIpDbStaticBindingEntry *pIpDbNextStaticBindingEntry = NULL;
    tIpDbBindingEntry  *pIpDbNextBindingEntry = NULL;
    INT4                i4RetStat = IPDB_SUCCESS;

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_ENTRY, IPDB_MODULE_NAME,
                   "Entering the function %s \r\n", __FUNCTION__);

    IPDB_TRC_ARG4 (IPDB_TRC_FLAG, IPDB_FN_ARGS, IPDB_MODULE_NAME,
                   "The arguments to the function %s VlanId %d, Binding type "
                   "%d Context ID : %d \r\n", __FUNCTION__, VlanId,
                   u1BindingType, u4ContextID);

    if (u1BindingType != IPDB_STATIC_BINDING)
    {
        pIpDbBindingEntry = (tIpDbBindingEntry *)
            RBTreeGetFirst (IPDB_MAC_BINDING_RBTREE);

        /* Loop through the complete RBTree of binding entries */
        while (pIpDbBindingEntry != NULL)
        {
            pIpDbNextBindingEntry = (tIpDbBindingEntry *)
                RBTreeGetNext (IPDB_MAC_BINDING_RBTREE, pIpDbBindingEntry,
                               NULL);

            if ((pIpDbBindingEntry->u1BindingType == u1BindingType) &&
                (pIpDbBindingEntry->u4ContextId == u4ContextID) &&
                ((VlanId == IPDB_ZERO) ||
                 (pIpDbBindingEntry->VlanId == VlanId)))
            {
                /* Delete the entries */
                if (IpdbProcDeleteBindingEntry (u4ContextID,
                                                pIpDbBindingEntry->HostMac,
                                                pIpDbBindingEntry->VlanId,
                                                IPDB_ZERO, IPDB_ZERO,
                                                u1BindingType) != IPDB_SUCCESS)
                {
                    IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                              "Failed to delete the binding entry\r\n");

                    i4RetStat = IPDB_FAILURE;
                }
            }

            pIpDbBindingEntry = pIpDbNextBindingEntry;
        }
    }
    else if (u1BindingType == IPDB_STATIC_BINDING)
    {
        /* The Binding ID given is STATIC. So loop through the static Array */
        pIpDbStaticBindingEntry = (tIpDbStaticBindingEntry *)
            RBTreeGetFirst (IPDB_STATIC_RBTREE);

        /* Loop through the complete RBTree of binding entries */
        while (pIpDbStaticBindingEntry != NULL)
        {
            pIpDbNextStaticBindingEntry = (tIpDbStaticBindingEntry *)
                RBTreeGetNext (IPDB_STATIC_RBTREE, pIpDbStaticBindingEntry,
                               NULL);

            if ((pIpDbStaticBindingEntry->u4ContextId == u4ContextID) &&
                ((VlanId == IPDB_ZERO) ||
                 (pIpDbStaticBindingEntry->VlanId == VlanId)))
            {
                /* Delete the entries */
                if (IpdbProcChangeStaticEntry (u4ContextID,
                                               pIpDbStaticBindingEntry->HostMac,
                                               pIpDbStaticBindingEntry->VlanId,
                                               IPDB_DESTROY) != IPDB_SUCCESS)
                {
                    IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                              "Failed to delete the static binding entry\r\n");

                    i4RetStat = IPDB_FAILURE;
                }
            }

            pIpDbStaticBindingEntry = pIpDbNextStaticBindingEntry;
        }
    }
    else
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Invalid binding type\r\n");
        i4RetStat = IPDB_FAILURE;
    }

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);
    return i4RetStat;
}

/*                                                                           */
/***************************** END OF FILE ***********************************/
/*                                                                           */
