/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: ipdbarp.c,v 1.7 2013/12/12 11:46:23 siva Exp $                 */
/*****************************************************************************/
/*    FILE  NAME            : ipdbarp.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : IP Binding Database management                 */
/*    MODULE NAME           : Handling of ARP packets from customer and      */
/*                            network ports.                                 */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains funtions related to         */
/*                            handling of ARP packets.                       */
/*---------------------------------------------------------------------------*/

#include "ipdbinc.h"
#include "ipdbextn.h"
#include "cfa.h"
#include "bridge.h"

/*****************************************************************************/
/* Function Name      : IpdbHandleArpPacket                                  */
/*                                                                           */
/* Description        : This function forwards/drops the incoming ARP        */
/*                      packets based in the IPDB entries.                   */
/*                                                                           */
/* Input(s)           : pBuf       - Incoming packet buffer                  */
/*                      u4InPort   - Incoming Port                           */
/*                      VlanTag    - Classified VLAN Tag of the incoming     */
/*                                   packets                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IPDB_SUCCESS/IPDB_FAILURE                            */
/*****************************************************************************/
INT4
IpdbHandleArpPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ContextID,
                     UINT4 u4InPort, tVlanTag VlanTag)
{
    tPortCtrlEntry     *pPortCtrlEntry = NULL;
    tVlanCtrlEntry     *pVlanCtrlEntry = NULL;
    tPortList          *pPortList = NULL;
    tIpDbArpInfo        IpDbArpInfo;
    tMacAddr            SrcMacAddress = { IPDB_ZERO };
    INT4                i4RetVal = IPDB_ZERO;
    UINT1               au1RecvBuf[IPDB_ARP_MAX_PKT_LEN] = { IPDB_ZERO };
    UINT2               u2EtherOffset = IPDB_ZERO;
    tVlanId             VlanId = IPDB_ZERO;
    UINT1               u1PortSecState = IPDB_ZERO;
    UINT1               u1PortType = IPDB_ZERO;

    /* linear buffer used for copying the packet */
    MEMSET (&IpDbArpInfo, IPDB_ZERO, sizeof (tIpDbArpInfo));

    VlanId = VlanTag.OuterVlanTag.u2VlanId;

    IPDB_TRC_ARG4 (IPDB_TRC_FLAG, IPDB_FN_ARGS, IPDB_MODULE_NAME,
                   "Entering the function %s with arguments - VlanId %d, InPort"
                   " %d, Context ID %d\r\n", __FUNCTION__, VlanId, u4InPort,
                   u4ContextID);

    if (IpDbGetPortCtrlEntry (u4InPort, &pPortCtrlEntry) != IPDB_SUCCESS)
    {
        IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                       "Getting the port entry of port %d failed.\r\n",
                       u4InPort);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return IPDB_FAILURE;
    }

    IpDbGetVlanCtrlEntry (VlanId, &pVlanCtrlEntry);

    if (CfaGetIfPortType (u4InPort, &u1PortType) != CFA_SUCCESS)
    {
        IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                       "Getting the port state of port %d failed.\r\n",
                       u4InPort);
        IPDB_PORTCTRL_INCR_ARP_DROPPED (pPortCtrlEntry);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return IPDB_FAILURE;
    }

    /* Get the length of ethernet header (including VLAN tags) */
    if (IpdbPortGetEtherHdrLen (pBuf, u4InPort, &u2EtherOffset) != IPDB_SUCCESS)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Failed to get the ethernet header length.\r\n");

        IPDB_PORTCTRL_INCR_ARP_DROPPED (pPortCtrlEntry);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return IPDB_FAILURE;
    }

    /* Copy the CRU buffer contents in to a linear buffer */
    CRU_BUF_Copy_FromBufChain (pBuf, au1RecvBuf,
                               u2EtherOffset, IPDB_ARP_MAX_PKT_LEN);
    /* Get the SrcIP, DestIp and Src MAC from the ARP header */

    IpdbExtractArpHeader (au1RecvBuf, &IpDbArpInfo);

    /* Get the Src MAC from the packet Ethernet header */
    IPDB_COPY_FROM_BUF (pBuf, SrcMacAddress, IPDB_ETH_SRC_MAC_OFFSET,
                        sizeof (tMacAddr));

    /* Compare the ARP Src MAC and Ether Src MAC, if not equal the 
       drop the packet */
    if (MEMCMP (IpDbArpInfo.SrcMac, SrcMacAddress, sizeof (tMacAddr))
        != IPDB_ZERO)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Ethernet Src MAC doesn't match the ARP Src MAC,"
                  "So dropping the packet.\r\n");

        IPDB_PORTCTRL_INCR_ARP_DROPPED (pPortCtrlEntry);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return IPDB_FAILURE;
    }

    if ((IpDbArpInfo.u2Opcode != IPDB_ARP_REQUEST) &&
        (IpDbArpInfo.u2Opcode != IPDB_ARP_RESPONSE))
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Invalid ARP packet.\r\n");

        IPDB_PORTCTRL_INCR_ARP_DROPPED (pPortCtrlEntry);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return IPDB_FAILURE;
    }
    pPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList == NULL)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "IpdbHandleArpPacket: "
                  "Error in allocating memory for pPortList\r\n");
        IPDB_PORTCTRL_INCR_ARP_DROPPED (pPortCtrlEntry);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return IPDB_FAILURE;
    }
    MEMSET (*pPortList, 0, sizeof (tPortList));

    if (pVlanCtrlEntry == NULL)
    {
        i4RetVal = IpDbArpValidateIncomingPacket (&IpDbArpInfo, u4ContextID,
                                                  IPDB_DISABLED, u4InPort,
                                                  u1PortType, VlanId,
                                                  *pPortList);
    }
    else
    {
        i4RetVal = IpDbArpValidateIncomingPacket (&IpDbArpInfo, u4ContextID,
                                                  pVlanCtrlEntry->
                                                  u1MacForceFwdStatus, u4InPort,
                                                  u1PortType, VlanId,
                                                  *pPortList);
    }

    if (u1PortType == IPDB_PORT_TYPE_DOWNLINK)
    {
        CfaGetIfPortSecState (u4InPort, &u1PortSecState);

        /* Get the upstream PortList */
        if (CfaGetUpLinkPortList (u4ContextID, VlanId, *pPortList) !=
            CFA_SUCCESS)
        {
            IPDB_PORTCTRL_INCR_ARP_DROPPED (pPortCtrlEntry);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            FsUtilReleaseBitList ((UINT1 *) pPortList);
            return IPDB_FAILURE;
        }

        if (u1PortSecState == IPDB_PORT_SEC_STATE_TRUSTED)
        {
            if (IpDbArpInfo.u2Opcode == IPDB_ARP_REQUEST)
            {
                /* If the incoming port is trusted Send the pkt to all the
                 * uplink ports without any condition.*/
                if (IpdbApiProcessPktWithPortList (pBuf, u4ContextID, VlanTag,
                                                   u4InPort,
                                                   IpDbArpInfo.DestMac,
                                                   IpDbArpInfo.SrcMac,
                                                   *pPortList) != IPDB_SUCCESS)
                {
                    IPDB_PORTCTRL_INCR_ARP_DROPPED (pPortCtrlEntry);
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    FsUtilReleaseBitList ((UINT1 *) pPortList);
                    IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                              "IpdbApiProcessPktWithPortList Failed.\r\n");
                    return IPDB_FAILURE;
                }
            }
            else
            {
                if (IpdbApiProcessPktWithPortList (pBuf, u4ContextID, VlanTag,
                                                   u4InPort,
                                                   IpDbArpInfo.DestMac,
                                                   IpDbArpInfo.SrcMac,
                                                   NULL) != IPDB_SUCCESS)
                {
                    IPDB_PORTCTRL_INCR_ARP_DROPPED (pPortCtrlEntry);
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

                    FsUtilReleaseBitList ((UINT1 *) pPortList);
                    IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                              "IpdbApiProcessPktWithPortList Failed.\r\n");
                    return IPDB_FAILURE;
                }
            }

            IPDB_PORTCTRL_INCR_ARP_FWD (pPortCtrlEntry);

            FsUtilReleaseBitList ((UINT1 *) pPortList);
            IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                           "Exiting the function %s \r\n", __FUNCTION__);
            return IPDB_SUCCESS;
        }
        else
        {
            if (i4RetVal == IPDB_SUCCESS)
            {
                if (IpDbArpInfo.u2Opcode == IPDB_ARP_REQUEST)
                {
                    /* If the incoming port is not trusted, Send the pkt to all
                     * the uplink ports without any condition.*/
                    if (IpdbApiProcessPktWithPortList (pBuf, u4ContextID,
                                                       VlanTag, u4InPort,
                                                       IpDbArpInfo.DestMac,
                                                       IpDbArpInfo.SrcMac,
                                                       *pPortList) !=
                        IPDB_SUCCESS)
                    {
                        IPDB_PORTCTRL_INCR_ARP_DROPPED (pPortCtrlEntry);
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

                        FsUtilReleaseBitList ((UINT1 *) pPortList);
                        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC,
                                  IPDB_MODULE_NAME,
                                  "IpdbApiProcessPktWithPortList Failed.\r\n");
                        return IPDB_FAILURE;
                    }
                }
                else
                {
                    if (IpdbApiProcessPktWithPortList (pBuf, u4ContextID,
                                                       VlanTag, u4InPort,
                                                       IpDbArpInfo.DestMac,
                                                       IpDbArpInfo.SrcMac,
                                                       NULL) != IPDB_SUCCESS)
                    {
                        IPDB_PORTCTRL_INCR_ARP_DROPPED (pPortCtrlEntry);
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

                        FsUtilReleaseBitList ((UINT1 *) pPortList);
                        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC,
                                  IPDB_MODULE_NAME,
                                  "IpdbApiProcessPktWithPortList Failed.\r\n");
                        return IPDB_FAILURE;
                    }
                }

                IPDB_PORTCTRL_INCR_ARP_FWD (pPortCtrlEntry);

                FsUtilReleaseBitList ((UINT1 *) pPortList);
                IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                               "Exiting the function %s \r\n", __FUNCTION__);
                return IPDB_SUCCESS;
            }
            else if (i4RetVal == IPDB_FAILURE)
            {
                IPDB_PORTCTRL_INCR_ARP_DROPPED (pPortCtrlEntry);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                FsUtilReleaseBitList ((UINT1 *) pPortList);
                return IPDB_FAILURE;
            }
        }
    }
    else
    {
        if ((IpDbArpInfo.u2Opcode == IPDB_ARP_REQUEST) &&
            (pPortCtrlEntry->u1DownstreamArpBcastStatus == IPDB_ARP_DROP))
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "Downstream Arp Bcast status is drop.\r\n");

            IPDB_PORTCTRL_INCR_ARP_DROPPED (pPortCtrlEntry);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            FsUtilReleaseBitList ((UINT1 *) pPortList);
            return IPDB_FAILURE;
        }
        else
        {
            if ((i4RetVal == IPDB_SUCCESS) || (i4RetVal == IPDB_NO_ENTRY))
            {
                /* If Entry exists in IPDB table, Packet should be
                 * forwarded only to the port learnt in the IPDB. 
                 * else, forward the packet to all the trusted ports.
                 * In both the cases, PortList will be filled by the 
                 * IpDbArpValidateIncomingPacket function.*/
                if (IpdbApiProcessPktWithPortList (pBuf, u4ContextID,
                                                   VlanTag, u4InPort,
                                                   IpDbArpInfo.DestMac,
                                                   IpDbArpInfo.SrcMac,
                                                   *pPortList) != IPDB_SUCCESS)
                {
                    IPDB_PORTCTRL_INCR_ARP_DROPPED (pPortCtrlEntry);
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

                    FsUtilReleaseBitList ((UINT1 *) pPortList);
                    IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                              "IpdbApiProcessPktWithPortList Failed.\r\n");
                    return IPDB_FAILURE;
                }
                IPDB_PORTCTRL_INCR_ARP_FWD (pPortCtrlEntry);

                FsUtilReleaseBitList ((UINT1 *) pPortList);
                IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                               "Exiting the function %s \r\n", __FUNCTION__);
                return IPDB_SUCCESS;
            }
            else
            {
                IPDB_PORTCTRL_INCR_ARP_DROPPED (pPortCtrlEntry);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                FsUtilReleaseBitList ((UINT1 *) pPortList);
                return IPDB_FAILURE;
            }
        }
    }

    FsUtilReleaseBitList ((UINT1 *) pPortList);
    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);
    return IPDB_FAILURE;
}

/*****************************************************************************/
/* Function Name      : IpdbEnqueueArpPkt                                    */
/*                                                                           */
/* Description        : This function is to enque ARP packets for the        */
/*                      IPDB task                                            */
/*                                                                           */
/* Input(s)           : pBuf       - DHCP Packet                             */
/*                      u4InPort   - Incoming port index                     */
/*                      VlanTag    - Classified S-VLAN Tag                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IpdbEnqueueArpPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4InPort,
                   tVlanTag VlanTag)
{
    tIpDbQMsg          *pIpDbQMsg = NULL;

    IPDB_TRC_ARG3 (IPDB_TRC_FLAG, IPDB_FN_ARGS, IPDB_MODULE_NAME,
                   "Entering the function %s with arguments - VlanId %d, InPort"
                   " %d,\r\n", __FUNCTION__, VlanTag.OuterVlanTag.u2VlanId,
                   u4InPort);

    if (gu1IsIpdbInitialised != IPDB_TRUE)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "IPDB module not initialized\r\n");
        return;
    }

    if ((pIpDbQMsg = (tIpDbQMsg *)
         IPDB_ALLOC_MEM_BLOCK (IPDB_Q_POOL_ID)) == NULL)
    {
        IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                       "Allocation of memory for Q message failed - pool ID"
                       " %d \r\n", IPDB_Q_POOL_ID);
        return;
    }

    pIpDbQMsg->u4EventType = IPDB_ARP_PKT_ARRIVAL_EVENT;
    pIpDbQMsg->pInQMsg = pBuf;
    MEMCPY (&(pIpDbQMsg->VlanTag), &VlanTag, sizeof (tVlanTag));
    pIpDbQMsg->u4InPort = u4InPort;

    /* Send the Messageto queue */
    if (IPDB_SEND_TO_QUEUE (IPDB_QUEUE_ID, (UINT1 *) &pIpDbQMsg,
                            OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Failed to send the message to Q\r\n");
        IPDB_RELEASE_MEM_BLOCK (IPDB_Q_POOL_ID, pIpDbQMsg);
        return;
    }

    /* Send an event to IPDB Task, indicating VLAN deletion */
    if (IPDB_SEND_EVENT (IPDB_ZERO, IPDB_TASK_NAME, IPDB_EVENT_ARRIVED)
        != OSIX_SUCCESS)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "Sending the ARP pkt arrival event to IPDB failed\r\n");
        return;
    }
    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);
    return;

}

/*****************************************************************************/
/* Function Name      : IpDbGetPortCtrlEntry                                 */
/*                                                                           */
/* Description        : This function is used to get a Port Ctrl Entry       */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Physical Interface Index                */
/*                                                                           */
/* Output(s)          : pPortCtrlEntry  - Pointer to the retreived           */
/*                                        Port ctrl entry                    */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
IpDbGetPortCtrlEntry (UINT4 u4IfIndex, tPortCtrlEntry ** pPortCtrlEntry)
{
    tPortCtrlEntry      PortCtrlEntry;

    IPDB_TRC_ARG2 (IPDB_TRC_FLAG, IPDB_FN_ARGS, IPDB_MODULE_NAME,
                   "Entering the function %s with arguments - InPort %d,\r\n",
                   __FUNCTION__, u4IfIndex);

    MEMSET (&PortCtrlEntry, IPDB_ZERO, sizeof (tPortCtrlEntry));

    PortCtrlEntry.u4IfIndex = u4IfIndex;

    *pPortCtrlEntry =
        (tPortCtrlEntry *) RBTreeGet (IPDB_PORTCTRL_RBTREE,
                                      (tPortCtrlEntry *) & PortCtrlEntry);

    if (*pPortCtrlEntry == NULL)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "No entry in the list.\r\n");
        *pPortCtrlEntry = NULL;
        return IPDB_FAILURE;
    }

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);
    return IPDB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IpdbPortGetEtherHdrLen                               */
/*                                                                           */
/* Description        : This routine calculates the Ethernet header length   */
/*                                                                           */
/* Input(s)           : pBuf - Pointer to the packet                         */
/*                      u4InPort - Incoming port                             */
/*                                                                           */
/* Output(s)          : pu2EthetOffset - Length of ethernet header           */
/*                                                                           */
/* Return Value(s)    : IPDB_SUCCESS/IPDB_FAILURE                            */
/*****************************************************************************/
INT4
IpdbPortGetEtherHdrLen (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4InPort,
                        UINT2 *pu2EthetOffset)
{
    UINT4               u4Offset = IPDB_ZERO;

    IPDB_TRC_ARG2 (IPDB_TRC_FLAG, IPDB_FN_ARGS, IPDB_MODULE_NAME,
                   "Entering the function %s with arguments - InPort"
                   " %d,\r\n", __FUNCTION__, u4InPort);

    if (VlanGetTagLenInFrame (pBuf, (UINT2) u4InPort, &u4Offset) !=
        VLAN_SUCCESS)
    {
        IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                  "VlanGetTagLenInFrame function Failed.\r\n");
        return IPDB_FAILURE;
    }

    *pu2EthetOffset = (UINT2) (u4Offset + VLAN_TYPE_OR_LEN_SIZE);

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);
    return IPDB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IpdbExtractArpHeader                                 */
/*                                                                           */
/* Description        : This routine extracts the ARP header                 */
/*                                                                           */
/* Input(s)           : pu1Buf - Pointer to the packet                       */
/*                      u2EtherOffset - Offset of the Ethernet header        */
/*                                                                           */
/* Output(s)          : pIpDbArpInfo - Pointer to the ArpInfo structure      */
/*                                                                           */
/* Return Value(s)    : IPDB_SUCCESS/IPDB_FAILURE                            */
/*****************************************************************************/
INT4
IpdbExtractArpHeader (UINT1 *pu1RecvBuf, tIpDbArpInfo * pIpDbArpInfo)
{
    UINT4               u4SrcIp = IPDB_ZERO;
    UINT4               u4DestIp = IPDB_ZERO;
    UINT2               u2Opcode = IPDB_ZERO;
    UINT1               u1HwaLen = IPDB_ZERO;
    UINT1               u1PaLen = IPDB_ZERO;
    UINT1              *pBufOffset = NULL;

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_ARGS, IPDB_MODULE_NAME,
                   "Entering the function %s " " \r\n", __FUNCTION__);

    /* To get the offset of HWA LEN in the ARP packet */
    pBufOffset = pu1RecvBuf + IPDB_ARP_HWALEN_OFFSET;
    MEMCPY (&u1HwaLen, pBufOffset, IPDB_ARP_HWALEN);

    /* To get the offset of protocol address LEN in the ARP packet */
    pBufOffset = pBufOffset + IPDB_ARP_HWALEN;
    MEMCPY (&u1PaLen, pBufOffset, IPDB_ARP_PALEN);

    /* To get the offset of Opcode in the ARP packet */
    pBufOffset = pBufOffset + IPDB_ARP_PALEN;
    MEMCPY (&u2Opcode, pBufOffset, IPDB_ARP_OPCODE_LEN);

    pIpDbArpInfo->u2Opcode = OSIX_NTOHS (u2Opcode);

    /* To get the offset of SRC MAC in the ARP packet */
    pBufOffset = pBufOffset + IPDB_ARP_OPCODE_LEN;
    MEMCPY (pIpDbArpInfo->SrcMac, pBufOffset, u1HwaLen);

    /* To get the offset of Src IP in the ARP packet */
    pBufOffset = pBufOffset + u1HwaLen;
    MEMCPY (&u4SrcIp, pBufOffset, u1PaLen);

    /* To get the offset of Dest MAC in the ARP packet */
    pBufOffset = pBufOffset + u1PaLen;
    MEMCPY (pIpDbArpInfo->DestMac, pBufOffset, u1HwaLen);

    /* To get the offset of Dest IP in the ARP packet */
    pBufOffset = pBufOffset + u1HwaLen;
    MEMCPY (&u4DestIp, pBufOffset, u1PaLen);

    pIpDbArpInfo->u4SrcIp = OSIX_NTOHL (u4SrcIp);
    pIpDbArpInfo->u4DestIp = OSIX_NTOHL (u4DestIp);

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);
    return IPDB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IpDbArpValidateIncomingPacket                        */
/*                                                                           */
/* Description        : This routine validates the incoming ARP header       */
/*                                                                           */
/* Input(s)           : pIpDbArpInfo - Pointer to the ArpInfo structure      */
/*                      pPortCtrlEntry - Pointer to the PortCtrl structure   */
/*                      u4InPort - Port in which ARP packet has been received*/
/*                      u1PortType - Port state of the incoming port        */
/*                      VlanId    - Classified S-VLAN Id                     */
/*                      PortList - Port list in which pkt should be sent out */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IPDB_SUCCESS/IPDB_FAILURE                            */
/*****************************************************************************/
INT4
IpDbArpValidateIncomingPacket (tIpDbArpInfo * pIpDbArpInfo, UINT4 u4ContextId,
                               UINT1 u1MacForceFwdStatus, UINT4 u4InPort,
                               UINT1 u1PortType, tVlanId VlanId,
                               tPortList PortList)
{
    tIpDbBindingEntry  *pIpDbBindingEntry = NULL;
    INT4                i4RetVal = IPDB_ZERO;

    IPDB_TRC_ARG4 (IPDB_TRC_FLAG, IPDB_FN_ARGS, IPDB_MODULE_NAME,
                   "Entering the function %s with arguments - VlanId %d"
                   "InPort %d & PortState %d,\r\n", __FUNCTION__, VlanId,
                   u4InPort, u1PortType);

    switch (u1PortType)
    {
        case IPDB_PORT_TYPE_DOWNLINK:

            /* If the MAC force forwarding is enabled, ARP packets should
             * should be forwarded irrespective of the IPDB entry.*/
            if (u1MacForceFwdStatus == IPDB_ENABLED)
            {
                IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                               "Exiting the function %s \r\n", __FUNCTION__);
                return IPDB_SUCCESS;
            }
            /* If MAC force forwarding is disabled, ARP packets should be
             * forwarded only if the Dest Ip in the packet is Gateway IP */
            else
            {
                pIpDbBindingEntry =
                    IpDbUtilGetBindEntryByIp (pIpDbArpInfo->u4SrcIp);

                /* Incoming ARP packets should be dropped if an Entry for 
                 * the SRC IP and MAC doesn't exist in the IPDB */
                if (pIpDbBindingEntry == NULL)
                {
                    IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                              "No Entry for this Src IP exists.\r\n");

                    return IPDB_FAILURE;
                }

                i4RetVal = MEMCMP (pIpDbArpInfo->SrcMac,
                                   pIpDbBindingEntry->HostMac,
                                   sizeof (tMacAddr));

                /* If the Inport is different, drop the packet */
                if ((i4RetVal != IPDB_ZERO)
                    || (u4InPort != pIpDbBindingEntry->u4InIfIndex))
                {
                    IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                              "Entry for this Src IP doesn't match the Src MAC"
                              "of pkt and the InPort.\r\n");

                    return IPDB_FAILURE;
                }

                if (IpDbIsGatewayIp (pIpDbBindingEntry,
                                     pIpDbArpInfo->u4DestIp) == IPDB_SUCCESS)
                {
                    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT,
                                   IPDB_MODULE_NAME,
                                   "Exiting the function %s \r\n",
                                   __FUNCTION__);
                    return IPDB_SUCCESS;
                }
                else
                {
                    IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC,
                              IPDB_MODULE_NAME,
                              "Mac force forwarding disabled and ARP packet"
                              "not destined to Gateway.\r\n");

                    return IPDB_FAILURE;
                }
            }
            break;

        case IPDB_PORT_TYPE_UPLINK:

            pIpDbBindingEntry =
                IpDbUtilGetBindEntryByIp (pIpDbArpInfo->u4DestIp);

            if (pIpDbBindingEntry == NULL)
            {
                /* Get the upstream PortList of the Vlan */
                if (CfaGetTrustedPortList (u4ContextId, VlanId, PortList)
                    != CFA_SUCCESS)
                {
                    IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                              "CfaGetTrustedPortList Failed.\r\n");
                    return IPDB_FAILURE;
                }

                IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                          "No Entry for this Src IP exists.\r\n");
                return IPDB_NO_ENTRY;
            }

            /* If Entry exists Send the packet to that specific ports. */
            OSIX_BITLIST_SET_BIT (PortList, pIpDbBindingEntry->u4InIfIndex,
                                  BRG_PORT_LIST_SIZE);
            break;

        default:

            return IPDB_NO_ENTRY;

    }

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);
    return IPDB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IpDbGetVlanCtrlEntry                                 */
/*                                                                           */
/* Description        : This function is used to get a Vlan Ctrl Entry       */
/*                                                                           */
/* Input(s)           : VlanId  - Vlan Index                                 */
/*                                                                           */
/* Output(s)          : pVlanCtrlEntry  - Pointer to the retreived           */
/*                                        Vlan ctrl entry                    */
/*                                                                           */
/* Return Value(s)    : IPDB_SUCCESS/IPDB_FAILURE                            */
/*****************************************************************************/
INT4
IpDbGetVlanCtrlEntry (tVlanId VlanId, tVlanCtrlEntry ** pVlanCtrlEntry)
{

    IPDB_TRC_ARG2 (IPDB_TRC_FLAG, IPDB_FN_ARGS, IPDB_MODULE_NAME,
                   "Entering the function %s with arguments - VlanId %d,\r\n",
                   __FUNCTION__, VlanId);

    TMO_DLL_Scan (&IPDB_VLANCTRL_DLL_LIST, *pVlanCtrlEntry, tVlanCtrlEntry *)
    {
        if ((*pVlanCtrlEntry)->VlanId == VlanId)
        {
            IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                           "Exiting the function %s \r\n", __FUNCTION__);
            return IPDB_SUCCESS;
        }
    }

    IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
              "No entry in the list.\r\n");
    return IPDB_FAILURE;
}

/*                                                                           */
/***************************** END OF FILE ***********************************/
/*                                                                           */
