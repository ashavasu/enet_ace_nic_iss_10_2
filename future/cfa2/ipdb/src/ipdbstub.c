
/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: ipdbstub.c, */
/*****************************************************************************/
/*    FILE  NAME            : ipdbstub.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : IP Binding Database management                 */
/*    MODULE NAME           : Ip Binding Database management Stub Module     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains stub funtions               */
/*                            for IP Binding Database management             */
/*---------------------------------------------------------------------------*/

#include "ipdbinc.h"
/****************************************************************************
 *    Function Name        : DcsApiAddPortEntry
 *
 *    Description          : This function is used to add entry in port control
 *                           table.
 *
 *    Input(s)             : u4IfIndex - Interface Index
 *
 *    Output(s)            : NONE
 *
 *    Returns              : IPDB_SUCCESS/IPDB_FAILURE
 *
 *****************************************************************************/
INT4
DcsApiAddPortEntry (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return IPDB_SUCCESS;
}

/*****************************************************************************
 *    Function Name        : DcsApiDeletePortEntry
 *
 *    Description          : This function is used to delete entry from port control
 *                           table.
 *
 *    Input(s)             : u4IfIndex - Interface Index
 *
 *    Output(s)            : NONE
 *
 *    Returns              : IPDB_SUCCESS/IPDB_FAILURE
 *
 *****************************************************************************/
INT4
DcsApiDeletePortEntry (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return IPDB_SUCCESS;
}
