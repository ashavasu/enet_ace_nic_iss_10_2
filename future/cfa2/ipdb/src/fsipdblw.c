/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsipdblw.c,v 1.14 2014/02/24 11:40:26 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "ipdbinc.h"
# include  "fssnmp.h"
# include  "ipdbextn.h"

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsIpDbNoOfBindings
 Input       :  The Indices

                The Object 
                retValFsIpDbNoOfBindings
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbNoOfBindings (UINT4 *pu4RetValFsIpDbNoOfBindings)
{
    *pu4RetValFsIpDbNoOfBindings = IPDB_GLOB_NOOF_BINDINGS;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpDbNoOfStaticBindings
 Input       :  The Indices

                The Object 
                retValFsIpDbNoOfStaticBindings
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbNoOfStaticBindings (UINT4 *pu4RetValFsIpDbNoOfStaticBindings)
{
    *pu4RetValFsIpDbNoOfStaticBindings = IPDB_GLOB_NOOF_STAT_BINDINGS;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsIpDbNoOfDHCPBindings  
Input       :  The Indices

The Object 
retValFsIpDbNoOfDHCPBindings
Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbNoOfDHCPBindings (UINT4 *pu4RetValFsIpDbNoOfDHCPBindings)
{
    *pu4RetValFsIpDbNoOfDHCPBindings = IPDB_GLOB_NOOF_DHCP_BINDINGS;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpDbNoOfPPPBindings
 Input       :  The Indices

                The Object 
                retValFsIpDbNoOfPPPBindings
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbNoOfPPPBindings (UINT4 *pu4RetValFsIpDbNoOfPPPBindings)
{
    *pu4RetValFsIpDbNoOfPPPBindings = IPDB_GLOB_NOOF_PPP_BINDINGS;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsIpDbStaticBindingTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIpDbStaticBindingTable
 Input       :  The Indices
                FsIpDbStaticHostVlanId
                FsIpDbStaticHostMac
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsIpDbStaticBindingTable (INT4
                                                  i4FsIpDbStaticHostVlanId,
                                                  tMacAddr FsIpDbStaticHostMac)
{
    /* Checking the Vlan Id with Default Values */
    if ((i4FsIpDbStaticHostVlanId > IPDB_MAX_VLAN_ID) ||
        (i4FsIpDbStaticHostVlanId <= IPDB_MIN_VLAN_ID))
    {
        return SNMP_FAILURE;
    }

    /* Checking whether the address is Unicast or Multicast */
    if ((IPDB_IS_MAC_MULTICAST (FsIpDbStaticHostMac)) == IPDB_ONE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIpDbStaticBindingTable
 Input       :  The Indices
                FsIpDbStaticHostVlanId
                FsIpDbStaticHostMac
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsIpDbStaticBindingTable (INT4 *pi4FsIpDbStaticHostVlanId,
                                          tMacAddr * pFsIpDbStaticHostMac)
{
    tRBElem            *pRBElem = NULL;
    tIpDbStaticBindingEntry *pIpDbStaticBindingEntry = NULL;
    tIpDbStaticBindingEntry IpDbStaticBindingEntry;

    /* Getting the First Element from the Tree */
    if ((pRBElem = RBTreeGetFirst (IPDB_STATIC_RBTREE)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbStaticBindingEntry = (tIpDbStaticBindingEntry *) pRBElem;

    while (pIpDbStaticBindingEntry->u4ContextId != IPDB_CURR_CONTEXT_ID)
    {
        if (pIpDbStaticBindingEntry->u4ContextId > IPDB_CURR_CONTEXT_ID)
        {
            return SNMP_FAILURE;
        }

        MEMSET (&IpDbStaticBindingEntry, IPDB_ZERO,
                sizeof (tIpDbStaticBindingEntry));

        IpDbStaticBindingEntry.u4ContextId =
            pIpDbStaticBindingEntry->u4ContextId;
        IpDbStaticBindingEntry.VlanId = pIpDbStaticBindingEntry->VlanId;
        MEMCPY (IpDbStaticBindingEntry.HostMac,
                pIpDbStaticBindingEntry->HostMac, sizeof (tMacAddr));

        if ((pIpDbStaticBindingEntry = (tIpDbStaticBindingEntry *)
             RBTreeGetNext (IPDB_STATIC_RBTREE, (tRBElem *)
                            & IpDbStaticBindingEntry, NULL)) == NULL)
        {
            return SNMP_FAILURE;
        }

    }

    *pi4FsIpDbStaticHostVlanId = (INT4) pIpDbStaticBindingEntry->VlanId;
    MEMCPY (pFsIpDbStaticHostMac, pIpDbStaticBindingEntry->HostMac,
            sizeof (tMacAddr));

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsIpDbStaticBindingTable                    
Input       :  The Indices
               FsIpDbStaticHostVlanId
               nextFsIpDbStaticHostVlanId
               FsIpDbStaticHostMac
               nextFsIpDbStaticHostMac
Output      :  The Get Next function gets the Next Index for
               the Index Value given in the Index Values. The
               Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIpDbStaticBindingTable (INT4 i4FsIpDbStaticHostVlanId,
                                         INT4
                                         *pi4NextFsIpDbStaticHostVlanId,
                                         tMacAddr FsIpDbStaticHostMac,
                                         tMacAddr * pNextFsIpDbStaticHostMac)
{
    tRBElem            *pRBElem = NULL;
    tIpDbStaticBindingEntry *pIpDbStaticBindingEntry = NULL;
    tIpDbStaticBindingEntry IpDbStaticBindingEntry;

    MEMSET (&IpDbStaticBindingEntry, IPDB_ZERO,
            sizeof (tIpDbStaticBindingEntry));

    IpDbStaticBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbStaticBindingEntry.VlanId = (tVlanId) i4FsIpDbStaticHostVlanId;
    MEMCPY (IpDbStaticBindingEntry.HostMac, FsIpDbStaticHostMac,
            sizeof (tMacAddr));

    /* Calling the Function to get Structure Pointer and   *
     * Checking whether that entry is Null or not          */
    if ((pRBElem =
         RBTreeGetNext (IPDB_STATIC_RBTREE,
                        (tRBElem *) & IpDbStaticBindingEntry, NULL)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbStaticBindingEntry = (tIpDbStaticBindingEntry *) pRBElem;

    if (pIpDbStaticBindingEntry->u4ContextId != IPDB_CURR_CONTEXT_ID)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsIpDbStaticHostVlanId = (INT4) pIpDbStaticBindingEntry->VlanId;
    MEMCPY (pNextFsIpDbStaticHostMac, pIpDbStaticBindingEntry->HostMac,
            sizeof (tMacAddr));
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsIpDbStaticHostIp
 Input       :  The Indices
                FsIpDbStaticHostVlanId
                FsIpDbStaticHostMac

                The Object 
                retValFsIpDbStaticHostIp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbStaticHostIp (INT4 i4FsIpDbStaticHostVlanId,
                          tMacAddr FsIpDbStaticHostMac,
                          UINT4 *pu4RetValFsIpDbStaticHostIp)
{
    tRBElem            *pRBElem = NULL;
    tIpDbStaticBindingEntry *pIpDbStaticBindingEntry = NULL;
    tIpDbStaticBindingEntry IpDbStaticBindingEntry;

    MEMSET (&IpDbStaticBindingEntry, IPDB_ZERO,
            sizeof (tIpDbStaticBindingEntry));

    IpDbStaticBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbStaticBindingEntry.VlanId = (tVlanId) i4FsIpDbStaticHostVlanId;
    MEMCPY (IpDbStaticBindingEntry.HostMac, FsIpDbStaticHostMac,
            sizeof (tMacAddr));

    /* Calling the Function to get Structure Pointer and   *
     * Checking whether that entry is Null or not          */
    if ((pRBElem = RBTreeGet (IPDB_STATIC_RBTREE,
                              (tRBElem *) & IpDbStaticBindingEntry)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbStaticBindingEntry = (tIpDbStaticBindingEntry *) pRBElem;
    *pu4RetValFsIpDbStaticHostIp = IPDB_STAT_HOST_IP (pIpDbStaticBindingEntry);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpDbStaticInIfIndex
 Input       :  The Indices
                FsIpDbStaticHostVlanId
                FsIpDbStaticHostMac

                The Object 
                retValFsIpDbStaticInIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbStaticInIfIndex (INT4 i4FsIpDbStaticHostVlanId,
                             tMacAddr FsIpDbStaticHostMac,
                             INT4 *pi4RetValFsIpDbStaticInIfIndex)
{
    tRBElem            *pRBElem = NULL;
    tIpDbStaticBindingEntry *pIpDbStaticBindingEntry = NULL;
    tIpDbStaticBindingEntry IpDbStaticBindingEntry;

    MEMSET (&IpDbStaticBindingEntry, IPDB_ZERO,
            sizeof (tIpDbStaticBindingEntry));

    IpDbStaticBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbStaticBindingEntry.VlanId = (tVlanId) i4FsIpDbStaticHostVlanId;
    MEMCPY (IpDbStaticBindingEntry.HostMac, FsIpDbStaticHostMac,
            sizeof (tMacAddr));

    /* Calling the Function to get Structure Pointer and   *
     * Checking whether that entry is Null or not          */
    if ((pRBElem = RBTreeGet (IPDB_STATIC_RBTREE,
                              (tRBElem *) & IpDbStaticBindingEntry)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbStaticBindingEntry = (tIpDbStaticBindingEntry *) pRBElem;
    *pi4RetValFsIpDbStaticInIfIndex = (INT4)
        IPDB_STAT_INTF_INDEX (pIpDbStaticBindingEntry);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpDbStaticGateway
 Input       :  The Indices
                FsIpDbStaticHostVlanId
                FsIpDbStaticHostMac

                The Object 
                retValFsIpDbStaticGateway
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbStaticGateway (INT4 i4FsIpDbStaticHostVlanId,
                           tMacAddr FsIpDbStaticHostMac,
                           UINT4 *pu4RetValFsIpDbStaticGateway)
{
    tRBElem            *pRBElem = NULL;
    tIpDbStaticBindingEntry *pIpDbStaticBindingEntry = NULL;
    tIpDbStaticBindingEntry IpDbStaticBindingEntry;

    MEMSET (&IpDbStaticBindingEntry, IPDB_ZERO,
            sizeof (tIpDbStaticBindingEntry));

    IpDbStaticBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbStaticBindingEntry.VlanId = (tVlanId) i4FsIpDbStaticHostVlanId;
    MEMCPY (IpDbStaticBindingEntry.HostMac, FsIpDbStaticHostMac,
            sizeof (tMacAddr));

    /* Calling the Function to get Structure Pointer and   *
     * Checking whether that entry is Null or not          */
    if ((pRBElem = RBTreeGet (IPDB_STATIC_RBTREE,
                              (tRBElem *) & IpDbStaticBindingEntry)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbStaticBindingEntry = (tIpDbStaticBindingEntry *) pRBElem;
    *pu4RetValFsIpDbStaticGateway =
        IPDB_STAT_GATEWAY_IP (pIpDbStaticBindingEntry);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpDbStaticBindingStatus
 Input       :  The Indices
                FsIpDbStaticHostVlanId
                FsIpDbStaticHostMac

                The Object 
                retValFsIpDbStaticBindingStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbStaticBindingStatus (INT4 i4FsIpDbStaticHostVlanId,
                                 tMacAddr FsIpDbStaticHostMac,
                                 INT4 *pi4RetValFsIpDbStaticBindingStatus)
{
    tRBElem            *pRBElem = NULL;
    tIpDbStaticBindingEntry *pIpDbStaticBindingEntry = NULL;
    tIpDbStaticBindingEntry IpDbStaticBindingEntry;

    MEMSET (&IpDbStaticBindingEntry, IPDB_ZERO,
            sizeof (tIpDbStaticBindingEntry));

    IpDbStaticBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbStaticBindingEntry.VlanId = (tVlanId) i4FsIpDbStaticHostVlanId;
    MEMCPY (IpDbStaticBindingEntry.HostMac, FsIpDbStaticHostMac,
            sizeof (tMacAddr));

    /* Calling the Function to get Structure Pointer and   *
     * Checking whether that entry is Null or not          */
    if ((pRBElem = RBTreeGet (IPDB_STATIC_RBTREE,
                              (tRBElem *) & IpDbStaticBindingEntry)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbStaticBindingEntry = (tIpDbStaticBindingEntry *) pRBElem;
    *pi4RetValFsIpDbStaticBindingStatus = (INT4)
        IPDB_STAT_BINDING_STAS (pIpDbStaticBindingEntry);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsIpDbStaticHostIp
 Input       :  The Indices
                FsIpDbStaticHostVlanId
                FsIpDbStaticHostMac

                The Object 
                setValFsIpDbStaticHostIp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIpDbStaticHostIp (INT4 i4FsIpDbStaticHostVlanId,
                          tMacAddr FsIpDbStaticHostMac,
                          UINT4 u4SetValFsIpDbStaticHostIp)
{
    tRBElem            *pRBElem = NULL;
    tIpDbStaticBindingEntry *pIpDbStaticBindingEntry = NULL;
    tIpDbStaticBindingEntry IpDbStaticBindingEntry;

    MEMSET (&IpDbStaticBindingEntry, IPDB_ZERO,
            sizeof (tIpDbStaticBindingEntry));

    IpDbStaticBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbStaticBindingEntry.VlanId = (tVlanId) i4FsIpDbStaticHostVlanId;
    MEMCPY (IpDbStaticBindingEntry.HostMac, FsIpDbStaticHostMac,
            sizeof (tMacAddr));

    /* Calling the Function to get Structure Pointer and   *
     * Checking whether that entry is Null or not          */
    if ((pRBElem = RBTreeGet (IPDB_STATIC_RBTREE,
                              (tRBElem *) & IpDbStaticBindingEntry)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbStaticBindingEntry = (tIpDbStaticBindingEntry *) pRBElem;
    IPDB_STAT_HOST_IP (pIpDbStaticBindingEntry) = u4SetValFsIpDbStaticHostIp;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIpDbStaticInIfIndex
 Input       :  The Indices
                FsIpDbStaticHostVlanId
                FsIpDbStaticHostMac

                The Object 
                setValFsIpDbStaticInIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIpDbStaticInIfIndex (INT4 i4FsIpDbStaticHostVlanId,
                             tMacAddr FsIpDbStaticHostMac,
                             INT4 i4SetValFsIpDbStaticInIfIndex)
{
    tRBElem            *pRBElem = NULL;
    tIpDbStaticBindingEntry *pIpDbStaticBindingEntry = NULL;
    tIpDbStaticBindingEntry IpDbStaticBindingEntry;

    MEMSET (&IpDbStaticBindingEntry, IPDB_ZERO,
            sizeof (tIpDbStaticBindingEntry));

    IpDbStaticBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbStaticBindingEntry.VlanId = (tVlanId) i4FsIpDbStaticHostVlanId;
    MEMCPY (IpDbStaticBindingEntry.HostMac, FsIpDbStaticHostMac,
            sizeof (tMacAddr));

    /* Calling the Function to get Structure Pointer and   *
     * Checking whether that entry is Null or not          */
    if ((pRBElem = RBTreeGet (IPDB_STATIC_RBTREE,
                              (tRBElem *) & IpDbStaticBindingEntry)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbStaticBindingEntry = (tIpDbStaticBindingEntry *) pRBElem;
    IPDB_STAT_INTF_INDEX (pIpDbStaticBindingEntry) = (UINT4)
        i4SetValFsIpDbStaticInIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIpDbStaticGateway
 Input       :  The Indices
                FsIpDbStaticHostVlanId
                FsIpDbStaticHostMac

                The Object 
                setValFsIpDbStaticGateway
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIpDbStaticGateway (INT4 i4FsIpDbStaticHostVlanId,
                           tMacAddr FsIpDbStaticHostMac,
                           UINT4 u4SetValFsIpDbStaticGateway)
{
    tRBElem            *pRBElem = NULL;
    tIpDbStaticBindingEntry *pIpDbStaticBindingEntry = NULL;
    tIpDbStaticBindingEntry IpDbStaticBindingEntry;

    MEMSET (&IpDbStaticBindingEntry, IPDB_ZERO,
            sizeof (tIpDbStaticBindingEntry));

    IpDbStaticBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbStaticBindingEntry.VlanId = (tVlanId) i4FsIpDbStaticHostVlanId;
    MEMCPY (IpDbStaticBindingEntry.HostMac, FsIpDbStaticHostMac,
            sizeof (tMacAddr));

    /* Calling the Function to get Structure Pointer and   *
     * Checking whether that entry is Null or not          */
    if ((pRBElem = RBTreeGet (IPDB_STATIC_RBTREE,
                              (tRBElem *) & IpDbStaticBindingEntry)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbStaticBindingEntry = (tIpDbStaticBindingEntry *) pRBElem;
    IPDB_STAT_GATEWAY_IP (pIpDbStaticBindingEntry) =
        u4SetValFsIpDbStaticGateway;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIpDbStaticBindingStatus
 Input       :  The Indices
                FsIpDbStaticHostVlanId
                FsIpDbStaticHostMac

                The Object 
                setValFsIpDbStaticBindingStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIpDbStaticBindingStatus (INT4 i4FsIpDbStaticHostVlanId,
                                 tMacAddr FsIpDbStaticHostMac,
                                 INT4 i4SetValFsIpDbStaticBindingStatus)
{
    tVlanId             VlanId = IPDB_ZERO;
    UINT1               u1IpDbStaticBindingStatus = IPDB_ZERO;

    VlanId = (tVlanId) i4FsIpDbStaticHostVlanId;
    u1IpDbStaticBindingStatus = (UINT1) i4SetValFsIpDbStaticBindingStatus;

    if ((IpdbProcChangeStaticEntry (IPDB_CURR_CONTEXT_ID, FsIpDbStaticHostMac,
                                    VlanId, u1IpDbStaticBindingStatus))
        != IPDB_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsIpDbStaticHostIp
 Input       :  The Indices
                FsIpDbStaticHostVlanId
                FsIpDbStaticHostMac

                The Object 
                testValFsIpDbStaticHostIp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIpDbStaticHostIp (UINT4 *pu4ErrorCode,
                             INT4 i4FsIpDbStaticHostVlanId,
                             tMacAddr FsIpDbStaticHostMac,
                             UINT4 u4TestValFsIpDbStaticHostIp)
{
    tRBElem            *pRBElem = NULL;
    tIpDbStaticBindingEntry *pIpDbStaticBindingEntry = NULL;
    tIpDbStaticBindingEntry IpDbStaticBindingEntry;

    MEMSET (&IpDbStaticBindingEntry, IPDB_ZERO,
            sizeof (tIpDbStaticBindingEntry));

    IpDbStaticBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbStaticBindingEntry.VlanId = (tVlanId) i4FsIpDbStaticHostVlanId;
    MEMCPY (IpDbStaticBindingEntry.HostMac, FsIpDbStaticHostMac,
            sizeof (tMacAddr));

    /* Calling the Function to get Structure Pointer and   *
     * Checking whether that entry is Null or not          */
    if ((pRBElem = RBTreeGet (IPDB_STATIC_RBTREE,
                              (tRBElem *) & IpDbStaticBindingEntry)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIpDbStaticBindingEntry = (tIpDbStaticBindingEntry *) pRBElem;

    if ((pIpDbStaticBindingEntry->u1RowStatus != IPDB_NOT_READY) &&
        (pIpDbStaticBindingEntry->u1RowStatus != IPDB_NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
        return SNMP_FAILURE;
    }

    if (!(IPDB_IS_VALID_IP (u4TestValFsIpDbStaticHostIp)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpDbStaticInIfIndex
 Input       :  The Indices
                FsIpDbStaticHostVlanId
                FsIpDbStaticHostMac

                The Object 
                testValFsIpDbStaticInIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIpDbStaticInIfIndex (UINT4 *pu4ErrorCode,
                                INT4 i4FsIpDbStaticHostVlanId,
                                tMacAddr FsIpDbStaticHostMac,
                                INT4 i4TestValFsIpDbStaticInIfIndex)
{
    tRBElem            *pRBElem = NULL;
    tIpDbStaticBindingEntry *pIpDbStaticBindingEntry = NULL;
    tIpDbStaticBindingEntry IpDbStaticBindingEntry;

    MEMSET (&IpDbStaticBindingEntry, IPDB_ZERO,
            sizeof (tIpDbStaticBindingEntry));

    IpDbStaticBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbStaticBindingEntry.VlanId = (tVlanId) i4FsIpDbStaticHostVlanId;
    MEMCPY (IpDbStaticBindingEntry.HostMac, FsIpDbStaticHostMac,
            sizeof (tMacAddr));

    /* Calling the Function to get Structure Pointer and   *
     * Checking whether that entry is Null or not          */
    if ((pRBElem = RBTreeGet (IPDB_STATIC_RBTREE,
                              (tRBElem *) & IpDbStaticBindingEntry)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIpDbStaticBindingEntry = (tIpDbStaticBindingEntry *) pRBElem;

    if ((pIpDbStaticBindingEntry->u1RowStatus != IPDB_NOT_READY) &&
        (pIpDbStaticBindingEntry->u1RowStatus != IPDB_NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIpDbStaticInIfIndex < IPDB_MIN_PORTS) ||
        (i4TestValFsIpDbStaticInIfIndex > IPDB_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpDbStaticGateway
 Input       :  The Indices
                FsIpDbStaticHostVlanId
                FsIpDbStaticHostMac

                The Object 
                testValFsIpDbStaticGateway
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIpDbStaticGateway (UINT4 *pu4ErrorCode,
                              INT4 i4FsIpDbStaticHostVlanId,
                              tMacAddr FsIpDbStaticHostMac,
                              UINT4 u4TestValFsIpDbStaticGateway)
{
    tRBElem            *pRBElem = NULL;
    tIpDbStaticBindingEntry *pIpDbStaticBindingEntry = NULL;
    tIpDbStaticBindingEntry IpDbStaticBindingEntry;

    MEMSET (&IpDbStaticBindingEntry, IPDB_ZERO,
            sizeof (tIpDbStaticBindingEntry));

    IpDbStaticBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbStaticBindingEntry.VlanId = (tVlanId) i4FsIpDbStaticHostVlanId;
    MEMCPY (IpDbStaticBindingEntry.HostMac, FsIpDbStaticHostMac,
            sizeof (tMacAddr));

    /* Calling the Function to get Structure Pointer and   *
     * Checking whether that entry is Null or not          */
    if ((pRBElem = RBTreeGet (IPDB_STATIC_RBTREE,
                              (tRBElem *) & IpDbStaticBindingEntry)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIpDbStaticBindingEntry = (tIpDbStaticBindingEntry *) pRBElem;

    if ((pIpDbStaticBindingEntry->u1RowStatus != IPDB_NOT_READY) &&
        (pIpDbStaticBindingEntry->u1RowStatus != IPDB_NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u4TestValFsIpDbStaticGateway);

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpDbStaticBindingStatus
 Input       :  The Indices
                FsIpDbStaticHostVlanId
                FsIpDbStaticHostMac

                The Object 
                testValFsIpDbStaticBindingStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIpDbStaticBindingStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsIpDbStaticHostVlanId,
                                    tMacAddr FsIpDbStaticHostMac,
                                    INT4 i4TestValFsIpDbStaticBindingStatus)
{
    tRBElem            *pRBElem = NULL;
    tIpDbStaticBindingEntry IpDbStaticBindingEntry;
    tIpDbStaticBindingEntry *pIpDbStaticBindingEntry = NULL;
    UINT4               u4Count = IPDB_ZERO;

    MEMSET (&IpDbStaticBindingEntry, IPDB_ZERO,
            sizeof (tIpDbStaticBindingEntry));

    IpDbStaticBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbStaticBindingEntry.VlanId = (tVlanId) i4FsIpDbStaticHostVlanId;
    MEMCPY (IpDbStaticBindingEntry.HostMac, FsIpDbStaticHostMac,
            sizeof (tMacAddr));

    /* Checking the Vlan Id with Default Values */
    if ((i4FsIpDbStaticHostVlanId > IPDB_MAX_VLAN_ID) ||
        (i4FsIpDbStaticHostVlanId <= IPDB_MIN_VLAN_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IPDB_VLAN_ERR);
        return SNMP_FAILURE;
    }
    /* Checking whether the address is Unicast or Multicast */
    if ((IPDB_IS_MAC_MULTICAST (FsIpDbStaticHostMac)) == IPDB_ONE)
    {
        return SNMP_FAILURE;
    }

    pRBElem = RBTreeGet (IPDB_STATIC_RBTREE,
                         (tRBElem *) & IpDbStaticBindingEntry);

    if ((i4TestValFsIpDbStaticBindingStatus == IPDB_CREATE_AND_WAIT) ||
        (i4TestValFsIpDbStaticBindingStatus == IPDB_CREATE_AND_GO))
    {
        RBTreeCount (IPDB_STATIC_RBTREE, &u4Count);

        if (u4Count >= IPDB_MAX_STATIC_ENTRIES)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_IPDB_COUNT_ERR);
            return SNMP_FAILURE;
        }

        if (pRBElem != NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    if (pRBElem == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValFsIpDbStaticBindingStatus == IPDB_ACTIVE)
    {
        pIpDbStaticBindingEntry = (tIpDbStaticBindingEntry *) pRBElem;

        /* If the new rowstatus is ACTIVE, and if IP-address, GatewayIP, and
         * incoming port fields are not set, return failure */
        if ((pIpDbStaticBindingEntry->u4HostIp == IPDB_ZERO) ||
            (pIpDbStaticBindingEntry->u4InIfIndex == IPDB_ZERO))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValFsIpDbStaticBindingStatus == IPDB_NOT_IN_SERVICE) ||
        (i4TestValFsIpDbStaticBindingStatus == IPDB_ACTIVE) ||
        (i4TestValFsIpDbStaticBindingStatus == IPDB_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhDepv2FsIpDbStaticBindingTable
 Input       :  The Indices
                FsIpDbStaticHostVlanId
                FsIpDbStaticHostMac
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIpDbStaticBindingTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsIpDbBindingTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIpDbBindingTable
 Input       :  The Indices
                FsIpDbHostVlanId
                FsIpDbHostMac
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsIpDbBindingTable (INT4 i4FsIpDbHostVlanId,
                                            tMacAddr FsIpDbHostMac)
{
    /* Checking the Vlan Id with Default Values */
    if ((i4FsIpDbHostVlanId > IPDB_MAX_VLAN_ID) ||
        (i4FsIpDbHostVlanId <= IPDB_MIN_VLAN_ID))
    {
        return SNMP_FAILURE;
    }

    /* Checking whether the address is Unicast or Multicast */
    if ((IPDB_IS_MAC_MULTICAST (FsIpDbHostMac)) == IPDB_ONE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIpDbBindingTable
 Input       :  The Indices
                FsIpDbHostVlanId
                FsIpDbHostMac
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsIpDbBindingTable (INT4 *pi4FsIpDbHostVlanId,
                                    tMacAddr * pFsIpDbHostMac)
{
    tRBElem            *pRBElem = NULL;
    tIpDbBindingEntry  *pIpDbBindingEntry = NULL;
    tIpDbBindingEntry   IpDbBindingEntry;

    /* Getting the first Element from the Tree   */
    if ((pRBElem = RBTreeGetFirst (IPDB_MAC_BINDING_RBTREE)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbBindingEntry = (tIpDbBindingEntry *) pRBElem;

    while (pIpDbBindingEntry->u4ContextId != IPDB_CURR_CONTEXT_ID)
    {
        if (pIpDbBindingEntry->u4ContextId > IPDB_CURR_CONTEXT_ID)
        {
            return SNMP_FAILURE;
        }

        MEMSET (&IpDbBindingEntry, IPDB_ZERO, sizeof (tIpDbBindingEntry));

        IpDbBindingEntry.u4ContextId = pIpDbBindingEntry->u4ContextId;
        IpDbBindingEntry.VlanId = pIpDbBindingEntry->VlanId;
        MEMCPY (IpDbBindingEntry.HostMac, pIpDbBindingEntry->HostMac,
                sizeof (tMacAddr));

        if ((pIpDbBindingEntry = (tIpDbBindingEntry *)
             RBTreeGetNext (IPDB_MAC_BINDING_RBTREE,
                            (tRBElem *) & IpDbBindingEntry, NULL)) == NULL)
        {
            return SNMP_FAILURE;
        }
    }

    *pi4FsIpDbHostVlanId = (INT4) pIpDbBindingEntry->VlanId;
    MEMCPY (pFsIpDbHostMac, pIpDbBindingEntry->HostMac, sizeof (tMacAddr));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIpDbBindingTable
 Input       :  The Indices
                FsIpDbHostVlanId
                nextFsIpDbHostVlanId
                FsIpDbHostMac
                nextFsIpDbHostMac
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIpDbBindingTable (INT4 i4FsIpDbHostVlanId,
                                   INT4 *pi4NextFsIpDbHostVlanId,
                                   tMacAddr FsIpDbHostMac,
                                   tMacAddr * pNextFsIpDbHostMac)
{
    tRBElem            *pRBElem = NULL;
    tIpDbBindingEntry  *pIpDbBindingEntry = NULL;
    tIpDbBindingEntry   IpDbBindingEntry;

    MEMSET (&IpDbBindingEntry, IPDB_ZERO, sizeof (tIpDbBindingEntry));

    IpDbBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbBindingEntry.VlanId = (tVlanId) i4FsIpDbHostVlanId;
    MEMCPY (IpDbBindingEntry.HostMac, FsIpDbHostMac, sizeof (tMacAddr));

    /* Calling the Function to get Structure Pointer and   *
     * Checking whether that entry is Null or not          */
    if ((pRBElem =
         RBTreeGetNext (IPDB_MAC_BINDING_RBTREE,
                        (tRBElem *) & IpDbBindingEntry, NULL)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbBindingEntry = (tIpDbBindingEntry *) pRBElem;

    if (pIpDbBindingEntry->u4ContextId != IPDB_CURR_CONTEXT_ID)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsIpDbHostVlanId = (INT4) pIpDbBindingEntry->VlanId;
    MEMCPY (pNextFsIpDbHostMac, pIpDbBindingEntry->HostMac, sizeof (tMacAddr));

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsIpDbHostBindingType
 Input       :  The Indices
                FsIpDbHostVlanId
                FsIpDbHostMac

                The Object 
                retValFsIpDbHostBindingType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbHostBindingType (INT4 i4FsIpDbHostVlanId,
                             tMacAddr FsIpDbHostMac,
                             INT4 *pi4RetValFsIpDbHostBindingType)
{
    tRBElem            *pRBElem = NULL;
    tIpDbBindingEntry  *pIpDbBindingEntry = NULL;
    tIpDbBindingEntry   IpDbBindingEntry;

    MEMSET (&IpDbBindingEntry, IPDB_ZERO, sizeof (tIpDbBindingEntry));

    IpDbBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbBindingEntry.VlanId = (tVlanId) i4FsIpDbHostVlanId;
    MEMCPY (IpDbBindingEntry.HostMac, FsIpDbHostMac, sizeof (tMacAddr));

    /* Calling the Function to get Structure Pointer and   *
     * Checking whether that entry is Null or not          */
    if ((pRBElem = RBTreeGet (IPDB_MAC_BINDING_RBTREE,
                              (tRBElem *) & IpDbBindingEntry)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbBindingEntry = (tIpDbBindingEntry *) pRBElem;

    *pi4RetValFsIpDbHostBindingType = (UINT4)
        IPDB_DB_BINDING_TYPE (pIpDbBindingEntry);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpDbHostIp
 Input       :  The Indices
                FsIpDbHostVlanId
                FsIpDbHostMac

                The Object 
                retValFsIpDbHostIp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbHostIp (INT4 i4FsIpDbHostVlanId, tMacAddr FsIpDbHostMac,
                    UINT4 *pu4RetValFsIpDbHostIp)
{
    tRBElem            *pRBElem = NULL;
    tIpDbBindingEntry  *pIpDbBindingEntry = NULL;
    tIpDbBindingEntry   IpDbBindingEntry;

    MEMSET (&IpDbBindingEntry, IPDB_ZERO, sizeof (tIpDbBindingEntry));

    IpDbBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbBindingEntry.VlanId = (tVlanId) i4FsIpDbHostVlanId;
    MEMCPY (IpDbBindingEntry.HostMac, FsIpDbHostMac, sizeof (tMacAddr));

    /* Calling the Function to get Structure Pointer and   *
     * Checking whether that entry is Null or not          */
    if ((pRBElem = RBTreeGet (IPDB_MAC_BINDING_RBTREE,
                              (tRBElem *) & IpDbBindingEntry)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbBindingEntry = (tIpDbBindingEntry *) pRBElem;
    *pu4RetValFsIpDbHostIp = IPDB_DB_HOST_IP (pIpDbBindingEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpDbHostInIfIndex
 Input       :  The Indices
                FsIpDbHostVlanId
                FsIpDbHostMac

                The Object 
                retValFsIpDbHostInIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbHostInIfIndex (INT4 i4FsIpDbHostVlanId,
                           tMacAddr FsIpDbHostMac,
                           INT4 *pi4RetValFsIpDbHostInIfIndex)
{
    tRBElem            *pRBElem = NULL;
    tIpDbBindingEntry  *pIpDbBindingEntry = NULL;
    tIpDbBindingEntry   IpDbBindingEntry;

    MEMSET (&IpDbBindingEntry, IPDB_ZERO, sizeof (tIpDbBindingEntry));

    IpDbBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbBindingEntry.VlanId = (tVlanId) i4FsIpDbHostVlanId;
    MEMCPY (IpDbBindingEntry.HostMac, FsIpDbHostMac, sizeof (tMacAddr));

    /* Calling the Function to get Structure Pointer and   *
     * Checking whether that entry is Null or not          */
    if ((pRBElem = RBTreeGet (IPDB_MAC_BINDING_RBTREE,
                              (tRBElem *) & IpDbBindingEntry)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbBindingEntry = (tIpDbBindingEntry *) pRBElem;
    *pi4RetValFsIpDbHostInIfIndex = (INT4)
        IPDB_DB_INTF_INDEX (pIpDbBindingEntry);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpDbHostRemLeaseTime
 Input       :  The Indices
                FsIpDbHostVlanId
                FsIpDbHostMac

                The Object 
                retValFsIpDbHostRemLeaseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbHostRemLeaseTime (INT4 i4FsIpDbHostVlanId,
                              tMacAddr FsIpDbHostMac,
                              INT4 *pi4RetValFsIpDbHostRemLeaseTime)
{
    tRBElem            *pRBElem = NULL;
    tIpDbBindingEntry  *pIpDbBindingEntry = NULL;
    tIpDbBindingEntry   IpDbBindingEntry;
    UINT4               u4RemainingTime = IPDB_ZERO;

    MEMSET (&IpDbBindingEntry, IPDB_ZERO, sizeof (tIpDbBindingEntry));

    IpDbBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbBindingEntry.VlanId = (tVlanId) i4FsIpDbHostVlanId;
    MEMCPY (IpDbBindingEntry.HostMac, FsIpDbHostMac, sizeof (tMacAddr));

    /* Calling the Function to get Structure Pointer and   *
     * Checking whether that entry is Null or not          */
    if ((pRBElem = RBTreeGet (IPDB_MAC_BINDING_RBTREE,
                              (tRBElem *) & IpDbBindingEntry)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbBindingEntry = (tIpDbBindingEntry *) pRBElem;

    if (TmrGetRemainingTime (IPDB_TIMER_LIST,
                             &(pIpDbBindingEntry->LeaseTimerNode.TmrBlk.
                               TimerNode), &u4RemainingTime) == TMR_FAILURE)
    {
        *pi4RetValFsIpDbHostRemLeaseTime = IPDB_ZERO;
        return SNMP_SUCCESS;
    }

    /* convert from timer ticks to seconds */
    u4RemainingTime = u4RemainingTime / IPDB_TIME_TICKS_PER_SEC;

    *pi4RetValFsIpDbHostRemLeaseTime = u4RemainingTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpDbHostBindingID
 Input       :  The Indices
                FsIpDbHostVlanId
                FsIpDbHostMac

                The Object 
                retValFsIpDbHostBindingID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbHostBindingID (INT4 i4FsIpDbHostVlanId,
                           tMacAddr FsIpDbHostMac,
                           UINT4 *pu4RetValFsIpDbHostBindingID)
{
    tRBElem            *pRBElem = NULL;
    tIpDbBindingEntry  *pIpDbBindingEntry = NULL;
    tIpDbBindingEntry   IpDbBindingEntry;

    MEMSET (&IpDbBindingEntry, IPDB_ZERO, sizeof (tIpDbBindingEntry));

    IpDbBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbBindingEntry.VlanId = (tVlanId) i4FsIpDbHostVlanId;
    MEMCPY (IpDbBindingEntry.HostMac, FsIpDbHostMac, sizeof (tMacAddr));

    /* Calling the Function to get Structure Pointer and   *
     * Checking whether that entry is Null or not          */
    if ((pRBElem = RBTreeGet (IPDB_MAC_BINDING_RBTREE,
                              (tRBElem *) & IpDbBindingEntry)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbBindingEntry = (tIpDbBindingEntry *) pRBElem;
    *pu4RetValFsIpDbHostBindingID = IPDB_DB_BINDING_ID (pIpDbBindingEntry);

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsIpDbGatewayIpTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIpDbGatewayIpTable
 Input       :  The Indices
                FsIpDbHostMac
                FsIpDbHostVlanId
                FsIpDbGatewayNetwork
                FsIpDbGatewayNetMask
                FsIpDbGatewayIp
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIpDbGatewayIpTable (tMacAddr FsIpDbHostMac,
                                              INT4 i4FsIpDbHostVlanId,
                                              UINT4 u4FsIpDbGatewayNetwork,
                                              UINT4 u4FsIpDbGatewayNetMask,
                                              UINT4 u4FsIpDbGatewayIp)
{
    tRBElem            *pRBElem = NULL;
    tIpDbBindingEntry   IpDbBindingEntry;

    MEMSET (&IpDbBindingEntry, IPDB_ZERO, sizeof (tIpDbBindingEntry));

    IpDbBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbBindingEntry.VlanId = (tVlanId) i4FsIpDbHostVlanId;
    MEMCPY (IpDbBindingEntry.HostMac, FsIpDbHostMac, sizeof (tMacAddr));

    /* For the Gateway table entry, there should be a 
     * corresponing binding entry */

    /* Calling the Function to get Structure Pointer and   *
     * Checking whether that entry is Null or not          */
    pRBElem = RBTreeGet (IPDB_MAC_BINDING_RBTREE,
                         (tRBElem *) & IpDbBindingEntry);
    if (pRBElem == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Gateway Ip Address Validation */
    if (!IPDB_IS_VALID_IP (u4FsIpDbGatewayIp))
    {
        return SNMP_FAILURE;
    }

    /* Gateway Netmask Validation */
    if ((IPDB_IS_ADDR_CLASS_D (u4FsIpDbGatewayNetwork)) ||
        (IPDB_IS_ADDR_CLASS_E (u4FsIpDbGatewayNetwork)) ||
        ((u4FsIpDbGatewayNetwork & u4FsIpDbGatewayNetMask) !=
         u4FsIpDbGatewayNetwork))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIpDbGatewayIpTable
 Input       :  The Indices
                FsIpDbHostMac
                FsIpDbHostVlanId
                FsIpDbGatewayNetwork
                FsIpDbGatewayNetMask
                FsIpDbGatewayIp
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIpDbGatewayIpTable (tMacAddr * pFsIpDbHostMac,
                                      INT4 *pi4FsIpDbHostVlanId,
                                      UINT4 *pu4FsIpDbGatewayNetwork,
                                      UINT4 *pu4FsIpDbGatewayNetMask,
                                      UINT4 *pu4FsIpDbGatewayIp)
{
    tIpDbBindingEntry  *pIpDbBindingEntry = NULL;
    tIpDbGatewayEntry  *pIpDbGatewayEntry = NULL;

    /* Getting the first Element from the Tree   */
    pIpDbBindingEntry =
        (tIpDbBindingEntry *) RBTreeGetFirst (IPDB_MAC_BINDING_RBTREE);

    if (pIpDbBindingEntry != NULL)
    {
        if (pIpDbBindingEntry->u4ContextId != IPDB_CURR_CONTEXT_ID)
        {
            return SNMP_FAILURE;
        }

        pIpDbGatewayEntry = (tIpDbGatewayEntry *)
            TMO_SLL_First (&(pIpDbBindingEntry->NetworkList));

        if (pIpDbGatewayEntry != NULL)
        {
            *pi4FsIpDbHostVlanId = (INT4) pIpDbBindingEntry->VlanId;
            MEMCPY (pFsIpDbHostMac, pIpDbBindingEntry->HostMac,
                    sizeof (tMacAddr));
            *pu4FsIpDbGatewayNetwork = pIpDbGatewayEntry->u4Network;
            *pu4FsIpDbGatewayNetMask = pIpDbGatewayEntry->u4NetMask;
            *pu4FsIpDbGatewayIp = pIpDbGatewayEntry->u4GatewayIp;

            return SNMP_SUCCESS;
        }
        else
        {
            /* Here failure is returned on the basis of assumption that, each 
             * binding entry will have atleast one gateway associated with it */
            return SNMP_FAILURE;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIpDbGatewayIpTable
 Input       :  The Indices
                FsIpDbHostMac
                nextFsIpDbHostMac
                FsIpDbHostVlanId
                nextFsIpDbHostVlanId
                FsIpDbGatewayNetwork
                nextFsIpDbGatewayNetwork
                FsIpDbGatewayNetMask
                nextFsIpDbGatewayNetMask
                FsIpDbGatewayIp
                nextFsIpDbGatewayIp
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIpDbGatewayIpTable (tMacAddr FsIpDbHostMac,
                                     tMacAddr * pNextFsIpDbHostMac,
                                     INT4 i4FsIpDbHostVlanId,
                                     INT4 *pi4NextFsIpDbHostVlanId,
                                     UINT4 u4FsIpDbGatewayNetwork,
                                     UINT4 *pu4NextFsIpDbGatewayNetwork,
                                     UINT4 u4FsIpDbGatewayNetMask,
                                     UINT4 *pu4NextFsIpDbGatewayNetMask,
                                     UINT4 u4FsIpDbGatewayIp,
                                     UINT4 *pu4NextFsIpDbGatewayIp)
{
    tIpDbBindingEntry  *pIpDbBindingEntry = NULL;
    tIpDbGatewayEntry  *pIpDbGatewayEntry = NULL;
    tIpDbBindingEntry   IpDbBindingEntry;

    MEMSET (&IpDbBindingEntry, IPDB_ZERO, sizeof (tIpDbBindingEntry));

    IpDbBindingEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbBindingEntry.VlanId = (tVlanId) i4FsIpDbHostVlanId;
    MEMCPY (IpDbBindingEntry.HostMac, FsIpDbHostMac, sizeof (tMacAddr));

    /* Get the binding entry */
    if ((pIpDbBindingEntry = (tIpDbBindingEntry *) RBTreeGet
         (IPDB_MAC_BINDING_RBTREE, (tRBElem *) & IpDbBindingEntry)) != NULL)
    {
        if (pIpDbBindingEntry->u4ContextId != IPDB_CURR_CONTEXT_ID)
        {
            return SNMP_FAILURE;
        }

        /* Scan through the Gateway table to find the next largest entry 
         * ASSUMPTION: The entries are added in sorted order */
        TMO_SLL_Scan (&(pIpDbBindingEntry->NetworkList), pIpDbGatewayEntry,
                      tIpDbGatewayEntry *)
        {
            /* If Network is greater than that is passed, return success */
            if (u4FsIpDbGatewayNetwork < pIpDbGatewayEntry->u4Network)
            {
                break;
            }
            else if (u4FsIpDbGatewayNetwork == pIpDbGatewayEntry->u4Network)
            {
                /* If network is same, but MASK is greater, return success */
                if (u4FsIpDbGatewayNetMask < pIpDbGatewayEntry->u4NetMask)
                {
                    break;
                }
                else if (u4FsIpDbGatewayNetMask == pIpDbGatewayEntry->u4NetMask)
                {
                    /* If Mask is the same, but gateway IP is greater, return 
                     * success */
                    if (u4FsIpDbGatewayIp < pIpDbGatewayEntry->u4GatewayIp)
                    {
                        break;
                    }
                }
            }
        }
        if (pIpDbGatewayEntry != NULL)
        {
            *pi4NextFsIpDbHostVlanId = (INT4) pIpDbBindingEntry->VlanId;
            MEMCPY (pNextFsIpDbHostMac, pIpDbBindingEntry->HostMac,
                    sizeof (tMacAddr));
            *pu4NextFsIpDbGatewayNetwork = pIpDbGatewayEntry->u4Network;
            *pu4NextFsIpDbGatewayNetMask = pIpDbGatewayEntry->u4NetMask;
            *pu4NextFsIpDbGatewayIp = pIpDbGatewayEntry->u4GatewayIp;

            return SNMP_SUCCESS;
        }
    }
    /* Calling the Function to get Structure Pointer and   *
     * Checking whether that entry is Null or not          */
    pIpDbBindingEntry = (tIpDbBindingEntry *)
        RBTreeGetNext (IPDB_MAC_BINDING_RBTREE,
                       (tRBElem *) & IpDbBindingEntry, NULL);

    if (pIpDbBindingEntry != NULL)
    {
        if (pIpDbBindingEntry->u4ContextId != IPDB_CURR_CONTEXT_ID)
        {
            return SNMP_SUCCESS;
        }

        pIpDbGatewayEntry = (tIpDbGatewayEntry *)
            TMO_SLL_First (&(pIpDbBindingEntry->NetworkList));

        if (pIpDbGatewayEntry != NULL)
        {
            *pi4NextFsIpDbHostVlanId = (INT4) pIpDbBindingEntry->VlanId;
            MEMCPY (pNextFsIpDbHostMac, pIpDbBindingEntry->HostMac,
                    sizeof (tMacAddr));
            *pu4NextFsIpDbGatewayNetwork = pIpDbGatewayEntry->u4Network;
            *pu4NextFsIpDbGatewayNetMask = pIpDbGatewayEntry->u4NetMask;
            *pu4NextFsIpDbGatewayIp = pIpDbGatewayEntry->u4GatewayIp;

            return SNMP_SUCCESS;
        }
        else
        {
            /* Here failure is returned on the basis of assumption that, each
             * binding entry will have atleast one gateway associated with it */
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpDbGatewayIpMode
 Input       :  The Indices
                FsIpDbHostMac
                FsIpDbHostVlanId
                FsIpDbGatewayNetwork
                FsIpDbGatewayNetMask
                FsIpDbGatewayIp

                The Object 
                retValFsIpDbGatewayIpMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbGatewayIpMode (tMacAddr FsIpDbHostMac,
                           INT4 i4FsIpDbHostVlanId,
                           UINT4 u4FsIpDbGatewayNetwork,
                           UINT4 u4FsIpDbGatewayNetMask,
                           UINT4 u4FsIpDbGatewayIp,
                           INT4 *pi4RetValFsIpDbGatewayIpMode)
{
    UNUSED_PARAM (FsIpDbHostMac);
    UNUSED_PARAM (i4FsIpDbHostVlanId);
    UNUSED_PARAM (u4FsIpDbGatewayNetwork);
    UNUSED_PARAM (u4FsIpDbGatewayNetMask);
    UNUSED_PARAM (u4FsIpDbGatewayIp);
    UNUSED_PARAM (pi4RetValFsIpDbGatewayIpMode);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsIpDbInterfaceTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIpDbInterfaceTable
 Input       :  The Indices
                FsIpDbIntfVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsIpDbInterfaceTable (INT4 i4FsIpDbIntfVlanId)
{
    /* Checking the Vlan Id with Default Values */
    if ((i4FsIpDbIntfVlanId > IPDB_MAX_VLAN_ID) ||
        (i4FsIpDbIntfVlanId <= IPDB_MIN_VLAN_ID))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIpDbInterfaceTable
 Input       :  The Indices
                FsIpDbIntfVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsIpDbInterfaceTable (INT4 *pi4FsIpDbIntfVlanId)
{
    tRBElem            *pRBElem = NULL;
    tIpDbIfaceEntry    *pIpDbIfaceEntry = NULL;
    tIpDbIfaceEntry     IpDbIfaceEntry;

    /* Getting the First Element from the Tree */
    if ((pRBElem = RBTreeGetFirst (IPDB_INTF_RBTREE)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbIfaceEntry = (tIpDbIfaceEntry *) pRBElem;

    while (pIpDbIfaceEntry->u4ContextId != IPDB_CURR_CONTEXT_ID)
    {
        if (pIpDbIfaceEntry->u4ContextId > IPDB_CURR_CONTEXT_ID)
        {
            return SNMP_FAILURE;
        }

        MEMSET (&IpDbIfaceEntry, IPDB_ZERO, sizeof (tIpDbIfaceEntry));

        IpDbIfaceEntry.u4ContextId = pIpDbIfaceEntry->u4ContextId;
        IpDbIfaceEntry.VlanId = pIpDbIfaceEntry->VlanId;

        if ((pIpDbIfaceEntry = (tIpDbIfaceEntry *)
             RBTreeGetNext (IPDB_INTF_RBTREE,
                            (tRBElem *) & IpDbIfaceEntry, NULL)) == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    pIpDbIfaceEntry = (tIpDbIfaceEntry *) pRBElem;
    *pi4FsIpDbIntfVlanId = (INT4) pIpDbIfaceEntry->VlanId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIpDbInterfaceTable
 Input       :  The Indices
                FsIpDbIntfVlanId
                nextFsIpDbIntfVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIpDbInterfaceTable (INT4 i4FsIpDbIntfVlanId,
                                     INT4 *pi4NextFsIpDbIntfVlanId)
{
    tRBElem            *pRBElem = NULL;
    tIpDbIfaceEntry    *pIpDbIfaceEntry = NULL;
    tIpDbIfaceEntry     IpDbIfaceEntry;

    MEMSET (&IpDbIfaceEntry, IPDB_ZERO, sizeof (tIpDbIfaceEntry));

    IpDbIfaceEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbIfaceEntry.VlanId = (tVlanId) i4FsIpDbIntfVlanId;

    /* Getting the Next Element from the Tree */
    if ((pRBElem = RBTreeGetNext (IPDB_INTF_RBTREE,
                                  (tRBElem *) & IpDbIfaceEntry, NULL)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbIfaceEntry = (tIpDbIfaceEntry *) pRBElem;

    if (pIpDbIfaceEntry->u4ContextId != IPDB_CURR_CONTEXT_ID)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsIpDbIntfVlanId = (INT4) pIpDbIfaceEntry->VlanId;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsIpDbIntfNoOfVlanBindings
 Input       :  The Indices
                FsIpDbIntfVlanId

                The Object 
                retValFsIpDbIntfNoOfVlanBindings
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbIntfNoOfVlanBindings (INT4 i4FsIpDbIntfVlanId,
                                  UINT4 *pu4RetValFsIpDbIntfNoOfVlanBindings)
{
    tRBElem            *pRBElem = NULL;
    tIpDbIfaceEntry    *pIpDbIfaceEntry = NULL;
    tIpDbIfaceEntry     IpDbIfaceEntry;

    MEMSET (&IpDbIfaceEntry, IPDB_ZERO, sizeof (tIpDbIfaceEntry));

    IpDbIfaceEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbIfaceEntry.VlanId = (tVlanId) i4FsIpDbIntfVlanId;

    /* Calling the Function to get the Structure Pointer and   *
     * Checking whether that entry is Null or not              */
    if ((pRBElem = RBTreeGet (IPDB_INTF_RBTREE,
                              (tRBElem *) & IpDbIfaceEntry)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbIfaceEntry = (tIpDbIfaceEntry *) pRBElem;
    *pu4RetValFsIpDbIntfNoOfVlanBindings =
        IPDB_INTF_NOOF_BINDINGS (pIpDbIfaceEntry);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpDbIntfNoOfVlanStaticBindings
 Input       :  The Indices
                FsIpDbIntfVlanId

                The Object 
                retValFsIpDbIntfNoOfVlanStaticBindings
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbIntfNoOfVlanStaticBindings (INT4 i4FsIpDbIntfVlanId,
                                        UINT4
                                        *pu4RetValFsIpDbIntfNoOfVlanStaticBindings)
{
    tRBElem            *pRBElem = NULL;
    tIpDbIfaceEntry    *pIpDbIfaceEntry = NULL;
    tIpDbIfaceEntry     IpDbIfaceEntry;

    MEMSET (&IpDbIfaceEntry, IPDB_ZERO, sizeof (tIpDbIfaceEntry));

    IpDbIfaceEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbIfaceEntry.VlanId = (tVlanId) i4FsIpDbIntfVlanId;

    /* Calling the Function to get the Structure Pointer and   *
     * Checking whether that entry is Null or not              */
    if ((pRBElem = RBTreeGet (IPDB_INTF_RBTREE,
                              (tRBElem *) & IpDbIfaceEntry)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbIfaceEntry = (tIpDbIfaceEntry *) pRBElem;

    *pu4RetValFsIpDbIntfNoOfVlanStaticBindings =
        IPDB_INTF_NOOF_STAT_BINDS (pIpDbIfaceEntry);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpDbIntfNoOfVlanDHCPBindings
 Input       :  The Indices
                FsIpDbIntfVlanId

                The Object 
                retValFsIpDbIntfNoOfVlanDHCPBindings
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbIntfNoOfVlanDHCPBindings (INT4 i4FsIpDbIntfVlanId,
                                      UINT4
                                      *pu4RetValFsIpDbIntfNoOfVlanDHCPBindings)
{
    tRBElem            *pRBElem = NULL;
    tIpDbIfaceEntry    *pIpDbIfaceEntry = NULL;
    tIpDbIfaceEntry     IpDbIfaceEntry;

    MEMSET (&IpDbIfaceEntry, IPDB_ZERO, sizeof (tIpDbIfaceEntry));

    IpDbIfaceEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbIfaceEntry.VlanId = (tVlanId) i4FsIpDbIntfVlanId;

    /* Calling the Function to get the Structure Pointer and   *
     * Checking whether that entry is Null or not              */
    if ((pRBElem = RBTreeGet (IPDB_INTF_RBTREE,
                              (tRBElem *) & IpDbIfaceEntry)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbIfaceEntry = (tIpDbIfaceEntry *) pRBElem;
    *pu4RetValFsIpDbIntfNoOfVlanDHCPBindings =
        IPDB_INTF_NOOF_DHCP_BINDS (pIpDbIfaceEntry);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpDbIntfNoOfVlanPPPBindings
 Input       :  The Indices
                FsIpDbIntfVlanId

                The Object 
                retValFsIpDbIntfNoOfVlanPPPBindings
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbIntfNoOfVlanPPPBindings (INT4 i4FsIpDbIntfVlanId,
                                     UINT4
                                     *pu4RetValFsIpDbIntfNoOfVlanPPPBindings)
{
    tRBElem            *pRBElem = NULL;
    tIpDbIfaceEntry    *pIpDbIfaceEntry = NULL;
    tIpDbIfaceEntry     IpDbIfaceEntry;

    MEMSET (&IpDbIfaceEntry, IPDB_ZERO, sizeof (tIpDbIfaceEntry));

    IpDbIfaceEntry.u4ContextId = IPDB_CURR_CONTEXT_ID;
    IpDbIfaceEntry.VlanId = (tVlanId) i4FsIpDbIntfVlanId;

    /* Calling the Function to get the Structure Pointer and   *
     * Checking whether that entry is Null or not              */
    if ((pRBElem = RBTreeGet (IPDB_INTF_RBTREE,
                              (tRBElem *) & IpDbIfaceEntry)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpDbIfaceEntry = (tIpDbIfaceEntry *) pRBElem;
    *pu4RetValFsIpDbIntfNoOfVlanPPPBindings =
        IPDB_INTF_NOOF_PPP_BINDS (pIpDbIfaceEntry);

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsIpDbSrcGuardConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIpDbSrcGuardConfigTable
 Input       :  The Indices
                FsIpDbSrcGuardIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIpDbSrcGuardConfigTable (INT4 i4FsIpDbSrcGuardIndex)
{
    tPortCtrlEntry     *pPortCtrlEntry = NULL;

    IpDbGetPortCtrlEntry ((UINT4) i4FsIpDbSrcGuardIndex, &pPortCtrlEntry);

    if (pPortCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIpDbSrcGuardConfigTable
 Input       :  The Indices
                FsIpDbSrcGuardIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIpDbSrcGuardConfigTable (INT4 *pi4FsIpDbSrcGuardIndex)
{
    tPortCtrlEntry     *pPortCtrlEntry = NULL;

    pPortCtrlEntry = (tPortCtrlEntry *) RBTreeGetFirst (IPDB_PORTCTRL_RBTREE);

    if (pPortCtrlEntry != NULL)
    {
        *pi4FsIpDbSrcGuardIndex = pPortCtrlEntry->u4IfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIpDbSrcGuardConfigTable
 Input       :  The Indices
                FsIpDbSrcGuardIndex
                nextFsIpDbSrcGuardIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIpDbSrcGuardConfigTable (INT4 i4FsIpDbSrcGuardIndex,
                                          INT4 *pi4NextFsIpDbSrcGuardIndex)
{
    tPortCtrlEntry     *pPortCtrlEntry = NULL;
    tPortCtrlEntry      PortCtrlEntry;

    MEMSET (&PortCtrlEntry, IPDB_ZERO, sizeof (tPortCtrlEntry));

    PortCtrlEntry.u4IfIndex = i4FsIpDbSrcGuardIndex;

    pPortCtrlEntry =
        (tPortCtrlEntry *) RBTreeGetNext (IPDB_PORTCTRL_RBTREE,
                                          (tPortCtrlEntry *) & PortCtrlEntry,
                                          NULL);

    if (pPortCtrlEntry != NULL)
    {
        *pi4NextFsIpDbSrcGuardIndex = pPortCtrlEntry->u4IfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIpDbSrcGuardStatus
 Input       :  The Indices
                FsIpDbSrcGuardIndex

                The Object 
                retValFsIpDbSrcGuardStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbSrcGuardStatus (INT4 i4FsIpDbSrcGuardIndex,
                            INT4 *pi4RetValFsIpDbSrcGuardStatus)
{
    tPortCtrlEntry     *pPortCtrlEntry = NULL;

    IpDbGetPortCtrlEntry ((UINT4) i4FsIpDbSrcGuardIndex, &pPortCtrlEntry);

    if (pPortCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsIpDbSrcGuardStatus = (INT4) pPortCtrlEntry->u1IpsgStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIpDbSrcGuardStatus
 Input       :  The Indices
                FsIpDbSrcGuardIndex

                The Object 
                setValFsIpDbSrcGuardStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIpDbSrcGuardStatus (INT4 i4FsIpDbSrcGuardIndex,
                            INT4 i4SetValFsIpDbSrcGuardStatus)
{
    tPortCtrlEntry     *pPortCtrlEntry = NULL;
    tAclFilterInfo      AclFilterInfo;
    UINT4               u4IpL2FilterId = IPDB_ZERO;
    UINT4               u4IpL3FilterId = IPDB_ZERO;
    UINT4               u4IpUDBFilterId = IPDB_ZERO;

    IpDbGetPortCtrlEntry ((UINT4) i4FsIpDbSrcGuardIndex, &pPortCtrlEntry);

    if (pPortCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPortCtrlEntry->u1IpsgStatus != i4SetValFsIpDbSrcGuardStatus)
    {
        pPortCtrlEntry->u1IpsgStatus = (UINT1) i4SetValFsIpDbSrcGuardStatus;

        if ((pPortCtrlEntry->u1IpsgStatus == IPDB_IPSG_IP_MODE) ||
            (pPortCtrlEntry->u1IpsgStatus == IPDB_IPSG_IP_MAC_MODE))
        {
            OSIX_BITLIST_SET_BIT (IPDB_IPSG_PORTLIST, i4FsIpDbSrcGuardIndex,
                                  sizeof (tPortList));
            IPDB_IPSG_REF_COUNT++;
        }
        else
        {
            OSIX_BITLIST_RESET_BIT (IPDB_IPSG_PORTLIST, i4FsIpDbSrcGuardIndex,
                                    sizeof (tPortList));
            IPDB_IPSG_REF_COUNT--;
        }

        /* While updation of IPDb entry, delete the ACL entry 
         * and create the entry again with the updated information */
        IssACLDeleteFilter (ISS_UDBFILTER, gu4IpL2FilterId,
                            gu4IpL3FilterId, gu4IpUdpFilterId);
        if (IPDB_IPSG_REF_COUNT == IPDB_ZERO)
        {
            /* Filter is not installed, as IPSG status is not enabled 
             * on any of the interfaces */
            return SNMP_SUCCESS;
        }
        MEMSET (&AclFilterInfo, 0x0, sizeof (tAclFilterInfo));

        AclFilterInfo.u1ProtocolId = ISS_IP;
        AclFilterInfo.u1Action = ISS_DROP;
        AclFilterInfo.u1Priority = ISS_DEFAULT_PRIORITY;
        AclFilterInfo.u1FilterType = ISS_UDBFILTER;
        MEMCPY (AclFilterInfo.PortList, IPDB_IPSG_PORTLIST, sizeof (tPortList));
        if (IssACLCreateFilter (&AclFilterInfo, &u4IpL2FilterId,
                                &u4IpL3FilterId, &u4IpUDBFilterId)
            == ISS_FAILURE)
        {
            IPDB_TRC (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                      "Failed to add the binding entry to ACL table\r\n");
            return SNMP_FAILURE;
        }
        gu4IpL3FilterId = u4IpL3FilterId;
        gu4IpL2FilterId = u4IpL2FilterId;
        gu4IpUdpFilterId = u4IpUDBFilterId;

        pPortCtrlEntry->u4UDBFilterId = u4IpUDBFilterId;
    }
    else
    {
        OSIX_BITLIST_SET_BIT (IPDB_IPSG_PORTLIST, i4FsIpDbSrcGuardIndex,
                              sizeof (tPortList));
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIpDbSrcGuardStatus
 Input       :  The Indices
                FsIpDbSrcGuardIndex

                The Object 
                testValFsIpDbSrcGuardStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIpDbSrcGuardStatus (UINT4 *pu4ErrorCode,
                               INT4 i4FsIpDbSrcGuardIndex,
                               INT4 i4TestValFsIpDbSrcGuardStatus)
{
    tPortCtrlEntry     *pPortCtrlEntry = NULL;

    IpDbGetPortCtrlEntry ((UINT4) i4FsIpDbSrcGuardIndex, &pPortCtrlEntry);

    if (pPortCtrlEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IPDB_INVALID_PORT_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIpDbSrcGuardStatus == IPDB_IPSG_DISABLE) ||
        (i4TestValFsIpDbSrcGuardStatus == IPDB_IPSG_IP_MODE) ||
        (i4TestValFsIpDbSrcGuardStatus == IPDB_IPSG_IP_MAC_MODE))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IPDB_INVALID_VALUE_ERR);
        return SNMP_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIpDbSrcGuardConfigTable
 Input       :  The Indices
                FsIpDbSrcGuardIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIpDbSrcGuardConfigTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIpDbTraceLevel
 Input       :  The Indices

                The Object 
                retValFsIpDbTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpDbTraceLevel (INT4 *pi4RetValFsIpDbTraceLevel)
{
    *pi4RetValFsIpDbTraceLevel = IPDB_TRC_FLAG;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIpDbTraceLevel
 Input       :  The Indices

                The Object 
                setValFsIpDbTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIpDbTraceLevel (INT4 i4SetValFsIpDbTraceLevel)
{
    IPDB_TRC_FLAG = (UINT4) i4SetValFsIpDbTraceLevel;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIpDbTraceLevel
 Input       :  The Indices

                The Object 
                testValFsIpDbTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIpDbTraceLevel (UINT4 *pu4ErrorCode, INT4 i4TestValFsIpDbTraceLevel)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsIpDbTraceLevel);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIpDbTraceLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIpDbTraceLevel (UINT4 *pu4ErrorCode,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/*                                                                           */
/***************************** END OF FILE ***********************************/
/*                                                                           */
