/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: ipdbintf.c,v 1.4 2013/12/16 15:27:56 siva Exp $                */
/*****************************************************************************/
/*    FILE  NAME            : ipdbintf.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : IP Binding Database management                 */
/*    MODULE NAME           : Ip Binding Database management Interface module*/
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains interface specific APIs for */
/*                            IP Binding Database management module          */
/*---------------------------------------------------------------------------*/

#include "ipdbinc.h"
#include "ipdbextn.h"

/*****************************************************************************/
/* Function Name      : IpdbIntfCheckIfEntryExist                            */
/*                                                                           */
/* Description        : This function checks whether interface entry exists  */
/*                                                                           */
/* Input(s)           : u4ContextID - Context Identifier                     */
/*                      VlanId      - VLAN Identifier                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NULL/Ponter to the entry.                            */
/*****************************************************************************/
tIpDbIfaceEntry    *
IpdbIntfCheckIfEntryExist (UINT4 u4ContextID, tVlanId VlanId)
{
    tIpDbIfaceEntry     IpDbIfaceEntry;
    tIpDbIfaceEntry    *pIpDbIfaceEntry = NULL;

    MEMSET (&IpDbIfaceEntry, IPDB_ZERO, sizeof (tIpDbIfaceEntry));

    IpDbIfaceEntry.u4ContextId = u4ContextID;
    IpDbIfaceEntry.VlanId = VlanId;

    /* Get the entry from the RBTree */
    pIpDbIfaceEntry = (tIpDbIfaceEntry *)
        RBTreeGet (IPDB_INTF_RBTREE, &IpDbIfaceEntry);

    return pIpDbIfaceEntry;
}

/*****************************************************************************/
/* Function Name      : IpdbIntfGetEntry                                     */
/*                                                                           */
/* Description        : This function is to get an interface entry. If the   */
/*                      entry does not exist, create one and return the      */
/*                      pointer                                              */
/*                                                                           */
/* Input(s)           : u4ContextID - Context Identifier                     */
/*                      VlanId      - VLAN Identifier                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NULL/Ponter to the entry created/exists.             */
/*****************************************************************************/
tIpDbIfaceEntry    *
IpdbIntfGetEntry (UINT4 u4ContextID, tVlanId VlanId)
{
    tIpDbIfaceEntry    *pIpDbIfaceEntry = NULL;

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_ENTRY, IPDB_MODULE_NAME,
                   "Entering the function %s \r\n", __FUNCTION__);

    IPDB_TRC_ARG3 (IPDB_TRC_FLAG, IPDB_FN_ARGS, IPDB_MODULE_NAME,
                   "The arguments to the function %s are Context %d VlanId %d"
                   "\r\n", __FUNCTION__, u4ContextID, VlanId);

    /* Check whether the interface entry already exist */
    pIpDbIfaceEntry = IpdbIntfCheckIfEntryExist (u4ContextID, VlanId);

    if (pIpDbIfaceEntry == NULL)
    {
        /* If entry does not exist, create one */
        if ((pIpDbIfaceEntry = (tIpDbIfaceEntry *)
             IPDB_ALLOC_MEM_BLOCK (IPDB_INTF_POOL_ID)) == NULL)
        {
            IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                           "Failed to allocate memory for"
                           "VLAN entry for VLAN ID %d \r\n", VlanId);
            return NULL;
        }

        MEMSET (pIpDbIfaceEntry, IPDB_ZERO, sizeof (tIpDbIfaceEntry));

        pIpDbIfaceEntry->u4ContextId = u4ContextID;
        pIpDbIfaceEntry->VlanId = VlanId;

        /* Add the entry to RBTree */
        if (RBTreeAdd (IPDB_INTF_RBTREE, (tRBElem *) pIpDbIfaceEntry)
            != RB_SUCCESS)
        {
            IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                           "Failed to add VLAN %d entry to RBTree \r\n",
                           VlanId);
            IPDB_RELEASE_MEM_BLOCK (IPDB_INTF_POOL_ID, pIpDbIfaceEntry);
            return NULL;
        }
    }

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);

    return pIpDbIfaceEntry;
}

/*****************************************************************************/
/* Function Name      : IpdbIntfDeleteEntry                                  */
/*                                                                           */
/* Description        : This function is to delete an interface entry        */
/*                                                                           */
/* Input(s)           : u4ContextID - Context Identifier                     */
/*                      VlanId      - VLAN Identifier                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IPDB_SUCCESS/IPDB_FAILURE                            */
/*****************************************************************************/
INT4
IpdbIntfDeleteEntry (UINT4 u4ContextID, tVlanId VlanId)
{
    tIpDbIfaceEntry    *pIpDbIfaceEntry = NULL;
    INT4                i4RetStatus = IPDB_SUCCESS;

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_ENTRY, IPDB_MODULE_NAME,
                   "Entering the function %s \r\n", __FUNCTION__);

    IPDB_TRC_ARG3 (IPDB_TRC_FLAG, IPDB_FN_ARGS, IPDB_MODULE_NAME,
                   "The arguments to the function %s are Context %d,VlanId %d"
                   "\r\n", __FUNCTION__, u4ContextID, VlanId);

    /* Check if the entry exists */
    pIpDbIfaceEntry = IpdbIntfCheckIfEntryExist (u4ContextID, VlanId);

    if (pIpDbIfaceEntry == NULL)
    {
        IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                       "Exiting the function %s \r\n", __FUNCTION__);

        /* If the entry does not exist return success */
        return IPDB_SUCCESS;
    }

    if (RBTreeRemove (IPDB_INTF_RBTREE, pIpDbIfaceEntry) != RB_SUCCESS)
    {
        IPDB_TRC_ARG2 (IPDB_TRC_FLAG, IPDB_FAIL_TRC, IPDB_MODULE_NAME,
                       "Failed to remove VLAN %d entry in context %d from "
                       "RBTree\r\n", VlanId, u4ContextID);
        i4RetStatus = IPDB_FAILURE;
    }

    IPDB_RELEASE_MEM_BLOCK (IPDB_INTF_POOL_ID, pIpDbIfaceEntry);

    IPDB_TRC_ARG1 (IPDB_TRC_FLAG, IPDB_FN_EXIT, IPDB_MODULE_NAME,
                   "Exiting the function %s \r\n", __FUNCTION__);

    return i4RetStatus;
}

/*                                                                           */
/***************************** END OF FILE ***********************************/
/*                                                                           */
