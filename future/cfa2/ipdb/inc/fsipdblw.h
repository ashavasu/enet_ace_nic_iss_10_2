/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsipdblw.h,v 1.4 2010/07/05 10:37:54 prabuc Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

#ifndef _FSIPDBLW_H
#define _FSIPDBLW_H
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpDbNoOfBindings ARG_LIST((UINT4 *));

INT1
nmhGetFsIpDbNoOfStaticBindings ARG_LIST((UINT4 *));

INT1
nmhGetFsIpDbNoOfDHCPBindings ARG_LIST((UINT4 *));

INT1
nmhGetFsIpDbNoOfPPPBindings ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for FsIpDbStaticBindingTable. */
INT1
nmhValidateIndexInstanceFsIpDbStaticBindingTable ARG_LIST((INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsIpDbStaticBindingTable  */

INT1
nmhGetFirstIndexFsIpDbStaticBindingTable ARG_LIST((INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIpDbStaticBindingTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpDbStaticHostIp ARG_LIST((INT4  , tMacAddr ,UINT4 *));

INT1
nmhGetFsIpDbStaticInIfIndex ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsIpDbStaticGateway ARG_LIST((INT4  , tMacAddr ,UINT4 *));

INT1
nmhGetFsIpDbStaticBindingStatus ARG_LIST((INT4  , tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIpDbStaticHostIp ARG_LIST((INT4  , tMacAddr  ,UINT4 ));

INT1
nmhSetFsIpDbStaticInIfIndex ARG_LIST((INT4  , tMacAddr  ,INT4 ));

INT1
nmhSetFsIpDbStaticGateway ARG_LIST((INT4  , tMacAddr  ,UINT4 ));

INT1
nmhSetFsIpDbStaticBindingStatus ARG_LIST((INT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIpDbStaticHostIp ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,UINT4 ));

INT1
nmhTestv2FsIpDbStaticInIfIndex ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

INT1
nmhTestv2FsIpDbStaticGateway ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,UINT4 ));

INT1
nmhTestv2FsIpDbStaticBindingStatus ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIpDbStaticBindingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsIpDbBindingTable. */
INT1
nmhValidateIndexInstanceFsIpDbBindingTable ARG_LIST((INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsIpDbBindingTable  */

INT1
nmhGetFirstIndexFsIpDbBindingTable ARG_LIST((INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIpDbBindingTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpDbHostBindingType ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsIpDbHostIp ARG_LIST((INT4  , tMacAddr ,UINT4 *));

INT1
nmhGetFsIpDbHostInIfIndex ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsIpDbHostRemLeaseTime ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsIpDbHostBindingID ARG_LIST((INT4  , tMacAddr ,UINT4 *));

/* Proto Validate Index Instance for FsIpDbGatewayIpTable. */
INT1
nmhValidateIndexInstanceFsIpDbGatewayIpTable ARG_LIST((tMacAddr  , INT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIpDbGatewayIpTable  */

INT1
nmhGetFirstIndexFsIpDbGatewayIpTable ARG_LIST((tMacAddr *  , INT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIpDbGatewayIpTable ARG_LIST((tMacAddr , tMacAddr *  , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpDbGatewayIpMode ARG_LIST((tMacAddr  , INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsIpDbInterfaceTable. */
INT1
nmhValidateIndexInstanceFsIpDbInterfaceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIpDbInterfaceTable  */

INT1
nmhGetFirstIndexFsIpDbInterfaceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIpDbInterfaceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpDbIntfNoOfVlanBindings ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpDbIntfNoOfVlanStaticBindings ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpDbIntfNoOfVlanDHCPBindings ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIpDbIntfNoOfVlanPPPBindings ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsIpDbSrcGuardConfigTable. */
INT1
nmhValidateIndexInstanceFsIpDbSrcGuardConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIpDbSrcGuardConfigTable  */

INT1
nmhGetFirstIndexFsIpDbSrcGuardConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIpDbSrcGuardConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpDbSrcGuardStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIpDbSrcGuardStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIpDbSrcGuardStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIpDbSrcGuardConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpDbTraceLevel ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIpDbTraceLevel ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIpDbTraceLevel ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIpDbTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

#endif
