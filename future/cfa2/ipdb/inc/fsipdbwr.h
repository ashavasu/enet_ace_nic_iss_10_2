#ifndef _FSIPDBWR_H
#define _FSIPDBWR_H

VOID RegisterFSIPDB(VOID);

VOID UnRegisterFSIPDB(VOID);
INT4 FsIpDbNoOfBindingsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbNoOfStaticBindingsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbNoOfDHCPBindingsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbNoOfPPPBindingsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsIpDbStaticBindingTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIpDbStaticHostIpGet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbStaticInIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbStaticGatewayGet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbStaticBindingStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbStaticHostIpSet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbStaticInIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbStaticGatewaySet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbStaticBindingStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbStaticHostIpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpDbStaticInIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpDbStaticGatewayTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpDbStaticBindingStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpDbStaticBindingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsIpDbBindingTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIpDbHostBindingTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbHostIpGet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbHostInIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbHostRemLeaseTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbHostBindingIDGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsIpDbGatewayIpTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIpDbGatewayIpModeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsIpDbInterfaceTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIpDbIntfNoOfVlanBindingsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbIntfNoOfVlanStaticBindingsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbIntfNoOfVlanDHCPBindingsGet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbIntfNoOfVlanPPPBindingsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsIpDbSrcGuardConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIpDbSrcGuardStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbSrcGuardStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbSrcGuardStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpDbSrcGuardConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsIpDbTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsIpDbTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpDbTraceLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSIPDBWR_H */
