#ifndef _IPDBDEFN_H
#define _IPDBDEFN_H
/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: ipdbdefn.h,v 1.7 2010/10/18 06:27:08 prabuc Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : ipdbdefn.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : IP Binding Database management                 */
/*    MODULE NAME           : IP Binding Database management definitions     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains type definitions            */
/*                            for IP Binding Database management module      */
/*---------------------------------------------------------------------------*/

/* Configatable values -- START */
/* Maximum number of static IP Binding entries */
#define IPDB_MAX_STATIC_ENTRIES     50
#define IPDB_MAX_BINDING_ENTRIES    254 /* Includes static and dynamic (PPPoE 
                                           and DHCP) entries accross all VLANS 
                                         */
#define IPDB_MAX_GW_ENTRIES         1   /* Maximum number of gateway entries 
                                           per host */
#define IPDB_DEFAULT_CONTEXT        L2IWF_DEFAULT_CONTEXT
/* Configatable values -- END */

/* Maximum allowed portname */
#define  IPDB_MAX_PORT_NAME_LENGTH  24

/* Minimum VLAN ID Allowed */
#define IPDB_MIN_NUM_VLAN           0
 
#ifndef __FUNCTION__
#define __FUNCTION__ "FunName"
#endif

/* CLI definitions */
#define IPDB_IP_STR_LEN             16
#define IPDB_MAC_STR_LEN            24
#define IPDB_BIND_TYPE_LEN          8

/* Osix, Task, Queue and Event related definitions */
#define IPDB_TASK_NAME              (const UINT1 *) "IPDB"
#define IPDB_SEM_NAME               (const UINT1 *) "IDBS"
#define IPDB_QUEUE_NAME             (UINT1 *) "IPDQ"
#define IPDB_MODULE_NAME            "IPDB"
#define IPDB_SEM_COUNT              1
#define IPDB_QUEUE_DEPTH            10
#define IPDB_SEM_FLAGS              OSIX_DEFAULT_SEM_MODE

/* Different events to be handled */
#define IPDB_EVENT_ARRIVED          0x01
#define IPDB_TIMER_EXP_EVENT        0x02
#define IPDB_VLAN_INTERFACE_EVENT   0x04
#define IPDB_ARP_PKT_ARRIVAL_EVENT  0x05

#define IPDB_PORT_CREATE_EVENT   0x06
#define IPDB_PORT_DELETE_EVENT   0x07

/* Timer ID */
#define IPDB_BINDING_TIMER          1

#define IPDB_MIN_PORTS              1
#define IPDB_MAX_PORTS              BRG_MAX_PHY_PLUS_LOG_PORTS

#define IPDB_PORT_TYPE_UPLINK       1
#define IPDB_PORT_TYPE_DOWNLINK     2

/* Mac Address validation */
#define IPDB_IS_MAC_MULTICAST(pMacAddr) (pMacAddr[0] & 0x01)

/* MTU size */
#define IPDB_MAX_MTU                1600

#define IPDB_IS_BCASTADDR(pMacAddr) \
    (((MEMCMP(pMacAddr,gIpdbBcastAddress, sizeof (tMacAddr)) == 0) || \
      (MEMCMP(pMacAddr,gIpdbNullBcastAddress, sizeof (tMacAddr)) == 0)) \
     ? IPDB_TRUE : IPDB_FALSE)

#define IPDB_IS_MCASTADDR(pMacAddr) \
    (((pMacAddr[0] & 0x01) != 0) ? \
     IPDB_TRUE : IPDB_FALSE)

/* Trace Related definitions */
#define IPDB_TRC_NONE               0x0

#ifdef TRACE_WANTED
#define IPDB_TRC(Flag, Value, Module, Fmt) \
    UtlTrcLog(Flag, Value, Module, Fmt)

#define IPDB_TRC_ARG1(Flag, Value, Module, Fmt, Arg1) \
    UtlTrcLog(Flag, Value, Module, Fmt, Arg1)

#define IPDB_TRC_ARG2(Flag, Value, Module, Fmt, Arg1, Arg2) \
    UtlTrcLog(Flag, Value, Module, Fmt, Arg1, Arg2)

#define IPDB_TRC_ARG3(Flag, Value, Module, Fmt, Arg1, Arg2, Arg3) \
    UtlTrcLog(Flag, Value, Module, Fmt, Arg1, Arg2, Arg3)

#define IPDB_TRC_ARG4(Flag, Value, Module, Fmt, Arg1, Arg2, Arg3, Arg4) \
    UtlTrcLog(Flag, Value, Module, Fmt, Arg1, Arg2, Arg3, Arg4)

#define IPDB_TRC_ARG5(Flag, Value, Module, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5) \
    UtlTrcLog(Flag, Value, Module, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)


#else
#define IPDB_TRC(Flag, Value, Module, Fmt)
#define IPDB_TRC_ARG1(Flag, Value, Module, Fmt, Arg1)
#define IPDB_TRC_ARG2(Flag, Value, Module, Fmt, Arg1, Arg2)
#define IPDB_TRC_ARG3(Flag, Value, Module, Fmt, Arg1, Arg2, Arg3)
#define IPDB_TRC_ARG4(Flag, Value, Module, Fmt, Arg1, Arg2, Arg3, Arg4)
#define IPDB_TRC_ARG5(Flag, Value, Module, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#endif

#define IPDB_MINUS_ONE              -1
#define IPDB_ZERO                    0
#define IPDB_ONE                     1
#define IPDB_TWO                     2
#define IPDB_THREE                   3
#define IPDB_PORT_SEC_STATE_TRUSTED  1
#define IPDB_INVALID_CXT_ID          0xFFFFFFFF 
#define IPDB_ISS_DEFAULT_PRIORITY    2

#endif /* _IPDBDEFN_H */
