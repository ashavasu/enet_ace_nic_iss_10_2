/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsipdbdb.h,v 1.3 2010/07/05 10:37:54 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSIPDBDB_H
#define _FSIPDBDB_H

UINT1 FsIpDbStaticBindingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsIpDbBindingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsIpDbGatewayIpTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsIpDbInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsIpDbSrcGuardConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fsipdb [] ={1,3,6,1,4,1,29601,2,2};
tSNMP_OID_TYPE fsipdbOID = {9, fsipdb};


UINT4 FsIpDbNoOfBindings [ ] ={1,3,6,1,4,1,29601,2,2,1,1};
UINT4 FsIpDbNoOfStaticBindings [ ] ={1,3,6,1,4,1,29601,2,2,1,2};
UINT4 FsIpDbNoOfDHCPBindings [ ] ={1,3,6,1,4,1,29601,2,2,1,3};
UINT4 FsIpDbNoOfPPPBindings [ ] ={1,3,6,1,4,1,29601,2,2,1,4};
UINT4 FsIpDbStaticHostVlanId [ ] ={1,3,6,1,4,1,29601,2,2,2,1,1,1};
UINT4 FsIpDbStaticHostMac [ ] ={1,3,6,1,4,1,29601,2,2,2,1,1,2};
UINT4 FsIpDbStaticHostIp [ ] ={1,3,6,1,4,1,29601,2,2,2,1,1,3};
UINT4 FsIpDbStaticInIfIndex [ ] ={1,3,6,1,4,1,29601,2,2,2,1,1,4};
UINT4 FsIpDbStaticGateway [ ] ={1,3,6,1,4,1,29601,2,2,2,1,1,5};
UINT4 FsIpDbStaticBindingStatus [ ] ={1,3,6,1,4,1,29601,2,2,2,1,1,6};
UINT4 FsIpDbHostVlanId [ ] ={1,3,6,1,4,1,29601,2,2,3,1,1,1};
UINT4 FsIpDbHostMac [ ] ={1,3,6,1,4,1,29601,2,2,3,1,1,2};
UINT4 FsIpDbHostBindingType [ ] ={1,3,6,1,4,1,29601,2,2,3,1,1,3};
UINT4 FsIpDbHostIp [ ] ={1,3,6,1,4,1,29601,2,2,3,1,1,4};
UINT4 FsIpDbHostInIfIndex [ ] ={1,3,6,1,4,1,29601,2,2,3,1,1,5};
UINT4 FsIpDbHostRemLeaseTime [ ] ={1,3,6,1,4,1,29601,2,2,3,1,1,6};
UINT4 FsIpDbHostBindingID [ ] ={1,3,6,1,4,1,29601,2,2,3,1,1,7};
UINT4 FsIpDbGatewayNetwork [ ] ={1,3,6,1,4,1,29601,2,2,3,2,1,1};
UINT4 FsIpDbGatewayNetMask [ ] ={1,3,6,1,4,1,29601,2,2,3,2,1,2};
UINT4 FsIpDbGatewayIp [ ] ={1,3,6,1,4,1,29601,2,2,3,2,1,3};
UINT4 FsIpDbGatewayIpMode [ ] ={1,3,6,1,4,1,29601,2,2,3,2,1,4};
UINT4 FsIpDbIntfVlanId [ ] ={1,3,6,1,4,1,29601,2,2,4,1,1,1};
UINT4 FsIpDbIntfNoOfVlanBindings [ ] ={1,3,6,1,4,1,29601,2,2,4,1,1,2};
UINT4 FsIpDbIntfNoOfVlanStaticBindings [ ] ={1,3,6,1,4,1,29601,2,2,4,1,1,3};
UINT4 FsIpDbIntfNoOfVlanDHCPBindings [ ] ={1,3,6,1,4,1,29601,2,2,4,1,1,4};
UINT4 FsIpDbIntfNoOfVlanPPPBindings [ ] ={1,3,6,1,4,1,29601,2,2,4,1,1,5};
UINT4 FsIpDbSrcGuardIndex [ ] ={1,3,6,1,4,1,29601,2,2,5,1,1,1};
UINT4 FsIpDbSrcGuardStatus [ ] ={1,3,6,1,4,1,29601,2,2,5,1,1,2};
UINT4 FsIpDbTraceLevel [ ] ={1,3,6,1,4,1,29601,2,2,1,5};




tMbDbEntry fsipdbMibEntry[]= {

{{11,FsIpDbNoOfBindings}, NULL, FsIpDbNoOfBindingsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsIpDbNoOfStaticBindings}, NULL, FsIpDbNoOfStaticBindingsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsIpDbNoOfDHCPBindings}, NULL, FsIpDbNoOfDHCPBindingsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsIpDbNoOfPPPBindings}, NULL, FsIpDbNoOfPPPBindingsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsIpDbTraceLevel}, NULL, FsIpDbTraceLevelGet, FsIpDbTraceLevelSet, FsIpDbTraceLevelTest, FsIpDbTraceLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,FsIpDbStaticHostVlanId}, GetNextIndexFsIpDbStaticBindingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIpDbStaticBindingTableINDEX, 2, 0, 0, NULL},

{{13,FsIpDbStaticHostMac}, GetNextIndexFsIpDbStaticBindingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsIpDbStaticBindingTableINDEX, 2, 0, 0, NULL},

{{13,FsIpDbStaticHostIp}, GetNextIndexFsIpDbStaticBindingTable, FsIpDbStaticHostIpGet, FsIpDbStaticHostIpSet, FsIpDbStaticHostIpTest, FsIpDbStaticBindingTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsIpDbStaticBindingTableINDEX, 2, 0, 0, NULL},

{{13,FsIpDbStaticInIfIndex}, GetNextIndexFsIpDbStaticBindingTable, FsIpDbStaticInIfIndexGet, FsIpDbStaticInIfIndexSet, FsIpDbStaticInIfIndexTest, FsIpDbStaticBindingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIpDbStaticBindingTableINDEX, 2, 0, 0, NULL},

{{13,FsIpDbStaticGateway}, GetNextIndexFsIpDbStaticBindingTable, FsIpDbStaticGatewayGet, FsIpDbStaticGatewaySet, FsIpDbStaticGatewayTest, FsIpDbStaticBindingTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsIpDbStaticBindingTableINDEX, 2, 0, 0, NULL},

{{13,FsIpDbStaticBindingStatus}, GetNextIndexFsIpDbStaticBindingTable, FsIpDbStaticBindingStatusGet, FsIpDbStaticBindingStatusSet, FsIpDbStaticBindingStatusTest, FsIpDbStaticBindingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIpDbStaticBindingTableINDEX, 2, 0, 1, NULL},

{{13,FsIpDbHostVlanId}, GetNextIndexFsIpDbBindingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIpDbBindingTableINDEX, 2, 0, 0, NULL},

{{13,FsIpDbHostMac}, GetNextIndexFsIpDbBindingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsIpDbBindingTableINDEX, 2, 0, 0, NULL},

{{13,FsIpDbHostBindingType}, GetNextIndexFsIpDbBindingTable, FsIpDbHostBindingTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIpDbBindingTableINDEX, 2, 0, 0, NULL},

{{13,FsIpDbHostIp}, GetNextIndexFsIpDbBindingTable, FsIpDbHostIpGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsIpDbBindingTableINDEX, 2, 0, 0, NULL},

{{13,FsIpDbHostInIfIndex}, GetNextIndexFsIpDbBindingTable, FsIpDbHostInIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIpDbBindingTableINDEX, 2, 0, 0, NULL},

{{13,FsIpDbHostRemLeaseTime}, GetNextIndexFsIpDbBindingTable, FsIpDbHostRemLeaseTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIpDbBindingTableINDEX, 2, 0, 0, NULL},

{{13,FsIpDbHostBindingID}, GetNextIndexFsIpDbBindingTable, FsIpDbHostBindingIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsIpDbBindingTableINDEX, 2, 0, 0, NULL},

{{13,FsIpDbGatewayNetwork}, GetNextIndexFsIpDbGatewayIpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIpDbGatewayIpTableINDEX, 5, 0, 0, NULL},

{{13,FsIpDbGatewayNetMask}, GetNextIndexFsIpDbGatewayIpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIpDbGatewayIpTableINDEX, 5, 0, 0, NULL},

{{13,FsIpDbGatewayIp}, GetNextIndexFsIpDbGatewayIpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIpDbGatewayIpTableINDEX, 5, 0, 0, NULL},

{{13,FsIpDbGatewayIpMode}, GetNextIndexFsIpDbGatewayIpTable, FsIpDbGatewayIpModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIpDbGatewayIpTableINDEX, 5, 0, 0, NULL},

{{13,FsIpDbIntfVlanId}, GetNextIndexFsIpDbInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIpDbInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,FsIpDbIntfNoOfVlanBindings}, GetNextIndexFsIpDbInterfaceTable, FsIpDbIntfNoOfVlanBindingsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpDbInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,FsIpDbIntfNoOfVlanStaticBindings}, GetNextIndexFsIpDbInterfaceTable, FsIpDbIntfNoOfVlanStaticBindingsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpDbInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,FsIpDbIntfNoOfVlanDHCPBindings}, GetNextIndexFsIpDbInterfaceTable, FsIpDbIntfNoOfVlanDHCPBindingsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpDbInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,FsIpDbIntfNoOfVlanPPPBindings}, GetNextIndexFsIpDbInterfaceTable, FsIpDbIntfNoOfVlanPPPBindingsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIpDbInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,FsIpDbSrcGuardIndex}, GetNextIndexFsIpDbSrcGuardConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsIpDbSrcGuardConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsIpDbSrcGuardStatus}, GetNextIndexFsIpDbSrcGuardConfigTable, FsIpDbSrcGuardStatusGet, FsIpDbSrcGuardStatusSet, FsIpDbSrcGuardStatusTest, FsIpDbSrcGuardConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIpDbSrcGuardConfigTableINDEX, 1, 0, 0, "1"},
};
tMibData fsipdbEntry = { 29, fsipdbMibEntry };

#endif /* _FSIPDBDB_H */

