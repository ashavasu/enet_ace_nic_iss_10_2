/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssystlw.h,v 1.1.1.1 2010/05/24 05:15:53 prabuc Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsSystemPortCtrlTable. */
INT1
nmhValidateIndexInstanceFsSystemPortCtrlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsSystemPortCtrlTable  */

INT1
nmhGetFirstIndexFsSystemPortCtrlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSystemPortCtrlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSystemPortCtrlDownstreamArpBcast ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsSystemPortCtrlArpPktsForwarded ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsSystemPortCtrlArpPktsDropped ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSystemPortCtrlDownstreamArpBcast ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSystemPortCtrlDownstreamArpBcast ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSystemPortCtrlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsSystemVlanCtrlTable. */
INT1
nmhValidateIndexInstanceFsSystemVlanCtrlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsSystemVlanCtrlTable  */

INT1
nmhGetFirstIndexFsSystemVlanCtrlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSystemVlanCtrlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSystemVlanMacForceFwdStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsSystemVlanRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSystemVlanMacForceFwdStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsSystemVlanRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSystemVlanMacForceFwdStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsSystemVlanRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSystemVlanCtrlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
