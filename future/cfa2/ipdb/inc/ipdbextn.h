#ifndef _IPDBEXTN_H
#define _IPDBEXTN_H
/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: ipdbextn.h,v 1.4 2013/01/24 09:27:04 siva Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : ipdbextn.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : IP Binding Database management                 */
/*    MODULE NAME           : IP Binding Database management externs         */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains external variables          */
/*                            for IP Binding Database management Module      */
/*---------------------------------------------------------------------------*/

/* Instance of global Statistics */
extern tIpDbGlobalStats        gIpDbGlobalStats;

/* Instance of global Informations */
extern tIpDbGlobalInfo         gIpDbGlobalInfo;

/* Mac address string */
extern UINT1                   gau1MacString[IPDB_MAC_STR_LEN];

/* Module initialization status */
extern UINT1                   gu1IsIpdbInitialised;

/* Broadcast MAC address */
extern tMacAddr                gIpdbBcastAddress;
extern tMacAddr                gIpdbNullBcastAddress;

/* Stores the filter ID of default IP filter which is used 
 * to drop all the IP packets */
extern UINT4              gu4IpL3FilterId;
extern UINT4              gu4IpL2FilterId;
extern UINT4              gu4IpUdpFilterId;

#endif /* _IPDBEXTN_H */


