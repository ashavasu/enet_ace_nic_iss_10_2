#ifndef _FSSYSTWR_H
#define _FSSYSTWR_H
INT4 GetNextIndexFsSystemPortCtrlTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSSYST(VOID);

VOID UnRegisterFSSYST(VOID);
INT4 FsSystemPortCtrlDownstreamArpBcastGet(tSnmpIndex *, tRetVal *);
INT4 FsSystemPortCtrlArpPktsForwardedGet(tSnmpIndex *, tRetVal *);
INT4 FsSystemPortCtrlArpPktsDroppedGet(tSnmpIndex *, tRetVal *);
INT4 FsSystemPortCtrlDownstreamArpBcastSet(tSnmpIndex *, tRetVal *);
INT4 FsSystemPortCtrlDownstreamArpBcastTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSystemPortCtrlTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsSystemVlanCtrlTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSystemVlanMacForceFwdStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSystemVlanRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSystemVlanMacForceFwdStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsSystemVlanRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsSystemVlanMacForceFwdStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSystemVlanRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSystemVlanCtrlTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSSYSTWR_H */
