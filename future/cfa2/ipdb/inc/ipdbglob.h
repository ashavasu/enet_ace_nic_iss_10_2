#ifndef _IPDBGLOB_H
#define _IPDBGLOB_H
/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: ipdbglob.h,v 1.4 2013/01/24 09:27:04 siva Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : ipdbglob.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : IP Binding Database management                 */
/*    MODULE NAME           : IP Binding Database management Globals         */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains global variables            */
/*                            for IP Binding Database management Module      */
/*---------------------------------------------------------------------------*/

/* Instance of global Statistics */
tIpDbGlobalStats      gIpDbGlobalStats;

/* Instance of global Informations */
tIpDbGlobalInfo       gIpDbGlobalInfo;

/* Mac address string */
UINT1                 gau1MacString[IPDB_MAC_STR_LEN];

/*  gu1IsIpdbInitialised  - Indicates whether IPDB module is initialised or not
 * during boot time. This is to avoid posting messages to IPDB module before
 * it is initialised. */
UINT1                 gu1IsIpdbInitialised = IPDB_FALSE;

/* Broadcast MAC address */
tMacAddr           gIpdbBcastAddress ={0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
tMacAddr           gIpdbNullBcastAddress ={0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

/* Stores the filter ID of default IP filter which is used 
 * to drop all the IP packets */
UINT4              gu4IpL3FilterId;
UINT4              gu4IpL2FilterId;
UINT4              gu4IpUdpFilterId;

#endif /* _IPDBGLOB_H */
