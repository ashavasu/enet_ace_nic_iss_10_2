#ifndef _IPDBINC_H
#define _IPDBINC_H
/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: ipdbinc.h,v 1.5 2012/03/21 13:04:01 siva Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : ipdbinc.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : IP Binding Database management                 */
/*    MODULE NAME           : IP Binding Database management Header files    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the header files required   */
/*                            for IP Binding Database management Module      */
/*---------------------------------------------------------------------------*/

#include "lr.h"
#include "fsvlan.h"
#include "l2iwf.h"
#include "ipdb.h"
#include "ipdbcli.h"

#include "ipdbdefn.h"
#include "ipdbtdfs.h"
#include "ipdbmacs.h"
#include "ipdbprot.h"
#include  "fsipdblw.h"
#include  "fsipdbwr.h"
#include  "fssystlw.h"
#include  "fssystwr.h"
#include  "fsmiiplw.h"
#include  "fsmiipwr.h"
#include  "vcm.h"
#include  "iss.h"
#include  "ipdbsz.h"

#endif /* _IPDBINC_H */
