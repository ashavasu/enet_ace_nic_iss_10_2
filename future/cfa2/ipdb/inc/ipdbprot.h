#ifndef _IPDBPROT_H
#define _IPDBPROT_H
/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: ipdbprot.h,v 1.16 2013/12/07 10:53:49 siva Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : ipdbprot.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : IP Binding Database management                 */
/*    MODULE NAME           : IP Binding Database management Prototypes      */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains function prototypes         */
/*                            for IP Binding Database management Module      */
/*---------------------------------------------------------------------------*/

/* l2dsintf.c */
PUBLIC tIpDbIfaceEntry *IpdbIntfCheckIfEntryExist PROTO ((UINT4, tVlanId));

PUBLIC tIpDbIfaceEntry *IpdbIntfGetEntry PROTO ((UINT4, tVlanId));

PUBLIC INT4 IpdbIntfDeleteEntry PROTO ((UINT4, tVlanId));

/* ipdbmain.c */
PUBLIC VOID IpdbMainProcessEvent PROTO ((VOID));

PUBLIC VOID IpdbMainProcessTmrEvent PROTO ((VOID));

PUBLIC INT4 IpdbInit PROTO ((VOID));

PUBLIC VOID IpdbDeInit PROTO ((VOID));

PUBLIC INT4 IpdbPostCfgMessage PROTO ((UINT4, UINT4));

PUBLIC VOID IpdbProcessCfgMsgEvent PROTO ((tIpDbQMsg *));

PUBLIC INT4 IpdbGetAllPorts PROTO ((VOID));
    
PUBLIC VOID IpdbAssignMempoolIds PROTO ((VOID));

/* ipdbutil.c */
PUBLIC tIpDbBindingEntry *IpDbUtilGetBindEntryByIp PROTO ((UINT4));

PUBLIC INT4 IpDbIsGatewayIp PROTO ((tIpDbBindingEntry *, UINT4));

PUBLIC INT4 IpDbUtilGetPortForIp PROTO ((UINT4, UINT4 *));

PUBLIC INT4 IpDbUtilTakeLock PROTO ((VOID));

PUBLIC INT4 IpDbUtilReleaseLock PROTO ((VOID));

PUBLIC VOID IpdbUtilRegisterFsIpdbMib PROTO ((VOID));

PUBLIC INT4 IpdbUtilRBTreeStaticCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 IpdbUtilIpRBTreeBindCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 IpdbUtilMacRBTreeBindCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 IpdbUtilRBTreeIntfEntryCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC VOID IpdbUtilRbTreeStaticFree PROTO ((tIpDbStaticBindingEntry *));

PUBLIC VOID IpdbUtilMacRbTreeBindingFree PROTO ((tIpDbBindingEntry *));

PUBLIC VOID IpdbUtilIpRbTreeBindingFree PROTO ((tIpDbBindingEntry *));

PUBLIC VOID IpdbUtilRbTreeIntfFree PROTO ((tIpDbIfaceEntry *));

PUBLIC tCRU_BUF_CHAIN_HEADER *
IpdbUtilDuplicateBuf PROTO ((tCRU_BUF_CHAIN_HEADER *));

PUBLIC INT4 IpdbUtilIsIPDBEntryPresent PROTO ((UINT4, tVlanId, tMacAddr,
                                               UINT4, UINT4));

PUBLIC INT4 IpdbUtilRBTreePortCtrlEntryCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC VOID IpdbUtilRbTreePortCtrlFree PROTO ((tPortCtrlEntry *));

PUBLIC INT4 IpDbUtilCreatePortCtrlEntry PROTO ((UINT4 u4IfIndex));

PUBLIC INT4 IpDbUtilDeletePortCtrlEntry PROTO ((UINT4 u4IfIndex));

PUBLIC INT4 IpDbUtilSetContext PROTO ((UINT4));

PUBLIC VOID IpDbUtilResetContext PROTO ((VOID));

PUBLIC INT4 IpDbUtilGetVcmSystemMode PROTO ((UINT2));

PUBLIC INT4 IpDbUtilGetVcmSystemModeExt PROTO ((UINT2));

PUBLIC INT4 IpDbUtilVcmIsVcExist PROTO ((UINT4));

PUBLIC INT4 IpDbUtilGetNextActiveCtxt PROTO ((UINT4, UINT4 *));

/* ipdbport.c */
PUBLIC UINT1 *IpdbPortConvertMacToStr PROTO ((tMacAddr));

PUBLIC INT4 IpdbPortMacToStr PROTO ((UINT1 *, tMacAddr));

PUBLIC INT4 IpdbPortGetIfName PROTO ((UINT4, INT1 *));

/* ipdbproc.c */
PUBLIC tIpDbBindingEntry *
IpdbProcCreateBindingEntry PROTO ((UINT4, tMacAddr, tVlanId, 
                                   UINT4, UINT4, UINT1));

PUBLIC tIpDbStaticBindingEntry *
IpdbProcCreateStaticEntry PROTO ((UINT4, tMacAddr, tVlanId));

PUBLIC INT4 IpdbProcDeleteBindingEntry PROTO ((UINT4, tMacAddr, tVlanId, 
                                               UINT4, UINT4, UINT1));

PUBLIC INT4 IpdbProcChangeStaticEntry PROTO ((UINT4, tMacAddr, 
                                              tVlanId, UINT1));

PUBLIC INT4 IpdbProcUpdateBindingEntry PROTO ((tIpDbEntry *, UINT4, 
                                               tIpDbGateway *));

PUBLIC INT4 IpdbProcDeleteEntries PROTO ((UINT1, UINT4, tVlanId));

/*ipdbarp.c*/
PUBLIC INT4 IpdbPortGetEtherHdrLen PROTO ((tCRU_BUF_CHAIN_HEADER *, UINT4,
                                           UINT2 *));
PUBLIC INT4 IpDbGetPortCtrlEntry PROTO ((UINT4, tPortCtrlEntry **));

PUBLIC INT4 IpdbHandleArpPacket PROTO ((tCRU_BUF_CHAIN_HEADER *, UINT4, 
                                        UINT4, tVlanTag));

PUBLIC INT4 IpdbExtractArpHeader PROTO ((UINT1 *, tIpDbArpInfo *));

PUBLIC INT4 IpDbArpValidateIncomingPacket PROTO ((tIpDbArpInfo *, UINT4, UINT1,
                                                  UINT4, UINT1, tVlanId, 
                                                  tPortList));

PUBLIC INT4 IpDbGetVlanCtrlEntry PROTO ((tVlanId, tVlanCtrlEntry **));
    
/* ipdbcli.c */
PUBLIC INT4 IpdbCliStaticBindingAdd PROTO ((tCliHandle, UINT4, UINT1 *, 
                                            INT4, UINT4, UINT4));
PUBLIC INT4 IpdbCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel); 

PUBLIC INT4 IpdbCliDeleteStaticBindingEntry PROTO ((tCliHandle, INT4, 
                                                    UINT1 *));

PUBLIC INT4 IpdbCliShowBindingDatabase PROTO ((tCliHandle, UINT1 *, 
                                               INT4, INT4));

PUBLIC INT4 IpdbCliPrintBindingInfo PROTO ((tCliHandle, INT4, tMacAddr, 
                                            INT4, UINT4 *));

PUBLIC INT4 IpdbShowRunningConfig PROTO ((tCliHandle, UINT4, UINT4));

PUBLIC INT4 IpdbShowRunningConfigGetId PROTO ((tCliHandle));

PUBLIC INT4 IpdbShowRunningConfigDetails PROTO ((tCliHandle, INT4, tMacAddr));

PUBLIC INT4 IpdbCliSetArpInfo PROTO ((tCliHandle, UINT4, tVlanId, 
                                      UINT1, UINT1));

PUBLIC INT4 IpdbCliShowArpSpoofing PROTO ((tCliHandle, UINT4));

PUBLIC INT4 IpdbCliShowMacForceFwd PROTO ((tCliHandle, INT4));

PUBLIC INT4 IpdbShowRunningConfigIntfDetails PROTO ((tCliHandle, UINT4));

PUBLIC INT4 IpdbShowRunningConfigVlanDetails PROTO ((tCliHandle, UINT4));

PUBLIC INT4 IpdbCliSetTrace PROTO ((INT4, UINT1));

PUBLIC INT4 IpdbCliShowBindingCount PROTO ((tCliHandle, UINT1 *, INT4, INT4));

PUBLIC INT4 IpdbCliShowGlobalBindingCount PROTO ((tCliHandle));

PUBLIC INT4 IpdbCliPrintVlanBinding PROTO ((tCliHandle, INT4, INT4, UINT4 *));

PUBLIC INT4 IpdbCliSetIpSrcGuardStatus PROTO ((tCliHandle, UINT4, INT4));

PUBLIC INT4 IpdbCliShowIpSrcGuardStatus PROTO ((tCliHandle, UINT4));

PUBLIC INT4 IpdbCliShowIpsgStatus PROTO ((tCliHandle, UINT4));

PUBLIC INT4 IpDbCliSelectContextOnMode PROTO ((tCliHandle, UINT4, UINT4 *));

PUBLIC INT4 IpDbVcmIsSwitchExist PROTO ((UINT1 *pu1Alias, UINT4 *pu4VcNum));

PUBLIC VOID IpdbPrintContextInfo PROTO ((tCliHandle, INT4));

/* ipdbstub.c */
PUBLIC INT4 DcsApiAddPortEntry PROTO ((UINT4));

PUBLIC INT4 DcsApiDeletePortEntry PROTO ((UINT4));

#endif /*  _IPDBPROT_H */
