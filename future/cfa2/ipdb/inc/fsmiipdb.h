/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmiipdb.h,v 1.1 2010/07/31 10:53:43 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMIIPDB_H
#define _FSMIIPDB_H

UINT1 FsMIIpDbStaticBindingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsMIIpDbBindingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsMIIpDbGatewayIpTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIIpDbInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIIpDbSrcGuardConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fsmiip [] ={1,3,6,1,4,1,29601,2,48};
tSNMP_OID_TYPE fsmiipOID = {9, fsmiip};


UINT4 FsMIIpDbNoOfBindings [ ] ={1,3,6,1,4,1,29601,2,48,1,1};
UINT4 FsMIIpDbNoOfStaticBindings [ ] ={1,3,6,1,4,1,29601,2,48,1,2};
UINT4 FsMIIpDbNoOfDHCPBindings [ ] ={1,3,6,1,4,1,29601,2,48,1,3};
UINT4 FsMIIpDbNoOfPPPBindings [ ] ={1,3,6,1,4,1,29601,2,48,1,4};
UINT4 FsMIIpDbTraceLevel [ ] ={1,3,6,1,4,1,29601,2,48,1,5};
UINT4 FsMIIpDbContextId [ ] ={1,3,6,1,4,1,29601,2,48,2,1,1,1};
UINT4 FsMIIpDbStaticHostVlanId [ ] ={1,3,6,1,4,1,29601,2,48,2,1,1,2};
UINT4 FsMIIpDbStaticHostMac [ ] ={1,3,6,1,4,1,29601,2,48,2,1,1,3};
UINT4 FsMIIpDbStaticHostIp [ ] ={1,3,6,1,4,1,29601,2,48,2,1,1,4};
UINT4 FsMIIpDbStaticInIfIndex [ ] ={1,3,6,1,4,1,29601,2,48,2,1,1,5};
UINT4 FsMIIpDbStaticGateway [ ] ={1,3,6,1,4,1,29601,2,48,2,1,1,6};
UINT4 FsMIIpDbStaticBindingStatus [ ] ={1,3,6,1,4,1,29601,2,48,2,1,1,7};
UINT4 FsMIIpDbHostContextId [ ] ={1,3,6,1,4,1,29601,2,48,3,1,1,1};
UINT4 FsMIIpDbHostVlanId [ ] ={1,3,6,1,4,1,29601,2,48,3,1,1,2};
UINT4 FsMIIpDbHostMac [ ] ={1,3,6,1,4,1,29601,2,48,3,1,1,3};
UINT4 FsMIIpDbHostBindingType [ ] ={1,3,6,1,4,1,29601,2,48,3,1,1,4};
UINT4 FsMIIpDbHostIp [ ] ={1,3,6,1,4,1,29601,2,48,3,1,1,5};
UINT4 FsMIIpDbHostInIfIndex [ ] ={1,3,6,1,4,1,29601,2,48,3,1,1,6};
UINT4 FsMIIpDbHostRemLeaseTime [ ] ={1,3,6,1,4,1,29601,2,48,3,1,1,7};
UINT4 FsMIIpDbHostBindingID [ ] ={1,3,6,1,4,1,29601,2,48,3,1,1,8};
UINT4 FsMIIpDbGatewayNetwork [ ] ={1,3,6,1,4,1,29601,2,48,3,2,1,1};
UINT4 FsMIIpDbGatewayNetMask [ ] ={1,3,6,1,4,1,29601,2,48,3,2,1,2};
UINT4 FsMIIpDbGatewayIp [ ] ={1,3,6,1,4,1,29601,2,48,3,2,1,3};
UINT4 FsMIIpDbGatewayIpMode [ ] ={1,3,6,1,4,1,29601,2,48,3,2,1,4};
UINT4 FsMIIpDbIntfContextId [ ] ={1,3,6,1,4,1,29601,2,48,4,1,1,1};
UINT4 FsMIIpDbIntfVlanId [ ] ={1,3,6,1,4,1,29601,2,48,4,1,1,2};
UINT4 FsMIIpDbIntfNoOfVlanBindings [ ] ={1,3,6,1,4,1,29601,2,48,4,1,1,3};
UINT4 FsMIIpDbIntfNoOfVlanStaticBindings [ ] ={1,3,6,1,4,1,29601,2,48,4,1,1,4};
UINT4 FsMIIpDbIntfNoOfVlanDHCPBindings [ ] ={1,3,6,1,4,1,29601,2,48,4,1,1,5};
UINT4 FsMIIpDbIntfNoOfVlanPPPBindings [ ] ={1,3,6,1,4,1,29601,2,48,4,1,1,6};
UINT4 FsMIIpDbSrcGuardIndex [ ] ={1,3,6,1,4,1,29601,2,48,5,1,1,1};
UINT4 FsMIIpDbSrcGuardStatus [ ] ={1,3,6,1,4,1,29601,2,48,5,1,1,2};




tMbDbEntry fsmiipMibEntry[]= {

{{11,FsMIIpDbNoOfBindings}, NULL, FsMIIpDbNoOfBindingsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsMIIpDbNoOfStaticBindings}, NULL, FsMIIpDbNoOfStaticBindingsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsMIIpDbNoOfDHCPBindings}, NULL, FsMIIpDbNoOfDHCPBindingsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsMIIpDbNoOfPPPBindings}, NULL, FsMIIpDbNoOfPPPBindingsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsMIIpDbTraceLevel}, NULL, FsMIIpDbTraceLevelGet, FsMIIpDbTraceLevelSet, FsMIIpDbTraceLevelTest, FsMIIpDbTraceLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,FsMIIpDbContextId}, GetNextIndexFsMIIpDbStaticBindingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIIpDbStaticBindingTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpDbStaticHostVlanId}, GetNextIndexFsMIIpDbStaticBindingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIIpDbStaticBindingTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpDbStaticHostMac}, GetNextIndexFsMIIpDbStaticBindingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsMIIpDbStaticBindingTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpDbStaticHostIp}, GetNextIndexFsMIIpDbStaticBindingTable, FsMIIpDbStaticHostIpGet, FsMIIpDbStaticHostIpSet, FsMIIpDbStaticHostIpTest, FsMIIpDbStaticBindingTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsMIIpDbStaticBindingTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpDbStaticInIfIndex}, GetNextIndexFsMIIpDbStaticBindingTable, FsMIIpDbStaticInIfIndexGet, FsMIIpDbStaticInIfIndexSet, FsMIIpDbStaticInIfIndexTest, FsMIIpDbStaticBindingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIIpDbStaticBindingTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpDbStaticGateway}, GetNextIndexFsMIIpDbStaticBindingTable, FsMIIpDbStaticGatewayGet, FsMIIpDbStaticGatewaySet, FsMIIpDbStaticGatewayTest, FsMIIpDbStaticBindingTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsMIIpDbStaticBindingTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpDbStaticBindingStatus}, GetNextIndexFsMIIpDbStaticBindingTable, FsMIIpDbStaticBindingStatusGet, FsMIIpDbStaticBindingStatusSet, FsMIIpDbStaticBindingStatusTest, FsMIIpDbStaticBindingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpDbStaticBindingTableINDEX, 3, 0, 1, NULL},

{{13,FsMIIpDbHostContextId}, GetNextIndexFsMIIpDbBindingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIIpDbBindingTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpDbHostVlanId}, GetNextIndexFsMIIpDbBindingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIIpDbBindingTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpDbHostMac}, GetNextIndexFsMIIpDbBindingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsMIIpDbBindingTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpDbHostBindingType}, GetNextIndexFsMIIpDbBindingTable, FsMIIpDbHostBindingTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIIpDbBindingTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpDbHostIp}, GetNextIndexFsMIIpDbBindingTable, FsMIIpDbHostIpGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsMIIpDbBindingTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpDbHostInIfIndex}, GetNextIndexFsMIIpDbBindingTable, FsMIIpDbHostInIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpDbBindingTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpDbHostRemLeaseTime}, GetNextIndexFsMIIpDbBindingTable, FsMIIpDbHostRemLeaseTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIIpDbBindingTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpDbHostBindingID}, GetNextIndexFsMIIpDbBindingTable, FsMIIpDbHostBindingIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIIpDbBindingTableINDEX, 3, 0, 0, NULL},

{{13,FsMIIpDbGatewayNetwork}, GetNextIndexFsMIIpDbGatewayIpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIIpDbGatewayIpTableINDEX, 6, 0, 0, NULL},

{{13,FsMIIpDbGatewayNetMask}, GetNextIndexFsMIIpDbGatewayIpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIIpDbGatewayIpTableINDEX, 6, 0, 0, NULL},

{{13,FsMIIpDbGatewayIp}, GetNextIndexFsMIIpDbGatewayIpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIIpDbGatewayIpTableINDEX, 6, 0, 0, NULL},

{{13,FsMIIpDbGatewayIpMode}, GetNextIndexFsMIIpDbGatewayIpTable, FsMIIpDbGatewayIpModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIIpDbGatewayIpTableINDEX, 6, 0, 0, NULL},

{{13,FsMIIpDbIntfContextId}, GetNextIndexFsMIIpDbInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIIpDbInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMIIpDbIntfVlanId}, GetNextIndexFsMIIpDbInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIIpDbInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMIIpDbIntfNoOfVlanBindings}, GetNextIndexFsMIIpDbInterfaceTable, FsMIIpDbIntfNoOfVlanBindingsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpDbInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMIIpDbIntfNoOfVlanStaticBindings}, GetNextIndexFsMIIpDbInterfaceTable, FsMIIpDbIntfNoOfVlanStaticBindingsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpDbInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMIIpDbIntfNoOfVlanDHCPBindings}, GetNextIndexFsMIIpDbInterfaceTable, FsMIIpDbIntfNoOfVlanDHCPBindingsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpDbInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMIIpDbIntfNoOfVlanPPPBindings}, GetNextIndexFsMIIpDbInterfaceTable, FsMIIpDbIntfNoOfVlanPPPBindingsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIIpDbInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMIIpDbSrcGuardIndex}, GetNextIndexFsMIIpDbSrcGuardConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIIpDbSrcGuardConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsMIIpDbSrcGuardStatus}, GetNextIndexFsMIIpDbSrcGuardConfigTable, FsMIIpDbSrcGuardStatusGet, FsMIIpDbSrcGuardStatusSet, FsMIIpDbSrcGuardStatusTest, FsMIIpDbSrcGuardConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIIpDbSrcGuardConfigTableINDEX, 1, 0, 0, "1"},
};
tMibData fsmiipEntry = { 32, fsmiipMibEntry };

#endif /* _FSMIIPDB_H */

