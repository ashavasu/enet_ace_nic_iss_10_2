#ifndef _IPDBTDFS_H
#define _IPDBTDFS_H
/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: ipdbtdfs.h,v 1.9 2013/12/07 10:53:49 siva Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : ipdbtdfs.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : IP Binding Management                          */
/*    MODULE NAME           : IP binding Data Structures                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains Data Structures             */
/*                            for IP binding management module               */
/*---------------------------------------------------------------------------*/


/* Global Statistics */
typedef struct _IpDbGlobalStats
{
    UINT4  u4NoOfBindings;       /* Total number of (host, IP) bindings, 
                                    accross all VLANS */
    UINT4  u4NoOfStaticBindings; /* Total number of static (host, IP) bindings,
                                    accross all VLANS */
    UINT4  u4NoOfDHCPBindings;   /* Total number of (host, IP) bindings, conf-
                                    igured through DHCP accross all VLANS */
    UINT4  u4NoOfPPPBindings;    /* Total number of (host, IP) bindings, conf-
                                    igured through PPP accross all VLANS */
} tIpDbGlobalStats;


/* Global informations */
typedef struct _IpDbGlobalInfo
{
    tRBTree       StaticBindingEntry;  /* RBTree for Static Binding Entry */
    tRBTree       DbMacBindingEntry;   /* RBTree based on MAC and VLAN for 
                                          Binding Database */
    tRBTree       DbIpBindingEntry;    /* RBTree based on IP address for 
                                          Binding Database */
    tRBTree       InterfaceEntry;      /* RBTree for interface entries */
    tRBTree       PortCtrlEntry;       /* RBTree for the PortCtrl table*/
    tTMO_DLL      VlanCtrlDllList;     /* Dll list for the PortCtrl table*/
    tTimerListId  IpDbTmrListId;       /* Timer List ID */
    tMemPoolId    BindingEntryPoolId;  /* Memory pool ID for Binding entries */
    tMemPoolId    StaticEntryPoolId;   /* Memory pool ID for static binding
                                          entries */
    tMemPoolId    IntfEntryMemPoolId;  /* Memory pool for interface entries */
    tMemPoolId    GwEntryMemPoolId;    /* Memory pool for Gateway entries */
    tMemPoolId    QMemPoolId;          /* Memory pool for Queue */
    tMemPoolId    PortCtrlMemPoolId;   /* Memory pool for the PortCtrl entry */
    tMemPoolId    VlanCtrlMemPoolId;   /* Memory pool for the VlanCtrl entry */
    tOsixSemId    IpDbSemId;           /* Semapore for the module */
    tOsixQId      IpDbQId;             /* Id of Queue */ 
    tPortList     IpsgStatusPortList;  /* Port list which maintains the IPSG 
                                          status for all the ports. 
                                          If bit is set, IPSG status is enabled
                                          with mode as ip or ip-mac. 
                                          If bit is not set, IPSG status is 
                                          disabled */
    UINT4         u4DebugFlag;         /* Flag used for Trace */
    UINT4         u4DebugLvl;         /* Flag used for Trace */ 
    UINT4         u4CurrContextId;     /* Current Context ID */
    UINT4         u4IpsgStatusRefCount;/* Reference count which indicates the 
                                          number of ports for which IPSG 
                                          status is enabled */
} tIpDbGlobalInfo;


/* Timer entry */
typedef struct _IpDbTimerEntry
{
    tTmrBlk       TmrBlk;           /* FSAP timer block */
    UINT1         u1Status;         /* Status of the timer */
    UINT1         u1Reserved[3];    /* Padding */
} tIpDbTimerEntry;


/* Static host IP Binding Database */
typedef struct _IpDbStaticBindingEntry
{
    tRBNodeEmbd   StaticBindingNode;
    tMacAddr      HostMac;            /* Mac address of the host */
    tVlanId       VlanId;             /* VLAN to which the host belongs */
    UINT4         u4HostIp;           /* IP address assigned to the host */
    UINT4         u4InIfIndex;        /* The Port through which the host is
                                         connected */    
    UINT4         u4GatewayIp;        /* Gateway Ip of the Host */
    UINT4         u4ContextId;        /* Context ID */
    UINT1         u1RowStatus;        /* Row status */
    UINT1         u1Reserved[3];      /* Padding */
} tIpDbStaticBindingEntry;
            

/* (IP, Host) binding Database */
typedef struct _IpDbBindingEntry
{
    tRBNodeEmbd         BindingMacRbNode; /* Rbtree based on VLAN, MAC */
    tRBNodeEmbd         BindingIpRbNode;  /* Rbtree based on IpAddress */
    tMacAddr            HostMac;          /* Mac address of the host */
    tVlanId             VlanId;           /* VLAN to which the host belongs */
    tTMO_SLL            NetworkList;      /* SLL corresponding to the Gateways,
                                             that this host have access to */
    tIpDbTimerEntry     LeaseTimerNode;   /* Lease time(DHCP)/Idle timer(PPP)*/
    UINT4               u4HostIp;         /* IP address assigned to the host */
    UINT4               u4InIfIndex;      /* The Port through which the host is
                                             connected */
    UINT4               u4BindingId;      /* An integer value identifying the 
                                             binding */
    UINT4               u4ContextId;      /* Context ID */
    UINT4               u4L2FilterId;     /* L2 Filter Identifier */
    UINT4               u4L3FilterId;     /* L3 Filter Identifier */
    UINT4               u4UDBFilterId;    /* User defined Filter Identifier */
    UINT1               u1BindingType;    /* Binding type (static/DHCP/PPP) */
    UINT1               u1Reserved[3];    /* Padding */
} tIpDbBindingEntry;


/* Gateway/AR entries; */
typedef struct _IpDbGatewayEntry
{
    tTMO_SLL_NODE GatewayNode;
    UINT4         u4Network;      /* The network for which the gateway
                                     is given */
    UINT4         u4NetMask;      /* The mask for the network to which
                                     the gateway is given */
    UINT4         u4GatewayIp;    /* Gateway Ip */
} tIpDbGatewayEntry;


/* Layer 2 VLAN Entry */
typedef struct _IpDbIfaceEntry
{
    tRBNodeEmbd   VlanRbNode;
    UINT4         u4NoOfBindings;       /* Number of (host, IP) bindings */
    UINT4         u4NoOfStaticBindings; /* Number of static (host, IP) 
                                           bindings */
    UINT4         u4NoOfDHCPBindings;   /* Number of (host, IP) bindings, 
                                           configured through DHCP */
    UINT4         u4NoOfPPPBindings;    /* Number of (host, IP) bindings, 
                                           configured  through PPP */
    UINT4         u4ContextId;          /* Context ID */
    tVlanId       VlanId;               /* VLAN for which this entry is 
                                           created */
    UINT1         u2Reserved[2];        /* Padding */  
} tIpDbIfaceEntry;


/* Structure for handling incoming events */
typedef struct _IpDbQMsg
{
    tCRU_BUF_CHAIN_HEADER   *pInQMsg;     /* Packet received */
    UINT4                   u4EventType;  /* Type of the event */
    tVlanTag                VlanTag;      /* Classified VLAN Tag for the 
                                             received packets */
    UINT4                   u4InPort;     /* Incoming port */
    UINT4                   u4ContextId;  /* Context ID */
    tVlanId                 VlanId;       /* VlanId - Used for notifying VLAN
                                             creation/deletion */
    UINT1             au1Padding[2];
} tIpDbQMsg;


/* Data Structures Which Contains the MIB Objects for Port */

typedef struct _PortCtrlEntry
{
    tRBNodeEmbd       PortCtrlRBNode;   /* Port control RBtree Node */
    UINT4             u4IfIndex;      /* Unique id for Port */
    UINT4             u4ArpPktsForwarded;   /*No of ARP packets forwarded*/
    UINT4             u4ArpPktsDropped;   /* No of ARP packets dropped */
    UINT4             u4UDBFilterId;      /*Unique id for userdefined filters*/
    UINT1             u1DownstreamArpBcastStatus;  /* Forwarding of Downstream
                                                      Arp-Bcast status */ 
    UINT1             u1IpsgStatus;      /* IP source guard status
                                          Possible Values are 
                                          1. IPDB_IPDG_DISABLE - To disable 
                                          2. IPDB_IPSG_IP_MODE - Ip based mode
                                          3. IPDB_IPSG_IP_MAC_MODE - Ip & mac 
                                                                    based mode */
    UINT1             au1Padding[2];
}tPortCtrlEntry;

/* Data Structures Which Contains the MIB Objects for Vlan*/

typedef struct _VlanCtrlEntry
{
    tTMO_DLL_NODE     VlanCtrlDllNode;   /* Port DLL Node */
    tVlanId           VlanId;            /* Unique id for Vlan */
    UINT1             u1MacForceFwdStatus; /* Mac force forwarding status */
    UINT1             u1RowStatus;    /* Row status */

}tVlanCtrlEntry;

typedef struct _IpDbArpInfo
{
    UINT4              u4SrcIp;   /* Src IP in the incoming ARP packet*/
    UINT4              u4DestIp;  /* Dest IP in the incoming ARP packet*/
    tMacAddr           SrcMac;    /* Src MAC in the incoming ARP packet*/
    tMacAddr           DestMac;   /* Dest MAC in the incoming ARP packet*/
    UINT2              u2Opcode;  /* Opcode in the incoming ARP packet*/
    UINT1              u2Reserved[2];  /* Padding */  
}tIpDbArpInfo; 

#endif /* _IPDBTDFS_H */
