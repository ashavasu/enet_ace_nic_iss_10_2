/* $Id: fsmiipnc.h,v 1.1 2016/06/18 11:43:28 siva Exp $
    ISS Wrapper header
    module ARICENT-MIIPDB-MIB

 */
#ifndef _H_i_ARICENT_MIIPDB_MIB
#define _H_i_ARICENT_MIIPDB_MIB
#define VXLAN_INET_HTONL(GrpIp1)\
{\
        UINT4   u4TmpGrpAddr = 0;\
        UINT4   u4Count1 = 0;\
        UINT1   u1Index = 0;\
        UINT1   au1TmpGrpIp[IPVX_MAX_INET_ADDR_LEN];\
          \
        MEMSET (au1TmpGrpIp, 0, IPVX_MAX_INET_ADDR_LEN);\
        \
        for (u4Count1 = 0, u1Index = 0; u4Count1 < IPVX_MAX_INET_ADDR_LEN; \
                         u1Index++, u4Count1 = u4Count1 + 4)\
        {\
               MEMCPY (&u4TmpGrpAddr, (GrpIp1 + u4Count1), sizeof(UINT4));\
                    u4TmpGrpAddr = (UINT4 )OSIX_HTONL (u4TmpGrpAddr);\
               MEMCPY (&(au1TmpGrpIp[u4Count1]), &u4TmpGrpAddr, sizeof(UINT4));\
                }\
        MEMCPY (GrpIp1, au1TmpGrpIp, IPVX_MAX_INET_ADDR_LEN);\
}

/********************************************************************
* FUNCTION NcFsMIIpDbNoOfBindingsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbNoOfBindingsGet (
                UINT4 *pu4FsMIIpDbNoOfBindings );

/********************************************************************
* FUNCTION NcFsMIIpDbNoOfStaticBindingsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbNoOfStaticBindingsGet (
                UINT4 *pu4FsMIIpDbNoOfStaticBindings );

/********************************************************************
* FUNCTION NcFsMIIpDbNoOfDHCPBindingsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbNoOfDHCPBindingsGet (
                UINT4 *pu4FsMIIpDbNoOfDHCPBindings );

/********************************************************************
* FUNCTION NcFsMIIpDbNoOfPPPBindingsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbNoOfPPPBindingsGet (
                UINT4 *pu4FsMIIpDbNoOfPPPBindings );

/********************************************************************
* FUNCTION NcFsMIIpDbTraceLevelSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbTraceLevelSet (
                INT4 i4FsMIIpDbTraceLevel );

/********************************************************************
* FUNCTION NcFsMIIpDbTraceLevelTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbTraceLevelTest (UINT4 *pu4Error,
                INT4 i4FsMIIpDbTraceLevel );

/********************************************************************
* FUNCTION NcFsMIIpDbStaticHostIpSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbStaticHostIpSet (
                INT4 i4FsMIIpDbContextId,
                INT4 i4FsMIIpDbStaticHostVlanId,
                UINT1 *pFsMIIpDbStaticHostMac,
                UINT4 pFsMIIpDbStaticHostIp );

/********************************************************************
* FUNCTION NcFsMIIpDbStaticHostIpTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbStaticHostIpTest (UINT4 *pu4Error,
                INT4 i4FsMIIpDbContextId,
                INT4 i4FsMIIpDbStaticHostVlanId,
                UINT1 *pFsMIIpDbStaticHostMac,
                UINT4 pFsMIIpDbStaticHostIp );

/********************************************************************
* FUNCTION NcFsMIIpDbStaticInIfIndexSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbStaticInIfIndexSet (
                INT4 i4FsMIIpDbContextId,
                INT4 i4FsMIIpDbStaticHostVlanId,
                UINT1 *pFsMIIpDbStaticHostMac,
                INT4 i4FsMIIpDbStaticInIfIndex );

/********************************************************************
* FUNCTION NcFsMIIpDbStaticInIfIndexTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbStaticInIfIndexTest (UINT4 *pu4Error,
                INT4 i4FsMIIpDbContextId,
                INT4 i4FsMIIpDbStaticHostVlanId,
                UINT1 *pFsMIIpDbStaticHostMac,
                INT4 i4FsMIIpDbStaticInIfIndex );

/********************************************************************
* FUNCTION NcFsMIIpDbStaticGatewaySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbStaticGatewaySet (
                INT4 i4FsMIIpDbContextId,
                INT4 i4FsMIIpDbStaticHostVlanId,
                UINT1 *pFsMIIpDbStaticHostMac,
                UINT4 pFsMIIpDbStaticGateway );

/********************************************************************
* FUNCTION NcFsMIIpDbStaticGatewayTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbStaticGatewayTest (UINT4 *pu4Error,
                INT4 i4FsMIIpDbContextId,
                INT4 i4FsMIIpDbStaticHostVlanId,
                UINT1 *pFsMIIpDbStaticHostMac,
                UINT4 pFsMIIpDbStaticGateway );

/********************************************************************
* FUNCTION NcFsMIIpDbStaticBindingStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbStaticBindingStatusSet (
                INT4 i4FsMIIpDbContextId,
                INT4 i4FsMIIpDbStaticHostVlanId,
                UINT1 *pFsMIIpDbStaticHostMac,
                INT4 i4FsMIIpDbStaticBindingStatus );

/********************************************************************
* FUNCTION NcFsMIIpDbStaticBindingStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbStaticBindingStatusTest (UINT4 *pu4Error,
                INT4 i4FsMIIpDbContextId,
                INT4 i4FsMIIpDbStaticHostVlanId,
                UINT1 *pFsMIIpDbStaticHostMac,
                INT4 i4FsMIIpDbStaticBindingStatus );

/********************************************************************
* FUNCTION NcFsMIIpDbHostBindingTypeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbHostBindingTypeGet (
                INT4 i4FsMIIpDbHostContextId,
                INT4 i4FsMIIpDbHostVlanId,
                UINT1 *pFsMIIpDbHostMac,
                INT4 *pi4FsMIIpDbHostBindingType );

/********************************************************************
* FUNCTION NcFsMIIpDbHostIpGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbHostIpGet (
                INT4 i4FsMIIpDbHostContextId,
                INT4 i4FsMIIpDbHostVlanId,
                UINT1 *pFsMIIpDbHostMac,
                UINT4 *pFsMIIpDbHostIp );

/********************************************************************
* FUNCTION NcFsMIIpDbHostInIfIndexGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbHostInIfIndexGet (
                INT4 i4FsMIIpDbHostContextId,
                INT4 i4FsMIIpDbHostVlanId,
                UINT1 *pFsMIIpDbHostMac,
                INT4 *pi4FsMIIpDbHostInIfIndex );

/********************************************************************
* FUNCTION NcFsMIIpDbHostRemLeaseTimeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbHostRemLeaseTimeGet (
                INT4 i4FsMIIpDbHostContextId,
                INT4 i4FsMIIpDbHostVlanId,
                UINT1 *pFsMIIpDbHostMac,
                INT4 *pi4FsMIIpDbHostRemLeaseTime );

/********************************************************************
* FUNCTION NcFsMIIpDbHostBindingIDGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbHostBindingIDGet (
                INT4 i4FsMIIpDbHostContextId,
                INT4 i4FsMIIpDbHostVlanId,
                UINT1 *pFsMIIpDbHostMac,
                UINT4 *pu4FsMIIpDbHostBindingID );

/********************************************************************
* FUNCTION NcFsMIIpDbGatewayIpModeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbGatewayIpModeGet (
                INT4 i4FsMIIpDbHostContextId,
                UINT1 *FsMIIpDbHostMac,
                INT4 i4FsMIIpDbHostVlanId,
                UINT4 pFsMIIpDbGatewayNetwork,
                UINT4 pFsMIIpDbGatewayNetMask,
                UINT4 pFsMIIpDbGatewayIp,
                INT4 *pi4FsMIIpDbGatewayIpMode );

/********************************************************************
* FUNCTION NcFsMIIpDbIntfNoOfVlanBindingsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbIntfNoOfVlanBindingsGet (
                INT4 i4FsMIIpDbIntfContextId,
                INT4 i4FsMIIpDbIntfVlanId,
                UINT4 *pu4FsMIIpDbIntfNoOfVlanBindings );

/********************************************************************
* FUNCTION NcFsMIIpDbIntfNoOfVlanStaticBindingsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbIntfNoOfVlanStaticBindingsGet (
                INT4 i4FsMIIpDbIntfContextId,
                INT4 i4FsMIIpDbIntfVlanId,
                UINT4 *pu4FsMIIpDbIntfNoOfVlanStaticBindings );

/********************************************************************
* FUNCTION NcFsMIIpDbIntfNoOfVlanDHCPBindingsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbIntfNoOfVlanDHCPBindingsGet (
                INT4 i4FsMIIpDbIntfContextId,
                INT4 i4FsMIIpDbIntfVlanId,
                UINT4 *pu4FsMIIpDbIntfNoOfVlanDHCPBindings );

/********************************************************************
* FUNCTION NcFsMIIpDbIntfNoOfVlanPPPBindingsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbIntfNoOfVlanPPPBindingsGet (
                INT4 i4FsMIIpDbIntfContextId,
                INT4 i4FsMIIpDbIntfVlanId,
                UINT4 *pu4FsMIIpDbIntfNoOfVlanPPPBindings );

/********************************************************************
* FUNCTION NcFsMIIpDbSrcGuardStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbSrcGuardStatusSet (
                INT4 i4FsMIIpDbSrcGuardIndex,
                INT4 i4FsMIIpDbSrcGuardStatus );

/********************************************************************
* FUNCTION NcFsMIIpDbSrcGuardStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIIpDbSrcGuardStatusTest (UINT4 *pu4Error,
                INT4 i4FsMIIpDbSrcGuardIndex,
                INT4 i4FsMIIpDbSrcGuardStatus );

/* END i_ARICENT_MIIPDB_MIB.c */



#endif
