/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmiiplw.h,v 1.1 2010/07/31 10:53:43 prabuc Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpDbNoOfBindings ARG_LIST((UINT4 *));

INT1
nmhGetFsMIIpDbNoOfStaticBindings ARG_LIST((UINT4 *));

INT1
nmhGetFsMIIpDbNoOfDHCPBindings ARG_LIST((UINT4 *));

INT1
nmhGetFsMIIpDbNoOfPPPBindings ARG_LIST((UINT4 *));

INT1
nmhGetFsMIIpDbTraceLevel ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIIpDbTraceLevel ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIIpDbTraceLevel ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIIpDbTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIIpDbStaticBindingTable. */
INT1
nmhValidateIndexInstanceFsMIIpDbStaticBindingTable ARG_LIST((INT4  , INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsMIIpDbStaticBindingTable  */

INT1
nmhGetFirstIndexFsMIIpDbStaticBindingTable ARG_LIST((INT4 * , INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIIpDbStaticBindingTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpDbStaticHostIp ARG_LIST((INT4  , INT4  , tMacAddr ,UINT4 *));

INT1
nmhGetFsMIIpDbStaticInIfIndex ARG_LIST((INT4  , INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsMIIpDbStaticGateway ARG_LIST((INT4  , INT4  , tMacAddr ,UINT4 *));

INT1
nmhGetFsMIIpDbStaticBindingStatus ARG_LIST((INT4  , INT4  , tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIIpDbStaticHostIp ARG_LIST((INT4  , INT4  , tMacAddr  ,UINT4 ));

INT1
nmhSetFsMIIpDbStaticInIfIndex ARG_LIST((INT4  , INT4  , tMacAddr  ,INT4 ));

INT1
nmhSetFsMIIpDbStaticGateway ARG_LIST((INT4  , INT4  , tMacAddr  ,UINT4 ));

INT1
nmhSetFsMIIpDbStaticBindingStatus ARG_LIST((INT4  , INT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIIpDbStaticHostIp ARG_LIST((UINT4 *  ,INT4  , INT4  , tMacAddr  ,UINT4 ));

INT1
nmhTestv2FsMIIpDbStaticInIfIndex ARG_LIST((UINT4 *  ,INT4  , INT4  , tMacAddr  ,INT4 ));

INT1
nmhTestv2FsMIIpDbStaticGateway ARG_LIST((UINT4 *  ,INT4  , INT4  , tMacAddr  ,UINT4 ));

INT1
nmhTestv2FsMIIpDbStaticBindingStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIIpDbStaticBindingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIIpDbBindingTable. */
INT1
nmhValidateIndexInstanceFsMIIpDbBindingTable ARG_LIST((INT4  , INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsMIIpDbBindingTable  */

INT1
nmhGetFirstIndexFsMIIpDbBindingTable ARG_LIST((INT4 * , INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIIpDbBindingTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpDbHostBindingType ARG_LIST((INT4  , INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsMIIpDbHostIp ARG_LIST((INT4  , INT4  , tMacAddr ,UINT4 *));

INT1
nmhGetFsMIIpDbHostInIfIndex ARG_LIST((INT4  , INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsMIIpDbHostRemLeaseTime ARG_LIST((INT4  , INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsMIIpDbHostBindingID ARG_LIST((INT4  , INT4  , tMacAddr ,UINT4 *));

/* Proto Validate Index Instance for FsMIIpDbGatewayIpTable. */
INT1
nmhValidateIndexInstanceFsMIIpDbGatewayIpTable ARG_LIST((INT4  , tMacAddr  , INT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIIpDbGatewayIpTable  */

INT1
nmhGetFirstIndexFsMIIpDbGatewayIpTable ARG_LIST((INT4 * , tMacAddr *  , INT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIIpDbGatewayIpTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpDbGatewayIpMode ARG_LIST((INT4  , tMacAddr  , INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIIpDbInterfaceTable. */
INT1
nmhValidateIndexInstanceFsMIIpDbInterfaceTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIIpDbInterfaceTable  */

INT1
nmhGetFirstIndexFsMIIpDbInterfaceTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIIpDbInterfaceTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpDbIntfNoOfVlanBindings ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIIpDbIntfNoOfVlanStaticBindings ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIIpDbIntfNoOfVlanDHCPBindings ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIIpDbIntfNoOfVlanPPPBindings ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIIpDbSrcGuardConfigTable. */
INT1
nmhValidateIndexInstanceFsMIIpDbSrcGuardConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIIpDbSrcGuardConfigTable  */

INT1
nmhGetFirstIndexFsMIIpDbSrcGuardConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIIpDbSrcGuardConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIIpDbSrcGuardStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIIpDbSrcGuardStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIIpDbSrcGuardStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIIpDbSrcGuardConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
