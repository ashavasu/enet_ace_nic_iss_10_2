#ifndef _FSMIIPWR_H
#define _FSMIIPWR_H

VOID RegisterFSMIIP(VOID);

VOID UnRegisterFSMIIP(VOID);
INT4 FsMIIpDbNoOfBindingsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbNoOfStaticBindingsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbNoOfDHCPBindingsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbNoOfPPPBindingsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbTraceLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsMIIpDbStaticBindingTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIIpDbStaticHostIpGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbStaticInIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbStaticGatewayGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbStaticBindingStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbStaticHostIpSet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbStaticInIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbStaticGatewaySet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbStaticBindingStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbStaticHostIpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbStaticInIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbStaticGatewayTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbStaticBindingStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbStaticBindingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIIpDbBindingTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIIpDbHostBindingTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbHostIpGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbHostInIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbHostRemLeaseTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbHostBindingIDGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIIpDbGatewayIpTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIIpDbGatewayIpModeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIIpDbInterfaceTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIIpDbIntfNoOfVlanBindingsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbIntfNoOfVlanStaticBindingsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbIntfNoOfVlanDHCPBindingsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbIntfNoOfVlanPPPBindingsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIIpDbSrcGuardConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIIpDbSrcGuardStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbSrcGuardStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbSrcGuardStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIIpDbSrcGuardConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSMIIPWR_H */
