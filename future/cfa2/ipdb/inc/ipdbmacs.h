#ifndef _IPDBMACS_H
#define _IPDBMACS_H
/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: ipdbmacs.h,v 1.7 2013/12/07 10:53:49 siva Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : ipdbmacs.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : IP Binding Database management                 */
/*    MODULE NAME           : IP Binding Database management Macros          */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains macro definitions           */
/*                            for IP Binding Database management Module      */
/*---------------------------------------------------------------------------*/

/* Trace related Macro */
#define IPDB_TRC_FLAG           gIpDbGlobalInfo.u4DebugFlag
#define IPDB_TRC_LVL           gIpDbGlobalInfo.u4DebugLvl 
                                               

/* Maximum number of VLAN entries */
#define IPDB_MAX_NUM_VLAN       VLAN_DEV_MAX_NUM_VLAN

#define IPDB_MAX_VLAN_ID        VLAN_DEV_MAX_VLAN_ID
#define IPDB_MIN_VLAN_ID        IPDB_MIN_NUM_VLAN


/* Locks */
#define IPDB_LOCK               IpDbUtilTakeLock
#define IPDB_UNLOCK             IpDbUtilReleaseLock

/* Task, Queue and Event related macros, OSIX related macros */
#define IPDB_TRUE               OSIX_TRUE
#define IPDB_FALSE              OSIX_FALSE

#define IPDB_CREATE_QUEUE       OsixQueCrt
#define IPDB_DELETE_QUEUE       OsixQueDel
#define IPDB_SEND_TO_QUEUE      OsixQueSend
#define IPDB_RECV_FROM_QUEUE    OsixQueRecv
#define IPDB_CREATE_SEM         OsixCreateSem
#define IPDB_DELETE_SEM         OsixDeleteSem
#define IPDB_TAKE_SEM           OsixTakeSem
#define IPDB_GIVE_SEM           OsixGiveSem
#define IPDB_RECEIVE_EVENT      OsixReceiveEvent
#define IPDB_SEND_EVENT         OsixSendEvent

#define IPDB_EVENT_WAIT_FLAG    OSIX_WAIT
#define IPDB_RECEIVE_Q_FLAG     OSIX_NO_WAIT
#define IPDB_TIME_TICKS_PER_SEC SYS_TIME_TICKS_IN_A_SEC

#define IPDB_SEM_ID             gIpDbGlobalInfo.IpDbSemId

#define IPDB_QUEUE_ID           gIpDbGlobalInfo.IpDbQId

/* Timer List ID */
#define IPDB_TIMER_LIST         gIpDbGlobalInfo.IpDbTmrListId

#define IPDB_OFFSET(x, y)    FSAP_OFFSETOF (x,y)

/* Timer offset */
#define IPDB_BINDING_TMR_OFFSET \
    IPDB_OFFSET(tIpDbBindingEntry, LeaseTimerNode)

/* IP based RBTree Offset */
#define IPDB_IP_BIND_RBTREE_OFFSET \
    IPDB_OFFSET(tIpDbBindingEntry, BindingIpRbNode)

/* Mac Based RBTree offset */
#define IPDB_MAC_BIND_RBTREE_OFFSET \
    IPDB_OFFSET(tIpDbBindingEntry, BindingMacRbNode)

/* Rowstatus Macros */
#define IPDB_ACTIVE               ACTIVE
#define IPDB_NOT_IN_SERVICE       NOT_IN_SERVICE
#define IPDB_NOT_READY            NOT_READY
#define IPDB_CREATE_AND_WAIT      CREATE_AND_WAIT
#define IPDB_CREATE_AND_GO        CREATE_AND_GO
#define IPDB_DESTROY              DESTROY


/* Memory related MACRO */
#define IPDB_CREATE_MEM_POOL    MemCreateMemPool

#define IPDB_DELETE_MEM_POOL(PoolId) \
{\
    if (PoolId != 0)\
    {\
        (VOID) MemDeleteMemPool(PoolId);\
    }\
}

#define IPDB_ALLOC_MEM_BLOCK(PoolId)   MemAllocMemBlk(PoolId)

#define IPDB_RELEASE_MEM_BLOCK(PoolId, pu1Msg) \
{\
    MemReleaseMemBlock(PoolId, (UINT1 *) pu1Msg);\
    pu1Msg = NULL;\
}

#define IPDB_STATIC_MEMPOOL_ID  gIpDbGlobalInfo.StaticEntryPoolId
#define IPDB_STATIC_MEMBLK_SIZE sizeof(tIpDbStaticBindingEntry)

#define IPDB_BINDING_MEMPOOL_ID gIpDbGlobalInfo.BindingEntryPoolId
#define IPDB_BINDING_MEMBLK_SIZE \
    sizeof(tIpDbBindingEntry)

#define IPDB_INTF_POOL_ID       gIpDbGlobalInfo.IntfEntryMemPoolId
#define IPDB_INTF_MEMBLK_SIZE   sizeof(tIpDbIfaceEntry)

#define IPDB_GW_POOL_ID         gIpDbGlobalInfo.GwEntryMemPoolId
#define IPDB_GW_MEMBLK_SIZE   sizeof(tIpDbGatewayEntry)

#define IPDB_Q_POOL_ID          gIpDbGlobalInfo.QMemPoolId
#define IPDB_Q_MEMBLK_SIZE      sizeof(tIpDbQMsg)

#define IPDB_PORTCTRL_POOL_ID       gIpDbGlobalInfo.PortCtrlMemPoolId
#define IPDB_PORTCTRL_MEMBLK_SIZE   sizeof(tPortCtrlEntry)

#define IPDB_VLANCTRL_POOL_ID       gIpDbGlobalInfo.VlanCtrlMemPoolId
#define IPDB_VLANCTRL_MEMBLK_SIZE   sizeof(tVlanCtrlEntry)

#define IPDB_MEMORY_TYPE        MEM_DEFAULT_MEMORY_TYPE

/* Total number of gateway entries required */
#define IPDB_TOTAL_GW_ENTRIES   (IPDB_MAX_BINDING_ENTRIES * \
                                 IPDB_MAX_GW_ENTRIES)

/* Total number of PortCtrl entries */
#define IPDB_MAX_PORTCTRL_ENTRIES   SYS_DEF_MAX_PHYSICAL_INTERFACES

/* Total number of VlanCtrl entries */
#define IPDB_MAX_VLANCTRL_ENTRIES   VLAN_DEV_MAX_VLAN_ID

/* RBTree Related Macros */
#define IPDB_INTF_RBTREE        gIpDbGlobalInfo.InterfaceEntry
#define IPDB_STATIC_RBTREE      gIpDbGlobalInfo.StaticBindingEntry
#define IPDB_MAC_BINDING_RBTREE gIpDbGlobalInfo.DbMacBindingEntry
#define IPDB_IP_BINDING_RBTREE  gIpDbGlobalInfo.DbIpBindingEntry

/* DLL list*/
#define IPDB_PORTCTRL_RBTREE    gIpDbGlobalInfo.PortCtrlEntry
#define IPDB_VLANCTRL_DLL_LIST  gIpDbGlobalInfo.VlanCtrlDllList
#define IPDB_CURR_CONTEXT_ID    gIpDbGlobalInfo.u4CurrContextId
#define IPDB_IPSG_PORTLIST      gIpDbGlobalInfo.IpsgStatusPortList
#define IPDB_IPSG_REF_COUNT     gIpDbGlobalInfo.u4IpsgStatusRefCount

#define IPDB_CREATE_RBTREE      RBTreeCreateEmbedded
#define IPDB_DELETE_RBTREE      RBTreeDestroy

/* Macros to get interface statistics */
#define IPDB_INTF_NOOF_BINDINGS(pIpDbIfaceEntry) \
    pIpDbIfaceEntry->u4NoOfBindings

#define IPDB_INTF_NOOF_STAT_BINDS(pIpDbIfaceEntry) \
    pIpDbIfaceEntry->u4NoOfStaticBindings

#define IPDB_INTF_NOOF_DHCP_BINDS(pIpDbIfaceEntry) \
    pIpDbIfaceEntry->u4NoOfDHCPBindings

#define IPDB_INTF_NOOF_PPP_BINDS(pIpDbIfaceEntry) \
        pIpDbIfaceEntry->u4NoOfPPPBindings

/* Macros to get global stats */
#define IPDB_GLOB_NOOF_BINDINGS gIpDbGlobalStats.u4NoOfBindings
#define IPDB_GLOB_NOOF_STAT_BINDINGS gIpDbGlobalStats.u4NoOfStaticBindings
#define IPDB_GLOB_NOOF_DHCP_BINDINGS gIpDbGlobalStats.u4NoOfDHCPBindings
#define IPDB_GLOB_NOOF_PPP_BINDINGS gIpDbGlobalStats.u4NoOfPPPBindings

/* Macros to increment the interface statistics */
#define IPDB_INTF_INCR_BINDINGS(pIpDbIfaceEntry) \
    pIpDbIfaceEntry->u4NoOfBindings++

#define IPDB_INTF_INCR_STAT_BINDS(pIpDbIfaceEntry) \
    pIpDbIfaceEntry->u4NoOfStaticBindings++

#define IPDB_INTF_INCR_DHCP_BINDS(pIpDbIfaceEntry) \
        pIpDbIfaceEntry->u4NoOfDHCPBindings++

#define IPDB_INTF_INCR_PPP_BINDS(pIpDbIfaceEntry) \
            pIpDbIfaceEntry->u4NoOfPPPBindings++


/* Macros to increment the PortCtrl ARP statistics */
#define IPDB_PORTCTRL_INCR_ARP_FWD(pPortCtrlEntry) \
            pPortCtrlEntry->u4ArpPktsForwarded++

#define IPDB_PORTCTRL_INCR_ARP_DROPPED(pPortCtrlEntry) \
            pPortCtrlEntry->u4ArpPktsDropped++

#define IPDB_PORTCTRL_DECR_ARP_FWD(pPortCtrlEntry) \
            pPortCtrlEntry->u4ArpPktsForwarded--

#define IPDB_PORTCTRL_DECR_ARP_DROPPED(pPortCtrlEntry) \
            pPortCtrlEntry->u4ArpPktsDropped--

/* Macros to increment global stats */
#define IPDB_GLOB_INCR_BINDINGS gIpDbGlobalStats.u4NoOfBindings++
#define IPDB_GLOB_INCR_STAT_BINDINGS gIpDbGlobalStats.u4NoOfStaticBindings++
#define IPDB_GLOB_INCR_DHCP_BINDINGS gIpDbGlobalStats.u4NoOfDHCPBindings++
#define IPDB_GLOB_INCR_PPP_BINDINGS gIpDbGlobalStats.u4NoOfPPPBindings++

/* Macros to decrement the interface statistics */
#define IPDB_INTF_DECR_BINDINGS(pIpDbIfaceEntry) \
    pIpDbIfaceEntry->u4NoOfBindings--

#define IPDB_INTF_DECR_STAT_BINDS(pIpDbIfaceEntry) \
    pIpDbIfaceEntry->u4NoOfStaticBindings--

#define IPDB_INTF_DECR_DHCP_BINDS(pIpDbIfaceEntry) \
        pIpDbIfaceEntry->u4NoOfDHCPBindings--

#define IPDB_INTF_DECR_PPP_BINDS(pIpDbIfaceEntry) \
            pIpDbIfaceEntry->u4NoOfPPPBindings--

/* Macros to decrement global stats */
#define IPDB_GLOB_DECR_BINDINGS gIpDbGlobalStats.u4NoOfBindings--
#define IPDB_GLOB_DECR_STAT_BINDINGS gIpDbGlobalStats.u4NoOfStaticBindings--
#define IPDB_GLOB_DECR_DHCP_BINDINGS gIpDbGlobalStats.u4NoOfDHCPBindings--
#define IPDB_GLOB_DECR_PPP_BINDINGS gIpDbGlobalStats.u4NoOfPPPBindings--

#define IPDB_MAC_TO_STR         IpdbPortConvertMacToStr

/* Macros to get Static Binding Entry fields */
#define IPDB_STAT_HOST_IP(pIpDbStaticBindingEntry) \
    pIpDbStaticBindingEntry->u4HostIp

#define IPDB_STAT_INTF_INDEX(pIpDbStaticBindingEntry) \
    pIpDbStaticBindingEntry->u4InIfIndex

#define IPDB_STAT_GATEWAY_IP(pIpDbStaticBindingEntry) \
    pIpDbStaticBindingEntry->u4GatewayIp

#define IPDB_STAT_BINDING_STAS(pIpDbStaticBindingEntry) \
    pIpDbStaticBindingEntry->u1RowStatus

/* Macros to get DB Binding Entry fields */
#define IPDB_DB_BINDING_TYPE(pIpDbBindingEntry) \
    pIpDbBindingEntry->u1BindingType

#define IPDB_DB_INTF_INDEX(pIpDbBindingEntry) \
    pIpDbBindingEntry->u4InIfIndex

#define IPDB_DB_HOST_IP(pIpDbBindingEntry) \
    pIpDbBindingEntry->u4HostIp

#define IPDB_DB_BINDING_ID(pIpDbBindingEntry) \
    pIpDbBindingEntry->u4BindingId

/* Macro used to check whether the Ip Address is valid or not */
#define  IPDB_IS_ZERO_NETWORK(u4Addr)   ((u4Addr & 0xff000000) == 0)
#define  IPDB_IS_ADDR_CLASS_A(u4Addr)   ((u4Addr &  0x80000000) == 0)
#define  IPDB_IS_ADDR_CLASS_B(u4Addr)   ((u4Addr &  0xc0000000) == 0x80000000)
#define  IPDB_IS_ADDR_CLASS_C(u4Addr)   ((u4Addr &  0xe0000000) == 0xc0000000)
#define  IPDB_IS_ADDR_CLASS_D(u4Addr)   ((u4Addr &  0xf0000000) == 0xe0000000)
#define  IPDB_IS_ADDR_CLASS_E(u4Addr)   ((u4Addr &  0xf0000000) == 0xf0000000)

#define IPDB_IS_VALID_IP(u4Addr) \
    ((IPDB_IS_ADDR_CLASS_A(u4Addr) ? \
                (IPDB_IS_ZERO_NETWORK(u4Addr) ? 0: 1) : 0)\
              || (IPDB_IS_ADDR_CLASS_B (u4Addr))\
              || (IPDB_IS_ADDR_CLASS_C(u4Addr))\
             )

/* Macro used for extracting the ARP header */
#define IPDB_ARP_HW_TYPE_LEN      2
#define IPDB_ARP_PROT_TYPE_LEN    2
#define IPDB_ARP_HWALEN           1 
#define IPDB_ARP_PALEN            1 
#define IPDB_ARP_OPCODE_LEN       2 

/* used to verify the opcode in ARP packet*/
#define IPDB_ARP_REQUEST          1
#define IPDB_ARP_RESPONSE         2
#define IPDB_ARP_MAX_PKT_LEN      28 /*MAX ARP header (28) */

#define IPDB_ETH_SRC_MAC_OFFSET   6
#define IPDB_ARP_HWALEN_OFFSET \
           (IPDB_ARP_HW_TYPE_LEN + IPDB_ARP_PROT_TYPE_LEN) 

#define IPDB_IS_PORT_VALID(u4Port) \
            (((u4Port > BRG_MAX_PHY_PLUS_LOG_PORTS) || (u4Port <= 0)\
              || (IPDB_IS_PORT_CHANNEL(u4Port) == IPDB_TRUE)) ? IPDB_FALSE : IPDB_TRUE)

#define IPDB_IS_PORT_CHANNEL(u4Port) \
            (((u4Port > SYS_DEF_MAX_PHYSICAL_INTERFACES) \
             && (u4Port <= (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG))) \
             ? IPDB_TRUE : IPDB_FALSE)
           
#define IPDB_IS_VLAN_VALID(u4VlanId) \
            (((u4VlanId < 1) || (u4VlanId > VLAN_DEV_MAX_VLAN_ID))\
             ? IPDB_FALSE : IPDB_TRUE)

#define  IPDB_IS_NULL_PORT_LIST(aPortList,u2ListLen,u1Result) \
{\
    UINT2 u2LocIndx = 0;\
        u1Result = IPDB_TRUE;\
    for (u2LocIndx = 0; u2LocIndx < (u2ListLen); u2LocIndx++) {\
        if ((aPortList)[u2LocIndx] != 0) {\
            (u1Result) = IPDB_FALSE;\
            break;\
        }\
    }\
}

/* CRU_Buffer copy */
#define IPDB_COPY_FROM_BUF(pBuf, pu1Dst, u4Offset, u4Size) \
    CRU_BUF_Copy_FromBufChain ((pBuf), (UINT1 *)(pu1Dst), u4Offset, u4Size)

#define IPDB_MAX_CONTEXTS       SYS_DEF_MAX_NUM_CONTEXTS
#endif /* _IPDBMACS_H */
