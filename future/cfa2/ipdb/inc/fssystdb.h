/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssystdb.h,v 1.1.1.1 2010/05/24 05:15:53 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSSYSTDB_H
#define _FSSYSTDB_H

UINT1 FsSystemPortCtrlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsSystemVlanCtrlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fssyst [] ={1,3,6,1,4,1,29601,2,5};
tSNMP_OID_TYPE fssystOID = {9, fssyst};


UINT4 FsSystemPortCtrlIndex [ ] ={1,3,6,1,4,1,29601,2,5,1,1,1,1};
UINT4 FsSystemPortCtrlDownstreamArpBcast [ ] ={1,3,6,1,4,1,29601,2,5,1,1,1,2};
UINT4 FsSystemPortCtrlArpPktsForwarded [ ] ={1,3,6,1,4,1,29601,2,5,1,1,1,3};
UINT4 FsSystemPortCtrlArpPktsDropped [ ] ={1,3,6,1,4,1,29601,2,5,1,1,1,4};
UINT4 FsSystemVlanIndex [ ] ={1,3,6,1,4,1,29601,2,5,1,2,1,1};
UINT4 FsSystemVlanMacForceFwdStatus [ ] ={1,3,6,1,4,1,29601,2,5,1,2,1,2};
UINT4 FsSystemVlanRowStatus [ ] ={1,3,6,1,4,1,29601,2,5,1,2,1,3};




tMbDbEntry fssystMibEntry[]= {

{{13,FsSystemPortCtrlIndex}, GetNextIndexFsSystemPortCtrlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSystemPortCtrlTableINDEX, 1, 0, 0, NULL},

{{13,FsSystemPortCtrlDownstreamArpBcast}, GetNextIndexFsSystemPortCtrlTable, FsSystemPortCtrlDownstreamArpBcastGet, FsSystemPortCtrlDownstreamArpBcastSet, FsSystemPortCtrlDownstreamArpBcastTest, FsSystemPortCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSystemPortCtrlTableINDEX, 1, 0, 0, "1"},

{{13,FsSystemPortCtrlArpPktsForwarded}, GetNextIndexFsSystemPortCtrlTable, FsSystemPortCtrlArpPktsForwardedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSystemPortCtrlTableINDEX, 1, 0, 0, NULL},

{{13,FsSystemPortCtrlArpPktsDropped}, GetNextIndexFsSystemPortCtrlTable, FsSystemPortCtrlArpPktsDroppedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSystemPortCtrlTableINDEX, 1, 0, 0, NULL},

{{13,FsSystemVlanIndex}, GetNextIndexFsSystemVlanCtrlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSystemVlanCtrlTableINDEX, 1, 0, 0, NULL},

{{13,FsSystemVlanMacForceFwdStatus}, GetNextIndexFsSystemVlanCtrlTable, FsSystemVlanMacForceFwdStatusGet, FsSystemVlanMacForceFwdStatusSet, FsSystemVlanMacForceFwdStatusTest, FsSystemVlanCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSystemVlanCtrlTableINDEX, 1, 0, 0, "2"},

{{13,FsSystemVlanRowStatus}, GetNextIndexFsSystemVlanCtrlTable, FsSystemVlanRowStatusGet, FsSystemVlanRowStatusSet, FsSystemVlanRowStatusTest, FsSystemVlanCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSystemVlanCtrlTableINDEX, 1, 0, 1, NULL},
};
tMibData fssystEntry = { 7, fssystMibEntry };

#endif /* _FSSYSTDB_H */

