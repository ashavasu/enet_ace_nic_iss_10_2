/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ifmmain.h,v 1.74 2016/03/14 10:26:00 siva Exp $
 *
 * Description:This file contains the definitions of the    
 *             global structures of the Interface       
 *             Management module of the CFA.        
 *
 *******************************************************************/
#ifndef _IFMMAIN_H
#define _IFMMAIN_H

#define   CFA_GEN_ADDR_LEN   1                            

/* Total no. of mgmt ports */
#define  CFA_NO_OF_OOB_PORTS 1
/* LA Virtual Agg Info */
#define   CFA_LAGG_NAME_PREFIX  "lagg"                           
#define   CFA_LAGG_DESCR         "IEEE 802.3ad Link Aggregate" 

/* Internal interface Info */
#define   CFA_BRIDGED_INTERFACE_NAME_PREFIX  "internal"                           
#define   CFA_BRIDGED_INTERFACE_DESCR         "Internal Interface" 

/* I-LAN interface Info */
#define   CFA_ILAN_NAME_PREFIX  "ilan"                           
#define   CFA_ILAN_DESCR         "ILAN Interface" 

/* Ethernet Info */
#define   CFA_MAX_FAST_ENET_MTU   1500 


#define   CFA_ENET_NAME_PREFIX    "eth"                           

/* 10 Mbps Enet */
#define   CFA_ENET_DESCR     "Ethernet Interface" 

/* Tunnel interface info */
#define   CFA_TNL_MTU          CFA_ENET_MTU - IP_HDR_LEN
#define   CFA_TNL_NAME_PREFIX  "tunnel"
#define   CFA_TNL_SPEED        10000000
#define   CFA_TNL_DESCR        "Encapsulation Interface"



#define   CFA_L3IPVLAN_DESCR             "L3IPVLAN Interface"
#define   CFA_LOOPBACK_DESCR             "Loopback Interface"
#define   CFA_L2VLAN_DESCR                "L2VLAN Interface"                
#define   CFA_L3IPVLAN_NAME_PREFIX       "vlan"                          
#define   CFA_OTHER_DESCR                "Connecting Interface"
/* count of number of interfaces */
#define   CFA_PPP_IF_COUNT      gu1NumofPppInterface

/* MPLS Info */
#define   CFA_MPLS_DEF_MTU           9180                            
#define   CFA_MPLS_DESCR             "MPLS Interface"
#define   CFA_MPLS_NAME_PREFIX       "mpls"
#define   CFA_MPLS_TNL_DESCR             "MPLS Tunnel Interface"
#define   CFA_MPLS_TNL_NAME_PREFIX       "mplsTunnel"

#define CFA_FLAG_LOCK(flag) if(flag) { CFA_LOCK(); }
#define CFA_FLAG_UNLOCK(flag) if(flag) { CFA_UNLOCK(); }


#define   CFA_VLAN_IF_COUNT    gu1NumofVlanInterface  
/* PPP INFO */  
/* it is equal to the speed of lower layer */
#define   CFA_PPP_DEF_MTU            1500
/* 1504 - 2(HDLC) - 2(PPP) */
#define   CFA_PPP_SPEED              64000
/* it is equal to the speed of lower layer */
#define   CFA_PPP_DESCR              "PPP Interface"
#define   CFA_PPP_HDLC_HEADER_SIZE   2

#define   CFA_MIN_MTU_SIZE           46

/* HDLC INFO */
#ifdef HDLC_WANTED
#define   CFA_HDLC_SPEED              1544000
#define   CFA_HDLC_DESCR     "HDLC Interface"
#endif

#ifdef TLM_WANTED
#define CFA_TELINK_DEF_MTU             1500
#define CFA_TELINK_DESCR               "TeLink Interface"
#define CFA_TELINK_NAME_PREFIX         "telink"
#endif

#define   CFA_DEF_PSEUDO_WIRE_MTU     (CFA_ENET_MTU - \
                                      (CFA_ENET_V2_HEADER_SIZE + MPLS_HDR_LEN))
#ifdef VXLAN_WANTED
#define CFA_DEF_VXLAN_NVE_MTU          9000
#endif
/* stack layer directions */
#define   CFA_HIGH                       1
#define   CFA_LOW                        2

/* Status of Interfaces' RcvAddressTable Entry */
#define   CFA_RCVADDR_OTHER              1
/* treates as volatile */
#define   CFA_RCVADDR_VOL                2
#define   CFA_RCVADDR_NVOL               3
/* only the unicast & bcast address is non-volatile */

/* Demand Circuit Status - during all these phases the operstatus remains
down and hence we need these intermediate status' */
#define   CFA_DEMAND_CKT_CLOSE           1
#define   CFA_DEMAND_CKT_ESTABLISHING    2
#define   CFA_DEMAND_CKT_Q_CLEARING      3

#define   EXTRY_NOT_EXISTS_FLAG          0
#define   EXTRY_EXISTS_FLAG              1

/* Macros used for dynamic memory allocation */
#define CFA_IFINFO_MAX_INTERFACE      CFA_MAX_INTERFACES_IN_SYS

#define CFA_MAX_TLV_INFO              BRG_MAX_PHY_PLUS_LOG_PORTS

#define CFA_MAX_TLV_INFO_BUDDY        BRG_MAX_PHY_PLUS_LOG_PORTS

#define CFA_DEF_INTERFACES_IN_SYSTEM  (SYS_DEF_MAX_INTERFACES + \
                                       CFA_MAX_NUM_OF_VIP + \
                                       CFA_MAX_NUM_OF_OF + \
                                       CFA_MAX_SISP_INTERFACES)
                                      /* Macro used to allocate memory for
                                       * gaCfaCommonIfInfo */

#define CFA_MAX_PKT_SIZE                (CFA_MAX_GIGA_ENET_MTU  + 4)
#define CFA_MAX_TLV_INFO_BUDDY_IN_SYSTEM \
                                         BRG_NUM_PHY_PLUS_LOG_PORTS

#define  CFA_PPP_IP_DATAGRAM    0x0021

#if defined (WLC_WANTED) || defined (WTP_WANTED)
#define CFA_WSS_MAX_PKT_LEN         2500

#define   CFA_RADIO_NAME_PREFIX  "Radio"                           
#define   CFA_RADIO_DESCR         "Radio Interface" 
        
#define   CFA_WLAN_RADIO_NAME_PREFIX  "wlanRadio"                           
#define   CFA_WLAN_RADIO_DESCR         "WLAN Bound Radio Interface" 
        
#define   CFA_CAPWAP_VIRT_RADIO_NAME_PREFIX  "virtRadio"                           
#define   CFA_CAPWAP_VIRT_RADIO_DESCR         "WTP Virtual Radio Interface" 
        
#define   CFA_CAPWAP_DOT11_PROFILE_NAME_PREFIX  "dot11Profile"                           
#define   CFA_CAPWAP_DOT11_PROFILE_DESCR         "WLAN Profile Interface" 
        
#define   CFA_CAPWAP_DOT11_BSS_NAME_PREFIX  "dot11Bss"                           
#define   CFA_CAPWAP_DOT11_BSS_DESCR         "WLAN BSS Interface" 
#endif

/* CFA's system sizing parameters */
typedef struct {
    UINT4  u4MaxInterfaces;
    UINT2  u2MaxPhysInterfaces;
    UINT2  u2HostCacheSize;
    UINT4  u4DefaultIpAddr;
} tCfaSystemSizingParams;


/* global structure for interfaces */
typedef struct {
    UINT4  u4IfTableLastChange;
    UINT4  u4IfStackLastChange;
    UINT4  u4IfNumber;           /* number of entries currently present in 
                                  * ifTable 
                                  */
    UINT4  u4AvailableIfIndex;
    UINT4  u4MaxInterfaces;      /* maximum no. of interfaces in the system */
    UINT2  u2MaxPhysInterfaces;  /* maximum physical interfaces */
    UINT2  u2MaxTunlInterfaces; /*Maximum Logical interface */
} tIfGlobalStruct;


/* high & low stack entry struct */
typedef struct {
    tTMO_SLL_NODE  NextEntry;
    UINT4          u4IfIndex;
    UINT1          u1StackStatus;
    UINT1          au1Reserved[3];
} tStackInfoStruct;

typedef struct {
UINT1               au1PortArray[CFA_MGMT_VLAN_LIST_SIZE];
} tPortArray;


/* RcvAddres table entry struct */
typedef struct {
    tTMO_SLL_NODE  NextEntry;
    UINT1          u1AddressStatus;
    UINT1          u1AddressType;
    UINT1          au1Address[CFA_MAX_MEDIA_ADDR_LEN];
    UINT2          u2Reserved;
} tRcvAddressInfo;


/* Demand Circuit Buffer Queue Node's struct */
typedef struct {
   tTMO_SLL_NODE           NextEntry;
   tCRU_BUF_CHAIN_HEADER   *pBuf;
} tDemandCktBuf;


/* For all HC packet counters, show the normal counters
after typecasting. For HC octet counters, show the high counter along with
the normal counter. The VHC conformance group is not implemented as an
interface with speed higher than 650Mbps is not expected. */

/* the object ifCounterDiscontinuityTime is not implemented as we cannot keep
track of the creation->deletion->re-creation of an interface. This object
should always show 0 */

/* ifOutQLen is deprecated and is always shown as 0.
ifHighSpeed is not implemented and always shown as zero. ifPhysAddress is
shown as the first address of the ifRcvAddressTable. ifConnectorPresent is
shown on the basis of ifType - only the interfaces corresponding to the
device drivers have this value as CFA_TRUE. */


/* structure for propagating Admin-Oper status up the stack */
typedef struct {
   tTMO_SLL_NODE     NextEntry;
   tTMO_SLL          *pStack;
   tStackInfoStruct  *pNode;
} tIfStatusPropagate;



/* structure to store the default values of interface related params */
typedef struct {
    UINT4 u4IfType; /* type of the interface */
    UINT4 u4IfMtu; /* MTU of the interface */
    UINT4 u4IfSpeed; /* speed of the interface */
    UINT4 u4IfCount; /* Total no. of interfaces present */
    UINT1 au1AliasPrefix[CFA_MAX_PORT_NAME_LENGTH]; /* IfPrefix. e.g., "eth"*/
    UINT1 au1AliasDescr[CFA_MAX_DESCR_LENGTH]; /* IfDescr. e.g.,"Ethernet Interface" */
    UINT1 au1IfType[CFA_MAX_PORT_NAME_LENGTH]; /* IfType. e.g., "ENET"*/
} tCfaIfDefaultVal;

typedef struct CfaLinkStatusMsg
{
    UINT4  u4IfIndex;
    UINT1  u1LinkStatus;
    UINT1  u1Reserved[3];
}
tCfaLinkStatusMsg;

typedef struct CfaDynamicSchannelIf
{
    UINT4  u4SChIfIndex;
    UINT4  u4UapIfIndex;
} tCfaDynamicSchannelIf;

typedef struct CfaVipOperStatusMsg
{
    UINT4  u4IfIndex;
    UINT1  u1VipOperStatus;
    UINT1  u1Reserved[3];
}
tCfaVipOperStatusMsg;
typedef struct CfaDefPortParams
{
    tIp6Addr localIp6Addr;
    UINT4  u4IpAddress;
    UINT4  u4SubnetMask;
    UINT4  u4ConfigMode;
    UINT4  u4IpAddrAllocProto;
    UINT1  au1IntfName[CFA_MAX_PORT_NAME_LENGTH]; /* IfPrefix. e.g., "eth"*/
    UINT1  u1PrefixLen;
    UINT1  u1Pad[3];
}tCfaDefPortParams;
typedef struct CfaIpInfoMsg
{
    UINT4  u4IfIndex;
    tIpInterfaceParams IpInfo;
}tCfaIpInfoMsg;

typedef struct CfaBaseBridgeModeMsg
{
    UINT1  u1BaseBridgeMode;
    UINT1  au1Reserved[3];
}tCfaBaseBridgeModeMsg;

typedef struct CfaIfUpdateMsg
{
    UINT4  u4IfIndex;
    UINT4  u4Mtu;
}tCfaIfUpdateMsg;

#ifdef ICCH_WANTED
typedef struct CfaMclagOperStatusMsg
{
    UINT4  u4IfIndex;
    UINT1  u1MclagIvrOperStatus;
    UINT1  u1Reserved[3];
}tCfaMclagOperStatusMsg;
#endif

typedef struct CfaExtTriggerMsg
{
    INT4   i4MsgType;
    union
    {
        tCfaLinkStatusMsg       LinkStatusMsg;
        tCfaVipOperStatusMsg    VipOperStatusMsg;
        tCfaIpInfoMsg           IpInfoMsg;
        UINT2                   u2PortChannelId;
        tCfaBaseBridgeModeMsg  BaseBridgeModeMsg;
        tCfaIfUpdateMsg         IfUpdateMsg;
        tCfaDynamicSchannelIf    DynSchannelIf;
        /* changing from RM to L2RED */
#ifdef L2RED_WANTED
        tCfaRmMsg          CfaRmMsg;
#endif
#ifdef ICCH_WANTED
        tCfaMclagOperStatusMsg  MclagOperStatusMsg;
#endif
    }uExtTrgMsg;
}
tCfaExtTriggerMsg;

typedef struct CfaTxPktOnPortListMsg
{
    tCRU_BUF_CHAIN_HEADER *pFrame;
    tPortList              PortList;
}
tCfaTxPktOnPortListMsg;


typedef struct _tCfaPktPoolBlock
{
    UINT1   au1CfaPktPoolBlock[CFA_MAX_PKT_SIZE];
}tCfaPktPoolBlock;

typedef struct _tCfaDriverMtuBlock
{
    UINT1   au1CfaDriverMtuBlock[CFA_MAX_DRIVER_MTU * 2];
}tCfaDriverMtuBlock;

extern tMemPoolId  ExtTriggerMsgPoolId;
extern tMemPoolId  TxPktOnPortListMsgPoolId;
extern INT4  i4IfTlvBuddyMemPoolId;
extern tMemPoolId IfTlvEntryMemPoolId;

#ifdef PPP_WANTED
/* IP address info struct for PPP interfaces */
typedef struct {
    UINT4  u4LocalIpAddr;
    UINT4  u4PeerIpAddr;
    UINT1  u1SubnetMask;
    UINT1  u1Reserved;
    UINT2  u2Reserved;
} tPppIpInfo;
#endif /* PPP_WANTED */


/* Application CallBack Structure */
typedef union CfaUtilCallBackEntry
{
    INT4 (*pCfaCustFwlCheck) (UINT4);

}unCfaUtilCallBackEntry;
#endif /* _IFMMAIN_H */

