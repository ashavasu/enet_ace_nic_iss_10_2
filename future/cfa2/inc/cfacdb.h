/* $Id: cfacdb.h,v 1.40 2016/10/03 10:34:39 siva Exp $ */

#ifndef _CFACDB_H
#define _CFACDB_H

#ifdef _CFACDB_C_
tCfaIfInfo      gaCfaCommonIfInfo [CFA_DEF_INTERFACES_IN_SYSTEM]; 
                                       /* Pointer to array of Common inteface 
                                       info */
UINT1           gIntfVlanBitList [VLAN_LIST_SIZE]; /* Bit list array of 
                                                     interface Vlans   */
tMgmtVlanList   gMgmtVlanList; /* VLAN list associated with the management
                                  VLAN interface */
tSecVlanList   gSecVlanList; /* VLAN list associated with the Security */
/* Private VLAN IVR mapping table.*/
tRBTree             gCfaIvrMapTable;

/* MemPool for CfaL3PvlanInfo table*/
tMemPoolId          gCfaIvrMapMemPoolId;

tMemPoolId          gCfaCdbMemPoolId;

tMemPoolId          gCfaMauMemPoolId;
/* CfaCdb RBTree */
tCfaIfDb        gCfaCdbIfTable;

/* Interface type based array for CDB */
tCfaIfTypeDb        gaCfaCdbIfTypeDb[CFA_MAX_IF_TYPES];

/* Global array for storing the VLAN list */
UINT1 gCfaVlanList[CFA_MGMT_VLAN_LIST_SIZE];
#ifdef NPAPI_WANTED
tNpIpVlanMappingInfo gIpVlanMappingInfo;
#endif
#else
extern tCfaIfInfo     gaCfaCommonIfInfo [CFA_DEF_INTERFACES_IN_SYSTEM];
extern UINT1          gIntfVlanBitList [VLAN_LIST_SIZE];
extern tMgmtVlanList   gMgmtVlanList;
extern tSecVlanList   gSecVlanList; /* VLAN list associated with the Security */
extern tRBTree             gCfaIvrMapTable;
extern tMemPoolId          gCfaIvrMapMemPoolId;
extern UINT1     gCfaVlanList[CFA_MGMT_VLAN_LIST_SIZE];
extern tCfaIfDb       gCfaCdbIfTable;
extern tCfaIfTypeDb   gaCfaCdbIfTypeDb[CFA_MAX_IF_TYPES];
extern tMemPoolId          gCfaCdbMemPoolId;
extern tMemPoolId          gCfaMauMemPoolId;
#ifdef NPAPI_WANTED
extern tNpIpVlanMappingInfo gIpVlanMappingInfo;
#endif
#endif

#define CFA_CDB_IF_MEMPOOL     gCfaCdbMemPoolId
#define CFA_MAU_IF_MEMPOOL     gCfaMauMemPoolId

#define CFA_DS_LOCK         CfaCommonDSLock
#define CFA_DS_UNLOCK       CfaCommonDSUnLock
#define CFA_CDB_SEMAPHORE   ((const UINT1 *)"CFAC")

#define CFA_TNL_DS_LOCK    CfaTunnelDSLock
#define CFA_TNL_DS_UNLOCK  CfaTunnelDSUnLock
#define CFA_TNL_SEMAPHORE  ((const UINT1 *)"CFAT")

#define   CFA_MGMT_VLANLIST()    (gMgmtVlanList)
#define   CFA_SEC_VLANLIST()     (gSecVlanList)

#define   CFA_CDB_IF_ENTRY(u4IfIndex)   CFA_CDB_GET(u4IfIndex)

#define   CFA_CDB_IF_COUNTERS(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->IfCounters)   
    
#define   CFA_CDB_SET_INTF_VALID(u4IfIndex,i4Status) \
          { \
              (CFA_CDB_GET(u4IfIndex))->i4Valid = i4Status; \
          }

#define   CFA_CDB_SET_INTF_ACTIVE(u4IfIndex,i4IfActive) \
          { \
              (CFA_CDB_GET(u4IfIndex))->i4Active = i4IfActive; \
          }

/* Flexible IfIndex changes: ===
 * For the CFA_CDB_IS_INTF_VALID and
 * CFA_CDB_IS_INTF_ACTIVE macro we return CFA_FALSE
 * if the node dosen't exist (when RBTree is used for CDB DB) */

#define   CFA_CDB_IS_INTF_VALID(u4IfIndex) \
          ((NULL != CfaIfUtlCdbGetIfDbEntry(u4IfIndex)) ? \
          ((CfaIfUtlCdbGetIfDbEntry(u4IfIndex))->i4Valid) : \
           CFA_FALSE)

#define   CFA_CDB_IS_INTF_ACTIVE(u4IfIndex) \
          ((NULL != CfaIfUtlCdbGetIfDbEntry(u4IfIndex)) ? \
          ((CfaIfUtlCdbGetIfDbEntry(u4IfIndex))->i4Active) : \
           CFA_FALSE)
#define   CFA_CDB_IF_UFD_OPER(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1UfdOperStatus)
#define   CFA_CDB_IF_ROLE(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1PortRole)
#define   CFA_CDB_IF_UFD_GROUP_ID(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u4UfdGroupId)
#define   CFA_CDB_IF_UFD_DOWNLINKDISABLED_COUNT(u4IfIndex)\
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u4UfdDownlinkDisabledCount)
#define   CFA_CDB_IF_UFD_DOWNLINKENABLED_COUNT(u4IfIndex)\
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u4UfdDownlinkEnabledCount)
#define   CFA_CDB_IF_DESIG_UPLINK_STATUS(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1DesigUplinkStatus)
#define   CFA_CDB_IS_INTF_INTERNAL(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->i4IsInternalPort)
#define   CFA_CDB_IF_OPER(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1IfOperStatus)
#define   CFA_CDB_IF_MTU(u4IfIndex)     \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u4IfMtu)
#define   CFA_CDB_IF_IP_CHANGE(u4IfIndex)     \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1Ipv4AddressChange)
#define   CFA_CDB_IF_SPEED(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u4IfSpeed)
#define   CFA_CDB_IF_ADMIN_STATUS(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1IfAdminStatus)
#define   CFA_CDB_IF_ROW_STATUS(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1IfRowStatus)
#define   CFA_CDB_IF_HIGHSPEED(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u4IfHighSpeed)
#define   CFA_CDB_IF_SYS_SPECIFIC_PORTID(u4IfIndex)   \
              (CFA_CDB_IF_ENTRY(u4IfIndex)->u4SysSpecificPortID)
#define   CFA_CDB_IF_ALIAS(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->au1IfName)
#define   CFA_CDB_PORT_NAME(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->au1PortName)
#define   CFA_CDB_IF_DESC(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->au1Descr)
#define   CFA_CDB_IF_TYPE(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1IfType)
#define   CFA_CDB_IF_SUB_TYPE(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1IfSubType)
#define   CFA_CDB_ETHERNET_TYPE(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1EthernetType)
#define   CFA_CDB_IF_IP_IPPORT(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->i4IpPort)
#ifdef LNXIP4_WANTED
#define   CFA_CDB_IDX_MGR_PORT(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u4IndexMgrPort)
#endif

#define   CFA_CDB_IF_ALIAS_NAME(u4IfIndex)  \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->au1IfAliasName)

#define   CFA_CDB_IF_PAUSE_ADMIN_MODE(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1PauseAdminNode)
#define   CFA_CDB_IF_PAUSE_OPER_MODE(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1PauseOperMode)

#define   CFA_CDB_IF_DUPLEXSTATUS(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1DuplexStatus)
#define   CFA_CDB_IF_AUTONEGSTATUS(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1AutoNegStatus)
#define   CFA_CDB_IF_AUTONEGSUPPORT(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1AutoNegSupport)
    
#define   CFA_CDB_IF_OPER_MAU_TYPE(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u2OperMauType)
#define   CFA_CDB_IF_ADVT_CAPABILITY(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u2AdvtCapability)
#define   CFA_CDB_IF_VLANID_OF_IVR(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u2VlanId)
#define   CFA_CDB_IF_BRIDGED_IFACE_STATUS(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1BridgedIface)
#define   CFA_CDB_IF_BRIDGE_PORT_TYPE(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1BrgPortType)
#define   CFA_GET_IF_IN_OCTETS(u4IfIndex) \
          (CFA_CDB_IF_COUNTERS(u4IfIndex).u4InOctets) 
#define   CFA_GET_IF_IN_UCAST(u4IfIndex) \
          (CFA_CDB_IF_COUNTERS(u4IfIndex).u4InUcastPkts)
#define   CFA_GET_IF_IN_MCAST(u4IfIndex) \
         (CFA_CDB_IF_COUNTERS(u4IfIndex).u4InMulticastPkts)
#define   CFA_GET_IF_IN_BCAST(u4IfIndex) \
          (CFA_CDB_IF_COUNTERS(u4IfIndex).u4InBroadcastPkts)
#define   CFA_GET_IF_IN_HC_OCTETS(u4IfIndex) \
         (CFA_CDB_IF_COUNTERS(u4IfIndex).u4HighInOctets)
#define   CFA_GET_IF_IN_DISCARDS(u4IfIndex) \
         (CFA_CDB_IF_COUNTERS(u4IfIndex).u4InDiscards)
#define   CFA_GET_IF_IN_ERR(u4IfIndex) \
          (CFA_CDB_IF_COUNTERS(u4IfIndex).u4InErrors)
#define   CFA_GET_IF_IN_UNKNOWNPROTOS(u4IfIndex) \
         (CFA_CDB_IF_COUNTERS(u4IfIndex).u4InUnknownProtos) 
#define   CFA_GET_IF_OUT_OCTETS(u4IfIndex) \
         (CFA_CDB_IF_COUNTERS(u4IfIndex).u4OutOctets)
#define   CFA_GET_IF_OUT_UCAST(u4IfIndex) \
         (CFA_CDB_IF_COUNTERS(u4IfIndex).u4OutUcastPkts)
#define   CFA_GET_IF_OUT_MCAST(u4IfIndex) \
         (CFA_CDB_IF_COUNTERS(u4IfIndex).u4OutMulticastPkts)
#define   CFA_GET_IF_OUT_BCAST(u4IfIndex) \
          (CFA_CDB_IF_COUNTERS(u4IfIndex).u4OutBroadcastPkts)
#define   CFA_GET_IF_OUT_HC_OCTETS(u4IfIndex) \
         (CFA_CDB_IF_COUNTERS(u4IfIndex).u4HighOutOctets)
#define   CFA_GET_IF_OUT_DISCARDS(u4IfIndex) \
         (CFA_CDB_IF_COUNTERS(u4IfIndex).u4OutDiscards)
#define   CFA_GET_IF_OUT_ERR(u4IfIndex) \
          (CFA_CDB_IF_COUNTERS(u4IfIndex).u4OutErrors)
#define   CFA_CDB_IF_EOAM_STATUS(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->EoamParams.u1EoamStatus)
#define   CFA_CDB_IF_EOAM_MUX_STATE(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->EoamParams.u1MuxState)
#define   CFA_CDB_IF_EOAM_PAR_STATE(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->EoamParams.u1ParState)
#define   CFA_CDB_IF_EOAM_UNIDIR_SUPP(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->EoamParams.u1UniDirSupp)
#define   CFA_CDB_IF_EOAM_LINK_FAULT(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->EoamParams.u1LinkFault)
#define   CFA_CDB_IF_EOAM_REMOTE_LB(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->EoamParams.u1RemoteLB)
#define   CFA_CDB_IF_PORT_SEC_STATE(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1PortSecState)
#define   CFA_CDB_IF_WAN_TYPE(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1WanType)
#define   CFA_CDB_IF_NW_TYPE(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u1NwType)
#define   CFA_CDB_IF_MAC(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->au1MacAddr)
#define   CFA_CDB_IF_DESCRIPTION(u4IfIndex)  \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->au1Desc)
#define   CFA_CDB_IF_STORAGE_TYPE(u4IfIndex)   \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->i4IfMainStorageType)
#define   CFA_CDB_IF_MAU_ENTRY_VALID(pCfaIfInfo) \
          (CFA_CDB_IF_MAU_ENTRY(pCfaIfInfo) != NULL)
 
#define   CFA_CDB_IF_MAU_ENTRY(pCfaIfInfo)   \
            (pCfaIfInfo->IfMauEntry.pIfMauInfo)
           
#define   CFA_CDB_IF_MAU_STATUS(pCfaIfInfo)   \
            (CFA_CDB_IF_MAU_ENTRY(pCfaIfInfo)->i4Status)
#define   CFA_CDB_IF_MAU_DEF_TYPE(pCfaIfInfo)   \
            (CFA_CDB_IF_MAU_ENTRY(pCfaIfInfo)->u4DefType)
#define   CFA_CDB_IF_MAU_ANSTATUS(pCfaIfInfo)   \
            (CFA_CDB_IF_MAU_ENTRY(pCfaIfInfo)->i4ANStatus)
#define   CFA_CDB_IF_MAU_ANRESTART(pCfaIfInfo)   \
            (CFA_CDB_IF_MAU_ENTRY(pCfaIfInfo)->i4ANRestart)
#define   CFA_CDB_IF_MAU_ANCAPADVTBITS(pCfaIfInfo)   \
            (CFA_CDB_IF_MAU_ENTRY(pCfaIfInfo)->i4ANCapAdvtBits)            
#define   CFA_CDB_IF_MAU_ANREMFLTADVT(pCfaIfInfo)   \
            (CFA_CDB_IF_MAU_ENTRY(pCfaIfInfo)->i4ANRemFltAdvt)

#define   CFA_CDB_IF_AC_PORT(u4IfIndex)     \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u4ACPort)

#define   CFA_CDB_IF_AC_VLAN(u4IfIndex)     \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u2ACVlan)

#define   CFA_CDB_IP_INTF_STATS(u4IfIndex)     \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->u4IpIntfStats)

#define   CFA_CDB_LINKUP_DELAY_STATUS(u4IfIndex)     \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->IfLinkUpDelay.u1LinkUpDelayStatus)

#define   CFA_CDB_LINKUP_DELAY_TIMER(u4IfIndex)     \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->IfLinkUpDelay.u4ConfLinkUpTime)

#define   CFA_CDB_LINKUP_DELAY_REMAINING_TIME(u4IfIndex)     \
          (CFA_CDB_IF_ENTRY(u4IfIndex)->IfLinkUpDelay.u4LinkUpRemainingTime)

#define   CFA_CDB_SET_ENCAP_STATUS(u4IfIndex,i4Status) \
          { \
              (CFA_CDB_GET(u4IfIndex))->u1IsEncapSet = i4Status; \
          }

#define   CFA_CDB_GET_ENCAP_STATUS(u4IfIndex,i4Status) \
          { \
              i4Status = (CFA_CDB_GET(u4IfIndex))->u1IsEncapSet ; \
          }
#endif /* _CFACDB_H */
