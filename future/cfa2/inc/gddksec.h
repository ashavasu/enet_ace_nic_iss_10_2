/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddksec.h,v 1.6 2014/11/25 13:04:46 siva Exp $
 *
 * Description:This file contains the prototypes for all
 *             the functions of the CFA system with respect to 
 *             kernel space
 *
 *******************************************************************/

#ifndef _GDDKSEC_H
#define _GDDKSEC_H


#define CFA_VLAN_CFI_MASK                0x1000
#define CFA_VLAN_VID_MASK                0x0fff
#define CFA_IP_VER_IHL_MASK              0x45 /* IP version and IP header
                                                 length */
#define CFA_IPV6_VER_PRIORITY_MASK       0x60 
                                        
/* GDD protos */   
/* From `gddksec.c': */

INT4 CfaGddInit  PROTO ((VOID));

INT4
CfaProcessSkb (tSkb * pSkb, tNetDevice * pNetDev,
               tPktType * pPktType, tNetDevice * pOrgDev);

INT4
CfaIwfEnetProcessRxFrame (tCRU_BUF_CHAIN_HEADER * pCruBuf, UINT4 u4IfIndex,
                          UINT4 u4PktSize, UINT2 u2Protocol,
                          UINT1 u1Direction);
INT4
GddKSecMoveOffSet (tSkb * pSkb);

INT4
GddKSecPortGetDirection (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1Direction,
                          tNetDevice * pNetDev);

INT4
GddKSecUpdateOutTag(UINT1 u1Direction, UINT2 u2DsaVlanId);

INT4
GddKSecAddSpecificTag (tCRU_BUF_CHAIN_HEADER * pBuf);

INT4 GddKSecGetIntAndDir PROTO((tCRU_BUF_CHAIN_HEADER * pBuf , 
                                UINT1 *u1Direction , UINT4 *u4IfIndex, 
                                tNetDevice * pNetDev));

INT4 CfaIwfEnetProcessTxFrame PROTO((tCRU_BUF_CHAIN_HEADER * pCruBuf , 
                                    UINT4 u4IfIndex , UINT1 *au1DestHwAddr , 
         UINT4 u4PktSize , UINT2 u2Protocol , 
         UINT1 u1EncapType ));

INT4 CfaHandlePktFromSec PROTO((tCRU_BUF_CHAIN_HEADER * pBuf ));

INT4 CfaGddWrite PROTO((tCRU_BUF_CHAIN_HEADER * pBuf , UINT4 u4IfIndex , 
                        UINT4 u4PktSize , UINT1 u1VlanTagCheck , 
      tCfaIfInfo * pIfInfo ));

VOID CfaGddDeInit (VOID);
/* Port API's */


#endif /* _CFAKPROTO_H */
