/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: cfaufd.h,v 1.3 2015/08/26 10:54:25 siva Exp $
 *
 * Description: This file contains Structures and macro's required
 *              for CFA UFD  Module.
 *
 *******************************************************************/
#ifndef _CFA_UFD_H_
#define _CFA_UFD_H_

#ifdef __CFAUFD_C__
UINT4                    gu4UfdPortFlag;
UINT4                    gu4UfdGroupCount;
tUfdSysControl           gUfdSystemControl;
tUfdEnable               gUfdModuleStatus;
tShSysControl            gShSystemControl;
tShEnable                gShModuleStatus;
tRBTree            gUfdGroupInfoTable;
tMemPoolId  gCfaUfdGroupMemPoolId;
#else
extern UINT4                    gu4UfdPortFlag;
extern UINT4                    gu4UfdGroupCount;
extern tRBTree            gUfdGroupInfoTable;
extern tMemPoolId  gCfaUfdGroupMemPoolId;
#endif



#define CFA_UFD_GROUP_MEMPOOL  gCfaUfdGroupMemPoolId
#define CFA_UFD_PORT_LIST_SIZE ((SYS_DEF_MAX_PHYSICAL_INTERFACES  + LA_MAX_AGG_INTF + 31)/32 * 4)

#define   CFA_UFD_NUMERIC_ASCII_START   48
#define   CFA_UFD_NUMERIC_ASCII_END     57
#define   CFA_UFD_CALPHA_ASCII_START    65
#define   CFA_UFD_CALPHA_ASCII_END      90
#define   CFA_UFD_SALPHA_ASCII_START    97
#define   CFA_UFD_SALPHA_ASCII_END      122

#endif
