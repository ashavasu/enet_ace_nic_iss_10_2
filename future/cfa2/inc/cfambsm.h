/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfambsm.h,v 1.11 2011/09/12 06:54:44 siva Exp $
 *
 * Description:Header file for CFA MBSM feature                 
 *
 *******************************************************************/

#ifndef _CFAMBSM_H
#define _CFAMBSM_H



#define CFA_MBSM_ALIAS_PREFIX            "Slot"
#define CFA_MBSM_STK_ALIAS_PREFIX        "STK"

#ifdef MBSM_WANTED
typedef struct _tCfaMbsmSlotMsgBlock
{
    tMbsmCfaHwInfoMsg   MbsmCfaHwInfoMsg;
    tMbsmSlotPortInfo   MbsmSlotPortInfo[MBSM_MAX_SLOTS];
}tCfaMbsmSlotMsgBlock;
#else
typedef UINT1 tCfaMbsmSlotMsgBlock;
#endif

PUBLIC INT4 IssMbsmCfaCardInsert PROTO ((tMbsmProtoMsg *pProtMsg));

PUBLIC INT4 IssMbsmCfaCardRemove PROTO ((tMbsmProtoMsg *pProtMsg));

PUBLIC INT4 CfaMbsmCreateIvrInterface PROTO ((tMbsmProtoMsg *pProtMsg));

INT4 CfaMbsmCardInsertIvrMapTable (tMbsmProtoMsg *pProtMsg);

PUBLIC INT4 CfaMbsmUpdateMtuToHw (UINT4 u4IfIndex);

#ifdef MPLS_WANTED
PUBLIC INT4 CfaMbsmCreateMplsInterface PROTO ((tMbsmProtoMsg * pProtMsg));
#endif
PUBLIC INT4 CfaMbsmCreateCustomParam PROTO ((tMbsmProtoMsg *pProtMsg));
PUBLIC INT4 CfaMbsmInterfaceMacAddr PROTO ((tMbsmProtoMsg *pProtMsg));
#endif

