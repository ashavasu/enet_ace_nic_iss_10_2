
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfNumber ARG_LIST((INT4 *));

/* Proto Validate Index Instance for IfTable. */
INT1
nmhValidateIndexInstanceIfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IfTable  */

INT1
nmhGetFirstIndexIfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfDescr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIfType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfMtu ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfSpeed ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfPhysAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIfAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfLastChange ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfInOctets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfInUcastPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfInNUcastPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfInDiscards ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfInErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfInUnknownProtos ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfOutOctets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfOutUcastPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfOutNUcastPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfOutDiscards ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfOutErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfOutQLen ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfSpecific ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfAdminStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfTableLastChange ARG_LIST((UINT4 *));

INT1
nmhGetIfStackLastChange ARG_LIST((UINT4 *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
/* Proto Validate Index Instance for IfXTable. */
INT1
nmhValidateIndexInstanceIfXTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IfXTable  */

INT1
nmhGetFirstIndexIfXTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfXTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIfInMulticastPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfInBroadcastPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfOutMulticastPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfOutBroadcastPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfHCInOctets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIfHCInUcastPkts ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIfHCInMulticastPkts ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIfHCInBroadcastPkts ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIfHCOutOctets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIfHCOutUcastPkts ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIfHCOutMulticastPkts ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIfHCOutBroadcastPkts ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIfLinkUpDownTrapEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfHighSpeed ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfPromiscuousMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfConnectorPresent ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfAlias ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIfCounterDiscontinuityTime ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfLinkUpDownTrapEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfPromiscuousMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfAlias ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfLinkUpDownTrapEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfPromiscuousMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfAlias ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfXTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IfStackTable. */
INT1
nmhValidateIndexInstanceIfStackTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IfStackTable  */

INT1
nmhGetFirstIndexIfStackTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfStackTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfStackStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfStackStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfStackStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfStackTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
/* Proto Validate Index Instance for IfRcvAddressTable. */
INT1
nmhValidateIndexInstanceIfRcvAddressTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for IfRcvAddressTable  */

INT1
nmhGetFirstIndexIfRcvAddressTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfRcvAddressTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfRcvAddressStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIfRcvAddressType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfRcvAddressStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetIfRcvAddressType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfRcvAddressStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2IfRcvAddressType ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfRcvAddressTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
