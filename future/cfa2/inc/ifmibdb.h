/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: ifmibdb.h,v 1.7 2008/08/20 13:40:38 premap-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _IFMIBDB_H
#define _IFMIBDB_H

UINT1 IfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 IfXTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 IfStackTableINDEX [ ] = {SNMP_DATA_TYPE_INTEGER32, SNMP_DATA_TYPE_INTEGER32};

UINT1 IfRcvAddressTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM };

UINT4 ifmib [ ] = { 1,3,6,1,2,1,2 };
tSNMP_OID_TYPE ifmibOID = {7, ifmib};


UINT4 IfNumber [ ] = {1,3,6,1,2,1,2,1};
UINT4 IfIndex [ ] = {1,3,6,1,2,1,2,2,1,1};
UINT4 IfDescr [ ] = {1,3,6,1,2,1,2,2,1,2};
UINT4 IfType [ ] = {1,3,6,1,2,1,2,2,1,3};
UINT4 IfMtu [ ] = {1,3,6,1,2,1,2,2,1,4};
UINT4 IfSpeed [ ] = {1,3,6,1,2,1,2,2,1,5};
UINT4 IfPhysAddress [ ] = {1,3,6,1,2,1,2,2,1,6};
UINT4 IfAdminStatus [ ] = {1,3,6,1,2,1,2,2,1,7};
UINT4 IfOperStatus [ ] = {1,3,6,1,2,1,2,2,1,8};
UINT4 IfLastChange [ ] = {1,3,6,1,2,1,2,2,1,9};
UINT4 IfInOctets [ ] = {1,3,6,1,2,1,2,2,1,10};
UINT4 IfInUcastPkts [ ] = {1,3,6,1,2,1,2,2,1,11};
UINT4 IfInNUcastPkts [ ] = {1,3,6,1,2,1,2,2,1,12};
UINT4 IfInDiscards [ ] = {1,3,6,1,2,1,2,2,1,13};
UINT4 IfInErrors [ ] = {1,3,6,1,2,1,2,2,1,14};
UINT4 IfInUnknownProtos [ ] = {1,3,6,1,2,1,2,2,1,15};
UINT4 IfOutOctets [ ] = {1,3,6,1,2,1,2,2,1,16};
UINT4 IfOutUcastPkts [ ] = {1,3,6,1,2,1,2,2,1,17};
UINT4 IfOutNUcastPkts [ ] = {1,3,6,1,2,1,2,2,1,18};
UINT4 IfOutDiscards [ ] = {1,3,6,1,2,1,2,2,1,19};
UINT4 IfOutErrors [ ] = {1,3,6,1,2,1,2,2,1,20};
UINT4 IfOutQLen [ ] = {1,3,6,1,2,1,2,2,1,21};
UINT4 IfSpecific [ ] = {1,3,6,1,2,1,2,2,1,22};
UINT4 IfTableLastChange [ ] ={1,3,6,1,2,1,31,1,5};
UINT4 IfStackLastChange [ ] ={1,3,6,1,2,1,31,1,6};
UINT4 IfName [ ] = {1,3,6,1,2,1,31,1,1,1,1};
UINT4 IfInMulticastPkts [ ] = {1,3,6,1,2,1,31,1,1,1,2};
UINT4 IfInBroadcastPkts [ ] = {1,3,6,1,2,1,31,1,1,1,3};
UINT4 IfOutMulticastPkts [ ] = {1,3,6,1,2,1,31,1,1,1,4};
UINT4 IfOutBroadcastPkts [ ] = {1,3,6,1,2,1,31,1,1,1,5};
UINT4 IfHCInOctets [ ] = {1,3,6,1,2,1,31,1,1,1,6};
UINT4 IfHCInUcastPkts [ ] = {1,3,6,1,2,1,31,1,1,1,7};
UINT4 IfHCInMulticastPkts [ ] = {1,3,6,1,2,1,31,1,1,1,8};
UINT4 IfHCInBroadcastPkts [ ] = {1,3,6,1,2,1,31,1,1,1,9};
UINT4 IfHCOutOctets [ ] = {1,3,6,1,2,1,31,1,1,1,10};
UINT4 IfHCOutUcastPkts [ ] = {1,3,6,1,2,1,31,1,1,1,11};
UINT4 IfHCOutMulticastPkts [ ] = {1,3,6,1,2,1,31,1,1,1,12};
UINT4 IfHCOutBroadcastPkts [ ] = {1,3,6,1,2,1,31,1,1,1,13};
UINT4 IfLinkUpDownTrapEnable [ ] = {1,3,6,1,2,1,31,1,1,1,14};
UINT4 IfHighSpeed [ ] = {1,3,6,1,2,1,31,1,1,1,15};
UINT4 IfPromiscuousMode [ ] = {1,3,6,1,2,1,31,1,1,1,16};
UINT4 IfConnectorPresent [ ] = {1,3,6,1,2,1,31,1,1,1,17};
UINT4 IfAlias [ ] = {1,3,6,1,2,1,31,1,1,1,18};
UINT4 IfCounterDiscontinuityTime [ ] = {1,3,6,1,2,1,31,1,1,1,19};
UINT4 IfStackHigherLayer [ ] = {1,3,6,1,2,1,31,1,2,1,1};
UINT4 IfStackLowerLayer [ ] = {1,3,6,1,2,1,31,1,2,1,2};
UINT4 IfStackStatus [ ] = {1,3,6,1,2,1,31,1,2,1,3};
UINT4 IfRcvAddressAddress [ ] = {1,3,6,1,2,1,31,1,4,1,1};
UINT4 IfRcvAddressStatus [ ] = {1,3,6,1,2,1,31,1,4,1,2};
UINT4 IfRcvAddressType [ ] = {1,3,6,1,2,1,31,1,4,1,3};


tMbDbEntry ifmibMibEntry[]= {

{{8,IfNumber}, NULL, IfNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,IfIndex}, GetNextIndexIfTable, IfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfDescr}, GetNextIndexIfTable, IfDescrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfType}, GetNextIndexIfTable, IfTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfMtu}, GetNextIndexIfTable, IfMtuGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfSpeed}, GetNextIndexIfTable, IfSpeedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfPhysAddress}, GetNextIndexIfTable, IfPhysAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfAdminStatus}, GetNextIndexIfTable, IfAdminStatusGet, IfAdminStatusSet, IfAdminStatusTest, IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfOperStatus}, GetNextIndexIfTable, IfOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfLastChange}, GetNextIndexIfTable, IfLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfInOctets}, GetNextIndexIfTable, IfInOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfInUcastPkts}, GetNextIndexIfTable, IfInUcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfInNUcastPkts}, GetNextIndexIfTable, IfInNUcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfInDiscards}, GetNextIndexIfTable, IfInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfInErrors}, GetNextIndexIfTable, IfInErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfInUnknownProtos}, GetNextIndexIfTable, IfInUnknownProtosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfOutOctets}, GetNextIndexIfTable, IfOutOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfOutUcastPkts}, GetNextIndexIfTable, IfOutUcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfOutNUcastPkts}, GetNextIndexIfTable, IfOutNUcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfOutDiscards}, GetNextIndexIfTable, IfOutDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfOutErrors}, GetNextIndexIfTable, IfOutErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfOutQLen}, GetNextIndexIfTable, IfOutQLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{10,IfSpecific}, GetNextIndexIfTable, IfSpecificGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, IfTableINDEX, 1, 0, 0, NULL},

{{11,IfName}, GetNextIndexIfXTable, IfNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, IfXTableINDEX, 1, 0, 0, NULL},

{{11,IfInMulticastPkts}, GetNextIndexIfXTable, IfInMulticastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IfXTableINDEX, 1, 0, 0, NULL},

{{11,IfInBroadcastPkts}, GetNextIndexIfXTable, IfInBroadcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IfXTableINDEX, 1, 0, 0, NULL},

{{11,IfOutMulticastPkts}, GetNextIndexIfXTable, IfOutMulticastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IfXTableINDEX, 1, 0, 0, NULL},

{{11,IfOutBroadcastPkts}, GetNextIndexIfXTable, IfOutBroadcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IfXTableINDEX, 1, 0, 0, NULL},

{{11,IfHCInOctets}, GetNextIndexIfXTable, IfHCInOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IfXTableINDEX, 1, 0, 0, NULL},

{{11,IfHCInUcastPkts}, GetNextIndexIfXTable, IfHCInUcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IfXTableINDEX, 1, 0, 0, NULL},

{{11,IfHCInMulticastPkts}, GetNextIndexIfXTable, IfHCInMulticastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IfXTableINDEX, 1, 0, 0, NULL},

{{11,IfHCInBroadcastPkts}, GetNextIndexIfXTable, IfHCInBroadcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IfXTableINDEX, 1, 0, 0, NULL},

{{11,IfHCOutOctets}, GetNextIndexIfXTable, IfHCOutOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IfXTableINDEX, 1, 0, 0, NULL},

{{11,IfHCOutUcastPkts}, GetNextIndexIfXTable, IfHCOutUcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IfXTableINDEX, 1, 0, 0, NULL},

{{11,IfHCOutMulticastPkts}, GetNextIndexIfXTable, IfHCOutMulticastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IfXTableINDEX, 1, 0, 0, NULL},

{{11,IfHCOutBroadcastPkts}, GetNextIndexIfXTable, IfHCOutBroadcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IfXTableINDEX, 1, 0, 0, NULL},

{{11,IfLinkUpDownTrapEnable}, GetNextIndexIfXTable, IfLinkUpDownTrapEnableGet, IfLinkUpDownTrapEnableSet, IfLinkUpDownTrapEnableTest, IfXTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfXTableINDEX, 1, 0, 0, NULL},

{{11,IfHighSpeed}, GetNextIndexIfXTable, IfHighSpeedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, IfXTableINDEX, 1, 0, 0, NULL},

{{11,IfPromiscuousMode}, GetNextIndexIfXTable, IfPromiscuousModeGet, IfPromiscuousModeSet, IfPromiscuousModeTest, IfXTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfXTableINDEX, 1, 0, 0, NULL},

{{11,IfConnectorPresent}, GetNextIndexIfXTable, IfConnectorPresentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IfXTableINDEX, 1, 0, 0, NULL},

{{11,IfAlias}, GetNextIndexIfXTable, IfAliasGet, IfAliasSet, IfAliasTest, IfXTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IfXTableINDEX, 1, 0, 0, NULL},

{{11,IfCounterDiscontinuityTime}, GetNextIndexIfXTable, IfCounterDiscontinuityTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IfXTableINDEX, 1, 0, 0, NULL},

{{11,IfStackHigherLayer}, GetNextIndexIfStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IfStackTableINDEX, 2, 0, 0, NULL},

{{11,IfStackLowerLayer}, GetNextIndexIfStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IfStackTableINDEX, 2, 0, 0, NULL},

{{11,IfStackStatus}, GetNextIndexIfStackTable, IfStackStatusGet, IfStackStatusSet, IfStackStatusTest, IfStackTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfStackTableINDEX, 2, 0, 1, NULL},

{{11,IfRcvAddressAddress}, GetNextIndexIfRcvAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, IfRcvAddressTableINDEX, 2, 0, 0, NULL},

{{11,IfRcvAddressStatus}, GetNextIndexIfRcvAddressTable, IfRcvAddressStatusGet, IfRcvAddressStatusSet, IfRcvAddressStatusTest, IfRcvAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfRcvAddressTableINDEX, 2, 0, 1, NULL},

{{11,IfRcvAddressType}, GetNextIndexIfRcvAddressTable, IfRcvAddressTypeGet, IfRcvAddressTypeSet, IfRcvAddressTypeTest, IfRcvAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfRcvAddressTableINDEX, 2, 0, 0, "2"},

{{9,IfTableLastChange}, NULL, IfTableLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,IfStackLastChange}, NULL, IfStackLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData ifmibEntry = { 50, ifmibMibEntry };
#endif /* _IFMIBDB_H */

