#ifndef _STDETHWR_H
#define _STDETHWR_H
INT4 GetNextIndexDot3StatsTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTDETH(VOID);

VOID UnRegisterSTDETH(VOID);
INT4 Dot3StatsIndexGet(tSnmpIndex *, tRetVal *);
INT4 Dot3StatsAlignmentErrorsGet(tSnmpIndex *, tRetVal *);
INT4 Dot3StatsFCSErrorsGet(tSnmpIndex *, tRetVal *);
INT4 Dot3StatsSingleCollisionFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot3StatsMultipleCollisionFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot3StatsSQETestErrorsGet(tSnmpIndex *, tRetVal *);
INT4 Dot3StatsDeferredTransmissionsGet(tSnmpIndex *, tRetVal *);
INT4 Dot3StatsLateCollisionsGet(tSnmpIndex *, tRetVal *);
INT4 Dot3StatsExcessiveCollisionsGet(tSnmpIndex *, tRetVal *);
INT4 Dot3StatsInternalMacTransmitErrorsGet(tSnmpIndex *, tRetVal *);
INT4 Dot3StatsCarrierSenseErrorsGet(tSnmpIndex *, tRetVal *);
INT4 Dot3StatsFrameTooLongsGet(tSnmpIndex *, tRetVal *);
INT4 Dot3StatsInternalMacReceiveErrorsGet(tSnmpIndex *, tRetVal *);
INT4 Dot3StatsEtherChipSetGet(tSnmpIndex *, tRetVal *);
INT4 Dot3StatsSymbolErrorsGet(tSnmpIndex *, tRetVal *);
INT4 Dot3StatsDuplexStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot3StatsRateControlAbilityGet(tSnmpIndex *, tRetVal *);
INT4 Dot3StatsRateControlStatusGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot3CollTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot3CollCountGet(tSnmpIndex *, tRetVal *);
INT4 Dot3CollFrequenciesGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot3ControlTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot3ControlFunctionsSupportedGet(tSnmpIndex *, tRetVal *);
INT4 Dot3ControlInUnknownOpcodesGet(tSnmpIndex *, tRetVal *);
INT4 Dot3HCControlInUnknownOpcodesGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot3PauseTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot3PauseAdminModeGet(tSnmpIndex *, tRetVal *);
INT4 Dot3PauseOperModeGet(tSnmpIndex *, tRetVal *);
INT4 Dot3InPauseFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OutPauseFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot3HCInPauseFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot3HCOutPauseFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot3PauseAdminModeSet(tSnmpIndex *, tRetVal *);
INT4 Dot3PauseAdminModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3PauseTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexDot3HCStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot3HCStatsAlignmentErrorsGet(tSnmpIndex *, tRetVal *);
INT4 Dot3HCStatsFCSErrorsGet(tSnmpIndex *, tRetVal *);
INT4 Dot3HCStatsInternalMacTransmitErrorsGet(tSnmpIndex *, tRetVal *);
INT4 Dot3HCStatsFrameTooLongsGet(tSnmpIndex *, tRetVal *);
INT4 Dot3HCStatsInternalMacReceiveErrorsGet(tSnmpIndex *, tRetVal *);
INT4 Dot3HCStatsSymbolErrorsGet(tSnmpIndex *, tRetVal *);

#endif /* _STDETHWR_H */
