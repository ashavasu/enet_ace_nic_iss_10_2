#ifndef _FSTUNLWR_H
#define _FSTUNLWR_H
INT4 GetNextIndexFsTunlIfTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSTUNL(VOID);
INT4 FsTunlIfAddressTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfLocalInetAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfRemoteInetAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfEncapsMethodGet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfConfigIDGet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfHopLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfSecurityGet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfTOSGet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfFlowLabelGet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfMTUGet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfDirFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfDirectionGet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfEncaplmtGet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfEncapOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfAliasGet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfCksumFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfPmtuFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfHopLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfTOSSet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfFlowLabelSet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfDirFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfDirectionSet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfEncaplmtSet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfEncapOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfAliasSet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfCksumFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfPmtuFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsTunlIfHopLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTunlIfTOSTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTunlIfFlowLabelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTunlIfDirFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTunlIfDirectionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTunlIfEncaplmtTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTunlIfEncapOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTunlIfAliasTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTunlIfCksumFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTunlIfPmtuFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTunlIfStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTunlIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);











#endif /* _FSTUNLWR_H */
