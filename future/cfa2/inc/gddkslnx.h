/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddkslnx.h,v 1.1 2014/10/27 10:58:32 siva Exp $
 *
 * Description:This file contains the prototypes for all
 *             the functions of the CFA system with respect to 
 *             kernel space for linux
 *
 *******************************************************************/

#ifndef _GDDKSEC_LNX_H
#define _GDDKSEC_LNX_H

#define SEC_DEF_BOND_DEV_0 "eth0"
#define SEC_DEF_BOND_DEV_1 "eth1"

#endif


