/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdethdb.h,v 1.8 2008/12/31 06:01:29 premap-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDETHDB_H
#define _STDETHDB_H

UINT1 Dot3StatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot3CollTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot3ControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot3PauseTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot3HCStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 stdeth [] ={1,3,6,1,2,1,10,7,2};
tSNMP_OID_TYPE stdethOID = {9, stdeth};


UINT4 Dot3StatsIndex [ ] ={1,3,6,1,2,1,10,7,2,1,1};
UINT4 Dot3StatsAlignmentErrors [ ] ={1,3,6,1,2,1,10,7,2,1,2};
UINT4 Dot3StatsFCSErrors [ ] ={1,3,6,1,2,1,10,7,2,1,3};
UINT4 Dot3StatsSingleCollisionFrames [ ] ={1,3,6,1,2,1,10,7,2,1,4};
UINT4 Dot3StatsMultipleCollisionFrames [ ] ={1,3,6,1,2,1,10,7,2,1,5};
UINT4 Dot3StatsSQETestErrors [ ] ={1,3,6,1,2,1,10,7,2,1,6};
UINT4 Dot3StatsDeferredTransmissions [ ] ={1,3,6,1,2,1,10,7,2,1,7};
UINT4 Dot3StatsLateCollisions [ ] ={1,3,6,1,2,1,10,7,2,1,8};
UINT4 Dot3StatsExcessiveCollisions [ ] ={1,3,6,1,2,1,10,7,2,1,9};
UINT4 Dot3StatsInternalMacTransmitErrors [ ] ={1,3,6,1,2,1,10,7,2,1,10};
UINT4 Dot3StatsCarrierSenseErrors [ ] ={1,3,6,1,2,1,10,7,2,1,11};
UINT4 Dot3StatsFrameTooLongs [ ] ={1,3,6,1,2,1,10,7,2,1,13};
UINT4 Dot3StatsInternalMacReceiveErrors [ ] ={1,3,6,1,2,1,10,7,2,1,16};
UINT4 Dot3StatsEtherChipSet [ ] ={1,3,6,1,2,1,10,7,2,1,17};
UINT4 Dot3StatsSymbolErrors [ ] ={1,3,6,1,2,1,10,7,2,1,18};
UINT4 Dot3StatsDuplexStatus [ ] ={1,3,6,1,2,1,10,7,2,1,19};
UINT4 Dot3StatsRateControlAbility [ ] ={1,3,6,1,2,1,10,7,2,1,20};
UINT4 Dot3StatsRateControlStatus [ ] ={1,3,6,1,2,1,10,7,2,1,21};
UINT4 Dot3CollCount [ ] ={1,3,6,1,2,1,10,7,5,1,2};
UINT4 Dot3CollFrequencies [ ] ={1,3,6,1,2,1,10,7,5,1,3};
UINT4 Dot3ControlFunctionsSupported [ ] ={1,3,6,1,2,1,10,7,9,1,1};
UINT4 Dot3ControlInUnknownOpcodes [ ] ={1,3,6,1,2,1,10,7,9,1,2};
UINT4 Dot3HCControlInUnknownOpcodes [ ] ={1,3,6,1,2,1,10,7,9,1,3};
UINT4 Dot3PauseAdminMode [ ] ={1,3,6,1,2,1,10,7,10,1,1};
UINT4 Dot3PauseOperMode [ ] ={1,3,6,1,2,1,10,7,10,1,2};
UINT4 Dot3InPauseFrames [ ] ={1,3,6,1,2,1,10,7,10,1,3};
UINT4 Dot3OutPauseFrames [ ] ={1,3,6,1,2,1,10,7,10,1,4};
UINT4 Dot3HCInPauseFrames [ ] ={1,3,6,1,2,1,10,7,10,1,5};
UINT4 Dot3HCOutPauseFrames [ ] ={1,3,6,1,2,1,10,7,10,1,6};
UINT4 Dot3HCStatsAlignmentErrors [ ] ={1,3,6,1,2,1,10,7,11,1,1};
UINT4 Dot3HCStatsFCSErrors [ ] ={1,3,6,1,2,1,10,7,11,1,2};
UINT4 Dot3HCStatsInternalMacTransmitErrors [ ] ={1,3,6,1,2,1,10,7,11,1,3};
UINT4 Dot3HCStatsFrameTooLongs [ ] ={1,3,6,1,2,1,10,7,11,1,4};
UINT4 Dot3HCStatsInternalMacReceiveErrors [ ] ={1,3,6,1,2,1,10,7,11,1,5};
UINT4 Dot3HCStatsSymbolErrors [ ] ={1,3,6,1,2,1,10,7,11,1,6};


tMbDbEntry stdethMibEntry[]= {

{{11,Dot3StatsIndex}, GetNextIndexDot3StatsTable, Dot3StatsIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3StatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3StatsAlignmentErrors}, GetNextIndexDot3StatsTable, Dot3StatsAlignmentErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3StatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3StatsFCSErrors}, GetNextIndexDot3StatsTable, Dot3StatsFCSErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3StatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3StatsSingleCollisionFrames}, GetNextIndexDot3StatsTable, Dot3StatsSingleCollisionFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3StatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3StatsMultipleCollisionFrames}, GetNextIndexDot3StatsTable, Dot3StatsMultipleCollisionFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3StatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3StatsSQETestErrors}, GetNextIndexDot3StatsTable, Dot3StatsSQETestErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3StatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3StatsDeferredTransmissions}, GetNextIndexDot3StatsTable, Dot3StatsDeferredTransmissionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3StatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3StatsLateCollisions}, GetNextIndexDot3StatsTable, Dot3StatsLateCollisionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3StatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3StatsExcessiveCollisions}, GetNextIndexDot3StatsTable, Dot3StatsExcessiveCollisionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3StatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3StatsInternalMacTransmitErrors}, GetNextIndexDot3StatsTable, Dot3StatsInternalMacTransmitErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3StatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3StatsCarrierSenseErrors}, GetNextIndexDot3StatsTable, Dot3StatsCarrierSenseErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3StatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3StatsFrameTooLongs}, GetNextIndexDot3StatsTable, Dot3StatsFrameTooLongsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3StatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3StatsInternalMacReceiveErrors}, GetNextIndexDot3StatsTable, Dot3StatsInternalMacReceiveErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3StatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3StatsEtherChipSet}, GetNextIndexDot3StatsTable, Dot3StatsEtherChipSetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, Dot3StatsTableINDEX, 1, 1, 0, NULL},

{{11,Dot3StatsSymbolErrors}, GetNextIndexDot3StatsTable, Dot3StatsSymbolErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3StatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3StatsDuplexStatus}, GetNextIndexDot3StatsTable, Dot3StatsDuplexStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3StatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3StatsRateControlAbility}, GetNextIndexDot3StatsTable, Dot3StatsRateControlAbilityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3StatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3StatsRateControlStatus}, GetNextIndexDot3StatsTable, Dot3StatsRateControlStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3StatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3CollCount}, GetNextIndexDot3CollTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot3CollTableINDEX, 2, 0, 0, NULL},

{{11,Dot3CollFrequencies}, GetNextIndexDot3CollTable, Dot3CollFrequenciesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3CollTableINDEX, 2, 0, 0, NULL},

{{11,Dot3ControlFunctionsSupported}, GetNextIndexDot3ControlTable, Dot3ControlFunctionsSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot3ControlTableINDEX, 1, 0, 0, NULL},

{{11,Dot3ControlInUnknownOpcodes}, GetNextIndexDot3ControlTable, Dot3ControlInUnknownOpcodesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3ControlTableINDEX, 1, 0, 0, NULL},

{{11,Dot3HCControlInUnknownOpcodes}, GetNextIndexDot3ControlTable, Dot3HCControlInUnknownOpcodesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot3ControlTableINDEX, 1, 0, 0, NULL},

{{11,Dot3PauseAdminMode}, GetNextIndexDot3PauseTable, Dot3PauseAdminModeGet, Dot3PauseAdminModeSet, Dot3PauseAdminModeTest, Dot3PauseTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3PauseTableINDEX, 1, 0, 0, NULL},

{{11,Dot3PauseOperMode}, GetNextIndexDot3PauseTable, Dot3PauseOperModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3PauseTableINDEX, 1, 0, 0, NULL},

{{11,Dot3InPauseFrames}, GetNextIndexDot3PauseTable, Dot3InPauseFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3PauseTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OutPauseFrames}, GetNextIndexDot3PauseTable, Dot3OutPauseFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3PauseTableINDEX, 1, 0, 0, NULL},

{{11,Dot3HCInPauseFrames}, GetNextIndexDot3PauseTable, Dot3HCInPauseFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot3PauseTableINDEX, 1, 0, 0, NULL},

{{11,Dot3HCOutPauseFrames}, GetNextIndexDot3PauseTable, Dot3HCOutPauseFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot3PauseTableINDEX, 1, 0, 0, NULL},

{{11,Dot3HCStatsAlignmentErrors}, GetNextIndexDot3HCStatsTable, Dot3HCStatsAlignmentErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot3HCStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3HCStatsFCSErrors}, GetNextIndexDot3HCStatsTable, Dot3HCStatsFCSErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot3HCStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3HCStatsInternalMacTransmitErrors}, GetNextIndexDot3HCStatsTable, Dot3HCStatsInternalMacTransmitErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot3HCStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3HCStatsFrameTooLongs}, GetNextIndexDot3HCStatsTable, Dot3HCStatsFrameTooLongsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot3HCStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3HCStatsInternalMacReceiveErrors}, GetNextIndexDot3HCStatsTable, Dot3HCStatsInternalMacReceiveErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot3HCStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3HCStatsSymbolErrors}, GetNextIndexDot3HCStatsTable, Dot3HCStatsSymbolErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot3HCStatsTableINDEX, 1, 0, 0, NULL},
};
tMibData stdethEntry = { 35, stdethMibEntry };
#endif /* _STDETHDB_H */

