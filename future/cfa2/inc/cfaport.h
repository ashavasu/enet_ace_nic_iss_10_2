/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfaport.h,v 1.69 2017/10/10 11:57:20 siva Exp $
 *
 * Description:This file contains the definitions and    
 *             MACROS for the porting of the CFA system.    
 *
 *******************************************************************/
#ifndef _CFAPORT_H
#define _CFAPORT_H


/***************** Values used for Task, IPC and Timers *********************/
/* for indicating PPP bypass status to CFA */
#define   ACFC_PLAIN_RX      0x1
#define   NON_PLAIN_RX      0x10
#define   NON_PLAIN_TX      0x20


#define   CFA_TASK_ID               gCfaTaskId
#define   CFA_AST_PKTTX_TASK_ID     gCfaTxPktTaskId
#define   CFA_INITIALISED           gu1CfaInitialised
#define   CFA_DEFAULT_VLAN_ID       gCfaDefautVlanId
#ifdef CFA_DEBUG
#define   CFA_DEFAULT_DEBUG_LEVEL   CFA_TRC_ALL     /* full debug mode */
#else /* CFA_DEBUG mode */
#define   CFA_DEFAULT_DEBUG_LEVEL   CFA_TRC_NONE     /* no debug-trace */
#endif /* no CFA_DEBUG mode */

/* resources for which faults are handled by FM */
#define   CFA_MEMORY_RESOURCE   0 
#define   CFA_TIMER_RESOURCE    1 

#define   CFA_FM                    "FM"         
#define   CFA_FFM                   "FFM"        
#define   CFA_IFM                   "IFM"        
#define   CFA_IWF                   "IWF"        
#define   CFA_GDD                   "GDD"        
#define   CFA_MAIN                  "CFA"        
#define   CFA_IF                    "CFAIF"
#define   CFA_UFD                   "CFAUFD"

#define   CFA_TUNL                  "TNL"        
#define   CFA_L2                    "L2"

/* global flag & Module names for tracing */
#define   CFA_GLB_TRC()   (gu4CfaTraceLevel) 
#define   CFA_GLB_DBG_LVL()   (gu4CfaDebugLevel) 
#define   CFA_UNUSED_PARAM(x) {\
                               INT4 y;\
                               y=x;\
                              }\

#define CFA_RECEIVE_EVENT             OsixEvtRecv

#define CFA_SEND_EVENT                OsixEvtSend

#define CFA_SPAWN_TASK                OsixTskCrt

#define CFA_DELETE_TASK               OsixTskDel 

#ifdef TRACE_WANTED
/* MACROS for Debug-Trace Mechanism */
#define   CFA_DBG(Flag, Module, Fmt) \
          UtlTrcLog(CFA_GLB_TRC(), Flag, Module, Fmt)

#define   CFA_DBG1(Flag, Module, Fmt, Arg) \
          UtlTrcLog(CFA_GLB_TRC(), Flag, Module, Fmt, Arg)

#define   CFA_DBG2(Flag, Module, Fmt, Arg1, Arg2) \
          UtlTrcLog(CFA_GLB_TRC(), Flag, Module, Fmt, Arg1, Arg2)

#define   CFA_DBG3(Flag, Module, Fmt, Arg1, Arg2, Arg3) \
          UtlTrcLog(CFA_GLB_TRC(), Flag, Module, Fmt, Arg1, Arg2, Arg3)

#define   CFA_DBG4(Flag, Module, Fmt, Arg1, Arg2, Arg3, Arg4) \
          UtlTrcLog(CFA_GLB_TRC(), Flag, Module, Fmt, Arg1, Arg2, Arg3, Arg4)

#define   CFA_DBG5(Flag, Module, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5) \
          UtlTrcLog(CFA_GLB_TRC(), Flag, Module, Fmt,\
          Arg1, Arg2, Arg3, Arg4, Arg5)

#define   CFA_DBG6(Flag, Module, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) \
          UtlTrcLog(CFA_GLB_TRC(), Flag, Module, Fmt,\
          Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)

#define   CFA_DBG7(Flag, Module, Fmt,\
          Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7) \
          UtlTrcLog(CFA_GLB_TRC(), Flag, Module, Fmt,\
          Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)

#define   CFA_DBG8(Flag, Module, Fmt, \
          Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8) \
          UtlTrcLog(CFA_GLB_TRC(), Flag, Module, Fmt, \
          Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)

#else
#define   CFA_DBG(Flag, Module, Fmt) CFA_UNUSED_PARAM(Flag)

#define   CFA_DBG1(Flag, Module, Fmt, Arg) CFA_UNUSED_PARAM(Flag)

#define   CFA_DBG2(Flag, Module, Fmt, Arg1, Arg2) \
{\
    CFA_UNUSED_PARAM(Flag); \
    CFA_UNUSED_PARAM(Arg1); \
    CFA_UNUSED_PARAM(Arg2); \
}
#define   CFA_DBG3(Flag, Module, Fmt, Arg1, Arg2, Arg3) CFA_UNUSED_PARAM(Flag)

#define   CFA_DBG4(Flag, Module, Fmt, Arg1, Arg2, Arg3, Arg4) \
          CFA_UNUSED_PARAM(Flag)

#define   CFA_DBG5(Flag, Module, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5) \
          CFA_UNUSED_PARAM(Flag)

#define   CFA_DBG6(Flag, Module, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) \
          CFA_UNUSED_PARAM(Flag)

#define   CFA_DBG7(Flag, Module, Fmt,\
          Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7) CFA_UNUSED_PARAM(Flag)

#define   CFA_DBG8(Flag, Module, Fmt, \
          Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8) CFA_UNUSED_PARAM(Flag)
#endif
/* MACRO for Packet or Message dumping - we dump in both directions and the
full packet or message. */
#define   CFA_DUMP(Flag, pBuf) \
          UtlDmpMsg(CFA_GLB_TRC(), 0, pBuf, Flag, 0, CFA_MAX_U4_VALUE, FALSE)


/* CFA's task Id, name, priority, semaphore etc. */
#define   CFA_TASK_PRIORITY   20        

#define   CFA_PACKET_MUX_QID    gCfaPktMuxQId
#define   CFA_PACKET_QID        gCfaPktQId
#define   CFA_AST_PACKET_QID    gCfaTxPktQId
#define   CFA_PACKET_PLIST_QID  gCfaPktPlQId
#define   CFA_EXT_TRIGGER_QID   gCfaExtTrgrQId
#define   CFA_PACKET_PARAMS_QID gCfaPktParamsQId
/* Id of the queue created for handling attack packets */
#define   CFA_ATTACK_PACKET_QID gCfaAttackPktQId

#define   CFA_PROTOCOL_LOCK   (CONST UINT1 *) "CFAP"
#define   CFA_PKTTSK_LOCK   (CONST UINT1 *) "PKT"
#define   CFA_AST_PKTTSK_LOCK (CONST UINT1 *) "AST_PKT"

/* the poll timeout is given in terms of msec - we scale it according to
 * the STUPS given to LR 
 */
#define   CFA_GDD_POLL_TIMEOUT    1

/* CFA task's Queues, events, commands and timer list */
#define   CFA_EVENT_WAIT_FLAGS        OSIX_WAIT 
#define   CFA_EVENT_WAIT_TIMEOUT      0                         
#define   CFA_STACK_SIZE              20000                     

#define   CFA_PACKET_QUEUE            "CFA0"                    
#define   CFA_PACKET_PORTLIST_QUEUE   "CFA1"                    
#define   CFA_EXT_TRIGGER_QUEUE       "CFAX" 
#define   CFA_PKT_PARAMS_QUEUE        "CFA2"
/* Name of the queue created for handling attack packets */
#define   CFA_ATTACK_PKT_QUEUE     "CFA3"
#define   CFA_AST_PACKET_QUEUE     "CFA4"

#if defined (IGS_WANTED) || defined (MLDS_WANTED)
#ifdef ISS_WANTED
#define   CFA_PACKET_QUEUE_DEPTH      (SNOOP_MAX_IGMP_PKTS_PROCESSED_IN_A_SEC + CFA_NUM_INTERFACES_IN_SYSTEM)
#else
#define   CFA_PACKET_QUEUE_DEPTH     20
#endif
#else
#ifdef ISS_WANTED
#define   CFA_PACKET_QUEUE_DEPTH      (20 + CFA_NUM_INTERFACES_IN_SYSTEM)
#else
#define   CFA_PACKET_QUEUE_DEPTH     20
#endif
#endif
/* Depth of the queue created for handling attack packets 
 * Only for logging purpose these attack packets are coming to CPU.
 * So a queue with minimum queue depth (20)is enough*/
#define   CFA_ATTACK_PKT_Q_DEPTH      20


/* Event Definition */
#define   CFA_PACKET_ARRIVAL_EVENT          0x00000001                
#define   CFA_GDD_POLL_TMR_EXP_EVENT        0x00000002                
#define   CFA_IP_UP_EVENT                   0x00000004
/* NAT_TIMER_EXPIRY_EVENT is defined in nat.h with value 0x00000008 */
#define   CFA_BOOTUP_START_EVENT            0x00000010
#define   CFA_LINKUP_DELAY_TMR_EXP_EVENT    0x00000020
#define   CFA_TUNL_PKT_RECV_EVENT           0x00000100
#define   CFA_GRE_TUNL_PKT_RECV_EVENT       0x00000200
#ifdef    CFA_INTERRUPT_MODE
#define   CFA_GDD_INTERRUPT_EVENT           0x00010000
#endif
#define   CFA_EXT_TRIGGER_EVENT             0x00020000                
#define   CFA_PACKET_PROCESS_START_EVENT    0x00200000
#define   CFA_PACKET_PORTLIST_ARRIVAL_EVENT 0x00040000
#define   CFA_PORT_CHANNEL_DELETE_EVENT     0x00080000
#define   CFA_OOB_DATA_RCVD_EVENT           0x00100000
#define   TAP_IF_PKT_EVENT                  0x01000000 
/* IPSEC_TIMER_EXPIRY_EVENT is defined in sec.h with value 0x40000000 */
#define   CFA_SUB_BULK_UPDT_EVENT           0x80000000
/* Event to identify the reception of attack packet */
#define   CFA_ATTACK_PACKET_RX_EVENT        0x10000000
#define   CFA_AST_BPDU_EVENT           0x00000011
/* Message Types for external trigger events */ 
#define   CFA_LINK_STATUS_CHANGE_MSG      1
#define   CFA_PORT_CHANNEL_DELETE_MSG     2
/* Message Type for external trigger events
 * related to dynamic s-channel interface */
#define CFA_CREATE_DYN_SCH_IF 11
#define CFA_DELETE_DYN_SCH_IF 12
#define CFA_SCH_OPER_STATUS_MSG 13
 /* Message for configurig Ip Information for the 
    Interface. */
#define   CFA_IPINFO_CONFIG_MSG           3
#define   CFA_RM_MESSAGE                  4
#define   CFA_BASE_BRIDGE_MODE_MSG        5
#define   CFA_VIP_OPER_STATUS_MSG         6
#define   CFA_IF_MTU_UPDATE_CHANGE_MSG    7
#define   CFA_ADD_PORT_TO_PORT_CHANNEL_MSG 8
#define   CFA_PORT_CHANNEL_OPER_MSG        9
#define   CFA_MCLAG_STATUS_CHANGE_MSG      10

#define   CFA_HL_PORT_OPER_IND_MSG         25
#define   CFA_LA_PORT_CREATE_IND_MSG       26

#define   CFA_GDD_TMR_LIST            CfaGddTimerListId       
#define   CFA_LINKUP_DELAY_TMR_LIST   CfaLinkUpDelayTimerListId       

/****************************************************************************/

/* Definitions */
#define   CFA_MIN_PHYS_INTERFACES_IN_SYS   1     
#define   CFA_MIN_INTERFACES_IN_SYS        10

#define   CFA_ROUTER_IFINDEX                1         /* Default interface's 
                                                       * ifIndex 
                                                       */
#define   CFA_DEFAULT_VLAN_INTERFACE  "vlan"

#ifdef WGS_WANTED
#define   CFA_DEFAULT_MGMT_INTERFACE  "vlanMgmt"
#endif /* WGS_WANTED */

#define   CFA_MAX_FRAME_HEADER_SIZE        22         /* currently only takes
                                                       * Enet into 
                                                       * account 
                                                       */
#define   CFA_MAX_DRIVER_MTU               9188       /* For AAL5 PDU along with
                                                       * LLC-SNAP header 
                                                       */
#define   CFA_MAX_DEMAND_CKT_QLEN          15         /* the max number of 
                                                       * packets that should be
                                                       * queued while a demand
                                                       * circuit is being opened                                                       */

/* macros for accessing the Module data */
/* change this size to the sizeof user data */
#define   SIZE_OF_MODULE_DATA   (sizeof(tCfaModuleData)) 

/* modify the macro below to point to the start of user data */
#define   CFA_CLEAR_MODULE_DATA(pBuf) \
          (MEMSET(GET_MODULE_DATA_PTR(pBuf), 0, SIZE_OF_MODULE_DATA))

#define   CFA_GET_IFINDEX(pBuf)   (GET_MODULE_DATA_PTR(pBuf)->u4IfIndex) 

#define   CFA_SET_IFINDEX(pBuf, u4IfId) \
          (GET_MODULE_DATA_PTR(pBuf)->u4IfIndex = u4IfId)

#define   CFA_GET_PROTOCOL(pBuf) \
          ((UINT2)(GET_MODULE_DATA_PTR(pBuf)->u4Protocol))

#define   CFA_SET_PROTOCOL(pBuf, u2Prot) \
          (GET_MODULE_DATA_PTR(pBuf)->u4Protocol = (UINT4)u2Prot)

#define   CFA_GET_PKT_TYPE(pBuf) \
          (GET_MODULE_DATA_PTR(pBuf)->u1PktType)

#define   CFA_SET_PKT_TYPE(pBuf, u1PType) \
          (GET_MODULE_DATA_PTR(pBuf)->u1PktType = u1PType)

#define   CFA_GET_NH_IP_ADDR(pBuf,au1UserData) \
          MEMCPY(au1UserData,&(GET_MODULE_DATA_PTR(pBuf)->u4NextHopIpAddr), \
          CFA_IP_ADDR_SIZE)

#define   CFA_SET_NH_IP_ADDR(pBuf,au1UserData) \
          MEMCPY(&(GET_MODULE_DATA_PTR(pBuf)->u4NextHopIpAddr),au1UserData, \
          CFA_IP_ADDR_SIZE)

#define   CFA_GET_COMMAND(pBuf)   (GET_MODULE_DATA_PTR(pBuf)->u1Command) 

#define   CFA_SET_COMMAND(pBuf, u1Cmd) \
          (GET_MODULE_DATA_PTR(pBuf)->u1Command = u1Cmd)

#define   CFA_GET_DEST_MEDIA_ADDR(pBuf, au1DestHwAddr) \
          MEMCPY(au1DestHwAddr,GET_MODULE_DATA_PTR(pBuf)->au1NextHopMediaAddr, \
          CFA_ENET_ADDR_LEN)

#define   CFA_SET_DEST_MEDIA_ADDR(pBuf, au1DestHwAddr) \
          MEMCPY(GET_MODULE_DATA_PTR(pBuf)->au1NextHopMediaAddr,au1DestHwAddr, \
          CFA_ENET_ADDR_LEN)

#define   CFA_GET_ENCAP_TYPE(pBuf)   (GET_MODULE_DATA_PTR(pBuf)->u1EncapType) 

#define   CFA_SET_ENCAP_TYPE(pBuf, u1EType) \
          (GET_MODULE_DATA_PTR(pBuf)->u1EncapType = u1EType)

#ifdef MBSM_WANTED
#define   CFA_GET_SLOT_COUNT(pBuf)   (GET_MODULE_DATA_PTR(pBuf)->u1SlotCount) 

#define   CFA_SET_SLOT_COUNT(pBuf, u1SlotCount) \
          (GET_MODULE_DATA_PTR(pBuf)->u1SlotCount = u1SlotCount)
#endif
   
/* commands for Interface layering through tIfLayerInfo struct*/
#define   CFA_LAYER_ADD                  1
#define   CFA_LAYER_DEL                  2
#define   CFA_LAYER_IGNORE               3

/* For adding MCAST addresses to Ethernet */
#define   CFA_ADD_IP                     1
#define   CFA_DEL_IP                     3
#define   CFA_ADD_ALL_MULTI              5
#define   CFA_DEL_ALL_MULTI              6

/* type of encaps - same as MIB variable enums */
#define   CFA_ENCAP_NLPIP_SNAP           3
#define   CFA_ENCAP_CUD_NLPID            4
#define   CFA_ENCAP_CUD_NLPID_SNAP       5

#define   CFA_PERS_OTHER                 1
/* Protocol defines used for masking */
#define   CFA_PROT_IPV4                0x1
#define   CFA_PROT_ARP                 0x2
#define   CFA_PROT_RARP                0x4
#define   CFA_PROT_STAP               0x12

/* Flag to identify whether the system has mgmt port or not.
 * The value of this flag is set to TRUE, if the system has mgmt port,
 * set to FALSE, otherwise. */
#define   CFA_MGMT_PORT    gu4MgmtPort

#define   CFA_DEFAULT_ROUTER_IFINDEX   \
        ((CFA_MGMT_PORT == TRUE) ? CFA_OOB_MGMT_IFINDEX : CFA_ROUTER_IFINDEX)

#define CFA_MAX_PORT_SPEED        1000000000 /* This define should be
                                              * ported based on the
                                              * maximum speed 
                                              * supported by the 
                                              * ports of a switch 
                                              */

    /********************** SYSTEM SIZING *******************************/
   

#define CFA_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)

#ifdef PPP_WANTED
extern INT4 PppGetPeerIpAddress (UINT4, UINT4*);
#endif
#endif /* _CFAPORT_H */
