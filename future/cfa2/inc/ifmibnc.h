
/* $Id: ifmibnc.h,v 1.1 2016/06/16 12:09:43 siva Exp $
    ISS Wrapper header
    module IF-MIB

 */


#ifndef _H_i_IF_MIB
#define _H_i_IF_MIB
/********************************************************************
* FUNCTION NcIfNumberGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfNumberGet (
                INT4 *pi4IfNumber );

/********************************************************************
* FUNCTION NcIfTableLastChangeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfTableLastChangeGet (
                UINT4 *pu4IfTableLastChange );

/********************************************************************
* FUNCTION NcIfStackLastChangeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfStackLastChangeGet (
                UINT4 *pu4IfStackLastChange );

/********************************************************************
* FUNCTION NcIfDescrGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfDescrGet (
                INT4 i4IfIndex,
                UINT1 *pIfDescr );

/********************************************************************
* FUNCTION NcIfTypeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfTypeGet (
                INT4 i4IfIndex,
                INT4 *pi4IfType );

/********************************************************************
* FUNCTION NcIfMtuGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfMtuGet (
                INT4 i4IfIndex,
                INT4 *pi4IfMtu );

/********************************************************************
* FUNCTION NcIfSpeedGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfSpeedGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfSpeed );

/********************************************************************
* FUNCTION NcIfPhysAddressGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfPhysAddressGet (
                INT4 i4IfIndex,
                UINT1 *pIfPhysAddress );

/********************************************************************
* FUNCTION NcIfAdminStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfAdminStatusSet (
                INT4 i4IfIndex,
                INT4 i4IfAdminStatus );

/********************************************************************
* FUNCTION NcIfAdminStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfAdminStatusTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                INT4 i4IfAdminStatus );

/********************************************************************
* FUNCTION NcIfOperStatusGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfOperStatusGet (
                INT4 i4IfIndex,
                INT4 *pi4IfOperStatus );

/********************************************************************
* FUNCTION NcIfLastChangeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfLastChangeGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfLastChange );

/********************************************************************
* FUNCTION NcIfInOctetsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfInOctetsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfInOctets );

/********************************************************************
* FUNCTION NcIfInUcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfInUcastPktsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfInUcastPkts );

/********************************************************************
* FUNCTION NcIfInNUcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfInNUcastPktsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfInNUcastPkts );

/********************************************************************
* FUNCTION NcIfInDiscardsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfInDiscardsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfInDiscards );

/********************************************************************
* FUNCTION NcIfInErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfInErrorsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfInErrors );

/********************************************************************
* FUNCTION NcIfInUnknownProtosGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfInUnknownProtosGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfInUnknownProtos );

/********************************************************************
* FUNCTION NcIfOutOctetsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfOutOctetsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfOutOctets );

/********************************************************************
* FUNCTION NcIfOutUcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfOutUcastPktsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfOutUcastPkts );

/********************************************************************
* FUNCTION NcIfOutNUcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfOutNUcastPktsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfOutNUcastPkts );

/********************************************************************
* FUNCTION NcIfOutDiscardsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfOutDiscardsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfOutDiscards );

/********************************************************************
* FUNCTION NcIfOutErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfOutErrorsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfOutErrors );

/********************************************************************
* FUNCTION NcIfOutQLenGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfOutQLenGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfOutQLen );

/********************************************************************
* FUNCTION NcIfSpecificGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfSpecificGet (
                INT4 i4IfIndex,
                UINT1 *pIfSpecific );

/********************************************************************
* FUNCTION NcIfNameGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfNameGet (
                INT4 i4IfIndex,
                UINT1 *pIfName );

/********************************************************************
* FUNCTION NcIfInMulticastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfInMulticastPktsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfInMulticastPkts );

/********************************************************************
* FUNCTION NcIfInBroadcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfInBroadcastPktsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfInBroadcastPkts );

/********************************************************************
* FUNCTION NcIfOutMulticastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfOutMulticastPktsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfOutMulticastPkts );

/********************************************************************
* FUNCTION NcIfOutBroadcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfOutBroadcastPktsGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfOutBroadcastPkts );

/********************************************************************
* FUNCTION NcIfHCInOctetsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfHCInOctetsGet (
                INT4 i4IfIndex,
                unsigned long long *pu8IfHCInOctets );

/********************************************************************
* FUNCTION NcIfHCInUcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfHCInUcastPktsGet (
                INT4 i4IfIndex,
                unsigned long long *pu8IfHCInUcastPkts );

/********************************************************************
* FUNCTION NcIfHCInMulticastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfHCInMulticastPktsGet (
                INT4 i4IfIndex,
                unsigned long long *pu8IfHCInMulticastPkts );

/********************************************************************
* FUNCTION NcIfHCInBroadcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfHCInBroadcastPktsGet (
                INT4 i4IfIndex,
                unsigned long long *pu8IfHCInBroadcastPkts );

/********************************************************************
* FUNCTION NcIfHCOutOctetsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfHCOutOctetsGet (
                INT4 i4IfIndex,
                unsigned long long *pu8IfHCOutOctets );

/********************************************************************
* FUNCTION NcIfHCOutUcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfHCOutUcastPktsGet (
                INT4 i4IfIndex,
                unsigned long long *pu8IfHCOutUcastPkts );

/********************************************************************
* FUNCTION NcIfHCOutMulticastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfHCOutMulticastPktsGet (
                INT4 i4IfIndex,
                unsigned long long *pu8IfHCOutMulticastPkts );

/********************************************************************
* FUNCTION NcIfHCOutBroadcastPktsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfHCOutBroadcastPktsGet (
                INT4 i4IfIndex,
                unsigned long long *pu8IfHCOutBroadcastPkts );

/********************************************************************
* FUNCTION NcIfLinkUpDownTrapEnableSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfLinkUpDownTrapEnableSet (
                INT4 i4IfIndex,
                INT4 i4IfLinkUpDownTrapEnable );

/********************************************************************
* FUNCTION NcIfLinkUpDownTrapEnableTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfLinkUpDownTrapEnableTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                INT4 i4IfLinkUpDownTrapEnable );

/********************************************************************
* FUNCTION NcIfHighSpeedGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfHighSpeedGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfHighSpeed );

/********************************************************************
* FUNCTION NcIfPromiscuousModeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfPromiscuousModeSet (
                INT4 i4IfIndex,
                INT4 i4IfPromiscuousMode );

/********************************************************************
* FUNCTION NcIfPromiscuousModeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfPromiscuousModeTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                INT4 i4IfPromiscuousMode );

/********************************************************************
* FUNCTION NcIfConnectorPresentGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfConnectorPresentGet (
                INT4 i4IfIndex,
                INT4 *pi4IfConnectorPresent );

/********************************************************************
* FUNCTION NcIfAliasSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfAliasSet (
                INT4 i4IfIndex,
                UINT1 *pIfAlias );

/********************************************************************
* FUNCTION NcIfAliasTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfAliasTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                UINT1 *pIfAlias );

/********************************************************************
* FUNCTION NcIfCounterDiscontinuityTimeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfCounterDiscontinuityTimeGet (
                INT4 i4IfIndex,
                UINT4 *pu4IfCounterDiscontinuityTime );

/********************************************************************
* FUNCTION NcIfTestIdSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfTestIdSet (
                INT4 i4IfIndex,
                INT4 i4IfTestId );

/********************************************************************
* FUNCTION NcIfTestIdTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfTestIdTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                INT4 i4IfTestId );

/********************************************************************
* FUNCTION NcIfTestStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfTestStatusSet (
                INT4 i4IfIndex,
                INT4 i4IfTestStatus );

/********************************************************************
* FUNCTION NcIfTestStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfTestStatusTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                INT4 i4IfTestStatus );

/********************************************************************
* FUNCTION NcIfTestTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfTestTypeSet (
                INT4 i4IfIndex,
                const UINT1 *pIfTestType );

/********************************************************************
* FUNCTION NcIfTestTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfTestTypeTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                const UINT1 *pIfTestType );

/********************************************************************
* FUNCTION NcIfTestResultGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfTestResultGet (
                INT4 i4IfIndex,
                INT4 *pi4IfTestResult );

/********************************************************************
* FUNCTION NcIfTestCodeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfTestCodeGet (
                INT4 i4IfIndex,
                UINT1 *pIfTestCode );

/********************************************************************
* FUNCTION NcIfTestOwnerSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfTestOwnerSet (
                INT4 i4IfIndex,
                UINT1 *pIfTestOwner );

/********************************************************************
* FUNCTION NcIfTestOwnerTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfTestOwnerTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                UINT1 *pIfTestOwner );

/********************************************************************
* FUNCTION NcIfStackStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfStackStatusSet (
                INT4 i4IfStackHigherLayer,
                INT4 i4IfStackLowerLayer,
                INT4 i4IfStackStatus );

/********************************************************************
* FUNCTION NcIfStackStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfStackStatusTest (UINT4 *pu4Error,
                INT4 i4IfStackHigherLayer,
                INT4 i4IfStackLowerLayer,
                INT4 i4IfStackStatus );

/********************************************************************
* FUNCTION NcIfRcvAddressStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfRcvAddressStatusSet (
                INT4 i4IfIndex,
                UINT1 *pIfRcvAddressAddress,
                INT4 i4IfRcvAddressStatus );

/********************************************************************
* FUNCTION NcIfRcvAddressStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfRcvAddressStatusTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                UINT1 *pIfRcvAddressAddress,
                INT4 i4IfRcvAddressStatus );

/********************************************************************
* FUNCTION NcIfRcvAddressTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfRcvAddressTypeSet (
                INT4 i4IfIndex,
                UINT1 *pIfRcvAddressAddress,
                INT4 i4IfRcvAddressType );

/********************************************************************
* FUNCTION NcIfRcvAddressTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcIfRcvAddressTypeTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                UINT1 *pIfRcvAddressAddress,
                INT4 i4IfRcvAddressType );

/* END i_IF_MIB.c */


#endif
