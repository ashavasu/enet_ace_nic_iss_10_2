/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fscfadb.h,v 1.40 2016/10/03 10:34:39 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSCFADB_H
#define _FSCFADB_H

UINT1 IfMainTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 IfIpTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 IfWanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 IfAutoCktProfileTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 IfIvrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 IfSecondaryIpAddressTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 IfMainExtTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 IfCustTLVTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 IfCustOpaqueAttrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 IfBridgeILanIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 IfTypeProtoDenyTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 IfIvrMappingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FfHostCacheTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 IfHCErrorTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 IfAvailableIndexTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsPacketAnalyserTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsPacketTransmitterTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 IfACTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 IfUfdGroupTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT4 fscfa [] ={1,3,6,1,4,1,2076,27};
tSNMP_OID_TYPE fscfaOID = {8, fscfa};


UINT4 IfMaxInterfaces [ ] ={1,3,6,1,4,1,2076,27,1,1};
UINT4 IfMaxPhysInterfaces [ ] ={1,3,6,1,4,1,2076,27,1,2};
UINT4 IfAvailableIndex [ ] ={1,3,6,1,4,1,2076,27,1,3};
UINT4 IfMainIndex [ ] ={1,3,6,1,4,1,2076,27,1,4,1,1};
UINT4 IfMainType [ ] ={1,3,6,1,4,1,2076,27,1,4,1,2};
UINT4 IfMainMtu [ ] ={1,3,6,1,4,1,2076,27,1,4,1,3};
UINT4 IfMainAdminStatus [ ] ={1,3,6,1,4,1,2076,27,1,4,1,4};
UINT4 IfMainOperStatus [ ] ={1,3,6,1,4,1,2076,27,1,4,1,5};
UINT4 IfMainEncapType [ ] ={1,3,6,1,4,1,2076,27,1,4,1,6};
UINT4 IfMainBrgPortType [ ] ={1,3,6,1,4,1,2076,27,1,4,1,7};
UINT4 IfMainRowStatus [ ] ={1,3,6,1,4,1,2076,27,1,4,1,8};
UINT4 IfMainSubType [ ] ={1,3,6,1,4,1,2076,27,1,4,1,9};
UINT4 IfMainNetworkType [ ] ={1,3,6,1,4,1,2076,27,1,4,1,10};
UINT4 IfMainWanType [ ] ={1,3,6,1,4,1,2076,27,1,4,1,11};
UINT4 IfMainDesc [ ] ={1,3,6,1,4,1,2076,27,1,4,1,12};
UINT4 IfMainStorageType [ ] ={1,3,6,1,4,1,2076,27,1,4,1,13};
UINT4 IfMainExtSubType [ ] ={1,3,6,1,4,1,2076,27,1,4,1,14};
UINT4 IfMainPortRole [ ] ={1,3,6,1,4,1,2076,27,1,4,1,15};
UINT4 IfMainUfdOperStatus [ ] ={1,3,6,1,4,1,2076,27,1,4,1,16};
UINT4 IfMainUfdGroupId [ ] ={1,3,6,1,4,1,2076,27,1,4,1,17};
UINT4 IfMainUfdDownlinkDisabledCount [ ] ={1,3,6,1,4,1,2076,27,1,4,1,18};
UINT4 IfMainUfdDownlinkEnabledCount [ ] ={1,3,6,1,4,1,2076,27,1,4,1,19};
UINT4 IfMainDesigUplinkStatus [ ] ={1,3,6,1,4,1,2076,27,1,4,1,20};
UINT4 IfMainEncapDot1qVlanId [ ] ={1,3,6,1,4,1,2076,27,1,4,1,21};
UINT4 IfIpAddrAllocMethod [ ] ={1,3,6,1,4,1,2076,27,1,5,1,1};
UINT4 IfIpAddr [ ] ={1,3,6,1,4,1,2076,27,1,5,1,2};
UINT4 IfIpSubnetMask [ ] ={1,3,6,1,4,1,2076,27,1,5,1,3};
UINT4 IfIpBroadcastAddr [ ] ={1,3,6,1,4,1,2076,27,1,5,1,4};
UINT4 IfIpForwardingEnable [ ] ={1,3,6,1,4,1,2076,27,1,5,1,5};
UINT4 IfIpAddrAllocProtocol [ ] ={1,3,6,1,4,1,2076,27,1,5,1,6};
UINT4 IfIpDestMacAddress [ ] ={1,3,6,1,4,1,2076,27,1,5,1,7};
UINT4 IfIpUnnumAssocIPIf [ ] ={1,3,6,1,4,1,2076,27,1,5,1,8};
UINT4 IfIpIntfStatsEnable [ ] ={1,3,6,1,4,1,2076,27,1,5,1,9};
UINT4 IfIpPortVlanId [ ] ={1,3,6,1,4,1,2076,27,1,5,1,10};
UINT4 IfWanInterfaceType [ ] ={1,3,6,1,4,1,2076,27,1,6,1,1};
UINT4 IfWanConnectionType [ ] ={1,3,6,1,4,1,2076,27,1,6,1,2};
UINT4 IfWanVirtualPathId [ ] ={1,3,6,1,4,1,2076,27,1,6,1,3};
UINT4 IfWanVirtualCircuitId [ ] ={1,3,6,1,4,1,2076,27,1,6,1,4};
UINT4 IfWanPeerMediaAddress [ ] ={1,3,6,1,4,1,2076,27,1,6,1,5};
UINT4 IfWanSustainedSpeed [ ] ={1,3,6,1,4,1,2076,27,1,6,1,6};
UINT4 IfWanPeakSpeed [ ] ={1,3,6,1,4,1,2076,27,1,6,1,7};
UINT4 IfWanMaxBurstSize [ ] ={1,3,6,1,4,1,2076,27,1,6,1,8};
UINT4 IfWanIpQosProfileIndex [ ] ={1,3,6,1,4,1,2076,27,1,6,1,9};
UINT4 IfWanIdleTimeout [ ] ={1,3,6,1,4,1,2076,27,1,6,1,10};
UINT4 IfWanPeerIpAddr [ ] ={1,3,6,1,4,1,2076,27,1,6,1,11};
UINT4 IfWanRtpHdrComprEnable [ ] ={1,3,6,1,4,1,2076,27,1,6,1,12};
UINT4 IfWanPersistence [ ] ={1,3,6,1,4,1,2076,27,1,6,1,13};
UINT4 IfAutoProfileIfIndex [ ] ={1,3,6,1,4,1,2076,27,1,7,1,1};
UINT4 IfAutoProfileIfType [ ] ={1,3,6,1,4,1,2076,27,1,7,1,2};
UINT4 IfAutoProfileIpAddrAllocMethod [ ] ={1,3,6,1,4,1,2076,27,1,7,1,3};
UINT4 IfAutoProfileDefIpSubnetMask [ ] ={1,3,6,1,4,1,2076,27,1,7,1,4};
UINT4 IfAutoProfileDefIpBroadcastAddr [ ] ={1,3,6,1,4,1,2076,27,1,7,1,5};
UINT4 IfAutoProfileIdleTimeout [ ] ={1,3,6,1,4,1,2076,27,1,7,1,6};
UINT4 IfAutoProfileEncapType [ ] ={1,3,6,1,4,1,2076,27,1,7,1,7};
UINT4 IfAutoProfileIpQosProfileIndex [ ] ={1,3,6,1,4,1,2076,27,1,7,1,8};
UINT4 IfAutoProfileRowStatus [ ] ={1,3,6,1,4,1,2076,27,1,7,1,9};
UINT4 IfIvrBridgedIface [ ] ={1,3,6,1,4,1,2076,27,1,8,1,1};
UINT4 IfSetMgmtVlanList [ ] ={1,3,6,1,4,1,2076,27,1,9};
UINT4 IfResetMgmtVlanList [ ] ={1,3,6,1,4,1,2076,27,1,10};
UINT4 IfSecondaryIpAddress [ ] ={1,3,6,1,4,1,2076,27,1,11,1,1};
UINT4 IfSecondaryIpSubnetMask [ ] ={1,3,6,1,4,1,2076,27,1,11,1,2};
UINT4 IfSecondaryIpBroadcastAddr [ ] ={1,3,6,1,4,1,2076,27,1,11,1,3};
UINT4 IfSecondaryIpRowStatus [ ] ={1,3,6,1,4,1,2076,27,1,11,1,4};
UINT4 IfMainExtMacAddress [ ] ={1,3,6,1,4,1,2076,27,1,12,1,8};
UINT4 IfMainExtSysSpecificPortID [ ] ={1,3,6,1,4,1,2076,27,1,12,1,9};
UINT4 IfMainExtInterfaceType [ ] ={1,3,6,1,4,1,2076,27,1,12,1,10};
UINT4 IfMainExtPortSecState [ ] ={1,3,6,1,4,1,2076,27,1,12,1,11};
UINT4 IfMainExtInPkts [ ] ={1,3,6,1,4,1,2076,27,1,12,1,12};
UINT4 IfMainExtLinkUpEnabledStatus [ ] ={1,3,6,1,4,1,2076,27,1,12,1,13};
UINT4 IfMainExtLinkUpDelayTimer [ ] ={1,3,6,1,4,1,2076,27,1,12,1,14};
UINT4 IfMainExtLinkUpRemainingTime [ ] ={1,3,6,1,4,1,2076,27,1,12,1,15};
UINT4 IfCustTLVType [ ] ={1,3,6,1,4,1,2076,27,1,13,1,1};
UINT4 IfCustTLVLength [ ] ={1,3,6,1,4,1,2076,27,1,13,1,2};
UINT4 IfCustTLVValue [ ] ={1,3,6,1,4,1,2076,27,1,13,1,3};
UINT4 IfCustTLVRowStatus [ ] ={1,3,6,1,4,1,2076,27,1,13,1,4};
UINT4 IfCustOpaqueAttributeID [ ] ={1,3,6,1,4,1,2076,27,1,14,1,1};
UINT4 IfCustOpaqueAttribute [ ] ={1,3,6,1,4,1,2076,27,1,14,1,2};
UINT4 IfCustOpaqueRowStatus [ ] ={1,3,6,1,4,1,2076,27,1,14,1,3};
UINT4 IfBridgeILanIfStatus [ ] ={1,3,6,1,4,1,2076,27,1,15,1,1};
UINT4 IfTypeProtoDenyContextId [ ] ={1,3,6,1,4,1,2076,27,1,16,1,1};
UINT4 IfTypeProtoDenyMainType [ ] ={1,3,6,1,4,1,2076,27,1,16,1,2};
UINT4 IfTypeProtoDenyBrgPortType [ ] ={1,3,6,1,4,1,2076,27,1,16,1,3};
UINT4 IfTypeProtoDenyProtocol [ ] ={1,3,6,1,4,1,2076,27,1,16,1,4};
UINT4 IfTypeProtoDenyRowStatus [ ] ={1,3,6,1,4,1,2076,27,1,16,1,5};
UINT4 IfmDebug [ ] ={1,3,6,1,4,1,2076,27,1,17};
UINT4 IfIvrAssociatedVlan [ ] ={1,3,6,1,4,1,2076,27,1,18,1,1};
UINT4 IfIvrMappingRowStatus [ ] ={1,3,6,1,4,1,2076,27,1,18,1,2};
UINT4 FfFastForwardingEnable [ ] ={1,3,6,1,4,1,2076,27,2,1};
UINT4 FfCacheSize [ ] ={1,3,6,1,4,1,2076,27,2,2};
UINT4 FfIpChecksumValidationEnable [ ] ={1,3,6,1,4,1,2076,27,2,3};
UINT4 FfCachePurgeCount [ ] ={1,3,6,1,4,1,2076,27,2,4};
UINT4 FfCacheLastPurgeTime [ ] ={1,3,6,1,4,1,2076,27,2,5};
UINT4 FfStaticEntryInvalidTrapEnable [ ] ={1,3,6,1,4,1,2076,27,2,6};
UINT4 FfCurrentStaticEntryInvalidCount [ ] ={1,3,6,1,4,1,2076,27,2,7};
UINT4 FfTotalEntryCount [ ] ={1,3,6,1,4,1,2076,27,2,8};
UINT4 FfStaticEntryCount [ ] ={1,3,6,1,4,1,2076,27,2,9};
UINT4 FfTotalPktsFastForwarded [ ] ={1,3,6,1,4,1,2076,27,2,10};
UINT4 FfHostCacheDestAddr [ ] ={1,3,6,1,4,1,2076,27,2,11,1,1};
UINT4 FfHostCacheNextHopAddr [ ] ={1,3,6,1,4,1,2076,27,2,11,1,2};
UINT4 FfHostCacheIfIndex [ ] ={1,3,6,1,4,1,2076,27,2,11,1,3};
UINT4 FfHostCacheNextHopMediaAddr [ ] ={1,3,6,1,4,1,2076,27,2,11,1,4};
UINT4 FfHostCacheHits [ ] ={1,3,6,1,4,1,2076,27,2,11,1,5};
UINT4 FfHostCacheLastHitTime [ ] ={1,3,6,1,4,1,2076,27,2,11,1,6};
UINT4 FfHostCacheEntryType [ ] ={1,3,6,1,4,1,2076,27,2,11,1,7};
UINT4 FfHostCacheRowStatus [ ] ={1,3,6,1,4,1,2076,27,2,11,1,8};
UINT4 FmMemoryResourceTrapEnable [ ] ={1,3,6,1,4,1,2076,27,3,1};
UINT4 FmTimersResourceTrapEnable [ ] ={1,3,6,1,4,1,2076,27,3,2};
UINT4 FmTracingEnable [ ] ={1,3,6,1,4,1,2076,27,3,3};
UINT4 FmMemAllocFailCount [ ] ={1,3,6,1,4,1,2076,27,3,4};
UINT4 FmTimerReqFailCount [ ] ={1,3,6,1,4,1,2076,27,3,5};
UINT4 IfHCInDiscards [ ] ={1,3,6,1,4,1,2076,27,1,19,1,1};
UINT4 IfHCInErrors [ ] ={1,3,6,1,4,1,2076,27,1,19,1,2};
UINT4 IfHCInUnknownProtos [ ] ={1,3,6,1,4,1,2076,27,1,19,1,3};
UINT4 IfHCOutDiscards [ ] ={1,3,6,1,4,1,2076,27,1,19,1,4};
UINT4 IfHCOutErrors [ ] ={1,3,6,1,4,1,2076,27,1,19,1,5};
UINT4 IfSecurityBridging [ ] ={1,3,6,1,4,1,2076,27,1,20};
UINT4 IfSetSecVlanList [ ] ={1,3,6,1,4,1,2076,27,1,21};
UINT4 IfResetSecVlanList [ ] ={1,3,6,1,4,1,2076,27,1,22};
UINT4 IfSecIvrIfIndex [ ] ={1,3,6,1,4,1,2076,27,1,23};
UINT4 IfAvailableFreeIndex [ ] ={1,3,6,1,4,1,2076,27,1,24,1,1};
UINT4 IfACPortIdentifier [ ] ={1,3,6,1,4,1,2076,27,1,25,1,1};
UINT4 IfACCustomerVlan [ ] ={1,3,6,1,4,1,2076,27,1,25,1,2};
UINT4 IfUfdSystemControl [ ] ={1,3,6,1,4,1,2076,27,1,26};                                                                     UINT4 IfUfdModuleStatus [ ] ={1,3,6,1,4,1,2076,27,1,27};                                                                      UINT4 IfSplitHorizonSysControl [ ] ={1,3,6,1,4,1,2076,27,1,28};                                                               UINT4 IfSplitHorizonModStatus [ ] ={1,3,6,1,4,1,2076,27,1,29};                                                                UINT4 IfUfdGroupId [ ] ={1,3,6,1,4,1,2076,27,1,30,1,1};
UINT4 IfUfdGroupName [ ] ={1,3,6,1,4,1,2076,27,1,30,1,2};                                                                     UINT4 IfUfdGroupStatus [ ] ={1,3,6,1,4,1,2076,27,1,30,1,3};                                                                   UINT4 IfUfdGroupDownlinkPorts [ ] ={1,3,6,1,4,1,2076,27,1,30,1,4};
UINT4 IfUfdGroupUplinkPorts [ ] ={1,3,6,1,4,1,2076,27,1,30,1,5};
UINT4 IfUfdGroupDesigUplinkPort [ ] ={1,3,6,1,4,1,2076,27,1,30,1,6};
UINT4 IfUfdGroupUplinkCount [ ] ={1,3,6,1,4,1,2076,27,1,30,1,7};
UINT4 IfUfdGroupDownlinkCount [ ] ={1,3,6,1,4,1,2076,27,1,30,1,8};
UINT4 IfUfdGroupRowStatus [ ] ={1,3,6,1,4,1,2076,27,1,30,1,9};
UINT4 IfLinkUpEnabledStatus [ ] ={1,3,6,1,4,1,2076,27,1,31};
UINT4 IfOOBNode0SecondaryIpAddress [ ] ={1,3,6,1,4,1,2076,27,1,32};
UINT4 IfOOBNode0SecondaryIpMask [ ] ={1,3,6,1,4,1,2076,27,1,33};
UINT4 IfOOBNode1SecondaryIpAddress [ ] ={1,3,6,1,4,1,2076,27,1,34};
UINT4 IfOOBNode1SecondaryIpMask [ ] ={1,3,6,1,4,1,2076,27,1,35};

UINT4 FsPacketAnalyserIndex [ ] ={1,3,6,1,4,1,2076,27,5,1,1,1};
UINT4 FsPacketAnalyserWatchValue [ ] ={1,3,6,1,4,1,2076,27,5,1,1,2};
UINT4 FsPacketAnalyserWatchMask [ ] ={1,3,6,1,4,1,2076,27,5,1,1,3};
UINT4 FsPacketAnalyserWatchPorts [ ] ={1,3,6,1,4,1,2076,27,5,1,1,4};
UINT4 FsPacketAnalyserMatchPorts [ ] ={1,3,6,1,4,1,2076,27,5,1,1,5};
UINT4 FsPacketAnalyserCounter [ ] ={1,3,6,1,4,1,2076,27,5,1,1,6};
UINT4 FsPacketAnalyserTime [ ] ={1,3,6,1,4,1,2076,27,5,1,1,7};
UINT4 FsPacketAnalyserCreateTime [ ] ={1,3,6,1,4,1,2076,27,5,1,1,8};
UINT4 FsPacketAnalyserStatus [ ] ={1,3,6,1,4,1,2076,27,5,1,1,9};
UINT4 FsPacketTransmitterIndex [ ] ={1,3,6,1,4,1,2076,27,5,2,1,1};
UINT4 FsPacketTransmitterValue [ ] ={1,3,6,1,4,1,2076,27,5,2,1,2};
UINT4 FsPacketTransmitterPort [ ] ={1,3,6,1,4,1,2076,27,5,2,1,3};
UINT4 FsPacketTransmitterInterval [ ] ={1,3,6,1,4,1,2076,27,5,2,1,4};
UINT4 FsPacketTransmitterCount [ ] ={1,3,6,1,4,1,2076,27,5,2,1,5};
UINT4 FsPacketTransmitterStatus [ ] ={1,3,6,1,4,1,2076,27,5,2,1,6};




tMbDbEntry fscfaMibEntry[]= {

{{10,IfMaxInterfaces}, NULL, IfMaxInterfacesGet, IfMaxInterfacesSet, IfMaxInterfacesTest, IfMaxInterfacesDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 1, 0, NULL},

{{10,IfMaxPhysInterfaces}, NULL, IfMaxPhysInterfacesGet, IfMaxPhysInterfacesSet, IfMaxPhysInterfacesTest, IfMaxPhysInterfacesDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 1, 0, NULL},

{{10,IfAvailableIndex}, NULL, IfAvailableIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,IfMainIndex}, GetNextIndexIfMainTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IfMainTableINDEX, 1, 0, 0, NULL},

{{12,IfMainType}, GetNextIndexIfMainTable, IfMainTypeGet, IfMainTypeSet, IfMainTypeTest, IfMainTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfMainTableINDEX, 1, 0, 0, NULL},

{{12,IfMainMtu}, GetNextIndexIfMainTable, IfMainMtuGet, IfMainMtuSet, IfMainMtuTest, IfMainTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IfMainTableINDEX, 1, 0, 0, NULL},

/* Since the default value of IfMainAdminStatus is 'down(2)', whenever user configures
 * the IfMainAdminStatus as 'down(2)' the value is not saved(because incase of
 * incremental save on the default values of an object will not be saved). Because of                                                                            * this change in ip address is not notified(will be notified only if there is a
 * change in admin status) to other modules.
 * So, manually modified the default value of the IfMainAdminStatus as NULL */   {{12,IfMainAdminStatus}, GetNextIndexIfMainTable, IfMainAdminStatusGet, IfMainAdminStatusSet, IfMainAdminStatusTest, IfMainTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfMainTableINDEX, 1, 0, 0, NULL},

{{12,IfMainOperStatus}, GetNextIndexIfMainTable, IfMainOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IfMainTableINDEX, 1, 0, 0, NULL},

{{12,IfMainEncapType}, GetNextIndexIfMainTable, IfMainEncapTypeGet, IfMainEncapTypeSet, IfMainEncapTypeTest, IfMainTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfMainTableINDEX, 1, 0, 0, NULL},

{{12,IfMainBrgPortType}, GetNextIndexIfMainTable, IfMainBrgPortTypeGet, IfMainBrgPortTypeSet, IfMainBrgPortTypeTest, IfMainTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfMainTableINDEX, 1, 0, 0, "8"},

{{12,IfMainRowStatus}, GetNextIndexIfMainTable, IfMainRowStatusGet, IfMainRowStatusSet, IfMainRowStatusTest, IfMainTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfMainTableINDEX, 1, 0, 1, NULL},

{{12,IfMainSubType}, GetNextIndexIfMainTable, IfMainSubTypeGet, IfMainSubTypeSet, IfMainSubTypeTest, IfMainTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfMainTableINDEX, 1, 0, 0, NULL},

{{12,IfMainNetworkType}, GetNextIndexIfMainTable, IfMainNetworkTypeGet, IfMainNetworkTypeSet, IfMainNetworkTypeTest, IfMainTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfMainTableINDEX, 1, 0, 0, NULL},

{{12,IfMainWanType}, GetNextIndexIfMainTable, IfMainWanTypeGet, IfMainWanTypeSet, IfMainWanTypeTest, IfMainTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfMainTableINDEX, 1, 0, 0, NULL},

{{12,IfMainDesc}, GetNextIndexIfMainTable, IfMainDescGet, IfMainDescSet, IfMainDescTest, IfMainTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IfMainTableINDEX, 1, 0, 0, NULL},

{{12,IfMainStorageType}, GetNextIndexIfMainTable, IfMainStorageTypeGet, IfMainStorageTypeSet, IfMainStorageTypeTest, IfMainTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfMainTableINDEX, 1, 0, 0, "3"},

{{12,IfMainExtSubType}, GetNextIndexIfMainTable, IfMainExtSubTypeGet, IfMainExtSubTypeSet, IfMainExtSubTypeTest, IfMainTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfMainTableINDEX, 1, 0, 0, NULL},


{{12,IfMainPortRole}, GetNextIndexIfMainTable, IfMainPortRoleGet, IfMainPortRoleSet, IfMainPortRoleTest, IfMainTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfMainTableINDEX, 1, 0, 0, "2"},

{{12,IfMainUfdOperStatus}, GetNextIndexIfMainTable, IfMainUfdOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IfMainTableINDEX, 1, 0, 0, NULL},

{{12,IfMainUfdGroupId}, GetNextIndexIfMainTable, IfMainUfdGroupIdGet, IfMainUfdGroupIdSet, IfMainUfdGroupIdTest, IfMainTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IfMainTableINDEX, 1, 0, 0, NULL},

{{12,IfMainUfdDownlinkDisabledCount}, GetNextIndexIfMainTable, IfMainUfdDownlinkDisabledCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IfMainTableINDEX, 1, 1, 0, NULL},

{{12,IfMainUfdDownlinkEnabledCount}, GetNextIndexIfMainTable, IfMainUfdDownlinkEnabledCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IfMainTableINDEX, 1, 1, 0, NULL},

{{12,IfMainDesigUplinkStatus}, GetNextIndexIfMainTable, IfMainDesigUplinkStatusGet, IfMainDesigUplinkStatusSet, IfMainDesigUplinkStatusTest, IfMainTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfMainTableINDEX, 1, 0, 0, "2"},

{{12,IfMainEncapDot1qVlanId}, GetNextIndexIfMainTable, IfMainEncapDot1qVlanIdGet, IfMainEncapDot1qVlanIdSet, IfMainEncapDot1qVlanIdTest, IfMainTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IfMainTableINDEX, 1, 0, 0, "0"},

{{12,IfIpAddrAllocMethod}, GetNextIndexIfIpTable, IfIpAddrAllocMethodGet, IfIpAddrAllocMethodSet, IfIpAddrAllocMethodTest, IfIpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfIpTableINDEX, 1, 0, 0, "4"},

   /* Manually modified the default value of the IfIpAddr because for the 
    * default ivr vlan 1, the ip address is set by system initialization and
    * it cannot be reset */
{{12,IfIpAddr}, GetNextIndexIfIpTable, IfIpAddrGet, IfIpAddrSet, IfIpAddrTest, IfIpTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IfIpTableINDEX, 1, 0, 0, NULL},

{{12,IfIpSubnetMask}, GetNextIndexIfIpTable, IfIpSubnetMaskGet, IfIpSubnetMaskSet, IfIpSubnetMaskTest, IfIpTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IfIpTableINDEX, 1, 0, 0, NULL},

{{12,IfIpBroadcastAddr}, GetNextIndexIfIpTable, IfIpBroadcastAddrGet, IfIpBroadcastAddrSet, IfIpBroadcastAddrTest, IfIpTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IfIpTableINDEX, 1, 0, 0, NULL},

{{12,IfIpForwardingEnable}, GetNextIndexIfIpTable, IfIpForwardingEnableGet, IfIpForwardingEnableSet, IfIpForwardingEnableTest, IfIpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfIpTableINDEX, 1, 0, 0, "1"},

{{12,IfIpAddrAllocProtocol}, GetNextIndexIfIpTable, IfIpAddrAllocProtocolGet, IfIpAddrAllocProtocolSet, IfIpAddrAllocProtocolTest, IfIpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfIpTableINDEX, 1, 0, 0, "2"},

{{12,IfIpDestMacAddress}, GetNextIndexIfIpTable, IfIpDestMacAddressGet, IfIpDestMacAddressSet, IfIpDestMacAddressTest, IfIpTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, IfIpTableINDEX, 1, 0, 0, NULL},

{{12,IfIpUnnumAssocIPIf}, GetNextIndexIfIpTable, IfIpUnnumAssocIPIfGet, IfIpUnnumAssocIPIfSet, IfIpUnnumAssocIPIfTest, IfIpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfIpTableINDEX, 1, 0, 0, NULL},

{{12,IfIpIntfStatsEnable}, GetNextIndexIfIpTable, IfIpIntfStatsEnableGet, IfIpIntfStatsEnableSet, IfIpIntfStatsEnableTest, IfIpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfIpTableINDEX, 1, 0, 0, "2"},

{{12,IfIpPortVlanId}, GetNextIndexIfIpTable, IfIpPortVlanIdGet, IfIpPortVlanIdSet, IfIpPortVlanIdTest, IfIpTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IfIpTableINDEX, 1, 0, 0, "0"},

{{12,IfWanInterfaceType}, GetNextIndexIfWanTable, IfWanInterfaceTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IfWanTableINDEX, 1, 1, 0, NULL},

{{12,IfWanConnectionType}, GetNextIndexIfWanTable, IfWanConnectionTypeGet, IfWanConnectionTypeSet, IfWanConnectionTypeTest, IfWanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfWanTableINDEX, 1, 1, 0, NULL},

{{12,IfWanVirtualPathId}, GetNextIndexIfWanTable, IfWanVirtualPathIdGet, IfWanVirtualPathIdSet, IfWanVirtualPathIdTest, IfWanTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IfWanTableINDEX, 1, 1, 0, "0"},

{{12,IfWanVirtualCircuitId}, GetNextIndexIfWanTable, IfWanVirtualCircuitIdGet, IfWanVirtualCircuitIdSet, IfWanVirtualCircuitIdTest, IfWanTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IfWanTableINDEX, 1, 1, 0, "0"},

{{12,IfWanPeerMediaAddress}, GetNextIndexIfWanTable, IfWanPeerMediaAddressGet, IfWanPeerMediaAddressSet, IfWanPeerMediaAddressTest, IfWanTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IfWanTableINDEX, 1, 1, 0, NULL},

{{12,IfWanSustainedSpeed}, GetNextIndexIfWanTable, IfWanSustainedSpeedGet, IfWanSustainedSpeedSet, IfWanSustainedSpeedTest, IfWanTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IfWanTableINDEX, 1, 1, 0, NULL},

{{12,IfWanPeakSpeed}, GetNextIndexIfWanTable, IfWanPeakSpeedGet, IfWanPeakSpeedSet, IfWanPeakSpeedTest, IfWanTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IfWanTableINDEX, 1, 1, 0, NULL},

{{12,IfWanMaxBurstSize}, GetNextIndexIfWanTable, IfWanMaxBurstSizeGet, IfWanMaxBurstSizeSet, IfWanMaxBurstSizeTest, IfWanTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IfWanTableINDEX, 1, 1, 0, NULL},

{{12,IfWanIpQosProfileIndex}, GetNextIndexIfWanTable, IfWanIpQosProfileIndexGet, IfWanIpQosProfileIndexSet, IfWanIpQosProfileIndexTest, IfWanTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IfWanTableINDEX, 1, 1, 0, "0"},

{{12,IfWanIdleTimeout}, GetNextIndexIfWanTable, IfWanIdleTimeoutGet, IfWanIdleTimeoutSet, IfWanIdleTimeoutTest, IfWanTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IfWanTableINDEX, 1, 1, 0, "0"},

{{12,IfWanPeerIpAddr}, GetNextIndexIfWanTable, IfWanPeerIpAddrGet, IfWanPeerIpAddrSet, IfWanPeerIpAddrTest, IfWanTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IfWanTableINDEX, 1, 1, 0, "0"},

{{12,IfWanRtpHdrComprEnable}, GetNextIndexIfWanTable, IfWanRtpHdrComprEnableGet, IfWanRtpHdrComprEnableSet, IfWanRtpHdrComprEnableTest, IfWanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfWanTableINDEX, 1, 1, 0, "2"},

{{12,IfWanPersistence}, GetNextIndexIfWanTable, IfWanPersistenceGet, IfWanPersistenceSet, IfWanPersistenceTest, IfWanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfWanTableINDEX, 1, 1, 0, "1"},

{{12,IfAutoProfileIfIndex}, GetNextIndexIfAutoCktProfileTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IfAutoCktProfileTableINDEX, 1, 1, 0, NULL},

{{12,IfAutoProfileIfType}, GetNextIndexIfAutoCktProfileTable, IfAutoProfileIfTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IfAutoCktProfileTableINDEX, 1, 1, 0, NULL},

{{12,IfAutoProfileIpAddrAllocMethod}, GetNextIndexIfAutoCktProfileTable, IfAutoProfileIpAddrAllocMethodGet, IfAutoProfileIpAddrAllocMethodSet, IfAutoProfileIpAddrAllocMethodTest, IfAutoCktProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfAutoCktProfileTableINDEX, 1, 1, 0, "1"},

{{12,IfAutoProfileDefIpSubnetMask}, GetNextIndexIfAutoCktProfileTable, IfAutoProfileDefIpSubnetMaskGet, IfAutoProfileDefIpSubnetMaskSet, IfAutoProfileDefIpSubnetMaskTest, IfAutoCktProfileTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IfAutoCktProfileTableINDEX, 1, 1, 0, NULL},

{{12,IfAutoProfileDefIpBroadcastAddr}, GetNextIndexIfAutoCktProfileTable, IfAutoProfileDefIpBroadcastAddrGet, IfAutoProfileDefIpBroadcastAddrSet, IfAutoProfileDefIpBroadcastAddrTest, IfAutoCktProfileTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IfAutoCktProfileTableINDEX, 1, 1, 0, NULL},

{{12,IfAutoProfileIdleTimeout}, GetNextIndexIfAutoCktProfileTable, IfAutoProfileIdleTimeoutGet, IfAutoProfileIdleTimeoutSet, IfAutoProfileIdleTimeoutTest, IfAutoCktProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IfAutoCktProfileTableINDEX, 1, 1, 0, NULL},

{{12,IfAutoProfileEncapType}, GetNextIndexIfAutoCktProfileTable, IfAutoProfileEncapTypeGet, IfAutoProfileEncapTypeSet, IfAutoProfileEncapTypeTest, IfAutoCktProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfAutoCktProfileTableINDEX, 1, 1, 0, NULL},

{{12,IfAutoProfileIpQosProfileIndex}, GetNextIndexIfAutoCktProfileTable, IfAutoProfileIpQosProfileIndexGet, IfAutoProfileIpQosProfileIndexSet, IfAutoProfileIpQosProfileIndexTest, IfAutoCktProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IfAutoCktProfileTableINDEX, 1, 1, 0, "0"},

{{12,IfAutoProfileRowStatus}, GetNextIndexIfAutoCktProfileTable, IfAutoProfileRowStatusGet, IfAutoProfileRowStatusSet, IfAutoProfileRowStatusTest, IfAutoCktProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfAutoCktProfileTableINDEX, 1, 1, 1, NULL},

{{12,IfIvrBridgedIface}, GetNextIndexIfIvrTable, IfIvrBridgedIfaceGet, IfIvrBridgedIfaceSet, IfIvrBridgedIfaceTest, IfIvrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfIvrTableINDEX, 1, 0, 0, NULL},

{{10,IfSetMgmtVlanList}, NULL, IfSetMgmtVlanListGet, IfSetMgmtVlanListSet, IfSetMgmtVlanListTest, IfSetMgmtVlanListDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,IfResetMgmtVlanList}, NULL, IfResetMgmtVlanListGet, IfResetMgmtVlanListSet, IfResetMgmtVlanListTest, IfResetMgmtVlanListDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,IfSecondaryIpAddress}, GetNextIndexIfSecondaryIpAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IfSecondaryIpAddressTableINDEX, 2, 0, 0, NULL},

{{12,IfSecondaryIpSubnetMask}, GetNextIndexIfSecondaryIpAddressTable, IfSecondaryIpSubnetMaskGet, IfSecondaryIpSubnetMaskSet, IfSecondaryIpSubnetMaskTest, IfSecondaryIpAddressTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IfSecondaryIpAddressTableINDEX, 2, 0, 0, NULL},

{{12,IfSecondaryIpBroadcastAddr}, GetNextIndexIfSecondaryIpAddressTable, IfSecondaryIpBroadcastAddrGet, IfSecondaryIpBroadcastAddrSet, IfSecondaryIpBroadcastAddrTest, IfSecondaryIpAddressTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IfSecondaryIpAddressTableINDEX, 2, 0, 0, NULL},

{{12,IfSecondaryIpRowStatus}, GetNextIndexIfSecondaryIpAddressTable, IfSecondaryIpRowStatusGet, IfSecondaryIpRowStatusSet, IfSecondaryIpRowStatusTest, IfSecondaryIpAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfSecondaryIpAddressTableINDEX, 2, 0, 1, NULL},

{{12,IfMainExtMacAddress}, GetNextIndexIfMainExtTable, IfMainExtMacAddressGet, IfMainExtMacAddressSet, IfMainExtMacAddressTest, IfMainExtTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, IfMainExtTableINDEX, 1, 0, 0, NULL},

{{12,IfMainExtSysSpecificPortID}, GetNextIndexIfMainExtTable, IfMainExtSysSpecificPortIDGet, IfMainExtSysSpecificPortIDSet, IfMainExtSysSpecificPortIDTest, IfMainExtTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IfMainExtTableINDEX, 1, 0, 0, "0"},

{{12,IfMainExtInterfaceType}, GetNextIndexIfMainExtTable, IfMainExtInterfaceTypeGet, IfMainExtInterfaceTypeSet, IfMainExtInterfaceTypeTest, IfMainExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfMainExtTableINDEX, 1, 0, 0, "1"},

{{12,IfMainExtPortSecState}, GetNextIndexIfMainExtTable, IfMainExtPortSecStateGet, IfMainExtPortSecStateSet, IfMainExtPortSecStateTest, IfMainExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfMainExtTableINDEX, 1, 0, 0, "1"},

{{12,IfMainExtInPkts}, GetNextIndexIfMainExtTable, IfMainExtInPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IfMainExtTableINDEX, 1, 0, 0, NULL},

{{12,IfMainExtLinkUpEnabledStatus}, GetNextIndexIfMainExtTable, IfMainExtLinkUpEnabledStatusGet, IfMainExtLinkUpEnabledStatusSet, IfMainExtLinkUpEnabledStatusTest, IfMainExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfMainExtTableINDEX, 1, 0, 0, "2"},

{{12,IfMainExtLinkUpDelayTimer}, GetNextIndexIfMainExtTable, IfMainExtLinkUpDelayTimerGet, IfMainExtLinkUpDelayTimerSet, IfMainExtLinkUpDelayTimerTest, IfMainExtTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IfMainExtTableINDEX, 1, 0, 0, "2"},

{{12,IfMainExtLinkUpRemainingTime}, GetNextIndexIfMainExtTable, IfMainExtLinkUpRemainingTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, IfMainExtTableINDEX, 1, 0, 0, NULL},

{{12,IfCustTLVType}, GetNextIndexIfCustTLVTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, IfCustTLVTableINDEX, 2, 0, 0, NULL},

{{12,IfCustTLVLength}, GetNextIndexIfCustTLVTable, IfCustTLVLengthGet, IfCustTLVLengthSet, IfCustTLVLengthTest, IfCustTLVTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IfCustTLVTableINDEX, 2, 0, 0, NULL},

{{12,IfCustTLVValue}, GetNextIndexIfCustTLVTable, IfCustTLVValueGet, IfCustTLVValueSet, IfCustTLVValueTest, IfCustTLVTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IfCustTLVTableINDEX, 2, 0, 0, NULL},

{{12,IfCustTLVRowStatus}, GetNextIndexIfCustTLVTable, IfCustTLVRowStatusGet, IfCustTLVRowStatusSet, IfCustTLVRowStatusTest, IfCustTLVTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfCustTLVTableINDEX, 2, 0, 1, NULL},

{{12,IfCustOpaqueAttributeID}, GetNextIndexIfCustOpaqueAttrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IfCustOpaqueAttrTableINDEX, 2, 0, 0, NULL},

{{12,IfCustOpaqueAttribute}, GetNextIndexIfCustOpaqueAttrTable, IfCustOpaqueAttributeGet, IfCustOpaqueAttributeSet, IfCustOpaqueAttributeTest, IfCustOpaqueAttrTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IfCustOpaqueAttrTableINDEX, 2, 0, 0, "0"},

{{12,IfCustOpaqueRowStatus}, GetNextIndexIfCustOpaqueAttrTable, IfCustOpaqueRowStatusGet, IfCustOpaqueRowStatusSet, IfCustOpaqueRowStatusTest, IfCustOpaqueAttrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfCustOpaqueAttrTableINDEX, 2, 0, 1, NULL},

{{12,IfBridgeILanIfStatus}, GetNextIndexIfBridgeILanIfTable, IfBridgeILanIfStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IfBridgeILanIfTableINDEX, 1, 0, 0, NULL},

{{12,IfTypeProtoDenyContextId}, GetNextIndexIfTypeProtoDenyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, IfTypeProtoDenyTableINDEX, 4, 0, 0, NULL},

{{12,IfTypeProtoDenyMainType}, GetNextIndexIfTypeProtoDenyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IfTypeProtoDenyTableINDEX, 4, 0, 0, NULL},

{{12,IfTypeProtoDenyBrgPortType}, GetNextIndexIfTypeProtoDenyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IfTypeProtoDenyTableINDEX, 4, 0, 0, NULL},

{{12,IfTypeProtoDenyProtocol}, GetNextIndexIfTypeProtoDenyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IfTypeProtoDenyTableINDEX, 4, 0, 0, NULL},

{{12,IfTypeProtoDenyRowStatus}, GetNextIndexIfTypeProtoDenyTable, IfTypeProtoDenyRowStatusGet, IfTypeProtoDenyRowStatusSet, IfTypeProtoDenyRowStatusTest, IfTypeProtoDenyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfTypeProtoDenyTableINDEX, 4, 0, 1, NULL},

{{10,IfmDebug}, NULL, IfmDebugGet, IfmDebugSet, IfmDebugTest, IfmDebugDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,IfIvrAssociatedVlan}, GetNextIndexIfIvrMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IfIvrMappingTableINDEX, 2, 0, 0, NULL},

{{12,IfIvrMappingRowStatus}, GetNextIndexIfIvrMappingTable, IfIvrMappingRowStatusGet, IfIvrMappingRowStatusSet, IfIvrMappingRowStatusTest, IfIvrMappingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfIvrMappingTableINDEX, 2, 0, 1, NULL},

{{12,IfHCInDiscards}, GetNextIndexIfHCErrorTable, IfHCInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IfHCErrorTableINDEX, 1, 0, 0, NULL},

{{12,IfHCInErrors}, GetNextIndexIfHCErrorTable, IfHCInErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IfHCErrorTableINDEX, 1, 0, 0, NULL},

{{12,IfHCInUnknownProtos}, GetNextIndexIfHCErrorTable, IfHCInUnknownProtosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IfHCErrorTableINDEX, 1, 0, 0, NULL},

{{12,IfHCOutDiscards}, GetNextIndexIfHCErrorTable, IfHCOutDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IfHCErrorTableINDEX, 1, 0, 0, NULL},

{{12,IfHCOutErrors}, GetNextIndexIfHCErrorTable, IfHCOutErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IfHCErrorTableINDEX, 1, 0, 0, NULL},

{{10,IfSecurityBridging}, NULL, IfSecurityBridgingGet, IfSecurityBridgingSet, IfSecurityBridgingTest, IfSecurityBridgingDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,IfSetSecVlanList}, NULL, IfSetSecVlanListGet, IfSetSecVlanListSet, IfSetSecVlanListTest, IfSetSecVlanListDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,IfResetSecVlanList}, NULL, IfResetSecVlanListGet, IfResetSecVlanListSet, IfResetSecVlanListTest, IfResetSecVlanListDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,IfSecIvrIfIndex}, NULL, IfSecIvrIfIndexGet, IfSecIvrIfIndexSet, IfSecIvrIfIndexTest, IfSecIvrIfIndexDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,IfAvailableFreeIndex}, GetNextIndexIfAvailableIndexTable, IfAvailableFreeIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IfAvailableIndexTableINDEX, 1, 0, 0, NULL},

{{12,IfACPortIdentifier}, GetNextIndexIfACTable, IfACPortIdentifierGet, IfACPortIdentifierSet, IfACPortIdentifierTest, IfACTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfACTableINDEX, 1, 0, 0, NULL},

{{12,IfACCustomerVlan}, GetNextIndexIfACTable, IfACCustomerVlanGet, IfACCustomerVlanSet, IfACCustomerVlanTest, IfACTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfACTableINDEX, 1, 0, 0, NULL},

{{10,IfUfdSystemControl}, NULL, IfUfdSystemControlGet, IfUfdSystemControlSet, IfUfdSystemControlTest, IfUfdSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,IfUfdModuleStatus}, NULL, IfUfdModuleStatusGet, IfUfdModuleStatusSet, IfUfdModuleStatusTest, IfUfdModuleStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,IfSplitHorizonSysControl}, NULL, IfSplitHorizonSysControlGet, IfSplitHorizonSysControlSet, IfSplitHorizonSysControlTest, IfSplitHorizonSysControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
                                                                                                                              {{10,IfSplitHorizonModStatus}, NULL, IfSplitHorizonModStatusGet, IfSplitHorizonModStatusSet, IfSplitHorizonModStatusTest, IfSplitHorizonModStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

                                                                                                                              {{12,IfUfdGroupId}, GetNextIndexIfUfdGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IfUfdGroupTableINDEX, 1, 0, 0, NULL},

                                                                                                                              {{12,IfUfdGroupName}, GetNextIndexIfUfdGroupTable, IfUfdGroupNameGet, IfUfdGroupNameSet, IfUfdGroupNameTest, IfUfdGroupTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IfUfdGroupTableINDEX, 1, 0, 0, NULL},

                                                                                                                              {{12,IfUfdGroupStatus}, GetNextIndexIfUfdGroupTable, IfUfdGroupStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IfUfdGroupTableINDEX, 1, 0, 0, "2"},

                                                                                                                              {{12,IfUfdGroupDownlinkPorts}, GetNextIndexIfUfdGroupTable, IfUfdGroupDownlinkPortsGet, IfUfdGroupDownlinkPortsSet, IfUfdGroupDownlinkPortsTest, IfUfdGroupTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IfUfdGroupTableINDEX, 1, 0, 0, NULL},

                                                                                                                              {{12,IfUfdGroupUplinkPorts}, GetNextIndexIfUfdGroupTable, IfUfdGroupUplinkPortsGet, IfUfdGroupUplinkPortsSet, IfUfdGroupUplinkPortsTest, IfUfdGroupTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IfUfdGroupTableINDEX, 1, 0, 0, NULL},

                                                                                                                              {{12,IfUfdGroupDesigUplinkPort}, GetNextIndexIfUfdGroupTable, IfUfdGroupDesigUplinkPortGet, IfUfdGroupDesigUplinkPortSet, IfUfdGroupDesigUplinkPortTest, IfUfdGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfUfdGroupTableINDEX, 1, 0, 0, NULL},

                                                                                                                              {{12,IfUfdGroupUplinkCount}, GetNextIndexIfUfdGroupTable, IfUfdGroupUplinkCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IfUfdGroupTableINDEX, 1, 0, 0, NULL},

                                                                                                                              {{12,IfUfdGroupDownlinkCount}, GetNextIndexIfUfdGroupTable, IfUfdGroupDownlinkCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IfUfdGroupTableINDEX, 1, 0, 0, NULL},

                                                                                                                              {{12,IfUfdGroupRowStatus}, GetNextIndexIfUfdGroupTable, IfUfdGroupRowStatusGet, IfUfdGroupRowStatusSet, IfUfdGroupRowStatusTest, IfUfdGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IfUfdGroupTableINDEX, 1, 0, 1, NULL},
{{10,IfLinkUpEnabledStatus}, NULL, IfLinkUpEnabledStatusGet, IfLinkUpEnabledStatusSet, IfLinkUpEnabledStatusTest, IfLinkUpEnabledStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,IfOOBNode0SecondaryIpAddress}, NULL, IfOOBNode0SecondaryIpAddressGet, IfOOBNode0SecondaryIpAddressSet, IfOOBNode0SecondaryIpAddressTest, IfOOBNode0SecondaryIpAddressDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,IfOOBNode0SecondaryIpMask}, NULL, IfOOBNode0SecondaryIpMaskGet, IfOOBNode0SecondaryIpMaskSet, IfOOBNode0SecondaryIpMaskTest, IfOOBNode0SecondaryIpMaskDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,IfOOBNode1SecondaryIpAddress}, NULL, IfOOBNode1SecondaryIpAddressGet, IfOOBNode1SecondaryIpAddressSet, IfOOBNode1SecondaryIpAddressTest, IfOOBNode1SecondaryIpAddressDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,IfOOBNode1SecondaryIpMask}, NULL, IfOOBNode1SecondaryIpMaskGet, IfOOBNode1SecondaryIpMaskSet, IfOOBNode1SecondaryIpMaskTest, IfOOBNode1SecondaryIpMaskDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},



{{10,FfFastForwardingEnable}, NULL, FfFastForwardingEnableGet, FfFastForwardingEnableSet, FfFastForwardingEnableTest, FfFastForwardingEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 1, 0, "2"},

{{10,FfCacheSize}, NULL, FfCacheSizeGet, FfCacheSizeSet, FfCacheSizeTest, FfCacheSizeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 1, 0, NULL},

{{10,FfIpChecksumValidationEnable}, NULL, FfIpChecksumValidationEnableGet, FfIpChecksumValidationEnableSet, FfIpChecksumValidationEnableTest, FfIpChecksumValidationEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 1, 0, "1"},

{{10,FfCachePurgeCount}, NULL, FfCachePurgeCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 1, 0, NULL},

{{10,FfCacheLastPurgeTime}, NULL, FfCacheLastPurgeTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 1, 0, NULL},

{{10,FfStaticEntryInvalidTrapEnable}, NULL, FfStaticEntryInvalidTrapEnableGet, FfStaticEntryInvalidTrapEnableSet, FfStaticEntryInvalidTrapEnableTest, FfStaticEntryInvalidTrapEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 1, 0, "1"},

{{10,FfCurrentStaticEntryInvalidCount}, NULL, FfCurrentStaticEntryInvalidCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 1, 0, NULL},

{{10,FfTotalEntryCount}, NULL, FfTotalEntryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 1, 0, NULL},




{{10,FfStaticEntryCount}, NULL, FfStaticEntryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 1, 0, NULL},

{{10,FfTotalPktsFastForwarded}, NULL, FfTotalPktsFastForwardedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 1, 0, NULL},

{{12,FfHostCacheDestAddr}, GetNextIndexFfHostCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FfHostCacheTableINDEX, 1, 1, 0, NULL},

{{12,FfHostCacheNextHopAddr}, GetNextIndexFfHostCacheTable, FfHostCacheNextHopAddrGet, FfHostCacheNextHopAddrSet, FfHostCacheNextHopAddrTest, FfHostCacheTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FfHostCacheTableINDEX, 1, 1, 0, NULL},

{{12,FfHostCacheIfIndex}, GetNextIndexFfHostCacheTable, FfHostCacheIfIndexGet, FfHostCacheIfIndexSet, FfHostCacheIfIndexTest, FfHostCacheTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FfHostCacheTableINDEX, 1, 1, 0, NULL},

{{12,FfHostCacheNextHopMediaAddr}, GetNextIndexFfHostCacheTable, FfHostCacheNextHopMediaAddrGet, FfHostCacheNextHopMediaAddrSet, FfHostCacheNextHopMediaAddrTest, FfHostCacheTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FfHostCacheTableINDEX, 1, 1, 0, NULL},

{{12,FfHostCacheHits}, GetNextIndexFfHostCacheTable, FfHostCacheHitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FfHostCacheTableINDEX, 1, 1, 0, NULL},

{{12,FfHostCacheLastHitTime}, GetNextIndexFfHostCacheTable, FfHostCacheLastHitTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FfHostCacheTableINDEX, 1, 1, 0, NULL},

{{12,FfHostCacheEntryType}, GetNextIndexFfHostCacheTable, FfHostCacheEntryTypeGet, FfHostCacheEntryTypeSet, FfHostCacheEntryTypeTest, FfHostCacheTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FfHostCacheTableINDEX, 1, 1, 0, "1"},

{{12,FfHostCacheRowStatus}, GetNextIndexFfHostCacheTable, FfHostCacheRowStatusGet, FfHostCacheRowStatusSet, FfHostCacheRowStatusTest, FfHostCacheTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FfHostCacheTableINDEX, 1, 1, 1, NULL},

{{10,FmMemoryResourceTrapEnable}, NULL, FmMemoryResourceTrapEnableGet, FmMemoryResourceTrapEnableSet, FmMemoryResourceTrapEnableTest, FmMemoryResourceTrapEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 1, 0, "1"},

{{10,FmTimersResourceTrapEnable}, NULL, FmTimersResourceTrapEnableGet, FmTimersResourceTrapEnableSet, FmTimersResourceTrapEnableTest, FmTimersResourceTrapEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 1, 0, "1"},

{{10,FmTracingEnable}, NULL, FmTracingEnableGet, FmTracingEnableSet, FmTracingEnableTest, FmTracingEnableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 1, 0, "0"},

{{10,FmMemAllocFailCount}, NULL, FmMemAllocFailCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FmTimerReqFailCount}, NULL, FmTimerReqFailCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsPacketAnalyserIndex}, GetNextIndexFsPacketAnalyserTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsPacketAnalyserTableINDEX, 1, 0, 0, NULL},

{{12,FsPacketAnalyserWatchValue}, GetNextIndexFsPacketAnalyserTable, FsPacketAnalyserWatchValueGet, FsPacketAnalyserWatchValueSet, FsPacketAnalyserWatchValueTest, FsPacketAnalyserTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPacketAnalyserTableINDEX, 1, 0, 0, NULL},

{{12,FsPacketAnalyserWatchMask}, GetNextIndexFsPacketAnalyserTable, FsPacketAnalyserWatchMaskGet, FsPacketAnalyserWatchMaskSet, FsPacketAnalyserWatchMaskTest, FsPacketAnalyserTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPacketAnalyserTableINDEX, 1, 0, 0, NULL},

{{12,FsPacketAnalyserWatchPorts}, GetNextIndexFsPacketAnalyserTable, FsPacketAnalyserWatchPortsGet, FsPacketAnalyserWatchPortsSet, FsPacketAnalyserWatchPortsTest, FsPacketAnalyserTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPacketAnalyserTableINDEX, 1, 0, 0, NULL},

{{12,FsPacketAnalyserMatchPorts}, GetNextIndexFsPacketAnalyserTable, FsPacketAnalyserMatchPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPacketAnalyserTableINDEX, 1, 0, 0, NULL},

{{12,FsPacketAnalyserCounter}, GetNextIndexFsPacketAnalyserTable, FsPacketAnalyserCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPacketAnalyserTableINDEX, 1, 0, 0, NULL},

{{12,FsPacketAnalyserTime}, GetNextIndexFsPacketAnalyserTable, FsPacketAnalyserTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPacketAnalyserTableINDEX, 1, 0, 0, NULL},

{{12,FsPacketAnalyserCreateTime}, GetNextIndexFsPacketAnalyserTable, FsPacketAnalyserCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsPacketAnalyserTableINDEX, 1, 0, 0, NULL},

{{12,FsPacketAnalyserStatus}, GetNextIndexFsPacketAnalyserTable, FsPacketAnalyserStatusGet, FsPacketAnalyserStatusSet, FsPacketAnalyserStatusTest, FsPacketAnalyserTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPacketAnalyserTableINDEX, 1, 0, 1, NULL},

{{12,FsPacketTransmitterIndex}, GetNextIndexFsPacketTransmitterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsPacketTransmitterTableINDEX, 1, 0, 0, NULL},

{{12,FsPacketTransmitterValue}, GetNextIndexFsPacketTransmitterTable, FsPacketTransmitterValueGet, FsPacketTransmitterValueSet, FsPacketTransmitterValueTest, FsPacketTransmitterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPacketTransmitterTableINDEX, 1, 0, 0, NULL},

{{12,FsPacketTransmitterPort}, GetNextIndexFsPacketTransmitterTable, FsPacketTransmitterPortGet, FsPacketTransmitterPortSet, FsPacketTransmitterPortTest, FsPacketTransmitterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPacketTransmitterTableINDEX, 1, 0, 0, NULL},

{{12,FsPacketTransmitterInterval}, GetNextIndexFsPacketTransmitterTable, FsPacketTransmitterIntervalGet, FsPacketTransmitterIntervalSet, FsPacketTransmitterIntervalTest, FsPacketTransmitterTableDep, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READWRITE, FsPacketTransmitterTableINDEX, 1, 0, 0, NULL},

{{12,FsPacketTransmitterCount}, GetNextIndexFsPacketTransmitterTable, FsPacketTransmitterCountGet, FsPacketTransmitterCountSet, FsPacketTransmitterCountTest, FsPacketTransmitterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsPacketTransmitterTableINDEX, 1, 0, 0, NULL},

{{12,FsPacketTransmitterStatus}, GetNextIndexFsPacketTransmitterTable, FsPacketTransmitterStatusGet, FsPacketTransmitterStatusSet, FsPacketTransmitterStatusTest, FsPacketTransmitterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPacketTransmitterTableINDEX, 1, 0, 1, NULL}
};
tMibData fscfaEntry = { 155, fscfaMibEntry };

#endif /* _FSCFADB_H */

