/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: iwfppp.h,v 
 *
 * Description:This file contains the definitions of the    
 *             global structures of the Inter-working    
 *             Functionality module of the CFA. It contains 
 *             Macros for accessing the IWF data strucuture 
 *             for the PPP/MP interfaces.           
 *
 *******************************************************************/
#ifndef _IWFPPP_H
#define _IWFPPP_H

#define   CFA_PPP_PID_SIZE                      2
#define   CFA_PPP_DEF_FCS_SIZE                 16
#define   CFA_PPP_TX_ACCM_SIZE                 32
#define   CFA_PPP_RX_ACCM_SIZE                  4

#define   CFA_LCP_PID                      0xc021
#define   CFA_BPDU_PID                     0x0031
#define   CFA_IPV4_PID                     0x0021
#define   CFA_RTP_CONTXT_PID               0x2065
#define   CFA_RTP_COMP_FULLHEAD_PID        0x0061
#define   CFA_RTP_COMP_UDP8_PID            0x0067
#define   CFA_RTP_COMP_RTP8_PID            0x0069
#define   CFA_RTP_COMP_UDP16_PID           0x2067
#define   CFA_RTP_COMP_RTP16_PID           0x2069
#define   CFA_STAP_PID                     0x0201

/* the following definitions map to the corresponding defines in
FuturePPP */

#define   CFA_PPP_INIT_FCS_16              0xffff
#define   CFA_PPP_GOOD_FCS_16              0xf0b8
#define   CFA_PPP_INIT_FCS_32          0xffffffff
#define   CFA_PPP_GOOD_FCS_32          0xdebb20e3

/* PPP/MP IWF struct */
typedef struct {
    UINT1  u1PppBypass;
    UINT1  u1Reserved;
    UINT2  u2Reserved;
} tPppIwfStruct;


/* MACROS and definitions for IP over PPP & PPP over HDLC*********************/

/* Macros for accessing IWF structures    */
#define   CFA_PPP_IWF(u4IfIndex)   ( (tPppIwfStruct *)CFA_IF_IWF(u4IfIndex) ) 

#define   CFA_PPP_IWF_BYPASS(u4IfIndex) \
          (CFA_PPP_IWF(u4IfIndex)->u1PppBypass)

#define   CFA_PPP_IS_PLAIN(u4IfIndex) \
          (!CFA_PPP_IWF_BYPASS(u4IfIndex))

#define   CFA_PPP_IS_ACFC_RX(u4IfIndex) \
          (CFA_PPP_IWF_BYPASS(u4IfIndex) & ACFC_PLAIN_RX)

#define   CFA_PPP_IS_ACFC_TX(u4IfIndex) \
          (CFA_PPP_IWF_BYPASS(u4IfIndex) & ACFC_PLAIN_TX)

#define   CFA_PPP_IS_PFC_RX(u4IfIndex) \
          (CFA_PPP_IWF_BYPASS(u4IfIndex) & PFC_PLAIN_RX)

#define   CFA_PPP_IS_PFC_TX(u4IfIndex) \
          (CFA_PPP_IWF_BYPASS(u4IfIndex) & PFC_PLAIN_TX)

#define   CFA_PPP_IS_NON_PLAIN_RX(u4IfIndex) \
          (CFA_PPP_IWF_BYPASS(u4IfIndex) & NON_PLAIN_RX)

#define   CFA_PPP_IS_NON_PLAIN_TX(u4IfIndex) \
          (CFA_PPP_IWF_BYPASS(u4IfIndex) & NON_PLAIN_TX)

#define   CFA_IS_HDLC_HDR(u2Hdr)            (u2Hdr == CFA_HDLC_HDR) 
#define   CFA_IS_LCP_PKT(u2Hdr)             (u2Hdr == CFA_LCP_PID)  
#define   CFA_IS_PFC_PKT(u2Hdr)             (u2Hdr & 0x0100)        
#define   CFA_IS_PFC_POSSIBLE(u2Protocol)   (u2Protocol < 256)      

#define   CFA_INIT_ASYNC_PARAMS(u4IfIndex) \
         { \
         CFA_GDD_ASYNC_PARAMS(u4IfIndex).u1TxFcsSize = CFA_PPP_DEF_FCS_SIZE; \
         CFA_GDD_ASYNC_PARAMS(u4IfIndex).u1RxFcsSize = CFA_PPP_DEF_FCS_SIZE; \
         CFA_GDD_ASYNC_PARAMS(u4IfIndex).pTxACCMap = gCfaDefaultTxACCM; \
         CFA_GDD_ASYNC_PARAMS(u4IfIndex).pRxACCMap = gCfaDefaultRxACCM; \
         }

#define   CFA_DEINIT_ACCM_PARAMS(u4IfIndex) \
      { \
       if (CFA_GDD_ASYNC_PARAMS(u4IfIndex).pTxACCMap != gCfaDefaultTxACCM) \
               MEM_FREE(CFA_GDD_ASYNC_PARAMS(u4IfIndex).pTxACCMap); \
         CFA_GDD_ASYNC_PARAMS(u4IfIndex).pTxACCMap = NULL; \
       if (CFA_GDD_ASYNC_PARAMS(u4IfIndex).pRxACCMap != gCfaDefaultRxACCM) \
               MEM_FREE(CFA_GDD_ASYNC_PARAMS(u4IfIndex).pRxACCMap); \
         CFA_GDD_ASYNC_PARAMS(u4IfIndex).pRxACCMap = NULL; \
      }

/*   
*   This macro checks whether given Octet is flagged in the ACC map or not.
*/
#define   MAPPED_IN_ACCM(Octet, pACCMap)   \
          ((pACCMap [Octet/8] & (0x01 << Octet % 8)) ? 1 : 0)

#endif /* _IWFPPP_H */

