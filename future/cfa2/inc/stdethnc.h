
#ifndef _H_i_EtherLike_MIB
#define _H_i_EtherLike_MIB
/* $Id: stdethnc.h,v 1.2 2016/10/21 13:42:21 siva Exp $
    ISS Wrapper header
    module EtherLike-MIB

 */


/********************************************************************
* FUNCTION NcDot3StatsAlignmentErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3StatsAlignmentErrorsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsAlignmentErrors );

/********************************************************************
* FUNCTION NcDot3StatsFCSErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3StatsFCSErrorsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsFCSErrors );

/********************************************************************
* FUNCTION NcDot3StatsSingleCollisionFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3StatsSingleCollisionFramesGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsSingleCollisionFrames );

/********************************************************************
* FUNCTION NcDot3StatsMultipleCollisionFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3StatsMultipleCollisionFramesGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsMultipleCollisionFrames );

/********************************************************************
* FUNCTION NcDot3StatsSQETestErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3StatsSQETestErrorsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsSQETestErrors );

/********************************************************************
* FUNCTION NcDot3StatsDeferredTransmissionsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3StatsDeferredTransmissionsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsDeferredTransmissions );

/********************************************************************
* FUNCTION NcDot3StatsLateCollisionsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3StatsLateCollisionsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsLateCollisions );

/********************************************************************
* FUNCTION NcDot3StatsExcessiveCollisionsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3StatsExcessiveCollisionsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsExcessiveCollisions );

/********************************************************************
* FUNCTION NcDot3StatsInternalMacTransmitErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3StatsInternalMacTransmitErrorsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsInternalMacTransmitErrors );

/********************************************************************
* FUNCTION NcDot3StatsCarrierSenseErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3StatsCarrierSenseErrorsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsCarrierSenseErrors );

/********************************************************************
* FUNCTION NcDot3StatsFrameTooLongsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3StatsFrameTooLongsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsFrameTooLongs );

/********************************************************************
* FUNCTION NcDot3StatsInternalMacReceiveErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3StatsInternalMacReceiveErrorsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsInternalMacReceiveErrors );

/********************************************************************
* FUNCTION NcDot3StatsEtherChipSetGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3StatsEtherChipSetGet (
                INT4 i4Dot3StatsIndex,
                UINT1 *pDot3StatsEtherChipSet );

/********************************************************************
* FUNCTION NcDot3StatsSymbolErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3StatsSymbolErrorsGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3StatsSymbolErrors );

/********************************************************************
* FUNCTION NcDot3StatsDuplexStatusGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3StatsDuplexStatusGet (
                INT4 i4Dot3StatsIndex,
                INT4 *pi4Dot3StatsDuplexStatus );

/********************************************************************
* FUNCTION NcDot3StatsRateControlAbilityGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3StatsRateControlAbilityGet (
                INT4 i4Dot3StatsIndex,
                INT4 *pi4Dot3StatsRateControlAbility );

/********************************************************************
* FUNCTION NcDot3StatsRateControlStatusGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3StatsRateControlStatusGet (
                INT4 i4Dot3StatsIndex,
                INT4 *pi4Dot3StatsRateControlStatus );

/********************************************************************
* FUNCTION NcDot3CollFrequenciesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3CollFrequenciesGet (
                INT4 i4IfIndex,
                INT4 i4Dot3CollCount,
                UINT4 *pu4Dot3CollFrequencies );

/********************************************************************
* FUNCTION NcDot3ControlFunctionsSupportedGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3ControlFunctionsSupportedGet (
                INT4 i4Dot3StatsIndex,
                UINT1   *pu1ControlFunctionsSupported);

/********************************************************************
* FUNCTION NcDot3ControlInUnknownOpcodesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3ControlInUnknownOpcodesGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3ControlInUnknownOpcodes );

/********************************************************************
* FUNCTION NcDot3HCControlInUnknownOpcodesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3HCControlInUnknownOpcodesGet (
                INT4 i4Dot3StatsIndex,
                unsigned long long *pu8Dot3HCControlInUnknownOpcodes );

/********************************************************************
* FUNCTION NcDot3PauseAdminModeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3PauseAdminModeSet (
                INT4 i4Dot3StatsIndex,
                INT4 i4Dot3PauseAdminMode );

/********************************************************************
* FUNCTION NcDot3PauseAdminModeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3PauseAdminModeTest (UINT4 *pu4Error,
                INT4 i4Dot3StatsIndex,
                INT4 i4Dot3PauseAdminMode );

/********************************************************************
* FUNCTION NcDot3PauseOperModeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3PauseOperModeGet (
                INT4 i4Dot3StatsIndex,
                INT4 *pi4Dot3PauseOperMode );

/********************************************************************
* FUNCTION NcDot3InPauseFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3InPauseFramesGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3InPauseFrames );

/********************************************************************
* FUNCTION NcDot3OutPauseFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3OutPauseFramesGet (
                INT4 i4Dot3StatsIndex,
                UINT4 *pu4Dot3OutPauseFrames );

/********************************************************************
* FUNCTION NcDot3HCInPauseFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3HCInPauseFramesGet (
                INT4 i4Dot3StatsIndex,
                unsigned long long *pu8Dot3HCInPauseFrames );

/********************************************************************
* FUNCTION NcDot3HCOutPauseFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3HCOutPauseFramesGet (
                INT4 i4Dot3StatsIndex,
                unsigned long long *pu8Dot3HCOutPauseFrames );

/********************************************************************
* FUNCTION NcDot3HCStatsAlignmentErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3HCStatsAlignmentErrorsGet (
                INT4 i4Dot3StatsIndex,
                unsigned long long *pu8Dot3HCStatsAlignmentErrors );

/********************************************************************
* FUNCTION NcDot3HCStatsFCSErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3HCStatsFCSErrorsGet (
                INT4 i4Dot3StatsIndex,
                unsigned long long *pu8Dot3HCStatsFCSErrors );

/********************************************************************
* FUNCTION NcDot3HCStatsInternalMacTransmitErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3HCStatsInternalMacTransmitErrorsGet (
                INT4 i4Dot3StatsIndex,
                unsigned long long *pu8Dot3HCStatsInternalMacTransmitErrors );

/********************************************************************
* FUNCTION NcDot3HCStatsFrameTooLongsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3HCStatsFrameTooLongsGet (
                INT4 i4Dot3StatsIndex,
                unsigned long long *pu8Dot3HCStatsFrameTooLongs );

/********************************************************************
* FUNCTION NcDot3HCStatsInternalMacReceiveErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3HCStatsInternalMacReceiveErrorsGet (
                INT4 i4Dot3StatsIndex,
                unsigned long long *pu8Dot3HCStatsInternalMacReceiveErrors );

/********************************************************************
* FUNCTION NcDot3HCStatsSymbolErrorsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT4 NcDot3HCStatsSymbolErrorsGet (
                INT4 i4Dot3StatsIndex,
                unsigned long long *pu8Dot3HCStatsSymbolErrors );

/* END i_EtherLike_MIB.c */


#endif
