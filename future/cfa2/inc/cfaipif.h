/* $Id: cfaipif.h,v 1.14 2016/03/06 10:07:08 siva Exp $ */

#ifndef __CFAIPIF_H__
#define __CFAIPIF_H__ 

/* Secondary IP address information */
typedef struct
{
    tTMO_SLL_NODE    NextNode;  /* Pointer to Next Secondary Address */ 
    UINT4            u4Addr;    /* Secondary Address */
    UINT4            u4BcastAddr; /* Broadcast Address associated with
                                     secondary IP address */
    INT4             i4RowStatus; 
    UINT2            u2AliasIndex; /* Alias index used for configuring
                                      in LinuxIp */
    UINT1            u1SubnetMask;  /* NetMask for the secondary address */
    UINT1            u1AllocMethod; /* Allocation Method */
}tIpAddrInfo;


INT4   CfaIpIfCreateIpIfTable  PROTO ((VOID));
INT4   CfaIpIfDestroyIpIfTable PROTO ((VOID));

INT4   CfaIpIfCreateIpInterface PROTO ((UINT4   u4CfaIfIndex));

INT4   CfaIpIfCompareInterfaceIndex PROTO ((tRBElem * pRBElem1, 
                                            tRBElem * pRBElem2));

INT4 CfaIpIfSetSecondaryAddressInfo PROTO ((UINT4 u4IfIndex, UINT4 u4Flag ,
                                            tIpAddrInfo *pIpAddrInfo));

INT4 CfaIpIfGetSecondaryAddressInfo PROTO ((UINT4 u4IfIndex,
                                           UINT4  u4SecIpAddress,
                                           tIpAddrInfo *pIpAddrInfo));
INT4 CfaIpIfGetNextSecondaryIpIndex PROTO ((UINT4 u4IfIndex,
                                            UINT4 u4SecIpAddress, 
                                            UINT4 *pu4NextIndex,
                                            UINT4 *pu4NextSecIpAddress));

INT4  CfaIpIfGetNextIndexIpIfTable PROTO ((UINT4 u4CfaIfIndex, 
                                           UINT4 *pu4NextIfIndex));

UINT1   CfaIpIfIsLocalNetOnIntf PROTO ((tIpIfRecord  *pIpIntf, 
                                        UINT4 u4IpAddress, UINT4 u4NetMask));

INT4 CfaIpIfIsOurInterfaceAddress PROTO ((UINT4 u4CfaIfIndex, UINT4 u4IpAddress));

VOID CfaIpIfHandleSecondaryAddress PROTO ((UINT4 u4IfIndex, UINT1 u1Flag));

VOID  CfaIpIfGetSecondaryAddressCount PROTO ((UINT4 u4IfIndex, UINT4 *pu4IfIndex));

INT4 CfaIpIfTblUnlock PROTO ((VOID));
INT4 CfaIpIfTblLock PROTO ((VOID));
VOID CfaHandleOutBoundPktFromLnxIpTap(tCRU_BUF_CHAIN_HEADER *, INT4, UINT4);
VOID CfaHandleInBoundPktToLnxIpTap(UINT4 u4IfIdx, tCRU_BUF_CHAIN_HEADER *pBuf,
                                   tEnetV2Header *pEthHdr, UINT1 u1Flag);
INT4 CfaIsInterfaceVlanExists (UINT2 u2PortVlanId);

extern VOID LnxIpPostPktToTap(UINT4, tCRU_BUF_CHAIN_HEADER *,tEnetV2Header *, UINT1);

#define CFA_IP_IF_TBL_SEMAPHORE       (UINT1 *) "CFAIPIF"

/* Prototype of functions used for linux programming. (used for OOB Mgmt port) */
#if defined(CLI_LNXIP_WANTED) || defined (SNMP_LNXIP_WANTED)
INT4 CfaIpUpdateInterfaceParams (UINT1 *pu1IntfName, UINT4 u4Flag,
                                 tIpConfigInfo * pIpPortParams);
#ifdef IP6_WANTED
INT4 CfaLinuxIpUpdateIpv6Addr (UINT4 u4IfIndex, tIp6Addr * pIp6Addr,
                                  INT4 i4PrefixLen, INT4 i4Command);
#endif
#endif 
#endif
