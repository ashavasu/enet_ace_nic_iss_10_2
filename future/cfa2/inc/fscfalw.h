/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fscfalw.h,v 1.32 2016/10/03 10:34:39 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfMaxInterfaces ARG_LIST((INT4 *));

INT1
nmhGetIfMaxPhysInterfaces ARG_LIST((INT4 *));

INT1
nmhGetIfAvailableIndex ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfMaxInterfaces ARG_LIST((INT4 ));

INT1
nmhSetIfMaxPhysInterfaces ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfMaxInterfaces ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IfMaxPhysInterfaces ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfMaxInterfaces ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IfMaxPhysInterfaces ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IfMainTable. */
INT1
nmhValidateIndexInstanceIfMainTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IfMainTable  */

INT1
nmhGetFirstIndexIfMainTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfMainTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfMainType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfMainMtu ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfMainAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfMainOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfMainEncapType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfMainBrgPortType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfMainRowStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfMainSubType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfMainNetworkType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfMainWanType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfMainDesc ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIfMainStorageType ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetIfMainExtSubType ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfMainType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfMainMtu ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfMainAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfMainEncapType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfMainBrgPortType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfMainRowStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfMainSubType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfMainNetworkType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfMainWanType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfMainDesc ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIfMainStorageType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfMainExtSubType ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfMainType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfMainMtu ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfMainAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfMainEncapType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfMainBrgPortType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfMainRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfMainSubType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfMainNetworkType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfMainWanType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfMainDesc ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IfMainStorageType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfMainExtSubType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfMainTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IfIpTable. */
INT1
nmhValidateIndexInstanceIfIpTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IfIpTable  */

INT1
nmhGetFirstIndexIfIpTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfIpTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfIpAddrAllocMethod ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfIpAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfIpSubnetMask ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfIpBroadcastAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfIpForwardingEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfIpAddrAllocProtocol ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfIpAddrAllocMethod ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfIpAddr ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIfIpSubnetMask ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIfIpBroadcastAddr ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIfIpForwardingEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfIpAddrAllocProtocol ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfIpAddrAllocMethod ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfIpAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IfIpSubnetMask ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IfIpBroadcastAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IfIpForwardingEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfIpAddrAllocProtocol ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfIpTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IfWanTable. */
INT1
nmhValidateIndexInstanceIfWanTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IfWanTable  */

INT1
nmhGetFirstIndexIfWanTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfWanTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfWanInterfaceType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfWanConnectionType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfWanVirtualPathId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfWanVirtualCircuitId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfWanPeerMediaAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIfWanSustainedSpeed ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfWanPeakSpeed ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfWanMaxBurstSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfWanIpQosProfileIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfWanIdleTimeout ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfWanPeerIpAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfWanRtpHdrComprEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfWanPersistence ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfWanConnectionType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfWanVirtualPathId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfWanVirtualCircuitId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfWanPeerMediaAddress ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIfWanSustainedSpeed ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfWanPeakSpeed ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfWanMaxBurstSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfWanIpQosProfileIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfWanIdleTimeout ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfWanPeerIpAddr ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIfWanRtpHdrComprEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfWanPersistence ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfWanConnectionType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfWanVirtualPathId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfWanVirtualCircuitId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfWanPeerMediaAddress ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IfWanSustainedSpeed ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfWanPeakSpeed ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfWanMaxBurstSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfWanIpQosProfileIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfWanIdleTimeout ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfWanPeerIpAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IfWanRtpHdrComprEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfWanPersistence ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfWanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IfAutoCktProfileTable. */
INT1
nmhValidateIndexInstanceIfAutoCktProfileTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IfAutoCktProfileTable  */

INT1
nmhGetFirstIndexIfAutoCktProfileTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfAutoCktProfileTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfAutoProfileIfType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfAutoProfileIpAddrAllocMethod ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfAutoProfileDefIpSubnetMask ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfAutoProfileDefIpBroadcastAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfAutoProfileIdleTimeout ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfAutoProfileEncapType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfAutoProfileIpQosProfileIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfAutoProfileRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfAutoProfileIpAddrAllocMethod ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfAutoProfileDefIpSubnetMask ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIfAutoProfileDefIpBroadcastAddr ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIfAutoProfileIdleTimeout ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfAutoProfileEncapType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfAutoProfileIpQosProfileIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfAutoProfileRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfAutoProfileIpAddrAllocMethod ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfAutoProfileDefIpSubnetMask ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IfAutoProfileDefIpBroadcastAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IfAutoProfileIdleTimeout ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfAutoProfileEncapType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfAutoProfileIpQosProfileIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfAutoProfileRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfAutoCktProfileTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IfIvrTable. */
INT1
nmhValidateIndexInstanceIfIvrTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IfIvrTable  */

INT1
nmhGetFirstIndexIfIvrTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfIvrTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfIvrBridgedIface ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfIvrBridgedIface ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfIvrBridgedIface ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfIvrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfSetMgmtVlanList ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIfResetMgmtVlanList ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfSetMgmtVlanList ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIfResetMgmtVlanList ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfSetMgmtVlanList ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IfResetMgmtVlanList ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfSetMgmtVlanList ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IfResetMgmtVlanList ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IfSecondaryIpAddressTable. */
INT1
nmhValidateIndexInstanceIfSecondaryIpAddressTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IfSecondaryIpAddressTable  */

INT1
nmhGetFirstIndexIfSecondaryIpAddressTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfSecondaryIpAddressTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfSecondaryIpSubnetMask ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIfSecondaryIpBroadcastAddr ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIfSecondaryIpRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfSecondaryIpSubnetMask ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetIfSecondaryIpBroadcastAddr ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetIfSecondaryIpRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfSecondaryIpSubnetMask ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2IfSecondaryIpBroadcastAddr ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2IfSecondaryIpRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfSecondaryIpAddressTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IfMainExtTable. */
INT1
nmhValidateIndexInstanceIfMainExtTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IfMainExtTable  */

INT1
nmhGetFirstIndexIfMainExtTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfMainExtTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfMainExtMacAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetIfMainExtSysSpecificPortID ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfMainExtInterfaceType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfMainExtPortSecState ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetIfMainExtInPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfMainExtLinkUpEnabledStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfMainExtLinkUpDelayTimer ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfMainExtLinkUpRemainingTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhSetIfMainExtLinkUpEnabledStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfMainExtLinkUpDelayTimer ARG_LIST((INT4  ,UINT4 ));

INT1
nmhTestv2IfMainExtLinkUpEnabledStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfMainExtLinkUpDelayTimer ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfMainExtMacAddress ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetIfMainExtSysSpecificPortID ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIfMainExtInterfaceType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfMainExtPortSecState ARG_LIST((INT4  ,INT4 ));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfMainExtMacAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2IfMainExtSysSpecificPortID ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IfMainExtInterfaceType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfMainExtPortSecState ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfMainExtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IfCustTLVTable. */
INT1
nmhValidateIndexInstanceIfCustTLVTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IfCustTLVTable  */

INT1
nmhGetFirstIndexIfCustTLVTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfCustTLVTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfCustTLVLength ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIfCustTLVValue ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIfCustTLVRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfCustTLVLength ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetIfCustTLVValue ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIfCustTLVRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfCustTLVLength ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2IfCustTLVValue ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IfCustTLVRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfCustTLVTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IfCustOpaqueAttrTable. */
INT1
nmhValidateIndexInstanceIfCustOpaqueAttrTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IfCustOpaqueAttrTable  */

INT1
nmhGetFirstIndexIfCustOpaqueAttrTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfCustOpaqueAttrTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfCustOpaqueAttribute ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIfCustOpaqueRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfCustOpaqueAttribute ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetIfCustOpaqueRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfCustOpaqueAttribute ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2IfCustOpaqueRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfCustOpaqueAttrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IfBridgeILanIfTable. */
INT1
nmhValidateIndexInstanceIfBridgeILanIfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IfBridgeILanIfTable  */

INT1
nmhGetFirstIndexIfBridgeILanIfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfBridgeILanIfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfBridgeILanIfStatus ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for IfTypeProtoDenyTable. */
INT1
nmhValidateIndexInstanceIfTypeProtoDenyTable ARG_LIST((UINT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IfTypeProtoDenyTable  */

INT1
nmhGetFirstIndexIfTypeProtoDenyTable ARG_LIST((UINT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfTypeProtoDenyTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfTypeProtoDenyRowStatus ARG_LIST((UINT4  , INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfTypeProtoDenyRowStatus ARG_LIST((UINT4  , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfTypeProtoDenyRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfTypeProtoDenyTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhGetIfmDebug ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfmDebug ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfmDebug ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfmDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IfIvrMappingTable. */
INT1
nmhValidateIndexInstanceIfIvrMappingTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IfIvrMappingTable  */

INT1
nmhGetFirstIndexIfIvrMappingTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfIvrMappingTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfIvrMappingRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfIvrMappingRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfIvrMappingRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfIvrMappingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));



/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFfFastForwardingEnable ARG_LIST((INT4 *));

INT1
nmhGetFfCacheSize ARG_LIST((INT4 *));

INT1
nmhGetFfIpChecksumValidationEnable ARG_LIST((INT4 *));

INT1
nmhGetFfCachePurgeCount ARG_LIST((UINT4 *));

INT1
nmhGetFfCacheLastPurgeTime ARG_LIST((UINT4 *));

INT1
nmhGetFfStaticEntryInvalidTrapEnable ARG_LIST((INT4 *));

INT1
nmhGetFfCurrentStaticEntryInvalidCount ARG_LIST((UINT4 *));

INT1
nmhGetFfTotalEntryCount ARG_LIST((UINT4 *));

INT1
nmhGetFfStaticEntryCount ARG_LIST((UINT4 *));

INT1
nmhGetFfTotalPktsFastForwarded ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFfFastForwardingEnable ARG_LIST((INT4 ));

INT1
nmhSetFfCacheSize ARG_LIST((INT4 ));

INT1
nmhSetFfIpChecksumValidationEnable ARG_LIST((INT4 ));

INT1
nmhSetFfStaticEntryInvalidTrapEnable ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FfFastForwardingEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FfCacheSize ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FfIpChecksumValidationEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FfStaticEntryInvalidTrapEnable ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FfFastForwardingEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FfCacheSize ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FfIpChecksumValidationEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FfStaticEntryInvalidTrapEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FfHostCacheTable. */
INT1
nmhValidateIndexInstanceFfHostCacheTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FfHostCacheTable  */

INT1
nmhGetFirstIndexFfHostCacheTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFfHostCacheTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFfHostCacheNextHopAddr ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFfHostCacheIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFfHostCacheNextHopMediaAddr ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFfHostCacheHits ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFfHostCacheLastHitTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFfHostCacheEntryType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFfHostCacheRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFfHostCacheNextHopAddr ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFfHostCacheIfIndex ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFfHostCacheNextHopMediaAddr ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFfHostCacheEntryType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFfHostCacheRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FfHostCacheNextHopAddr ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FfHostCacheIfIndex ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FfHostCacheNextHopMediaAddr ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FfHostCacheEntryType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FfHostCacheRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FfHostCacheTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFmMemoryResourceTrapEnable ARG_LIST((INT4 *));

INT1
nmhGetFmTimersResourceTrapEnable ARG_LIST((INT4 *));

INT1
nmhGetFmTracingEnable ARG_LIST((INT4 *));

INT1
nmhGetFmMemAllocFailCount ARG_LIST((UINT4 *));

INT1
nmhGetFmTimerReqFailCount ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFmMemoryResourceTrapEnable ARG_LIST((INT4 ));

INT1
nmhSetFmTimersResourceTrapEnable ARG_LIST((INT4 ));

INT1
nmhSetFmTracingEnable ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FmMemoryResourceTrapEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FmTimersResourceTrapEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FmTracingEnable ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FmMemoryResourceTrapEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FmTimersResourceTrapEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FmTracingEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IfHCErrorTable. */
INT1
nmhValidateIndexInstanceIfHCErrorTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IfHCErrorTable  */

INT1
nmhGetFirstIndexIfHCErrorTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfHCErrorTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfHCInDiscards ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIfHCInErrors ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIfHCInUnknownProtos ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIfHCOutDiscards ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIfHCOutErrors ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for IfAvailableIndexTable. */
INT1
nmhValidateIndexInstanceIfAvailableIndexTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IfAvailableIndexTable  */

INT1
nmhGetFirstIndexIfAvailableIndexTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfAvailableIndexTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfAvailableFreeIndex ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for FsPacketAnalyserTable. */
INT1
nmhValidateIndexInstanceFsPacketAnalyserTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPacketAnalyserTable  */

INT1
nmhGetFirstIndexFsPacketAnalyserTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPacketAnalyserTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPacketAnalyserWatchValue ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPacketAnalyserWatchMask ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPacketAnalyserWatchPorts ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPacketAnalyserMatchPorts ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPacketAnalyserCounter ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsPacketAnalyserTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsPacketAnalyserCreateTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsPacketAnalyserStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPacketAnalyserWatchValue ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPacketAnalyserWatchMask ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPacketAnalyserWatchPorts ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPacketAnalyserStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPacketAnalyserWatchValue ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPacketAnalyserWatchMask ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPacketAnalyserWatchPorts ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPacketAnalyserStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPacketAnalyserTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPacketTransmitterTable. */
INT1
nmhValidateIndexInstanceFsPacketTransmitterTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPacketTransmitterTable  */

INT1
nmhGetFirstIndexFsPacketTransmitterTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPacketTransmitterTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPacketTransmitterValue ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPacketTransmitterPort ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPacketTransmitterInterval ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsPacketTransmitterCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsPacketTransmitterStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPacketTransmitterValue ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPacketTransmitterPort ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhSetFsPacketTransmitterInterval ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsPacketTransmitterCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsPacketTransmitterStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPacketTransmitterValue ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPacketTransmitterPort ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhTestv2FsPacketTransmitterInterval ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsPacketTransmitterCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsPacketTransmitterStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPacketTransmitterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhGetIfSecurityBridging ARG_LIST((INT4 *));

INT1
nmhGetIfSetSecVlanList ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIfResetSecVlanList ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIfSecIvrIfIndex ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfSecurityBridging ARG_LIST((INT4 ));

INT1
nmhSetIfSetSecVlanList ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIfResetSecVlanList ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIfSecIvrIfIndex ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfSecurityBridging ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IfSetSecVlanList ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IfResetSecVlanList ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IfSecIvrIfIndex ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfSecurityBridging ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IfSetSecVlanList ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IfResetSecVlanList ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IfSecIvrIfIndex ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhGetIfIpDestMacAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhTestv2IfIpDestMacAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhSetIfIpDestMacAddress ARG_LIST((INT4  ,tMacAddr ));

INT1
 nmhTestv2IfIpUnnumAssocIPIf (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                              UINT4 u4TestValIfIpUnnumAssocIPIf);
INT1
nmhSetIfIpUnnumAssocIPIf (INT4 i4IfMainIndex,
                           UINT4 u4TestValIfIpUnnumAssocIPIf);
INT1
nmhGetIfIpUnnumAssocIPIf (INT4 i4IfMainIndex,
                           UINT4 *pu4RetValIfIpUnnumAssocIPIf );

/* Proto Validate Index Instance for IfACTable. */
INT1
nmhValidateIndexInstanceIfACTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IfACTable  */

INT1
nmhGetFirstIndexIfACTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfACTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfACPortIdentifier ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfACCustomerVlan ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfACPortIdentifier ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfACCustomerVlan ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfACPortIdentifier ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfACCustomerVlan ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfACTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhGetIfIpIntfStatsEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhSetIfIpIntfStatsEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhTestv2IfIpIntfStatsEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


/* Low Level Routines for PortVlanId Object */
  
INT1
nmhGetIfIpPortVlanId ARG_LIST((INT4 ,INT4 *));
 
INT1
nmhSetIfIpPortVlanId ARG_LIST((INT4  ,INT4 ));
 
INT1
nmhTestv2IfIpPortVlanId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


INT1
nmhGetIfMainUfdOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfMainUfdDownlinkDisabledCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfMainUfdDownlinkEnabledCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIfMainDesigUplinkStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfMainEncapDot1qVlanId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfMainPortRole ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfMainUfdGroupId ARG_LIST((INT4 ,INT4 *));

INT1
nmhSetIfMainPortRole ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfMainUfdGroupId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfMainDesigUplinkStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfMainEncapDot1qVlanId ARG_LIST((INT4  ,INT4 ));


INT1
nmhTestv2IfMainPortRole ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfMainUfdGroupId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfMainDesigUplinkStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfMainEncapDot1qVlanId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhGetIfUfdSystemControl ARG_LIST((INT4 *));

INT1
nmhGetIfUfdModuleStatus ARG_LIST((INT4 *));
INT1

nmhSetIfUfdSystemControl ARG_LIST((INT4 ));

INT1
nmhSetIfUfdModuleStatus ARG_LIST((INT4 ));

INT1
nmhTestv2IfUfdSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IfUfdModuleStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhDepv2IfUfdSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IfUfdModuleStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IfUfdGroupTable. */
INT1
nmhValidateIndexInstanceIfUfdGroupTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IfUfdGroupTable  */

INT1
nmhGetFirstIndexIfUfdGroupTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIfUfdGroupTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIfUfdGroupName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIfUfdGroupStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfUfdGroupUplinkPorts ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIfUfdGroupDownlinkPorts ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIfUfdGroupDesigUplinkPort ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfUfdGroupUplinkCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfUfdGroupDownlinkCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIfUfdGroupRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfUfdGroupName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIfUfdGroupUplinkPorts ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIfUfdGroupDownlinkPorts ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIfUfdGroupDesigUplinkPort ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIfUfdGroupRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfUfdGroupName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IfUfdGroupUplinkPorts ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IfUfdGroupDownlinkPorts ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IfUfdGroupDesigUplinkPort ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IfUfdGroupRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfUfdGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));



/* Split Horizon */

INT1
nmhTestv2IfSplitHorizonSysControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IfSplitHorizonModStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhDepv2IfSplitHorizonSysControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IfSplitHorizonModStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhSetIfSplitHorizonSysControl ARG_LIST((INT4 ));

INT1
nmhSetIfSplitHorizonModStatus ARG_LIST((INT4 ));

INT1
nmhGetIfSplitHorizonSysControl ARG_LIST((INT4 *));

INT1
nmhGetIfSplitHorizonModStatus ARG_LIST((INT4 *));

/* End of Split Horizon */

/* Proto type for Low Level GET Routine for Secondary ip of OOB interface.*/

INT1
nmhGetIfOOBNode0SecondaryIpAddress ARG_LIST((UINT4 *));

INT1
nmhGetIfOOBNode0SecondaryIpMask ARG_LIST((UINT4 *));

INT1
nmhGetIfOOBNode1SecondaryIpAddress ARG_LIST((UINT4 *));

INT1
nmhGetIfOOBNode1SecondaryIpMask ARG_LIST((UINT4 *));

/* Low Level SET Routine for Secondary ip of OOB interface. */

INT1
nmhSetIfOOBNode0SecondaryIpAddress ARG_LIST((UINT4 ));

INT1
nmhSetIfOOBNode0SecondaryIpMask ARG_LIST((UINT4 ));

INT1
nmhSetIfOOBNode1SecondaryIpAddress ARG_LIST((UINT4 ));

INT1
nmhSetIfOOBNode1SecondaryIpMask ARG_LIST((UINT4 ));

/* Low Level TEST Routines for secondary ip of OOB interface.  */

INT1
nmhTestv2IfOOBNode0SecondaryIpAddress ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2IfOOBNode0SecondaryIpMask ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2IfOOBNode1SecondaryIpAddress ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2IfOOBNode1SecondaryIpMask ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for secondary ip of OOB interface.  */

INT1
nmhDepv2IfOOBNode0SecondaryIpAddress ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IfOOBNode0SecondaryIpMask ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IfOOBNode1SecondaryIpAddress ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IfOOBNode1SecondaryIpMask ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhGetIfLinkUpEnabledStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIfLinkUpEnabledStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IfLinkUpEnabledStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfLinkUpEnabledStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
/* End of Split Horizon */
