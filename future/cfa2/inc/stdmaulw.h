/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: stdmaulw.h,v 1.16 2016/09/17 12:43:02 siva Exp $
 * Description: This file contains the definitions of the prototypes
 *              and defines used by the low level routines of the
 *              ethermib.
 *
 *******************************************************************/

#ifndef _STDMAULW_H_
#define _STDMAULW_H_

INT4 nmhValidateIndexInstanceRpMauTable PROTO ((INT4, INT4, INT4));
INT4 nmhGetRpMauType PROTO ((INT4, INT4, INT4, tSNMP_OID_TYPE *));
INT4 nmhTestv2RpMauStatus PROTO ((UINT4 *, INT4, INT4, INT4, INT4 *));
INT4 nmhSetRpMauStatus PROTO ((INT4, INT4, INT4, INT4));
INT4 nmhGetRpMauStatus PROTO ((INT4, INT4, INT4, INT4 *));
INT4 nmhGetRpMauMediaAvailable PROTO ((INT4, INT4, INT4, INT4 *));
INT4 nmhGetRpMauMediaAvailableStateExits PROTO ((INT4, INT4, INT4, UINT4 *));
INT4 nmhGetRpMauJabberState PROTO ((INT4, INT4, INT4, INT4 *));
INT4 nmhGetRpMauJabberingStateEnters PROTO ((INT4, INT4, INT4, UINT4 *));
INT4 nmhGetRpMauFalseCarriers PROTO ((INT4, INT4, INT4, UINT4 *));
/* Low Level DEP Routines for.  */

INT1
nmhDepv2RpMauTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT4 nmhValidateIndexInstanceRpJackTable PROTO ((INT4, INT4, INT4, INT4));
INT4 nmhGetRpJackType PROTO ((INT4, INT4, INT4, INT4, INT4 *));
INT4 nmhValidateIndexInstanceIfMauTable PROTO ((INT4, INT4));
INT4 nmhGetIfMauType PROTO ((INT4, INT4, tSNMP_OID_TYPE *));
INT4 nmhTestv2IfMauStatus PROTO ((UINT4 *, INT4, INT4, INT4 *));
INT4 nmhSetIfMauStatus PROTO ((INT4, INT4, INT4));
INT4 nmhGetIfMauStatus PROTO ((INT4, INT4, INT4 *));
INT4 nmhGetIfMauMediaAvailable PROTO ((INT4, INT4, INT4 *));
INT4 nmhGetIfMauMediaAvailableStateExits PROTO ((INT4, INT4, UINT4 *));
INT4 nmhGetIfMauJabberState PROTO ((INT4, INT4, INT4 *));
INT4 nmhGetIfMauJabberingStateEnters PROTO ((INT4, INT4, UINT4 *));
INT4 nmhGetIfMauFalseCarriers PROTO ((INT4, INT4, UINT4 *));
INT4 nmhGetIfMauTypeList PROTO ((INT4, INT4, INT4 *));
INT4 nmhTestv2IfMauDefaultType PROTO ((UINT4 *, INT4, INT4, tSNMP_OID_TYPE *));
INT4 nmhSetIfMauDefaultType PROTO ((INT4, INT4, tSNMP_OID_TYPE *));
INT4 nmhGetIfMauDefaultType PROTO ((INT4, INT4, tSNMP_OID_TYPE *));
INT4 nmhGetIfMauAutoNegSupported PROTO ((INT4, INT4, INT4 *));
INT4 nmhGetIfMauTypeListBits PROTO ((INT4, INT4, tSNMP_OCTET_STRING_TYPE*));
INT4 nmhValidateIndexInstanceIfJackTable PROTO ((INT4, INT4, INT4));
INT4 nmhGetIfJackType PROTO ((INT4, INT4, INT4, INT4 *));
INT4 nmhTestv2IfMauAutoNegAdminStatus PROTO ((UINT4 *, INT4, INT4, INT4 *));
INT4 nmhSetIfMauAutoNegAdminStatus PROTO ((INT4, INT4, INT4));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfMauTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT4 nmhValidateIndexInstanceIfMauAutoNegTable PROTO ((INT4, INT4));
INT4 nmhGetIfMauAutoNegAdminStatus PROTO ((INT4, INT4, INT4 *));
INT4 nmhGetIfMauAutoNegRemoteSignaling PROTO ((INT4, INT4, INT4 *));
INT4 nmhGetIfMauAutoNegConfig PROTO ((INT4, INT4, INT4 *));
INT4 nmhGetIfMauAutoNegCapability PROTO ((INT4, INT4, INT4 *));
INT4 nmhTestv2IfMauAutoNegCapAdvertised PROTO ((UINT4 *, INT4, INT4, INT4 *));
INT4 nmhSetIfMauAutoNegCapAdvertised PROTO ((INT4, INT4, INT4));
INT4 nmhGetIfMauAutoNegCapAdvertised PROTO ((INT4, INT4, INT4 *));
INT4 nmhGetIfMauAutoNegCapReceived PROTO ((INT4, INT4, INT4 *));
INT4 nmhTestv2IfMauAutoNegRestart PROTO ((UINT4 *, INT4, INT4, INT4 *));
INT4 nmhSetIfMauAutoNegRestart PROTO ((INT4, INT4, INT4));
INT4 nmhGetIfMauAutoNegRestart PROTO ((INT4, INT4, INT4 *));
INT4 nmhGetIfMauAutoNegCapabilityBits PROTO ((INT4, INT4, tSNMP_OCTET_STRING_TYPE*));
INT4 nmhTestv2IfMauAutoNegCapAdvertisedBits PROTO ((UINT4 *, INT4, INT4, 
                                                    tSNMP_OCTET_STRING_TYPE *));
INT4 nmhSetIfMauAutoNegCapAdvertisedBits PROTO ((INT4, INT4, tSNMP_OCTET_STRING_TYPE *));
INT4 nmhGetIfMauAutoNegCapAdvertisedBits PROTO ((INT4, INT4, tSNMP_OCTET_STRING_TYPE*));
INT4 nmhGetIfMauAutoNegCapReceivedBits PROTO ((INT4, INT4, tSNMP_OCTET_STRING_TYPE*));
INT4 nmhTestv2IfMauAutoNegRemoteFaultAdvertised PROTO ((UINT4 *, INT4, INT4,
                                                        INT4  *));
INT4 nmhSetIfMauAutoNegRemoteFaultAdvertised PROTO ((INT4, INT4, INT4));
INT4 nmhGetIfMauAutoNegRemoteFaultAdvertised PROTO ((INT4, INT4, INT4 *));
INT4 nmhGetIfMauAutoNegRemoteFaultReceived PROTO ((INT4, INT4, INT4 *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IfMauAutoNegTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT4 nmhValidateIndexInstanceBroadMauBasicTable PROTO ((INT4, INT4));
INT4 nmhGetBroadMauXmtRcvSplitType PROTO ((INT4, INT4, INT4 *));
INT4 nmhGetBroadMauXmtCarrierFreq PROTO ((INT4, INT4, INT4 *));
INT4 nmhGetBroadMauTranslationFreq PROTO ((INT4, INT4, INT4 *));

/* nmh Get NEXT FUNCTIONS    */
INT4 nmhGetFirstIndexBroadMauBasicTable PROTO ((INT4 *, INT4 *));
INT4 nmhGetNextIndexBroadMauBasicTable PROTO ((INT4, INT4 *, INT4, INT4 *));
INT4 nmhGetFirstIndexIfMauAutoNegTable PROTO ((INT4 *, INT4 *));
INT4 nmhGetNextIndexIfMauAutoNegTable PROTO ((INT4, INT4 *, INT4, INT4 *));
INT4 nmhGetFirstIndexIfJackTable PROTO ((INT4 *, INT4 *, INT4 *));
INT4 nmhGetNextIndexIfJackTable PROTO ((INT4, INT4 *, INT4, INT4 *, INT4, 
                                        INT4 *));
INT4 nmhGetFirstIndexIfMauTable PROTO ((INT4 *, INT4 *));
INT4 nmhGetNextIndexIfMauTable PROTO ((INT4, INT4 *, INT4, INT4 *));
INT4 nmhGetFirstIndexRpJackTable PROTO ((INT4 *, INT4 *, INT4 *, INT4 *));
INT4 nmhGetNextIndexRpJackTable PROTO ((INT4, INT4 *, INT4, INT4 *, INT4,
                                        INT4 *, INT4, INT4 *));
INT4 nmhGetFirstIndexRpMauTable PROTO ((INT4 *, INT4 *, INT4 *));
INT4 nmhGetNextIndexRpMauTable PROTO ((INT4, INT4 *, INT4, INT4 *, INT4, 
                                       INT4 *));
INT4 nmhGetIfMauHCFalseCarriers ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));
#define MAU_TYPE_LEN                 8   /* for ifMauType & ifMauDefault Type*/
#define MAU_MAX_IF_COUNT             1    /* The maximum number of MAUs 
                                           * attached to an interface 
                                           */                               
#define MAU_MAX_JACK_COUNT           1    /* Same as above */ 
#define MAU_AUTO_NEG_BIT_SIZE        4

#ifndef NPAPI_WANTED
#define MAU_TYPE_AUI                 1
#define MAU_TYPE_10_BASE_5           2          
#define MAU_TYPE_FOIRL               3
#define MAU_TYPE_10_BASE_2           4
#define MAU_TYPE_10_BASE_T           5
#define MAU_TYPE_10_BASE_FP          6
#define MAU_TYPE_10_BASE_FB          7 
#define MAU_TYPE_10_BASE_FL          8
#define MAU_TYPE_10_BROAD_36         9
#define MAU_TYPE_10_BASE_THD        10
#define MAU_TYPE_10_BASE_TFD        11
#define MAU_TYPE_10_BASE_FLHD       12
#define MAU_TYPE_10_BASE_FLFD       13
#define MAU_TYPE_100_BASE_T4        14
#define MAU_TYPE_100_BASE_TXHD      15
#define MAU_TYPE_100_BASE_TXFD      16
#define MAU_TYPE_100_BASE_FXHD      17
#define MAU_TYPE_100_BASE_FXFD      18
#define MAU_TYPE_100_BASE_T2HD      19
#define MAU_TYPE_100_BASE_T2FD      20
#define MAU_TYPE_1000_BASE_XHD      21
#define MAU_TYPE_1000_BASE_XFD      22
#define MAU_TYPE_1000_BASE_LXHD     23
#define MAU_TYPE_1000_BASE_LXFD     24
#define MAU_TYPE_1000_BASE_SXHD     25
#define MAU_TYPE_1000_BASE_SXFD     26
#define MAU_TYPE_1000_BASE_CXHD     27
#define MAU_TYPE_1000_BASE_CXFD     28
#define MAU_TYPE_1000_BASE_THD      29
#define MAU_TYPE_1000_BASE_TFD      30
#define MAU_TYPE_10GIG_BASE_X       31
/* For 40GB next available macro value from NPAPI is 54 */
#define MAU_TYPE_56GIG_BASE_X       57
#define MAU_TYPE_40GIG_BASE_X       54
#define MAU_TYPE_2500_BASE_THD      55
#define MAU_TYPE_2500_BASE_TFD      56
#define MAU_TYPE_25GIG_BASE_X       58
#define MAU_TYPE_100GIG_BASE_X      59


#define MAU_AUTO_OTHER               1
#define MAU_AUTO_CONFIGURING         2
#define MAU_AUTO_COMPLETE            3
#define MAU_AUTO_DISABLED            4
#define MAU_AUTO_PLL_DETECT_FAIL     5
#define MAU_AUTO_NO_ERROR            1
#define MAU_AUTO_ERROR               4
#define MAU_AUTO_SIG_DETECTED        1  
#define MAU_NO_AUTO_SIG_DETECTED     2
#define MAU_AUTO_RESTART             1
#define MAU_AUTO_NO_RESTART          2
#define MAU_JACK_OTHER               1
#define MAU_STATUS_OTHER             1
#define MAU_STATUS_UNKNOWN           2
#define MAU_STATUS_RESET             6
#define MAU_JACK_OTHER               1
#define MAU_JABBER_UNKNOWN           2
#define MAU_MEDIA_UNKNOWN            2

/* IANAifMauAutoNegCapBits  */
#define MAU_AUTO_NEG_BIT_OTHER           0x1       /* bit position 0 */
#define MAU_AUTO_NEG_BIT_10_BASE_T       0x2       /* bit position 1 */
#define MAU_AUTO_NEG_BIT_10_BASE_TFD     0x4       /* bit position 2 */
#define MAU_AUTO_NEG_BIT_100_BASE_T4     0x8       /* bit position 3 */
#define MAU_AUTO_NEG_BIT_100_BASE_TX     0x10      /* bit position 4 */
#define MAU_AUTO_NEG_BIT_100_BASE_TXFD   0x20      /* bit position 5 */
#define MAU_AUTO_NEG_BIT_100_BASE_T2     0x40      /* bit position 6 */
#define MAU_AUTO_NEG_BIT_100_BASE_T2FD   0x80      /* bit position 7 */
#define MAU_AUTO_NEG_BIT_FDX_PAUSE       0x100     /* bit position 8 */
#define MAU_AUTO_NEG_BIT_FDX_A_PAUSE     0x200     /* bit position 9 */
#define MAU_AUTO_NEG_BIT_FDX_S_PAUSE     0x400     /* bit position 10 */
#define MAU_AUTO_NEG_BIT_FDX_B_PAUSE     0x800     /* bit position 11 */
#define MAU_AUTO_NEG_BIT_1000_BASE_X     0x1000    /* bit position 12 */
#define MAU_AUTO_NEG_BIT_1000_BASE_XFD   0x2000    /* bit position 13 */
#define MAU_AUTO_NEG_BIT_1000_BASE_T     0x4000    /* bit position 14 */
#define MAU_AUTO_NEG_BIT_1000_BASE_TFD   0x8000    /* bit position 15 */

#endif

#endif   /* _STDMAULW_H_ */
