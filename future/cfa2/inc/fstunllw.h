/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstunllw.h,v 1.4 2008/08/20 13:40:38 premap-iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsTunlIfTable. */
INT1
nmhValidateIndexInstanceFsTunlIfTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsTunlIfTable  */

INT1
nmhGetFirstIndexFsTunlIfTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsTunlIfTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTunlIfHopLimit ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTunlIfSecurity ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTunlIfTOS ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTunlIfFlowLabel ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTunlIfMTU ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTunlIfDirFlag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTunlIfDirection ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTunlIfEncaplmt ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsTunlIfEncapOption ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTunlIfIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTunlIfAlias ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsTunlIfCksumFlag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTunlIfPmtuFlag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsTunlIfStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsTunlIfHopLimit ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTunlIfTOS ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTunlIfFlowLabel ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTunlIfDirFlag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTunlIfDirection ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTunlIfEncaplmt ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsTunlIfEncapOption ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTunlIfAlias ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsTunlIfCksumFlag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTunlIfPmtuFlag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsTunlIfStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsTunlIfHopLimit ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTunlIfTOS ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTunlIfFlowLabel ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTunlIfDirFlag ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTunlIfDirection ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTunlIfEncaplmt ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsTunlIfEncapOption ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTunlIfAlias ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsTunlIfCksumFlag ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTunlIfPmtuFlag ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTunlIfStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsTunlIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
