/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddvtss.h,v 1.1 2014/08/13 12:56:21 siva Exp $   
 *
 * Description:This file contains the routines for the     
 *             Generic Device Driver Module of the CFA.    
 *             These routines are called at various places     
 *             in the CFA modules for interfacing with the    
 *             device drivers in the system. These routines   
 *             have to be modified when moving onto different  
 *             drivers. 
 *               Drivers currently supported are                  
 *                  SOCK_PACKET for Ethernet                      
 *                  WANIC HDLC driver                             
 *                  SANGOMA wanpipe driver                        
 *                  ETINC driver                                  
 *                  ATMVC's support                               
 *                              
 *******************************************************************/
#ifndef _GDDVTSS_H
#define _GDDVTSS_H

#endif /*_GDDVTSS_H*/
