/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddvitesse.h,v 1.1 2014/06/12 14:03:40 siva Exp $
 *
 * Description:This file contains the routines for the     
 *             Generic Device Driver Module of the CFA.    
 *             These routines are called at various places     
 *             in the CFA modules for interfacing with the    
 *             device drivers in the system. These routines   
 *             have to be modified when moving onto different  
 *             drivers. 
 *               Drivers currently supported are                  
 *                  SOCK_PACKET for Ethernet                      
 *                  WANIC HDLC driver                             
 *                  SANGOMA wanpipe driver                        
 *                  ETINC driver                                  
 *                  ATMVC's support                               
 *                              
 *******************************************************************/
#ifndef _GDDVITESSE_H
#define _GDDVITESSE_H

#define ENABLE_DMA_FRAME_IO
#define NPI_ENCAP_LEN 16 // DA = BC, SA, ETYPE = 88:80, 00:05 = 16 bytes

extern BOOL1        CFA_DEBUG_ENABLE;

#define MAX_PAYLOAD (2*1024)  /* maximum payload size*/
#define ETH_VLAN  1             /* Default VLAN */


#ifdef __BYTE_ORDER
#define IS_BIG_ENDIAN (__BYTE_ORDER == __BIG_ENDIAN)
#else
# error Unable to determine byteorder!
#endif

#if (__BYTE_ORDER == __BIG_ENDIAN)
#define PCIE_HOST_CVT(x) __builtin_bswap32((x))  /* PCIe is LE - we're BE, so swap */
#define CPU_HTONL(x)     (x)                     /* We're network order already */
#else
#define PCIE_HOST_CVT(x) (x)                     /* We're LE already */
#define CPU_HTONL(x)     __builtin_bswap32(x)    /* LE to network order */
#endif

#if (__BYTE_ORDER == __BIG_ENDIAN)
#define PCIE_HOST_CVT(x) __builtin_bswap32((x))  /* PCIe is LE - we're BE, so swap */
#else
#define PCIE_HOST_CVT(x) (x)                     /* We're LE already */
#endif




int filter_fd;
/* Trace levels of NPAPI layer */
vtss_trace_level_t vtss_npapi_trace_level[VTSS_TRACE_GROUP_COUNT];

#if defined(VTSS_USERMODE)

#define MMAP_SIZE   0x02000000
#define CHIPID_OFF (0x01070000 >> 2)
#define ENDIAN_OFF (0x01000000 >> 2)

#define HWSWAP_BE 0x81818181       /* Big-endian */
#define HWSWAP_LE 0x00000000       /* Little-endian */

tOsixSemId          gVtssTxSemId;
tOsixSemId          gVtssApiSemId;
tOsixSemId          gVtssNpapiSemId;


#endif  /* VTSS_USERMODE */

#endif /*_GDDVITESSE_H*/
