/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfaextn.h,v 1.65 2017/10/10 11:57:20 siva Exp $
 *
 * Description: This file contains the externs.
 *
 *******************************************************************/
#ifndef _CFAEXTN_H
#define _CFAEXTN_H


extern tOsixTaskId      gCfaTaskId;
extern UINT1            gu1CfaInitialised;
extern UINT4            gu4IsIvrEnabled;
extern UINT1            gu1IpForwardingEnable; /* a flag which indicates if 
                                                * the IP Forwarding has been 
                                                * enabled in the system or not.
                                                */
extern UINT2        gCfaDefautVlanId;
extern UINT4               gu4CfaTraceLevel; 
extern UINT4               gu4CfaDebugLevel; 
extern UINT4        gu4SecModeForBridgedTraffic; 
extern UINT4       gu4SecIvrIfIndex; 
extern tIfGlobalStruct  gIfGlobal;             /* Globals of Interface table*/

extern tIfInfoStruct      *gapIfTable[SYS_DEF_MAX_INTERFACES];
extern UINT4 gu4StackingModel;

/* A flag which indicates if the default router interface has been configured 
 * successfully with IP.
 */

extern tOsixTaskId      CfaTaskId;
extern tOsixQId         gCfaPktMuxQId;
extern tOsixQId         gCfaPktQId;
extern tOsixQId         gCfaTxPktQId;
extern tOsixQId         gCfaPktPlQId;
extern tOsixQId         gCfaExtTrgrQId;
extern tOsixQId         gCfaPktParamsQId;

extern tOsixSemId       gCfaTnlSemId;


extern tMemPoolId        GddEntryMemPoolId;
extern tMemPoolId        WanEntryMemPoolId;
extern UINT4             u4CidrSubnetMask[];
extern UINT1             gCfaDefaultTxACCM[];
extern UINT1             gCfaDefaultRxACCM[];


extern INT4      gi4MibResStatus;
extern UINT4      gu4CfaSysLogId;

extern tCfaNodeStatus    gCfaNodeStatus;

extern UINT1   gu1CfaIpInitComplete;
extern tCfaDefaultPortParams   gCfaDefaultPortInfo[SYS_DEF_MAX_PHYSICAL_INTERFACES];

extern UINT1 gu1CfaDataPktQueueStatus;
extern UINT1 gu1CfaPortListPktQueueStatus;
extern UINT1 gu1CfaExtTriggerQueueStatus;

extern tCfaTnlGblInfo gCfaTnlGblInfo;
extern tFsModSizingInfo gFsCfaSizingInfo;

extern tMemPoolId gCfaMbsmSlotMsgPoolId;
extern tMemPoolId gCfaMbsmProtoMsgPoolId;
extern tMemPoolId gCfaPktPoolId;
extern tMemPoolId gCfaDriverMtuPoolId;
extern tMemPoolId gCfaStackPoolId;
extern tMemPoolId gCfaILanPortPoolId;
extern tMemPoolId gCfaRcvAddrPoolId;
extern tMemPoolId gCfaIntfStatusPoolId;
extern tMemPoolId gCfaPortArrayPoolId;
extern tMacAddr   gu1CfaUnNumMCastMac;
extern tHwPortInfo gLocalUnitHwPortInfo;

extern UINT4 gu4IfOOBNode0SecondaryIpAddress;
extern UINT4 gu4IfOOBNode0SecondaryIpMask;
extern UINT4 gu4IfOOBNode1SecondaryIpAddress;
extern UINT4 gu4IfOOBNode1SecondaryIpMask;
extern UINT4 gu4DualOob;

/* Application CallBack Structure */
extern unCfaUtilCallBackEntry CFA_UTIL_CALL_BACK [CFA_CUST_MAX_CALL_BACK_EVENTS];
extern INT4
Ip4TnlIfUpdateNotifyFromCfa PROTO ((UINT4 u4IfIndex, tTnlIfInfo * pCfaTnlIfInfo));

extern VOID  D6ClCliShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex);
extern VOID D6SrCliShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex);
extern INT4 D6SrCliSetClntUdpPorts (tCliHandle CliHandle, UINT4 u4Type, UINT4 u4Value);       
extern INT4 PnacShowRunningConfigInterfaceDetails PROTO ((tCliHandle CliHandle,                                                                 INT4 i4Index));               
extern INT4 RmonShowRunningConfigInterface (tCliHandle,UINT1,INT4);                    

extern VOID EcfmShowRunningConfigInterfaceDetails(tCliHandle,INT4, UINT1 *);
extern VOID PbbShowRunningConfigInterfaceDetails(tCliHandle CliHandle,INT4                                                              i4IfIndex);
extern VOID EoamCliShowRunningConfigIfInfo (tCliHandle, INT4, BOOL1); 
extern INT4 VlanGarpShowRunningConfigInterface (tCliHandle CliHandle, UINT4 u4CurrentIfIndex, UINT1 *u1HeadFlag);    
extern VOID MrpSrcShowRunningConfigIfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex);                                                                                                                                                                             
extern INT4 RipShowRunningConfigIfaceAndAggDetailsCxt (tCliHandle CliHandle, UINT4 u4CfaIndex); 
extern INT4 OspfShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex);                          

extern INT4 FsIpShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index);
extern VOID IpvxShowRunningConfigIfDetails (tCliHandle CliHandle, INT4 i4Index);

extern INT4 Ipv6ShowRunningConfigInterfaceDetails(tCliHandle CliHandle,INT4 i4IfIndex, UINT1 *pu1Flag);

extern INT4 Ospfv3ShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex);
 #ifdef RIP_WANTED
extern INT4 RipDeleteIfRecord ( UINT4 u4IfIndex, UINT4 u4IpAddr , UINT4 u4Flag);
 #endif
      
       
#ifdef VCM_WANTED
extern INT4 VcmShowInterfaceMapping (tCliHandle, UINT4);
extern INT4 VcmGetVcAlias PROTO ((UINT4 , UINT1 *));
#endif
#ifdef MPLS_WANTED
extern INT4 MplsVpwsShowRunningConfig (tCliHandle, INT4, UINT4, INT4, UINT4, UINT4,
                                       UINT1 *, UINT1 *);
 extern INT4 MplsPwXConnectShowRunningConfig (tCliHandle, INT4, UINT4,
                                             INT4, UINT1 *, UINT1 *);                                 
#endif


extern VOID LaShowRunningConfigPhysicalInterfaceDetails(tCliHandle,INT4,UINT4);
extern VOID LaShowRunningConfigPortChannelInterfaceDetails (tCliHandle, INT4);

extern INT4 DvmrpShowRunningConfigInterfaceDetails(tCliHandle CliHandle, INT4 i4Index);

extern VOID IsIsShowRunningConfigInterfaceInCxt PROTO((tCliHandle CliHandle, INT4 i4Index));

extern VOID D6RlCliShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex);

extern VOID FmCliShowRunningConfigIfInfo (tCliHandle, INT4, BOOL1);

extern VOID
IgmpHandleIncomingPkt (tIP_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len,
                       UINT4 u4Index,tIP_INTERFACE IfId,  UINT1 u1Flag);

extern INT4
VlanEvbShowRunningConfigInterface(tCliHandle CliHandle, INT4 i4IfIndex);

extern INT4
NatShowRunningConfigInterface (tCliHandle CliHandle, INT4 i4IfIndex);
extern INT4
RipShowRunningConfigInterfaceCxt (tCliHandle CliHandle, INT4);


#endif /* _CFAEXTN_H */
