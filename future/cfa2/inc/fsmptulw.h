/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmptulw.h,v 1.1 2009/10/23 10:21:35 prabuc Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMITunlIfTable. */
INT1
nmhValidateIndexInstanceFsMITunlIfTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMITunlIfTable  */

INT1
nmhGetFirstIndexFsMITunlIfTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMITunlIfTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMITunlIfHopLimit ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITunlIfSecurity ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITunlIfTOS ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITunlIfFlowLabel ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITunlIfMTU ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITunlIfDirFlag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITunlIfDirection ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITunlIfEncaplmt ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMITunlIfEncapOption ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITunlIfIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITunlIfAlias ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMITunlIfCksumFlag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITunlIfPmtuFlag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMITunlIfStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMITunlIfHopLimit ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMITunlIfTOS ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMITunlIfFlowLabel ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMITunlIfDirFlag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMITunlIfDirection ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMITunlIfEncaplmt ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsMITunlIfEncapOption ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMITunlIfAlias ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMITunlIfCksumFlag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMITunlIfPmtuFlag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMITunlIfStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMITunlIfHopLimit ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMITunlIfTOS ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMITunlIfFlowLabel ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMITunlIfDirFlag ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMITunlIfDirection ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMITunlIfEncaplmt ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsMITunlIfEncapOption ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMITunlIfAlias ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMITunlIfCksumFlag ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMITunlIfPmtuFlag ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMITunlIfStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMITunlIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
