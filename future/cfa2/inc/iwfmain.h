/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: iwfmain.h,v 1.39 2016/10/03 10:34:39 siva Exp $
 *
 * Description:This file contains the definitions of the    
 *             global structures of the Inter-working    
 *             Functionality module of the CFA. It contains 
 *             Macros for accessing the IWF data strucuture
 *             for the Enet interfaces.             
 *
 *******************************************************************/
#ifndef _IWFMAIN_H
#define _IWFMAIN_H

#define   CFA_ENET_MIN_FRAME_SIZE            60 
#define   CFA_ENET_MIN_V2_DATA_SIZE          46
#define   CFA_ENET_MIN_SNAP_DATA_SIZE        38
#define   CFA_ENET_MIN_LLC_DATA_SIZE         43 

#define   CFA_BMAC_TYPE                    0x01
#define   CFA_BRIDGE_HEADER_SIZE              2
#define   CFA_BFLAGS                       0x00
#define   CFA_NLPID_PPP                    0xCF

/*****************************************************************************/

/* complete LLC-SNAP Header Format - for AAL5 and others */
typedef struct {
    UINT1  u1DstLSap;
    UINT1  u1SrcLSap;
    UINT1  u1Control;
    UINT1  u1Oui1;
    UINT1  u1Oui2;
    UINT1  u1Oui3;
    UINT2  u2ProtocolType;
} tLlcSnapHeader;

/*  
 * Bridged Ethernet/802.3 Header :
 * 
 * LLC header + SNAP header + PAD
 *   3 Bytes  +   3 Bytes   + 2 Bytes = 10 Bytes
 *   
 * +-----------------------------------------+
 * |      LLC   0xAA-AA-03                   |
 * +-----------------------------------------+
 * |      OUI   0x00-80-c2                   |
 * +-----------------------------------------+
 * |      PID   0x00-07 (w/o preserved FCS)  |
 * +-----------------------------------------+
 * |      PAD   0x00-00                      |
 * +-----------------------------------------+
 */
typedef struct {
    UINT1  u1DstLSap;
    UINT1  u1SrcLSap;
    UINT1  u1Control;
    UINT1  u1Oui1;
    UINT1  u1Oui2;
    UINT1  u1Oui3;
    UINT2  u2ProtocolType;
    UINT1  u1Pad1;  
    UINT1  u1Pad2;
    UINT2  u2Dummy;
} tLlcBpduSnapHeader;


/* MACROS and definitions for IP/ARP/RARP over Enet **************************/

#define   CFA_ENET_IWF_V2_IPHDR(u4IfIndex) \
          (CFA_ENET_IWF(u4IfIndex)->EnetV2HeaderIp)
    
#define   CFA_ENET_IWF_V2_ARPHDR(u4IfIndex) \
          (CFA_ENET_IWF(u4IfIndex)->EnetV2HeaderArp)
    
#define   CFA_ENET_IWF_V2_RARPHDR(u4IfIndex) \
          (CFA_ENET_IWF(u4IfIndex)->EnetV2HeaderRarp)

#define   CFA_ENET_IWF_SNAP_IPHDR(u4IfIndex) \
          (CFA_ENET_IWF(u4IfIndex)->EnetSnapHeaderIp)
    
#define   CFA_ENET_IWF_SNAP_ARPHDR(u4IfIndex) \
          (CFA_ENET_IWF(u4IfIndex)->EnetSnapHeaderArp)
    
#define   CFA_ENET_IWF_SNAP_RARPHDR(u4IfIndex) \
          (CFA_ENET_IWF(u4IfIndex)->EnetSnapHeaderRarp)
    
#define   CFA_ENET_SNAP_ETYPE(pHdr)  \
          (((tEnetSnapHeader *)pHdr)->u2ProtocolType)
          
#ifdef IP6_WANTED
#define CFA_ENET_IWF_V2_IP6HDR(u4IfIndex) \
                              (CFA_ENET_IWF(u4IfIndex)->EnetV2HeaderIp6)
#endif /* IP6_WANTED */
    
#define   CFA_ENET_DSTADDR(pHdr)      (((tEnetV2Header *)pHdr)->au1DstAddr)  
#define   CFA_ENET_SRCADDR(pHdr)      (((tEnetV2Header *)pHdr)->au1SrcAddr)  
#define   CFA_ENET_V2_ETYPE(pHdr)     (((tEnetV2Header *)pHdr)->u2LenOrType) 
#define   CFA_ENET_SNAP_DSAP(pHdr)    (((tEnetSnapHeader *)pHdr)->u1DstLSap) 
#define   CFA_ENET_SNAP_SSAP(pHdr)    (((tEnetSnapHeader *)pHdr)->u1SrcLSap) 
#define   CFA_ENET_SNAP_CNTRL(pHdr)   (((tEnetSnapHeader *)pHdr)->u1Control) 
#define   CFA_ENET_SNAP_OUI1(pHdr)    (((tEnetSnapHeader *)pHdr)->u1Oui1)    
#define   CFA_ENET_SNAP_OUI2(pHdr)    (((tEnetSnapHeader *)pHdr)->u1Oui2)    
#define   CFA_ENET_SNAP_OUI3(pHdr)    (((tEnetSnapHeader *)pHdr)->u1Oui3)    


#define   CFA_ENET_IWF_LLC_BPDUHDR(u4IfIndex) \
          (CFA_ENET_IWF(u4IfIndex)->EnetLlcHeaderBpdu)
#define   CFA_BRIDGE_FLAGS(pHdr)      (((tBridgeHeader *)pHdr)->u1Flags)    
#define   CFA_BRIDGE_MAC_TYPE(pHdr)   (((tBridgeHeader *)pHdr)->u1MacType)  


#define   CFA_ENET_LLC_DSAP(pHdr)     (((tEnetLlcHeader *)pHdr)->u1DstLSap) 
#define   CFA_ENET_LLC_SSAP(pHdr)     (((tEnetLlcHeader *)pHdr)->u1SrcLSap) 
#define   CFA_ENET_LLC_CNTRL(pHdr)    (((tEnetLlcHeader *)pHdr)->u1Control) 

#define   CFA_ENET_IWF_LLC_OSIHDR(u4IfIndex) \
          (CFA_ENET_IWF(u4IfIndex)->EnetLlcHeaderOsi)

#define   CFA_LLC_OSI_DSAP(pHdr)    (((tLlcOsiHeader *)pHdr)->u1DstLSap)      
#define   CFA_LLC_OSI_SSAP(pHdr)    (((tLlcOsiHeader *)pHdr)->u1SrcLSap)      
#define   CFA_LLC_OSI_CNTRL(pHdr)   (((tLlcOsiHeader *)pHdr)->u1Control)      
#define   CFA_ENET_OSI_SRCADDR(pHdr) (((tEnetLlcHeader *)pHdr)->au1SrcAddr)  

/* MACROS for accessing LLC-SNAP HEADER */
#define   CFA_LLC_SNAP_DSAP(pHdr)    (((tLlcSnapHeader *)pHdr)->u1DstLSap)      
#define   CFA_LLC_SNAP_SSAP(pHdr)    (((tLlcSnapHeader *)pHdr)->u1SrcLSap)      
#define   CFA_LLC_SNAP_CNTRL(pHdr)   (((tLlcSnapHeader *)pHdr)->u1Control)      
#define   CFA_LLC_SNAP_OUI1(pHdr)    (((tLlcSnapHeader *)pHdr)->u1Oui1)         
#define   CFA_LLC_SNAP_OUI2(pHdr)    (((tLlcSnapHeader *)pHdr)->u1Oui2)         
#define   CFA_LLC_SNAP_OUI3(pHdr)    (((tLlcSnapHeader *)pHdr)->u1Oui3)         
#define   CFA_LLC_SNAP_PTYPE(pHdr)   (((tLlcSnapHeader *)pHdr)->u2ProtocolType) 

/* 
 * MACROS for accessing LLC-BPDU-SNAP HEADER */
#define   CFA_LLC_BPDU_SNAP_DSAP(pHdr) \
          (((tLlcBpduSnapHeader *)pHdr)->u1DstLSap)      

#define   CFA_LLC_BPDU_SNAP_SSAP(pHdr) \
          (((tLlcBpduSnapHeader *)pHdr)->u1SrcLSap)      

#define   CFA_LLC_BPDU_SNAP_CNTRL(pHdr) \
          (((tLlcBpduSnapHeader *)pHdr)->u1Control)      

#define   CFA_LLC_BPDU_SNAP_OUI1(pHdr)  (((tLlcBpduSnapHeader *)pHdr)->u1Oui1)         
#define   CFA_LLC_BPDU_SNAP_OUI2(pHdr)  (((tLlcBpduSnapHeader *)pHdr)->u1Oui2)         
#define   CFA_LLC_BPDU_SNAP_OUI3(pHdr)  (((tLlcBpduSnapHeader *)pHdr)->u1Oui3)         
#define   CFA_LLC_BPDU_SNAP_PAD1(pHdr)  (((tLlcBpduSnapHeader *)pHdr)->u1Pad1)

#define   CFA_LLC_BPDU_SNAP_PAD2(pHdr)  (((tLlcBpduSnapHeader *)pHdr)->u1Pad2)         
#define   CFA_LLC_BPDU_SNAP_PTYPE(pHdr) \
          (((tLlcBpduSnapHeader *)pHdr)->u2ProtocolType) 

#define   CFA_ENET_IWF(u2IfIndex) \
          ((tEnetIwfStruct *)CFA_IF_IWF(u2IfIndex))

#define CFA_CDB_SET_ENET_IWF_LOCAL_MAC(u4IfIndex,pu1HwAddr,u2Length) \
        { \
            tCfaIfInfo *pCfaCdbInterfaceInfo = NULL; \
            pCfaCdbInterfaceInfo = CFA_CDB_GET(u4IfIndex); \
            MEMCPY (pCfaCdbInterfaceInfo->au1MacAddr, pu1HwAddr, \
                    u2Length); \
        }


#define CFA_CDB_GET_ENET_IWF_LOCAL_MAC(u4IfIndex,pu1HwAddr) \
        { \
            tCfaIfInfo *pCfaCdbInterfaceInfo = NULL; \
            pCfaCdbInterfaceInfo = CFA_CDB_GET(u4IfIndex); \
            MEMCPY (pu1HwAddr, pCfaCdbInterfaceInfo->au1MacAddr, \
                    CFA_ENET_ADDR_LEN); \
        }

#define CFA_CDB_CHECK_IF_TYPE(u1IfType,u1DefRouterIdx) \
        { \
            if((u1IfType == CFA_ENET) || \
               (u1IfType == CFA_LAGG) || \
               (u1IfType == CFA_L3IPVLAN) || \
               (u1IfType == CFA_PIP) || \
               (u1IfType == CFA_LOOPBACK) || \
               (u1IfType == CFA_L2VLAN) || \
               (u1IfType == CFA_BRIDGED_INTERFACE) || \
               (u1IfType == CFA_PROP_VIRTUAL_INTERFACE) || \
               (u1IfType == CFA_PSEUDO_WIRE) || \
               (u1IfType == CFA_VXLAN_NVE) || \
               (u1IfType == CFA_CAPWAP_DOT11_BSS) || \
               (u1IfType == CFA_TAP) || \
               (u1IfType == CFA_L3SUB_INTF) || \
               (u1IfType == CFA_OTHER)) \
            { \
                u1DefRouterIdx=CFA_FALSE; \
            } \
            else \
            { \
                u1DefRouterIdx=CFA_TRUE; \
            } \
        }


#define   CFA_CDB_ENET_IWF_UNNUM_PEER_MAC(u4IfIndex) \
                 (CFA_CDB_IF_ENTRY(u4IfIndex)->au1PeerMacAddr) 

/* Sizing parameters */
enum
{
    CFA_PACKET_PORTLIST_QUEUE_MEMPOOL_SIZING_ID = 0,
    CFA_EXT_TRIGGER_QUEUE_MEMPOOL_SIZING_ID,
    CFA_COMMON_INFO_SIZING_ID,
    CFA_GAPIF_TABLE_SIZING_ID,
    CFA_IPIF_RECORD_MEMPOOL_SIZING_ID,
    CFA_IPADDR_INFO_MEMPOOL_SIZING_ID,
    CFA_IF_DEF_VAL_SIZING_ID,
    CFA_IFINFO_MEMPOOL_SIZING_ID,
    CFA_GDD_REG_MEMPOOL_SIZING_ID,
    CFA_TLV_INFO_MEMPOOL_SIZING_ID,
    CFA_TLV_INFO_BUDDY_POOL_SIZING_ID,
    CFA_REG_PARAMS_POOL_SIZING_ID,
    CFA_TUNL_INTF_MEMPOOL_SIZING_ID,
#if defined (L2RED_WANTED) && defined (NPAPI_WANTED)
    CFA_RED_BUF_ENTRIES_MEMPOOL_SIZING_ID,
#endif
  CFA_IVR_MAPPING_MEMPOOL_SIZING_ID,
#if defined MBSM_WANTED
    CFA_MBSM_SLOT_MSG_MEMPOOL_SIZING_ID,
#endif
    CFA_ENT_IWF_MEMPOOL_SIZING_ID,
    CFA_PKT_MEMPOOL_SIZING_ID,
    CFA_DRIVER_MTU_MEMPOOL_SIZING_ID,
    CFA_STACK_MEMPOOL_SIZING_ID,
#ifdef MI_WANTED
    CFA_ILAN_PORT_MEMPOOL_SIZING_ID,
#endif
    CFA_RCV_ADDR_MEMPOOL_SIZING_ID,
    CFA_INTF_STATUS_MEMPOOL_SIZING_ID,
    CFA_NO_SIZING_ID
};

#define  CFA_PPPOE_DISCOVERY_FRAME 0x8863
#define  CFA_PPPOE_SESSION_FRAME 0x8864
#ifdef HDLC_WANTED
#define  CFA_PPPOHDLC_FRAME      0xff03
#endif
#define  CFA_PPP_IP_DATAGRAM    0x0021

#define   IS_PPPOE_PKT(prot) \
          (((prot == CFA_PPPOE_SESSION_FRAME) || \
           (prot == CFA_PPPOE_DISCOVERY_FRAME)) ? 1:0)



#endif /* _IWFMAIN_H */
