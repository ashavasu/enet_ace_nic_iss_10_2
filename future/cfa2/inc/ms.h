/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: ms.h,v 1.8 2013/04/23 11:54:26 siva Exp $
 *
 *******************************************************************/


#define CONF_FILENAME   (gpConfFilename)
#define MS_PORT         8000
#define MS_MRU          2000
#define RTR_PORT_BASE   MS_PORT

#define MAX_LINKS 300
struct Link {
    int ss;                           /* source switch                       */
    int sp;                           /* source port                         */
    int st;                           /* status                              */
    int fl;                           /* used/ not-used                      */
};

#define IFNAME_LEN      8
#define MAX_EXT_LINKS   8

struct LinkExt {
        char ifname[IFNAME_LEN];
        int  st;
        int  ifindex;
        int  sock;                    /* socket Id for reading from interface */
};

#define NETWORK_NAMELEN (80)
#define MAX_NETWORKS    (32)

struct network
{
    char networkName[NETWORK_NAMELEN];
    int  noOfLinks;                    /* no of links present in Links       */
    int  sniff;                        /* sniff or do-not-sniff              */
    unsigned long sendToIpAddress;     /* ip address of node listening to the*
                                        * sniffed packets                    */
    int  sendToPort;                   /* UDP port of the node listening to  *
                                        * to the sniffed packets             */
    struct Link Links[MAX_LINKS];
    struct LinkExt LinkExt;
};

struct network Networks[MAX_NETWORKS];

/* switch to UDP/IP address mapping */
#define MAX_RTRS (20)
#define MAX_SWITCH_NUMBER (MAX_RTRS*sizeof(unsigned char)*8)
struct switch2Ip
{
    unsigned long ipAddress; /* UDP/IP address of switch */
    int s;                   /* switch number            */
};

/* max no of pkt sniffers */
#define MAX_PKT_SNIFFERS (15)

/* control message codes */
#define CTRL_MSG_LINK_STATUS    (1)
#define CTRL_MSG_SNIFF_PKTS     (2)
#define CTRL_MSG_GENERATE_PKTS  (3)
#define CTRL_MSG_READ_CONF      (4)
#define CTRL_MSG_MS_ADMIN       (5)
        

/* sniff or do-not-sniff */
#define PKT_DO_NOT_SNIFF (0)
#define PKT_SNIFF        (1)

/* cable status */
#define LINK_STATUS_DOWN (0)
#define LINK_STATUS_UP   (1)

struct sniffNetwork
{
    int sniff;                 /* start-sniff or stop-sniff          */
    char network[NETWORK_NAMELEN];/* network to sniff pkts on        */
};

struct pktGen
{
    int   sendToSwitch;        /* send packet to switch              */
    int   sendToPort;          /* send packet to port                */
    char  filename[160];       /* filename to read and send packet   */
};

struct readConf
{
    char confFilename[160];     /* configuration file name            */
};

/* control message for cable status */
struct linkStatus
{
    int s;                     /* switch number                      */
    int p;                     /* port number                        */
    int status;                /* CABLE_CONNECT / CABLE_DISCONNECT   */
};

/* control message for cable status */
#define MS_ADMIN_DOWN    (0)
#define MS_ADMIN_UP      (1)
struct msAdmin
{
    int status;                /* up/down */
};
#ifndef PACK_REQUIRED
#pragma pack(1)
#endif
struct ctrlMsg
{
    short s;
    short p;
    char  filler[2];
    char  au1pad[2];
    int   code;
    union
    {
        struct sniffNetwork sniffNetwork;
        struct pktGen       pktGen;
        struct linkStatus   linkStatus;
        struct readConf     readConf;
     struct msAdmin      msAdmin;
    } data;
};
#ifndef PACK_REQUIRED
#pragma pack()
#endif
extern int  MS_Route_External (char *ifname, char Buf[], int len);
