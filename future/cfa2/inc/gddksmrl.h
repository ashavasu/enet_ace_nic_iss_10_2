/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddksmrl.h,v 1.4 2015/02/24 11:52:38 siva Exp $
 *
 * Description:This file contains the prototypes for all
 *             the functions of the CFA system with respect to 
 *             kernel space specific to marvel
 *
 *******************************************************************/

#ifndef _GDDKSEC_MRVL_H
#define _GDDKSEC_MRVL_H

#define SEC_DEF_BOND_DEV_0 "eth0"
#define SEC_DEF_BOND_DEV_1 "eth0"

/* Target Specific Macros */
/* IF YOU CHANGE THESE PORTS MAKE SURE TO CHANGE CORRESPONDING NPAPI MACRO
 * PRESENT IN code/future/npapi/xcat/fsdxapi.h
 * These are CPSS port numbers for use in DSA tag not ISS port numbers.
 * */
#define SECURITY_CASCD_INBOUND_PORT      22
#define SECURITY_CASCD_OUTBOUND_PORT     22

/* Target Specific Macros */
/* IF YOU CHANGE THESE MACROS MAKE SURE TO CHANGE CORRESPONDING NPAPI MACRO
 * PRESENT IN future/npapi/xcat/npla.h and code/future/inc/xcat.h
 * These are used to compute CFA LAGG index from CPSS LAGG index derived from DSA tag */
#define SECURITY_LA_TRUNK_ID_START_INDEX   56
#define SECURITY_SYS_DEF_MAX_PHYSICAL_INTERFACES 28

/* DSA related Macros */
#define CFA_DSA_TAG_START                    12
#define CFA_DSA_TAG_LEN                      8
#define CFA_DSA_TAG_OFFSET                   4
#define CFA_DSA_TAG_PORT_OFFSET              13
#define CFA_DSA_TAG_VLAN_OFFSET              14
#define CFA_DSA_PORT_BITSHIFT                11
#define CFA_DSA_OUT_CPU_CODE                 0xe0
#define CFA_DSA_OUT_PORT                     0xb0
#define CFA_DSA_OUT_INFO_1                   0x10
#define CFA_DSA_OUT_INFO_2                   0x00
#define CFA_DSA_OUT_INFO_3                   0x01
#define CFA_DSA_OUT_INFO_4                   0x70
#define CFA_DSA_DEFAULT_VLAN_BYTE_1          0x00
#define CFA_DSA_DEFAULT_VLAN_BYTE_2          0x01
#define CFA_VLAN_PROTOCOL_ID_BYTE_1          0x81
#define CFA_VLAN_PROTOCOL_ID_BYTE_2          0x00


/* Converts CPSS port to DSA port
 * Bitshifting port number by 3 will give us 
 * the correct hex value for use in DSA tag.
 */
#define CONVERT_CPSS_PORT_TO_DSA(_port) \
         (_port)<<3
#endif


