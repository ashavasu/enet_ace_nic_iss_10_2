/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfaiph.h,v 1.22 2014/04/05 10:38:17 siva Exp $
 *
 * Description:This file contains the common definitions    
 *             for the CFA system.              
 *
 *******************************************************************/
#ifndef _CFAIPH_H
#define _CFAIPH_H

#define   DF_FLAG                0x4000
#define   CFA_IP_VALID_CHKSUM         0

#define  CFA_HEADER_LEN               1
/* Constants representing type of DOS attack detected */
#define  CFA_LAND_ATTACK              1
#define  CFA_SMURF_ATTACK             2 
#define  CFA_WINNUKE_ATTACK           3 
#define  CFA_UNKNOWN_IP               4 
#define  CFA_IRDP_ADVT                5 
#define  CFA_SNORK_ATTACK             6 
#define  CFA_ASCEND_ATTACK            7 
#define  CFA_UDP_SHTHDR               8 
#define  CFA_W2KDC_ATTACK             9
#define  CFA_TCP_SHTHDR               10
#define  CFA_TCP_XMAS_ATTACK          11
#define  CFA_TCP_NULL_ATTACK          12
#define  CFA_UDP_LOOPBACK_ATTACK      13


/* Constants used for validating IP address */
#define  CFA_BCAST_ADDR               255
#define  CFA_MCAST_ADDR               224
#define  CFA_ZERO_NETW_ADDR           0
#define  CFA_ZERO_ADDR                100
#define  CFA_LOOPBACK_ADDR            127
#define  CFA_UCAST_ADDR               1
#define  CFA_INVALID_ADDR             2
#define  CFA_CLASS_NETADDR            10  
#define  CFA_CLASS_BCASTADDR          200  
#define  CFA_CLASSE_ADDR              240

#define CFA_IPSEC    1
#define CFA_IKE      2

/*constants used for validating the alias name for loopback*/
#define CFA_MIN_LOOPBACK_VALUE 0
#define CFA_MAX_LOOPBACK_VALUE 100
#define CFA_LOOPBACK_ALIAS_NAME "loopback"
#define CFA_LOOPBACK_MAX_ALIAS_LEN    11
#define CFA_ZERO 0
#define CFA_ONE  1

/*Constants used for validateing the alias name for interface vlan*/
#define CFA_VLAN_ALIAS_NAME "vlan"
/*****************************************************************************/
/*       Structure to hold the Higher layer information                      */
/*****************************************************************************/

typedef struct {
    UINT1  u1Ack;
    UINT1  u1Rst;
    UINT1  u1Fin;
    UINT1  u1Syn;
    UINT2  u2SrcPort;
    UINT2  u2DestPort;
} tHLInfo;

/*****************************************************************************/
/*       Structure to hold the ICMP Type and Code Info                       */
/*****************************************************************************/

typedef struct {
    UINT1  u1Type;
    UINT1  u1Code;
    UINT1  au1Reserved[2];
} tIcmpInfo;

#endif /* _CFAIPH_H */

