/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains the definitions of the prototypes
 *              and defines used by the mid level wrapper routines of
 *              the MAU MIB.
 *
 *******************************************************************/

#ifndef _STDMAUWRAP_H 
#define _STDMAUWRAP_H 

VOID RegisterSTDMAU PROTO ((VOID));
INT4 RpMauGroupIndexGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 RpMauPortIndexGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 RpMauIndexGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 RpMauTypeGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 RpMauStatusTest PROTO (( UINT4 *, tSnmpIndex *, tRetVal *));
INT4 RpMauStatusSet PROTO ((tSnmpIndex *, tRetVal *));
INT4 RpMauStatusGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 RpMauMediaAvailableGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 RpMauMediaAvailableStateExitsGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 RpMauJabberStateGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 RpMauJabberingStateEntersGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 RpMauFalseCarriersGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 RpJackTypeGet PROTO ((tSnmpIndex *, tRetVal *));

INT4 RpMauTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 IfMauIfIndexGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauIndexGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauTypeGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauStatusTest PROTO (( UINT4 *, tSnmpIndex *, tRetVal *));
INT4 IfMauStatusSet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauStatusGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauMediaAvailableGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauMediaAvailableStateExitsGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauJabberStateGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauJabberingStateEntersGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauFalseCarriersGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauTypeListGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauDefaultTypeTest PROTO (( UINT4 *, tSnmpIndex *, tRetVal *));
INT4 IfMauDefaultTypeSet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauDefaultTypeGet PROTO ((tSnmpIndex *, tRetVal *));

INT4 IfMauTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 IfMauAutoNegSupportedGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauTypeListBitsGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfJackTypeGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegAdminStatusTest PROTO (( UINT4 *, tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegAdminStatusSet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegAdminStatusGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegRemoteSignalingGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegConfigGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegCapabilityGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegCapAdvertisedTest PROTO (( UINT4 *, tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegCapAdvertisedSet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegCapAdvertisedGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegCapReceivedGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegRestartTest PROTO (( UINT4 *, tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegRestartSet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegRestartGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegCapabilityBitsGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegCapAdvertisedBitsTest PROTO ((UINT4 *, tSnmpIndex *, 
                                               tRetVal *));
INT4 IfMauAutoNegCapAdvertisedBitsSet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegCapAdvertisedBitsGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegCapReceivedBitsGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegRemoteFaultAdvertisedTest PROTO ((UINT4 *, tSnmpIndex *, 
                                                   tRetVal *));
INT4 IfMauAutoNegRemoteFaultAdvertisedSet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegRemoteFaultAdvertisedGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 IfMauAutoNegRemoteFaultReceivedGet PROTO ((tSnmpIndex *, tRetVal *));

INT4 IfMauAutoNegTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);







INT4 BroadMauIfIndexGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 BroadMauIndexGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 BroadMauXmtRcvSplitTypeGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 BroadMauXmtCarrierFreqGet PROTO ((tSnmpIndex *, tRetVal *));
INT4 BroadMauTranslationFreqGet PROTO ((tSnmpIndex *, tRetVal *));

 /*  GetNext Function Prototypes */

INT4 GetNextIndexBroadMauBasicTable PROTO ((tSnmpIndex *, tSnmpIndex *));
INT4 GetNextIndexIfMauAutoNegTable PROTO ((tSnmpIndex *, tSnmpIndex *));
INT4 GetNextIndexIfJackTable PROTO ((tSnmpIndex *, tSnmpIndex *));
INT4 GetNextIndexIfMauTable PROTO ((tSnmpIndex *, tSnmpIndex *));
INT4 GetNextIndexRpJackTable PROTO ((tSnmpIndex *, tSnmpIndex *));
INT4 GetNextIndexRpMauTable PROTO ((tSnmpIndex *, tSnmpIndex *));
INT4 IfMauHCFalseCarriersGet(tSnmpIndex *, tRetVal *);
VOID UnRegisterSTDMAU(VOID);
#endif
