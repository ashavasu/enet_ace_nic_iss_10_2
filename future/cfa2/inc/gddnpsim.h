
/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddnpsim.h,v 1.8 2015/03/26 05:21:05 siva Exp $
 *
 * Description: This file contains the function implementations  of APIs to 
 *              invoke NPSIM.
 ****************************************************************************/

#define CFA_NPSIM_IPC_ETHERTYPE_OFFSET 12
#define CFA_NPSIM_IPC_ETHERTYPE_LEN 2
/* offset 14 */
#define CFA_NPSIM_IPC_MSGTYPE_OFFSET (CFA_NPSIM_IPC_ETHERTYPE_OFFSET+CFA_NPSIM_IPC_ETHERTYPE_LEN)
/* offset 18 */
#define CFA_NPSIM_IPC_MSGLENGTH_OFFSET (CFA_NPSIM_IPC_MSGTYPE_OFFSET + 4) 
/* Sequence number and return Value in the IPC message will be
 * used only for NPAPI IPC Message or for  other IPC messages
 * the seq number and ret value will be 0.
 */
/* offset 22 */
#define CFA_NPSIM_NPAPI_IPC_SEQID_OFFSET (CFA_NPSIM_IPC_MSGLENGTH_OFFSET + 4)
/* offset 26 */
#define CFA_NPSIM_NPAPI_IPC_RETVAL_OFFSET (CFA_NPSIM_NPAPI_IPC_SEQID_OFFSET + 4)
/* offset 30 */
#define CFA_NPSIM_IPC_BUF_OFFSET (CFA_NPSIM_NPAPI_IPC_RETVAL_OFFSET + 4)


#define CFA_NPSIM_IPC_MSG_ETHERTYPE  0x88b5
#define CFA_NPSIM_IPC_MSG_ETHERTYPE_LEN 2
#define CFA_NPSIM_HB_MSG_ETHERTYPE  HB_MSG_ETHERTYPE
#define CFA_NPSIM_HB_MSG_ETHERTYPE_LEN HB_MSG_ETHERTYPE_LEN
#define CFA_NPSIM_HB_MSG_ETHERTYPE_OFFSET HB_MSG_ETHERTYPE_OFFSET
#define CFA_NPSIM_HB_MSG_HEADER_SIZE HB_MSG_ETH_HEADER_SIZE

INT4 NpSimOpenMSSocket PROTO((UINT2 u2SwNo, UINT2 u2SlotNo, 
      UINT2 u2Port, UINT4 u4IpAddr));

INT4 NpSimCloseMSSocket (VOID);

INT4 NpSimOpenEnetSocket PROTO((char *Portname, UINT2 u2Port));

INT4 NpSimGoActive PROTO((UINT2 u2SwNo, UINT2 u2SlotNo));

INT4 NpCfaSendPktToHw PROTO((INT2 i2Port, UINT4 u4Size));

INT4 CfaNpSimRmCallBk PROTO((UINT1 u1Event, tCRU_BUF_CHAIN_HEADER * pBuf, 
        UINT2 u2PktLen));

INT4 NpSimOpenSecuritySocket PROTO ((VOID));

INT4 NpSimCloseSecuritySocket PROTO((VOID));

INT4
CfaNpSimProcessIPCMsgFromRemoteCPU PROTO ((UINT1 * pkt, UINT1 *pu1IsPktHandled));
INT4
CfaNpSimCustProcessIPCMsg PROTO((UINT1 * pu1Pkt, INT4 i4Offset));
VOID CfaNpSimPostPktToActive PROTO ((UINT1 * pu1Buf, UINT4 u4IfIndex, UINT4 u4PktLen));
UINT4 CfaNpSimGetIPCMsgType PROTO ((UINT1 *pu1Buf));
VOID CfaNpSimPostPktToActive PROTO ((UINT1 * pu1Buf, UINT4 u4IfIndex, UINT4 u4PktLen));
VOID CfaNpSimProcessAndPostPktToCfaQ PROTO ((UINT1 *pu1Buf, UINT4 u4IfIndex, UINT4 u4PktLen));
INT4 CfaNpSimPostPktToStandby PROTO((UINT1 * pu1Buf, UINT4 u4IfIndex, UINT4 u4PktLen));
VOID CfaNpSimProcessAndPostHBPktToRMQ PROTO((UINT1 *pu1Buf, UINT4 u4IfIndex, UINT4 u4PktLen));
INT4 CfaNpSimProcessIPCMsgInInit PROTO ((UINT1 *pu1Buf, tHwInfo * pHwInfo, INT4 i4Offset));
VOID CfaNpSimProcessAndPostStdbyToSyslog (UINT1 *,UINT4);
VOID CfaNpSimPostHBPktToICCHQ PROTO ((tCRU_BUF_CHAIN_HEADER *pu1Buf, UINT4 u4IfIndex, UINT4 u4PktLen));


