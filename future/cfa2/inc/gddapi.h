/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddapi.h,v 1.18 2015/10/15 10:58:40 siva Exp $
 *
 * Description:This file includes header files for GDD module compilation   
 *
 *******************************************************************/
#ifndef _GDDLINUX_H
#define _GDDLINUX_H

#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#ifndef OS_QNX
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#else
#include <sys/socket.h>
#endif
#include <fcntl.h>
#include <linux/if_tun.h>

typedef struct pollfd pollfdstruct; /* for ease of reference */


/* the global structure which is used for polling interfaces */
typedef struct {
   pollfdstruct    *aFdList;
   UINT4           *au4IfIndex;
   UINT2           u2LastIndex;
   UINT2           u2Reserved;
} tFdListStruct;

pollfdstruct aGddFdList [SYS_DEF_MAX_INTERFACES]; 
UINT4        au4GddIfIndexList [SYS_DEF_MAX_INTERFACES];

/* Macros for accessing the polling structure */
#define   CFA_GDD_FDLIST()                 (FdTable.aFdList)                
#define   CFA_GDD_IFINDEX_LIST()           (FdTable.au4IfIndex)             
#define   CFA_GDD_FDLIST_LAST_INDEX()      (FdTable.u2LastIndex)            
#define   CFA_GDD_FDLIST_IFINDEX(Index)    (FdTable.au4IfIndex[Index])      
#define   CFA_GDD_FDLIST_FDSTRUCT(Index)   (FdTable.aFdList[Index])         
#define   CFA_GDD_FDLIST_FD(Index)         (FdTable.aFdList[Index].fd)      
#define   CFA_GDD_FDLIST_EVT(Index)        (FdTable.aFdList[Index].events)  
#define   CFA_GDD_FDLIST_REVT(Index)       (FdTable.aFdList[Index].revents) 

#define CFA_GDD_FDLIST_INIT(u4IfIndex, Fd) \
      { \
      CFA_GDD_FDLIST_IFINDEX(CFA_GDD_FDLIST_LAST_INDEX()) = u4IfIndex; \
      CFA_GDD_FDLIST_FD(CFA_GDD_FDLIST_LAST_INDEX()) = Fd; \
      CFA_GDD_FDLIST_EVT(CFA_GDD_FDLIST_LAST_INDEX()) = (POLLIN | POLLPRI); \
      CFA_GDD_FDLIST_REVT(CFA_GDD_FDLIST_LAST_INDEX()) = 0; \
      ++CFA_GDD_FDLIST_LAST_INDEX(); \
      }

#define CFA_GDD_FDLIST_DEINIT(Index) \
      { \
      if (Index != (CFA_GDD_FDLIST_LAST_INDEX() - 1)) { \
         CFA_GDD_FDLIST_IFINDEX(Index) = \
               CFA_GDD_FDLIST_IFINDEX(CFA_GDD_FDLIST_LAST_INDEX() - 1); \
         CFA_GDD_FDLIST_FD(Index) = \
               CFA_GDD_FDLIST_FD(CFA_GDD_FDLIST_LAST_INDEX() - 1); \
         CFA_GDD_FDLIST_EVT(Index) = POLLIN | POLLPRI; \
         CFA_GDD_FDLIST_REVT(Index) = 0; \
      } \
      --CFA_GDD_FDLIST_LAST_INDEX(); \
      }

#define CFA_GDD_IS_DATA_AVAIL(Index) \
      (CFA_GDD_FDLIST_REVT(Index) & (POLLIN | POLLPRI))

/* gddlinux.c function prototypes*/
INT4 CfaGddLinuxEthSockOpen PROTO((UINT4 u4IfIndex));

#endif /* _GDDLINUX_H */ 
