/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfainc.h,v 1.96 2016/07/23 11:41:24 siva Exp $
 *
 * Description:This file includes other header files    
 *             for the CFA system.              
 *
 *******************************************************************/
#ifndef _CFAINC_H
#define _CFAINC_H

#include "lr.h"
#include "l2iwf.h"
#include "iss.h"
#include "utilipvx.h"
#include "fsbuddy.h"
#include <unistd.h>
/* SNMP LL specific files */
#include "snmccons.h"
#include "snmcdefn.h"
#include "fssnmp.h" 
#include "cfa.h"
#include "r6374.h"

#include "ifmiblow.h"
#include "fscfalw.h"
#include "ifmibwr.h"
#include "fscfawr.h"
#include "bridge.h"

#ifdef RM_WANTED
#include "rmgr.h"
#endif
#ifdef MBSM_WANTED
#include "cfambsm.h"
#include "mbsnp.h"
#endif

#ifdef SYNCE_WANTED
#include "synce.h"
#endif

#include "issu.h"
#include "ip.h"
#include "arp.h"

#include "cfalnkup.h"
#include "cfatnlmgr.h"

#include "lldp.h"
#include "eoam.h"
#include "pnac.h"
#include "fsvlan.h"
#include "mrp.h"
#include "hwaud.h"
#include "cfared.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif
#include "snmputil.h"
#include "rstp.h"
#include "la.h"
#include "ospf.h"
#include "tcp.h"
#include "radius.h"
#include "rsvp.h"
#include "pim.h"
#include "dvmrp.h"
#include "ftp.h"
#include "dhcp.h"
#include "bgp.h"
#include "fsike.h"
#include "sec.h"
#include "selutil.h"
#include "vcm.h"
#include "fssyslog.h"
#ifdef IP6_WANTED
#include "ipv6.h"
#endif

#ifdef NAT_WANTED
#include "nat.h"
#endif
#ifdef FIREWALL_WANTED
#include "firewall.h"
#endif
#ifdef MPLS_WANTED
#include "mpls.h"
#ifdef NPAPI_WANTED
#include "mplsnp.h"
#include "mplsnpwr.h"
#endif
#endif

#ifdef TLM_WANTED
#include "tlm.h"
#endif

#ifdef RMON_WANTED
#include "rmon.h"
#endif 

#ifdef RMON2_WANTED
#include "rmon2.h"
#endif

#ifdef VRRP_WANTED
#include "vrrp.h"
#endif

#ifdef ISIS_WANTED
#include "isis.h"
#endif

#include "cfacli.h"
#include "cfaufd.h"
#ifdef ISS_WANTED
#include "isslow.h"
#include "fsisswr.h"
#include "fsisselw.h"
#endif
#include "msr.h"

#include "fstunllw.h"
#include "fstunlwr.h"

#include "stdethlw.h"
#include "stdmaulw.h"
#include "stdethwr.h"
#include "stdmauwr.h"

#include "eoam.h"
#include "fsntp.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "cfanp.h"
#include "ipnp.h"
#include "rportnp.h"
#include "issnp.h"   
#include "maunp.h"
#include "ethernp.h"
#include "ip6np.h"
#include "cfanpwr.h"
#ifdef RMON_WANTED
#include "rmonnp.h"
#endif
#endif

#include "cfaipif.h"

#if ((defined (WLC_WANTED)) || (defined (WTP_WANTED)))
#include "capwap.h"
#include "radioif.h"
#include "wsswlan.h"
#include "wlchdlr.h"

#ifdef WTP_WANTED
#include "aphdlr.h"
#endif
#endif

 
/* CFA modules' proprietary header files */
#include "cfagen.h"
#include "cfaport.h"
#include "cfapktul.h"
#include "cfaiph.h"
#include "ifmmain.h"
#include "ifmmacs.h"
#include "gddmain.h"
#include "iwfmain.h"
#include "iwfppp.h"
#include "ppp.h"
#include "cfaifutl.h"
#include "cfaproto.h"
#include "cfaextn.h"
#include "cfacdb.h"
#include "cfapktreg.h"
#ifdef KERNEL_WANTED
#include "chrdev.h"
#endif
#ifdef L2RED_WANTED
#include "hwaudmap.h"
#endif /* L2RED_WANTED */
#include "cfaifmap.h"
#include "ipdb.h"

#include "mbsm.h"
#include "cfambsm.h"

#include "cfasz.h"
#include "vpn.h"
#include "secmod.h"
#include "ids.h"
#ifdef VXLAN_WANTED
#include "fsvxlan.h"
#endif
#ifdef ICCH_WANTED
#include "icch.h"
#endif
#ifdef HB_WANTED
#include "hb.h"
#endif
#ifdef VCPEMGR_WANTED
#include "fstap.h"
#endif
#endif /* _CFAINC_H */
