/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmptudb.h,v 1.1 2009/10/23 10:21:35 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMPTUDB_H
#define _FSMPTUDB_H

UINT1 FsMITunlIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fsmptu [] ={1,3,6,1,4,1,29601,2,39};
tSNMP_OID_TYPE fsmptuOID = {9, fsmptu};


UINT4 FsMITunlIfAddressType [ ] ={1,3,6,1,4,1,29601,2,39,1,1,1,1,1};
UINT4 FsMITunlIfLocalInetAddress [ ] ={1,3,6,1,4,1,29601,2,39,1,1,1,1,2};
UINT4 FsMITunlIfRemoteInetAddress [ ] ={1,3,6,1,4,1,29601,2,39,1,1,1,1,3};
UINT4 FsMITunlIfEncapsMethod [ ] ={1,3,6,1,4,1,29601,2,39,1,1,1,1,4};
UINT4 FsMITunlIfConfigID [ ] ={1,3,6,1,4,1,29601,2,39,1,1,1,1,5};
UINT4 FsMITunlIfHopLimit [ ] ={1,3,6,1,4,1,29601,2,39,1,1,1,1,6};
UINT4 FsMITunlIfSecurity [ ] ={1,3,6,1,4,1,29601,2,39,1,1,1,1,7};
UINT4 FsMITunlIfTOS [ ] ={1,3,6,1,4,1,29601,2,39,1,1,1,1,8};
UINT4 FsMITunlIfFlowLabel [ ] ={1,3,6,1,4,1,29601,2,39,1,1,1,1,9};
UINT4 FsMITunlIfMTU [ ] ={1,3,6,1,4,1,29601,2,39,1,1,1,1,10};
UINT4 FsMITunlIfDirFlag [ ] ={1,3,6,1,4,1,29601,2,39,1,1,1,1,11};
UINT4 FsMITunlIfDirection [ ] ={1,3,6,1,4,1,29601,2,39,1,1,1,1,12};
UINT4 FsMITunlIfEncaplmt [ ] ={1,3,6,1,4,1,29601,2,39,1,1,1,1,13};
UINT4 FsMITunlIfEncapOption [ ] ={1,3,6,1,4,1,29601,2,39,1,1,1,1,14};
UINT4 FsMITunlIfIndex [ ] ={1,3,6,1,4,1,29601,2,39,1,1,1,1,15};
UINT4 FsMITunlIfAlias [ ] ={1,3,6,1,4,1,29601,2,39,1,1,1,1,16};
UINT4 FsMITunlIfCksumFlag [ ] ={1,3,6,1,4,1,29601,2,39,1,1,1,1,17};
UINT4 FsMITunlIfPmtuFlag [ ] ={1,3,6,1,4,1,29601,2,39,1,1,1,1,18};
UINT4 FsMITunlIfStatus [ ] ={1,3,6,1,4,1,29601,2,39,1,1,1,1,19};


tMbDbEntry fsmptuMibEntry[]= {

{{14,FsMITunlIfAddressType}, GetNextIndexFsMITunlIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMITunlIfTableINDEX, 6, 0, 0, NULL},

{{14,FsMITunlIfLocalInetAddress}, GetNextIndexFsMITunlIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMITunlIfTableINDEX, 6, 0, 0, NULL},

{{14,FsMITunlIfRemoteInetAddress}, GetNextIndexFsMITunlIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMITunlIfTableINDEX, 6, 0, 0, NULL},

{{14,FsMITunlIfEncapsMethod}, GetNextIndexFsMITunlIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMITunlIfTableINDEX, 6, 0, 0, NULL},

{{14,FsMITunlIfConfigID}, GetNextIndexFsMITunlIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMITunlIfTableINDEX, 6, 0, 0, NULL},

{{14,FsMITunlIfHopLimit}, GetNextIndexFsMITunlIfTable, FsMITunlIfHopLimitGet, FsMITunlIfHopLimitSet, FsMITunlIfHopLimitTest, FsMITunlIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMITunlIfTableINDEX, 6, 0, 0, NULL},

{{14,FsMITunlIfSecurity}, GetNextIndexFsMITunlIfTable, FsMITunlIfSecurityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMITunlIfTableINDEX, 6, 0, 0, NULL},

{{14,FsMITunlIfTOS}, GetNextIndexFsMITunlIfTable, FsMITunlIfTOSGet, FsMITunlIfTOSSet, FsMITunlIfTOSTest, FsMITunlIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMITunlIfTableINDEX, 6, 0, 0, NULL},

{{14,FsMITunlIfFlowLabel}, GetNextIndexFsMITunlIfTable, FsMITunlIfFlowLabelGet, FsMITunlIfFlowLabelSet, FsMITunlIfFlowLabelTest, FsMITunlIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMITunlIfTableINDEX, 6, 0, 0, NULL},

{{14,FsMITunlIfMTU}, GetNextIndexFsMITunlIfTable, FsMITunlIfMTUGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMITunlIfTableINDEX, 6, 0, 0, NULL},

{{14,FsMITunlIfDirFlag}, GetNextIndexFsMITunlIfTable, FsMITunlIfDirFlagGet, FsMITunlIfDirFlagSet, FsMITunlIfDirFlagTest, FsMITunlIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMITunlIfTableINDEX, 6, 0, 0, "2"},

{{14,FsMITunlIfDirection}, GetNextIndexFsMITunlIfTable, FsMITunlIfDirectionGet, FsMITunlIfDirectionSet, FsMITunlIfDirectionTest, FsMITunlIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMITunlIfTableINDEX, 6, 0, 0, "2"},

{{14,FsMITunlIfEncaplmt}, GetNextIndexFsMITunlIfTable, FsMITunlIfEncaplmtGet, FsMITunlIfEncaplmtSet, FsMITunlIfEncaplmtTest, FsMITunlIfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMITunlIfTableINDEX, 6, 0, 0, "4"},

{{14,FsMITunlIfEncapOption}, GetNextIndexFsMITunlIfTable, FsMITunlIfEncapOptionGet, FsMITunlIfEncapOptionSet, FsMITunlIfEncapOptionTest, FsMITunlIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMITunlIfTableINDEX, 6, 0, 0, "2"},

{{14,FsMITunlIfIndex}, GetNextIndexFsMITunlIfTable, FsMITunlIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMITunlIfTableINDEX, 6, 0, 0, NULL},

{{14,FsMITunlIfAlias}, GetNextIndexFsMITunlIfTable, FsMITunlIfAliasGet, FsMITunlIfAliasSet, FsMITunlIfAliasTest, FsMITunlIfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMITunlIfTableINDEX, 6, 0, 0, NULL},

{{14,FsMITunlIfCksumFlag}, GetNextIndexFsMITunlIfTable, FsMITunlIfCksumFlagGet, FsMITunlIfCksumFlagSet, FsMITunlIfCksumFlagTest, FsMITunlIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMITunlIfTableINDEX, 6, 0, 0, "2"},

{{14,FsMITunlIfPmtuFlag}, GetNextIndexFsMITunlIfTable, FsMITunlIfPmtuFlagGet, FsMITunlIfPmtuFlagSet, FsMITunlIfPmtuFlagTest, FsMITunlIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMITunlIfTableINDEX, 6, 0, 0, "2"},

{{14,FsMITunlIfStatus}, GetNextIndexFsMITunlIfTable, FsMITunlIfStatusGet, FsMITunlIfStatusSet, FsMITunlIfStatusTest, FsMITunlIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMITunlIfTableINDEX, 6, 0, 1, NULL},
};
tMibData fsmptuEntry = { 19, fsmptuMibEntry };
#endif /* _FSMPTUDB_H */

