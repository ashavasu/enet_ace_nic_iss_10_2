/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfared.h,v 1.16 2015/11/27 12:35:52 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros related to redundancy.                   
 *
 *******************************************************************/

#ifndef _CFARED_H
#define _CFARED_H

/* Peer node Id. */
typedef VOID * tCfaRmPeerId;
extern UINT1 gu1CfaNoOfPeers;
extern BOOL1 bCfaBulkReqRecvd;
extern UINT4 gu4CfaBulkUptIfIndex;
extern UINT1 gu1CfaAuditFlag;
extern tOsixTaskId gCfaAuditTaskId;

/* To handle message/events given by redundancy manager. */
typedef struct CfaRmMsg
{
    tRmMsg        *pFrame;     /* Message given by RM module. */
    tCfaRmPeerId   PeerId;
    UINT2          u2Length;   /* Length of message given by RM module. */
    UINT1          u1Event;    /* Event given by RM module. */
    UINT1          u1Reserved;
}tCfaRmMsg;

typedef tNpSyncBufferEntry tCfaBufferEntry;

/* Macros to write in to RM buffer. */
#define CFA_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), (u4MesgType)); \
        *(pu4Offset) += 4;\
}while (0)

#define CFA_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), (u2MesgType)); \
        *(pu4Offset) += 2;\
}while (0)

#define CFA_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), (u1MesgType)); \
        *(pu4Offset) += 1;\
}while (0)

#define CFA_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
    RM_COPY_TO_OFFSET((pdest), (psrc), *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define CFA_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define CFA_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define CFA_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) = (UINT4)(*(pu4Offset) + 4);\
}while (0)

#define CFA_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
    RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)


#define CFA_BULK_MSG_PORT_COUNT_OFFSET    4 /* 1 + 2 + 1*/
#define CFA_PORT_COUNT_PER_BULK_MSG       48
#define CFA_RM_MIN_MSG_SIZE               6 /* 4 + 2 - Msg Type + Length */
#define CFA_RM_BULK_MSG_SIZE          3 /* 1 + 2 - Msg Type + Length */
#define CFA_RM_SYNC_MSG_SIZE          17 /* 1 + 2 + 2  + + 4+ 4 + 4
                                     Type + Length + Count + Sub Msg type+ IfIndex + Value*/
#define CFA_RM_GLB_MSG_SIZE           6 /* 1 + 2 + 1 + 2-
                                           Msg Type + Length + CFA-MsgType + Port Count */
#define CFA_RM_INTF_MSG_SIZE          31 /* 4 + 1+ 4+ 4+ 4 + 1 + 4 + 4 + 2 + 2 + 1
                                            IfIndex + OperStat
                                            + Speed + High Speed + Duplex Stat
                                            + Ethernet type + auto neg support
                                            + auto neg status + adv capability
                                            + mau type + ufd */
#define CFA_RM_BULK_RESP_MSG_SIZE     (CFA_RM_GLB_MSG_SIZE + (CFA_PORT_COUNT_PER_BULK_MSG * CFA_RM_INTF_MSG_SIZE))  

#define CFA_RM_GLB_CLOCK_MSG_SIZE      4 /* 1 + 2 + 1-
                                           Msg Type + Length + CFA-MsgType */
#define CFA_BULK_REQ_MSG             1
#define CFA_BULK_RESP_MSG            2
#define CFA_BULK_UPD_TAIL_MSG        3
#define CFA_SYNC_PORT_INFO_MSG       4
#define CFA_SYNC_PORT_OPER_STATUS    5
#define CFA_SYNC_PORT_SPEED          6
#define CFA_SYNC_PORT_HIGH_SPEED     7
#define CFA_SYNC_PORT_DUPLEX_STATUS  8
#define CFA_SYNC_PORT_ETHER_TYPE     9
#define CFA_SYNC_PORT_AUTO_NEG_SUPPORT 10
#define CFA_SYNC_PORT_AUTO_NEG_STATUS  11
#define CFA_SYNC_PORT_ADV_CAP          12
#define CFA_SYNC_PORT_OPER_MAU_TYPE    13
#define CFA_PORT_INFO_MSG            1
#define CFA_CLOCK_INFO_MSG           2
#define CFA_NUM_STANDBY_NODES() gu1CfaNoOfPeers
#define CFA_IS_BULK_REQ_RECVD() bCfaBulkReqRecvd


/* gu1CfaAuditFlag takes any of the three values.
 *
 * CFA_RED_AUDIT_STOP  -- Mean the audit event should not happen or already
 *                        done.
 * CFA_RED_AUDIT_CLEAN_AND_STOP  -- Means, audit event has to stopped.
 *                                  This will occur when GO_STANDBY event comes
 *                                  on auditing time
 * CFA_RED_AUDIT_START -- Means, audit event is started and its currently
 *                        going on.
 */
#define CFA_RED_AUDIT_STOP              0
#define CFA_RED_AUDIT_START             1
#define CFA_RED_AUDIT_CLEAN_AND_STOP    2


#define CFA_AUDIT_TASK_PRIORITY     240 /* should have lower priority */
#define CFA_AUDIT_TASK            ((UINT1*)"CFAU")

#define CFA_RED_AUDIT_START_EVENT    0x00000001
#define CFA_RED_AUDIT_TSK_DEL_EVENT  0x00000002

#define CFA_RED_AUDIT_FLAG() (gu1CfaAuditFlag)

#define CFA_NPSYNC_BLK()    (gCFANpSyncBlk)

#define CFA_AUDIT_TASK_ID() (gCfaAuditTaskId)

#define CFA_RED_BUF_MEMPOOL()    CfaRedBuffMemPoolId                


#define CFA_IS_STANDBY_UP() \
          ((gu1CfaNoOfPeers > 0) ? CFA_TRUE : CFA_FALSE)
INT4 CfaHandleRmEvents PROTO((tCfaRmMsg CfaRmMsg));
INT4 CfaSendBulkMessage PROTO((UINT1 u1MsgType));
INT4 CfaSendMsgToRm PROTO((tRmMsg * pMsg, UINT2 u2BufSize));
VOID CfaHandleMessageFromRm PROTO((tRmMsg * pMesg));
INT4 CfaHandleBulkReqEvent PROTO((VOID *pvPeerId));
VOID CfaTriggerHigherLayer PROTO((UINT1 u1TrigType));
VOID CfaProcessPortInformation PROTO((tRmMsg * pMesg, UINT4 u4Offset,
                                     UINT2 u2MsgCount));
INT4 CfaSyncPortInformation PROTO((UINT4 u4IfIndex, UINT4 u4MsgType, UINT4 u4Value));

VOID CfaProcessBulkResponse PROTO ((tRmMsg * pMesg, UINT4 u4Offset,
                                    UINT2 u2MsgCount));
VOID
CfaRedOperStatusUpdate PROTO ((UINT4 u4IfIndex, UINT4 u4Value));
    
VOID
CfaRedCreateAuditTask (VOID);

VOID
CfaRedAuditMain (INT1 *pi1Param);

INT4
CfaRedInitBufferPool (VOID);

INT4
CfaRedDeInitBufferPool (VOID);

VOID CfaRedHandleDynSyncAudit (VOID);
VOID CfaExecuteCmdAndCalculateChkSum  (VOID);
#ifdef NPAPI_WANTED
VOID
CfaRedStartAudit (VOID);

VOID
CfaRedDeleteAuditTask (VOID);


VOID
CfaRedAuditBufferEntry (tCfaBufferEntry     *pBuf);

VOID
CfaRedAuditCreateInternalPort (tCfaBufferEntry *pBuf);

VOID
CfaRedAuditDelInternalPort (tCfaBufferEntry *pBuf);

VOID
CfaRedAuditAddPortIlan (tCfaBufferEntry *pBuf);

VOID
CfaRedAuditDelPortIlan (tCfaBufferEntry *pBuf);

VOID
CfaRedAuditCreateIlan (tCfaBufferEntry *pBuf);

VOID
CfaRedAuditDeleteIlan (tCfaBufferEntry *pBuf);

VOID
CfaHwAuditCreateOrFlushBuffer (unNpSync  *pNpSync, UINT4 u4NpApiId,
                               UINT4 u4EventId);

VOID
CfaNpSyncProcessSyncMsg (tRmMsg * pMsg, UINT2 *u2OffSet);
#endif

#endif

