/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: cfamux.h,v 1.2 2010/08/25 11:04:00 prabuc Exp $
 *
 * Description: This file contains Structures and macro's required
 *              for registering or deregistering a module to/from the
 *              Protocol Registration Handler Module
 *
 *******************************************************************/
#ifndef _CFA_MUX_H_
#define _CFA_MUX_H_

#if defined (KERNEL_WANTED) && !defined (NP_KERNEL_WANTED)
/* Structure which holds the Registration information of the protocols */
typedef tProtocolInfo tCfaMuxRegTbl;

#define CFA_MUX_PROT_INFO(x)            (x)
#define CFA_MUX_PROT_FN_PTR(x)          (x)->ProtocolFnPtr
#define CFA_MUX_PROT_REG_TUPLE(x)       (x)->ProtRegTuple
#define CFA_MUX_REG_COMMAND(x)          (x)->i4Command
#define CFA_MUX_REG_STATUS(x)           (x)->i4Enabled
#define CFA_MUX_SEND_TO_FLAG(x)         (x)->u1SendToFlag
#define CFA_MUX_REG_COUNT(x)            (x)->u1RegCount
#define CFA_MUX_MODULE_ID(x)            (x)->i1ModuleId

#else

/* Structure of Protocol Registration Table Instances */
typedef struct _ProtRegTbl
{
   tRBNode       rbTupleNode;  /* RBTree Based on 3 tuple ProtRegTuple */
   tProtocolInfo ProtocolInfo; /* Tuples used to Register/De Register */
} tCfaMuxRegTbl;

#define CFA_MUX_PROT_INFO(x)            (&((x)->ProtocolInfo))
#define CFA_MUX_PROT_FN_PTR(x)          (x)->ProtocolInfo.ProtocolFnPtr
#define CFA_MUX_PROT_REG_TUPLE(x)       (x)->ProtocolInfo.ProtRegTuple
#define CFA_MUX_REG_COMMAND(x)          (x)->ProtocolInfo.i4Command
#define CFA_MUX_REG_STATUS(x)           (x)->ProtocolInfo.i4Enabled
#define CFA_MUX_SEND_TO_FLAG(x)         (x)->ProtocolInfo.u1SendToFlag
#define CFA_MUX_REG_COUNT(x)            (x)->ProtocolInfo.u1RegCount
#define CFA_MUX_MODULE_ID(x)            (x)->ProtocolInfo.i1ModuleId

#endif

/* Macro's for easier accessibility of elements in the Registration Table */

#define MUX_REG_MAC_INFO(x)          ((x)->MacInfo)
#define MUX_REG_MAC_ADDR(x)          ((x)->MacInfo.MacAddr)
#define MUX_REG_MAC_MASK(x)          ((x)->MacInfo.MacMask)
#define MUX_REG_ETH_PROTO(x)         ((x)->EthInfo.u2Protocol)
#define MUX_REG_ETH_MASK(x)          ((x)->EthInfo.u2Mask)
#define MUX_REG_IP_PROTO(x)          ((x)->IPInfo.u2Protocol)
#define MUX_REG_IP_MASK(x)           ((x)->IPInfo.u2Mask)
#define MUX_REG_STATUS(x)            ((x)->u1RegStatus)
#define MUX_REG_SENDTO_FLAG(x)       ((x)->u1SendToFlag)
#define MUX_REG_MODULE_ID(x)         ((x)->u4ModuleId)

#define MUX_REG_ETH_PROTO_CMP(x,y) \
           (MUX_REG_ETH_PROTO (x) == MUX_REG_ETH_PROTO (y))

#define MUX_REG_IP_PROTO_CMP(x,y) \
           (MUX_REG_IP_PROTO (x) == MUX_REG_IP_PROTO (y))

/* Macro to Scan the entries present in a RB Tree */
#define MUX_SCAN_TREE(Tree, pNode, pNext, Typecast) \
for ( (pNode = RBTreeGetFirst(Tree)); ( pNext=((pNode==NULL) ? NULL : \
    ((Typecast) RBTreeGetNext (Tree, pNode, NULL))) ), pNode != NULL; \
      pNode = pNext)

#ifdef _CFA_MUX_C_
tCfaMuxRegTbl         gaCfaMuxRegTbl[PACKET_HDLR_PROTO_MAX + 1];
tRBTree               gpIssRegnTree;
#endif

INT4 CfaMuxInitProtRegnTable PROTO ((VOID));
INT4 CfaMuxRegisterArpModule PROTO ((VOID));

#endif /* _CFA_MUX_H_ */

/* ENF OF FILE */
