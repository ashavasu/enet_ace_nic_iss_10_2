
#ifndef _IFMIBWRAP_H 
#define _IFMIBWRAP_H 
VOID RegisterIFMIB(VOID);
INT4 IfNumberGet (tSnmpIndex *, tRetVal *);
INT4 IfIndexGet (tSnmpIndex *, tRetVal *);
INT4 IfDescrGet (tSnmpIndex *, tRetVal *);
INT4 IfTypeGet (tSnmpIndex *, tRetVal *);
INT4 IfMtuGet (tSnmpIndex *, tRetVal *);
INT4 IfSpeedGet (tSnmpIndex *, tRetVal *);
INT4 IfPhysAddressGet (tSnmpIndex *, tRetVal *);
INT4 IfAdminStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IfAdminStatusSet (tSnmpIndex *, tRetVal *);
INT4 IfAdminStatusGet (tSnmpIndex *, tRetVal *);
INT4 IfOperStatusGet (tSnmpIndex *, tRetVal *);
INT4 IfLastChangeGet (tSnmpIndex *, tRetVal *);
INT4 IfInOctetsGet (tSnmpIndex *, tRetVal *);
INT4 IfInUcastPktsGet (tSnmpIndex *, tRetVal *);
INT4 IfInNUcastPktsGet (tSnmpIndex *, tRetVal *);
INT4 IfInDiscardsGet (tSnmpIndex *, tRetVal *);
INT4 IfInErrorsGet (tSnmpIndex *, tRetVal *);
INT4 IfInUnknownProtosGet (tSnmpIndex *, tRetVal *);
INT4 IfOutOctetsGet (tSnmpIndex *, tRetVal *);
INT4 IfOutUcastPktsGet (tSnmpIndex *, tRetVal *);
INT4 IfOutNUcastPktsGet (tSnmpIndex *, tRetVal *);
INT4 IfOutDiscardsGet (tSnmpIndex *, tRetVal *);
INT4 IfOutErrorsGet (tSnmpIndex *, tRetVal *);
INT4 IfOutQLenGet (tSnmpIndex *, tRetVal *);
INT4 IfSpecificGet (tSnmpIndex *, tRetVal *);
INT4 IfTableLastChangeGet (tSnmpIndex *, tRetVal *);
INT4 IfStackLastChangeGet (tSnmpIndex *, tRetVal *);
INT4 IfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 IfNameGet (tSnmpIndex *, tRetVal *);
INT4 IfInMulticastPktsGet (tSnmpIndex *, tRetVal *);
INT4 IfInBroadcastPktsGet (tSnmpIndex *, tRetVal *);
INT4 IfOutMulticastPktsGet (tSnmpIndex *, tRetVal *);
INT4 IfOutBroadcastPktsGet (tSnmpIndex *, tRetVal *);
INT4 IfHCInOctetsGet (tSnmpIndex *, tRetVal *);
INT4 IfHCInUcastPktsGet (tSnmpIndex *, tRetVal *);
INT4 IfHCInMulticastPktsGet (tSnmpIndex *, tRetVal *);
INT4 IfHCInBroadcastPktsGet (tSnmpIndex *, tRetVal *);
INT4 IfHCOutOctetsGet (tSnmpIndex *, tRetVal *);
INT4 IfHCOutUcastPktsGet (tSnmpIndex *, tRetVal *);
INT4 IfHCOutMulticastPktsGet (tSnmpIndex *, tRetVal *);
INT4 IfHCOutBroadcastPktsGet (tSnmpIndex *, tRetVal *);
INT4 IfLinkUpDownTrapEnableTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IfLinkUpDownTrapEnableSet (tSnmpIndex *, tRetVal *);
INT4 IfLinkUpDownTrapEnableGet (tSnmpIndex *, tRetVal *);
INT4 IfHighSpeedGet (tSnmpIndex *, tRetVal *);
INT4 IfPromiscuousModeTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IfPromiscuousModeSet (tSnmpIndex *, tRetVal *);
INT4 IfPromiscuousModeGet (tSnmpIndex *, tRetVal *);
INT4 IfConnectorPresentGet (tSnmpIndex *, tRetVal *);
INT4 IfAliasTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IfAliasSet (tSnmpIndex *, tRetVal *);
INT4 IfAliasGet (tSnmpIndex *, tRetVal *);
INT4 IfXTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 IfCounterDiscontinuityTimeGet (tSnmpIndex *, tRetVal *);
INT4 IfStackHigherLayerGet (tSnmpIndex *, tRetVal *);
INT4 IfStackLowerLayerGet (tSnmpIndex *, tRetVal *);
INT4 IfStackStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IfStackStatusSet (tSnmpIndex *, tRetVal *);
INT4 IfStackStatusGet (tSnmpIndex *, tRetVal *);
INT4 IfStackTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 IfRcvAddressAddressGet (tSnmpIndex *, tRetVal *);
INT4 IfRcvAddressStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IfRcvAddressStatusSet (tSnmpIndex *, tRetVal *);
INT4 IfRcvAddressStatusGet (tSnmpIndex *, tRetVal *);
INT4 IfRcvAddressTypeTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IfRcvAddressTypeSet (tSnmpIndex *, tRetVal *);
INT4 IfRcvAddressTypeGet (tSnmpIndex *, tRetVal *);

INT4 IfRcvAddressTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



 /*  GetNext Function Prototypes */

INT4 GetNextIndexIfRcvAddressTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexIfStackTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexIfXTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexIfTable( tSnmpIndex *, tSnmpIndex *);
#endif
