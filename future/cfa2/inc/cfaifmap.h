/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfaifmap.h,v 1.3 2011/02/15 13:12:04 siva Exp $
 *
 * Description: Header file of PVLAN L3 module              
 *
 *******************************************************************/

#ifndef _CFAPVLAN_H
#define _CFAPVLAN_H

#define CFA_IVR_MAP_TEST 0
#define CFA_IVR_MAP_SET  1


#define CFA_IVR_MAPPING_MEMPOOL()    gCfaIvrMapMemPoolId

/*
 * PVLAN IVR Mapping Table node strucutre
 */
typedef struct _tCfaVlanToIvrMapInfo
{
		tRBNodeEmbd		CfaNextVlanToIvrMapInfo;
		UINT4			u4ContextId;
		UINT4			u4IvrIfIndex;
		tVlanId			VlanId;
		UINT1			au1Reserved[2];
}tCfaVlanToIvrMapInfo;

INT4 CfaIvrMapInit (VOID);
INT4 CfaIvrMapDeInit (VOID);
INT4 CfaIvrMapInfoRBFree (tRBElem * pRBElem, UINT4 u4Arg);
INT4 CfaIvrMapInfoRBCmp (tRBElem *pNodeA, tRBElem *pNodeB);
INT4 CfaIvrMapGetInfo (UINT4 u4IfIndex, tVlanId VlanId, 
		tCfaVlanToIvrMapInfo *pPvlanInfo);
INT4
CfaIvrMapSetInfo (UINT4 u4IfIndex, tVlanId VlanId);

INT4
CfaIvrMapDeleteInfo (UINT4 u4IfMainIndex, UINT4 u4IfIvrMappingVlan);
INT4
CfaIvrMapIvrMapping (tCliHandle CliHandle, UINT4 u4IfIndex,
		    UINT1 *pu1VlanList, UINT1 u1Flag);
INT4
CfaIvrMapGetNextInfo (UINT4 u4IfIndex, tVlanId VlanId,
		    INT4 *pi4NextIfMainIndex, INT4 *pi4NextIfIvrMappingVlan);
UINT4
CfaIvrMapGetVlanIfIndex (UINT4 u4ContextId, tVlanId VlanId);
INT4
CfaIvrMapCliUpdate (tCliHandle CliHandle, UINT4 u4IfIndex,
		    UINT1 *pu1VlanList, UINT1 u1Flag);
INT4
CfaIvrMapCliAddRemove(UINT4 u4IfIndex, UINT1 *pu1VlanList, UINT1 u1Flag);
INT4
CfaIvrMapCheckVlanInList (tVlanId VlanId, UINT1 *pu1VlanList);
INT4
CfaIvrMapCliOverwrite (UINT4 u4IfIndex, UINT1 *pu1AddList, UINT1 *pu1DelList, UINT1 u1Flag);

INT4 CfaIvrMapDeleteMappedVlans (UINT4 u4IfIndex);

INT4 CfaIvrMapGetMappedVlans (UINT4 u4IfIndex, tVlanList MappedVlanList);

INT4
CfaIvrMapUpdtMappedVlans (UINT4 u4VrId, UINT4 u4IfIndex, tVlanId u2VlanId,
		    UINT1 u1Status);
#endif
