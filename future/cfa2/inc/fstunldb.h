/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstunldb.h,v 1.4 2008/08/20 13:40:38 premap-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSTUNLDB_H
#define _FSTUNLDB_H

UINT1 FsTunlIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fstunl [] ={1,3,6,1,4,1,2076,95};
tSNMP_OID_TYPE fstunlOID = {8, fstunl};


UINT4 FsTunlIfAddressType [ ] ={1,3,6,1,4,1,2076,95,1,1,1,1,1};
UINT4 FsTunlIfLocalInetAddress [ ] ={1,3,6,1,4,1,2076,95,1,1,1,1,2};
UINT4 FsTunlIfRemoteInetAddress [ ] ={1,3,6,1,4,1,2076,95,1,1,1,1,3};
UINT4 FsTunlIfEncapsMethod [ ] ={1,3,6,1,4,1,2076,95,1,1,1,1,4};
UINT4 FsTunlIfConfigID [ ] ={1,3,6,1,4,1,2076,95,1,1,1,1,5};
UINT4 FsTunlIfHopLimit [ ] ={1,3,6,1,4,1,2076,95,1,1,1,1,6};
UINT4 FsTunlIfSecurity [ ] ={1,3,6,1,4,1,2076,95,1,1,1,1,7};
UINT4 FsTunlIfTOS [ ] ={1,3,6,1,4,1,2076,95,1,1,1,1,8};
UINT4 FsTunlIfFlowLabel [ ] ={1,3,6,1,4,1,2076,95,1,1,1,1,9};
UINT4 FsTunlIfMTU [ ] ={1,3,6,1,4,1,2076,95,1,1,1,1,10};
UINT4 FsTunlIfDirFlag [ ] ={1,3,6,1,4,1,2076,95,1,1,1,1,11};
UINT4 FsTunlIfDirection [ ] ={1,3,6,1,4,1,2076,95,1,1,1,1,12};
UINT4 FsTunlIfEncaplmt [ ] ={1,3,6,1,4,1,2076,95,1,1,1,1,13};
UINT4 FsTunlIfEncapOption [ ] ={1,3,6,1,4,1,2076,95,1,1,1,1,14};
UINT4 FsTunlIfIndex [ ] ={1,3,6,1,4,1,2076,95,1,1,1,1,15};
UINT4 FsTunlIfAlias [ ] ={1,3,6,1,4,1,2076,95,1,1,1,1,16};
UINT4 FsTunlIfCksumFlag [ ] ={1,3,6,1,4,1,2076,95,1,1,1,1,17};
UINT4 FsTunlIfPmtuFlag [ ] ={1,3,6,1,4,1,2076,95,1,1,1,1,18};
UINT4 FsTunlIfStatus [ ] ={1,3,6,1,4,1,2076,95,1,1,1,1,19};


tMbDbEntry fstunlMibEntry[]= {

{{13,FsTunlIfAddressType}, GetNextIndexFsTunlIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsTunlIfTableINDEX, 5, 0, 0, NULL},

{{13,FsTunlIfLocalInetAddress}, GetNextIndexFsTunlIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsTunlIfTableINDEX, 5, 0, 0, NULL},

{{13,FsTunlIfRemoteInetAddress}, GetNextIndexFsTunlIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsTunlIfTableINDEX, 5, 0, 0, NULL},

{{13,FsTunlIfEncapsMethod}, GetNextIndexFsTunlIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsTunlIfTableINDEX, 5, 0, 0, NULL},

{{13,FsTunlIfConfigID}, GetNextIndexFsTunlIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsTunlIfTableINDEX, 5, 0, 0, NULL},

{{13,FsTunlIfHopLimit}, GetNextIndexFsTunlIfTable, FsTunlIfHopLimitGet, FsTunlIfHopLimitSet, FsTunlIfHopLimitTest, FsTunlIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTunlIfTableINDEX, 5, 0, 0, NULL},

{{13,FsTunlIfSecurity}, GetNextIndexFsTunlIfTable, FsTunlIfSecurityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsTunlIfTableINDEX, 5, 0, 0, NULL},

{{13,FsTunlIfTOS}, GetNextIndexFsTunlIfTable, FsTunlIfTOSGet, FsTunlIfTOSSet, FsTunlIfTOSTest, FsTunlIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTunlIfTableINDEX, 5, 0, 0, NULL},

{{13,FsTunlIfFlowLabel}, GetNextIndexFsTunlIfTable, FsTunlIfFlowLabelGet, FsTunlIfFlowLabelSet, FsTunlIfFlowLabelTest, FsTunlIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTunlIfTableINDEX, 5, 0, 0, NULL},

{{13,FsTunlIfMTU}, GetNextIndexFsTunlIfTable, FsTunlIfMTUGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsTunlIfTableINDEX, 5, 0, 0, NULL},

{{13,FsTunlIfDirFlag}, GetNextIndexFsTunlIfTable, FsTunlIfDirFlagGet, FsTunlIfDirFlagSet, FsTunlIfDirFlagTest, FsTunlIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTunlIfTableINDEX, 5, 0, 0, "2"},

{{13,FsTunlIfDirection}, GetNextIndexFsTunlIfTable, FsTunlIfDirectionGet, FsTunlIfDirectionSet, FsTunlIfDirectionTest, FsTunlIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTunlIfTableINDEX, 5, 0, 0, "2"},

{{13,FsTunlIfEncaplmt}, GetNextIndexFsTunlIfTable, FsTunlIfEncaplmtGet, FsTunlIfEncaplmtSet, FsTunlIfEncaplmtTest, FsTunlIfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsTunlIfTableINDEX, 5, 0, 0, "4"},

{{13,FsTunlIfEncapOption}, GetNextIndexFsTunlIfTable, FsTunlIfEncapOptionGet, FsTunlIfEncapOptionSet, FsTunlIfEncapOptionTest, FsTunlIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTunlIfTableINDEX, 5, 0, 0, "2"},

{{13,FsTunlIfIndex}, GetNextIndexFsTunlIfTable, FsTunlIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsTunlIfTableINDEX, 5, 0, 0, NULL},

{{13,FsTunlIfAlias}, GetNextIndexFsTunlIfTable, FsTunlIfAliasGet, FsTunlIfAliasSet, FsTunlIfAliasTest, FsTunlIfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsTunlIfTableINDEX, 5, 0, 0, NULL},

{{13,FsTunlIfCksumFlag}, GetNextIndexFsTunlIfTable, FsTunlIfCksumFlagGet, FsTunlIfCksumFlagSet, FsTunlIfCksumFlagTest, FsTunlIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTunlIfTableINDEX, 5, 0, 0, "2"},

{{13,FsTunlIfPmtuFlag}, GetNextIndexFsTunlIfTable, FsTunlIfPmtuFlagGet, FsTunlIfPmtuFlagSet, FsTunlIfPmtuFlagTest, FsTunlIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTunlIfTableINDEX, 5, 0, 0, "2"},

{{13,FsTunlIfStatus}, GetNextIndexFsTunlIfTable, FsTunlIfStatusGet, FsTunlIfStatusSet, FsTunlIfStatusTest, FsTunlIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTunlIfTableINDEX, 5, 0, 1, NULL},
};
tMibData fstunlEntry = { 19, fstunlMibEntry };
#endif /* _FSTUNLDB_H */

