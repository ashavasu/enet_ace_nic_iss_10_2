/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: cfaifutl.h,v 1.4 2012/02/22 12:38:54 siva Exp $
 *
 * Description: This file contains the definitions required for
 *              supporting the flexible interface index changes.
 *******************************************************************/

#ifndef _CFAIFUTL_H
#define _CFAIFUTL_H

#ifdef _CFAIFUTL_C_

/* Flexible IfIndex changes: ===
 * to maintain Ethernet type to IfIndex mapping.
 * This is needed since in MI solution, during
 * initialization itself the ethernet type for
 * physical interfaces is set. Once array is changed to
 * dynamic data structute since the node is not available,
 * (in MI the interfaces are not created by default) we
 * need to maintain a separate mapping to maintain
 * the current initialization behavior */

UINT1 gau1EthTypeIndexMap [ISS_MAX_PORTS];

#else

extern UINT1 gau1EthTypeIndexMap [ISS_MAX_PORTS];

#endif

/* Typedef definitions */

typedef tTMO_DLL tCfaIfTypeDb;

typedef tRBTree tCfaIfDb;

#ifdef CFA_IFDB_RBTREE_WANTED
typedef tRBElem tCfaIfDbElem;
#else
typedef tCfaIfInfo tCfaIfDbElem; 
#endif

typedef tRBCompareFn tCfaIfDbCmpFn;

typedef enum
{
    CFA_CDB_IFDB_REQ = 0,
    CFA_GAP_IFDB_REQ    
}tCfaIfDbReqType;

typedef enum
{
    CFA_CDB_IFTYPE_REQ = 0
}tCfaIfTypeDbReq;

typedef enum
{
    CFA_IFDB_ARRAY = 0,
    CFA_IFDB_RB
}tCfaIfDbType;

#define CFA_GAP_IF_INIT()  CfaIfUtlGapIfInit()

#define  CFA_GAP_IF_SET_IF_ENTRY(u4Index, pIfInfoStruct) \
             gapIfTable[u4Index - 1] = pIfInfoStruct 

/* Gets the required node from the GAPIFDB (RBTree)
 * Asserts if the node does not exist */

#define  CFA_GAP_IF_GET   CfaIfUtlGapIfGet

#define CFA_ALL_IFTYPE    0

#ifdef CFA_IFDB_RBTREE_WANTED
#define  CFA_IF_DB     CFA_IFDB_RB
#else
#define  CFA_IF_DB     CFA_IFDB_ARRAY
#endif

#define  CFA_CDB_IF_DB_SET_IFINDEX(u4Index) \
         if (CFA_IF_DB == CFA_IFDB_ARRAY) \
         { \
             gaCfaCommonIfInfo[u4Index - 1].u4CfaIfIndex = u4Index; \
         }

#define CFA_CDB_IF_DB_SET_INTF_ACTIVE(u4IfIndex, i4IfActive) \
         if (CFA_IF_DB == CFA_IFDB_ARRAY) \
         { \
             CFA_CDB_SET_INTF_ACTIVE(u4IfIndex, i4IfActive); \
         }

#define CFA_CDB_IF_DB_SET_INTF_VALID(u4IfIndex, i4IfValid) \
        if (CFA_IF_DB == CFA_IFDB_ARRAY) \
        { \
            CFA_CDB_SET_INTF_VALID(u4IfIndex, i4IfValid) \
        }

/* IfDB and IfTypeDB related defines */

#define  CFA_CALC_CDB_IF_TYPE_NODE_OFFSET \
                                FSAP_OFFSETOF(tCfaIfInfo, CfaIfTypeNode)

#define  CFA_CDB_IF_TYPE_DB     gaCfaCdbIfTypeDb

/* Gets the required node from the IFDB (RBTree)
 * Asserts if the node does not exist */ 

#define  CFA_CDB_GET     CfaIfUtlCdbGet


/* IFDB SCAN MACRO : With Lock and Without Lock 
 */

#ifdef CFA_IFDB_RBTREE_WANTED

#define CFA_CDB_SCAN(u4StartIfIndex, u4EndIfIndex, u1IfType) \
    for (((UNUSED_PARAM (u4EndIfIndex)), \
          u4StartIfIndex = CfaIfUtlIfDbGetIndex (u4StartIfIndex, u1IfType)); \
          u4StartIfIndex != 0; \
          u4StartIfIndex = CfaIfUtlIfDbGetNextIndex (u4StartIfIndex, u1IfType))

#define CFA_CDB_SCAN_WITH_LOCK(u4StartIfIndex, u4EndIfIndex, u1IfType) \
    for (((UNUSED_PARAM (u4EndIfIndex)), u4StartIfIndex = \
         CfaIfUtlIfDbGetIndexWithLock (u4StartIfIndex, u1IfType)); \
         u4StartIfIndex != 0; \
         u4StartIfIndex = \
                CfaIfUtlIfDbGetNextIndexWithLock (u4StartIfIndex, u1IfType))

#else

#define CFA_CDB_SCAN(u4StartIfIndex, u4EndIfIndex, u1IfType) \
    for (; u4StartIfIndex <= u4EndIfIndex; u4StartIfIndex++)

#define CFA_CDB_SCAN_WITH_LOCK(u4StartIfIndex, u4EndIfIndex, u1IfType) \
    for (; u4StartIfIndex <= u4EndIfIndex; u4StartIfIndex++)


#endif


/* GAPIFDB SCAN MACRO */

#ifdef CFA_IFDB_RBTREE_WANTED

/* To be used in case of RBTree */

#define CFA_GAP_IF_SCAN(u4StartIfIndex, u4EndIfIndex) \
        for (((UNUSED_PARAM (u4EndIfIndex)), \
                        u4StartIfIndex = CfaIfUtlGapIfDbGetIndex (u4StartIfIndex)); \
                       u4StartIfIndex != 0; \
                       u4StartIfIndex = CfaIfUtlGapIfDbGetNextIndex (u4StartIfIndex))

#else

/* To be used for array of pointes */

#define CFA_GAP_IF_SCAN(u4StartIfIndex, u4EndIfIndex) \
        for (; u4StartIfIndex <= u4EndIfIndex; u4StartIfIndex++)

#endif
#endif /* _CFAIFUTL_H */
