/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddksbcm.h,v 1.1 2014/10/27 10:58:32 siva Exp $
 *
 * Description:This file contains the prototypes for all
 *             the functions of the CFA system with respect to 
 *             kernel space specific to BCM
 *
 *******************************************************************/

#ifndef _GDDKSEC_BCM_H
#define _GDDKSEC_BCM_H
#define SEC_DEF_BOND_DEV_0 "bond0"
#define SEC_DEF_BOND_DEV_1 "bond1"
#endif


