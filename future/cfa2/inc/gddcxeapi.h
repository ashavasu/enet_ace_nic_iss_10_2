/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddcxeapi.h,v 1.5 2008/11/12 11:38:43 premap-iss Exp $
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/
#ifndef __GDDAPI_H__
#define __GDDAPI_H__

#include "iss.h"
#include "bridge.h"

#ifdef NPAPI_WANTED
#include "cfanp.h"
#endif

/*-------------------#define Constant:: Implementation Constants-------------*/
#define CFA_NP_MAC_ARRAY_TO_U4(arry, u4Msb, u2Lsb)\
         {  UINT4 u4Val = *((UINT4 *)(&arry[0])); \
             UINT2 u2Val = *((UINT2 *)(&arry[4])); \
	      u4Msb = OSIX_NTOHL(u4Val); \
	      u2Lsb  = OSIX_NTOHS(u2Val); \
          }

/*-----------------------MACRO::CFA-----------------------------------------*/
#define CFA_NP_MEMCPY     MEMCPY 
#define CFA_NP_MEMCMP     MEMCMP 
#define CFA_NP_MEMSET     MEMSET 

#define CFA_NP_MAX_RECV_BUF_SIZE    1520

#define  CFA_NP_IS_NULL_PORT_LIST(aPortList,u2ListLen,bRet) \
{\
	UINT2 u2LocIndx = 0;\
		bRet = CFA_NP_TRUE;\
	for (u2LocIndx = 0; u2LocIndx < (u2ListLen); u2LocIndx++) {\
		if ((aPortList)[u2LocIndx] != 0) {\
			(bRet) = CFA_NP_FALSE;\
			break;\
		}\
	}\
}

#define CFA_NP_MAC_ADDR_WORD_TO_STR(MacAddrWord,au1MacAddrStr)\
{\
  tMACADDRESS MacAddr;\
     MEMSET(&MacAddr, 0, sizeof(tMACADDRESS));\
     MacAddr.u4Dword = OSIX_HTONL ((MacAddrWord).u4Dword);\
     MacAddr.u2Word = (UINT2) (OSIX_HTONS ((MacAddrWord).u2Word));\
   MEMCPY (au1MacAddrStr, &((MacAddrWord).u4Dword), sizeof (UINT4));\
   MEMCPY (&((au1MacAddrStr)[4]), &((MacAddrWord).u2Word), sizeof (UINT2));\
}

#define CFA_NP_MAC_ADDR_STR_TO_WORD(au1MacAddrStr,MacAddrWord)\
{\
  tMACADDRESS MacAddr;\
     MEMSET(&MacAddr, 0, sizeof(tMACADDRESS));\
     MEMCPY (&(MacAddr.u4Dword), au1MacAddrStr, sizeof (UINT4));\
     MEMCPY (&(MacAddr.u2Word), &((au1MacAddrStr)[4]),sizeof (UINT2));\
     MEMSET(&(MacAddrWord), 0, sizeof(tMACADDRESS));\
     (MacAddrWord).u4Dword = OSIX_NTOHL (MacAddr.u4Dword);\
     (MacAddrWord).u2Word = (UINT2) (OSIX_NTOHS (MacAddr.u2Word));\
}


/*-----------------------MACRO:: RMON-----------------------------------------*/

PUBLIC VOID CfaNpFreeMem (UINT4 , UINT1 *);
PUBLIC UINT1 * CfaNpAllocMem (UINT4 , UINT4 );


/*-----------------------GLOBAL Data Declaration------------------------------*/
extern  tIssSysGroupInfo   gIssSysGroupInfo;

PUBLIC INT4
CfaGddEthSockOpen (UINT4 u4IfIndex);
PUBLIC INT4
CfaGddEthSockClose (UINT4 u4IfIndex);
PUBLIC INT4
CfaGddEthSockRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize);



#endif /* __GDDAPI_H__ */
