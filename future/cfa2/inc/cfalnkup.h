
/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: cfalnkup.h,v 1.1 2015/08/31 12:27:18 siva Exp $
 *
 * Description: This file contains Structures and macro's required
 *              for CFA LINKUP Delay Module
 *
 *******************************************************************/



extern tTimerListId        CfaLinkUpDelayTimerListId;
extern tCfaTimerDesc          gGlobalTimerDesc;

#define CFA_LINKUP_ZERO 0
INT4
CfaLinkUpDelayTmrInit (VOID);

INT4
CfaLinkUpDelayTmrDeInit (VOID);

VOID CfaTmrInitTmrDesc (VOID);
 
VOID CfaLinkUpDelayProcessTimerExpiryEvent (VOID);

VOID CfaLinkUpDelayTmrExpiry (VOID *pArg);


INT4 CfaLinkUpDelayTmrStart (UINT4 u4IfIndex);

INT4 CfaLinkUpDelayTmrProcessRemainingTime(UINT4 u4IfIndex,
                                      UINT4 u4SetValIfMainExtLinkUpDelayTimer);
INT4
CfaLinkUpDelayTmrStop(UINT4 u4IfIndex);

INT4
     CfaProcessPortOperStatus(UINT4 u4IfIndex, UINT1 u1AdminStatus,
                              UINT1 u1OperStatus, UINT1 u1IsFromMib,
                              UINT1 u1OldOperStatus);

VOID CfaCheckForLinkUpDelay (INT4 u4IfIndex,UINT1 u1OperStatus);

VOID CfaCleanUpLinkUpModuleStatus(INT4  i4LinkUpSystemStatus);

VOID CfaCleanUpLinkUpPortStatus(UINT4 i4IfMainIndex);

VOID CfaProcessLinkUpDelayStartUp(VOID);

INT4 CfaInterfaceLinkUpDelay(UINT4 u4IfIndex,UINT1 u1OperStatus,UINT1 u1OldOperStatus);


