/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: cfapktul.h,v 1.2 2013/09/26 12:16:23 siva Exp $
 *
 * Description:This file contains the definitions of the    
 *             global structures of the Packet Tx and 
 *             Packet Rx info for packet gen.
 *
 *******************************************************************/
#ifndef _CFAPKTUL_H
#define _CFAPKTUL_H

#define  CFA_MAX_PACKET_SIZE    3200
#define  MAX_CFA_PKTGEN         10

#define   CFA_PKT_GEN_TX_QID gCfaPktgenQId
#define   CFA_PKTGEN_QUEUE         "CFA6"
#define   CFA_PKT_GEN_TX_EVENT             0x00000800
#define   CFA_PKT_GEN_TMR_EVENT            0x00000400
#define   CFA_PKTGEN_Q_DEPTH          5
#define   CFA_PKTGEN_START   1
#define   CFA_PKTGEN_STOP    2
#define   CFA_MAX_PKT_GEN_ENTRIES 5
#define   CFA_MAX_PKT_GEN_PACKET_VAL 1600

typedef struct CfaPktRxInfo
{   
    UINT1  au1Val[1600];
    UINT1  au1Mask[1600];
    UINT1  au1RcvPorts[BRG_PORT_LIST_SIZE];
    UINT1  au1MatchPorts[BRG_PORT_LIST_SIZE];
    UINT4  u4Index;
    UINT4  u4Len;
    UINT4  u4Cnt;
    UINT4  u4LastPktRcvTime;
    UINT4  u4CreateTime;
    UINT4  u4RowStatus;
    UINT1  u1IsValueSet;
    UINT1  u1Pad[3];
}tCfaPktRxInfo;

typedef struct CfaPktTxInfo
{   
    tTmrBlk PktgenTmrBlk;
    UINT1  au1PktGenVal[1600];
    UINT1  au1Ports[BRG_PORT_LIST_SIZE];
    UINT4  u4Index;
    UINT4  u4Length;
    UINT4  u4PktCount;
    UINT4  u4Interval;
    UINT4  u4RowStatus;
    UINT1  u1IsPortSet;
    UINT1  u1Pad[3];
}tCfaPktTxInfo;

typedef struct PktgenQueInfo
{
    UINT4 u4Index;
    UINT4 u4Action;
}tPktgenQueInfo;

extern tCfaPktRxInfo *gpCfaPktRxInfoList;
extern tCfaPktTxInfo *gpCfaPktTxInfoList;
extern tOsixQId gCfaPktgenQId;

VOID CfaPktUtlAnalysePkt (tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4IfIndex, 
                     UINT4 u4PktSize);
VOID CfaPktGenTxPkt (UINT1 *pu1TransmitIndex);
VOID CfaPktGenTmrExpHdlr (VOID);
VOID CfaPktgenPacketSend(tCfaPktTxInfo *pCfaPktTxInfo);
INT4 CfaPktGenInit (VOID);
INT4 CfaPktGenShutdown (VOID);

#endif /* _CFAPKTUTL_H */

