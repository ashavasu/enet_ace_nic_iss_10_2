/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains the definitions of the prototypes
 *              and defines used by the low level routines of the 
 *              ethermib. 
 *
 *******************************************************************/
 
#ifndef _STDETHLW_H_
#define _STDETHLW_H_

/************* Prototypes of etherMIB low level routines ****************/

INT4 nmhValidateIndexInstanceDot3StatsTable PROTO ((INT4));
INT4 nmhGetDot3StatsAlignmentErrors PROTO ((INT4, UINT4 *));
INT4 nmhGetDot3StatsFCSErrors PROTO ((INT4, UINT4 *));
INT4 nmhGetDot3StatsSingleCollisionFrames PROTO ((INT4, UINT4 *));
INT4 nmhGetDot3StatsMultipleCollisionFrames PROTO ((INT4, UINT4 *));
INT4 nmhGetDot3StatsSQETestErrors PROTO ((INT4, UINT4 *));
INT4 nmhGetDot3StatsDeferredTransmissions PROTO ((INT4, UINT4 *));
INT4 nmhGetDot3StatsLateCollisions PROTO ((INT4, UINT4 *));
INT4 nmhGetDot3StatsExcessiveCollisions PROTO ((INT4, UINT4 *));
INT4 nmhGetDot3StatsInternalMacTransmitErrors PROTO ((INT4, UINT4 *));
INT4 nmhGetDot3StatsCarrierSenseErrors PROTO ((INT4, UINT4 *));
INT4 nmhGetDot3StatsFrameTooLongs PROTO ((INT4, UINT4 *));
INT4 nmhGetDot3StatsInternalMacReceiveErrors PROTO ((INT4, UINT4 *));
INT4 nmhGetDot3StatsEtherChipSet PROTO ((INT4, tSNMP_OID_TYPE *));
INT4 nmhGetDot3StatsSymbolErrors PROTO ((INT4, UINT4 *));
INT4 nmhGetDot3StatsDuplexStatus PROTO ((INT4, INT4 *));
INT1 nmhGetDot3StatsRateControlAbility PROTO ((INT4 ,INT4 *));
INT4 nmhGetDot3StatsRateControlStatus PROTO ((INT4 ,INT4 *));
INT4 nmhValidateIndexInstanceDot3CollTable PROTO ((INT4, INT4));
INT4 nmhGetDot3CollFrequencies PROTO ((INT4, INT4, UINT4 *));
INT4 nmhValidateIndexInstanceDot3ControlTable PROTO ((INT4));
INT4 nmhGetDot3ControlFunctionsSupported PROTO ((INT4, tSNMP_OCTET_STRING_TYPE*));
INT4 nmhGetDot3ControlInUnknownOpcodes PROTO ((INT4, UINT4 *));
INT4 nmhGetDot3HCControlInUnknownOpcodes PROTO ((INT4 ,tSNMP_COUNTER64_TYPE *));
INT4 nmhTestv2Dot3PauseAdminMode PROTO ((UINT4 *, INT4, INT4));
INT4 nmhValidateIndexInstanceDot3PauseTable PROTO ((INT4));
INT4 nmhGetDot3PauseAdminMode PROTO ((INT4, INT4 *));
INT4 nmhGetDot3PauseOperMode PROTO ((INT4, INT4 *));
INT4 nmhGetDot3InPauseFrames PROTO ((INT4, UINT4 *));
INT4 nmhGetDot3OutPauseFrames PROTO ((INT4, UINT4 *));
INT4 nmhGetDot3HCInPauseFrames PROTO ((INT4 ,tSNMP_COUNTER64_TYPE *));
INT4 nmhGetDot3HCOutPauseFrames PROTO ((INT4 ,tSNMP_COUNTER64_TYPE *));
INT4 nmhValidateIndexInstanceDot3HCStatsTable PROTO ((INT4 ));
INT4 nmhGetFirstIndexDot3HCStatsTable PROTO ((INT4 *));
INT4 nmhGetNextIndexDot3HCStatsTable PROTO((INT4 , INT4 *));
INT4 nmhGetDot3HCStatsAlignmentErrors PROTO ((INT4 ,tSNMP_COUNTER64_TYPE *));
INT4 nmhGetDot3HCStatsFCSErrors PROTO ((INT4 ,tSNMP_COUNTER64_TYPE *));
INT4 nmhGetDot3HCStatsInternalMacTransmitErrors PROTO ((INT4 ,tSNMP_COUNTER64_TYPE *));
INT4 nmhGetDot3HCStatsFrameTooLongs PROTO ((INT4 ,tSNMP_COUNTER64_TYPE *));
INT4 nmhGetDot3HCStatsInternalMacReceiveErrors PROTO ((INT4 ,tSNMP_COUNTER64_TYPE *));
INT4 nmhGetDot3HCStatsSymbolErrors PROTO ((INT4 ,tSNMP_COUNTER64_TYPE *));

/* nmh Get NEXT FUNCTIONS    */
INT4 nmhGetFirstIndexDot3PauseTable PROTO ((INT4 *));
INT4 nmhGetNextIndexDot3PauseTable PROTO ((INT4, INT4 *));
INT4 nmhGetFirstIndexDot3ControlTable PROTO ((INT4 *));
INT4 nmhGetNextIndexDot3ControlTable PROTO ((INT4, INT4 *));
INT4 nmhGetFirstIndexDot3CollTable PROTO ((INT4 *, INT4 *));
INT4 nmhGetNextIndexDot3CollTable PROTO ((INT4, INT4 *, INT4, INT4 *));
INT4 nmhGetFirstIndexDot3StatsTable PROTO ((INT4 *));
INT4 nmhGetNextIndexDot3StatsTable PROTO ((INT4, INT4 *));

INT4
nmhSetDot3PauseAdminMode ARG_LIST((INT4  ,INT4));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot3PauseTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

#define MIN_COLLISION_COUNT   1
#define MAX_COLLISION_COUNT   16

#define PAUSE_DISABLED        1
#define PAUSE_ENABLED_XMIT    2
#define PAUSE_ENABLED_RCV     3
#define PAUSE_ENABLED_BOTH    4
#define PAUSE_DISABLED_XMIT   5
#define PAUSE_DISABLED_RCV    6

#ifndef NPAPI_WANTED
#define ETH_PAUSE_DISABLED                  1
#define ETH_PAUSE_ENABLED_XMIT              2
#define ETH_PAUSE_ENABLED_RCV               3
#define ETH_PAUSE_ENABLED_XMIT_AND_RCV      4
#endif

#endif   /* _STDETHLW_H_ */
