#ifndef _FSMPTUWR_H
#define _FSMPTUWR_H
INT4 GetNextIndexFsMITunlIfTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSMPTU(VOID);

VOID UnRegisterFSMPTU(VOID);
INT4 FsMITunlIfHopLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfSecurityGet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfTOSGet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfFlowLabelGet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfMTUGet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfDirFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfDirectionGet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfEncaplmtGet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfEncapOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfAliasGet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfCksumFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfPmtuFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfHopLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfTOSSet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfFlowLabelSet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfDirFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfDirectionSet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfEncaplmtSet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfEncapOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfAliasSet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfCksumFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfPmtuFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfHopLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfTOSTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfFlowLabelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfDirFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfDirectionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfEncaplmtTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfEncapOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfAliasTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfCksumFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfPmtuFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMITunlIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSMPTUWR_H */
