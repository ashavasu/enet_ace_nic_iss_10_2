/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfaproto.h,v 1.191 2017/08/01 13:49:04 siva Exp $
 *
 * Description:This file contains the prototypes for all
 *             the functions of the CFA system.
 *
 *******************************************************************/
#ifndef _CFAPROTO_H
#define _CFAPROTO_H
/* GDD protos */
INT4 CfaGddTxPktOnVlanMemberPortsInCxt (UINT4 u4ContextId,UINT1 *pu1DataBuf, tVlanId VlanId, BOOL1 bBcast,UINT4 u4PktSize);
/* gddmain.c */
INT4 CfaGddInit PROTO((VOID));
VOID CfaGddShutdown PROTO((VOID));
INT4 CfaGddInitPort PROTO((UINT4 u4IfIndex));
INT4 CfaGddDeInitPort PROTO((UINT4 u4IfIndex));
INT4 CfaGddOpenPort PROTO((UINT4 u4IfIndex)); 
INT4 CfaGddClosePort PROTO((UINT4 u4IfIndex));
INT4 CfaGddProcessRecvEvent PROTO((VOID)); 
INT4 CfaGddRegisterPort PROTO((UINT4 u4IfIndex, UINT1 u1HigherIfType));
INT4 CfaGddDeRegisterPort PROTO((UINT4 u4IfIndex));
VOID Secv4ProcessTimeOut PROTO((VOID)); 
/* ported function prototypes */
INT4 CfaGddProcessRecvInterruptEvent PROTO((VOID));
INT4 CfaGddGetHwAddr PROTO((UINT4 u4IfIndex, UINT1 *au1HwAddr));
#if UNIQUE_VLAN_MAC_WANTED
INT4 CfaGddGetVlanHwAddr PROTO((UINT4 u4IfIndex, UINT1 *pau1HwAddr));
#endif
INT4 CfaGetPswHwAddr (UINT4 u4IfIndex, UINT1 *pau1HwAddr);
INT4 CfaCheckIsLocalMac PROTO ((tEnetV2Header * pHwAddr, UINT4 u4IfIndex,
                                    UINT2 u2VlanIfIndex));
INT4 CfaIwfCheckIsLocalEnetFrame PROTO ((UINT4 u4IfIndex, UINT4 u4VlanIfIndex,
                                         UINT1 *pau1HwAddr));
VOID CfaGddOsRemoveIfFromList PROTO((UINT4 u4IfIndex));
INT4 CfaGddOsProcessRecvEvent PROTO((VOID));
VOID CfaGddOsAddIfToList PROTO((UINT4 u4IfIndex));
INT4 CfaGddGetOsHwAddr PROTO((UINT4 u4IfIndex, UINT1 *au1HwAddr));
INT4 CfaGddSetGddType PROTO((UINT4 u4IfIndex));
INT4 CfaGddLinuxFixForWanPipe PROTO((UINT4 u4IfIndex));
UINT1 CfaIfIsBridgedInterface PROTO ((INT4 i4IfIndex));
INT4 CfaUtilSetDefaultBridgedIface PROTO ((UINT4 u4IfIndex,
                                           INT4 i4BridgedIfStat));
 
VOID CfaSnmpSendTrap PROTO ((UINT1 , INT1 *, UINT1 , VOID *));

VOID CfaIncMsrForTunlIfTable PROTO ((UINT4 u4CxtId, INT4 i4AddrType,
                      tSNMP_OCTET_STRING_TYPE * LocalAddresss,
                      tSNMP_OCTET_STRING_TYPE * RemoteAddress,
                      INT4 i4EncapMethod, INT4 i4ConfigId,
                      CHR1 cDatatype, VOID *pSetVal,
                      UINT4 *pu4ObjectId, UINT4 u4OIdLen, UINT1 IsRowStatus));

/* gddapi.c */
INT4 CfaGddEthOpen PROTO((UINT4 u4IfIndex));
INT4 CfaGddEthClose PROTO((UINT4 u4IfIndex));
INT4 CfaGddEthRead PROTO ((UINT1 *pu1DataBuf, UINT4 u4IfIndex,
                           UINT4 *pu4PktSize));
INT4 CfaGddEthWrite PROTO((UINT1 *pu1DataBuf, UINT4 u4IfIndex,
                               UINT4 u4PktSize));
INT4 CfaGddEthWriteWithPri PROTO ((UINT1 *pu1DataBuf, UINT4 u4IfIndex,
                                  UINT4 u4PktSize, UINT1 u1Priority));

INT4
CfaRmEnqChkSumMsgToRm PROTO ((UINT2, UINT2 ));

INT4
CfaGetShowCmdOutputAndCalcChkSum (UINT2 *);
INT4
CfaCliGetShowCmdOutputToFile (UINT1 *);

INT4
CfaCliCalcSwAudCheckSum (UINT1 *, UINT2 *);

INT1
CfaCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen);
/* WSS related function pointers */
#ifdef WLC_WANTED
INT4
CfaGddWssOpen PROTO ((UINT4 u4IfIndex));

INT4
CfaGddWssClose PROTO ((UINT4 u4IfIndex));

INT4
CfaGddWssWrite PROTO ((UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize));
INT4
CfaGddWssRead PROTO ((UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize));
#endif

UINT1 CfaGddGetLinkStatus PROTO((UINT4 u4IfIndex));
UINT1 CfaGddGetPhyAndLinkStatus (UINT2 u2IfIndex);
INT4 CfaGddProcessOobDataRcvdEvent PROTO ((VOID));
VOID CfaOobDataRcvd PROTO ((INT4 i4SockFd));

/* Utility Function Prototype */

/* cfacdb.c */
INT4 CfaCdbInit (VOID);
INT4 CfaCdbDeInit (VOID);
VOID CfaInitCdbInfo (UINT4 u4IfIndex);

INT4 CfaCdbRemoveFromIntfVlanList (UINT2 u2IfIvrVlanId);
INT4 CfaCdbAddToIntfVlanList (UINT2 u2IfIvrVlanId);

INT4 CfaSetIfValidStatus (UINT4 u4IfIndex, INT4 i4Status);
INT4 CfaValidateCfaIfIndex PROTO((UINT4 u4IfIndex));
INT4 CfaSetIfSpeed PROTO((UINT4 u4IfIndex, UINT4 u4Speed));
INT4 CfaSetIfHighSpeed PROTO((UINT4 u4IfIndex, UINT4 u4HighSpeed));
INT4 CfaGetIfHighSpeed PROTO((UINT4 u4IfIndex, UINT4 *pu4IfHighSpeed));

INT4 CfaSetIfOperStatus PROTO((UINT4 u4IfIndex, UINT1 u1OperStatus));
INT4 CfaGetIfSubType PROTO((UINT4 u4IfIndex, UINT1 *pu1IfSubType));
INT4 CfaSetIfSubType PROTO((UINT4 u4IfIndex, UINT1 u1IfSubType));
INT4 CfaSetEthernetType PROTO((UINT4 u4IfIndex, UINT1 u1EthernetType));
INT4 CfaSetIfHwAddr PROTO((UINT4 u4IfIndex, UINT1 *pu1HwAddr, UINT2 u2Count));
INT4 CfaGetIfName PROTO((UINT4 u4IfIndex, UINT1 *au1IfName));
INT4 CfaSetIfName PROTO((UINT4 u4IfIndex, UINT1 *au1IfName));
INT4 CfaSetIfMtu PROTO((UINT4 u4IfIndex, UINT4 u4IfMtu));
INT4 CfaSetIfBrgPortType PROTO ((UINT4 u4IfIndex, UINT1 u1BrgPortType));
INT4 CfaGetIfBrgPortType PROTO ((UINT4 u4IfIndex, UINT1 *pu1BrgPortType));
INT4 CfaGetIfIvrVlanId PROTO((UINT4 u4IfIndex, UINT2 *pu2IfIvrVlanId));
INT4 CfaSetIfIvrVlanId PROTO((UINT4 u4IfIndex, UINT2 u2IfIvrVlanId));
INT4 CfaSetIfLoopbackId PROTO((UINT4 u4IfIndex, UINT2 u2IfLoopbackId));
INT4 CfaMgmtVlanListValid PROTO((tMgmtVlanList MgmtVlanList));
VOID CfaSetDynamicIfName PROTO((UINT4 u4IfIndex , UINT1 *pu1Name, 
                                UINT1 u1count));
INT4 CfaGetIfDuplexStatus PROTO((UINT4 u4IfIndex, INT4 *i4Status));
INT4 CfaSetIfDuplexStatus PROTO ((UINT4 u4IfIndex, INT4 i4DuplexStatus));
INT4 CfaSetIfAutoNegStatus PROTO ((UINT4 u4IfIndex, INT4 i4Status));
INT4 CfaSetIfAutoNegSupport PROTO ((UINT4 u4IfIndex, INT4 i4Status));
INT4 CfaSetIfAdvtCapability PROTO ((UINT4 u4IfIndex, UINT2 u2AdvtCapability));
INT4 CfaSetIfOperMauType PROTO ((UINT4 u4IfIndex, UINT2 u2OperMauType));
INT4 CfaSetIfUnnumPeerMac PROTO ((UINT4 u4IfIndex, UINT1 *pu1MacAddr, UINT2 u2Length));
INT4 CfaSetIfUnnumAssocIPIf PROTO ((UINT4 u4IfIndex, UINT4 u4AssocIfId));
INT4 CfaSetIfIpPort PROTO ((UINT4 u4IfIndex, INT4 i4IpPort));
INT4 CfaSetIfActiveStatus (UINT4 u4IfIndex, INT4 i4Status);
INT4 CfaSetIfInternalStatus (UINT4 u4IfIndex, INT4 i4Status);
INT4 CfaSetCdbPortAdminStatus (UINT4 u4IfIndex, UINT1 u1AdminStatus);
INT4 CfaSetCdbRowStatus (UINT4 u4IfIndex, UINT1 u1RowStatus);
INT4 CfaGetIfRowStatus PROTO ((UINT4 u4IfIndex, UINT1 *pu1RowStatus));
INT4 CfaUpdtIvrInterface PROTO ((UINT4 u4IfIndex, UINT1 u1Status));
INT4 CfaCdbGetFirstIvrIfInfo PROTO ((UINT4 *pu4IfIndex, tCfaIfInfo *pIfInfo));

INT4 CfaCdbGetNextIvrIfInfo PROTO ((UINT4 u4IfIndex, 
                                    UINT4 *pu4NextIndex, tCfaIfInfo * pIfInfo));
INT4 CfaSetIfPortSecState PROTO ((UINT4 u4IfIndex, UINT1 u1PortSecState));

INT4 CfaCdbSetIfMauStatus PROTO ((UINT4 u4MauIfIndex, INT4 i4MauStatus));
INT4 CfaCdbSetIfMauDefaultType PROTO ((UINT4 u4MauIfIndex, UINT4 u4MauType));
INT4 CfaCdbSetIfMauAutoNegAdminStatus PROTO((UINT4 u4MauIfIndex, INT4 i4ANStatus));
INT4 CfaCdbSetIfMauAutoNegRestart PROTO ((UINT4 u4MauIfIndex, INT4 i4ANRestart));
INT4 CfaCdbSetIfMauAutoNegCapAdvtBits PROTO((UINT4 u4MauIfIndex, INT4 i4ANCapAdvtBits));
INT4 CfaCdbSetIfMauAutoNegRemFaultAdvt PROTO ((UINT4 u4MauIfIndex, INT4 i4ANRemFltAdvt));
tIfMauStruct * CfaCdbGetIfMauEntry PROTO ((UINT4 u4MauIfIndex, UINT4 u4IfMauIndex));
INT4 CfaUtilGetSubTypeFromNameAndType PROTO ((UINT1 *pu1Name, UINT4 u4Type,
                                              INT4 *pi4SubType));
INT4 CfaGetIfACPortIdentifier PROTO ((UINT4 u4IfIndex, UINT4 *pu4IfACPort));
INT4 CfaGetIfACVlan PROTO ((UINT4 u4IfIndex, UINT2 *pu2IfACVlan));
INT4 CfaSetIfACPortIdentifier PROTO ((UINT4 u4IfIndex, UINT4 u4IfACPort));
INT4 CfaSetIfACVlan PROTO ((UINT4 u4IfIndex, UINT2 u2IfACvlan));
INT4 CfaGetIfIpPortVlanId PROTO ((UINT4 u4IfIndex , INT4 *PortVlanId));
INT4
CfaGetIfLinkUpDelayStatus (UINT4 u4IfIndex, UINT2 *pu2LinkUpDelayStatus);
INT4
CfaSetIfLinkUpDelayStatus (UINT4 u4IfIndex, UINT4 u4LinkUpDelayStatus);
INT4
CfaGetIfLinkUpDelayTimer (UINT4 u4IfIndex, UINT2 *pu2LinkUpDelayTimer);
INT4
CfaSetIfLinkUpDelayTimer (UINT4 u4IfIndex, UINT4 u2LinkUpDelayTimer);
INT4
CfaGetIfLinkUpDelayGlobalStatus (UINT4 u4IfIndex, UINT2 *pu2LinkUpGlobalStatus);
INT4
CfaSetIfLinkUpDelayGlobalStatus (UINT4 u4IfIndex, UINT4 u4LinkUpGlobalStatus);
INT4
CfaGetIfLinkUpDelayRemainingTime (UINT4 u4IfIndex, UINT2 *pu2LinkUpDelayRemainingTime);


/* FM protos */ 
VOID CfaFmHandleResourceReqFail PROTO((UINT1 u1ResourceType));
 
/* Oob calls */
INT4 CfaGddOobOpen  PROTO ((UINT4 u4IfIndex));
INT4 CfaGddOobConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption);
INT4 CfaGddOobGetOsHwAddr PROTO ((UINT4 u4IfIndex, UINT1 *au1HwAddr));
INT4 CfaGddOobClose PROTO ((UINT4 u4IfIndex));
INT4 CfaGddOobRead  PROTO ((UINT1 *pu1DataBuf, UINT4 u4IfIndex,
                            UINT4 *pu4PktSize));
INT4 CfaGddOobWrite PROTO ((UINT1 *pu1DataBuf, UINT4 u4IfIndex,
                            UINT4 u4PktSize));
/* IWF protos */
/* iwfmain.c */
INT4 CfaIwfEnetInit PROTO((UINT4 u4IfIndex));
VOID CfaIwfEnetShutdown PROTO((VOID));
INT4 CfaIwfEnetProcessRxFrame PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                                     UINT4 u4IfIndex, UINT4 u4PktSize,
                                     UINT2 u2Protocol, UINT1 u1EncapType));
VOID CfaIwfEnetFormEnetMcastAddr PROTO((UINT4 u4IpAddr, UINT1 *au1HwAddr));
INT4 CfaIwfCheckLocalEnetFrame PROTO((UINT4 u4IfIndex, UINT1 *au1HwAddr));

INT4 CfaIwfPswInit PROTO((UINT4 u4IfIndex));
INT4 CfaIwfACInit PROTO((UINT4 u4IfIndex));
VOID CfaIwfPswShutdown PROTO((VOID));

tCRU_BUF_CHAIN_HEADER *
CfaCopyFrmPduForBackPlaneIntf PROTO ((tCRU_BUF_CHAIN_HEADER * pSrcCruBuf, 
          UINT4 u4OffSet, UINT4 u4OffSetSize));

tCRU_BUF_CHAIN_HEADER *
CfaCopyOverPduForBackPlaneIntf PROTO ((tCRU_BUF_CHAIN_HEADER * pSrcCruBuf,
           tCfaBackPlaneParams *pBackPlaneParams,
           UINT4 u4OffSet, UINT4 u4OffSetSize));


#ifdef BRIDGE_WANTED
INT4 CfaIwfEnetEncapBridgeFrame PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                                       UINT4 u4IfIndex, UINT4 *pu4PktSize));
INT4 CfaIwfEnetDeEncapBridgeFrame PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                                         UINT4 u4IfIndex, UINT2 *pu2Protocol)); 
#endif /* BRIDGE_WANTED */
#if defined(MPLS_WANTED) || defined(TLM_WANTED)
INT4 CfaIfmInitMplsIfEntry PROTO((UINT4 u4IfIndex, UINT1 *au1PortName));
INT4 CfaGetMplsIfFromMplsTnlIf PROTO ((UINT4 u4MplsTnlIfIndex, UINT4 *pu4MplsIfIndex));
INT4 CfaGetL3IfFromMplsIf PROTO ((UINT4 u4MplsIfIndex, UINT4 *pu4L3IfIndex));
#endif
#ifdef MPLS_WANTED
INT4 CfaIfmDeleteMplsInterface PROTO((UINT4 u4IfIndex, UINT1 u1DelIfEntry));
INT4 CfaCdbGetFirstMplsIfInfo PROTO ((UINT4 *pu4IfIndex, tCfaIfInfo * pIfInfo));
INT4 CfaCdbGetNextMplsIfInfo PROTO ((UINT4 u4IfIndex, UINT4 *pu4NextIndex,
                                     tCfaIfInfo * pIfInfo));
INT4 CfaGetMplsIfFromIfIndex PROTO ((UINT4 u4L3If, UINT4 *pu4MplsIf));
INT4 CfaGetMplsTnlIfFromMplsIfIndex PROTO ((UINT4 u4MplsL3If, UINT4 *pu4MplsInterIf));
INT4 CfaGetTeLinkIfFromMplsIf PROTO ((UINT4 u4MplsIfIndex, UINT4 *pu4TeLinkIf));
UINT1 CfaGetLLIfOperStatusFromMplsIf PROTO ((UINT4 u4MplsIf));
#ifdef NPAPI_WANTED
INT4
CfaIfmConfigL3MplsTunnelInterface PROTO((UINT4 u4IfIndex, INT4 ifStatus, BOOL1 bLockReq));
#endif
#ifdef LNXIP4_WANTED
INT4 CfaLinuxMplsSockOpenAndSend PROTO((tCRU_BUF_CHAIN_HEADER *pBuf, 
                                  UINT4  u4IfIndex, UINT4 u4PktSize));
#endif
#endif /* MPLS_WANTED */

VOID CfaUpdateACOperStatus PROTO ((UINT4 u4PhyIndex, UINT1 u1OperStatus));

#ifdef TLM_WANTED
INT4 CfaIfmInitTeLinkIfEntry PROTO ((UINT4 u4IfIndex, UINT1 *au1PortName));
INT4 CfaIfmDeleteTeLinkIf PROTO((UINT4 u4IfIndex, UINT1 u1DelIfEntry));
INT4 CfaGetMplsIfFromTeLinkIf PROTO ((UINT4 u4TeLinkIf, UINT4 *pu4MplsIf));
INT4 CfaGetTeLinkIfFromL3If PROTO ((UINT4 u4TeLinkIf, UINT1 u1Level, UINT4 *pu4MplsIf));
INT4 CfaGetL3IfFromTeLinkIf PROTO ((UINT4 u4TeLinkIf, UINT4 *pu4L3IfIndex));
UINT1 CfaGetL3IfOperStatusFromTeLinkIf PROTO ((UINT4 u4TeLinkIf));
#endif

/* IFM protos */
/* ifmmain.c */
INT4 CfaIfmInit PROTO((VOID));
VOID CfaIfmShutdown PROTO((VOID));
INT4 CfaIfmCreateAndInitIfEntry PROTO((UINT4 u4IfIndex, UINT1 u1IfType,
                                       UINT1 *au1PortName));
INT4 CfaIfmInitOtherIfEntry PROTO((UINT4 u4IfIndex, UINT1 *au1PortName));
INT4 CfaIfmInitEnetIfEntry PROTO((UINT4 u4IfIndex, UINT1 *au1PortName));
INT4 CfaIfmInitPortChannelIfEntry PROTO((UINT4 u4IfIndex, UINT1 *au1PortName));
INT4 CfaIfmInitInternalIfEntry PROTO((UINT4 u4IfIndex, UINT1 *au1PortName));
INT4 CfaIfmInitILanIfEntry PROTO((UINT4 u4IfIndex, UINT1 *au1PortName));
#ifdef HDLC_WANTED
INT4 CfaIfmInitHdlcIfEntry PROTO((UINT4 u4IfIndex, UINT1 *au1PortName));
#endif
INT1 CfaIfmInitDefaultVal PROTO ((VOID));
INT4 CfaIfmDeletePortChannelInterface(UINT4 u4IfIndex, UINT1 u1DelIfEntry, UINT1 u1IndicateHl);
INT4 CfaIfmDeleteInternalInterface(UINT4 u4IfIndex, UINT1 u1DelIfEntry, UINT1 u1IndicateHl);
INT4 CfaIfmDeleteILanInterface(UINT4 u4IfIndex, UINT1 u1DelIfEntry);
INT4 CfaIfmDeletePswInterface(UINT4 u4IfIndex, UINT1 u1DelIfEntry);
INT4 CfaIfmDeleteACInterface(UINT4 u4IfIndex, UINT1 u1DelIfEntry);

#ifdef VXLAN_WANTED
INT4 CfaIfmDeleteNveInterface(UINT4 u4IfIndex, UINT1 u1DelIfEntry);
#endif
#ifdef VCPEMGR_WANTED
INT4 CfaIfmDeleteTapInterface(UINT4 u4IfIndex, UINT1 u1DelIfEntry);
#endif
INT4 CfaIfmDeleteVlanInterface PROTO ((UINT4 u4IfIndex, UINT1 u1DelIfEntry));
INT4 CfaIfmDeleteL3SubInterface PROTO ((UINT4 u4IfIndex, UINT1 u1DelIfEntry));
INT4 CfaIfmDeleteLoopbackInterface PROTO ((UINT4 u4IfIndex, UINT1 u1DelIfEntry));
INT4 CfaIfmInitVlanIfEntry PROTO((UINT4 u4IfIndex, UINT1 *au1PortName));
INT4 CfaIfmInitL3SubIfEntry PROTO((UINT4 u4IfIndex, UINT1 *au1PortName));
INT4 CfaIfmCheckAndSetL3VlanIfWanType PROTO((UINT4 u4IfIndex,UINT1 u1Status));

INT4 CfaIfmInitLoopbackIfEntry PROTO ((UINT4 u4IfIndex, UINT1 *au1PortName));
VOID
CfaProcessPktFromArpForIvr PROTO((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                                  UINT1 *au1DestHwAddr, UINT4 u4PktSize,
                                  UINT2 u2Protocol, UINT1 u1EncapType));
UINT4
CfaGetVlanIfIndexAndUntagFrame PROTO ((UINT4 u4IfIndex,
                                       tCRU_BUF_CHAIN_HEADER * pBuf,
                                       tVlanIfaceVlanId * pVlanId));

UINT4
CfaGetVlanIfIndex PROTO ((UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER * pBuf,
                          tVlanIfaceVlanId * pVlanId));
#ifdef VRRP_WANTED
INT4 CfaDeleteVrrpInterface PROTO ((UINT4 u4IfIndex));
#endif

INT4 CfaIfmDeleteIfEntry PROTO((UINT4 u4IfIndex, UINT1 u1IfType));
INT4 CfaIfmConfigNetworkInterface PROTO((UINT1 u1OperCode, UINT4 u4IfIndex,
                                UINT4 u4Flag, tIpConfigInfo *pIpConfigInfo));
INT4 CfaIfmDeleteEnetInterface PROTO((UINT4 u4IfIndex, UINT1 u1DelIfEntry));

INT4 CfaIfmInitTunlIfEntry PROTO ((UINT4 u4IfIndex, tCfaIfCreateParams * pIfaceInfo));
INT4 CfaIfmDeInitTunlInterface PROTO ((UINT4 u4IfIndex, UINT1 u1DelIfEntry));
VOID CfaGetL3L4Info PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4SrcIp,
                            UINT1 *pu1L3Proto, UINT2 *pu2L4DstPort));

 
/* ifmutils.c */
INT4 CfaIfmAddStackEntry PROTO((UINT4 u4IfIndex, UINT1 u1Direction,
                                UINT4 u4StackIfIndex, UINT1 u1StackStatus));
VOID CfaIfmUpdateStackForAddition PROTO((UINT4 u4IfIndex,
                                         tStackInfoStruct *pStackLayer,
                                         tTMO_SLL *pIfStack,
                                         UINT1 u1Replace, UINT1 u1Direction));
VOID CfaIfmDeleteStackEntry PROTO((UINT4 u4IfIndex, UINT1 u1Direction,
                                   UINT4 u4StackIfIndex));
VOID CfaIfmUpdateStackForDeletion PROTO((tTMO_SLL *pIfStack,
                                         UINT1 u1Replace,
                                         UINT4 u4StackIfIndex));
INT4 CfaIfmAddRcvAddrEntry PROTO((UINT4 u4IfIndex, UINT1 *au1HwAddr,
                                  UINT1 u1AddrType));
VOID CfaIfmDeleteRcvAddrEntry PROTO((UINT4 u4IfIndex, UINT1 *au1HwAddr));
INT4 CfaIfmChangeInterfaceStatus PROTO((UINT4 u4IfIndex,
                                        UINT1 u1AdminStatus,
                                        UINT1 *pu1OperStatus,
                                        UINT1 u1IsFromMib));
INT4 CfaIfmNotifyInterfaceStatus PROTO((UINT4 u4IfIndex,
                                        UINT1 u1AdminStatus,
                                        UINT1 *pu1OperStatus,
     UINT1 u1IsFromMib, UINT1 u1IsRegToIp,
     tCfaIfInfo * pIfInfo)); 
VOID CfaIfmUpdateHighCounters PROTO((UINT4 u4IfIndex, UINT4 u4Octets,
                                     UINT1 u1Direction));
INT4 CfaIfmUpdateInterfaceStatus PROTO ((UINT4 u4IfIndex,
                     UINT2 u2BridgedIface));
PUBLIC VOID CfaSnmpifSendTrap PROTO ((UINT4 u4IfIndex,UINT1 u1OperStatus,
                   UINT1 u1AdminStatus));

VOID CfaGetIfDescr PROTO ((UINT4 u4IfIndex, UINT1 *pu1Descr));


INT4 CfaIfmAddDynamicStackEntry PROTO((UINT4 u4IfStackHigherLayer,
                                       UINT4 u4IfStackLowerLayer));
tSNMP_OID_TYPE     *CfaMakeObjIdFromDotNew (UINT1 *textStr);

/* cfaiph.c */
INT4 CfaExtractIpHdr PROTO((t_IP_HEADER *pIp, tCRU_BUF_CHAIN_HEADER *pBuf));
UINT2 CfaIpChksumCompute PROTO((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4Size,
                                UINT4 u4ReadOffset));
VOID CfaIpVerifyTtl PROTO((tCRU_BUF_CHAIN_HEADER *pBuf));
VOID CfaHandleIpPktFromIp PROTO((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4IfIndex));
#ifdef IP6_WANTED
VOID CfaHandlePktFromIpv6 PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                                  UINT1 *au1DestHwAddr, UINT4 u4PktSize,
                                  UINT2 u2Protocol, UINT1 u1EncapType));
#ifdef MPLS_IPV6_WANTED
INT4 CfaExtractIpv6Hdr PROTO((tIp6Hdr *pIp, tCRU_BUF_CHAIN_HEADER *pBuf));
#endif
#endif /* IP6_WANTED */

INT4 CfaProcessOutgoingIpPktForIwf PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,                                                           UINT4 u4IfIndex, UINT4 u4PktSize));
UINT4 CfaCheckAttacks PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, t_IP_HEADER *pIpHdr, 
                              UINT1 u1HdrLen));

/* cfaport.c */


INT4 CfaHandlePktFromL2 PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4IfIndex,
                                UINT4 u4PktSize, UINT2 u2Protocol, UINT1 *au1DestAddr, 
                                UINT1 u1EncapType));
 

INT4 CfaIwfPppHandleOutgoingPkt PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                        UINT4 u4IfIndex, UINT4 u4PktSize,
                                        UINT2 u2Protocol));
INT4
CfaVcmSispGetSispPortsOfPhysicalPort (UINT4 u4PhyIfIndex, UINT1 u1RetLocalPorts,
          VOID *paSispPorts, UINT4 *pu4PortCount);

VOID
CfaHandleBaseBridgeModeMsg PROTO ((tCfaBaseBridgeModeMsg CfaBaseBridgeModeMsg));

VOID
CfaHandleMclagIvrStatusChange PROTO ((UINT4 u4IfIndex, UINT1 u1IvrOperStatus));

INT4 CfaIwfPppHandleIncomingPkt      PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                             UINT4 u4IfIndex, UINT4 u4PktSize,
                                             UINT2 u2Protocol,
                                             UINT1 u1EncapsType));
INT4 CfaIwfPppHandleIncomingPktFromPpp PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                               UINT4 u4IfIndex,
                                               UINT4 u4PktSize,
                                               UINT2 u2Protocol));
INT4 CfaIwfPppHandleOutGoingPktFromPpp PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                               UINT4 u4IfIndex,
                                               UINT4 u4PktSize,
                                               UINT2 u2Protocol));
INT4 CfaIwfPppProcessTxAsyncPkt        PROTO ((tLlAsyncParams * pAsyncParams,
                                               tCRU_BUF_CHAIN_HEADER * pData,
                                               UINT4 *pu4Length,
                                               UINT1 *pNewData,
                                               UINT2 u2MaxLength));
INT4 CfaIwfPppProcessRxAsyncPkt        PROTO ((tLlAsyncParams * pAsyncParams,
                                               UINT1 *pData,
                                               UINT4 *pu4Length));
/* ifmppp.c */
#ifdef PPP_WANTED
INT4 CfaIfmInitPppIfEntry PROTO ((UINT4 u4IfIndex, UINT1 *au1PortName));
INT4 CfaIfmCreatePppInterface PROTO ((UINT4 u4IfIndex));
INT4 CfaIfmUpdatePppInterface PROTO ((UINT4 u4IfIndex,
                                      tIfLayerInfo *pLowInterface,
                                      tIfLayerInfo *pHighInterface,
                                      tPppIpInfo   *pIpInfo,
                                      UINT4        u4IdleTimer,
                                      UINT4        u4Mtu));
INT4 CfaIfmDeletePppInterface PROTO ((UINT4 u4IfIndex, UINT1 u1DelIfEntry));
INT4 CfaIfmPppCreatePppLinkForMpBundle PROTO ((UINT4 u4MpIfIndex,
                                               UINT4 u4PhysIfIndex));
INT4 CfaIwfPPPoEHandleOutGoingPkt      PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                                               UINT4 u4IfIndex,
                                               UINT4 u4PktSize,
                                               UINT2 u2Protocol,
                                               UINT1* au1DestHwAddr));
#endif /* PPP_WANTED */

INT4 CfaIfmInitPswIfEntry PROTO ((UINT4 u4IfIndex, UINT1 *au1PortName));
INT4 CfaIfmInitACIfEntry PROTO ((UINT4 u4IfIndex, UINT1 *au1PortName));
 
#ifdef VXLAN_WANTED
INT4 CfaIfmInitNveIfEntry PROTO ((UINT4 u4IfIndex, UINT1 *au1PortName));
#endif

#ifdef VCPEMGR_WANTED 
INT4 CfaIfmInitTapIfEntry PROTO ((UINT4 u4IfIndex, UINT1 *au1PortName));
#endif

/* cfamain.c */
INT4 CfaHandleShutdown PROTO ((VOID));
INT4 CfaHandleModuleStart PROTO ((VOID));
VOID CfaProcessIpInitComplete PROTO((VOID));
VOID CfaTaskInitFailureShutdown PROTO ((VOID));

UINT4 CfaGetIfFlowControl PROTO((UINT4 u4IfIndex));
INT4 CfaGetIfAutoNegSupport PROTO((UINT4 u4IfMauIfIndex, INT4 i4IfMauIndex, 
                                   INT4 *pi4RetSupport));
INT4 CfaGetIfOperMauType PROTO((UINT4 u4IfIndex, INT4 i4MauIndex, 
                                UINT4 *pu4RetOperMauType));
INT4 CfaGetIfAdvtCapability PROTO((UINT4 u4IfIndex, INT4 i4MauIndex, 
                                   UINT2 *pu2RetAdvtCapability));


INT4 CfaUpdateHwParams (UINT4 u4IfIndex);

INT4 CfaStartGddTimer PROTO ((VOID));

/* external protos */
INT4 IpHandlePktFromCfa PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                               UINT2 u2Port, UINT1 u1PktType));

#ifdef ISIS_WANTED
INT4 CfaIwfHandleOutgoingIsisPkt PROTO((tCRU_BUF_CHAIN_HEADER * pBuf,
                                     UINT4 u4IfIndex, UINT4 u4PktSize,
                                     UINT2 u2Protocol));
#endif
#ifdef RMON_WANTED
INT4 CfaIwfRmonUpdateTables PROTO ((UINT1 *pu1DataBuf, UINT4 u4IfIndex,
                             UINT4 u4PktSize));
#endif /* RMON_WANTED */
#if ((defined (RMON2_WANTED)) && (defined (NPAPI_WANTED)))
PUBLIC INT4 CfaIwfRmon2UpdatePktHdr PROTO((UINT1 *, tPktHeader *));
PUBLIC INT4 Rmon2MainUpdateTables PROTO((tPktHdrInfo *));
#endif
#ifdef RMON2_WANTED
PUBLIC VOID Rmon2CfaIfaceUpdateChgs PROTO((UINT4, UINT1));
#ifdef DSMON_WANTED
PUBLIC VOID DsmonCfaIfaceUpdateChgs PROTO((UINT4, UINT1));
#endif
#endif
VOID CfaIvrExtractVlanIdAndPrcsInfo(tCRU_BUF_CHAIN_HEADER * pBuf,
      INT4 (*FuntionPtr)(tCRU_BUF_CHAIN_HEADER *pDupBuf,  UINT2 u2VlanId),
      UINT1 *pu1IsPktSent);

INT4 CfaPrintSecVlanList PROTO((tCliHandle CliHandle));
#ifdef WGS_WANTED
VOID CfaSetVlanListForMgmtIface PROTO((VOID));
#endif
#ifdef CLI_WANTED
INT4 CfaPrintMgmtVlanList PROTO((tCliHandle CliHandle));
#endif /*CLI_WANTED*/
#ifdef WGS_WANTED
INT4 CfaIvrProcessPktFromMgmtIface PROTO ((UINT1 *pu1Buf, UINT4 u4PktSize));
#endif
INT4
CfaUtilTxPktOnVlanMemberPorts ( UINT1 *pu1DataBuf, tVlanId VlanId, BOOL1 bBcast,
                               UINT4 u4PktSize);
INT4
CfaValIfTypeProtoDenyTblIndex (INT4 i4IfTypeProtoDenyContextId,
                               INT4 i4IfTypeProtoDenyIfType,
                               INT4 i4IfTypeProtoDenyBrgPortType,
                               INT4 i4IfTypeProtoDenyProtocol);

/* cfareg.c */
INT4 CfaInitHLRegnTbl PROTO ((VOID));
INT4 CfaDeInitHLRegnTbl PROTO ((VOID));
VOID CfaNotifyIfCreate PROTO ((tCfaRegInfo *pCfaIfInfo));
VOID CfaNotifyIfDelete PROTO ((tCfaRegInfo *pCfaIfInfo));
VOID CfaNotifyIfUpdate PROTO ((tCfaRegInfo *pCfaIfInfo));
VOID CfaNotifyTnlIfUpdate PROTO ((tCfaRegInfo *pCfaInfo));
VOID CfaNotifyIfOperStChg PROTO ((tCfaRegInfo *pCfaIfInfo));
INT4 CfaNotifyIfPktRcvd PROTO ((UINT2 u2LenOrType, tCfaRegInfo *pCfaIfInfo));
UINT2 CfaGetRegTblIndx PROTO ((UINT2 u2LenOrType));
VOID CfaRegNotifyHigherLayer PROTO ((UINT4 u4IfIndex, UINT1 u1Action));
/* cfatnlmgr.c */
INT4 CfaInitTnlTable   PROTO ((VOID));
INT4 CfaDeInitTnlTable PROTO ((VOID));
INT4 CfaCreateAndInitTnlIfEntryInCxt PROTO ((tCfaTnlCxt * pTunlCxt,
                             tTnlIfEntry *pTnlIfEntry));

#ifdef MBSM_WANTED
INT4 CfaHandleIfUpdatePktFromMbsm PROTO ((tCRU_BUF_CHAIN_HEADER *pMsg));
INT4 CfaUpdateOperStatusNotPresent PROTO ((tMbsmPortInfo *pPortInfo));
INT4 CfaUpdateOperStatusDown PROTO ((tMbsmPortInfo *pPortInfo));
INT4 CfaCreateInterfacesForSlot PROTO ((INT4 i4SlotId, 
   tMbsmPortInfo *pPortInfo));
INT4 CfaDeleteInterfacesForSlot PROTO ((tMbsmPortInfo *pPortInfo)); 
INT4 CfaUpdateIfParamsForSlot PROTO ((tMbsmPortInfo *pPortInfo));
INT4 CfaMbsmIfmBringUpInterface PROTO ((UINT4 u4IfIndex, INT1 *pi1Alias));
INT4 CfaMbsmIfmBringUpConnectingInterface PROTO ((UINT4 u4IfIndex, INT1 *pi1AliasName));
INT4 CfaIfmUpdateIfParams PROTO ((UINT4 u4IfIndex));
INT4 CfaMbsmHwConfig PROTO ((UINT4 u4IfIndex));
INT4 CfaHandlePreConfigDelFromMbsm PROTO ((tMbsmCfaHwInfoMsg *pMsg));
INT4 CfaHandleCardUpdateFromMbsm PROTO ((UINT1 *pMsg, INT1 i1IsInsert));
INT4 CfaHandlePreConfigDelMsgFromMbsm PROTO ((UINT1 *pMsg));
INT4 CfaHandleLinkStatusUpdateMsgFromMbsm PROTO ((UINT1 *pMsg));
INT4 CfaMbsmHandleInterfaceStatusUpdate PROTO ((UINT4 u4IfIndex,UINT1 u1AdminStatus,
                                                UINT1 u1OperStatus));
PUBLIC INT4 CfaMbsmGddInit PROTO ((tMbsmSlotInfo *pSlotInfo));
PUBLIC INT4 CfaMbsmGddDeInit (tMbsmSlotInfo * pSlotInfo);

INT4 CfaMbsmDelSispInterfacesForSlot (tMbsmPortInfo *pPortInfo);
#endif /* MBSM_WANTED */

INT4
CfaUtilTxL2PktOnPortList PROTO ((tCfaTxPktOnPortListMsg *pTxPktOnPortListMsg));


INT4 
CfaL3VlanIntfWrite PROTO ((UINT1 *pu1DataBuf, UINT4 u4PktSize,
                           tCfaVlanInfo VlanInfo));

INT4
CfaTxIvrPktOnPort PROTO ((UINT1 *pu1DataBuf, INT4 u4PktSize, tVlanId VlanId,
                          UINT4 u4IfIndex, BOOL1 bIsOutTag));

VOID 
CfaTxFillVlanInfo PROTO ((tMacAddr DestAddr, UINT4 u4OutPort, BOOL1 bTag, 
                          tPortList Ports, tPortList UnTagPorts,
                          tCfaVlanInfo *pVlanInfo));
INT4
CfaGetIfaceInfo PROTO ((UINT1 *pModName, tCfaIfaceInfo * pIfaceInfo));

VOID
CfaUpdateHLInfoFromPkt PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, tHLInfo * pHLInfo, 
                        UINT1 u1Proto, UINT1 u1IpHeadLen));

VOID
CfaUpdateIcmpInfoFromPkt PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                 tIcmpInfo * pIcmp, UINT1 u1IpHeadLen));
UINT4
CfaCheckDOSAttacks  PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, t_IP_HEADER * pIpHdr,
                    tHLInfo * pHLInfo, tIcmpInfo * pIcmpHdr, 
                    UINT1   u1HdrLen));
UINT4
CfaValidateIpAddress PROTO ((UINT4 u4IpAddr));

UINT1
CfaGetTransportHeaderLength PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                             t_IP_HEADER * pIpHdr, UINT1 u1Headlen));

#ifdef L2RED_WANTED
VOID CfaGetNodeStatusFromRm PROTO ((VOID));
#endif
VOID CfaGetDefaultPortParams PROTO ((tCfaDefPortParams *pCfaDefPortParams));

VOID CfaNpFillInterfaceName(VOID);

#ifdef PORT_MAC_WANTED
INT1 CfaIvrVlanIfAssignMac PROTO ((UINT4 u4IfIndex));
#endif


/*This function fetches the system specific portID for a particular port*/
INT4 CfaGetIfAlias PROTO((UINT4 u4IfIndex, UINT1 *au1IfAlias));
INT4 CfaSetIfAlias PROTO((UINT4 u4IfIndex, UINT1 *au1IfAlias));
INT4 CfaUtilIfGetSysSpecificPortID PROTO ((INT4 i4IfMainIndex, UINT4* pu4SysSpecificPortID));
#ifdef MBSM_WANTED
INT4 CfaGetStackIvrOperStatus PROTO ((UINT1 * pu1OperStatus));
#endif
VOID CfaSetHgPortsEtherType (UINT2 u2IfIndex, UINT1 *pu1IfType);
VOID CfaIncMsrForIfIpTable PROTO ((INT4 i4IfIndex, INT4 i4SetVal, CHR1 cDatatype, 
                                   UINT4 *pu4ObjectId, UINT4 u4OIdLen, 
                                   UINT1 IsRowStatus));
INT1
CfaCliGetIVRNameFromIfNameAndCxtId PROTO ((UINT4 u4L2CxtId, UINT1 *pu1IfName,
                                           UINT1 *pu1IvrName));

INT4 CfaSispSetBrgPortType (UINT4 u4IfIndex, UINT1 u1BrgPortType);
VOID CfaInterfaceMtuChangeIndication PROTO ((UINT4 u4IfIndex, UINT4 u4Mtu));
INT4 CfaSetIfMainMtu PROTO ((UINT4 u4IfIndex, UINT4 u4Mtu));

INT4 CfaUtilDeletePortChannelInterface PROTO ((UINT4 u4IfIndex)); 
INT4 CfaCopyBridgePortPropToSispPorts (UINT4 u4PhyIfIndex, UINT1 u1PropType, 
                                       UINT4 u4PropVal);
PUBLIC INT4
CfaIssFillProtoInfoAndNotifyPkt PROTO ((tCfaRegInfo * pCfaInfo,
                                        UINT2 u2LenOrType));

UINT1 CfaIsVipOrSispInterface PROTO ((UINT4 u4IfIndex));
PUBLIC INT4
CfaDoIPSrcGuardValidation PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4IfIndex,
                                  tEnetV2Header *pEthHdr));

INT4 CfaGetNextIvrIfIndex (UINT4 u4IfIndex, UINT4 *p4TmpIfIndex);
INT4
CfaIfmInitVpncIfEntry PROTO ((UINT4 u4IfIndex, UINT1 *pu1PortName));

/*Application Call Back function */
INT4 CfaUtilCallBack PROTO ((UINT4 u4Event, UINT4 u4IfIndex));
INT1 CfaGetSecVlanList (tSecVlanList SecVlanList);

VOID
CfaUtilTxPktOnSecVlanMemberPorts PROTO ((UINT4    u4IfL2CxtId,
                                         UINT1*   pu1DataBuf,
                 UINT4    u4PktSize));

VOID
CfaChkSecForOutBoundBridgedFrame PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                                         UINT4                  u4IfIndex,
                                         UINT1                  *pu1Result));

/*stub function*/
INT4 L2VpnGetMPLSPwIndex PROTO ((UINT4 u4IfIndex, UINT4 *pu4PwIndex));
VOID L2VpnDelMPLSPwIfIndex PROTO ((UINT4 u4PwIndex));

/* Wrapper prototype for flexible gapIfTable changes*/

tIfInfoStruct*
CfaIfUtlGapIfGet  PROTO ((UINT4 u4IfIndex));

tIfInfoStruct*
CfaIfUtlGetGapIfDbEntry PROTO ((UINT4 u4IfIndex));

tIfInfoStruct*
CfaIfUtlGapIfArrayDbGet PROTO ((UINT4 u4CfaIfIndex));

tIfInfoStruct*
CfaIfUtlGapIfDbGet PROTO ((tCfaIfDb CfaGapIfDb, tIfInfoStruct *pCfaGapIfDbElem));

INT4
CfaIfUtlGapIfArrayDbAdd PROTO ((tIfInfoStruct *pIfInfoStruct));

INT4
CfaIfUtlGapIfArrayDbRem PROTO ((UINT4 u4CfaIfIndex));

INT4
CfaIfUtlGapIfDbIndicesCmp PROTO ((tIfInfoStruct *pRBElem1, tIfInfoStruct *pRBElem2));

VOID
CfaIfUtlGapIfInit PROTO ((VOID));

UINT4
CfaIfUtlGapIfDbGetIndex PROTO ((UINT4 u4CfaGapIfIndex));

UINT4
CfaIfUtlGapIfDbGetNextIndex PROTO ((UINT4 u4CfaGapIfIndex));

/* Wrapper prototypes for flexible ifIndex changes */

INT4
CfaIfUtlCreateIfDb PROTO ((tCfaIfDbReqType eCfaIfDbReqType));

VOID
CfaIfUtlDestroyIfDb PROTO ((tCfaIfDbReqType eCfaIfDbReqType));

INT4
CfaIfUtlIfDbIndicesCmp PROTO ((tCfaIfDbElem *pRBElem1, 
                               tCfaIfDbElem *pRBElem2));

INT4
CfaIfUtlIfDbAdd PROTO ((UINT4 u4CfaIfIndex,
                        tCfaIfDbReqType eCfaIfDbReqType));


INT4
CfaIfUtlIfTypeDbNodeInit PROTO ((UINT4 u4IfIndex,
                                 tCfaIfTypeDbReq eCfaIfTypeDbReq));

INT4
CfaIfUtlIfTypeDbAddNode PROTO ((UINT4 u4IfIndex,
                                tCfaIfTypeDbReq eCfaIfTypeDbReq));


VOID
CfaIfUtlIfTypeDbInit PROTO ((tCfaIfTypeDbReq eCfaIfTypeDbReq));

INT4
CfaIfUtlIfTypeDbUpdate PROTO ((UINT4 u4IfIndex, UINT1 u1IfType,
                               tCfaIfTypeDbReq eCfaIfTypeDbReq));

INT4
CfaIfUtlIfTypeDbRem PROTO ((UINT4 u4IfIndex,
                            tCfaIfTypeDbReq eCfaIfTypeDbReq));

tCfaIfDbElem*
CfaIfUtlIfDbGet PROTO ((tCfaIfDb CfaIfDb, tCfaIfDbElem *pCfaIfDbElem));

INT4
CfaIfUtlIfDbRem PROTO ((UINT4 u4CfaIfIndex,
                        tCfaIfDbReqType eCfaIfDbReqType));

VOID
CfaIfUtlAssert PROTO ((UINT4 u4IfIndex));

tCfaIfInfo*
CfaIfUtlCdbGet PROTO ((UINT4 u4IfIndex));

tCfaIfInfo*
CfaIfUtlCdbGetIfDbEntry PROTO ((UINT4 u4IfIndex));

tCfaIfDbElem*
CfaIfUtlIfArrayDbGet PROTO ((UINT4 u4CfaIfIndex));

INT4
CfaIfUtlIfArrayDbAdd PROTO ((UINT4 u4CfaIfIndex));

INT4
CfaIfUtlIfArrayDbRem PROTO ((UINT4 u4CfaIfIndex));

VOID
CfaIfUtlCreateCdbMemPool PROTO ((VOID ));

UINT4
CfaIfUtlIfDbGetIndex PROTO ((UINT4 u4CfaIfIndex, UINT1 u1IfType));

UINT4
CfaIfUtlIfDbGetNextIndex PROTO ((UINT4 u4CfaIfIndex, UINT1 u1IfType));

UINT4
CfaIfUtlIfDbGetNextIndexWithLock PROTO ((UINT4 u4CfaIfIndex, UINT1 u1IfType));

UINT4
CfaIfUtlIfDbGetIndexWithLock PROTO ((UINT4 u4CfaIfIndex, UINT1 u1IfType));

/* Flexible IfIndex change for Mau Table */
VOID CfaIfUtlCreateMauMemPool PROTO ((VOID)); 

INT4 CfaIfUtlMauMemAlloc PROTO ((UINT4 u4IfIndex));

INT4 CfaIfUtlMauMemRelease PROTO ((UINT4 u4IfIndex));

INT4
CfaIfmStackDynamicTeLink PROTO ((UINT4 u4TeIfIndex, UINT4 u4CompIfIndex));
INT4
CfaIfmCreateDynamicTeLinkIf (UINT4 u4IfIndex, UINT4 u4CompIfIndex);
INT4
CfaIfmCreateDynamicMplsIf (UINT4 u4IfIndex);
INT4 CfaCreateRmIPInterface PROTO ((VOID));
VOID
CfaGetDefaultNetMask (UINT4 u4IpAddress, UINT4 *pu4NetMask);
/*PUBLIC UINT4 SyslogNotifyIfStatusChange (UINT4 u4IfIndex);*/
#if defined (WLC_WANTED) || defined (WTP_WANTED)
UINT4 CfaIsRadioIntf PROTO ((UINT4 u4IfIndex));

UINT4 CfaIsWssIntf PROTO ((UINT4 u4IfIndex));

UINT4 CfaIfmHandleWssInterfaceStatusChange PROTO ((UINT4 u4IfIndex, 
                    INT4 i4AdminStatus, UINT1 u1OperStatus));

INT4 CfaIfmInitVirtualRadioIfEntry PROTO ((UINT4 u4IfIndex, UINT1 *au1PortName));

INT4 CfaIfmInitRadioIfEntry PROTO ((UINT4 u4IfIndex, UINT1 *au1PortName));

INT4 CfaIfmInitWlanRadioIfEntry PROTO ((UINT4 u4IfIndex, UINT1 *au1PortName));

INT4 CfaIfmInitWlanProfileIfEntry PROTO ((UINT4 u4IfIndex, UINT1 *au1PortName));

INT4 CfaIfmInitWlanBssIfEntry PROTO ((UINT4 u4IfIndex, UINT1 *au1PortName));

INT4 CfaIfmDeleteRadioInterface PROTO((UINT4 u4IfIndex, UINT1 u1DelIfEntry));

INT4 CfaIfmDeleteWssInterface PROTO((UINT4 u4IfIndex, UINT1 u1DelIfEntry));

INT4 CfaProcessOutgoingWssPacket PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                                         UINT4 u4IfIndex,
      UINT4 u4PktSize,
      UINT2 u2Protocol,
      UINT1* au1DestHwAddr));

UINT1 CfaCreateInterfaceIndex PROTO ((UINT4 u4IfType, UINT4 *pu4CfaIndex));

INT4 CfaCreateWssInterface PROTO ((UINT4 u4IfIndex, UINT1 *pu1IfName, INT4 i4IfType));

INT4 CfaCreateRadioInterface PROTO ((UINT4 u4IfIndex, UINT1 *pu1IfName, INT4 i4IfType));
#endif

#ifndef MBSM_WANTED
INT4 CfaGetIfIndexFromAliasName PROTO ((UINT1 *pu1IfName,UINT4* u4IfIndex));
INT4 CfaGetAliasNameFromIfIndex PROTO ((UINT4 u4IfIndex, UINT1 * pu1IfAlias));
#endif
UINT1 CfaIsOpenFlowIndex (UINT4 u4IfIndex);
INT4 CfaIfmInitOfcVlanIfEntry (UINT4 u4IfIndex, UINT1 *au1IfName);
#ifdef LNXIP4_WANTED
UINT4  CfaGetIdxMgrPort (UINT4 u4IfIndex);
#endif

INT4 CfaIpGetAddrTypeInCxt(UINT4 u4ContextId, UINT4 u4Addr);

PUBLIC INT4 AclUpdateOverPortChannel PROTO ((UINT4 u4IfIndex));

#endif /* _CFAPROTO_H */


/* cfaufd.c */
INT4 CfaUfdStart PROTO ((VOID));
INT4 CfaUfdShutdown PROTO ((VOID));
VOID CfaUfdEnable PROTO ((VOID));
VOID CfaUfdDisable PROTO ((VOID));
VOID CfaUfdGetSystemControl PROTO ((UINT1*));
VOID CfaUfdGetModuleStatus PROTO ((UINT1*)); 
VOID CfaUfdGetGroupCount PROTO ((UINT4*));
INT4 CfaIfUfdGroupInfoTableCmp PROTO 
   ((tCfaIfDbElem *, tCfaIfDbElem *));
INT4 CfaIfUfdGroupInfoDbCreate PROTO ((VOID));
INT4 CfaIfChangePortRole PROTO ((UINT4, UINT1));
INT4 CfaUfdCreateGroup PROTO ((INT4));
VOID CfaUfdDeleteGroup PROTO ((INT4));
INT4 CfaUfdAddPortToGroup PROTO ((UINT4, UINT4));
INT4 CfaUfdRemovePortFromGroup PROTO ((UINT4, UINT4));
INT4 CfaUfdAddUplinkPort PROTO ((UINT4, UINT4));
INT4 CfaUfdAddDownlinkPort PROTO ((UINT4, UINT4));
INT4 CfaUfdRemoveUplinkPort PROTO ((UINT4, UINT4));
INT4 CfaUfdRemoveDownlinkPort PROTO ((UINT4, UINT4));
INT4 CfaUfdChangeDesigUplink PROTO ((UINT4 u4PortIndex, UINT4 u4UfdGroupId));
INT4 CfaUfdSetGroupStatus PROTO ((UINT4, INT4));
INT4 CfaUfdGetGroupStatus PROTO ((UINT4, UINT1 *));
INT4 CfaUfdDeleteAllGroupEntries PROTO ((VOID));
INT4 CfaUfdCheckPortRole PROTO ((UINT4, UINT1));
INT4 CfaUfdCheckAnyActiveUplinkInGroup PROTO ((UINT4, UINT4));
INT4 CfaNotifyUfdOperStatusUpdate PROTO ((UINT4, UINT1));
INT4 CfaUfdChangeOperStatusToUp PROTO ((UINT4 u4PortIndex));
INT4 CfaUfdChangeOperStatusToDown PROTO ((UINT4 u4PortIndex)); 
tUfdGroupInfo* CfaIfUfdGroupInfoDbGet PROTO ((UINT4));
INT4 CfaSetIfUfdOperStatus PROTO ((UINT4, UINT4));
INT4 CfaGetIfUfdOperStatus PROTO ((UINT4, UINT1*));
INT4 CfaGetIfUfdDownlinkEnabledCount PROTO ((UINT4, UINT4 *));
INT4 CfaGetIfUfdDownlinkDisabledCount PROTO ((UINT4, UINT4 *));
INT4 CfaUfdCheckAnyDesiguplinkPresent PROTO ((UINT4, UINT4 *));
INT4 CfaCliSetUfdSystemCtrl PROTO((tCliHandle CliHandle, INT4 i4Status));
INT4 CfaCliSetUfdModuleStatus PROTO((tCliHandle CliHandle, INT4 i4Status));
INT4 CfaCliSetPortRole PROTO((tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4PortRole, UINT4 u4DesigUplinkStatus));
INT4 CfaCliCreateUfdGroup PROTO((tCliHandle CliHandle, UINT4 u4UfdGroupId, UINT1 *pau1UfdGroupName));
INT4 CfaCliDeleteUfdGroup PROTO((tCliHandle CliHandle, INT4 i4UfdGroupId));
INT4 CfaCliShowUfdDatabase PROTO((tCliHandle CliHandle, INT4 i4NextGroupId, UINT4 u4Command));
VOID CfaCliShowUfdGroupDatabase PROTO ((tCliHandle CliHandle, INT4 i4NextIndex, INT4 i4NextGroupId));
INT4 CfaCliShowPortRole PROTO ((tCliHandle CliHandle, INT4 i4NextIndex));
INT4 CfaCliShowPortUfdStats PROTO ((tCliHandle CliHandle, INT4 i4NextIndex));
INT4 CfaCliUfdGroupSetPorts PROTO ((tCliHandle CliHandle, tPortList IfPortList,UINT4 u4Flag));

INT4
CfaCliSetDot1QEncapForL3SubIf (tCliHandle CliHandle, UINT4 u4IfIndex,
                                                     UINT4 u4VlanId);
INT4
CfaCliDelDot1QEncapForL3SubIf (tCliHandle CliHandle, UINT4 u4IfIndex);

INT4 CfaCliSetDesigUplinkPort PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
INT4 CfaCliEnableSplitHorizon PROTO ((tCliHandle CliHandle));
INT4 CfaCliDisableSplitHorizon PROTO ((tCliHandle CliHandle));
INT4 CfaSetIfPortRole PROTO((UINT4 u4IfIndex, UINT1 u1IfPortRole));
INT4 CfaGetIfPortRole PROTO((UINT4 u4IfIndex, UINT1 *pu1IfPortRole));
INT4 CfaGetIfDesigUplinkStatus PROTO((UINT4 u4IfIndex, UINT1 *pu1IfDesigUplinkStatus));
INT4 CfaSetIfDesigUplinkStatus PROTO((UINT4 u4IfIndex, UINT1 u1IfDesigUplinkStatus));
INT4 CfaSetIfUfdGroupId PROTO((UINT4 u4IfIndex, UINT4 u4IfUfdGroupId));
INT4 CfaGetIfUfdGroupId PROTO((UINT4 u4IfIndex, UINT4 *pu1IfUfdGroupId));
INT4 CfaGetFirstUfdGroup PROTO((UINT4 *pu4UfdGroupId));
INT4 CfaGetNextUfdGroup PROTO((UINT4 u4UfdGroupId, UINT4 *pu4NextUfdGroupId));

INT4 CfaUfdSeperatePortlistOnPortRole PROTO ((tPortList IfPortList, tPortList *pUplinkPorts ,
                         tPortList *pDownlinkPorts));
UINT4 CfaUfdCountPortlistSetBits PROTO ((tPortList IfPortList));
INT4 CfaUfdValidateRemovePortList PROTO ((tPortList IfPortList , UINT4 u4UfdGroupId));
INT4 CfaUfdCheckForAlpha PROTO ((UINT1 *pu1UfdGroupName));
INT4 CfaUfdCheckGroupNameUnique PROTO ((UINT1 *pu1UfdGroupName, UINT4 u4UfdGroupId));
INT4 CfaUfdCheckLastDownlinkInGroup PROTO ((UINT4 u4IfIndex));
VOID CfaUfdPortChangeUpToErrorDisabled PROTO ((VOID));
VOID CfaUfdPortChangeErrorDisabledToUp PROTO ((VOID));
VOID CfaUfdRemoveAllPortsInGroup PROTO ((UINT4 u4UfdGroupId));
INT4 CfaCliSetShModuleStatus PROTO ((tCliHandle CliHandle, INT4 i4Status));
INT4 CfaCliSetShSystemCtrl PROTO ((tCliHandle CliHandle, INT4 i4Status));
INT4 CfaShShowRunningConfig PROTO ((tCliHandle CliHandle, UINT4 u4Module));
INT4 CfaShModifyPortEntries PROTO ((tIssUpdtPortIsolation * pIssUpdtPortIsolation , UINT4 u4Scan));
INT4 CfaConfigureShRules PROTO((INT4));
VOID CfaShStart PROTO ((VOID));
INT4 CfaShShutdown PROTO ((VOID));
INT4 CfaShEnable PROTO ((INT4));
INT4 CfaShDisable PROTO ((INT4));
VOID IssShowSplitHorizon PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
INT4 CfaUfdShowRunningConfig PROTO ((tCliHandle CliHandle, UINT4 u4Module));

VOID CfaSnmpifUfdSendTrap (UINT4 u4IfIndex, UINT1 u1UfdStatus);

INT4
CfaCliUfdSetPortList (tCliHandle CliHandle,tPortList IfPortList,
                      UINT4 u4UfdGroupId, UINT4 u4Flag);
CHR1*
CfaIcchApiGetIcclIpInterface  (UINT2 u2Index);
UINT4 CfaIcchApiGetIcclVlanId  (UINT2 u2Index);
INT1
CfaIcchSetIcclIfIndex (UINT4 u4IfIndex);
UINT4
CfaIcchApiGetIcclIpMask  (UINT2 u2Index);
UINT4 CfaIcchApiGetIcclIpAddr  (UINT2 u2Index);
VOID
CfaIcchSetIcclL3IfIndex (UINT4 u4IfIndex);
VOID
CfaIcchGetIcclIfIndex (UINT4 *pu4IfIndex);


INT4 CfaIvrSetIpAddressForPortVlanId (UINT4 u4IfIndex, UINT4 u4IpAddr,
                                            UINT4 u4IpSubnetMask);
INT4
CfaIvrDeleteIpAddressForPortVlanId (UINT4 u4IfIndex, UINT4 u4IpAddr);
INT4
CfaHbIcchPostHBPktToIcch (tCRU_BUF_CHAIN_HEADER * pBuf);
UINT4 CfaIcchIsIcclVlan (UINT2 u2VlanId);
INT4
CfaIfSetOOBLocalNodeSecondaryIpAddress (UINT4 u4IpAddr , UINT4 u4IpMask);
INT4
CfaGetSecondaryOobInterfaceName (UINT1 *pu1IfName);
VOID
CfaVlanApiEvbGetSbpPortsOnUap (UINT4 u4UapIfIndex,tVlanEvbSbpArray *pSbpArray);
INT4
CfaUtilCreateSChannelInterface (UINT4 u4UapIfIndex, UINT4 u4IfIndex, UINT1 u1Status);
INT4
CfaUtilDeleteSChannelInterface (UINT4 u4IfIndex);
INT4
CfaUtilSetSChannelOperStatus  (UINT4 u4IfIndex , UINT1 u1OperStatus);

INT4 CfaLaApiIsMemberofMcLag (UINT4 u4IfIndex);
VOID CfaSetBrgPortFromBrgMode(UINT4 u4IfIndex);
VOID CfaApiIcchGetIcclL3IfIndex (UINT4 *pu4IfIndex);
UINT4 CfaIsIcchNextIpAddress (UINT4 u4IpAddr, UINT4 u4NetMask);
UINT4
CfaIsVlanOverLapping (UINT4 u4L3SubIfIndex, UINT2 u2VlanId);
UINT4
CfaL3SubIfShutDownNotify (UINT4  u4IfMainIndex , UINT1 u1OperStatus);
UINT4 
CfaGetVlanInterfaceL3IndexInCxt (UINT4 u4L3ContextId, tVlanIfaceVlanId VlanId);
UINT4
CfaIsL3VlanOverLapWithL3SubVlan (UINT4 u4ContextId, UINT2 u2VlanId);



