/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ifmmacs.h,v 1.64 2015/09/25 12:33:26 siva Exp $
 *
 * Description:This file contains the MACROS for accessing  
 *             global structures of the Interface       
 *             Management module of the CFA.        
 *
 *******************************************************************/
#ifndef _IFMMACS_H
#define _IFMMACS_H

/* Maximum num. of interfaces as specified in IANAif standards */
#define CFA_MAX_IF_TYPES         255 

#define CFA_DEREGISTER      0
#define CFA_REGISTER        1

#define CFA_TLV_MIN_LENGTH    4
#define CFA_TLV_MAX_LENGTH    32
#define CFA_32BIT_MAX                  4294967295U  /* 0xffffffff */


/*  MACROS for interface data structures  */
/* for interface globals */

#define   CFA_CHECK_IF_NULL(pNode,i4RetVal) \
{\
    if (pNode == NULL)\
    {\
       return i4RetVal;\
    }\
}
    
#define   CFA_IFNUMBER()         (gIfGlobal.u4IfNumber)          
#define   CFA_AVAIL_IFINDEX()    (gIfGlobal.u4AvailableIfIndex)  
#define   CFA_IFTAB_CHNG()       (gIfGlobal.u4IfTableLastChange) 
#define   CFA_IFSTK_CHNG()       (gIfGlobal.u4IfStackLastChange) 
#define   CFA_MAX_INTERFACES()   (gIfGlobal.u4MaxInterfaces)
#define   CFA_PHYS_NUM()         (gIfGlobal.u2MaxPhysInterfaces) 

#define   CFA_MAX_TUNL_INTERFACES() (gIfGlobal.u2MaxTunlInterfaces)

extern tMemPoolId          IfEntryMemPoolId;
#define   CFA_IF_MEMPOOL         IfEntryMemPoolId                
#define   CFA_GDD_MEMPOOL        GddEntryMemPoolId               
#define   CFA_IPIF_MEMPOOL       IpEntryMemPoolId
#define   CFA_WAN_MEMPOOL        WanEntryMemPoolId              
#define   CFA_TLV_MEMPOOL        IfTlvEntryMemPoolId
#define   CFA_TLV_BUDDY_MEMPOOL  i4IfTlvBuddyMemPoolId
#define   CFA_ENET_IWF_MEMPOOL   EnetIwfMemPoolId


#define   CFA_REG_MEMPOOL        CfaRegMemPoolId

#define   CFA_NODE_STATUS()      gCfaNodeStatus

#define   CFA_RED_IPINIT_COMPLETE_FLAG()  gu1CfaIpInitComplete

#define   CFA_RED_STADNBY_RCVD               (0xff)

#ifdef RM_WANTED
#define   CFA_IS_NP_PROGRAMMING_ALLOWED() \
        ((L2RED_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE) ? CFA_TRUE: CFA_FALSE)
#else
#define   CFA_IS_NP_PROGRAMMING_ALLOWED() CFA_TRUE
#endif

/* Macro for accessing the IfEntry */
#define   CFA_IF_ENTRY(u4IfIndex)    CFA_GAP_IF_GET(u4IfIndex) 

#define   CFA_IF_OPQ_ATTR(i4IfIndex,i4OpqAttrId) \
          (CFA_IF_ENTRY(i4IfIndex)->CustIfInfo.au4OpqAttrValue[i4OpqAttrId-1])

#define   CFA_IF_OPQ_ATTR_RS(i4IfIndex,i4OpqAttrId) \
          (CFA_IF_ENTRY(i4IfIndex)->CustIfInfo.au1OpqAttrStatus[i4OpqAttrId-1])

#define   CFA_IF_IWF(u4IfIndex)      (CFA_IF_ENTRY(u4IfIndex)->pIwfStruct)

/* MACRO to check if the entry in the IfTable is being used */
#define   CFA_IF_ENTRY_USED(u4IfIndex)    (CfaIfUtlGetGapIfDbEntry(u4IfIndex) != NULL)

#define   CFA_IF_OPQ_CONFIGURED(i4IfIndex,i4OpqAttrId) \
                    ((CFA_IF_ENTRY_USED(i4IfIndex)) && \
                     (CFA_IF_OPQ_ATTR_RS(i4IfIndex,i4OpqAttrId) != CFA_RS_INVALID))

#define   CFA_IF_ADMIN(u4IfIndex)        \
          (CFA_IF_ENTRY(u4IfIndex)->u1AdminStatus) 

#define   CFA_IF_STORAGE_TYPE(u4IfIndex)        \
          (CFA_IF_ENTRY(u4IfIndex)->i4IfMainStorageType) 

#define   CFA_IF_PROTOS(u4IfIndex)       \
          (CFA_IF_ENTRY(u4IfIndex)->u4RegProtMask) 

#define   CFA_IF_GENERAL(u4IfIndex)      \
          (CFA_IF_ENTRY(u4IfIndex)->IfGeneralInfo) 

#define   CFA_IF_HIGH_INDEX(u4IfIndex)   \
          (CFA_IF_ENTRY(u4IfIndex)->u4HighIfIndex) 

#define   CFA_IF_STACK_LOW(u4IfIndex)    \
          (CFA_IF_ENTRY(u4IfIndex)->IfStackLowerLayer)
    
#define   CFA_IF_STACK_HIGH(u4IfIndex)   \
          (CFA_IF_ENTRY(u4IfIndex)->IfStackHigherLayer)

#define   CFA_IF_RCVADDRTAB(u4IfIndex)   \
          (CFA_IF_ENTRY(u4IfIndex)->IfRcvAddressTable)

#define CFA_IF_TLV_INFO(u4IfIndex)   \
             (CFA_IF_ENTRY(u4IfIndex)->CustIfInfo.IfTlvTable)

#define   CFA_IF_IPADDR(u4IfIndex)      (CFA_IP_ENTRY(u4IfIndex)->u4IpAddr)
#define   CFA_IP_ENTRY(u4IfIndex)      (CFA_IF_ENTRY(u4IfIndex)->pIpEntry)      
#define   CFA_IF_IPSUBNET(u4IfIndex)    (CFA_IP_ENTRY(u4IfIndex)->u1SubnetMask)
#define   CFA_IF_IPBCAST(u4IfIndex)     (CFA_IP_ENTRY(u4IfIndex)->u4BcastAddr)
#define   CFA_IF_IPFORWARD(u2IfIndex)   \
                  (CFA_IP_ENTRY(u2IfIndex)->u1IpForwardEnable)
#define   CFA_IF_DEF_ROUTE_METRIC(u2IfIndex) \
              (CFA_IP_ENTRY(u2IfIndex)->i2DefRouteMetric)

#define   CFA_IF_IDLE_TIMEOUT(u4IfIndex) \
              (CFA_WAN_ENTRY(u4IfIndex)->u4IdleTimeout)

#define   CFA_IF_IDLE_LAST_COUNTERS(u4IfIndex) \
              (CFA_WAN_ENTRY(u4IfIndex)->u4LastTimeoutCounters)

  
#define   CFA_IF_RS(u4IfIndex)         (CFA_IF_ENTRY(u4IfIndex)->u1RowStatus)  
#define   CFA_IF_ENCAP(u4IfIndex)      (CFA_IF_ENTRY(u4IfIndex)->u1EncapType)  
#define   CFA_IF_BRG_PORT_TYPE(u4IfIndex)  \
                     (CFA_IF_ENTRY(u4IfIndex)->u1BrgPortType)  
#define   CFA_IF_IS_BACKPLANE(u4IfIndex) \
                     (CFA_IF_ENTRY (u4IfIndex)->u1IsBackPlaneIntf)
#define   CFA_IF_FLAG(u4IfIndex)       (CFA_IF_ENTRY(u4IfIndex)->u1Reserved)   
#define   CFA_GDD_ENTRY(u4IfIndex)     (CFA_IF_ENTRY(u4IfIndex)->pGddEntry)    
#define   CFA_WAN_ENTRY(u4IfIndex)     (CFA_IF_ENTRY(u4IfIndex)->pWanEntry)    

#define   CFA_TNL_ENTRY(u4IfIndex)     (CFA_IF_ENTRY(u4IfIndex)->pTnlIfEntry)
/* Macros for accessing the General Info Entry */
#define   CFA_IF_TRAP_EN(u4IfIndex) \
          (CFA_IF_GENERAL(u4IfIndex).u1LinkUpDownTrapEnable)

#define   CFA_IF_PROMISC(u4IfIndex) \
          (CFA_IF_GENERAL(u4IfIndex).u1PromiscuousMode)

#define   CFA_IF_CHNG(u4IfIndex)    \
          (CFA_IF_GENERAL(u4IfIndex).u4LastChange)

#define   CFA_IF_DESCR(u4IfIndex)   \
          (CFA_IF_GENERAL(u4IfIndex).au1Descr)

/* Macros for accesing the Stack Table */
#define   CFA_IF_STACK_LOW_ENTRY(u4IfIndex) \
          (TMO_SLL_First(&CFA_IF_STACK_LOW(u4IfIndex)))

#define   CFA_IF_STACK_HIGH_ENTRY(u4IfIndex) \
          (TMO_SLL_First(&CFA_IF_STACK_HIGH(u4IfIndex)))

#define   CFA_IF_STACK_IFINDEX(pNode) \
          (((tStackInfoStruct *)pNode)->u4IfIndex)

#define   CFA_IF_STACK_STATUS(pNode) \
          (((tStackInfoStruct *)pNode)->u1StackStatus)


/* Macros for accesing the RcvAddr Table */
#define   CFA_IF_RCVADDRTAB_ENTRY(u4IfIndex) \
          (TMO_SLL_First(&CFA_IF_RCVADDRTAB(u4IfIndex)))

#define   CFA_IF_RCVADDRTAB_ADDR(pNode) \
          (((tRcvAddressInfo *)pNode)->au1Address)

#define   CFA_IF_RCVADDRTAB_STATUS(pNode) \
          (((tRcvAddressInfo *)pNode)->u1AddressStatus)

#define   CFA_IF_RCVADDRTAB_TYPE(pNode) \
          (((tRcvAddressInfo *)pNode)->u1AddressType)

#define CFA_SEC_ADD_VLAN_LIST(au1List1, au1List2) \
              {\
                 UINT2 u2ByteIndex;\
                 \
                 for (u2ByteIndex = 0;\
                      u2ByteIndex < CFA_SEC_VLAN_LIST_SIZE;\
                      u2ByteIndex++) {\
                    au1List1[u2ByteIndex] |= au1List2[u2ByteIndex];\
                 }\
              }

#define CFA_SEC_REMOVE_VLAN_LIST(au1List1, au1List2) \
              {\
                 UINT2 u2ByteIndex;\
                 \
                 for (u2ByteIndex = 0;\
                      u2ByteIndex < CFA_SEC_VLAN_LIST_SIZE;\
                      u2ByteIndex++) {\
                    au1List1[u2ByteIndex] = (UINT1)(au1List1[u2ByteIndex] & ~au1List2[u2ByteIndex]);\
                 }\
              }
#define CFA_SEC_VLANS_PER_BYTE   8
#define CFA_SEC_BIT8             0x80
#define CFA_MGMT_VLANS_PER_BYTE  8
#define CFA_MGMT_BIT8            0x80

#define CFA_MGMT_ADD_VLAN_LIST(au1List1, au1List2) \
              {\
                 UINT2 u2ByteIndex;\
                 \
                 for (u2ByteIndex = 0;\
                      u2ByteIndex < CFA_MGMT_VLAN_LIST_SIZE;\
                      u2ByteIndex++) {\
                    au1List1[u2ByteIndex] |= au1List2[u2ByteIndex];\
                 }\
              }

#define CFA_MGMT_REMOVE_VLAN_LIST(au1List1, au1List2) \
              {\
                 UINT2 u2ByteIndex;\
                 \
                 for (u2ByteIndex = 0;\
                      u2ByteIndex < CFA_MGMT_VLAN_LIST_SIZE;\
                      u2ByteIndex++) {\
                    au1List1[u2ByteIndex] &= ~au1List2[u2ByteIndex];\
                 }\
              }

#define CFA_MGMT_IS_MEMBER_VLAN(au1PortArray, u2Port, u1Result) \
        {\
           UINT2 u2PortBytePos;\
           UINT2 u2PortBitPos;\
           u2PortBytePos = (UINT2)(u2Port / CFA_MGMT_VLANS_PER_BYTE);\
           u2PortBitPos  = (UINT2)(u2Port % CFA_MGMT_VLANS_PER_BYTE);\
           if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
           \
           if ((au1PortArray[u2PortBytePos] \
                & gau1IvrPortBitMaskMap[u2PortBitPos]) != 0) {\
           \
              u1Result = CFA_TRUE;\
           }\
           else {\
           \
              u1Result = CFA_FALSE; \
           } \
        }

#define CFA_MGMT_SET_MEMBER_VLAN(au1PortArray, u2Port) \
           {\
              UINT2 u2PortBytePos;\
              UINT2 u2PortBitPos;\
              u2PortBytePos = (UINT2)(u2Port / CFA_MGMT_VLANS_PER_BYTE);\
              u2PortBitPos  = (UINT2)(u2Port % CFA_MGMT_VLANS_PER_BYTE);\
              if (u2PortBitPos  == 0) {u2PortBytePos =(UINT2)( u2PortBytePos - 1);} \
           \
             au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos]\
                             | gau1IvrPortBitMaskMap[u2PortBitPos]);\
           }


/* Macros for accessing the Wan Entry config struct */
#define   CFA_IF_CONN_TYPE(u4IfIndex) \
          (CFA_WAN_ENTRY(u4IfIndex)->u1ConnType)

#define   CFA_IF_COMPR_EN(u4IfIndex) \
          (CFA_WAN_ENTRY(u4IfIndex)->u1CompressionEnable)

#define   CFA_IF_PEER_MEDIA(u4IfIndex) \
          (CFA_WAN_ENTRY(u4IfIndex)->au1PeerAddress)

#define   CFA_IF_QOS_INDEX(u4IfIndex) \
          (CFA_WAN_ENTRY(u4IfIndex)->u4QosIndex)

#define   CFA_IF_PERST(u4IfIndex)   \
          (CFA_WAN_ENTRY(u4IfIndex)->u1Persistence)

#define   CFA_IF_DEMAND_CKT_STATUS(u4IfIndex)   \
          (CFA_WAN_ENTRY(u4IfIndex)->u1DemandCktStatus)

#define   CFA_IF_PEER_IPADDR(u4IfIndex) \
          (CFA_WAN_ENTRY(u4IfIndex)->u4ConfigPeerIpAddr)

#define   CFA_IF_NEGO_PEER_IPADDR(u4IfIndex) \
          (CFA_WAN_ENTRY(u4IfIndex)->u4NegoPeerIpAddr)

#define   CFA_IF_CONFIG_MTU(u4IfIndex) \
          (CFA_WAN_ENTRY(u4IfIndex)->u4ConfigMtu)

#define   CFA_IF_DNS_PRI_IPADDR(u4IfIndex) \
                  (CFA_IP_ENTRY(u4IfIndex)->u4DnsPrimaryIpAddr)

#define   CFA_IF_DNS_SEC_IPADDR(u2IfIndex) \
                  (CFA_IP_ENTRY(u2IfIndex)->u4DnsSecondaryIpAddr)

/* MACRO to check if the IWF entry in the IfTable is being used */
#define   CFA_IWF_ENTRY_USED(u4IfIndex)   (CFA_IF_IWF(u4IfIndex) != NULL)    

/* Macro to check if IP is registered over this interface */
#define   CFA_IF_ENTRY_REG_WITH_IP(u4IfIndex, pNode) \
          ((CFA_IF_STACK_IFINDEX(pNode) \
          == CFA_NONE) && \
          (CFA_IF_STACK_STATUS(pNode) \
          == CFA_RS_ACTIVE) &&\
          (CFA_IF_IPPORT(u4IfIndex) != CFA_INVALID_INDEX))

#define CFA_IS_MAC_MULTICAST(pMacAddr)\
            (pMacAddr[0] & 0x01)

#ifdef MPLS_WANTED
#define CFA_IS_MAC_UNNUM_MULTICAST(pMacAddr) \
        (MEMCMP (pMacAddr, gu1CfaUnNumMCastMac, CFA_ENET_ADDR_LEN))
#else
#define CFA_IS_MAC_UNNUM_MULTICAST(pMacAddr) (CFA_TRUE)
#endif /* MPLS_WANTED */

#define CFA_IS_PHYSICAL_IFACE(IfaceType) \
        (IfaceType == CFA_ENET)
#define CFA_IS_LOGICAL_IFACE(IfaceType) \
         ((IfaceType == CFA_MPLS) || (IfaceType == CFA_L3IPVLAN) || \
         (IfaceType == CFA_TUNNEL))
 
#define   CFA_ADDRESS_ABOVE_CLASS_C(u4Addr) \
          (u4Addr > 0xdfffffff)

#define   CFA_IS_RES_MCAST_ADDR(u4Addr) \
          (u4Addr >= 0xe0000000 && u4Addr <= 0xe00000ff)

#define CFA_ISALPHA(c) (((c) >= 'a' && (c) <= 'z') || ((c) >= 'A' && (c) <= 'Z'))
              
/*****************************************************************************/
/*       Macros for getting a specific no. of bytes                          */
/*****************************************************************************/

#define CFA_GET_1_BYTE(pMsg, u4Offset, u1Val)\
        u1Val = *(UINT1 *)((UINT1 *)(CB_FVB(pMsg))+u4Offset)

#define CFA_GET_2_BYTE(pMsg, u4Offset, u2Val)                     \
do{                                                           \
        u2Val = *(UINT2 *)(VOID *)((UINT1 *)(CB_FVB(pMsg))+u4Offset); \
        u2Val = OSIX_NTOHS((u2Val));                            \
}while(0)

#define  CFA_GET_4_BYTE(pBuf, offset, value) \
           CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *)&value, (offset), 4); \
           value = IP_NTOHL(value)
 
#define CFA_ENET_CREATE_DELETE_ALLOWED \
    ((VcmGetSystemModeExt (CFA_PROTOCOL_ID) == VCM_SI_MODE) ? 0 : 1)

#define CFA_PSW_CREATE_DELETE_ALLOWED \
    ((VcmGetSystemModeExt (CFA_PROTOCOL_ID) == VCM_SI_MODE) ? 0 : 1)

#define CFA_AC_CREATE_DELETE_ALLOWED \
    ((VcmGetSystemModeExt (CFA_PROTOCOL_ID) == VCM_SI_MODE) ? 0 : 1)

#ifdef MBSM_WANTED
#define CFA_DEFAULT_IFINDEX_SUPPORTED 0 /* No Default interface in MBSM */ 
#else
/* Default interface exists in SI */ 
#define CFA_DEFAULT_IFINDEX_SUPPORTED \
    ((VcmGetSystemModeExt (CFA_PROTOCOL_ID) == VCM_SI_MODE) ? 1 : 0)
#endif

#define CFA_IS_BRG_PORT_TYPE_VALID(u1PortType) \
   (((u1PortType == CFA_PROVIDER_NETWORK_PORT) || \
     (u1PortType == CFA_CNP_STAGGED_PORT) || \
     (u1PortType == CFA_CNP_PORTBASED_PORT) || \
     (u1PortType == CFA_CUSTOMER_EDGE_PORT) || \
     (u1PortType == CFA_PROP_CUSTOMER_EDGE_PORT) || \
     (u1PortType == CFA_PROP_CUSTOMER_NETWORK_PORT) || \
     (u1PortType == CFA_CUSTOMER_BRIDGE_PORT) || \
     (u1PortType == CFA_PROP_PROVIDER_NETWORK_PORT) || \
     (u1PortType == CFA_CNP_CTAGGED_PORT) || \
     (u1PortType == CFA_VIRTUAL_INSTANCE_PORT) || \
     (u1PortType == CFA_PROVIDER_INSTANCE_PORT) || \
     (u1PortType == CFA_CUSTOMER_BACKBONE_PORT) || \
     (u1PortType == CFA_UPLINK_ACCESS_PORT) || \
     (u1PortType == CFA_INVALID_BRIDGE_PORT))? \
     CFA_TRUE : CFA_FALSE)

/* To check port type for internal type interface */
#define CFA_IS_INTERNAL_INTERFACE_PORT_TYPE_VALID(u1PortType) \
   (((u1PortType == CFA_PROVIDER_INSTANCE_PORT) || \
     (u1PortType == CFA_CUSTOMER_BACKBONE_PORT))?  \
     CFA_TRUE : CFA_FALSE)

/*Macro for internal-lan name offset*/
#define CFA_ILAN_NAME_OFFSET       12

#define CFA_IS_PORT_SEC_STATE_VALID(u1PortSecState) \
       (((u1PortSecState == CFA_PORT_STATE_TRUSTED) || \
              (u1PortSecState == CFA_PORT_STATE_UNTRUSTED))? \
             CFA_TRUE : CFA_FALSE)
#define   CFA_TRC_PPP_PKT_DUMP      (0x01000000)


/* Macros for defining the global custom callback*/
#define CFA_UTIL_CALL_BACK gaCfaUtilCallBack

/* Memory Pool Id Macros */
#define CFA_PKT_GEN_QUE_POOL_ID  CFAMemPoolIds[MAX_CFA_PKTGEN_SIZING_ID]
#define CFA_PKT_GEN_TX_POOL_ID   CFAMemPoolIds[MAX_CFA_PKTGEN_TX_INFO_SIZING_ID]
#define CFA_PKT_GEN_RX_POOL_ID   CFAMemPoolIds[MAX_CFA_PKTGEN_RX_INFO_SIZING_ID]

#endif /* _IFMMACS_H */

