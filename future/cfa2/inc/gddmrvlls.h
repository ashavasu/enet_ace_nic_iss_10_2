/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddmrvlls.h,v 1.3 2010/08/16 13:52:30 prabuc Exp $
 *
 * Description:This file includes header files for GDD module compilation   
 *
 *******************************************************************/
#ifndef _GDDLINUX_H
#define _GDDLINUX_H

#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#ifdef LNXIP4_WANTED
#include "lnxip.h"
#include <linux/sockios.h>
#include <linux/if_ether.h>
#endif

typedef struct pollfd pollfdstruct; /* for ease of reference */


/* the global structure which is used for polling interfaces */
typedef struct {
   pollfdstruct    *aFdList;
   UINT4           *au4IfIndex;
   UINT2           u2LastIndex;
   UINT2           u2Reserved;
} tFdListStruct;


/* Macros for accessing the polling structure */
#define   CFA_GDD_FDLIST()                 (FdTable.aFdList)                
#define   CFA_GDD_IFINDEX_LIST()           (FdTable.au4IfIndex)             
#define   CFA_GDD_FDLIST_LAST_INDEX()      (FdTable.u2LastIndex)            
#define   CFA_GDD_FDLIST_IFINDEX(Index)    (FdTable.au4IfIndex[Index])      
#define   CFA_GDD_FDLIST_FDSTRUCT(Index)   (FdTable.aFdList[Index])         
#define   CFA_GDD_FDLIST_FD(Index)         (FdTable.aFdList[Index].fd)      
#define   CFA_GDD_FDLIST_EVT(Index)        (FdTable.aFdList[Index].events)  
#define   CFA_GDD_FDLIST_REVT(Index)       (FdTable.aFdList[Index].revents) 

#define CFA_GDD_FDLIST_INIT(u4IfIndex, Fd) \
      { \
      CFA_GDD_FDLIST_IFINDEX(CFA_GDD_FDLIST_LAST_INDEX()) = u4IfIndex; \
      CFA_GDD_FDLIST_FD(CFA_GDD_FDLIST_LAST_INDEX()) = Fd; \
      CFA_GDD_FDLIST_EVT(CFA_GDD_FDLIST_LAST_INDEX()) = (POLLIN | POLLPRI); \
      CFA_GDD_FDLIST_REVT(CFA_GDD_FDLIST_LAST_INDEX()) = 0; \
      ++CFA_GDD_FDLIST_LAST_INDEX(); \
      }

#define CFA_GDD_FDLIST_DEINIT(Index) \
      { \
      if (Index != (CFA_GDD_FDLIST_LAST_INDEX() - 1)) { \
         CFA_GDD_FDLIST_IFINDEX(Index) = \
               CFA_GDD_FDLIST_IFINDEX(CFA_GDD_FDLIST_LAST_INDEX() - 1); \
         CFA_GDD_FDLIST_FD(Index) = \
               CFA_GDD_FDLIST_FD(CFA_GDD_FDLIST_LAST_INDEX() - 1); \
         CFA_GDD_FDLIST_EVT(Index) = POLLIN | POLLPRI; \
         CFA_GDD_FDLIST_REVT(Index) = 0; \
      } \
      --CFA_GDD_FDLIST_LAST_INDEX(); \
      }

#define CFA_GDD_IS_DATA_AVAIL(Index) \
      (CFA_GDD_FDLIST_REVT(Index) & (POLLIN | POLLPRI))

UINT4 CfaGddSendIntrEvent (VOID);

#ifdef LNXIP4_WANTED
/* ----------------- Constant & Macro Definitions ------------------ */

/* Bridge Device related Constants */

#define CFA_GDD_BRIDGE_INTF_NAME   "bridge"

#define CFA_GDD_ADD_BRIDGE         1
#define CFA_GDD_DEL_BRIDGE         2

#define CFA_GDD_ADD_BRIDGE_IF      1 
#define CFA_GDD_DEL_BRIDGE_IF      2 

#define CFA_GDD_ETHERNET_PREFIX    "eth"

#define CFA_GDD_DEV_NAME_SIZE      8

/* Number of Ports visible to the system [connected to CPU]. These interfaces 
 * are actually added to the Bridge Device for Packet RX purposes.
 * Marvell 6095 Linkstreet has 2 interfaces visible to Linux. Out of it,
 * we skip the OOB Port[eth1] while adding physical interfaces to the Bridge
 * Device so as to avoid trapping packets arriving at the OOB Interface 
 * Note : This Constant has to be definitely updated if we are interested to 
 * recieve packet via those interfaces using Netfilter Bridge hook */

#define CFA_GDD_MIN_PHY_PORTS      0    /* eth0 */
#define CFA_GDD_MAX_PHY_PORTS      0    /* excluding eth1 [refer the explanation
                                           above] */

#endif /* LNXIP4_WANTED */

#endif /* _GDDLINUX_H */ 
