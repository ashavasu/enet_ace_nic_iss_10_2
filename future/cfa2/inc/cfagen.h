/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfagen.h,v 1.38 2015/07/13 04:43:26 siva Exp $
 *
 * Description:This file contains the common definitions    
 *             for the CFA system.              
 *
 *******************************************************************/
#ifndef _CFAGEN_H
#define _CFAGEN_H

#define   CFA_ALWAYS                             1                

#define   CFA_MAX_U4_VALUE            (0xffffffff)     

/* for init of socket/file descript */
#define   CFA_INVALID_DESC            (-1)  

/* Row Status Definitions - same as the MIB variable */
#define   CFA_RS_ACTIVE               1                
#define   CFA_RS_NOTINSERVICE         2                
#define   CFA_RS_NOTREADY             3                
#define   CFA_RS_CREATEANDGO          4                
#define   CFA_RS_CREATEANDWAIT        5                
#define   CFA_RS_DESTROY              6                
#define   CFA_RS_INVALID              0   /* proprietary = NO_SUCH_INSTANCE */


/* same as the MIB variable - TruthValue */
#define   CFA_VALID                   1                
#define   CFA_INVALID                 2                
#define   CFA_IGNORE                  0   /* proprietary - not in MIB */

/* Interface Status - same as the MIB variable */
#define   CFA_IF_TEST                 3                
#define   CFA_IF_UNK                  4                
#define   CFA_IF_LLDOWN               7               
#define   CFA_IF_INVALID              0   /* proprietary - not used in MIB */
#define   CFA_IF_LINK_STATUS_CHANGE   3

/* commands used while enqueuing packets to CFA task */
#define   CFA_IP_UP                   1                
#define   IP_TO_CFA                   2                
#define   ARP_TO_CFA                  3                
#define   ATMARP_TO_CFA               7                
#define   PPP_TO_CFA_IN               4
#define   PPP_TO_CFA_OUT              5 
#define   PPPOE_PKT_OUT               9
#ifdef IP6_WANTED
#define   IP6_TO_CFA                 10                
#endif /* IP6_WANTED */
#ifdef MPLS_WANTED
#define   MPLS_TO_CFA_OUT            11
#endif
#ifdef ISIS_WANTED
#define  ISIS_TO_CFA                 12
#endif
#define   L2_TO_CFA                  15
#define   VLAN_TO_CFA                16
#define   UPDATE_IF_STATUS           18
/* commands used while enqueing the packet from WSS to CFA */
#if defined (WLC_WANTED) || defined (WTP_WANTED)
#define    WSS_TO_CFA                20
#endif
/* network interface config commands */
#define   CFA_NET_IF_NEW                  1                
#define   CFA_NET_IF_UPD                  2                
#define   CFA_NET_IF_DEL                  3                
#define   CFA_NET_IF_NIS                  4

/* enabling disabling of RTPHC */
#define   CFA_WAN_COMPR_ENABLE            1                
#define   CFA_WAN_COMPR_DISABLE           2                

/* IP V4 defines */
#define   CFA_IP_ADDR_SIZE                4                
#define   CFA_INVALID_SUBNET_MASK         0                
#define   CFA_INVALID_BCAST_ADDR          0                
#define   CFA_INVALID_IP_ADDR             0                

#define   CFA_LLC_SNAP_HEADER_SIZE        8                
#define   CFA_LLC_BPDU_SNAP_HEADER_SIZE   10               
#define   CFA_LLC_NLPID_HEADER_SIZE       4                
#define   CFA_LLC_HEADER_SIZE             3

/* For OSI Protocols, the Protocol Data Starts from
 * NLPID, Hence while giving the packet to OSI Protocols
 * only LLC Header (FE FE 03) should be stripped
 */

#define   CFA_LLC_OSI_NLPID_HEADER_SIZE   3                
#define   CFA_MAX_VPI                   255              
#define   CFA_MAX_VCI                 65535            

#define CFA_TRAP_LINK_DOWN 2
#define CFA_TRAP_LINK_UP   3

#define CFA_OBJECT_NAME_LEN             256

#define CFA_IF_CREATE_TRAP_OID "1.3.6.1.4.1.2076.27.4.0.4"
#define CFA_IF_DELETE_TRAP_OID "1.3.6.1.4.1.2076.27.4.0.5"

typedef enum
{
    CFA_IF_CREATE_TRAP=1,
    CFA_IF_DELETE_TRAP=2
} tCfaTrapVal;
#define CFA_31BIT_MASK                  31


/* Port property macros */
#define CFA_PORT_PROP_BRG_TYPE          1
#define CFA_PORT_PROP_OPER_STATUS       2

/* Macro to be used while doing string copy
*/
#define CFA_STR_DELIM_SIZE              1


/* Tag Control Identifier TCI contains 3bits priority, 
 * 1 canonical form bit and 12 bits Vlan ID 
 * Mask used for segregation of VLAN ID from TCI field 
 */

#define CFA_VLAN_VID_MASK                0x0fff

/*CLI*/
#define CFA_AUDIT_SHOW_CMD    "show interfaces > "
#define CFA_AUDIT_FILE_ACTIVE    "/tmp/cfa_output_file_active"
#define CFA_AUDIT_FILE_STDBY    "/tmp/cfa_output_file_stdby"
#define CFA_CLI_EOF                  2
#define CFA_CLI_NO_EOF                 1
#define CFA_CLI_RDONLY               OSIX_FILE_RO
#define CFA_CLI_WRONLY               OSIX_FILE_WO
#define CFA_CLI_MAX_GROUPS_LINE_LEN  200
#endif /* _CFAGEN_H */
