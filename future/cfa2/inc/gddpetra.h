/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddpetra.h,v 1.1 2011/06/21 09:48:46 siva Exp $
 *
 * Description: This file contains header includes for gddpetra.c.
 ****************************************************************************/
#ifndef __GDDPETRA_H__
#define __GDDPETRA_H__

PUBLIC INT4 CfaGddEthSockOpen (UINT4 u4IfIndex);
PUBLIC INT4 CfaGddEthSockClose (UINT4 u4IfIndex);
PUBLIC INT4 CfaGddEthSockRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize);

#endif /* __GDDPETRA_H__ */
