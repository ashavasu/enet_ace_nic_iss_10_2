/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: cfatnlmgr.h,v 1.13 2015/10/26 14:26:12 siva Exp $
*
* Description: Tunnel manager related macros and data structures. 
*********************************************************************/
#ifndef __CFATNLMGR_H__
#define __CFATNLMGR_H__ 

/* Used to add new entries to TnlIfTbl in sorted order.  
 * TnlIfTbl indices - 4B(AddrType)+16B(TnlSrcIp)+16B(TnlDestIp)+
*                     4B(EncapsMethod)+4B(ConfigId) = 44Bytes */
#define SIZE_OF_TNL_TBL_INDICES 44

#define TNL_GRE_PROTO           0x2F
#define TNL_IPV6IP_PROTO        0x29

#define IP6ADDR_SIZE             sizeof(tIp6Addr)
#define MIN_GRE_HDR_LEN          4  /* Default hdr length in bytes */ 
#define MAX_GRE_HDR_LEN          8  /* full hdr len if cksum present in bytes */
#define CFA_MAX_TUNL_CONTEXT     FsCFASizingParams[MAX_CFA_TUNL_CONTEXT_SIZING_ID].u4PreAllocatedUnits
#define CFA_DEF_TUNL_CONTEXT     VCM_DEFAULT_CONTEXT 

#if defined (LNXIP4_WANTED) && defined (IP6_WANTED)
extern INT4 gi4RawSockId;
extern INT4 gi4GreRawSockId;
#endif

typedef struct _CfaTnlCxt
{
    tTMO_SLL            TnlIfSLL;  
    UINT4               u4ContextId;       
    UINT1               u1TunlIfExists;
                        /* var used to register with tunnel manager for the very first creation
                         * of tunnel entry */
    UINT1               au1Align[3];
}
tCfaTnlCxt;

typedef struct _TnlGblInfo
{
    tCfaTnlCxt  *apCfaTnlCxt[SYS_DEF_MAX_NUM_CONTEXTS];    
    tCfaTnlCxt  *pTunlGblCxt;            
    tMemPoolId  CfaTnlCxtMemPoolId;
                 /* MemPool for Max Tunnel Context */
    tMemPoolId  CfaTnlMemPoolId;
                 /* MemPool for TunlTbl */
}
tCfaTnlGblInfo;

/* GRE header */
typedef struct
{
    UINT2 u2CkFlgAndVer;
    UINT2 u2ProtoType;
    UINT2 u2Checksum;
    UINT2 u2Reserved1;   

/* Bit 0 - set to 1 if cksum is present */    
#define GRE_SET_CKSUM_FLAG  0x8000

/* Bits 1-12 - Reserved0 
 * Bits1 to 5 should be zero, Bits6 -12 reserved for future use */
#define GRE_RESERVED0_MASK   0x7FF8

/* Bits 13-15 - MUST be zero */
#define GRE_VER_NUM_MAS_MASKK     0x0007

#define GRE_PROTO_TYPE_OFFSET     2
#define GRE_CKSUM_OFFSET          4
}tTnlGREHdr;

/* fstunl.mib objects range check macros */
#define  MIN_TUNL_IF_HOP_LIMIT  0
#define  MAX_TUNL_IF_HOP_LIMIT  255
#define  MIN_TUNL_IF_TOS        -2
#define  MAX_TUNL_IF_TOS        63
#define  MIN_TUNL_IF_FLOWLABEL  -1
#define  MAX_TUNL_IF_FLOWLABEL  1048575
#define  MIN_TUNL_IF_ALIAS_LEN  0
#define  MAX_TUNL_IF_ALIAS_LEN  64

/* Sets the socket receive in non blocking mode */
#define CFA_TNL_RAWSOCK_NON_BLOCK     16


/* Function prototypes */
INT4 CfaSnmpGetTnlEntryInCxt PROTO ((tCfaTnlCxt *pTunlCxt, INT4 i4FsTunlIfAddressType, 
               tSNMP_OCTET_STRING_TYPE *pFsTunlIfLocalInetAddress, 
     tSNMP_OCTET_STRING_TYPE *pFsTunlIfRemoteInetAddress, 
     INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
     tTnlIfNode *pTnlIfNode));

INT4 CfaGetTnlInCxt PROTO ((tCfaTnlCxt * pTunlCxt, UINT4 u4AddrType, UINT4 u4SrcIpAddr,
        UINT4 u4DestIpAddr, UINT1 *pu1SrcIp6Addr, UINT1 *pu1DestIp6Addr,
        UINT4 u4EncapsMethod, UINT4 u4ConfigId,
        tTnlIfNode *pTnlIfNode));

INT4 CfaSnmpDeleteTnlEntryInCxt PROTO ((tCfaTnlCxt *pTunlCxt, INT4 i4FsTunlIfAddressType, 
     tSNMP_OCTET_STRING_TYPE *pFsTunlIfLocalInetAddress, 
     tSNMP_OCTET_STRING_TYPE *pFsTunlIfRemoteInetAddress, 
     INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID));

INT4 CfaTnlMgrRegWithIp4InCxt PROTO ((UINT4));

VOID CfaTnlMgrRxFrame PROTO ((tIP_BUF_CHAIN_HEADER *pBuf, UINT2 u2Len, 
                  UINT4 u4IfIndex, tIP_INTERFACE IfaceId, UINT1 u1Flag));

VOID CfaTnlMgrRtChgHdlr PROTO ((tNetIpv4RtInfo * pNetIpv4RtInfo, tNetIpv4RtInfo * pNetIpv4RtInfo1, UINT1 u1CmdType));

INT4 CfaTnlFindEntryInCxt PROTO ((UINT4 u4ContextId,
                 UINT4 u4AddrType, tTnlIpAddr *pTnlSrcAddr,
               tTnlIpAddr *pTnlDestAddr, UINT1 u1Direction, 
                             tTnlIfEntry *pTnlIfEntry));

INT4 CfaTnlEnqPktToIp PROTO ((tTnlPktTxInfo *pTnlPktInfo));

INT4 CfaDeleteTnlIfEntryInCxt PROTO ((tCfaTnlCxt *pTunlCxt,
                                      UINT4 u4IfIndex));

INT4 CfaSnmpGetMatchingEntryInCxt PROTO ((tCfaTnlCxt *pTunlCxt, 
                                     UINT4 u4EncapsMethod, 
                          tSNMP_OCTET_STRING_TYPE *pTunlIfLocalAddress,
     tTnlIfNode *pTnlIfNode));

INT4 CfaTnlMgrPmtuRegWithV4 PROTO ((tTnlIfNode *pTnlIfEntry));

INT4 CfaTnlMgrPmtuDeregWithV4 PROTO ((tTnlIfNode *pTnlIfEntry));

VOID CfaTnlMgrNotifyIpv4PMtuChgInCxt PROTO ((UINT4 u4CxtId, INT4 i4SdIndex, UINT2 u2Pmtu));

INT4 CfaFindTnlEntryFromIfIndex PROTO ((UINT4 u4IfIndex, tTnlIfEntry *pTnlIfEntry));

INT4 CfaFindTnlEntryFromContextId PROTO ((UINT4 u4CxtId, UINT4 u4Index,
                                          tTnlIfEntry *pTnlIfEntry));

VOID CfaTnlCopyEntry PROTO ((tTnlIfEntry *pDestTnl, tTnlIfNode *pSrcTnl));

INT4 CfaTnlPhyIfOperStChg PROTO ((tCfaRegInfo * pCfaInfo));
INT4 CfaTnlPhyIfUpdate    PROTO ((tCfaRegInfo * pCfaInfo));
INT4 CfaTnlPhyIfDelete    PROTO ((tCfaRegInfo * pCfaInfo));
INT4 CfaTnlMgrRegisterLL  PROTO ((VOID));


INT4 CfaTnlGetFirstEntryInCxt  PROTO ((tCfaTnlCxt *pTunlCxt, INT4 *pi4FsTunlIfAddressType,
                          tSNMP_OCTET_STRING_TYPE *pFsTunlIfLocalInetAddress,
                          tSNMP_OCTET_STRING_TYPE *pFsTunlIfRemoteInetAddress,
                          INT4 *pi4FsTunlIfEncapsMethod, 
                          INT4 *pi4FsTunlIfConfigID));

INT4  CfaTnlGetNextEntryInCxt PROTO ((tCfaTnlCxt *pTunlCxt, INT4 i4FsTunlIfAddressType,
                          INT4 *pi4NextFsTunlIfAddressType,
                          tSNMP_OCTET_STRING_TYPE *pFsTunlIfLocalInetAddress,
                         tSNMP_OCTET_STRING_TYPE *pNextFsTunlIfLocalInetAddress,
                          tSNMP_OCTET_STRING_TYPE *pFsTunlIfRemoteInetAddress,
                        tSNMP_OCTET_STRING_TYPE *pNextFsTunlIfRemoteInetAddress,
                          INT4 i4FsTunlIfEncapsMethod,
           INT4 *pi4NextFsTunlIfEncapsMethod,
                          INT4 i4FsTunlIfConfigID,
                         INT4 *pi4NextFsTunlIfConfigID));

INT4 CfaSnmpGetMatchingAutoTnlEntryInCxt PROTO ((tCfaTnlCxt *pTunlCxt, 
              UINT4 u4EncapsMethod, 
              tSNMP_OCTET_STRING_TYPE * pTunlIfLocalAddress, 
              tTnlIfNode *pTnlIfNode));

tTnlIfNode *CfaTnlCheckSourceAddrInCxt PROTO ((tCfaTnlCxt *pTunlCxt,
                   tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress));

VOID CfaSetIfTnlEntry PROTO ((UINT4 u4Index, tTnlIfNode *pTnlIfNode));
INT4 CfaSetTnlIfIndexInCxt PROTO ((tCfaTnlCxt *pTunlCxt, INT4 i4FsTunlIfAddressType,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                      INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
        UINT4 u4Index));

INT4 CfaSetTnlIfAliasInCxt PROTO ((tCfaTnlCxt *pTunlCxt, INT4 i4FsTunlIfAddressType,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                      INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                     tSNMP_OCTET_STRING_TYPE * pSetValFsTunlIfAlias));
  
INT4 CfaSetTnlIfHopLimitInCxt PROTO ((tCfaTnlCxt *pTunlCxt, INT4 i4FsTunlIfAddressType,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                      INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                      INT4 i4SetValFsTunlIfHopLimit));
  
INT4 CfaSetTnlIfTOSInCxt PROTO ((tCfaTnlCxt *pTunlCxt, INT4 i4FsTunlIfAddressType,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                      INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                      INT4 i4SetValFsTunlIfTOS));
  
INT4 CfaSetTnlIfFlowLabelInCxt PROTO ((tCfaTnlCxt *pTunlCxt, INT4 i4FsTunlIfAddressType,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                      INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                      INT4 i4SetValFsTunlIfFlowLabel));
  
INT4 CfaSetTnlIfDirFlagInCxt PROTO ((tCfaTnlCxt *pTunlCxt, INT4 i4FsTunlIfAddressType,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                      INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                      INT4 i4SetValFsTunlIfDirFlag));
  
INT4 CfaSetTnlIfDirectionInCxt PROTO ((tCfaTnlCxt *pTunlCxt, INT4 i4FsTunlIfAddressType,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                      INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                      INT4 i4SetValFsTunlIfDirection));
  
INT4 CfaSetTnlIfEncapLimitInCxt PROTO ((tCfaTnlCxt *pTunlCxt, INT4 i4FsTunlIfAddressType,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                      INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                      UINT4 u4SetValFsTunlIfEncaplmt));
  
INT4 CfaSetTnlIfEncapOptionInCxt PROTO ((tCfaTnlCxt *pTunlCxt, INT4 i4FsTunlIfAddressType,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                      INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                      INT4 i4SetValFsTunlIfEncapOption));
  
INT4 CfaSetTnlIfCksumFlagInCxt PROTO ((tCfaTnlCxt *pTunlCxt, INT4 i4FsTunlIfAddressType,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                      INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                      INT4 i4SetValFsTunlIfCksumFlag));
  
INT4 CfaSetTnlIfPmtuFlagInCxt PROTO ((tCfaTnlCxt *pTunlCxt, INT4 i4FsTunlIfAddressType,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                      INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID,
                      INT4 i4SetValFsTunlIfPmtuFlag));
  
INT4 CfaSetTnlIfActiveStatusInCxt PROTO ((tCfaTnlCxt *pTunlCxt, INT4 i4FsTunlIfAddressType,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                      INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID));
  
INT4 CfaSetTnlIfNotInServiceStatusInCxt PROTO ((tCfaTnlCxt *pTunlCxt, INT4 i4FsTunlIfAddressType,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfLocalInetAddress,
                      tSNMP_OCTET_STRING_TYPE * pFsTunlIfRemoteInetAddress,
                      INT4 i4FsTunlIfEncapsMethod, INT4 i4FsTunlIfConfigID));
  
tTnlIfNode *
CfaGetTnlNodeInCxt PROTO ((tCfaTnlCxt *pTunlCxt, UINT4 u4AddrType, UINT4 u4SrcIpAddr,
           UINT4 u4DestIpAddr, UINT1 *pu1SrcIp6Addr, UINT1 *pu1DestIp6Addr,
           UINT4 u4EncapsMethod, UINT4 u4ConfigId));

tTnlIfNode *CfaGetIfTnlEntry PROTO ((UINT4 u4Index));

INT4  CfaTnlUtilIsValidCxtId PROTO ((UINT4 u4TunlIfConfigID));


tCfaTnlCxt  * CfaUtilGetCxtEntryFromCxtId PROTO ((UINT4 u4ContextId));

INT4 CfaTnlUtilGetFirstCxtId PROTO ((UINT4 * pu4FirstCxtId));

INT4 CfaTnlUtilGetNextCxtId PROTO ((UINT4 u4ContextId, UINT4 * pu4NextCxtId));

VOID CfaVcmCallbackFn PROTO ((UINT4 u4IfIndex, 
                             UINT4 u4ContextId, UINT1 u1Event));
VOID CfaTunlCreateContext PROTO ((UINT4 u4ContextId));
VOID CfaTunlDeleteContext PROTO ((UINT4 u4ContextId));

#if defined (LNXIP4_WANTED) && defined (IP6_WANTED)
INT4 CfaTnlCreateRawSock PROTO ((VOID));

VOID CfaTnlCloseRawSock PROTO ((VOID));

INT4 CfaSetTnlRawSockOptions PROTO ((VOID));

INT4 CfaTnlEnqPktToLinuxIp PROTO ((tTnlPktTxInfo * pTnlPktInfo, 
                            UINT4 u4EncapsMethod));

VOID IpifRecvTnlPkt PROTO ((VOID));

VOID IpifRecvGreTnlPkt PROTO ((VOID));

VOID CfaNotifyTnlTask PROTO ((INT4 i4SockFd));

VOID CfaNotifyGreTnlTask PROTO ((INT4 i4SockFd));
#endif
tCfaTnlCxt * CfaTnlGetCxt PROTO ((UINT4 u4ContextId));

#endif /* __CFATNLMGR_H__ */
