
/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: cfapktreg.h,v 1.1
 *
 * Description:This file contains the prototypes for routines used by
 *              protocols to register with cfa for periodic packet tx
 *             
 *
 *******************************************************************/
#ifndef _CFAPKTREG_H
#define _CFAPKTREG_H
#define   CFA_PKT_TASK_ID           gCfaPktTaskId
#define   CFA_PACKET_START_EVENT    0x00400000
#define   CFA_PROTOCOL_REG_EVENT    0x00200000
#define   CFA_PKT_TASK_NAME         "PKTTX" 
#define   CFA_AST_PKTTX_TASK_NAME   "CFATX"
INT4 CfaInitPeriodicPktTxRegnTbl PROTO ((VOID));
INT4 CfaDeInitPeriodicPktTxRegnTbl PROTO ((VOID));
#endif
