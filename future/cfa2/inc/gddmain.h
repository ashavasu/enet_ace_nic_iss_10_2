/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddmain.h,v 1.14 2016/07/23 11:41:25 siva Exp $
 *
 * Description:This file contains the definitions of the   
 *             global structure of the Generic Device    
 *             Driver Module of the CFA. It also contains   
 *             Macros for accessing the GDD Registration    
 *             Table and definitions pertaining to GDD.     
 *
 *******************************************************************/
#ifndef _GDDMAIN_H
#define _GDDMAIN_H

/* device driver types */
#define   CFA_ETHERNET        6    /* same as CFA_ENET */
#define   CFA_WANIC         250
#define   CFA_WANPIPE       251
#define   CFA_ETINC         252


/* defines for Async support */
#define   FLAG_SEQ         0x7e
#define   CTRL_ESC         0x7d
#define   FLAG_PHASE          0
#define   DATA_PHASE          1
#define   END_OF_PACKET    0xff

/* for Wanpipe driver */
#define   HDLC             0x16

/* for Etinc Driver */
# define LINKUP_TIME_OUT 5

/* Macros for the GDD Structure */
#define CFA_GDD_ENTRY_USED(u4IfIndex) \
             (CFA_GDD_ENTRY(u4IfIndex) != NULL)

#define CFA_GDD_TYPE(u4IfIndex) \
             (CFA_GDD_ENTRY(u4IfIndex)->u1DevType)

#define CFA_GDD_PORT(u4IfIndex) \
        (CFA_GDD_ENTRY(u4IfIndex)->u1PortNum)

#define CFA_GDD_REGSTAT(u4IfIndex) \
        (CFA_GDD_ENTRY(u4IfIndex)->u1PhysIfRegValidity)

#define CFA_GDD_HIGHTYPE(u4IfIndex) \
        (CFA_GDD_ENTRY(u4IfIndex)->u1HigherIfType)

#define CFA_GDD_PORT_DESC(u4IfIndex) \
        (CFA_GDD_ENTRY(u4IfIndex)->i4PortDescriptor)

#define CFA_GDD_PORT_NAME(u4IfIndex) \
        (CFA_GDD_ENTRY(u4IfIndex)->au1PortName)

#define CFA_GDD_FLAG(u4IfIndex) \
        (CFA_GDD_ENTRY(u4IfIndex)->u1Flag)

#define CFA_GDD_WRITE_FNPTR(u4IfIndex) \
        (CFA_GDD_ENTRY(u4IfIndex)->fn_write)

#define CFA_GDD_WRITE_FNPTR_WITH_PRIO(u4IfIndex) \
       (CFA_GDD_ENTRY(u4IfIndex)->fn_pm_write)

#define CFA_GDD_READ_FNPTR(u4IfIndex) \
        (CFA_GDD_ENTRY(u4IfIndex)->fn_read)

#define CFA_GDD_OPEN_FNPTR(u4IfIndex) \
        (CFA_GDD_ENTRY(u4IfIndex)->fn_open)

#define CFA_GDD_CLOSE_FNPTR(u4IfIndex) \
        (CFA_GDD_ENTRY(u4IfIndex)->fn_close)

#define CFA_GDD_HL_RX_FNPTR(u4IfIndex) \
        (CFA_GDD_ENTRY(u4IfIndex)->fn_hl_rx)

#define CFA_GDD_ASYNC_PARAMS(u4IfIndex) \
        (CFA_GDD_ENTRY(u4IfIndex)->AsyncParams)


#endif /* _GDDMAIN_H */
