/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: l2dsutil.c,v 1.18 2014/01/07 10:36:36 siva Exp $       */
/*****************************************************************************/
/*    FILE  NAME            : l2dsutil.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : L2DHCP Snooping                                */
/*    MODULE NAME           : L2DHCP Snooping Util Module                    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the utility funtions        */
/*                            for L2DHCP Snooping module                     */
/*---------------------------------------------------------------------------*/

#include "l2dsinc.h"
#include "l2dsextn.h"

/*****************************************************************************/
/* Function Name      : L2dsUtilGetOption                                    */
/*                                                                           */
/* Description        : This routine is to get the specified option from     */
/*                      the DHCP Packet                                      */
/*                                                                           */
/* Input(s)           : pL2DhcpSnpPacket- Incoming DHCP packet               */
/*                      u1OptionCode - Option to be obtained                 */
/*                                                                           */
/* Output(s)          : pL2DhcpSnpOption - Option                            */
/*                                                                           */
/* Return Value(s)    : L2DS_FOUND/L2DS_NOT_FOUND                            */
/*****************************************************************************/
INT4
L2dsUtilGetOption (tL2DhcpSnpPacket * pL2DhcpSnpPacket,
                   tL2DhcpSnpOption * pL2DhcpSnpOption, UINT1 u1OptionCode)
{
    UINT1              *pu1Options = NULL;
    UINT2               u2OptLen = L2DS_ZERO;
    UINT2               u2Offset = L2DS_ZERO;
    UINT1               u1Type = L2DS_ZERO;
    UINT1               u1Len = L2DS_ZERO;
    UINT1               u1OptOverLoad = L2DS_ZERO;

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_ENTRY, L2DS_MODULE_NAME,
                   "L2dsUtilGetOption:Entering the function %s \r\n",
                   __FUNCTION__);

    L2DS_TRC_ARG2 (L2DS_TRC_FLAG, L2DS_FN_ARGS, L2DS_MODULE_NAME,
                   "L2dsUtilGetOption:The arguments to the function %s is "
                   "option code %d\r\n", __FUNCTION__, u1OptionCode);

    MEMSET (pL2DhcpSnpOption, L2DS_ZERO, sizeof (tL2DhcpSnpOption));

    u2OptLen = pL2DhcpSnpPacket->u2OptLen;
    pu1Options = pL2DhcpSnpPacket->au1Options;

    /* Loop through the option */
    while (u2OptLen > u2Offset)
    {
        u1Type = pu1Options[u2Offset];
        u1Len = pu1Options[u2Offset + L2DS_ONE];

        if (u1Type == u1OptionCode)
        {
            /* If the particular option is found, return the value */
            pL2DhcpSnpOption->u1Type = u1Type;
            pL2DhcpSnpOption->u1Len = u1Len;

            pL2DhcpSnpOption->pu1Val = (UINT1 *) &gau1OptionVal[L2DS_ZERO];

            if (pL2DhcpSnpOption->pu1Val != NULL)
            {
                MEMSET (pL2DhcpSnpOption->pu1Val, L2DS_ZERO, u1Len);

                MEMCPY (pL2DhcpSnpOption->pu1Val,
                        (pu1Options + u2Offset + L2DS_OPTION_VAL_OFFSET),
                        u1Len);

                L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT, L2DS_MODULE_NAME,
                               "L2dsUtilGetOption:Exiting the function %s"
                               "after finding the option \r\n", __FUNCTION__);
                return L2DS_FOUND;
            }
            else
            {
                break;
            }
        }
        else if (u1Type == L2DS_OPT_PAD)
        {
            /* If the option is PAD option(It does not have type and value), 
             * continue */
            u2Offset++;
            continue;
        }
        else if (u1Type == L2DS_OPT_END)
        {
            /* If the option is END option, break the loop */
            break;
        }
        else if (u1Type == L2DS_OPT_OVERLOAD)
        {
            /* If option overloading is there, SNAME and FILE fields will be 
             * used for carrying the options */
            u1OptOverLoad = pu1Options[u2Offset + L2DS_OPTION_VAL_OFFSET];
        }

        u2Offset = (UINT2) (u2Offset + u1Len + L2DS_OPTION_VAL_OFFSET);
    }

    /* If Option overload value is 1, FILE field is used for carrying options
     * If Option overload value is 2, SNAME field is used for carrying options
     * If Option overload value is 3, both FILE and SNAME fields are used for 
     * carrying options */
    if ((u1OptOverLoad == L2DS_ZERO) || (u1OptOverLoad > L2DS_THREE))
    {
        L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT, L2DS_MODULE_NAME,
                       "L2dsUtilGetOption:Exiting the function %s after failing"
                       " to find the option\r\n", __FUNCTION__);
        return L2DS_NOT_FOUND;
    }

    if (u1OptOverLoad == L2DS_ONE)
    {
        u2OptLen = L2DS_FILE_LEN;
        pu1Options = pL2DhcpSnpPacket->u1File;
    }
    else if (u1OptOverLoad == L2DS_TWO)
    {
        u2OptLen = L2DS_SNAME_LEN;
        pu1Options = pL2DhcpSnpPacket->u1Sname;
    }
    else
    {
        u2OptLen = L2DS_FILE_LEN + L2DS_SNAME_LEN;
        pu1Options = pL2DhcpSnpPacket->u1Sname;
    }

    u2Offset = L2DS_ZERO;

    while (u2OptLen > (u2Offset + L2DS_OPTION_VAL_OFFSET))
    {
        u1Type = pu1Options[u2Offset];
        u1Len = pu1Options[u2Offset + L2DS_ONE];

        if (u1Type == u1OptionCode)
        {
            pL2DhcpSnpOption->u1Type = u1OptionCode;
            pL2DhcpSnpOption->u1Len = u1Len;

            pL2DhcpSnpOption->pu1Val = (UINT1 *) &gau1OptionVal[L2DS_ZERO];

            if (pL2DhcpSnpOption->pu1Val != NULL)
            {
                MEMSET (pL2DhcpSnpOption->pu1Val, L2DS_ZERO, u1Len);

                MEMCPY (pL2DhcpSnpOption->pu1Val,
                        (pu1Options + u2Offset + L2DS_OPTION_VAL_OFFSET),
                        u1Len);

                L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT, L2DS_MODULE_NAME,
                               "L2dsUtilGetOption:Exiting the function %s "
                               "after finding the option \r\n", __FUNCTION__);

                return L2DS_FOUND;
            }
            else
            {
                break;
            }
        }
        else if (u1Type == L2DS_OPT_PAD)
        {
            u2Offset++;
            continue;
        }
        else if (u1Type == L2DS_OPT_END)
        {
            /* If option overload value is 3, and the options in SNAME field 
             * have come to an end, scan through the FILE field */
            if (u1OptOverLoad == L2DS_THREE)
            {
                u2Offset = L2DS_ZERO;
                if (u2OptLen == L2DS_FILE_LEN)
                {
                    break;
                }
                u2OptLen = L2DS_FILE_LEN;
                pu1Options = pL2DhcpSnpPacket->u1File;
                continue;
            }
            else
            {
                break;
            }
        }
        u2Offset = (UINT2) (u2Offset + u1Len + L2DS_OPTION_VAL_OFFSET);
    }

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT, L2DS_MODULE_NAME,
                   "L2dsUtilGetOption:Exiting the function %s after failing to "
                   "find the option\r\n", __FUNCTION__);

    return L2DS_NOT_FOUND;
}

/*****************************************************************************/
/* Function Name      : L2dsUtilGetDslOptions                                */
/*                                                                           */
/* Description        : This routine is to get the DSL options for the       */
/*                      DHCP Packet                                          */
/*                                                                           */
/* Input(s)           : u2Port - Incoming Port                               */
/*                                                                           */
/* Output(s)          : pu1VndrOption - DSL Options                          */
/*                      pu1VndrLen - Length of DSL Option                    */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsUtilGetDslOptions (UINT1 *pu1VndrOption, UINT1 *pu1VndrLen, UINT4 u4Port,
                       tVlanTag VlanTag)
{
    UINT1               au1DslOptions[L2DS_DSL_OPTION_LEN] = { L2DS_ZERO };
    UINT1               u1DslLen = L2DS_ZERO;
    /* Get the DSL Sub-Options to be added */
    if (L2dsPortGetDslOptions (au1DslOptions, &u1DslLen, u4Port,
                               VlanTag) != L2DS_SUCCESS)
    {
        return L2DS_FAILURE;
    }

    /* Copy the Type and length of vendor specific option */
    pu1VndrOption[L2DS_ZERO] = L2DS_OPT_VENDOR_SPEC;
    pu1VndrOption[L2DS_ONE] = u1DslLen;

    /* Copy DSL Sub-option to vendor specific option */
    MEMCPY (pu1VndrOption + L2DS_TWO, au1DslOptions, u1DslLen);

    *pu1VndrLen = (UINT1) (u1DslLen + L2DS_TWO);
    return L2DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2dsUtilAddOptions                                   */
/*                                                                           */
/* Description        : This routine is to add option-82 and DSL options     */
/*                      to DHCP Packet                                       */
/*                                                                           */
/* Input(s)           : pu1LinearBuf - The incoming packet                   */
/*                      pL2DhcpSnpPktInfo - DHCP packet                      */
/*                                                                           */
/* Output(s)          : pu2AddedLength - No of additional bytes              */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsUtilAddOptions (tL2DhcpSnpPktInfo * pL2DhcpSnpPktInfo,
                    UINT2 *pu2AddedLength, tVlanTag VlanTag)
{
    UINT1              *pu1Options = NULL;
    UINT1               au1CircuitId[DCS_CIRCUIT_ID_LEN] = { L2DS_ZERO };
    UINT1               au1RemoteId[DCS_REMOTE_ID_LEN + L2DS_ONE]
        = { L2DS_ZERO };
    UINT1               au1VndrOptions[L2DS_VENDOR_OPTION_LEN] = { L2DS_ZERO };
    UINT2               u2TempOffset = L2DS_ZERO;
    UINT1               u1CircuitIDLen = L2DS_ZERO;
    UINT1               u1RemoteIDLen = L2DS_ZERO;
    UINT1               u1VndrLen = L2DS_ZERO;
    UINT1               u1IfType = L2DS_ZERO;
    tVlanId             VlanId = L2DS_ZERO;
    UINT1               u1Status = L2DS_ZERO;
    UINT2               u2OptLen = L2DS_ZERO;
    UINT2               u2Offset = L2DS_ZERO;
    UINT1               u1Type = L2DS_ZERO;
    UINT1               u1Len = L2DS_ZERO;

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_ENTRY, L2DS_MODULE_NAME,
                   "L2dsUtilAddOptions:Entering the function %s \r\n",
                   __FUNCTION__);

    u2OptLen = (pL2DhcpSnpPktInfo->L2DhcpSnpPacket).u2OptLen;

    pu1Options = (pL2DhcpSnpPktInfo->L2DhcpSnpPacket).au1Options;
    VlanId = VlanTag.InnerVlanTag.u2VlanId;

    if (VlanId == L2DS_ZERO)
    {
        /* Failed to get the C-VLAN ID from the packet. So get the default 
         * PVID configured for the port */
        /* TBD: This function is to be added */
        if (L2dsPortGetDefaultCTag (pL2DhcpSnpPktInfo->u4PortNumber,
                                    &VlanId) == L2DS_FAILURE)
        {
            return L2DS_FAILURE;
        }
    }

    /* Parse till the end of options */
    while (u2OptLen > u2Offset)
    {
        u1Type = pu1Options[u2Offset];

        if (u1Type == L2DS_OPT_PAD)
        {
            u2Offset++;
            continue;
        }
        else if (u1Type == L2DS_OPT_END)
        {
            break;
        }
        u1Len = pu1Options[u2Offset + L2DS_ONE];
        u2Offset = (UINT2) (u2Offset + u1Len + L2DS_OPTION_VAL_OFFSET);
    }
    /* Now the u2Offset will point to the type of end option.
     * The end option will be overwritten, and will again be 
     * added at the end */

    pu1Options[u2Offset] = L2DS_OPT_RAI;
    u2Offset++;

    /* Fill the length of the option later,after getting 
     * circuitID and RemoteID */
    u2TempOffset = u2Offset;

    /*Moving the offset to point to circuit id */
    u2Offset++;

    /* calling a function to get circuit id string */
    if (DcsUtilGetFreeStringACI (pL2DhcpSnpPktInfo->u4PortNumber,
                                 au1CircuitId, &u1CircuitIDLen) == DCS_FAILURE)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsUtilAddOptions:Getting circuit id failed.\r\n");

        return L2DS_FAILURE;
    }

    /* If length is equal to zero then we are going to add the automatic
     * generated string to the Agent circuit Identifier string as 
     * circuit id (sub-option 1(0x01))in incoming DHCP packets.*/
    if (u1CircuitIDLen == L2DS_ZERO)
    {
        if (DcsUtilGetCircuitIdString (pL2DhcpSnpPktInfo->u4PortNumber,
                                       VlanId, au1CircuitId,
                                       &u1CircuitIDLen) == DCS_FAILURE)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsUtilAddOptions:Getting circuit id failed.\r\n");
            return L2DS_FAILURE;
        }
    }
    else
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsUtilAddOptions:The Free String configured "
                  "is added.\r\n");
    }

    /* Calling a function to get Remote Agent ID Status. */
    if (DcsGetRemoteAgentIdStatus (pL2DhcpSnpPktInfo->u4PortNumber, &u1Status)
        == DCS_SUCCESS)
    {
        /* If the status is enabled then we are going to add configured Remote"
           Agent Identifier srting as a remote id (sub-option 2 (0x02) in "
           "incoming DHCP packets. */
        if (u1Status == DCS_ENABLE)
        {
            /* calling a function to get Remote id String */
            if (DcsUtilGetRemoteIdString
                (au1RemoteId, &u1RemoteIDLen) == DCS_FAILURE)
            {
                L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                          "L2dsUtilAddOptions: Unable to get Remote ID "
                          "string.\r\n");
                return L2DS_FAILURE;
            }
        }
        else
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsUtilAddOptions:Remote ID status is not enabled in "
                      " this port.\r\n");
        }
    }

    /* Addition of options should not make the packet size greater than MTU */
    if (u2OptLen + u1CircuitIDLen + u1RemoteIDLen + L2DS_OPTION_VAL_OFFSET >
        L2DS_OPTION_LEN)
    {
        L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT, L2DS_MODULE_NAME,
                       "L2dsUtilAddOptions:Exiting the function %s \r\n",
                       __FUNCTION__);

        return L2DS_SUCCESS;
    }

    /* Appending the circuit Id before L2DS_OPT_PAD */
    /* u2Offset is pointing after option type and option length */
    /* Circuit Id consists of suboption, total circuitId length and
     * access node string */

    MEMCPY ((pu1Options + u2Offset), au1CircuitId, (u1CircuitIDLen));

    /* Moving the offset to point to end of circuit id */
    u2Offset = (UINT2) (u2Offset + u1CircuitIDLen);

    /* Appending the remote Id after circuit Id and before L2DS_OPT_PAD */
    MEMCPY ((pu1Options + u2Offset), au1RemoteId, u1RemoteIDLen);
    u2Offset = (UINT2) (u2Offset + u1RemoteIDLen);

    /*Appending the length of option-82, that is (circuit id + remoteid) */
    pu1Options[u2TempOffset] = (UINT1) (u1RemoteIDLen + u1CircuitIDLen);
    pu1Options[u2Offset] = L2DS_OPT_END;

    /* total length of option-82 (Type + Length + CircuitId + Remote Id) */
    *pu2AddedLength =
        (UINT2) (u1CircuitIDLen + u1RemoteIDLen + L2DS_OPTION_VAL_OFFSET);

    /* update the Length in DHCP Packet */
    (pL2DhcpSnpPktInfo->L2DhcpSnpPacket).u2OptLen +=
        (u1CircuitIDLen + u1RemoteIDLen + L2DS_OPTION_VAL_OFFSET);

    /* Assumption: Option 43 with vendor enterprise number as 
     * DSL forum enterprise number will not be generated by any interface 
     * for which IP address needs to be allocated from DHCP server. 
     *
     * So there is no need to check whether option 43 is present 
     * there already in the packet from host. 
     *
     * Note: There is a possibility of having many option 43 in the
     * same DHCP discover/request packet from host with different
     * vendor specific number. 
     * */

    /* Get the type of the incoming port.If the port-type is DSL, we will add 
     * option-82 which contains agent circuit identifier and agent remote 
     * identifier and DSL line characteristics. */

    /* If the port-type is Ethernet, we will add only option-82 which contains 
     * agent circuit identifier and agent remote identifier */
    CfaGetIfaceType (pL2DhcpSnpPktInfo->u4PortNumber, &u1IfType);
    if ((u1IfType == CFA_XDSL2) || (u1IfType == CFA_PVC))
    {
        /* Calling a function to get Access Loop Characteristics Status. */
        if (DcsGetAccessLoopStatus (pL2DhcpSnpPktInfo->u4PortNumber, &u1Status)
            == DCS_SUCCESS)
        {
            /* If the status is enabled then we are going to add configured 
             * Access Loop Characteristics in incoming DHCP packets. */
            if (u1Status == DCS_ENABLE)
            {
                /* Get the DSL Options */
                if (L2dsUtilGetDslOptions (au1VndrOptions, &u1VndrLen,
                                           pL2DhcpSnpPktInfo->u4PortNumber,
                                           VlanTag) == L2DS_SUCCESS)
                {
                    if ((u2OptLen + u1CircuitIDLen + u1RemoteIDLen +
                         L2DS_OPTION_VAL_OFFSET + u1VndrLen) < L2DS_OPTION_LEN)
                    {
                        /* If the packetsize does not exceed maximum MTU, copy 
                         * the DSL Options to the existing options */
                        MEMCPY ((pu1Options + u2Offset), au1VndrOptions,
                                u1VndrLen);
                        u2Offset = (UINT2) (u2Offset + u1VndrLen);
                        pu1Options[u2Offset] = L2DS_OPT_END;
                        *pu2AddedLength += u1VndrLen;
                        (pL2DhcpSnpPktInfo->L2DhcpSnpPacket).u2OptLen +=
                            u1VndrLen;
                        return L2DS_SUCCESS;
                    }
                    else
                    {
                        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC,
                                  L2DS_MODULE_NAME, "L2dsUtilAddOptions:"
                                  "Packetsize exceeds the maximum MTU. \r\n");
                    }
                }
            }
            else
            {
                L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                          "L2dsUtilAddOptions:Access Loop Characteristics"
                          "status is not enabled in this port.\r\n");
            }
        }
        else
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsUtilAddOptions:Unable to get the Access Loop"
                      "Characteristics status.\r\n");
        }
    }
    else
    {
        pu1Options[u2Offset] = L2DS_OPT_END;
    }

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT, L2DS_MODULE_NAME,
                   "L2dsUtilAddOptions:Exiting the function %s \r\n",
                   __FUNCTION__);

    return L2DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2dsUtilRemoveOptions                                */
/*                                                                           */
/* Description        : This routine is to remove the option-82 and DSL      */
/*                      options from DHCP Packet                             */
/*                                                                           */
/* Input(s)           : pL2DhcpSnpPktInfo - DHCP Packet                      */
/*                                                                           */
/* Output(s)          : pu1RemovedLen - No. of byte removed                  */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsUtilRemoveOptions (tL2DhcpSnpPktInfo * pL2DhcpSnpPktInfo,
                       UINT1 *pu1RemovedLen)
{
    UINT1              *pu1Options = NULL;
    UINT2               u2TempOffset = L2DS_ZERO;
    UINT2               u2OptLen = L2DS_ZERO;
    UINT2               u2Offset = L2DS_ZERO;
    UINT1               u1Type = L2DS_ZERO;
    UINT1               u1Len = L2DS_ZERO;
    UINT1               u1VndrLen = L2DS_ZERO;

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_ENTRY, L2DS_MODULE_NAME,
                   "L2dsUtilRemoveOptions:Entering the function %s \r\n",
                   __FUNCTION__);

    u2OptLen = (pL2DhcpSnpPktInfo->L2DhcpSnpPacket).u2OptLen;
    pu1Options = (pL2DhcpSnpPktInfo->L2DhcpSnpPacket).au1Options;

    *pu1RemovedLen = L2DS_ZERO;

    while (u2OptLen > u2Offset)
    {
        u1Type = pu1Options[u2Offset];

        if (u1Type == L2DS_OPT_PAD)
        {
            u2Offset++;
            continue;
        }
        else if (u1Type == L2DS_OPT_RAI)
        {
            /* u2TempOffset is having the length of circuit id + remote id */
            u2TempOffset = pu1Options[u2Offset + L2DS_ONE];

            /* Remove the Relay Agent information option */
            MEMCPY ((pu1Options + u2Offset),
                    (pu1Options + u2Offset + u2TempOffset +
                     L2DS_OPTION_VAL_OFFSET),
                    (u2OptLen - (u2Offset + u2TempOffset +
                                 L2DS_OPTION_VAL_OFFSET)));

            MEMSET ((pu1Options + u2OptLen - (u2TempOffset +
                                              L2DS_OPTION_VAL_OFFSET)),
                    L2DS_ZERO, u2TempOffset + L2DS_OPTION_VAL_OFFSET);

            /* updating the removed length in the packet */
            (pL2DhcpSnpPktInfo->L2DhcpSnpPacket).u2OptLen -=
                u2TempOffset + L2DS_OPTION_VAL_OFFSET;

            /* pu1RemovedLen is total length of option-82 */
            *pu1RemovedLen = (UINT1) (*pu1RemovedLen + u2TempOffset +
                                      L2DS_OPTION_VAL_OFFSET);

            /* there is no change in the value of u2Offset; but the u2OptLen 
             * will be decreased by L2DS_OPTION_RAI_LEN */
            u2OptLen = (UINT2)
                (u2OptLen - u2TempOffset + L2DS_OPTION_VAL_OFFSET);
            continue;
        }
        else if (u1Type == L2DS_OPT_VENDOR_SPEC)
        {
            /* TBD: Now we are removing the Vendor specific options 
             * blindily. Actually we should remove only DSL sub-option */
            if (pu1Options[u2Offset + L2DS_TWO] == L2DS_SUBOPT_DSL)
            {
                /* The vendor specific option contains DSL Suboption 
                 * So we can safely assume that this option was added by us */

                /* Get DSL SUB-Option length */
                u1Len = pu1Options[u2Offset + L2DS_ONE];

                /* Total  Vendor specific options length */
                u1VndrLen = (UINT1) (u1Len + L2DS_TWO);

                /* Remove the Vendor specific options */
                MEMCPY ((pu1Options + u2Offset),
                        (pu1Options + u2Offset + u1VndrLen),
                        (u2OptLen - (u2Offset + u1VndrLen)));

                MEMSET ((pu1Options + u2OptLen - u1VndrLen), L2DS_ZERO,
                        u1VndrLen);

                (pL2DhcpSnpPktInfo->L2DhcpSnpPacket).u2OptLen -= u1VndrLen;

                *pu1RemovedLen = (UINT1) (*pu1RemovedLen + u1VndrLen);

                /* there is no change in the value of u2Offset; but the u2OptLen
                 * will be decreased by u1VndrLen */
                u2OptLen = (UINT2) (u2OptLen - u1VndrLen);
                continue;
            }
        }
        else if (u1Type == L2DS_OPT_END)
        {
            return L2DS_SUCCESS;
        }

        u1Len = pu1Options[u2Offset + L2DS_ONE];
        u2Offset = (UINT2) (u2Offset + u1Len + L2DS_OPTION_VAL_OFFSET);
    }

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT, L2DS_MODULE_NAME,
                   "L2dsUtilRemoveOptions:Exiting the function %s after "
                   "removing option-82 \r\n", __FUNCTION__);

    return L2DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2dsUtilGetPortAndVlanFromOption                     */
/*                                                                           */
/* Description        : This is to get the out-port and C-Vlan from the      */
/*                      CIRCUIT-ID sub-option                                */
/*                                                                           */
/* Input(s)           : pL2DhcpSnpOption - Option-82                         */
/*                                                                           */
/* Output(s)          : pu4OutPort - Out port                                */
/*                    : pVlanId    - Vlan Identifier                         */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsUtilGetPortAndVlanFromOption (tL2DhcpSnpOption * pL2DhcpSnpOption,
                                  UINT4 *pu4OutPort, tVlanId * pVlanId)
{
    UINT1               au1CircuitID[DCS_CIRCUIT_ID_LEN + L2DS_ONE]
        = { L2DS_ZERO };
    UINT1               au1Name[L2DS_CIRCUIT_MAX_LEN] = { L2DS_ZERO };
    UINT1              *pu1Options = NULL;
    UINT1              *pu1IfType = NULL;
    UINT1              *pu1Port = NULL;
    UINT1              *pu1Slot = NULL;
    UINT1              *pu1Vpi = NULL;
    UINT1              *pu1Vci = NULL;
    UINT1              *pu1Name = NULL;
    UINT1              *pi1VlanId = NULL;
    UINT4               u4DslIfIndex = L2DS_ZERO;
    UINT4               u4IfIndex = L2DS_ZERO;
    UINT4               u4OutPort = L2DS_ZERO;
    UINT1               u1OptLen = L2DS_ZERO;
    UINT1               u1Offset = L2DS_ZERO;
    UINT1               u1SubType = L2DS_ZERO;
    UINT1               u1SubTypeLen = L2DS_ZERO;
    INT4                i4Vci = L2DS_ZERO;
    INT4                i4Vpi = L2DS_ZERO;
    INT4                i4Slot = L2DS_ZERO;
    INT4                i4Port = L2DS_ZERO;
    tVlanId             VlanId = L2DS_ZERO;

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_ENTRY, L2DS_MODULE_NAME,
                   "L2dsUtilGetPortAndVlanFromOption:Entering the"
                   "function %s \r\n", __FUNCTION__);

    pu1Name = au1Name;

    pu1Options = pL2DhcpSnpOption->pu1Val;

    if (pL2DhcpSnpOption->u1Type != L2DS_OPT_RAI)
    {
        /* The passed option is not option-82 */
        return L2DS_FAILURE;
    }

    /* Get the option length */
    u1OptLen = pL2DhcpSnpOption->u1Len;

    while (u1OptLen > u1Offset)
    {
        u1SubType = pu1Options[u1Offset++];
        u1SubTypeLen = pu1Options[u1Offset++];

        if (u1SubType != L2DS_CIRCUIT_SUBOPT)
        {
            /* if the sub-option is not Circuit-ID, get the next sub-option */
            u1Offset = (UINT1) (u1Offset + u1SubTypeLen);
            continue;
        }
        else
        {
            break;
        }
    }

    /* Agent circuit id inserted by Access node must not 
       exceed 63 characters  */
    if (u1SubTypeLen > DCS_CIRCUIT_ID_LEN)
    {
        return L2DS_FAILURE;
    }

    /* offset is pointing to circuit id 
     * string(access-node-id atm slot/port:vpi.vci). 
     * au1CircuitID is having this string.*/
    MEMCPY (au1CircuitID, (pu1Options + u1Offset), u1SubTypeLen);

    au1CircuitID[u1SubTypeLen] = '\0';

    /*access-node-id */
    STRTOK (au1CircuitID, " ");

    /* pu1IfType is having atm/eth */
    pu1IfType = (UINT1 *) STRTOK (NULL, " ");
    if (pu1IfType == L2DS_ZERO)
    {
        return L2DS_FAILURE;
    }

    if (STRCMP (pu1IfType, "atm") == L2DS_ZERO)
    {
        /* pu1Slot is having slot value */
        pu1Slot = (UINT1 *) STRTOK (NULL, "/");
        if (pu1Slot != NULL)
        {
            /* Converting the string into integer */
            i4Slot = (INT4) ATOI (pu1Slot);
        }
        /* pu1Port is having Port value */
        pu1Port = (UINT1 *) STRTOK (NULL, ":");
        if (pu1Port != NULL)
        {
            /* Converting the string into integer */
            i4Port = (INT4) ATOI (pu1Port);
        }
        /* pu1Vpi is having VPI value */
        pu1Vpi = (UINT1 *) STRTOK (NULL, ".");
        if (pu1Vpi != NULL)
        {
            /* Converting the string into integer */
            i4Vpi = (INT4) ATOI (pu1Vpi);
        }
        /* pu1Vci is having VCI value */
        pu1Vci = (UINT1 *) STRTOK (NULL, " ");
        if (pu1Vci != NULL)
        {
            /* Converting the string into integer */
            i4Vci = (INT4) ATOI (pu1Vci);
        }

        SPRINTF ((CHR1 *) pu1Name, "%s%d/%d", ISS_ALIAS_PREFIX, i4Slot, i4Port);

        if (CfaGetInterfaceIndexFromName (pu1Name, &u4DslIfIndex) !=
            OSIX_FAILURE)
        {
            /* calling a function to get PVC index from vpi,vci and DSL
               index */
            if (CfaGetPvcIfIndexFromDslIndexVpiVci (u4DslIfIndex,
                                                    i4Vpi, i4Vci,
                                                    &u4IfIndex) != CFA_FAILURE)
            {
                *pu4OutPort = u4IfIndex;
                *pVlanId = VlanId;

                L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT,
                               L2DS_MODULE_NAME,
                               "L2dsUtilGetPortAndVlanFromOption:"
                               "Exiting the function %s after getting"
                               "outport index from PVC Index,vpi and "
                               "vci \r\n", __FUNCTION__);

                return L2DS_SUCCESS;
            }
        }
    }

    if (STRCMP (pu1IfType, "eth") == L2DS_ZERO)
    {
        /* pu1Slot is having slot value */
        pu1Slot = (UINT1 *) STRTOK (NULL, "/");
        if (pu1Slot != NULL)
        {
            /* Converting the string into integer */
            i4Slot = (INT4) ATOI (pu1Slot);
        }
        /* pi4Port is having Port value */
        pu1Port = (UINT1 *) STRTOK (NULL, ":");
        if (pu1Port != NULL)
        {
            /* Converting the string into integer */
            i4Port = (INT4) ATOI (pu1Port);
        }
        /* pi1VlanId is having VlanId */
        pi1VlanId = (UINT1 *) STRTOK (NULL, "");
        if (pi1VlanId != NULL)
        {
            /* Converting the string into integer */
            VlanId = (UINT2) ATOI (pi1VlanId);
        }
        SPRINTF ((CHR1 *) pu1Name, "%s%d/%d", ISS_ALIAS_PREFIX, i4Slot, i4Port);

        /* calling a function to get outport from the ifname */
        if (CfaGetInterfaceIndexFromName (pu1Name, &u4OutPort) != OSIX_FAILURE)
        {
            *pu4OutPort = u4OutPort;
            *pVlanId = VlanId;

            L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT,
                           L2DS_MODULE_NAME,
                           "L2dsUtilGetPortAndVlanFromOption:"
                           "Exiting the function %s after getting"
                           "outport index and VlanID from DSL"
                           "Index \r\n", __FUNCTION__);
            return L2DS_SUCCESS;
        }
    }

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT, L2DS_MODULE_NAME,
                   "L2dsUtilGetPortAndVlanFromOption:Exiting the function"
                   "%s after failing to find the OutPort and VlanID \r\n",
                   __FUNCTION__);
    return L2DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2dsUtilGetRemoteIdMac                               */
/*                                                                           */
/* Description        : This is to get the MAC in the REMOTE-ID sub-option   */
/*                                                                           */
/* Input(s)           : pL2DhcpSnpOption - Option-82                         */
/*                                                                           */
/* Output(s)          : RemoteMac - Mac address                              */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsUtilGetRemoteIdMac (tL2DhcpSnpOption * pL2DhcpSnpOption, tMacAddr RemoteMac)
{
    UINT1              *pu1Options = NULL;
    UINT1               u1OptLen = L2DS_ZERO;
    UINT1               u1Offset = L2DS_ZERO;
    UINT1               u1SubType = L2DS_ZERO;
    UINT1               u1SubTypeLen = L2DS_ZERO;

    pu1Options = pL2DhcpSnpOption->pu1Val;

    if (pL2DhcpSnpOption->u1Type != L2DS_OPT_RAI)
    {
        /* The passed option is not option-82 */
        return L2DS_FAILURE;
    }

    u1OptLen = pL2DhcpSnpOption->u1Len;

    while (u1OptLen > u1Offset)
    {
        u1SubType = pu1Options[u1Offset++];
        u1SubTypeLen = pu1Options[u1Offset++];

        if (u1SubType != L2DS_REMOTE_SUBOPT)
        {
            /* if the sub-option is not Remote-ID, get the next sub-option */
            u1Offset = (UINT1) (u1Offset + u1SubTypeLen);
            continue;
        }
        else
        {
            if (u1SubTypeLen != L2DS_REMOTE_SUBOPT_LEN)
            {
                return L2DS_FAILURE;
            }
            if (pu1Options[u1Offset++] != L2DS_REMOTE_ID_TYPE)
            {
                return L2DS_FAILURE;
            }
            if (pu1Options[u1Offset++] != L2DS_REMOTE_LEN)
            {
                return L2DS_FAILURE;
            }

            MEMCPY (RemoteMac, (pu1Options + u1Offset), sizeof (tMacAddr));

            return L2DS_SUCCESS;
        }
    }
    return L2DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2dsUtilGetAccessNodeId                              */
/*                                                                           */
/* Description        : This is to get the AccessNodeID in the CIRCUIT-ID    */
/*                      sub-option                                           */
/*                                                                           */
/* Input(s)           : pL2DhcpSnpOption - Option-82                         */
/*                      u1AccessIdLen    - Length of AccessNodeID            */
/*                                                                           */
/* Output(s)          : pu1AccessId - access node id                         */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsUtilGetAccessNodeId (tL2DhcpSnpOption * pL2DhcpSnpOption,
                         UINT1 u1AccessIdLen, UINT1 *pu1AccessId)
{
    UINT1              *pu1Options = NULL;
    UINT1               u1OptLen = L2DS_ZERO;
    UINT1               u1Offset = L2DS_ZERO;
    UINT1               u1SubType = L2DS_ZERO;
    UINT1               u1SubTypeLen = L2DS_ZERO;

    pu1Options = pL2DhcpSnpOption->pu1Val;

    if (pL2DhcpSnpOption->u1Type != L2DS_OPT_RAI)
    {
        /* The passed option is not option-82 */
        return L2DS_FAILURE;
    }

    u1OptLen = pL2DhcpSnpOption->u1Len;

    while (u1OptLen > u1Offset)
    {
        u1SubType = pu1Options[u1Offset++];
        u1SubTypeLen = pu1Options[u1Offset++];

        if (u1SubType != L2DS_CIRCUIT_SUBOPT)
        {
            /* if the sub-option is not Circuit-ID, get the next sub-option */
            u1Offset = (UINT1) (u1Offset + u1SubTypeLen);
            continue;
        }
        else
        {
            MEMCPY (pu1AccessId, (pu1Options + u1Offset), u1AccessIdLen);

            return L2DS_SUCCESS;
        }
    }
    return L2DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2dsUtileGetFreeString                               */
/*                                                                           */
/* Description        : This is to get the FreeString in the CIRCUIT-ID      */
/*                      sub-option                                           */
/*                                                                           */
/* Input(s)           : pL2DhcpSnpOption - Option-82                         */
/*                      u1ACILen   - Length of FreeString                    */
/*                                                                           */
/* Output(s)          : pu1ACI     -  free string identifier                 */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsUtilGetFreeString (tL2DhcpSnpOption * pL2DhcpSnpOption,
                       UINT1 u1ACILen, UINT1 *pu1ACI)
{
    UINT1              *pu1Options = NULL;
    UINT1               u1OptLen = L2DS_ZERO;
    UINT1               u1Offset = L2DS_ZERO;
    UINT1               u1SubType = L2DS_ZERO;
    UINT1               u1SubTypeLen = L2DS_ZERO;

    pu1Options = pL2DhcpSnpOption->pu1Val;

    if (pL2DhcpSnpOption->u1Type != L2DS_OPT_RAI)
    {
        /* The passed option is not option-82 */
        return L2DS_FAILURE;
    }

    u1OptLen = pL2DhcpSnpOption->u1Len;

    while (u1OptLen > u1Offset)
    {
        u1SubType = pu1Options[u1Offset++];
        u1SubTypeLen = pu1Options[u1Offset++];

        if (u1SubType != L2DS_CIRCUIT_SUBOPT)
        {
            /* if the sub-option is not Circuit-ID, get the next sub-option */
            u1Offset = (UINT1) (u1Offset + u1SubTypeLen);
            continue;
        }
        else
        {
            MEMCPY (pu1ACI, (pu1Options + u1Offset), u1ACILen);

            return L2DS_SUCCESS;
        }
    }
    return L2DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2dsUtilVerifyCookie                                 */
/*                                                                           */
/* Description        : This routine verifies the Magic cookie in the DHCP   */
/*                      packet                                               */
/*                                                                           */
/* Input(s)           : pu1LinearBuf - Linear buffer                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsUtilVerifyCookie (UINT1 *pu1LinearBuf)
{
    UINT1               au1MagicCookie[L2DS_MAGIC_COOKIE_LEN] =
        { 0x63, 0x82, 0x53, 0x63 };

    if (MEMCMP (pu1LinearBuf, au1MagicCookie, L2DS_MAGIC_COOKIE_LEN) !=
        L2DS_ZERO)
    {
        return L2DS_FAILURE;
    }
    return L2DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2dsUtilCalcUdpCkSum                                 */
/*                                                                           */
/* Description        : This routine calculates the UDP Checksum of the DHCP */
/*                      packet                                               */
/*                                                                           */
/* Input(s)           : u2UdpLen - Pkt length includes UDP header and data   */
/*                      u4DestIp - destination IP                            */
/*                      u4SrcIp  - source IP                                 */
/*                      pu1LinearBuf - Linear buffer                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Calculated UDP Checksum                              */
/*****************************************************************************/
INT2
L2dsUtilCalcUdpCkSum (UINT2 u2UdpLen, UINT4 u4DestIp, UINT4 u4SrcIp,
                      UINT1 *pu1LinearBuf)
{
    UINT4               u4Sum = L2DS_ZERO;
    UINT2               u2Tmp = L2DS_ZERO;
    UINT2               u2Size = L2DS_ZERO;

    /* Adding the Pseudo Header */
    u4Sum += (u4SrcIp >> L2DS_SIXTEEN);
    u4Sum += (u4SrcIp & 0xffff);
    u4Sum += (u4DestIp >> L2DS_SIXTEEN);
    u4Sum += (u4DestIp & 0xffff);
    u4Sum += (UINT4) L2DS_UDP_PROTO_ID;    /* UDP protocol = 17. */
    u4Sum += (UINT4) u2UdpLen;

    u2Size = u2UdpLen;

    while (u2Size > L2DS_ONE)
    {
        /* Make a UINT2 word of adjacent 2 bytes */
        u2Tmp = (UINT2) (((pu1LinearBuf[L2DS_ZERO] << L2DS_EIGHT) &
                          (UINT2) 0xFF00) +
                         (pu1LinearBuf[L2DS_ONE] & (UINT1) 0xFF));
        u4Sum += (UINT4) u2Tmp;
        pu1LinearBuf += (UINT1) sizeof (UINT2);
        u2Size -= sizeof (UINT2);
    }

    if (u2Size == L2DS_ONE)
    {
        u2Tmp = (UINT2) (((pu1LinearBuf[L2DS_ZERO] << L2DS_EIGHT) &
                          (UINT2) 0xFF00));
        u4Sum += (UINT4) u2Tmp;
    }

    /* keep only the last 16 bits of the 32 bit calculated
     * sum and add the carries */
    while (u4Sum >> L2DS_SIXTEEN)
    {
        u4Sum = (u4Sum & 0xFFFF) + (u4Sum >> L2DS_SIXTEEN);
    }
    /* Take the one's complement of sum */
    u2Tmp = (UINT2) ~u4Sum;

    u2Tmp = OSIX_HTONS (u2Tmp);

    return (u2Tmp);
}

/*****************************************************************************/
/* Function Name      : L2dsUtilLinearToCRU                                  */
/*                                                                           */
/* Description        : This routine converts the given linear buffer        */
/*                      to CRU Buffer                                        */
/*                                                                           */
/* Input(s)           : pu1LinearBuf - Linear buffer                         */
/*                      u4PktLength  - Packet length                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Pointer to CRU Buffer/NULL                           */
/*****************************************************************************/
tCRU_BUF_CHAIN_HEADER *
L2dsUtilLinearToCRU (UINT1 *pu1LinearBuf, UINT4 u4PktLength)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktLength, L2DS_ZERO);

    if (pBuf == NULL)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsUtilLinearToCRU:Unable to allocate CRU buffer.\r\n");
        return NULL;
    }
    CRU_BUF_Copy_OverBufChain (pBuf, pu1LinearBuf, L2DS_ZERO, u4PktLength);

    return pBuf;
}

/*****************************************************************************/
/* Function Name      : L2dsUtilRBTreeIntfEntryCmp                           */
/*                                                                           */
/* Description        : This function is used for comparing two entries of   */
/*                      RBTree for interface entries                         */
/*                                                                           */
/* Input(s)           : pL2DhcpSnpIfaceEntryOne -First Entry                 */
/*                      pL2DhcpSnpIfaceEntryTwo -second Entry                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when entry 1 is Less/Greater than entry 2    */
/*                      0    -> when entries elements are Equal              */
/*****************************************************************************/
INT4
L2dsUtilRBTreeIntfEntryCmp (tRBElem * pL2DhcpSnpIfaceEntryOne,
                            tRBElem * pL2DhcpSnpIfaceEntryTwo)
{
    if (((tL2DhcpSnpIfaceEntry *) pL2DhcpSnpIfaceEntryOne)->u4L2dsCxtId <
        ((tL2DhcpSnpIfaceEntry *) pL2DhcpSnpIfaceEntryTwo)->u4L2dsCxtId)
    {
        return L2DS_MINUS_ONE;
    }
    if (((tL2DhcpSnpIfaceEntry *) pL2DhcpSnpIfaceEntryOne)->u4L2dsCxtId >
        ((tL2DhcpSnpIfaceEntry *) pL2DhcpSnpIfaceEntryTwo)->u4L2dsCxtId)
    {
        return L2DS_ONE;
    }

    if (((tL2DhcpSnpIfaceEntry *) pL2DhcpSnpIfaceEntryOne)->VlanId <
        ((tL2DhcpSnpIfaceEntry *) pL2DhcpSnpIfaceEntryTwo)->VlanId)
    {
        return L2DS_MINUS_ONE;
    }
    else if (((tL2DhcpSnpIfaceEntry *) pL2DhcpSnpIfaceEntryOne)->VlanId >
             ((tL2DhcpSnpIfaceEntry *) pL2DhcpSnpIfaceEntryTwo)->VlanId)
    {
        return L2DS_ONE;
    }
    else
    {
        return L2DS_ZERO;
    }
}

/*****************************************************************************/
/* Function Name      : L2dsUtilRBTreeEntryFree                              */
/*                                                                           */
/* Description        : This routine is used for releasing the Memory        */
/*                      allocated for RbTree Node for Interface structure.   */
/*                                                                           */
/* Input(s)           : pL2DhcpSnpIfaceEntry - Pointer to Interface entry    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2dsUtilRBTreeEntryFree (tRBElem * pL2DhcpSnpIfaceEntry)
{
    if (pL2DhcpSnpIfaceEntry != NULL)
    {
        L2DS_RELEASE_MEM_BLOCK (L2DS_INTF_POOL_ID, pL2DhcpSnpIfaceEntry);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : L2dsUtilTakeLock                                     */
/*                                                                           */
/* Description        : This function is to take the protocol lock for the   */
/*                      module                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/
INT4
L2dsUtilTakeLock (VOID)
{
    if (L2DS_TAKE_SEM (L2DS_ZERO, L2DS_SEM_NAME, L2DS_ZERO, L2DS_ZERO)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2dsUtilReleaseLock                                  */
/*                                                                           */
/* Description        : This function is to release the protocol lock taken  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/
INT4
L2dsUtilReleaseLock (VOID)
{
    L2DS_GIVE_SEM (L2DS_ZERO, L2DS_SEM_NAME);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2dsUtilRegisterFsDhcSnpMib                          */
/*                                                                           */
/* Description        : This function is to register the protocol MIB        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2dsUtilRegisterFsDhcSnpMib (VOID)
{
    RegisterFSMIDH ();
    /* Registering SI mibs in case of SI alone. */
    if (L2dsGetVcmSystemModeExt (L2DS_PROTOCOL_ID) == VCM_SI_MODE)
    {
        RegisterFSDHCS ();
    }
    return;
}

/*****************************************************************************/
/* Function Name      : L2dsUtilValidateFreeString                           */
/*                                                                           */
/* Description        : This is to validate to which port free string is     */
/*                      configured.                                          */
/*                                                                           */
/* Input(s)           : pL2DhcpSnpOption - Option-82                         */
/*                                                                           */
/* Output(s)          : pu4OutPort     - The port to which the free-string   */
/*                                       is configured                       */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsUtilValidateFreeString (tL2DhcpSnpOption * pL2DhcpSnpOption,
                            UINT4 *pu4OutPort)
{
    UINT1               au1FreeString[DCS_CIRCUIT_ID_LEN] = { L2DS_ZERO };
    UINT1               au1FreeStringACI[DCS_CIRCUIT_ID_LEN] = { L2DS_ZERO };
    UINT4               u4IfIndex = L2DS_ZERO;
    INT4                i4FreeStringLen = L2DS_ZERO;
    INT4                i4RetStatus = L2DS_ZERO;

    for (u4IfIndex = L2DS_ONE; u4IfIndex <= (SYS_DEF_MAX_PHYSICAL_INTERFACES +
                                             SYS_DEF_MAX_PVCS +
                                             LA_MAX_PORTS_PER_AGG); u4IfIndex++)
    {
        if (DcsGetFreeStringACI
            ((INT4) u4IfIndex, au1FreeString, &i4FreeStringLen) != DCS_FAILURE)
        {
            if (i4FreeStringLen != L2DS_ZERO)
            {
                /* Get the Free String configured in the ACI */
                i4RetStatus =
                    L2dsUtilGetFreeString (pL2DhcpSnpOption,
                                           (UINT1) i4FreeStringLen,
                                           au1FreeStringACI);

                if (i4RetStatus == L2DS_SUCCESS)
                {
                    if (MEMCMP
                        (au1FreeStringACI, au1FreeString,
                         i4FreeStringLen) == L2DS_ZERO)
                    {
                        *pu4OutPort = u4IfIndex;
                        return L2DS_SUCCESS;
                    }
                }
            }
        }
    }
    return L2DS_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name        : DcsGetAccessNodeIdentifier
 *
 *    Description          : This function is used to get configured Access 
 *                           Node Identifier.
 *
 *    Input(s)             : pu1AccessNodeName -Pointer to the AccessNodeID
 *
 *    Output(s)            : NONE
 *    
 *    Return               : pu1AccessNodeName - Name will be return.
 *   
 *****************************************************************************/
UINT1              *
DcsGetAccessNodeIdentifier (UINT1 *pu1AccessNodeName)
{
    pu1AccessNodeName = IssSysGetSwitchName ();
    return pu1AccessNodeName;
}

/*****************************************************************************/
/* Function Name      : DcsUtilGetCircuitIdString                            */
/*                                                                           */
/* Description        : This routine is to add Agent Circuit ID in the       */
/*                      incoming PPPoE Discovery Stage Packet                */
/*                                                                           */
/* Input(s)           : u4PortNumber - Incoming Port                         */
/*                      VlanId       - Vlan Identifier                       */
/*                                                                           */
/* Output(s)          : pu1CircuitId - Pointer to Agent Circuit ID           */
/*                      pu1CircuitIDLen -Total circuit id Length             */
/*                                                                           */
/* Return Value(s)    : DCS_SUCCESS/DCS_FAILURE                              */
/*****************************************************************************/
INT4
DcsUtilGetCircuitIdString (UINT4 u4PortNumber, tVlanId VlanId,
                           UINT1 *pu1CircuitId, UINT1 *pu1CircuitIDLen)
{
    UINT1               au1Temp[DCS_CIRCUIT_ID_LEN] = { L2DS_ZERO };
    UINT1               au1AccessNode[DCS_ACCESS_NODE_LEN + L2DS_ONE]
        = { L2DS_ZERO };
    UINT1              *pu1AccessNodeName = NULL;
    INT1               *pi1CircuitIdString = NULL;
    UINT1               u1AccessNodeLength = L2DS_ZERO;
    INT4                i4SlotPortNum = L2DS_ZERO;
    UINT4               u4DslIfIndex = L2DS_ZERO;
    UINT1               u1IfType = L2DS_ZERO;
    INT4                i4SlotNum = L2DS_ZERO;
    INT4                i4Vpi = L2DS_ZERO;
    INT4                i4Vci = L2DS_ZERO;

    /* Maximum Length that can be filled in the Circuit-Id Fields */
    /* AccessNodeName = 24 (DCS_ACCESS_NODE_LEN)
     * SlotNumber     = 4 Bytes
     * PortNumber     = 4 Bytes
     * VPI            = 4 Bytes
     * VCI            = 4 Bytes
     * VlanId(1-4094) = 2 Bytes.*/

    /* If the incoming interface is DSL Interface then  
     * circuit-Id value will be as follows :               */
    /*******************************************************/
    /* AccessNodeName "atm" SlotNum / PortNumber : VPI.VCI */
    /*******************************************************/
    /*  Example : ISS atm 0/25:8.35                        */
    /*******************************************************/

    /* If the incoming interface is ethernet then  
     * circuit-Id packet format will be as follows :       */
    /*******************************************************/
    /* AccessNodeName "eth" SlotNum / PortNumber : VlanId  */
    /*******************************************************/
    /*  Example : ISS eth 0/1:2                            */
    /*******************************************************/

    pi1CircuitIdString = (INT1 *) au1Temp;

    if (CfaGetIfaceType (u4PortNumber, &u1IfType) == CFA_FAILURE)
    {
        L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_ENTRY, L2DS_MODULE_NAME,
                       "DcsUtilGetCircuitIdString:Failed to get Iftype"
                       "for interface index %d. \r\n", u4PortNumber);
        return DCS_FAILURE;
    }

    /*calling a function to get Access Node Identifier */
    pu1AccessNodeName = DcsGetAccessNodeIdentifier (au1AccessNode);

    if (u1IfType == CFA_PVC)
    {
        /*calling a function to get DSL Interface Index, vpi and vci
         * from PVC Interface Index */
        if (CfaGetVciVpiDslIfIndex (u4PortNumber,
                                    &u4DslIfIndex, &i4Vpi, &i4Vci) ==
            CFA_FAILURE)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "DcsiUtilGetCircuitIdString:Failed to get VPI,VCI and "
                      "DslIndex for the PVC Interface Index \r\n");
            return DCS_FAILURE;
        }

        u4PortNumber = u4DslIfIndex;
    }

    /*calling a function to get slot and port from Interface index */
    if (CfaGetSlotAndPortFromIfIndex (u4PortNumber,
                                      &i4SlotNum,
                                      &i4SlotPortNum) == CFA_FAILURE)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "DcsUtilGetCircuitIdString:Failed to get Slot and Port for "
                  "the Interface Index \r\n");

        return DCS_FAILURE;
    }

    if (u1IfType == CFA_PVC)
    {
        SPRINTF ((CHR1 *) pi1CircuitIdString, "%s %s %d/%d:%d.%d",
                 pu1AccessNodeName, "atm", i4SlotNum, i4SlotPortNum, i4Vpi,
                 i4Vci);
    }
    else
    {
        SPRINTF ((CHR1 *) pi1CircuitIdString, "%s %s %d/%d:%d",
                 pu1AccessNodeName, "eth", i4SlotNum, i4SlotPortNum, VlanId);
    }

    u1AccessNodeLength = (UINT1) STRLEN (pi1CircuitIdString);

    pu1CircuitId[L2DS_ZERO] = DCS_CIRCUIT_SUBOPT;    /* Sub-tag type */
    pu1CircuitId[L2DS_ONE] = u1AccessNodeLength;    /* Length */

    MEMCPY (&(pu1CircuitId[L2DS_TWO]), pi1CircuitIdString, u1AccessNodeLength);

    /* Circuit ID is Type-Length-Value format.To get Total Circuit ID Length
     * Adding Type and Length Bytes with the Circuit ID string length*/
    *pu1CircuitIDLen = (UINT1) (u1AccessNodeLength + DCS_CIRCUIT_ID_HEADER_LEN);

    return DCS_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : DcsUtilGetRemoteIdString                             */
/*                                                                           */
/* Description        : This routine is to add Agent Remote ID in the        */
/*                      incoming PPPoE Discovery Stage Packet                */
/*                                                                           */
/* Output(s)          : au1RemoteId - Remote ID                              */
/*                      pu1RemoteIdLength -Total Remote ID Length            */
/*                                                                           */
/* Return Value(s)    : DCS_SUCCESS/DCS_FAILURE                              */
/*****************************************************************************/
INT4
DcsUtilGetRemoteIdString (UINT1 *pu1RemoteId, UINT1 *pu1RemoteIdLength)
{
    /* Remote ID is Type-Length-Value format.To get Total Remote ID Length
     *      * Adding Type and Length Bytes with the Remote ID length*/

    pu1RemoteId[L2DS_ZERO] = DCS_REMOTE_SUBOPT;
    pu1RemoteId[L2DS_ONE] = L2DS_ZERO;

    *pu1RemoteIdLength = DCS_REMOTE_ID_HEADER_LEN;

    return DCS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2dsSelectContext                                */
/*                                                                           */
/*    Description         : This function switches to given context          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : L2DS_SUCCESS / L2DS_FAILURE.                     */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
L2dsSelectContext (UINT4 u4ContextId)
{
    if (L2dsGetVcmSystemMode (L2DS_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (L2dsVcmIsVcExist (u4ContextId) == L2DS_FALSE)
        {
            return L2DS_FAILURE;
        }
        L2DS_CURR_CXT_ID = u4ContextId;
    }
    return L2DS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : L2dsVcmIsSwitchExist                               */
/*                                                                           */
/*     DESCRIPTION      : This function will return whether the given        */
/*                        context exist or not.                              */
/*                                                                           */
/*     INPUT            : u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : L2DS_TRUE / L2DS_FALSE                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
L2dsVcmIsSwitchExist (UINT4 u4ContextId)
{
    if (L2dsGetVcmSystemMode (L2DS_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (L2dsVcmIsVcExist (u4ContextId) == VCM_FALSE)
        {
            return L2DS_FAILURE;
        }
    }
    else
    {
        if (u4ContextId != L2DS_DEFAULT_CXT_ID)
        {
            return L2DS_FAILURE;
        }
    }
    return L2DS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : L2dsVcmIsVcExist                                   */
/*                                                                           */
/*     DESCRIPTION      : This function will return whether the given        */
/*                        context exist or not.                              */
/*                                                                           */
/*     INPUT            : u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : L2DS_TRUE / L2DS_FALSE                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
L2dsVcmIsVcExist (UINT4 u4ContextId)
{
    INT4                i4RetVal = VCM_FALSE;

    i4RetVal = VcmIsL2VcExist (u4ContextId);

    return ((i4RetVal == VCM_FALSE) ? L2DS_FALSE : L2DS_TRUE);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : L2dsGetVcmSystemMode                               */
/*                                                                           */
/*     DESCRIPTION      : This function calls the VCM Module to get the      */
/*                        mode of the system (SI / MI).                      */
/*                                                                           */
/*     INPUT            : u2ProtocolId - Protocol Identifier                 */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : VCM_MI_MODE / VCM_SI_MODE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
L2dsGetVcmSystemMode (UINT2 u2ProtocolId)
{
    return (VcmGetSystemMode (u2ProtocolId));
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : L2dsGetVcmSystemModeExt                            */
/*                                                                           */
/*     DESCRIPTION      : This function calls the VCM Module to get the      */
/*                        mode of the system (SI / MI).                      */
/*                                                                           */
/*     INPUT            : u2ProtocolId - Protocol Identifier                 */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : VCM_MI_MODE / VCM_SI_MODE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
L2dsGetVcmSystemModeExt (UINT2 u2ProtocolId)
{
    return (VcmGetSystemModeExt (u2ProtocolId));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : L2dsReleaseContext                                         */
/*                                                                           */
/* Description  : This function makes the switch context to default Context  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
L2dsReleaseContext ()
{
    if (L2dsGetVcmSystemMode (L2DS_PROTOCOL_ID) == VCM_MI_MODE)
    {
        L2DS_CURR_CXT_ID = L2DS_DEFAULT_CXT_ID;
    }
    return;
}

/*****************************************************************************
 * FUNCTION NAME    : L2dsVcmIsSwitchNameExist                                   
 *
 * DESCRIPTION      : Routine used to get the context Id for the Alias Name
 *
 * INPUT            : pu1Alias - Context Name
 *                   
 * OUTPUT           : pu4VcNum - Context Identifier
 *
 * RETURNS          : L2DS_TRUE/L2DS_FALSE
 *  
 * ***************************************************************************/
PUBLIC INT4
L2dsVcmIsSwitchNameExist (UINT1 *pu1Alias, UINT4 *pu4VcNum)
{
    INT4                i4RetVal = VCM_FALSE;

    i4RetVal = VcmIsSwitchExist (pu1Alias, pu4VcNum);

    return ((i4RetVal == VCM_FALSE) ? L2DS_FALSE : L2DS_TRUE);
}

/*****************************************************************************/
/* Function Name      : L2dsVcmGetContextInfoFromIfIndex                     */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      Context-Id and the Localport number.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex      - Interface Identifier.               */
/*                                                                           */
/* Output(s)          : pu4ContextId   - Context Identifier.                 */
/*                      pu2LocalPortId - Local port number.                  */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
L2dsVcmGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                                  UINT2 *pu2LocalPortId)
{
    INT4                i4RetVal = VCM_FALSE;

    i4RetVal = VcmGetContextInfoFromIfIndex (u4IfIndex, pu4ContextId,
                                             pu2LocalPortId);

    return ((i4RetVal == VCM_FAILURE) ? L2DS_FAILURE : L2DS_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2dsGetNextActiveContext                         */
/*                                                                           */
/*    Description         : This function is used to get the next Active     */
/*                          context present in the system.                   */
/*                                                                           */
/*    Input(s)            : u4CurrContextId - Current Context Id.            */
/*                                                                           */
/*    Output(s)           : pu4NextContextId - Next Context Id.              */
/*                                                                           */
/*    Returns            : L2DS_SUCCESS/L2DS_FAILURE                         */
/*****************************************************************************/

PUBLIC INT4
L2dsGetNextActiveContext (UINT4 u4CurrContextId, UINT4 *pu4NextContextId)
{
    if (VcmGetNextActiveL2Context (u4CurrContextId, pu4NextContextId)
        == VCM_FAILURE)
    {
        return L2DS_FAILURE;
    }
    return L2DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2dsUtilIsDhcpSnoopingEnabled                        */
/*                                                                           */
/* Description        : This function checks if Dhcp snooping feature is     */
/*                      enabled in the system or not.                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
L2dsUtilIsDhcpSnoopingEnabled (UINT4 u4ContextId)
{
    if (L2DS_ADMIN_STATUS (u4ContextId) != L2DS_ENABLED)
    {
        return L2DS_DISABLED;
    }
    return L2DS_ENABLED;
}

/*                                                                           */
/***************************** END OF FILE ***********************************/
/*                                                                           */
