/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: l2dsport.c,v 1.11 2018/01/09 11:03:04 siva Exp $       */
/*****************************************************************************/
/*    FILE  NAME            : l2dsport.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : L2DHCP Snooping                                */
/*    MODULE NAME           : L2DHCP Snooping Porting Module                 */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains porting functions           */
/*                            for L2DHCP Snooping module                     */
/*---------------------------------------------------------------------------*/

#include "l2dsinc.h"
#include "l2dsextn.h"

/*****************************************************************************/
/* Function Name      : L2dsPortEnquesDhcpPkts                               */
/*                                                                           */
/* Description        : This function is to enque DHCP packets for the       */
/*                      L2DHCP Snooping task                                 */
/*                                                                           */
/* Input(s)           : pBuf       - DHCP Packet                             */
/*                      u4IfIndex  - Incoming port index                     */
/*                      VlanTag    - Incoming VLAN tags                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2dsPortEnquesDhcpPkts (tCRU_BUF_CHAIN_HEADER * pBuf,
                        UINT4 u4IfIndex, tVlanTag VlanTag)
{
    tL2DhcpSnpQMsg     *pL2DhcpSnpQMsg = NULL;
    UINT4               u4ContextId = L2DS_ZERO;
    UINT2               u2InstPort = L2DS_ZERO;

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_ENTRY, L2DS_MODULE_NAME,
                   "L2dsPortEnquesDhcpPkts:Entering the function %s \r\n",
                   __FUNCTION__);

    L2DS_TRC_ARG3 (L2DS_TRC_FLAG, L2DS_FN_ARGS, L2DS_MODULE_NAME,
                   "L2dsPortEnquesDhcpPkts:The arguments to the function %s "
                   "are VlanId %d, InPort %d,\r\n", __FUNCTION__,
                   VlanTag.OuterVlanTag.u2VlanId, u4IfIndex);

    if ((pL2DhcpSnpQMsg = (tL2DhcpSnpQMsg *)
         (L2DS_ALLOC_MEM_BLOCK (L2DS_Q_POOL_ID))) == NULL)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPortEnquesDhcpPkts:Memory Allocation failed for Message"
                  "\r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    MEMSET (pL2DhcpSnpQMsg, L2DS_ZERO, sizeof (tL2DhcpSnpQMsg));

    if (L2dsVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2InstPort) != L2DS_SUCCESS)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPortEnquesDhcpPkts:Failed to get context Id" "\r\n");
        L2DS_RELEASE_MEM_BLOCK (L2DS_Q_POOL_ID, pL2DhcpSnpQMsg);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    if (L2DS_ADMIN_STATUS (u4ContextId) == L2DS_DISABLED)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPortEnquesDhcpPkts:Module not enabled. Dropping Message"
                  "\r\n");
        L2DS_RELEASE_MEM_BLOCK (L2DS_Q_POOL_ID, pL2DhcpSnpQMsg);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    pL2DhcpSnpQMsg->u4EventType = L2DS_PKT_RECEIVE_EVENT;
    pL2DhcpSnpQMsg->pInQMsg = pBuf;
    pL2DhcpSnpQMsg->u4InPort = u4IfIndex;
    pL2DhcpSnpQMsg->u4L2dsCxtId = u4ContextId;
    MEMCPY (&(pL2DhcpSnpQMsg->VlanTag), &VlanTag, sizeof (tVlanTag));

    if (L2DS_SEND_TO_QUEUE (L2DS_QUEUE_ID, (UINT1 *) &pL2DhcpSnpQMsg,
                            OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPortEnquesDhcpPkts:Sending the packet to queue failed"
                  "\r\n");

        L2DS_RELEASE_MEM_BLOCK (L2DS_Q_POOL_ID, pL2DhcpSnpQMsg);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    if (L2DS_SEND_EVENT (L2DS_ZERO, L2DS_TASK_NAME, L2DS_EVENT_ARRIVED)
        != OSIX_SUCCESS)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPortEnquesDhcpPkts:Sending the event failed\r\n");
        return;
    }
    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT, L2DS_MODULE_NAME,
                   "L2dsPortEnquesDhcpPkts:Exiting the function %s \r\n",
                   __FUNCTION__);
    return;
}

/*****************************************************************************/
/* Function Name      : L2dsPortVlanDelete                                   */
/*                                                                           */
/* Description        : This function is to indicate the L2DHCP Snooping     */
/*                      task about VLAN Deletion                             */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      VlanId     - Deleted VLAN Id                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2dsPortVlanDelete (UINT4 u4ContextId, tVlanId VlanId)
{
    tL2DhcpSnpQMsg     *pL2DhcpSnpQMsg = NULL;

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_ENTRY, L2DS_MODULE_NAME,
                   "L2dsPortVlanDelete:Entering the function %s \r\n",
                   __FUNCTION__);

    L2DS_TRC_ARG2 (L2DS_TRC_FLAG, L2DS_FN_ARGS, L2DS_MODULE_NAME,
                   "L2dsPortVlanDelete:The arguments to the function %s is "
                   "VlanId %d\r\n", __FUNCTION__, VlanId);
    if ((pL2DhcpSnpQMsg = (tL2DhcpSnpQMsg *)
         (L2DS_ALLOC_MEM_BLOCK (L2DS_Q_POOL_ID))) == NULL)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPortVlanDelete:Memory Allocation failed for Message"
                  "\r\n");
        return;
    }

    pL2DhcpSnpQMsg->u4EventType = L2DS_VLAN_INTERFACE_EVENT;
    pL2DhcpSnpQMsg->u4L2dsCxtId = u4ContextId;
    pL2DhcpSnpQMsg->VlanId = VlanId;

    if (L2DS_SEND_TO_QUEUE (L2DS_QUEUE_ID, (UINT1 *) &pL2DhcpSnpQMsg,
                            OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPortVlanDelete:Sending the info to queue failed\r\n");

        L2DS_RELEASE_MEM_BLOCK (L2DS_Q_POOL_ID, pL2DhcpSnpQMsg);
        return;
    }

    if (L2DS_SEND_EVENT (L2DS_ZERO, L2DS_TASK_NAME, L2DS_EVENT_ARRIVED)
        != OSIX_SUCCESS)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPortVlanDelete:Sending the event failed\r\n");
        return;
    }

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT, L2DS_MODULE_NAME,
                   "L2dsPortVlanDelete:Exiting the function %s \r\n",
                   __FUNCTION__);
    return;
}

/*****************************************************************************/
/* Function Name      : L2dsPortGetPortType                                  */
/*                                                                           */
/* Description        : This function is to get the port type of given port  */
/*                                                                           */
/* Input(s)           : u2Port     -  Incoming port                          */
/*                                                                           */
/* Output(s)          : pu1PortType - Returns value (port type)              */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsPortGetPortType (UINT2 u2InPort, UINT1 *pu1PortType)
{
    UINT1               u1PortType = L2DS_ZERO;
    /* Get the upstream status of the Port */
    if (CfaGetIfPortType (u2InPort, &u1PortType) != CFA_SUCCESS)
    {
        L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                       "L2dsPortGetPortType:Getting the port type failed for "
                       "port %d\r\n", u2InPort);
        return L2DS_FAILURE;
    }

    if (u1PortType == CFA_PORT_TYPE_DOWNLINK)
    {
        *pu1PortType = L2DS_DOWNSTREAM_PORT;
    }
    else if (u1PortType == CFA_PORT_TYPE_UPLINK)
    {
        *pu1PortType = L2DS_UPSTREAM_PORT;
    }
    else
    {
        return L2DS_FAILURE;
    }
    return L2DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2dsPortGetPortState                                 */
/*                                                                           */
/* Description        : This function is to get the port state of given port */
/*                                                                           */
/* Input(s)           : u2Port     - Incoming port                           */
/*                                                                           */
/* Output(s)          : pu1PortState - Returns value (port state)            */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsPortGetPortState (UINT2 u2InPort, UINT1 *pu1PortState)
{
    UINT1               u1PortState = L2DS_ZERO;
    /* Get the trusted status of the Port */
    if (CfaGetIfPortSecState (u2InPort, &u1PortState) != CFA_SUCCESS)
    {
        L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                       "L2dsPortGetPortState:Getting the port type failed for "
                       "port %d\r\n", u2InPort);
        return L2DS_FAILURE;
    }

    if (u1PortState == CFA_PORT_STATE_TRUSTED)
    {
        *pu1PortState = L2DS_TRUSTED_PORT;
    }
    else if (u1PortState == CFA_PORT_STATE_UNTRUSTED)
    {
        *pu1PortState = L2DS_UNTRUSTED_PORT;
    }
    else
    {
        return L2DS_FAILURE;
    }
    return L2DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2dsPortGetTrustedList                               */
/*                                                                           */
/* Description        : This function is to get the trusted portlist for     */
/*                      the given VLAN.                                      */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      VlanId      - VLAN Identifier                        */
/*                                                                           */
/* Output(s)          : PortList - list of upstream ports                    */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsPortGetTrustedList (UINT4 u4ContextId, tVlanId VlanId, tPortList PortList)
{
    MEMSET (PortList, L2DS_ZERO, sizeof (tPortList));

    /* Get the upstream status of the Port */
    if (CfaGetTrustedPortList (u4ContextId, (UINT2) VlanId, PortList)
        == CFA_SUCCESS)
    {
        return L2DS_SUCCESS;
    }
    else
    {
        L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                       "L2dsPortGetTrustedList:Getting the trusted portlist "
                       "failed for VLAN %d\r\n", VlanId);
        return L2DS_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name      : L2dsPortGetUpStreamList                              */
/*                                                                           */
/* Description        : This function is to get the upstream portlist for    */
/*                      the given VLAN.                                      */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      VlanId      - VLAN Identifier                        */
/*                                                                           */
/* Output(s)          : PortList - list of upstream ports                    */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsPortGetUpStreamList (UINT4 u4ContextId, tVlanId VlanId, tPortList PortList)
{

    MEMSET (PortList, L2DS_ZERO, sizeof (tPortList));

    /* Get the upstream status of the Port */
    if (CfaGetUpLinkPortList (u4ContextId, VlanId, PortList) == CFA_SUCCESS)
    {
        return L2DS_SUCCESS;
    }
    else
    {
        L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                       "L2dsPortGetUpStreamList:Getting the upstream portlist "
                       "failed for VLAN %d\r\n", VlanId);
        return L2DS_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name      : L2dsPortCalcIpCkSum                                  */
/*                                                                           */
/* Description        : This routine calculates the IP header checksum       */
/*                                                                           */
/* Input(s)           : pu1IpHdr - IP Header                                 */
/*                      u4Size - Ip Header Length                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Calculated IP Checksum                               */
/*****************************************************************************/
INT2
L2dsPortCalcIpCkSum (UINT1 *pu1IpHdr, UINT4 u4Size)
{
    return (UtlIpCSumLinBuf ((INT1 *) pu1IpHdr, u4Size));
}

/*****************************************************************************/
/* Function Name      : L2dsPortGetEtherHdrLen                               */
/*                                                                           */
/* Description        : This routine calculates the Ethernet header length   */
/*                                                                           */
/* Input(s)           : pBuf - Pointer to the packet                         */
/*                      u4InPort - Incoming port                             */
/*                                                                           */
/* Output(s)          : pu2EthetOffset - Length of ethernet header           */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsPortGetEtherHdrLen (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4InPort,
                        UINT2 *pu2EthetOffset)
{
    UINT4               u4Offset = L2DS_ZERO;

    if (VlanGetTagLenInFrame (pBuf, (UINT2) u4InPort,
                              &u4Offset) != VLAN_SUCCESS)
    {
        return L2DS_FAILURE;
    }

    *pu2EthetOffset = (UINT2) (u4Offset + L2DS_TWO);
    return L2DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2dsPortGetDefaultCTag                               */
/*                                                                           */
/* Description        : This routine gets the default C-Vlan Tag configured  */
/*                      for the given port                                   */
/*                                                                           */
/* Input(s)           : u4Port - The port for which VLAN ID is to be found   */
/*                                                                           */
/* Output(s)          : pVlanId - C-Vlan Id                                  */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsPortGetDefaultCTag (UINT4 u4Port, tVlanId * pVlanId)
{
    /* TBD: Check whether the function is correct */
    return (L2IwfGetVlanPortPvid ((UINT2) u4Port, pVlanId));
}

/*****************************************************************************/
/* Function Name      : L2dsPortForwardPkt                                   */
/*                                                                           */
/* Description        : Forwards the DHCP packet out                         */
/*                                                                           */
/* Input(s)           : pBuf       - The DHCP Packet                         */
/*                      u4ContextId - Context Identifier                     */
/*                      VlanTag    - VLAN tag informations of the DHCP Packet*/
/*                      u4InPort   - The port through which packet came in   */
/*                      DstMacAddr - Destination MAC address                 */
/*                      SrcMacAddr - Source MAC address                      */
/*                      PortList   - list of the outgoing port               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsPortForwardPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ContextId,
                    tVlanTag VlanTag, UINT4 u4InPort, tMacAddr DstMacAddr,
                    tMacAddr SrcMacAddr, tPortList PortList)
{
    if (IpdbApiProcessPktWithPortList (pBuf, u4ContextId, VlanTag, u4InPort,
                                       DstMacAddr, SrcMacAddr, PortList)
        == IPDB_SUCCESS)
    {
        return L2DS_SUCCESS;
    }
    else
    {
        return L2DS_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name      : L2dsPortUpdateEntry                                  */
/*                                                                           */
/* Description        : This function is to create or delete or update a     */
/*                      binding entry                                        */
/*                                                                           */
/* Input(s)           : pL2DhcpSnpPktInfo - Packet info                      */
/*                      u4ContextId - Context Identifier                     */
/*                      u4InPort - Port to which host is connected           */
/*                      HostMac - Mac address of the host                    */
/*                      u4HostIp - IP address of the host                    */
/*                      u1Status - DELETE/CREATE                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsPortUpdateEntry (tL2DhcpSnpPktInfo * pL2DhcpSnpPktInfo, UINT4 u4ContextId,
                     UINT4 u4InPort, tVlanId CVlanId, tMacAddr HostMac,
                     UINT4 u4HostIp, UINT1 u1Status)
{
    tIpDbEntry          IpDbEntry;
    tIpDbGateway        IpDbGateway;
    tVlanTag            VlanTag;
    UINT4               u4BridgeMode = L2DS_ZERO;

    MEMSET (&VlanTag, L2DS_ZERO, sizeof (tVlanTag));
    MEMSET (&IpDbEntry, L2DS_ZERO, sizeof (tIpDbEntry));
    MEMSET (&IpDbGateway, L2DS_ZERO, sizeof (tIpDbGateway));

    MEMCPY (IpDbEntry.HostMac, HostMac, sizeof (tMacAddr));

    IpDbEntry.u4ContextId = u4ContextId;
    IpDbEntry.u4HostIp = u4HostIp;
    IpDbEntry.u4InIfIndex = u4InPort;

    /* Depends on Bridge-Mode we will Update the IP binding entry :
     * 1)If it is PROVIDER-EDGE or PROVIDER-CORE bridge mode then
     * S-VlanId (Outer VLAN tag) will be updated in the IP Binding
     * table to traverse all the modules with S-VlanId.
     * 2)If is CUSTOMER_BRIDGE the update the CVlanId in the IP 
     * binding table to traverse all the modules with C-VlanId.
     * */
    /* Get the bridge mode configured in L2IWF */
    L2IwfGetBridgeMode (u4ContextId, &u4BridgeMode);

    if ((u4BridgeMode == VLAN_PROVIDER_EDGE_BRIDGE_MODE)
        || (u4BridgeMode == VLAN_PROVIDER_CORE_BRIDGE_MODE))
    {
        IpDbEntry.VlanId = VlanTag.OuterVlanTag.u2VlanId;
    }
    else
    {
        IpDbEntry.VlanId = CVlanId;
    }

    IpDbEntry.u1BindingType = IPDB_DHCP_BINDING;

    if (u1Status == L2DS_IPDB_CREATE)
    {
        IpDbEntry.u4LeaseTime = pL2DhcpSnpPktInfo->u4LeaseDuration;
        IpDbEntry.u4BindingId = (pL2DhcpSnpPktInfo->L2DhcpSnpPacket).u4Xid;
        IpDbGateway.u4GatewayIp = pL2DhcpSnpPktInfo->u4DefaultGwIP;

        if (IpdbApiUpdateBindingEntry
            (&IpDbEntry, L2DS_ONE, &IpDbGateway, IPDB_CREATE_ENTRY)
            != IPDB_SUCCESS)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsPortUpdateEntry:Failure is returned from IPDB "
                      "Module\r\n");
            return L2DS_FAILURE;
        }

        return L2DS_SUCCESS;
    }

    if (u1Status == L2DS_IPDB_DELETE)
    {
        if (IpdbApiUpdateBindingEntry (&IpDbEntry, L2DS_ZERO, NULL,
                                       IPDB_DELETE_ENTRY) != IPDB_SUCCESS)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsPortUpdateEntry:Failure is returned from IPDB "
                      "Module\r\n");

            return L2DS_FAILURE;
        }
        return L2DS_SUCCESS;
    }

    return L2DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2dsPortDeleteEntries                                */
/*                                                                           */
/* Description        : This function is to delete all the binding entries   */
/*                      associated with a VLAN                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                    */
/*                      VlanId      - VLAN Identifier                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsPortDeleteEntries (UINT4 u4ContextId, tVlanId VlanId)
{
    if (IpdbApiDeleteBindingEntries (IPDB_DHCP_BINDING, u4ContextId, VlanId)
        != IPDB_SUCCESS)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPortDeleteEntries:Failure is returned from IPDB Module"
                  "\r\n");
        return L2DS_FAILURE;
    }
    return L2DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2dsPortGetDslOptions                                */
/*                                                                           */
/* Description        : This function is get DSL Options from CFA Module     */
/*                                                                           */
/* Input(s)           : u4Port - Incoming port                               */
/*                                                                           */
/* Output(s)          : pu1DslOptions - DSL sub-options                     */
/*                      pu1DslLen - DSL sub-option length                    */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsPortGetDslOptions (UINT1 *pu1DslOptions, UINT1 *pu1DslLen, UINT4 u4Port,
                       tVlanTag VlanTag)
{
    UINT1               u1Offset = L2DS_ZERO;
    UINT4               u4EntrpriseId = L2DS_ZERO;
    UINT1               u1IfType = L2DS_ZERO;
    UINT1               u1DsloptionLen = L2DS_ZERO;
    UINT1               au1DslOptions[DCS_DSL_OPTION_LEN + L2DS_ONE]
        = { L2DS_ZERO };

    /* Fill the sub-option type */
    pu1DslOptions[u1Offset++] = L2DS_SUBOPT_DSL;

    /* We will fill the length later */
    u1Offset++;

    u4EntrpriseId = L2DS_SUBOPT_ENTERPRISE_ID;
    u4EntrpriseId = OSIX_HTONL (u4EntrpriseId);

    MEMCPY ((pu1DslOptions + u1Offset), &u4EntrpriseId, L2DS_ENTERPRISE_ID_LEN);

    u1Offset = (UINT1) (u1Offset + L2DS_ENTERPRISE_ID_LEN);

    /* We will fill the length later */
    u1Offset++;

    CfaGetIfaceType (u4Port, &u1IfType);

    if (DcsUtilGetDslLineCharacteristics
        (u4Port, au1DslOptions, &u1DsloptionLen, VlanTag) == DCS_FAILURE)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPortGetDslOptions: Failure is returned from the "
                  "DCS module for geting the DSL line Characteristics.\r\n");

        return L2DS_FAILURE;
    }

    *pu1DslLen = (UINT1) (u1Offset + u1DsloptionLen);
    /* Fill the DSL sub-option length field */
    pu1DslOptions[L2DS_ONE] = (UINT1) (u1DsloptionLen +
                                       L2DS_ENTERPRISE_ID_LEN + L2DS_ONE);

    pu1DslOptions[L2DS_SUB_OPTION_DATA_LEN] = u1DsloptionLen;
    MEMCPY ((pu1DslOptions + u1Offset), au1DslOptions, DCS_DSL_OPTION_LEN);

    return L2DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2dsApiCreateContext                                 */
/*                                                                           */
/* Description        : This function is invoked by l2iwf Module to Create   */
/*                      Context                                              */
/*                                                                           */
/* Input(s)           : Context Identifier                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

PUBLIC VOID
L2dsApiCreateContext (UINT4 u4ContextId)
{
    /* Set the Global status and MacVerification status */
    L2DS_ADMIN_STATUS (u4ContextId) = L2DS_DISABLED;
    L2DS_MAC_VERIFY_STATUS (u4ContextId) = L2DS_ENABLED;
    L2DS_IS_CONTEXT_EXIST (u4ContextId) = L2DS_TRUE;
    return;
}

/*****************************************************************************/
/* Function Name      : L2dsApiDeleteContext                                 */
/*                                                                           */
/* Description        : This function is invoked by l2iwf Module to Delete   */
/*                      Context                                              */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

PUBLIC VOID
L2dsApiDeleteContext (UINT4 u4ContextId)
{
    /* Set the Global status and MacVerification status */
    L2DS_ADMIN_STATUS (u4ContextId) = L2DS_DISABLED;
    L2DS_MAC_VERIFY_STATUS (u4ContextId) = L2DS_ENABLED;
    L2DS_IS_CONTEXT_EXIST (u4ContextId) = L2DS_FALSE;
    return;
}

/*                                                                           */
/***************************** END OF FILE ***********************************/
/*                                                                           */
