/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: l2dsintf.c,v 1.3 2010/08/04 08:51:03 prabuc Exp $               */
/*****************************************************************************/
/*    FILE  NAME            : l2dsintf.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : L2DHCP Snooping                                */
/*    MODULE NAME           : L2DHCP Snooping Interface Module               */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains interface related functions */
/*                            for L2DHCP Snooping module                     */
/*---------------------------------------------------------------------------*/

#include "l2dsinc.h"
#include "l2dsextn.h"

/*****************************************************************************/
/* Function Name      : L2dsIntfDeleteIntfEntry                              */
/*                                                                           */
/* Description        : This function is to delete an interface entry        */
/*                                                                           */
/* Input(s)           : u4L2dsCxtId - Context Identifier                     */
/*                      VlanId     - Incoming VLAN Id                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsIntfDeleteIntfEntry (UINT4 u4L2dsCxtId, tVlanId VlanId)
{
    tL2DhcpSnpIfaceEntry L2DhcpSnpIfaceEntry;
    tL2DhcpSnpIfaceEntry *pL2DhcpSnpIfaceEntry = NULL;
    INT4                i4RetStat = L2DS_SUCCESS;

    MEMSET (&L2DhcpSnpIfaceEntry, L2DS_ZERO, sizeof (tL2DhcpSnpIfaceEntry));

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_ENTRY, L2DS_MODULE_NAME,
                   "L2dsIntfDeleteIntfEntry:Entering the function %s \r\n",
                   __FUNCTION__);

    L2DS_TRC_ARG2 (L2DS_TRC_FLAG, L2DS_FN_ARGS, L2DS_MODULE_NAME,
                   "L2dsIntfDeleteIntfEntry:The arguments to the function %s "
                   "is VlanId %d \r\n", __FUNCTION__, VlanId);

    L2DhcpSnpIfaceEntry.u4L2dsCxtId = u4L2dsCxtId;
    L2DhcpSnpIfaceEntry.VlanId = VlanId;

    /* Get the interface entry */
    pL2DhcpSnpIfaceEntry = (tL2DhcpSnpIfaceEntry *)
        RBTreeGet (L2DS_INTF_RBTREE, &L2DhcpSnpIfaceEntry);

    if (pL2DhcpSnpIfaceEntry == NULL)
    {
        L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                       "L2dsIntfDeleteIntfEntry:Unable to find the entry for "
                       "VLAN %d.\r\n", VlanId);
        return L2DS_SUCCESS;
    }
    /* Clear all entries in the binding database, 
     * which are based on this VLAN */
    if (L2DS_INT_ROWSTATUS (pL2DhcpSnpIfaceEntry) == L2DS_ACTIVE)
    {
        if (L2dsPortDeleteEntries (u4L2dsCxtId, VlanId) != L2DS_SUCCESS)
        {
            L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                           "L2dsIntfDeleteIntfEntry:Unable to clear the binding"
                           " entried for hosts on VLAN %d.\r\n", VlanId);
            i4RetStat = L2DS_FAILURE;
        }
    }
    /* remove the entry from RBTree */
    if (RBTreeRemove (L2DS_INTF_RBTREE,
                      (tRBElem *) pL2DhcpSnpIfaceEntry) != RB_SUCCESS)
    {
        L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                       "L2dsIntfDeleteIntfEntry:Removal from RBTree failed for "
                       "VLAN %d.\r\n", VlanId);
        i4RetStat = L2DS_FAILURE;
    }
    /* Release the memory back to pool */
    L2DS_RELEASE_MEM_BLOCK (L2DS_INTF_POOL_ID, pL2DhcpSnpIfaceEntry);

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT, L2DS_MODULE_NAME,
                   "L2dsIntfDeleteIntfEntry:Exiting the function %s \r\n",
                   __FUNCTION__);
    return i4RetStat;
}

/*****************************************************************************/
/* Function Name      : L2dsIntfCreateIntfEntry                              */
/*                                                                           */
/* Description        : This function is to create an interface entry        */
/*                                                                           */
/* Input(s)           : u4L2dsCxtId - Context Identifier                     */
/*                      VlanId     - Incoming VLAN Id                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NULL/Pointer to interface entry                      */
/*****************************************************************************/
tL2DhcpSnpIfaceEntry *
L2dsIntfCreateIntfEntry (UINT4 u4L2dsCxtId, tVlanId VlanId)
{
    tL2DhcpSnpIfaceEntry L2DhcpSnpIfaceEntry;
    tL2DhcpSnpIfaceEntry *pL2DhcpSnpIfaceEntry = NULL;

    MEMSET (&L2DhcpSnpIfaceEntry, L2DS_ZERO, sizeof (tL2DhcpSnpIfaceEntry));

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_ENTRY, L2DS_MODULE_NAME,
                   "L2dsIntfCreateIntfEntry:Entering the function %s \r\n",
                   __FUNCTION__);

    L2DS_TRC_ARG2 (L2DS_TRC_FLAG, L2DS_FN_ARGS, L2DS_MODULE_NAME,
                   "L2dsIntfCreateIntfEntry:The arguments to the function %s "
                   "are VlanId %d \r\n ", __FUNCTION__, VlanId);

    L2DhcpSnpIfaceEntry.u4L2dsCxtId = u4L2dsCxtId;
    L2DhcpSnpIfaceEntry.VlanId = VlanId;

    /* Get the interface entry */
    pL2DhcpSnpIfaceEntry = (tL2DhcpSnpIfaceEntry *)
        RBTreeGet (L2DS_INTF_RBTREE, &L2DhcpSnpIfaceEntry);

    if (pL2DhcpSnpIfaceEntry != NULL)
    {
        return pL2DhcpSnpIfaceEntry;
    }

    /* Allocate memory for the interface entry */
    if ((pL2DhcpSnpIfaceEntry = (tL2DhcpSnpIfaceEntry *)
         (L2DS_ALLOC_MEM_BLOCK (L2DS_INTF_POOL_ID))) == NULL)
    {
        L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                       "L2dsIntfCreateIntfEntry:Allocation of memory for "
                       "interface entry of VLAN %d failed.\r\n", VlanId);
        return NULL;
    }

    MEMSET (pL2DhcpSnpIfaceEntry, L2DS_ZERO, sizeof (tL2DhcpSnpIfaceEntry));

    pL2DhcpSnpIfaceEntry->u4L2dsCxtId = u4L2dsCxtId;
    pL2DhcpSnpIfaceEntry->VlanId = VlanId;
    pL2DhcpSnpIfaceEntry->u1VlanSnpStatus = L2DS_DISABLED;

    /* Add the entry to RBTree */
    if (RBTreeAdd (L2DS_INTF_RBTREE, (tRBElem *) pL2DhcpSnpIfaceEntry) ==
        RB_FAILURE)
    {
        L2DS_RELEASE_MEM_BLOCK (L2DS_INTF_POOL_ID, pL2DhcpSnpIfaceEntry);

        L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                       "L2dsIntfCreateIntfEntry:Addition of interface entry of "
                       "VLAN %d to RBTree failed .\r\n", VlanId);
        return NULL;
    }

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT, L2DS_MODULE_NAME,
                   "L2dsIntfCreateIntfEntry:Successfully exiting the function "
                   "%s \r\n", __FUNCTION__);

    return pL2DhcpSnpIfaceEntry;
}

/*                                                                           */
/***************************** END OF FILE ***********************************/
/*                                                                           */
