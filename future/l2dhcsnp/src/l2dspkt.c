/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc. All Rights Reserved                       */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: l2dspkt.c,v 1.14 2013/12/18 12:48:02 siva Exp $     */
/*****************************************************************************/
/*    FILE  NAME            : l2dspkt.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : L2DHCP Snooping                                */
/*    MODULE NAME           : L2DHCP Snooping Packet handling Module         */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the packet handling         */
/*                            functions for L2DHCP Snooping module           */
/*---------------------------------------------------------------------------*/

#include "l2dsinc.h"
#include "l2dsextn.h"

/*****************************************************************************/
/* Function Name      : L2dsPktHandleIncomingPkt                             */
/*                                                                           */
/* Description        : This function handles the incoming DHCP Packet.      */
/*                                                                           */
/* Input(s)           : pBuf       - Incoming packet buffer                  */
/*                      u4ContextId- Context Identifier                      */
/*                      u4InPort   - Incoming Port                           */
/*                      VlanTag    - VLAN tag informations of the packet     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsPktHandleIncomingPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ContextId,
                          UINT4 u4InPort, tVlanTag VlanTag)
{
    tL2DhcpSnpIfaceEntry L2DhcpSnpIfaceEntry;
    tL2DhcpSnpIfaceEntry *pL2DhcpSnpIfaceEntry = NULL;
    tMacAddr            DstMacAddress = { L2DS_ZERO };
    tMacAddr            SrcMacAddress = { L2DS_ZERO };
    tVlanId             VlanId = L2DS_ZERO;
    UINT4               u4PktLength = L2DS_ZERO;
    UINT2               u2EtherOffset = L2DS_ZERO;
    UINT1               u1PortType = L2DS_ZERO;

    MEMSET (&L2DhcpSnpIfaceEntry, L2DS_ZERO, sizeof (tL2DhcpSnpIfaceEntry));

    VlanId = VlanTag.OuterVlanTag.u2VlanId;

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_ENTRY, L2DS_MODULE_NAME,
                   "L2dsPktHandleIncomingPkt:Entering the function %s \r\n",
                   __FUNCTION__);

    L2DS_TRC_ARG3 (L2DS_TRC_FLAG, L2DS_FN_ARGS, L2DS_MODULE_NAME,
                   "L2dsPktHandleIncomingPkt:The arguments to the function %s "
                   "are VlanId %d InPort %d\n and address of CRU buffer\r\n",
                   __FUNCTION__, VlanId, u4InPort);

    L2DhcpSnpIfaceEntry.u4L2dsCxtId = u4ContextId;
    L2DhcpSnpIfaceEntry.VlanId = VlanId;
    /* Get the corresponding VLAN interface entry */
    pL2DhcpSnpIfaceEntry = (tL2DhcpSnpIfaceEntry *)
        RBTreeGet (L2DS_INTF_RBTREE, (tRBElem *) & L2DhcpSnpIfaceEntry);

    /* If there is no interface entry, broadcast the packet on the VLAN */
    if (pL2DhcpSnpIfaceEntry == NULL)
    {
        L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                       "L2dsPktHandleIncomingPkt:Interface entry for VLAN: %d "
                       "does not exist. So forwarding "
                       "the packet on all the member ports\r\n", VlanId);

        L2DS_COPY_FROM_BUF (pBuf, DstMacAddress, L2DS_ZERO, sizeof (tMacAddr));
        L2DS_COPY_FROM_BUF (pBuf, SrcMacAddress, sizeof (tMacAddr),
                            sizeof (tMacAddr));

        if (L2dsPortForwardPkt (pBuf, u4ContextId, VlanTag, u4InPort,
                                DstMacAddress, SrcMacAddress, NULL)
            == L2DS_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return L2DS_FAILURE;
        }

        return L2DS_SUCCESS;
    }

    /* Check the rowstatus of the intercface entry; If status is not active, 
     * broadcast the packet on the VLAN */
    if (pL2DhcpSnpIfaceEntry->u1RowStatus != L2DS_ACTIVE)
    {
        L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                       "L2dsPktHandleIncomingPkt:Interface entry for VLAN: %d "
                       "is not in active state. So forwarding the packet on all"
                       " the member ports\r\n", VlanId);

        L2DS_COPY_FROM_BUF (pBuf, DstMacAddress, L2DS_ZERO, sizeof (tMacAddr));
        L2DS_COPY_FROM_BUF (pBuf, SrcMacAddress, sizeof (tMacAddr),
                            sizeof (tMacAddr));

        if (L2dsPortForwardPkt (pBuf, u4ContextId, VlanTag, u4InPort,
                                DstMacAddress, SrcMacAddress, NULL)
            == L2DS_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return L2DS_FAILURE;
        }
        return L2DS_SUCCESS;
    }

    /* If L2 DHCP Snooping is not enabled on the VLAN, broadcast the packet on
     * the VLAN */
    if (pL2DhcpSnpIfaceEntry->u1VlanSnpStatus != L2DS_ENABLED)
    {
        L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                       "L2dsPktHandleIncomingPkt:L2Dhcp Snooping is not "
                       "enabled on the VLAN: %d. So forwarding the packet on "
                       "all the member ports\r\n", VlanId);

        L2DS_COPY_FROM_BUF (pBuf, DstMacAddress, L2DS_ZERO, sizeof (tMacAddr));
        L2DS_COPY_FROM_BUF (pBuf, SrcMacAddress, sizeof (tMacAddr),
                            sizeof (tMacAddr));

        if (L2dsPortForwardPkt (pBuf, u4ContextId, VlanTag, u4InPort,
                                DstMacAddress, SrcMacAddress, NULL)
            == L2DS_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return L2DS_FAILURE;
        }

        return L2DS_SUCCESS;
    }

    /* Get the type of the incoming port */
    if (L2dsPortGetPortType ((UINT2) u4InPort, &u1PortType) != L2DS_SUCCESS)
    {
        L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                       "L2dsPktHandleIncomingPkt:Getting the port-type of port "
                       "%d failed.\r\n", u4InPort);

        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return L2DS_FAILURE;
    }

    MEMSET (gpau1L2dsDataBuffer, L2DS_ZERO, L2DS_MAX_MTU);

    /* Get the length of ethernet header (including VLAN tags) */
    if (L2dsPortGetEtherHdrLen (pBuf, u4InPort, &u2EtherOffset) != L2DS_SUCCESS)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPktHandleIncomingPkt:Failed to get the ethernet header "
                  "length.\r\n");

        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return L2DS_FAILURE;
    }

    /* Get the length of the packet */
    u4PktLength = CRU_BUF_Get_ChainValidByteCount (pBuf);

    /* Copy the CRU buffer contents in to a linear buffer */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) gpau1L2dsDataBuffer, L2DS_ZERO,
                               u4PktLength);

    /* Release the CRU Buffer */
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

    if (u1PortType == L2DS_DOWNSTREAM_PORT)
    {
        /* If the port is a downstream port, handle the packets as coming 
         * from a host */
        if (L2dsPktProcessPktFromHost (gpau1L2dsDataBuffer,
                                       pL2DhcpSnpIfaceEntry,
                                       u4ContextId, u4InPort,
                                       VlanTag, u2EtherOffset) != L2DS_SUCCESS)
        {
            L2DS_TRC_ARG2 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                           "L2dsPktHandleIncomingPkt:Handling of the incoming "
                           "DHCP Packet on VLAN %d, Port %d failed.\r\n",
                           VlanId, u4InPort);

            return L2DS_FAILURE;
        }
    }
    else if (u1PortType == L2DS_UPSTREAM_PORT)
    {
        /* If the port is a upstream port, handle the packets as coming
         * from the server */
        if (L2dsPktProcessPktFromSrvr (gpau1L2dsDataBuffer, u4ContextId,
                                       u4InPort, VlanTag, u2EtherOffset)
            != L2DS_SUCCESS)
        {
            L2DS_TRC_ARG2 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                           "L2dsPktHandleIncomingPkt:Handling of the incoming "
                           "DHCP Packet on VLAN %d, Port %d failed.\r\n",
                           VlanId, u4InPort);
            return L2DS_FAILURE;
        }
    }

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT, L2DS_MODULE_NAME,
                   "L2dsPktHandleIncomingPkt:Successfully exiting the function "
                   "%s \r\n", __FUNCTION__);
    return L2DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2dsPktProcessPktFromHost                            */
/*                                                                           */
/* Description        : This function handles the incoming DHCP Packet, from */
/*                      the hosts connected to downstream interface          */
/*                                                                           */
/* Input(s)           : pu1RecvBuf           - Incoming packet buffer        */
/*                      pL2DhcpSnpIfaceEntry - Interface entry               */
/*                      u4ContextId          - Context Identifier            */
/*                      u4InPort             - Incoming Port                 */
/*                      VlanTag              - VLAN tag informations of the  */
/*                                             packet                        */
/*                      u2EtherOffset        - Ethernet Header length        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsPktProcessPktFromHost (UINT1 *pu1RecvBuf,
                           tL2DhcpSnpIfaceEntry * pL2DhcpSnpIfaceEntry,
                           UINT4 u4ContextId, UINT4 u4InPort,
                           tVlanTag VlanTag, UINT2 u2EtherOffset)
{

    tL2DhcpSnpPktInfo  *pL2DhcpSnpPktInfo = NULL;
    tL2DhcpSnpOption    L2DhcpSnpOption;
    tPortList          *pPortList = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT1              *pu1PktOffset = NULL;
    UINT1              *pu1Options = NULL;
    tVlanId             VlanId = L2DS_ZERO;
    tVlanId             CVlanId = L2DS_ZERO;
    UINT4               u4PktLength = L2DS_ZERO;
    UINT4               u4NewPktLength = L2DS_ZERO;
    UINT2               u2NewOptLen = L2DS_ZERO;
    UINT2               u2PktChgLen = L2DS_ZERO;
    UINT2               u2CheckSum = L2DS_ZERO;
    UINT2               u2Offset = L2DS_ZERO;
    UINT2               u2AddedLength = L2DS_ZERO;
    UINT1               u1PortState = L2DS_ZERO;

    if ((pL2DhcpSnpPktInfo = (tL2DhcpSnpPktInfo *)
         (L2DS_ALLOC_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID))) == NULL)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPktProcessPktFromSrvr:Memory Allocation failed for Message"
                  "\r\n");
        return L2DS_FAILURE;
    }
    MEMSET (pL2DhcpSnpPktInfo, L2DS_ZERO, sizeof (tL2DhcpSnpPktInfo));
    MEMSET (&L2DhcpSnpOption, L2DS_ZERO, sizeof (tL2DhcpSnpOption));

    VlanId = VlanTag.OuterVlanTag.u2VlanId;

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_ENTRY, L2DS_MODULE_NAME,
                   "L2dsPktProcessPktFromHost:Entering the function %s \r\n",
                   __FUNCTION__);

    L2DS_TRC_ARG4 (L2DS_TRC_FLAG, L2DS_FN_ARGS, L2DS_MODULE_NAME,
                   "L2dsPktProcessPktFromHost:The arguments to the function %s "
                   "are VlanId %d, InPort %d, Ethernet header length %d and "
                   "address of received buffer\r\n", __FUNCTION__, VlanId,
                   u4InPort, u2EtherOffset);

    /* Extract the information from the packet */
    if (L2dsPktGetPktInfo (pu1RecvBuf, pL2DhcpSnpPktInfo, u4InPort, VlanId,
                           u2EtherOffset, &u4PktLength) != L2DS_SUCCESS)
    {
        L2DS_TRC_ARG2 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                       "L2dsPktProcessPktFromHost:Getting information of the "
                       "incoming DHCP Packet on VLAN %d, Port %d failed.\r\n",
                       VlanId, u4InPort);
        L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
        return L2DS_FAILURE;
    }

    /* Get the port-state (trusted or untrusted) */
    if (L2dsPortGetPortState ((UINT2) u4InPort, &u1PortState) != L2DS_SUCCESS)
    {
        L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                       "L2dsPktProcessPktFromHost:Getting the port state "
                       "of the Port %d failed.\r\n", u4InPort);
        L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
        return L2DS_FAILURE;
    }

    pPortList = (tPortList *) (FsUtilAllocBitList (sizeof (tPortList)));
    if (pPortList == NULL)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPktProcessPktFromHost:Memory Allocation failed for Message"
                  "\r\n");
        L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
        return L2DS_FAILURE;
    }
    MEMSET (pPortList, L2DS_ZERO, sizeof (tPortList));

    if (u1PortState == L2DS_TRUSTED_PORT)
    {
        /* The port is a trusted port. So broadcast the packet out through 
         * upstream interfaces, if the packet is a broadcast packet; otherwise 
         * sent the packet out through the corresponding upstream interface. */

        /* Convert the packet back to cru-buffer */
        pBuf = L2dsUtilLinearToCRU (pu1RecvBuf, u4PktLength);
        if (pBuf == NULL)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsPktProcessPktFromHost:Unable to convert the linear "
                      "buffer to CRU-buffer\r\n");
            FsUtilReleaseBitList ((UINT1 *) pPortList);
            L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
            return L2DS_FAILURE;
        }

        /* For a broadcast packet, get the upstream list and sent the 
         * packet out through all the upstream ports. For a unicast packet,
         * we have to sent the packet on the correct interface, but if we
         * dont, know the exact port, it should be broadcasted on the 
         * upstream ports. */
        if (L2dsPortGetUpStreamList (u4ContextId, VlanId, *pPortList) !=
            L2DS_SUCCESS)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsPktProcessPktFromHost:Unable to get the upstream"
                      " portlist\r\n");

            /* Release the CRU Buffer */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            FsUtilReleaseBitList ((UINT1 *) pPortList);
            L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
            return L2DS_FAILURE;
        }

        /* Send the packet out through the upstream interface(s) */
        if (L2dsPortForwardPkt (pBuf, u4ContextId, VlanTag, u4InPort,
                                pL2DhcpSnpPktInfo->DstMacAddress,
                                pL2DhcpSnpPktInfo->SrcMacAddress,
                                *pPortList) != L2DS_SUCCESS)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsPktProcessPktFromHost:Unable to send the packet "
                      "out through upstream interfaces.\r\n");

            /* Release the CRU Buffer */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            FsUtilReleaseBitList ((UINT1 *) pPortList);
            L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
            return L2DS_FAILURE;
        }
    }
    else if (u1PortState == L2DS_UNTRUSTED_PORT)
    {
        /* The packet is coming through un-trusted port. So do the necessary 
         * validations, add options and sent the packet out through upstream 
         * interfaces, if the packet is a broadcast packet; otherwise sent the 
         * packet out through the corresponding upstream interface.
         *
         * ASSUMPTION: The packet validation, as whether the packet is 
         * allowed, based on DstMac/Gateway IP should be done by VLAN Module */

        /* If the incoming packet is not a packet that is supposed to come 
         * from a DHCP client, drop the packet */
        switch (pL2DhcpSnpPktInfo->u1PktType)
        {
            case L2DS_DHCP_DISCOVER:
            case L2DS_DHCP_DECLINE:
            case L2DS_DHCP_RELEASE:
            case L2DS_DHCP_INFORM:
            case L2DS_DHCP_REQUEST:
                break;
            default:

                L2DS_TRC_ARG2 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                               "L2dsPktProcessPktFromHost:Incoming DHCP Packet "
                               "on VLAN %d, Port %d Dropped.\r\n", VlanId,
                               u4InPort);

                /* Update the statistics */
                L2DS_INCR_STAT_TOTAL_DISC (pL2DhcpSnpIfaceEntry);
                L2DS_INCR_STAT_SRV_DISC (pL2DhcpSnpIfaceEntry);
                FsUtilReleaseBitList ((UINT1 *) pPortList);
                L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID,
                                        pL2DhcpSnpPktInfo);
                return L2DS_FAILURE;
        }

        /* Update the VLAN statistics */
        L2dsPktUpdateStat (pL2DhcpSnpPktInfo->u1PktType, u4ContextId, VlanId);

        /* If the mac verify status is enabled, check the source MAC address 
         * against the client hardware address inside the DHCP Header */
        if (L2DS_MAC_VERIFY_STATUS (u4ContextId) == L2DS_ENABLED)
        {
            if (MEMCMP (pL2DhcpSnpPktInfo->SrcMacAddress,
                        pL2DhcpSnpPktInfo->L2DhcpSnpPacket.Chaddr,
                        sizeof (tMacAddr)) != L2DS_ZERO)
            {
                L2DS_TRC_ARG2 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                               "L2dsPktProcessPktFromHost:Incoming DHCP Packet "
                               "on VLAN %d, Port %d dropped due to failure in "
                               "MAC address verification\r\n", VlanId,
                               u4InPort);

                /* Update the statistics */
                L2DS_INCR_STAT_TOTAL_DISC (pL2DhcpSnpIfaceEntry);
                L2DS_INCR_STAT_MAC_DISC (pL2DhcpSnpIfaceEntry);
                FsUtilReleaseBitList ((UINT1 *) pPortList);
                L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID,
                                        pL2DhcpSnpPktInfo);
                return L2DS_FAILURE;
            }
        }

        /* Check whether the option 82 is already added or giaddr is set */
        if (pL2DhcpSnpPktInfo->L2DhcpSnpPacket.u4Giaddr != L2DS_ZERO)
        {
            L2DS_TRC_ARG2 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                           "L2dsPktProcessPktFromHost:Incoming DHCP Packet on "
                           "VLAN %d, Port %d dropped  because Giaddr field is "
                           "set\r\n", VlanId, u4InPort);

            /* Update the statistics */
            L2DS_INCR_STAT_TOTAL_DISC (pL2DhcpSnpIfaceEntry);
            L2DS_INCR_STAT_OPT_DISC (pL2DhcpSnpIfaceEntry);
            FsUtilReleaseBitList ((UINT1 *) pPortList);
            L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
            return L2DS_FAILURE;
        }

        if (L2dsUtilGetOption (&(pL2DhcpSnpPktInfo->L2DhcpSnpPacket),
                               &L2DhcpSnpOption, L2DS_OPT_RAI) == L2DS_FOUND)
        {
            L2DS_TRC_ARG2 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                           "L2dsPktProcessPktFromHost:Incoming DHCP Packet on "
                           "VLAN %d, Port %d dropped because Option 82 was "
                           "found in the packet\r\n", VlanId, u4InPort);

            MEMSET (L2DhcpSnpOption.pu1Val, L2DS_ZERO, L2DS_OPTION_LEN);
            /* Update the statistics */
            L2DS_INCR_STAT_TOTAL_DISC (pL2DhcpSnpIfaceEntry);
            L2DS_INCR_STAT_OPT_DISC (pL2DhcpSnpIfaceEntry);
            FsUtilReleaseBitList ((UINT1 *) pPortList);
            L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
            return L2DS_FAILURE;
        }

        /* Add option-82 and vendor specific options.
         * Even if the addition of options fail, we will send the packet out */
        if (L2dsUtilAddOptions (pL2DhcpSnpPktInfo, &u2AddedLength,
                                VlanTag) != L2DS_SUCCESS)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsPktProcessPktFromHost:addition of option-82 and "
                      "vendor specific option failed.\r\n");
        }
        else
        {
            /* Copy the new options on to the packet */
            pu1Options = pL2DhcpSnpPktInfo->L2DhcpSnpPacket.au1Options;
            u2NewOptLen = pL2DhcpSnpPktInfo->L2DhcpSnpPacket.u2OptLen;

            u2Offset = (UINT2) L2DS_DHCP_OPT_OFFSET ((pL2DhcpSnpPktInfo));
            pu1PktOffset = pu1RecvBuf + u2Offset;

            MEMCPY (pu1PktOffset, pu1Options, u2NewOptLen);
            u4NewPktLength = u4PktLength + (UINT4) u2AddedLength;

            /* Update the ip length field */
            u2Offset = (UINT2) L2DS_IP_LENGTH_OFFSET ((pL2DhcpSnpPktInfo));
            pu1PktOffset = pu1RecvBuf + u2Offset;

            MEMCPY (&u2PktChgLen, pu1PktOffset, L2DS_IP_DGRAM_LEN);

            u2PktChgLen = OSIX_NTOHS (u2PktChgLen);
            u2PktChgLen += u2AddedLength;
            u2PktChgLen = OSIX_HTONS (u2PktChgLen);

            MEMCPY (pu1PktOffset, &u2PktChgLen, L2DS_IP_DGRAM_LEN);

            /* Calculate the new IP Header checksum */
            u2Offset = (UINT2) L2DS_IP_HDR_CKSUM_OFFSET ((pL2DhcpSnpPktInfo));
            pu1PktOffset = pu1RecvBuf + u2Offset;

            MEMSET (pu1PktOffset, L2DS_ZERO, L2DS_IP_HDR_CKSUM_LEN);

            u2CheckSum = L2dsPortCalcIpCkSum
                ((pu1RecvBuf + (pL2DhcpSnpPktInfo->u1EtherHdrLen)),
                 (pL2DhcpSnpPktInfo->u1IpHdrLen));

            u2CheckSum = OSIX_HTONS (u2CheckSum);

            MEMCPY (pu1PktOffset, &u2CheckSum, L2DS_IP_HDR_CKSUM_LEN);

            /* Update the udp-header length field */
            u2Offset = (UINT2) L2DS_UDP_HDR_LEN_OFFSET ((pL2DhcpSnpPktInfo));
            pu1PktOffset = pu1RecvBuf + u2Offset;

            MEMCPY (&u2PktChgLen, pu1PktOffset, L2DS_UDP_DGRAM_LEN);

            u2PktChgLen = OSIX_NTOHS (u2PktChgLen);
            u2PktChgLen += u2AddedLength;
            u2PktChgLen = OSIX_HTONS (u2PktChgLen);

            MEMCPY (pu1PktOffset, &u2PktChgLen, L2DS_UDP_DGRAM_LEN);

            /* Put the UDP Checksum to 0. */
            u2Offset = (UINT2) L2DS_UDP_CKSUM_OFFSET ((pL2DhcpSnpPktInfo));
            pu1PktOffset = pu1RecvBuf + u2Offset;

            MEMSET (pu1PktOffset, L2DS_ZERO, L2DS_UDP_CKSUM_LEN);

            /* Calculate the new Checksum */
            u2CheckSum =
                L2dsUtilCalcUdpCkSum (OSIX_NTOHS (u2PktChgLen),
                                      pL2DhcpSnpPktInfo->u4SrcAddr,
                                      pL2DhcpSnpPktInfo->u4DstAddr,
                                      (pu1RecvBuf +
                                       pL2DhcpSnpPktInfo->u1EtherHdrLen +
                                       pL2DhcpSnpPktInfo->u1IpHdrLen));

            MEMCPY (pu1PktOffset, &u2CheckSum, L2DS_UDP_CKSUM_LEN);
        }

        /* If the packet is a DHCP RELEASE or DECLINE, update the database */
        if ((pL2DhcpSnpPktInfo->u1PktType == L2DS_DHCP_RELEASE) ||
            (pL2DhcpSnpPktInfo->u1PktType == L2DS_DHCP_DECLINE))
        {
            /* Get the C-VLAN ID from the Packet.
             * 1)Incase of Provider-Bridge-mode the InnerVlanTag will be the 
             * C-VlanId.
             * 2)Incase of Customer-Bridge-mode the InnerVlanTag will be Zero.
             **/
            CVlanId = VlanTag.InnerVlanTag.u2VlanId;
            if (CVlanId == L2DS_ZERO)
            {
                /* Failed to get the C-VLAN ID from the packet. So get the default
                 * PVID configured for the port */
                if (L2dsPortGetDefaultCTag (pL2DhcpSnpPktInfo->u4PortNumber,
                                            &CVlanId) == L2DS_FAILURE)
                {
                    FsUtilReleaseBitList ((UINT1 *) pPortList);
                    L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID,
                                            pL2DhcpSnpPktInfo);
                    return L2DS_FAILURE;
                }
            }

            /* update the database */
            if (L2dsPortUpdateEntry
                (pL2DhcpSnpPktInfo, u4ContextId, u4InPort, CVlanId,
                 (pL2DhcpSnpPktInfo->L2DhcpSnpPacket).Chaddr,
                 (pL2DhcpSnpPktInfo->L2DhcpSnpPacket).u4Ciaddr,
                 L2DS_IPDB_DELETE) == L2DS_FAILURE)
            {
                L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                          "L2dsPktProcessPktFromHost:updation of the binding "
                          "database failed.\r\n");
            }
        }

        /* Convert the packet to a CRU-buffer */
        pBuf = L2dsUtilLinearToCRU (pu1RecvBuf, u4NewPktLength);
        if (pBuf == NULL)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsPktProcessPktFromHost:Unable to convert the linear "
                      "buffer to CRU-buffer\r\n");
            FsUtilReleaseBitList ((UINT1 *) pPortList);
            L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
            return L2DS_FAILURE;
        }

        /* For a broadcast packet, get the upstream list and sent the
         * packet out through all the upstream ports. For a unicast packet,
         * we have to sent the packet on the correct interface, but if we
         * dont, know the exact port, it should be broadcasted on the
         * upstream ports. */
        if (L2dsPortGetUpStreamList (u4ContextId, VlanId, *pPortList) !=
            L2DS_SUCCESS)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsPktProcessPktFromHost:Unable to get the upstream"
                      " portlist\r\n");

            /* Release the CRU Buffer */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            FsUtilReleaseBitList ((UINT1 *) pPortList);
            L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
            return L2DS_FAILURE;
        }

        /* send the packet out through the upstream interface(s) */
        if (L2dsPortForwardPkt (pBuf, u4ContextId, VlanTag, u4InPort,
                                pL2DhcpSnpPktInfo->DstMacAddress,
                                pL2DhcpSnpPktInfo->SrcMacAddress,
                                *pPortList) != L2DS_SUCCESS)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsPktProcessPktFromHost:Unable to send the packet "
                      "out through upstream interfaces.\r\n");

            /* Release the CRU Buffer */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            FsUtilReleaseBitList ((UINT1 *) pPortList);
            L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
            return L2DS_FAILURE;
        }
    }
    else
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPktProcessPktFromHost:Invalid port state.\r\n");
        FsUtilReleaseBitList ((UINT1 *) pPortList);
        L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
        return L2DS_FAILURE;
    }

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT, L2DS_MODULE_NAME,
                   "L2dsPktProcessPktFromHost:Exiting the function %s \r\n",
                   __FUNCTION__);
    FsUtilReleaseBitList ((UINT1 *) pPortList);
    L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
    return L2DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2dsPktGetPktInfo                                    */
/*                                                                           */
/* Description        : This function do get the informations from the DHCP  */
/*                      packet                                               */
/*                                                                           */
/* Input(s)           : pu1RecvBuf            - Incoming packet buffer       */
/*                      u4InPort              - Incoming Port                */
/*                      VlanId                - Incoming VLAN ID             */
/*                      u2EtherOffset         - Ethernet Header length       */
/*                                                                           */
/* Output(s)          : pL2DhcpSnpPktInfo     - packet informations          */
/*                      pu4PktLength          - total packet length          */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsPktGetPktInfo (UINT1 *pu1RecvBuf, tL2DhcpSnpPktInfo * pL2DhcpSnpPktInfo,
                   UINT4 u4InPort, tVlanId VlanId, UINT2 u2EtherOffset,
                   UINT4 *pu4PktLength)
{
    tL2DhcpSnpOption    L2DhcpSnpOption;
    UINT1              *pu1PktOffset = NULL;
    UINT2               u2Offset = L2DS_ZERO;
    UINT2               u2IpPktLen = L2DS_ZERO;
    UINT1               u1IpHdrLen = L2DS_ZERO;

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_ENTRY, L2DS_MODULE_NAME,
                   "L2dsPktGetPktInfo:Entering the function %s \r\n",
                   __FUNCTION__);

    MEMSET (&L2DhcpSnpOption, L2DS_ZERO, sizeof (tL2DhcpSnpOption));
    MEMSET (pL2DhcpSnpPktInfo, L2DS_ZERO, sizeof (tL2DhcpSnpPktInfo));

    pL2DhcpSnpPktInfo->u4PortNumber = u4InPort;
    pL2DhcpSnpPktInfo->VlanId = VlanId;
    pL2DhcpSnpPktInfo->u1EtherHdrLen = (UINT1) u2EtherOffset;

    /* Get the destination mac from the ethernet header */
    MEMCPY (pL2DhcpSnpPktInfo->DstMacAddress, pu1RecvBuf, sizeof (tMacAddr));

    /* Get the source mac from the ethernet header */
    u2Offset = L2DS_SRCMAC_OFFSET;
    pu1PktOffset = pu1RecvBuf + u2Offset;

    MEMCPY (pL2DhcpSnpPktInfo->SrcMacAddress, pu1PktOffset, sizeof (tMacAddr));

    /* Get the IP header length, which will be the last four bits of the first 
     * byte of the IP Header */
    u1IpHdrLen = *(pu1RecvBuf + u2EtherOffset);
    u1IpHdrLen = (UINT1) ((u1IpHdrLen & L2DS_IP_HDRLEN_BITMASK) *
                          L2DS_BYTE_IN_WORD);

    pL2DhcpSnpPktInfo->u1IpHdrLen = u1IpHdrLen;

    /* Get the total length of the Packet */
    u2Offset = (UINT2) L2DS_IP_LENGTH_OFFSET (pL2DhcpSnpPktInfo);
    pu1PktOffset = pu1RecvBuf + u2Offset;

    MEMCPY (&u2IpPktLen, pu1PktOffset, L2DS_IP_DGRAM_LEN);
    u2IpPktLen = OSIX_NTOHS (u2IpPktLen);

    *pu4PktLength = (UINT4) u2EtherOffset + (UINT4) u2IpPktLen;

    /* Get the Src IP address and Destination IP Address 
     * (This is for calculating checksum) */
    u2Offset = (UINT2) L2DS_IPSRC_ADDR_OFFSET (pL2DhcpSnpPktInfo);
    pu1PktOffset = pu1RecvBuf + u2Offset;

    MEMCPY ((UINT1 *) &(pL2DhcpSnpPktInfo->u4SrcAddr), pu1PktOffset,
            sizeof (UINT4));
    pL2DhcpSnpPktInfo->u4SrcAddr = OSIX_NTOHL (pL2DhcpSnpPktInfo->u4SrcAddr);

    u2Offset = (UINT2) L2DS_IPDST_ADDR_OFFSET (pL2DhcpSnpPktInfo);
    pu1PktOffset = pu1RecvBuf + u2Offset;

    MEMCPY ((UINT1 *) &(pL2DhcpSnpPktInfo->u4DstAddr), pu1PktOffset,
            sizeof (UINT4));
    pL2DhcpSnpPktInfo->u4DstAddr = OSIX_NTOHL (pL2DhcpSnpPktInfo->u4DstAddr);

    /* Verify Magic Cookie */
    u2Offset = (UINT2) L2DS_MAGIC_COOKIE_OFFSET (pL2DhcpSnpPktInfo);
    pu1PktOffset = pu1RecvBuf + u2Offset;

    if (L2dsUtilVerifyCookie (pu1PktOffset) != L2DS_SUCCESS)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPktGetPktInfo:Verification of Magic Cookie failed.\r\n");

        return L2DS_FAILURE;
    }

    /* Copy the details in the DHCP Header */
    u2Offset = (UINT2) L2DS_DHCP_HDR_OFFSET (pL2DhcpSnpPktInfo);
    pu1PktOffset = pu1RecvBuf + u2Offset;

    L2dsPktCopyHeader (&(pL2DhcpSnpPktInfo->L2DhcpSnpPacket), pu1PktOffset,
                       (*pu4PktLength - u2Offset));

    if (L2dsUtilGetOption (&(pL2DhcpSnpPktInfo->L2DhcpSnpPacket),
                           &L2DhcpSnpOption, L2DS_OPT_MSG_TYPE) != L2DS_FOUND)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPktGetPktInfo:Message type option not present."
                  "so return Failure.\r\n");
        return L2DS_FAILURE;
    }

    pL2DhcpSnpPktInfo->u1PktType = L2DhcpSnpOption.pu1Val[L2DS_ZERO];
    MEMSET (L2DhcpSnpOption.pu1Val, L2DS_ZERO, L2DS_OPTION_LEN);

    /* update the statistics */
    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT, L2DS_MODULE_NAME,
                   "L2dsPktGetPktInfo:Exiting the function %s \r\n",
                   __FUNCTION__);
    return L2DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2dsPktUpdateStat                                    */
/*                                                                           */
/* Description        : This function is to update VLAN statistics           */
/*                                                                           */
/* Input(s)           : u1PktType      - Incoming packet type                */
/*                      u4ContextId    - Context Identifier                  */
/*                      VlanId         - Incoming Vlan Id                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2dsPktUpdateStat (UINT1 u1PktType, UINT4 u4ContextId, tVlanId VlanId)
{
    tL2DhcpSnpIfaceEntry L2DhcpSnpIfaceEntry;
    tL2DhcpSnpIfaceEntry *pL2DhcpSnpIfaceEntry = NULL;

    MEMSET (&L2DhcpSnpIfaceEntry, L2DS_ZERO, sizeof (tL2DhcpSnpIfaceEntry));

    L2DhcpSnpIfaceEntry.u4L2dsCxtId = u4ContextId;
    L2DhcpSnpIfaceEntry.VlanId = VlanId;

    /* Get the corresponding VLAN interface entry */
    pL2DhcpSnpIfaceEntry = (tL2DhcpSnpIfaceEntry *)
        RBTreeGet (L2DS_INTF_RBTREE, (tRBElem *) & L2DhcpSnpIfaceEntry);

    if (pL2DhcpSnpIfaceEntry != NULL)
    {
        switch (u1PktType)
        {
            case L2DS_DHCP_DISCOVER:
            {
                L2DS_INCR_STAT_RX_DISC (pL2DhcpSnpIfaceEntry);
                break;
            }
            case L2DS_DHCP_REQUEST:
            {
                L2DS_INCR_STAT_RX_REQS (pL2DhcpSnpIfaceEntry);
                break;
            }
            case L2DS_DHCP_RELEASE:
            {
                L2DS_INCR_STAT_RX_RELS (pL2DhcpSnpIfaceEntry);
                break;
            }
            case L2DS_DHCP_DECLINE:
            {
                L2DS_INCR_STAT_RX_DECL (pL2DhcpSnpIfaceEntry);
                break;
            }
            case L2DS_DHCP_INFORM:
            {
                L2DS_INCR_STAT_RX_INTF (pL2DhcpSnpIfaceEntry);
                break;
            }
            case L2DS_DHCP_OFFER:
            {
                L2DS_INCR_STAT_TX_OFFR (pL2DhcpSnpIfaceEntry);
                break;
            }
            case L2DS_DHCP_ACK:
            {
                L2DS_INCR_STAT_TX_ACKS (pL2DhcpSnpIfaceEntry);
                break;
            }
            case L2DS_DHCP_NACK:
            {
                L2DS_INCR_STAT_TX_NAKS (pL2DhcpSnpIfaceEntry);
                break;
            }
            default:
            {
                break;
            }
        }
        return;
    }
}

/*****************************************************************************/
/* Function Name      : L2dsPktCopyHeader                                    */
/*                                                                           */
/* Description        : This function is to copy DHCP header informations    */
/*                                                                           */
/* Input(s)           : pu1DhcpBuf  - Incoming DHCP packet buffer            */
/*                      u4DhcpPktLength - DHCP packet length                 */
/*                                                                           */
/* Output(s)          : pL2DhcpSnpPacket - DHCP Header informations          */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsPktCopyHeader (tL2DhcpSnpPacket * pL2DhcpSnpPacket, UINT1 *pu1DhcpBuf,
                   UINT4 u4DhcpPktLength)
{
    UINT2               u2Offset = L2DS_ZERO;

    /* Copy the OP code */
    pL2DhcpSnpPacket->u1Op = pu1DhcpBuf[u2Offset];

    pL2DhcpSnpPacket->u2OptLen = (UINT2) (u4DhcpPktLength -
                                          L2DS_HDR_LEN_WITH_COOKIE);

    /* Copy the transaction ID */
    MEMCPY (&(pL2DhcpSnpPacket->u4Xid), (pu1DhcpBuf + L2DS_XID_OFFSET),
            L2DS_XID_LEN);

    pL2DhcpSnpPacket->u4Xid = L2DS_NTOHL (pL2DhcpSnpPacket->u4Xid);

    /* Copy the client address */
    MEMCPY (&(pL2DhcpSnpPacket->u4Ciaddr), (pu1DhcpBuf + L2DS_CIADDR_OFFSET),
            L2DS_IP_ADDR_LEN);

    pL2DhcpSnpPacket->u4Ciaddr = L2DS_NTOHL (pL2DhcpSnpPacket->u4Ciaddr);

    /* copy the 'your' client address field */
    MEMCPY (&(pL2DhcpSnpPacket->u4Yiaddr), (pu1DhcpBuf + L2DS_YIADDR_OFFSET),
            L2DS_IP_ADDR_LEN);

    pL2DhcpSnpPacket->u4Yiaddr = L2DS_NTOHL (pL2DhcpSnpPacket->u4Yiaddr);

    /* copy the server address field */
    MEMCPY (&(pL2DhcpSnpPacket->u4Siaddr), (pu1DhcpBuf + L2DS_SIADDR_OFFSET),
            L2DS_IP_ADDR_LEN);

    pL2DhcpSnpPacket->u4Siaddr = L2DS_NTOHL (pL2DhcpSnpPacket->u4Siaddr);

    /* Copy the relay agent address field */
    MEMCPY (&(pL2DhcpSnpPacket->u4Giaddr), (pu1DhcpBuf + L2DS_GIADDR_OFFSET),
            L2DS_IP_ADDR_LEN);

    pL2DhcpSnpPacket->u4Giaddr = L2DS_NTOHL (pL2DhcpSnpPacket->u4Giaddr);

    /* Copy the client hardware address field */
    MEMCPY (&(pL2DhcpSnpPacket->Chaddr), (pu1DhcpBuf + L2DS_CHADDR_OFFSET),
            sizeof (tMacAddr));

    /* Copy the sname field */
    MEMCPY (&(pL2DhcpSnpPacket->u1Sname), (pu1DhcpBuf + L2DS_SNAME_OFFSET),
            L2DS_SNAME_LEN);

    /* copy the file field */
    MEMCPY (&(pL2DhcpSnpPacket->u1File), (pu1DhcpBuf + L2DS_FILE_OFFSET),
            L2DS_FILE_LEN);

    /* copy the options */
    MEMCPY (&(pL2DhcpSnpPacket->au1Options),
            (pu1DhcpBuf + L2DS_HDR_LEN_WITH_COOKIE),
            (u4DhcpPktLength - L2DS_HDR_LEN_WITH_COOKIE));

    return L2DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2dsPktProcessPktFromSrvr                            */
/*                                                                           */
/* Description        : This function handles the incoming DHCP Packet, from */
/*                      the servers connected to upstream interface          */
/*                                                                           */
/* Input(s)           : pu1RecvBuf   - Incoming packet buffer                */
/*                      u4ContextId  - Context Identifier                    */
/*                      u4InPort     - Incoming Port                         */
/*                      VlanTag      - VLAN tag informations of the packet   */
/*                      u2EtherOffset- Ethernet header length                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsPktProcessPktFromSrvr (UINT1 *pu1RecvBuf, UINT4 u4ContextId, UINT4 u4InPort,
                           tVlanTag VlanTag, UINT2 u2EtherOffset)
{

    tL2DhcpSnpPktInfo  *pL2DhcpSnpPktInfo = NULL;
    tL2DhcpSnpOption    L2DhcpSnpOption;
    UINT1               au1AccessNode[L2DS_ACCESS_NODE_ARRAY_LEN] =
        { L2DS_ZERO };
    UINT1               au1AccessNodeId[L2DS_MAX_OPT_LEN] = { L2DS_ZERO };
    tPortList          *pUpstreamPortList = NULL;
    tVlanId             CVlanId = L2DS_ZERO;
    tVlanId             SVlanId = L2DS_ZERO;
    UINT4               u4OutPort = L2DS_ZERO;
    UINT4               u4PktLength = L2DS_ZERO;
    UINT1               u1AccessNodeIdLen = L2DS_ZERO;
    UINT1              *pu1AccessNodeName = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    if ((pL2DhcpSnpPktInfo = (tL2DhcpSnpPktInfo *)
         (L2DS_ALLOC_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID))) == NULL)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPktProcessPktFromSrvr:Memory Allocation failed for Message"
                  "\r\n");
        return L2DS_FAILURE;
    }

    MEMSET (pL2DhcpSnpPktInfo, L2DS_ZERO, sizeof (tL2DhcpSnpPktInfo));
    MEMSET (&L2DhcpSnpOption, L2DS_ZERO, sizeof (tL2DhcpSnpOption));

    SVlanId = VlanTag.OuterVlanTag.u2VlanId;

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_ENTRY, L2DS_MODULE_NAME,
                   "L2dsPktProcessPktFromSrvr:Entering the function %s \r\n",
                   __FUNCTION__);

    L2DS_TRC_ARG4 (L2DS_TRC_FLAG, L2DS_FN_ARGS, L2DS_MODULE_NAME,
                   "L2dsPktProcessPktFromSrvr:The arguments to the function %s "
                   "are VlanId %d, InPort %d, Ethernet header length %d and "
                   "address of received buffer\r\n", __FUNCTION__, SVlanId,
                   u4InPort, u2EtherOffset);

    /* Extract the information from the packet */
    if (L2dsPktGetPktInfo (pu1RecvBuf, pL2DhcpSnpPktInfo, u4InPort, SVlanId,
                           u2EtherOffset, &u4PktLength) != L2DS_SUCCESS)
    {
        L2DS_TRC_ARG2 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                       "L2dsPktProcessPktFromSrvr:Getting information of the "
                       "incoming DHCP Packet on VLAN %d, Port %d failed.\r\n",
                       SVlanId, u4InPort);
        L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
        return L2DS_FAILURE;
    }

    if ((pL2DhcpSnpPktInfo->u1PktType == L2DS_DHCP_DISCOVER) ||
        (pL2DhcpSnpPktInfo->u1PktType == L2DS_DHCP_RELEASE) ||
        (pL2DhcpSnpPktInfo->u1PktType == L2DS_DHCP_INFORM))
    {
        /* If packet is any of the Client packets, forward it through
         * other upstream interfaces */

        /* Convert the packet to CRU-buffer */
        pBuf = L2dsUtilLinearToCRU (pu1RecvBuf, u4PktLength);
        if (pBuf == NULL)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsPktProcessPktFromSrvr:Unable to convert the linear "
                      "buffer to CRU-buffer\r\n");
            L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
            return L2DS_FAILURE;
        }

        if ((pUpstreamPortList = (tPortList *)
             (FsUtilAllocBitList (sizeof (tPortList)))) == NULL)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsPktProcessPktFromSrvr:Memory Allocation failed for Message"
                      "\r\n");
            L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
            /* Release the CRU Buffer */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return L2DS_FAILURE;
        }
        MEMSET (pUpstreamPortList, L2DS_ZERO, sizeof (tPortList));

        /* For a broadcast packet, get the upstream list and sent the
         * packet out through all the upstream ports, exept through which 
         * the packet came. If it is unicast, sent the packet out through
         * the specified interface */

        if (L2dsPortGetUpStreamList (u4ContextId, SVlanId, *pUpstreamPortList)
            != L2DS_SUCCESS)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsPktProcessPktFromSrvr:Unable to get the upstream "
                      "portlist\r\n");

            /* Release the CRU Buffer */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

            FsUtilReleaseBitList ((UINT1 *) pUpstreamPortList);
            L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
            return L2DS_FAILURE;
        }

        /* exclude the incoming upstream port */
        OSIX_BITLIST_RESET_BIT (*pUpstreamPortList, u4InPort,
                                sizeof (tPortList));

        /* Send the packet out through the remaining upstream interface(s) */
        if (L2dsPortForwardPkt (pBuf, u4ContextId, VlanTag, u4InPort,
                                pL2DhcpSnpPktInfo->DstMacAddress,
                                pL2DhcpSnpPktInfo->SrcMacAddress,
                                *pUpstreamPortList) != L2DS_SUCCESS)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsPktProcessPktFromSrvr:Unable to send the packet "
                      "out through upstream interfaces.\r\n");

            /* Release the CRU Buffer */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

            FsUtilReleaseBitList ((UINT1 *) pUpstreamPortList);
            L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
            return L2DS_FAILURE;
        }
        L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT, L2DS_MODULE_NAME,
                       "L2dsPktProcessPktFromSrvr:Exiting the function %s \r\n",
                       __FUNCTION__);
        L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
        FsUtilReleaseBitList ((UINT1 *) pUpstreamPortList);
        return L2DS_SUCCESS;
    }

    /* get the option 82 from the DHCP Packet */
    if (L2dsUtilGetOption (&(pL2DhcpSnpPktInfo->L2DhcpSnpPacket),
                           &L2DhcpSnpOption, L2DS_OPT_RAI) == L2DS_FOUND)
    {
        if (L2dsUtilValidateFreeString (&L2DhcpSnpOption, &u4OutPort)
            != L2DS_SUCCESS)
        {
            /* TBD: Remote Agent ID validation */
            /* caliing a function to Get AccessNodeIdentifier */
            pu1AccessNodeName = DcsGetAccessNodeIdentifier (au1AccessNode);
            u1AccessNodeIdLen = (UINT1) STRLEN (pu1AccessNodeName);

            /* Get the Access node Identifier in the Circuit ID */
            if (L2dsUtilGetAccessNodeId (&L2DhcpSnpOption, u1AccessNodeIdLen,
                                         au1AccessNodeId) != L2DS_SUCCESS)
            {
                /* DHCP server does not understands option-82.That time it will
                 * not echo back the option-82 {circuit-id and Remote-id}.Server
                 * is not acquiring IP to HOST.
                 * So broadcast the packet on that VLAN */

                MEMSET (L2DhcpSnpOption.pu1Val, L2DS_ZERO, L2DS_OPTION_LEN);

                /* Convert the packet to CRU-buffer */
                pBuf = L2dsUtilLinearToCRU (pu1RecvBuf, u4PktLength);
                if (pBuf == NULL)
                {
                    L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                              "L2dsPktProcessPktFromSrvr:Unable to convert the"
                              "linear buffer to CRU-buffer\r\n");

                    L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID,
                                            pL2DhcpSnpPktInfo);
                    return L2DS_FAILURE;
                }

                if (L2dsPortForwardPkt (pBuf, u4ContextId, VlanTag, u4InPort,
                                        pL2DhcpSnpPktInfo->DstMacAddress,
                                        pL2DhcpSnpPktInfo->SrcMacAddress,
                                        NULL) != L2DS_SUCCESS)
                {
                    L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                              "L2dsPktProcessPktFromSrvr:Unable"
                              "to send the packet out\r\n");

                    /* Release the CRU Buffer */
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID,
                                            pL2DhcpSnpPktInfo);
                    return L2DS_FAILURE;
                }
                L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID,
                                        pL2DhcpSnpPktInfo);
                return L2DS_SUCCESS;
            }

            /* Check whether the remote Id and Access node id is added 
             * by us */
            if (MEMCMP (au1AccessNodeId, pu1AccessNodeName, u1AccessNodeIdLen)
                != L2DS_ZERO)
            {
                /* The options are not added by us */
                /* So forward the packet on all trusted port as well as on 
                 * remaining upstream ports */
                if (L2dsPktFwdOnTrusted
                    (pu1RecvBuf, u4ContextId, u4InPort, VlanTag,
                     pL2DhcpSnpPktInfo->DstMacAddress,
                     pL2DhcpSnpPktInfo->SrcMacAddress,
                     u4PktLength) != L2DS_SUCCESS)
                {
                    MEMSET (L2DhcpSnpOption.pu1Val, L2DS_ZERO, L2DS_OPTION_LEN);
                    L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC,
                              L2DS_MODULE_NAME, "L2dsPktProcessPktFromSrvr:"
                              "Unable to send the packet out through "
                              "upstream and trusted interfaces\r\n");
                    L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID,
                                            pL2DhcpSnpPktInfo);
                    return L2DS_FAILURE;
                }
                L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID,
                                        pL2DhcpSnpPktInfo);
                return L2DS_SUCCESS;
            }

            /* Options are added by us */
            /* Get the port, on which the packet is to be sent, from options */
            if (L2dsUtilGetPortAndVlanFromOption (&L2DhcpSnpOption, &u4OutPort,
                                                  &CVlanId) != L2DS_SUCCESS)
            {
                MEMSET (L2DhcpSnpOption.pu1Val, L2DS_ZERO, L2DS_OPTION_LEN);
                L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                          "L2dsPktProcessPktFromSrvr:Unable to get the outport "
                          "from the options\r\n");
                L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID,
                                        pL2DhcpSnpPktInfo);
                return L2DS_FAILURE;
            }
        }

        /* Remove the options and sent the packet out */
        if (L2dsPktHandlePktFromSrvr
            (pu1RecvBuf, u4ContextId, u4OutPort, VlanTag, CVlanId, u4PktLength,
             pL2DhcpSnpPktInfo) != L2DS_SUCCESS)
        {
            MEMSET (L2DhcpSnpOption.pu1Val, L2DS_ZERO, L2DS_OPTION_LEN);
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsPktProcessPktFromSrvr:Unable to remove options and "
                      "send packet out\r\n");
            L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
            return L2DS_FAILURE;
        }

        MEMSET (L2DhcpSnpOption.pu1Val, L2DS_ZERO, L2DS_OPTION_LEN);
    }

    else
    {
        /* There is no option 82 present in the packet. This means that the
         * DHCP server upstream does not support or understands option-82.
         * So broadcast the packet on that VLAN, or send the packet based on 
         * destination MAC address*/

        /* Convert the packet to CRU-buffer */
        pBuf = L2dsUtilLinearToCRU (pu1RecvBuf, u4PktLength);
        if (pBuf == NULL)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsPktProcessPktFromSrvr:Unable to convert the linear "
                      "buffer to CRU-buffer\r\n");
            L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
            return L2DS_FAILURE;
        }

        if (L2dsPortForwardPkt (pBuf, u4ContextId, VlanTag, u4InPort,
                                pL2DhcpSnpPktInfo->DstMacAddress,
                                pL2DhcpSnpPktInfo->SrcMacAddress,
                                NULL) != L2DS_SUCCESS)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsPktProcessPktFromSrvr:Unable to send the packet out"
                      "\r\n");
            L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
            /* Release the CRU Buffer */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return L2DS_FAILURE;
        }
    }
    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT, L2DS_MODULE_NAME,
                   "L2dsPktProcessPktFromSrvr:Exiting the function %s \r\n",
                   __FUNCTION__);
    L2DS_RELEASE_MEM_BLOCK (L2DS_PKT_INFO_POOL_ID, pL2DhcpSnpPktInfo);
    return L2DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2dsPktHandlePktFromSrvr                             */
/*                                                                           */
/* Description        : This function handles the incoming DHCP Packet, from */
/*                      the servers connected to upstream interface          */
/*                                                                           */
/* Input(s)           : pu1RecvBuf         - Incoming packet buffer          */
/*                      u4ContextId        - Context Identifier              */
/*                      u4OutPort          - Outgoing Port                   */
/*                      VlanTag            - VLAN tag informations of the    */
/*                                           packet                          */
/*                      CVlanId            - Customer VLAN ID                */
/*                      u4PktLength        - total packet length             */
/*                      pL2DhcpSnpPktInfo  - Packet informations             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsPktHandlePktFromSrvr (UINT1 *pu1RecvBuf, UINT4 u4ContextId, UINT4 u4OutPort,
                          tVlanTag VlanTag, tVlanId CVlanId, UINT4 u4PktLength,
                          tL2DhcpSnpPktInfo * pL2DhcpSnpPktInfo)
{
    tPortList          *pOutPortList = NULL;
    tL2DhcpSnpOption    L2DhcpSnpOption;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT1              *pu1Options = NULL;
    UINT1              *pu1PktOffset = NULL;
    tVlanId             VlanId = L2DS_ZERO;
    UINT4               u4DefaultGateway = L2DS_ZERO;
    UINT4               u4NewPktLength = L2DS_ZERO;
    UINT4               u4LeaseTime = L2DS_ZERO;
    UINT2               u2Offset = L2DS_ZERO;
    UINT2               u2NewOptLen = L2DS_ZERO;
    UINT2               u2PktChgLen = L2DS_ZERO;
    UINT2               u2CheckSum = L2DS_ZERO;
    UINT1               u1RemovedLength = L2DS_ZERO;

    /* As of now, we are not using the C-VLAN tag, 
     * that is contained in the Circuit-ID */

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_ENTRY, L2DS_MODULE_NAME,
                   "L2dsPktHandlePktFromSrvr:Entering the function %s \r\n",
                   __FUNCTION__);

    MEMSET (&L2DhcpSnpOption, L2DS_ZERO, sizeof (tL2DhcpSnpOption));

    VlanId = VlanTag.OuterVlanTag.u2VlanId;

    /* Update the VLAN statistics */
    L2dsPktUpdateStat (pL2DhcpSnpPktInfo->u1PktType, u4ContextId, VlanId);

    if (pL2DhcpSnpPktInfo->u1PktType == L2DS_DHCP_ACK)
    {
        /* The packet is an DHCP ACK Packet */
        /* Get the lease time */
        if (L2dsUtilGetOption (&(pL2DhcpSnpPktInfo->L2DhcpSnpPacket),
                               &L2DhcpSnpOption, L2DS_OPT_LEASE_TIME)
            != L2DS_FOUND)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsPktHandlePktFromSrvr:Unable to get the lease time "
                      "option\r\n");
        }
        else
        {
            MEMCPY (&u4LeaseTime, L2DhcpSnpOption.pu1Val, sizeof (UINT4));
            u4LeaseTime = OSIX_NTOHL (u4LeaseTime);
            pL2DhcpSnpPktInfo->u4LeaseDuration = u4LeaseTime;

            MEMSET (L2DhcpSnpOption.pu1Val, L2DS_ZERO, L2DS_OPTION_LEN);
        }

        /* get the default gateway IP address */
        if (L2dsUtilGetOption (&(pL2DhcpSnpPktInfo->L2DhcpSnpPacket),
                               &L2DhcpSnpOption, L2DS_OPT_ROUTER_OPTION)
            != L2DS_FOUND)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsPktHandlePktFromSrvr:Unable to get the default "
                      "gateway option\r\n");
        }
        else
        {
            MEMCPY (&u4DefaultGateway, L2DhcpSnpOption.pu1Val, sizeof (UINT4));
            u4DefaultGateway = OSIX_NTOHL (u4DefaultGateway);
            pL2DhcpSnpPktInfo->u4DefaultGwIP = u4DefaultGateway;

            MEMSET (L2DhcpSnpOption.pu1Val, L2DS_ZERO, L2DS_OPTION_LEN);
        }
    }

    /* Remove the options added by us */
    if (L2dsUtilRemoveOptions (pL2DhcpSnpPktInfo, &u1RemovedLength)
        != L2DS_SUCCESS)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPktHandlePktFromSrvr:Unable to remove the options "
                  "added by us\r\n");
    }
    else
    {
        /* Copy the rest of the options back to the packet */
        u2Offset = (UINT2) L2DS_DHCP_OPT_OFFSET (pL2DhcpSnpPktInfo);
        pu1PktOffset = pu1RecvBuf + u2Offset;

        pu1Options = pL2DhcpSnpPktInfo->L2DhcpSnpPacket.au1Options;
        u2NewOptLen = pL2DhcpSnpPktInfo->L2DhcpSnpPacket.u2OptLen;

        MEMCPY (pu1PktOffset, pu1Options, u2NewOptLen);
        MEMSET ((pu1PktOffset + u2NewOptLen), L2DS_ZERO, u1RemovedLength);

        u4NewPktLength = u4PktLength - (UINT4) u1RemovedLength;

        /* Update the ip length field */
        u2Offset = (UINT2) L2DS_IP_LENGTH_OFFSET (pL2DhcpSnpPktInfo);
        pu1PktOffset = pu1RecvBuf + u2Offset;

        MEMCPY (&u2PktChgLen, pu1PktOffset, L2DS_IP_DGRAM_LEN);

        u2PktChgLen = OSIX_NTOHS (u2PktChgLen);
        u2PktChgLen -= u1RemovedLength;
        u2PktChgLen = OSIX_HTONS (u2PktChgLen);

        MEMCPY (pu1PktOffset, &u2PktChgLen, L2DS_IP_DGRAM_LEN);

        /* Calculate the new IP Header checksum */
        u2Offset = (UINT2) L2DS_IP_HDR_CKSUM_OFFSET (pL2DhcpSnpPktInfo);
        pu1PktOffset = pu1RecvBuf + u2Offset;

        MEMSET (pu1PktOffset, L2DS_ZERO, L2DS_IP_HDR_CKSUM_LEN);

        u2CheckSum = L2dsPortCalcIpCkSum
            ((pu1RecvBuf + pL2DhcpSnpPktInfo->u1EtherHdrLen),
             pL2DhcpSnpPktInfo->u1IpHdrLen);

        u2CheckSum = OSIX_HTONS (u2CheckSum);

        MEMCPY (pu1PktOffset, &u2CheckSum, L2DS_IP_HDR_CKSUM_LEN);

        /* Update the udp-header length field */
        u2Offset = (UINT2) L2DS_UDP_HDR_LEN_OFFSET (pL2DhcpSnpPktInfo);
        pu1PktOffset = pu1RecvBuf + u2Offset;

        MEMCPY (&u2PktChgLen, pu1PktOffset, L2DS_UDP_DGRAM_LEN);

        u2PktChgLen = OSIX_NTOHS (u2PktChgLen);
        u2PktChgLen -= u1RemovedLength;
        u2PktChgLen = OSIX_HTONS (u2PktChgLen);

        MEMCPY (pu1PktOffset, &u2PktChgLen, L2DS_UDP_DGRAM_LEN);

        /* Put the UDP Checksum to 0. */
        u2Offset = (UINT2) L2DS_UDP_CKSUM_OFFSET (pL2DhcpSnpPktInfo);
        pu1PktOffset = pu1RecvBuf + u2Offset;

        MEMSET (pu1PktOffset, L2DS_ZERO, L2DS_UDP_CKSUM_LEN);

        /* Calculate the new Checksum */
        u2CheckSum = L2dsUtilCalcUdpCkSum (OSIX_NTOHS (u2PktChgLen),
                                           pL2DhcpSnpPktInfo->u4SrcAddr,
                                           pL2DhcpSnpPktInfo->u4DstAddr,
                                           (pu1RecvBuf +
                                            pL2DhcpSnpPktInfo->u1EtherHdrLen +
                                            pL2DhcpSnpPktInfo->u1IpHdrLen));

        MEMCPY (pu1PktOffset, &u2CheckSum, L2DS_UDP_CKSUM_LEN);
    }
    /* Convert the packet to CRU-Buffer */
    pBuf = L2dsUtilLinearToCRU (pu1RecvBuf, u4NewPktLength);
    if (pBuf == NULL)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPktHandlePktFromSrvr:Unable to convert the linear buffer"
                  " to CRU-buffer\r\n");
        return L2DS_FAILURE;
    }
    pOutPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pOutPortList == NULL)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPktHandlePktFromSrvr: "
                  "Error in allocating memory for pOutPortList\r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return L2DS_FAILURE;
    }
    MEMSET (*pOutPortList, 0, sizeof (tPortList));

    OSIX_BITLIST_SET_BIT ((*pOutPortList), u4OutPort, sizeof (tPortList));

    /* Sent the packet out through the specified out-port */
    if (L2dsPortForwardPkt (pBuf, u4ContextId, VlanTag,
                            pL2DhcpSnpPktInfo->u4PortNumber,
                            pL2DhcpSnpPktInfo->DstMacAddress,
                            pL2DhcpSnpPktInfo->SrcMacAddress,
                            *pOutPortList) == L2DS_SUCCESS)
    {
        /* If the packet is correctly sent out, and the packet type is DHCP 
         * ACK or NAK, Update the IP binding database */
        if (pL2DhcpSnpPktInfo->u1PktType == L2DS_DHCP_ACK)
        {
            /* update the databse */
            if (L2dsPortUpdateEntry
                (pL2DhcpSnpPktInfo, u4ContextId, u4OutPort, CVlanId,
                 (pL2DhcpSnpPktInfo->L2DhcpSnpPacket).Chaddr,
                 (pL2DhcpSnpPktInfo->L2DhcpSnpPacket).u4Yiaddr,
                 L2DS_IPDB_CREATE) != L2DS_SUCCESS)
            {
                L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                          "L2dsPktHandlePktFromSrvr:updation of the binding "
                          "database failed.\r\n");
            }
        }
        else if (pL2DhcpSnpPktInfo->u1PktType == L2DS_DHCP_NACK)
        {
            /* update the databse */
            if (L2dsPortUpdateEntry
                (pL2DhcpSnpPktInfo, u4ContextId, u4OutPort, CVlanId,
                 (pL2DhcpSnpPktInfo->L2DhcpSnpPacket).Chaddr,
                 (pL2DhcpSnpPktInfo->L2DhcpSnpPacket).u4Yiaddr,
                 L2DS_IPDB_DELETE) != L2DS_SUCCESS)
            {
                L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                          "L2dsPktHandlePktFromSrvr:updation of the binding "
                          "database failed.\r\n");
            }
        }
    }
    else
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPktHandlePktFromSrvr:Failed to send the packet out."
                  "\r\n");

        /* Release the CRU Buffer */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        FsUtilReleaseBitList ((UINT1 *) pOutPortList);
        return L2DS_FAILURE;
    }
    FsUtilReleaseBitList ((UINT1 *) pOutPortList);

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT, L2DS_MODULE_NAME,
                   "L2dsPktHandlePktFromSrvr:Exiting the function %s \r\n",
                   __FUNCTION__);
    return L2DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2dsPktFwdOnTrusted                                  */
/*                                                                           */
/* Description        : This function is to sent a packet out through the    */
/*                      trusted ports                                        */
/*                                                                           */
/* Input(s)           : pu1RecvBuf  - Incoming packet buffer                 */
/*                      u4ContextId   - Context Identifier                   */
/*                      u4InPort    - Incoming Port                          */
/*                      VlanTag       - VLAN tag informations of the packet  */
/*                      DstMacAddress - Destination MAC of the packet        */
/*                      SrcMacAddress - Source MAC of the packet             */
/*                      u4PktLength - total packet length                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2DS_SUCCESS/L2DS_FAILURE                            */
/*****************************************************************************/
INT4
L2dsPktFwdOnTrusted (UINT1 *pu1RecvBuf, UINT4 u4ContextId, UINT4 u4InPort,
                     tVlanTag VlanTag, tMacAddr DstMacAddress,
                     tMacAddr SrcMacAddress, UINT4 u4PktLength)
{
    tPortList          *pTrustedPortList = NULL;
    tVlanId             VlanId = L2DS_ZERO;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    VlanId = VlanTag.OuterVlanTag.u2VlanId;

    pBuf = L2dsUtilLinearToCRU (pu1RecvBuf, u4PktLength);
    if (pBuf == NULL)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPktFwdOnTrusted:Unable to convert the "
                  "linear buffer to CRU-buffer\r\n");

        return L2DS_FAILURE;
    }

    pTrustedPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pTrustedPortList == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPktFwdOnTrusted: "
                  "Error in allocating memory for pTrustedPortList\r\n");
        return L2DS_FAILURE;
    }
    MEMSET (pTrustedPortList, 0, sizeof (tPortList));

    if (L2dsPortGetTrustedList (u4ContextId, VlanId, *pTrustedPortList)
        != L2DS_SUCCESS)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPktFwdOnTrusted:Unable to get the trusted "
                  "portlist\r\n");
        /* Release the CRU Buffer */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        FsUtilReleaseBitList ((UINT1 *) pTrustedPortList);
        return L2DS_FAILURE;
    }

    /* Sent the packet out */
    if (L2dsPortForwardPkt (pBuf, u4ContextId, VlanTag, u4InPort, DstMacAddress,
                            SrcMacAddress, *pTrustedPortList) != L2DS_SUCCESS)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsPktFwdOnTrusted:Unable to send the packet " "out\r\n");

        /* Release the CRU Buffer */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        FsUtilReleaseBitList ((UINT1 *) pTrustedPortList);
        return L2DS_FAILURE;
    }
    FsUtilReleaseBitList ((UINT1 *) pTrustedPortList);
    return L2DS_SUCCESS;
}

/*                                                                           */
/***************************** END OF FILE ***********************************/
/*                                                                           */
