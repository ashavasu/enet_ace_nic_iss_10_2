/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: l2dsmain.c,v 1.12 2013/12/18 12:48:01 siva Exp $               */
/*****************************************************************************/
/*    FILE  NAME            : l2dsmain.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : L2DHCP Snooping                                */
/*    MODULE NAME           : L2DHCP Snooping Main Module                    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains init, deinit funtions       */
/*                            for L2DHCP Snooping module                     */
/*---------------------------------------------------------------------------*/

#include "l2dsinc.h"
#include "l2dsglob.h"

/*****************************************************************************/
/* Function Name      : L2DSMain                                             */
/*                                                                           */
/* Description        : This function is the main entry point function for   */
/*                      the L2DHCP Snooping task                             */
/*                                                                           */
/* Input(s)           : pi1Param - unused                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2DSMain (INT1 *pi1Param)
{
    UINT4               u4EventsRecvd = L2DS_ZERO;

    UNUSED_PARAM (pi1Param);
    L2DS_TRC_FLAG = L2DS_TRC_NONE;

    /* Create semaphore for mutual exclusion */
    if (L2DS_CREATE_SEM (L2DS_SEM_NAME, L2DS_SEM_COUNT, L2DS_SEM_FLAGS,
                         &(L2DS_SEM_ID)) != OSIX_SUCCESS)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2DSMain:Creation of L2DHCP snooping semaphore failed.\r\n");

        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* Create Queue */
    if (L2DS_CREATE_QUEUE (L2DS_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                           L2DS_QUEUE_DEPTH, &(L2DS_QUEUE_ID)) != OSIX_SUCCESS)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2DSMain:Creation of L2DHCP Snooping Queue failed.\r\n");

        L2dsMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* Creation of all mempools */
    if (L2dsSizingMemCreateMemPools () == L2DS_FAILURE)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2DSMain:Creation of memory pools failed.\r\n");

        L2dsMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }
    /*Assigning mempool Id's */
    L2dsMainAssignMempoolIds ();

    if (gpau1L2dsDataBuffer == NULL)
    {
        if ((gpau1L2dsDataBuffer =
             (UINT1 *) L2DS_ALLOC_MEM_BLOCK (L2DS_DYN_POOL_ID)) == NULL)
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2DSMain: Allocation of memory for duplicating "
                      " the data buffer \r\n");
            L2dsMainDeInit ();
            lrInitComplete (OSIX_FAILURE);
            return;
        }
    }

    /* Create the RBTree For Interface nodes */
    L2DS_INTF_RBTREE =
        L2DS_CREATE_RBTREE (L2DS_ZERO, L2dsUtilRBTreeIntfEntryCmp);
    if (L2DS_INTF_RBTREE == NULL)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2DSMain:Creation of RBTree for interface entire failed."
                  "\r\n");

        L2dsMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

#ifdef SNMP_2_WANTED
    /* Register the L2DHCP Snooping MIB */
    L2dsUtilRegisterFsDhcSnpMib ();
#endif
    /* Set the Global status and MacVerification status */
    L2DS_ADMIN_STATUS (L2DS_DEFAULT_CXT_ID) = L2DS_DISABLED;
    L2DS_MAC_VERIFY_STATUS (L2DS_DEFAULT_CXT_ID) = L2DS_ENABLED;

    if (L2dsGetVcmSystemModeExt (L2DS_PROTOCOL_ID) == VCM_SI_MODE)
    {
        L2DS_IS_CONTEXT_EXIST (L2DS_DEFAULT_CXT_ID) = L2DS_TRUE;
    }

    lrInitComplete (OSIX_SUCCESS);

    /* Wait infinitely till an event comes. When the event comes, process it
     * and wait again for further events. */
    while (L2DS_TRUE)
    {
        if (L2DS_RECEIVE_EVENT (L2DS_EVENT_ARRIVED, L2DS_EVENT_WAIT_FLAG,
                                L2DS_ZERO, &u4EventsRecvd) == OSIX_SUCCESS)
        {
            if ((u4EventsRecvd & L2DS_EVENT_ARRIVED) == L2DS_EVENT_ARRIVED)
            {
                /* Process the events */
                L2dsMainProcessEvent ();
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : L2dsMainDeInit                                       */
/*                                                                           */
/* Description        : This function Deletes Semaphore, Queue and MemPools  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
L2dsMainDeInit (VOID)
{
    if (L2DS_SEM_ID != L2DS_ZERO)
    {
        L2DS_DELETE_SEM (L2DS_ZERO, L2DS_SEM_NAME);
    }

    if (L2DS_QUEUE_ID != L2DS_ZERO)
    {
        L2DS_DELETE_QUEUE (L2DS_QUEUE_ID);
    }

    L2dsSizingMemDeleteMemPools ();

    return;
}

/*****************************************************************************/
/* Function Name      : L2dsMainProcessEvent                                 */
/*                                                                           */
/* Description        : This function handles the events.                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
L2dsMainProcessEvent (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tL2DhcpSnpQMsg     *pL2DhcpSnpQMsg = NULL;
    tOsixMsg           *pOsixQMsg = NULL;
    tVlanTag            VlanTag;
    tVlanId             VlanId = L2DS_ZERO;
    UINT4               u4InPort = L2DS_ZERO;
    UINT4               u4L2dsCxtId = L2DS_ZERO;

    MEMSET (&VlanTag, L2DS_ZERO, sizeof (tVlanTag));

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_ENTRY, L2DS_MODULE_NAME,
                   "L2dsMainProcessEvent:Entering the function %s \r\n",
                   __FUNCTION__);

    /* Take the lock, before processing the event */
    if (L2DS_LOCK () == SNMP_FAILURE)
    {
        L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                  "L2dsMainProcessEvent:Taking the protocol lock failed.\r\n");
        return;
    }

    /* Receive the event from the queue */
    while (L2DS_RECV_FROM_QUEUE
           (L2DS_QUEUE_ID, (UINT1 *) &pOsixQMsg, OSIX_DEF_MSG_LEN,
            L2DS_ZERO) == OSIX_SUCCESS)
    {
        pL2DhcpSnpQMsg = (tL2DhcpSnpQMsg *) pOsixQMsg;

        if (pL2DhcpSnpQMsg->u4EventType == L2DS_PKT_RECEIVE_EVENT)
        {
            /* This is an incoming packet related event */
            pBuf = pL2DhcpSnpQMsg->pInQMsg;
            u4L2dsCxtId = pL2DhcpSnpQMsg->u4L2dsCxtId;
            u4InPort = pL2DhcpSnpQMsg->u4InPort;

            MEMCPY (&VlanTag, &(pL2DhcpSnpQMsg->VlanTag), sizeof (tVlanTag));

            /* Process the incoming packet */
            if (L2dsPktHandleIncomingPkt (pBuf, u4L2dsCxtId,
                                          u4InPort, VlanTag) == L2DS_FAILURE)
            {
                L2DS_TRC_ARG2 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                               "L2dsMainProcessEvent:Handling of the incoming "
                               "DHCP Packet on VLAN %d, Port %d failed.\r\n",
                               VlanTag.OuterVlanTag.u2VlanId, u4InPort);
            }
        }
        else if (pL2DhcpSnpQMsg->u4EventType == L2DS_VLAN_INTERFACE_EVENT)
        {
            /* This is a VLAN deletion related event */
            u4L2dsCxtId = pL2DhcpSnpQMsg->u4L2dsCxtId;
            VlanId = pL2DhcpSnpQMsg->VlanId;

            if (L2dsIntfDeleteIntfEntry (u4L2dsCxtId, VlanId) == L2DS_FAILURE)
            {
                L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                               "L2dsMainProcessEvent:Handling the deletion of "
                               "VLAN %d failed.\r\n", VlanId);
            }
        }
        else
        {
            L2DS_TRC (L2DS_TRC_FLAG, L2DS_FAIL_TRC, L2DS_MODULE_NAME,
                      "L2dsMainProcessEvent:Invalid Event.\r\n");
        }
        L2DS_RELEASE_MEM_BLOCK (L2DS_Q_POOL_ID, pL2DhcpSnpQMsg);
    }
    L2DS_UNLOCK ();

    L2DS_TRC_ARG1 (L2DS_TRC_FLAG, L2DS_FN_EXIT, L2DS_MODULE_NAME,
                   "L2dsMainProcessEvent:Exiting the function %s \r\n",
                   __FUNCTION__);
    return;
}

/*****************************************************************************/
/* Function Name      : L2dsMainAssignMempoolIds                             */
/*                                                                           */
/* Description        : This function is used to assign respective mempool   */
/*                       ID's                                                */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
L2dsMainAssignMempoolIds (VOID)
{
    /*tL2DhcpSnpQMsg */
    L2DS_Q_POOL_ID = L2DSMemPoolIds[MAX_L2DS_Q_MESG_SIZING_ID];
    /*tL2DhcpSnpIfaceEntry */
    L2DS_INTF_POOL_ID = L2DSMemPoolIds[MAX_L2DS_IFACE_ENTRIES_SIZING_ID];
    L2DS_DYN_POOL_ID = L2DSMemPoolIds[MAX_L2DS_PDU_COUNT_SIZING_ID];
    /*tL2DhcpSnpPktInfo */
    L2DS_PKT_INFO_POOL_ID = L2DSMemPoolIds[MAX_L2DS_PKT_INFO_SIZING_ID];
}

/*                                                                           */
/***************************** END OF FILE ***********************************/
/*                                                                           */
