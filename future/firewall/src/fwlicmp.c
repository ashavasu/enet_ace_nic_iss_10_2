/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlicmp.c,v 1.8 2014/01/25 13:52:27 siva Exp $
 *
 * *******************************************************************/

#include "fwlinc.h"

/* Local Prototypes */
PRIVATE UINT4
    FwlInspectIcmpErrorPayload PROTO ((tCRU_BUF_CHAIN_HEADER * pPktBuf,
                                       UINT4 u4IcmpOffset, UINT1 u1Direction));
PRIVATE UINT4
    FwlInspectIcmpv6ErrorPayload PROTO ((tCRU_BUF_CHAIN_HEADER * pPktBuf,
                                         UINT4 u4IcmpOffset,
                                         UINT1 u1Direction));

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlInspectIcmpOption                             */
/*                                                                          */
/*    Description        : Inspects the ICMP packet and allows only if it   */
/*                         is valid.                                        */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the Packet            */
/*                         pIfaceInfo   -- Pointer to the Interface         */
/*                         pIpHdr       -- IP Header Info                   */
/*                         pIcmpHdr     -- ICMP Header Info                 */
/*                         u4IfaceNum   -- interface number                 */
/*                         u1Direction  -- Direction IN/OUT                 */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS |                                    */
/*                         FWL_IN_ICMP_ALLOW  | FWL_IN_ICMP_DISCARD  |      */
/*                         FWL_OUT_ICMP_ALLOW | FWL_OUT_ICMP_DISCARD        */
/*                                                                          */
/****************************************************************************/
PUBLIC UINT4
FwlInspectIcmpOption (tCRU_BUF_CHAIN_HEADER * pBuf, tIfaceInfo * pIfaceInfo,
                      tIpHeader * pIpHdr, tIcmpInfo * pIcmpHdr,
                      UINT4 u4IfaceNum, UINT1 u1Direction)
{
    UINT4               u4Attack = FWL_ZERO;
    UINT1               u1IcmpType = FWL_ZERO;
    UINT4               u4IfIpAddr = FWL_ZERO;
    INT1                i1RetVal = FWL_ZERO;
    u1IcmpType = pIcmpHdr->u1Type;

    /* If ICMP Inspect Option is not set,
     * permit ICMP Request/Reply, to/from the WAN Interface.    */
    if (FwlChkIfExtInterface (u4IfaceNum) == FWL_SUCCESS)
    {
        if ((i1RetVal =
             CfaGetIfIpAddr ((INT4) u4IfaceNum, &u4IfIpAddr)) == OSIX_FAILURE)
        {
            FWL_DBG (FWL_DBG_EXIT,
                     "\n Exiting from fn. FwlInspectIcmpOption \n");

            return FWL_SUCCESS;
        }

        if (u1Direction == FWL_DIRECTION_IN)
        {
            /* If incoming packet is ICMP Request 
             * ICMP Inpsect option is set,
             * discard the packet, if it is destined to WAN interface */
            if ((u1IcmpType == FWL_ICMP_TYPE_ECHO_REQUEST) &&
                (gIfaceInfo.u1IcmpType == FWL_ICMP_INSPECT_OPTION) &&
                (pIpHdr->DestAddr.v4Addr == u4IfIpAddr))
            {
                /* Packet is to be denied. */
                MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_INSPECT, "FWL",
                         "\n Icmp inspect option set, deny the packet \n");

                INC_INSPECT_COUNT;
                INC_DISCARD_COUNT;
                INC_ICMP_DISCARD_COUNT;
                INC_IFACE_DISCARD_COUNT (pIfaceInfo);
                INC_IFACE_ICMP_DISCARD_COUNT (pIfaceInfo);

                /* Log the message */
                FwlLogMessage (FWL_ICMP_INSPECT_ON, u4IfaceNum, pBuf,
                               FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                               (UINT1 *) FWL_MSG_ATTACK);

                FWL_DBG (FWL_DBG_EXIT,
                         "\n Exiting from fn. FwlInspectIcmpOption \n");
                return (FWL_IN_ICMP_DISCARD);
            }
            /* If incoming packet is ICMP Request or Reply and 
             * ICMP Inpsect option is not set,
             * allow the packet, if it is destined to WAN interface */
            if ((u1IcmpType == FWL_ICMP_TYPE_ECHO_REQUEST) &&
                (gIfaceInfo.u1IcmpType != FWL_ICMP_INSPECT_OPTION) &&
                (pIpHdr->DestAddr.v4Addr == u4IfIpAddr))
            {
                INC_INSPECT_COUNT;
                INC_PERMIT_COUNT;
                INC_IFACE_PERMIT_COUNT (pIfaceInfo);
                FWL_DBG (FWL_DBG_EXIT,
                         "\n Exiting from fn. FwlInspectIcmpOption \n");

                return (FWL_OUT_ICMP_ALLOW);
            }
            if ((u1IcmpType == FWL_ICMP_TYPE_ECHO_REPLY) &&
                (pIpHdr->DestAddr.v4Addr == u4IfIpAddr))
            {
                INC_INSPECT_COUNT;
                INC_PERMIT_COUNT;
                INC_IFACE_PERMIT_COUNT (pIfaceInfo);
                FWL_DBG (FWL_DBG_EXIT,
                         "\n Exiting from fn. FwlInspectIcmpOption \n");

                return (FWL_OUT_ICMP_ALLOW);
            }
        }
    }
    /* Outbound ICMP Packets with ICMP type as Timestamp reply or 
     * Address Mask reply should be dropped */
    if ((FwlChkIfExtInterface (u4IfaceNum) == FWL_SUCCESS)
        && (u1Direction == FWL_DIRECTION_OUT))
    {
        /* Initialize Attack flag. */
        u4Attack = FWL_PERMITTED;

        /* Dropping ICMP Timestamp Reply Messages */
        if (u1IcmpType == FWL_ICMP_TYPE_TIMESTAMP_REPLY)
        {
            FWL_DBG (FWL_DBG_EXIT, "\n ICMP Timestamp Reply detected \n");
            u4Attack = FWL_ICMP_TIMESTAMP_REPLY_ATTACK;
        }

        /* Dropping ICMP_MASK_REPLY Messages */
        if (u1IcmpType == FWL_ICMP_TYPE_MASK_REPLY)
        {
            FWL_DBG (FWL_DBG_EXIT, "\n ICMP_MASK_REPLY detected \n");
            u4Attack = FWL_ICMP_MASK_REPLY_ATTACK;
        }

        if (u4Attack != FWL_PERMITTED)
        {
            INC_INSPECT_COUNT;
            INC_DISCARD_COUNT;
            INC_ATTACKS_DISCARD_COUNT;

            if (IF_NUM_OF_FWL_ATTACKS_EXCEEDS_LIMIT)
            {
                FwlGenerateAttackSumTrap ();
            }
            INC_IFACE_DISCARD_COUNT (pIfaceInfo);
            INC_IFACE_ATTACKS_DISCARD_COUNT (pIfaceInfo);
            FwlLogMessage (u4Attack, u4IfaceNum, pBuf,
                           FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                           (UINT1 *) FWL_MSG_ATTACK);
            FWL_DBG (FWL_DBG_EXIT,
                     "\n Exiting from fn. FwlInspectIcmpOption \n");
            return (FWL_OUT_ICMP_DISCARD);
        }
    }

    /* Process ICMP Error messages, if enabled */
    if ((u1IcmpType != FWL_ICMP_TYPE_ECHO_REQUEST) &&
        (u1IcmpType != FWL_ICMP_TYPE_ECHO_REPLY))
    {
        if (pIpHdr->SrcAddr.v4Addr == u4IfIpAddr)
        {
            /* allow if the packet originates from this node */
            return FWL_OUT_ICMP_ALLOW;
        }
        if (gFwlAclInfo.u1InspectIcmpErrMsg == FWL_ENABLE)
        {
            if (FwlInspectIcmpErrorPayload
                (pBuf, pIpHdr->u1HeadLen,
                 u1Direction) == FWL_STATEFUL_ENTRY_MATCH)
            {
                INC_INSPECT_COUNT;
                INC_PERMIT_COUNT;
                INC_IFACE_PERMIT_COUNT (pIfaceInfo);
                FwlLogMessage (FWL_PERMITTED, u4IfaceNum, pBuf,
                               FWL_LOG_BRF, FWLLOG_INFO_LEVEL,
                               (UINT1 *) FWL_MSG_ICMP_ERR_MATCH);
                FWL_DBG (FWL_DBG_EXIT,
                         "\n Exiting from fn. FwlInspectIcmpOption \n");
                return ((u1Direction == FWL_DIRECTION_OUT) ?
                        FWL_OUT_ICMP_ALLOW : FWL_IN_ICMP_ALLOW);
            }
            else
            {
                INC_INSPECT_COUNT;
                INC_DISCARD_COUNT;
                INC_ATTACKS_DISCARD_COUNT;

                if (IF_NUM_OF_FWL_ATTACKS_EXCEEDS_LIMIT)
                {
                    FwlGenerateAttackSumTrap ();
                }
                INC_IFACE_DISCARD_COUNT (pIfaceInfo);
                INC_IFACE_ATTACKS_DISCARD_COUNT (pIfaceInfo);
                FwlLogMessage (FWL_STATIC_FILTER, u4IfaceNum, pBuf,
                               FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                               (UINT1 *) FWL_MSG_ICMP_ERR_NO_MATCH);
                FWL_DBG (FWL_DBG_EXIT,
                         "\n Exiting from fn. FwlInspectIcmpOption \n");
                return ((u1Direction == FWL_DIRECTION_OUT) ?
                        FWL_OUT_ICMP_DISCARD : FWL_IN_ICMP_DISCARD);
            }
        }                        /* u1InspectIcmpErrMsg == FWL_ENABLE */
        else
        {
            INC_INSPECT_COUNT;
            INC_DISCARD_COUNT;
            INC_ATTACKS_DISCARD_COUNT;

            if (IF_NUM_OF_FWL_ATTACKS_EXCEEDS_LIMIT)
            {
                FwlGenerateAttackSumTrap ();
            }
            INC_IFACE_DISCARD_COUNT (pIfaceInfo);
            INC_IFACE_ATTACKS_DISCARD_COUNT (pIfaceInfo);
            FwlLogMessage (FWL_STATIC_FILTER, u4IfaceNum, pBuf,
                           FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                           (UINT1 *) FWL_MSG_NOMATCH);
            FWL_DBG (FWL_DBG_EXIT,
                     "\n Exiting from fn. FwlInspectIcmpOption \n");
            return ((u1Direction == FWL_DIRECTION_OUT) ?
                    FWL_OUT_ICMP_DISCARD : FWL_IN_ICMP_DISCARD);
        }
    }
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from fn. FwlInspectIcmpOption \n");
    return (FWL_SUCCESS);

}                                /* FwlInspectIcmpOption) */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlInspectIcmp6Option                            */
/*                                                                          */
/*    Description        : Inspects the ICMPv6 packet and allows only if it */
/*                         is valid.                                        */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the Packet            */
/*                         pIfaceInfo   -- Pointer to the Interface         */
/*                         pIpHdr       -- IP Header Info                   */
/*                         pIcmpHdr     -- ICMP Header Info                 */
/*                         u4IfaceNum   -- interface number                 */
/*                         u1Direction  -- Direction IN/OUT                 */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS |                                    */
/*                         FWL_IN_ICMP_ALLOW  | FWL_IN_ICMP_DISCARD  |      */
/*                         FWL_OUT_ICMP_ALLOW | FWL_OUT_ICMP_DISCARD        */
/*                                                                          */
/****************************************************************************/
PUBLIC UINT4
FwlInspectIcmp6Option (tCRU_BUF_CHAIN_HEADER * pBuf, tIfaceInfo * pIfaceInfo,
                       tIpHeader * pIpHdr, tIcmpInfo * pIcmpHdr,
                       UINT4 u4IfaceNum, UINT1 u1Direction)
{
    UINT1               u1Icmpv6Type = FWL_ZERO;
    UINT1               u1PktDeny = FALSE;
    UINT1               u1AddrMatch = OSIX_FALSE;
    tIp6Addr           *pIp6Addr = NULL;
    tFwlIpAddr          IfIpAddr;
    MEMSET (&IfIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));
    u1Icmpv6Type = pIcmpHdr->u1Type;
    if (FwlChkIfExtInterface (u4IfaceNum) == FWL_SUCCESS)
    {
        pIp6Addr = Sec6UtilGetGlobalAddr (u4IfaceNum, NULL);
        if (pIp6Addr == NULL)
        {
            FWL_DBG (FWL_DBG_EXIT, "\n Exiting from fn. FwlInspectPkt \n");
            return FWL_SUCCESS;
        }
        Ip6AddrCopy (&(IfIpAddr.v6Addr), pIp6Addr);
        if (Ip6AddrCompare (pIpHdr->DestAddr.v6Addr, IfIpAddr.v6Addr)
            == IP6_ZERO)
        {
            u1AddrMatch = OSIX_TRUE;
        }
        if (u1Direction == FWL_DIRECTION_IN)
        {
            if ((gIfaceInfo.i4Icmpv6MsgType == FWL_ICMPV6_NO_INSPECT) &&
                (u1AddrMatch == OSIX_TRUE))
            {
                INC_V6_INSPECT_COUNT;
                INC_V6_PERMIT_COUNT;
                INC_V6_IFACE_PERMIT_COUNT (pIfaceInfo);
                FWL_DBG (FWL_DBG_EXIT,
                         "\n Exiting from fn. FwlInspectIcmpOption \n");
                return (FWL_OUT_ICMP_ALLOW);
            }
            else
            {
                switch (u1Icmpv6Type)
                {
                    case FWL_ICMPV6_TYPE_DESTINATION_UNREACHABLE:
                        if (((gIfaceInfo.i4Icmpv6MsgType &
                              FWL_ICMPV6_DESTINATION_UNREACHABLE) != FWL_ZERO)
                            && (u1AddrMatch == OSIX_TRUE))
                        {
                            u1PktDeny = TRUE;
                        }
                        break;
                    case FWL_ICMPV6_TYPE_TIME_EXCEEDED:
                        if (((gIfaceInfo.i4Icmpv6MsgType &
                              FWL_ICMPV6_TIME_EXCEEDED) != FWL_ZERO) &&
                            (u1AddrMatch == OSIX_TRUE))
                        {
                            u1PktDeny = TRUE;
                        }
                        break;
                    case FWL_ICMPV6_TYPE_PARAMETER_PROBLEM:
                        if (((gIfaceInfo.i4Icmpv6MsgType &
                              FWL_ICMPV6_PARAMETER_PROBLEM) != FWL_ZERO) &&
                            (u1AddrMatch == OSIX_TRUE))
                        {
                            u1PktDeny = TRUE;
                        }
                        break;
                    case FWL_ICMPV6_TYPE_ECHO_REQUEST:
                        if (((gIfaceInfo.i4Icmpv6MsgType &
                              FWL_ICMPV6_ECHO_REQUEST) != FWL_ZERO) &&
                            (u1AddrMatch == OSIX_TRUE))
                        {
                            u1PktDeny = TRUE;
                        }
                        break;
                    case FWL_ICMPV6_TYPE_ECHO_REPLY:
                        if (((gIfaceInfo.i4Icmpv6MsgType &
                              FWL_ICMPV6_ECHO_REPLY) != FWL_ZERO) &&
                            (u1AddrMatch == OSIX_TRUE))
                        {
                            u1PktDeny = TRUE;
                        }
                        break;
                    case FWL_ICMPV6_TYPE_REDIRECT:
                        if (((gIfaceInfo.i4Icmpv6MsgType &
                              FWL_ICMPV6_REDIRECT) != FWL_ZERO) &&
                            (u1AddrMatch == OSIX_TRUE))
                        {
                            u1PktDeny = TRUE;
                        }
                        break;
                    case FWL_ICMPV6_TYPE_INFORMATION_REQUEST:
                        if (((gIfaceInfo.i4Icmpv6MsgType &
                              FWL_ICMPV6_INFORMATION_REQUEST) != FWL_ZERO) &&
                            (u1AddrMatch == OSIX_TRUE))
                        {
                            u1PktDeny = TRUE;
                        }
                        break;
                    case FWL_ICMPV6_TYPE_INFORMATION_REPLY:
                        if (((gIfaceInfo.i4Icmpv6MsgType &
                              FWL_ICMPV6_INFORMATION_REPLY) != FWL_ZERO) &&
                            (u1AddrMatch == OSIX_TRUE))
                        {
                            u1PktDeny = TRUE;
                        }
                        break;
                    default:
                        break;
                }
                if (u1PktDeny == TRUE)
                {
                    MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_INSPECT, "FWL",
                             "\n Icmp inspect option set, deny the packet \n");
                    INC_V6_INSPECT_COUNT;
                    INC_V6_DISCARD_COUNT;
                    INC_V6_ICMP_DISCARD_COUNT;
                    INC_V6_IFACE_DISCARD_COUNT (pIfaceInfo);
                    INC_V6_IFACE_ICMP_DISCARD_COUNT (pIfaceInfo);
                    FwlLogMessage (FWL_ICMP_INSPECT_ON, u4IfaceNum, pBuf,
                                   FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                                   (UINT1 *) FWL_MSG_ATTACK);
                    FWL_DBG (FWL_DBG_EXIT,
                             "\n Exiting from fn. FwlInspectIcmpOption \n");
                    return (FWL_IN_ICMP_DISCARD);
                }
            }
        }
    }
    if ((u1Icmpv6Type != FWL_ICMPV6_TYPE_ECHO_REQUEST) &&
        (u1Icmpv6Type != FWL_ICMPV6_TYPE_ECHO_REPLY))
    {
        if (u1AddrMatch == OSIX_TRUE)
        {
            return FWL_OUT_ICMP_ALLOW;
        }
        if (gFwlAclInfo.u1InspectIcmpv6ErrMsg == FWL_ENABLE)
        {
            if (FwlInspectIcmpv6ErrorPayload
                (pBuf, pIpHdr->u1HeadLen,
                 u1Direction) == FWL_STATEFUL_ENTRY_MATCH)
            {
                INC_V6_INSPECT_COUNT;
                INC_V6_PERMIT_COUNT;
                INC_V6_IFACE_PERMIT_COUNT (pIfaceInfo);
                FwlLogMessage (FWL_PERMITTED, u4IfaceNum, pBuf,
                               FWL_LOG_BRF, FWLLOG_INFO_LEVEL,
                               (UINT1 *) FWL_MSG_ICMP_ERR_MATCH);
                FWL_DBG (FWL_DBG_EXIT,
                         "\n Exiting from fn. FwlInspectIcmp6Option \n");
                return ((u1Direction == FWL_DIRECTION_OUT) ?
                        FWL_OUT_ICMP_ALLOW : FWL_IN_ICMP_ALLOW);
            }
            else
            {
                INC_V6_INSPECT_COUNT;
                INC_V6_DISCARD_COUNT;
                INC_V6_ATTACKS_DISCARD_COUNT;
                if (IF_NUM_OF_FWL_ATTACKS_EXCEEDS_LIMIT)
                {
                    FwlGenerateAttackSumTrap ();
                }
                INC_V6_IFACE_DISCARD_COUNT (pIfaceInfo);
                INC_IFACE_ATTACKS_DISCARD_COUNT (pIfaceInfo);
                FwlLogMessage (FWL_STATIC_FILTER, u4IfaceNum, pBuf,
                               FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                               (UINT1 *) FWL_MSG_ICMP_ERR_NO_MATCH);
                FWL_DBG (FWL_DBG_EXIT,
                         "\n Exiting from fn. FwlInspectIcmp6Option \n");
                return ((u1Direction == FWL_DIRECTION_OUT) ?
                        FWL_OUT_ICMP_DISCARD : FWL_IN_ICMP_DISCARD);
            }
        }                        /* u1InspectIcmpErrMsg == FWL_ENABLE */
        else
        {
            INC_V6_INSPECT_COUNT;
            INC_V6_DISCARD_COUNT;
            INC_V6_ATTACKS_DISCARD_COUNT;
            if (IF_NUM_OF_FWL_V6_ATTACKS_EXCEEDS_LIMIT)
            {
                FwlGenerateAttackSumTrap ();
            }
            INC_V6_IFACE_DISCARD_COUNT (pIfaceInfo);
            INC_IFACE_ATTACKS_DISCARD_COUNT (pIfaceInfo);
            FwlLogMessage (FWL_STATIC_FILTER, u4IfaceNum, pBuf,
                           FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                           (UINT1 *) FWL_MSG_NOMATCH);
            FWL_DBG (FWL_DBG_EXIT,
                     "\n Exiting from fn. FwlInspectIcmp6Option \n");
            return ((u1Direction == FWL_DIRECTION_OUT) ?
                    FWL_OUT_ICMP_DISCARD : FWL_IN_ICMP_DISCARD);
        }
    }
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from fn. FwlInspectIcmp6Option \n");
    return (FWL_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlInspectIcmpErrorPayload                       */
/*                                                                          */
/*    Description        : Inspects the payload in the ICMP packet and      */
/*                         check if it matches iin the stateful entry ie.   */
/*                         the error packet should be a response to the     */
/*                         packet sent                                      */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the Packet            */
/*                         u4IcmpOffset -- Offset at which payload begins   */
/*                         u1Direction  -- Direction IN/OUT                 */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_STATEFUL_ENTRY_MATCH | FWL_FAILURE           */
/****************************************************************************/

PRIVATE UINT4
FwlInspectIcmpErrorPayload (tCRU_BUF_CHAIN_HEADER * pPktBuf,
                            UINT4 u4IcmpOffset, UINT1 u1Direction)
{
    tIpHeader           IpHdr;
    tHLInfo             HLInfo;
    tIcmpInfo           IcmpHdr;
    tCRU_BUF_CHAIN_HEADER *pBufIcmpPayload = NULL;
    UINT4               u4PktLen = FWL_ZERO;
    UINT4               u4Status = FWL_FAILURE;
    UINT1               u1LogTrigger = FWL_ZERO;
    tStatefulSessionNode *pStateNode = NULL;
    INT4                i4RetVal = FWL_ZERO;

    MEMSET (&HLInfo, FWL_ZERO, sizeof (tHLInfo));
    /* If it is an ICMP error message, check the ICMP payload
     * and transport info in the payload should match the
     * stateful entry ie. the error packet should be a response
     * for the packet sent out
     */

    /* Toggle the direction field, for matching the entry in the state table */
    u1Direction = (u1Direction == FWL_DIRECTION_OUT) ?
        FWL_DIRECTION_IN : FWL_DIRECTION_OUT;

    i4RetVal =
        CRU_BUF_Fragment_BufChain (pPktBuf,
                                   u4IcmpOffset + FWL_ICMP_PAYLOAD_OFFSET,
                                   &pBufIcmpPayload);
    if (i4RetVal != FWL_SUCCESS)
    {
        return FWL_FAILURE;
    }

    if (pBufIcmpPayload == NULL)
    {
        return (FWL_FAILURE);
    }

    u4PktLen = CRU_BUF_Get_ChainValidByteCount (pBufIcmpPayload);

    if (u4PktLen < FWL_IP_HEADER_LEN + FWL_ICMP_HEADER_LEN)
    {
        /* join the ICMP hdr and the payload */
        CRU_BUF_Concat_MsgBufChains (pPktBuf, pBufIcmpPayload);
        return (FWL_FAILURE);
    }

    u4Status = FwlUpdateIpHeaderInfoFromPkt (pBufIcmpPayload, &IpHdr);

    switch (u4Status)
    {
        case FWL_IP_PKT_TOO_BIG:
        case FWL_INVALID_IP_OPTIONS:
        case FWL_ZERO_LEN_IP_OPTION:
            /* join the ICMP hdr and the payload */
            CRU_BUF_Concat_MsgBufChains (pPktBuf, pBufIcmpPayload);
            return (FWL_FAILURE);
        case FWL_IPHDR_LEN_FIELDS_INVALID:
        default:
            break;
    }                            /* End-of-Switch-Case */

    switch (IpHdr.u1Proto)
    {
        case FWL_ICMP:
            /* Updation of Icmp type and Code in the ICMP info struct  */
            FwlUpdateIcmpv4v6InfoFromPkt (pBufIcmpPayload, &IcmpHdr,
                                          IpHdr.u1HeadLen);
            break;

        case FWL_TCP:
        case FWL_UDP:
            FwlUpdateHLInfoFromPkt (pBufIcmpPayload, &HLInfo, IpHdr.u1Proto,
                                    IpHdr.u1HeadLen);
            break;

        default:
            /* join the ICMP hdr and the payload */
            CRU_BUF_Concat_MsgBufChains (pPktBuf, pBufIcmpPayload);
            return (FWL_FAILURE);
    }

    /* join the ICMP hdr and the payload */
    CRU_BUF_Concat_MsgBufChains (pPktBuf, pBufIcmpPayload);

    if (FwlMatchStateFilter (&IpHdr,
                             &HLInfo,
                             &IcmpHdr,
                             u1Direction,
                             &u1LogTrigger, &pStateNode) == FWL_MATCH)
    {
        return (FWL_STATEFUL_ENTRY_MATCH);
    }

    return (FWL_FAILURE);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlInspectIcmpv6ErrorPayload                     */
/*                                                                          */
/*    Description        : Inspects the payload in the ICMPv6 packet and    */
/*                         check if it matches iin the stateful entry ie.   */
/*                         the error packet should be a response to the     */
/*                         packet sent                                      */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the Packet            */
/*                         u4IcmpOffset -- Offset at which payload begins   */
/*                         u1Direction  -- Direction IN/OUT                 */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_STATEFUL_ENTRY_MATCH | FWL_FAILURE           */
/****************************************************************************/

PRIVATE UINT4
FwlInspectIcmpv6ErrorPayload (tCRU_BUF_CHAIN_HEADER * pPktBuf,
                              UINT4 u4Icmpv6Offset, UINT1 u1Direction)
{
    tIpHeader           IpHdr;
    tHLInfo             HLInfo;
    tIcmpInfo           Icmpv6Hdr;
    tCRU_BUF_CHAIN_HEADER *pBufIcmpPayload = NULL;
    UINT4               u4PktLen = FWL_ZERO;
    UINT4               u4Status = FWL_FAILURE;
    UINT1               u1LogTrigger = FWL_ZERO;
    INT4                i4RetVal = FWL_ZERO;

    tStatefulSessionNode *pStateNode = NULL;
    MEMSET (&HLInfo, FWL_ZERO, sizeof (tHLInfo));
    u1Direction = (u1Direction == FWL_DIRECTION_OUT) ?
        FWL_DIRECTION_IN : FWL_DIRECTION_OUT;
    i4RetVal = CRU_BUF_Fragment_BufChain (pPktBuf,
                                          u4Icmpv6Offset +
                                          FWL_ICMP_PAYLOAD_OFFSET,
                                          &pBufIcmpPayload);

    UNUSED_PARAM (i4RetVal);
    if (pBufIcmpPayload == NULL)
    {
        return (FWL_FAILURE);
    }
    u4PktLen = CRU_BUF_Get_ChainValidByteCount (pBufIcmpPayload);
    if (u4PktLen < FWL_IP6_HEADER_LEN + FWL_ICMP_HEADER_LEN)
    {
        CRU_BUF_Concat_MsgBufChains (pPktBuf, pBufIcmpPayload);
        return (FWL_FAILURE);
    }
    u4Status = FwlUpdateIpv6HeaderInfoFromPkt (pBufIcmpPayload, &IpHdr);
    switch (u4Status)
    {
        case FWL_IP_PKT_TOO_BIG:
        case FWL_INVALID_IP_OPTIONS:
        case FWL_ZERO_LEN_IP_OPTION:
            CRU_BUF_Concat_MsgBufChains (pPktBuf, pBufIcmpPayload);
            return (FWL_FAILURE);
        case FWL_IPHDR_LEN_FIELDS_INVALID:
        default:
            break;
    }                            /* End-of-Switch-Case */
    switch (IpHdr.u1Proto)
    {
        case FWL_ICMP:
            FwlUpdateIcmpv4v6InfoFromPkt (pBufIcmpPayload, &Icmpv6Hdr,
                                          IpHdr.u1HeadLen);
            break;
        case FWL_TCP:
        case FWL_UDP:
            FwlUpdateHLInfoFromPkt (pBufIcmpPayload, &HLInfo, IpHdr.u1Proto,
                                    IpHdr.u1HeadLen);
            break;
        default:
            CRU_BUF_Concat_MsgBufChains (pPktBuf, pBufIcmpPayload);
            return (FWL_FAILURE);
    }
    CRU_BUF_Concat_MsgBufChains (pPktBuf, pBufIcmpPayload);
    if (FwlMatchStateFilter (&IpHdr,
                             &HLInfo,
                             &Icmpv6Hdr,
                             u1Direction,
                             &u1LogTrigger, &pStateNode) == FWL_MATCH)
    {
        return (FWL_STATEFUL_ENTRY_MATCH);
    }
    return (FWL_FAILURE);
}
