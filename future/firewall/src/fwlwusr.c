/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlwusr.c,v 1.23 2017/01/13 11:56:45 siva Exp $
 *
 * *******************************************************************/

#include "fwlwincs.h"

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalMasterControlSwitch
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalMasterControlSwitch (INT4 *pi4RetValFwlGlobalMasterControlSwitch)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalMasterControlSwitch lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_MASTER_CONTROL_SWITCH;
    lv.pi4RetValFwlGlobalMasterControlSwitch =
        pi4RetValFwlGlobalMasterControlSwitch;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalICMPControlSwitch
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalICMPControlSwitch (INT4 *pi4RetValFwlGlobalICMPControlSwitch)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalICMPControlSwitch lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_I_C_M_P_CONTROL_SWITCH;
    lv.pi4RetValFwlGlobalICMPControlSwitch =
        pi4RetValFwlGlobalICMPControlSwitch;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalIpSpoofFiltering
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalIpSpoofFiltering (INT4 *pi4RetValFwlGlobalIpSpoofFiltering)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalIpSpoofFiltering lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_IP_SPOOF_FILTERING;
    lv.pi4RetValFwlGlobalIpSpoofFiltering = pi4RetValFwlGlobalIpSpoofFiltering;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalSrcRouteFiltering
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalSrcRouteFiltering (INT4 *pi4RetValFwlGlobalSrcRouteFiltering)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalSrcRouteFiltering lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_SRC_ROUTE_FILTERING;
    lv.pi4RetValFwlGlobalSrcRouteFiltering =
        pi4RetValFwlGlobalSrcRouteFiltering;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalTinyFragmentFiltering
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalTinyFragmentFiltering (INT4
                                      *pi4RetValFwlGlobalTinyFragmentFiltering)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalTinyFragmentFiltering lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_TINY_FRAGMENT_FILTERING;
    lv.pi4RetValFwlGlobalTinyFragmentFiltering =
        pi4RetValFwlGlobalTinyFragmentFiltering;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalTcpIntercept
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalTcpIntercept (INT4 *pi4RetValFwlGlobalTcpIntercept)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalTcpIntercept lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_TCP_INTERCEPT;
    lv.pi4RetValFwlGlobalTcpIntercept = pi4RetValFwlGlobalTcpIntercept;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalTrap
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalTrap (INT4 *pi4RetValFwlGlobalTrap)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalTrap lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_TRAP;
    lv.pi4RetValFwlGlobalTrap = pi4RetValFwlGlobalTrap;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalTrace
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalTrace (INT4 *pi4RetValFwlGlobalTrace)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalTrace lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_TRACE;
    lv.pi4RetValFwlGlobalTrace = pi4RetValFwlGlobalTrace;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalDebug
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalDebug (INT4 *pi4RetValFwlGlobalDebug)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalDebug lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_DEBUG;
    lv.pi4RetValFwlGlobalDebug = pi4RetValFwlGlobalDebug;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalMaxFilters
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalMaxFilters (INT4 *pi4RetValFwlGlobalMaxFilters)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalMaxFilters lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_MAX_FILTERS;
    lv.pi4RetValFwlGlobalMaxFilters = pi4RetValFwlGlobalMaxFilters;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalMaxRules
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalMaxRules (INT4 *pi4RetValFwlGlobalMaxRules)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalMaxRules lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_MAX_RULES;
    lv.pi4RetValFwlGlobalMaxRules = pi4RetValFwlGlobalMaxRules;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalUrlFiltering
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalUrlFiltering (INT4 *pi4RetValFwlGlobalUrlFiltering)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalUrlFiltering lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_URL_FILTERING;
    lv.pi4RetValFwlGlobalUrlFiltering = pi4RetValFwlGlobalUrlFiltering;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalLogFileSize
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalLogFileSize (UINT4 *pu4RetValFwlGlobalLogFileSize)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalLogFileSize lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_LOG_FILE_SIZE;
    lv.pu4RetValFwlGlobalLogFileSize = pu4RetValFwlGlobalLogFileSize;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalLogSizeThreshold
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalLogSizeThreshold (UINT4 *pu4RetValFwlGlobalLogSizeThreshold)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalLogSizeThreshold lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_LOG_SIZE_THRESHOLD;
    lv.pu4RetValFwlGlobalLogSizeThreshold = pu4RetValFwlGlobalLogSizeThreshold;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalIdsLogSize
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalIdsLogSize (UINT4 *pu4RetValFwlGlobalIdsLogSize)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalIdsLogSize lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_IDS_LOG_SIZE;
    lv.pu4RetValFwlGlobalIdsLogSize = pu4RetValFwlGlobalIdsLogSize;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalIdsLogThreshold
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalIdsLogThreshold (UINT4 *pu4RetValFwlGlobalIdsLogThreshold)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalIdsLogThreshold lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_IDS_LOG_THRESHOLD;
    lv.pu4RetValFwlGlobalIdsLogThreshold = pu4RetValFwlGlobalIdsLogThreshold;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalIdsVersionInfo
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalIdsVersionInfo (tSNMP_OCTET_STRING_TYPE *
                               pRetValFwlGlobalIdsVersionInfo)
{
    tIdsGetInfo         IdsGetInfo;

    MEMSET (&IdsGetInfo, 0, sizeof (tIdsGetInfo));
    pRetValFwlGlobalIdsVersionInfo->i4_Length = 0;

    if (SecIdsGetInfo (ISS_IDS_VERSION_REQ, &IdsGetInfo) != IDS_FAILURE)
    {
        if (&IdsGetInfo != NULL)
        {
            pRetValFwlGlobalIdsVersionInfo->i4_Length =
                STRLEN (IdsGetInfo.au1Version);
            MEMCPY (pRetValFwlGlobalIdsVersionInfo->pu1_OctetList,
                    IdsGetInfo.au1Version,
                    pRetValFwlGlobalIdsVersionInfo->i4_Length);
        }
    }
    return SNMP_SUCCESS;
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalReloadIds
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalReloadIds (INT4 *pi4RetValFwlGlobalReloadIds)
{
    *pi4RetValFwlGlobalReloadIds = 0;
    return SNMP_SUCCESS;
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalIdsStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalIdsStatus (INT4 *pi4RetValFwlGlobalIdsStatus)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalIdsStatus lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_IDS_STATUS;
    lv.pi4RetValFwlGlobalIdsStatus = pi4RetValFwlGlobalIdsStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 Function    :  nmhGetFwlGlobalLoadIdsRules
 *
 * -------------------------------------------------------------*/
INT1
nmhGetFwlGlobalLoadIdsRules (INT4 *pi4RetValFwlGlobalLoadIdsRules)
{
    *pi4RetValFwlGlobalLoadIdsRules = gi4IdsRulesStatus;
    return SNMP_SUCCESS;
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlTrapFileName
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlTrapFileName (tSNMP_OCTET_STRING_TYPE * pRetValFwlTrapFileName)
{
    int                 rc;
    tFwlwnmhGetFwlTrapFileName lv;

    lv.cmd = NMH_GET_FWL_TRAP_FILE_NAME;
    lv.pRetValFwlTrapFileName = pRetValFwlTrapFileName;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlIdsTrapFileName
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlIdsTrapFileName (tSNMP_OCTET_STRING_TYPE * pRetValFwlIdsTrapFileName)
{
    int                 rc;
    tFwlwnmhGetFwlIdsTrapFileName lv;

    lv.cmd = NMH_GET_FWL_IDS_TRAP_FILE_NAME;
    lv.pRetValFwlIdsTrapFileName = pRetValFwlIdsTrapFileName;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalMasterControlSwitch
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalMasterControlSwitch (INT4 i4SetValFwlGlobalMasterControlSwitch)
{
    int                 rc;
    tFwlwnmhSetFwlGlobalMasterControlSwitch lv;

    if ((i4SetValFwlGlobalMasterControlSwitch == FWL_ENABLE) && 
	  (FWL_DISABLE == FwlUtilGetGlobalStatus ())) 
    {
        FwlNpEnableDosAttack();
        FwlNpEnableIpHeaderValidation();
    }
    if ((i4SetValFwlGlobalMasterControlSwitch == FWL_DISABLE) && 
	  (FWL_ENABLE == FwlUtilGetGlobalStatus ()))
    {
        FwlNpDisableDosAttack(); 
        FwlNpDisableIpHeaderValidation();
    }

    lv.cmd = NMH_SET_FWL_GLOBAL_MASTER_CONTROL_SWITCH;
    lv.i4SetValFwlGlobalMasterControlSwitch =
        i4SetValFwlGlobalMasterControlSwitch;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

#ifdef NPAPI_WANTED
    /* Set the security throughput meter limit according to the 
     * status of firewall module */
     FsSecNpHwSetRateLimit(ISS_SEC_FWL_MODULE,
            i4SetValFwlGlobalMasterControlSwitch);
#endif
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalICMPControlSwitch
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalICMPControlSwitch (INT4 i4SetValFwlGlobalICMPControlSwitch)
{
    int                 rc;
    tFwlwnmhSetFwlGlobalICMPControlSwitch lv;

    lv.cmd = NMH_SET_FWL_GLOBAL_I_C_M_P_CONTROL_SWITCH;
    lv.i4SetValFwlGlobalICMPControlSwitch = i4SetValFwlGlobalICMPControlSwitch;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalIpSpoofFiltering
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalIpSpoofFiltering (INT4 i4SetValFwlGlobalIpSpoofFiltering)
{
    int                 rc;
    tFwlwnmhSetFwlGlobalIpSpoofFiltering lv;

    lv.cmd = NMH_SET_FWL_GLOBAL_IP_SPOOF_FILTERING;
    lv.i4SetValFwlGlobalIpSpoofFiltering = i4SetValFwlGlobalIpSpoofFiltering;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalSrcRouteFiltering
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalSrcRouteFiltering (INT4 i4SetValFwlGlobalSrcRouteFiltering)
{
    int                 rc;
    tFwlwnmhSetFwlGlobalSrcRouteFiltering lv;

    lv.cmd = NMH_SET_FWL_GLOBAL_SRC_ROUTE_FILTERING;
    lv.i4SetValFwlGlobalSrcRouteFiltering = i4SetValFwlGlobalSrcRouteFiltering;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalTinyFragmentFiltering
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalTinyFragmentFiltering (INT4
                                      i4SetValFwlGlobalTinyFragmentFiltering)
{
    int                 rc;
    tFwlwnmhSetFwlGlobalTinyFragmentFiltering lv;

    lv.cmd = NMH_SET_FWL_GLOBAL_TINY_FRAGMENT_FILTERING;
    lv.i4SetValFwlGlobalTinyFragmentFiltering =
        i4SetValFwlGlobalTinyFragmentFiltering;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalTcpIntercept
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalTcpIntercept (INT4 i4SetValFwlGlobalTcpIntercept)
{
    int                 rc;
    tFwlwnmhSetFwlGlobalTcpIntercept lv;

    lv.cmd = NMH_SET_FWL_GLOBAL_TCP_INTERCEPT;
    lv.i4SetValFwlGlobalTcpIntercept = i4SetValFwlGlobalTcpIntercept;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalTrap
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalTrap (INT4 i4SetValFwlGlobalTrap)
{
    int                 rc;
    tFwlwnmhSetFwlGlobalTrap lv;

    lv.cmd = NMH_SET_FWL_GLOBAL_TRAP;
    lv.i4SetValFwlGlobalTrap = i4SetValFwlGlobalTrap;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalTrace
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalTrace (INT4 i4SetValFwlGlobalTrace)
{
    int                 rc;
    int                 i4RetValFwlGlobalTrap;
    tFwlwnmhSetFwlGlobalTrace lv;

    lv.cmd = NMH_SET_FWL_GLOBAL_TRACE;
    lv.i4SetValFwlGlobalTrace = i4SetValFwlGlobalTrace;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    if (rc >= 0)
    {
        nmhGetFwlGlobalTrap (&i4RetValFwlGlobalTrap);
        if (i4RetValFwlGlobalTrap == FWL_ENABLE)
        {
            SecIdsSetInfo (ISS_IDS_SET_PKT_DROP_THRESH,
                           gFwlAclInfo.i4TrapThreshold);
        }
        else
        {
            SecIdsSetInfo (ISS_IDS_SET_PKT_DROP_THRESH, 0);
        }
    }

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalDebug
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalDebug (INT4 i4SetValFwlGlobalDebug)
{
    int                 rc;
    tFwlwnmhSetFwlGlobalDebug lv;

    lv.cmd = NMH_SET_FWL_GLOBAL_DEBUG;
    lv.i4SetValFwlGlobalDebug = i4SetValFwlGlobalDebug;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalUrlFiltering
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalUrlFiltering (INT4 i4SetValFwlGlobalUrlFiltering)
{
    int                 rc;
    tFwlwnmhSetFwlGlobalUrlFiltering lv;

    lv.cmd = NMH_SET_FWL_GLOBAL_URL_FILTERING;
    lv.i4SetValFwlGlobalUrlFiltering = i4SetValFwlGlobalUrlFiltering;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalLogFileSize
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalLogFileSize (UINT4 u4SetValFwlGlobalLogFileSize)
{
    int                 rc;
    tFwlwnmhSetFwlGlobalLogFileSize lv;

    lv.cmd = NMH_SET_FWL_GLOBAL_LOG_FILE_SIZE;
    lv.u4SetValFwlGlobalLogFileSize = u4SetValFwlGlobalLogFileSize;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalLogSizeThreshold
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalLogSizeThreshold (UINT4 u4SetValFwlGlobalLogSizeThreshold)
{
    int                 rc;
    tFwlwnmhSetFwlGlobalLogSizeThreshold lv;

    lv.cmd = NMH_SET_FWL_GLOBAL_LOG_SIZE_THRESHOLD;
    lv.u4SetValFwlGlobalLogSizeThreshold = u4SetValFwlGlobalLogSizeThreshold;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalIdsLogSize
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalIdsLogSize (UINT4 u4SetValFwlGlobalIdsLogSize)
{
    int                 rc;
    tFwlwnmhSetFwlGlobalIdsLogSize lv;

    lv.cmd = NMH_SET_FWL_GLOBAL_IDS_LOG_SIZE;
    lv.u4SetValFwlGlobalIdsLogSize = u4SetValFwlGlobalIdsLogSize;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalIdsLogThreshold
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalIdsLogThreshold (UINT4 u4SetValFwlGlobalIdsLogThreshold)
{
    int                 rc;
    tFwlwnmhSetFwlGlobalIdsLogThreshold lv;

    lv.cmd = NMH_SET_FWL_GLOBAL_IDS_LOG_THRESHOLD;
    lv.u4SetValFwlGlobalIdsLogThreshold = u4SetValFwlGlobalIdsLogThreshold;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalReloadIds
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalReloadIds (INT4 i4SetValFwlGlobalReloadIds)
{
    UNUSED_PARAM (i4SetValFwlGlobalReloadIds);

    /* This is done to update IDS status in kernel space */
    nmhGetFwlGlobalIdsStatus (&gu4PrevIdsStatus);
    if (nmhSetFwlGlobalIdsStatus (IDS_DISABLE) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (SecIdsSetInfo (ISS_IDS_RESET_IDS, 0) != IDS_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalIdsStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalIdsStatus (INT4 i4SetValFwlGlobalIdsStatus)
{
    int                 rc;
    tFwlwnmhSetFwlGlobalIdsStatus lv;

    if ((i4SetValFwlGlobalIdsStatus == IDS_ENABLE) &&
        (gi4IdsRulesStatus != IDS_RULES_LOADED))
    {
        return SNMP_FAILURE;
    }
    /*Store the current IDsStatus in kernel before overwriting it*/

    gu4PrevIdsStatus = gu4IdsStatus; 

#ifdef NPAPI_WANTED
    /* Set the security throughput meter limit according to the 
     * status of IDS module */
    FsSecNpHwSetRateLimit (ISS_SEC_IDS_MODULE, i4SetValFwlGlobalIdsStatus);
#endif
    lv.cmd = NMH_SET_FWL_GLOBAL_IDS_STATUS;
    lv.i4SetValFwlGlobalIdsStatus = i4SetValFwlGlobalIdsStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalLoadIdsRules
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalLoadIdsRules (INT4 i4SetValFwlGlobalLoadIdsRules)
{
    /* This is done to update IDS status in kernel space */
    nmhGetFwlGlobalIdsStatus (&gu4PrevIdsStatus);
    if (nmhSetFwlGlobalIdsStatus (IDS_DISABLE) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFwlGlobalLoadIdsRules == IDS_LOAD_RULES)
    {
        if (SecIdsSetInfo (ISS_IDS_LOAD_RULES, 0) != IDS_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        gi4IdsRulesStatus = IDS_RULES_LOAD_IN_PROGRESS;
    }
    else if (i4SetValFwlGlobalLoadIdsRules == IDS_UNLOAD_RULES)
    {
        if (SecIdsSetInfo (ISS_IDS_UNLOAD_RULES, 0) != IDS_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        gi4IdsRulesStatus = IDS_RULES_NOT_LOADED;
    }
    return SNMP_SUCCESS;
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalMasterControlSwitch
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalMasterControlSwitch (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFwlGlobalMasterControlSwitch)
{
    int                 rc;
    tFwlwnmhTestv2FwlGlobalMasterControlSwitch lv;

    lv.cmd = NMH_TESTV2_FWL_GLOBAL_MASTER_CONTROL_SWITCH;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFwlGlobalMasterControlSwitch =
        i4TestValFwlGlobalMasterControlSwitch;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    /* 
     * below check done in fsfwllw.c will be in effect only for user space
     * compilation. since FwlUtilCallBack won't be available in kernel space 
     * the same is added in the user space (here)
     */

    if ((FWL_DISABLE == i4TestValFwlGlobalMasterControlSwitch)
        && (FWL_ENABLE == FwlUtilGetGlobalStatus ()))
    {
        if (FWL_FAILURE == FwlUtilCallBack (FWL_CUST_IF_CHECK_EVENT))
        {
            FwlLogMessage (FWL_WAN_INTF_STATUS_CHECK_FAIL,
                           0, NULL, FWL_LOG_MUST, FWLLOG_ALERT_LEVEL,
                           (UINT1 *) FWL_MSG_WAN_INTF_STATUS_CHECK_FAIL);
            *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
            rc = -1;
        }
    }

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalICMPControlSwitch
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalICMPControlSwitch (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFwlGlobalICMPControlSwitch)
{
    int                 rc;
    tFwlwnmhTestv2FwlGlobalICMPControlSwitch lv;

    lv.cmd = NMH_TESTV2_FWL_GLOBAL_I_C_M_P_CONTROL_SWITCH;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFwlGlobalICMPControlSwitch =
        i4TestValFwlGlobalICMPControlSwitch;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalIpSpoofFiltering
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalIpSpoofFiltering (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFwlGlobalIpSpoofFiltering)
{
    int                 rc;
    tFwlwnmhTestv2FwlGlobalIpSpoofFiltering lv;

    lv.cmd = NMH_TESTV2_FWL_GLOBAL_IP_SPOOF_FILTERING;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFwlGlobalIpSpoofFiltering = i4TestValFwlGlobalIpSpoofFiltering;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalSrcRouteFiltering
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalSrcRouteFiltering (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFwlGlobalSrcRouteFiltering)
{
    int                 rc;
    tFwlwnmhTestv2FwlGlobalSrcRouteFiltering lv;

    lv.cmd = NMH_TESTV2_FWL_GLOBAL_SRC_ROUTE_FILTERING;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFwlGlobalSrcRouteFiltering =
        i4TestValFwlGlobalSrcRouteFiltering;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalTinyFragmentFiltering
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalTinyFragmentFiltering (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4TestValFwlGlobalTinyFragmentFiltering)
{
    int                 rc;
    tFwlwnmhTestv2FwlGlobalTinyFragmentFiltering lv;

    lv.cmd = NMH_TESTV2_FWL_GLOBAL_TINY_FRAGMENT_FILTERING;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFwlGlobalTinyFragmentFiltering =
        i4TestValFwlGlobalTinyFragmentFiltering;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalTcpIntercept
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalTcpIntercept (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFwlGlobalTcpIntercept)
{
    int                 rc;
    tFwlwnmhTestv2FwlGlobalTcpIntercept lv;

    lv.cmd = NMH_TESTV2_FWL_GLOBAL_TCP_INTERCEPT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFwlGlobalTcpIntercept = i4TestValFwlGlobalTcpIntercept;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalTrap
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalTrap (UINT4 *pu4ErrorCode, INT4 i4TestValFwlGlobalTrap)
{
    int                 rc;
    tFwlwnmhTestv2FwlGlobalTrap lv;

    lv.cmd = NMH_TESTV2_FWL_GLOBAL_TRAP;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFwlGlobalTrap = i4TestValFwlGlobalTrap;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalTrace
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalTrace (UINT4 *pu4ErrorCode, INT4 i4TestValFwlGlobalTrace)
{
    int                 rc;
    tFwlwnmhTestv2FwlGlobalTrace lv;

    lv.cmd = NMH_TESTV2_FWL_GLOBAL_TRACE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFwlGlobalTrace = i4TestValFwlGlobalTrace;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalDebug
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalDebug (UINT4 *pu4ErrorCode, INT4 i4TestValFwlGlobalDebug)
{
    int                 rc;
    tFwlwnmhTestv2FwlGlobalDebug lv;

    lv.cmd = NMH_TESTV2_FWL_GLOBAL_DEBUG;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFwlGlobalDebug = i4TestValFwlGlobalDebug;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalUrlFiltering
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalUrlFiltering (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFwlGlobalUrlFiltering)
{
    int                 rc;
    tFwlwnmhTestv2FwlGlobalUrlFiltering lv;

    lv.cmd = NMH_TESTV2_FWL_GLOBAL_URL_FILTERING;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFwlGlobalUrlFiltering = i4TestValFwlGlobalUrlFiltering;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalNetBiosFiltering
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalNetBiosFiltering (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFwlGlobalNetBiosFiltering)
{
    int                 rc;
    tFwlwnmhTestv2FwlGlobalNetBiosFiltering lv;

    lv.cmd = NMH_TESTV2_FWL_GLOBAL_NET_BIOS_FILTERING;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFwlGlobalNetBiosFiltering = i4TestValFwlGlobalNetBiosFiltering;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlDefnTcpInterceptThreshold
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlDefnTcpInterceptThreshold (INT4 *pi4RetValFwlDefnTcpInterceptThreshold)
{
    int                 rc;
    tFwlwnmhGetFwlDefnTcpInterceptThreshold lv;

    lv.cmd = NMH_GET_FWL_DEFN_TCP_INTERCEPT_THRESHOLD;
    lv.pi4RetValFwlDefnTcpInterceptThreshold =
        pi4RetValFwlDefnTcpInterceptThreshold;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlDefnInterceptTimeout
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlDefnInterceptTimeout (UINT4 *pu4RetValFwlDefnInterceptTimeout)
{
    int                 rc;
    tFwlwnmhGetFwlDefnInterceptTimeout lv;

    lv.cmd = NMH_GET_FWL_DEFN_INTERCEPT_TIMEOUT;
    lv.pu4RetValFwlDefnInterceptTimeout = pu4RetValFwlDefnInterceptTimeout;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlDefnTcpInterceptThreshold
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlDefnTcpInterceptThreshold (INT4 i4SetValFwlDefnTcpInterceptThreshold)
{
    int                 rc;
    tFwlwnmhSetFwlDefnTcpInterceptThreshold lv;

    lv.cmd = NMH_SET_FWL_DEFN_TCP_INTERCEPT_THRESHOLD;
    lv.i4SetValFwlDefnTcpInterceptThreshold =
        i4SetValFwlDefnTcpInterceptThreshold;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlDefnInterceptTimeout
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlDefnInterceptTimeout (UINT4 u4SetValFwlDefnInterceptTimeout)
{
    int                 rc;
    tFwlwnmhSetFwlDefnInterceptTimeout lv;

    lv.cmd = NMH_SET_FWL_DEFN_INTERCEPT_TIMEOUT;
    lv.u4SetValFwlDefnInterceptTimeout = u4SetValFwlDefnInterceptTimeout;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlDefnTcpInterceptThreshold
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlDefnTcpInterceptThreshold (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFwlDefnTcpInterceptThreshold)
{
    int                 rc;
    tFwlwnmhTestv2FwlDefnTcpInterceptThreshold lv;

    lv.cmd = NMH_TESTV2_FWL_DEFN_TCP_INTERCEPT_THRESHOLD;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFwlDefnTcpInterceptThreshold =
        i4TestValFwlDefnTcpInterceptThreshold;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlDefnInterceptTimeout
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlDefnInterceptTimeout (UINT4 *pu4ErrorCode,
                                  UINT4 u4TestValFwlDefnInterceptTimeout)
{
    int                 rc;
    tFwlwnmhTestv2FwlDefnInterceptTimeout lv;

    lv.cmd = NMH_TESTV2_FWL_DEFN_INTERCEPT_TIMEOUT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.u4TestValFwlDefnInterceptTimeout = u4TestValFwlDefnInterceptTimeout;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceFwlDefnFilterTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceFwlDefnFilterTable (tSNMP_OCTET_STRING_TYPE *
                                            pFwlFilterFilterName)
{
    int                 rc;
    tFwlwnmhValidateIndexInstanceFwlDefnFilterTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_FWL_DEFN_FILTER_TABLE;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexFwlDefnFilterTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexFwlDefnFilterTable (tSNMP_OCTET_STRING_TYPE *
                                    pFwlFilterFilterName)
{
    int                 rc;
    tFwlwnmhGetFirstIndexFwlDefnFilterTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_FWL_DEFN_FILTER_TABLE;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexFwlDefnFilterTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexFwlDefnFilterTable (tSNMP_OCTET_STRING_TYPE *
                                   pFwlFilterFilterName,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextFwlFilterFilterName)
{
    int                 rc;
    tFwlwnmhGetNextIndexFwlDefnFilterTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_FWL_DEFN_FILTER_TABLE;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pNextFwlFilterFilterName = pNextFwlFilterFilterName;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlFilterSrcAddress
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlFilterSrcAddress (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                           tSNMP_OCTET_STRING_TYPE * pRetValFwlFilterSrcAddress)
{
    int                 rc;
    tFwlwnmhGetFwlFilterSrcAddress lv;

    lv.cmd = NMH_GET_FWL_FILTER_SRC_ADDRESS;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pRetValFwlFilterSrcAddress = pRetValFwlFilterSrcAddress;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlFilterDestAddress
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlFilterDestAddress (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFwlFilterDestAddress)
{
    int                 rc;
    tFwlwnmhGetFwlFilterDestAddress lv;

    lv.cmd = NMH_GET_FWL_FILTER_DEST_ADDRESS;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pRetValFwlFilterDestAddress = pRetValFwlFilterDestAddress;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlFilterProtocol
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlFilterProtocol (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                         INT4 *pi4RetValFwlFilterProtocol)
{
    int                 rc;
    tFwlwnmhGetFwlFilterProtocol lv;

    lv.cmd = NMH_GET_FWL_FILTER_PROTOCOL;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pi4RetValFwlFilterProtocol = pi4RetValFwlFilterProtocol;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlFilterSrcPort
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlFilterSrcPort (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                        tSNMP_OCTET_STRING_TYPE * pRetValFwlFilterSrcPort)
{
    int                 rc;
    tFwlwnmhGetFwlFilterSrcPort lv;

    lv.cmd = NMH_GET_FWL_FILTER_SRC_PORT;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pRetValFwlFilterSrcPort = pRetValFwlFilterSrcPort;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlFilterDestPort
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlFilterDestPort (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                         tSNMP_OCTET_STRING_TYPE * pRetValFwlFilterDestPort)
{
    int                 rc;
    tFwlwnmhGetFwlFilterDestPort lv;

    lv.cmd = NMH_GET_FWL_FILTER_DEST_PORT;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pRetValFwlFilterDestPort = pRetValFwlFilterDestPort;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlFilterAckBit
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlFilterAckBit (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                       INT4 *pi4RetValFwlFilterAckBit)
{
    int                 rc;
    tFwlwnmhGetFwlFilterAckBit lv;

    lv.cmd = NMH_GET_FWL_FILTER_ACK_BIT;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pi4RetValFwlFilterAckBit = pi4RetValFwlFilterAckBit;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlFilterRstBit
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlFilterRstBit (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                       INT4 *pi4RetValFwlFilterRstBit)
{
    int                 rc;
    tFwlwnmhGetFwlFilterRstBit lv;

    lv.cmd = NMH_GET_FWL_FILTER_RST_BIT;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pi4RetValFwlFilterRstBit = pi4RetValFwlFilterRstBit;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlFilterTos
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlFilterTos (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                    INT4 *pi4RetValFwlFilterTos)
{
    int                 rc;
    tFwlwnmhGetFwlFilterTos lv;

    lv.cmd = NMH_GET_FWL_FILTER_TOS;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pi4RetValFwlFilterTos = pi4RetValFwlFilterTos;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlFilterAccounting
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlFilterAccounting (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                           INT4 *pi4RetValFwlFilterAccounting)
{
    int                 rc;
    tFwlwnmhGetFwlFilterAccounting lv;

    lv.cmd = NMH_GET_FWL_FILTER_ACCOUNTING;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pi4RetValFwlFilterAccounting = pi4RetValFwlFilterAccounting;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlFilterHitClear
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlFilterHitClear (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                         INT4 *pi4RetValFwlFilterHitClear)
{
    int                 rc;
    tFwlwnmhGetFwlFilterHitClear lv;

    lv.cmd = NMH_GET_FWL_FILTER_HIT_CLEAR;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pi4RetValFwlFilterHitClear = pi4RetValFwlFilterHitClear;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlFilterHitsCount
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlFilterHitsCount (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                          UINT4 *pu4RetValFwlFilterHitsCount)
{
    int                 rc;
    tFwlwnmhGetFwlFilterHitsCount lv;

    lv.cmd = NMH_GET_FWL_FILTER_HITS_COUNT;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pu4RetValFwlFilterHitsCount = pu4RetValFwlFilterHitsCount;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlFilterRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlFilterRowStatus (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                          INT4 *pi4RetValFwlFilterRowStatus)
{
    int                 rc;
    tFwlwnmhGetFwlFilterRowStatus lv;

    lv.cmd = NMH_GET_FWL_FILTER_ROW_STATUS;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pi4RetValFwlFilterRowStatus = pi4RetValFwlFilterRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlFilterSrcAddress
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlFilterSrcAddress (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                           tSNMP_OCTET_STRING_TYPE * pSetValFwlFilterSrcAddress)
{
    int                 rc;
    tFwlwnmhSetFwlFilterSrcAddress lv;

    lv.cmd = NMH_SET_FWL_FILTER_SRC_ADDRESS;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pSetValFwlFilterSrcAddress = pSetValFwlFilterSrcAddress;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlFilterDestAddress
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlFilterDestAddress (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFwlFilterDestAddress)
{
    int                 rc;
    tFwlwnmhSetFwlFilterDestAddress lv;

    lv.cmd = NMH_SET_FWL_FILTER_DEST_ADDRESS;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pSetValFwlFilterDestAddress = pSetValFwlFilterDestAddress;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlFilterProtocol
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlFilterProtocol (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                         INT4 i4SetValFwlFilterProtocol)
{
    int                 rc;
    tFwlwnmhSetFwlFilterProtocol lv;

    lv.cmd = NMH_SET_FWL_FILTER_PROTOCOL;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.i4SetValFwlFilterProtocol = i4SetValFwlFilterProtocol;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlFilterSrcPort
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlFilterSrcPort (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                        tSNMP_OCTET_STRING_TYPE * pSetValFwlFilterSrcPort)
{
    int                 rc;
    tFwlwnmhSetFwlFilterSrcPort lv;

    lv.cmd = NMH_SET_FWL_FILTER_SRC_PORT;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pSetValFwlFilterSrcPort = pSetValFwlFilterSrcPort;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlFilterDestPort
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlFilterDestPort (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                         tSNMP_OCTET_STRING_TYPE * pSetValFwlFilterDestPort)
{
    int                 rc;
    tFwlwnmhSetFwlFilterDestPort lv;

    lv.cmd = NMH_SET_FWL_FILTER_DEST_PORT;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pSetValFwlFilterDestPort = pSetValFwlFilterDestPort;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlFilterAckBit
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlFilterAckBit (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                       INT4 i4SetValFwlFilterAckBit)
{
    int                 rc;
    tFwlwnmhSetFwlFilterAckBit lv;

    lv.cmd = NMH_SET_FWL_FILTER_ACK_BIT;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.i4SetValFwlFilterAckBit = i4SetValFwlFilterAckBit;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlFilterRstBit
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlFilterRstBit (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                       INT4 i4SetValFwlFilterRstBit)
{
    int                 rc;
    tFwlwnmhSetFwlFilterRstBit lv;

    lv.cmd = NMH_SET_FWL_FILTER_RST_BIT;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.i4SetValFwlFilterRstBit = i4SetValFwlFilterRstBit;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlFilterTos
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlFilterTos (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                    INT4 i4SetValFwlFilterTos)
{
    int                 rc;
    tFwlwnmhSetFwlFilterTos lv;

    lv.cmd = NMH_SET_FWL_FILTER_TOS;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.i4SetValFwlFilterTos = i4SetValFwlFilterTos;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlFilterAccounting
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlFilterAccounting (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                           INT4 i4SetValFwlFilterAccounting)
{
    int                 rc;
    tFwlwnmhSetFwlFilterAccounting lv;

    lv.cmd = NMH_SET_FWL_FILTER_ACCOUNTING;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.i4SetValFwlFilterAccounting = i4SetValFwlFilterAccounting;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlFilterHitClear
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlFilterHitClear (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                         INT4 i4SetValFwlFilterHitClear)
{
    int                 rc;
    tFwlwnmhSetFwlFilterHitClear lv;

    lv.cmd = NMH_SET_FWL_FILTER_HIT_CLEAR;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.i4SetValFwlFilterHitClear = i4SetValFwlFilterHitClear;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlFilterRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlFilterRowStatus (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                          INT4 i4SetValFwlFilterRowStatus)
{
    int                 rc;
    tFwlwnmhSetFwlFilterRowStatus lv;

    lv.cmd = NMH_SET_FWL_FILTER_ROW_STATUS;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.i4SetValFwlFilterRowStatus = i4SetValFwlFilterRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlFilterSrcAddress
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlFilterSrcAddress (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValFwlFilterSrcAddress)
{
    int                 rc;
    tFwlwnmhTestv2FwlFilterSrcAddress lv;

    lv.cmd = NMH_TESTV2_FWL_FILTER_SRC_ADDRESS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pTestValFwlFilterSrcAddress = pTestValFwlFilterSrcAddress;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlFilterDestAddress
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlFilterDestAddress (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFwlFilterDestAddress)
{
    int                 rc;
    tFwlwnmhTestv2FwlFilterDestAddress lv;

    lv.cmd = NMH_TESTV2_FWL_FILTER_DEST_ADDRESS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pTestValFwlFilterDestAddress = pTestValFwlFilterDestAddress;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlFilterProtocol
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlFilterProtocol (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                            INT4 i4TestValFwlFilterProtocol)
{
    int                 rc;
    tFwlwnmhTestv2FwlFilterProtocol lv;

    lv.cmd = NMH_TESTV2_FWL_FILTER_PROTOCOL;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.i4TestValFwlFilterProtocol = i4TestValFwlFilterProtocol;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlFilterSrcPort
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlFilterSrcPort (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                           tSNMP_OCTET_STRING_TYPE * pTestValFwlFilterSrcPort)
{
    int                 rc;
    tFwlwnmhTestv2FwlFilterSrcPort lv;

    lv.cmd = NMH_TESTV2_FWL_FILTER_SRC_PORT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pTestValFwlFilterSrcPort = pTestValFwlFilterSrcPort;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlFilterDestPort
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlFilterDestPort (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                            tSNMP_OCTET_STRING_TYPE * pTestValFwlFilterDestPort)
{
    int                 rc;
    tFwlwnmhTestv2FwlFilterDestPort lv;

    lv.cmd = NMH_TESTV2_FWL_FILTER_DEST_PORT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pTestValFwlFilterDestPort = pTestValFwlFilterDestPort;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlFilterAckBit
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlFilterAckBit (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                          INT4 i4TestValFwlFilterAckBit)
{
    int                 rc;
    tFwlwnmhTestv2FwlFilterAckBit lv;

    lv.cmd = NMH_TESTV2_FWL_FILTER_ACK_BIT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.i4TestValFwlFilterAckBit = i4TestValFwlFilterAckBit;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlFilterRstBit
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlFilterRstBit (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                          INT4 i4TestValFwlFilterRstBit)
{
    int                 rc;
    tFwlwnmhTestv2FwlFilterRstBit lv;

    lv.cmd = NMH_TESTV2_FWL_FILTER_RST_BIT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.i4TestValFwlFilterRstBit = i4TestValFwlFilterRstBit;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlFilterTos
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlFilterTos (UINT4 *pu4ErrorCode,
                       tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                       INT4 i4TestValFwlFilterTos)
{
    int                 rc;
    tFwlwnmhTestv2FwlFilterTos lv;

    lv.cmd = NMH_TESTV2_FWL_FILTER_TOS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.i4TestValFwlFilterTos = i4TestValFwlFilterTos;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlFilterAccounting
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlFilterAccounting (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                              INT4 i4TestValFwlFilterAccounting)
{
    int                 rc;
    tFwlwnmhTestv2FwlFilterAccounting lv;

    lv.cmd = NMH_TESTV2_FWL_FILTER_ACCOUNTING;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.i4TestValFwlFilterAccounting = i4TestValFwlFilterAccounting;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlFilterHitClear
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlFilterHitClear (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                            INT4 i4TestValFwlFilterHitClear)
{
    int                 rc;
    tFwlwnmhTestv2FwlFilterHitClear lv;

    lv.cmd = NMH_TESTV2_FWL_FILTER_HIT_CLEAR;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.i4TestValFwlFilterHitClear = i4TestValFwlFilterHitClear;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlFilterRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlFilterRowStatus (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                             INT4 i4TestValFwlFilterRowStatus)
{
    int                 rc;
    tFwlwnmhTestv2FwlFilterRowStatus lv;

    lv.cmd = NMH_TESTV2_FWL_FILTER_ROW_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.i4TestValFwlFilterRowStatus = i4TestValFwlFilterRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceFwlDefnRuleTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceFwlDefnRuleTable (tSNMP_OCTET_STRING_TYPE *
                                          pFwlRuleRuleName)
{
    int                 rc;
    tFwlwnmhValidateIndexInstanceFwlDefnRuleTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_FWL_DEFN_RULE_TABLE;
    lv.pFwlRuleRuleName = pFwlRuleRuleName;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexFwlDefnRuleTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexFwlDefnRuleTable (tSNMP_OCTET_STRING_TYPE * pFwlRuleRuleName)
{
    int                 rc;
    tFwlwnmhGetFirstIndexFwlDefnRuleTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_FWL_DEFN_RULE_TABLE;
    lv.pFwlRuleRuleName = pFwlRuleRuleName;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexFwlDefnRuleTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexFwlDefnRuleTable (tSNMP_OCTET_STRING_TYPE * pFwlRuleRuleName,
                                 tSNMP_OCTET_STRING_TYPE * pNextFwlRuleRuleName)
{
    int                 rc;
    tFwlwnmhGetNextIndexFwlDefnRuleTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_FWL_DEFN_RULE_TABLE;
    lv.pFwlRuleRuleName = pFwlRuleRuleName;
    lv.pNextFwlRuleRuleName = pNextFwlRuleRuleName;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlRuleFilterSet
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlRuleFilterSet (tSNMP_OCTET_STRING_TYPE * pFwlRuleRuleName,
                        tSNMP_OCTET_STRING_TYPE * pRetValFwlRuleFilterSet)
{
    int                 rc;
    tFwlwnmhGetFwlRuleFilterSet lv;

    lv.cmd = NMH_GET_FWL_RULE_FILTER_SET;
    lv.pFwlRuleRuleName = pFwlRuleRuleName;
    lv.pRetValFwlRuleFilterSet = pRetValFwlRuleFilterSet;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlRuleRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlRuleRowStatus (tSNMP_OCTET_STRING_TYPE * pFwlRuleRuleName,
                        INT4 *pi4RetValFwlRuleRowStatus)
{
    int                 rc;
    tFwlwnmhGetFwlRuleRowStatus lv;

    lv.cmd = NMH_GET_FWL_RULE_ROW_STATUS;
    lv.pFwlRuleRuleName = pFwlRuleRuleName;
    lv.pi4RetValFwlRuleRowStatus = pi4RetValFwlRuleRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlRuleFilterSet
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlRuleFilterSet (tSNMP_OCTET_STRING_TYPE * pFwlRuleRuleName,
                        tSNMP_OCTET_STRING_TYPE * pSetValFwlRuleFilterSet)
{
    int                 rc;
    tFwlwnmhSetFwlRuleFilterSet lv;

    lv.cmd = NMH_SET_FWL_RULE_FILTER_SET;
    lv.pFwlRuleRuleName = pFwlRuleRuleName;
    lv.pSetValFwlRuleFilterSet = pSetValFwlRuleFilterSet;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlRuleRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlRuleRowStatus (tSNMP_OCTET_STRING_TYPE * pFwlRuleRuleName,
                        INT4 i4SetValFwlRuleRowStatus)
{
    int                 rc;
    tFwlwnmhSetFwlRuleRowStatus lv;

    lv.cmd = NMH_SET_FWL_RULE_ROW_STATUS;
    lv.pFwlRuleRuleName = pFwlRuleRuleName;
    lv.i4SetValFwlRuleRowStatus = i4SetValFwlRuleRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlRuleFilterSet
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlRuleFilterSet (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pFwlRuleRuleName,
                           tSNMP_OCTET_STRING_TYPE * pTestValFwlRuleFilterSet)
{
    int                 rc;
    tFwlwnmhTestv2FwlRuleFilterSet lv;

    lv.cmd = NMH_TESTV2_FWL_RULE_FILTER_SET;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFwlRuleRuleName = pFwlRuleRuleName;
    lv.pTestValFwlRuleFilterSet = pTestValFwlRuleFilterSet;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlRuleRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlRuleRowStatus (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pFwlRuleRuleName,
                           INT4 i4TestValFwlRuleRowStatus)
{
    int                 rc;
    tFwlwnmhTestv2FwlRuleRowStatus lv;

    lv.cmd = NMH_TESTV2_FWL_RULE_ROW_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFwlRuleRuleName = pFwlRuleRuleName;
    lv.i4TestValFwlRuleRowStatus = i4TestValFwlRuleRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceFwlDefnAclTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceFwlDefnAclTable (INT4 i4FwlAclIfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFwlAclAclName, INT4 i4FwlAclDirection)
{
    int                 rc;
    tFwlwnmhValidateIndexInstanceFwlDefnAclTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_FWL_DEFN_ACL_TABLE;
    lv.i4FwlAclIfIndex = i4FwlAclIfIndex;
    lv.pFwlAclAclName = pFwlAclAclName;
    lv.i4FwlAclDirection = i4FwlAclDirection;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexFwlDefnAclTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexFwlDefnAclTable (INT4 *pi4FwlAclIfIndex,
                                 tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                                 INT4 *pi4FwlAclDirection)
{
    int                 rc;
    tFwlwnmhGetFirstIndexFwlDefnAclTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_FWL_DEFN_ACL_TABLE;
    lv.pi4FwlAclIfIndex = pi4FwlAclIfIndex;
    lv.pFwlAclAclName = pFwlAclAclName;
    lv.pi4FwlAclDirection = pi4FwlAclDirection;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexFwlDefnAclTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexFwlDefnAclTable (INT4 i4FwlAclIfIndex,
                                INT4 *pi4NextFwlAclIfIndex,
                                tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                                tSNMP_OCTET_STRING_TYPE * pNextFwlAclAclName,
                                INT4 i4FwlAclDirection,
                                INT4 *pi4NextFwlAclDirection)
{
    int                 rc;
    tFwlwnmhGetNextIndexFwlDefnAclTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_FWL_DEFN_ACL_TABLE;
    lv.i4FwlAclIfIndex = i4FwlAclIfIndex;
    lv.pi4NextFwlAclIfIndex = pi4NextFwlAclIfIndex;
    lv.pFwlAclAclName = pFwlAclAclName;
    lv.pNextFwlAclAclName = pNextFwlAclAclName;
    lv.i4FwlAclDirection = i4FwlAclDirection;
    lv.pi4NextFwlAclDirection = pi4NextFwlAclDirection;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlAclAction
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlAclAction (INT4 i4FwlAclIfIndex,
                    tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                    INT4 i4FwlAclDirection, INT4 *pi4RetValFwlAclAction)
{
    int                 rc;
    tFwlwnmhGetFwlAclAction lv;

    lv.cmd = NMH_GET_FWL_ACL_ACTION;
    lv.i4FwlAclIfIndex = i4FwlAclIfIndex;
    lv.pFwlAclAclName = pFwlAclAclName;
    lv.i4FwlAclDirection = i4FwlAclDirection;
    lv.pi4RetValFwlAclAction = pi4RetValFwlAclAction;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlAclSequenceNumber
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlAclSequenceNumber (INT4 i4FwlAclIfIndex,
                            tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                            INT4 i4FwlAclDirection,
                            INT4 *pi4RetValFwlAclSequenceNumber)
{
    int                 rc;
    tFwlwnmhGetFwlAclSequenceNumber lv;

    lv.cmd = NMH_GET_FWL_ACL_SEQUENCE_NUMBER;
    lv.i4FwlAclIfIndex = i4FwlAclIfIndex;
    lv.pFwlAclAclName = pFwlAclAclName;
    lv.i4FwlAclDirection = i4FwlAclDirection;
    lv.pi4RetValFwlAclSequenceNumber = pi4RetValFwlAclSequenceNumber;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlAclAclType
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlAclAclType (INT4 i4FwlAclIfIndex,
                     tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                     INT4 i4FwlAclDirection, INT4 *pi4RetValFwlAclAclType)
{
    int                 rc;
    tFwlwnmhGetFwlAclAclType lv;

    lv.cmd = NMH_GET_FWL_ACL_ACL_TYPE;
    lv.i4FwlAclIfIndex = i4FwlAclIfIndex;
    lv.pFwlAclAclName = pFwlAclAclName;
    lv.i4FwlAclDirection = i4FwlAclDirection;
    lv.pi4RetValFwlAclAclType = pi4RetValFwlAclAclType;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlAclLogTrigger
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlAclLogTrigger (INT4 i4FwlAclIfIndex,
                        tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                        INT4 i4FwlAclDirection, INT4 *pi4RetValFwlAclLogTrigger)
{
    int                 rc;
    tFwlwnmhGetFwlAclLogTrigger lv;

    lv.cmd = NMH_GET_FWL_ACL_LOG_TRIGGER;
    lv.i4FwlAclIfIndex = i4FwlAclIfIndex;
    lv.pFwlAclAclName = pFwlAclAclName;
    lv.i4FwlAclDirection = i4FwlAclDirection;
    lv.pi4RetValFwlAclLogTrigger = pi4RetValFwlAclLogTrigger;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlAclFragAction
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlAclFragAction (INT4 i4FwlAclIfIndex,
                        tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                        INT4 i4FwlAclDirection, INT4 *pi4RetValFwlAclFragAction)
{
    int                 rc;
    tFwlwnmhGetFwlAclFragAction lv;

    lv.cmd = NMH_GET_FWL_ACL_FRAG_ACTION;
    lv.i4FwlAclIfIndex = i4FwlAclIfIndex;
    lv.pFwlAclAclName = pFwlAclAclName;
    lv.i4FwlAclDirection = i4FwlAclDirection;
    lv.pi4RetValFwlAclFragAction = pi4RetValFwlAclFragAction;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlAclRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlAclRowStatus (INT4 i4FwlAclIfIndex,
                       tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                       INT4 i4FwlAclDirection, INT4 *pi4RetValFwlAclRowStatus)
{
    int                 rc;
    tFwlwnmhGetFwlAclRowStatus lv;

    lv.cmd = NMH_GET_FWL_ACL_ROW_STATUS;
    lv.i4FwlAclIfIndex = i4FwlAclIfIndex;
    lv.pFwlAclAclName = pFwlAclAclName;
    lv.i4FwlAclDirection = i4FwlAclDirection;
    lv.pi4RetValFwlAclRowStatus = pi4RetValFwlAclRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlAclAction
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlAclAction (INT4 i4FwlAclIfIndex,
                    tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                    INT4 i4FwlAclDirection, INT4 i4SetValFwlAclAction)
{
    int                 rc;
    tFwlwnmhSetFwlAclAction lv;

    lv.cmd = NMH_SET_FWL_ACL_ACTION;
    lv.i4FwlAclIfIndex = i4FwlAclIfIndex;
    lv.pFwlAclAclName = pFwlAclAclName;
    lv.i4FwlAclDirection = i4FwlAclDirection;
    lv.i4SetValFwlAclAction = i4SetValFwlAclAction;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlAclSequenceNumber
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlAclSequenceNumber (INT4 i4FwlAclIfIndex,
                            tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                            INT4 i4FwlAclDirection,
                            INT4 i4SetValFwlAclSequenceNumber)
{
    int                 rc;
    tFwlwnmhSetFwlAclSequenceNumber lv;

    lv.cmd = NMH_SET_FWL_ACL_SEQUENCE_NUMBER;
    lv.i4FwlAclIfIndex = i4FwlAclIfIndex;
    lv.pFwlAclAclName = pFwlAclAclName;
    lv.i4FwlAclDirection = i4FwlAclDirection;
    lv.i4SetValFwlAclSequenceNumber = i4SetValFwlAclSequenceNumber;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlAclLogTrigger
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlAclLogTrigger (INT4 i4FwlAclIfIndex,
                        tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                        INT4 i4FwlAclDirection, INT4 i4SetValFwlAclLogTrigger)
{
    int                 rc;
    tFwlwnmhSetFwlAclLogTrigger lv;

    lv.cmd = NMH_SET_FWL_ACL_LOG_TRIGGER;
    lv.i4FwlAclIfIndex = i4FwlAclIfIndex;
    lv.pFwlAclAclName = pFwlAclAclName;
    lv.i4FwlAclDirection = i4FwlAclDirection;
    lv.i4SetValFwlAclLogTrigger = i4SetValFwlAclLogTrigger;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlAclFragAction
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlAclFragAction (INT4 i4FwlAclIfIndex,
                        tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                        INT4 i4FwlAclDirection, INT4 i4SetValFwlAclFragAction)
{
    int                 rc;
    tFwlwnmhSetFwlAclFragAction lv;

    lv.cmd = NMH_SET_FWL_ACL_FRAG_ACTION;
    lv.i4FwlAclIfIndex = i4FwlAclIfIndex;
    lv.pFwlAclAclName = pFwlAclAclName;
    lv.i4FwlAclDirection = i4FwlAclDirection;
    lv.i4SetValFwlAclFragAction = i4SetValFwlAclFragAction;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlAclRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlAclRowStatus (INT4 i4FwlAclIfIndex,
                       tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                       INT4 i4FwlAclDirection, INT4 i4SetValFwlAclRowStatus)
{
    int                 rc;
    tFwlwnmhSetFwlAclRowStatus lv;

    lv.cmd = NMH_SET_FWL_ACL_ROW_STATUS;
    lv.i4FwlAclIfIndex = i4FwlAclIfIndex;
    lv.pFwlAclAclName = pFwlAclAclName;
    lv.i4FwlAclDirection = i4FwlAclDirection;
    lv.i4SetValFwlAclRowStatus = i4SetValFwlAclRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlAclAction
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlAclAction (UINT4 *pu4ErrorCode,
                       INT4 i4FwlAclIfIndex,
                       tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                       INT4 i4FwlAclDirection, INT4 i4TestValFwlAclAction)
{
    int                 rc;
    tFwlwnmhTestv2FwlAclAction lv;

    lv.cmd = NMH_TESTV2_FWL_ACL_ACTION;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FwlAclIfIndex = i4FwlAclIfIndex;
    lv.pFwlAclAclName = pFwlAclAclName;
    lv.i4FwlAclDirection = i4FwlAclDirection;
    lv.i4TestValFwlAclAction = i4TestValFwlAclAction;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlAclSequenceNumber
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlAclSequenceNumber (UINT4 *pu4ErrorCode,
                               INT4 i4FwlAclIfIndex,
                               tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                               INT4 i4FwlAclDirection,
                               INT4 i4TestValFwlAclSequenceNumber)
{
    int                 rc;
    tFwlwnmhTestv2FwlAclSequenceNumber lv;

    lv.cmd = NMH_TESTV2_FWL_ACL_SEQUENCE_NUMBER;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FwlAclIfIndex = i4FwlAclIfIndex;
    lv.pFwlAclAclName = pFwlAclAclName;
    lv.i4FwlAclDirection = i4FwlAclDirection;
    lv.i4TestValFwlAclSequenceNumber = i4TestValFwlAclSequenceNumber;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlAclLogTrigger
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlAclLogTrigger (UINT4 *pu4ErrorCode,
                           INT4 i4FwlAclIfIndex,
                           tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                           INT4 i4FwlAclDirection,
                           INT4 i4TestValFwlAclLogTrigger)
{
    int                 rc;
    tFwlwnmhTestv2FwlAclLogTrigger lv;

    lv.cmd = NMH_TESTV2_FWL_ACL_LOG_TRIGGER;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FwlAclIfIndex = i4FwlAclIfIndex;
    lv.pFwlAclAclName = pFwlAclAclName;
    lv.i4FwlAclDirection = i4FwlAclDirection;
    lv.i4TestValFwlAclLogTrigger = i4TestValFwlAclLogTrigger;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlAclFragAction
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlAclFragAction (UINT4 *pu4ErrorCode,
                           INT4 i4FwlAclIfIndex,
                           tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                           INT4 i4FwlAclDirection,
                           INT4 i4TestValFwlAclFragAction)
{
    int                 rc;
    tFwlwnmhTestv2FwlAclFragAction lv;

    lv.cmd = NMH_TESTV2_FWL_ACL_FRAG_ACTION;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FwlAclIfIndex = i4FwlAclIfIndex;
    lv.pFwlAclAclName = pFwlAclAclName;
    lv.i4FwlAclDirection = i4FwlAclDirection;
    lv.i4TestValFwlAclFragAction = i4TestValFwlAclFragAction;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlAclRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlAclRowStatus (UINT4 *pu4ErrorCode,
                          INT4 i4FwlAclIfIndex,
                          tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                          INT4 i4FwlAclDirection, INT4 i4TestValFwlAclRowStatus)
{
    int                 rc;
    tFwlwnmhTestv2FwlAclRowStatus lv;

    lv.cmd = NMH_TESTV2_FWL_ACL_ROW_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FwlAclIfIndex = i4FwlAclIfIndex;
    lv.pFwlAclAclName = pFwlAclAclName;
    lv.i4FwlAclDirection = i4FwlAclDirection;
    lv.i4TestValFwlAclRowStatus = i4TestValFwlAclRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceFwlDefnIfTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceFwlDefnIfTable (INT4 i4FwlIfIfIndex)
{
    int                 rc;
    tFwlwnmhValidateIndexInstanceFwlDefnIfTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_FWL_DEFN_IF_TABLE;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexFwlDefnIfTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexFwlDefnIfTable (INT4 *pi4FwlIfIfIndex)
{
    int                 rc;
    tFwlwnmhGetFirstIndexFwlDefnIfTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_FWL_DEFN_IF_TABLE;
    lv.pi4FwlIfIfIndex = pi4FwlIfIfIndex;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexFwlDefnIfTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexFwlDefnIfTable (INT4 i4FwlIfIfIndex, INT4 *pi4NextFwlIfIfIndex)
{
    int                 rc;
    tFwlwnmhGetNextIndexFwlDefnIfTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_FWL_DEFN_IF_TABLE;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.pi4NextFwlIfIfIndex = pi4NextFwlIfIfIndex;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlIfIfType
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlIfIfType (INT4 i4FwlIfIfIndex, INT4 *pi4RetValFwlIfIfType)
{
    int                 rc;
    tFwlwnmhGetFwlIfIfType lv;

    lv.cmd = NMH_GET_FWL_IF_IF_TYPE;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.pi4RetValFwlIfIfType = pi4RetValFwlIfIfType;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlIfIpOptions
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlIfIpOptions (INT4 i4FwlIfIfIndex, INT4 *pi4RetValFwlIfIpOptions)
{
    int                 rc;
    tFwlwnmhGetFwlIfIpOptions lv;

    lv.cmd = NMH_GET_FWL_IF_IP_OPTIONS;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.pi4RetValFwlIfIpOptions = pi4RetValFwlIfIpOptions;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlIfFragments
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlIfFragments (INT4 i4FwlIfIfIndex, INT4 *pi4RetValFwlIfFragments)
{
    int                 rc;
    tFwlwnmhGetFwlIfFragments lv;

    lv.cmd = NMH_GET_FWL_IF_FRAGMENTS;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.pi4RetValFwlIfFragments = pi4RetValFwlIfFragments;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlIfFragments
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlIfFragmentSize (INT4 i4FwlIfIfIndex, UINT4 *pu4RetValFwlIfFragmentSize)
{
    int                 rc;
    tFwlwnmhGetFwlIfFragmentSize lv;

    lv.cmd = NMH_GET_FWL_IF_FRAGMENT_SIZE;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.pu4RetValFwlIfFragmentSize = pu4RetValFwlIfFragmentSize;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlIfICMPType
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlIfICMPType (INT4 i4FwlIfIfIndex, INT4 *pi4RetValFwlIfICMPType)
{
    int                 rc;
    tFwlwnmhGetFwlIfICMPType lv;

    lv.cmd = NMH_GET_FWL_IF_I_C_M_P_TYPE;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.pi4RetValFwlIfICMPType = pi4RetValFwlIfICMPType;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlIfICMPCode
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlIfICMPCode (INT4 i4FwlIfIfIndex, INT4 *pi4RetValFwlIfICMPCode)
{
    int                 rc;
    tFwlwnmhGetFwlIfICMPCode lv;

    lv.cmd = NMH_GET_FWL_IF_I_C_M_P_CODE;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.pi4RetValFwlIfICMPCode = pi4RetValFwlIfICMPCode;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlIfRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlIfRowStatus (INT4 i4FwlIfIfIndex, INT4 *pi4RetValFwlIfRowStatus)
{
    int                 rc;
    tFwlwnmhGetFwlIfRowStatus lv;

    lv.cmd = NMH_GET_FWL_IF_ROW_STATUS;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.pi4RetValFwlIfRowStatus = pi4RetValFwlIfRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlIfIfType
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlIfIfType (INT4 i4FwlIfIfIndex, INT4 i4SetValFwlIfIfType)
{
    int                 rc;
    tFwlwnmhSetFwlIfIfType lv;

    lv.cmd = NMH_SET_FWL_IF_IF_TYPE;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.i4SetValFwlIfIfType = i4SetValFwlIfIfType;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlIfIpOptions
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlIfIpOptions (INT4 i4FwlIfIfIndex, INT4 i4SetValFwlIfIpOptions)
{
    int                 rc;
    tFwlwnmhSetFwlIfIpOptions lv;

    lv.cmd = NMH_SET_FWL_IF_IP_OPTIONS;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.i4SetValFwlIfIpOptions = i4SetValFwlIfIpOptions;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlIfFragments
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlIfFragments (INT4 i4FwlIfIfIndex, INT4 i4SetValFwlIfFragments)
{
    int                 rc;
    tFwlwnmhSetFwlIfFragments lv;

    lv.cmd = NMH_SET_FWL_IF_FRAGMENTS;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.i4SetValFwlIfFragments = i4SetValFwlIfFragments;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlIfICMPType
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlIfICMPType (INT4 i4FwlIfIfIndex, INT4 i4SetValFwlIfICMPType)
{
    int                 rc;
    tFwlwnmhSetFwlIfICMPType lv;

    lv.cmd = NMH_SET_FWL_IF_I_C_M_P_TYPE;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.i4SetValFwlIfICMPType = i4SetValFwlIfICMPType;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlIfICMPCode
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlIfICMPCode (INT4 i4FwlIfIfIndex, INT4 i4SetValFwlIfICMPCode)
{
    int                 rc;
    tFwlwnmhSetFwlIfICMPCode lv;

    lv.cmd = NMH_SET_FWL_IF_I_C_M_P_CODE;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.i4SetValFwlIfICMPCode = i4SetValFwlIfICMPCode;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlIfRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlIfRowStatus (INT4 i4FwlIfIfIndex, INT4 i4SetValFwlIfRowStatus)
{
    int                 rc;
    tFwlwnmhSetFwlIfRowStatus lv;

    lv.cmd = NMH_SET_FWL_IF_ROW_STATUS;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.i4SetValFwlIfRowStatus = i4SetValFwlIfRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlIfIfType
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlIfIfType (UINT4 *pu4ErrorCode,
                      INT4 i4FwlIfIfIndex, INT4 i4TestValFwlIfIfType)
{
    int                 rc;
    tFwlwnmhTestv2FwlIfIfType lv;

    lv.cmd = NMH_TESTV2_FWL_IF_IF_TYPE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.i4TestValFwlIfIfType = i4TestValFwlIfIfType;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlIfIpOptions
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlIfIpOptions (UINT4 *pu4ErrorCode,
                         INT4 i4FwlIfIfIndex, INT4 i4TestValFwlIfIpOptions)
{
    int                 rc;
    tFwlwnmhTestv2FwlIfIpOptions lv;

    lv.cmd = NMH_TESTV2_FWL_IF_IP_OPTIONS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.i4TestValFwlIfIpOptions = i4TestValFwlIfIpOptions;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlIfFragments
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlIfFragments (UINT4 *pu4ErrorCode,
                         INT4 i4FwlIfIfIndex, INT4 i4TestValFwlIfFragments)
{
    int                 rc;
    tFwlwnmhTestv2FwlIfFragments lv;

    lv.cmd = NMH_TESTV2_FWL_IF_FRAGMENTS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.i4TestValFwlIfFragments = i4TestValFwlIfFragments;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlIfICMPType
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlIfICMPType (UINT4 *pu4ErrorCode,
                        INT4 i4FwlIfIfIndex, INT4 i4TestValFwlIfICMPType)
{
    int                 rc;
    tFwlwnmhTestv2FwlIfICMPType lv;

    lv.cmd = NMH_TESTV2_FWL_IF_I_C_M_P_TYPE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.i4TestValFwlIfICMPType = i4TestValFwlIfICMPType;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlIfICMPCode
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlIfICMPCode (UINT4 *pu4ErrorCode,
                        INT4 i4FwlIfIfIndex, INT4 i4TestValFwlIfICMPCode)
{
    int                 rc;
    tFwlwnmhTestv2FwlIfICMPCode lv;

    lv.cmd = NMH_TESTV2_FWL_IF_I_C_M_P_CODE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.i4TestValFwlIfICMPCode = i4TestValFwlIfICMPCode;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlIfRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlIfRowStatus (UINT4 *pu4ErrorCode,
                         INT4 i4FwlIfIfIndex, INT4 i4TestValFwlIfRowStatus)
{
    int                 rc;
    tFwlwnmhTestv2FwlIfRowStatus lv;

    lv.cmd = NMH_TESTV2_FWL_IF_ROW_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.i4TestValFwlIfRowStatus = i4TestValFwlIfRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceFwlDefnDmzTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceFwlDefnDmzTable (UINT4 u4FwlDmzIpIndex)
{
    int                 rc;
    tFwlwnmhValidateIndexInstanceFwlDefnDmzTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_FWL_DEFN_DMZ_TABLE;
    lv.u4FwlDmzIpIndex = u4FwlDmzIpIndex;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexFwlDefnDmzTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexFwlDefnDmzTable (UINT4 *pu4FwlDmzIpIndex)
{
    int                 rc;
    tFwlwnmhGetFirstIndexFwlDefnDmzTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_FWL_DEFN_DMZ_TABLE;
    lv.pu4FwlDmzIpIndex = pu4FwlDmzIpIndex;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexFwlDefnDmzTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexFwlDefnDmzTable (UINT4 u4FwlDmzIpIndex,
                                UINT4 *pu4NextFwlDmzIpIndex)
{
    int                 rc;
    tFwlwnmhGetNextIndexFwlDefnDmzTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_FWL_DEFN_DMZ_TABLE;
    lv.u4FwlDmzIpIndex = u4FwlDmzIpIndex;
    lv.pu4NextFwlDmzIpIndex = pu4NextFwlDmzIpIndex;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlDmzRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlDmzRowStatus (UINT4 u4FwlDmzIpIndex, INT4 *pi4RetValFwlDmzRowStatus)
{
    int                 rc;
    tFwlwnmhGetFwlDmzRowStatus lv;

    lv.cmd = NMH_GET_FWL_DMZ_ROW_STATUS;
    lv.u4FwlDmzIpIndex = u4FwlDmzIpIndex;
    lv.pi4RetValFwlDmzRowStatus = pi4RetValFwlDmzRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlDmzRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlDmzRowStatus (UINT4 u4FwlDmzIpIndex, INT4 i4SetValFwlDmzRowStatus)
{
    int                 rc;
    tFwlwnmhSetFwlDmzRowStatus lv;

    lv.cmd = NMH_SET_FWL_DMZ_ROW_STATUS;
    lv.u4FwlDmzIpIndex = u4FwlDmzIpIndex;
    lv.i4SetValFwlDmzRowStatus = i4SetValFwlDmzRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlDmzRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlDmzRowStatus (UINT4 *pu4ErrorCode,
                          UINT4 u4FwlDmzIpIndex, INT4 i4TestValFwlDmzRowStatus)
{
    int                 rc;
    tFwlwnmhTestv2FwlDmzRowStatus lv;

    lv.cmd = NMH_TESTV2_FWL_DMZ_ROW_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.u4FwlDmzIpIndex = u4FwlDmzIpIndex;
    lv.i4TestValFwlDmzRowStatus = i4TestValFwlDmzRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceFwlUrlFilterTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceFwlUrlFilterTable (tSNMP_OCTET_STRING_TYPE *
                                           pFwlUrlString)
{
    int                 rc;
    tFwlwnmhValidateIndexInstanceFwlUrlFilterTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_FWL_URL_FILTER_TABLE;
    lv.pFwlUrlString = pFwlUrlString;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexFwlUrlFilterTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexFwlUrlFilterTable (tSNMP_OCTET_STRING_TYPE * pFwlUrlString)
{
    int                 rc;
    tFwlwnmhGetFirstIndexFwlUrlFilterTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_FWL_URL_FILTER_TABLE;
    lv.pFwlUrlString = pFwlUrlString;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexFwlUrlFilterTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexFwlUrlFilterTable (tSNMP_OCTET_STRING_TYPE * pFwlUrlString,
                                  tSNMP_OCTET_STRING_TYPE * pNextFwlUrlString)
{
    int                 rc;
    tFwlwnmhGetNextIndexFwlUrlFilterTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_FWL_URL_FILTER_TABLE;
    lv.pFwlUrlString = pFwlUrlString;
    lv.pNextFwlUrlString = pNextFwlUrlString;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlUrlHitCount
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlUrlHitCount (tSNMP_OCTET_STRING_TYPE * pFwlUrlString,
                      UINT4 *pu4RetValFwlUrlHitCount)
{
    int                 rc;
    tFwlwnmhGetFwlUrlHitCount lv;

    lv.cmd = NMH_GET_FWL_URL_HIT_COUNT;
    lv.pFwlUrlString = pFwlUrlString;
    lv.pu4RetValFwlUrlHitCount = pu4RetValFwlUrlHitCount;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlUrlFilterRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlUrlFilterRowStatus (tSNMP_OCTET_STRING_TYPE * pFwlUrlString,
                             INT4 *pi4RetValFwlUrlFilterRowStatus)
{
    int                 rc;
    tFwlwnmhGetFwlUrlFilterRowStatus lv;

    lv.cmd = NMH_GET_FWL_URL_FILTER_ROW_STATUS;
    lv.pFwlUrlString = pFwlUrlString;
    lv.pi4RetValFwlUrlFilterRowStatus = pi4RetValFwlUrlFilterRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlUrlFilterRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlUrlFilterRowStatus (tSNMP_OCTET_STRING_TYPE * pFwlUrlString,
                             INT4 i4SetValFwlUrlFilterRowStatus)
{
    int                 rc;
    tFwlwnmhSetFwlUrlFilterRowStatus lv;

    lv.cmd = NMH_SET_FWL_URL_FILTER_ROW_STATUS;
    lv.pFwlUrlString = pFwlUrlString;
    lv.i4SetValFwlUrlFilterRowStatus = i4SetValFwlUrlFilterRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlUrlFilterRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlUrlFilterRowStatus (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFwlUrlString,
                                INT4 i4TestValFwlUrlFilterRowStatus)
{
    int                 rc;
    tFwlwnmhTestv2FwlUrlFilterRowStatus lv;

    lv.cmd = NMH_TESTV2_FWL_URL_FILTER_ROW_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFwlUrlString = pFwlUrlString;
    lv.i4TestValFwlUrlFilterRowStatus = i4TestValFwlUrlFilterRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatInspectedPacketsCount
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatInspectedPacketsCount (UINT4
                                    *pu4RetValFwlStatInspectedPacketsCount)
{
    int                 rc;
    tIdsGetInfo         IdsGetInfor;
    tFwlwnmhGetFwlStatInspectedPacketsCount lv;

    lv.cmd = NMH_GET_FWL_STAT_INSPECTED_PACKETS_COUNT;
    lv.pu4RetValFwlStatInspectedPacketsCount =
        pu4RetValFwlStatInspectedPacketsCount;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    if (rc >= 0)
    {
        MEMSET (&IdsGetInfor, 0, sizeof (tIdsGetInfo));

        /* The number of packets inspected is fetched from IDS */
        SecIdsGetInfo (ISS_IDS_GLB_STATS_REQ, &IdsGetInfor);

        *pu4RetValFwlStatInspectedPacketsCount += IdsGetInfor.u4AllowStats
            + IdsGetInfor.u4DropStats;
    }

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatTotalPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatTotalPacketsDenied (UINT4 *pu4RetValFwlStatTotalPacketsDenied)
{
    int                 rc;
    tIdsGetInfo         IdsGetInfor;
    tFwlwnmhGetFwlStatTotalPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_TOTAL_PACKETS_DENIED;
    lv.pu4RetValFwlStatTotalPacketsDenied = pu4RetValFwlStatTotalPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    if (rc >= 0)
    {
        MEMSET (&IdsGetInfor, 0, sizeof (tIdsGetInfo));

        /* The number of packets dropped is fetched from the IDS */
        SecIdsGetInfo (ISS_IDS_GLB_STATS_REQ, &IdsGetInfor);

        *pu4RetValFwlStatTotalPacketsDenied += IdsGetInfor.u4DropStats;
    }

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatTotalPacketsAccepted
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatTotalPacketsAccepted (UINT4 *pu4RetValFwlStatTotalPacketsAccepted)
{
    int                 rc;
    tIdsGetInfo         IdsGetInfor;
    tFwlwnmhGetFwlStatTotalPacketsAccepted lv;

    lv.cmd = NMH_GET_FWL_STAT_TOTAL_PACKETS_ACCEPTED;
    lv.pu4RetValFwlStatTotalPacketsAccepted =
        pu4RetValFwlStatTotalPacketsAccepted;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    if (rc >= 0)
    {
        MEMSET (&IdsGetInfor, 0, sizeof (tIdsGetInfo));

        /*The number of packets accepted is fetched from the IDS */
        SecIdsGetInfo (ISS_IDS_GLB_STATS_REQ, &IdsGetInfor);

        *pu4RetValFwlStatTotalPacketsAccepted += IdsGetInfor.u4AllowStats;
    }

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatTotalIcmpPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatTotalIcmpPacketsDenied (UINT4
                                     *pu4RetValFwlStatTotalIcmpPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatTotalIcmpPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_TOTAL_ICMP_PACKETS_DENIED;
    lv.pu4RetValFwlStatTotalIcmpPacketsDenied =
        pu4RetValFwlStatTotalIcmpPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatTotalSynPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatTotalSynPacketsDenied (UINT4
                                    *pu4RetValFwlStatTotalSynPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatTotalSynPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_TOTAL_SYN_PACKETS_DENIED;
    lv.pu4RetValFwlStatTotalSynPacketsDenied =
        pu4RetValFwlStatTotalSynPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatTotalIpSpoofedPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatTotalIpSpoofedPacketsDenied (UINT4
                                          *pu4RetValFwlStatTotalIpSpoofedPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatTotalIpSpoofedPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_TOTAL_IP_SPOOFED_PACKETS_DENIED;
    lv.pu4RetValFwlStatTotalIpSpoofedPacketsDenied =
        pu4RetValFwlStatTotalIpSpoofedPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatTotalSrcRoutePacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatTotalSrcRoutePacketsDenied (UINT4
                                         *pu4RetValFwlStatTotalSrcRoutePacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatTotalSrcRoutePacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_TOTAL_SRC_ROUTE_PACKETS_DENIED;
    lv.pu4RetValFwlStatTotalSrcRoutePacketsDenied =
        pu4RetValFwlStatTotalSrcRoutePacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatTotalTinyFragmentPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatTotalTinyFragmentPacketsDenied (UINT4
                                             *pu4RetValFwlStatTotalTinyFragmentPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatTotalTinyFragmentPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_TOTAL_TINY_FRAGMENT_PACKETS_DENIED;
    lv.pu4RetValFwlStatTotalTinyFragmentPacketsDenied =
        pu4RetValFwlStatTotalTinyFragmentPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatTotalFragmentedPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatTotalFragmentedPacketsDenied (UINT4
                                           *pu4RetValFwlStatTotalFragmentedPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatTotalFragmentedPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_TOTAL_FRAGMENTED_PACKETS_DENIED;
    lv.pu4RetValFwlStatTotalFragmentedPacketsDenied =
        pu4RetValFwlStatTotalFragmentedPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatTotalIpOptionPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatTotalIpOptionPacketsDenied (UINT4
                                         *pu4RetValFwlStatTotalIpOptionPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatTotalIpOptionPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_TOTAL_IP_OPTION_PACKETS_DENIED;
    lv.pu4RetValFwlStatTotalIpOptionPacketsDenied =
        pu4RetValFwlStatTotalIpOptionPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatMemoryAllocationFailCount
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatMemoryAllocationFailCount (UINT4
                                        *pu4RetValFwlStatMemoryAllocationFailCount)
{
    int                 rc;
    tFwlwnmhGetFwlStatMemoryAllocationFailCount lv;

    lv.cmd = NMH_GET_FWL_STAT_MEMORY_ALLOCATION_FAIL_COUNT;
    lv.pu4RetValFwlStatMemoryAllocationFailCount =
        pu4RetValFwlStatMemoryAllocationFailCount;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatClear
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatClear (INT4 *pi4RetValFwlStatClear)
{
    int                 rc;
    tFwlwnmhGetFwlStatClear lv;

    lv.cmd = NMH_GET_FWL_STAT_CLEAR;
    lv.pi4RetValFwlStatClear = pi4RetValFwlStatClear;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlTrapThreshold
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlTrapThreshold (INT4 *pi4RetValFwlTrapThreshold)
{
    int                 rc;
    tFwlwnmhGetFwlTrapThreshold lv;

    lv.cmd = NMH_GET_FWL_TRAP_THRESHOLD;
    lv.pi4RetValFwlTrapThreshold = pi4RetValFwlTrapThreshold;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlStatClear
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlStatClear (INT4 i4SetValFwlStatClear)
{
    int                 rc;
    tFwlwnmhSetFwlStatClear lv;

    lv.cmd = NMH_SET_FWL_STAT_CLEAR;
    lv.i4SetValFwlStatClear = i4SetValFwlStatClear;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlTrapThreshold
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlTrapThreshold (INT4 i4SetValFwlTrapThreshold)
{
    int                 rc;
    INT4                i4RetValFwlGlobalTrap;
    tFwlwnmhSetFwlTrapThreshold lv;

    lv.cmd = NMH_SET_FWL_TRAP_THRESHOLD;
    lv.i4SetValFwlTrapThreshold = i4SetValFwlTrapThreshold;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    if (rc >= 0)
    {
        /* The configured trap threshold value is sent to ids only
         *        the trap is enabled globally . */
        nmhGetFwlGlobalTrap (&i4RetValFwlGlobalTrap);
        if (i4RetValFwlGlobalTrap == FWL_ENABLE)
        {
            SecIdsSetInfo (ISS_IDS_SET_PKT_DROP_THRESH,
                           gFwlAclInfo.i4TrapThreshold);
        }
    }

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlStatClear
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlStatClear (UINT4 *pu4ErrorCode, INT4 i4TestValFwlStatClear)
{
    int                 rc;
    tFwlwnmhTestv2FwlStatClear lv;

    lv.cmd = NMH_TESTV2_FWL_STAT_CLEAR;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFwlStatClear = i4TestValFwlStatClear;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlTrapThreshold
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlTrapThreshold (UINT4 *pu4ErrorCode, INT4 i4TestValFwlTrapThreshold)
{
    int                 rc;
    tFwlwnmhTestv2FwlTrapThreshold lv;

    lv.cmd = NMH_TESTV2_FWL_TRAP_THRESHOLD;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFwlTrapThreshold = i4TestValFwlTrapThreshold;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceFwlStatIfTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceFwlStatIfTable (INT4 i4FwlStatIfIfIndex)
{
    int                 rc;
    tFwlwnmhValidateIndexInstanceFwlStatIfTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_FWL_STAT_IF_TABLE;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexFwlStatIfTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexFwlStatIfTable (INT4 *pi4FwlStatIfIfIndex)
{
    int                 rc;
    tFwlwnmhGetFirstIndexFwlStatIfTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_FWL_STAT_IF_TABLE;
    lv.pi4FwlStatIfIfIndex = pi4FwlStatIfIfIndex;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexFwlStatIfTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexFwlStatIfTable (INT4 i4FwlStatIfIfIndex,
                               INT4 *pi4NextFwlStatIfIfIndex)
{
    int                 rc;
    tFwlwnmhGetNextIndexFwlStatIfTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_FWL_STAT_IF_TABLE;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.pi4NextFwlStatIfIfIndex = pi4NextFwlStatIfIfIndex;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIfFilterCount
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIfFilterCount (INT4 i4FwlStatIfIfIndex,
                            INT4 *pi4RetValFwlStatIfFilterCount)
{
    int                 rc;
    tFwlwnmhGetFwlStatIfFilterCount lv;

    lv.cmd = NMH_GET_FWL_STAT_IF_FILTER_COUNT;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.pi4RetValFwlStatIfFilterCount = pi4RetValFwlStatIfFilterCount;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIfPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIfPacketsDenied (INT4 i4FwlStatIfIfIndex,
                              UINT4 *pu4RetValFwlStatIfPacketsDenied)
{
    int                 rc;
    tIdsGetInfo         IdsGetInfor;
    tFwlwnmhGetFwlStatIfPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_IF_PACKETS_DENIED;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.pu4RetValFwlStatIfPacketsDenied = pu4RetValFwlStatIfPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    if (rc >= 0)
    {
        MEMSET (&IdsGetInfor, 0, sizeof (tIdsGetInfo));

        IdsGetInfor.u4IfIndex = i4FwlStatIfIfIndex;
        SecIdsGetInfo (ISS_IDS_INTF_STATS_REQ, &IdsGetInfor);
        *pu4RetValFwlStatIfPacketsDenied += IdsGetInfor.u4DropStats;
    }

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIfPacketsAccepted
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIfPacketsAccepted (INT4 i4FwlStatIfIfIndex,
                                UINT4 *pu4RetValFwlStatIfPacketsAccepted)
{
    int                 rc;
    tIdsGetInfo         IdsGetInfor;
    tFwlwnmhGetFwlStatIfPacketsAccepted lv;

    lv.cmd = NMH_GET_FWL_STAT_IF_PACKETS_ACCEPTED;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.pu4RetValFwlStatIfPacketsAccepted = pu4RetValFwlStatIfPacketsAccepted;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    if (rc >= 0)
    {
        MEMSET (&IdsGetInfor, 0, sizeof (tIdsGetInfo));

        IdsGetInfor.u4IfIndex = i4FwlStatIfIfIndex;
        SecIdsGetInfo (ISS_IDS_INTF_STATS_REQ, &IdsGetInfor);
        *pu4RetValFwlStatIfPacketsAccepted += IdsGetInfor.u4AllowStats;
    }

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIfSynPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIfSynPacketsDenied (INT4 i4FwlStatIfIfIndex,
                                 UINT4 *pu4RetValFwlStatIfSynPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatIfSynPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_IF_SYN_PACKETS_DENIED;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.pu4RetValFwlStatIfSynPacketsDenied = pu4RetValFwlStatIfSynPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIfIcmpPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIfIcmpPacketsDenied (INT4 i4FwlStatIfIfIndex,
                                  UINT4 *pu4RetValFwlStatIfIcmpPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatIfIcmpPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_IF_ICMP_PACKETS_DENIED;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.pu4RetValFwlStatIfIcmpPacketsDenied =
        pu4RetValFwlStatIfIcmpPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIfIpSpoofedPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIfIpSpoofedPacketsDenied (INT4 i4FwlStatIfIfIndex,
                                       UINT4
                                       *pu4RetValFwlStatIfIpSpoofedPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatIfIpSpoofedPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_IF_IP_SPOOFED_PACKETS_DENIED;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.pu4RetValFwlStatIfIpSpoofedPacketsDenied =
        pu4RetValFwlStatIfIpSpoofedPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIfSrcRoutePacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIfSrcRoutePacketsDenied (INT4 i4FwlStatIfIfIndex,
                                      UINT4
                                      *pu4RetValFwlStatIfSrcRoutePacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatIfSrcRoutePacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_IF_SRC_ROUTE_PACKETS_DENIED;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.pu4RetValFwlStatIfSrcRoutePacketsDenied =
        pu4RetValFwlStatIfSrcRoutePacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIfTinyFragmentPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIfTinyFragmentPacketsDenied (INT4 i4FwlStatIfIfIndex,
                                          UINT4
                                          *pu4RetValFwlStatIfTinyFragmentPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatIfTinyFragmentPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_IF_TINY_FRAGMENT_PACKETS_DENIED;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.pu4RetValFwlStatIfTinyFragmentPacketsDenied =
        pu4RetValFwlStatIfTinyFragmentPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIfFragmentPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIfFragmentPacketsDenied (INT4 i4FwlStatIfIfIndex,
                                      UINT4
                                      *pu4RetValFwlStatIfFragmentPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatIfFragmentPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_IF_FRAGMENT_PACKETS_DENIED;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.pu4RetValFwlStatIfFragmentPacketsDenied =
        pu4RetValFwlStatIfFragmentPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIfIpOptionPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIfIpOptionPacketsDenied (INT4 i4FwlStatIfIfIndex,
                                      UINT4
                                      *pu4RetValFwlStatIfIpOptionPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatIfIpOptionPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_IF_IP_OPTION_PACKETS_DENIED;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.pu4RetValFwlStatIfIpOptionPacketsDenied =
        pu4RetValFwlStatIfIpOptionPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIfClear
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIfClear (INT4 i4FwlStatIfIfIndex, INT4 *pi4RetValFwlStatIfClear)
{
    int                 rc;
    tFwlwnmhGetFwlStatIfClear lv;

    lv.cmd = NMH_GET_FWL_STAT_IF_CLEAR;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.pi4RetValFwlStatIfClear = pi4RetValFwlStatIfClear;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlIfTrapThreshold
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlIfTrapThreshold (INT4 i4FwlStatIfIfIndex,
                          INT4 *pi4RetValFwlIfTrapThreshold)
{
    int                 rc;
    tFwlwnmhGetFwlIfTrapThreshold lv;

    lv.cmd = NMH_GET_FWL_IF_TRAP_THRESHOLD;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.pi4RetValFwlIfTrapThreshold = pi4RetValFwlIfTrapThreshold;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlStatIfClear
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlStatIfClear (INT4 i4FwlStatIfIfIndex, INT4 i4SetValFwlStatIfClear)
{
    int                 rc;
    tFwlwnmhSetFwlStatIfClear lv;

    lv.cmd = NMH_SET_FWL_STAT_IF_CLEAR;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.i4SetValFwlStatIfClear = i4SetValFwlStatIfClear;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlIfTrapThreshold
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlIfTrapThreshold (INT4 i4FwlStatIfIfIndex,
                          INT4 i4SetValFwlIfTrapThreshold)
{
    int                 rc;
    tFwlwnmhSetFwlIfTrapThreshold lv;

    lv.cmd = NMH_SET_FWL_IF_TRAP_THRESHOLD;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.i4SetValFwlIfTrapThreshold = i4SetValFwlIfTrapThreshold;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlStatIfClear
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlStatIfClear (UINT4 *pu4ErrorCode,
                         INT4 i4FwlStatIfIfIndex, INT4 i4TestValFwlStatIfClear)
{
    int                 rc;
    tFwlwnmhTestv2FwlStatIfClear lv;

    lv.cmd = NMH_TESTV2_FWL_STAT_IF_CLEAR;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.i4TestValFwlStatIfClear = i4TestValFwlStatIfClear;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlIfTrapThreshold
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlIfTrapThreshold (UINT4 *pu4ErrorCode,
                             INT4 i4FwlStatIfIfIndex,
                             INT4 i4TestValFwlIfTrapThreshold)
{
    int                 rc;
    tFwlwnmhTestv2FwlIfTrapThreshold lv;

    lv.cmd = NMH_TESTV2_FWL_IF_TRAP_THRESHOLD;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.i4TestValFwlIfTrapThreshold = i4TestValFwlIfTrapThreshold;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlTrapMemFailMessage
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlTrapMemFailMessage (tSNMP_OCTET_STRING_TYPE *
                             pRetValFwlTrapMemFailMessage)
{
    int                 rc;
    tFwlwnmhGetFwlTrapMemFailMessage lv;

    lv.cmd = NMH_GET_FWL_TRAP_MEM_FAIL_MESSAGE;
    lv.pRetValFwlTrapMemFailMessage = pRetValFwlTrapMemFailMessage;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlTrapAttackMessage
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlTrapAttackMessage (tSNMP_OCTET_STRING_TYPE *
                            pRetValFwlTrapAttackMessage)
{
    int                 rc;
    tFwlwnmhGetFwlTrapAttackMessage lv;

    lv.cmd = NMH_GET_FWL_TRAP_ATTACK_MESSAGE;
    lv.pRetValFwlTrapAttackMessage = pRetValFwlTrapAttackMessage;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlTrapMemFailMessage
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlTrapMemFailMessage (tSNMP_OCTET_STRING_TYPE *
                             pSetValFwlTrapMemFailMessage)
{
    int                 rc;
    tFwlwnmhSetFwlTrapMemFailMessage lv;

    lv.cmd = NMH_SET_FWL_TRAP_MEM_FAIL_MESSAGE;
    lv.pSetValFwlTrapMemFailMessage = pSetValFwlTrapMemFailMessage;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlTrapAttackMessage
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlTrapAttackMessage (tSNMP_OCTET_STRING_TYPE *
                            pSetValFwlTrapAttackMessage)
{
    int                 rc;
    tFwlwnmhSetFwlTrapAttackMessage lv;

    lv.cmd = NMH_SET_FWL_TRAP_ATTACK_MESSAGE;
    lv.pSetValFwlTrapAttackMessage = pSetValFwlTrapAttackMessage;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlTrapMemFailMessage
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlTrapMemFailMessage (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFwlTrapMemFailMessage)
{
    int                 rc;
    tFwlwnmhTestv2FwlTrapMemFailMessage lv;

    lv.cmd = NMH_TESTV2_FWL_TRAP_MEM_FAIL_MESSAGE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pTestValFwlTrapMemFailMessage = pTestValFwlTrapMemFailMessage;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlTrapAttackMessage
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlTrapAttackMessage (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFwlTrapAttackMessage)
{
    int                 rc;
    tFwlwnmhTestv2FwlTrapAttackMessage lv;

    lv.cmd = NMH_TESTV2_FWL_TRAP_ATTACK_MESSAGE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pTestValFwlTrapAttackMessage = pTestValFwlTrapAttackMessage;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlLogTrigger
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlLogTrigger (UINT4 u4Interface,
                     UINT1 u1LogTrigger, UINT1 *pu1AclName, UINT4 u4Direction)
{
    int                 rc;
    tFwlwnmhSetFwlLogTrigger lv;

    lv.cmd = NMH_SET_FWL_LOG_TRIGGER;
    lv.u4Interface = u4Interface;
    lv.u1LogTrigger = u1LogTrigger;
    lv.pu1AclName = pu1AclName;
    lv.u4Direction = u4Direction;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlFragAction
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlFragAction (UINT4 u4Interface,
                     UINT1 u1FragAction, UINT1 *pu1AclName, UINT4 u4Direction)
{
    int                 rc;
    tFwlwnmhSetFwlFragAction lv;

    lv.cmd = NMH_SET_FWL_FRAG_ACTION;
    lv.u4Interface = u4Interface;
    lv.u1FragAction = u1FragAction;
    lv.pu1AclName = pu1AclName;
    lv.u4Direction = u4Direction;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlIfFragmentSize
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlIfFragmentSize (INT4 i4FwlIfIfIndex, UINT4 u4SetValFwlIfFragmentSize)
{
    int                 rc;
    tFwlwnmhSetFwlIfFragmentSize lv;

    lv.cmd = NMH_SET_FWL_IF_FRAGMENT_SIZE;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.u4SetValFwlIfFragmentSize = u4SetValFwlIfFragmentSize;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalNetBiosFiltering
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalNetBiosFiltering (INT4 i4SetValFwlGlobalNetBiosFiltering)
{
    int                 rc;
    tFwlwnmhSetFwlGlobalNetBiosFiltering lv;

    lv.cmd = NMH_SET_FWL_GLOBAL_NET_BIOS_FILTERING;
    lv.i4SetValFwlGlobalNetBiosFiltering = i4SetValFwlGlobalNetBiosFiltering;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlIfFragmentSize
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlIfFragmentSize (UINT4 *pu4ErrorCode,
                            INT4 i4FwlIfIfIndex,
                            UINT4 u4TestValFwlIfFragmentSize)
{
    int                 rc;
    tFwlwnmhTestv2FwlIfFragmentSize lv;

    lv.cmd = NMH_TESTV2_FWL_IF_FRAGMENT_SIZE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.u4TestValFwlIfFragmentSize = u4TestValFwlIfFragmentSize;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatTotalLargeFragmentPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatTotalLargeFragmentPacketsDenied (UINT4
                                              *pu4RetValFwlStatTotalLargeFragmentPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatTotalLargeFragmentPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_TOTAL_LARGE_FRAGMENT_PACKETS_DENIED;
    lv.pu4RetValFwlStatTotalLargeFragmentPacketsDenied =
        pu4RetValFwlStatTotalLargeFragmentPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatTotalAttacksPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatTotalAttacksPacketsDenied (UINT4
                                        *pu4RetValFwlStatTotalAttacksPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatTotalAttacksPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_TOTAL_ATTACKS_PACKETS_DENIED;
    lv.pu4RetValFwlStatTotalAttacksPacketsDenied =
        pu4RetValFwlStatTotalAttacksPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalNetBiosFiltering
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalNetBiosFiltering (INT4 *pi4RetValFwlNetBiosFiltering)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalNetBiosFiltering lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_NET_BIOS_FILTERING;
    lv.pi4RetValFwlNetBiosFiltering = pi4RetValFwlNetBiosFiltering;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalNetBiosLan2Wan
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalNetBiosLan2Wan (INT4 *pi4RetValFwlNetBiosLan2Wan)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalNetBiosLan2Wan lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_NET_BIOS_LAN2_WAN;
    lv.pi4RetValFwlNetBiosLan2Wan = pi4RetValFwlNetBiosLan2Wan;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalNetBiosLan2Wan
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalNetBiosLan2Wan (INT4 i4SetValFwlGlobalNetBiosLan2Wan)
{
    int                 rc;
    tFwlwnmhSetFwlGlobalNetBiosLan2Wan lv;

    lv.cmd = NMH_SET_FWL_GLOBAL_NET_BIOS_LAN2_WAN;
    lv.i4SetValFwlGlobalNetBiosLan2Wan = i4SetValFwlGlobalNetBiosLan2Wan;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalNetBiosLan2Wan
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalNetBiosLan2Wan (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFwlGlobalNetBiosLan2Wan)
{
    int                 rc;
    tFwlwnmhTestv2FwlGlobalNetBiosLan2Wan lv;

    lv.cmd = NMH_TESTV2_FWL_GLOBAL_NET_BIOS_LAN2_WAN;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFwlGlobalNetBiosLan2Wan = i4TestValFwlGlobalNetBiosLan2Wan;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* MAnually added */
/* ------------------------------------------------------------
 *
 * Function: FwlAddDefaultRules
 *
 * -------------------------------------------------------------
 */
VOID
FwlAddDefaultRules (VOID)
{
    int                 rc;
    tFwlwFwlAddDefaultRules lv;

    lv.cmd = FWL_ADD_DEFAULT_RULES;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    if (rc < 0)
    {
        perror ("-E- FwlAddDefaultRules ");
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: FwlHandleInterfaceIndication
 *
 * -------------------------------------------------------------
 */
INT1
FwlHandleInterfaceIndication (UINT4 u4Interface, UINT4 u4Status)
{
    int                 rc;
    tFwlwFwlHandleInterfaceIndication lv;

    lv.cmd = FWL_HANDLE_INTERFACE_INDICATION;
    lv.u4Interface = u4Interface;
    lv.u4Status = u4Status;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) FWL_FAILURE : lv.rval);
}

INT4
FwlCliCommitInKernel (VOID)
{
    int                 rc;
    tFwlwFwlCliCommit   lv;

    lv.cmd = FWL_CLI_COMMIT;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return rc;
}

/* -------------------------------------------------------------
 *
 * Function: FwlCleanAllAppEntry
 *
 * -------------------------------------------------------------
 */
VOID
FwlCleanAllAppEntry (VOID)
{
    int                 rc;
    tNpwFwlCleanAllAppEntry lv;

    lv.cmd = FWL_CLEAN_ALL_APP_ENTRY;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    if (rc < 0)
    {
        perror ("-E- FwlCleanAllAppEntry ");
    }
    return;
}

#ifdef TR69_WANTED
/* -------------------------------------------------------------
 *
 * Function: FwlSetTr69AcsPortNum
 *
 * -------------------------------------------------------------
 */
VOID
FwlSetTr69AcsPortNum (UINT2 u2PortNo)
{
    int                 rc;
    tFwlSetTr69AcsPortNum lv;

    lv.cmd = FWL_SET_TR69_ACS_PORT_NUM;
    lv.u2PortNo = u2PortNo;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    if (rc < 0)
    {
        perror ("-E- FwlSetTr69AcsPortNum ");
    }
    return;
}
#endif

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceFwlStateTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceFwlStateTable (INT4 i4FwlStateType,
                                       INT4 i4FwlStateLocalIpAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFwlStateLocalIpAddress,
                                       INT4 i4FwlStateRemoteIpAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFwlStateRemoteIpAddress,
                                       INT4 i4FwlStateLocalPort,
                                       INT4 i4FwlStateRemotePort,
                                       INT4 i4FwlStateProtocol,
                                       INT4 i4FwlStateDirection)
{
    int                 rc;
    tFwlwnmhValidateIndexInstanceFwlStateTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_FWL_STATE_TABLE;
    lv.i4FwlStateType = i4FwlStateType;
    lv.i4FwlStateLocalIpAddrType = i4FwlStateLocalIpAddrType;
    lv.pFwlStateLocalIpAddress = pFwlStateLocalIpAddress;
    lv.i4FwlStateRemoteIpAddrType = i4FwlStateRemoteIpAddrType;
    lv.pFwlStateRemoteIpAddress = pFwlStateRemoteIpAddress;
    lv.i4FwlStateLocalPort = i4FwlStateLocalPort;
    lv.i4FwlStateRemotePort = i4FwlStateRemotePort;
    lv.i4FwlStateProtocol = i4FwlStateProtocol;
    lv.i4FwlStateDirection = i4FwlStateDirection;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexFwlStateTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexFwlStateTable (INT4 *pi4FwlStateType,
                               INT4 *pi4FwlStateLocalIpAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFwlStateLocalIpAddress,
                               INT4 *pi4FwlStateRemoteIpAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFwlStateRemoteIpAddress,
                               INT4 *pi4FwlStateLocalPort,
                               INT4 *pi4FwlStateRemotePort,
                               INT4 *pi4FwlStateProtocol,
                               INT4 *pi4FwlStateDirection)
{
    int                 rc;
    tFwlwnmhGetFirstIndexFwlStateTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_FWL_STATE_TABLE;
    lv.pi4FwlStateType = pi4FwlStateType;
    lv.pi4FwlStateLocalIpAddrType = pi4FwlStateLocalIpAddrType;
    lv.pFwlStateLocalIpAddress = pFwlStateLocalIpAddress;
    lv.pi4FwlStateRemoteIpAddrType = pi4FwlStateRemoteIpAddrType;
    lv.pFwlStateRemoteIpAddress = pFwlStateRemoteIpAddress;
    lv.pi4FwlStateLocalPort = pi4FwlStateLocalPort;
    lv.pi4FwlStateRemotePort = pi4FwlStateRemotePort;
    lv.pi4FwlStateProtocol = pi4FwlStateProtocol;
    lv.pi4FwlStateDirection = pi4FwlStateDirection;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexFwlStateTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexFwlStateTable (INT4 i4FwlStateType,
                              INT4 *pi4NextFwlStateType,
                              INT4 i4FwlStateLocalIpAddrType,
                              INT4 *pi4NextFwlStateLocalIpAddrType,
                              tSNMP_OCTET_STRING_TYPE * pFwlStateLocalIpAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pNextFwlStateLocalIpAddress,
                              INT4 i4FwlStateRemoteIpAddrType,
                              INT4 *pi4NextFwlStateRemoteIpAddrType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFwlStateRemoteIpAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pNextFwlStateRemoteIpAddress,
                              INT4 i4FwlStateLocalPort,
                              INT4 *pi4NextFwlStateLocalPort,
                              INT4 i4FwlStateRemotePort,
                              INT4 *pi4NextFwlStateRemotePort,
                              INT4 i4FwlStateProtocol,
                              INT4 *pi4NextFwlStateProtocol,
                              INT4 i4FwlStateDirection,
                              INT4 *pi4NextFwlStateDirection)
{
    int                 rc;
    tFwlwnmhGetNextIndexFwlStateTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_FWL_STATE_TABLE;
    lv.i4FwlStateType = i4FwlStateType;
    lv.pi4NextFwlStateType = pi4NextFwlStateType;
    lv.i4FwlStateLocalIpAddrType = i4FwlStateLocalIpAddrType;
    lv.pi4NextFwlStateLocalIpAddrType = pi4NextFwlStateLocalIpAddrType;
    lv.pFwlStateLocalIpAddress = pFwlStateLocalIpAddress;
    lv.pNextFwlStateLocalIpAddress = pNextFwlStateLocalIpAddress;
    lv.i4FwlStateRemoteIpAddrType = i4FwlStateRemoteIpAddrType;
    lv.pi4NextFwlStateRemoteIpAddrType = pi4NextFwlStateRemoteIpAddrType;
    lv.pFwlStateRemoteIpAddress = pFwlStateRemoteIpAddress;
    lv.pNextFwlStateRemoteIpAddress = pNextFwlStateRemoteIpAddress;
    lv.i4FwlStateLocalPort = i4FwlStateLocalPort;
    lv.pi4NextFwlStateLocalPort = pi4NextFwlStateLocalPort;
    lv.i4FwlStateRemotePort = i4FwlStateRemotePort;
    lv.pi4NextFwlStateRemotePort = pi4NextFwlStateRemotePort;
    lv.i4FwlStateProtocol = i4FwlStateProtocol;
    lv.pi4NextFwlStateProtocol = pi4NextFwlStateProtocol;
    lv.i4FwlStateDirection = i4FwlStateDirection;
    lv.pi4NextFwlStateDirection = pi4NextFwlStateDirection;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStateEstablishedTime
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStateEstablishedTime (INT4 i4FwlStateType,
                               INT4 i4FwlStateLocalIpAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFwlStateLocalIpAddress,
                               INT4 i4FwlStateRemoteIpAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFwlStateRemoteIpAddress,
                               INT4 i4FwlStateLocalPort,
                               INT4 i4FwlStateRemotePort,
                               INT4 i4FwlStateProtocol,
                               INT4 i4FwlStateDirection,
                               UINT4 *pu4RetValFwlStateEstablishedTime)
{
    int                 rc;
    tFwlwnmhGetFwlStateEstablishedTime lv;

    lv.cmd = NMH_GET_FWL_STATE_ESTABLISHED_TIME;
    lv.i4FwlStateType = i4FwlStateType;
    lv.i4FwlStateLocalIpAddrType = i4FwlStateLocalIpAddrType;
    lv.pFwlStateLocalIpAddress = pFwlStateLocalIpAddress;
    lv.i4FwlStateRemoteIpAddrType = i4FwlStateRemoteIpAddrType;
    lv.pFwlStateRemoteIpAddress = pFwlStateRemoteIpAddress;
    lv.i4FwlStateLocalPort = i4FwlStateLocalPort;
    lv.i4FwlStateRemotePort = i4FwlStateRemotePort;
    lv.i4FwlStateProtocol = i4FwlStateProtocol;
    lv.i4FwlStateDirection = i4FwlStateDirection;
    lv.pu4RetValFwlStateEstablishedTime = pu4RetValFwlStateEstablishedTime;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStateLocalState
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStateLocalState (INT4 i4FwlStateType,
                          INT4 i4FwlStateLocalIpAddrType,
                          tSNMP_OCTET_STRING_TYPE * pFwlStateLocalIpAddress,
                          INT4 i4FwlStateRemoteIpAddrType,
                          tSNMP_OCTET_STRING_TYPE * pFwlStateRemoteIpAddress,
                          INT4 i4FwlStateLocalPort,
                          INT4 i4FwlStateRemotePort,
                          INT4 i4FwlStateProtocol,
                          INT4 i4FwlStateDirection,
                          INT4 *pi4RetValFwlStateLocalState)
{
    int                 rc;
    tFwlwnmhGetFwlStateLocalState lv;

    lv.cmd = NMH_GET_FWL_STATE_LOCAL_STATE;
    lv.i4FwlStateType = i4FwlStateType;
    lv.i4FwlStateLocalIpAddrType = i4FwlStateLocalIpAddrType;
    lv.pFwlStateLocalIpAddress = pFwlStateLocalIpAddress;
    lv.i4FwlStateRemoteIpAddrType = i4FwlStateRemoteIpAddrType;
    lv.pFwlStateRemoteIpAddress = pFwlStateRemoteIpAddress;
    lv.i4FwlStateLocalPort = i4FwlStateLocalPort;
    lv.i4FwlStateRemotePort = i4FwlStateRemotePort;
    lv.i4FwlStateProtocol = i4FwlStateProtocol;
    lv.i4FwlStateDirection = i4FwlStateDirection;
    lv.pi4RetValFwlStateLocalState = pi4RetValFwlStateLocalState;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStateRemoteState
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStateRemoteState (INT4 i4FwlStateType,
                           INT4 i4FwlStateLocalIpAddrType,
                           tSNMP_OCTET_STRING_TYPE * pFwlStateLocalIpAddress,
                           INT4 i4FwlStateRemoteIpAddrType,
                           tSNMP_OCTET_STRING_TYPE * pFwlStateRemoteIpAddress,
                           INT4 i4FwlStateLocalPort,
                           INT4 i4FwlStateRemotePort,
                           INT4 i4FwlStateProtocol,
                           INT4 i4FwlStateDirection,
                           INT4 *pi4RetValFwlStateRemoteState)
{
    int                 rc;
    tFwlwnmhGetFwlStateRemoteState lv;

    lv.cmd = NMH_GET_FWL_STATE_REMOTE_STATE;
    lv.i4FwlStateType = i4FwlStateType;
    lv.i4FwlStateLocalIpAddrType = i4FwlStateLocalIpAddrType;
    lv.pFwlStateLocalIpAddress = pFwlStateLocalIpAddress;
    lv.i4FwlStateRemoteIpAddrType = i4FwlStateRemoteIpAddrType;
    lv.pFwlStateRemoteIpAddress = pFwlStateRemoteIpAddress;
    lv.i4FwlStateLocalPort = i4FwlStateLocalPort;
    lv.i4FwlStateRemotePort = i4FwlStateRemotePort;
    lv.i4FwlStateProtocol = i4FwlStateProtocol;
    lv.i4FwlStateDirection = i4FwlStateDirection;
    lv.pi4RetValFwlStateRemoteState = pi4RetValFwlStateRemoteState;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStateLogLevel
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStateLogLevel (INT4 i4FwlStateType,
                        INT4 i4FwlStateLocalIpAddrType,
                        tSNMP_OCTET_STRING_TYPE * pFwlStateLocalIpAddress,
                        INT4 i4FwlStateRemoteIpAddrType,
                        tSNMP_OCTET_STRING_TYPE * pFwlStateRemoteIpAddress,
                        INT4 i4FwlStateLocalPort,
                        INT4 i4FwlStateRemotePort,
                        INT4 i4FwlStateProtocol,
                        INT4 i4FwlStateDirection,
                        INT4 *pi4RetValFwlStateLogLevel)
{
    int                 rc;
    tFwlwnmhGetFwlStateLogLevel lv;

    lv.cmd = NMH_GET_FWL_STATE_LOG_LEVEL;
    lv.i4FwlStateType = i4FwlStateType;
    lv.i4FwlStateLocalIpAddrType = i4FwlStateLocalIpAddrType;
    lv.pFwlStateLocalIpAddress = pFwlStateLocalIpAddress;
    lv.i4FwlStateRemoteIpAddrType = i4FwlStateRemoteIpAddrType;
    lv.pFwlStateRemoteIpAddress = pFwlStateRemoteIpAddress;
    lv.i4FwlStateLocalPort = i4FwlStateLocalPort;
    lv.i4FwlStateRemotePort = i4FwlStateRemotePort;
    lv.i4FwlStateProtocol = i4FwlStateProtocol;
    lv.i4FwlStateDirection = i4FwlStateDirection;
    lv.pi4RetValFwlStateLogLevel = pi4RetValFwlStateLogLevel;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStateCallStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStateCallStatus (INT4 i4FwlStateType,
                          INT4 i4FwlStateLocalIpAddrType,
                          tSNMP_OCTET_STRING_TYPE * pFwlStateLocalIpAddress,
                          INT4 i4FwlStateRemoteIpAddrType,
                          tSNMP_OCTET_STRING_TYPE * pFwlStateRemoteIpAddress,
                          INT4 i4FwlStateLocalPort,
                          INT4 i4FwlStateRemotePort,
                          INT4 i4FwlStateProtocol,
                          INT4 i4FwlStateDirection,
                          INT4 *pi4RetValFwlStateCallStatus)
{
    int                 rc;
    tFwlwnmhGetFwlStateCallStatus lv;

    lv.cmd = NMH_GET_FWL_STATE_CALL_STATUS;
    lv.i4FwlStateType = i4FwlStateType;
    lv.i4FwlStateLocalIpAddrType = i4FwlStateLocalIpAddrType;
    lv.pFwlStateLocalIpAddress = pFwlStateLocalIpAddress;
    lv.i4FwlStateRemoteIpAddrType = i4FwlStateRemoteIpAddrType;
    lv.pFwlStateRemoteIpAddress = pFwlStateRemoteIpAddress;
    lv.i4FwlStateLocalPort = i4FwlStateLocalPort;
    lv.i4FwlStateRemotePort = i4FwlStateRemotePort;
    lv.i4FwlStateProtocol = i4FwlStateProtocol;
    lv.i4FwlStateDirection = i4FwlStateDirection;
    lv.pi4RetValFwlStateCallStatus = pi4RetValFwlStateCallStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2FwlDefnWhiteListTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2FwlDefnWhiteListTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tFwlwnmhDepv2FwlDefnWhiteListTable lv;

    lv.cmd = NMH_DEPV2_FWL_DEFN_WHITE_LIST_TABLE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlWhiteListRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlWhiteListRowStatus (UINT4 *pu4ErrorCode,
                                INT4 i4FwlWhiteListIpAddressType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFwlWhiteListIpAddress,
                                UINT4 u4FwlWhiteListIpMask,
                                INT4 i4TestValFwlWhiteListRowStatus)
{
    int                 rc;
    tFwlwnmhTestv2FwlWhiteListRowStatus lv;

    lv.cmd = NMH_TESTV2_FWL_WHITE_LIST_ROW_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FwlWhiteListIpAddressType = i4FwlWhiteListIpAddressType;
    lv.pFwlWhiteListIpAddress = pFwlWhiteListIpAddress;
    lv.u4FwlWhiteListIpMask = u4FwlWhiteListIpMask;
    lv.i4TestValFwlWhiteListRowStatus = i4TestValFwlWhiteListRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlWhiteListRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlWhiteListRowStatus (INT4 i4FwlWhiteListIpAddressType,
                             tSNMP_OCTET_STRING_TYPE * pFwlWhiteListIpAddress,
                             UINT4 u4FwlWhiteListIpMask,
                             INT4 i4SetValFwlWhiteListRowStatus)
{
    int                 rc;
    tFwlwnmhSetFwlWhiteListRowStatus lv;

    lv.cmd = NMH_SET_FWL_WHITE_LIST_ROW_STATUS;
    lv.i4FwlWhiteListIpAddressType = i4FwlWhiteListIpAddressType;
    lv.pFwlWhiteListIpAddress = pFwlWhiteListIpAddress;
    lv.u4FwlWhiteListIpMask = u4FwlWhiteListIpMask;
    lv.i4SetValFwlWhiteListRowStatus = i4SetValFwlWhiteListRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlWhiteListHitsCount
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlWhiteListHitsCount (INT4 i4FwlWhiteListIpAddressType,
                             tSNMP_OCTET_STRING_TYPE * pFwlWhiteListIpAddress,
                             UINT4 u4FwlWhiteListIpMask,
                             UINT4 *pu4RetValFwlWhiteListHitsCount)
{
    int                 rc;
    tFwlwnmhGetFwlWhiteListHitsCount lv;

    lv.cmd = NMH_GET_FWL_WHITE_LIST_HITS_COUNT;
    lv.i4FwlWhiteListIpAddressType = i4FwlWhiteListIpAddressType;
    lv.pFwlWhiteListIpAddress = pFwlWhiteListIpAddress;
    lv.u4FwlWhiteListIpMask = u4FwlWhiteListIpMask;
    lv.pu4RetValFwlWhiteListHitsCount = pu4RetValFwlWhiteListHitsCount;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlWhiteListRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlWhiteListRowStatus (INT4 i4FwlWhiteListIpAddressType,
                             tSNMP_OCTET_STRING_TYPE * pFwlWhiteListIpAddress,
                             UINT4 u4FwlWhiteListIpMask,
                             INT4 *pi4RetValFwlWhiteListRowStatus)
{
    int                 rc;
    tFwlwnmhGetFwlWhiteListRowStatus lv;

    lv.cmd = NMH_GET_FWL_WHITE_LIST_ROW_STATUS;
    lv.i4FwlWhiteListIpAddressType = i4FwlWhiteListIpAddressType;
    lv.pFwlWhiteListIpAddress = pFwlWhiteListIpAddress;
    lv.u4FwlWhiteListIpMask = u4FwlWhiteListIpMask;
    lv.pi4RetValFwlWhiteListRowStatus = pi4RetValFwlWhiteListRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexFwlDefnWhiteListTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexFwlDefnWhiteListTable (INT4 i4FwlWhiteListIpAddressType,
                                      INT4 *pi4NextFwlWhiteListIpAddressType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFwlWhiteListIpAddress,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextFwlWhiteListIpAddress,
                                      UINT4 u4FwlWhiteListIpMask,
                                      UINT4 *pu4NextFwlWhiteListIpMask)
{
    int                 rc;
    tFwlwnmhGetNextIndexFwlDefnWhiteListTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_FWL_DEFN_WHITE_LIST_TABLE;
    lv.i4FwlWhiteListIpAddressType = i4FwlWhiteListIpAddressType;
    lv.pi4NextFwlWhiteListIpAddressType = pi4NextFwlWhiteListIpAddressType;
    lv.pFwlWhiteListIpAddress = pFwlWhiteListIpAddress;
    lv.pNextFwlWhiteListIpAddress = pNextFwlWhiteListIpAddress;
    lv.u4FwlWhiteListIpMask = u4FwlWhiteListIpMask;
    lv.pu4NextFwlWhiteListIpMask = pu4NextFwlWhiteListIpMask;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexFwlDefnWhiteListTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexFwlDefnWhiteListTable (INT4 *pi4FwlWhiteListIpAddressType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFwlWhiteListIpAddress,
                                       UINT4 *pu4FwlWhiteListIpMask)
{
    int                 rc;
    tFwlwnmhGetFirstIndexFwlDefnWhiteListTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_FWL_DEFN_WHITE_LIST_TABLE;
    lv.pi4FwlWhiteListIpAddressType = pi4FwlWhiteListIpAddressType;
    lv.pFwlWhiteListIpAddress = pFwlWhiteListIpAddress;
    lv.pu4FwlWhiteListIpMask = pu4FwlWhiteListIpMask;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceFwlDefnWhiteListTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceFwlDefnWhiteListTable (INT4 i4FwlWhiteListIpAddressType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFwlWhiteListIpAddress,
                                               UINT4 u4FwlWhiteListIpMask)
{
    int                 rc;
    tFwlwnmhValidateIndexInstanceFwlDefnWhiteListTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_FWL_DEFN_WHITE_LIST_TABLE;
    lv.i4FwlWhiteListIpAddressType = i4FwlWhiteListIpAddressType;
    lv.pFwlWhiteListIpAddress = pFwlWhiteListIpAddress;
    lv.u4FwlWhiteListIpMask = u4FwlWhiteListIpMask;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2FwlDefnBlkListTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2FwlDefnBlkListTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tFwlwnmhDepv2FwlDefnBlkListTable lv;

    lv.cmd = NMH_DEPV2_FWL_DEFN_BLK_LIST_TABLE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlBlkListRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlBlkListRowStatus (UINT4 *pu4ErrorCode,
                              INT4 i4FwlBlkListIpAddressType,
                              tSNMP_OCTET_STRING_TYPE * pFwlBlkListIpAddress,
                              UINT4 u4FwlBlkListIpMask,
                              INT4 i4TestValFwlBlkListRowStatus)
{
    int                 rc;
    tFwlwnmhTestv2FwlBlkListRowStatus lv;

    lv.cmd = NMH_TESTV2_FWL_BLK_LIST_ROW_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FwlBlkListIpAddressType = i4FwlBlkListIpAddressType;
    lv.pFwlBlkListIpAddress = pFwlBlkListIpAddress;
    lv.u4FwlBlkListIpMask = u4FwlBlkListIpMask;
    lv.i4TestValFwlBlkListRowStatus = i4TestValFwlBlkListRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlBlkListRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlBlkListRowStatus (INT4 i4FwlBlkListIpAddressType,
                           tSNMP_OCTET_STRING_TYPE * pFwlBlkListIpAddress,
                           UINT4 u4FwlBlkListIpMask,
                           INT4 i4SetValFwlBlkListRowStatus)
{
    int                 rc;
    tFwlwnmhSetFwlBlkListRowStatus lv;

    lv.cmd = NMH_SET_FWL_BLK_LIST_ROW_STATUS;
    lv.i4FwlBlkListIpAddressType = i4FwlBlkListIpAddressType;
    lv.pFwlBlkListIpAddress = pFwlBlkListIpAddress;
    lv.u4FwlBlkListIpMask = u4FwlBlkListIpMask;
    lv.i4SetValFwlBlkListRowStatus = i4SetValFwlBlkListRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlBlkListRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlBlkListRowStatus (INT4 i4FwlBlkListIpAddressType,
                           tSNMP_OCTET_STRING_TYPE * pFwlBlkListIpAddress,
                           UINT4 u4FwlBlkListIpMask,
                           INT4 *pi4RetValFwlBlkListRowStatus)
{
    int                 rc;
    tFwlwnmhGetFwlBlkListRowStatus lv;

    lv.cmd = NMH_GET_FWL_BLK_LIST_ROW_STATUS;
    lv.i4FwlBlkListIpAddressType = i4FwlBlkListIpAddressType;
    lv.pFwlBlkListIpAddress = pFwlBlkListIpAddress;
    lv.u4FwlBlkListIpMask = u4FwlBlkListIpMask;
    lv.pi4RetValFwlBlkListRowStatus = pi4RetValFwlBlkListRowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlBlkListEntryType
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlBlkListEntryType (INT4 i4FwlBlkListIpAddressType,
                           tSNMP_OCTET_STRING_TYPE * pFwlBlkListIpAddress,
                           UINT4 u4FwlBlkListIpMask,
                           INT4 *pi4RetValFwlBlkListEntryType)
{
    int                 rc;
    tFwlwnmhGetFwlBlkListEntryType lv;

    lv.cmd = NMH_GET_FWL_BLK_LIST_ENTRY_TYPE;
    lv.i4FwlBlkListIpAddressType = i4FwlBlkListIpAddressType;
    lv.pFwlBlkListIpAddress = pFwlBlkListIpAddress;
    lv.u4FwlBlkListIpMask = u4FwlBlkListIpMask;
    lv.pi4RetValFwlBlkListEntryType = pi4RetValFwlBlkListEntryType;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlBlkListHitsCount
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlBlkListHitsCount (INT4 i4FwlBlkListIpAddressType,
                           tSNMP_OCTET_STRING_TYPE * pFwlBlkListIpAddress,
                           UINT4 u4FwlBlkListIpMask,
                           UINT4 *pu4RetValFwlBlkListHitsCount)
{
    int                 rc;
    tFwlwnmhGetFwlBlkListHitsCount lv;

    lv.cmd = NMH_GET_FWL_BLK_LIST_HITS_COUNT;
    lv.i4FwlBlkListIpAddressType = i4FwlBlkListIpAddressType;
    lv.pFwlBlkListIpAddress = pFwlBlkListIpAddress;
    lv.u4FwlBlkListIpMask = u4FwlBlkListIpMask;
    lv.pu4RetValFwlBlkListHitsCount = pu4RetValFwlBlkListHitsCount;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexFwlDefnBlkListTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexFwlDefnBlkListTable (INT4 i4FwlBlkListIpAddressType,
                                    INT4 *pi4NextFwlBlkListIpAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFwlBlkListIpAddress,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextFwlBlkListIpAddress,
                                    UINT4 u4FwlBlkListIpMask,
                                    UINT4 *pu4NextFwlBlkListIpMask)
{
    int                 rc;
    tFwlwnmhGetNextIndexFwlDefnBlkListTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_FWL_DEFN_BLK_LIST_TABLE;
    lv.i4FwlBlkListIpAddressType = i4FwlBlkListIpAddressType;
    lv.pi4NextFwlBlkListIpAddressType = pi4NextFwlBlkListIpAddressType;
    lv.pFwlBlkListIpAddress = pFwlBlkListIpAddress;
    lv.pNextFwlBlkListIpAddress = pNextFwlBlkListIpAddress;
    lv.u4FwlBlkListIpMask = u4FwlBlkListIpMask;
    lv.pu4NextFwlBlkListIpMask = pu4NextFwlBlkListIpMask;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexFwlDefnBlkListTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexFwlDefnBlkListTable (INT4 *pi4FwlBlkListIpAddressType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFwlBlkListIpAddress,
                                     UINT4 *pu4FwlBlkListIpMask)
{
    int                 rc;
    tFwlwnmhGetFirstIndexFwlDefnBlkListTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_FWL_DEFN_BLK_LIST_TABLE;
    lv.pi4FwlBlkListIpAddressType = pi4FwlBlkListIpAddressType;
    lv.pFwlBlkListIpAddress = pFwlBlkListIpAddress;
    lv.pu4FwlBlkListIpMask = pu4FwlBlkListIpMask;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceFwlDefnBlkListTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceFwlDefnBlkListTable (INT4 i4FwlBlkListIpAddressType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFwlBlkListIpAddress,
                                             UINT4 u4FwlBlkListIpMask)
{
    int                 rc;
    tFwlwnmhValidateIndexInstanceFwlDefnBlkListTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_FWL_DEFN_BLK_LIST_TABLE;
    lv.i4FwlBlkListIpAddressType = i4FwlBlkListIpAddressType;
    lv.pFwlBlkListIpAddress = pFwlBlkListIpAddress;
    lv.u4FwlBlkListIpMask = u4FwlBlkListIpMask;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: FwlHandleBlackListInfoFromIDS
 *
 * -------------------------------------------------------------
 */
INT4
FwlHandleBlackListInfoFromIDS (UINT1 *pu1Buf)
{
    int                 rc;
    tFwlwFwlHandleBlackListInfoFromIDS lv;

    lv.cmd = FWL_HANDLE_BLACK_LIST_INFO_FROM_I_D_S;
    lv.pu1Buf = pu1Buf;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT4) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalICMPv6ControlSwitch
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalICMPv6ControlSwitch (INT4 *pi4RetValFwlGlobalICMPv6ControlSwitch)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalICMPv6ControlSwitch lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_I_C_M_PV6_CONTROL_SWITCH;
    lv.pi4RetValFwlGlobalICMPv6ControlSwitch =
        pi4RetValFwlGlobalICMPv6ControlSwitch;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlGlobalIpv6SpoofFiltering
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlGlobalIpv6SpoofFiltering (INT4 *pi4RetValFwlGlobalIpv6SpoofFiltering)
{
    int                 rc;
    tFwlwnmhGetFwlGlobalIpv6SpoofFiltering lv;

    lv.cmd = NMH_GET_FWL_GLOBAL_IPV6_SPOOF_FILTERING;
    lv.pi4RetValFwlGlobalIpv6SpoofFiltering =
        pi4RetValFwlGlobalIpv6SpoofFiltering;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalICMPv6ControlSwitch
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalICMPv6ControlSwitch (INT4 i4SetValFwlGlobalICMPv6ControlSwitch)
{
    int                 rc;
    tFwlwnmhSetFwlGlobalICMPv6ControlSwitch lv;

    lv.cmd = NMH_SET_FWL_GLOBAL_I_C_M_PV6_CONTROL_SWITCH;
    lv.i4SetValFwlGlobalICMPv6ControlSwitch =
        i4SetValFwlGlobalICMPv6ControlSwitch;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlGlobalIpv6SpoofFiltering
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlGlobalIpv6SpoofFiltering (INT4 i4SetValFwlGlobalIpv6SpoofFiltering)
{
    int                 rc;
    tFwlwnmhSetFwlGlobalIpv6SpoofFiltering lv;

    lv.cmd = NMH_SET_FWL_GLOBAL_IPV6_SPOOF_FILTERING;
    lv.i4SetValFwlGlobalIpv6SpoofFiltering =
        i4SetValFwlGlobalIpv6SpoofFiltering;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalICMPv6ControlSwitch
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalICMPv6ControlSwitch (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFwlGlobalICMPv6ControlSwitch)
{
    int                 rc;
    tFwlwnmhTestv2FwlGlobalICMPv6ControlSwitch lv;

    lv.cmd = NMH_TESTV2_FWL_GLOBAL_I_C_M_PV6_CONTROL_SWITCH;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFwlGlobalICMPv6ControlSwitch =
        i4TestValFwlGlobalICMPv6ControlSwitch;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalIpv6SpoofFiltering
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalIpv6SpoofFiltering (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFwlGlobalIpv6SpoofFiltering)
{
    int                 rc;
    tFwlwnmhTestv2FwlGlobalIpv6SpoofFiltering lv;

    lv.cmd = NMH_TESTV2_FWL_GLOBAL_IPV6_SPOOF_FILTERING;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFwlGlobalIpv6SpoofFiltering =
        i4TestValFwlGlobalIpv6SpoofFiltering;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalLogFileSize
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalLogFileSize (UINT4 *pu4ErrorCode,
                               UINT4 u4TestValFwlGlobalLogFileSize)
{
    int                 rc;
    tFwlwnmhTestv2FwlGlobalLogFileSize lv;

    lv.cmd = NMH_TESTV2_FWL_GLOBAL_LOG_FILE_SIZE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.u4TestValFwlGlobalLogFileSize = u4TestValFwlGlobalLogFileSize;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalLogSizeThreshold
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalLogSizeThreshold (UINT4 *pu4ErrorCode,
                                    UINT4 u4TestValFwlGlobalLogSizeThreshold)
{
    int                 rc;
    tFwlwnmhTestv2FwlGlobalLogSizeThreshold lv;

    lv.cmd = NMH_TESTV2_FWL_GLOBAL_LOG_SIZE_THRESHOLD;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.u4TestValFwlGlobalLogSizeThreshold = u4TestValFwlGlobalLogSizeThreshold;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalIdsLogSize
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalIdsLogSize (UINT4 *pu4ErrorCode,
                              UINT4 u4TestValFwlGlobalIdsLogSize)
{
    int                 rc;
    tFwlwnmhTestv2FwlGlobalIdsLogSize lv;

    lv.cmd = NMH_TESTV2_FWL_GLOBAL_IDS_LOG_SIZE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.u4TestValFwlGlobalIdsLogSize = u4TestValFwlGlobalIdsLogSize;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalIdsLogThreshold
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalIdsLogThreshold (UINT4 *pu4ErrorCode,
                                   UINT4 u4TestValFwlGlobalIdsLogThreshold)
{
    int                 rc;
    tFwlwnmhTestv2FwlGlobalIdsLogThreshold lv;

    lv.cmd = NMH_TESTV2_FWL_GLOBAL_IDS_LOG_THRESHOLD;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.u4TestValFwlGlobalIdsLogThreshold = u4TestValFwlGlobalIdsLogThreshold;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalReloadIds
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalReloadIds (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFwlGlobalReloadIds)
{
    if (i4TestValFwlGlobalReloadIds != IDS_RELOAD)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalIdsStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalIdsStatus (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFwlGlobalIdsStatus)
{
    int                 rc;
    tFwlwnmhTestv2FwlGlobalIdsStatus lv;

    lv.cmd = NMH_TESTV2_FWL_GLOBAL_IDS_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFwlGlobalIdsStatus = i4TestValFwlGlobalIdsStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlGlobalLoadIdsRules
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlGlobalLoadIdsRules (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFwlGlobalLoadIdsRules)
{
    if ((i4TestValFwlGlobalLoadIdsRules != IDS_LOAD_RULES) &&
        (i4TestValFwlGlobalLoadIdsRules != IDS_UNLOAD_RULES))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2FwlGlobalICMPv6ControlSwitch
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2FwlGlobalICMPv6ControlSwitch (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tFwlwnmhDepv2FwlGlobalICMPv6ControlSwitch lv;

    lv.cmd = NMH_DEPV2_FWL_GLOBAL_I_C_M_PV6_CONTROL_SWITCH;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2FwlGlobalIpv6SpoofFiltering
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2FwlGlobalIpv6SpoofFiltering (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tFwlwnmhDepv2FwlGlobalIpv6SpoofFiltering lv;

    lv.cmd = NMH_DEPV2_FWL_GLOBAL_IPV6_SPOOF_FILTERING;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2FwlGlobalLogFileSize
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2FwlGlobalLogFileSize (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tFwlwnmhDepv2FwlGlobalLogFileSize lv;

    lv.cmd = NMH_DEPV2_FWL_GLOBAL_LOG_FILE_SIZE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2FwlGlobalLogSizeThreshold
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2FwlGlobalLogSizeThreshold (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tFwlwnmhDepv2FwlGlobalLogSizeThreshold lv;

    lv.cmd = NMH_DEPV2_FWL_GLOBAL_LOG_SIZE_THRESHOLD;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2FwlGlobalIdsLogSize
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2FwlGlobalIdsLogSize (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tFwlwnmhDepv2FwlGlobalIdsLogSize lv;

    lv.cmd = NMH_DEPV2_FWL_GLOBAL_IDS_LOG_SIZE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2FwlGlobalIdsLogThreshold
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2FwlGlobalIdsLogThreshold (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tFwlwnmhDepv2FwlGlobalIdsLogThreshold lv;

    lv.cmd = NMH_DEPV2_FWL_GLOBAL_IDS_LOG_THRESHOLD;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2FwlGlobalReloadIds
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2FwlGlobalReloadIds (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2FwlGlobalIdsStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2FwlGlobalIdsStatus (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tFwlwnmhDepv2FwlGlobalIdsStatus lv;

    lv.cmd = NMH_DEPV2_FWL_GLOBAL_IDS_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

INT1
nmhDepv2FwlGlobalLoadIdsRules (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlFilterAddrType
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlFilterAddrType (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                         INT4 *pi4RetValFwlFilterAddrType)
{
    int                 rc;
    tFwlwnmhGetFwlFilterAddrType lv;

    lv.cmd = NMH_GET_FWL_FILTER_ADDR_TYPE;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pi4RetValFwlFilterAddrType = pi4RetValFwlFilterAddrType;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlFilterFlowId
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlFilterFlowId (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                       UINT4 *pu4RetValFwlFilterFlowId)
{
    int                 rc;
    tFwlwnmhGetFwlFilterFlowId lv;

    lv.cmd = NMH_GET_FWL_FILTER_FLOW_ID;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pu4RetValFwlFilterFlowId = pu4RetValFwlFilterFlowId;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlFilterDscp
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlFilterDscp (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                     INT4 *pi4RetValFwlFilterDscp)
{
    int                 rc;
    tFwlwnmhGetFwlFilterDscp lv;

    lv.cmd = NMH_GET_FWL_FILTER_DSCP;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.pi4RetValFwlFilterDscp = pi4RetValFwlFilterDscp;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlFilterAddrType
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlFilterAddrType (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                         INT4 i4SetValFwlFilterAddrType)
{
    int                 rc;
    tFwlwnmhSetFwlFilterAddrType lv;

    lv.cmd = NMH_SET_FWL_FILTER_ADDR_TYPE;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.i4SetValFwlFilterAddrType = i4SetValFwlFilterAddrType;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlFilterFlowId
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlFilterFlowId (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                       UINT4 u4SetValFwlFilterFlowId)
{
    int                 rc;
    tFwlwnmhSetFwlFilterFlowId lv;

    lv.cmd = NMH_SET_FWL_FILTER_FLOW_ID;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.u4SetValFwlFilterFlowId = u4SetValFwlFilterFlowId;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlFilterDscp
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlFilterDscp (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                     INT4 i4SetValFwlFilterDscp)
{
    int                 rc;
    tFwlwnmhSetFwlFilterDscp lv;

    lv.cmd = NMH_SET_FWL_FILTER_DSCP;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.i4SetValFwlFilterDscp = i4SetValFwlFilterDscp;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlFilterAddrType
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlFilterAddrType (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                            INT4 i4TestValFwlFilterAddrType)
{
    int                 rc;
    tFwlwnmhTestv2FwlFilterAddrType lv;

    lv.cmd = NMH_TESTV2_FWL_FILTER_ADDR_TYPE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.i4TestValFwlFilterAddrType = i4TestValFwlFilterAddrType;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlFilterFlowId
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlFilterFlowId (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                          UINT4 u4TestValFwlFilterFlowId)
{
    int                 rc;
    tFwlwnmhTestv2FwlFilterFlowId lv;

    lv.cmd = NMH_TESTV2_FWL_FILTER_FLOW_ID;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.u4TestValFwlFilterFlowId = u4TestValFwlFilterFlowId;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlFilterDscp
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlFilterDscp (UINT4 *pu4ErrorCode,
                        tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                        INT4 i4TestValFwlFilterDscp)
{
    int                 rc;
    tFwlwnmhTestv2FwlFilterDscp lv;

    lv.cmd = NMH_TESTV2_FWL_FILTER_DSCP;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFwlFilterFilterName = pFwlFilterFilterName;
    lv.i4TestValFwlFilterDscp = i4TestValFwlFilterDscp;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlIfICMPv6MsgType
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlIfICMPv6MsgType (INT4 i4FwlIfIfIndex,
                          INT4 *pi4RetValFwlIfICMPv6MsgType)
{
    int                 rc;
    tFwlwnmhGetFwlIfICMPv6MsgType lv;

    lv.cmd = NMH_GET_FWL_IF_I_C_M_PV6_MSG_TYPE;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.pi4RetValFwlIfICMPv6MsgType = pi4RetValFwlIfICMPv6MsgType;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlIfICMPv6MsgType
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlIfICMPv6MsgType (INT4 i4FwlIfIfIndex, INT4 i4SetValFwlIfICMPv6MsgType)
{
    int                 rc;
    tFwlwnmhSetFwlIfICMPv6MsgType lv;

    lv.cmd = NMH_SET_FWL_IF_I_C_M_PV6_MSG_TYPE;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.i4SetValFwlIfICMPv6MsgType = i4SetValFwlIfICMPv6MsgType;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlIfICMPv6MsgType
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlIfICMPv6MsgType (UINT4 *pu4ErrorCode,
                             INT4 i4FwlIfIfIndex,
                             INT4 i4TestValFwlIfICMPv6MsgType)
{
    int                 rc;
    tFwlwnmhTestv2FwlIfICMPv6MsgType lv;

    lv.cmd = NMH_TESTV2_FWL_IF_I_C_M_PV6_MSG_TYPE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FwlIfIfIndex = i4FwlIfIfIndex;
    lv.i4TestValFwlIfICMPv6MsgType = i4TestValFwlIfICMPv6MsgType;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceFwlDefnIPv6DmzTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceFwlDefnIPv6DmzTable (tSNMP_OCTET_STRING_TYPE *
                                             pFwlDmzIpv6Index)
{
    int                 rc;
    tFwlwnmhValidateIndexInstanceFwlDefnIPv6DmzTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_FWL_DEFN_I_PV6_DMZ_TABLE;
    lv.pFwlDmzIpv6Index = pFwlDmzIpv6Index;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexFwlDefnIPv6DmzTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexFwlDefnIPv6DmzTable (tSNMP_OCTET_STRING_TYPE * pFwlDmzIpv6Index)
{
    int                 rc;
    tFwlwnmhGetFirstIndexFwlDefnIPv6DmzTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_FWL_DEFN_I_PV6_DMZ_TABLE;
    lv.pFwlDmzIpv6Index = pFwlDmzIpv6Index;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexFwlDefnIPv6DmzTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexFwlDefnIPv6DmzTable (tSNMP_OCTET_STRING_TYPE * pFwlDmzIpv6Index,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextFwlDmzIpv6Index)
{
    int                 rc;
    tFwlwnmhGetNextIndexFwlDefnIPv6DmzTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_FWL_DEFN_I_PV6_DMZ_TABLE;
    lv.pFwlDmzIpv6Index = pFwlDmzIpv6Index;
    lv.pNextFwlDmzIpv6Index = pNextFwlDmzIpv6Index;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlDmzAddressType
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlDmzAddressType (tSNMP_OCTET_STRING_TYPE * pFwlDmzIpv6Index,
                         INT4 *pi4RetValFwlDmzAddressType)
{
    int                 rc;
    tFwlwnmhGetFwlDmzAddressType lv;

    lv.cmd = NMH_GET_FWL_DMZ_ADDRESS_TYPE;
    lv.pFwlDmzIpv6Index = pFwlDmzIpv6Index;
    lv.pi4RetValFwlDmzAddressType = pi4RetValFwlDmzAddressType;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlDmzIpv6RowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlDmzIpv6RowStatus (tSNMP_OCTET_STRING_TYPE * pFwlDmzIpv6Index,
                           INT4 *pi4RetValFwlDmzIpv6RowStatus)
{
    int                 rc;
    tFwlwnmhGetFwlDmzIpv6RowStatus lv;

    lv.cmd = NMH_GET_FWL_DMZ_IPV6_ROW_STATUS;
    lv.pFwlDmzIpv6Index = pFwlDmzIpv6Index;
    lv.pi4RetValFwlDmzIpv6RowStatus = pi4RetValFwlDmzIpv6RowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlDmzAddressType
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlDmzAddressType (tSNMP_OCTET_STRING_TYPE * pFwlDmzIpv6Index,
                         INT4 i4SetValFwlDmzAddressType)
{
    int                 rc;
    tFwlwnmhSetFwlDmzAddressType lv;

    lv.cmd = NMH_SET_FWL_DMZ_ADDRESS_TYPE;
    lv.pFwlDmzIpv6Index = pFwlDmzIpv6Index;
    lv.i4SetValFwlDmzAddressType = i4SetValFwlDmzAddressType;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlDmzIpv6RowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlDmzIpv6RowStatus (tSNMP_OCTET_STRING_TYPE * pFwlDmzIpv6Index,
                           INT4 i4SetValFwlDmzIpv6RowStatus)
{
    int                 rc;
    tFwlwnmhSetFwlDmzIpv6RowStatus lv;

    lv.cmd = NMH_SET_FWL_DMZ_IPV6_ROW_STATUS;
    lv.pFwlDmzIpv6Index = pFwlDmzIpv6Index;
    lv.i4SetValFwlDmzIpv6RowStatus = i4SetValFwlDmzIpv6RowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlDmzAddressType
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlDmzAddressType (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFwlDmzIpv6Index,
                            INT4 i4TestValFwlDmzAddressType)
{
    int                 rc;
    tFwlwnmhTestv2FwlDmzAddressType lv;

    lv.cmd = NMH_TESTV2_FWL_DMZ_ADDRESS_TYPE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFwlDmzIpv6Index = pFwlDmzIpv6Index;
    lv.i4TestValFwlDmzAddressType = i4TestValFwlDmzAddressType;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlDmzIpv6RowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlDmzIpv6RowStatus (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFwlDmzIpv6Index,
                              INT4 i4TestValFwlDmzIpv6RowStatus)
{
    int                 rc;
    tFwlwnmhTestv2FwlDmzIpv6RowStatus lv;

    lv.cmd = NMH_TESTV2_FWL_DMZ_IPV6_ROW_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFwlDmzIpv6Index = pFwlDmzIpv6Index;
    lv.i4TestValFwlDmzIpv6RowStatus = i4TestValFwlDmzIpv6RowStatus;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2FwlDefnIPv6DmzTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2FwlDefnIPv6DmzTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tFwlwnmhDepv2FwlDefnIPv6DmzTable lv;

    lv.cmd = NMH_DEPV2_FWL_DEFN_I_PV6_DMZ_TABLE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIPv6InspectedPacketsCount
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIPv6InspectedPacketsCount (UINT4
                                        *pu4RetValFwlStatIPv6InspectedPacketsCount)
{
    int                 rc;
    tFwlwnmhGetFwlStatIPv6InspectedPacketsCount lv;

    lv.cmd = NMH_GET_FWL_STAT_I_PV6_INSPECTED_PACKETS_COUNT;
    lv.pu4RetValFwlStatIPv6InspectedPacketsCount =
        pu4RetValFwlStatIPv6InspectedPacketsCount;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIPv6TotalPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIPv6TotalPacketsDenied (UINT4
                                     *pu4RetValFwlStatIPv6TotalPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatIPv6TotalPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_I_PV6_TOTAL_PACKETS_DENIED;
    lv.pu4RetValFwlStatIPv6TotalPacketsDenied =
        pu4RetValFwlStatIPv6TotalPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIPv6TotalPacketsAccepted
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIPv6TotalPacketsAccepted (UINT4
                                       *pu4RetValFwlStatIPv6TotalPacketsAccepted)
{
    int                 rc;
    tFwlwnmhGetFwlStatIPv6TotalPacketsAccepted lv;

    lv.cmd = NMH_GET_FWL_STAT_I_PV6_TOTAL_PACKETS_ACCEPTED;
    lv.pu4RetValFwlStatIPv6TotalPacketsAccepted =
        pu4RetValFwlStatIPv6TotalPacketsAccepted;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIPv6TotalIcmpPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIPv6TotalIcmpPacketsDenied (UINT4
                                         *pu4RetValFwlStatIPv6TotalIcmpPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatIPv6TotalIcmpPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_I_PV6_TOTAL_ICMP_PACKETS_DENIED;
    lv.pu4RetValFwlStatIPv6TotalIcmpPacketsDenied =
        pu4RetValFwlStatIPv6TotalIcmpPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIPv6TotalSpoofedPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIPv6TotalSpoofedPacketsDenied (UINT4
                                            *pu4RetValFwlStatIPv6TotalSpoofedPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatIPv6TotalSpoofedPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_I_PV6_TOTAL_SPOOFED_PACKETS_DENIED;
    lv.pu4RetValFwlStatIPv6TotalSpoofedPacketsDenied =
        pu4RetValFwlStatIPv6TotalSpoofedPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIPv6TotalAttacksPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIPv6TotalAttacksPacketsDenied (UINT4
                                            *pu4RetValFwlStatIPv6TotalAttacksPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatIPv6TotalAttacksPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_I_PV6_TOTAL_ATTACKS_PACKETS_DENIED;
    lv.pu4RetValFwlStatIPv6TotalAttacksPacketsDenied =
        pu4RetValFwlStatIPv6TotalAttacksPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatClearIPv6
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatClearIPv6 (INT4 *pi4RetValFwlStatClearIPv6)
{
    int                 rc;
    tFwlwnmhGetFwlStatClearIPv6 lv;

    lv.cmd = NMH_GET_FWL_STAT_CLEAR_I_PV6;
    lv.pi4RetValFwlStatClearIPv6 = pi4RetValFwlStatClearIPv6;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlStatClearIPv6
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlStatClearIPv6 (INT4 i4SetValFwlStatClearIPv6)
{
    int                 rc;
    tFwlwnmhSetFwlStatClearIPv6 lv;

    lv.cmd = NMH_SET_FWL_STAT_CLEAR_I_PV6;
    lv.i4SetValFwlStatClearIPv6 = i4SetValFwlStatClearIPv6;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlStatClearIPv6
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlStatClearIPv6 (UINT4 *pu4ErrorCode, INT4 i4TestValFwlStatClearIIPv6)
{
    int                 rc;
    tFwlwnmhTestv2FwlStatClearIPv6 lv;

    lv.cmd = NMH_TESTV2_FWL_STAT_CLEAR_I_PV6;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFwlStatClearIIPv6 = i4TestValFwlStatClearIIPv6;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhDepv2FwlStatClearIPv6
 *
 * -------------------------------------------------------------
 */
INT1
nmhDepv2FwlStatClearIPv6 (UINT4 *pu4ErrorCode,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    int                 rc;
    tFwlwnmhDepv2FwlStatClearIPv6 lv;

    lv.cmd = NMH_DEPV2_FWL_STAT_CLEAR_I_PV6;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pSnmpIndexList = pSnmpIndexList;
    lv.pSnmpVarBind = pSnmpVarBind;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIfClearIPv6
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIfClearIPv6 (INT4 i4FwlStatIfIfIndex,
                          INT4 *pi4RetValFwlStatIfClearIPv6)
{
    int                 rc;
    tFwlwnmhGetFwlStatIfClearIPv6 lv;

    lv.cmd = NMH_GET_FWL_STAT_IF_CLEAR_I_PV6;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.pi4RetValFwlStatIfClearIPv6 = pi4RetValFwlStatIfClearIPv6;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIfIPv6PacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIfIPv6PacketsDenied (INT4 i4FwlStatIfIfIndex,
                                  UINT4 *pu4RetValFwlStatIfIPv6PacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatIfIPv6PacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_IF_I_PV6_PACKETS_DENIED;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.pu4RetValFwlStatIfIPv6PacketsDenied =
        pu4RetValFwlStatIfIPv6PacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIfIPv6PacketsAccepted
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIfIPv6PacketsAccepted (INT4 i4FwlStatIfIfIndex,
                                    UINT4
                                    *pu4RetValFwlStatIfIPv6PacketsAccepted)
{
    int                 rc;
    tFwlwnmhGetFwlStatIfIPv6PacketsAccepted lv;

    lv.cmd = NMH_GET_FWL_STAT_IF_I_PV6_PACKETS_ACCEPTED;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.pu4RetValFwlStatIfIPv6PacketsAccepted =
        pu4RetValFwlStatIfIPv6PacketsAccepted;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIfIPv6IcmpPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIfIPv6IcmpPacketsDenied (INT4 i4FwlStatIfIfIndex,
                                      UINT4
                                      *pu4RetValFwlStatIfIPv6IcmpPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatIfIPv6IcmpPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_IF_I_PV6_ICMP_PACKETS_DENIED;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.pu4RetValFwlStatIfIPv6IcmpPacketsDenied =
        pu4RetValFwlStatIfIPv6IcmpPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetFwlStatIfIPv6SpoofedPacketsDenied
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFwlStatIfIPv6SpoofedPacketsDenied (INT4 i4FwlStatIfIfIndex,
                                         UINT4
                                         *pu4RetValFwlStatIfIPv6SpoofedPacketsDenied)
{
    int                 rc;
    tFwlwnmhGetFwlStatIfIPv6SpoofedPacketsDenied lv;

    lv.cmd = NMH_GET_FWL_STAT_IF_I_PV6_SPOOFED_PACKETS_DENIED;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.pu4RetValFwlStatIfIPv6SpoofedPacketsDenied =
        pu4RetValFwlStatIfIPv6SpoofedPacketsDenied;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetFwlStatIfClearIPv6
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetFwlStatIfClearIPv6 (INT4 i4FwlStatIfIfIndex,
                          INT4 i4SetValFwlStatIfClearIPv6)
{
    int                 rc;
    tFwlwnmhSetFwlStatIfClearIPv6 lv;

    lv.cmd = NMH_SET_FWL_STAT_IF_CLEAR_I_PV6;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.i4SetValFwlStatIfClearIPv6 = i4SetValFwlStatIfClearIPv6;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2FwlStatIfClearIPv6
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2FwlStatIfClearIPv6 (UINT4 *pu4ErrorCode,
                             INT4 i4FwlStatIfIfIndex,
                             INT4 i4TestValFwlStatIfClearIPv6)
{
    int                 rc;
    tFwlwnmhTestv2FwlStatIfClearIPv6 lv;

    lv.cmd = NMH_TESTV2_FWL_STAT_IF_CLEAR_I_PV6;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FwlStatIfIfIndex = i4FwlStatIfIfIndex;
    lv.i4TestValFwlStatIfClearIPv6 = i4TestValFwlStatIfClearIPv6;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    return (rc < 0 ? (INT1) SNMP_SUCCESS : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: FwlSetHwFilterCapabilities
 *
 * -------------------------------------------------------------
 */
INT4
FwlSetHwFilterCapabilities (UINT1 *pFwlHwFilterCapabilities)
{
    INT4                rc;
    tFwlwFwlSetHwFilterCapabilities lv;

    lv.cmd = FWL_SET_HW_FIL_CAP_IN_KERNEL;
    lv.pu1HwFilterCapabilities = pFwlHwFilterCapabilities;

    rc = ioctl (gi4SecDevFd, FWL_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) FWL_SUCCESS : lv.rval);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlUtilGetIpAddress                                */
/*                                                                           */
/*     DESCRIPTION      : This function copies the IP address to pFwlIpAddr  */
/*                                                                           */
/*     INPUT            : i4Afi  - Address Family Identifier                 */
/*                                                                           */
/*                        pOctetString - IPv4 /IPv6 address                  */
/*                                                                           */
/*     OUTPUT           : pFwlIpAddr - Pointer to Fwl IPvx address           */
/*                                                                           */
/*     RETURNS          : FWL_SUCCESS or FWL_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
FwlUtilGetIpAddress (INT4 i4Afi, tSNMP_OCTET_STRING_TYPE * pOctetString,
                     tFwlIpAddr * pFwlIpAddr)
{
    UINT4               u4PeerAddress = 0;

    switch (i4Afi)
    {
        case IPVX_ADDR_FMLY_IPV4:
            MEMCPY ((CHR1 *) & u4PeerAddress,
                    (CHR1 *) pOctetString->pu1_OctetList, sizeof (UINT4));
            pFwlIpAddr->uIpAddr.Ip4Addr = u4PeerAddress;
            break;

        case IPVX_ADDR_FMLY_IPV6:
            MEMCPY (pFwlIpAddr->uIpAddr.Ip6Addr.ip6_addr_u.u1ByteAddr,
                    pOctetString->pu1_OctetList, FWL_IPV6_PREFIX_LEN);
            break;
        default:
            return FWL_FAILURE;
    }
    return FWL_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlDosAttackAcceptRedirect
 Input       :  The Indices

                The Object
                retValFwlDosAttackAcceptRedirect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlDosAttackAcceptRedirect(INT4 *pi4RetValFwlDosAttackAcceptRedirect)
{
    UNUSED_PARAM (pi4RetValFwlDosAttackAcceptRedirect);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlDosAttackAcceptRedirect
 Input       :  The Indices

                The Object
                setValFwlDosAttackAcceptRedirect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlDosAttackAcceptRedirect(INT4 i4SetValFwlDosAttackAcceptRedirect)
{
    UNUSED_PARAM (i4SetValFwlDosAttackAcceptRedirect);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFwlDosAttackAcceptSmurfAttack
 Input       :  The Indices

                The Object
                retValFwlDosAttackAcceptSmurfAttack
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlDosAttackAcceptSmurfAttack(INT4 *pi4RetValFwlDosAttackAcceptSmurfAttack)
{
    UNUSED_PARAM (pi4RetValFwlDosAttackAcceptSmurfAttack);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlDosAttackAcceptSmurfAttack
 Input       :  The Indices

                The Object
                setValFwlDosAttackAcceptSmurfAttack
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlDosAttackAcceptSmurfAttack(INT4 i4SetValFwlDosAttackAcceptSmurfAttack)
{
    UNUSED_PARAM (i4SetValFwlDosAttackAcceptSmurfAttack);
    return SNMP_SUCCESS;


}

/****************************************************************************
 Function    :  nmhTestv2FwlDosAttackAcceptSmurfAttack
 Input       :  The Indices

                The Object
                testValFwlDosAttackAcceptSmurfAttack
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlDosAttackAcceptSmurfAttack(UINT4 *pu4ErrorCode , INT4 i4TestValFwlDosAttackAcceptSmurfAttack)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFwlDosAttackAcceptSmurfAttack);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2FwlDosAttackAcceptSmurfAttack
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlDosAttackAcceptSmurfAttack(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FwlDosAttackAcceptRedirect
 Input       :  The Indices

                The Object
                testValFwlDosAttackAcceptRedirect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlDosAttackAcceptRedirect(UINT4 *pu4ErrorCode , INT4 i4TestValFwlDosAttackAcceptRedirect)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFwlDosAttackAcceptRedirect);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2FwlDosAttackAcceptRedirect
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlDosAttackAcceptRedirect(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlDosLandAttack
 Input       :  The Indices

                The Object
                retValFwlDosLandAttack
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlDosLandAttack(INT4 *pi4RetValFwlDosLandAttack)
{
    UNUSED_PARAM (pi4RetValFwlDosLandAttack);
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetFwlDosLandAttack
 Input       :  The Indices

                The Object
                setValFwlDosLandAttack
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlDosLandAttack(INT4 i4SetValFwlDosLandAttack)
{
    UNUSED_PARAM (i4SetValFwlDosLandAttack);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FwlDosLandAttack
 Input       :  The Indices

                The Object
                testValFwlDosLandAttack
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlDosLandAttack(UINT4 *pu4ErrorCode , INT4 i4TestValFwlDosLandAttack)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFwlDosLandAttack);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2FwlDosLandAttack
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlDosLandAttack(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlDosShortHeaderAttack
 Input       :  The Indices

                The Object
                retValFwlDosShortHeaderAttack
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlDosShortHeaderAttack(INT4 *pi4RetValFwlDosShortHeaderAttack)
{
    UNUSED_PARAM (pi4RetValFwlDosShortHeaderAttack);
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetFwlDosShortHeaderAttack
 Input       :  The Indices

                The Object
                setValFwlDosShortHeaderAttack
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlDosShortHeaderAttack(INT4 i4SetValFwlDosShortHeaderAttack)
{
    UNUSED_PARAM (i4SetValFwlDosShortHeaderAttack);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FwlDosShortHeaderAttack
 Input       :  The Indices

                The Object
                testValFwlDosShortHeaderAttack
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlDosShortHeaderAttack(UINT4 *pu4ErrorCode , INT4 i4TestValFwlDosShortHeaderAttack)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFwlDosShortHeaderAttack);
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhDepv2FwlDosShortHeaderAttack
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlDosShortHeaderAttack(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlSnorkTable
 Input       :  The Indices
                FwlSnorkPortNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFwlSnorkTable(INT4 i4FwlSnorkPortNo)
{
    UNUSED_PARAM (i4FwlSnorkPortNo);
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFirstIndexFwlSnorkTable
 Input       :  The Indices
                FwlSnorkPortNo
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFwlSnorkTable(INT4 *pi4FwlSnorkPortNo)
{
    UNUSED_PARAM (pi4FwlSnorkPortNo);
    return SNMP_FAILURE;

}
/****************************************************************************
 Function    :  nmhGetNextIndexFwlSnorkTable
 Input       :  The Indices
                FwlSnorkPortNo
                nextFwlSnorkPortNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFwlSnorkTable(INT4 i4FwlSnorkPortNo ,INT4 *pi4NextFwlSnorkPortNo )
{
   UNUSED_PARAM (i4FwlSnorkPortNo);
   UNUSED_PARAM (pi4NextFwlSnorkPortNo);
   return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFwlSnorkRowStatus
 Input       :  The Indices
                FwlSnorkPortNo

                The Object
                retValFwlSnorkRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlSnorkRowStatus(INT4 i4FwlSnorkPortNo , INT4 *pi4RetValFwlSnorkRowStatus)
{
    UNUSED_PARAM (i4FwlSnorkPortNo);
    UNUSED_PARAM (pi4RetValFwlSnorkRowStatus);
    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlSnorkRowStatus
 Input       :  The Indices
                FwlSnorkPortNo

                The Object
                setValFwlSnorkRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlSnorkRowStatus(INT4 i4FwlSnorkPortNo , INT4 i4SetValFwlSnorkRowStatus)
{
    UNUSED_PARAM (i4FwlSnorkPortNo);
    UNUSED_PARAM (i4SetValFwlSnorkRowStatus);
    return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlSnorkRowStatus
 Input       :  The Indices
                FwlSnorkPortNo

                The Object
                testValFwlSnorkRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlSnorkRowStatus(UINT4 *pu4ErrorCode , INT4 i4FwlSnorkPortNo , INT4 i4TestValFwlSnorkRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FwlSnorkPortNo);
    UNUSED_PARAM (i4TestValFwlSnorkRowStatus);
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhDepv2FwlSnorkTable
 Input       :  The Indices
                FwlSnorkPortNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlSnorkTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlRateLimitTable
 Input       :  The Indices
                FwlRateLimitPortIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFwlRateLimitTable(INT4 i4FwlRateLimitPortIndex)
{

 UNUSED_PARAM(i4FwlRateLimitPortIndex);
 return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFwlRateLimitTable
 Input       :  The Indices
                FwlRateLimitPortIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFwlRateLimitTable(INT4 *pi4FwlRateLimitPortIndex)
{
      UNUSED_PARAM (pi4FwlRateLimitPortIndex);
      return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFwlRateLimitTable
 Input       :  The Indices
                FwlRateLimitPortIndex
                nextFwlRateLimitPortIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFwlRateLimitTable(INT4 i4FwlRateLimitPortIndex ,INT4 *pi4NextFwlRateLimitPortIndex )
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (pi4NextFwlRateLimitPortIndex);
    return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFwlRateLimitPortNumber
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                retValFwlRateLimitPortNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlRateLimitPortNumber(INT4 i4FwlRateLimitPortIndex , INT4 *pi4RetValFwlRateLimitPortNumber)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (pi4RetValFwlRateLimitPortNumber);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlRateLimitPortType
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                retValFwlRateLimitPortType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlRateLimitPortType(INT4 i4FwlRateLimitPortIndex , INT4 *pi4RetValFwlRateLimitPortType)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (pi4RetValFwlRateLimitPortType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlRateLimitValue
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                retValFwlRateLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlRateLimitValue(INT4 i4FwlRateLimitPortIndex , INT4 *pi4RetValFwlRateLimitValue)
{
     UNUSED_PARAM (i4FwlRateLimitPortIndex);
     UNUSED_PARAM (pi4RetValFwlRateLimitValue);
     return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlRateLimitBurstSize
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                retValFwlRateLimitBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlRateLimitBurstSize(INT4 i4FwlRateLimitPortIndex , INT4 *pi4RetValFwlRateLimitBurstSize)
{
     UNUSED_PARAM (i4FwlRateLimitPortIndex);
     UNUSED_PARAM (pi4RetValFwlRateLimitBurstSize);
     return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlRateLimitTrafficMode
 Input       :  The Indices

                The Object
                retValFwlRateLimitTrafficMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlRateLimitTrafficMode(INT4 i4FwlRateLimitPortIndex,INT4 *pi4RetValFwlRateLimitTrafficMode)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (pi4RetValFwlRateLimitTrafficMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlRateLimitRowStatus
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                retValFwlRateLimitRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlRateLimitRowStatus(INT4 i4FwlRateLimitPortIndex , INT4 *pi4RetValFwlRateLimitRowStatus)
{
     UNUSED_PARAM (i4FwlRateLimitPortIndex);
     UNUSED_PARAM (pi4RetValFwlRateLimitRowStatus);
     return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlRateLimitPortNumber
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                setValFwlRateLimitPortNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlRateLimitPortNumber(INT4 i4FwlRateLimitPortIndex , INT4 i4SetValFwlRateLimitPortNumber)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4SetValFwlRateLimitPortNumber);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlRateLimitPortType
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                setValFwlRateLimitPortType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlRateLimitPortType(INT4 i4FwlRateLimitPortIndex , INT4 i4SetValFwlRateLimitPortType)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4SetValFwlRateLimitPortType);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFwlRateLimitValue
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                setValFwlRateLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlRateLimitValue(INT4 i4FwlRateLimitPortIndex , INT4 i4SetValFwlRateLimitValue)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4SetValFwlRateLimitValue);
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetFwlRateLimitBurstSize
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                setValFwlRateLimitBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlRateLimitBurstSize(INT4 i4FwlRateLimitPortIndex , INT4 i4SetValFwlRateLimitBurstSize)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4SetValFwlRateLimitBurstSize);
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetFwlRateLimitTrafficMode
 Input       :  The Indices

                The Object
                setValFwlRateLimitTrafficMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlRateLimitTrafficMode(INT4 i4FwlRateLimitPortIndex ,INT4 i4SetValFwlRateLimitTrafficMode)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4SetValFwlRateLimitTrafficMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlRateLimitRowStatus
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                setValFwlRateLimitRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlRateLimitRowStatus(INT4 i4FwlRateLimitPortIndex , INT4 i4SetValFwlRateLimitRowStatus)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4SetValFwlRateLimitRowStatus);
    return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FwlRateLimitPortNumber
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                testValFwlRateLimitPortNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlRateLimitPortNumber(UINT4 *pu4ErrorCode , INT4 i4FwlRateLimitPortIndex , INT4 i4TestValFwlRateLimitPortNumber)
{
     UNUSED_PARAM (pu4ErrorCode);
     UNUSED_PARAM (i4FwlRateLimitPortIndex);
     UNUSED_PARAM (i4TestValFwlRateLimitPortNumber);
     return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlRateLimitPortType
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                testValFwlRateLimitPortType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlRateLimitPortType(UINT4 *pu4ErrorCode , INT4 i4FwlRateLimitPortIndex , INT4 i4TestValFwlRateLimitPortType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4TestValFwlRateLimitPortType);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlRateLimitValue
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                testValFwlRateLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlRateLimitValue(UINT4 *pu4ErrorCode , INT4 i4FwlRateLimitPortIndex , INT4 i4TestValFwlRateLimitValue)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4TestValFwlRateLimitValue);
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2FwlRateLimitBurstSize
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                testValFwlRateLimitBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlRateLimitBurstSize(UINT4 *pu4ErrorCode , INT4 i4FwlRateLimitPortIndex , INT4 i4TestValFwlRateLimitBurstSize)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4TestValFwlRateLimitBurstSize);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FwlRateLimitTrafficMode
 Input       :  The Indices

                The Object
                testValFwlRateLimitTrafficMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlRateLimitTrafficMode(UINT4 *pu4ErrorCode ,INT4 i4FwlRateLimitPortIndex ,INT4 i4TestValFwlRateLimitTrafficMode)
{
     UNUSED_PARAM (pu4ErrorCode);
     UNUSED_PARAM (i4FwlRateLimitPortIndex);
     UNUSED_PARAM (i4TestValFwlRateLimitTrafficMode);
     return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlRateLimitRowStatus
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                testValFwlRateLimitRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlRateLimitRowStatus(UINT4 *pu4ErrorCode , INT4 i4FwlRateLimitPortIndex , INT4 i4TestValFwlRateLimitRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4TestValFwlRateLimitRowStatus);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlRateLimitTable
 Input       :  The Indices
                FwlRateLimitPortIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlRateLimitTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFwlRpfTable
 Input       :  The Indices
                FwlRpfInIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFwlRpfTable(INT4 *pi4FwlRpfInIndex)
{
    UNUSED_PARAM(pi4FwlRpfInIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFwlRpfTable
 Input       :  The Indices
                FwlRpfInIndex
                nextFwlRpfInIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFwlRpfTable(INT4 i4FwlRpfInIndex ,INT4 *pi4NextFwlRpfInIndex )
{
    UNUSED_PARAM(i4FwlRpfInIndex);
    UNUSED_PARAM(pi4NextFwlRpfInIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlRpfTable
 Input       :  The Indices
                FwlRpfInIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFwlRpfTable(INT4 i4FwlRpfInIndex)
{
    UNUSED_PARAM(i4FwlRpfInIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlRpfMode
 Input       :  The Indices
                FwlRpfInIndex

                The Object
                setValFwlRpfMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlRpfMode(INT4 i4FwlRpfInIndex , INT4 i4SetValFwlRpfMode)
{
    UNUSED_PARAM(i4FwlRpfInIndex);
    UNUSED_PARAM(i4SetValFwlRpfMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlRpfRowStatus
 Input       :  The Indices
                FwlRpfInIndex

                The Object
                setValFwlRpfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlRpfRowStatus(INT4 i4FwlRpfInIndex , INT4 i4SetValFwlRpfRowStatus)
{
    UNUSED_PARAM(i4FwlRpfInIndex);
    UNUSED_PARAM(i4SetValFwlRpfRowStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FwlRpfMode
 Input       :  The Indices
                FwlRpfInIndex

                The Object
                testValFwlRpfMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlRpfMode(UINT4 *pu4ErrorCode , INT4 i4FwlRpfInIndex , INT4 i4TestValFwlRpfMode)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4FwlRpfInIndex);
    UNUSED_PARAM(i4TestValFwlRpfMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FwlRpfRowStatus
 Input       :  The Indices
                FwlRpfInIndex

                The Object
                testValFwlRpfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlRpfRowStatus(UINT4 *pu4ErrorCode , INT4 i4FwlRpfInIndex , INT4 i4TestValFwlRpfRowStatus)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4FwlRpfInIndex);
    UNUSED_PARAM(i4TestValFwlRpfRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlRpfTable
 Input       :  The Indices
                FwlRpfInIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlRpfTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlRpfMode
 Input       :  The Indices
                FwlRpfInIndex

                The Object
                retValFwlRpfMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlRpfMode(INT4 i4FwlRpfInIndex , INT4 *pi4RetValFwlRpfMode)
{
    UNUSED_PARAM(i4FwlRpfInIndex);
    UNUSED_PARAM(pi4RetValFwlRpfMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlRpfRowStatus
 Input       :  The Indices
                FwlRpfInIndex

                The Object
                retValFwlRpfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlRpfRowStatus(INT4 i4FwlRpfInIndex , INT4 *pi4RetValFwlRpfRowStatus)
{

    UNUSED_PARAM(i4FwlRpfInIndex);
    UNUSED_PARAM(pi4RetValFwlRpfRowStatus);
    return SNMP_SUCCESS;
}

