/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsfwllnxlw.c,v 1.7 2016/03/19 13:06:25 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
/* Low Level GET Routine for All Objects  */
# include  "fwlinc.h"
# include  "fssnmp.h"
#include "fsmpfwlcli.h"
#include "fwlnp.h"
#include "iss.h"
#ifdef FLOWMGR_WANTED
#include "flowmgr.h"
#endif

#ifdef LNXIP4_WANTED
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

tFwlRateLimit gFwlRateLimit[FWL_RATE_LIMIT_MAX_PORT];
extern UINT4 IssGetMgmtPortFromNvRam PROTO ((VOID));
/****************************************************************************
 Function    :  nmhGetFwlGlobalMasterControlSwitch
 Input       :  The Indices

                The Object 
                retValFwlGlobalMasterControlSwitch
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifndef DSECURITY_KERNEL_MAKE_WANTED
INT1 nmhGetFwlGlobalMasterControlSwitch(INT4 *pi4RetValFwlGlobalMasterControlSwitch)
{  
   UNUSED_PARAM(pi4RetValFwlGlobalMasterControlSwitch);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalICMPControlSwitch
 Input       :  The Indices

                The Object 
                retValFwlGlobalICMPControlSwitch
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalICMPControlSwitch(INT4 *pi4RetValFwlGlobalICMPControlSwitch)
{
   UNUSED_PARAM(pi4RetValFwlGlobalICMPControlSwitch);
   return SNMP_SUCCESS;
}
#endif
/****************************************************************************
 Function    :  nmhGetFwlGlobalIpSpoofFiltering
 Input       :  The Indices

                The Object 
                retValFwlGlobalIpSpoofFiltering
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalIpSpoofFiltering(INT4 *pi4RetValFwlGlobalIpSpoofFiltering)
{
    UNUSED_PARAM(pi4RetValFwlGlobalIpSpoofFiltering);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFwlDosAttackAcceptRedirect
 Input       :  The Indices

                The Object
                retValFwlDosAttackAcceptRedirect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlDosAttackAcceptRedirect(INT4 *pi4RetValFwlDosAttackAcceptRedirect)
{
   FWL_DBG (FWL_DBG_ENTRY, "\nDos attack redirect  Switch GET Entry\n");
  
   *pi4RetValFwlDosAttackAcceptRedirect = gFwlAclInfo.i4DosAttackRedirect;
    FWL_DBG (FWL_DBG_ENTRY, "\nDos attack  Switch GET Exit\n"); 
 return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlDosAttackAcceptRedirect
 Input       :  The Indices

                The Object
                setValFwlDosAttackAcceptRedirect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlDosAttackAcceptRedirect(INT4 i4SetValFwlDosAttackAcceptRedirect)
{
#ifdef LNXIP4_WANTED
   CHR1                ac1Command[FWL_DOS_LINE_LEN];

    MEMSET (ac1Command, 0, sizeof (ac1Command));
   
   FWL_DBG (FWL_DBG_ENTRY,
             "\n nmhSetFwlDosAttackAcceptRedirect  Filtering Control Switch SET Entry \n");
    
    if (i4SetValFwlDosAttackAcceptRedirect == FWL_ONE)
    {
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), 
		"echo %d > %s",FWL_ZERO,FWL_PROC_REDIRECT);
    }
    else
    {
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), 
		"echo %d > %s",FWL_ONE,FWL_PROC_REDIRECT);
    }
    system (ac1Command);
    FWL_DBG (FWL_DBG_EXIT, "\nnmhSetFwlDosAttackAcceptRedirect control Switch SET Exit \n");
#endif 

    gFwlAclInfo.i4DosAttackRedirect = i4SetValFwlDosAttackAcceptRedirect;

    return SNMP_SUCCESS;
   
}

/****************************************************************************
 Function    :  nmhGetFwlDosAttackAcceptSmurfAttack
 Input       :  The Indices

                The Object
                retValFwlDosAttackAcceptSmurfAttack
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlDosAttackAcceptSmurfAttack(INT4 *pi4RetValFwlDosAttackAcceptSmurfAttack)
{
   FWL_DBG (FWL_DBG_ENTRY, "\nDos attack redirect  Switch GET Entry\n");
  
   *pi4RetValFwlDosAttackAcceptSmurfAttack = gFwlAclInfo.i4SmurfAttack;
    FWL_DBG (FWL_DBG_ENTRY, "\nDos attack  Switch GET Exit\n"); 
 return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlDosAttackAcceptSmurfAttack
 Input       :  The Indices

                The Object
                setValFwlDosAttackAcceptSmurfAttack
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlDosAttackAcceptSmurfAttack(INT4 i4SetValFwlDosAttackAcceptSmurfAttack)
{
#ifdef LNXIP4_WANTED
   CHR1                ac1Command[FWL_DOS_LINE_LEN];

    MEMSET (ac1Command, 0, sizeof (ac1Command));
   
   FWL_DBG (FWL_DBG_ENTRY,
             "\n nmhSetFwlDosAttackAcceptSmurfAttack  Filtering Control Switch SET Entry \n");
    
    
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), 
		"echo %d > %s",
              (INT2)i4SetValFwlDosAttackAcceptSmurfAttack,FWL_PROC_SMURF);
    system (ac1Command);
    FWL_DBG (FWL_DBG_EXIT, "\nnmhSetFwlDosAttackAcceptSmurfAttack control Switch SET Exit \n");
#endif 

    gFwlAclInfo.i4SmurfAttack = i4SetValFwlDosAttackAcceptSmurfAttack;

    return SNMP_SUCCESS;
   

}

/****************************************************************************
 Function    :  nmhTestv2FwlDosAttackAcceptSmurfAttack
 Input       :  The Indices

                The Object
                testValFwlDosAttackAcceptSmurfAttack
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlDosAttackAcceptSmurfAttack(UINT4 *pu4ErrorCode , INT4 i4TestValFwlDosAttackAcceptSmurfAttack)
{
 INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n nmhTestv2FwlDosAttackAcceptSmurfAttack  Control Switch TEST Entry\n");

    /* Validate the value of IP Spoof Filtering Control switch */
    if ((i4TestValFwlDosAttackAcceptSmurfAttack == FWL_ONE)
        || (i4TestValFwlDosAttackAcceptSmurfAttack == FWL_ZERO))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Ip Spoof Filtering Control Switch Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\nnmhTestv2FwlDosAttackAcceptSmurfAttack Switch TEST Exit\n");

    return i1Status;

}

/****************************************************************************
 Function    :  nmhDepv2FwlDosAttackAcceptSmurfAttack
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlDosAttackAcceptSmurfAttack(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}


/****************************************************************************
 Function    :  nmhTestv2FwlDosAttackAcceptRedirect
 Input       :  The Indices

                The Object
                testValFwlDosAttackAcceptRedirect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlDosAttackAcceptRedirect(UINT4 *pu4ErrorCode , INT4 i4TestValFwlDosAttackAcceptRedirect)
{
    INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Ip Spoof Filtering Control Switch TEST Entry\n");

    /* Validate the value of IP Spoof Filtering Control switch */
    if ((i4TestValFwlDosAttackAcceptRedirect == FWL_ZERO)
        || (i4TestValFwlDosAttackAcceptRedirect == FWL_ONE))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Ip Spoof Filtering Control Switch Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\nIp Spoof Filtering Control Switch TEST Exit\n");

    return i1Status;

}

/****************************************************************************
 Function    :  nmhDepv2FwlDosAttackAcceptRedirect
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlDosAttackAcceptRedirect(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlDosLandAttack
 Input       :  The Indices

                The Object
                retValFwlDosLandAttack
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlDosLandAttack(INT4 *pi4RetValFwlDosLandAttack)
{
    FWL_DBG (FWL_DBG_ENTRY, "\nDos LAND attack redirect  Switch GET Entry\n");

   *pi4RetValFwlDosLandAttack = gFwlAclInfo.i4LandAttack;
    FWL_DBG (FWL_DBG_ENTRY, "\nDos LAND attack  Switch GET Exit\n");
 return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetFwlDosLandAttack
 Input       :  The Indices

                The Object
                setValFwlDosLandAttack
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlDosLandAttack(INT4 i4SetValFwlDosLandAttack)
{
#ifdef LNXIP4_WANTED
  
   FWL_DBG (FWL_DBG_ENTRY,
             "\n nmhSetFwlDosLandAttack  Filtering Control Switch SET Entry \n");
   CHR1                ac1Command[FWL_DOS_LINE_LEN];
   struct ifreq ifr;
   struct sockaddr_in* ipaddr;
   INT4 i4Fd = -1;
   UINT1               au1Buffer[MAX_COLUMN_LENGTH];
   CHR1               *pu1Addr = NULL;
   UINT1               au1IpAddr[FWL_DOS_LINE_LEN];
   UINT4       u4IpAddr = 0;
   pu1Addr = (CHR1 *) au1IpAddr;
 
   MEMSET (au1Buffer, 0, MAX_COLUMN_LENGTH);
   MEMSET (au1IpAddr, 0, sizeof (au1IpAddr)); 
   STRNCPY (&au1Buffer,
             IssGetInterfaceFromNvRam (), STRLEN (IssGetInterfaceFromNvRam ()));
    
 
   size_t  if_name_len = STRLEN(au1Buffer);
      
	
   i4Fd	=	socket(AF_INET,SOCK_DGRAM,0);

   if (if_name_len<sizeof(ifr.ifr_name))
   {
	   MEMCPY(ifr.ifr_name,au1Buffer,if_name_len);
	   ifr.ifr_name[if_name_len]=0;
   }
   if (i4Fd==-1)
   {
	   perror ("ioctl call failed due to !!!\n");
       return SNMP_FAILURE;
   }

   if (ioctl(i4Fd,SIOCGIFADDR,&ifr)==-1)
   {
	   close(i4Fd);
   }
   else
   {
	   close(i4Fd);
   }
   ipaddr = (struct sockaddr_in*) (VOID *) &ifr.ifr_addr;
   u4IpAddr = OSIX_NTOHL (ipaddr->sin_addr.s_addr);
   CLI_CONVERT_IPADDR_TO_STR (pu1Addr,u4IpAddr);


   if(i4SetValFwlDosLandAttack == FWL_ONE)
   { 
   MEMSET (ac1Command, 0, sizeof (ac1Command));
   SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
		   "%s INPUT -s %s %s",FWL_IPTBL_ADD,pu1Addr,FWL_IPTBL_DROP);
   system (ac1Command);
   }
   else
   {
   MEMSET (ac1Command, 0, sizeof (ac1Command));
   SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
		   "%s INPUT -s %s %s",FWL_IPTBL_DEL,pu1Addr,FWL_IPTBL_DROP);
   system (ac1Command);

   }
   FWL_DBG (FWL_DBG_EXIT, "\nnmhSetFwlDosLandAttack control Switch SET Exit \n");
#endif
#ifdef NPAPI_WANTED
      if(FwlNpEnableDosAttackCust(FWL_DOS_LAND_ATTACK,i4SetValFwlDosLandAttack)!= SNMP_SUCCESS)
      {
         return (SNMP_FAILURE);

      }
#endif
    gFwlAclInfo.i4LandAttack = i4SetValFwlDosLandAttack;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2FwlDosLandAttack
 Input       :  The Indices

                The Object
                testValFwlDosLandAttack
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlDosLandAttack(UINT4 *pu4ErrorCode , INT4 i4TestValFwlDosLandAttack)
{
   INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n nmhTestv2FwlDosLandAttack Control Switch TEST Entry\n");
    /* Validate the value of IP Spoof Filtering Control switch */
    if ((i4TestValFwlDosLandAttack == FWL_ZERO)
        || (i4TestValFwlDosLandAttack == FWL_ONE))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n nmhTestv2FwlDosLandAttack Control Switch Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\nnmhTestv2FwlDosLandAttack Control Switch TEST Exit\n");

    return i1Status;

}

/****************************************************************************
 Function    :  nmhDepv2FwlDosLandAttack
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlDosLandAttack(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}



/****************************************************************************
 Function    :  nmhGetFwlDosShortHeaderAttack
 Input       :  The Indices

                The Object
                retValFwlDosShortHeaderAttack
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlDosShortHeaderAttack(INT4 *pi4RetValFwlDosShortHeaderAttack)
{
  FWL_DBG (FWL_DBG_ENTRY, "\nDos short header attack Switch GET Entry\n");

   *pi4RetValFwlDosShortHeaderAttack = gFwlAclInfo.i4ShortHeaderlength;
    FWL_DBG (FWL_DBG_ENTRY, "\nDos short header attack  Switch GET Exit\n");
 return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetFwlDosShortHeaderAttack
 Input       :  The Indices

                The Object
                setValFwlDosShortHeaderAttack
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlDosShortHeaderAttack(INT4 i4SetValFwlDosShortHeaderAttack)
{
#ifdef LNXIP4_WANTED
   CHR1                ac1Command[FWL_DOS_LINE_LEN];

    MEMSET (ac1Command, 0, sizeof (ac1Command));
   
   FWL_DBG (FWL_DBG_ENTRY,
             "\n nmhSetFwlDosShortHeaderAttack  Filtering Control Switch SET Entry \n");

    if(i4SetValFwlDosShortHeaderAttack != FWL_ZERO)
    {
       gFwlAclInfo.i4ShortHeaderlength = i4SetValFwlDosShortHeaderAttack;
       SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                        "%s INPUT -p tcp -m length --length %d",
              FWL_IPTBL_ADD,(INT4)i4SetValFwlDosShortHeaderAttack);
       system (ac1Command);
       /* short header installation for UDP*/
       MEMSET (ac1Command, 0, sizeof (ac1Command));
       SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                        "%s INPUT -p udp -m length --length %d",
              FWL_IPTBL_ADD,(INT4)i4SetValFwlDosShortHeaderAttack);
      system (ac1Command);
    }
    else
    {
       i4SetValFwlDosShortHeaderAttack = gFwlAclInfo.i4ShortHeaderlength;
       SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                        "%s INPUT -p tcp -m length --length %d",
              FWL_IPTBL_DEL,(INT4)i4SetValFwlDosShortHeaderAttack);
       system (ac1Command);
       MEMSET (ac1Command, 0, sizeof (ac1Command));
       SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                        "%s INPUT -p udp -m length --length %d",
              FWL_IPTBL_DEL,(INT4)i4SetValFwlDosShortHeaderAttack);
       system (ac1Command);
       gFwlAclInfo.i4ShortHeaderlength = FWL_ZERO;
    }
    FWL_DBG (FWL_DBG_EXIT, "\nnmhSetFwlDosShortHeaderAttack control Switch SET Exit \n");
#endif 

    return SNMP_SUCCESS;
 
}
/****************************************************************************
 Function    :  nmhTestv2FwlDosShortHeaderAttack
 Input       :  The Indices

                The Object
                testValFwlDosShortHeaderAttack
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlDosShortHeaderAttack(UINT4 *pu4ErrorCode , INT4 i4TestValFwlDosShortHeaderAttack)
{
    INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n nmhTestv2FwlDosShortHeaderAttack Control Switch TEST Entry\n");

    /* Validate the value of IP Spoof Filtering Control switch */
    if ((i4TestValFwlDosShortHeaderAttack >= FWL_ZERO)
        && (i4TestValFwlDosShortHeaderAttack < 1000))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n nmhTestv2FwlDosShortHeaderAttack Control Switch Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\nnmhTestv2FwlDosShortHeaderAttack Control Switch TEST Exit\n");

    return i1Status;

}
/****************************************************************************
 Function    :  nmhDepv2FwlDosShortHeaderAttack
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlDosShortHeaderAttack(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FwlRateLimitTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlRateLimitTable
 Input       :  The Indices
                FwlRateLimitPortIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFwlRateLimitTable(INT4 i4FwlRateLimitPortIndex)
{
 
 UNUSED_PARAM(i4FwlRateLimitPortIndex);
 return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFwlRateLimitTable
 Input       :  The Indices
                FwlRateLimitPortIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFwlRateLimitTable(INT4 *pi4FwlRateLimitPortIndex)
{

INT4 i4Count  = FWL_ZERO;

    for(i4Count =0 ; i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
	{
	  if((gFwlRateLimit[i4Count].i4PortNo != FWL_ZERO)
		  && (gFwlRateLimit[i4Count].i4RowStatus == FWL_ACTIVE))
	  {
		 *pi4FwlRateLimitPortIndex = gFwlRateLimit[i4Count].i4PortNo;
         return SNMP_SUCCESS;
	  } 
	}
      return (SNMP_FAILURE);
}
/****************************************************************************
 Function    :  nmhGetNextIndexFwlRateLimitTable
 Input       :  The Indices
                FwlRateLimitPortIndex
                nextFwlRateLimitPortIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFwlRateLimitTable(INT4 i4FwlRateLimitPortIndex ,INT4 *pi4NextFwlRateLimitPortIndex )
{


   INT4 i4CheckCount  = FWL_ZERO;
   INT4 i4Count  = FWL_ZERO;

    for(i4Count = 0  ; i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
	{
	  if(gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
	  {
		  i4CheckCount = i4Count;
          break;
	  } 
	}
    for(i4Count = i4CheckCount + 1  ; i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
	{
	  if((gFwlRateLimit[i4Count].i4PortNo != FWL_ZERO)
		  && (gFwlRateLimit[i4Count].i4RowStatus == FWL_ACTIVE))
	  {
		 *pi4NextFwlRateLimitPortIndex = gFwlRateLimit[i4Count].i4PortNo;
         return SNMP_SUCCESS;
	  } 
	}
      return (SNMP_FAILURE);



 return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlRateLimitPortNumber
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                retValFwlRateLimitPortNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlRateLimitPortNumber(INT4 i4FwlRateLimitPortIndex , INT4 *pi4RetValFwlRateLimitPortNumber)
{
   INT4 i4Count = FWL_ZERO;
    for(i4Count =0 ; i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
	{
	  if(gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
	  {
          *pi4RetValFwlRateLimitPortNumber = gFwlRateLimit[i4Count].i4PortNo;
	    	break;
	  } 
	}
 return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlRateLimitPortType
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                retValFwlRateLimitPortType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlRateLimitPortType(INT4 i4FwlRateLimitPortIndex , INT4 *pi4RetValFwlRateLimitPortType)
{
   INT4 i4Count = FWL_ZERO;
    for(i4Count =0 ; i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
	{
	  if(gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
	  {
          *pi4RetValFwlRateLimitPortType = gFwlRateLimit[i4Count].i4ProtoType;
	    	break;
	  } 
	}
 return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlRateLimitValue
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                retValFwlRateLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlRateLimitValue(INT4 i4FwlRateLimitPortIndex , INT4 *pi4RetValFwlRateLimitValue)
{
   INT4 i4Count = FWL_ZERO;
    for(i4Count =0 ; i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
	{
	  if(gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
	  {
          *pi4RetValFwlRateLimitValue = gFwlRateLimit[i4Count].i4RateLimit;
	    	break;
	  } 
	}
 return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlRateLimitBurstSize
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                retValFwlRateLimitBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlRateLimitBurstSize(INT4 i4FwlRateLimitPortIndex , INT4 *pi4RetValFwlRateLimitBurstSize)
{
   INT4 i4Count = FWL_ZERO;
    for(i4Count =0 ; i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
	{
	  if(gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
	  {
          *pi4RetValFwlRateLimitBurstSize = gFwlRateLimit[i4Count].i4BurstSize;
	    	break;
	  } 
	}
 return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlRateLimitTrafficMode
 Input       :  The Indices

                The Object
                retValFwlRateLimitTrafficMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlRateLimitTrafficMode(INT4 i4FwlRateLimitPortIndex,INT4 *pi4RetValFwlRateLimitTrafficMode)
{
   INT4 i4Count = FWL_ZERO;
    for(i4Count =0 ; i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
    {
      if(gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
      {
          *pi4RetValFwlRateLimitTrafficMode = gFwlRateLimit[i4Count].i4TrafficMode;
            break;
      }
    }
 return SNMP_SUCCESS;


}

/****************************************************************************
 Function    :  nmhGetFwlRateLimitRowStatus
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                retValFwlRateLimitRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlRateLimitRowStatus(INT4 i4FwlRateLimitPortIndex , INT4 *pi4RetValFwlRateLimitRowStatus)
{
   INT4 i4Count = FWL_ZERO;
    for(i4Count =0 ; i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
	{
	  if(gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
	  {
          *pi4RetValFwlRateLimitRowStatus = gFwlRateLimit[i4Count].i4RowStatus;
	    	break;
	  } 
	}
 return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlRateLimitPortNumber
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                setValFwlRateLimitPortNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlRateLimitPortNumber(INT4 i4FwlRateLimitPortIndex , INT4 i4SetValFwlRateLimitPortNumber)
{
    INT4         i4Count= FWL_ZERO; 
    UNUSED_PARAM(i4FwlRateLimitPortIndex);


    for(i4Count =0 ; i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
	{
	  if(gFwlRateLimit[i4Count].i4PortNo == FWL_ZERO)
	  {
	       gFwlRateLimit[i4Count].i4PortNo = i4SetValFwlRateLimitPortNumber;
	    	break;
	  } 
	}
    if(i4Count == FWL_RATE_LIMIT_MAX_PORT)
	{
      return (SNMP_FAILURE);
	}
    return (SNMP_SUCCESS);
}
/****************************************************************************
 Function    :  nmhSetFwlRateLimitPortType
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                setValFwlRateLimitPortType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlRateLimitPortType(INT4 i4FwlRateLimitPortIndex , INT4 i4SetValFwlRateLimitPortType)
{
  INT4         i4Count= FWL_ZERO; 



    for(i4Count =0 ; i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
	{
	  if(gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
	  {
	       gFwlRateLimit[i4Count].i4ProtoType = i4SetValFwlRateLimitPortType;
	    	break;
	  } 
	}
    if(i4Count == FWL_RATE_LIMIT_MAX_PORT)
	{
      return (SNMP_FAILURE);
	}
    return (SNMP_SUCCESS);

}
/****************************************************************************
 Function    :  nmhSetFwlRateLimitValue
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                setValFwlRateLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlRateLimitValue(INT4 i4FwlRateLimitPortIndex , INT4 i4SetValFwlRateLimitValue)
{
 INT4         i4Count= FWL_ZERO; 



    for(i4Count =0 ; i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
	{
	  if(gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
	  {
	       gFwlRateLimit[i4Count].i4RateLimit = i4SetValFwlRateLimitValue;
	    	break;
	  } 
	}
    if(i4Count == FWL_RATE_LIMIT_MAX_PORT)
	{
      return (SNMP_FAILURE);
	}
    return (SNMP_SUCCESS);

}
/****************************************************************************
 Function    :  nmhSetFwlRateLimitBurstSize
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                setValFwlRateLimitBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlRateLimitBurstSize(INT4 i4FwlRateLimitPortIndex , INT4 i4SetValFwlRateLimitBurstSize)
{
 INT4         i4Count= FWL_ZERO; 



    for(i4Count =0 ; i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
	{
	  if(gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
	  {
	       gFwlRateLimit[i4Count].i4BurstSize = i4SetValFwlRateLimitBurstSize;
	    	break;
	  } 
	}
    if(i4Count == FWL_RATE_LIMIT_MAX_PORT)
	{
      return (SNMP_FAILURE);
	}
    return (SNMP_SUCCESS);

}
/****************************************************************************
 Function    :  nmhSetFwlRateLimitTrafficMode
 Input       :  The Indices

                The Object
                setValFwlRateLimitTrafficMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlRateLimitTrafficMode(INT4 i4FwlRateLimitPortIndex ,INT4 i4SetValFwlRateLimitTrafficMode)
{
 
 INT4         i4Count= FWL_ZERO; 
   for(i4Count =0 ; i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
    {
      if(gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
      {
           gFwlRateLimit[i4Count].i4TrafficMode = i4SetValFwlRateLimitTrafficMode;
            break;
      }
    }
    if(i4Count == FWL_RATE_LIMIT_MAX_PORT)
    {
      return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFwlRateLimitRowStatus
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                setValFwlRateLimitRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlRateLimitRowStatus(INT4 i4FwlRateLimitPortIndex , INT4 i4SetValFwlRateLimitRowStatus)
{
  INT1                i1Status = FWL_ZERO;
  INT4                i4Count = FWL_ZERO;
  INT4                i4RateLimit = FWL_ZERO;
  INT4                i4BurstSize = FWL_ZERO;
  CHR1                ac1Command[FWL_DOS_LINE_LEN];





   for(i4Count = 0 ; i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
   {
		  if((gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
			 && (gFwlRateLimit[i4Count].i4RowStatus == i4SetValFwlRateLimitRowStatus))
		  {
				return (SNMP_SUCCESS);
		  } 
   }
 
    FWL_DBG (FWL_DBG_ENTRY, "\n Interface Row Status SET Entry \n");

    /* create the interface node by setting the row status. If interface node
     * is already created then the Row Status is ACTIVE. if the interface node
     * doesn exist then it should be created by setting the RowStatus CREATE_
     * AND_GO.
     */
    switch (i4SetValFwlRateLimitRowStatus)
    {
        case FWL_ACTIVE:
                   for(i4Count =0 ; i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
				   {
					  if(gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
					  {
				        gFwlRateLimit[i4Count].i4RowStatus = FWL_ACTIVE;
						break;
					  } 
				   }
	 	           if(i4Count == FWL_RATE_LIMIT_MAX_PORT)
				   {
				   i1Status = SNMP_FAILURE;
				   }
                   if(gFwlRateLimit[i4Count].i4TrafficMode == FWL_RATE_LIMIT_PPS)
                   {
                       i4RateLimit = gFwlRateLimit[i4Count].i4RateLimit;
                       i4BurstSize = gFwlRateLimit[i4Count].i4BurstSize;

                   }
                   else if (gFwlRateLimit[i4Count].i4TrafficMode == FWL_RATE_LIMIT_KBPS)
                   {
                       i4RateLimit = gFwlRateLimit[i4Count].i4RateLimit/MAX_PKT_SIZE;
                       i4BurstSize = gFwlRateLimit[i4Count].i4BurstSize/MAX_PKT_SIZE;
                   }
                   else
                   {
                       i4RateLimit = gFwlRateLimit[i4Count].i4RateLimit/MAX_PKT_SIZE ;
                       i4BurstSize = gFwlRateLimit[i4Count].i4BurstSize/MAX_PKT_SIZE;
                       i4RateLimit = i4RateLimit/1024;
                       i4BurstSize = i4BurstSize/1024;
                   }

                   if(gFwlRateLimit[i4Count].i4ProtoType == FWL_RATE_LIMIT_TCP)
				   {
                    MEMSET (ac1Command, 0, sizeof (ac1Command));
                    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                     "%s INPUT -p tcp --tcp-flags SYN SYN -m  limit --limit %d/minute --limit-burst %d %s",
                    FWL_IPTBL_ADD,i4RateLimit,i4BurstSize,FWL_IPTBL_DROP);
                    system (ac1Command);
				
				   }
				   else if(gFwlRateLimit[i4Count].i4ProtoType == FWL_RATE_LIMIT_UDP)
	               {
                    MEMSET (ac1Command, 0, sizeof (ac1Command));
                     SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                     "%s INPUT -p udp --sport %d -m limit --limit %d/minute --limit-burst %d %s",
                    FWL_IPTBL_ADD,gFwlRateLimit[i4Count].i4PortNo,i4RateLimit,i4BurstSize,FWL_IPTBL_DROP);
                    system (ac1Command);
				
				   }
				   else
				   {
                    MEMSET (ac1Command, 0, sizeof (ac1Command));
                     SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                     "%s INPUT -p icmp --icmp-type echo-reply -m limit --limit %d/minute --limit-burst %d %s",
                    FWL_IPTBL_ADD,i4RateLimit,i4BurstSize,FWL_IPTBL_DROP);
                    system (ac1Command);
                   }
#ifdef NPAPI_WANTED
			           
				   i1Status = FwlNpRateLimitEnable(&gFwlRateLimit[i4Count]);
#endif	
			 break;
			  
        case FWL_CREATE_AND_WAIT:
        case FWL_CREATE_AND_GO:
        case FWL_NOT_IN_SERVICE:
                   for(i4Count =0 ;i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
				   {
					  if(gFwlRateLimit[i4Count].i4PortNo == FWL_ZERO)
					  {
				        gFwlRateLimit[i4Count].i4RowStatus = FWL_CREATE_AND_WAIT;
						break;
					  } 
				   }
	 	           if(i4Count == FWL_RATE_LIMIT_MAX_PORT)
				   {
				   i1Status = SNMP_FAILURE;
				   }
				   i1Status = SNMP_SUCCESS;
			 break;
        case FWL_DESTROY:
                   for(i4Count =0 ; i4Count< FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
				   {
					  if(gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
					  {
						
						break;
					  } 
				   }
	 	           if(i4Count == FWL_RATE_LIMIT_MAX_PORT)
				   {
				      return (SNMP_FAILURE);
				   }
                   if(gFwlRateLimit[i4Count].i4TrafficMode == FWL_RATE_LIMIT_PPS)
                   {
                       i4RateLimit = gFwlRateLimit[i4Count].i4RateLimit;
                       i4BurstSize = gFwlRateLimit[i4Count].i4BurstSize;

                   }
                   else if (gFwlRateLimit[i4Count].i4TrafficMode == FWL_RATE_LIMIT_KBPS)
                   {
                       i4RateLimit = gFwlRateLimit[i4Count].i4RateLimit/MAX_PKT_SIZE;
                       i4BurstSize = gFwlRateLimit[i4Count].i4BurstSize/MAX_PKT_SIZE;
                   }
                   else
                   {
                       i4RateLimit = gFwlRateLimit[i4Count].i4RateLimit/MAX_PKT_SIZE ;
                       i4BurstSize = gFwlRateLimit[i4Count].i4BurstSize/MAX_PKT_SIZE;
                       i4RateLimit = i4RateLimit/1024;
                       i4BurstSize = i4BurstSize/1024;
                   }

                   if(gFwlRateLimit[i4Count].i4ProtoType == FWL_RATE_LIMIT_TCP)
				   {
                    MEMSET (ac1Command, 0, sizeof (ac1Command));
                     SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                     "%s INPUT -p tcp --tcp-flags SYN SYN -m  limit --limit %d/minute --limit-burst %d %s",
                    FWL_IPTBL_DEL,i4RateLimit,i4BurstSize,FWL_IPTBL_DROP);
                    system (ac1Command);
				
				   }
				   else if(gFwlRateLimit[i4Count].i4ProtoType == FWL_RATE_LIMIT_UDP)
	               {
                    MEMSET (ac1Command, 0, sizeof (ac1Command));
                     SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                     "%s INPUT -p udp --sport %d -m limit --limit %d/minute --limit-burst %d %s",
                    FWL_IPTBL_DEL,gFwlRateLimit[i4Count].i4PortNo,i4RateLimit,i4BurstSize,FWL_IPTBL_DROP);
                    system (ac1Command);
				
				   }
				   else
				   {
                    MEMSET (ac1Command, 0, sizeof (ac1Command));
                     SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                     "%s INPUT -p icmp --icmp-type echo-reply -m limit --limit %d/minute --limit-burst %d %s",
                    FWL_IPTBL_DEL,i4RateLimit,i4BurstSize,FWL_IPTBL_DROP);
                    system (ac1Command);
                   }
#ifdef NPAPI_WANTED
				   i1Status = FwlNpRateLimitDisable(&gFwlRateLimit[i4Count]);
#endif 
				   gFwlRateLimit[i4Count].i4RowStatus = FWL_NOT_IN_SERVICE;
				   gFwlRateLimit[i4Count].i4PortNo = FWL_ZERO;
               
				   i1Status = SNMP_SUCCESS;
             break;
        default:
            break;
    }

 return i1Status;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlRateLimitPortNumber
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                testValFwlRateLimitPortNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlRateLimitPortNumber(UINT4 *pu4ErrorCode , INT4 i4FwlRateLimitPortIndex , INT4 i4TestValFwlRateLimitPortNumber)
{
UNUSED_PARAM (i4FwlRateLimitPortIndex); 
     if ((i4TestValFwlRateLimitPortNumber < 0) ||
      (i4TestValFwlRateLimitPortNumber > FWL_RATE_LIMIT_MAX_PORT))
      {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
         return(SNMP_FAILURE);
    
      } 
 return(SNMP_SUCCESS);


	
}
/****************************************************************************
 Function    :  nmhTestv2FwlRateLimitPortType
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                testValFwlRateLimitPortType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlRateLimitPortType(UINT4 *pu4ErrorCode , INT4 i4FwlRateLimitPortIndex , INT4 i4TestValFwlRateLimitPortType)
{
 INT4                i4Count = FWL_ZERO;
	for(i4Count =0 ;i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
    {
          if(gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
          {
              break;
          }
     }
	 if(i4Count == FWL_RATE_LIMIT_MAX_PORT)
	 {	
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
         return(SNMP_FAILURE);
	 }
      if ((i4TestValFwlRateLimitPortType == FWL_RATE_LIMIT_TCP) ||
				(i4TestValFwlRateLimitPortType == FWL_RATE_LIMIT_UDP)
			|| (i4TestValFwlRateLimitPortType == FWL_RATE_LIMIT_ICMP))
      {
              return(SNMP_SUCCESS);
            
      }
	  else
      { 
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
         return(SNMP_FAILURE);
	  }

}
/****************************************************************************
 Function    :  nmhTestv2FwlRateLimitValue
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                testValFwlRateLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlRateLimitValue(UINT4 *pu4ErrorCode , INT4 i4FwlRateLimitPortIndex , INT4 i4TestValFwlRateLimitValue)
{

 INT4                i4Count = FWL_ZERO;
 INT4                i4FwlRateLimitTrafficMode = 0;
	for(i4Count =0 ;i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
    {
          if(gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
          {
              break;
          }
     }
	 if(i4Count == FWL_RATE_LIMIT_MAX_PORT)
	 {	
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
         return(SNMP_FAILURE);
	 }
      if ((i4FwlRateLimitTrafficMode == FWL_RATE_LIMIT_PPS) &&
          ((i4TestValFwlRateLimitValue >= FWL_MIN_RATE_LIMIT_PPS) ||
          (i4TestValFwlRateLimitValue <= FWL_MAX_RATE_LIMIT_PPS)))
      {
           return(SNMP_SUCCESS);
      }
      else if((i4FwlRateLimitTrafficMode == FWL_RATE_LIMIT_KBPS) &&
             ((i4TestValFwlRateLimitValue >= FWL_MIN_RATE_LIMIT_KBPS) ||
              (i4TestValFwlRateLimitValue <= FWL_MAX_RATE_LIMIT_KBPS)))
      {
          return(SNMP_SUCCESS);
      }
      else if((i4FwlRateLimitTrafficMode == FWL_RATE_LIMIT_BPS) &&
             ((i4TestValFwlRateLimitValue >= FWL_MIN_RATE_LIMIT_BPS) ||
              (i4TestValFwlRateLimitValue <= FWL_MAX_RATE_LIMIT_BPS)))
      {
          return(SNMP_SUCCESS);
      }
     else         
      {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
         return(SNMP_FAILURE);
    
      } 
 return(SNMP_SUCCESS);

}
/****************************************************************************
 Function    :  nmhTestv2FwlRateLimitBurstSize
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                testValFwlRateLimitBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlRateLimitBurstSize(UINT4 *pu4ErrorCode , INT4 i4FwlRateLimitPortIndex , INT4 i4TestValFwlRateLimitBurstSize)
{

 INT4                i4Count = FWL_ZERO;

	for(i4Count =0 ;i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
    {
          if(gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
          {
              break;
          }
     }
	 if(i4Count == FWL_RATE_LIMIT_MAX_PORT)
	 {	
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
         return(SNMP_FAILURE);
	 }
     if ((i4TestValFwlRateLimitBurstSize < 0) ||
      (i4TestValFwlRateLimitBurstSize > 80000000))
      {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
         return(SNMP_FAILURE);
    
      } 
 return(SNMP_SUCCESS);


}

/****************************************************************************
 Function    :  nmhTestv2FwlRateLimitTrafficMode
 Input       :  The Indices

                The Object
                testValFwlRateLimitTrafficMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlRateLimitTrafficMode(UINT4 *pu4ErrorCode ,INT4 i4FwlRateLimitPortIndex ,INT4 i4TestValFwlRateLimitTrafficMode)
{
    INT4                i4Count = FWL_ZERO;
    for(i4Count =0 ;i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
    {
          if(gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
          {
              break;
          }
     }
     if(i4Count == FWL_RATE_LIMIT_MAX_PORT)
     {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
         return(SNMP_FAILURE);
     }
      if ((i4TestValFwlRateLimitTrafficMode == FWL_RATE_LIMIT_PPS) ||
                (i4TestValFwlRateLimitTrafficMode == FWL_RATE_LIMIT_KBPS)
            || (i4TestValFwlRateLimitTrafficMode == FWL_RATE_LIMIT_BPS))
      {
              return(SNMP_SUCCESS);

      }
      else
      {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
         return(SNMP_FAILURE);
      }


}

/****************************************************************************
 Function    :  nmhTestv2FwlRateLimitRowStatus
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                testValFwlRateLimitRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlRateLimitRowStatus(UINT4 *pu4ErrorCode , INT4 i4FwlRateLimitPortIndex , INT4 i4TestValFwlRateLimitRowStatus)
{
 INT1                i1Status = (INT1) SNMP_SUCCESS;
 INT4                i4Count = FWL_ZERO;

    switch (i4TestValFwlRateLimitRowStatus)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
        {		
		          for(i4Count =0 ;i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
                  {
                      if(gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
                      {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        i1Status = SNMP_FAILURE;
                        return (SNMP_FAILURE);
                      }
                   }
                   i1Status = SNMP_SUCCESS;
                        break;
	          
        }

        case NOT_IN_SERVICE:
        case NOT_READY:
        case DESTROY:
        case ACTIVE:
        {
			      for(i4Count =0 ;i4Count < FWL_RATE_LIMIT_MAX_PORT ;i4Count++)
                  {
                      if(gFwlRateLimit[i4Count].i4PortNo == i4FwlRateLimitPortIndex)
                      {
                        i1Status = SNMP_SUCCESS;
                        break;
                      }
                   }
				   if(i4Count == FWL_RATE_LIMIT_MAX_PORT)
				   {
						*pu4ErrorCode = SNMP_ERR_NO_CREATION;
                        i1Status = SNMP_FAILURE;
						
				   }
                   break;	
        }
        default:
        {
            i1Status  = (INT1) SNMP_FAILURE;
            break;
        }
    }
    return i1Status;
 

}
/****************************************************************************
 Function    :  nmhDepv2FwlRateLimitTable
 Input       :  The Indices
                FwlRateLimitPortIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlRateLimitTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalSrcRouteFiltering
 Input       :  The Indices

                The Object 
                retValFwlGlobalSrcRouteFiltering
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalSrcRouteFiltering(INT4 *pi4RetValFwlGlobalSrcRouteFiltering)
{
   UNUSED_PARAM(pi4RetValFwlGlobalSrcRouteFiltering);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalTinyFragmentFiltering
 Input       :  The Indices

                The Object 
                retValFwlGlobalTinyFragmentFiltering
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalTinyFragmentFiltering(INT4 *pi4RetValFwlGlobalTinyFragmentFiltering)
{
   UNUSED_PARAM(pi4RetValFwlGlobalTinyFragmentFiltering);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalTcpIntercept
 Input       :  The Indices

                The Object 
                retValFwlGlobalTcpIntercept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalTcpIntercept(INT4 *pi4RetValFwlGlobalTcpIntercept)
{
   UNUSED_PARAM(pi4RetValFwlGlobalTcpIntercept);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalTrap
 Input       :  The Indices

                The Object 
                retValFwlGlobalTrap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalTrap(INT4 *pi4RetValFwlGlobalTrap)
{
   UNUSED_PARAM(pi4RetValFwlGlobalTrap);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalTrace
 Input       :  The Indices

                The Object 
                retValFwlGlobalTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalTrace(INT4 *pi4RetValFwlGlobalTrace)
{
   UNUSED_PARAM(pi4RetValFwlGlobalTrace);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalDebug
 Input       :  The Indices

                The Object 
                retValFwlGlobalDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalDebug(INT4 *pi4RetValFwlGlobalDebug)
{
   UNUSED_PARAM(pi4RetValFwlGlobalDebug);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalMaxFilters
 Input       :  The Indices

                The Object 
                retValFwlGlobalMaxFilters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalMaxFilters(INT4 *pi4RetValFwlGlobalMaxFilters)
{
   UNUSED_PARAM(pi4RetValFwlGlobalMaxFilters);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalMaxRules
 Input       :  The Indices

                The Object 
                retValFwlGlobalMaxRules
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalMaxRules(INT4 *pi4RetValFwlGlobalMaxRules)
{
   UNUSED_PARAM(pi4RetValFwlGlobalMaxRules);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalUrlFiltering
 Input       :  The Indices

                The Object 
                retValFwlGlobalUrlFiltering
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalUrlFiltering(INT4 *pi4RetValFwlGlobalUrlFiltering)
{
   UNUSED_PARAM(pi4RetValFwlGlobalUrlFiltering);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalNetBiosFiltering
 Input       :  The Indices

                The Object 
                retValFwlGlobalNetBiosFiltering
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalNetBiosFiltering(INT4 *pi4RetValFwlGlobalNetBiosFiltering)
{
   UNUSED_PARAM(pi4RetValFwlGlobalNetBiosFiltering);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalNetBiosLan2Wan
 Input       :  The Indices

                The Object 
                retValFwlGlobalNetBiosLan2Wan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalNetBiosLan2Wan(INT4 *pi4RetValFwlGlobalNetBiosLan2Wan)
{
   UNUSED_PARAM(pi4RetValFwlGlobalNetBiosLan2Wan);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalICMPv6ControlSwitch
 Input       :  The Indices

                The Object 
                retValFwlGlobalICMPv6ControlSwitch
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalICMPv6ControlSwitch(INT4 *pi4RetValFwlGlobalICMPv6ControlSwitch)
{
   UNUSED_PARAM(pi4RetValFwlGlobalICMPv6ControlSwitch);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalIpv6SpoofFiltering
 Input       :  The Indices

                The Object 
                retValFwlGlobalIpv6SpoofFiltering
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalIpv6SpoofFiltering(INT4 *pi4RetValFwlGlobalIpv6SpoofFiltering)
{
   UNUSED_PARAM(pi4RetValFwlGlobalIpv6SpoofFiltering);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalLogFileSize
 Input       :  The Indices

                The Object 
                retValFwlGlobalLogFileSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalLogFileSize(UINT4 *pu4RetValFwlGlobalLogFileSize)
{
   UNUSED_PARAM(pu4RetValFwlGlobalLogFileSize);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalLogSizeThreshold
 Input       :  The Indices

                The Object 
                retValFwlGlobalLogSizeThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalLogSizeThreshold(UINT4 *pu4RetValFwlGlobalLogSizeThreshold)
{
   UNUSED_PARAM(pu4RetValFwlGlobalLogSizeThreshold);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalIdsLogSize
 Input       :  The Indices

                The Object 
                retValFwlGlobalIdsLogSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalIdsLogSize(UINT4 *pu4RetValFwlGlobalIdsLogSize)
{
   UNUSED_PARAM(pu4RetValFwlGlobalIdsLogSize);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalIdsLogThreshold
 Input       :  The Indices

                The Object 
                retValFwlGlobalIdsLogThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalIdsLogThreshold(UINT4 *pu4RetValFwlGlobalIdsLogThreshold)
{
   UNUSED_PARAM(pu4RetValFwlGlobalIdsLogThreshold);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalIdsVersionInfo
 Input       :  The Indices

                The Object 
                retValFwlGlobalIdsVersionInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalIdsVersionInfo(tSNMP_OCTET_STRING_TYPE * pRetValFwlGlobalIdsVersionInfo)
{
   UNUSED_PARAM(pRetValFwlGlobalIdsVersionInfo);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalReloadIds
 Input       :  The Indices

                The Object 
                retValFwlGlobalReloadIds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalReloadIds(INT4 *pi4RetValFwlGlobalReloadIds)
{
   UNUSED_PARAM(pi4RetValFwlGlobalReloadIds);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalIdsStatus
 Input       :  The Indices

                The Object 
                retValFwlGlobalIdsStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalIdsStatus(INT4 *pi4RetValFwlGlobalIdsStatus)
{
   UNUSED_PARAM(pi4RetValFwlGlobalIdsStatus);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlGlobalLoadIdsRules
 Input       :  The Indices

                The Object 
                retValFwlGlobalLoadIdsRules
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlGlobalLoadIdsRules(INT4 *pi4RetValFwlGlobalLoadIdsRules)
{
   UNUSED_PARAM(pi4RetValFwlGlobalLoadIdsRules);
   return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlGlobalMasterControlSwitch
 Input       :  The Indices

                The Object 
                setValFwlGlobalMasterControlSwitch
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalMasterControlSwitch(INT4 i4SetValFwlGlobalMasterControlSwitch)
{
   UNUSED_PARAM(i4SetValFwlGlobalMasterControlSwitch);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlGlobalICMPControlSwitch
 Input       :  The Indices

                The Object 
                setValFwlGlobalICMPControlSwitch
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalICMPControlSwitch(INT4 i4SetValFwlGlobalICMPControlSwitch)
{
   UNUSED_PARAM(i4SetValFwlGlobalICMPControlSwitch);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlGlobalIpSpoofFiltering
 Input       :  The Indices

                The Object 
                setValFwlGlobalIpSpoofFiltering
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalIpSpoofFiltering(INT4 i4SetValFwlGlobalIpSpoofFiltering)
{
  UNUSED_PARAM(i4SetValFwlGlobalIpSpoofFiltering);  
  return SNMP_SUCCESS;

}


/****************************************************************************
 Function    :  nmhSetFwlGlobalSrcRouteFiltering
 Input       :  The Indices

                The Object 
                setValFwlGlobalSrcRouteFiltering
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalSrcRouteFiltering(INT4 i4SetValFwlGlobalSrcRouteFiltering)
{
   UNUSED_PARAM(i4SetValFwlGlobalSrcRouteFiltering);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlGlobalTinyFragmentFiltering
 Input       :  The Indices

                The Object 
                setValFwlGlobalTinyFragmentFiltering
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalTinyFragmentFiltering(INT4 i4SetValFwlGlobalTinyFragmentFiltering)
{
   UNUSED_PARAM(i4SetValFwlGlobalTinyFragmentFiltering);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlGlobalTcpIntercept
 Input       :  The Indices

                The Object 
                setValFwlGlobalTcpIntercept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalTcpIntercept(INT4 i4SetValFwlGlobalTcpIntercept)
{
   UNUSED_PARAM(i4SetValFwlGlobalTcpIntercept);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlGlobalTrap
 Input       :  The Indices

                The Object 
                setValFwlGlobalTrap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalTrap(INT4 i4SetValFwlGlobalTrap)
{
   UNUSED_PARAM(i4SetValFwlGlobalTrap);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlGlobalTrace
 Input       :  The Indices

                The Object 
                setValFwlGlobalTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalTrace(INT4 i4SetValFwlGlobalTrace)
{
   UNUSED_PARAM(i4SetValFwlGlobalTrace);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlGlobalDebug
 Input       :  The Indices

                The Object 
                setValFwlGlobalDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalDebug(INT4 i4SetValFwlGlobalDebug)
{
   UNUSED_PARAM(i4SetValFwlGlobalDebug);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlGlobalUrlFiltering
 Input       :  The Indices

                The Object 
                setValFwlGlobalUrlFiltering
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalUrlFiltering(INT4 i4SetValFwlGlobalUrlFiltering)
{
   UNUSED_PARAM(i4SetValFwlGlobalUrlFiltering);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlGlobalNetBiosFiltering
 Input       :  The Indices

                The Object 
                setValFwlGlobalNetBiosFiltering
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalNetBiosFiltering(INT4 i4SetValFwlGlobalNetBiosFiltering)
{
   UNUSED_PARAM(i4SetValFwlGlobalNetBiosFiltering);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlGlobalNetBiosLan2Wan
 Input       :  The Indices

                The Object 
                setValFwlGlobalNetBiosLan2Wan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalNetBiosLan2Wan(INT4 i4SetValFwlGlobalNetBiosLan2Wan)
{
   UNUSED_PARAM(i4SetValFwlGlobalNetBiosLan2Wan);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlGlobalICMPv6ControlSwitch
 Input       :  The Indices

                The Object 
                setValFwlGlobalICMPv6ControlSwitch
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalICMPv6ControlSwitch(INT4 i4SetValFwlGlobalICMPv6ControlSwitch)
{
   UNUSED_PARAM(i4SetValFwlGlobalICMPv6ControlSwitch);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlGlobalIpv6SpoofFiltering
 Input       :  The Indices

                The Object 
                setValFwlGlobalIpv6SpoofFiltering
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalIpv6SpoofFiltering(INT4 i4SetValFwlGlobalIpv6SpoofFiltering)
{
   UNUSED_PARAM(i4SetValFwlGlobalIpv6SpoofFiltering);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlGlobalLogFileSize
 Input       :  The Indices

                The Object 
                setValFwlGlobalLogFileSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalLogFileSize(UINT4 u4SetValFwlGlobalLogFileSize)
{
   UNUSED_PARAM(u4SetValFwlGlobalLogFileSize);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlGlobalLogSizeThreshold
 Input       :  The Indices

                The Object 
                setValFwlGlobalLogSizeThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalLogSizeThreshold(UINT4 u4SetValFwlGlobalLogSizeThreshold)
{
   UNUSED_PARAM(u4SetValFwlGlobalLogSizeThreshold);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlGlobalIdsLogSize
 Input       :  The Indices

                The Object 
                setValFwlGlobalIdsLogSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalIdsLogSize(UINT4 u4SetValFwlGlobalIdsLogSize)
{
   UNUSED_PARAM(u4SetValFwlGlobalIdsLogSize);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlGlobalIdsLogThreshold
 Input       :  The Indices

                The Object 
                setValFwlGlobalIdsLogThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalIdsLogThreshold(UINT4 u4SetValFwlGlobalIdsLogThreshold)
{
   UNUSED_PARAM(u4SetValFwlGlobalIdsLogThreshold);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlGlobalReloadIds
 Input       :  The Indices

                The Object 
                setValFwlGlobalReloadIds
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalReloadIds(INT4 i4SetValFwlGlobalReloadIds)
{
   UNUSED_PARAM(i4SetValFwlGlobalReloadIds);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlGlobalIdsStatus
 Input       :  The Indices

                The Object 
                setValFwlGlobalIdsStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalIdsStatus(INT4 i4SetValFwlGlobalIdsStatus)
{
   UNUSED_PARAM(i4SetValFwlGlobalIdsStatus);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlGlobalLoadIdsRules
 Input       :  The Indices

                The Object 
                setValFwlGlobalLoadIdsRules
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlGlobalLoadIdsRules(INT4 i4SetValFwlGlobalLoadIdsRules)
{
   UNUSED_PARAM(i4SetValFwlGlobalLoadIdsRules);
   return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalMasterControlSwitch
 Input       :  The Indices

                The Object 
                testValFwlGlobalMasterControlSwitch
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalMasterControlSwitch(UINT4 *pu4ErrorCode , INT4 i4TestValFwlGlobalMasterControlSwitch)
{
   UNUSED_PARAM(i4TestValFwlGlobalMasterControlSwitch);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalICMPControlSwitch
 Input       :  The Indices

                The Object 
                testValFwlGlobalICMPControlSwitch
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalICMPControlSwitch(UINT4 *pu4ErrorCode , INT4 i4TestValFwlGlobalICMPControlSwitch)
{
   UNUSED_PARAM(i4TestValFwlGlobalICMPControlSwitch);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalIpSpoofFiltering
 Input       :  The Indices

                The Object 
                testValFwlGlobalIpSpoofFiltering
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalIpSpoofFiltering(UINT4 *pu4ErrorCode , INT4 i4TestValFwlGlobalIpSpoofFiltering)
{
    UNUSED_PARAM(pu4ErrorCode);   
    UNUSED_PARAM(i4TestValFwlGlobalIpSpoofFiltering);   
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalSrcRouteFiltering
 Input       :  The Indices

                The Object 
                testValFwlGlobalSrcRouteFiltering
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalSrcRouteFiltering(UINT4 *pu4ErrorCode , INT4 i4TestValFwlGlobalSrcRouteFiltering)
{
   UNUSED_PARAM(i4TestValFwlGlobalSrcRouteFiltering);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalTinyFragmentFiltering
 Input       :  The Indices

                The Object 
                testValFwlGlobalTinyFragmentFiltering
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalTinyFragmentFiltering(UINT4 *pu4ErrorCode , INT4 i4TestValFwlGlobalTinyFragmentFiltering)
{
   UNUSED_PARAM(i4TestValFwlGlobalTinyFragmentFiltering);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalTcpIntercept
 Input       :  The Indices

                The Object 
                testValFwlGlobalTcpIntercept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalTcpIntercept(UINT4 *pu4ErrorCode , INT4 i4TestValFwlGlobalTcpIntercept)
{
   UNUSED_PARAM(i4TestValFwlGlobalTcpIntercept);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalTrap
 Input       :  The Indices

                The Object 
                testValFwlGlobalTrap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalTrap(UINT4 *pu4ErrorCode , INT4 i4TestValFwlGlobalTrap)
{
   UNUSED_PARAM(i4TestValFwlGlobalTrap);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalTrace
 Input       :  The Indices

                The Object 
                testValFwlGlobalTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalTrace(UINT4 *pu4ErrorCode , INT4 i4TestValFwlGlobalTrace)
{
   UNUSED_PARAM(i4TestValFwlGlobalTrace);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalDebug
 Input       :  The Indices

                The Object 
                testValFwlGlobalDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalDebug(UINT4 *pu4ErrorCode , INT4 i4TestValFwlGlobalDebug)
{
   UNUSED_PARAM(i4TestValFwlGlobalDebug);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalUrlFiltering
 Input       :  The Indices

                The Object 
                testValFwlGlobalUrlFiltering
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalUrlFiltering(UINT4 *pu4ErrorCode , INT4 i4TestValFwlGlobalUrlFiltering)
{
   UNUSED_PARAM(i4TestValFwlGlobalUrlFiltering);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalNetBiosFiltering
 Input       :  The Indices

                The Object 
                testValFwlGlobalNetBiosFiltering
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalNetBiosFiltering(UINT4 *pu4ErrorCode , INT4 i4TestValFwlGlobalNetBiosFiltering)
{
   UNUSED_PARAM(i4TestValFwlGlobalNetBiosFiltering);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalNetBiosLan2Wan
 Input       :  The Indices

                The Object 
                testValFwlGlobalNetBiosLan2Wan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalNetBiosLan2Wan(UINT4 *pu4ErrorCode , INT4 i4TestValFwlGlobalNetBiosLan2Wan)
{
   UNUSED_PARAM(i4TestValFwlGlobalNetBiosLan2Wan);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalICMPv6ControlSwitch
 Input       :  The Indices

                The Object 
                testValFwlGlobalICMPv6ControlSwitch
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalICMPv6ControlSwitch(UINT4 *pu4ErrorCode , INT4 i4TestValFwlGlobalICMPv6ControlSwitch)
{
   UNUSED_PARAM(i4TestValFwlGlobalICMPv6ControlSwitch);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalIpv6SpoofFiltering
 Input       :  The Indices

                The Object 
                testValFwlGlobalIpv6SpoofFiltering
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalIpv6SpoofFiltering(UINT4 *pu4ErrorCode , INT4 i4TestValFwlGlobalIpv6SpoofFiltering)
{
   UNUSED_PARAM(i4TestValFwlGlobalIpv6SpoofFiltering);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalLogFileSize
 Input       :  The Indices

                The Object 
                testValFwlGlobalLogFileSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalLogFileSize(UINT4 *pu4ErrorCode , UINT4 u4TestValFwlGlobalLogFileSize)
{
   UNUSED_PARAM(u4TestValFwlGlobalLogFileSize);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalLogSizeThreshold
 Input       :  The Indices

                The Object 
                testValFwlGlobalLogSizeThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalLogSizeThreshold(UINT4 *pu4ErrorCode , UINT4 u4TestValFwlGlobalLogSizeThreshold)
{
   UNUSED_PARAM(u4TestValFwlGlobalLogSizeThreshold);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalIdsLogSize
 Input       :  The Indices

                The Object 
                testValFwlGlobalIdsLogSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalIdsLogSize(UINT4 *pu4ErrorCode , UINT4 u4TestValFwlGlobalIdsLogSize)
{
   UNUSED_PARAM(u4TestValFwlGlobalIdsLogSize);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalIdsLogThreshold
 Input       :  The Indices

                The Object 
                testValFwlGlobalIdsLogThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalIdsLogThreshold(UINT4 *pu4ErrorCode , UINT4 u4TestValFwlGlobalIdsLogThreshold)
{
   UNUSED_PARAM(u4TestValFwlGlobalIdsLogThreshold);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalReloadIds
 Input       :  The Indices

                The Object 
                testValFwlGlobalReloadIds
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalReloadIds(UINT4 *pu4ErrorCode , INT4 i4TestValFwlGlobalReloadIds)
{
   UNUSED_PARAM(i4TestValFwlGlobalReloadIds);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalIdsStatus
 Input       :  The Indices

                The Object 
                testValFwlGlobalIdsStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalIdsStatus(UINT4 *pu4ErrorCode , INT4 i4TestValFwlGlobalIdsStatus)
{
   UNUSED_PARAM(i4TestValFwlGlobalIdsStatus);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlGlobalLoadIdsRules
 Input       :  The Indices

                The Object 
                testValFwlGlobalLoadIdsRules
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlGlobalLoadIdsRules(UINT4 *pu4ErrorCode , INT4 i4TestValFwlGlobalLoadIdsRules)
{
   UNUSED_PARAM(i4TestValFwlGlobalLoadIdsRules);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalMasterControlSwitch
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalMasterControlSwitch(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalICMPControlSwitch
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalICMPControlSwitch(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalIpSpoofFiltering
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalIpSpoofFiltering(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalSrcRouteFiltering
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalSrcRouteFiltering(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalTinyFragmentFiltering
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalTinyFragmentFiltering(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalTcpIntercept
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalTcpIntercept(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalTrap
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalTrap(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalTrace(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalDebug(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalUrlFiltering
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalUrlFiltering(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalNetBiosFiltering
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalNetBiosFiltering(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalNetBiosLan2Wan
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalNetBiosLan2Wan(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalICMPv6ControlSwitch
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalICMPv6ControlSwitch(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalIpv6SpoofFiltering
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalIpv6SpoofFiltering(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalLogFileSize
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalLogFileSize(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalLogSizeThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalLogSizeThreshold(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalIdsLogSize
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalIdsLogSize(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalIdsLogThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalIdsLogThreshold(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalReloadIds
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalReloadIds(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalIdsStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalIdsStatus(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlGlobalLoadIdsRules
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlGlobalLoadIdsRules(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlDefnTcpInterceptThreshold
 Input       :  The Indices

                The Object 
                retValFwlDefnTcpInterceptThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlDefnTcpInterceptThreshold(INT4 *pi4RetValFwlDefnTcpInterceptThreshold)
{
   UNUSED_PARAM(pi4RetValFwlDefnTcpInterceptThreshold);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlDefnInterceptTimeout
 Input       :  The Indices

                The Object 
                retValFwlDefnInterceptTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlDefnInterceptTimeout(UINT4 *pu4RetValFwlDefnInterceptTimeout)
{
   UNUSED_PARAM(pu4RetValFwlDefnInterceptTimeout);
   return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlDefnTcpInterceptThreshold
 Input       :  The Indices

                The Object 
                setValFwlDefnTcpInterceptThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlDefnTcpInterceptThreshold(INT4 i4SetValFwlDefnTcpInterceptThreshold)
{
   UNUSED_PARAM(i4SetValFwlDefnTcpInterceptThreshold);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlDefnInterceptTimeout
 Input       :  The Indices

                The Object 
                setValFwlDefnInterceptTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlDefnInterceptTimeout(UINT4 u4SetValFwlDefnInterceptTimeout)
{
   UNUSED_PARAM(u4SetValFwlDefnInterceptTimeout);
   return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlDefnTcpInterceptThreshold
 Input       :  The Indices

                The Object 
                testValFwlDefnTcpInterceptThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlDefnTcpInterceptThreshold(UINT4 *pu4ErrorCode , INT4 i4TestValFwlDefnTcpInterceptThreshold)
{
   UNUSED_PARAM(i4TestValFwlDefnTcpInterceptThreshold);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlDefnInterceptTimeout
 Input       :  The Indices

                The Object 
                testValFwlDefnInterceptTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlDefnInterceptTimeout(UINT4 *pu4ErrorCode , UINT4 u4TestValFwlDefnInterceptTimeout)
{
   UNUSED_PARAM(u4TestValFwlDefnInterceptTimeout);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlDefnTcpInterceptThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlDefnTcpInterceptThreshold(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlDefnInterceptTimeout
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlDefnInterceptTimeout(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FwlDefnFilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlDefnFilterTable
 Input       :  The Indices
                FwlFilterFilterName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFwlDefnFilterTable(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFwlDefnFilterTable
 Input       :  The Indices
                FwlFilterFilterName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFwlDefnFilterTable(tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFwlDefnFilterTable
 Input       :  The Indices
                FwlFilterFilterName
                nextFwlFilterFilterName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFwlDefnFilterTable(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName ,tSNMP_OCTET_STRING_TYPE * pNextFwlFilterFilterName )
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pNextFwlFilterFilterName);
   return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlFilterSrcAddress
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterSrcAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlFilterSrcAddress(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , tSNMP_OCTET_STRING_TYPE * pRetValFwlFilterSrcAddress)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pRetValFwlFilterSrcAddress);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlFilterDestAddress
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterDestAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlFilterDestAddress(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , tSNMP_OCTET_STRING_TYPE * pRetValFwlFilterDestAddress)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pRetValFwlFilterDestAddress);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlFilterProtocol
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlFilterProtocol(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 *pi4RetValFwlFilterProtocol)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pi4RetValFwlFilterProtocol);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlFilterSrcPort
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterSrcPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlFilterSrcPort(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , tSNMP_OCTET_STRING_TYPE * pRetValFwlFilterSrcPort)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pRetValFwlFilterSrcPort);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlFilterDestPort
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterDestPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlFilterDestPort(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , tSNMP_OCTET_STRING_TYPE * pRetValFwlFilterDestPort)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pRetValFwlFilterDestPort);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlFilterAckBit
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterAckBit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlFilterAckBit(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 *pi4RetValFwlFilterAckBit)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pi4RetValFwlFilterAckBit);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlFilterRstBit
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterRstBit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlFilterRstBit(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 *pi4RetValFwlFilterRstBit)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pi4RetValFwlFilterRstBit);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlFilterTos
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterTos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlFilterTos(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 *pi4RetValFwlFilterTos)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pi4RetValFwlFilterTos);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlFilterAccounting
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterAccounting
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlFilterAccounting(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 *pi4RetValFwlFilterAccounting)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pi4RetValFwlFilterAccounting);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlFilterHitClear
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterHitClear
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlFilterHitClear(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 *pi4RetValFwlFilterHitClear)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pi4RetValFwlFilterHitClear);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlFilterHitsCount
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterHitsCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlFilterHitsCount(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , UINT4 *pu4RetValFwlFilterHitsCount)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pu4RetValFwlFilterHitsCount);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlFilterAddrType
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlFilterAddrType(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 *pi4RetValFwlFilterAddrType)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pi4RetValFwlFilterAddrType);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlFilterFlowId
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterFlowId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlFilterFlowId(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , UINT4 *pu4RetValFwlFilterFlowId)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pu4RetValFwlFilterFlowId);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlFilterDscp
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlFilterDscp(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 *pi4RetValFwlFilterDscp)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pi4RetValFwlFilterDscp);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlFilterRowStatus
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlFilterRowStatus(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 *pi4RetValFwlFilterRowStatus)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pi4RetValFwlFilterRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlFilterSrcAddress
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterSrcAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlFilterSrcAddress(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , tSNMP_OCTET_STRING_TYPE *pSetValFwlFilterSrcAddress)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pSetValFwlFilterSrcAddress);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlFilterDestAddress
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterDestAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlFilterDestAddress(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , tSNMP_OCTET_STRING_TYPE *pSetValFwlFilterDestAddress)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pSetValFwlFilterDestAddress);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlFilterProtocol
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlFilterProtocol(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 i4SetValFwlFilterProtocol)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(i4SetValFwlFilterProtocol);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlFilterSrcPort
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterSrcPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlFilterSrcPort(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , tSNMP_OCTET_STRING_TYPE *pSetValFwlFilterSrcPort)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pSetValFwlFilterSrcPort);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlFilterDestPort
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterDestPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlFilterDestPort(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , tSNMP_OCTET_STRING_TYPE *pSetValFwlFilterDestPort)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pSetValFwlFilterDestPort);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlFilterAckBit
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterAckBit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlFilterAckBit(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 i4SetValFwlFilterAckBit)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(i4SetValFwlFilterAckBit);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlFilterRstBit
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterRstBit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlFilterRstBit(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 i4SetValFwlFilterRstBit)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(i4SetValFwlFilterRstBit);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlFilterTos
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterTos
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlFilterTos(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 i4SetValFwlFilterTos)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(i4SetValFwlFilterTos);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlFilterAccounting
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterAccounting
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlFilterAccounting(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 i4SetValFwlFilterAccounting)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(i4SetValFwlFilterAccounting);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlFilterHitClear
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterHitClear
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlFilterHitClear(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 i4SetValFwlFilterHitClear)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(i4SetValFwlFilterHitClear);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlFilterAddrType
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlFilterAddrType(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 i4SetValFwlFilterAddrType)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(i4SetValFwlFilterAddrType);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlFilterFlowId
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterFlowId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlFilterFlowId(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , UINT4 u4SetValFwlFilterFlowId)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(u4SetValFwlFilterFlowId);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlFilterDscp
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlFilterDscp(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 i4SetValFwlFilterDscp)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(i4SetValFwlFilterDscp);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlFilterRowStatus
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlFilterRowStatus(tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 i4SetValFwlFilterRowStatus)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(i4SetValFwlFilterRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlFilterSrcAddress
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterSrcAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlFilterSrcAddress(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , tSNMP_OCTET_STRING_TYPE *pTestValFwlFilterSrcAddress)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(pTestValFwlFilterSrcAddress);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlFilterDestAddress
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterDestAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlFilterDestAddress(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , tSNMP_OCTET_STRING_TYPE *pTestValFwlFilterDestAddress)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(pTestValFwlFilterDestAddress);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlFilterProtocol
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlFilterProtocol(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 i4TestValFwlFilterProtocol)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlFilterProtocol);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlFilterSrcPort
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterSrcPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlFilterSrcPort(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , tSNMP_OCTET_STRING_TYPE *pTestValFwlFilterSrcPort)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(pTestValFwlFilterSrcPort);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlFilterDestPort
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterDestPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlFilterDestPort(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , tSNMP_OCTET_STRING_TYPE *pTestValFwlFilterDestPort)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(pTestValFwlFilterDestPort);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlFilterAckBit
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterAckBit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlFilterAckBit(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 i4TestValFwlFilterAckBit)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlFilterAckBit);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlFilterRstBit
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterRstBit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlFilterRstBit(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 i4TestValFwlFilterRstBit)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlFilterRstBit);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlFilterTos
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterTos
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlFilterTos(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 i4TestValFwlFilterTos)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlFilterTos);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlFilterAccounting
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterAccounting
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlFilterAccounting(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 i4TestValFwlFilterAccounting)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlFilterAccounting);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlFilterHitClear
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterHitClear
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlFilterHitClear(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 i4TestValFwlFilterHitClear)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlFilterHitClear);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlFilterAddrType
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlFilterAddrType(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 i4TestValFwlFilterAddrType)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlFilterAddrType);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlFilterFlowId
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterFlowId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlFilterFlowId(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , UINT4 u4TestValFwlFilterFlowId)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(u4TestValFwlFilterFlowId);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlFilterDscp
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlFilterDscp(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 i4TestValFwlFilterDscp)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlFilterDscp);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlFilterRowStatus
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlFilterRowStatus(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFwlFilterFilterName , INT4 i4TestValFwlFilterRowStatus)
{
   UNUSED_PARAM(pFwlFilterFilterName);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlFilterRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlDefnFilterTable
 Input       :  The Indices
                FwlFilterFilterName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlDefnFilterTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FwlDefnRuleTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlDefnRuleTable
 Input       :  The Indices
                FwlRuleRuleName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFwlDefnRuleTable(tSNMP_OCTET_STRING_TYPE *pFwlRuleRuleName)
{
   UNUSED_PARAM(pFwlRuleRuleName);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFwlDefnRuleTable
 Input       :  The Indices
                FwlRuleRuleName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFwlDefnRuleTable(tSNMP_OCTET_STRING_TYPE * pFwlRuleRuleName)
{

   UNUSED_PARAM(pFwlRuleRuleName);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFwlDefnRuleTable
 Input       :  The Indices
                FwlRuleRuleName
                nextFwlRuleRuleName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFwlDefnRuleTable(tSNMP_OCTET_STRING_TYPE *pFwlRuleRuleName ,tSNMP_OCTET_STRING_TYPE * pNextFwlRuleRuleName )
{
   UNUSED_PARAM(pNextFwlRuleRuleName);
   UNUSED_PARAM(pFwlRuleRuleName);
   return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlRuleFilterSet
 Input       :  The Indices
                FwlRuleRuleName

                The Object 
                retValFwlRuleFilterSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlRuleFilterSet(tSNMP_OCTET_STRING_TYPE *pFwlRuleRuleName , tSNMP_OCTET_STRING_TYPE * pRetValFwlRuleFilterSet)
{
   UNUSED_PARAM(pFwlRuleRuleName);
   UNUSED_PARAM(pRetValFwlRuleFilterSet);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlRuleRowStatus
 Input       :  The Indices
                FwlRuleRuleName

                The Object 
                retValFwlRuleRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlRuleRowStatus(tSNMP_OCTET_STRING_TYPE *pFwlRuleRuleName , INT4 *pi4RetValFwlRuleRowStatus)
{
   UNUSED_PARAM(pFwlRuleRuleName);
   UNUSED_PARAM(pi4RetValFwlRuleRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlRuleFilterSet
 Input       :  The Indices
                FwlRuleRuleName

                The Object 
                setValFwlRuleFilterSet
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlRuleFilterSet(tSNMP_OCTET_STRING_TYPE *pFwlRuleRuleName , tSNMP_OCTET_STRING_TYPE *pSetValFwlRuleFilterSet)
{
   UNUSED_PARAM(pFwlRuleRuleName);
   UNUSED_PARAM(pSetValFwlRuleFilterSet);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlRuleRowStatus
 Input       :  The Indices
                FwlRuleRuleName

                The Object 
                setValFwlRuleRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlRuleRowStatus(tSNMP_OCTET_STRING_TYPE *pFwlRuleRuleName , INT4 i4SetValFwlRuleRowStatus)
{
   UNUSED_PARAM(pFwlRuleRuleName);
   UNUSED_PARAM(i4SetValFwlRuleRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlRuleFilterSet
 Input       :  The Indices
                FwlRuleRuleName

                The Object 
                testValFwlRuleFilterSet
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlRuleFilterSet(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFwlRuleRuleName , tSNMP_OCTET_STRING_TYPE *pTestValFwlRuleFilterSet)
{
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(pFwlRuleRuleName);
   UNUSED_PARAM(pTestValFwlRuleFilterSet);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlRuleRowStatus
 Input       :  The Indices
                FwlRuleRuleName

                The Object 
                testValFwlRuleRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlRuleRowStatus(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFwlRuleRuleName , INT4 i4TestValFwlRuleRowStatus)
{
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(pFwlRuleRuleName);
   UNUSED_PARAM(i4TestValFwlRuleRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlDefnRuleTable
 Input       :  The Indices
                FwlRuleRuleName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlDefnRuleTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FwlDefnAclTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlDefnAclTable
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFwlDefnAclTable(INT4 i4FwlAclIfIndex , tSNMP_OCTET_STRING_TYPE *pFwlAclAclName , INT4 i4FwlAclDirection)
{
   UNUSED_PARAM(i4FwlAclIfIndex);
   UNUSED_PARAM(pFwlAclAclName);
   UNUSED_PARAM(i4FwlAclDirection);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFwlDefnAclTable
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFwlDefnAclTable(INT4 *pi4FwlAclIfIndex , tSNMP_OCTET_STRING_TYPE * pFwlAclAclName , INT4 *pi4FwlAclDirection)
{
   UNUSED_PARAM(pi4FwlAclIfIndex);
   UNUSED_PARAM(pFwlAclAclName);
   UNUSED_PARAM(pi4FwlAclDirection);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFwlDefnAclTable
 Input       :  The Indices
                FwlAclIfIndex
                nextFwlAclIfIndex
                FwlAclAclName
                nextFwlAclAclName
                FwlAclDirection
                nextFwlAclDirection
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFwlDefnAclTable(INT4 i4FwlAclIfIndex ,INT4 *pi4NextFwlAclIfIndex  , tSNMP_OCTET_STRING_TYPE *pFwlAclAclName ,tSNMP_OCTET_STRING_TYPE * pNextFwlAclAclName  , INT4 i4FwlAclDirection ,INT4 *pi4NextFwlAclDirection )
{
   UNUSED_PARAM(i4FwlAclIfIndex);
   UNUSED_PARAM(pi4NextFwlAclIfIndex);
   UNUSED_PARAM(pFwlAclAclName);
   UNUSED_PARAM(pNextFwlAclAclName);
   UNUSED_PARAM(i4FwlAclDirection);
   UNUSED_PARAM(pi4NextFwlAclDirection);
   return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlAclAction
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                retValFwlAclAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlAclAction(INT4 i4FwlAclIfIndex , tSNMP_OCTET_STRING_TYPE *pFwlAclAclName , INT4 i4FwlAclDirection , INT4 *pi4RetValFwlAclAction)
{
   UNUSED_PARAM(i4FwlAclIfIndex);
   UNUSED_PARAM(pFwlAclAclName);
   UNUSED_PARAM(i4FwlAclDirection);
   UNUSED_PARAM(pi4RetValFwlAclAction);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlAclSequenceNumber
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                retValFwlAclSequenceNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlAclSequenceNumber(INT4 i4FwlAclIfIndex , tSNMP_OCTET_STRING_TYPE *pFwlAclAclName , INT4 i4FwlAclDirection , INT4 *pi4RetValFwlAclSequenceNumber)
{
   UNUSED_PARAM(i4FwlAclIfIndex);
   UNUSED_PARAM(pFwlAclAclName);
   UNUSED_PARAM(i4FwlAclDirection);
   UNUSED_PARAM(pi4RetValFwlAclSequenceNumber);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlAclAclType
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                retValFwlAclAclType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlAclAclType(INT4 i4FwlAclIfIndex , tSNMP_OCTET_STRING_TYPE *pFwlAclAclName , INT4 i4FwlAclDirection , INT4 *pi4RetValFwlAclAclType)
{
   UNUSED_PARAM(i4FwlAclIfIndex);
   UNUSED_PARAM(pFwlAclAclName);
   UNUSED_PARAM(i4FwlAclDirection);
   UNUSED_PARAM(pi4RetValFwlAclAclType);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlAclLogTrigger
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                retValFwlAclLogTrigger
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlAclLogTrigger(INT4 i4FwlAclIfIndex , tSNMP_OCTET_STRING_TYPE *pFwlAclAclName , INT4 i4FwlAclDirection , INT4 *pi4RetValFwlAclLogTrigger)
{
   UNUSED_PARAM(i4FwlAclIfIndex);
   UNUSED_PARAM(pFwlAclAclName);
   UNUSED_PARAM(i4FwlAclDirection);
   UNUSED_PARAM(pi4RetValFwlAclLogTrigger);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlAclFragAction
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                retValFwlAclFragAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlAclFragAction(INT4 i4FwlAclIfIndex , tSNMP_OCTET_STRING_TYPE *pFwlAclAclName , INT4 i4FwlAclDirection , INT4 *pi4RetValFwlAclFragAction)
{
   UNUSED_PARAM(i4FwlAclIfIndex);
   UNUSED_PARAM(pFwlAclAclName);
   UNUSED_PARAM(i4FwlAclDirection);
   UNUSED_PARAM(pi4RetValFwlAclFragAction);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlAclRowStatus
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                retValFwlAclRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlAclRowStatus(INT4 i4FwlAclIfIndex , tSNMP_OCTET_STRING_TYPE *pFwlAclAclName , INT4 i4FwlAclDirection , INT4 *pi4RetValFwlAclRowStatus)
{
   UNUSED_PARAM(i4FwlAclIfIndex);
   UNUSED_PARAM(pFwlAclAclName);
   UNUSED_PARAM(i4FwlAclDirection);
   UNUSED_PARAM(pi4RetValFwlAclRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlAclAction
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                setValFwlAclAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlAclAction(INT4 i4FwlAclIfIndex , tSNMP_OCTET_STRING_TYPE *pFwlAclAclName , INT4 i4FwlAclDirection , INT4 i4SetValFwlAclAction)
{
   UNUSED_PARAM(i4FwlAclIfIndex);
   UNUSED_PARAM(pFwlAclAclName);
   UNUSED_PARAM(i4FwlAclDirection);
   UNUSED_PARAM(i4SetValFwlAclAction);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlAclSequenceNumber
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                setValFwlAclSequenceNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlAclSequenceNumber(INT4 i4FwlAclIfIndex , tSNMP_OCTET_STRING_TYPE *pFwlAclAclName , INT4 i4FwlAclDirection , INT4 i4SetValFwlAclSequenceNumber)
{
   UNUSED_PARAM(i4FwlAclIfIndex);
   UNUSED_PARAM(pFwlAclAclName);
   UNUSED_PARAM(i4FwlAclDirection);
   UNUSED_PARAM(i4SetValFwlAclSequenceNumber);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlAclLogTrigger
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                setValFwlAclLogTrigger
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlAclLogTrigger(INT4 i4FwlAclIfIndex , tSNMP_OCTET_STRING_TYPE *pFwlAclAclName , INT4 i4FwlAclDirection , INT4 i4SetValFwlAclLogTrigger)
{
   UNUSED_PARAM(i4FwlAclIfIndex);
   UNUSED_PARAM(pFwlAclAclName);
   UNUSED_PARAM(i4FwlAclDirection);
   UNUSED_PARAM(i4SetValFwlAclLogTrigger);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlAclFragAction
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                setValFwlAclFragAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlAclFragAction(INT4 i4FwlAclIfIndex , tSNMP_OCTET_STRING_TYPE *pFwlAclAclName , INT4 i4FwlAclDirection , INT4 i4SetValFwlAclFragAction)
{
   UNUSED_PARAM(i4FwlAclIfIndex);
   UNUSED_PARAM(pFwlAclAclName);
   UNUSED_PARAM(i4FwlAclDirection);
   UNUSED_PARAM(i4SetValFwlAclFragAction);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlAclRowStatus
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                setValFwlAclRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlAclRowStatus(INT4 i4FwlAclIfIndex , tSNMP_OCTET_STRING_TYPE *pFwlAclAclName , INT4 i4FwlAclDirection , INT4 i4SetValFwlAclRowStatus)
{
   UNUSED_PARAM(i4FwlAclIfIndex);
   UNUSED_PARAM(pFwlAclAclName);
   UNUSED_PARAM(i4FwlAclDirection);
   UNUSED_PARAM(i4SetValFwlAclRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlAclAction
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                testValFwlAclAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlAclAction(UINT4 *pu4ErrorCode , INT4 i4FwlAclIfIndex , tSNMP_OCTET_STRING_TYPE *pFwlAclAclName , INT4 i4FwlAclDirection , INT4 i4TestValFwlAclAction)
{
   UNUSED_PARAM(i4FwlAclIfIndex);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(pFwlAclAclName);
   UNUSED_PARAM(i4FwlAclDirection);
   UNUSED_PARAM(i4TestValFwlAclAction);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlAclSequenceNumber
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                testValFwlAclSequenceNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlAclSequenceNumber(UINT4 *pu4ErrorCode , INT4 i4FwlAclIfIndex , tSNMP_OCTET_STRING_TYPE *pFwlAclAclName , INT4 i4FwlAclDirection , INT4 i4TestValFwlAclSequenceNumber)
{
   UNUSED_PARAM(i4FwlAclIfIndex);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(pFwlAclAclName);
   UNUSED_PARAM(i4FwlAclDirection);
   UNUSED_PARAM(i4TestValFwlAclSequenceNumber);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlAclLogTrigger
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                testValFwlAclLogTrigger
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlAclLogTrigger(UINT4 *pu4ErrorCode , INT4 i4FwlAclIfIndex , tSNMP_OCTET_STRING_TYPE *pFwlAclAclName , INT4 i4FwlAclDirection , INT4 i4TestValFwlAclLogTrigger)
{
   UNUSED_PARAM(i4FwlAclIfIndex);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(pFwlAclAclName);
   UNUSED_PARAM(i4FwlAclDirection);
   UNUSED_PARAM(i4TestValFwlAclLogTrigger);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlAclFragAction
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                testValFwlAclFragAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlAclFragAction(UINT4 *pu4ErrorCode , INT4 i4FwlAclIfIndex , tSNMP_OCTET_STRING_TYPE *pFwlAclAclName , INT4 i4FwlAclDirection , INT4 i4TestValFwlAclFragAction)
{
   UNUSED_PARAM(i4FwlAclIfIndex);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(pFwlAclAclName);
   UNUSED_PARAM(i4FwlAclDirection);
   UNUSED_PARAM(i4TestValFwlAclFragAction);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlAclRowStatus
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                testValFwlAclRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlAclRowStatus(UINT4 *pu4ErrorCode , INT4 i4FwlAclIfIndex , tSNMP_OCTET_STRING_TYPE *pFwlAclAclName , INT4 i4FwlAclDirection , INT4 i4TestValFwlAclRowStatus)
{
   UNUSED_PARAM(i4FwlAclIfIndex);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(pFwlAclAclName);
   UNUSED_PARAM(i4FwlAclDirection);
   UNUSED_PARAM(i4TestValFwlAclRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlDefnAclTable
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlDefnAclTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FwlDefnIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlDefnIfTable
 Input       :  The Indices
                FwlIfIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFwlDefnIfTable(INT4 i4FwlIfIfIndex)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFwlDefnIfTable
 Input       :  The Indices
                FwlIfIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFwlDefnIfTable(INT4 *pi4FwlIfIfIndex)
{
   UNUSED_PARAM(pi4FwlIfIfIndex);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFwlDefnIfTable
 Input       :  The Indices
                FwlIfIfIndex
                nextFwlIfIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFwlDefnIfTable(INT4 i4FwlIfIfIndex ,INT4 *pi4NextFwlIfIfIndex )
{
   UNUSED_PARAM(pi4NextFwlIfIfIndex);
   UNUSED_PARAM(i4FwlIfIfIndex);
   return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlIfIfType
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                retValFwlIfIfType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlIfIfType(INT4 i4FwlIfIfIndex , INT4 *pi4RetValFwlIfIfType)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(pi4RetValFwlIfIfType);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlIfIpOptions
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                retValFwlIfIpOptions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlIfIpOptions(INT4 i4FwlIfIfIndex , INT4 *pi4RetValFwlIfIpOptions)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(pi4RetValFwlIfIpOptions);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlIfFragments
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                retValFwlIfFragments
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlIfFragments(INT4 i4FwlIfIfIndex , INT4 *pi4RetValFwlIfFragments)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(pi4RetValFwlIfFragments);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlIfFragmentSize
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                retValFwlIfFragmentSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlIfFragmentSize(INT4 i4FwlIfIfIndex , UINT4 *pu4RetValFwlIfFragmentSize)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(pu4RetValFwlIfFragmentSize);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlIfICMPType
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                retValFwlIfICMPType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlIfICMPType(INT4 i4FwlIfIfIndex , INT4 *pi4RetValFwlIfICMPType)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(pi4RetValFwlIfICMPType);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlIfICMPCode
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                retValFwlIfICMPCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlIfICMPCode(INT4 i4FwlIfIfIndex , INT4 *pi4RetValFwlIfICMPCode)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(pi4RetValFwlIfICMPCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlIfICMPv6MsgType
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                retValFwlIfICMPv6MsgType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlIfICMPv6MsgType(INT4 i4FwlIfIfIndex , INT4 *pi4RetValFwlIfICMPv6MsgType)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(pi4RetValFwlIfICMPv6MsgType);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlIfRowStatus
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                retValFwlIfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlIfRowStatus(INT4 i4FwlIfIfIndex , INT4 *pi4RetValFwlIfRowStatus)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(pi4RetValFwlIfRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlIfIfType
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                setValFwlIfIfType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlIfIfType(INT4 i4FwlIfIfIndex , INT4 i4SetValFwlIfIfType)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(i4SetValFwlIfIfType);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlIfIpOptions
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                setValFwlIfIpOptions
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlIfIpOptions(INT4 i4FwlIfIfIndex , INT4 i4SetValFwlIfIpOptions)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(i4SetValFwlIfIpOptions);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlIfFragments
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                setValFwlIfFragments
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlIfFragments(INT4 i4FwlIfIfIndex , INT4 i4SetValFwlIfFragments)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(i4SetValFwlIfFragments);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlIfFragmentSize
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                setValFwlIfFragmentSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlIfFragmentSize(INT4 i4FwlIfIfIndex , UINT4 u4SetValFwlIfFragmentSize)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(u4SetValFwlIfFragmentSize);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlIfICMPType
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                setValFwlIfICMPType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlIfICMPType(INT4 i4FwlIfIfIndex , INT4 i4SetValFwlIfICMPType)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(i4SetValFwlIfICMPType);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlIfICMPCode
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                setValFwlIfICMPCode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlIfICMPCode(INT4 i4FwlIfIfIndex , INT4 i4SetValFwlIfICMPCode)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(i4SetValFwlIfICMPCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlIfICMPv6MsgType
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                setValFwlIfICMPv6MsgType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlIfICMPv6MsgType(INT4 i4FwlIfIfIndex , INT4 i4SetValFwlIfICMPv6MsgType)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(i4SetValFwlIfICMPv6MsgType);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlIfRowStatus
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                setValFwlIfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlIfRowStatus(INT4 i4FwlIfIfIndex , INT4 i4SetValFwlIfRowStatus)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(i4SetValFwlIfRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlIfIfType
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                testValFwlIfIfType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlIfIfType(UINT4 *pu4ErrorCode , INT4 i4FwlIfIfIndex , INT4 i4TestValFwlIfIfType)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlIfIfType);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlIfIpOptions
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                testValFwlIfIpOptions
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlIfIpOptions(UINT4 *pu4ErrorCode , INT4 i4FwlIfIfIndex , INT4 i4TestValFwlIfIpOptions)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlIfIpOptions);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlIfFragments
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                testValFwlIfFragments
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlIfFragments(UINT4 *pu4ErrorCode , INT4 i4FwlIfIfIndex , INT4 i4TestValFwlIfFragments)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlIfFragments);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlIfFragmentSize
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                testValFwlIfFragmentSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlIfFragmentSize(UINT4 *pu4ErrorCode , INT4 i4FwlIfIfIndex , UINT4 u4TestValFwlIfFragmentSize)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(u4TestValFwlIfFragmentSize);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlIfICMPType
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                testValFwlIfICMPType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlIfICMPType(UINT4 *pu4ErrorCode , INT4 i4FwlIfIfIndex , INT4 i4TestValFwlIfICMPType)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlIfICMPType);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlIfICMPCode
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                testValFwlIfICMPCode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlIfICMPCode(UINT4 *pu4ErrorCode , INT4 i4FwlIfIfIndex , INT4 i4TestValFwlIfICMPCode)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlIfICMPCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlIfICMPv6MsgType
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                testValFwlIfICMPv6MsgType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlIfICMPv6MsgType(UINT4 *pu4ErrorCode , INT4 i4FwlIfIfIndex , INT4 i4TestValFwlIfICMPv6MsgType)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlIfICMPv6MsgType);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlIfRowStatus
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                testValFwlIfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlIfRowStatus(UINT4 *pu4ErrorCode , INT4 i4FwlIfIfIndex , INT4 i4TestValFwlIfRowStatus)
{
   UNUSED_PARAM(i4FwlIfIfIndex);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlIfRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlDefnIfTable
 Input       :  The Indices
                FwlIfIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlDefnIfTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FwlDefnDmzTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlDefnDmzTable
 Input       :  The Indices
                FwlDmzIpIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFwlDefnDmzTable(UINT4 u4FwlDmzIpIndex)
{
   UNUSED_PARAM(u4FwlDmzIpIndex);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFwlDefnDmzTable
 Input       :  The Indices
                FwlDmzIpIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFwlDefnDmzTable(UINT4 *pu4FwlDmzIpIndex)
{
   UNUSED_PARAM(pu4FwlDmzIpIndex);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFwlDefnDmzTable
 Input       :  The Indices
                FwlDmzIpIndex
                nextFwlDmzIpIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFwlDefnDmzTable(UINT4 u4FwlDmzIpIndex ,UINT4 *pu4NextFwlDmzIpIndex )
{
   UNUSED_PARAM(u4FwlDmzIpIndex);
   UNUSED_PARAM(pu4NextFwlDmzIpIndex);
   return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlDmzRowStatus
 Input       :  The Indices
                FwlDmzIpIndex

                The Object 
                retValFwlDmzRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlDmzRowStatus(UINT4 u4FwlDmzIpIndex , INT4 *pi4RetValFwlDmzRowStatus)
{
   UNUSED_PARAM(u4FwlDmzIpIndex);
   UNUSED_PARAM(pi4RetValFwlDmzRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlDmzRowStatus
 Input       :  The Indices
                FwlDmzIpIndex

                The Object 
                setValFwlDmzRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlDmzRowStatus(UINT4 u4FwlDmzIpIndex , INT4 i4SetValFwlDmzRowStatus)
{
   UNUSED_PARAM(u4FwlDmzIpIndex);
   UNUSED_PARAM(i4SetValFwlDmzRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlDmzRowStatus
 Input       :  The Indices
                FwlDmzIpIndex

                The Object 
                testValFwlDmzRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlDmzRowStatus(UINT4 *pu4ErrorCode , UINT4 u4FwlDmzIpIndex , INT4 i4TestValFwlDmzRowStatus)
{
   UNUSED_PARAM(u4FwlDmzIpIndex);
   UNUSED_PARAM(i4TestValFwlDmzRowStatus);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlDefnDmzTable
 Input       :  The Indices
                FwlDmzIpIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlDefnDmzTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FwlUrlFilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlUrlFilterTable
 Input       :  The Indices
                FwlUrlString
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFwlUrlFilterTable(tSNMP_OCTET_STRING_TYPE *pFwlUrlString)
{
   UNUSED_PARAM(pFwlUrlString);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFwlUrlFilterTable
 Input       :  The Indices
                FwlUrlString
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFwlUrlFilterTable(tSNMP_OCTET_STRING_TYPE * pFwlUrlString)
{
   UNUSED_PARAM(pFwlUrlString);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFwlUrlFilterTable
 Input       :  The Indices
                FwlUrlString
                nextFwlUrlString
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFwlUrlFilterTable(tSNMP_OCTET_STRING_TYPE *pFwlUrlString ,tSNMP_OCTET_STRING_TYPE * pNextFwlUrlString )
{
   UNUSED_PARAM(pFwlUrlString);
   UNUSED_PARAM(pNextFwlUrlString);
   return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlUrlHitCount
 Input       :  The Indices
                FwlUrlString

                The Object 
                retValFwlUrlHitCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlUrlHitCount(tSNMP_OCTET_STRING_TYPE *pFwlUrlString , UINT4 *pu4RetValFwlUrlHitCount)
{
   UNUSED_PARAM(pFwlUrlString);
   UNUSED_PARAM(pu4RetValFwlUrlHitCount);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlUrlFilterRowStatus
 Input       :  The Indices
                FwlUrlString

                The Object 
                retValFwlUrlFilterRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlUrlFilterRowStatus(tSNMP_OCTET_STRING_TYPE *pFwlUrlString , INT4 *pi4RetValFwlUrlFilterRowStatus)
{
   UNUSED_PARAM(pFwlUrlString);
   UNUSED_PARAM(pi4RetValFwlUrlFilterRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlUrlFilterRowStatus
 Input       :  The Indices
                FwlUrlString

                The Object 
                setValFwlUrlFilterRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlUrlFilterRowStatus(tSNMP_OCTET_STRING_TYPE *pFwlUrlString , INT4 i4SetValFwlUrlFilterRowStatus)
{
   UNUSED_PARAM(pFwlUrlString);
   UNUSED_PARAM(i4SetValFwlUrlFilterRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlUrlFilterRowStatus
 Input       :  The Indices
                FwlUrlString

                The Object 
                testValFwlUrlFilterRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlUrlFilterRowStatus(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFwlUrlString , INT4 i4TestValFwlUrlFilterRowStatus)
{
   UNUSED_PARAM(pFwlUrlString);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlUrlFilterRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlUrlFilterTable
 Input       :  The Indices
                FwlUrlString
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlUrlFilterTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlStatInspectedPacketsCount
 Input       :  The Indices

                The Object 
                retValFwlStatInspectedPacketsCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatInspectedPacketsCount(UINT4 *pu4RetValFwlStatInspectedPacketsCount)
{
   UNUSED_PARAM(pu4RetValFwlStatInspectedPacketsCount);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatTotalPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatTotalPacketsDenied(UINT4 *pu4RetValFwlStatTotalPacketsDenied)
{
   UNUSED_PARAM(pu4RetValFwlStatTotalPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatTotalPacketsAccepted
 Input       :  The Indices

                The Object 
                retValFwlStatTotalPacketsAccepted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatTotalPacketsAccepted(UINT4 *pu4RetValFwlStatTotalPacketsAccepted)
{
   UNUSED_PARAM(pu4RetValFwlStatTotalPacketsAccepted);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatTotalIcmpPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalIcmpPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatTotalIcmpPacketsDenied(UINT4 *pu4RetValFwlStatTotalIcmpPacketsDenied)
{
   UNUSED_PARAM(pu4RetValFwlStatTotalIcmpPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatTotalSynPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalSynPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatTotalSynPacketsDenied(UINT4 *pu4RetValFwlStatTotalSynPacketsDenied)
{
   UNUSED_PARAM(pu4RetValFwlStatTotalSynPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatTotalIpSpoofedPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalIpSpoofedPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatTotalIpSpoofedPacketsDenied(UINT4 *pu4RetValFwlStatTotalIpSpoofedPacketsDenied)
{
   UNUSED_PARAM(pu4RetValFwlStatTotalIpSpoofedPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatTotalSrcRoutePacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalSrcRoutePacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatTotalSrcRoutePacketsDenied(UINT4 *pu4RetValFwlStatTotalSrcRoutePacketsDenied)
{
   UNUSED_PARAM(pu4RetValFwlStatTotalSrcRoutePacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatTotalTinyFragmentPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalTinyFragmentPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatTotalTinyFragmentPacketsDenied(UINT4 *pu4RetValFwlStatTotalTinyFragmentPacketsDenied)
{
   UNUSED_PARAM(pu4RetValFwlStatTotalTinyFragmentPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatTotalFragmentedPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalFragmentedPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatTotalFragmentedPacketsDenied(UINT4 *pu4RetValFwlStatTotalFragmentedPacketsDenied)
{
   UNUSED_PARAM(pu4RetValFwlStatTotalFragmentedPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatTotalLargeFragmentPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalLargeFragmentPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatTotalLargeFragmentPacketsDenied(UINT4 *pu4RetValFwlStatTotalLargeFragmentPacketsDenied)
{
   UNUSED_PARAM(pu4RetValFwlStatTotalLargeFragmentPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatTotalIpOptionPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalIpOptionPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatTotalIpOptionPacketsDenied(UINT4 *pu4RetValFwlStatTotalIpOptionPacketsDenied)
{
   UNUSED_PARAM(pu4RetValFwlStatTotalIpOptionPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatTotalAttacksPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalAttacksPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatTotalAttacksPacketsDenied(UINT4 *pu4RetValFwlStatTotalAttacksPacketsDenied)
{
   UNUSED_PARAM(pu4RetValFwlStatTotalAttacksPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatMemoryAllocationFailCount
 Input       :  The Indices

                The Object 
                retValFwlStatMemoryAllocationFailCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatMemoryAllocationFailCount(UINT4 *pu4RetValFwlStatMemoryAllocationFailCount)
{
   UNUSED_PARAM(pu4RetValFwlStatMemoryAllocationFailCount);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIPv6InspectedPacketsCount
 Input       :  The Indices

                The Object 
                retValFwlStatIPv6InspectedPacketsCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIPv6InspectedPacketsCount(UINT4 *pu4RetValFwlStatIPv6InspectedPacketsCount)
{
   UNUSED_PARAM(pu4RetValFwlStatIPv6InspectedPacketsCount);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIPv6TotalPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatIPv6TotalPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIPv6TotalPacketsDenied(UINT4 *pu4RetValFwlStatIPv6TotalPacketsDenied)
{
   UNUSED_PARAM(pu4RetValFwlStatIPv6TotalPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIPv6TotalPacketsAccepted
 Input       :  The Indices

                The Object 
                retValFwlStatIPv6TotalPacketsAccepted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIPv6TotalPacketsAccepted(UINT4 *pu4RetValFwlStatIPv6TotalPacketsAccepted)
{
   UNUSED_PARAM(pu4RetValFwlStatIPv6TotalPacketsAccepted);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIPv6TotalIcmpPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatIPv6TotalIcmpPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIPv6TotalIcmpPacketsDenied(UINT4 *pu4RetValFwlStatIPv6TotalIcmpPacketsDenied)
{
   UNUSED_PARAM(pu4RetValFwlStatIPv6TotalIcmpPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIPv6TotalSpoofedPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatIPv6TotalSpoofedPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIPv6TotalSpoofedPacketsDenied(UINT4 *pu4RetValFwlStatIPv6TotalSpoofedPacketsDenied)
{
   UNUSED_PARAM(pu4RetValFwlStatIPv6TotalSpoofedPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIPv6TotalAttacksPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatIPv6TotalAttacksPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIPv6TotalAttacksPacketsDenied(UINT4 *pu4RetValFwlStatIPv6TotalAttacksPacketsDenied)
{
   UNUSED_PARAM(pu4RetValFwlStatIPv6TotalAttacksPacketsDenied);
   return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FwlStateTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlStateTable
 Input       :  The Indices
                FwlStateType
                FwlStateLocalIpAddrType
                FwlStateLocalIpAddress
                FwlStateRemoteIpAddrType
                FwlStateRemoteIpAddress
                FwlStateLocalPort
                FwlStateRemotePort
                FwlStateProtocol
                FwlStateDirection
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFwlStateTable(INT4 i4FwlStateType , INT4 i4FwlStateLocalIpAddrType , tSNMP_OCTET_STRING_TYPE *pFwlStateLocalIpAddress , INT4 i4FwlStateRemoteIpAddrType , tSNMP_OCTET_STRING_TYPE *pFwlStateRemoteIpAddress , INT4 i4FwlStateLocalPort , INT4 i4FwlStateRemotePort , INT4 i4FwlStateProtocol , INT4 i4FwlStateDirection)
{
   UNUSED_PARAM(i4FwlStateType);
   UNUSED_PARAM(i4FwlStateLocalIpAddrType);
   UNUSED_PARAM(pFwlStateLocalIpAddress);
   UNUSED_PARAM(i4FwlStateRemoteIpAddrType);
   UNUSED_PARAM(pFwlStateRemoteIpAddress);
   UNUSED_PARAM(i4FwlStateLocalPort);
   UNUSED_PARAM(i4FwlStateRemotePort);
   UNUSED_PARAM(i4FwlStateProtocol);
   UNUSED_PARAM(i4FwlStateDirection);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFwlStateTable
 Input       :  The Indices
                FwlStateType
                FwlStateLocalIpAddrType
                FwlStateLocalIpAddress
                FwlStateRemoteIpAddrType
                FwlStateRemoteIpAddress
                FwlStateLocalPort
                FwlStateRemotePort
                FwlStateProtocol
                FwlStateDirection
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFwlStateTable(INT4 *pi4FwlStateType , INT4 *pi4FwlStateLocalIpAddrType , tSNMP_OCTET_STRING_TYPE * pFwlStateLocalIpAddress , INT4 *pi4FwlStateRemoteIpAddrType , tSNMP_OCTET_STRING_TYPE * pFwlStateRemoteIpAddress , INT4 *pi4FwlStateLocalPort , INT4 *pi4FwlStateRemotePort , INT4 *pi4FwlStateProtocol , INT4 *pi4FwlStateDirection)
{
   UNUSED_PARAM(pi4FwlStateType);
   UNUSED_PARAM(pi4FwlStateLocalIpAddrType);
   UNUSED_PARAM(pFwlStateLocalIpAddress);
   UNUSED_PARAM(pi4FwlStateRemoteIpAddrType);
   UNUSED_PARAM(pFwlStateRemoteIpAddress);
   UNUSED_PARAM(pi4FwlStateLocalPort);
   UNUSED_PARAM(pi4FwlStateRemotePort);
   UNUSED_PARAM(pi4FwlStateProtocol);
   UNUSED_PARAM(pi4FwlStateDirection);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFwlStateTable
 Input       :  The Indices
                FwlStateType
                nextFwlStateType
                FwlStateLocalIpAddrType
                nextFwlStateLocalIpAddrType
                FwlStateLocalIpAddress
                nextFwlStateLocalIpAddress
                FwlStateRemoteIpAddrType
                nextFwlStateRemoteIpAddrType
                FwlStateRemoteIpAddress
                nextFwlStateRemoteIpAddress
                FwlStateLocalPort
                nextFwlStateLocalPort
                FwlStateRemotePort
                nextFwlStateRemotePort
                FwlStateProtocol
                nextFwlStateProtocol
                FwlStateDirection
                nextFwlStateDirection
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFwlStateTable(INT4 i4FwlStateType ,INT4 *pi4NextFwlStateType  , INT4 i4FwlStateLocalIpAddrType ,INT4 *pi4NextFwlStateLocalIpAddrType  , tSNMP_OCTET_STRING_TYPE *pFwlStateLocalIpAddress ,tSNMP_OCTET_STRING_TYPE * pNextFwlStateLocalIpAddress  , INT4 i4FwlStateRemoteIpAddrType ,INT4 *pi4NextFwlStateRemoteIpAddrType  , tSNMP_OCTET_STRING_TYPE *pFwlStateRemoteIpAddress ,tSNMP_OCTET_STRING_TYPE * pNextFwlStateRemoteIpAddress  , INT4 i4FwlStateLocalPort ,INT4 *pi4NextFwlStateLocalPort  , INT4 i4FwlStateRemotePort ,INT4 *pi4NextFwlStateRemotePort  , INT4 i4FwlStateProtocol ,INT4 *pi4NextFwlStateProtocol  , INT4 i4FwlStateDirection ,INT4 *pi4NextFwlStateDirection )
{
   UNUSED_PARAM(i4FwlStateType);
   UNUSED_PARAM(pi4NextFwlStateType);
   UNUSED_PARAM(pi4NextFwlStateLocalIpAddrType);
   UNUSED_PARAM(i4FwlStateLocalIpAddrType);
   UNUSED_PARAM(pFwlStateLocalIpAddress);
   UNUSED_PARAM(pNextFwlStateLocalIpAddress);
   UNUSED_PARAM(i4FwlStateRemoteIpAddrType);
   UNUSED_PARAM(pi4NextFwlStateRemoteIpAddrType);
   UNUSED_PARAM(pFwlStateRemoteIpAddress);
   UNUSED_PARAM(pNextFwlStateRemoteIpAddress);
   UNUSED_PARAM(i4FwlStateLocalPort);
   UNUSED_PARAM(pi4NextFwlStateRemotePort);
   UNUSED_PARAM(i4FwlStateRemotePort);
   UNUSED_PARAM(pi4NextFwlStateLocalPort);
   UNUSED_PARAM(i4FwlStateProtocol);
   UNUSED_PARAM(pi4NextFwlStateProtocol);
   UNUSED_PARAM(i4FwlStateDirection);
   UNUSED_PARAM(pi4NextFwlStateDirection);
   return SNMP_FAILURE;

}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlStateEstablishedTime
 Input       :  The Indices
                FwlStateType
                FwlStateLocalIpAddrType
                FwlStateLocalIpAddress
                FwlStateRemoteIpAddrType
                FwlStateRemoteIpAddress
                FwlStateLocalPort
                FwlStateRemotePort
                FwlStateProtocol
                FwlStateDirection

                The Object 
                retValFwlStateEstablishedTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStateEstablishedTime(INT4 i4FwlStateType , INT4 i4FwlStateLocalIpAddrType , tSNMP_OCTET_STRING_TYPE *pFwlStateLocalIpAddress , INT4 i4FwlStateRemoteIpAddrType , tSNMP_OCTET_STRING_TYPE *pFwlStateRemoteIpAddress , INT4 i4FwlStateLocalPort , INT4 i4FwlStateRemotePort , INT4 i4FwlStateProtocol , INT4 i4FwlStateDirection , UINT4 *pu4RetValFwlStateEstablishedTime)
{
   UNUSED_PARAM(i4FwlStateType);
   UNUSED_PARAM(i4FwlStateLocalIpAddrType);
   UNUSED_PARAM(pFwlStateLocalIpAddress);
   UNUSED_PARAM(i4FwlStateRemoteIpAddrType);
   UNUSED_PARAM(pFwlStateRemoteIpAddress);
   UNUSED_PARAM(i4FwlStateLocalPort);
   UNUSED_PARAM(i4FwlStateRemotePort);
   UNUSED_PARAM(i4FwlStateProtocol);
   UNUSED_PARAM(i4FwlStateDirection);
   UNUSED_PARAM(pu4RetValFwlStateEstablishedTime);
   return SNMP_SUCCESS;
 }
/****************************************************************************
 Function    :  nmhGetFwlStateLocalState
 Input       :  The Indices
                FwlStateType
                FwlStateLocalIpAddrType
                FwlStateLocalIpAddress
                FwlStateRemoteIpAddrType
                FwlStateRemoteIpAddress
                FwlStateLocalPort
                FwlStateRemotePort
                FwlStateProtocol
                FwlStateDirection

                The Object 
                retValFwlStateLocalState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStateLocalState(INT4 i4FwlStateType , INT4 i4FwlStateLocalIpAddrType , tSNMP_OCTET_STRING_TYPE *pFwlStateLocalIpAddress , INT4 i4FwlStateRemoteIpAddrType , tSNMP_OCTET_STRING_TYPE *pFwlStateRemoteIpAddress , INT4 i4FwlStateLocalPort , INT4 i4FwlStateRemotePort , INT4 i4FwlStateProtocol , INT4 i4FwlStateDirection , INT4 *pi4RetValFwlStateLocalState)
{
   UNUSED_PARAM(i4FwlStateType);
   UNUSED_PARAM(i4FwlStateLocalIpAddrType);
   UNUSED_PARAM(pFwlStateLocalIpAddress);
   UNUSED_PARAM(i4FwlStateRemoteIpAddrType);
   UNUSED_PARAM(pFwlStateRemoteIpAddress);
   UNUSED_PARAM(i4FwlStateLocalPort);
   UNUSED_PARAM(i4FwlStateRemotePort);
   UNUSED_PARAM(i4FwlStateProtocol);
   UNUSED_PARAM(i4FwlStateDirection);
   UNUSED_PARAM(pi4RetValFwlStateLocalState);
   return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFwlStateRemoteState
 Input       :  The Indices
                FwlStateType
                FwlStateLocalIpAddrType
                FwlStateLocalIpAddress
                FwlStateRemoteIpAddrType
                FwlStateRemoteIpAddress
                FwlStateLocalPort
                FwlStateRemotePort
                FwlStateProtocol
                FwlStateDirection

                The Object 
                retValFwlStateRemoteState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStateRemoteState(INT4 i4FwlStateType , INT4 i4FwlStateLocalIpAddrType , tSNMP_OCTET_STRING_TYPE *pFwlStateLocalIpAddress , INT4 i4FwlStateRemoteIpAddrType , tSNMP_OCTET_STRING_TYPE *pFwlStateRemoteIpAddress , INT4 i4FwlStateLocalPort , INT4 i4FwlStateRemotePort , INT4 i4FwlStateProtocol , INT4 i4FwlStateDirection , INT4 *pi4RetValFwlStateRemoteState)
{
   UNUSED_PARAM(i4FwlStateType);
   UNUSED_PARAM(i4FwlStateLocalIpAddrType);
   UNUSED_PARAM(pFwlStateLocalIpAddress);
   UNUSED_PARAM(i4FwlStateRemoteIpAddrType);
   UNUSED_PARAM(pFwlStateRemoteIpAddress);
   UNUSED_PARAM(i4FwlStateLocalPort);
   UNUSED_PARAM(i4FwlStateRemotePort);
   UNUSED_PARAM(i4FwlStateProtocol);
   UNUSED_PARAM(i4FwlStateDirection);
   UNUSED_PARAM(pi4RetValFwlStateRemoteState);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStateLogLevel
 Input       :  The Indices
                FwlStateType
                FwlStateLocalIpAddrType
                FwlStateLocalIpAddress
                FwlStateRemoteIpAddrType
                FwlStateRemoteIpAddress
                FwlStateLocalPort
                FwlStateRemotePort
                FwlStateProtocol
                FwlStateDirection

                The Object 
                retValFwlStateLogLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStateLogLevel(INT4 i4FwlStateType , INT4 i4FwlStateLocalIpAddrType , tSNMP_OCTET_STRING_TYPE *pFwlStateLocalIpAddress , INT4 i4FwlStateRemoteIpAddrType , tSNMP_OCTET_STRING_TYPE *pFwlStateRemoteIpAddress , INT4 i4FwlStateLocalPort , INT4 i4FwlStateRemotePort , INT4 i4FwlStateProtocol , INT4 i4FwlStateDirection , INT4 *pi4RetValFwlStateLogLevel)
{
   UNUSED_PARAM(i4FwlStateType);
   UNUSED_PARAM(i4FwlStateLocalIpAddrType);
   UNUSED_PARAM(pFwlStateLocalIpAddress);
   UNUSED_PARAM(i4FwlStateRemoteIpAddrType);
   UNUSED_PARAM(pFwlStateRemoteIpAddress);
   UNUSED_PARAM(i4FwlStateLocalPort);
   UNUSED_PARAM(i4FwlStateRemotePort);
   UNUSED_PARAM(i4FwlStateProtocol);
   UNUSED_PARAM(i4FwlStateDirection);
   UNUSED_PARAM(pi4RetValFwlStateLogLevel);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStateCallStatus
 Input       :  The Indices
                FwlStateType
                FwlStateLocalIpAddrType
                FwlStateLocalIpAddress
                FwlStateRemoteIpAddrType
                FwlStateRemoteIpAddress
                FwlStateLocalPort
                FwlStateRemotePort
                FwlStateProtocol
                FwlStateDirection

                The Object 
                retValFwlStateCallStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStateCallStatus(INT4 i4FwlStateType , INT4 i4FwlStateLocalIpAddrType , tSNMP_OCTET_STRING_TYPE *pFwlStateLocalIpAddress , INT4 i4FwlStateRemoteIpAddrType , tSNMP_OCTET_STRING_TYPE *pFwlStateRemoteIpAddress , INT4 i4FwlStateLocalPort , INT4 i4FwlStateRemotePort , INT4 i4FwlStateProtocol , INT4 i4FwlStateDirection , INT4 *pi4RetValFwlStateCallStatus)
{
   UNUSED_PARAM(i4FwlStateType);
   UNUSED_PARAM(i4FwlStateLocalIpAddrType);
   UNUSED_PARAM(pFwlStateLocalIpAddress);
   UNUSED_PARAM(i4FwlStateRemoteIpAddrType);
   UNUSED_PARAM(pFwlStateRemoteIpAddress);
   UNUSED_PARAM(i4FwlStateLocalPort);
   UNUSED_PARAM(i4FwlStateRemotePort);
   UNUSED_PARAM(i4FwlStateProtocol);
   UNUSED_PARAM(i4FwlStateDirection);
   UNUSED_PARAM(pi4RetValFwlStateCallStatus);
   return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FwlStatIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlStatIfTable
 Input       :  The Indices
                FwlStatIfIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFwlStatIfTable(INT4 i4FwlStatIfIfIndex)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFwlStatIfTable
 Input       :  The Indices
                FwlStatIfIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFwlStatIfTable(INT4 *pi4FwlStatIfIfIndex)
{
   UNUSED_PARAM(pi4FwlStatIfIfIndex);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFwlStatIfTable
 Input       :  The Indices
                FwlStatIfIfIndex
                nextFwlStatIfIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFwlStatIfTable(INT4 i4FwlStatIfIfIndex ,INT4 *pi4NextFwlStatIfIfIndex )
{
   UNUSED_PARAM(pi4NextFwlStatIfIfIndex);
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlStatIfFilterCount
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfFilterCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIfFilterCount(INT4 i4FwlStatIfIfIndex , INT4 *pi4RetValFwlStatIfFilterCount)
{
   UNUSED_PARAM(pi4RetValFwlStatIfFilterCount);
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIfPacketsDenied
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIfPacketsDenied(INT4 i4FwlStatIfIfIndex , UINT4 *pu4RetValFwlStatIfPacketsDenied)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(pu4RetValFwlStatIfPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIfPacketsAccepted
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfPacketsAccepted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIfPacketsAccepted(INT4 i4FwlStatIfIfIndex , UINT4 *pu4RetValFwlStatIfPacketsAccepted)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(pu4RetValFwlStatIfPacketsAccepted);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIfSynPacketsDenied
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfSynPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIfSynPacketsDenied(INT4 i4FwlStatIfIfIndex , UINT4 *pu4RetValFwlStatIfSynPacketsDenied)
{
   UNUSED_PARAM(pu4RetValFwlStatIfSynPacketsDenied);
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIfIcmpPacketsDenied
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfIcmpPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIfIcmpPacketsDenied(INT4 i4FwlStatIfIfIndex , UINT4 *pu4RetValFwlStatIfIcmpPacketsDenied)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(pu4RetValFwlStatIfIcmpPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIfIpSpoofedPacketsDenied
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfIpSpoofedPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIfIpSpoofedPacketsDenied(INT4 i4FwlStatIfIfIndex , UINT4 *pu4RetValFwlStatIfIpSpoofedPacketsDenied)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(pu4RetValFwlStatIfIpSpoofedPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIfSrcRoutePacketsDenied
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfSrcRoutePacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIfSrcRoutePacketsDenied(INT4 i4FwlStatIfIfIndex , UINT4 *pu4RetValFwlStatIfSrcRoutePacketsDenied)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(pu4RetValFwlStatIfSrcRoutePacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIfTinyFragmentPacketsDenied
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfTinyFragmentPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIfTinyFragmentPacketsDenied(INT4 i4FwlStatIfIfIndex , UINT4 *pu4RetValFwlStatIfTinyFragmentPacketsDenied)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(pu4RetValFwlStatIfTinyFragmentPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIfFragmentPacketsDenied
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfFragmentPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIfFragmentPacketsDenied(INT4 i4FwlStatIfIfIndex , UINT4 *pu4RetValFwlStatIfFragmentPacketsDenied)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(pu4RetValFwlStatIfFragmentPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIfIpOptionPacketsDenied
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfIpOptionPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIfIpOptionPacketsDenied(INT4 i4FwlStatIfIfIndex , UINT4 *pu4RetValFwlStatIfIpOptionPacketsDenied)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(pu4RetValFwlStatIfIpOptionPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIfClear
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfClear
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIfClear(INT4 i4FwlStatIfIfIndex , INT4 *pi4RetValFwlStatIfClear)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(pi4RetValFwlStatIfClear);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlIfTrapThreshold
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlIfTrapThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlIfTrapThreshold(INT4 i4FwlStatIfIfIndex , INT4 *pi4RetValFwlIfTrapThreshold)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(pi4RetValFwlIfTrapThreshold);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIfIPv6PacketsDenied
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfIPv6PacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIfIPv6PacketsDenied(INT4 i4FwlStatIfIfIndex , UINT4 *pu4RetValFwlStatIfIPv6PacketsDenied)
{

   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(pu4RetValFwlStatIfIPv6PacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIfIPv6PacketsAccepted
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfIPv6PacketsAccepted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIfIPv6PacketsAccepted(INT4 i4FwlStatIfIfIndex , UINT4 *pu4RetValFwlStatIfIPv6PacketsAccepted)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(pu4RetValFwlStatIfIPv6PacketsAccepted);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIfIPv6IcmpPacketsDenied
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfIPv6IcmpPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIfIPv6IcmpPacketsDenied(INT4 i4FwlStatIfIfIndex , UINT4 *pu4RetValFwlStatIfIPv6IcmpPacketsDenied)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(pu4RetValFwlStatIfIPv6IcmpPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIfIPv6SpoofedPacketsDenied
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfIPv6SpoofedPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIfIPv6SpoofedPacketsDenied(INT4 i4FwlStatIfIfIndex , UINT4 *pu4RetValFwlStatIfIPv6SpoofedPacketsDenied)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(pu4RetValFwlStatIfIPv6SpoofedPacketsDenied);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatIfClearIPv6
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfClearIPv6
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatIfClearIPv6(INT4 i4FwlStatIfIfIndex , INT4 *pi4RetValFwlStatIfClearIPv6)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(pi4RetValFwlStatIfClearIPv6);
   return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlStatIfClear
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                setValFwlStatIfClear
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlStatIfClear(INT4 i4FwlStatIfIfIndex , INT4 i4SetValFwlStatIfClear)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(i4SetValFwlStatIfClear);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlIfTrapThreshold
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                setValFwlIfTrapThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlIfTrapThreshold(INT4 i4FwlStatIfIfIndex , INT4 i4SetValFwlIfTrapThreshold)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(i4SetValFwlIfTrapThreshold);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlStatIfClearIPv6
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                setValFwlStatIfClearIPv6
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlStatIfClearIPv6(INT4 i4FwlStatIfIfIndex , INT4 i4SetValFwlStatIfClearIPv6)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(i4SetValFwlStatIfClearIPv6);
   return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlStatIfClear
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                testValFwlStatIfClear
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlStatIfClear(UINT4 *pu4ErrorCode , INT4 i4FwlStatIfIfIndex , INT4 i4TestValFwlStatIfClear)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlStatIfClear);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlIfTrapThreshold
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                testValFwlIfTrapThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlIfTrapThreshold(UINT4 *pu4ErrorCode , INT4 i4FwlStatIfIfIndex , INT4 i4TestValFwlIfTrapThreshold)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlIfTrapThreshold);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlStatIfClearIPv6
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                testValFwlStatIfClearIPv6
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlStatIfClearIPv6(UINT4 *pu4ErrorCode , INT4 i4FwlStatIfIfIndex , INT4 i4TestValFwlStatIfClearIPv6)
{
   UNUSED_PARAM(i4FwlStatIfIfIndex);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlStatIfClearIPv6);
   return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlStatIfTable
 Input       :  The Indices
                FwlStatIfIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlStatIfTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlStatClear
 Input       :  The Indices

                The Object 
                retValFwlStatClear
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatClear(INT4 *pi4RetValFwlStatClear)
{
   UNUSED_PARAM(pi4RetValFwlStatClear);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlStatClearIPv6
 Input       :  The Indices

                The Object 
                retValFwlStatClearIPv6
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlStatClearIPv6(INT4 *pi4RetValFwlStatClearIPv6)
{
   UNUSED_PARAM(pi4RetValFwlStatClearIPv6);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlTrapThreshold
 Input       :  The Indices

                The Object 
                retValFwlTrapThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlTrapThreshold(INT4 *pi4RetValFwlTrapThreshold)
{
   UNUSED_PARAM(pi4RetValFwlTrapThreshold);
   return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlStatClear
 Input       :  The Indices

                The Object 
                setValFwlStatClear
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlStatClear(INT4 i4SetValFwlStatClear)
{
   UNUSED_PARAM(i4SetValFwlStatClear);
   return SNMP_SUCCESS;
   }
/****************************************************************************
 Function    :  nmhSetFwlStatClearIPv6
 Input       :  The Indices

                The Object 
                setValFwlStatClearIPv6
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlStatClearIPv6(INT4 i4SetValFwlStatClearIPv6)
{
   UNUSED_PARAM(i4SetValFwlStatClearIPv6);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlTrapThreshold
 Input       :  The Indices

                The Object 
                setValFwlTrapThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlTrapThreshold(INT4 i4SetValFwlTrapThreshold)
{
   UNUSED_PARAM(i4SetValFwlTrapThreshold);
   return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlStatClear
 Input       :  The Indices

                The Object 
                testValFwlStatClear
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlStatClear(UINT4 *pu4ErrorCode , INT4 i4TestValFwlStatClear)
{
   UNUSED_PARAM(i4TestValFwlStatClear);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlStatClearIPv6
 Input       :  The Indices

                The Object 
                testValFwlStatClearIPv6
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlStatClearIPv6(UINT4 *pu4ErrorCode , INT4 i4TestValFwlStatClearIPv6)
{
   UNUSED_PARAM(i4TestValFwlStatClearIPv6);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlTrapThreshold
 Input       :  The Indices

                The Object 
                testValFwlTrapThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlTrapThreshold(UINT4 *pu4ErrorCode , INT4 i4TestValFwlTrapThreshold)
{
   UNUSED_PARAM(i4TestValFwlTrapThreshold);
   UNUSED_PARAM(pu4ErrorCode);
   return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlStatClear
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlStatClear(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlStatClearIPv6
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlStatClearIPv6(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlTrapThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlTrapThreshold(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlTrapMemFailMessage
 Input       :  The Indices

                The Object 
                retValFwlTrapMemFailMessage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlTrapMemFailMessage(tSNMP_OCTET_STRING_TYPE * pRetValFwlTrapMemFailMessage)
{
   UNUSED_PARAM(pRetValFwlTrapMemFailMessage);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlTrapAttackMessage
 Input       :  The Indices

                The Object 
                retValFwlTrapAttackMessage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlTrapAttackMessage(tSNMP_OCTET_STRING_TYPE * pRetValFwlTrapAttackMessage)
{
   UNUSED_PARAM(pRetValFwlTrapAttackMessage);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlTrapFileName
 Input       :  The Indices

                The Object 
                retValFwlTrapFileName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlTrapFileName(tSNMP_OCTET_STRING_TYPE * pRetValFwlTrapFileName)
{
   UNUSED_PARAM(pRetValFwlTrapFileName);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlIdsTrapFileName
 Input       :  The Indices

                The Object 
                retValFwlIdsTrapFileName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlIdsTrapFileName(tSNMP_OCTET_STRING_TYPE * pRetValFwlIdsTrapFileName)
{
   UNUSED_PARAM(pRetValFwlIdsTrapFileName);
   return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlTrapMemFailMessage
 Input       :  The Indices

                The Object 
                setValFwlTrapMemFailMessage
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlTrapMemFailMessage(tSNMP_OCTET_STRING_TYPE *pSetValFwlTrapMemFailMessage)
{
   UNUSED_PARAM(pSetValFwlTrapMemFailMessage);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlTrapAttackMessage
 Input       :  The Indices

                The Object 
                setValFwlTrapAttackMessage
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlTrapAttackMessage(tSNMP_OCTET_STRING_TYPE *pSetValFwlTrapAttackMessage)
{
   UNUSED_PARAM(pSetValFwlTrapAttackMessage);
   return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlTrapMemFailMessage
 Input       :  The Indices

                The Object 
                testValFwlTrapMemFailMessage
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlTrapMemFailMessage(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pTestValFwlTrapMemFailMessage)
{
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(pTestValFwlTrapMemFailMessage);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlTrapAttackMessage
 Input       :  The Indices

                The Object 
                testValFwlTrapAttackMessage
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlTrapAttackMessage(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pTestValFwlTrapAttackMessage)
{
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(pTestValFwlTrapAttackMessage);
   return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlTrapMemFailMessage
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlTrapMemFailMessage(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FwlTrapAttackMessage
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlTrapAttackMessage(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FwlDefnBlkListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlDefnBlkListTable
 Input       :  The Indices
                FwlBlkListIpAddressType
                FwlBlkListIpAddress
                FwlBlkListIpMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFwlDefnBlkListTable(INT4 i4FwlBlkListIpAddressType , tSNMP_OCTET_STRING_TYPE *pFwlBlkListIpAddress , UINT4 u4FwlBlkListIpMask)
{
   UNUSED_PARAM(i4FwlBlkListIpAddressType);
   UNUSED_PARAM(pFwlBlkListIpAddress);
   UNUSED_PARAM(u4FwlBlkListIpMask);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFwlDefnBlkListTable
 Input       :  The Indices
                FwlBlkListIpAddressType
                FwlBlkListIpAddress
                FwlBlkListIpMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFwlDefnBlkListTable(INT4 *pi4FwlBlkListIpAddressType , tSNMP_OCTET_STRING_TYPE * pFwlBlkListIpAddress , UINT4 *pu4FwlBlkListIpMask)
{
   UNUSED_PARAM(pi4FwlBlkListIpAddressType);
   UNUSED_PARAM(pFwlBlkListIpAddress);
   UNUSED_PARAM(pu4FwlBlkListIpMask);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFwlDefnBlkListTable
 Input       :  The Indices
                FwlBlkListIpAddressType
                nextFwlBlkListIpAddressType
                FwlBlkListIpAddress
                nextFwlBlkListIpAddress
                FwlBlkListIpMask
                nextFwlBlkListIpMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFwlDefnBlkListTable(INT4 i4FwlBlkListIpAddressType ,INT4 *pi4NextFwlBlkListIpAddressType  , tSNMP_OCTET_STRING_TYPE *pFwlBlkListIpAddress ,tSNMP_OCTET_STRING_TYPE * pNextFwlBlkListIpAddress  , UINT4 u4FwlBlkListIpMask ,UINT4 *pu4NextFwlBlkListIpMask )
{
   UNUSED_PARAM(i4FwlBlkListIpAddressType);
   UNUSED_PARAM(pi4NextFwlBlkListIpAddressType);
   UNUSED_PARAM(pFwlBlkListIpAddress);
   UNUSED_PARAM(pNextFwlBlkListIpAddress);
   UNUSED_PARAM(u4FwlBlkListIpMask);
   UNUSED_PARAM(pu4NextFwlBlkListIpMask);
   return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlBlkListHitsCount
 Input       :  The Indices
                FwlBlkListIpAddressType
                FwlBlkListIpAddress
                FwlBlkListIpMask

                The Object 
                retValFwlBlkListHitsCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlBlkListHitsCount(INT4 i4FwlBlkListIpAddressType , tSNMP_OCTET_STRING_TYPE *pFwlBlkListIpAddress , UINT4 u4FwlBlkListIpMask , UINT4 *pu4RetValFwlBlkListHitsCount)
{
   UNUSED_PARAM(i4FwlBlkListIpAddressType);
   UNUSED_PARAM(pFwlBlkListIpAddress);
   UNUSED_PARAM(u4FwlBlkListIpMask);
   UNUSED_PARAM(pu4RetValFwlBlkListHitsCount);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlBlkListEntryType
 Input       :  The Indices
                FwlBlkListIpAddressType
                FwlBlkListIpAddress
                FwlBlkListIpMask

                The Object 
                retValFwlBlkListEntryType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlBlkListEntryType(INT4 i4FwlBlkListIpAddressType , tSNMP_OCTET_STRING_TYPE *pFwlBlkListIpAddress , UINT4 u4FwlBlkListIpMask , INT4 *pi4RetValFwlBlkListEntryType)
{
   UNUSED_PARAM(i4FwlBlkListIpAddressType);
   UNUSED_PARAM(pFwlBlkListIpAddress);
   UNUSED_PARAM(u4FwlBlkListIpMask);
   UNUSED_PARAM(pi4RetValFwlBlkListEntryType);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlBlkListRowStatus
 Input       :  The Indices
                FwlBlkListIpAddressType
                FwlBlkListIpAddress
                FwlBlkListIpMask

                The Object 
                retValFwlBlkListRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlBlkListRowStatus(INT4 i4FwlBlkListIpAddressType , tSNMP_OCTET_STRING_TYPE *pFwlBlkListIpAddress , UINT4 u4FwlBlkListIpMask , INT4 *pi4RetValFwlBlkListRowStatus)
{
   UNUSED_PARAM(i4FwlBlkListIpAddressType);
   UNUSED_PARAM(pFwlBlkListIpAddress);
   UNUSED_PARAM(u4FwlBlkListIpMask);
   UNUSED_PARAM(pi4RetValFwlBlkListRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlBlkListRowStatus
 Input       :  The Indices
                FwlBlkListIpAddressType
                FwlBlkListIpAddress
                FwlBlkListIpMask

                The Object 
                setValFwlBlkListRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlBlkListRowStatus(INT4 i4FwlBlkListIpAddressType , tSNMP_OCTET_STRING_TYPE *pFwlBlkListIpAddress , UINT4 u4FwlBlkListIpMask , INT4 i4SetValFwlBlkListRowStatus)
{
   UNUSED_PARAM(i4FwlBlkListIpAddressType);
   UNUSED_PARAM(pFwlBlkListIpAddress);
   UNUSED_PARAM(u4FwlBlkListIpMask);
   UNUSED_PARAM(i4SetValFwlBlkListRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlBlkListRowStatus
 Input       :  The Indices
                FwlBlkListIpAddressType
                FwlBlkListIpAddress
                FwlBlkListIpMask

                The Object 
                testValFwlBlkListRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlBlkListRowStatus(UINT4 *pu4ErrorCode , INT4 i4FwlBlkListIpAddressType , tSNMP_OCTET_STRING_TYPE *pFwlBlkListIpAddress , UINT4 u4FwlBlkListIpMask , INT4 i4TestValFwlBlkListRowStatus)
{
   UNUSED_PARAM(i4FwlBlkListIpAddressType);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(pFwlBlkListIpAddress);
   UNUSED_PARAM(u4FwlBlkListIpMask);
   UNUSED_PARAM(i4TestValFwlBlkListRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlDefnBlkListTable
 Input       :  The Indices
                FwlBlkListIpAddressType
                FwlBlkListIpAddress
                FwlBlkListIpMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlDefnBlkListTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FwlDefnWhiteListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlDefnWhiteListTable
 Input       :  The Indices
                FwlWhiteListIpAddressType
                FwlWhiteListIpAddress
                FwlWhiteListIpMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFwlDefnWhiteListTable(INT4 i4FwlWhiteListIpAddressType , tSNMP_OCTET_STRING_TYPE *pFwlWhiteListIpAddress , UINT4 u4FwlWhiteListIpMask)
{
   UNUSED_PARAM(i4FwlWhiteListIpAddressType);
   UNUSED_PARAM(pFwlWhiteListIpAddress);
   UNUSED_PARAM(u4FwlWhiteListIpMask);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFwlDefnWhiteListTable
 Input       :  The Indices
                FwlWhiteListIpAddressType
                FwlWhiteListIpAddress
                FwlWhiteListIpMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFwlDefnWhiteListTable(INT4 *pi4FwlWhiteListIpAddressType , tSNMP_OCTET_STRING_TYPE * pFwlWhiteListIpAddress , UINT4 *pu4FwlWhiteListIpMask)
{
   UNUSED_PARAM(pi4FwlWhiteListIpAddressType);
   UNUSED_PARAM(pFwlWhiteListIpAddress);
   UNUSED_PARAM(pu4FwlWhiteListIpMask);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFwlDefnWhiteListTable
 Input       :  The Indices
                FwlWhiteListIpAddressType
                nextFwlWhiteListIpAddressType
                FwlWhiteListIpAddress
                nextFwlWhiteListIpAddress
                FwlWhiteListIpMask
                nextFwlWhiteListIpMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFwlDefnWhiteListTable(INT4 i4FwlWhiteListIpAddressType ,INT4 *pi4NextFwlWhiteListIpAddressType  , tSNMP_OCTET_STRING_TYPE *pFwlWhiteListIpAddress ,tSNMP_OCTET_STRING_TYPE * pNextFwlWhiteListIpAddress  , UINT4 u4FwlWhiteListIpMask ,UINT4 *pu4NextFwlWhiteListIpMask )
{
   UNUSED_PARAM(i4FwlWhiteListIpAddressType);
   UNUSED_PARAM(pi4NextFwlWhiteListIpAddressType);
   UNUSED_PARAM(pNextFwlWhiteListIpAddress);
   UNUSED_PARAM(pFwlWhiteListIpAddress);
   UNUSED_PARAM(pu4NextFwlWhiteListIpMask);
   UNUSED_PARAM(u4FwlWhiteListIpMask);
   return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlWhiteListHitsCount
 Input       :  The Indices
                FwlWhiteListIpAddressType
                FwlWhiteListIpAddress
                FwlWhiteListIpMask

                The Object 
                retValFwlWhiteListHitsCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlWhiteListHitsCount(INT4 i4FwlWhiteListIpAddressType , tSNMP_OCTET_STRING_TYPE *pFwlWhiteListIpAddress , UINT4 u4FwlWhiteListIpMask , UINT4 *pu4RetValFwlWhiteListHitsCount)
{
   UNUSED_PARAM(i4FwlWhiteListIpAddressType);
   UNUSED_PARAM(pFwlWhiteListIpAddress);
   UNUSED_PARAM(u4FwlWhiteListIpMask);
   UNUSED_PARAM(pu4RetValFwlWhiteListHitsCount);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlWhiteListRowStatus
 Input       :  The Indices
                FwlWhiteListIpAddressType
                FwlWhiteListIpAddress
                FwlWhiteListIpMask

                The Object 
                retValFwlWhiteListRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlWhiteListRowStatus(INT4 i4FwlWhiteListIpAddressType , tSNMP_OCTET_STRING_TYPE *pFwlWhiteListIpAddress , UINT4 u4FwlWhiteListIpMask , INT4 *pi4RetValFwlWhiteListRowStatus)
{
   UNUSED_PARAM(i4FwlWhiteListIpAddressType);
   UNUSED_PARAM(pFwlWhiteListIpAddress);
   UNUSED_PARAM(u4FwlWhiteListIpMask);
   UNUSED_PARAM(pi4RetValFwlWhiteListRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlWhiteListRowStatus
 Input       :  The Indices
                FwlWhiteListIpAddressType
                FwlWhiteListIpAddress
                FwlWhiteListIpMask

                The Object 
                setValFwlWhiteListRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlWhiteListRowStatus(INT4 i4FwlWhiteListIpAddressType , tSNMP_OCTET_STRING_TYPE *pFwlWhiteListIpAddress , UINT4 u4FwlWhiteListIpMask , INT4 i4SetValFwlWhiteListRowStatus)
{
   UNUSED_PARAM(i4FwlWhiteListIpAddressType);
   UNUSED_PARAM(pFwlWhiteListIpAddress);
   UNUSED_PARAM(u4FwlWhiteListIpMask);
   UNUSED_PARAM(i4SetValFwlWhiteListRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlWhiteListRowStatus
 Input       :  The Indices
                FwlWhiteListIpAddressType
                FwlWhiteListIpAddress
                FwlWhiteListIpMask

                The Object 
                testValFwlWhiteListRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlWhiteListRowStatus(UINT4 *pu4ErrorCode , INT4 i4FwlWhiteListIpAddressType , tSNMP_OCTET_STRING_TYPE *pFwlWhiteListIpAddress , UINT4 u4FwlWhiteListIpMask , INT4 i4TestValFwlWhiteListRowStatus)
{
   UNUSED_PARAM(i4FwlWhiteListIpAddressType);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(pFwlWhiteListIpAddress);
   UNUSED_PARAM(u4FwlWhiteListIpMask);
   UNUSED_PARAM(i4TestValFwlWhiteListRowStatus);
   return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlDefnWhiteListTable
 Input       :  The Indices
                FwlWhiteListIpAddressType
                FwlWhiteListIpAddress
                FwlWhiteListIpMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlDefnWhiteListTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FwlDefnIPv6DmzTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlDefnIPv6DmzTable
 Input       :  The Indices
                FwlDmzIpv6Index
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFwlDefnIPv6DmzTable(tSNMP_OCTET_STRING_TYPE *pFwlDmzIpv6Index)
{
   UNUSED_PARAM(pFwlDmzIpv6Index);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFwlDefnIPv6DmzTable
 Input       :  The Indices
                FwlDmzIpv6Index
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFwlDefnIPv6DmzTable(tSNMP_OCTET_STRING_TYPE * pFwlDmzIpv6Index)
{
   UNUSED_PARAM(pFwlDmzIpv6Index);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFwlDefnIPv6DmzTable
 Input       :  The Indices
                FwlDmzIpv6Index
                nextFwlDmzIpv6Index
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFwlDefnIPv6DmzTable(tSNMP_OCTET_STRING_TYPE *pFwlDmzIpv6Index ,tSNMP_OCTET_STRING_TYPE * pNextFwlDmzIpv6Index )
{
   UNUSED_PARAM(pFwlDmzIpv6Index);
   UNUSED_PARAM(pNextFwlDmzIpv6Index);
   return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlDmzAddressType
 Input       :  The Indices
                FwlDmzIpv6Index

                The Object 
                retValFwlDmzAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlDmzAddressType(tSNMP_OCTET_STRING_TYPE *pFwlDmzIpv6Index , INT4 *pi4RetValFwlDmzAddressType)
{
   UNUSED_PARAM(pFwlDmzIpv6Index);
   UNUSED_PARAM(pi4RetValFwlDmzAddressType);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFwlDmzIpv6RowStatus
 Input       :  The Indices
                FwlDmzIpv6Index

                The Object 
                retValFwlDmzIpv6RowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlDmzIpv6RowStatus(tSNMP_OCTET_STRING_TYPE *pFwlDmzIpv6Index , INT4 *pi4RetValFwlDmzIpv6RowStatus)
{
   UNUSED_PARAM(pFwlDmzIpv6Index);
   UNUSED_PARAM(pi4RetValFwlDmzIpv6RowStatus);
   return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlDmzAddressType
 Input       :  The Indices
                FwlDmzIpv6Index

                The Object 
                setValFwlDmzAddressType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlDmzAddressType(tSNMP_OCTET_STRING_TYPE *pFwlDmzIpv6Index , INT4 i4SetValFwlDmzAddressType)
{
   UNUSED_PARAM(pFwlDmzIpv6Index);
   UNUSED_PARAM(i4SetValFwlDmzAddressType);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFwlDmzIpv6RowStatus
 Input       :  The Indices
                FwlDmzIpv6Index

                The Object 
                setValFwlDmzIpv6RowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlDmzIpv6RowStatus(tSNMP_OCTET_STRING_TYPE *pFwlDmzIpv6Index , INT4 i4SetValFwlDmzIpv6RowStatus)
{
   UNUSED_PARAM(pFwlDmzIpv6Index);
   UNUSED_PARAM(i4SetValFwlDmzIpv6RowStatus);
   return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlDmzAddressType
 Input       :  The Indices
                FwlDmzIpv6Index

                The Object 
                testValFwlDmzAddressType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlDmzAddressType(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFwlDmzIpv6Index , INT4 i4TestValFwlDmzAddressType)
{
   UNUSED_PARAM(pFwlDmzIpv6Index);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlDmzAddressType);
   return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FwlDmzIpv6RowStatus
 Input       :  The Indices
                FwlDmzIpv6Index

                The Object 
                testValFwlDmzIpv6RowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlDmzIpv6RowStatus(UINT4 *pu4ErrorCode , tSNMP_OCTET_STRING_TYPE *pFwlDmzIpv6Index , INT4 i4TestValFwlDmzIpv6RowStatus)
{
   UNUSED_PARAM(pFwlDmzIpv6Index);
   UNUSED_PARAM(pu4ErrorCode);
   UNUSED_PARAM(i4TestValFwlDmzIpv6RowStatus);
   return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlDefnIPv6DmzTable
 Input       :  The Indices
                FwlDmzIpv6Index
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlDefnIPv6DmzTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlSnorkTable
 Input       :  The Indices
                FwlSnorkPortNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFwlSnorkTable(INT4 i4FwlSnorkPortNo)
{
    tFwlSnorkInfo        *pSnorkNode = NULL;
    INT1                i1Status = FWL_ZERO;

    pSnorkNode = (tFwlSnorkInfo *) NULL;
    i1Status = SNMP_FAILURE;

        /* Check whether the filter name exists or not */
        pSnorkNode = FwlDbaseSearchSnork (i4FwlSnorkPortNo);
        if (pSnorkNode != NULL)
        {
            i1Status = SNMP_SUCCESS;
        }

    return i1Status;

}
/****************************************************************************
 Function    :  nmhGetFirstIndexFwlSnorkTable
 Input       :  The Indices
                FwlSnorkPortNo
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFwlSnorkTable(INT4 *pi4FwlSnorkPortNo)
{
     tFwlSnorkInfo        *pSnorkNode = NULL;
     pSnorkNode = (tFwlSnorkInfo *) NULL;
     INT1                i1Status = SNMP_FAILURE;
     pSnorkNode = (tFwlSnorkInfo *) TMO_SLL_First (&gFwlSnorkList);
     if(pSnorkNode != NULL)
	 {
	  	*pi4FwlSnorkPortNo = pSnorkNode->i4PortNo;
         i1Status = SNMP_SUCCESS;
	 }

return i1Status;
     
}
/****************************************************************************
 Function    :  nmhGetNextIndexFwlSnorkTable
 Input       :  The Indices
                FwlSnorkPortNo
                nextFwlSnorkPortNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFwlSnorkTable(INT4 i4FwlSnorkPortNo ,INT4 *pi4NextFwlSnorkPortNo )
{
   tFwlSnorkInfo        *pSnorkNode = NULL;
   tFwlSnorkInfo        *pNextSnorkNode = NULL;
     pSnorkNode = (tFwlSnorkInfo *) NULL;
     INT1                i1Status = SNMP_FAILURE;

    pSnorkNode = FwlDbaseSearchSnork (i4FwlSnorkPortNo);
    if (pSnorkNode != NULL)
    {
         pNextSnorkNode  = (tFwlSnorkInfo *) TMO_SLL_Next (&gFwlSnorkList,
                                                        (tTMO_SLL_NODE *)
                                                        pSnorkNode);

        if (pNextSnorkNode != NULL)
        {
			*pi4NextFwlSnorkPortNo = pNextSnorkNode->i4PortNo;
            i1Status = SNMP_SUCCESS;
        }
    }

   
return i1Status;

 
}
/****************************************************************************
 Function    :  nmhGetFwlSnorkRowStatus
 Input       :  The Indices
                FwlSnorkPortNo

                The Object
                retValFwlSnorkRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlSnorkRowStatus(INT4 i4FwlSnorkPortNo , INT4 *pi4RetValFwlSnorkRowStatus)
{
    tFwlSnorkInfo       *pSnorkNode = NULL;
    pSnorkNode  = (tFwlSnorkInfo *) NULL;
    pSnorkNode  = FwlDbaseSearchSnork (i4FwlSnorkPortNo);
    if (pSnorkNode == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFwlSnorkRowStatus = pSnorkNode->i4RowStatus;
return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlSnorkRowStatus
 Input       :  The Indices
                FwlSnorkPortNo

                The Object
                setValFwlSnorkRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlSnorkRowStatus(INT4 i4FwlSnorkPortNo , INT4 i4SetValFwlSnorkRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tFwlSnorkInfo       *pSnorkNode = NULL;
     UINT4               u4Status = FWL_FAILURE;
    CHR1                ac1Command[FWL_DOS_LINE_LEN];

    MEMSET (ac1Command, 0, sizeof (ac1Command));


    pSnorkNode = FwlDbaseSearchSnork (i4FwlSnorkPortNo);

    if ((NULL != pSnorkNode) &&
        ( pSnorkNode->i4RowStatus == i4SetValFwlSnorkRowStatus))
    {
        return SNMP_SUCCESS;
    }
    switch (i4SetValFwlSnorkRowStatus)
    {
        case FWL_CREATE_AND_WAIT:
            if (pSnorkNode != NULL)
            {
                i1Status = SNMP_SUCCESS;
                break;
            }
            /* Memory Allocation and Initialisation of Filter Node .  */
            if (FwlSnorkMemAllocate ((UINT1 **) (VOID *) &pSnorkNode) ==
                FWL_SUCCESS)
            {
                   pSnorkNode->i4PortNo = i4FwlSnorkPortNo;
                   pSnorkNode->i4RowStatus = i4SetValFwlSnorkRowStatus;
				   TMO_SLL_Init_Node (&pSnorkNode->nextSnorkInfo);
                   FwlDbaseAddSnork (pSnorkNode); 
                   i1Status = SNMP_SUCCESS;
            }
			else
			{
			   return SNMP_FAILURE;	
			}
            break;
	    case FWL_DESTROY:
            if (pSnorkNode == NULL)
            {
                break;
            }
            /*  Search the Node in the Filter List and
            *   delete it if reference count is Zero.
                  */

            if (pSnorkNode->i4RowStatus == FWL_ACTIVE)
            {
               u4Status = FwlDbaseDeleteSnork (i4FwlSnorkPortNo);
#ifdef LNXIP4_WANTED
             SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
             "%s INPUT -p udp --sport %d --dport %d %s",
                FWL_IPTBL_DEL,i4FwlSnorkPortNo,i4FwlSnorkPortNo,FWL_IPTBL_DROP);
             system (ac1Command);
#endif
#ifdef NPAPI_WANTED
             if(gFwlSnorkList.u4_Count == FWL_ZERO)
			 {
                if(FwlNpEnableDosAttackCust(FWL_DOS_SNORK_ATTACK,FWL_ZERO)!= SNMP_SUCCESS)
                {
                 return (SNMP_FAILURE);
                }
			}
#endif
            }

            if (u4Status == FWL_SUCCESS)
            {
                i1Status = SNMP_SUCCESS;

            }

            break;
         case FWL_ACTIVE:
            if (pSnorkNode == NULL)
            {
                break;
            }
             pSnorkNode->i4RowStatus = FWL_ACTIVE;
#ifdef LNXIP4_WANTED
             SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
             "%s INPUT -p udp --sport %d --dport %d %s",
                FWL_IPTBL_ADD,i4FwlSnorkPortNo,i4FwlSnorkPortNo,FWL_IPTBL_DROP);
             system (ac1Command);
#endif
#ifdef NPAPI_WANTED
             if(gFwlSnorkList.u4_Count == FWL_ONE) 
             {
               if(FwlNpEnableDosAttackCust(FWL_DOS_SNORK_ATTACK,FWL_ONE)!= SNMP_SUCCESS)
               {
                return (SNMP_FAILURE);

               }
			 }
#endif
            i1Status = SNMP_SUCCESS;
            break;
        default:
            i1Status = SNMP_FAILURE;
            break;
   }
 return i1Status;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlSnorkRowStatus
 Input       :  The Indices
                FwlSnorkPortNo

                The Object
                testValFwlSnorkRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlSnorkRowStatus(UINT4 *pu4ErrorCode , INT4 i4FwlSnorkPortNo , INT4 i4TestValFwlSnorkRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tFwlSnorkInfo        *pSnorkNode = NULL;

    pSnorkNode = (tFwlSnorkInfo *) NULL;

   pSnorkNode = FwlDbaseSearchSnork (i4FwlSnorkPortNo);

    if ((NULL != pSnorkNode) &&
        ( pSnorkNode->i4RowStatus == i4TestValFwlSnorkRowStatus))
    {
        return SNMP_SUCCESS;
    }
     switch (i4TestValFwlSnorkRowStatus)
        {
            case FWL_CREATE_AND_WAIT:
                /* If the index doesnt exist already, then it can be created. */
                if (pSnorkNode == NULL)
                {
                    i1Status = SNMP_SUCCESS;
                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                     i1Status = SNMP_FAILURE;
                }
                break;
              case FWL_NOT_IN_SERVICE:
                /* The row can be made FWL_NOT_IN_SERVICE if the row exist. */
                if (pSnorkNode != NULL)
                {
                    if (pSnorkNode->i4RowStatus == FWL_ACTIVE)
                    {
                        i1Status = SNMP_SUCCESS;
                    }
                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                }
                break;
                case FWL_ACTIVE:
                if (pSnorkNode != NULL)
                {
                        i1Status = SNMP_SUCCESS;

                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                }
                break;
            case FWL_DESTROY:
                if (pSnorkNode != NULL) 
                {
                    i1Status = SNMP_SUCCESS;
                }
                else
                {
                    if (pSnorkNode == NULL)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                        CLI_SET_ERR (CLI_FWL_NO_SUCH_ACL);
                    }
                    else
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (CLI_FWL_REFERENCED_FILTER);
                    }
                }
                break;
            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i1Status = SNMP_FAILURE;
       }


return i1Status;

}
/****************************************************************************
 Function    :  nmhDepv2FwlSnorkTable
 Input       :  The Indices
                FwlSnorkPortNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlSnorkTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}


/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlRpfTable
 Input       :  The Indices
                FwlRpfInIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFwlRpfTable(INT4 i4FwlRpfInIndex)
{
      tFwlRpfEntryInfo        *pRpfEntryNode = NULL;
      INT1                     i1Status = SNMP_FAILURE;


        /* Check whether the filter name exists or not */
        pRpfEntryNode = FwlDbaseSearchRpfEntry (i4FwlRpfInIndex);
        if (pRpfEntryNode != NULL)
        {
            i1Status = SNMP_SUCCESS;
        }

    return i1Status;

}
/****************************************************************************
 Function    :  nmhGetFirstIndexFwlRpfTable
 Input       :  The Indices
                FwlRpfInIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFwlRpfTable(INT4 *pi4FwlRpfInIndex)
{
    tFwlRpfEntryInfo        *pRpfEntryNode = NULL;
     INT1                i1Status = SNMP_FAILURE;
     pRpfEntryNode = (tFwlRpfEntryInfo *) TMO_SLL_First (&gFwlRpfEntryList);
     if(pRpfEntryNode != NULL)
     {
        *pi4FwlRpfInIndex = pRpfEntryNode->i4IfIndex;
         i1Status = SNMP_SUCCESS;
     }

return i1Status;


}
/****************************************************************************
 Function    :  nmhGetNextIndexFwlRpfTable
 Input       :  The Indices
                FwlRpfInIndex
                nextFwlRpfInIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFwlRpfTable(INT4 i4FwlRpfInIndex ,INT4 *pi4NextFwlRpfInIndex )
{
    tFwlRpfEntryInfo        *pRpfEntryNode = NULL;
    tFwlRpfEntryInfo        *pNextRpfEntryNode = NULL;
     INT1                i1Status = SNMP_FAILURE;

    pRpfEntryNode = FwlDbaseSearchRpfEntry (i4FwlRpfInIndex);
    if (pRpfEntryNode != NULL)
    {
         pNextRpfEntryNode  = (tFwlRpfEntryInfo *) TMO_SLL_Next (&gFwlRpfEntryList,
                                                        (tTMO_SLL_NODE *)
                                                        pRpfEntryNode);

        if (pNextRpfEntryNode != NULL)
        {
            *pi4NextFwlRpfInIndex = pNextRpfEntryNode->i4IfIndex;
            i1Status = SNMP_SUCCESS;
        }
    }


return i1Status;


}
/* Low Level GET Routine for All Objects  */


/****************************************************************************
 Function    :  nmhGetFwlRpfMode
 Input       :  The Indices
                FwlRpfInIndex

                The Object
                retValFwlRpfMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlRpfMode(INT4 i4FwlRpfInIndex , INT4 *pi4RetValFwlRpfMode)
{
    tFwlRpfEntryInfo       *pRpfEntryNode = NULL;
    pRpfEntryNode  = FwlDbaseSearchRpfEntry (i4FwlRpfInIndex);
    if (pRpfEntryNode == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFwlRpfMode = pRpfEntryNode->i4RpfMode;
return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFwlRpfRowStatus
 Input       :  The Indices
                FwlRpfInIndex

                The Object
                retValFwlRpfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFwlRpfRowStatus(INT4 i4FwlRpfInIndex , INT4 *pi4RetValFwlRpfRowStatus)
{
    tFwlRpfEntryInfo       *pRpfEntryNode = NULL;
    pRpfEntryNode  = FwlDbaseSearchRpfEntry (i4FwlRpfInIndex);
    if (pRpfEntryNode == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFwlRpfRowStatus = pRpfEntryNode->i4RowStatus;
return SNMP_SUCCESS;

}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlRpfMode
 Input       :  The Indices
                FwlRpfInIndex

                The Object
                setValFwlRpfMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlRpfMode(INT4 i4FwlRpfInIndex , INT4 i4SetValFwlRpfMode)
{
    tFwlRpfEntryInfo       *pRpfEntryNode = NULL;
    pRpfEntryNode  = FwlDbaseSearchRpfEntry (i4FwlRpfInIndex);
    if (pRpfEntryNode == NULL)
    {
        return (SNMP_FAILURE);
    }
    pRpfEntryNode->i4RpfMode = i4SetValFwlRpfMode;
return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetFwlRpfRowStatus
 Input       :  The Indices
                FwlRpfInIndex

t
                The Object
                setValFwlRpfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFwlRpfRowStatus(INT4 i4FwlRpfInIndex , INT4 i4SetValFwlRpfRowStatus)
{
    tFwlRpfEntryInfo       *pRpfEntryNode = NULL;
    UINT4               u4Status = FWL_FAILURE;
    CHR1                ac1Command[FWL_DOS_LINE_LEN];
    UINT1               au1IfName[FWL_DOS_LINE_LEN];
    UINT1               au1Buffer[FWL_MAX_PORT_NAME_LENGTH];

    MEMSET (au1Buffer, 0, sizeof(au1Buffer));
    MEMSET (au1IfName, 0, sizeof (au1IfName));
    MEMSET (ac1Command, 0, sizeof (ac1Command));


    CfaGetInterfaceNameFromIndex(i4FwlRpfInIndex, (UINT1 *) au1IfName);
    pRpfEntryNode = FwlDbaseSearchRpfEntry (i4FwlRpfInIndex);

    if ((NULL != pRpfEntryNode) &&
        ( pRpfEntryNode->i4RowStatus == i4SetValFwlRpfRowStatus))
    {
        return SNMP_SUCCESS;
    }
    switch (i4SetValFwlRpfRowStatus)
    {
        case FWL_CREATE_AND_WAIT:
            if (pRpfEntryNode != NULL)
            {
                u4Status = SNMP_SUCCESS;
                break;
            }
            /* Memory Allocation and Initialisation of Filter Node .  */
            if (FwlRpfMemAllocate ((UINT1 **) (VOID *) &pRpfEntryNode) ==
                FWL_SUCCESS)
            {
                   pRpfEntryNode->i4IfIndex = i4FwlRpfInIndex;
                   pRpfEntryNode->i4RowStatus = i4SetValFwlRpfRowStatus;
                   TMO_SLL_Init_Node (&pRpfEntryNode->nextRpfEntryInfo);
                   FwlDbaseAddRpfEntry (pRpfEntryNode);
                   u4Status = SNMP_SUCCESS;
            }
            else
            {
               return SNMP_FAILURE;
            }
            break;
        case FWL_DESTROY:
            if (pRpfEntryNode == NULL)
            {
                break;
            }
        /*  Search the Node in the Filter List and
            *   delete it if reference count is Zero.
                  */

            if (pRpfEntryNode->i4RowStatus == FWL_ACTIVE)
            {
#ifdef LNXIP4_WANTED

             if ((i4FwlRpfInIndex == CFA_INVALID_IFINDEX) && (CfaIsMgmtPortEnabled() == TRUE))
            {
             STRNCPY (&au1Buffer,
                IssGetInterfaceFromNvRam (), STRLEN (IssGetInterfaceFromNvRam ()));

              SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
              "echo %d > %s/%s/%s",
              FWL_RPF_DISABLE,FWL_PROC_CONF,au1Buffer,FWL_PROC_RP_FILTER);
              system (ac1Command);


            }
            else
            {
              SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
              "echo %d > %s/%s/%s",
              FWL_RPF_DISABLE,FWL_PROC_CONF,au1IfName,FWL_PROC_RP_FILTER);
              system (ac1Command);

            }



#endif
               if(FwlDbaseDeleteRpfEntry (i4FwlRpfInIndex) != FWL_SUCCESS)
               {
                  return (SNMP_FAILURE);
               }
            }
            u4Status = SNMP_SUCCESS;


            break;
         case FWL_ACTIVE:
            if (pRpfEntryNode == NULL)
            {
                break;
            }
             pRpfEntryNode->i4RowStatus = FWL_ACTIVE;
#ifdef LNXIP4_WANTED
             if ((i4FwlRpfInIndex == CFA_INVALID_IFINDEX) && (CfaIsMgmtPortEnabled() == TRUE))
            {
             STRNCPY (&au1Buffer,
                IssGetInterfaceFromNvRam (), STRLEN (IssGetInterfaceFromNvRam ()));

              SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
              "echo %d > %s/%s/%s",
              pRpfEntryNode->i4RpfMode,FWL_PROC_CONF,au1Buffer,FWL_PROC_RP_FILTER);
              system (ac1Command);

            }
            else
            {

              SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
              "echo %d > %s/%s/%s",
              pRpfEntryNode->i4RpfMode,FWL_PROC_CONF,au1IfName,FWL_PROC_RP_FILTER);
              system (ac1Command);

            }

#endif
            u4Status = SNMP_SUCCESS;
            break;
        default:
            u4Status = SNMP_FAILURE;
            break;
   }
 return u4Status;


}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlRpfMode
 Input       :  The Indices
                FwlRpfInIndex

                The Object
                testValFwlRpfMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlRpfMode(UINT4 *pu4ErrorCode , INT4 i4FwlRpfInIndex , INT4 i4TestValFwlRpfMode)
{
    UNUSED_PARAM(pu4ErrorCode);
    tFwlRpfEntryInfo        *pRpfEntryNode = NULL;
    

    pRpfEntryNode = FwlDbaseSearchRpfEntry (i4FwlRpfInIndex);

    if (NULL == pRpfEntryNode)
    {
        CLI_SET_ERR (CLI_FWL_RPF_NOT_ENABLED);
        return SNMP_FAILURE;
    }
    if(pRpfEntryNode->i4RowStatus == FWL_ACTIVE)
    {
          if(i4TestValFwlRpfMode != pRpfEntryNode->i4RpfMode)
          {
              CLI_SET_ERR (CLI_FWL_RPF_NOT_VALID);
              return SNMP_FAILURE;
          }
    }
    if( (i4TestValFwlRpfMode > FWL_ZERO) || (i4TestValFwlRpfMode < FWL_TWO))
    {
      return SNMP_SUCCESS;
    }
 return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhTestv2FwlRpfRowStatus
 Input       :  The Indices
                FwlRpfInIndex

                The Object
                testValFwlRpfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FwlRpfRowStatus(UINT4 *pu4ErrorCode , INT4 i4FwlRpfInIndex , INT4 i4TestValFwlRpfRowStatus)
{
   INT1                i1Status = SNMP_FAILURE;
    tFwlRpfEntryInfo        *pRpfEntryNode = NULL;


   pRpfEntryNode = FwlDbaseSearchRpfEntry (i4FwlRpfInIndex);

    if ((NULL != pRpfEntryNode) &&
        ( pRpfEntryNode->i4RowStatus == i4TestValFwlRpfRowStatus))
    {
        return SNMP_SUCCESS;
    }
     switch (i4TestValFwlRpfRowStatus)
        {
            case FWL_CREATE_AND_WAIT:
                /* If the index doesnt exist already, then it can be created. */
                if (pRpfEntryNode == NULL)
                {
                    i1Status = SNMP_SUCCESS;
                }
                else
                {
                     CLI_SET_ERR (CLI_FWL_RPF_ALREADY_ENABLED);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                     i1Status = SNMP_FAILURE;
                }
                break;
              case FWL_NOT_IN_SERVICE:
                /* The row can be made FWL_NOT_IN_SERVICE if the row exist. */
                if (pRpfEntryNode != NULL)
                {
                    if (pRpfEntryNode->i4RowStatus == FWL_ACTIVE)
                    {
                        i1Status = SNMP_SUCCESS;
                    }
                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                }
                break;
              case FWL_ACTIVE:
                if (pRpfEntryNode != NULL)
                {
                        i1Status = SNMP_SUCCESS;

                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                }
                break;
            case FWL_DESTROY:
                if (pRpfEntryNode != NULL)
                {
                    i1Status = SNMP_SUCCESS;
                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    CLI_SET_ERR (CLI_FWL_RPF_NOT_ENABLED);
                    i1Status = SNMP_FAILURE;
                }
                break;
            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i1Status = SNMP_FAILURE;
       }


return i1Status;

}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlRpfTable
 Input       :  The Indices
                FwlRpfInIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FwlRpfTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

         
