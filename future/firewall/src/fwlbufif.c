/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlbufif.c,v 1.12 2016/02/27 10:05:03 siva Exp $
 *
 * Description:This file is used to extract the necessary 
 *             fields from the IP, TCP, UDP and ICMP      
 *             headers of the packet into the local       
 *             Data structure.                            
 *
 *******************************************************************/
#include "fwlinc.h"

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlUpdateIpHeaderInfoFromPkt                     */
/*                                                                          */
/*    Description        : Updates the IpHeader with packet information     */
/*                                                                          */
/*    Input(s)           : pBuf     -- Pointer to the packet                */
/*                         pIpInfo  -- Pointer to the IpHeader              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlUpdateIpHeaderInfoFromPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tIpHeader * pIpInfo)
#else
PUBLIC VOID
FwlUpdateIpHeaderInfoFromPkt (pBuf, pIpInfo)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     tIpHeader          *pIpInfo;
#endif
{
    UINT4               u4Retval = FWL_ZERO;
    FWL_GET_1_BYTE (pBuf, FWL_IP_HLEN_OFFSET, pIpInfo->u1IpVersion);
    pIpInfo->u1IpVersion >>= FWL_FOUR;

    if (pIpInfo->u1IpVersion == FWL_IP_VERSION_4)
    {
        u4Retval = FwlUpdateIpv4HeaderInfoFromPkt (pBuf, pIpInfo);
        return u4Retval;
    }
    else if (pIpInfo->u1IpVersion == FWL_IP_VERSION_6)
    {
        u4Retval = FwlUpdateIpv6HeaderInfoFromPkt (pBuf, pIpInfo);
        return u4Retval;

    }
    else
        return (FWL_INVALID_IP_VERSION);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlUpdateIpv4HeaderInfoFromPkt                   */
/*                                                                          */
/*    Description        : Updates the IpHeader with IPv4 packet information*/
/*                                                                          */
/*    Input(s)           : pBuf     -- Pointer to the packet                */
/*                         pIpInfo  -- Pointer to the IpHeader              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

PUBLIC UINT4
FwlUpdateIpv4HeaderInfoFromPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                                tIpHeader * pIpInfo)
{
    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering the fn. FwlUpdateIpv4HeaderInfoFromPkt \n");

    /* Update the IP header values */
    FWL_GET_N_BYTE_FROM_BUF (pBuf, FWL_IP_HLEN_OFFSET, pIpInfo,
                             FWL_IP_HEADER_LEN);

    pIpInfo->u1HeadLen = (UINT1)
        ((pIpInfo->u1VerHeadLen & FWL_IP_HEAD_LEN_MASK) * FWL_HEAD_LEN_FACTOR);

    /* Source IP & Destination IP address */
    pIpInfo->SrcAddr.v4Addr = OSIX_NTOHL (pIpInfo->u4SrcAddr);
    pIpInfo->SrcAddr.u4AddrType = FWL_IP_VERSION_4;

    pIpInfo->DestAddr.v4Addr = OSIX_NTOHL (pIpInfo->u4DestAddr);
    pIpInfo->DestAddr.u4AddrType = FWL_IP_VERSION_4;

    /* Total length */
    pIpInfo->u2TotalLen = OSIX_NTOHS (pIpInfo->u2TotalLen);

    pIpInfo->u2Id = OSIX_NTOHS (pIpInfo->u2Id);
    pIpInfo->u2FragOffset = OSIX_NTOHS (pIpInfo->u2FragOffset);
    pIpInfo->u2HeaderCkSum = OSIX_NTOHS (pIpInfo->u2HeaderCkSum);

    pIpInfo->u1Tos = (UINT1) ((pIpInfo->u1Tos & FWL_IP_TOS_MASK) >> FWL_TWO);

    /* Validate the IP-Headers' Header-Length & Total-Length values. */
    if ((pIpInfo->u1HeadLen < FWL_IP_HEADER_LEN) ||
        (pIpInfo->u1HeadLen > (FWL_MAX_IP_OPTIONS_LEN + FWL_IP_HEADER_LEN)) ||
        (pIpInfo->u2TotalLen < FWL_IP_HEADER_LEN) ||
        (pIpInfo->u2TotalLen < pIpInfo->u1HeadLen) ||
        ((CRU_BUF_Get_ChainValidByteCount (pBuf)) < pIpInfo->u2TotalLen))
    {
        return (FWL_IPHDR_LEN_FIELDS_INVALID);
    }
    /* Validate the higher layer data length only for the first fragment. */
    if (!(pIpInfo->u2FragOffset & FWL_FRAG_OFFSET_BIT))
    {
        switch (pIpInfo->u1Proto)
        {
            case FWL_TCP:
                if ((pIpInfo->u2TotalLen - pIpInfo->u1HeadLen) <
                    FWL_MIN_TCP_HEADER_SIZE)
                {
                    return (FWL_TCP_SHTHDR);
                }
                break;

            case FWL_UDP:
                if ((pIpInfo->u2TotalLen - pIpInfo->u1HeadLen) <
                    FWL_MIN_UDP_HEADER_SIZE)
                {
                    return (FWL_UDP_SHTHDR);
                }
                break;

            case FWL_ICMP:
                if ((pIpInfo->u2TotalLen - pIpInfo->u1HeadLen) <
                    FWL_MIN_ICMP_HEADER_SIZE)
                {
                    return (FWL_ICMP_SHTHDR);
                }
                break;

            default:
                break;
        }                        /*End-of-Switch */
    }                            /* End-of-Code-4-Valid-Proto-Hdr-Check */

    /* Get the IP option code byte from the packet if options exist in pkt */
    if (pIpInfo->u1HeadLen != FWL_IP_HEADER_LEN)
    {
        return FwlParseIpOptions (pBuf, pIpInfo);
    }

    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting the fn. FwlUpdateIpv4HeaderInfoFromPkt \n");

    return FWL_SUCCESS;
}                                /* End of the Function -- FwlUpdateIpv4HeaderInfoFromPkt   */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlUpdateIpv6HeaderInfoFromPkt                   */
/*                                                                          */
/*    Description        : Updates the IpHeader with IPv6 packet information*/
/*                                                                          */
/*    Input(s)           : pBuf     -- Pointer to the packet                */
/*                         pIpInfo  -- Pointer to the IpHeader              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

PUBLIC UINT4
FwlUpdateIpv6HeaderInfoFromPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                                tIpHeader * pIpInfo)
{
    UINT4               u4Ip6Head = FWL_ZERO;
    UINT1               u1OffSet = FWL_ZERO;
    UINT2               u2Length = FWL_ZERO;
    /* payload length present in the ipv6 pkt */
    UINT1               u1ExtHdrLen = FWL_ZERO;
    /* Length of all the extension header present */
    UINT1               u1NextHeader = FWL_ZERO;
    UINT1               u1NoMoreExtHdr = OSIX_FALSE;
    UINT1               u1OptLen = FWL_ZERO;
    UINT1               u1HopLimit = FWL_ZERO;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering the fn. FwlUpdateIpv6HeaderInfoFromPkt \n");

    FWL_GET_4_BYTE (pBuf, FWL_IP_HLEN_OFFSET, u4Ip6Head);

    pIpInfo->u1Tos = (UINT1) ((u4Ip6Head & FWL_IPv6_DSCP_MASK) >> FWL_TWENTY);
    pIpInfo->u4FlowLabel = (u4Ip6Head & FWL_IPv6_FLOWID_MASK);

    /* Get the Source address from the pkt */
    FWL_GET_N_BYTE (pBuf, FWL_IP6_SRC_ADDR_OFFSET, pIpInfo->SrcAddr.v6Addr,
                    FWL_SIXTEEN);
    FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n SrcAddres = %x \n",
              Ip6PrintAddr (&(pIpInfo->SrcAddr.v6Addr)));
    pIpInfo->SrcAddr.u4AddrType = FWL_IP_VERSION_6;

    /* Get the Destination address from the pkt */
    FWL_GET_N_BYTE (pBuf, FWL_IP6_DST_ADDR_OFFSET, pIpInfo->DestAddr.v6Addr,
                    FWL_SIXTEEN);
    FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n DstAddres = %x \n",
              Ip6PrintAddr (&(pIpInfo->DestAddr.v6Addr)));
    pIpInfo->DestAddr.u4AddrType = FWL_IP_VERSION_6;

    /* Length of the payload including Extension headers */
    FWL_GET_2_BYTE (pBuf, FWL_IP6_PAYLOAD_LEN_OFFSET, u2Length);
    pIpInfo->u2TotalLen = (UINT2) ((FWL_IP6_HEADER_LEN) + u2Length);

    FWL_GET_1_BYTE (pBuf, FWL_IP6_HOP_LIMIT_OFFSET, u1HopLimit);
    if (u1HopLimit == FWL_ZERO)
    {
        return (FWL_INVALID_HOPLIMIT);
    }

    /* Get the value of the Next header value in the IPv6 header */
    FWL_GET_1_BYTE (pBuf, FWL_IP6_NH_OFFSET, u1NextHeader);

    /* Set the offset to the start of the extension header  40 */
    u1OffSet = FWL_IP6_FIRST_NH_OFFSET;

    do
    {
        switch (u1NextHeader)
        {
            case FWL_HOP_BY_HOP_HDR:
            case FWL_DEST_OPT_HDR:
            case FWL_ROUTING_HDR:

                /* Get the length of the extension header present in the packet */
                FWL_GET_1_BYTE (pBuf, (UINT4) u1OffSet + FWL_ONE, u1OptLen);

                /* First 8 byte will not be included in the length field and the
                 * value present in the length field is a 8-octet value*/
                u1ExtHdrLen = (UINT1) (u1ExtHdrLen + FWL_EIGHT +
                                       (u1OptLen * FWL_EIGHT));
                break;

            case FWL_NO_NXT_HDR:

                if (u1ExtHdrLen != u2Length)
                {
                    return FWL_IPHDR_LEN_FIELDS_INVALID;
                }
                u1NoMoreExtHdr = (UINT1) OSIX_TRUE;
                break;

            case FWL_FRAGMENT_HDR:

                /* FWL_FRAGMENT_HEADER_LEN == 8 */
                u1ExtHdrLen = (UINT1) (u1ExtHdrLen + FWL_FRAGMENT_HEADER_LEN);

                FWL_GET_2_BYTE (pBuf, (UINT4) (u1OffSet + FWL_TWO),
                                pIpInfo->u2FragOffset);

                /* Only for the non-first fragment packets */
                /* FWL_IP6_FRAG_OFFSET_BIT = 0xfff8 */
                if (pIpInfo->u2FragOffset & FWL_IP6_FRAG_OFFSET_BIT)
                {
                    FWL_GET_1_BYTE (pBuf, u1OffSet, u1NextHeader);
                    u1NoMoreExtHdr = OSIX_TRUE;
                }
                break;

            case FWL_AUTHENTICATION_HDR:
            case FWL_ENCAP_SEC_PAY_HDR:
            case FWL_TCP:
            case FWL_UDP:
            case FWL_ICMPV6:
            case FWL_OSPFIGP:
                u1NoMoreExtHdr = OSIX_TRUE;
                break;
            default:
                return (FWL_INVALID_NH);
                break;
        }

        if (u1NoMoreExtHdr == OSIX_TRUE)
        {
            if (u1ExtHdrLen > u2Length)
            {
                return FWL_IPHDR_LEN_FIELDS_INVALID;
            }

            /* For non-fragment and the first fragmented packets */
            if (!(pIpInfo->u2FragOffset & FWL_IP6_FRAG_OFFSET_BIT))
            {
                switch (u1NextHeader)
                {
                    case FWL_TCP:
                        if ((u2Length - u1ExtHdrLen) < FWL_MIN_TCP_HEADER_SIZE)
                        {
                            return (FWL_TCP_SHTHDR);
                        }
                        break;

                    case FWL_UDP:
                        if ((u2Length - u1ExtHdrLen) < FWL_MIN_UDP_HEADER_SIZE)
                        {
                            return (FWL_UDP_SHTHDR);
                        }
                        break;

                    case FWL_ICMPV6:
                        if ((u2Length - u1ExtHdrLen) < FWL_MIN_ICMP_HEADER_SIZE)
                        {
                            return (FWL_ICMP_SHTHDR);
                        }
                        break;

                    default:
                        break;
                }
            }

            /* FWL_IP6_HEADER_LEN == 40 */

            pIpInfo->u1HeadLen = (UINT1) (FWL_IP6_HEADER_LEN + u1ExtHdrLen);
            pIpInfo->u1Proto = u1NextHeader;
            break;
        }

        FWL_GET_1_BYTE (pBuf, u1OffSet, u1NextHeader);
        u1OffSet = (UINT1) (FWL_IP6_HEADER_LEN + u1ExtHdrLen);

    }
    while (u1ExtHdrLen < u2Length);

    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting the fn. FwlUpdateIpv6HeaderInfoFromPkt \n");

    return FWL_SUCCESS;
}                                /* End of the Function -- FwlUpdateIpv6HeaderInfoFromPkt   */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlParseIpOptions                                */
/*                                                                          */
/*    Description        : Parses the packets for IP options                */
/*                                                                          */
/*    Input(s)           : pbuf - Packet                                    */
/*                         pIpHdr - IP header info                          */
/*                                                                          */
/*    Output(s)          : pIpHdr - IP header info                          */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

PUBLIC UINT4
FwlParseIpOptions (tCRU_BUF_CHAIN_HEADER * pBuf, tIpHeader * pIpHdr)
{
    UINT1               u1Index = FWL_ZERO;
    UINT1               u1Opt_len = FWL_ZERO;    /* Current Ip Option Length */
    UINT1               u1OptCode = FWL_ZERO;    /* IP Option TYPE or CODE */
    UINT2               u2IpOptionsLen = FWL_ZERO;
    /* Length of IpOptions in the IP packet */
    UINT1               u1IpOptOffset = FWL_IP_HEADER_LEN;
    UINT4              *pu4Options = &(pIpHdr->u4Options);

   /**  BEGIN of Code for IP Option Processing  **/
    /* Initialisation of variables */
    *pu4Options = FWL_ZERO;

    /* Begin IP Options Parsing */
    if (pIpHdr->u1HeadLen > FWL_IP_HEADER_LEN)
    {
        u2IpOptionsLen = (UINT2) (pIpHdr->u1HeadLen - FWL_IP_HEADER_LEN);
        if (u2IpOptionsLen > FWL_MAX_IP_OPTIONS_LEN)
        {
            return FWL_INVALID_IP_OPTIONS;
        }
        while ((u1Index < u2IpOptionsLen) &&
               ((FWL_GET_1_BYTE (pBuf, u1IpOptOffset, u1OptCode)) == FWL_ONE))
        {
            if (u1OptCode == FWL_IP_OPT_EOL)
            {
                /* reached the end of the option field */
                break;
            }
            switch (u1OptCode)
            {
                case FWL_IP_OPT_NOP:
                    u1Opt_len = FWL_ONE;
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_NOP_BIT;
                    break;
                case FWL_IP_OPT_SECURITY:
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_SECURITY_BIT;
                    break;
                case FWL_IP_OPT_LSROUTE:
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_LSROUTE_BIT;
                    break;
                case FWL_IP_OPT_TSTAMP:
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_TSTAMP_BIT;
                    break;
                case FWL_IP_OPT_ESECURITY:
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_ESECURITY_BIT;
                    break;
                case FWL_IP_OPT_RROUTE:
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_RROUTE_BIT;
                    break;
                case FWL_IP_OPT_STREAM_IDENT:
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_STREAM_IDENT_BIT;
                    break;
                case FWL_IP_OPT_SSROUTE:
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_SSROUTE_BIT;
                    break;
                case FWL_IP_OPT_ROUTERALERT:
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_ROUTERALERT_BIT;
                    break;

                case FWL_IP_OPT_TROUTE:
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_TROUTE_BIT;
                    break;

                default:
                    /* Invalid IP option. */
                    FWL_DBG (FWL_DBG_EXIT, "\n Invalid IP Option Type\n");
                    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlParseIpOptions()\n");
                    return FWL_INVALID_IP_OPTIONS;
            }                    /*End-of-Switch */

            /* Get the Option_Length */
            if (u1OptCode != FWL_IP_OPT_NOP)
                FWL_GET_1_BYTE (pBuf, (UINT4) (u1IpOptOffset + FWL_ONE),
                                u1Opt_len);
            /* If option length field is 0 or if it is > option 
             * length in IP packet then the packet is Invalid.   */
            if ((u1Opt_len > u2IpOptionsLen) ||
                ((u1Opt_len + u1Index) > u2IpOptionsLen))
            {
                FWL_DBG (FWL_DBG_EXIT, "\n Invalid IP Option length\n");
                FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlParseIpOptions()\n");
                return FWL_INVALID_IP_OPTIONS;
            }
            else if (FWL_ZERO == u1Opt_len)
            {
                FWL_GET_1_BYTE (pBuf, (UINT4) (u1IpOptOffset + FWL_ONE),
                                u1Opt_len);
                /* If option length field is 0 or if it is > option 
                 * length in IP packet then the packet is Invalid.   */
                if ((u1Opt_len > u2IpOptionsLen) ||
                    ((u1Opt_len + u1Index) > u2IpOptionsLen))
                {
                    FWL_DBG (FWL_DBG_EXIT, "\n Invalid IP Option length\n");
                    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlParseIpOptions()\n");
                    return FWL_INVALID_IP_OPTIONS;
                }
                else if (FWL_ZERO == u1Opt_len)
                {
                    FWL_DBG (FWL_DBG_EXIT, "\n Zero length IP Option \n");
                    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlParseIpOptions()\n");
                    return FWL_ZERO_LEN_IP_OPTION;
                }
            }                    /*End-ofIF-Block */

            u1Index = (UINT1) (u1Index + u1Opt_len);
            u1IpOptOffset = (UINT1) (u1IpOptOffset + u1Opt_len);
        }                        /*End-of-While-Loop */
    }                            /*End-of-IF-Block */

    return FWL_SUCCESS;            /* All IP-Options Valid */
   /**  END of Code for IP Option Processing  **/
}                                /* End of the function -- FwlParseIpOptions  */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlUpdateIcmpv4v6InfoFromPkt                     */
/*                                                                          */
/*    Description        : Updates the IcmpInfo srtucture with pkt info     */
/*                                                                          */
/*    Input(s)           : pBuf     -- Pointer to the packet                */
/*                         pIcmp    -- Pointer to the IcmpInfo              */
/*                         u1IpHeadLen - Value of the IP header length      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlUpdateIcmpv4v6InfoFromPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                              tIcmpInfo * pIcmp, UINT1 u1IpHeadLen)
#else
PUBLIC VOID
FwlUpdateIcmpv4v6InfoFromPkt (pBuf, pIcmp u1IpHeadLen)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     tIcmpInfo          *pIcmp;
     UINT1               u1IpHeadLen;
#endif
{
    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering the fn. FwlUpdateIcmpv4v6InfoFromPkt \n");

    /* Update the ICMP header values */
    FWL_GET_N_BYTE_FROM_BUF (pBuf, u1IpHeadLen, pIcmp, FWL_EIGHT_BYTES);

    pIcmp->u2CkSum = OSIX_NTOHS (pIcmp->u2CkSum);

    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting the fn. FwlUpdateIcmpv4v6InfoFromPkt \n");

}                                /* End of the Function -- FwlUpdateIcmpv4v6InfoFromPkt  */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlUpdateHLInfoFromPkt                           */
/*                                                                          */
/*    Description        : Updates the Higher layer TCP/UDP information     */
/*                         like Src and Dest port and If the packet is for  */
/*                         TCP, updates the Ack and Rst bits.               */
/*                                                                          */
/*    Input(s)           : pBuf     -- Pointer to the packet                */
/*                         pHLInfo  -- Pointer to the HLInfo                */
/*                         u1Proto  -- Protocol ( UDP or TCP )              */
/*                         u1IpHeadLen - IP header length                   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlUpdateHLInfoFromPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                        tHLInfo * pHLInfo, UINT1 u1Proto, UINT1 u1IpHeadLen)
#else
PUBLIC VOID
FwlUpdateHLInfoFromPkt (pBuf, pHLInfo, u1Proto, u1IpHeadLen)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     tHLInfo            *pHLInfo;
     UINT1               u1Proto;
     UINT1               u1IpHeadLen;
#endif
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlUpdateHLInfoFromPkt \n");

    if (u1Proto == FWL_TCP)
    {
        /* Update the TCP header values */
        FWL_GET_N_BYTE_FROM_BUF (pBuf, u1IpHeadLen, pHLInfo,
                                 FWL_TCP_HEADER_LEN);

        /* Source & destination ports */
        pHLInfo->u2SrcPort = OSIX_NTOHS (pHLInfo->u2SrcPort);
        pHLInfo->u2DestPort = OSIX_NTOHS (pHLInfo->u2DestPort);

        /* Sequence Number */
        pHLInfo->u4SeqNum = OSIX_NTOHL (pHLInfo->u4SeqNum);

        /* Acknowledgement Number */
        pHLInfo->u4AckNum = OSIX_NTOHL (pHLInfo->u4AckNum);

        /* Checksum */
        pHLInfo->u2CkSum = OSIX_NTOHS (pHLInfo->u2CkSum);
        pHLInfo->u2UrgPtr = OSIX_NTOHS (pHLInfo->u2UrgPtr);

        /* get the Rst bit from the code bit and right shift by two times */
        pHLInfo->u1Rst =
            (UINT1) ((pHLInfo->u1TcpCodeBit & FWL_TCP_RST_MASK) >> FWL_TWO);

        /* get the Ack bit from the code bit and right shift by Four times */
        pHLInfo->u1Ack =
            (UINT1) ((pHLInfo->u1TcpCodeBit & FWL_TCP_ACK_MASK) >> FWL_FOUR);

        /* get the Fin bit from the code bit */
        pHLInfo->u1Fin = (pHLInfo->u1TcpCodeBit & FWL_TCP_FIN_MASK);

        /* Get the Syn bit from the code bit */
        pHLInfo->u1Syn =
            (UINT1) ((pHLInfo->u1TcpCodeBit & FWL_TCP_SYN_MASK) >> FWL_ONE);

        FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n SrcPort = %d \n",
                  pHLInfo->u2SrcPort);
        FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n DestPort = %d \n",
                  pHLInfo->u2DestPort);
        FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n Rst Bit = %d \n", pHLInfo->u1Rst);
        FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n Ack Bit = %d \n", pHLInfo->u1Ack);
        FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n Fin Bit = %d \n", pHLInfo->u1Fin);
        FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n Syn Bit = %d \n", pHLInfo->u1Syn);

    }
    else
    {                            /* u1Proto is UDP */
        /* Update the UDP header values */
        /* Get the Source port & destination port */
        FWL_GET_N_BYTE_FROM_BUF (pBuf, u1IpHeadLen, pHLInfo, FWL_FOUR_BYTES);

        /* Source & destination ports */
        pHLInfo->u2SrcPort = OSIX_NTOHS (pHLInfo->u2SrcPort);
        pHLInfo->u2DestPort = OSIX_NTOHS (pHLInfo->u2DestPort);

        /* the following fields are not used, if the packet is UDP and 
         * initialised to the Zero.
         */
        pHLInfo->u1Rst = FWL_ZERO;
        pHLInfo->u1Ack = FWL_ZERO;
        pHLInfo->u1Fin = FWL_ZERO;
        pHLInfo->u1Syn = FWL_ZERO;

        FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n SrcPort = %d \n",
                  pHLInfo->u2SrcPort);
        FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n DestPort = %d \n",
                  pHLInfo->u2DestPort);
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlUpdateHLInfoFromPkt \n");

}                                /* End of the Function -- FwlUpdateHLInfoFromPkt  */

/* 
 * CHANGE1 : All allocation and deallocation routines are modified to support
 * dynamic allocation and deallocation.
 */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlRuleMemAllocate                               */
/*                                                                          */
/*    Description        : Allocates the memory for the Rule Node from      */
/*                         the Memory Pool. If the Pool exhausts, it does   */
/*                         'malloc' and allocates the memory required.      */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : ppu1Block -- Pointer to the Rule Node Allocated  */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if Rule node is allocated,           */
/*                         else FWL_FAILURE.                                */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlRuleMemAllocate (UINT1 **ppu1Block)
#else
PUBLIC UINT4
FwlRuleMemAllocate (ppu1Block)
     UIN1              **ppu1Block;
#endif
{
    UINT4               u4MemAllocateStatus = FWL_ZERO;

    u4MemAllocateStatus = MEM_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlRuleMemAllocate \n");

    u4MemAllocateStatus = MemAllocateMemBlock (FWL_RULE_PID, ppu1Block);
    if (u4MemAllocateStatus == MEM_FAILURE)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, ALL_FAILURE_TRC, "FWL",
                 "\n Memory Pool Exhausts for Rule List - "
                 "Dynamic Allocation Done \n");
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlRuleMemAllocate \n");

    return u4MemAllocateStatus;

}                                /* End of the Function -- FwlRuleMemAllocate  */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlRuleMemFree                                   */
/*                                                                          */
/*    Description        : Releases  the memory of the Rule Node from       */
/*                         the Memory Pool. If the allocation is not from   */
/*                         the Pool, then it does 'freemem'.                */
/*                                                                          */
/*    Input(s)           : pu1Block  -- Pointer to the Rule Node Allocated  */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if Rule node is Released,            */
/*                         else FWL_FAILURE.                                */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlRuleMemFree (UINT1 *pu1Block)
#else
PUBLIC VOID
FwlRuleMemFree (pu1Block)
     UINT1              *pu1Block;
#endif
{
    UINT4               u4MemReleaseStatus = FWL_ZERO;

    u4MemReleaseStatus = MEM_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlRuleMemFree \n");

    u4MemReleaseStatus = MemReleaseMemBlock (FWL_RULE_PID, pu1Block);
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlRuleMemFree \n");
    UNUSED_PARAM (u4MemReleaseStatus);
}                                /* End of the Function -- FwlRuleMemFree  */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlFilterMemAllocate                             */
/*                                                                          */
/*    Description        : Allocates the memory for the Filter Node from    */
/*                         the Memory Pool. If the Pool exhausts, it does   */
/*                         'malloc' and allocates the memory required.      */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : ppu1Block   -- Pointer to the Filter Node        */
/*                         Allocated                                        */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if Filter Node is allocated          */
/*                         else FWL_FAILURE.                                */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlFilterMemAllocate (UINT1 **ppu1Block)
#else
PUBLIC UINT4
FwlFilterMemAllocate (ppu1Block)
     UINT1             **ppu1Block;
#endif
{
    UINT4               u4MemAllocateStatus = FWL_ZERO;

    u4MemAllocateStatus = MEM_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlFilterMemAllocate\n");

    u4MemAllocateStatus = MemAllocateMemBlock (FWL_FILTER_PID, ppu1Block);
    if (u4MemAllocateStatus == MEM_FAILURE)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, ALL_FAILURE_TRC, "FWL",
                 "\n Memory Pool Exhausts for Filter List - "
                 "Dynamic Allocation Done \n");
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlFilterMemAllocate\n");

    return u4MemAllocateStatus;

}                                /* End of the Function -- FwlFilterMemAllocate */
/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSnorkMemAllocate                              */
/*                                                                          */
/*    Description        : Allocates the memory for the Filter Node from    */
/*                         the Memory Pool. If the Pool exhausts, it does   */
/*                         'malloc' and allocates the memory required.      */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : ppu1Block   -- Pointer to the Filter Node        */
/*                         Allocated                                        */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if Filter Node is allocated          */
/*                         else FWL_FAILURE.                                */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlSnorkMemAllocate (UINT1 **ppu1Block)
#else
PUBLIC UINT4
FwlSnorkMemAllocate (ppu1Block)
     UINT1             **ppu1Block;
#endif
{
    UINT4               u4MemAllocateStatus = MEM_FAILURE;


    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlSnorkMemAllocate\n");

    u4MemAllocateStatus = MemAllocateMemBlock (FWL_SNORK_ATTACK_PID, ppu1Block);
    if (u4MemAllocateStatus == MEM_FAILURE)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, ALL_FAILURE_TRC, "FWL",
                 "\n Memory Pool Exhausts for Snork List - "
                 "Dynamic Allocation Done \n");
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlSnorkMemAllocate\n");

    return u4MemAllocateStatus;

}                                /* End of the Function -- FwlFilterMemAllocate */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlRpfMemAllocate                                */
/*                                                                          */
/*    Description        : Allocates the memory for the Filter Node from    */
/*                         the Memory Pool. If the Pool exhausts, it does   */
/*                         'malloc' and allocates the memory required.      */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : ppu1Block   -- Pointer to the Filter Node        */
/*                         Allocated                                        */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if Filter Node is allocated          */
/*                         else FWL_FAILURE.                                */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlRpfMemAllocate (UINT1 **ppu1Block)
#else
PUBLIC UINT4
FwlRpfMemAllocate (ppu1Block)
     UINT1             **ppu1Block;
#endif
{
    UINT4               u4MemAllocateStatus = MEM_FAILURE;


    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlRpfMemAllocate\n");

    u4MemAllocateStatus = MemAllocateMemBlock (FWL_RPF_CHECK_PID, ppu1Block);
    if (u4MemAllocateStatus == MEM_FAILURE)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, ALL_FAILURE_TRC, "FWL",
                 "\n Memory Pool Exhausts for Rpf List - "
                 "Dynamic Allocation Done \n");
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlRpfMemAllocate\n");

    return u4MemAllocateStatus;

} 
/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlFilterMemFree                                 */
/*                                                                          */
/*    Description        : Releases  the memory of the Filter Node from     */
/*                         the Memory Pool. If the allocation is not from   */
/*                         the Pool, then it does 'freemem'.                */
/*                                                                          */
/*    Input(s)           : pFilterNode -- Pointer to the Filter Node        */
/*                         Allocated                                        */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if Filter Node is Released           */
/*                         else FWL_FAILURE.                                */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlFilterMemFree (UINT1 *pu1Block)
#else
PUBLIC VOID
FwlFilterMemFree (pu1Block)
     UINT1              *pu1Block;
#endif
{
    UINT4               u4MemReleaseStatus = FWL_ZERO;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlFilterMemFree\n");

    u4MemReleaseStatus = MemReleaseMemBlock (FWL_FILTER_PID, pu1Block);
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlFilterMemFree\n");
    UNUSED_PARAM (u4MemReleaseStatus);
}                                /* End of the Function -- FwlFilterMemFree */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSnorkMemFree                                  */
/*                                                                          */
/*    Description        : Releases  the memory of the Filter Node from     */
/*                         the Memory Pool. If the allocation is not from   */
/*                         the Pool, then it does 'freemem'.                */
/*                                                                          */
/*    Input(s)           : pFilterNode -- Pointer to the Filter Node        */
/*                         Allocated                                        */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if Filter Node is Released           */
/*                         else FWL_FAILURE.                                */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlSnorkMemFree (UINT1 *pu1Block)
#else
PUBLIC VOID
FwlSnorkMemFree (pu1Block)
     UINT1              *pu1Block;
#endif
{

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlFilterMemFree\n");

    MemReleaseMemBlock (FWL_SNORK_ATTACK_PID, pu1Block);
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlFilterMemFree\n");
} 
/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlRpfEntryMemFree                               */
/*                                                                          */
/*    Description        : Releases  the memory of the Filter Node from     */
/*                         the Memory Pool. If the allocation is not from   */
/*                         the Pool, then it does 'freemem'.                */
/*                                                                          */
/*    Input(s)           : pFilterNode -- Pointer to the Filter Node        */
/*                         Allocated                                        */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if Filter Node is Released           */
/*                         else FWL_FAILURE.                                */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlRpfEntryMemFree (UINT1 *pu1Block)
#else
PUBLIC VOID
FwlRpfEntryMemFree (pu1Block)
     UINT1              *pu1Block;
#endif
{

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlFilterMemFree\n");

    MemReleaseMemBlock (FWL_RPF_CHECK_PID, pu1Block);
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlFilterMemFree\n");
} 


/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlAclMemAllocate                                */
/*                                                                          */
/*    Description        : Allocates the memory for the InFilter Node  or   */
/*                         Out Filter Node from the memory pool.            */
/*                         If the Pool exhausts, it does                    */
/*                         'malloc' and allocates the memory required.      */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : ppu1Block-- Pointer to the InFilter Node or      */
/*                                     OutFilter Node alocated.             */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if Acl  node is allocated,           */
/*                         else FWL_FAILURE.                                */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlAclMemAllocate (UINT1 **ppu1Block)
#else
PUBLIC UINT4
FwlAclMemAllocate (ppu1Block)
     UINT1             **ppu1Block;
#endif
{
    UINT4               u4MemAllocateStatus = FWL_ZERO;

    u4MemAllocateStatus = MEM_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlAclMemAllocate\n");

    u4MemAllocateStatus = MemAllocateMemBlock (FWL_ACL_INFO_PID, ppu1Block);
    if (u4MemAllocateStatus == MEM_FAILURE)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, ALL_FAILURE_TRC, "FWL",
                 "\n Memory Pool Exhausts for Acl List - "
                 "Dynamic Allocation Done \n");
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlAclMemAllocate\n");

    return u4MemAllocateStatus;

}                                /* End of the Function -- FwlAclMemAllocate */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlAclMemFree                                    */
/*                                                                          */
/*    Description        : Releases  the memory of the  Acl Node into       */
/*                         the Memory Pool. If the allocation is not from   */
/*                         the Pool, then it does 'freemem'.                */
/*                                                                          */
/*    Input(s)           : pu1Block -- Pointer to the  Acl Node             */
/*                         Allocated                                        */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if  Acl node is Released,            */
/*                         else FWL_FAILURE.                                */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlAclMemFree (UINT1 *pu1Block)
#else
PUBLIC VOID
FwlAclMemFree (pu1Block)
     UINT1              *pu1Block;
#endif
{
    UINT4               u4MemReleaseStatus = FWL_ZERO;
    tAclInfo           *pAclNode = NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlAclMemFree\n");

    pAclNode = (tAclInfo *) (void *) pu1Block;

    u4MemReleaseStatus = MemReleaseMemBlock (FWL_ACL_INFO_PID, pu1Block);
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlAclMemFree\n");
    UNUSED_PARAM (u4MemReleaseStatus);
    UNUSED_PARAM (*pAclNode);
}                                /* End of the Function -- FwlAclMemFree */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlIfaceMemAllocate                              */
/*                                                                          */
/*    Description        : Allocates the memory for the Iface Node from     */
/*                         the Memory Pool. If the Pool exhausts, it does   */
/*                         'malloc' and allocates the memory required.      */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : pIfaceNode -- Pointer to the Iface Node          */
/*                         Allocated                                        */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if Iface node is allocated,          */
/*                         else FWL_FAILURE.                                */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlIfaceMemAllocate (UINT1 **ppu1Block)
#else
PUBLIC UINT4
FwlIfaceMemAllocate (ppu1Block)
     UINT1             **ppu1Block;
#endif
{
    UINT4               u4MemAllocateStatus = FWL_ZERO;

    u4MemAllocateStatus = MEM_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlIfaceMemAllocate\n");

    u4MemAllocateStatus = MemAllocateMemBlock (FWL_IFACE_PID, ppu1Block);

    if (u4MemAllocateStatus == MEM_FAILURE)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, ALL_FAILURE_TRC, "FWL",
                 "\n Memory Pool Exhausts for Interface List - "
                 "Dynamic Allocation Done \n");
    }
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlIfaceMemAllocate\n");

    return u4MemAllocateStatus;

}                                /* End of the Function -- FwlIfaceMemAllocate */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlIfaceMemFree                                  */
/*                                                                          */
/*    Description        : Releases  the memory of the Iface Node into      */
/*                         the Memory Pool. If the allocation is not from   */
/*                         the Pool, then it does 'freemem'.                */
/*                                                                          */
/*    Input(s)           : pIfaceNode -- Pointer to the Iface Node          */
/*                         Allocated                                        */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if Rule node is Released,            */
/*                         else FWL_FAILURE.                                */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlIfaceMemFree (UINT1 *pu1Block)
#else
PUBLIC VOID
FwlIfaceMemFree (pu1Block)
     UINT1              *pu1Block;
#endif
{
    UINT4               u4MemReleaseStatus = FWL_ZERO;
    u4MemReleaseStatus = MEM_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlIfaceMemFree\n");

    u4MemReleaseStatus = MemReleaseMemBlock (FWL_IFACE_PID, pu1Block);

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlIfaceMemFree\n");
    UNUSED_PARAM (u4MemReleaseStatus);
}                                /* End of the Function -- FwlIfaceMemFree */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlStateMemAllocate                              */
/*                                                                          */
/*    Description        : Allocates the memory for StateFilter Node from   */
/*                         the Memory Pool. If the Pool exhausts, it does   */
/*                         'malloc' and allocates the memory required.      */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : ppu1Block --Pointer to StateFilterNode Allocated */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if StateFilter node is allocated,    */
/*                         else FWL_FAILURE.                                */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlStateMemAllocate (UINT1 **ppu1Block)
#else
PUBLIC UINT4
FwlStateMemAllocate (ppu1Block)
     UINT1             **ppu1Block;
#endif
{
    UINT4               u4MemAllocateStatus = FWL_ZERO;

    u4MemAllocateStatus = MEM_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlStateMemAllocate \n");

    /* The Allocation done from MemPool. If MemPool exhausts then malloc is
     * done. */
    u4MemAllocateStatus = MemAllocateMemBlock (FWL_STATE_PID, ppu1Block);
    if (u4MemAllocateStatus == MEM_FAILURE)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, ALL_FAILURE_TRC, "FWL",
                 "\n Memory Pool Exhausts for State Filter List - "
                 "Dynamic Allocation Done \n");
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlStateMemAllocate \n");
    return u4MemAllocateStatus;
}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlStateMemFree                                  */
/*                                                                          */
/*    Description        : Releases the memory of the StateFilter Node from */
/*                         the Memory Pool. If the allocation is not from   */
/*                         the Pool, then it does 'freemem'.                */
/*                                                                          */
/*    Input(s)           : pu1Block --Pointer to StateFilter Node Allocated */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if StateFilter node is Released,     */
/*                         else FWL_FAILURE.                                */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlStateMemFree (UINT1 *pu1Block)
#else
PUBLIC VOID
FwlStateMemFree (pu1Block)
     UINT1              *pu1Block;
#endif
{
    UINT4               u4MemReleaseStatus = FWL_ZERO;
    tStatefulSessionNode *pState = NULL;

    u4MemReleaseStatus = MEM_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlStateMemFree \n");

    pState = (tStatefulSessionNode *) (void *) pu1Block;
    u4MemReleaseStatus = MemReleaseMemBlock (FWL_STATE_PID, pu1Block);
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlStateMemFree \n");
    UNUSED_PARAM (u4MemReleaseStatus);
    UNUSED_PARAM (*pState);
}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTcpInitFlowMemAllocate                        */
/*                                                                          */
/*    Description        : Allocates the memory for TcpInitFlowNode from    */
/*                         the Memory Pool. If the Pool exhausts, it does   */
/*                         'malloc' and allocates the memory required.      */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : ppu1Block --Pointer to TcpInitFlowNode Allocated */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if TcpInitFlow node is allocated,    */
/*                         else FWL_FAILURE.                                */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlTcpInitFlowMemAllocate (UINT1 **ppu1Block)
#else
PUBLIC UINT4
FwlTcpInitFlowMemAllocate (ppu1Block)
     UINT1             **ppu1Block;
#endif
{
    UINT4               u4MemAllocateStatus = FWL_ZERO;

    u4MemAllocateStatus = MEM_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlTcpInitFlowMemAllocate \n");

    /* The Allocation done from MemPool. */
    u4MemAllocateStatus = MemAllocateMemBlock (FWL_TCP_INIT_FLOW_PID,
                                               ppu1Block);
    if (u4MemAllocateStatus == MEM_FAILURE)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, ALL_FAILURE_TRC, "FWL",
                 "\n Memory Pool Exhausts for TcpInitFlow List - "
                 "Dynamic Allocation Done \n");
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlTcpInitFlowMemAllocate \n");
    return u4MemAllocateStatus;
}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTcpInitFlowMemFree                            */
/*                                                                          */
/*    Description        : Releases the memory of the TcpInitFlow Node from */
/*                         the Memory Pool. If the allocation is not from   */
/*                         the Pool, then it does 'freemem'.                */
/*                                                                          */
/*    Input(s)           : pu1Block --Pointer to TcpInitFlow Node Allocated */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if TcpInitFlow node is Released,     */
/*                         else FWL_FAILURE.                                */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlTcpInitFlowMemFree (UINT1 *pu1Block)
#else
PUBLIC VOID
FwlTcpInitFlowMemFree (pu1Block)
     VOID               *pu1Block;
#endif
{
    UINT4               u4MemReleaseStatus = FWL_ZERO;
    tStatefulSessionNode *pNode = NULL;

    pNode = (tStatefulSessionNode *) (void *) pu1Block;

    u4MemReleaseStatus = MEM_FAILURE;
    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlTcpInitFlowMemFree \n");

    u4MemReleaseStatus = MemReleaseMemBlock (FWL_TCP_INIT_FLOW_PID, pu1Block);
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlTcpInitFlowMemFree \n");
    UNUSED_PARAM (u4MemReleaseStatus);
    UNUSED_PARAM (pNode);
}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlPartialLinksMemAllocate                       */
/*                                                                          */
/*    Description        : Allocates the memory for PartialLinkNode from    */
/*                         the Memory Pool. If the Pool exhausts, it does   */
/*                         'malloc' and allocates the memory required.      */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : ppu1Block --Pointer to PartialLinkNode Allocated */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if PartialLink node is allocated,    */
/*                         else FWL_FAILURE.                                */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlPartialLinksMemAllocate (UINT1 **ppu1Block)
#else
PUBLIC UINT4
FwlPartialLinksMemAllocate (ppu1Block)
     UINT1             **ppu1Block;
#endif
{
    UINT4               u4MemAllocateStatus = FWL_ZERO;

    u4MemAllocateStatus = MEM_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering the fn. FwlPartialLinksMemAllocate \n");

    /* The Allocation done from MemPool. */
    u4MemAllocateStatus = MemAllocateMemBlock (FWL_PARTIAL_LINKS_PID,
                                               ppu1Block);
    if (u4MemAllocateStatus == MEM_FAILURE)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, ALL_FAILURE_TRC, "FWL",
                 "\n Memory Pool Exhausts for PartialLinks List - "
                 "Dynamic Allocation Done \n");
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlPartialLinksMemAllocate \n");
    return u4MemAllocateStatus;
}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlPartialLinksMemFree                           */
/*                                                                          */
/*    Description        : Releases the memory of the PartialLink Node from */
/*                         the Memory Pool. If the allocation is not from   */
/*                         the Pool, then it does 'freemem'.                */
/*                                                                          */
/*    Input(s)           : pu1Block --Pointer to PartialLink Node Allocated */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if PartialLink node is Released,     */
/*                         else FWL_FAILURE.                                */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlPartialLinksMemFree (UINT1 *pu1Block)
#else
PUBLIC VOID
FwlPartialLinksMemFree (pu1Block)
     VOID               *pu1Block;
#endif
{
    UINT4               u4MemReleaseStatus = FWL_ZERO;

    u4MemReleaseStatus = MEM_FAILURE;
    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlPartialLinksMemFree \n");

    u4MemReleaseStatus = MemReleaseMemBlock (FWL_PARTIAL_LINKS_PID, pu1Block);

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlPartialLinksMemFree \n");
    UNUSED_PARAM (u4MemReleaseStatus);
}                                /*End of Function */

/****************************************************************************/
/*                 End of the file -- fwlbufif.c                            */
/****************************************************************************/
