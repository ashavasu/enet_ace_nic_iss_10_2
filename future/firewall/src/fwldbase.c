/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwldbase.c,v 1.9 2016/02/27 10:05:03 siva Exp $
 *
 * Description:This file contains routines to update      
 *             and modify Data Bases relating to          
 *             accesslist                            
 *
 *******************************************************************/
#include "fwlinc.h"

/*****************************************************************************/
/*       P R O T O T Y P E    D E C L A R A T I O N S                        */
/*****************************************************************************/

PRIVATE UINT4 FwlDbaseFindPositionAndInsert PROTO ((tTMO_SLL * pAclList,
                                                    tAclInfo * pAclNode));

PRIVATE UINT4 FwlDbaseDelNodeAndUpdateStat PROTO ((tIfaceInfo * pIfaceNode,
                                                   UINT1 u1Direction,
                                                   tAclInfo * pAclNode,
                                                   UINT1 u1AddrType));
VOID FwlAddWanInfoOnPhysicalIf PROTO ((UINT4 u4PppIfIndex,
                                       UINT4 u4PhysIfIndex));

UINT4 FwlGetReservedPriority PROTO ((UINT4 *pPriority,
                                     UINT4 u4Direction, UINT4 u4IfaceNum));

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseSearchFilter                             */
/*                                                                          */
/*    Description        : Searches the Filter List for the given filter    */
/*                         name                                             */
/*                                                                          */
/*    Input(s)           : au1FilterName -- String containing Filter name   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Pointer to the filter node if exists, otherwise  */
/*                         returns NULL.                                    */
/****************************************************************************/

#ifdef __STDC__
PUBLIC tFilterInfo *FwlDbaseSearchFilter
    (UINT1 au1FilterName[FWL_MAX_FILTER_NAME_LEN])
#else
PUBLIC tFilterInfo *
FwlDbaseSearchFilter (au1FilterName[FWL_MAX_FILTER_NAME_LEN])
     UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN];
#endif
{
    tFilterInfo        *pFilterNode = NULL;
    UINT1               u1Status = FWL_ZERO;
    u1Status = FWL_NOT_MATCH;
    pFilterNode = (tFilterInfo *) NULL;
    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into the fn. FwlDbaseSearchFilter \n");
    /* Search the Filter List for the Particular filter Name. If found 
     * return the pointer to the Filter Node , else NULL
     */
    TMO_SLL_Scan (&(gFwlAclInfo.filterList), pFilterNode, tFilterInfo *)
    {
        if (FWL_STRCMP
            ((INT1 *) au1FilterName,
             (INT1 *) pFilterNode->au1FilterName) == FWL_STRING_EQUAL)
        {
            u1Status = FWL_MATCH;
            break;
        }
    }
    if (u1Status == FWL_MATCH)
    {
        FWL_DBG1 (FWL_DBG_DBASE,
                  "\n pFilterNode->au1FilterName = %s\n",
                  pFilterNode->au1FilterName);
        FWL_DBG (FWL_DBG_EXIT,
                 "\n Exiting from the fn. FwlDbaseSearchFilter \n");
        return pFilterNode;
    }
    return NULL;
}                                /* End of the function -- FwlDbaseSearchFilter */
/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseSearchSnork                              */
/*                                                                          */
/*    Description        : Searches the Filter List for the given filter    */
/*                         name                                             */
/*                                                                          */
/*    Input(s)           : portno        -- String containing Filter name   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Pointer to the filter node if exists, otherwise  */
/*                         returns NULL.                                    */
/****************************************************************************/

PUBLIC 
tFwlSnorkInfo  *FwlDbaseSearchSnork (INT4 i4PortNo)
{
    tFwlSnorkInfo        *pSnorkNode = NULL;
    UINT1               u1Status = FWL_ZERO;
    u1Status = FWL_NOT_MATCH;
    pSnorkNode = (tFwlSnorkInfo *) NULL;
    TMO_SLL_Scan (&gFwlSnorkList, pSnorkNode ,tFwlSnorkInfo *)
    {
        if(i4PortNo == pSnorkNode->i4PortNo )
        {
            u1Status = FWL_MATCH;
            break;
        }
    }
    if (u1Status == FWL_MATCH)
    {
        return pSnorkNode;
    }
    return NULL;
}
/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseSearchRpfEntry                           */
/*                                                                          */
/*    Description        : Searches the Filter List for the given filter    */
/*                         name                                             */
/*                                                                          */
/*    Input(s)           : i4IfIndex        -- String containing Filter name*/
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Pointer to the filter node if exists, otherwise  */
/*                         returns NULL.                                    */
/****************************************************************************/

PUBLIC 
tFwlRpfEntryInfo  *FwlDbaseSearchRpfEntry (INT4 i4IfIndex)
{
    tFwlRpfEntryInfo        *pRpfEntryNode = NULL;
    UINT1               u1Status = FWL_NOT_MATCH;
    pRpfEntryNode = (tFwlRpfEntryInfo *) NULL;
    TMO_SLL_Scan (&gFwlRpfEntryList, pRpfEntryNode ,tFwlRpfEntryInfo *)
    {
        if(i4IfIndex == pRpfEntryNode->i4IfIndex )
        {
            u1Status = FWL_MATCH;
            break;
        }
    }
    if (u1Status == FWL_MATCH)
    {
        return pRpfEntryNode;
    }
    return NULL;
} 
/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseSearchRule                               */
/*                                                                          */
/*    Description        : Searches the  Rule List for the given Rule name  */
/*                                                                          */
/*    Input(s)           : au1RuleName      -- String containing Rule Name  */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Pointer to the Rule Node if exists, otherwise    */
/*                         returns NULL.                                    */
/****************************************************************************/

#ifdef __STDC__
PUBLIC tRuleInfo   *
FwlDbaseSearchRule (UINT1 au1RuleName[FWL_MAX_RULE_NAME_LEN])
#else
PUBLIC tRuleInfo   *
FwlDbaseSearchRule (au1RuleName)
     UINT1               au1RuleName[FWL_MAX_RULE_NAME_LEN];
#endif
{
    tRuleInfo          *pRuleNode = (tRuleInfo *) NULL;
    UINT1               u1Status = FWL_NOT_MATCH;
    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into the fn. FwlDbaseSearchRule \n");
    /* Search the  Rule List for the Particular Rule Name. If found 
     * return the pointer to the  Rule Node , else NULL
     */
    TMO_SLL_Scan (&(gFwlAclInfo.ruleList), pRuleNode, tRuleInfo *)
    {
        if (FWL_STRCMP
            ((INT1 *) au1RuleName,
             (INT1 *) pRuleNode->au1RuleName) == FWL_STRING_EQUAL)
        {
            u1Status = FWL_MATCH;
            break;
        }
    }

    if (u1Status == FWL_MATCH)
    {

        FWL_DBG1 (FWL_DBG_DBASE, "\n Rule Name = %s\n", pRuleNode->au1RuleName);
        return pRuleNode;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from the fn. FwlDbaseSearchRule \n");
    return NULL;
}                                /* End of the function -- FwlDbaseSearchRule */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseSearchIface                              */
/*                                                                          */
/*    Description        : Searches the  Interface array for the given      */
/*                         interface number.                                */
/*                                                                          */
/*    Input(s)           : u4IfaceNum    -- Interface Number                */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Pointer to the interface Node if exists otherwise*/
/*                         returns NULL.                                    */
/****************************************************************************/

#ifdef __STDC__
PUBLIC tIfaceInfo  *
FwlDbaseSearchIface (UINT4 u4IfaceNum)
#else
PUBLIC tIfaceInfo  *
FwlDbaseSearchIface (u4IfaceNum)
     UINT1               u4IfaceNum;
#endif
{
    tIfaceInfo         *pIfaceNode = NULL;
    pIfaceNode = (tIfaceInfo *) NULL;
    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into the fn. FwlDbaseSearchIface \n");
    if (u4IfaceNum < FWL_MAX_NUM_OF_IF)
    {
        pIfaceNode = gFwlAclInfo.apIfaceList[u4IfaceNum];
    }
    else
    {
        pIfaceNode = (tIfaceInfo *) NULL;
    }

    FWL_DBG1 (FWL_DBG_DBASE, "\n InterFace Number = %d\n", u4IfaceNum);
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from the fn. FwlDbaseSearchIface \n");
    return pIfaceNode;
}                                /* End of the function -- FwlDbaseSearchIface */

/* CHANGE3 : The function FwlDbaseSearchInFilter and FwlDbaseSearchOutFilter
 * is merged to form a single function FwlDbaseSearchAclFilter. These two
 * functions have same functionality except the List differs. Though the lists
 * are different, the structure of the nodes that form the Lists are same.
 * Hence these two can be combined to form single function.
 */
/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseSearchAclFilter                          */
/*                                                                          */
/*    Description        : Searches the OutFilter List or The Infilter List */
/*                         for the given filter name.                       */
/*                                                                          */
/*    Input(s)           : au1AclName       -- String containing Filter name*/
/*                         pAclList         -- Pointer to the AclFilter list*/
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Pointer to the filter node if exists, otherwise  */
/*                         returns NULL                                     */
/****************************************************************************/

#ifdef __STDC__
PUBLIC tAclInfo    *FwlDbaseSearchAclFilter
    (tTMO_SLL * pAclList, UINT1 au1AclName[FWL_MAX_ACL_NAME_LEN])
#else
PUBLIC tAclInfo    *
FwlDbaseSearchAclFilter (pAclList, au1AclName)
     tTMO_SLL           *pAclList;
     UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN];
#endif
{
    tAclInfo           *pAclNode = NULL;
    UINT1               u1Status = FWL_NOT_MATCH;
    pAclNode = (tAclInfo *) NULL;
    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering into the fn. FwlDbaseSearchAclFilter \n");
    /* Search the OutFilter List or InFilter List for the Particular filter 
     * name. If found return the pointer to the Node, else NULL.
     */
    TMO_SLL_Scan (pAclList, pAclNode, tAclInfo *)
    {
        if (FWL_STRCMP
            ((INT1 *) au1AclName,
             (INT1 *) pAclNode->au1FilterName) == FWL_STRING_EQUAL)
        {
            u1Status = FWL_MATCH;
            break;
        }
    }                            /* End of the TMO_SLL_Scan */

    if (u1Status == FWL_MATCH)
    {

        FWL_DBG1 (FWL_DBG_DBASE, "\n Acl Name = %s\n", pAclNode->au1FilterName);
        return pAclNode;
    }

    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting from the fn. FwlDbaseSearchAclFilter \n");
    return NULL;
}                                /* End of the function -- FwlDbaseSearchAclFilter */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseFindPositionAndInsert                    */
/*                                                                          */
/*    Description        : Inserts the Node into the correct position based */
/*                         on sequence number                               */
/*                                                                          */
/*    Input(s)           : pAclList    --  IN or OUT Filter List            */
/*                         pAclNode    --  Node to be added in correct      */
/*                                            position                      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if node is added, otherwise          */
/*                         FWL_FAILURE                                      */
/****************************************************************************/

#ifdef __STDC__
PRIVATE UINT4
FwlDbaseFindPositionAndInsert (tTMO_SLL * pAclList, tAclInfo * pAclNode)
#else
PRIVATE UINT4
FwlDbaseFindPositionAndInsert (pAclList, pAclNode)
     tTMO_SLL           *pAclList;
     tAclInfo           *pAclNode;
#endif
{
    tAclInfo           *pAclTmpNode = NULL;
    tAclInfo           *pAclPrevNode = NULL;
    UINT4               u4Status = FWL_SUCCESS;
    pAclPrevNode = (tAclInfo *) NULL;
    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering into the fn." "FwlDbaseFindPositionAndInsert \n");
    TMO_SLL_Scan (pAclList, pAclTmpNode, tAclInfo *)
    {
        /* store the previous pointer in order to add the node in 
         * correct position
         */
        if (pAclTmpNode->u2SeqNum < pAclNode->u2SeqNum)
        {

            FWL_DBG (FWL_DBG_DBASE, "\n The Loop Continues \n");
            pAclPrevNode = pAclTmpNode;
        }
        else if (pAclTmpNode->u2SeqNum == pAclNode->u2SeqNum)
        {
            /* Already a Node exists with the same seq num, so this node
             * won't be added in the list, Free the node return FAILURE. 
             */

            u4Status = FWL_FAILURE;
            FWL_DBG (FWL_DBG_DBASE, "\n Position cannot be Found \n");
            break;
        }
        else
        {
            break;
        }
    }                            /* End for the TMO_SLL_Scan */

    /* if position is found, add the node. */
    if (u4Status == FWL_SUCCESS)
    {
        FWL_DBG (FWL_DBG_DBASE, "\n Position found and Acl Node Added\n");
        TMO_SLL_Insert (pAclList, (tTMO_SLL_NODE *) pAclPrevNode,
                        (tTMO_SLL_NODE *) pAclNode);
    }

    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting from the fn. FwlDbaseFindPositionAndInsert \n");
    return u4Status;
}                                /* End of the function -- FwlDbaseFindPositionAndInsert  */

/* CHANGE3: The function FwlDbaseAddInFilterInSequence and
 * FwlDbaseAddOutFilterInSequence are merged to form singe function
 * FwlDbaseAddAclFilterInSequence. These two functions differ only in the List
 * and the functionality is same. The structure of the nodes that form the
 * Lists are also same. Hence the functions are merged.
 */
/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseAddAclFilterInSequence                   */
/*                                                                          */
/*    Description        : Inserts the Node into OutFilter List or InFilter */
/*                         List based on the sequence Number                */
/*                                                                          */
/*    Input(s)           : u4IfaceNum    -- Interface Number                */
/*                         u2SeqNum      -- Sequence Number                 */
/*                         pAclNode      -- Pointer to the AclFilter node   */
/*                         i4Direction   -- Direction in which the filter is*/
/*                                          applied.                        */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if node is added, otherwise          */
/*                         FWL_FAILURE                                      */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlDbaseAddAclFilterInSequence (UINT4
                                u4IfaceNum,
                                UINT2
                                u2SeqNum, tAclInfo * pAclNode, INT4 i4Direction)
#else
PUBLIC UINT4
FwlDbaseAddAclFilterInSequence (u4IfaceNum, u2SeqNum, pAclNode, i4Direction)
     UINT4               IfaceNum = FWL_ZERO;
     UINT2               u2SeqNum = FWL_ZERO;
     tAclInfo           *pAclNode;
     INT4                i4Direction = FWL_ZERO;
#endif
{
    UINT4               u4Status = FWL_SUCCESS;
    tIfaceInfo         *pIfaceNode = NULL;
    tFilterInfo        *pFilter = NULL;
    tFilterInfo        *pFilterNode = NULL;
    tRuleInfo          *pRule = NULL;
    tRuleInfo          *pRuleFilter = NULL;
    pIfaceNode = (tIfaceInfo *) NULL;
    pFilter = (tFilterInfo *) NULL;
    pFilterNode = (tFilterInfo *) NULL;
    pRule = (tRuleInfo *) NULL;
    pRuleFilter = (tRuleInfo *) NULL;
    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering into the fn. FwlDbaseAddAclFilterInSequence \n");
    /* delete the node form the AclFilter list that is to be added in the list
     * based on the sequence number . Assign the sequence number value to
     * it. Then find the position to be added based on the sequence number.
     * Find the position to insert in the list. if the position could not
     * be identified (if sequence numbers are equal) then free the node.
     */
    pAclNode->u2SeqNum = u2SeqNum;
    pRuleFilter = (tRuleInfo *) pAclNode->pAclName;
    if (pRuleFilter == NULL)
    {
        return FWL_FAILURE;
    }
    /* Since the filter combination cannot be of mixed address type, it 
     * enough if we identify the address type of the first index in the 
     * FilterInfo array. */
    pFilterNode = pRuleFilter->apFilterInfo[FWL_RULE_FIRST_FILTER_INDEX];
    if (pFilterNode == NULL)
    {
        return FWL_FAILURE;
    }

    if (i4Direction == FWL_DIRECTION_OUT)
    {
        if (pFilterNode->u2AddrType == FWL_IP_VERSION_4)
        {
            TMO_SLL_Delete (&gFwlAclInfo.apIfaceList[u4IfaceNum]->outFilterList,
                            (tTMO_SLL_NODE *) pAclNode);
            u4Status =
                FwlDbaseFindPositionAndInsert (&gFwlAclInfo.apIfaceList
                                               [u4IfaceNum]->outFilterList,
                                               pAclNode);
        }
        else if (pFilterNode->u2AddrType == FWL_IP_VERSION_6)
        {
            TMO_SLL_Delete (&gFwlAclInfo.apIfaceList
                            [u4IfaceNum]->outIPv6FilterList,
                            (tTMO_SLL_NODE *) pAclNode);
            u4Status =
                FwlDbaseFindPositionAndInsert (&gFwlAclInfo.apIfaceList
                                               [u4IfaceNum]->outIPv6FilterList,
                                               pAclNode);
        }
        else
        {
            return FWL_FAILURE;
        }

    }
    else
    {                            /* direction is IN */
        if (pFilterNode->u2AddrType == FWL_IP_VERSION_4)
        {
            TMO_SLL_Delete (&gFwlAclInfo.apIfaceList[u4IfaceNum]->inFilterList,
                            (tTMO_SLL_NODE *) pAclNode);
            u4Status =
                FwlDbaseFindPositionAndInsert (&gFwlAclInfo.apIfaceList
                                               [u4IfaceNum]->inFilterList,
                                               pAclNode);
        }
        else if (pFilterNode->u2AddrType == FWL_IP_VERSION_6)
        {
            TMO_SLL_Delete (&gFwlAclInfo.apIfaceList
                            [u4IfaceNum]->inIPv6FilterList,
                            (tTMO_SLL_NODE *) pAclNode);
            u4Status =
                FwlDbaseFindPositionAndInsert (&gFwlAclInfo.apIfaceList
                                               [u4IfaceNum]->inIPv6FilterList,
                                               pAclNode);
        }
        else
        {
            return FWL_FAILURE;
        }
    }
    if (u4Status == FWL_FAILURE)
    {
        if (pAclNode->u1AclType == ACL_FILTER)
        {
            pFilter = (tFilterInfo *) pAclNode->pAclName;
            DEC_FILTER_REF_COUNT (pFilter);
        }
        else
        {                        /* This is for rule */
            pRule = (tRuleInfo *) pAclNode->pAclName;
            DEC_RULE_REF_COUNT (pRule);
            /* Remove the References made by this ACL with the Filters and
             * delete the Rule Node for this ACL */
            FwlDbaseDeleteRule (pRule->au1RuleName);
        }
        /* Free the node */
        FwlAclMemFree ((UINT1 *) pAclNode);
        pIfaceNode = gFwlAclInfo.apIfaceList[u4IfaceNum];
        DEC_IFACE_CONFIG_COUNT (pIfaceNode);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Sequence number already exists.\n"
                 "The Acl Filter is deleted\n");
    }

    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting from the fn. FwlDbaseAddAclFilterInSequence \n");
    return (u4Status);
}                                /* End of the function -- FwlDbaseAddAclFilterInSequence */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseDeleteFilter                             */
/*                                                                          */
/*    Description        : Deletes  the Filter From the Filter List         */
/*                                                                          */
/*    Input(s)           : au1FilterName -- String containing Filter name   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : SUCCESS if Filter Name exists , otherwisw        */
/*                         FAILURE                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlDbaseDeleteFilter (UINT1 au1FilterName[FWL_MAX_FILTER_NAME_LEN])
#else
PUBLIC UINT4
FwlDbaseDeleteFilter (au1FilterName)
     UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN];
#endif
{
    UINT4               u4Status = FWL_FAILURE;
    tFilterInfo        *pFilterNode = NULL;
    pFilterNode = (tFilterInfo *) NULL;
    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into the fn. FwlDbaseDeleteFilter \n");
    /* search for the filter node in the filter list that is to be deleted 
     * Get the filter pointer from the filter list 
     */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    /* if the node is found, reference count is 0 and classifier handle is
     * NULL , then delete the node .
     */
    if ((pFilterNode != NULL) &&
        (pFilterNode->u1FilterRefCount == FWL_DEFAULT_COUNT))
    {
        TMO_SLL_Delete (&gFwlAclInfo.filterList, (tTMO_SLL_NODE *) pFilterNode);
        FWL_DBG (FWL_DBG_DBASE, "\n Filter Node is deleted \n");
        /* Free the Node */
        FwlFilterMemFree ((UINT1 *) pFilterNode);
        u4Status = FWL_SUCCESS;
        FWL_DBG (FWL_DBG_DBASE, "\n Memory Free - Filter Node \n");
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from the fn. FwlDbaseDeleteFilter \n");
    return u4Status;
}                                /* End of the function -- FwlDbaseDeleteFilter */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseDeleteSnork                              */
/*                                                                          */
/*    Description        : Deletes  the Filter From the Filter List         */
/*                                                                          */
/*    Input(s)           : PortNo  -- String containing Filter name         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : SUCCESS if Filter Name exists , otherwisw        */
/*                         FAILURE                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlDbaseDeleteSnork (INT4 i4PortNo)
#else
PUBLIC UINT4
FwlDbaseDeleteSnork (i4PortNo)
    INT4 i4PortNo;
#endif
{
    UINT4               u4Status = FWL_FAILURE;
    tFwlSnorkInfo        *pSnorkNode = NULL;
    pSnorkNode = (tFwlSnorkInfo *) NULL;
    pSnorkNode = FwlDbaseSearchSnork (i4PortNo);
    
    if (pSnorkNode != NULL)
    {
        TMO_SLL_Delete (&gFwlSnorkList, (tTMO_SLL_NODE *) pSnorkNode);
        FwlSnorkMemFree ((UINT1 *) pSnorkNode);
        u4Status = FWL_SUCCESS;
        FWL_DBG (FWL_DBG_DBASE, "\n Memory Free - Snork Node \n");
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from the fn. FwlDbaseDeleteSnork \n");
    return u4Status;
}                                /* End of the function -- FwlDbaseDeleteFilter */



/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseDeleteRpfEntry                           */
/*                                                                          */
/*    Description        : Deletes  the Filter From the Filter List         */
/*                                                                          */
/*    Input(s)           : PortNo  -- String containing Filter name         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : SUCCESS if Filter Name exists , otherwisw        */
/*                         FAILURE                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlDbaseDeleteRpfEntry (INT4 i4PortNo)
#else
PUBLIC UINT4
FwlDbaseDeleteRpfEntry (i4PortNo)
    INT4 i4PortNo;
#endif
{
    UINT4               u4Status = FWL_FAILURE;
    tFwlRpfEntryInfo        *pRpfEntryNode = NULL;
    pRpfEntryNode = (tFwlRpfEntryInfo *) NULL;
    pRpfEntryNode = FwlDbaseSearchRpfEntry (i4PortNo);

    if (pRpfEntryNode != NULL)
    {
        TMO_SLL_Delete (&gFwlRpfEntryList, (tTMO_SLL_NODE *) pRpfEntryNode);
        FwlRpfEntryMemFree ((UINT1 *) pRpfEntryNode);
        u4Status = FWL_SUCCESS;
        FWL_DBG (FWL_DBG_DBASE, "\n Memory Free - RpfEntry Node \n");
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from the fn. FwlDbaseDeleteRpfEntry \n");
    return u4Status;
}                                /* End of the function -- FwlDbaseDeleteFilter */


/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseDeleteRule                               */
/*                                                                          */
/*    Description        : Deletes the Node From Rule List                  */
/*                                                                          */
/*    Input(s)           : au1RuleName      -- String Containing the Name   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if node is exists, otherwise         */
/*                         FWL_FAILURE.                                     */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlDbaseDeleteRule (UINT1 au1RuleName[FWL_MAX_RULE_NAME_LEN])
#else
PUBLIC UINT4
FwlDbaseDeleteRule (au1RuleName)
     UINT1               au1RuleName[FWL_MAX_RULE_NAME_LEN];
#endif
{
    UINT4               u4Status = FWL_FAILURE;
    UINT2               u2Index = FWL_ZERO;
    tRuleInfo          *pRuleNode = NULL;
    tFilterInfo        *pFilterNode = NULL;
    pRuleNode = (tRuleInfo *) NULL;
    pFilterNode = (tFilterInfo *) NULL;
    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into the fn. FwlDbaseDeleteRule \n");
    /* search the rule list for the particular rule name */
    pRuleNode = FwlDbaseSearchRule (au1RuleName);
    /* if the rule node is found and reference count is 0 then delete the 
     * rule node. 
     */
    if ((pRuleNode != NULL) && (pRuleNode->u1RuleRefCount == FWL_DEFAULT_COUNT))
    {
        for (u2Index = FWL_ZERO; u2Index < FWL_MAX_FILTERS_IN_RULE; u2Index++)
        {
            if (pRuleNode->apFilterInfo[u2Index] != NULL)
            {
                pFilterNode = pRuleNode->apFilterInfo[u2Index];
                if (pFilterNode->u1FilterRefCount != FWL_DEFAULT_COUNT)
                {
                    DEC_FILTER_REF_COUNT (pFilterNode);
                }
            }
        }
        TMO_SLL_Delete (&gFwlAclInfo.ruleList, (tTMO_SLL_NODE *) pRuleNode);
        FWL_DBG (FWL_DBG_DBASE, "\n Rule Node is deleted \n");
        /* Free the Node */
        FwlRuleMemFree ((UINT1 *) pRuleNode);
        FWL_DBG (FWL_DBG_DBASE, "\n Memory Free - Rule Node \n");
        u4Status = FWL_SUCCESS;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from the fn. FwlDbaseDeleteRule \n");
    return u4Status;
}                                /* End of the function -- FwlDbaseDeleteRule */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseDelNodeAndUpdateStat                     */
/*                                                                          */
/*    Description        : Deletes the Node in the Acl List and updates     */
/*                         statistics.                                      */
/*                                                                          */
/*    Input(s)           : pAclNode       -- Pointer to the node to be      */
/*                                           deleted.                       */
/*                         pIfaceNode     -- Pointer Interface node         */
/*                         u1Direction    -- The direction in which the     */
/*                                           node to be deleted is present  */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if node is deleted, otherwise        */
/*                         FWL_FAILURE.                                     */
/****************************************************************************/

#ifdef __STDC__
PRIVATE UINT4
FwlDbaseDelNodeAndUpdateStat (tIfaceInfo *
                              pIfaceNode,
                              UINT1 u1Direction,
                              tAclInfo * pAclNode, UINT1 u1AddrType)
#else
PRIVATE UINT4
FwlDbaseDelNodeAndUpdateStat (pIfaceNode, u1Direction, pAclNode, u1AddrType)
     tIfaceInfo         *pIfaceNode;
     UINT1               u1Direction = FWL_ZERO;
     tAclInfo           *pAclNode;
     UINT1               u1AddrType = FWL_ZERO;
#endif
{
    tRuleInfo          *pRuleNode = (tRuleInfo *) NULL;
    tFilterInfo        *pFilterNode = (tFilterInfo *) NULL;
    UINT4               u4Status = FWL_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering into the fn. FwlDbaseDelNodeAndUpdateStat \n");
    /* if the node is not null then find what type of node it is, whether
     * filter node or rule node . Based on that decrement 
     * reference count  and delete the node.
     */
    if (pAclNode != NULL)
    {
        if (pAclNode->u1AclType == ACL_FILTER)
        {
            pFilterNode = (tFilterInfo *) pAclNode->pAclName;
            if (pFilterNode != NULL)
            {
                DEC_FILTER_REF_COUNT (pFilterNode);
                FWL_DBG1 (FWL_DBG_DBASE,
                          "\n Filter Reference Count = %d \n",
                          pFilterNode->u1FilterRefCount);
            }
        }
        else                    /* This is of type RULE */
        {
            pRuleNode = (tRuleInfo *) pAclNode->pAclName;
            DEC_RULE_REF_COUNT (pRuleNode);
            FWL_DBG1 (FWL_DBG_DBASE,
                      "\n Rule Reference Count = %d \n",
                      pRuleNode->u1RuleRefCount);
        }
        if (u1Direction == FWL_DIRECTION_IN)
        {
            if (u1AddrType == FWL_IP_VERSION_4)
            {
                TMO_SLL_Delete (&pIfaceNode->inFilterList,
                                (tTMO_SLL_NODE *) pAclNode);
            }
            else if (u1AddrType == FWL_IP_VERSION_6)
            {
                TMO_SLL_Delete (&pIfaceNode->inIPv6FilterList,
                                (tTMO_SLL_NODE *) pAclNode);
            }
            else
            {
                return FWL_FAILURE;
            }
            /* Free the Node */
            FwlAclMemFree ((UINT1 *) pAclNode);
            FWL_DBG (FWL_DBG_DBASE, "\n In Filter Node Deleted\n");
        }
        else
        {
            if (u1AddrType == FWL_IP_VERSION_4)
            {
                TMO_SLL_Delete (&pIfaceNode->outFilterList,
                                (tTMO_SLL_NODE *) pAclNode);
            }
            else if (u1AddrType == FWL_IP_VERSION_6)
            {
                TMO_SLL_Delete (&pIfaceNode->outIPv6FilterList,
                                (tTMO_SLL_NODE *) pAclNode);
            }
            else
            {
                return FWL_FAILURE;
            }
            /* Free the Node */
            FwlAclMemFree ((UINT1 *) pAclNode);
            FWL_DBG (FWL_DBG_DBASE, "\n Out Filter Node Deleted\n");
        }
        DEC_IFACE_CONFIG_COUNT (pIfaceNode);
        FWL_DBG2 (FWL_DBG_DBASE,
                  "\n Config Count for If %d = %d\n",
                  pIfaceNode->u4IfaceNum,
                  pIfaceNode->ifaceStat.u4AclConfigCount);
        u4Status = FWL_SUCCESS;
    }
    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting from the fn. FwlDbaseDelNodeAndUpdateStat \n");
    return u4Status;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseDeleteAcl                                */
/*                                                                          */
/*    Description        : Deletes the AclNode From Acl List                */
/*                                                                          */
/*    Input(s)           : au1AclName      -- String Containing the Name    */
/*                         u4IfaceNum      -- Interface number              */
/*                         i4Direction    -- specifies Direction            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if node is deleted, otherwise        */
/*                         FWL_FAILURE.                                     */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlDbaseDeleteAcl (UINT4 u4IfaceNum,
                   INT4 i4Direction, UINT1 au1AclName[FWL_MAX_FILTER_NAME_LEN])
#else
PUBLIC UINT4
FwlDbaseDeleteAcl (u4IfaceNum, i4Direction, au1AclName)
     UINT4               u4IfaceNum = FWL_ZERO;
     INT4                i4Direction = FWL_ZERO;
     UINT1               au1AclName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
#endif
{
    UINT4               u4Status = FWL_SUCCESS;
    UINT4               u4IfaceIndex = FWL_ZERO;
    UINT1               u1StatusFlag = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;
    tInFilterInfo      *pInFilterNode = NULL;
    tOutFilterInfo     *pOutFilterNode = NULL;
    tInIpv6FilterInfo  *pInIpv6FilterNode = NULL;
    tOutIpv6FilterInfo *pOutIpv6FilterNode = NULL;

    /* initialise the interface index and status */
    pIfaceNode = (tIfaceInfo *) NULL;
    pInFilterNode = (tInFilterInfo *) NULL;
    pOutFilterNode = (tOutFilterInfo *) NULL;
    pInIpv6FilterNode = (tInIpv6FilterInfo *) NULL;
    pOutIpv6FilterNode = (tOutIpv6FilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into the fn. FwlDbaseDeleteAcl \n");
    /* if interface number is less than max value then search for that 
     * interface number. if direction is IN then search the infilter
     * list for the particular ACl name. if found delete it and update 
     * the statistics like decrementing the reference count and filter
     * count.  The while loop will be executed only once when the Interface
     * number is less than FWL_MAX_NUM_OF_IF.
     */
    /* CHANGE1 : If the Acl node is to be deleted globally , then the node
     * should be searched in all interfaces in the particular direction and
     * deleted.
     */
    if (u4IfaceNum < FWL_MAX_NUM_OF_IF)
    {
        u4IfaceIndex = FWL_MAX_NUM_OF_IF - FWL_ONE;
    }
    else if (u4IfaceNum == FWL_MAX_NUM_OF_IF)
    {
        u4IfaceNum = FWL_ZERO;
        u4IfaceIndex = FWL_ZERO;
    }
    while (u4IfaceIndex < FWL_MAX_NUM_OF_IF)
    {
        pIfaceNode = gFwlAclInfo.apIfaceList[u4IfaceNum];
        if (pIfaceNode != NULL)
        {
            if (i4Direction == FWL_DIRECTION_IN)
            {
                /* search the filter in the infilter list and if found,
                 * delete it and update the statistics.
                 */
                pInFilterNode = FwlDbaseSearchAclFilter
                    (&pIfaceNode->inFilterList, au1AclName);
                if (pInFilterNode != NULL)
                {
                    u4Status = FwlDbaseDelNodeAndUpdateStat (pIfaceNode,
                                                             FWL_DIRECTION_IN,
                                                             pInFilterNode,
                                                             FWL_IP_VERSION_4);
                }
                /* Node not found in infilter.Search in inIPv6FilterList */
                else
                {
                    pInIpv6FilterNode = FwlDbaseSearchAclFilter
                        (&pIfaceNode->inIPv6FilterList, au1AclName);
                    /* It is an IPv6 filter. So send the address type 
                     * to delete the corresponding list */
                    u4Status = FwlDbaseDelNodeAndUpdateStat (pIfaceNode,
                                                             FWL_DIRECTION_IN,
                                                             pInIpv6FilterNode,
                                                             FWL_IP_VERSION_6);
                }
                if (u4Status == FWL_SUCCESS)
                {
                    u1StatusFlag++;
                }
            }
            else
            {                    /* Direction is Out */
                /* search the filter in the outfilter list and if found,
                 * delete it and update the statistics.
                 */
                pOutFilterNode = FwlDbaseSearchAclFilter
                    (&pIfaceNode->outFilterList, au1AclName);
                if (pOutFilterNode != NULL)
                {
                    u4Status = FwlDbaseDelNodeAndUpdateStat (pIfaceNode,
                                                             FWL_DIRECTION_OUT,
                                                             pOutFilterNode,
                                                             FWL_IP_VERSION_4);
                }
                else
                {
                    pOutIpv6FilterNode = FwlDbaseSearchAclFilter
                        (&pIfaceNode->outIPv6FilterList, au1AclName);
                    /* It is an IPv6 filter. So send the address type
                     * to delete the corresponding list */

                    u4Status = FwlDbaseDelNodeAndUpdateStat (pIfaceNode,
                                                             FWL_DIRECTION_OUT,
                                                             pOutIpv6FilterNode,
                                                             FWL_IP_VERSION_6);
                }
                if (u4Status == FWL_SUCCESS)
                {
                    u1StatusFlag++;
                }
            }
        }
        u4IfaceNum++;
        u4IfaceIndex++;
    }
    if (u1StatusFlag > FWL_ZERO)
    {
        FwlCommitAcl ();
        u4Status = FWL_SUCCESS;
    }
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from the fn. FwlDbaseDeleteAcl \n");
    return u4Status;
}                                /* End of the function -- FwlDbaseDeleteAcl */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseDeleteIface                              */
/*                                                                          */
/*    Description        : Deletes the IfaceNode from InterFace List        */
/*                                                                          */
/*    Input(s)           : u4IfaceNum       -- Interface Number             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if node is deleted, otherwise        */
/*                         FWL_FAILURE.                                     */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlDbaseDeleteIface (UINT4 u4IfaceNum)
#else
PUBLIC UINT4
FwlDbaseDeleteIface (u4IfaceNum)
     UINT4               u4IfaceNum;
#endif
{
    UINT4               u4Status = FWL_FAILURE;
    tIfaceInfo         *pIfaceNode = NULL;
    pIfaceNode = (tIfaceInfo *) NULL;
    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into the fn. FwlDbaseDeleteIface \n");
    /* Delete the  interface node if the infilter list and the outfilter
     * list are NULL
     */
    /* If the interface number is a global one then deletion is not possible */
    if ((u4IfaceNum < FWL_MAX_NUM_OF_IF) && (u4IfaceNum != FWL_GLOBAL_IDX))
    {
        pIfaceNode = gFwlAclInfo.apIfaceList[u4IfaceNum];
        if (pIfaceNode->ifaceStat.u4AclConfigCount == FWL_DEFAULT_COUNT)
        {
            FwlIfaceMemFree ((UINT1 *) pIfaceNode);
            FWL_DBG (FWL_DBG_DBASE, "\n Memory Release - InterFace Node \n");
            gFwlAclInfo.apIfaceList[u4IfaceNum] = NULL;
            u4Status = FWL_SUCCESS;
        }
        else
        {
            u4Status = FWL_FAILURE;
        }
    }
    else
    {
        u4Status = FWL_FAILURE;
    }
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from the fn. FwlDbaseDeleteIface \n");
    return u4Status;
}                                /* End of the function -- FwlDbaseDeleteIface */

/* CHANGE2 : A filter or a rule is added at the end of their
 * coressponding list(when created). SNMP GETNEXT requires to display the list
 * in lexiographical order. In order to add to the list in sorted order the
 * following 3 functions are added.
 */
/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseAddFilter                                */
/*                                                                          */
/*    Description        : Add the Filter Node to Filter List in the sorted */
/*                         order                                            */
/*                                                                          */
/*    Input(s)           : pFilterNode  -- Pointer to the Filter Node       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/
#ifdef __STDC__
PUBLIC VOID
FwlDbaseAddFilter (tFilterInfo * pFilterNode)
#else
PUBLIC VOID
FwlDbaseAddFilter (pFilterNode)
     tFilterInfo        *pFilterNode;
#endif
{
    tFilterInfo        *pFilterTmpNode = NULL;
    tFilterInfo        *pFilterPrevNode = NULL;
    pFilterTmpNode = (tFilterInfo *) NULL;
    pFilterPrevNode = (tFilterInfo *) NULL;
    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into function FwlDbaseAddFilter \n");
    /* Search the filter list and add the node in sorted order. */
    TMO_SLL_Scan (&(gFwlAclInfo.filterList), pFilterTmpNode, tFilterInfo *)
    {
        if (FWL_STRLEN (pFilterTmpNode->au1FilterName)
            > FWL_STRLEN (pFilterNode->au1FilterName))
        {
            break;
        }
        if (FWL_STRLEN (pFilterTmpNode->au1FilterName)
            == FWL_STRLEN (pFilterNode->au1FilterName))
        {
            if (FWL_STRCMP (pFilterTmpNode->au1FilterName,
                            pFilterNode->au1FilterName) > FWL_ZERO)
            {
                break;
            }
        }
        pFilterPrevNode = pFilterTmpNode;
    }
    TMO_SLL_Insert (&(gFwlAclInfo.filterList),
                    (tTMO_SLL_NODE *) pFilterPrevNode,
                    (tTMO_SLL_NODE *) pFilterNode);
    FWL_DBG (FWL_DBG_EXIT, "\nExiting from function FwlDbaseAddFilter \n");
}                                /* End of the function -- FwlDbaseAddFilter */
/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseAddSnork                                 */
/*                                                                          */
/*    Description        : Add the Filter Node to Filter List in the sorted */
/*                         order                                            */
/*                                                                          */
/*    Input(s)           : pFilterNode  -- Pointer to the Filter Node       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/
#ifdef __STDC__
PUBLIC VOID
FwlDbaseAddSnork (tFwlSnorkInfo * pSnorkNode)
#else
PUBLIC VOID
FwlDbaseAddSnork (pSnorkNode)
     tFwlSnorkInfo        *pSnorkNode;
#endif
{
    tFwlSnorkInfo        *pSnorkTmpNode = NULL;
    tFwlSnorkInfo        *pSnorkPrevNode = NULL;
    if (pSnorkNode != NULL)
    {
        TMO_SLL_Scan (&gFwlSnorkList, pSnorkTmpNode, tFwlSnorkInfo *)
        {
            if (pSnorkTmpNode->i4PortNo > pSnorkNode->i4PortNo)
            {
                break;
            }
            pSnorkPrevNode = pSnorkTmpNode;
        }
        TMO_SLL_Insert (&gFwlSnorkList, (tTMO_SLL_NODE *) pSnorkPrevNode,
                        (tTMO_SLL_NODE *) pSnorkNode);
    }
 
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseAddRpfEntry                              */
/*                                                                          */
/*    Description        : Add the Filter Node to Filter List in the sorted */
/*                         order                                            */
/*                                                                          */
/*    Input(s)           : pFilterNode  -- Pointer to the Filter Node       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/
#ifdef __STDC__
PUBLIC VOID
FwlDbaseAddRpfEntry (tFwlRpfEntryInfo * pRpfEntryNode)
#else
PUBLIC VOID
FwlDbaseAddRpfEntry (pRpfEntryNode)
     tFwlRpfEntryInfo        *pRpfEntryNode;
#endif
{
    tFwlRpfEntryInfo        *pRpfEntryTmpNode = NULL;
    tFwlRpfEntryInfo        *pRpfEntryPrevNode = NULL;
    if (pRpfEntryNode != NULL)
    {
        TMO_SLL_Scan (&gFwlRpfEntryList, pRpfEntryTmpNode, tFwlRpfEntryInfo *)
        {
            if (pRpfEntryTmpNode->i4IfIndex > pRpfEntryNode->i4IfIndex)
            {
                break;
            }
            pRpfEntryPrevNode = pRpfEntryTmpNode;
        }
        TMO_SLL_Insert (&gFwlRpfEntryList, (tTMO_SLL_NODE *) pRpfEntryPrevNode,
                        (tTMO_SLL_NODE *) pRpfEntryNode);
    }
 
} 
/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseAddRule                                  */
/*                                                                          */
/*    Description        : Add the Rule Node to Rule List in the sorted     */
/*                         order                                            */
/*                                                                          */
/*    Input(s)           : pRuleNode  -- Pointer to the Rule Node           */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/
#ifdef __STDC__
PUBLIC VOID
FwlDbaseAddRule (tRuleInfo * pRuleNode)
#else
PUBLIC VOID
FwlDbaseAddRule (pRuleNode)
     tRuleInfo          *pRuleNode;
#endif
{
    tRuleInfo          *pRuleTmpNode = NULL;
    tRuleInfo          *pRulePrevNode = NULL;
    pRuleTmpNode = (tRuleInfo *) NULL;
    pRulePrevNode = (tRuleInfo *) NULL;
    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into function FwlDbaseAddRule \n");
    /* Search the Rule list and add the node in sorted order. */
    TMO_SLL_Scan (&(gFwlAclInfo.ruleList), pRuleTmpNode, tRuleInfo *)
    {
        if (FWL_STRLEN (pRuleTmpNode->au1RuleName)
            > FWL_STRLEN (pRuleNode->au1RuleName))
        {
            break;
        }
        if (FWL_STRLEN (pRuleTmpNode->au1RuleName)
            == FWL_STRLEN (pRuleNode->au1RuleName))
        {
            if (MEMCMP (pRuleTmpNode->au1RuleName, pRuleNode->au1RuleName,
                        FWL_MAX_RULE_NAME_LEN) > FWL_ZERO)
            {
                break;
            }
        }
        pRulePrevNode = pRuleTmpNode;
    }
    TMO_SLL_Insert (&(gFwlAclInfo.ruleList),
                    (tTMO_SLL_NODE *) pRulePrevNode,
                    (tTMO_SLL_NODE *) pRuleNode);
    FWL_DBG (FWL_DBG_EXIT, "\nExiting from function FwlDbaseAddRule \n");
}                                /* End of the function -- FwlDbaseAddRule */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlMatchStateEntryInAcl                          */
/*                                                                          */
/*    Description        : This function check the 5-tuple of state entry   */
/*                         in the ACL based on the direction. starting      */
/*                         with highest priority. There are two cases:      */
/*                 Case 1: If the rule has been modified to DENY or         */
/*                         if there is no rule which matches the state      */
/*                         entry, then the state entry is deleted and       */
/*                         session count is updated.                        */
/*                 Case 2: If the log level is modified in the rule, update */
/*                         the same log level in the state entry also.      */
/*                                                                          */
/*    Input(s)           : ppStateEntry - ptr to ptr to a stateful entry    */
/*                                                                          */
/*    Output(s)          : ppStateEntry - If rule has modified the log level*/
/*                                                                          */
/*    Returns            : FWL_DELETE_STATE_ENTRY | FWL_MATCH               */
/****************************************************************************/

PUBLIC UINT4
FwlMatchStateEntryInAcl (tStatefulSessionNode ** ppStateEntry)
{
    tStatefulSessionNode *pState = NULL;
    tAclInfo           *pAclNode = NULL;
    tFilterInfo        *pFilterInfo = NULL;
    tTMO_SLL           *pAclList = NULL;
    tIfaceInfo         *pIfaceNode = NULL;
    tRuleInfo          *pRuleFilter = NULL;
    UINT4               u4IfaceNum = FWL_ZERO;
    tFwlIpAddr          aIpAddr[FWL_IP_ADDR_LEN];
    UINT2               au2Port[FWL_PORT_NO_LEN] = { FWL_ZERO };
    UINT1               u1Protocol = FWL_ZERO;
    UINT1               u1Local = FWL_ZERO;
    UINT1               u1Dir = FWL_ZERO;
    UINT1               u1FilterIndex = FWL_ZERO;

    pState = *ppStateEntry;

    u1Dir = pState->u1Direction;
    u1Local = (UINT1) (u1Dir == FWL_DIRECTION_IN);

    u1Protocol = pState->u1Protocol;
    if (u1Protocol != FWL_ICMP)
    {
        /* Update port info for TCP/UDP */
        au2Port[FWL_INDEX_0] = (UINT2) FWL_LOCAL_PORT (pState, u1Protocol);
        au2Port[FWL_INDEX_1] = (UINT2) FWL_REMOTE_PORT (pState, u1Protocol);
    }

    FWL_IP_ADDR_COPY (&(aIpAddr[FWL_INDEX_0]), &(pState->LocalIP));
    FWL_IP_ADDR_COPY (&(aIpAddr[FWL_INDEX_1]), &(pState->RemoteIP));

    for (u4IfaceNum = FWL_ZERO; u4IfaceNum < FWL_MAX_NUM_OF_IF; u4IfaceNum++)
    {
        pIfaceNode = gFwlAclInfo.apIfaceList[u4IfaceNum];

        if (pIfaceNode == NULL)
        {
            continue;
        }

        if (aIpAddr[FWL_INDEX_0].u4AddrType == FWL_IP_VERSION_4)
        {
            pAclList = (u1Dir == FWL_DIRECTION_OUT) ?
                &pIfaceNode->outFilterList : &pIfaceNode->inFilterList;
        }
        else
        {
            pAclList = (u1Dir == FWL_DIRECTION_OUT) ?
                &pIfaceNode->outIPv6FilterList : &pIfaceNode->inIPv6FilterList;
        }

        TMO_SLL_Scan (pAclList, pAclNode, tAclInfo *)
        {
            if ((pRuleFilter = (tRuleInfo *) pAclNode->pAclName) != NULL)
            {
                if ((pFilterInfo =
                     (tFilterInfo *) pRuleFilter->
                     apFilterInfo[u1FilterIndex]) != NULL)
                {
                    /* First check, if globalAclSchedule is ENABLED and if 
                     * this ACL is a schduled one and global schedule 
                     * status is FWL_ACTIVE. */
                    if ((gu1AclScheduleStatus == FWL_NOT_IN_SERVICE) &&
                        (pAclNode->u4Scheduled == FWL_ENABLE))
                    {
                        return (FWL_DELETE_STATE_ENTRY);
                    }

                    /* Next check, if the IP Address and Protocol matches.... */
                    if (aIpAddr[u1Local].u4AddrType == FWL_IP_VERSION_4)
                    {
                        if (!
                            ((aIpAddr[u1Local].v4Addr >=
                              pFilterInfo->SrcStartAddr.v4Addr)
                             && (aIpAddr[u1Local].v4Addr <=
                                 pFilterInfo->SrcEndAddr.v4Addr)
                             && (aIpAddr[!u1Local].v4Addr >=
                                 pFilterInfo->DestStartAddr.v4Addr)
                             && (aIpAddr[!u1Local].v4Addr <=
                                 pFilterInfo->DestEndAddr.v4Addr)))
                        {
                            /* If it does not match, check the next ACL */
                            continue;
                        }
                    }
                    else if (aIpAddr[u1Local].u4AddrType == FWL_IP_VERSION_6)
                    {
                        if (Ip6IsAddrInBetween (&aIpAddr[u1Local].v6Addr,
                                                &pFilterInfo->SrcStartAddr.
                                                v6Addr,
                                                &pFilterInfo->SrcEndAddr.
                                                v6Addr) == IP6_FAILURE)
                        {
                            continue;
                        }
                        if (Ip6IsAddrInBetween (&aIpAddr[!u1Local].v6Addr,
                                                &pFilterInfo->DestStartAddr.
                                                v6Addr,
                                                &pFilterInfo->DestEndAddr.
                                                v6Addr) == IP6_FAILURE)
                        {
                            continue;
                        }
                    }
                    if (!
                        ((u1Protocol == pFilterInfo->u1Proto)
                         || (pFilterInfo->u1Proto == FWL_DEFAULT_PROTO)))
                    {
                        continue;
                    }

                    /* Check the port numbers for tcp/udp packets */
                    switch (u1Protocol)
                    {
                        case FWL_TCP:
                        case FWL_UDP:
                            if (FWL_CHECK_PORT_RANGE (au2Port[u1Local],
                                                      pFilterInfo->u2SrcMaxPort,
                                                      pFilterInfo->u2SrcMinPort)
                                != FWL_MATCH)
                            {
                                /* Source Port Range does not match... */
                                continue;
                            }

                            if (FWL_CHECK_PORT_RANGE (au2Port[!u1Local],
                                                      pFilterInfo->
                                                      u2DestMaxPort,
                                                      pFilterInfo->
                                                      u2DestMinPort) !=
                                FWL_MATCH)
                            {
                                /* Destination Port Range does not match... */
                                continue;
                            }

                        default:
                            break;
                    }

                    /* At this point, the 5-tuple in the stateful entry
                     * matches the ACL. */

                    /* case 1: */
                    if (pAclNode->u1Action == FWL_DENY)
                    {
                        return (FWL_DELETE_STATE_ENTRY);
                    }

                    /* case 2: */
                    pState->u1LogLevel = pAclNode->u1LogTrigger;
                    return (FWL_MATCH);

                }                /* pFilterInfo != NULL */

            }                    /* pRuleFilter != NULL */

        }                        /* TMO_SLL_Scan */
    }                            /* End of For loop */
    return (FWL_DELETE_STATE_ENTRY);

}                                /* End of function */

VOID
FwlCommitAcl (VOID)
{
    FwlGarbageCollect (FWL_DONT_FLUSH, FWL_COMMIT);
    return;
}

/* VPND This function adds Firewall rules to Physical Interface 
 * If Access method is PPTP/L2TP */
VOID
FwlAddWanInfoOnPhysicalIf (UINT4 u4PppIfIndex, UINT4 u4PhysIfIndex)
{
    tIfaceInfo         *pPppInfo = NULL;
    tIfaceInfo         *pPhysicalInfo = NULL;
    if ((u4PppIfIndex >= FWL_MAX_NUM_OF_IF) ||
        (u4PhysIfIndex >= FWL_MAX_NUM_OF_IF))
    {
        /* Beyand Firewall Interface Range */
        return;
    }
    pPppInfo = gFwlAclInfo.apIfaceList[u4PppIfIndex];
    pPhysicalInfo = gFwlAclInfo.apIfaceList[u4PhysIfIndex];

    if ((pPppInfo == NULL) || (pPhysicalInfo != NULL))
    {
        /* Source IfInfo is NULL (or) Destination IfInfo is not NULL */
        return;
    }
    gFwlAclInfo.apIfaceList[u4PhysIfIndex] = pPppInfo;
}

/****************************************************************************
 Function    :  FwlGetReservedPriority    
 Input       :  pPriority, u4Direction, u4IfaceNum.
               
 Output      :  This API will get the priority value from dynamic reserved
                value.
 Returns     :  FWL_DYNAMIC_PRIORITY_NOT_AVAILABLE on failure
                FWL_DYNAMIC_PRIORITY_AVAILABLE, on success
****************************************************************************/

UINT4
FwlGetReservedPriority (UINT4 *pPriority, UINT4 u4Direction, UINT4 u4IfaceNum)
{
    tIfaceInfo         *pIfaceInfo = NULL;
    tTMO_SLL           *pAclLst = NULL;
    tAclInfo           *pAclInfo = NULL;
    INT4                i4PrevPriority = FWL_ZERO;

    pAclInfo = (tAclInfo *) NULL;
    pIfaceInfo = (tIfaceInfo *) NULL;

    pIfaceInfo = gFwlAclInfo.apIfaceList[u4IfaceNum];

    if (pIfaceInfo != NULL)
    {
        /* Get the filter list pointer from the interface structure */
        if (u4Direction == FWL_IN_FILTER)
        {
            pAclLst = &pIfaceInfo->inFilterList;
        }
        else
        {
            pAclLst = &pIfaceInfo->outFilterList;
        }

        /* Scan the IN or OUT list and check the packet against the
         * configured Filter, or Rules
         */

        i4PrevPriority = FWL_DYNAMIC_MIN_RESV_PRIORITY - FWL_ONE;
        /* If no node is available then assign previous prioriy as 1499 */

        TMO_SLL_Scan (pAclLst, pAclInfo, tAclInfo *)
        {
            if ((pAclInfo->u2SeqNum < FWL_DYNAMIC_MIN_RESV_PRIORITY) ||
                ((pAclInfo->u2SeqNum - FWL_ONE) == i4PrevPriority))
            {
                /* If node found, but the available priority is less then
                 * 1499, then continue to next node. We will assign reserved
                 * priority value only when it crosses 1500 */
                if (pAclInfo->u2SeqNum >= i4PrevPriority)
                {

                    i4PrevPriority = pAclInfo->u2SeqNum;
                }
            }
            else
            {
                break;
            }
        }                        /* Scan ACL list */

        *pPriority = (UINT4) (i4PrevPriority + FWL_ONE);
        return FWL_DYNAMIC_PRIORITY_AVAILABLE;

    }                            /* (pIfaceInfo != NULL) */

    return (FWL_DYNAMIC_PRIORITY_NOT_AVAILABLE);
}

/******************************************************************************/
/*                      End of File fwldbase.c                                */
/******************************************************************************/
