/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlinit.c,v 1.25 2016/02/27 10:05:03 siva Exp $
 *
 * Description:This file contains all datastructures and  
 *             Mem pool initializing routines.           
 *
 *******************************************************************/
/* This file declares the global structure required for the module */
#ifndef _FWLINIT_C_
#define _FWLINIT_C_
#define FIREWALL_GLOBAL
#include "fwlinc.h"
#include "fssyslog.h"
#include "msr.h"
#include "iss.h"
#include "fwlglob.h"
#include "fwlnp.h"
/*****************************************************************************/
/*       P R O T O T Y P E    D E C L A R A T I O N S                        */
/*****************************************************************************/

#ifdef FUTURE_SNMP_WANTED
#include "include.h"
#include "fsfwlmdb.h"
INT4 RegisterFSFWLwithFutureSNMP PROTO ((void));
extern INT4 SNMP_AGT_RegisterMib PROTO ((tSNMP_GroupOIDType *,
                                         tSNMP_BaseOIDType *,
                                         tSNMP_MIBObjectDescrType *,
                                         tSNMP_GLOBAL_STRUCT, INT4));
#endif /* FUTURE_SNMP_WANTED */

extern void RegisterFSFWL PROTO ((void));
tTMO_SLL            gFwlSnorkList;
tTMO_SLL            gFwlRpfEntryList;
/*****************************************************************************/
/*                                                                           */
/*    Function Name             : FwlAclInit                                 */
/*                                                                           */
/*    Description               : Initialises all global variables           */
/*                                for default values.                        */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : FWL_SUCCESS if initialisation succeeds,    */
/*                                otherwise FWL_FAILURE.                     */
/*****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlAclInit (VOID)
#else
PUBLIC UINT4
FwlAclInit ()
#endif
{

    UINT4               u4IfaceNum = FWL_ZERO;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering FwlAclInit \n");

    /* The value is initially 1 and is controlled through MIB object */
    /* Changed the value of default trace from 1 to 0 */
    gFwlAclInfo.u4FwlTrc = FWL_SET_TRACE_INIT;
    gFwlAclInfo.u4FwlDbg = FWL_SET_TRACE_INIT;

    MOD_TRC (gFwlAclInfo.u4FwlTrc, INIT_SHUT_TRC, "FWL",
             "\n Memory  Pool Allocation \n");

    /* Create buffer pools for data structures */
    if (FirewallSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        FirewallSizingMemDeleteMemPools ();
        return FWL_FAILURE;
    }
    /* Initializing the SLL list for the Filter, Rule, Log, Authen filter
     */

    MOD_TRC (gFwlAclInfo.u4FwlTrc, INIT_SHUT_TRC, "FWL",
             "\n Initializing Global Data Structures \n");

    TMO_SLL_Init (&(gFwlAclInfo.filterList));
    TMO_SLL_Init (&(gFwlAclInfo.ruleList));
    FwlStatefulInit ();

    FwlUrlFilteringInit ();

    FwlDmzInit ();

    FwlTcpInitFlowListInit ();

    FwlPartialLinksListInit ();

    /* Initialise BlackList and WhiteList RBTree */
    FwlBlackWhiteInitLists ();
#if defined (LNXIP4_WANTED) && !defined (SECURITY_KERNEL_MAKE_WANTED)
     FwlEnableDefIptables();
     TMO_SLL_Init (&gFwlSnorkList);
     TMO_SLL_Init (&gFwlRpfEntryList);
     MEMSET(&gFwlRateLimit, 0 ,(FWL_RATE_LIMIT_MAX_PORT * sizeof(tFwlRateLimit)));  
#endif


    /* Initializing the array of interfaces */

    for (u4IfaceNum = FWL_ZERO; u4IfaceNum < FWL_MAX_NUM_OF_IF; u4IfaceNum++)
    {
        gFwlAclInfo.apIfaceList[u4IfaceNum] = NULL;
    }
    /*Register the Firewall module with SYSLOG  */
    gi4FwlSysLogId = SYS_LOG_REGISTER ((CONST UINT1 *) "FWL",
                                       SYSLOG_CRITICAL_LEVEL);
    /* Initializing the global statistical variables in FwlAclInfo structure */
    gFwlAclInfo.u4MemFailCount = FWL_ZERO;
    gFwlAclInfo.u4TotalPktsPermitCount = FWL_ZERO;
    gFwlAclInfo.u4TotalPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalFragmentPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalIpOptionPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalIcmpPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalPktsInspectCount = FWL_ZERO;
    gFwlAclInfo.u4TotalIPAddressSpoofedPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalSynPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalSrcRouteAttackPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalTinyFragAttackPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalLargeFragAttackPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalAttacksPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.i4TrapThreshold = FWL_DEF_TRAP_THRESHOLD;

    /* Initialise Rate Limiting variables for TCP,UDP and ICMP packets. */
    gFwlAclInfo.u2SynPktsAllowed = FWL_ZERO;
    gFwlAclInfo.u2RtrSynPktsAllowed = FWL_ZERO;
    gFwlAclInfo.u2UdpPktsAllowed = FWL_ZERO;
    gFwlAclInfo.u2IcmpPktsAllowed = FWL_ZERO;
    gFwlAclInfo.u2TcpSynLimit = FWL_DEFAULT_TCP_SYN_LIMIT;
    gFwlAclInfo.u2UdpLimit = FWL_DEFAULT_UDP_RATE_LIMIT;
    gFwlAclInfo.u2IcmpLimit = FWL_DEFAULT_ICMP_RATE_LIMIT;
    gFwlAclInfo.u2TcpInterceptTimeOut = FWL_DEFAULT_INTERCEPT_TIME_OUT;
    gFwlAclInfo.u2UdpInterceptTimeOut = FWL_DEFAULT_INTERCEPT_TIME_OUT;
    gFwlAclInfo.u2IcmpInterceptTimeOut = FWL_DEFAULT_INTERCEPT_TIME_OUT;

    /* Initializing the Firewall enable and Icmp generate switch */
    gu1FirewallStatus = FWL_ENABLE;
    gFwlAclInfo.u1IcmpGenerate = FWL_ICMP_SUPPRESS;
    gFwlAclInfo.u1Icmpv6Generate = FWL_ICMP_SUPPRESS;

    /* This flag will be set, if the packet is denied due to the 
     * Potential attacks or if ICMP packet is denied.
     */

    gFwlAclInfo.u1IpSpoofFilteringEnable = FWL_FILTERING_ENABLED;
    gFwlAclInfo.u1Ipv6SpoofFilteringEnable = FWL_FILTERING_ENABLED;
    gFwlAclInfo.u1SrcRouteFilteringEnable = FWL_FILTERING_ENABLED;
    gFwlAclInfo.u1TinyFragFilteringEnable = FWL_FILTERING_DISABLED;
    gFwlAclInfo.u1NetBiosFilteringEnable = FWL_FILTERING_DISABLED;

    gFwlAclInfo.u1TcpInterceptEnable = FWL_ENABLE;

    /* UDP and ICMP session rate limiting for the router traffic */
    gFwlAclInfo.u1UdpFloodInspect = FWL_ENABLE;
    gFwlAclInfo.u1IcmpFloodInspect = FWL_ENABLE;
    gFwlAclInfo.u1LimitConcurrentSess = FWL_DISABLE;
    gFwlAclInfo.u1NetBiosLan2WanEnable = FWL_DISABLE;

    /* ICMP Error Message Processing */
    gFwlAclInfo.u1InspectIcmpErrMsg = FWL_ENABLE;

    gFwlAclInfo.u1TrapEnable = FWL_DISABLE;
    gFwlAclInfo.u1TrapSentFlag = FWL_TRAP_NOT_SENT;

    /* 
     * Default timeout values for :
     *      - IDLE timer
     *      - TCP establishing/established and closed states
     *      - udp/icmp lifetime 
     */
    gFwlAclInfo.gu2IdleTimerValue = FWL_DEFAULT_IDLE_TIMER_VALUE;
    gFwlAclInfo.gu2TcpInitFlowTimeout = FWL_DEFAULT_TCP_INIT_FLOW_TIMEOUT;
    gFwlAclInfo.gu2TcpEstFlowTimeout = FWL_DEFAULT_TCP_EST_FLOW_TIMEOUT;
    gFwlAclInfo.gu2TcpFinFlowTimeout = FWL_DEFAULT_TCP_FIN_FLOW_TIMEOUT;
    gFwlAclInfo.gu2UdpFlowLargeTimeout = FWL_DEFAULT_UDP_FLOW_LARGE_TIMEOUT;
    gFwlAclInfo.gu2IcmpFlowTimeout = FWL_DEFAULT_ICMP_FLOW_TIMEOUT;
    gFwlAclInfo.i4SmurfAttack = FWL_ONE;
    /*DOS service default value */
    gFwlAclInfo.FwlDOSThresholds.u1OneMinLow = FWL_THRESHOLD_LOW;
    gFwlAclInfo.FwlDOSThresholds.u1OneMinHigh = FWL_THRESHOLD_HIGH;
    gFwlAclInfo.FwlDOSThresholds.u4FwlTcpInitLowerThresh = FWL_THRESHOLD_LOW;
    gFwlAclInfo.FwlDOSThresholds.u4FwlTcpInitUpperThresh = FWL_THRESHOLD_HIGH;
    gFwlAclInfo.FwlDOSThresholds.u4FwlTcpInitMaxPerNode =
        FWL_THRESHOLD_MAX_NODE;

    /* Initializing the TRAP Messages. */
    FWL_STRCPY (gFwlAclInfo.au1TrapMemFailMessage, "MEMORY FAILURE");
    FWL_STRCPY (gFwlAclInfo.au1TrapAttackMessage, "ATTACK EXCEEDED");

    /* Initializing the Memory Status Variable. */
    gFwlAclInfo.i4MemStatus = MEM_SUCCESS;

    /* Handle the exception: File Size limit reached (usually 2GB) */
    FwlSetDefaultIfaceValue (FWL_GLOBAL_IDX, &gIfaceInfo);
    gIfaceInfo.u1IfaceType = FWL_EXTERNAL_IF;
    gFwlAclInfo.apIfaceList[FWL_GLOBAL_IDX] = &gIfaceInfo;

    FwlTmrInitTmr ();
    /* If Firewall is enabled, the TCP Intercept timer  
     * must be started if the coressponding control switch
     * is enabled.
     */
    if (gFwlAclInfo.u1TcpInterceptEnable == FWL_ENABLE)
    {
        /* Start the timer if the TCP Intercept Control Switch is enabled */
        FwlTmrSetTimer (&gFwlAclInfo.TcpInterceptTimer,
                        FWL_TCP_INTERCEPT_TIMER,
                        gFwlAclInfo.u2TcpInterceptTimeOut);

    }
    /* Start timer if UDP Intercept Control Switch is enabled */
    if (gFwlAclInfo.u1UdpFloodInspect == FWL_ENABLE)
    {
        /* Initialise the Udp Packet counters */
        gFwlAclInfo.u2UdpPktsAllowed = FWL_ZERO;

        /* Start the timer if the TCP Intercept Control Switch is enabled */
        FwlTmrSetTimer (&gFwlAclInfo.UdpInterceptTimer,
                        FWL_UDP_INTERCEPT_TIMER,
                        gFwlAclInfo.u2UdpInterceptTimeOut);
    }
    /* Start timer if ICMP Intercept Control Switch is enabled */
    if (gFwlAclInfo.u1IcmpFloodInspect == FWL_ENABLE)
    {
        /* Initialise the Icmp Packet counters */
        gFwlAclInfo.u2IcmpPktsAllowed = FWL_ZERO;
        /* Start the timer if the TCP Intercept Control Switch is enabled */
        FwlTmrSetTimer (&gFwlAclInfo.IcmpInterceptTimer,
                        FWL_ICMP_INTERCEPT_TIMER,
                        gFwlAclInfo.u2IcmpInterceptTimeOut);
    }
    /* Start stateful inspection timer */
    FwlTmrSetTimer (&gFwlStatefulTmr, FWL_IDLE_TIMER, FWL_IDLE_TIMEOUT);
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlAclInit \n");

    /* Register the FSFWL with FSSNMP Agent */
    if (SEC_KERN != gi4SysOperMode)
    {
#ifdef NPAPI_WANTED
        FwlNpEnableDosAttack ();
        FwlNpEnableIpHeaderValidation ();
#endif

#ifdef SNMP_2_WANTED
        RegisterFSFWL ();
#endif

    }
    FWL_INIT_COMPLETE (OSIX_SUCCESS);
    /* Initialisation is successful */

    if (OsixCreateSem ((CONST UINT1 *) "FWLP", FWL_ONE, FWL_ZERO,
                       &gFwlSemId) != OSIX_SUCCESS)
    {
        return (FWL_FAILURE);
    }

    return FWL_SUCCESS;

}                                /* End of the Init funtion */

/* P2R4_1: There was no shut down function */
/****************************************************************************/
/*                                                                          */
/* Function     : FwlAclShutdown                                            */
/*                                                                          */
/* Description  : Shut downs firewall. Releases all memory                  */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlAclShutdown (VOID)
#else
PUBLIC VOID
FwlAclShutdown ()
#endif
{
    UINT2               u2Index = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;
    tAclInfo           *pAclNode = NULL;
    tRuleInfo          *pRuleNode = NULL;
    tFilterInfo        *pFilterNode = NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the FwlAclShutdown \n");

    /* Disable Firewall */
    nmhSetFwlGlobalMasterControlSwitch (FWL_DISABLE);
    for (u2Index = FWL_ZERO; u2Index < FWL_MAX_NUM_OF_IF; u2Index++)
    {
        pIfaceNode = gFwlAclInfo.apIfaceList[u2Index];

        if (pIfaceNode != NULL)
        {
            /* Delete All the Infilters */
            while (TMO_SLL_Count (&pIfaceNode->inFilterList) != FWL_ZERO)
            {
                pAclNode = (tAclInfo *) TMO_SLL_Get (&pIfaceNode->inFilterList);
                FwlDbaseDeleteAcl (pIfaceNode->u4IfaceNum,
                                   FWL_DIRECTION_IN, pAclNode->au1FilterName);
            }

            /* Delete All the Outfilters */
            while (TMO_SLL_Count (&pIfaceNode->outFilterList) != FWL_ZERO)
            {
                pAclNode =
                    (tAclInfo *) TMO_SLL_Get (&pIfaceNode->outFilterList);
                FwlDbaseDeleteAcl (pIfaceNode->u4IfaceNum, FWL_DIRECTION_OUT,
                                   pAclNode->au1FilterName);
            }

            /* Delete All the InIPv6filters */
            while (TMO_SLL_Count (&pIfaceNode->inIPv6FilterList) != FWL_ZERO)
            {
                pAclNode =
                    (tAclInfo *) TMO_SLL_Get (&pIfaceNode->inIPv6FilterList);
                FwlDbaseDeleteAcl (pIfaceNode->u4IfaceNum,
                                   FWL_DIRECTION_IN, pAclNode->au1FilterName);
            }
            /* Delete All the OUtIPv6Filters */
            while (TMO_SLL_Count (&pIfaceNode->outIPv6FilterList) != FWL_ZERO)
            {
                pAclNode =
                    (tAclInfo *) TMO_SLL_Get (&pIfaceNode->outIPv6FilterList);
                FwlDbaseDeleteAcl (pIfaceNode->u4IfaceNum, FWL_DIRECTION_OUT,
                                   pAclNode->au1FilterName);
            }

            /* Delete the interface */
            FwlDbaseDeleteIface (pIfaceNode->u4IfaceNum);
        }
    }

    /* delete the rules */
    while (TMO_SLL_Count (&gFwlAclInfo.ruleList) != FWL_ZERO)
    {
        pRuleNode = (tRuleInfo *) TMO_SLL_Get (&gFwlAclInfo.ruleList);
        FwlDbaseDeleteRule (pRuleNode->au1RuleName);
    }

    /* delete the filters */
    while (TMO_SLL_Count (&gFwlAclInfo.filterList) != FWL_ZERO)
    {
        pFilterNode = (tFilterInfo *) TMO_SLL_Get (&gFwlAclInfo.filterList);
        FwlDbaseDeleteRule (pFilterNode->au1FilterName);
    }
    FwlTmrDeInitTmr ();
    FirewallSizingMemDeleteMemPools ();
    /* Remove BlackList and WhiteList RBTree */
    FwlBlackWhiteDeInitLists ();
    gFwlAclInfo.i4MemStatus = MEM_SUCCESS;

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the FwlAclShutdown \n");

}                                /* End of the function -- FwlAclShutdown  */

#ifdef FUTURE_SNMP_WANTED
INT4
RegisterFSFWLwithFutureSNMP (void)
{
    if (SNMP_AGT_RegisterMib ((tSNMP_GroupOIDType *) & fsfwl_FMAS_GroupOIDTable,
                              (tSNMP_BaseOIDType *) & fsfwl_FMAS_BaseOIDTable,
                              (tSNMP_MIBObjectDescrType *) &
                              fsfwl_FMAS_MIBObjectTable,
                              fsfwl_FMAS_Global_data,
                              (INT4) fsfwl_MAX_OBJECTS) != SNMP_SUCCESS)
    {
        return FAILURE;
    }
    return SUCCESS;
}
#endif /* FUTURE_SNMP_WANTED */
/****************************************************************************/
/*                                                                          */
/* Function     : FwlAddDefaultRules                                        */
/*                                                                          */
/* Description  : Initialise the default firewall filters/access-lists      */
/*                rules and then enable FireWall Globally                   */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/
VOID
FwlAddDefaultRules (VOID)
{
    INT1                ai1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    INT1                ai1AclName[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
    UINT4               u4Priority = FWL_DEF_RULE_PRIORITY_MIN;

    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_FTP_Filter", (sizeof (ai1FilterName) -
                                               FWL_ONE));
    STRNCPY (ai1AclName, "Def_FTP_ACL", (sizeof (ai1AclName) - FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_FTP_CTRL_PORT, u4Priority++,
                             FWL_TCP, FWL_IP_VERSION_4)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Default Rule not initialised for FTP \n");
    }

    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_TELNET_Filter", (sizeof (ai1FilterName) -
                                                  FWL_ONE));
    STRNCPY (ai1AclName, "Def_TELNET_ACL", (sizeof (ai1AclName) - FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_TELNET_PORT, u4Priority++,
                             FWL_TCP, FWL_IP_VERSION_4)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Default Rule  not initialised for Telnet \n");
    }

    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_SMTP_Filter", (sizeof (ai1FilterName) -
                                                FWL_ONE));
    STRNCPY (ai1AclName, "Def_SMTP_ACL", (sizeof (ai1AclName) - FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_SMTP_PORT, u4Priority++,
                             FWL_TCP, FWL_IP_VERSION_4)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Default Rule  not initialised for SMTP \n");
    }

    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_DNS_TCP_Filter", (sizeof (ai1FilterName) -
                                                   FWL_ONE));
    STRNCPY (ai1AclName, "Def_DNS_TCP_ACL", (sizeof (ai1AclName) - FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_DNS_PORT, u4Priority++,
                             FWL_TCP, FWL_IP_VERSION_4)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Default rule not initialised for DNS TCP \n");
    }

    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_DNS_UDP_Filter", (sizeof (ai1FilterName) -
                                                   FWL_ONE));
    STRNCPY (ai1AclName, "Def_DNS_UDP_ACL", (sizeof (ai1AclName) - FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_DNS_PORT, u4Priority++,
                             FWL_UDP, FWL_IP_VERSION_4)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Default rule not initialised for DNS UDP\n");
    }

    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_HTTP_Filter", (sizeof (ai1FilterName) -
                                                FWL_ONE));
    STRNCPY (ai1AclName, "Def_HTTP_ACL", (sizeof (ai1AclName) - FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_HTTP_PORT, u4Priority++,
                             FWL_TCP, FWL_IP_VERSION_4)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Default rule not initialised for HTTP \n");
    }

    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_HTTPS_Filter", (sizeof (ai1FilterName) -
                                                 FWL_ONE));
    STRNCPY (ai1AclName, "Def_HTTPS_ACL", (sizeof (ai1AclName) - FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_HTTPS_PORT, u4Priority++,
                             FWL_TCP, FWL_IP_VERSION_4)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Default rule not initialised for HTTPS \n");
    }

    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_POP3_Filter", (sizeof (ai1FilterName) -
                                                FWL_ONE));
    STRNCPY (ai1AclName, "Def_POP3_ACL", (sizeof (ai1AclName) - FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_POP3_PORT, u4Priority++,
                             FWL_TCP, FWL_IP_VERSION_4)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n  Default rule not initialised for POP3 \n");
    }

    /* there is no NAT ALG support */
    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_IMAP_Filter", (sizeof (ai1FilterName) -
                                                FWL_ONE));
    STRNCPY (ai1AclName, "Def_IMAP_ACL", (sizeof (ai1AclName) - FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_IMAP_PORT, u4Priority++,
                             FWL_TCP, FWL_IP_VERSION_4)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Default rule not initialised for IMAP \n");
    }

    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_SNTP_UDP_Filter",
             (sizeof (ai1FilterName) - FWL_ONE));
    STRNCPY (ai1AclName, "Def_SNTP_ACL", (sizeof (ai1AclName) - FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_SNTP_PORT, u4Priority++,
                             FWL_UDP, FWL_IP_VERSION_4)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Default rule not initialised for SNTP UDP \n");
    }
    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_FTP_IPv6_Filter",
             (sizeof (ai1FilterName) - FWL_ONE));
    STRNCPY (ai1AclName, "Def_FTP_IPv6_ACL", (sizeof (ai1AclName) - FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_FTP_CTRL_PORT, u4Priority++,
                             FWL_TCP, FWL_IP_VERSION_6)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Default Rule not initialised for FTP \n");
    }

    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_TELNET_IPv6_Filter",
             (sizeof (ai1FilterName) - FWL_ONE));
    STRNCPY (ai1AclName, "Def_TELNET_IPv6_ACL", (sizeof (ai1AclName) -
                                                 FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_TELNET_PORT, u4Priority++,
                             FWL_TCP, FWL_IP_VERSION_6)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Default Rule  not initialised for Telnet \n");
    }

    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_SMTP_IPv6_Filter",
             (sizeof (ai1FilterName) - FWL_ONE));
    STRNCPY (ai1AclName, "Def_SMTP_IPv6_ACL", (sizeof (ai1AclName) - FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_SMTP_PORT, u4Priority++,
                             FWL_TCP, FWL_IP_VERSION_6)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Default Rule  not initialised for SMTP \n");
    }

    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_DNS_TCP_IPv6_Filter",
             (sizeof (ai1FilterName) - FWL_ONE));
    STRNCPY (ai1AclName, "Def_DNS_TCP_IPv6_ACL", (sizeof (ai1AclName) -
                                                  FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_DNS_PORT, u4Priority++,
                             FWL_TCP, FWL_IP_VERSION_6)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Default rule not initialised for DNS TCP \n");
    }

    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_DNS_UDP_IPv6_Filter",
             (sizeof (ai1FilterName) - FWL_ONE));
    STRNCPY (ai1AclName, "Def_DNS_UDP_IPv6_ACL", (sizeof (ai1AclName) -
                                                  FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_DNS_PORT, u4Priority++,
                             FWL_UDP, FWL_IP_VERSION_6)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Default rule not initialised for DNS UDP\n");
    }

    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_HTTP_IPv6_Filter",
             (sizeof (ai1FilterName) - FWL_ONE));
    STRNCPY (ai1AclName, "Def_HTTP_IPv6_ACL", (sizeof (ai1AclName) - FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_HTTP_PORT, u4Priority++,
                             FWL_TCP, FWL_IP_VERSION_6)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Default rule not initialised for HTTP \n");
    }

    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_HTTPS_IPv6_Filter",
             (sizeof (ai1FilterName) - FWL_ONE));
    STRNCPY (ai1AclName, "Def_HTTPS_IPv6_ACL", (sizeof (ai1AclName) - FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_HTTPS_PORT, u4Priority++,
                             FWL_TCP, FWL_IP_VERSION_6)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Default rule not initialised for HTTPS \n");
    }

    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_POP3_IPv6_Filter",
             (sizeof (ai1FilterName) - FWL_ONE));
    STRNCPY (ai1AclName, "Def_POP3_IPv6_ACL", (sizeof (ai1AclName) - FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_POP3_PORT, u4Priority++,
                             FWL_TCP, FWL_IP_VERSION_6)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n  Default rule not initialised for POP3 \n");
    }

    /* there is no NAT ALG support */
    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_IMAP_IPv6_Filter",
             (sizeof (ai1FilterName) - FWL_ONE));
    STRNCPY (ai1AclName, "Def_IMAP_IPv6_ACL", (sizeof (ai1AclName) - FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_IMAP_PORT, u4Priority++,
                             FWL_TCP, FWL_IP_VERSION_6)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Default rule not initialised for IMAP \n");
    }

    MEMSET (ai1FilterName, FWL_ZERO, sizeof (ai1FilterName));
    MEMSET (ai1AclName, FWL_ZERO, sizeof (ai1AclName));
    STRNCPY (ai1FilterName, "Def_SNTP_UDP_IPv6_Filter",
             (sizeof (ai1FilterName) - FWL_ONE));
    STRNCPY (ai1AclName, "Def_SNTP_IPv6_ACL", (sizeof (ai1AclName) - FWL_ONE));
    if ((FwlInitDefaultRule (ai1FilterName, ai1AclName,
                             FWL_SNTP_PORT, u4Priority++,
                             FWL_UDP, FWL_IP_VERSION_6)) != FWL_SUCCESS)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Default rule not initialised for SNTP UDP \n");
    }

}

/****************************************************************************/
/*                                                                          */
/* Function     : FwlInitDefaultRule                                        */
/*                                                                          */
/* Description  : Initialise the default firewall filters/acls/rules        */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/
INT1
FwlInitDefaultRule (INT1 *pFilterName, INT1 *pAclName,
                    UINT4 u4Port, UINT4 u4Priority, UINT1 u1Proto,
                    UINT1 u1AddrType)
{
    tFilterInfo        *pFwlFilterEntry = NULL;
    tIfaceInfo         *pIfaceNode = NULL;
    tRuleInfo          *pRuleNode = NULL;
    tAclInfo           *pAclNode = NULL;
    UINT4               u4MemStatus = FWL_FAILURE;
    UINT1               u1Index = FWL_ZERO;

    /* Allocate memory for FilterEntry, RuleEntry and ACLEntry */
    if ((u4MemStatus =
         FwlFilterMemAllocate ((UINT1 **) (VOID *) &pFwlFilterEntry)) ==
        FWL_SUCCESS)
    {
        if ((u4MemStatus = FwlRuleMemAllocate ((UINT1 **) (VOID *) &pRuleNode))
            == FWL_SUCCESS)
        {
            u4MemStatus = FwlAclMemAllocate ((UINT1 **) (VOID *) &pAclNode);
        }
    }

    if ((u4MemStatus == FWL_FAILURE) ||
        (pRuleNode == NULL) || (pAclNode == NULL))
    {
        /*  Send a Trap for Memory faliure */
        FwlGenerateMemFailureTrap (FWL_STATIC_FILTER_ALLOC_FAILURE);
        INC_MEM_FAILURE_COUNT;
        gFwlAclInfo.i4MemStatus = (INT4) MEM_FAILURE;
        if (pAclNode != NULL)
        {
            FwlAclMemFree ((UINT1 *) pAclNode);
        }
        FwlFilterMemFree ((UINT1 *) pFwlFilterEntry);
        if (pRuleNode != NULL)
        {
            FwlRuleMemFree ((UINT1 *) pRuleNode);
        }
        return FWL_FAILURE;
    }

    MEMSET (pFwlFilterEntry, FWL_ZERO, sizeof (tFilterInfo));
    MEMSET (pRuleNode, FWL_ZERO, sizeof (tRuleInfo));
    MEMSET (pAclNode, FWL_ZERO, sizeof (tAclInfo));

    /* Since the configurations are global (not interface specific),
     * the the interface node, which will be representing all the 
     * interfaces */
    pIfaceNode = gFwlAclInfo.apIfaceList[FWL_INDEX_0];

    /*                        *   
     * Adding default filter  *
     *                        */
    TMO_SLL_Init_Node (&pFwlFilterEntry->nextFilterInfo);
    STRCPY (pFwlFilterEntry->au1FilterName, pFilterName);
    if (u1AddrType == FWL_IP_VERSION_4)
    {
        if (SEC_KERN != gi4SysOperMode)
        {
            /* Source IP can be VLAN1 IP; Destination IP can be ANY */
            pFwlFilterEntry->SrcStartAddr.v4Addr =
                ((IssGetIpAddrFromNvRam () & IssGetSubnetMaskFromNvRam ()) +
                 FWL_ONE);
            pFwlFilterEntry->SrcEndAddr.v4Addr = (IssGetIpAddrFromNvRam () &
                                                  IssGetSubnetMaskFromNvRam ())
                | (~IssGetSubnetMaskFromNvRam ());
            FwlConcatAddrAndMask (IssGetIpAddrFromNvRam (),
                                  IssGetSubnetMaskFromNvRam (),
                                  pFwlFilterEntry->au1SrcAddr);
        }
        else
        {
            pFwlFilterEntry->SrcStartAddr.v4Addr = FWL_ZERO;
            pFwlFilterEntry->SrcEndAddr.v4Addr = FWL_ZERO;
            SPRINTF ((CHR1 *) pFwlFilterEntry->au1SrcAddr, "0.0.0.0/0");
        }
        pFwlFilterEntry->SrcStartAddr.u4AddrType = FWL_IP_VERSION_4;
        pFwlFilterEntry->SrcEndAddr.u4AddrType = FWL_IP_VERSION_4;

        pFwlFilterEntry->DestStartAddr.v4Addr = FWL_ZERO;
        pFwlFilterEntry->DestStartAddr.u4AddrType = FWL_IP_VERSION_4;
        pFwlFilterEntry->DestEndAddr.v4Addr = FWL_ZERO;
        pFwlFilterEntry->DestEndAddr.u4AddrType = FWL_IP_VERSION_4;
        STRCPY (pFwlFilterEntry->au1DestAddr, "0.0.0.0/0");
        pFwlFilterEntry->u2AddrType = FWL_IP_VERSION_4;
    }
    else
    {
        MEMSET (&pFwlFilterEntry->SrcStartAddr.v6Addr, FWL_ZERO,
                sizeof (tIp6Addr));
        pFwlFilterEntry->SrcStartAddr.u4AddrType = FWL_IP_VERSION_6;
        MEMSET (&pFwlFilterEntry->SrcEndAddr.v6Addr, FWL_ZERO,
                sizeof (tIp6Addr));
        pFwlFilterEntry->SrcEndAddr.u4AddrType = FWL_IP_VERSION_6;
        MEMSET (&pFwlFilterEntry->DestStartAddr.v6Addr, FWL_ZERO,
                sizeof (tIp6Addr));
        pFwlFilterEntry->DestStartAddr.u4AddrType = FWL_IP_VERSION_6;
        MEMSET (&pFwlFilterEntry->DestEndAddr.v6Addr, FWL_ZERO,
                sizeof (tIp6Addr));
        pFwlFilterEntry->DestEndAddr.u4AddrType = FWL_IP_VERSION_6;

        STRCPY (pFwlFilterEntry->au1SrcAddr, "::/0");
        STRCPY (pFwlFilterEntry->au1DestAddr, "::/0");
        pFwlFilterEntry->u2AddrType = FWL_IP_VERSION_6;
    }

    pFwlFilterEntry->u1Proto = u1Proto;
    /* Source port can be ANY (source port > 1) */
    pFwlFilterEntry->u2SrcMinPort = FWL_MIN_PORT_VALUE;
    pFwlFilterEntry->u2SrcMaxPort = FWL_MAX_PORT_VALUE;

    STRCPY (pFwlFilterEntry->au1SrcPort, ">1");

    if (u4Port != FWL_DEFAULT_PORT)
    {
        /* Destination port is well-known port = u4Port */
        pFwlFilterEntry->u2DestMinPort = (UINT2) (u4Port - FWL_ONE);
        pFwlFilterEntry->u2DestMaxPort = (UINT2) (u4Port + FWL_ONE);
        SPRINTF ((CHR1 *) pFwlFilterEntry->au1DestPort, "=%d", u4Port);
    }
    else
    {
        /* For destination port >1 */
        pFwlFilterEntry->u2DestMinPort = FWL_MIN_PORT_VALUE;
        pFwlFilterEntry->u2DestMaxPort = FWL_MAX_PORT_VALUE;
        STRCPY (pFwlFilterEntry->au1DestPort, ">1");
    }
    /* DHCP Reply <IN> direction */

    if (u4Port == FWL_DHCP_CLIENT_PORT)
    {
        STRCPY (pFwlFilterEntry->au1SrcPort, "=67");
        STRCPY (pFwlFilterEntry->au1DestPort, "=68");
        pFwlFilterEntry->u2SrcMinPort = FWL_DHCP_SERVER_PORT - FWL_ONE;
        pFwlFilterEntry->u2SrcMaxPort = FWL_DHCP_SERVER_PORT + FWL_ONE;
    }

    /* DHCP Discover <OUT> direction */

    if (u4Port == FWL_DHCP_SERVER_PORT)
    {
        STRCPY (pFwlFilterEntry->au1SrcPort, "=68");
        STRCPY (pFwlFilterEntry->au1DestPort, "=67");
        pFwlFilterEntry->u2SrcMinPort = FWL_DHCP_CLIENT_PORT - FWL_ONE;
        pFwlFilterEntry->u2SrcMaxPort = FWL_DHCP_CLIENT_PORT + FWL_ONE;
    }

    pFwlFilterEntry->u1TcpAck = FWL_TCP_ACK_ANY;
    pFwlFilterEntry->u1TcpRst = FWL_TCP_RST_ANY;
    pFwlFilterEntry->u1Tos = FWL_TOS_ANY;
    pFwlFilterEntry->u4FilterHitCount = FWL_DEFAULT_COUNT;
    pFwlFilterEntry->u1FilterAccounting = FWL_DISABLE;

    INC_FILTER_REF_COUNT (pFwlFilterEntry);
    pFwlFilterEntry->u1RowStatus = FWL_ACTIVE;

    /* ADD the Filter entry to the Database */
    FwlDbaseAddFilter (pFwlFilterEntry);

    /* RULE is needed, as code assumes, rule with same name as
     * ACL exists (eg: show firewall acl ) */

    /*                     *   
     * Adding default rule *
     *                     */
    TMO_SLL_Init_Node (&pRuleNode->nextRuleInfo);
    STRCPY (pRuleNode->au1RuleName, pAclName);
    STRCPY (pRuleNode->au1FilterSet, pFilterName);
    /* Associate the rule with the filter */
    pRuleNode->apFilterInfo[FWL_INDEX_0] = pFwlFilterEntry;
    for (u1Index = FWL_ONE; u1Index < FWL_MAX_FILTERS_IN_RULE; u1Index++)
    {
        pRuleNode->apFilterInfo[u1Index] = (tFilterInfo *) NULL;
    }
    pRuleNode->u1FilterConditionFlag = FWL_FILTER_OR;
    pRuleNode->u1RuleRefCount = FWL_ZERO;
    pRuleNode->u1RowStatus = FWL_ACTIVE;
    /* ADD the rule entry to the Database */
    FwlDbaseAddRule (pRuleNode);

    /*                    *   
     * Adding default ACL *
     *                    */
    STRCPY (pAclNode->au1FilterName, pAclName);
    pAclNode->pAclName = pRuleNode;
    TMO_SLL_Init_Node (&pAclNode->nextFilterInfo);
    pAclNode->u1AclType = ACL_RULE;
    INC_RULE_REF_COUNT (pRuleNode);
    pAclNode->u1Action = FWL_PERMIT;
    pAclNode->u2SeqNum = (UINT2) u4Priority;    /*Priority */
    pAclNode->u1LogTrigger = FWL_LOG_BRF;
    pAclNode->u1FragAction = FWL_PERMIT;    /* Fragment permit */
    pAclNode->u4StartTime = FWL_ZERO;
    pAclNode->u4EndTime = FWL_END_TIME;
    pAclNode->u4Scheduled = FWL_DISABLE;
    pAclNode->u1RowStatus = FWL_ACTIVE;

    /* ADD the ACL entry to the 'GLOBAL' interface node */
    if (u4Port == FWL_DHCP_CLIENT_PORT)
    {
        TMO_SLL_Add (&pIfaceNode->inFilterList, (tTMO_SLL_NODE *) pAclNode);
    }
    else
        TMO_SLL_Add (&pIfaceNode->outFilterList, (tTMO_SLL_NODE *) pAclNode);

    return FWL_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/* Function     : FwlClearGlobalStats                                       */
/*                                                                          */
/* Description  : Clears the Global Statistics related Information          */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/
VOID
FwlClearGlobalStats (VOID)
{
    gFwlAclInfo.u4TotalPktsPermitCount = FWL_ZERO;
    gFwlAclInfo.u4TotalPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalFragmentPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalIpOptionPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalIcmpPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalPktsInspectCount = FWL_ZERO;
    gFwlAclInfo.u4TotalIPAddressSpoofedPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalSynPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalSrcRouteAttackPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalTinyFragAttackPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalLargeFragAttackPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalAttacksPktsDenyCount = FWL_ZERO;
}

/****************************************************************************/
/*                                                                          */
/* Function     : FwlClearIPv6GlobalStats                                   */
/*                                                                          */
/* Description  : Clears the Global IPv6 Statistics related Information     */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/
VOID
FwlClearIPv6GlobalStats (VOID)
{
    gFwlAclInfo.u4TotalIPv6PktsInspectCount = FWL_ZERO;
    gFwlAclInfo.u4TotalIPv6PktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalIPv6PktsPermitCount = FWL_ZERO;
    gFwlAclInfo.u4TotalIPv6IcmpPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalIPv6AddressSpoofedPktsDenyCount = FWL_ZERO;
    gFwlAclInfo.u4TotalIPv6AttackPktsDenyCount = FWL_ZERO;
}

/****************************************************************************/
/*                                                                          */
/* Function     : FwlClearInterfaceStats                                    */
/*                                                                          */
/* Description  : Clears the Interface Statistics related Information       */
/*                                                                          */
/* Input        : u4IfIndex - Interface Index for which the statistics has  */
/*                to be cleared                                             */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/
VOID
FwlClearInterfaceStats (UINT4 u4IfIndex)
{
    tIfaceInfo         *pIfaceNode = NULL;

    /* search the interface list and get the Packets deny count */
    pIfaceNode = FwlDbaseSearchIface (u4IfIndex);
    if (NULL != pIfaceNode)
    {
        pIfaceNode->ifaceStat.u4PktsPermitCount = FWL_DEFAULT_COUNT;
        pIfaceNode->ifaceStat.u4PktsDenyCount = FWL_DEFAULT_COUNT;
        pIfaceNode->ifaceStat.u4FragmentPktsDenyCount = FWL_DEFAULT_COUNT;
        pIfaceNode->ifaceStat.u4IpOptionPktsDenyCount = FWL_DEFAULT_COUNT;
        pIfaceNode->ifaceStat.u4IcmpPktsDenyCount = FWL_DEFAULT_COUNT;
        pIfaceNode->ifaceStat.u4SrcRouteAttackPktsDenyCount = FWL_DEFAULT_COUNT;
        pIfaceNode->ifaceStat.u4TinyFragAttackPktsDenyCount = FWL_DEFAULT_COUNT;
        pIfaceNode->ifaceStat.u4IPAddrSpoofedPktsDenyCount = FWL_DEFAULT_COUNT;
        pIfaceNode->ifaceStat.u4SynPktsDenyCount = FWL_DEFAULT_COUNT;
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Failed while clearing the statistics for Interface %ld\n",
                      u4IfIndex);
    }
}

/****************************************************************************/
/*                                                                          */
/* Function     : FwlClearInterfaceIPv6Stats                                */
/*                                                                          */
/* Description  : Clears the Interface Statistics related Information       */
/*                                                                          */
/* Input        : u4IfIndex - Interface Index for which the statistics has  */
/*                to be cleared                                             */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/
VOID
FwlClearInterfaceIPv6Stats (UINT4 u4IfIndex)
{
    tIfaceInfo         *pIfaceNode = NULL;

    /* search the interface list and get the Packets deny count */
    pIfaceNode = FwlDbaseSearchIface (u4IfIndex);
    if (NULL != pIfaceNode)
    {
        pIfaceNode->ifaceStat.u4IPv6PktsPermitCount = FWL_DEFAULT_COUNT;
        pIfaceNode->ifaceStat.u4IPv6PktsDenyCount = FWL_DEFAULT_COUNT;
        pIfaceNode->ifaceStat.u4Icmpv6PktsDenyCount = FWL_DEFAULT_COUNT;
        pIfaceNode->ifaceStat.u4IPv6AddrSpoofedPktsDenyCount =
            FWL_DEFAULT_COUNT;
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Failed while clearing the IPv6 statistics for Interface %ld\n",
                      u4IfIndex);
    }
}

/****************************************************************************/
/*                                                                          */
/* Function     : FwlLock                                                   */
/*                                                                          */
/* Description  : Takes the Firewall Lock                                   */
/*                                                                          */
/* Input        : *pFwlLock - Pointer to the spin_lock_t in case of kernel, */
/*                            UINT4 in case of User Space                   */
/*                                                                          */
/*                pu4Flags  - Pointer to the Flags for the lock             */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/
INT4
FwlLock (VOID)
{
    if (OsixSemTake (gFwlSemId) != OSIX_SUCCESS)
    {
        return (FWL_FAILURE);
    }
    return (FWL_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/* Function     : FwlUnLock                                                 */
/*                                                                          */
/* Description  : Releases the Firewall Lock                                */
/*                                                                          */
/* Input        : *pFwlLock - Pointer to the spin_lock_t in case of kernel, */
/*                            UINT4 in case of User Space                   */
/*                                                                          */
/*                pu4Flags  - Pointer to the Flags for the lock             */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/

INT4
FwlUnLock (VOID)
{
    if (OsixSemGive (gFwlSemId) != OSIX_SUCCESS)
    {
        return (FWL_FAILURE);
    }
    return (FWL_SUCCESS);
}

/****************************************************************************
 Function    : FwlEnableDefIptables

 Description : Default configuration of the iptables

 Input       : u1Status - Status to be set

 OutPut      : None

 Returns     : IP_SUCCESS/IP_FAILURE
****************************************************************************/

#if defined (LNXIP4_WANTED) && !defined (SECURITY_KERNEL_MAKE_WANTED)
VOID
FwlEnableDefIptables (VOID)
{
    CHR1                ac1Command[FWL_DOS_LINE_LEN];

   /*filtering of malicious ICMP packets from management port based on length to protect it
     from Ping of Death (PoD) attack & Jolt attack by dropping very large fragmented &
     Jolt2 attack by dropping continuous streams of identical fragmented IP packets*/

    MEMSET (ac1Command, 0, sizeof (ac1Command));
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
             "iptables -A FORWARD -p icmp -f -j DROP");
    system (ac1Command);
   /*protection of management port against XMAS scan attack by dropping TCP packet where
 *      most of the options are set in TCP header. */

    MEMSET (ac1Command, 0, sizeof (ac1Command));
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
         "iptables -A INPUT -p tcp --tcp-flags ALL ALL -j DROP");
    system (ac1Command);
   /*protection of management port against TCP NULL scan attack by dropping TCP
 *  packets/segments with no flags in the TCP header.*/
    MEMSET (ac1Command, 0, sizeof (ac1Command));
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
        "iptables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP");
    system (ac1Command);
/*protection of management port against Win Nuke attack by dropping TCP packet with URG flag set.*/

    MEMSET (ac1Command, 0, sizeof (ac1Command));
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
            "iptables -A INPUT -p tcp --tcp-flags URG URG -j DROP");
    system (ac1Command);

}


#endif
#endif
/*****************************************************************************/
/*       End of the file - fwlinit.c                                         */
/*****************************************************************************/
