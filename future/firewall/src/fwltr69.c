/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwltr69.c,v 1.3 2013/12/14 11:29:37 siva Exp $
 *
 * Description: File containing TR69 routines
 * *******************************************************************/

#define _FWLTR69_C_
#include "fwlinc.h"
#include "tr.h"
#include "tr69dim.h"
#include "fwltr69.h"

/*****************************************************************************/
/*    Function Name             : FwlTr69InitTrList                          */
/*    Description               : Initialises Firewall Tr069 List            */
/*    Input(s)                  : None.                                      */
/*    Output(s)                 : None.                                      */
/*    Returns                   : None                                       */
/*****************************************************************************/
VOID
FwlTr69InitTrList (VOID)
{
    UTL_SLL_Init (&FwlDefnFilterTableList, 0);
    UTL_SLL_Init (&FwlDefnRuleTableList, 0);
    UTL_SLL_Init (&FwlDefnAclTableList, 0);
    UTL_SLL_Init (&FwlDefnIfTableList, 0);
    UTL_SLL_Init (&FwlDefnDmzTableList, 0);
    UTL_SLL_Init (&FwlUrlFilterTableList, 0);
    UTL_SLL_Init (&FwlStatIfTableList, 0);

    return;
}

/************************************************************************
 *  Function Name   : FwlTr69ScanAndUpdateTrList
 *  Description     : Scans and Updates Firewall TR069 Database
 *
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
FwlTr69ScanAndUpdateTrList ()
{
    TrScan_FwlDefnFilterTable ();
    TrScan_FwlDefnRuleTable ();
    TrScan_FwlDefnAclTable ();
    TrScan_FwlDefnIfTable ();
    TrScan_FwlDefnDmzTable ();
    TrScan_FwlUrlFilterTable ();
    TrScan_FwlStatIfTable ();

    return;
}

/************************************************************************
*                      fwlGlobal group begins                          *
************************************************************************/

/************************************************************************
 *  Function Name   : InternetGatewayDevice_Firewall
 *  Description     : Get function for InternetGatewayDevice_Firewall
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrGetInternetGatewayDevice_Firewall (char *name, ParameterValue * value)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT4               u4val = 0;
    INT4                i4rc = 0;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrGetInternetGatewayDevice_Firewall\n");

    if (STRCMP (name, "Trap") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlGlobalTrap, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "Exit: Get failed for FwlGlobalTrap\n");
            value->out_uint = 0;
            return OK;

        }
        if (u4val == TR_FWL_GLOBAL_STATUS_DISABLE)
        {
            value->out_uint = TR_FWL_FALSE;
        }
        else
        {
            value->out_uint = TR_FWL_TRUE;
        }
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlGlobalTrap ret %d\n", value->out_uint);
        return OK;
    }
    else if (STRCMP (name, "IpSpoofFiltering") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlGlobalIpSpoofFiltering, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlGlobalIpSpoofFiltering\n");
            value->out_uint = 0;
            return OK;
        }
        if (u4val == TR_FWL_GLOBAL_STATUS_DISABLE)
        {
            value->out_uint = TR_FWL_FALSE;
        }
        else
        {
            value->out_uint = TR_FWL_TRUE;
        }
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlGlobalIpSpoofFiltering ret %d\n",
                       value->out_uint);
        return OK;
    }
    else if (STRCMP (name, "ICMPType") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlIfICMPType, 12, FwlDefnIfTableINDEX, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "Exit: Get failed for FwlIfICMPType\n");
            value->out_int = 0;
            return OK;
        }
        value->out_int = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FwlIfICMPType ret %d\n", u4val);
        return OK;
    }
    else if (STRCMP (name, "MasterControlSwitch") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlGlobalMasterControlSwitch, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlGlobalMasterControlSwitch\n");
            value->out_uint = 0;
            return OK;
        }
        if (u4val == TR_FWL_GLOBAL_STATUS_DISABLE)
        {
            value->out_uint = TR_FWL_FALSE;
        }
        else
        {
            value->out_uint = TR_FWL_TRUE;
        }
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlGlobalMasterControlSwitch ret %d\n",
                       value->out_uint);
        return OK;
    }
    else if (STRCMP (name, "TcpInterceptTimeout") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlDefnInterceptTimeout, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_TIME_TICKS, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlDefnInterceptTimeout\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_uint = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlDefnInterceptTimeout ret %d\n", u4val);
        return OK;
    }
    else if (STRCMP (name, "TcpIntercept") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlGlobalTcpIntercept, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlGlobalTcpIntercept\n");
            value->out_uint = 0;
            return OK;
        }
        if (u4val == TR_FWL_GLOBAL_STATUS_DISABLE)
        {
            value->out_uint = TR_FWL_FALSE;
        }
        else
        {
            value->out_uint = TR_FWL_TRUE;
        }
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlGlobalTcpIntercept ret %d\n", value->out_int);
        return OK;
    }
    else if (STRCMP (name, "Fragments") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlIfFragments, 12, FwlDefnIfTableINDEX, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "Exit: Get failed for FwlIfFragments\n");
            value->out_int = 0;
            return OK;
        }
        value->out_int = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FwlIfFragments ret %d\n", u4val);
        return OK;
    }
    else if (STRCMP (name, "MaxFilters") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlGlobalMaxFilters, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_INTEGER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlGlobalMaxFilters\n");
            value->out_int = 0;
            return OK;
        }
        value->out_int = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FwlGlobalMaxFilters ret %d\n",
                       u4val);
        return OK;
    }
    else if (STRCMP (name, "NetBiosFiltering") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlGlobalNetBiosFiltering, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlGlobalNetBiosFiltering\n");
            value->out_uint = 0;
            return OK;
        }
        if (u4val == TR_FWL_GLOBAL_STATUS_DISABLE)
        {
            value->out_uint = TR_FWL_FALSE;
        }
        else
        {
            value->out_uint = TR_FWL_TRUE;
        }
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlGlobalNetBiosFiltering ret %d\n",
                       value->out_uint);
        return OK;
    }
    else if (STRCMP (name, "UrlFiltering") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlGlobalUrlFiltering, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlGlobalUrlFiltering\n");
            value->out_uint = 0;
            return OK;
        }
        if (u4val == TR_FWL_GLOBAL_STATUS_DISABLE)
        {
            value->out_uint = TR_FWL_FALSE;
        }
        else
        {
            value->out_uint = TR_FWL_TRUE;
        }
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlGlobalUrlFiltering ret %d\n", value->out_uint);
        return OK;
    }
    else if (STRCMP (name, "TcpInterceptThreshold") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlDefnTcpInterceptThreshold, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_INTEGER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlDefnTcpInterceptThreshold\n");
            value->out_int = 0;
            return OK;
        }
        value->out_int = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlDefnTcpInterceptThreshold ret %d\n", u4val);
        return OK;
    }
    else if (STRCMP (name, "IpOptions") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlIfIpOptions, 12, FwlDefnIfTableINDEX, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "Exit: Get failed for FwlIfIpOptions\n");
            value->out_int = 0;
            return OK;
        }
        value->out_int = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FwlIfIpOptions ret %d\n", u4val);
        return OK;
    }
    else if (STRCMP (name, "ICMPControlSwitch") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlGlobalICMPControlSwitch, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlGlobalICMPControlSwitch\n");
            value->out_int = 0;
            return OK;
        }
        if (u4val == TR_FWL_GLOBAL_STATUS_DISABLE)
        {
            value->out_uint = TR_FWL_FALSE;
        }
        else
        {
            value->out_uint = TR_FWL_TRUE;
        }
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlGlobalICMPControlSwitch ret %d\n",
                       value->out_uint);
        return OK;
    }
    else if (STRCMP (name, "MaxRules") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlGlobalMaxRules, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_INTEGER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "Exit: Get failed for FwlGlobalMaxRules\n");
            value->out_int = 0;
            return OK;
        }
        value->out_int = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FwlGlobalMaxRules ret %d\n", u4val);
        return OK;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrGetInternetGatewayDevice_Firewall\n");
    return i4rc;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_Firewall
 *  Description     : Get function for InternetGatewayDevice_Firewall
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrSetInternetGatewayDevice_Firewall (char *name, ParameterValue * value)
{
    INT4                i4rc = 0;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetInternetGatewayDevice_Firewall\n");
    i4rc = TrSetNonMand_InternetGatewayDevice_Firewall (name, value);

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrGetInternetGatewayDevice_Firewall\n");
    return i4rc;
}

/************************************************************************
 *  Function Name   : TrSetNonMand_InternetGatewayDevice_Firewall
 *  Description     : Set function for non-mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tFirewall entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetNonMand_InternetGatewayDevice_Firewall (char *name, ParameterValue * value)
{
    INT4                i4rc = 0;
    UINT4               u4val = 0;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    OctetStr.pu1_OctetList = au1OctetList;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetNonMand_InternetGatewayDevice_Firewall\n");

    if (STRCMP (name, "Trap") == 0)
    {
        if (value->in_uint == TR_FWL_FALSE)
        {
            u4val = TR_FWL_GLOBAL_STATUS_DISABLE;
        }
        else
        {
            u4val = TR_FWL_GLOBAL_STATUS_ENABLE;
        }

        i4rc =
            MynmhTest (*Join (FwlGlobalTrap, 10, NULL, 1, 0),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlGlobalTrap\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join (FwlGlobalTrap, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Set failed for FwlGlobalTrap\n");
            return SNMP_FAILURE;
        }
    }
    else if (STRCMP (name, "IpSpoofFiltering") == 0)
    {
        if (value->in_uint == TR_FWL_FALSE)
        {
            u4val = TR_FWL_GLOBAL_STATUS_DISABLE;
        }
        else
        {
            u4val = TR_FWL_GLOBAL_STATUS_ENABLE;
        }
        i4rc =
            MynmhTest (*Join (FwlGlobalIpSpoofFiltering, 10, NULL, 1, 0),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlGlobalIpSpoofFiltering\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join (FwlGlobalIpSpoofFiltering, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Set failed for FwlGlobalIpSpoofFiltering\n");
            return SNMP_FAILURE;
        }
    }
    else if (STRCMP (name, "ICMPType") == 0)
    {
        u4val = value->in_int;
        MynmhSet (*Join (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, 0),
                  SNMP_DATA_TYPE_INTEGER, (VOID *) NOT_IN_SERVICE);
        i4rc =
            MynmhTest (*Join (FwlIfICMPType, 12, FwlDefnIfTableINDEX, 1, 0),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlIfICMPType\n");
            MynmhSet (*Join (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) ACTIVE);
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join (FwlIfICMPType, 12, FwlDefnIfTableINDEX, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Set failed for FwlIfICMPType\n");
            MynmhSet (*Join (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) ACTIVE);
            return SNMP_FAILURE;
        }
        MynmhSet (*Join (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, 0),
                  SNMP_DATA_TYPE_INTEGER, (VOID *) ACTIVE);
    }
    else if (STRCMP (name, "MasterControlSwitch") == 0)
    {
        if (value->in_uint == TR_FWL_FALSE)
        {
            u4val = TR_FWL_GLOBAL_STATUS_DISABLE;
        }
        else
        {
            u4val = TR_FWL_GLOBAL_STATUS_ENABLE;
        }
        i4rc =
            MynmhTest (*Join (FwlGlobalMasterControlSwitch, 10, NULL, 1, 0),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlGlobalMasterControlSwitch\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join (FwlGlobalMasterControlSwitch, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Set failed for FwlGlobalMasterControlSwitch\n");
            return SNMP_FAILURE;
        }
    }
    else if (STRCMP (name, "TcpInterceptTimeout") == 0)
    {
        u4val = value->in_uint;
        i4rc =
            MynmhTest (*Join (FwlDefnInterceptTimeout, 10, NULL, 1, 0),
                       SNMP_DATA_TYPE_TIME_TICKS, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlDefnInterceptTimeout\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join (FwlDefnInterceptTimeout, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_TIME_TICKS, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Set failed for FwlDefnInterceptTimeout\n");
            return SNMP_FAILURE;
        }
    }
    else if (STRCMP (name, "TcpIntercept") == 0)
    {
        if (value->in_uint == TR_FWL_FALSE)
        {
            u4val = TR_FWL_GLOBAL_STATUS_DISABLE;
        }
        else
        {
            u4val = TR_FWL_GLOBAL_STATUS_ENABLE;
        }
        i4rc =
            MynmhTest (*Join (FwlGlobalTcpIntercept, 10, NULL, 1, 0),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlGlobalTcpIntercept\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join (FwlGlobalTcpIntercept, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Set failed for FwlGlobalTcpIntercept\n");
            return SNMP_FAILURE;
        }
    }
    else if (STRCMP (name, "Fragments") == 0)
    {
        u4val = value->in_int;
        MynmhSet (*Join (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, 0),
                  SNMP_DATA_TYPE_INTEGER, (VOID *) NOT_IN_SERVICE);
        i4rc =
            MynmhTest (*Join (FwlIfFragments, 12, FwlDefnIfTableINDEX, 1, 0),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlIfFragments\n");
            MynmhSet (*Join (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) ACTIVE);
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join (FwlIfFragments, 12, FwlDefnIfTableINDEX, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Set failed for FwlIfFragments\n");
            MynmhSet (*Join (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) ACTIVE);
            return SNMP_FAILURE;
        }
        MynmhSet (*Join (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, 0),
                  SNMP_DATA_TYPE_INTEGER, (VOID *) ACTIVE);
    }
    else if (STRCMP (name, "NetBiosFiltering") == 0)
    {
        if (value->in_uint == TR_FWL_FALSE)
        {
            u4val = TR_FWL_GLOBAL_STATUS_DISABLE;
        }
        else
        {
            u4val = TR_FWL_GLOBAL_STATUS_ENABLE;
        }
        i4rc =
            MynmhTest (*Join (FwlGlobalNetBiosFiltering, 10, NULL, 1, 0),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlGlobalNetBiosFiltering\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join (FwlGlobalNetBiosFiltering, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Set failed for FwlGlobalNetBiosFiltering\n");
            return SNMP_FAILURE;
        }
    }
    else if (STRCMP (name, "UrlFiltering") == 0)
    {
        if (value->in_uint == TR_FWL_FALSE)
        {
            u4val = TR_FWL_GLOBAL_STATUS_DISABLE;
        }
        else
        {
            u4val = TR_FWL_GLOBAL_STATUS_ENABLE;
        }
        i4rc =
            MynmhTest (*Join (FwlGlobalUrlFiltering, 10, NULL, 1, 0),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlGlobalUrlFiltering\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join (FwlGlobalUrlFiltering, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Set failed for FwlGlobalUrlFiltering\n");
            return SNMP_FAILURE;
        }
    }
    else if (STRCMP (name, "TcpInterceptThreshold") == 0)
    {
        u4val = value->in_int;
        i4rc =
            MynmhTest (*Join (FwlDefnTcpInterceptThreshold, 10, NULL, 1, 0),
                       SNMP_DATA_TYPE_INTEGER32, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlDefnTcpInterceptThreshold\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join (FwlDefnTcpInterceptThreshold, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_INTEGER32, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Set failed for FwlDefnTcpInterceptThreshold\n");
            return SNMP_FAILURE;
        }
    }
    else if (STRCMP (name, "IpOptions") == 0)
    {
        u4val = value->in_int;
        MynmhSet (*Join (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, 0),
                  SNMP_DATA_TYPE_INTEGER, (VOID *) NOT_IN_SERVICE);
        i4rc =
            MynmhTest (*Join (FwlIfIpOptions, 12, FwlDefnIfTableINDEX, 1, 0),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlIfIpOptions\n");
            MynmhSet (*Join (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) ACTIVE);
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join (FwlIfIpOptions, 12, FwlDefnIfTableINDEX, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Set failed for FwlIfIpOptions\n");
            MynmhSet (*Join (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) ACTIVE);
            return SNMP_FAILURE;
        }
        MynmhSet (*Join (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, 0),
                  SNMP_DATA_TYPE_INTEGER, (VOID *) ACTIVE);
    }
    else if (STRCMP (name, "ICMPControlSwitch") == 0)
    {
        if (value->in_uint == TR_FWL_FALSE)
        {
            u4val = TR_FWL_GLOBAL_STATUS_DISABLE;
        }
        else
        {
            u4val = TR_FWL_GLOBAL_STATUS_ENABLE;
        }
        i4rc =
            MynmhTest (*Join (FwlGlobalICMPControlSwitch, 10, NULL, 1, 0),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlGlobalICMPControlSwitch\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join (FwlGlobalICMPControlSwitch, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Set failed for FwlGlobalICMPControlSwitch\n");
            return SNMP_FAILURE;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetNonMand_InternetGatewayDevice_Firewall\n");
    return TR_NOT_MAND_PARAM;
}

/***********************firewall global group ends***********************/

/************************************************************************
*                      fwlDefinition group begins                       *
************************************************************************/

/****************************fwlDefnFilterTable*************************/

/************************************************************************
 *  Function Name   : TrScan_FwlDefnFilterTable
 *  Description     : Scans and detects available Firewall Filter.
 *
 *  Input           : none.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrScan_FwlDefnFilterTable ()
{
    tFwlDefnFilterTable *pFwlDefnFilterTable = NULL;
    tFwlDefnFilterTable *pNextfwlDefnFilterTable = NULL;
    tSNMP_OCTET_STRING_TYPE FilterName;
    tSNMP_OCTET_STRING_TYPE NextFilterName;
    UINT1               au1FilterName[MAX_OCTETSTRING_SIZE];
    UINT1               au1NextFilterName[MAX_OCTETSTRING_SIZE];
    UINT4               u4NewNode = 0;
    INT4                i4rc = 0;
    UINT4               u4TrIdx = 0;
    UINT4              *ptr = NULL;
    UINT4               u4Size = 0;

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrScan_FwlDefnFilterTable\n");

    MEMSET (&FilterName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextFilterName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (au1FilterName, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (au1NextFilterName, 0, MAX_OCTETSTRING_SIZE);

    FilterName.pu1_OctetList = au1FilterName;
    FilterName.i4_Length = STRLEN (au1FilterName);
    NextFilterName.pu1_OctetList = au1NextFilterName;
    NextFilterName.i4_Length = STRLEN (au1NextFilterName);

    i4rc = nmhGetFirstIndexFwlDefnFilterTable (&FilterName);
    if (i4rc == SNMP_FAILURE)
    {
        TR69_TRC (TR69_DBG_TRC,
                  "GetFirstIndex failed for FwlDefnFilterTable\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_FwlDefnFilterTable\n");
        return (OSIX_FAILURE);
    }
    STRNCPY (NextFilterName.pu1_OctetList,
             FilterName.pu1_OctetList, FilterName.i4_Length);
    NextFilterName.i4_Length = FilterName.i4_Length;

    UTL_SLL_OFFSET_SCAN (&FwlDefnFilterTableList, pFwlDefnFilterTable,
                         pNextfwlDefnFilterTable, tFwlDefnFilterTable *)
    {
        pFwlDefnFilterTable->u4Visited = 0;
    }
    while (1)
    {
        pFwlDefnFilterTable = NULL;
        u4NewNode = 1;

        /* Do not instantiate if already present */

        UTL_SLL_OFFSET_SCAN (&FwlDefnFilterTableList, pFwlDefnFilterTable,
                             pNextfwlDefnFilterTable, tFwlDefnFilterTable *)
        {
            if (STRCMP
                (pFwlDefnFilterTable->au1FilterName,
                 NextFilterName.pu1_OctetList) == 0)
            {
                u4NewNode = 0;
                pFwlDefnFilterTable->u4Visited = 1;
                break;
            }
        }

        /* Instantiate if this is a new instance */
        if (u4NewNode)
        {
            u4TrIdx = 0;
            i4rc =
                addObjectIntern
                ("InternetGatewayDevice.Firewall.fwlDefnFilterTable",
                 (int *) &u4TrIdx);

            if (i4rc == 0)
            {
                pFwlDefnFilterTable =
                    TrAdd_DefnFilterTable (u4TrIdx, &NextFilterName);
                if (pFwlDefnFilterTable == NULL)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- TrScan: FwlDefnFilterTable is Null\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrScan_FwlDefnFilterTable\n");
                    return OSIX_FAILURE;
                }
                pFwlDefnFilterTable->u4Visited = 1;
            }
            else
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "addObjectIntern failed, rc = %d\n", i4rc);
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrScan_FwlDefnFilterTable\n");
            }
        }

        STRNCPY (FilterName.pu1_OctetList, NextFilterName.pu1_OctetList,
                 NextFilterName.i4_Length);
        FilterName.i4_Length = NextFilterName.i4_Length;
        i4rc = nmhGetNextIndexFwlDefnFilterTable (&FilterName, &NextFilterName);
        if (i4rc == SNMP_FAILURE)
        {
            break;
        }
    }

    /* Incase if an entry is deleted via CLI or SNMP, in order 
       to synchronize TR database locate the node and initialize 
       the structure  elements to zero */

    UTL_SLL_OFFSET_SCAN (&FwlDefnFilterTableList, pFwlDefnFilterTable,
                         pNextfwlDefnFilterTable, tFwlDefnFilterTable *)
    {
        /* Before initializing  the structure elements to zero, check 
           is made against both visited flag and the table mask in order to make 
           sure that this does not intervene the row creation process */

        if ((pFwlDefnFilterTable->u4Visited == 0) &&
            (pFwlDefnFilterTable->u4ValMask == FWL_DEFN_FILTER_TABLE_VAL_MASK))
        {
            ptr = &(pFwlDefnFilterTable->u4ValMask);
            u4Size =
                sizeof (tFwlDefnFilterTable) - (sizeof (UINT4) +
                                                sizeof (tTMO_SLL_NODE));
            MEMSET (ptr, 0, u4Size);
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_FwlDefnFilterTable\n");
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrInitInternetGatewayDevice_Firewall_fwlDefnFilterTable
 *  Description     : Creates a TR Instance for fwlDefnFilterTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrInitInternetGatewayDevice_Firewall_fwlDefnFilterTable (int idx1)
{
    tFwlDefnFilterTable *pFwlDefnFilterTable = NULL;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrInit_IGD_Firewall_fwlDefnFilterTable\n");
    if (idx1 == 0)
    {
        TR69_TRC (TR69_DBG_TRC, "TrInstance is Zero\n");

        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrInit_IGD_Firewall_fwlDefnFilterTable\n");
        return SNMP_SUCCESS;
    }

    pFwlDefnFilterTable = TrAdd_DefnFilterTable (idx1, '\0');

    if (pFwlDefnFilterTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "-E- TrInit: FwlDefnFilterTable is Null\n");

        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrInit_IGD_Firewall_fwlDefnFilterTable\n");
        return SNMP_FAILURE;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrInit_IGD_Firewall_fwlDefnFilterTable\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrAdd_DefnFilterTable
 *  Description     : Adds a Filter node to TR69.
 *
 *  Input           : u4TrIdx - Tr Instance
 *                    OctetStrIdx - Index.
 *  Output          : None
 *  Returns         : pFwlDefnFilterTable - Allocated DefnFilter node.
 *************************************************************************/
tFwlDefnFilterTable *
TrAdd_DefnFilterTable (UINT4 u4TrIdx, tSNMP_OCTET_STRING_TYPE * pOctetStrIdx)
{
    tFwlDefnFilterTable *pFwlDefnFilterTable = NULL;
    tFwlDefnFilterTable *pNextFwlDefnFilterTable = NULL;

    /* If TrAdd is called via init function, then 
       allocate the memory and initalize structure members.
       If TrAdd is called via Scan function, 
       then locate the node, assign the SNMP index 
       and table mask. */

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrAdd_DefnFilterTable\n");
    if (pOctetStrIdx == NULL)
    {
        pFwlDefnFilterTable = MEM_MALLOC (sizeof (tFwlDefnFilterTable),
                                          tFwlDefnFilterTable);

        if (pFwlDefnFilterTable == NULL)
        {
            TR69_TRC (TR69_DBG_TRC, " Memory Allocation - Failed\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_DefnFilterTable\n");
            return NULL;
        }

        MEMSET (pFwlDefnFilterTable, 0, sizeof (tFwlDefnFilterTable));

        pFwlDefnFilterTable->u4TrInstance = u4TrIdx;
        pFwlDefnFilterTable->u4RowStatus = 0;
        pFwlDefnFilterTable->u4ValMask = 0;
        pFwlDefnFilterTable->u4NonMandValMask = 0;
        TMO_SLL_Add (&FwlDefnFilterTableList, &pFwlDefnFilterTable->Link);

        TR69_TRC (TR69_DBG_TRC, "Node Allocation - Success\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_DefnFilterTable\n");
        return pFwlDefnFilterTable;
    }

    UTL_SLL_OFFSET_SCAN (&FwlDefnFilterTableList, pFwlDefnFilterTable,
                         pNextFwlDefnFilterTable, tFwlDefnFilterTable *)
    {
        if (pFwlDefnFilterTable->u4TrInstance == u4TrIdx)
        {
            MEMCPY (pFwlDefnFilterTable->au1FilterName,
                    pOctetStrIdx->pu1_OctetList, pOctetStrIdx->i4_Length);
            pFwlDefnFilterTable->u4ValMask = FWL_DEFN_FILTER_TABLE_VAL_MASK;
            TR69_TRC (TR69_DBG_TRC, "Row Creation - Success\n");
            break;
        }
    }

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_DefnFilterTable\n");
    return pFwlDefnFilterTable;
}

/************************************************************************
 *  Function Name   : TrDeleteInternetGatewayDevice_Firewall_fwlDefnFilterTable
 *  Description     : Delete TR Instance for fwlDefnFilterTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/1
 ************************************************************************/
INT4
TrDeleteInternetGatewayDevice_Firewall_fwlDefnFilterTable (int idx1)
{
    tFwlDefnFilterTable *pFwlDefnFilterTable = NULL;
    tFwlDefnFilterTable *pNextfwlDefnFilterTable = NULL;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    UINT4               i4rc = 0;

    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrDelete_IGD_Firewall_fwlDefnFilterTable\n");

    UTL_SLL_OFFSET_SCAN (&FwlDefnFilterTableList, pFwlDefnFilterTable,
                         pNextfwlDefnFilterTable, tFwlDefnFilterTable *)
    {
        if (pFwlDefnFilterTable->u4TrInstance == (UINT4) idx1)
        {
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Deleting Filter Table entry TrInstance:%d String:%s\n",
                           pFwlDefnFilterTable->u4TrInstance,
                           pFwlDefnFilterTable->au1FilterName);

            OctetStrIdx.i4_Length = STRLEN (pFwlDefnFilterTable->au1FilterName);
            MEMCPY (OctetStrIdx.pu1_OctetList,
                    pFwlDefnFilterTable->au1FilterName, OctetStrIdx.i4_Length);

            if (nmhValidateIndexInstanceFwlDefnFilterTable (&OctetStrIdx) ==
                SNMP_SUCCESS)
            {
                i4rc = MynmhTest (*Join
                                  (FwlFilterRowStatus, 12,
                                   FwlDefnFilterTableINDEX, 1, &OctetStrIdx),
                                  SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC, "Test failed for Destroy\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDelete_IGD_Firewall_fwlDefnFilterTable\n");
                    return SNMP_FAILURE;
                }
                i4rc = MynmhSet (*Join
                                 (FwlFilterRowStatus, 12,
                                  FwlDefnFilterTableINDEX, 1, &OctetStrIdx),
                                 SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC, "Set failed for Destroy\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDelete_IGD_Firewall_fwlDefnFilterTable\n");
                    return SNMP_FAILURE;
                }

            }
            TMO_SLL_Delete (&FwlDefnFilterTableList,
                            (tTMO_SLL_NODE *) pFwlDefnFilterTable);
            MEM_FREE (pFwlDefnFilterTable);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrDelete_IGD_Firewall_fwlDefnFilterTable\n");
            return SNMP_SUCCESS;
        }
    }
    /* Instance not present - return SNMP_FAILURE */
    if (pFwlDefnFilterTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "FwlDefnFilterTable is Null\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrDelete_IGD_Firewall_fwlDefnFilterTable\n");
        return SNMP_FAILURE;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrDelete_IGD_Firewall_fwlDefnFilterTable\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_Firewall_fwlDefnFilterTable
 *  Description     : Get function for InternetGatewayDevice_Firewall_fwlDefnFilterTable
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrGetInternetGatewayDevice_Firewall_fwlDefnFilterTable (char *name, int idx1,
                                                        ParameterValue * value)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT4               u4val = 0;
    UINT4              *ptr = NULL;
    UINT4               u4Size = 0;
    INT4                i4rc = 0;

    tFwlDefnFilterTable *pFwlDefnFilterTable = NULL;
    tFwlDefnFilterTable *pNextFwlDefnFilterTable = NULL;
    tFwlDefnFilterTable *pNode = NULL;

    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrGet_IGD_Firewall_fwlDefnFilterTable\n");

    UTL_SLL_OFFSET_SCAN (&FwlDefnFilterTableList, pFwlDefnFilterTable,
                         pNextFwlDefnFilterTable, tFwlDefnFilterTable *)
    {
        if (pFwlDefnFilterTable->u4TrInstance == (UINT4) idx1)
        {
            OctetStrIdx.i4_Length = STRLEN (pFwlDefnFilterTable->au1FilterName);
            MEMCPY (OctetStrIdx.pu1_OctetList,
                    pFwlDefnFilterTable->au1FilterName, OctetStrIdx.i4_Length);
            if (pFwlDefnFilterTable->u4ValMask ==
                FWL_DEFN_FILTER_TABLE_VAL_MASK)
            {
                if (nmhValidateIndexInstanceFwlDefnFilterTable (&OctetStrIdx) !=
                    SNMP_SUCCESS)
                {
                    ptr = &(pNode->u4ValMask);
                    u4Size =
                        sizeof (tFwlDefnFilterTable) - (sizeof (UINT4) +
                                                        sizeof (tTMO_SLL_NODE));
                    MEMSET (ptr, 0, u4Size);
                    TR69_TRC_ARG1 (TR69_DBG_TRC,
                                   "-E- TrGet: nmhValidate failed for TrInstance:%d \n",
                                   pFwlDefnFilterTable->u4TrInstance);
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrGet_IGD_Firewall_fwlDefnFilterTable\n");
                }
            }
            break;
        }
    }

    if (pFwlDefnFilterTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "-E- TrGet: FwlDefnFilterTable is Null\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrGet_IGD_Firewall_fwlDefnFilterTable\n");
        return -1;
    }

    pNode = pFwlDefnFilterTable;

    if (STRCMP (name, "SrcPort") == 0)
    {
        if (((pNode->u4ValMask) & (FWL_DEFN_FILTER_TABLE_VAL_MASK)) ==
            FWL_DEFN_FILTER_TABLE_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FwlFilterSrcPort, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM, &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Get failed for FwlFilterSrcPort\n");
                MEMSET (pNode->au1SrcPort, '\0', 12);
                value->out_cval = pNode->au1SrcPort;
                return OK;
            }
            MEMSET (pNode->au1SrcPort, '\0', 12);
            MEMCPY (pNode->au1SrcPort, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);

            value->out_cval = pNode->au1SrcPort;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlFilterSrcPort ret %s\n", value->out_cval);
            return OK;
        }
        else
        {
            value->out_cval = pNode->au1SrcPort;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlFilterSrcPort ret %s\n", value->out_cval);
            return OK;
        }
    }
    else if (STRCMP (name, "DestAddress") == 0)
    {
        if (((pNode->u4ValMask & FWL_DEFN_FILTER_TABLE_VAL_MASK)) ==
            FWL_DEFN_FILTER_TABLE_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FwlFilterDestAddress, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM, &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Get failed for FwlFilterDestAddress\n");
                MEMSET (pNode->au1DestAddress, '\0', 20);
                value->out_cval = pNode->au1DestAddress;
                return OK;
            }
            MEMSET (pNode->au1DestAddress, '\0', 20);
            MEMCPY (pNode->au1DestAddress, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);

            value->out_cval = pNode->au1DestAddress;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlFilterDestAddress ret %s\n",
                           value->out_cval);
            return OK;
        }
        else
        {
            value->out_cval = pNode->au1DestAddress;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlFilterDestAddress ret %s\n",
                           value->out_cval);
            return OK;
        }
    }
    else if (STRCMP (name, "DestPort") == 0)
    {
        if (((pNode->u4ValMask) & (FWL_DEFN_FILTER_TABLE_VAL_MASK)) ==
            FWL_DEFN_FILTER_TABLE_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FwlFilterDestPort, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM, &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Get failed for FwlFilterDestPort\n");
                MEMSET (pNode->au1DestPort, '\0', 12);
                value->out_cval = pNode->au1DestPort;
                return OK;
            }
            MEMSET (pNode->au1DestPort, '\0', 12);
            MEMCPY (pNode->au1DestPort, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);

            value->out_cval = pNode->au1DestPort;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlFilterDestPort ret %s\n", value->out_cval);
            return OK;
        }
        else
        {
            value->out_cval = pNode->au1DestPort;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlFilterDestPort ret %s\n", value->out_cval);
            return OK;
        }
    }
    else if (STRCMP (name, "RowStatus") == 0)
    {
        if (((pNode->u4ValMask) & (FWL_DEFN_FILTER_TABLE_VAL_MASK)) ==
            FWL_DEFN_FILTER_TABLE_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Get failed for FwlFilterRowStatus\n");
                value->out_uint = 0;
                return OK;
            }
            if (u4val == TR_DISABLE_ROWSTATUS)
            {
                value->out_uint = 0;
                pNode->u4RowStatus = TR_DISABLE_ROWSTATUS;
            }
            else
            {
                value->out_uint = 1;
                pNode->u4RowStatus = u4val = TR_ENABLE_ROWSTATUS;
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlFilterRowStatus ret %d\n", value->out_uint);
            return OK;
        }
        else
        {
            if (pNode->u4RowStatus == TR_ENABLE_ROWSTATUS)
            {
                value->out_uint = 1;
            }
            else
            {
                value->out_uint = 0;
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlFilterRowStatus ret %d\n", value->out_uint);
            return OK;
        }
    }
    else if (STRCMP (name, "Tos") == 0)
    {
        if (((pNode->u4ValMask) & (FWL_DEFN_FILTER_TABLE_VAL_MASK)) ==
            FWL_DEFN_FILTER_TABLE_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FwlFilterTos, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER32, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "Exit: Get failed for FwlFilterTos\n");
                value->out_int = 0;
                return OK;
            }
            value->out_int = u4val;
            pNode->i4Tos = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlFilterTos ret %d\n", value->out_int);
            return OK;
        }
        else
        {
            value->out_int = pNode->i4Tos;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlFilterTos ret %d\n", value->out_int);
            return OK;
        }
    }
    else if (STRCMP (name, "FilterName") == 0)
    {
        value->out_cval = pNode->au1FilterName;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlFilterFilterName ret %s\n", value->out_cval);
        return OK;
    }
    else if (STRCMP (name, "SrcAddress") == 0)
    {
        if (((pNode->u4ValMask) & (FWL_DEFN_FILTER_TABLE_VAL_MASK)) ==
            FWL_DEFN_FILTER_TABLE_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FwlFilterSrcAddress, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM, &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Get failed for FwlFilterSrcAddress\n");
                MEMSET (pNode->au1SrcAddress, '\0', 20);
                value->out_cval = pNode->au1SrcAddress;
                return OK;
            }
            MEMSET (pNode->au1SrcAddress, '\0', 20);
            MEMCPY (pNode->au1SrcAddress, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);

            value->out_cval = pNode->au1SrcAddress;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlFilterSrcAddress ret %s\n",
                           value->out_cval);
            return OK;
        }
        else
        {
            value->out_cval = pNode->au1SrcAddress;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlFilterSrcAddress ret %s\n",
                           value->out_cval);
            return OK;
        }
    }
    else if (STRCMP (name, "HitsCount") == 0)
    {
        if (((pNode->u4ValMask) & (FWL_DEFN_FILTER_TABLE_VAL_MASK)) ==
            FWL_DEFN_FILTER_TABLE_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FwlFilterHitsCount, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_COUNTER32, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Get failed for FwlFilterHitsCount\n");
                value->out_uint = 0;
                return OK;
            }
            value->out_uint = u4val;
            pNode->u4HitsCount = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlFilterHitsCount ret %d\n", value->out_int);
            return OK;
        }
        else
        {
            value->out_uint = pNode->u4HitsCount;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlFilterHitsCount ret %d\n", value->out_int);
            return OK;
        }
    }
    else if (STRCMP (name, "Protocol") == 0)
    {
        if (((pNode->u4ValMask) & (FWL_DEFN_FILTER_TABLE_VAL_MASK)) ==
            FWL_DEFN_FILTER_TABLE_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FwlFilterProtocol, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Get failed for FwlFilterProtocol\n");
                value->out_int = 0;
                return OK;
            }
            value->out_int = u4val;
            pNode->i4Protocol = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlFilterProtocol ret %d\n", value->out_int);
            return OK;
        }
        else
        {
            value->out_int = pNode->i4Protocol;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlFilterProtocol ret %d\n", value->out_int);
            return OK;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrGet_IGD_Firewall_fwlDefnFilterTable\n");
    return OK;
}

/************************************************************************
 *  Function Name   : TrSet_InternetGatewayDevice_Firewall_fwlDefnFilterTable
 *  Description     : Set function for fwlDefnFilterTable
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrSetInternetGatewayDevice_Firewall_fwlDefnFilterTable (char *name, int idx1,
                                                        ParameterValue * value)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT4               u4val = 0;
    UINT4               u4MakeActive = 0;
    INT4                i4rc = 0;
    INT4                i4RetVal = 0;
    UINT4              *ptr = NULL;
    UINT4               u4Size = 0;

    tFwlDefnFilterTable *pFwlDefnFilterTable = NULL;
    tFwlDefnFilterTable *pNextFwlDefnFilterTable = NULL;
    tFwlDefnFilterTable *pNode = NULL;

    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSet_IGD_Firewall_fwlDefnFilterTable\n");

    UTL_SLL_OFFSET_SCAN (&FwlDefnFilterTableList, pFwlDefnFilterTable,
                         pNextFwlDefnFilterTable, tFwlDefnFilterTable *)
    {
        if (pFwlDefnFilterTable->u4TrInstance == (UINT4) idx1)
        {
            OctetStrIdx.i4_Length = STRLEN (pFwlDefnFilterTable->au1FilterName);
            MEMCPY (OctetStrIdx.pu1_OctetList,
                    pFwlDefnFilterTable->au1FilterName, OctetStrIdx.i4_Length);
            if (pFwlDefnFilterTable->u4ValMask ==
                FWL_DEFN_FILTER_TABLE_VAL_MASK)
            {
                if (nmhValidateIndexInstanceFwlDefnFilterTable (&OctetStrIdx) !=
                    SNMP_SUCCESS)
                {
                    ptr = &(pFwlDefnFilterTable->u4ValMask);
                    u4Size =
                        sizeof (tFwlDefnFilterTable) - (sizeof (UINT4) +
                                                        sizeof (tTMO_SLL_NODE));
                    MEMSET (ptr, 0, u4Size);
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- TrSet: nmhValidate - Failed \n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrSet_IGD_Firewall_fwlDefnFilterTable\n");
                    return SNMP_FAILURE;
                }
            }
            break;
        }
    }

    if (pFwlDefnFilterTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "-E- TrSet: FwlDefnFilterTable is Null\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrSet_IGD_Firewall_fwlDefnFilterTable\n");
        return SNMP_FAILURE;
    }

    pNode = pFwlDefnFilterTable;
    u4MakeActive = 0;
    if (((pNode->u4ValMask & FWL_DEFN_FILTER_TABLE_VAL_MASK) ==
         FWL_DEFN_FILTER_TABLE_VAL_MASK) && (STRCMP (name, "RowStatus") != 0))
    {
        u4val = NOT_IN_SERVICE;
        i4rc =
            MynmhTest (*Join
                       (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSet: Test failed for FwlFilterRowStatus\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSet: Set failed for FwlFilterRowStatus\n");
            return SNMP_FAILURE;
        }
        u4MakeActive = 1;
    }
    i4RetVal =
        TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnFilterTable (name,
                                                                        value,
                                                                        pNode);
    if (i4RetVal == TR_NOT_MAND_PARAM)
    {
        i4RetVal =
            TrSetMand_InternetGatewayDevice_Firewall_fwlDefnFilterTable (name,
                                                                         value,
                                                                         pNode);
    }

    if ((u4MakeActive) && (STRCMP (name, "RowStatus") != 0)
        && (pNode->u4RowStatus == ACTIVE))
    {
        u4val = ACTIVE;
        OctetStrIdx.i4_Length = STRLEN (pNode->au1FilterName);
        MEMCPY (OctetStrIdx.pu1_OctetList,
                pNode->au1FilterName, OctetStrIdx.i4_Length);
        i4rc =
            MynmhTest (*Join
                       (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSet: Test failed for FwlFilterRowStatus\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSet: Set failed for FwlFilterRowStatus\n");
            return SNMP_FAILURE;
        }
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrSet_IGD_Firewall_fwlDefnFilterTable - Failure\n");
        return SNMP_FAILURE;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSet_IGD_Firewall_fwlDefnFilterTable - Success\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnFilterTable
 *  Description     : Set function for non-mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tFwlDefnFilterTable entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnFilterTable (char *name,
                                                                ParameterValue *
                                                                value,
                                                                tFwlDefnFilterTable
                                                                * pNode)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    INT4                i4rc = 0;
    UINT4               u4val = 0;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetNonMand_IGD_Firewall_fwlDefnFilterTable\n");

    OctetStrIdx.i4_Length = STRLEN (pNode->au1FilterName);
    MEMCPY (OctetStrIdx.pu1_OctetList,
            pNode->au1FilterName, OctetStrIdx.i4_Length);

    if (STRCMP (name, "SrcPort") == 0)
    {
        if ((pNode->u4ValMask & FWL_DEFN_FILTER_TABLE_VAL_MASK) ==
            FWL_DEFN_FILTER_TABLE_VAL_MASK)
        {
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1SrcPort, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            i4rc =
                MynmhTest (*Join
                           (FwlFilterSrcPort, 12, FwlDefnFilterTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                           (VOID *) &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Test failed for FwlFilterSrcPort\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlFilterSrcPort, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                          (VOID *) &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Set failed for FwlFilterSrcPort\n");
                return SNMP_FAILURE;
            }
            pNode->u4NonMandValMask |= FWL_FILTER_SRC_PORT_MASK;
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnFilterTable - Success\n");
            return SNMP_SUCCESS;
        }
        else
        {
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnFilterTable (name,
                                                                        pNode,
                                                                        value,
                                                                        !TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnFilterTable\n");
            return SNMP_SUCCESS;
        }

    }
    else if (STRCMP (name, "DestAddress") == 0)
    {
        if ((pNode->u4ValMask & FWL_DEFN_FILTER_TABLE_VAL_MASK) ==
            FWL_DEFN_FILTER_TABLE_VAL_MASK)
        {
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1DestAddress, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            i4rc =
                MynmhTest (*Join
                           (FwlFilterDestAddress, 12, FwlDefnFilterTableINDEX,
                            1, &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                           (VOID *) &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Test failed for FwlFilterDestAddress\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlFilterDestAddress, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                          (VOID *) &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Set failed for FwlFilterDestAddress\n");
                return SNMP_FAILURE;
            }
            pNode->u4NonMandValMask |= FWL_FILTER_DEST_ADDRESS_MASK;
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnFilterTable - Success\n");
            return SNMP_SUCCESS;
        }
        else
        {
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnFilterTable (name,
                                                                        pNode,
                                                                        value,
                                                                        !TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnFilterTable\n");
            return SNMP_SUCCESS;
        }

    }
    else if (STRCMP (name, "DestPort") == 0)
    {
        if ((pNode->u4ValMask & FWL_DEFN_FILTER_TABLE_VAL_MASK) ==
            FWL_DEFN_FILTER_TABLE_VAL_MASK)
        {
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1DestPort, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            i4rc =
                MynmhTest (*Join
                           (FwlFilterDestPort, 12, FwlDefnFilterTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                           (VOID *) &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Test failed for FwlFilterDestPort\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlFilterDestPort, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                          (VOID *) &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Set failed for FwlFilterDestPort\n");
                return SNMP_FAILURE;
            }
            pNode->u4NonMandValMask |= FWL_FILTER_DEST_PORT_MASK;
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnFilterTable - Success\n");
            return SNMP_SUCCESS;
        }
        else
        {
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnFilterTable (name,
                                                                        pNode,
                                                                        value,
                                                                        !TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnFilterTable\n");
            return SNMP_SUCCESS;
        }

    }
    else if (STRCMP (name, "RowStatus") == 0)
    {
        if ((pNode->u4ValMask & FWL_DEFN_FILTER_TABLE_VAL_MASK) ==
            FWL_DEFN_FILTER_TABLE_VAL_MASK)
        {
            if (value->in_uint == TR_FWL_TRUE)
            {
                pNode->u4RowStatus = u4val = TR_ENABLE_ROWSTATUS;
            }
            else
            {
                pNode->u4RowStatus = u4val = TR_DISABLE_ROWSTATUS;
            }
            i4rc =
                MynmhTest (*Join
                           (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Test failed for FwlFilterRowStatus\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Set failed for FwlFilterRowStatus\n");
                return SNMP_FAILURE;
            }
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnFilterTable - Success\n");
            return SNMP_SUCCESS;
        }
        else
        {
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnFilterTable (name,
                                                                        pNode,
                                                                        value,
                                                                        !TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnFilterTable\n");
            return SNMP_SUCCESS;
        }

    }
    else if (STRCMP (name, "Tos") == 0)
    {
        if ((pNode->u4ValMask & FWL_DEFN_FILTER_TABLE_VAL_MASK) ==
            FWL_DEFN_FILTER_TABLE_VAL_MASK)
        {
            pNode->i4Tos = u4val = value->in_int;
            i4rc =
                MynmhTest (*Join
                           (FwlFilterTos, 12, FwlDefnFilterTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER32,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Test failed for FwlFilterTos\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlFilterTos, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER32,
                          (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Get failed for FwlFilterTos\n");
                return SNMP_FAILURE;
            }
            pNode->u4NonMandValMask |= FWL_FILTER_TOS_MASK;
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnFilterTable - Success\n");
            return SNMP_SUCCESS;
        }
        else
        {
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnFilterTable (name,
                                                                        pNode,
                                                                        value,
                                                                        !TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnFilterTable\n");
            return SNMP_SUCCESS;
        }

    }
    else if (STRCMP (name, "SrcAddress") == 0)
    {
        if ((pNode->u4ValMask & FWL_DEFN_FILTER_TABLE_VAL_MASK) ==
            FWL_DEFN_FILTER_TABLE_VAL_MASK)
        {
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1SrcAddress, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            i4rc =
                MynmhTest (*Join
                           (FwlFilterSrcAddress, 12, FwlDefnFilterTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                           (VOID *) &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Test failed for FwlFilterSrcAddress\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlFilterSrcAddress, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                          (VOID *) &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Set failed for FwlFilterSrcAddress\n");
                return SNMP_FAILURE;
            }
            pNode->u4NonMandValMask |= FWL_FILTER_SRC_ADDRESS_MASK;
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnFilterTable - Success\n");
            return SNMP_SUCCESS;
        }
        else
        {
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnFilterTable (name,
                                                                        pNode,
                                                                        value,
                                                                        !TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnFilterTable\n");
            return SNMP_SUCCESS;
        }

    }
    else if (STRCMP (name, "Protocol") == 0)
    {
        if ((pNode->u4ValMask & FWL_DEFN_FILTER_TABLE_VAL_MASK) ==
            FWL_DEFN_FILTER_TABLE_VAL_MASK)
        {
            pNode->i4Protocol = u4val = value->in_int;
            i4rc =
                MynmhTest (*Join
                           (FwlFilterProtocol, 12, FwlDefnFilterTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Test failed for FwlFilterProtocol\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlFilterProtocol, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Set failed for FwlFilterProtocol\n");
                return SNMP_FAILURE;
            }
            pNode->u4NonMandValMask |= FWL_FILTER_PROTOCOL_MASK;
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnFilterTable - Success\n");
            return SNMP_SUCCESS;
        }
        else
        {
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnFilterTable (name,
                                                                        pNode,
                                                                        value,
                                                                        !TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnFilterTable\n");
            return SNMP_SUCCESS;
        }

    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetNonMand_IGD_Firewall_fwlDefnFilterTable - Mand Param\n");
    return TR_NOT_MAND_PARAM;
}

/************************************************************************
 *  Function Name   : TrSetMand_InternetGatewayDevice_Firewall_fwlDefnFilterTable
 *  Description     : Set function for mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tfwlDefnFilterTable entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetMand_InternetGatewayDevice_Firewall_fwlDefnFilterTable (char *name,
                                                             ParameterValue *
                                                             value,
                                                             tFwlDefnFilterTable
                                                             * pNode)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    tFwlDefnFilterTable NewNode;
    UINT4              *ptr = NULL;
    UINT4               u4Size = 0;
    INT4                i4rc = 0;
#ifdef TR69MSR_WANTED
    UINT1               u1Tr69MsrStatus = 0;
#endif

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NewNode, 0, sizeof (tFwlDefnFilterTable));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetMand_IGD_Firewall_fwlDefnFilterTable\n");

    OctetStrIdx.i4_Length = STRLEN (pNode->au1FilterName);
    MEMCPY (OctetStrIdx.pu1_OctetList,
            pNode->au1FilterName, OctetStrIdx.i4_Length);

    if (pNode->u4ValMask != FWL_DEFN_FILTER_TABLE_VAL_MASK)
    {
        TrSetAll_InternetGatewayDevice_Firewall_fwlDefnFilterTable (name, pNode,
                                                                    value,
                                                                    !TR_DO_SNMP_SET);
        if ((pNode->u4ValMask & FWL_DEFN_FILTER_TABLE_VAL_MASK) ==
            FWL_DEFN_FILTER_TABLE_VAL_MASK)
        {
            TR69_TRC (TR69_DBG_TRC, "Creation of new row in Filter Table\n");

            OctetStrIdx.i4_Length = STRLEN (pNode->au1FilterName);
            MEMCPY (OctetStrIdx.pu1_OctetList,
                    pNode->au1FilterName, OctetStrIdx.i4_Length);

            i4rc =
                MynmhTest (*Join
                           (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) CREATE_AND_WAIT);
            if (i4rc == SNMP_FAILURE)
            {
#ifdef TR69MSR_WANTED
                Tr69MsrGetRestoreStatus (&u1Tr69MsrStatus);
                if (u1Tr69MsrStatus == OSIX_TRUE)
                {
                    return SNMP_SUCCESS;
                }
#endif
                ptr = &(pNode->u4ValMask);
                u4Size =
                    sizeof (tFwlDefnFilterTable) - (sizeof (UINT4) +
                                                    sizeof (tTMO_SLL_NODE));
                MEMSET (ptr, 0, u4Size);
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Test for CREATE_AND_WAIT failed\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnFilterTable\n");
                return SNMP_FAILURE;
            }
            i4rc =
                MynmhSet (*Join
                          (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) CREATE_AND_WAIT);
            if (i4rc == SNMP_FAILURE)
            {
                ptr = &(pNode->u4ValMask);
                u4Size =
                    sizeof (tFwlDefnFilterTable) - (sizeof (UINT4) +
                                                    sizeof (tTMO_SLL_NODE));
                MEMSET (ptr, 0, u4Size);
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Set for CREATE_AND_WAIT failed\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnFilterTable\n");
                return SNMP_FAILURE;
            }
            else
            {
                pNode->u4RowStatus = NOT_READY;
            }
            i4rc =
                TrSetAll_InternetGatewayDevice_Firewall_fwlDefnFilterTable
                (NULL, pNode, value, TR_DO_SNMP_SET);

            if (i4rc == SNMP_FAILURE)
            {
                MynmhSet (*Join
                          (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) DESTROY);
                ptr = &(pNode->u4ValMask);
                u4Size =
                    sizeof (tFwlDefnFilterTable) - (sizeof (UINT4) +
                                                    sizeof (tTMO_SLL_NODE));
                MEMSET (ptr, 0, u4Size);
                TR69_TRC (TR69_DBG_TRC, "-E- TrSetMand: SNMP Set failed\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnFilterTable\n");
                return SNMP_FAILURE;
            }
            i4rc =
                MynmhSet (*Join
                          (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) ACTIVE);
            if (i4rc == SNMP_FAILURE)
            {
                MynmhSet (*Join
                          (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) DESTROY);
                ptr = &(pNode->u4ValMask);
                u4Size =
                    sizeof (tFwlDefnFilterTable) - (sizeof (UINT4) +
                                                    sizeof (tTMO_SLL_NODE));
                MEMSET (ptr, 0, u4Size);
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Set for ACTIVE failed\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnFilterTable\n");
                return SNMP_FAILURE;
            }
            else
            {
                pNode->u4RowStatus = ACTIVE;
            }

        }
    }
    else
    {
        TR69_TRC (TR69_DBG_TRC,
                  "Modifiying an existing entry in Filter Table\n");
        MEMCPY (&NewNode, pNode, sizeof (*pNode));

        i4rc =
            MynmhTest (*Join
                       (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) DESTROY);

        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetMand: Test for DESTROY failed for FilterName:%s\n",
                           OctetStrIdx.pu1_OctetList);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnFilterTable\n");
            return SNMP_FAILURE;
        }
        i4rc =
            MynmhSet (*Join
                      (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);

        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetMand: Set for DESTROY failed for FilterName:%s\n",
                           OctetStrIdx.pu1_OctetList);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnFilterTable\n");
            return SNMP_FAILURE;
        }

        if (STRCMP (name, "FilterName") == 0)
        {
            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStrIdx.pu1_OctetList, value->in_cval,
                    OctetStrIdx.i4_Length);
        }

        i4rc =
            MynmhTest (*Join
                       (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) CREATE_AND_WAIT);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetMand: Test for CREATE_AND_WAIT failed for FilterName:%s\n",
                           OctetStrIdx.pu1_OctetList);

            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (pNode->au1FilterName);
            MEMCPY (OctetStrIdx.pu1_OctetList,
                    pNode->au1FilterName, OctetStrIdx.i4_Length);

            MynmhSet (*Join
                      (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) CREATE_AND_WAIT);

            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnFilterTable (NULL,
                                                                        pNode,
                                                                        value,
                                                                        TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnFilterTable\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) CREATE_AND_WAIT);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetMand: Set for CREATE_AND_WAIT failed for FilterName:%s\r\n",
                           OctetStrIdx.pu1_OctetList);

            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (pNode->au1FilterName);
            MEMCPY (OctetStrIdx.pu1_OctetList,
                    pNode->au1FilterName, OctetStrIdx.i4_Length);

            MynmhSet (*Join
                      (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) CREATE_AND_WAIT);

            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnFilterTable (NULL,
                                                                        pNode,
                                                                        value,
                                                                        TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnFilterTable\n");
            return SNMP_FAILURE;
        }

        /* First update NewNode with the new value */
        /* Then SET all the values for the new row */
        i4rc =
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnFilterTable (name,
                                                                        &NewNode,
                                                                        value,
                                                                        !TR_DO_SNMP_SET);
        i4rc =
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnFilterTable (NULL,
                                                                        &NewNode,
                                                                        value,
                                                                        TR_DO_SNMP_SET);

        if (i4rc == SNMP_FAILURE)
        {
            MynmhSet (*Join
                      (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);

            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (pNode->au1FilterName);
            MEMCPY (OctetStrIdx.pu1_OctetList,
                    pNode->au1FilterName, OctetStrIdx.i4_Length);

            MynmhSet (*Join
                      (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) CREATE_AND_WAIT);

            i4rc =
                TrSetAll_InternetGatewayDevice_Firewall_fwlDefnFilterTable
                (NULL, pNode, value, TR_DO_SNMP_SET);
            TR69_TRC (TR69_DBG_TRC, "-E- TrSetMand: SNMP Set failed\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnFilterTable\n");
            return SNMP_FAILURE;
        }
        else
        {
            MEMCPY (pNode, &NewNode, sizeof (NewNode));
        }
        MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
        OctetStrIdx.i4_Length = STRLEN (pNode->au1FilterName);
        MEMCPY (OctetStrIdx.pu1_OctetList,
                pNode->au1FilterName, OctetStrIdx.i4_Length);
        if ((STRCMP (name, "RowStatus") != 0) && (pNode->u4RowStatus == ACTIVE))
        {
            i4rc =
                MynmhSet (*Join
                          (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) ACTIVE);
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetMand_IGD_Firewall_fwlDefnFilterTable - Success\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrSetAll_InternetGatewayDevice_Firewall_fwlDefnFilterTable
 *  Description     : Set function for all objects of a table.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tFwlDefnFilterTable entry.
 *                    bTrDoSnmpSet - Indicates whether SNMP Set is to be done.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetAll_InternetGatewayDevice_Firewall_fwlDefnFilterTable (char *name,
                                                            tFwlDefnFilterTable
                                                            * pNode,
                                                            ParameterValue *
                                                            value,
                                                            BOOL1 bTrDoSnmpSet)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    INT4                i4rc = 0;
    UINT4               u4val = 0;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetAll_IGD_Firewall_fwlDefnFilterTable\n");

    OctetStrIdx.i4_Length = STRLEN (pNode->au1FilterName);
    MEMCPY (OctetStrIdx.pu1_OctetList,
            pNode->au1FilterName, OctetStrIdx.i4_Length);
    if (!name || STRCMP (name, "SrcPort") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            MEMSET (OctetStr.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1SrcPort, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            pNode->u4NonMandValMask |= FWL_FILTER_SRC_PORT_MASK;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
        /* While doing an add object, to ensure SNMP Set is allowed for 
           non-mandatory objects which are provided by the user, check is 
           made aganist the particular elements ValMask. */
        if ((pNode->u4NonMandValMask & (FWL_FILTER_SRC_PORT_MASK)) ==
            FWL_FILTER_SRC_PORT_MASK)
        {
            MEMSET (OctetStr.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStr.i4_Length = STRLEN (pNode->au1SrcPort);
            MEMCPY (OctetStr.pu1_OctetList, pNode->au1SrcPort,
                    OctetStr.i4_Length);
            i4rc =
                MynmhTest (*Join
                           (FwlFilterSrcPort, 12, FwlDefnFilterTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                           (VOID *) &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FwlFilterSrcPort\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlFilterSrcPort, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                          (VOID *) &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Set failed for FwlFilterSrcPort\n");
                return SNMP_FAILURE;
            }
        }
    }
    if (!name || STRCMP (name, "DestAddress") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1DestAddress, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            pNode->u4NonMandValMask |= FWL_FILTER_DEST_ADDRESS_MASK;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
        /* While doing an add object, to ensure SNMP Set is allowed for 
           non-mandatory objects which are provided by the user check is 
           made aganist the particular elements ValMask. */
        if ((pNode->u4NonMandValMask & FWL_FILTER_DEST_ADDRESS_MASK) ==
            FWL_FILTER_DEST_ADDRESS_MASK)
        {
            OctetStr.i4_Length = STRLEN (pNode->au1DestAddress);
            MEMCPY (OctetStr.pu1_OctetList, pNode->au1DestAddress,
                    OctetStr.i4_Length);
            i4rc =
                MynmhTest (*Join
                           (FwlFilterDestAddress, 12, FwlDefnFilterTableINDEX,
                            1, &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                           (VOID *) &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FwlFilterDestAddress\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlFilterDestAddress, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                          (VOID *) &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Set failed for FwlFilterDestAddress\n");
                return SNMP_FAILURE;
            }
        }
    }
    if (!name || STRCMP (name, "DestPort") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1DestPort, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            pNode->u4NonMandValMask |= FWL_FILTER_DEST_PORT_MASK;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
        /* While doing an add object, to ensure SNMP Set is allowed for 
           non-mandatory objects which are provided by the user, check is 
           made aganist the particular elements ValMask. */
        if ((pNode->u4NonMandValMask & FWL_FILTER_DEST_PORT_MASK) ==
            FWL_FILTER_DEST_PORT_MASK)
        {
            OctetStr.i4_Length = STRLEN (pNode->au1DestPort);
            MEMCPY (OctetStr.pu1_OctetList, pNode->au1DestPort,
                    OctetStr.i4_Length);
            i4rc =
                MynmhTest (*Join
                           (FwlFilterDestPort, 12, FwlDefnFilterTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                           (VOID *) &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FwlFilterDestPort\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlFilterDestPort, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                          (VOID *) &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Set failed for FwlFilterDestPort\n");
                return SNMP_FAILURE;
            }
        }
    }
    if (!name || STRCMP (name, "RowStatus") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if (value->in_uint == TR_FWL_TRUE)
            {
                pNode->u4RowStatus = u4val = TR_ENABLE_ROWSTATUS;
            }
            else
            {
                pNode->u4RowStatus = u4val = TR_DISABLE_ROWSTATUS;
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
        if (pNode->u4RowStatus != NOT_READY)
        {
            u4val = pNode->u4RowStatus;
            i4rc =
                MynmhTest (*Join
                           (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FwlFilterRowStatus\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Set failed for FwlFilterRowStatus\n");
                return SNMP_FAILURE;
            }
        }
    }
    if (!name || STRCMP (name, "Tos") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->i4Tos = u4val = value->in_int;
            pNode->u4NonMandValMask |= FWL_FILTER_TOS_MASK;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
        /* While doing an add object, to ensure SNMP Set is allowed for 
           non-mandatory objects which are provided by the user, check is 
           made aganist the particular elements ValMask. */

        if ((pNode->u4NonMandValMask & FWL_FILTER_TOS_MASK) ==
            FWL_FILTER_TOS_MASK)
        {
            u4val = pNode->i4Tos;
            i4rc =
                MynmhTest (*Join
                           (FwlFilterTos, 12, FwlDefnFilterTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER32,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FwlFilterTos\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlFilterTos, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER32,
                          (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Set failed for FwlFilterTos\n");
                return SNMP_FAILURE;
            }
        }
    }
    if (!name || STRCMP (name, "FilterName") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            MEMSET (pNode->au1FilterName, '\0', 36);
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMSET (OctetStr.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1FilterName, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            pNode->u4ValMask |= FWL_FILTER_FILTER_NAME_MASK;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
    }
    if (!name || STRCMP (name, "SrcAddress") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1SrcAddress, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            pNode->u4NonMandValMask |= FWL_FILTER_SRC_ADDRESS_MASK;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
        /* While doing an add object, to ensure SNMP Set is allowed for 
           non-mandatory objects which are provided by the user, check is 
           made aganist the particular elements ValMask. */
        if ((pNode->u4NonMandValMask & FWL_FILTER_SRC_ADDRESS_MASK) ==
            FWL_FILTER_SRC_ADDRESS_MASK)
        {
            OctetStr.i4_Length = STRLEN (pNode->au1SrcAddress);
            MEMCPY (OctetStr.pu1_OctetList, pNode->au1SrcAddress,
                    OctetStr.i4_Length);
            i4rc =
                MynmhTest (*Join
                           (FwlFilterSrcAddress, 12, FwlDefnFilterTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                           (VOID *) &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FwlFilterSrcAddress\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlFilterSrcAddress, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                          (VOID *) &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Set failed for FwlFilterSrcAddress\n");
                return SNMP_FAILURE;
            }
        }
    }
    if (!name || STRCMP (name, "Protocol") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->i4Protocol = u4val = value->in_int;
            pNode->u4NonMandValMask |= FWL_FILTER_PROTOCOL_MASK;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
        /* While doing an add object, to ensure SNMP Set is allowed for 
           non-mandatory objects which are provided by the user, check is 
           made aganist the particular elements ValMask. */
        if ((pNode->u4NonMandValMask & FWL_FILTER_PROTOCOL_MASK) ==
            FWL_FILTER_PROTOCOL_MASK)
        {
            u4val = pNode->i4Protocol;
            i4rc =
                MynmhTest (*Join
                           (FwlFilterProtocol, 12, FwlDefnFilterTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FwlFilterProtocol\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlFilterProtocol, 12, FwlDefnFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Set failed for FwlFilterProtocol\n");
                return SNMP_FAILURE;
            }
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetAll_IGD_Firewall_fwlDefnFilterTable - Success\n");
    return SNMP_SUCCESS;
}

/************************End of fwlDefnFilterTable***********************/

/****************************fwlDefnRuleTable*****************************/

/************************************************************************
 *  Function Name   : TrScan_FwlDefnRuleTable
 *  Description     : Scans and detects available Firewall Rules.
 *
 *  Input           : none.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
UINT4
TrScan_FwlDefnRuleTable ()
{
    tSNMP_OCTET_STRING_TYPE RuleName;
    tSNMP_OCTET_STRING_TYPE NextRuleName;
    tFwlDefnRuleTable  *pFwlDefnRuleTable = NULL;
    tFwlDefnRuleTable  *pNextfwlDefnRuleTable = NULL;
    UINT1               au1RuleName[MAX_OCTETSTRING_SIZE];
    UINT1               au1NextRuleName[MAX_OCTETSTRING_SIZE];
    UINT4               u4NewNode = 0;
    INT4                i4rc = 0;
    UINT4               u4TrIdx = 0;

    MEMSET (&RuleName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextRuleName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (au1RuleName, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (au1NextRuleName, 0, MAX_OCTETSTRING_SIZE);

    RuleName.pu1_OctetList = au1RuleName;
    NextRuleName.pu1_OctetList = au1NextRuleName;

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrScan_FwlDefnRuleTable\n");

    i4rc = nmhGetFirstIndexFwlDefnRuleTable (&RuleName);
    if (i4rc == SNMP_FAILURE)
    {
        return (OSIX_FAILURE);
    }
    STRNCPY (NextRuleName.pu1_OctetList, RuleName.pu1_OctetList,
             RuleName.i4_Length);
    NextRuleName.i4_Length = RuleName.i4_Length;

    UTL_SLL_OFFSET_SCAN (&FwlDefnRuleTableList, pFwlDefnRuleTable,
                         pNextfwlDefnRuleTable, tFwlDefnRuleTable *)
    {
        pFwlDefnRuleTable->u4Visited = 0;
    }
    while (1)
    {
        pFwlDefnRuleTable = NULL;
        u4NewNode = 1;

        /* Do not instantiate if already present */
        UTL_SLL_OFFSET_SCAN (&FwlDefnRuleTableList, pFwlDefnRuleTable,
                             pNextfwlDefnRuleTable, tFwlDefnRuleTable *)
        {
            if (STRCMP
                (pFwlDefnRuleTable->au1RuleName,
                 NextRuleName.pu1_OctetList) == 0)
            {
                u4NewNode = 0;
                pFwlDefnRuleTable->u4Visited = 1;
                break;
            }
        }

        /* Instantiate if this is a new instance */
        if (u4NewNode)
        {
            i4rc =
                addObjectIntern
                ("InternetGatewayDevice.Firewall.fwlDefnRuleTable",
                 (int *) &u4TrIdx);

            if (i4rc == 0)
            {
                pFwlDefnRuleTable =
                    TrAdd_DefnRuleTable (u4TrIdx, &NextRuleName);
                pFwlDefnRuleTable->u4Visited = 1;
                if (pFwlDefnRuleTable == NULL)
                {
                    return OSIX_FAILURE;
                }
            }
            else
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "addObjectIntern failed, rc = %d\n", i4rc);
            }
        }

        /* get the next elem */
        STRNCPY (RuleName.pu1_OctetList, NextRuleName.pu1_OctetList,
                 NextRuleName.i4_Length);
        RuleName.i4_Length = NextRuleName.i4_Length;
        i4rc = nmhGetNextIndexFwlDefnRuleTable (&RuleName, &NextRuleName);
        if (i4rc == SNMP_FAILURE)
        {
            break;
        }
    }
    UTL_SLL_OFFSET_SCAN (&FwlDefnRuleTableList, pFwlDefnRuleTable,
                         pNextfwlDefnRuleTable, tFwlDefnRuleTable *)
    {
        if (pFwlDefnRuleTable->u4Visited == 0)
        {

            pFwlDefnRuleTable->u4RowStatus = 0;
            pFwlDefnRuleTable->u4ValMask = 0;
            MEMSET (pFwlDefnRuleTable->au1RuleName, '\0', 36);
            MEMSET (pFwlDefnRuleTable->au1FilterSet, '\0', 252);
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_FwlDefnRuleTable\n");
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrAdd_DefnRuleTable
 *  Description     : Adds a rule node to TR69.
 *
 *  Input           : u4TrIdx - Tr Instance
 *                    OctetStrIdx - Index.
 *  Output          : None
 *  Returns         : pfwlDefnRule - Allocated DefnRule node.
 ************************************************************************/
tFwlDefnRuleTable  *
TrAdd_DefnRuleTable (UINT4 u4TrIdx, tSNMP_OCTET_STRING_TYPE * pOctetStrIdx)
{
    tFwlDefnRuleTable  *pFwlDefnRuleTable = NULL;
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrAdd_DefnRuleTable\n");

    pFwlDefnRuleTable = MEM_MALLOC (sizeof (tFwlDefnRuleTable),
                                    tFwlDefnRuleTable);

    if (pFwlDefnRuleTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "Memory Allocation Failed\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_DefnRuleTable\n");
        return NULL;
    }
    MEMSET (pFwlDefnRuleTable, 0, sizeof (tFwlDefnRuleTable));
    pFwlDefnRuleTable->u4TrInstance = u4TrIdx;
    MEMCPY (pFwlDefnRuleTable->au1RuleName, pOctetStrIdx->pu1_OctetList,
            pOctetStrIdx->i4_Length);
    pFwlDefnRuleTable->u4ValMask = FWL_DEFN_RULE_TABLE_VAL_MASK;

    TMO_SLL_Add (&FwlDefnRuleTableList, &pFwlDefnRuleTable->Link);

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_DefnRuleTable\n");
    return pFwlDefnRuleTable;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_Firewall_fwlDefnRuleTable
 *  Description     : Get function for InternetGatewayDevice_Firewall_fwlDefnRuleTable
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrGetInternetGatewayDevice_Firewall_fwlDefnRuleTable (char *name, int idx1,
                                                      ParameterValue * value)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT4               u4val = 0;
    INT4                i4rc = 0;

    tFwlDefnRuleTable  *pFwlDefnRuleTable = NULL;
    tFwlDefnRuleTable  *pNextFwlDefnRuleTable = NULL;
    tFwlDefnRuleTable  *pNode;

    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrGetInternetGatewayDevice_Firewall_fwlDefnRuleTable\n");

    OctetStr.pu1_OctetList = au1OctetList;
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    UTL_SLL_OFFSET_SCAN (&FwlDefnRuleTableList, pFwlDefnRuleTable,
                         pNextFwlDefnRuleTable, tFwlDefnRuleTable *)
    {
        if (pFwlDefnRuleTable->u4TrInstance == (UINT4) idx1)
        {
            OctetStrIdx.i4_Length = STRLEN (pFwlDefnRuleTable->au1RuleName);
            MEMCPY (OctetStrIdx.pu1_OctetList, pFwlDefnRuleTable->au1RuleName,
                    OctetStrIdx.i4_Length);
            if (nmhValidateIndexInstanceFwlDefnRuleTable (&OctetStrIdx) !=
                SNMP_SUCCESS)
            {
                pFwlDefnRuleTable->u4RowStatus = 0;
                pFwlDefnRuleTable->u4ValMask = 0;
                MEMSET (pFwlDefnRuleTable->au1RuleName, '\0', 36);
                MEMSET (pFwlDefnRuleTable->au1FilterSet, '\0', 252);
            }
            break;
        }
    }

    if (pFwlDefnRuleTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "In TrGet RuleTable is empty!!\n");
        return -1;
    }

    pNode = pFwlDefnRuleTable;

    if (STRCMP (name, "RuleName") == 0)
    {
        value->out_cval = pNode->au1RuleName;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlRuleRuleName ret %s\n", value->out_cval);
        return OK;
    }
    else if (STRCMP (name, "FilterSet") == 0)
    {
        if (((pNode->u4ValMask) & (FWL_DEFN_RULE_TABLE_VAL_MASK)) ==
            (FWL_DEFN_RULE_TABLE_VAL_MASK))
        {

            i4rc =
                MynmhGet (*Join
                          (FwlRuleFilterSet, 12, FwlDefnRuleTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM, &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Get failed for FwlRuleFilterSet\n");
                MEMSET (pFwlDefnRuleTable->au1FilterSet, '\0', 252);
                value->out_cval = pNode->au1FilterSet;
                return OK;
            }
            MEMCPY (pNode->au1FilterSet, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);

            value->out_cval = pNode->au1FilterSet;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlRuleFilterSet ret %s\n", value->out_cval);
            return OK;
        }
        else
        {
            value->out_cval = pNode->au1FilterSet;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlRuleFilterSet ret %s\n", value->out_cval);
            return OK;
        }
    }
    else if (STRCMP (name, "RowStatus") == 0)
    {
        if (((pNode->u4ValMask) & (FWL_DEFN_RULE_TABLE_VAL_MASK)) ==
            (FWL_DEFN_RULE_TABLE_VAL_MASK))
        {

            i4rc =
                MynmhGet (*Join
                          (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Get failed for FwlRuleRowStatus\n");
                value->out_uint = 0;
                return OK;
            }
            if (u4val == TR_DISABLE_ROWSTATUS)
            {
                value->out_uint = 0;
                pNode->u4RowStatus = TR_DISABLE_ROWSTATUS;
            }
            else
            {
                value->out_uint = 1;
                pNode->u4RowStatus = TR_ENABLE_ROWSTATUS;
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlRuleRowStatus ret %d\n", value->out_uint);
            return OK;
        }
        else
        {
            value->out_uint = pNode->u4RowStatus;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlRuleRowStatus ret %d\n", value->out_uint);
            return OK;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrGetInternetGatewayDevice_Firewall_fwlDefnRuleTable\n");
    return OK;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_Firewall_fwlDefnRuleTable
 *  Description     : Get function for InternetGatewayDevice_Firewall_fwlDefnRuleTable
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrSetInternetGatewayDevice_Firewall_fwlDefnRuleTable (char *name, int idx1,
                                                      ParameterValue * value)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT4               u4val = 0;
    UINT4               u4MakeActive = 0;
    INT4                i4rc = 0;

    tFwlDefnRuleTable  *pFwlDefnRuleTable = NULL;
    tFwlDefnRuleTable  *pNextFwlDefnRuleTable = NULL;
    tFwlDefnRuleTable  *pNode;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSet_IGD_Firewall_fwlDefnRuleTable\n");

    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    UTL_SLL_OFFSET_SCAN (&FwlDefnRuleTableList, pFwlDefnRuleTable,
                         pNextFwlDefnRuleTable, tFwlDefnRuleTable *)
    {
        if (pFwlDefnRuleTable->u4TrInstance == (UINT4) idx1)
        {
            OctetStrIdx.i4_Length = STRLEN (pFwlDefnRuleTable->au1RuleName);
            MEMCPY (OctetStrIdx.pu1_OctetList, pFwlDefnRuleTable->au1RuleName,
                    OctetStrIdx.i4_Length);
            if (nmhValidateIndexInstanceFwlDefnRuleTable (&OctetStrIdx) !=
                SNMP_SUCCESS)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Rule entry does not exist. \r\n");
                value->out_int = 0;
                return SNMP_FAILURE;
            }
            break;
        }
    }

    if (pFwlDefnRuleTable == NULL)
    {
        return SNMP_FAILURE;
    }

    pNode = pFwlDefnRuleTable;

    u4MakeActive = 0;
    if ((((pNode->u4ValMask) & (FWL_DEFN_RULE_TABLE_VAL_MASK)) ==
         (FWL_DEFN_RULE_TABLE_VAL_MASK)) && (STRCMP (name, "RowStatus") != 0))
    {
        u4val = NOT_IN_SERVICE;

        i4rc =
            MynmhTest (*Join
                       (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "Exit: Test failed for FwlRuleRowStatus\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "Exit: Set failed for FwlRuleRowStatus\n");
            return SNMP_FAILURE;
        }
        u4MakeActive = 1;
    }
    i4rc =
        TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnRuleTable (name,
                                                                      value,
                                                                      pNode);
    if (i4rc == TR_NOT_MAND_PARAM)
    {
        i4rc =
            TrSetMand_InternetGatewayDevice_Firewall_fwlDefnRuleTable (name,
                                                                       value,
                                                                       pNode);
        if (i4rc == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else if (i4rc == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((u4MakeActive) && (STRCMP (name, "RowStatus") != 0)
        && (pNode->u4RowStatus == ACTIVE))
    {
        u4val = ACTIVE;
        OctetStrIdx.i4_Length = STRLEN (pNode->au1RuleName);
        MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1RuleName,
                OctetStrIdx.i4_Length);
        i4rc =
            MynmhTest (*Join
                       (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "Exit: Test failed for FwlRuleRowStatus\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "Exit: Set failed for FwlRuleRowStatus\n");
            return SNMP_FAILURE;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSet_IGD_Firewall_fwlDefnRuleTable\n");
    return i4rc;
}

/************************************************************************
 *  Function Name   : TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnRuleTable
 *  Description     : Set function for non-mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tFwlDefnRuleTable entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnRuleTable (char *name,
                                                              ParameterValue *
                                                              value,
                                                              tFwlDefnRuleTable
                                                              * pNode)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    INT4                i4rc = 0;
    UINT4               u4val = 0;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetNonMand_IGD_Firewall_fwlDefnRuleTable\n");
    OctetStrIdx.i4_Length = STRLEN (pNode->au1RuleName);
    MEMCPY (OctetStrIdx.pu1_OctetList,
            pNode->au1RuleName, OctetStrIdx.i4_Length);

    if (STRCMP (name, "RowStatus") == 0)
    {
        if (((pNode->u4ValMask) & (FWL_DEFN_RULE_TABLE_VAL_MASK)) ==
            (FWL_DEFN_RULE_TABLE_VAL_MASK))
        {
            if (value->in_uint == TR_FWL_TRUE)
            {
                pNode->u4RowStatus = u4val = TR_ENABLE_ROWSTATUS;
            }
            else
            {
                pNode->u4RowStatus = u4val = TR_DISABLE_ROWSTATUS;
            }
            i4rc =
                MynmhTest (*Join
                           (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Test failed for FwlRuleRowStatus\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "Set failed for FwlRuleRowStatus\n");
                return SNMP_FAILURE;
            }
            TR69_TRC (TR69_DBG_TRC, "Exit: Set Success for FwlRuleRowStatus\n");
            return SNMP_SUCCESS;
        }
        else
        {
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnRuleTable (name,
                                                                      pNode,
                                                                      value,
                                                                      !TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnRuleTable\n");
            return SNMP_SUCCESS;
        }

    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetNonMand_IGD_Firewall_fwlDefnRuleTable - Mand Param\n");
    return TR_NOT_MAND_PARAM;
}

/************************************************************************
 *  Function Name   : TrSetMand_InternetGatewayDevice_Firewall_fwlDefnRuleTable
 *  Description     : Set function for mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tfwlDefnRuleTable entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetMand_InternetGatewayDevice_Firewall_fwlDefnRuleTable (char *name,
                                                           ParameterValue *
                                                           value,
                                                           tFwlDefnRuleTable *
                                                           pNode)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    INT4                i4rc = 0;
    tFwlDefnRuleTable   NewNode;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    OctetStrIdx.i4_Length = STRLEN (pNode->au1RuleName);
    MEMCPY (OctetStrIdx.pu1_OctetList,
            pNode->au1RuleName, OctetStrIdx.i4_Length);

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetMand_IGD_Firewall_fwlDefnRuleTable\n");

    if (pNode->u4RowStatus == 0)
    {
        TrSetAll_InternetGatewayDevice_Firewall_fwlDefnRuleTable (name, pNode,
                                                                  value,
                                                                  !TR_DO_SNMP_SET);
        if (((pNode->u4ValMask) & (FWL_DEFN_RULE_TABLE_VAL_MASK)) ==
            (FWL_DEFN_RULE_TABLE_VAL_MASK))
        {
            i4rc =
                MynmhSet (*Join
                          (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) CREATE_AND_WAIT);
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnRuleTable (NULL,
                                                                      pNode,
                                                                      value,
                                                                      TR_DO_SNMP_SET);
            i4rc =
                MynmhSet (*Join
                          (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) ACTIVE);

        }
    }
    else
    {
        MEMCPY (&NewNode, pNode, sizeof (*pNode));
        i4rc =
            MynmhTest (*Join
                       (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) DESTROY);

        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: Test for DESTROY failed for RuleName:%s\n",
                           OctetStrIdx.pu1_OctetList);
            return SNMP_FAILURE;
        }
        i4rc =
            MynmhSet (*Join
                      (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);

        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: Set for DESTROY failed for RuleName:%s\n",
                           OctetStrIdx.pu1_OctetList);
            return SNMP_FAILURE;
        }
        if (STRCMP (name, "RuleName") == 0)
        {
            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStrIdx.pu1_OctetList, value->in_cval,
                    OctetStrIdx.i4_Length);
        }

        i4rc =
            MynmhTest (*Join
                       (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) CREATE_AND_WAIT);

        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: Test for CREATE_AND_WAIT failed for RuleName:%s\n",
                           OctetStrIdx.pu1_OctetList);

            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (pNode->au1RuleName);
            MEMCPY (OctetStrIdx.pu1_OctetList,
                    pNode->au1RuleName, OctetStrIdx.i4_Length);

            MynmhSet (*Join
                      (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) CREATE_AND_WAIT);

            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnRuleTable (NULL,
                                                                      pNode,
                                                                      value,
                                                                      TR_DO_SNMP_SET);
            return SNMP_FAILURE;
        }
        i4rc =
            MynmhSet (*Join
                      (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) CREATE_AND_WAIT);

        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: Set for CREATE_AND_WAIT failed for RuleName:%s\n",
                           OctetStrIdx.pu1_OctetList);

            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (pNode->au1RuleName);
            MEMCPY (OctetStrIdx.pu1_OctetList,
                    pNode->au1RuleName, OctetStrIdx.i4_Length);

            i4rc =
                MynmhSet (*Join
                          (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) CREATE_AND_WAIT);

            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnRuleTable (NULL,
                                                                      pNode,
                                                                      value,
                                                                      TR_DO_SNMP_SET);
            return SNMP_FAILURE;
        }
        /* First update NewNode with the new value */
        /* Then SET all the values for the new row */
        i4rc =
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnRuleTable (name,
                                                                      &NewNode,
                                                                      value,
                                                                      !TR_DO_SNMP_SET);
        i4rc =
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnRuleTable (NULL,
                                                                      &NewNode,
                                                                      value,
                                                                      TR_DO_SNMP_SET);
        if (i4rc == SNMP_FAILURE)
        {
            MynmhSet (*Join
                      (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);

            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (pNode->au1RuleName);
            MEMCPY (OctetStrIdx.pu1_OctetList,
                    pNode->au1RuleName, OctetStrIdx.i4_Length);

            MynmhSet (*Join
                      (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) CREATE_AND_WAIT);

            i4rc =
                TrSetAll_InternetGatewayDevice_Firewall_fwlDefnRuleTable (NULL,
                                                                          pNode,
                                                                          value,
                                                                          TR_DO_SNMP_SET);
        }
        else
        {
            MEMCPY (pNode, &NewNode, sizeof (NewNode));
        }

        MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
        OctetStrIdx.i4_Length = STRLEN (pNode->au1RuleName);
        MEMCPY (OctetStrIdx.pu1_OctetList,
                pNode->au1RuleName, OctetStrIdx.i4_Length);
        if ((STRCMP (name, "RowStatus") != 0) && (pNode->u4RowStatus == ACTIVE))
        {
            i4rc =
                MynmhSet (*Join
                          (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) ACTIVE);
        }

    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetMand_IGD_Firewall_fwlDefnRuleTable\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrSetAll_InternetGatewayDevice_Firewall_fwlDefnRuleTable
 *  Description     : Set function for all objects of a table.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tFwlDefnRuleTable entry.
 *                    bTrDoSnmpSet - Indicates whether SNMP Set is to be done.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetAll_InternetGatewayDevice_Firewall_fwlDefnRuleTable (char *name,
                                                          tFwlDefnRuleTable *
                                                          pNode,
                                                          ParameterValue *
                                                          value,
                                                          BOOL1 bTrDoSnmpSet)
{
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    INT4                i4rc = 0;
    UINT4               u4val = 0;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetAll_IGD_Firewall_fwlDefnRuleTable\n");
    OctetStrIdx.i4_Length = STRLEN (pNode->au1RuleName);
    MEMCPY (OctetStrIdx.pu1_OctetList,
            pNode->au1RuleName, OctetStrIdx.i4_Length);

    if (!name || STRCMP (name, "RuleName") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            MEMSET (pNode->au1RuleName, '\0', 36);
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMSET (OctetStr.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1RuleName, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            pNode->u4ValMask |= FWL_RULE_RULE_NAME_MASK;
            return SNMP_SUCCESS;
        }
    }
    if (!name || STRCMP (name, "FilterSet") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            MEMSET (pNode->au1FilterSet, '\0', 36);
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMSET (OctetStr.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1FilterSet, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            pNode->u4ValMask |= FWL_RULE_FILTER_SET_MASK;
            return SNMP_SUCCESS;
        }
        OctetStr.i4_Length = STRLEN (pNode->au1FilterSet);
        MEMSET (OctetStr.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
        MEMCPY (OctetStr.pu1_OctetList, pNode->au1FilterSet,
                OctetStr.i4_Length);
        i4rc =
            MynmhTest (*Join
                       (FwlRuleFilterSet, 12, FwlDefnRuleTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                       (VOID *) &OctetStr);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "Exit: Test failed for FwlRuleFilterSet\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlRuleFilterSet, 12, FwlDefnRuleTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                      (VOID *) &OctetStr);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "Exit: Set failed for FwlRuleFilterSet\n");
            return SNMP_FAILURE;
        }
    }
    if (!name || STRCMP (name, "RowStatus") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->u4RowStatus = u4val = value->in_uint;
            pNode->u4ValMask |= FWL_RULE_ROW_STATUS_MASK;
            return SNMP_SUCCESS;
        }
        u4val = pNode->u4RowStatus;
        i4rc =
            MynmhTest (*Join
                       (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "Exit: Test failed for FwlRuleRowStatus\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "Exit: Set failed for FwlRuleRowStatus\n");
            return SNMP_FAILURE;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetAll_IGD_Firewall_fwlDefnRuleTable\n");
    return SNMP_SUCCESS;
}

/**********************End of fwlDefnRuleTable********************/

/*************************fwlDefnAclTable*************************/

/*****************************************************************
 *  Function Name   : TrScan_FwlDefnACLTable
 *  Description     : Scans and detects available Firewall ACL.
 *
 *  Input           : none.
 *  Output          : None
 *  Returns         : 0/-1
 ******************************************************************/
UINT4
TrScan_FwlDefnAclTable ()
{
    tSNMP_OCTET_STRING_TYPE FwlAclName;
    tSNMP_OCTET_STRING_TYPE NextFwlAclName;
    UINT1               au1FwlAclName[MAX_OCTETSTRING_SIZE];
    UINT1               au1NextFwlAclName[MAX_OCTETSTRING_SIZE];
    tFwlDefnAclTable   *pFwlDefnAclTable = NULL;
    tFwlDefnAclTable   *pNextFwlDefnAclTable = NULL;
    INT4                i4FwlAclIfIndex = 0;
    INT4                i4NextFwlAclIfIndex = 0;
    INT4                i4FwlAclDirection = 0;
    INT4                i4NextFwlAclDirection = 0;
    UINT4               u4NewNode = 0;
    INT4                i4rc = 0;
    UINT4               u4TrIdx = 0;
    UINT4              *ptr = NULL;
    UINT4               u4Size = 0;

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrScan_FwlDefnAclTable\n");

    MEMSET (&FwlAclName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextFwlAclName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (au1FwlAclName, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (au1NextFwlAclName, 0, MAX_OCTETSTRING_SIZE);

    FwlAclName.pu1_OctetList = au1FwlAclName;
    NextFwlAclName.pu1_OctetList = au1NextFwlAclName;

    i4rc =
        nmhGetFirstIndexFwlDefnAclTable (&i4FwlAclIfIndex, &FwlAclName,
                                         &i4FwlAclDirection);
    if (i4rc == SNMP_FAILURE)
    {
        TR69_TRC (TR69_DBG_TRC, "GetFirstIndex failed for FwlDefnAclTable\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_FwlDefnAclTable\n");
        return (OSIX_FAILURE);
    }
    MEMCPY (NextFwlAclName.pu1_OctetList, FwlAclName.pu1_OctetList,
            FwlAclName.i4_Length);
    NextFwlAclName.i4_Length = FwlAclName.i4_Length;
    i4NextFwlAclIfIndex = i4FwlAclIfIndex;
    i4NextFwlAclDirection = i4FwlAclDirection;

    UTL_SLL_OFFSET_SCAN (&FwlDefnAclTableList, pFwlDefnAclTable,
                         pNextFwlDefnAclTable, tFwlDefnAclTable *)
    {
        pFwlDefnAclTable->u4Visited = 0;
    }
    while (1)
    {
        pFwlDefnAclTable = NULL;
        u4NewNode = 1;

        /* Do not instantiate if already present */
        UTL_SLL_OFFSET_SCAN (&FwlDefnAclTableList, pFwlDefnAclTable,
                             pNextFwlDefnAclTable, tFwlDefnAclTable *)
        {
            if ((STRCMP
                 (pFwlDefnAclTable->au1AclName,
                  NextFwlAclName.pu1_OctetList) == 0)
                && (pFwlDefnAclTable->i4IfIndex == i4NextFwlAclIfIndex)
                && (pFwlDefnAclTable->i4Direction == i4NextFwlAclDirection))
            {
                u4NewNode = 0;
                pFwlDefnAclTable->u4Visited = 1;
                break;
            }
        }

        /* Instantiate if this is a new instance */
        if (u4NewNode)
        {
            u4TrIdx = 0;
            i4rc =
                addObjectIntern
                ("InternetGatewayDevice.Firewall.fwlDefnAclTable",
                 (int *) &u4TrIdx);

            if (i4rc == 0)
            {
                pFwlDefnAclTable =
                    TrAdd_FwlDefnAclTable (u4TrIdx, &NextFwlAclName,
                                           i4NextFwlAclIfIndex,
                                           i4NextFwlAclDirection);
                if (pFwlDefnAclTable == NULL)
                {
                    TR69_TRC (TR69_DBG_TRC, "FwlDefnAclTable is Null\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrScan_FwlDefnAclTable\n");
                    return OSIX_FAILURE;
                }
                pFwlDefnAclTable->u4Visited = 1;
            }
            else
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "addObjectIntern failed, rc = %d\n", i4rc);
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrScan_FwlDefnAclTable\n");
            }
        }

        /* get the next elem */
        MEMCPY (FwlAclName.pu1_OctetList, NextFwlAclName.pu1_OctetList,
                NextFwlAclName.i4_Length);
        FwlAclName.i4_Length = NextFwlAclName.i4_Length;

        i4FwlAclIfIndex = i4NextFwlAclIfIndex;
        i4FwlAclDirection = i4NextFwlAclDirection;

        i4rc =
            nmhGetNextIndexFwlDefnAclTable (i4FwlAclIfIndex,
                                            &i4NextFwlAclIfIndex, &FwlAclName,
                                            &NextFwlAclName, i4FwlAclDirection,
                                            &i4NextFwlAclDirection);
        if (i4rc == SNMP_FAILURE)
        {
            break;
        }
    }

    /* Incase if an entry is deleted via CLI or SNMP, in order 
       to synchronize TR database locate the node and initialize 
       the structure  elements to zero */
    UTL_SLL_OFFSET_SCAN (&FwlDefnAclTableList, pFwlDefnAclTable,
                         pNextFwlDefnAclTable, tFwlDefnAclTable *)
    {
        if ((pFwlDefnAclTable->u4Visited == 0) &&
            (pFwlDefnAclTable->u4ValMask == FWL_DEFN_ACL_TABLE_VAL_MASK))
        {
            ptr = &(pFwlDefnAclTable->u4ValMask);
            u4Size =
                sizeof (tFwlDefnAclTable) - (sizeof (UINT4) +
                                             sizeof (tTMO_SLL_NODE));
            MEMSET (ptr, 0, u4Size);

        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_FwlDefnAclTable\n");
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrInitInternetGatewayDevice_Firewall_fwlDefnAclTable
 *  Description     : Creates a TR Instance for fwlDefnAclTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrInitInternetGatewayDevice_Firewall_fwlDefnAclTable (int idx1)
{
    tFwlDefnAclTable   *pFwlDefnAclTable = NULL;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrInit_IGD_Firewall_fwlDefnAclTable\n");

    if (idx1 == 0)
    {
        TR69_TRC (TR69_DBG_TRC, "TrInstance is Zero\n");

        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrInit_IGD_Firewall_fwlDefnAclTable\n");
        return SNMP_SUCCESS;
    }

    pFwlDefnAclTable = TrAdd_FwlDefnAclTable (idx1, NULL, 0, 0);

    if (pFwlDefnAclTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "FwlDefnAclTable is Null\n");

        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrInit_IGD_Firewall_fwlDefnAclTable\n");
        return SNMP_FAILURE;
    }

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrInit_IGD_Firewall_fwlDefnAclTable\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrAdd_FwlDefnAclTable
 *  Description     : Adds a Firewall Acl node to TR69.
 *
 *  Input           : u4TrIdx - Tr Instance
 *                    OctetStrIdx - Index.
 *                    i4Idx0 -Index
 *                    i4Idx1 - Index
 *  Output          : None
 *  Returns         : pfwlUrlFilterTable - Allocated UrlFilter node.
 ************************************************************************/
tFwlDefnAclTable   *
TrAdd_FwlDefnAclTable (UINT4 u4TrIdx, tSNMP_OCTET_STRING_TYPE * pOctetStrIdx,
                       INT4 i4Idx0, INT4 i4Idx1)
{
    tFwlDefnAclTable   *pFwlDefnAclTable = NULL;
    tFwlDefnAclTable   *pNextFwlDefnAclTable = NULL;

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrAdd_FwlDefnAclTable\n");
    if ((pOctetStrIdx == NULL) && (i4Idx0 == 0) && (i4Idx1 == 0))
    {
        pFwlDefnAclTable =
            MEM_MALLOC (sizeof (tFwlDefnAclTable), tFwlDefnAclTable);
        if (pFwlDefnAclTable == NULL)
        {
            TR69_TRC (TR69_DBG_TRC, "Memory Allocation - Failed\n");

            TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_FwlDefnAclTable\n");
            return NULL;
        }
        MEMSET (pFwlDefnAclTable, 0, sizeof (tFwlDefnAclTable));

        pFwlDefnAclTable->u4TrInstance = u4TrIdx;
        TMO_SLL_Add (&FwlDefnAclTableList, &pFwlDefnAclTable->Link);

        TR69_TRC (TR69_DBG_TRC, "Node Allocation - Success\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_FwlDefnAclTable\n");
        return pFwlDefnAclTable;
    }
    UTL_SLL_OFFSET_SCAN (&FwlDefnAclTableList, pFwlDefnAclTable,
                         pNextFwlDefnAclTable, tFwlDefnAclTable *)
    {
        if (pFwlDefnAclTable->u4TrInstance == u4TrIdx)
        {
            MEMCPY (pFwlDefnAclTable->au1AclName, pOctetStrIdx->pu1_OctetList,
                    pOctetStrIdx->i4_Length);
            pFwlDefnAclTable->i4IfIndex = i4Idx0;
            pFwlDefnAclTable->i4Direction = i4Idx1;
            pFwlDefnAclTable->u4ValMask = FWL_DEFN_ACL_TABLE_VAL_MASK;
            TR69_TRC (TR69_DBG_TRC, "Row Creation - Success\n");
            break;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_FwlDefnAclTable\n");
    return pFwlDefnAclTable;
}

/************************************************************************
 *  Function Name   : TrDeleteInternetGatewayDevice_Firewall_fwlDefnAclTable
 *  Description     : Delete TR Instance for fwlDefnAclTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrDeleteInternetGatewayDevice_Firewall_fwlDefnAclTable (int idx1)
{
    tFwlDefnAclTable   *pFwlDefnAclTable = NULL;
    tFwlDefnAclTable   *pNextFwlDefnAclTable = NULL;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    UINT4               u4Idx0;
    UINT4               u4Idx1;
    INT4                i4rc = 0;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrDelete_IGD_Firewall_fwlDefnAclTable\n");

    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    UTL_SLL_OFFSET_SCAN (&FwlDefnAclTableList, pFwlDefnAclTable,
                         pNextFwlDefnAclTable, tFwlDefnAclTable *)
    {
        if (pFwlDefnAclTable->u4TrInstance == (UINT4) idx1)
        {
            OctetStrIdx.i4_Length = STRLEN (pFwlDefnAclTable->au1AclName);
            MEMCPY (OctetStrIdx.pu1_OctetList, pFwlDefnAclTable->au1AclName,
                    OctetStrIdx.i4_Length);
            u4Idx0 = (UINT4) pFwlDefnAclTable->i4IfIndex;
            u4Idx1 = (UINT4) pFwlDefnAclTable->i4Direction;

            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Deleting ACL Table entry TrInstance:%d String:%s\n",
                           pFwlDefnAclTable->u4TrInstance,
                           pFwlDefnAclTable->au1AclName);
            if ((nmhValidateIndexInstanceFwlDefnAclTable
                 (u4Idx0, &OctetStrIdx, u4Idx1)) == SNMP_SUCCESS)
            {
                /* After validating the index destory both ACL and Rule */
                i4rc = MynmhSet (*Join
                                 (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3,
                                  u4Idx0, &OctetStrIdx, u4Idx1),
                                 SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "Set for Destroy failed for FwlAclRowStatus \n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDelete_IGD_Firewall_fwlDefnAclTable\n");
                    return SNMP_FAILURE;
                }

                i4rc =
                    MynmhSet (*Join
                              (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                               &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                              (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "Set for Destroy failed for FwlRuleRowStatus\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDelete_IGD_Firewall_fwlDefnAclTable\n");
                    return SNMP_FAILURE;
                }
            }
            TMO_SLL_Delete (&FwlDefnAclTableList,
                            (tTMO_SLL_NODE *) pFwlDefnAclTable);
            MEM_FREE (pFwlDefnAclTable);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrDelete_IGD_Firewall_fwlDefnAclTable\n");
            return SNMP_SUCCESS;
        }
    }
    /* Instance not present - return SNMP_FAILURE  */
    if (pFwlDefnAclTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "FwlDefnAclTable is Null\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrDelete_IGD_Firewall_fwlDefnAclTable\n");
        return SNMP_FAILURE;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrDelete_IGD_Firewall_fwlDefnAclTable\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_Firewall_fwlDefnAclTable
 *  Description     : Get function for InternetGatewayDevice_Firewall_fwlDefnAclTable
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrGetInternetGatewayDevice_Firewall_fwlDefnAclTable (char *name, int idx1,
                                                     ParameterValue * value)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT4               u4val = 0;
    UINT4               u4Idx0 = 0;
    UINT4               u4Idx1 = 0;
    UINT4              *ptr = NULL;
    UINT4               u4Size = 0;
    INT4                i4rc = 0;
    tFwlDefnAclTable   *pFwlDefnAclTable = NULL;
    tFwlDefnAclTable   *pNextFwlDefnAclTable = NULL;
    tFwlDefnAclTable   *pNode = NULL;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrGet_IGD_Firewall_fwlDefnAclTable\n");

    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    UTL_SLL_OFFSET_SCAN (&FwlDefnAclTableList, pFwlDefnAclTable,
                         pNextFwlDefnAclTable, tFwlDefnAclTable *)
    {
        if (pFwlDefnAclTable->u4TrInstance == (UINT4) idx1)
        {
            OctetStrIdx.i4_Length = STRLEN (pFwlDefnAclTable->au1AclName);
            MEMCPY (OctetStrIdx.pu1_OctetList, pFwlDefnAclTable->au1AclName,
                    OctetStrIdx.i4_Length);
            u4Idx0 = (UINT4) pFwlDefnAclTable->i4IfIndex;
            u4Idx1 = (UINT4) pFwlDefnAclTable->i4Direction;
            if ((pFwlDefnAclTable->u4ValMask) == FWL_DEFN_ACL_TABLE_VAL_MASK)
            {
                if ((nmhValidateIndexInstanceFwlDefnAclTable
                     (u4Idx0, &OctetStrIdx, u4Idx1)) == SNMP_FAILURE)
                {
                    ptr = &(pFwlDefnAclTable->u4ValMask);
                    u4Size =
                        sizeof (tFwlDefnAclTable) - (sizeof (UINT4) +
                                                     sizeof (tTMO_SLL_NODE));
                    MEMSET (ptr, 0, u4Size);

                    TR69_TRC_ARG1 (TR69_DBG_TRC,
                                   "nmhValidate failed for TrInstance:%d\n",
                                   pFwlDefnAclTable->u4TrInstance);
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrGet_IGD_Firewall_fwlDefnAclTable\n");
                }
            }
            break;
        }
    }

    if (pFwlDefnAclTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "FwlDefnAclTable is Null\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrGet_IGD_Firewall_fwlDefnAclTable\n");
        return -1;
    }

    pNode = pFwlDefnAclTable;

    if (STRCMP (name, "Direction") == 0)
    {
        value->out_int = pNode->i4Direction;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlAclDirection ret %d\n", value->out_int);
        return OK;
    }
    else if (STRCMP (name, "FragAction") == 0)
    {
        if (((pNode->u4ValMask) & (FWL_DEFN_ACL_TABLE_VAL_MASK)) ==
            FWL_DEFN_ACL_TABLE_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FwlAclFragAction, 12, FwlDefnAclTableINDEX, 3,
                           u4Idx0, &OctetStrIdx, u4Idx1),
                          SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Get failed for FwlAclFragAction\n");
                value->out_int = 0;
                return OK;
            }
            value->out_int = u4val;
            pNode->i4FragAction = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlAclFragAction ret %d\n", value->out_int);
            return OK;
        }
        else
        {
            value->out_int = pNode->i4FragAction;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlAclFragAction ret %d\n", value->out_int);
            return OK;
        }
    }
    else if (STRCMP (name, "LogTrigger") == 0)
    {
        if (((pNode->u4ValMask) & (FWL_DEFN_ACL_TABLE_VAL_MASK)) ==
            FWL_DEFN_ACL_TABLE_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FwlAclLogTrigger, 12, FwlDefnAclTableINDEX, 3,
                           u4Idx0, &OctetStrIdx, u4Idx1),
                          SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Get failed for FwlAclLogTrigger\n");
                value->out_int = 0;
                return OK;
            }
            value->out_int = u4val;
            pNode->i4LogTrigger = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlAclLogTrigger ret %d\n", value->out_int);
            return OK;
        }
        else
        {
            value->out_int = pNode->i4LogTrigger;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlAclLogTrigger ret %d\n", value->out_int);
            return OK;
        }
    }
    else if (STRCMP (name, "IfIndex") == 0)
    {
        value->out_int = pNode->i4IfIndex;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FwlAclIfIndex ret %d\n",
                       value->out_int);
        return OK;
    }
    else if (STRCMP (name, "SequenceNumber") == 0)
    {
        if (((pNode->u4ValMask) & (FWL_DEFN_ACL_TABLE_VAL_MASK)) ==
            FWL_DEFN_ACL_TABLE_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FwlAclSequenceNumber, 12, FwlDefnAclTableINDEX, 3,
                           u4Idx0, &OctetStrIdx, u4Idx1),
                          SNMP_DATA_TYPE_INTEGER32, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Get failed for FwlAclSequenceNumber\n");
                value->out_int = 0;
                return OK;
            }
            value->out_int = u4val;
            pNode->i4SequenceNumber = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlAclSequenceNumber ret %d\n",
                           value->out_int);
            return OK;
        }
        else
        {
            value->out_int = pNode->i4SequenceNumber;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlAclSequenceNumber ret %d\n",
                           value->out_int);
            return OK;
        }
    }
    else if (STRCMP (name, "Action") == 0)
    {
        if (((pNode->u4ValMask) & (FWL_DEFN_ACL_TABLE_VAL_MASK)) ==
            FWL_DEFN_ACL_TABLE_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FwlAclAction, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                           &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                          &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "Exit: Get failed for FwlAclAction\n");
                value->out_int = 0;
                return OK;
            }
            value->out_int = u4val;
            pNode->i4Action = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlAclAction ret %d\n", value->out_int);
            return OK;
        }
        else
        {
            value->out_int = pNode->i4Action;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlAclAction ret %d\n", value->out_int);
            return OK;
        }
    }
    else if (STRCMP (name, "AclName") == 0)
    {
        value->out_cval = pNode->au1AclName;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlAclAclName ret %s\n", value->out_cval);
        return OK;
    }
    else if (STRCMP (name, "RowStatus") == 0)
    {
        if (((pNode->u4ValMask) & (FWL_DEFN_ACL_TABLE_VAL_MASK)) ==
            FWL_DEFN_ACL_TABLE_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                           &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                          &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Get failed for FwlAclRowStatus\n");
                value->out_int = 0;
                return OK;
            }
            if (u4val == TR_DISABLE_ROWSTATUS)
            {
                value->out_uint = 0;
                pNode->u4RowStatus = TR_DISABLE_ROWSTATUS;
            }
            else
            {
                value->out_uint = 1;
                pNode->u4RowStatus = TR_ENABLE_ROWSTATUS;
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlAclRowStatus ret %d\n", value->out_int);
            return OK;
        }
        else
        {
            if (pNode->u4RowStatus == TR_ENABLE_ROWSTATUS)
            {
                value->out_uint = 1;
            }
            else
            {
                value->out_uint = 0;
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlAclRowStatus ret %d\n", value->out_int);
            return OK;
        }
    }
    else if (STRCMP (name, "FilterName") == 0)
    {
        if (((pNode->u4ValMask) & (FWL_DEFN_ACL_TABLE_VAL_MASK)) ==
            FWL_DEFN_ACL_TABLE_VAL_MASK)
        {
            i4rc =
                MynmhGet (*Join
                          (FwlRuleFilterSet, 12, FwlDefnRuleTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM, &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Get failed for FwlAclFilterComb\n");
                return OK;
            }
            MEMCPY (pNode->au1FilterComb, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);

            value->out_cval = pNode->au1FilterComb;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlAclFilterComb ret %s\n", value->out_cval);
            return OK;
        }
        else
        {
            value->out_cval = pNode->au1FilterComb;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlRuleFilterSet ret %s\n", value->out_cval);
            return OK;
        }

    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrGet_IGD_Firewall_fwlDefnAclTable\n");
    return i4rc;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_Firewall_fwlDefnAclTable
 *  Description     : Get function for InternetGatewayDevice_Firewall_fwlDefnAclTable
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrSetInternetGatewayDevice_Firewall_fwlDefnAclTable (char *name, int idx1,
                                                     ParameterValue * value)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT4               u4val = 0;
    UINT4               u4MakeActive = 0;
    UINT4               u4Idx0 = 0;
    UINT4               u4Idx1 = 0;
    UINT4              *ptr = NULL;
    UINT4               u4Size = 0;
    INT4                i4rc = 0;
    INT4                i4RetVal = 0;

    tFwlDefnAclTable   *pFwlDefnAclTable = NULL;
    tFwlDefnAclTable   *pNextFwlDefnAclTable = NULL;
    tFwlDefnAclTable   *pNode = NULL;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSet_IGD_Firewall_fwlDefnAclTable\n");

    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    UTL_SLL_OFFSET_SCAN (&FwlDefnAclTableList, pFwlDefnAclTable,
                         pNextFwlDefnAclTable, tFwlDefnAclTable *)
    {
        if (pFwlDefnAclTable->u4TrInstance == (UINT4) idx1)
        {
            OctetStrIdx.i4_Length = STRLEN (pFwlDefnAclTable->au1AclName);
            MEMCPY (OctetStrIdx.pu1_OctetList, pFwlDefnAclTable->au1AclName,
                    OctetStrIdx.i4_Length);
            u4Idx0 = (UINT4) pFwlDefnAclTable->i4IfIndex;
            u4Idx1 = (UINT4) pFwlDefnAclTable->i4Direction;
            if (pFwlDefnAclTable->u4ValMask == FWL_DEFN_ACL_TABLE_VAL_MASK)
            {
                if ((nmhValidateIndexInstanceFwlDefnAclTable
                     (u4Idx0, &OctetStrIdx, u4Idx1)) == SNMP_FAILURE)
                {
                    ptr = &(pFwlDefnAclTable->u4ValMask);
                    u4Size =
                        sizeof (tFwlDefnAclTable) - (sizeof (UINT4) +
                                                     sizeof (tTMO_SLL_NODE));
                    MEMSET (ptr, 0, u4Size);
                    TR69_TRC_ARG1 (TR69_DBG_TRC,
                                   "nmhValidate failed for TrInstance:%d\n",
                                   pFwlDefnAclTable->u4TrInstance);
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrSet_IGD_Firewall_fwlDefnAclTable\n");
                    return SNMP_FAILURE;
                }
            }
            break;
        }
    }

    if (pFwlDefnAclTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "FwlDefnAclTable is Null\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrSet_IGD_Firewall_fwlDefnAclTable\n");
        return SNMP_FAILURE;
    }

    pNode = pFwlDefnAclTable;

    u4MakeActive = 0;
    if (((pNode->u4ValMask & FWL_DEFN_ACL_TABLE_VAL_MASK) ==
         FWL_DEFN_ACL_TABLE_VAL_MASK) && (STRCMP (name, "RowStatus") != 0))
    {
        u4val = NOT_IN_SERVICE;

        i4rc =
            MynmhTest (*Join
                       (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                        &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlAclRowStatus\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                       &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Set failed for FwlAclRowStatus \n");
            return SNMP_FAILURE;
        }
        u4MakeActive = 1;
    }
    i4RetVal =
        TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnAclTable (name,
                                                                     value,
                                                                     pNode);
    if (i4RetVal == TR_NOT_MAND_PARAM)
    {
        i4RetVal =
            TrSetMand_InternetGatewayDevice_Firewall_fwlDefnAclTable (name,
                                                                      value,
                                                                      pNode);
    }

    if ((u4MakeActive) && (STRCMP (name, "RowStatus") != 0)
        && (pNode->u4RowStatus == ACTIVE))
    {
        u4val = ACTIVE;
        OctetStrIdx.i4_Length = STRLEN (pNode->au1AclName);
        MEMCPY (OctetStrIdx.pu1_OctetList,
                pNode->au1AclName, OctetStrIdx.i4_Length);
        u4Idx0 = (UINT4) pNode->i4IfIndex;
        u4Idx1 = (UINT4) pNode->i4Direction;

        i4rc =
            MynmhTest (*Join
                       (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                        &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlAclRowStatus \n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                       &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Set failed for FwlAclRowStatus \n");
            return SNMP_FAILURE;
        }
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrSet_IGD_Firewall_fwlDefnAclTable - Failed\n");
        return SNMP_FAILURE;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSet_IGD_Firewall_fwlDefnAclTable - Success\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnAclTable
 *  Description     : Set function for non-mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tFwlDefnAclTable entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnAclTable (char *name,
                                                             ParameterValue *
                                                             value,
                                                             tFwlDefnAclTable *
                                                             pNode)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    INT4                i4rc = 0;
    UINT4               u4Idx0 = 0;
    UINT4               u4val = 0;
    UINT4               u4Idx1 = 0;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetNonMand_IGD_Firewall_fwlDefnAclTable\n");

    OctetStrIdx.i4_Length = STRLEN (pNode->au1AclName);
    MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1AclName,
            OctetStrIdx.i4_Length);
    u4Idx0 = (UINT4) pNode->i4IfIndex;
    u4Idx1 = (UINT4) pNode->i4Direction;

    if (STRCMP (name, "LogTrigger") == 0)
    {
        if ((pNode->u4ValMask & FWL_DEFN_ACL_TABLE_VAL_MASK) ==
            FWL_DEFN_ACL_TABLE_VAL_MASK)
        {
            pNode->i4LogTrigger = u4val = value->in_int;
            i4rc =
                MynmhTest (*Join
                           (FwlAclLogTrigger, 12, FwlDefnAclTableINDEX, 3,
                            u4Idx0, &OctetStrIdx, u4Idx1),
                           SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Test failed for FwlAclLogTrigger\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlAclLogTrigger, 12, FwlDefnAclTableINDEX, 3,
                           u4Idx0, &OctetStrIdx, u4Idx1),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Set failed for FwlAclLogTrigger\n");
                return SNMP_FAILURE;
            }
            pNode->u4NonMandValMask |= FWL_ACL_LOG_TRIGGER_MASK;
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnAclTable - Success\n");
            return SNMP_SUCCESS;
        }
        else
        {
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnAclTable (name,
                                                                     pNode,
                                                                     value,
                                                                     !TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnAclTable\n");
            return SNMP_SUCCESS;
        }

    }
    else if (STRCMP (name, "RowStatus") == 0)
    {
        if ((pNode->u4ValMask & FWL_DEFN_ACL_TABLE_VAL_MASK) ==
            FWL_DEFN_ACL_TABLE_VAL_MASK)
        {
            if (value->in_uint == TR_FWL_TRUE)
            {
                pNode->u4RowStatus = u4val = TR_ENABLE_ROWSTATUS;
            }
            else
            {
                pNode->u4RowStatus = u4val = TR_DISABLE_ROWSTATUS;
            }
            i4rc =
                MynmhTest (*Join
                           (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3,
                            u4Idx0, &OctetStrIdx, u4Idx1),
                           SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Test failed for FwlAclRowStatus\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                           &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Set failed for FwlAclRowStatus\n");
                return SNMP_FAILURE;
            }
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnAclTable - Success\n");
            return SNMP_SUCCESS;
        }
        else
        {
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnAclTable (name,
                                                                     pNode,
                                                                     value,
                                                                     !TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnAclTable\n");
            return SNMP_SUCCESS;
        }

    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "-E- TrSetNonMand: TrSetNonMand_IGD_Firewall_fwlDefnAclTable - Mand Param\n");
    return TR_NOT_MAND_PARAM;
}

/************************************************************************
 *  Function Name   : TrSetMand_InternetGatewayDevice_Firewall_fwlDefnAclTable
 *  Description     : Set function for mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tfwlDefnAclTable entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetMand_InternetGatewayDevice_Firewall_fwlDefnAclTable (char *name,
                                                          ParameterValue *
                                                          value,
                                                          tFwlDefnAclTable *
                                                          pNode)
{
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    tSNMP_OCTET_STRING_TYPE RuleFilterSet;
    tFwlDefnAclTable    NewNode;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT1               au1RuleFilterSet[MAX_OCTETSTRING_SIZE];
    INT4                i4rc = 0;
    UINT4               u4val = 0;
    UINT4               u4Idx1 = 0;
    UINT4               u4Idx0 = 0;
    UINT4               u4Index0 = 0;
    UINT4               u4Index1 = 0;
    UINT4              *ptr = NULL;
    UINT4               u4Size = 0;
#ifdef TR69MSR_WANTED
    UINT1               u1Tr69MsrStatus = 0;
#endif

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&RuleFilterSet, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1RuleFilterSet, 0, MAX_OCTETSTRING_SIZE);

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");

    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;
    RuleFilterSet.pu1_OctetList = au1RuleFilterSet;

    if (pNode->u4ValMask != FWL_DEFN_ACL_TABLE_VAL_MASK)
    {
        TrSetAll_InternetGatewayDevice_Firewall_fwlDefnAclTable (name, pNode,
                                                                 value,
                                                                 !TR_DO_SNMP_SET);

        if (STRCMP (name, "FilterName") == 0)
        {
            MEMSET (pNode->au1FilterComb, '\0', 252);
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMSET (OctetStr.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1FilterComb, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            pNode->u4ValMask |= FWL_ACL_FILTER_COMB_MASK;
        }

        if ((pNode->u4ValMask & FWL_DEFN_ACL_TABLE_VAL_MASK) ==
            FWL_DEFN_ACL_TABLE_VAL_MASK)
        {
            TR69_TRC (TR69_DBG_TRC, "Creation of new row in ACL Table\n");

            /* Copy the SNMP indices (u4Idx0, &OctetStrIdx, u4Idx1) */

            OctetStrIdx.i4_Length = STRLEN (pNode->au1AclName);
            MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1AclName,
                    OctetStrIdx.i4_Length);
            u4Idx0 = (UINT4) pNode->i4IfIndex;
            u4Idx1 = (UINT4) pNode->i4Direction;

            /* Copy the Filter Combination provided for this ACL from pNode */
            OctetStr.i4_Length = STRLEN (pNode->au1FilterComb);
            MEMCPY (OctetStr.pu1_OctetList,
                    pNode->au1FilterComb, OctetStr.i4_Length);

            i4rc = MynmhGet (*Join
                             (FwlFilterRowStatus, 12, FwlDefnFilterTableINDEX,
                              1, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_SUCCESS)
            {
                ptr = &(pNode->u4ValMask);
                u4Size =
                    sizeof (tFwlDefnAclTable) - (sizeof (UINT4) +
                                                 sizeof (tTMO_SLL_NODE));
                MEMSET (ptr, 0, u4Size);
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Filter is available with the same ACL name - Failure\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
                return SNMP_FAILURE;
            }
            i4rc =
                MynmhGet (*Join
                          (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                /* Create the Rule if it does not exist */
                i4rc =
                    MynmhTest (*Join
                               (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                                &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                               (VOID *) CREATE_AND_WAIT);
                if (i4rc == SNMP_FAILURE)
                {
                    ptr = &(pNode->u4ValMask);
                    u4Size =
                        sizeof (tFwlDefnAclTable) - (sizeof (UINT4) +
                                                     sizeof (tTMO_SLL_NODE));
                    MEMSET (ptr, 0, u4Size);
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- TrSetMand: Test for CREATE_AND_WAIT failed"
                              " for RuleRowStatus\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
                    return SNMP_FAILURE;
                }
                i4rc =
                    MynmhSet (*Join
                              (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                               &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                              (VOID *) CREATE_AND_WAIT);
                if (i4rc == SNMP_FAILURE)
                {
                    ptr = &(pNode->u4ValMask);
                    u4Size =
                        sizeof (tFwlDefnAclTable) - (sizeof (UINT4) +
                                                     sizeof (tTMO_SLL_NODE));
                    MEMSET (ptr, 0, u4Size);
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- TrSetMand: Set failed for CREATE_AND_WAIT RuleRowStatus\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
                    return SNMP_FAILURE;
                }
                /* Create the FilterSet for the corresponding Rule */
                i4rc =
                    MynmhTest (*Join
                               (FwlRuleFilterSet, 12, FwlDefnRuleTableINDEX, 1,
                                &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                               (VOID *) &OctetStr);
                if (i4rc == SNMP_FAILURE)
                {
                    ptr = &(pNode->u4ValMask);
                    u4Size =
                        sizeof (tFwlDefnAclTable) - (sizeof (UINT4) +
                                                     sizeof (tTMO_SLL_NODE));
                    MEMSET (ptr, 0, u4Size);
                    MynmhSet (*Join
                              (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                               &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                              (VOID *) DESTROY);
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- TrSetMand: Test failed for RuleFilterSet\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
                    return SNMP_FAILURE;
                }

                i4rc =
                    MynmhSet (*Join
                              (FwlRuleFilterSet, 12, FwlDefnRuleTableINDEX, 1,
                               &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                              (VOID *) &OctetStr);
                if (i4rc == SNMP_FAILURE)
                {
                    ptr = &(pNode->u4ValMask);
                    u4Size =
                        sizeof (tFwlDefnAclTable) - (sizeof (UINT4) +
                                                     sizeof (tTMO_SLL_NODE));
                    MEMSET (ptr, 0, u4Size);
                    MynmhSet (*Join
                              (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                               &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                              (VOID *) DESTROY);
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- TrSetMand: Set failed for FwlRuleFilterSet\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
                    return SNMP_FAILURE;
                }

            }
            else                /*If the rule already exist then return SNMP_FAILURE */
            {
#ifdef TR69MSR_WANTED
                Tr69MsrGetRestoreStatus (&u1Tr69MsrStatus);
                if (u1Tr69MsrStatus == OSIX_TRUE)
                {
                    return SNMP_SUCCESS;
                }
#endif
                TR69_TRC (TR69_DBG_TRC, "-E- TrSetMand: Rule already exist\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) ACTIVE);
            if (i4rc == SNMP_FAILURE)
            {
                ptr = &(pNode->u4ValMask);
                u4Size =
                    sizeof (tFwlDefnAclTable) - (sizeof (UINT4) +
                                                 sizeof (tTMO_SLL_NODE));
                MEMSET (ptr, 0, u4Size);
                MynmhSet (*Join
                          (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) DESTROY);
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Setting to ACTIVE failed"
                          "for RuleRowStatus\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
                return SNMP_FAILURE;
            }
            /* Creation of new row in ACL table */
            i4rc =
                MynmhTest (*Join
                           (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3,
                            u4Idx0, &OctetStrIdx, u4Idx1),
                           SNMP_DATA_TYPE_INTEGER, (VOID *) CREATE_AND_WAIT);
            if (i4rc == SNMP_FAILURE)
            {
                ptr = &(pNode->u4ValMask);
                u4Size =
                    sizeof (tFwlDefnAclTable) - (sizeof (UINT4) +
                                                 sizeof (tTMO_SLL_NODE));
                MEMSET (ptr, 0, u4Size);
                MynmhSet (*Join
                          (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) DESTROY);
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Test for CREATE_AND_WAIT failed for AclRowStatus\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
                return SNMP_FAILURE;

            }

            i4rc =
                MynmhSet (*Join
                          (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                           &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) CREATE_AND_WAIT);
            if (i4rc == SNMP_FAILURE)
            {
                ptr = &(pNode->u4ValMask);
                u4Size =
                    sizeof (tFwlDefnAclTable) - (sizeof (UINT4) +
                                                 sizeof (tTMO_SLL_NODE));
                MEMSET (ptr, 0, u4Size);
                MynmhSet (*Join
                          (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) DESTROY);
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Set for CREATE_AND_WAIT failed for AclRowStatus\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
                return SNMP_FAILURE;

            }
            else
            {
                pNode->u4RowStatus = NOT_IN_SERVICE;
            }

            i4rc =
                TrSetAll_InternetGatewayDevice_Firewall_fwlDefnAclTable (NULL,
                                                                         pNode,
                                                                         value,
                                                                         TR_DO_SNMP_SET);
            if (i4rc == SNMP_FAILURE)
            {
                ptr = &(pNode->u4ValMask);
                u4Size =
                    sizeof (tFwlDefnAclTable) - (sizeof (UINT4) +
                                                 sizeof (tTMO_SLL_NODE));
                MEMSET (ptr, 0, u4Size);

                MynmhSet (*Join
                          (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                           &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) DESTROY);

                MynmhSet (*Join
                          (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) DESTROY);
                TR69_TRC (TR69_DBG_TRC, "-E- TrSetMand: SNMP Set Failed\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                           &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) ACTIVE);
            if (i4rc == SNMP_FAILURE)
            {
                ptr = &(pNode->u4ValMask);
                u4Size =
                    sizeof (tFwlDefnAclTable) - (sizeof (UINT4) +
                                                 sizeof (tTMO_SLL_NODE));
                MEMSET (ptr, 0, u4Size);

                MynmhSet (*Join
                          (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                           &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) DESTROY);

                MynmhSet (*Join
                          (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) DESTROY);
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Setting to ACTIVE failed for AclRowStatus\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
                return SNMP_FAILURE;
            }
            else
            {
                pNode->u4RowStatus = ACTIVE;
            }
        }
    }
    else
    {
        /*  Copy the SNMP indices (u4Idx0, &OctetStrIdx, u4Idx1) */
        TR69_TRC (TR69_DBG_TRC, "Modifiying an existing entry in ACL Table\n");

        OctetStrIdx.i4_Length = STRLEN (pNode->au1AclName);
        MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1AclName,
                OctetStrIdx.i4_Length);
        u4Idx0 = (UINT4) pNode->i4IfIndex;
        u4Idx1 = (UINT4) pNode->i4Direction;

        MEMCPY (&NewNode, pNode, sizeof (*pNode));

        /* When FilterName associated to the ACL is tried to modified, 
           check whether the incoming value is same as the RuleFilterSet 
           in the corrosponding RuleTable. If thery are different then return a failure */
        if (STRCMP (name, "FilterName") == 0)
        {
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMSET (OctetStr.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);

            i4rc =
                MynmhGet (*Join
                          (FwlRuleFilterSet, 12, FwlDefnRuleTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                          &RuleFilterSet);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Get failed for FwlAclFilterComb\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
                return SNMP_FAILURE;
            }
            else
            {
                if (STRCMP (OctetStr.pu1_OctetList, RuleFilterSet.pu1_OctetList)
                    != 0)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- TrSetMand: FwlAclFilterComb cannot be modified\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
                    return SNMP_FAILURE;
                }
            }
        }

        if (STRCMP (name, "SequenceNumber") != 0)
        {
            if (pNode->i4SequenceNumber == FWL_DEF_PRIORITY_MIN)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Change the Sequence Number in order"
                          "to modify other parameters in ACL Table\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
                return SNMP_FAILURE;
            }
        }
        else
        {
            if ((value->in_int <= 0) || (value->in_int > 65535))
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Wrong value for Sequence Number\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
                return SNMP_FAILURE;
            }
        }
        if (STRCMP (name, "IfIndex") == 0)
        {
            u4Index0 = value->in_int;
            if (u4Index0 != 0)
            {
                i4rc = SecUtilValidateIfIndex (u4Index0);
            }
            if (i4rc == CFA_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "-E- TrSetMand: Invalid IfIndex\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
                return SNMP_FAILURE;
            }
        }
        if (STRCMP (name, "Direction") == 0)
        {
            u4Index1 = value->in_int;
            if ((u4Index1 != FWLDEFNACLTABLE_ACL_DIRECTION_IN)
                && (u4Index1 != FWLDEFNACLTABLE_ACL_DIRECTION_OUT))
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Direction value can be either 1 or 2\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
                return SNMP_FAILURE;
            }

        }
        i4rc =
            MynmhTest (*Join
                       (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                        &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) DESTROY);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetMand: Test for DESTROY failed for AclName:%s\n",
                           OctetStrIdx.pu1_OctetList);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
            return SNMP_FAILURE;
        }
        i4rc =
            MynmhSet (*Join
                      (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                       &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) DESTROY);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetMand: Set for Destroy failed for AclName:%s\n",
                           OctetStrIdx.pu1_OctetList);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
            return SNMP_FAILURE;
        }

        if (STRCMP (name, "AclName") == 0)
        {
            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStrIdx.pu1_OctetList, value->in_cval,
                    OctetStrIdx.i4_Length);
        }
        if (STRCMP (name, "IfIndex") == 0)
        {
            u4Idx0 = value->in_int;
        }

        if (STRCMP (name, "Direction") == 0)
        {
            u4Idx1 = value->in_int;
        }
        i4rc =
            MynmhTest (*Join
                       (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                        &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) CREATE_AND_WAIT);
        if (i4rc == SNMP_FAILURE)
        {

            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (pNode->au1AclName);
            MEMCPY (OctetStrIdx.pu1_OctetList,
                    pNode->au1AclName, OctetStrIdx.i4_Length);
            u4Idx0 = (UINT4) pNode->i4IfIndex;
            u4Idx1 = (UINT4) pNode->i4Direction;

            i4rc =
                MynmhSet (*Join
                          (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                           &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) CREATE_AND_WAIT);

            i4rc =
                TrSetAll_InternetGatewayDevice_Firewall_fwlDefnAclTable (NULL,
                                                                         pNode,
                                                                         value,
                                                                         TR_DO_SNMP_SET);
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetMand: Test for CREATE_AND_WAIT failed for AclName:%s\n",
                           OctetStrIdx.pu1_OctetList);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                       &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) CREATE_AND_WAIT);
        if (i4rc == SNMP_FAILURE)
        {
            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (pNode->au1AclName);
            MEMCPY (OctetStrIdx.pu1_OctetList,
                    pNode->au1AclName, OctetStrIdx.i4_Length);
            u4Idx0 = (UINT4) pNode->i4IfIndex;
            u4Idx1 = (UINT4) pNode->i4Direction;

            i4rc =
                MynmhSet (*Join
                          (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                           &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) CREATE_AND_WAIT);

            i4rc =
                TrSetAll_InternetGatewayDevice_Firewall_fwlDefnAclTable (NULL,
                                                                         pNode,
                                                                         value,
                                                                         TR_DO_SNMP_SET);
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetMand: Set for CREATE_AND_WAIT failed for AclName:%s\n",
                           OctetStrIdx.pu1_OctetList);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
            return SNMP_FAILURE;
        }

        /* First update NewNode with the new value */
        /* Then SET all the values for the new row */
        i4rc =
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnAclTable (name,
                                                                     &NewNode,
                                                                     value,
                                                                     !TR_DO_SNMP_SET);
        i4rc =
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnAclTable (NULL,
                                                                     &NewNode,
                                                                     value,
                                                                     TR_DO_SNMP_SET);

        if (i4rc == SNMP_FAILURE)
        {
            i4rc =
                MynmhSet (*Join
                          (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3,
                           u4Idx0, &OctetStrIdx, u4Idx1),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);

            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (pNode->au1AclName);
            MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1AclName,
                    OctetStrIdx.i4_Length);
            u4Idx0 = (UINT4) pNode->i4IfIndex;
            u4Idx1 = (UINT4) pNode->i4Direction;

            i4rc =
                MynmhGet (*Join
                          (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: During modification get for"
                          "RuleRowStatus failed\n");

                i4rc =
                    MynmhSet (*Join
                              (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                               &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                              (VOID *) CREATE_AND_WAIT);

                OctetStr.i4_Length = STRLEN (pNode->au1FilterComb);
                MEMSET (OctetStr.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
                MEMCPY (OctetStr.pu1_OctetList, pNode->au1FilterComb,
                        OctetStr.i4_Length);
                i4rc =
                    MynmhSet (*Join
                              (FwlRuleFilterSet, 12, FwlDefnRuleTableINDEX, 1,
                               &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                              (VOID *) &OctetStr);
                i4rc =
                    MynmhSet (*Join
                              (FwlRuleRowStatus, 12, FwlDefnRuleTableINDEX, 1,
                               &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                              (VOID *) ACTIVE);
            }

            i4rc =
                MynmhSet (*Join
                          (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                           &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) CREATE_AND_WAIT);

            i4rc =
                TrSetAll_InternetGatewayDevice_Firewall_fwlDefnAclTable (NULL,
                                                                         pNode,
                                                                         value,
                                                                         TR_DO_SNMP_SET);
            TR69_TRC (TR69_DBG_TRC, "-E- TrSetMand: SNMP Set failed\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable\n");
            return SNMP_FAILURE;
        }
        else
        {
            MEMCPY (pNode, &NewNode, sizeof (NewNode));
        }

        MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
        OctetStrIdx.i4_Length = STRLEN (pNode->au1AclName);
        MEMCPY (OctetStrIdx.pu1_OctetList,
                pNode->au1AclName, OctetStrIdx.i4_Length);
        u4Idx0 = (UINT4) pNode->i4IfIndex;
        u4Idx1 = (UINT4) pNode->i4Direction;

        if ((STRCMP (name, "RowStatus") != 0) && (pNode->u4RowStatus == ACTIVE))
        {
            i4rc =
                MynmhSet (*Join
                          (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                           &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) ACTIVE);
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetMand_IGD_Firewall_fwlDefnAclTable - Success\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrSetAll_InternetGatewayDevice_Firewall_fwlDefnAclTable
 *  Description     : Set function for all objects of a table.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tFwlDefnAclTable entry.
 *                    bTrDoSnmpSet - Indicates whether SNMP Set is to be done.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetAll_InternetGatewayDevice_Firewall_fwlDefnAclTable (char *name,
                                                         tFwlDefnAclTable *
                                                         pNode,
                                                         ParameterValue * value,
                                                         BOOL1 bTrDoSnmpSet)
{
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    UINT4               u4Idx0 = 0;
    INT4                i4rc = 0;
    UINT4               u4val = 0;
    UINT4               u4Idx1 = 0;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetAll_IGD_Firewall_fwlDefnAclTable\n");

    OctetStrIdx.i4_Length = STRLEN (pNode->au1AclName);
    MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1AclName,
            OctetStrIdx.i4_Length);
    u4Idx0 = (UINT4) pNode->i4IfIndex;
    u4Idx1 = (UINT4) pNode->i4Direction;

    if (!name || STRCMP (name, "Direction") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->i4Direction = u4val = value->in_int;
            pNode->u4ValMask |= FWL_ACL_DIRECTION_MASK;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
    }
    if (!name || STRCMP (name, "FragAction") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->i4FragAction = u4val = value->in_int;
            pNode->u4ValMask |= FWL_ACL_FRAG_ACTION_MASK;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
        u4val = pNode->i4FragAction;
        i4rc =
            MynmhTest (*Join
                       (FwlAclFragAction, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                        &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Test failed for FwlAclFragAction\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlAclFragAction, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                       &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Set failed for FwlAclFragAction\n");
            return SNMP_FAILURE;
        }
    }
    if (!name || STRCMP (name, "IfIndex") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->i4IfIndex = u4val = value->in_int;
            pNode->u4ValMask |= FWL_ACL_IF_INDEX_MASK;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
    }
    if (!name || STRCMP (name, "SequenceNumber") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->i4SequenceNumber = u4val = value->in_int;
            pNode->u4ValMask |= FWL_ACL_SEQUENCE_NUMBER_MASK;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
        u4val = pNode->i4SequenceNumber;

        i4rc =
            MynmhTest (*Join
                       (FwlAclSequenceNumber, 12, FwlDefnAclTableINDEX, 3,
                        u4Idx0, &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER32,
                       (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Test failed for FwlAclSequenceNumber\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlAclSequenceNumber, 12, FwlDefnAclTableINDEX, 3,
                       u4Idx0, &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER32,
                      (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Set failed for FwlAclSequenceNumber\n");
            return SNMP_FAILURE;
        }
    }
    if (!name || STRCMP (name, "Action") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->i4Action = u4val = value->in_int;
            pNode->u4ValMask |= FWL_ACL_ACTION_MASK;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
        u4val = pNode->i4Action;
        i4rc =
            MynmhTest (*Join
                       (FwlAclAction, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                        &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Test failed for FwlAclAction\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlAclAction, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                       &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Set failed for FwlAclAction\n");
            return SNMP_FAILURE;
        }
    }
    if (!name || STRCMP (name, "AclName") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            MEMSET (pNode->au1AclName, '\0', 36);
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMSET (OctetStr.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1AclName, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            pNode->u4ValMask |= FWL_ACL_ACL_NAME_MASK;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
    }
    if (!name || STRCMP (name, "LogTrigger") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->i4LogTrigger = u4val = value->in_int;
            pNode->u4NonMandValMask |= FWL_ACL_LOG_TRIGGER_MASK;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }

        /* While doing an add object, to ensure SNMP Set is allowed for
           non-mandatory objects which are provided by the user check is
           made aganist the particular elements ValMask. */
        if ((pNode->u4NonMandValMask & (FWL_ACL_LOG_TRIGGER_MASK)) ==
            FWL_ACL_LOG_TRIGGER_MASK)
        {
            u4val = pNode->i4LogTrigger;
            i4rc =
                MynmhTest (*Join
                           (FwlAclLogTrigger, 12, FwlDefnAclTableINDEX, 3,
                            u4Idx0, &OctetStrIdx, u4Idx1),
                           SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FwlAclLogTrigger\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlAclLogTrigger, 12, FwlDefnAclTableINDEX, 3,
                           u4Idx0, &OctetStrIdx, u4Idx1),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Set failed for FwlAclLogTrigger\n");
                return SNMP_FAILURE;
            }
        }
    }
    if (!name || STRCMP (name, "RowStatus") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if (value->in_uint == TR_FWL_TRUE)
            {
                pNode->u4RowStatus = u4val = TR_ENABLE_ROWSTATUS;
            }
            else
            {
                pNode->u4RowStatus = u4val = TR_DISABLE_ROWSTATUS;
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
        u4val = pNode->u4RowStatus;
        i4rc =
            MynmhTest (*Join
                       (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                        &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Test failed for FwlAclRowStatus\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlAclRowStatus, 12, FwlDefnAclTableINDEX, 3, u4Idx0,
                       &OctetStrIdx, u4Idx1), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Set failed for FwlAclRowStatus\n");
            return SNMP_FAILURE;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetAll_IGD_Firewall_fwlDefnAclTable - Success\n");
    return SNMP_SUCCESS;
}

/************************End of fwlDefnAclTable*************************/

/***************************fwlDefnIfTable******************************/

/************************************************************************
 *  Function Name   : TrScan_FwlDefnIfTable
 *  Description     : Scans and detects available Interface .
 *
 *  Input           : none.
 *  Output          : None
 *  Returns         : 0/-1
************************************************************************/
UINT4
TrScan_FwlDefnIfTable ()
{
    tFwlDefnIfTable    *pFwlDefnIfTable = NULL;
    tFwlDefnIfTable    *pNextFwlDefnIfTable = NULL;
    INT4                i4FwlIfIndex = 0;
    INT4                i4NextFwlIfIndex = 0;
    UINT4               u4NewNode = 0;
    INT4                i4rc = 0;
    UINT4               u4TrIdx = 0;

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrScan_FwlDefnIfTable\n");

    i4rc = nmhGetFirstIndexFwlDefnIfTable (&i4FwlIfIndex);
    if (i4rc == SNMP_FAILURE)
    {
        TR69_TRC (TR69_DBG_TRC, "GetFirstIndex failed for FwlDefnIfTable\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_FwlDefnIfTable\n");
        return (OSIX_FAILURE);
    }
    i4NextFwlIfIndex = i4FwlIfIndex;

    UTL_SLL_OFFSET_SCAN (&FwlDefnIfTableList, pFwlDefnIfTable,
                         pNextFwlDefnIfTable, tFwlDefnIfTable *)
    {
        pFwlDefnIfTable->u4Visited = 0;
    }
    while (1)
    {
        pFwlDefnIfTable = NULL;
        u4NewNode = 1;

        if (i4NextFwlIfIndex == FWL_GLOBAL_IDX)
        {
            i4FwlIfIndex = i4NextFwlIfIndex;
            i4rc =
                nmhGetNextIndexFwlDefnIfTable (i4FwlIfIndex, &i4NextFwlIfIndex);
            if (i4rc == SNMP_FAILURE)
            {
                break;
            }
            continue;
        }
        /* Do not instantiate if already present */
        UTL_SLL_OFFSET_SCAN (&FwlDefnIfTableList, pFwlDefnIfTable,
                             pNextFwlDefnIfTable, tFwlDefnIfTable *)
        {
            if (pFwlDefnIfTable->i4IfIndex == i4NextFwlIfIndex)
            {
                u4NewNode = 0;
                pFwlDefnIfTable->u4Visited = 1;
                break;
            }
        }
/* Instantiate if this is a new instance */
        if (u4NewNode)
        {
            u4TrIdx = 0;
            i4rc =
                addObjectIntern
                ("InternetGatewayDevice.Firewall.fwlDefnIfTable",
                 (int *) &u4TrIdx);
            if (i4rc == 0)
            {
                pFwlDefnIfTable =
                    TrAdd_DefnIfTable (u4TrIdx, i4NextFwlIfIndex,
                                       TR_POPULATE_IF_ENTRY);
                if (pFwlDefnIfTable == NULL)
                {
                    TR69_TRC (TR69_DBG_TRC, "FwlDefnIfTable is Null\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrScan_FwlDefnIfTable\n");
                    return OSIX_FAILURE;
                }
                pFwlDefnIfTable->u4Visited = 1;
            }
            else
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "addObjectIntern failed, rc = %d\n", i4rc);
                TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_FwlDefnIfTable\n");
            }
        }

        i4FwlIfIndex = i4NextFwlIfIndex;
        i4rc = nmhGetNextIndexFwlDefnIfTable (i4FwlIfIndex, &i4NextFwlIfIndex);
        if (i4rc == SNMP_FAILURE)
        {
            break;
        }
    }

    /* Incase if an entry is deleted via CLI or SNMP, in order 
       to synchronize TR database locate the node and initialize 
       the structure  elements to zero */

    UTL_SLL_OFFSET_SCAN (&FwlDefnIfTableList, pFwlDefnIfTable,
                         pNextFwlDefnIfTable, tFwlDefnIfTable *)
    {
        /* Before initializing  the structure elements to zero, check 
           is made against both visited flag and the table mask in order to make 
           sure that this does not intervene the row creation process */
        if ((pFwlDefnIfTable->u4Visited == 0) &&
            (pFwlDefnIfTable->u4ValMask == FWL_DEFN_IF_TABLE_VAL_MASK))
        {
            pFwlDefnIfTable->i4IfIndex = 0;
            pFwlDefnIfTable->i4IfType = 0;
            pFwlDefnIfTable->u4RowStatus = 0;
            pFwlDefnIfTable->u4ValMask = 0;
            pFwlDefnIfTable->u4NonMandValMask = 0;
        }
    }

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_FwlDefnIfTable\n");
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrInitInternetGatewayDevice_Firewall_fwlDefnIfTable
 *  Description     : Creates a TR Instance for fwlDefnIfTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrInitInternetGatewayDevice_Firewall_fwlDefnIfTable (int idx1)
{
    tFwlDefnIfTable    *pFwlDefnIfTable = NULL;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrInit_IGD_Firewall_fwlDefnIfTable\n");
    if (idx1 == 0)
    {
        TR69_TRC (TR69_DBG_TRC, "TrInstance is Zero\n");

        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrInit_IGD_Firewall_fwlDefnIfTable\n");
        return SNMP_SUCCESS;
    }
    pFwlDefnIfTable = TrAdd_DefnIfTable (idx1, 0, !TR_POPULATE_IF_ENTRY);
    if (pFwlDefnIfTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "FwlDefnIfTable is Null\n");

        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrInit_IGD_Firewall_fwlDefnIfTable\n");
        return SNMP_FAILURE;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrInit_IGD_Firewall_fwlDefnIfTable\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrAdd_DefnIfTable
 *  Description     : Adds an Interface node to TR69.
 *
 *  Input           : u4TrIdx - Tr Instance
 *                    u4IfIdx - CFA i/f index.
 *                    bPopulateIfTable - Indicates whether to populate table 
                      entries 
 *  Output          : None
 *  Returns         : pFwlDefnIfTable - Allocated Interface node.
 ************************************************************************/
tFwlDefnIfTable    *
TrAdd_DefnIfTable (UINT4 u4TrIdx, UINT4 u4IfIdx, BOOL1 bPopulateIfEntry)
{
    tFwlDefnIfTable    *pFwlDefnIfTable = NULL;
    tFwlDefnIfTable    *pNextFwlDefnIfTable = NULL;

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrAdd_DefnIfTable\n");
    /* If TrAdd is called via init function, then 
       allocate the memory and initalize structure members.
       If TrAdd is called via Scan function, 
       then locate the node, assign the SNMP index 
       and table mask */

    if (u4IfIdx == 0)
    {
        /* By default DefnIf table has an entry with index '0'.
           To populate this entry in TR database following check is done.
           If TrAdd is called via Scan function,which can be identified
           by using 'bPopulateIfEntry' variable, then node is populated. */
        if (!bPopulateIfEntry)
        {
            pFwlDefnIfTable =
                MEM_MALLOC (sizeof (tFwlDefnIfTable), tFwlDefnIfTable);
            if (pFwlDefnIfTable == NULL)
            {
                TR69_TRC (TR69_DBG_TRC, " Memory Allocation - Failed\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_DefnIfTable\n");
                return NULL;
            }

            MEMSET (pFwlDefnIfTable, 0, sizeof (tFwlDefnIfTable));

            pFwlDefnIfTable->u4TrInstance = u4TrIdx;
            pFwlDefnIfTable->i4IfIndex = u4IfIdx;
            pFwlDefnIfTable->u4ValMask = 0;
            pFwlDefnIfTable->u4NonMandValMask = 0;
            pFwlDefnIfTable->u4RowStatus = 0;
            TMO_SLL_Add (&FwlDefnIfTableList, &pFwlDefnIfTable->Link);
            TR69_TRC (TR69_DBG_TRC, "Node Allocation - Success\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_DefnIfTable\n");

            return pFwlDefnIfTable;
        }
    }
    UTL_SLL_OFFSET_SCAN (&FwlDefnIfTableList, pFwlDefnIfTable,
                         pNextFwlDefnIfTable, tFwlDefnIfTable *)
    {
        if (pFwlDefnIfTable->u4TrInstance == u4TrIdx)
        {
            pFwlDefnIfTable->i4IfIndex = u4IfIdx;
            pFwlDefnIfTable->u4ValMask = FWL_DEFN_IF_TABLE_VAL_MASK;
            TR69_TRC (TR69_DBG_TRC, "Row Creation - Success\n");
            break;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_DefnIfTable\n");
    return pFwlDefnIfTable;
}

/************************************************************************
 *  Function Name   : TrDeleteInternetGatewayDevice_Firewall_fwlDefnIfTable
 *  Description     : Delete TR Instance for fwlDefnIfTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrDeleteInternetGatewayDevice_Firewall_fwlDefnIfTable (int idx1)
{
    tFwlDefnIfTable    *pFwlDefnIfTable = NULL;
    tFwlDefnIfTable    *pNextFwlDefnIfTable = NULL;
    UINT4               u4Idx = 0;
    INT4                i4rc = 0;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrDelete_IGD_Firewall_fwlDefnIfTable\n");

    UTL_SLL_OFFSET_SCAN (&FwlDefnIfTableList, pFwlDefnIfTable,
                         pNextFwlDefnIfTable, tFwlDefnIfTable *)
    {
        if (pFwlDefnIfTable->u4TrInstance == (UINT4) idx1)
        {
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Deleting DefnIf entry TrInstance:%d IfIndex:%d\n",
                           pFwlDefnIfTable->u4TrInstance,
                           pFwlDefnIfTable->i4IfIndex);

            u4Idx = (UINT4) pFwlDefnIfTable->i4IfIndex;
            if (nmhValidateIndexInstanceFwlDefnIfTable (u4Idx) == SNMP_SUCCESS)
            {
                i4rc = MynmhTest (*Join
                                  (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1,
                                   u4Idx), SNMP_DATA_TYPE_INTEGER,
                                  (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC, "Test failed for Destroy\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDelete_IGD_Firewall_fwlDefnIfTable\n");
                    return SNMP_FAILURE;
                }
                i4rc = MynmhSet (*Join
                                 (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1,
                                  u4Idx), SNMP_DATA_TYPE_INTEGER,
                                 (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC, "Set failed for Destroy\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDelete_IGD_Firewall_fwlDefnIfTable\n");

                    return SNMP_FAILURE;
                }

            }
            TMO_SLL_Delete (&FwlDefnIfTableList,
                            (tTMO_SLL_NODE *) pFwlDefnIfTable);
            MEM_FREE (pFwlDefnIfTable);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrDelete_IGD_Firewall_fwlDefnIfTable\n");
            return SNMP_SUCCESS;
        }
    }
    /* Instance not present - return SNMP_FAILURE  */
    if (pFwlDefnIfTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "DefnIfTable is Null\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrDelete_IGD_Firewall_fwlDefnIfTable\n");
        return SNMP_FAILURE;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrDelete_IGD_Firewall_fwlDefnIfTable\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_Firewall_fwlDefnIfTable
 *  Description     : Get function for InternetGatewayDevice_Firewall_fwlDefnIfTable
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrGetInternetGatewayDevice_Firewall_fwlDefnIfTable (char *name, int idx1,
                                                    ParameterValue * value)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tFwlDefnIfTable    *pFwlDefnIfTable = NULL;
    tFwlDefnIfTable    *pNextFwlDefnIfTable = NULL;
    tFwlDefnIfTable    *pNode = NULL;
    UINT4               u4val = 0;
    INT4                i4rc = 0;
    UINT4               u4Idx0 = 0;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrGet_IGD_Firewall_fwlDefnIfTable\n");

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;
    UTL_SLL_OFFSET_SCAN (&FwlDefnIfTableList, pFwlDefnIfTable,
                         pNextFwlDefnIfTable, tFwlDefnIfTable *)
    {
        if (pFwlDefnIfTable->u4TrInstance == (UINT4) idx1)
        {
            u4Idx0 = (UINT4) pFwlDefnIfTable->i4IfIndex;
            if (pFwlDefnIfTable->u4ValMask == FWL_DEFN_IF_TABLE_VAL_MASK)
            {
                if (nmhValidateIndexInstanceFwlDefnIfTable (u4Idx0) !=
                    SNMP_SUCCESS)
                {
                    pFwlDefnIfTable->i4IfIndex = 0;
                    pFwlDefnIfTable->i4IfType = 0;
                    pFwlDefnIfTable->u4RowStatus = 0;
                    pFwlDefnIfTable->u4ValMask = 0;
                    pFwlDefnIfTable->u4NonMandValMask = 0;
                    TR69_TRC_ARG1 (TR69_DBG_TRC,
                                   "nmhValidate failed for TrInstance:%d\n",
                                   pFwlDefnIfTable->u4TrInstance);
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrGet_IGD_Firewall_fwlDefnIfTable\n");
                }
            }
            break;
        }
    }

    if (pFwlDefnIfTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "fwlDefnIfTable is Null\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrGet_IGD_Firewall_fwlDefnIfTable\n");
        return -1;
    }

    pNode = pFwlDefnIfTable;

    if (STRCMP (name, "IfIndex") == 0)
    {
        value->out_int = pNode->i4IfIndex;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FwlIfIfIndex ret %d\n",
                       value->out_int);
        return OK;
    }
    else if (STRCMP (name, "RowStatus") == 0)
    {
        if ((pNode->u4ValMask & FWL_DEFN_IF_TABLE_VAL_MASK) ==
            FWL_DEFN_IF_TABLE_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Get failed for FwlIfRowStatus\n");
                value->out_uint = 0;
                return OK;
            }
            if (u4val == TR_DISABLE_ROWSTATUS)
            {
                value->out_uint = 0;
                pNode->u4RowStatus = TR_DISABLE_ROWSTATUS;
            }
            else
            {
                value->out_uint = 1;
                pNode->u4RowStatus = TR_ENABLE_ROWSTATUS;
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlIfRowStatus ret %d\n", value->out_uint);
            return OK;
        }
        else
        {
            if (pNode->u4RowStatus == TR_ENABLE_ROWSTATUS)
            {
                value->out_uint = 1;
            }
            else
            {
                value->out_uint = 0;
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FwlIfRowStatus ret %d\n", u4val);
            return OK;
        }
    }
    else if (STRCMP (name, "IfType") == 0)
    {
        if ((pNode->u4ValMask & FWL_DEFN_IF_TABLE_VAL_MASK) ==
            FWL_DEFN_IF_TABLE_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FwlIfIfType, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "Exit: Get failed for FwlIfIfType\n");
                value->out_uint = 0;
                return OK;
            }
            value->out_int = u4val;
            pNode->i4IfType = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FwlIfIfType ret %d\n", u4val);
            return OK;
        }
        else
        {
            value->out_int = pNode->i4IfType;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlIfIfType ret %d\n", pNode->i4IfType);
            return OK;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrGet_IGD_Firewall_fwlDefnIfTable\n");
    return OK;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_Firewall_fwlDefnIfTable
 *  Description     : Get function for InternetGatewayDevice_Firewall_fwlDefnIfTable
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrSetInternetGatewayDevice_Firewall_fwlDefnIfTable (char *name, int idx1,
                                                    ParameterValue * value)
{
    tFwlDefnIfTable    *pFwlDefnIfTable = NULL;
    tFwlDefnIfTable    *pNextFwlDefnIfTable = NULL;
    tFwlDefnIfTable    *pNode = NULL;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT4               u4val = 0;
    UINT4               u4MakeActive = 0;
    INT4                i4rc = 0;
    INT4                i4RetVal = 0;
    UINT4               u4Idx0 = 0;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSet_IGD_Firewall_fwlDefnIfTable\n");
    UTL_SLL_OFFSET_SCAN (&FwlDefnIfTableList, pFwlDefnIfTable,
                         pNextFwlDefnIfTable, tFwlDefnIfTable *)
    {
        if (pFwlDefnIfTable->u4TrInstance == (UINT4) idx1)
        {
            u4Idx0 = (UINT4) pFwlDefnIfTable->i4IfIndex;
            if (pFwlDefnIfTable->u4ValMask == FWL_DEFN_IF_TABLE_VAL_MASK)
            {
                if (nmhValidateIndexInstanceFwlDefnIfTable (u4Idx0) !=
                    SNMP_SUCCESS)
                {
                    pFwlDefnIfTable->i4IfIndex = 0;
                    pFwlDefnIfTable->i4IfType = 0;
                    pFwlDefnIfTable->u4RowStatus = 0;
                    pFwlDefnIfTable->u4ValMask = 0;
                    pFwlDefnIfTable->u4NonMandValMask = 0;
                    TR69_TRC_ARG1 (TR69_DBG_TRC,
                                   "nmhValidate failed for TrInstance:%d\n",
                                   pFwlDefnIfTable->u4TrInstance);
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrSet_IGD_Firewall_fwlDefnIfTable\n");
                    return SNMP_FAILURE;
                }
            }
            break;
        }
    }

    if (pFwlDefnIfTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "fwlDefnIfTable is Null\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrSet_IGD_Firewall_fwlDefnIfTable\n");
        return SNMP_FAILURE;
    }

    pNode = pFwlDefnIfTable;

    u4MakeActive = 0;
    if (((pNode->u4ValMask & FWL_DEFN_IF_TABLE_VAL_MASK) ==
         FWL_DEFN_IF_TABLE_VAL_MASK) && (STRCMP (name, "RowStatus") != 0))
    {
        u4val = NOT_IN_SERVICE;

        i4rc =
            MynmhTest (*Join
                       (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlIfRowStatus\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Set failed for FwlIfRowStatus\n");
            return SNMP_FAILURE;
        }
        u4MakeActive = 1;
    }
    i4RetVal =
        TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnIfTable (name, value,
                                                                    pNode);
    if (i4RetVal == TR_NOT_MAND_PARAM)
    {
        i4RetVal =
            TrSetMand_InternetGatewayDevice_Firewall_fwlDefnIfTable (name,
                                                                     value,
                                                                     pNode);
    }

    if ((u4MakeActive) && (STRCMP (name, "RowStatus") != 0)
        && (pNode->u4RowStatus == ACTIVE))
    {
        u4val = ACTIVE;
        u4Idx0 = (UINT4) pNode->i4IfIndex;

        i4rc =
            MynmhTest (*Join
                       (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlIfRowStatus\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Set failed for FwlIfRowStatus\n");
            return SNMP_FAILURE;
        }
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrSet_IGD_Firewall_fwlDefnIfTable - Failure\n");
        return SNMP_FAILURE;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSet_IGD_Firewall_fwlDefnIfTable - Success\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnIfTable
 *  Description     : Set function for non-mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tFwlDefnIfTable entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnIfTable (char *name,
                                                            ParameterValue *
                                                            value,
                                                            tFwlDefnIfTable *
                                                            pNode)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    INT4                i4rc = 0;
    UINT4               u4val = 0;
    UINT4               u4Idx0 = 0;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    OctetStr.pu1_OctetList = au1OctetList;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetNonMand_IGD_Firewall_fwlDefnIfTable\n");

    u4Idx0 = (UINT4) pNode->i4IfIndex;

    if (STRCMP (name, "RowStatus") == 0)
    {
        if ((pNode->u4ValMask & FWL_DEFN_IF_TABLE_VAL_MASK) ==
            FWL_DEFN_IF_TABLE_VAL_MASK)
        {
            if (value->in_uint == TR_FWL_TRUE)
            {
                pNode->u4RowStatus = u4val = TR_ENABLE_ROWSTATUS;
            }
            else
            {
                pNode->u4RowStatus = u4val = TR_DISABLE_ROWSTATUS;
            }
            i4rc =
                MynmhTest (*Join
                           (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                           SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Test failed for FwlIfRowStatus\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Set failed for FwlIfRowStatus\n");
                return SNMP_FAILURE;
            }
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnIfTable - Success\n");
            return SNMP_SUCCESS;
        }
        else
        {
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnIfTable (name, pNode,
                                                                    value,
                                                                    !TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnIfTable\n");
            return SNMP_SUCCESS;
        }

    }
    else if (STRCMP (name, "IfType") == 0)
    {
        if ((pNode->u4ValMask & FWL_DEFN_IF_TABLE_VAL_MASK) ==
            FWL_DEFN_IF_TABLE_VAL_MASK)
        {
            pNode->i4IfType = u4val = value->in_int;
            i4rc =
                MynmhTest (*Join
                           (FwlIfIfType, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                           SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Test failed for FwlIfIfType\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlIfIfType, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Set failed for FwlIfIfType\n");
                return SNMP_FAILURE;
            }
            pNode->u4NonMandValMask |= FWL_IF_IF_TYPE_MASK;
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnIfTable - Success\n");
            return SNMP_SUCCESS;
        }
        else
        {
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnIfTable (name, pNode,
                                                                    value,
                                                                    !TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnIfTable\n");
            return SNMP_SUCCESS;
        }

    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetNonMand_IGD_Firewall_fwlDefnIfTable - Mand Param\n");
    return TR_NOT_MAND_PARAM;
}

/************************************************************************
 *  Function Name   : TrSetMand_InternetGatewayDevice_Firewall_fwlDefnIfTable
 *  Description     : Set function for mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tfwlDefnIfTable entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetMand_InternetGatewayDevice_Firewall_fwlDefnIfTable (char *name,
                                                         ParameterValue * value,
                                                         tFwlDefnIfTable *
                                                         pNode)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tFwlDefnIfTable     NewNode;
    INT4                i4rc = 0;
    INT4                i4RetVal = 0;
    UINT4               u4Idx0 = 0;
    UINT4               u4Index = 0;
#ifdef TR69MSR_WANTED
    UINT1               u1Tr69MsrStatus = 0;
#endif

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    OctetStr.pu1_OctetList = au1OctetList;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetMand_IGD_Firewall_fwlDefnIfTable\n");
    u4Idx0 = (UINT4) pNode->i4IfIndex;

    if (pNode->u4ValMask != FWL_DEFN_IF_TABLE_VAL_MASK)
    {
        TrSetAll_InternetGatewayDevice_Firewall_fwlDefnIfTable (name, pNode,
                                                                value,
                                                                !TR_DO_SNMP_SET);
        if ((pNode->u4ValMask & FWL_DEFN_IF_TABLE_VAL_MASK) ==
            FWL_DEFN_IF_TABLE_VAL_MASK)
        {
            TR69_TRC (TR69_DBG_TRC, "Creation of new row in DefnIf Table\n");
            u4Idx0 = (UINT4) pNode->i4IfIndex;

            /* this check is done bcos in low level return value of 
               SecUtilValidateIfIndex in nmhTest is not handled properly */
            i4rc = SecUtilValidateIfIndex (u4Idx0);
            if (i4rc == CFA_FAILURE)
            {
                pNode->u4ValMask = 0;
                pNode->u4RowStatus = 0;
                pNode->u4NonMandValMask = 0;
                pNode->i4IfIndex = 0;
                pNode->i4IfType = 0;
                TR69_TRC (TR69_DBG_TRC, "-E- TrSetMand: Invalid IfIndex\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnIfTable\n");
                return SNMP_FAILURE;
            }
            i4rc =
                MynmhTest (*Join
                           (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                           SNMP_DATA_TYPE_INTEGER, (VOID *) CREATE_AND_WAIT);
            if (i4rc == SNMP_FAILURE)
            {
#ifdef TR69MSR_WANTED
                Tr69MsrGetRestoreStatus (&u1Tr69MsrStatus);
                if (u1Tr69MsrStatus == OSIX_TRUE)
                {
                    return SNMP_SUCCESS;
                }
#endif
                pNode->u4ValMask = 0;
                pNode->u4RowStatus = 0;
                pNode->u4NonMandValMask = 0;
                pNode->i4IfIndex = 0;
                pNode->i4IfType = 0;
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Test for CREATE_AND_WAIT failed\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnIfTable\n");
                return SNMP_FAILURE;
            }
            i4rc =
                MynmhSet (*Join
                          (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) CREATE_AND_WAIT);
            if (i4rc == SNMP_FAILURE)
            {
                pNode->u4ValMask = 0;
                pNode->u4RowStatus = 0;
                pNode->u4NonMandValMask = 0;
                pNode->i4IfIndex = 0;
                pNode->i4IfType = 0;
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Set for CREATE_AND_WAIT failed\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnIfTable\n");
                return SNMP_FAILURE;
            }
            else
            {
                pNode->u4RowStatus = CREATE_AND_GO;
            }
            i4rc =
                TrSetAll_InternetGatewayDevice_Firewall_fwlDefnIfTable (NULL,
                                                                        pNode,
                                                                        value,
                                                                        TR_DO_SNMP_SET);
            if (i4rc == SNMP_FAILURE)
            {
                MynmhSet (*Join
                          (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);
                pNode->u4ValMask = 0;
                pNode->u4RowStatus = 0;
                pNode->u4NonMandValMask = 0;
                pNode->i4IfIndex = 0;
                pNode->i4IfType = 0;
                TR69_TRC (TR69_DBG_TRC, "-E- TrSetMand: SNMP set failed\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnIfTable\n");
                return SNMP_FAILURE;
            }
            i4rc =
                MynmhSet (*Join
                          (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) ACTIVE);
            if (i4rc == SNMP_FAILURE)
            {
                MynmhSet (*Join
                          (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);
                pNode->u4ValMask = 0;
                pNode->u4RowStatus = 0;
                pNode->u4NonMandValMask = 0;
                pNode->i4IfIndex = 0;
                pNode->i4IfType = 0;
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Set for ACTIVE failed\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnIfTable\n");
                return SNMP_FAILURE;
            }
            else
            {
                pNode->u4RowStatus = ACTIVE;
            }

        }
    }
    else
    {
        TR69_TRC (TR69_DBG_TRC,
                  "Modifiying an existing entry in DefnIf Table\n");

        MEMCPY (&NewNode, pNode, sizeof (*pNode));
        if (STRCMP (name, "IfIndex") == 0)
        {
            u4Index = value->in_int;
            i4RetVal = SecUtilValidateIfIndex (u4Index);
            if (i4RetVal == CFA_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "-E- TrSetMand: Invalid IfIndex\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnIfTable\n");
                return SNMP_FAILURE;
            }
        }
        i4rc =
            MynmhTest (*Join
                       (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetMand: Test for DESTROY failed for IfIndex:%d\n",
                           u4Idx0);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnIfTable\n");
            return SNMP_FAILURE;
        }
        i4rc =
            MynmhSet (*Join
                      (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);

        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetMand: Set for DESTROY failed for IfIndex:%d \n",
                           u4Idx0);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnIfTable\n");
            return SNMP_FAILURE;
        }

        if (STRCMP (name, "IfIndex") == 0)
        {
            u4Idx0 = value->in_int;
        }

        i4rc =
            MynmhTest (*Join
                       (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) CREATE_AND_WAIT);
        if (i4rc == SNMP_FAILURE)
        {
            u4Idx0 = (UINT4) pNode->i4IfIndex;

            i4rc =
                MynmhSet (*Join
                          (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) CREATE_AND_WAIT);

            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnIfTable (NULL, pNode,
                                                                    value,
                                                                    TR_DO_SNMP_SET);
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetMand: Test for CREATE_AND_WAIT failed for IfIndex:%d \n",
                           u4Idx0);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnIfTable\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) CREATE_AND_WAIT);
        if (i4rc == SNMP_FAILURE)
        {
            u4Idx0 = (UINT4) pNode->i4IfIndex;

            i4rc =
                MynmhSet (*Join
                          (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) CREATE_AND_WAIT);

            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnIfTable (NULL, pNode,
                                                                    value,
                                                                    TR_DO_SNMP_SET);
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetMand: Set for CREATE_AND_WAIT failed for IfIndex:%d \n",
                           u4Idx0);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnIfTable\n");
            return SNMP_FAILURE;
        }
        /* First update NewNode with the new value */
        /* Then SET all the values for the new row */
        i4rc =
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnIfTable (name,
                                                                    &NewNode,
                                                                    value,
                                                                    !TR_DO_SNMP_SET);
        i4rc =
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnIfTable (NULL,
                                                                    &NewNode,
                                                                    value,
                                                                    TR_DO_SNMP_SET);
        if (i4rc == SNMP_FAILURE)
        {
            MynmhSet (*Join
                      (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);

            u4Idx0 = (UINT4) pNode->i4IfIndex;

            MynmhSet (*Join
                      (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) CREATE_AND_WAIT);

            i4rc =
                TrSetAll_InternetGatewayDevice_Firewall_fwlDefnIfTable (NULL,
                                                                        pNode,
                                                                        value,
                                                                        TR_DO_SNMP_SET);
            TR69_TRC (TR69_DBG_TRC, "-E- TrSetMand: SNMP set failed\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnIfTable\n");
            return SNMP_FAILURE;
        }
        else
        {
            MEMCPY (pNode, &NewNode, sizeof (NewNode));
        }
        u4Idx0 = (UINT4) pNode->i4IfIndex;
        if ((STRCMP (name, "RowStatus") != 0) && (pNode->u4RowStatus == ACTIVE))
        {
            i4rc =
                MynmhSet (*Join
                          (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) ACTIVE);
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetMand_IGD_Firewall_fwlDefnIfTable - Success\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrSetAll_InternetGatewayDevice_Firewall_fwlDefnIfTable
 *  Description     : Set function for all objects of a table.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tFwlDefnIfTable entry.
 *                    bTrDoSnmpSet - Indicates whether SNMP Set is to be done.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetAll_InternetGatewayDevice_Firewall_fwlDefnIfTable (char *name,
                                                        tFwlDefnIfTable * pNode,
                                                        ParameterValue * value,
                                                        BOOL1 bTrDoSnmpSet)
{
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    INT4                i4rc = 0;
    UINT4               u4val = 0;
    UINT4               u4Idx0 = 0;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    OctetStr.pu1_OctetList = au1OctetList;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetAll_IGD_Firewall_fwlDefnIfTable\n");
    u4Idx0 = (UINT4) pNode->i4IfIndex;

    if (!name || STRCMP (name, "IfIndex") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->i4IfIndex = u4val = value->in_int;
            pNode->u4ValMask |= FWL_IF_IF_INDEX_MASK;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
    }
    if (!name || STRCMP (name, "IfType") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->i4IfType = u4val = value->in_int;
            pNode->u4NonMandValMask |= FWL_IF_IF_TYPE_MASK;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
        /* While doing an add object, to ensure SNMP Set is allowed for
           non-mandatory objects which are provided by the user check is
           made aganist the particular elements ValMask. */

        if (((pNode->u4NonMandValMask) & FWL_IF_IF_TYPE_MASK) ==
            FWL_IF_IF_TYPE_MASK)
        {
            MynmhSet (*Join
                      (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) NOT_IN_SERVICE);
            u4val = pNode->i4IfType;
            i4rc =
                MynmhTest (*Join
                           (FwlIfIfType, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                           SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FwlIfIfType\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlIfIfType, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Set failed for FwlIfIfType\n");
                return SNMP_FAILURE;
            }
        }
    }
    if (!name || STRCMP (name, "RowStatus") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if (value->in_uint == TR_FWL_TRUE)
            {
                pNode->u4RowStatus = u4val = TR_ENABLE_ROWSTATUS;
            }
            else
            {
                pNode->u4RowStatus = u4val = TR_DISABLE_ROWSTATUS;
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
        if (pNode->u4RowStatus != CREATE_AND_GO)
        {
            u4val = pNode->u4RowStatus;
            i4rc =
                MynmhTest (*Join
                           (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                           SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FwlIfRowStatus\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlIfRowStatus, 12, FwlDefnIfTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Set failed for FwlIfRowStatus\n");
                return SNMP_FAILURE;
            }
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetAll_IGD_Firewall_fwlDefnIfTable - Success\n");
    return SNMP_SUCCESS;
}

/************************End of fwlDefnIfTable**************************/

/***************************fwlDefnDmzTable******************************/

/************************************************************************
 *  Function Name   : TrScan_FwlDefnDmzTable
 *  Description     : Scans and detects available DMZ .
 *
 *  Input           : none.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
UINT4
TrScan_FwlDefnDmzTable ()
{
    tFwlDefnDmzTable   *pFwlDefnDmzTable = NULL;
    tFwlDefnDmzTable   *pFwlNextDefnDmzTable = NULL;
    UINT4               u4FwlDmzIndex = 0;
    UINT4               u4NextFwlDmzIndex = 0;
    UINT4               u4NewNode = 0;
    INT4                i4rc = 0;
    UINT4               u4TrIdx = 0;

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrScan_FwlDefnDmzTable\n");

    i4rc = nmhGetFirstIndexFwlDefnDmzTable (&u4FwlDmzIndex);
    if (i4rc == SNMP_FAILURE)
    {
        TR69_TRC (TR69_DBG_TRC, "GetFirstIndex failed for FwlDefnDmzTable\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_FwlDefnDmzTable\n");
        return (OSIX_FAILURE);
    }
    u4NextFwlDmzIndex = u4FwlDmzIndex;

    UTL_SLL_OFFSET_SCAN (&FwlDefnDmzTableList, pFwlDefnDmzTable,
                         pFwlNextDefnDmzTable, tFwlDefnDmzTable *)
    {
        pFwlDefnDmzTable->u4Visited = 0;
    }
    while (1)
    {
        pFwlDefnDmzTable = NULL;
        u4NewNode = 1;

        /* Do not instantiate if already present */
        UTL_SLL_OFFSET_SCAN (&FwlDefnDmzTableList, pFwlDefnDmzTable,
                             pFwlNextDefnDmzTable, tFwlDefnDmzTable *)
        {
            if (pFwlDefnDmzTable->u4IpIndex == u4NextFwlDmzIndex)

            {
                u4NewNode = 0;
                pFwlDefnDmzTable->u4Visited = 1;
                break;
            }
        }

        /* Instantiate if this is a new instance */
        if (u4NewNode)
        {
            u4TrIdx = 0;
            i4rc =
                addObjectIntern
                ("InternetGatewayDevice.Firewall.fwlDefnDmzTable",
                 (int *) &u4TrIdx);
            if (i4rc == 0)
            {
                pFwlDefnDmzTable =
                    TrAdd_DefnDmzTable (u4TrIdx, u4NextFwlDmzIndex);
                if (pFwlDefnDmzTable == NULL)
                {
                    TR69_TRC (TR69_DBG_TRC, "FwlDefnDmzTable is Null\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrScan_FwlDefnDmzTable\n");
                    return OSIX_FAILURE;
                }
                pFwlDefnDmzTable->u4Visited = 1;
            }
            else
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "addObjectIntern failed, rc = %d\n", i4rc);
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrScan_FwlDefnDmzTable\n");
            }
        }

        u4FwlDmzIndex = u4NextFwlDmzIndex;
        i4rc =
            nmhGetNextIndexFwlDefnDmzTable (u4FwlDmzIndex, &u4NextFwlDmzIndex);
        if (i4rc == SNMP_FAILURE)
        {
            break;
        }
    }

    /* Incase if an entry is deleted via CLI or SNMP, in order 
       to synchronize TR database locate the node and initialize 
       the structure  elements to zero */

    UTL_SLL_OFFSET_SCAN (&FwlDefnDmzTableList, pFwlDefnDmzTable,
                         pFwlNextDefnDmzTable, tFwlDefnDmzTable *)
    {
        /* Before initializing  the structure elements to zero, check 
           is made against both visited flag and the table mask in order to make 
           sure that this does not intervene the row creation process */
        if ((pFwlDefnDmzTable->u4Visited == 0) &&
            (pFwlDefnDmzTable->u4ValMask == FWL_DEFN_DMZ_TABLE_VAL_MASK))
        {
            pFwlDefnDmzTable->u4IpIndex = 0;
            pFwlDefnDmzTable->u4RowStatus = 0;
            pFwlDefnDmzTable->u4ValMask = 0;
            MEMSET (pFwlDefnDmzTable->au1IpIndex, '\0', 16);
        }
    }

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_FwlDefnDmzTable\n");
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrInitInternetGatewayDevice_Firewall_fwlDefnDmzTable
 *  Description     : Creates a TR Instance for fwlDefnDmzTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrInitInternetGatewayDevice_Firewall_fwlDefnDmzTable (int idx1)
{
    tFwlDefnDmzTable   *pFwlDefnDmzTable = NULL;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrInit_IGD_Firewall_fwlDefnDmzTable\n");
    if (idx1 == 0)
    {
        TR69_TRC (TR69_DBG_TRC, "TrInstance is Zero\n");

        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrInit_IGD_Firewall_fwlDefnDmzTable\n");
        return SNMP_SUCCESS;
    }
    pFwlDefnDmzTable = TrAdd_DefnDmzTable (idx1, 0);
    if (pFwlDefnDmzTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "FwlDefnDmzTable is Null\n");

        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrInit_IGD_Firewall_fwlDefnDmzTable\n");

        return SNMP_FAILURE;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrInit_IGD_Firewall_fwlDefnDmzTable\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrAdd_DefnDmzTable
 *  Description     : Adds an DMZ node to TR69.
 *
 *  Input           : u4TrIdx - Tr Instance
 *                    u4IfIdx - CFA i/f index.
 *  Output          : None
 *  Returns         : pFwlDefnDmzTable - Allocated Interface node.
 ************************************************************************/
tFwlDefnDmzTable   *
TrAdd_DefnDmzTable (UINT4 u4TrIdx, UINT4 u4IfIdx)
{
    tFwlDefnDmzTable   *pFwlDefnDmzTable = NULL;
    tFwlDefnDmzTable   *pFwlNextDefnDmzTable = NULL;

    /* If TrAdd is called via the init function, then 
       allocate the memory and initalize structure members.
       If TrAdd is called via the Scan function, 
       then locate the node, assign the SNMP index 
       and table mask. */

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrAdd_DefnDmzTable\n");

    if (u4IfIdx == 0)
    {
        pFwlDefnDmzTable =
            MEM_MALLOC (sizeof (tFwlDefnDmzTable), tFwlDefnDmzTable);
        if (pFwlDefnDmzTable == NULL)
        {
            TR69_TRC (TR69_DBG_TRC, " Memory Allocation - Failed\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_DefnDmzTable\n");
            return NULL;
        }
        MEMSET (pFwlDefnDmzTable, 0, sizeof (tFwlDefnDmzTable));

        pFwlDefnDmzTable->u4TrInstance = u4TrIdx;
        pFwlDefnDmzTable->u4IpIndex = u4IfIdx;
        pFwlDefnDmzTable->u4ValMask = 0;
        pFwlDefnDmzTable->u4RowStatus = 0;
        TMO_SLL_Add (&FwlDefnDmzTableList, &pFwlDefnDmzTable->Link);

        TR69_TRC (TR69_DBG_TRC, "Node Allocation - Success\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_DefnDmzTable\n");

        return pFwlDefnDmzTable;
    }

    UTL_SLL_OFFSET_SCAN (&FwlDefnDmzTableList, pFwlDefnDmzTable,
                         pFwlNextDefnDmzTable, tFwlDefnDmzTable *)
    {
        if (pFwlDefnDmzTable->u4TrInstance == u4TrIdx)
        {
            pFwlDefnDmzTable->u4IpIndex = u4IfIdx;
            pFwlDefnDmzTable->u4ValMask = FWL_DEFN_DMZ_TABLE_VAL_MASK;
            TR69_TRC (TR69_DBG_TRC, "Row Creation - Success\n");
            break;
        }
    }

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_DefnDmzTable\n");
    return pFwlDefnDmzTable;
}

/************************************************************************
 *  Function Name   : TrDeleteInternetGatewayDevice_Firewall_fwlDefnDmzTable
 *  Description     : Delete TR Instance for fwlDefnDmzTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/1
 ************************************************************************/
INT4
TrDeleteInternetGatewayDevice_Firewall_fwlDefnDmzTable (int idx1)
{
    tFwlDefnDmzTable   *pFwlDefnDmzTable = NULL;
    tFwlDefnDmzTable   *pFwlNextDefnDmzTable = NULL;
    UINT4               u4Idx = 0;
    INT4                i4rc = 0;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrDelete_IGD_Firewall_fwlDefnDmzTable\n");

    UTL_SLL_OFFSET_SCAN (&FwlDefnDmzTableList, pFwlDefnDmzTable,
                         pFwlNextDefnDmzTable, tFwlDefnDmzTable *)
    {
        if (pFwlDefnDmzTable->u4TrInstance == (UINT4) idx1)
        {
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Deleting DMZ Host TrInstance:%d String:%s\n",
                           pFwlDefnDmzTable->u4TrInstance,
                           pFwlDefnDmzTable->au1IpIndex);

            u4Idx = pFwlDefnDmzTable->u4IpIndex;
            if (nmhValidateIndexInstanceFwlDefnDmzTable (u4Idx) == SNMP_SUCCESS)
            {
                i4rc = MynmhTest (*Join
                                  (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1,
                                   u4Idx), SNMP_DATA_TYPE_INTEGER,
                                  (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC, "Test failed for Destroy\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDelete_IGD_Firewall_fwlDefnDmzTable\n");
                    return SNMP_FAILURE;
                }

                i4rc = MynmhSet (*Join
                                 (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1,
                                  u4Idx), SNMP_DATA_TYPE_INTEGER,
                                 (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC, "Set failed for Destroy\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDelete_IGD_Firewall_fwlDefnDmzTable\n");
                    return SNMP_FAILURE;
                }
            }
            TMO_SLL_Delete (&FwlDefnDmzTableList,
                            (tTMO_SLL_NODE *) pFwlDefnDmzTable);
            MEM_FREE (pFwlDefnDmzTable);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrDelete_IGD_Firewall_fwlDefnDmzTable\n");
            return SNMP_SUCCESS;
        }
    }
    /* Instance not present - return SNMP_FAILURE  */
    if (pFwlDefnDmzTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "DefnDmzTable is Null\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrDelete_IGD_Firewall_fwlDefnDmzTable\n");
        return SNMP_FAILURE;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrDelete_IGD_Firewall_fwlDefnDmzTable\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_Firewall_fwlDefnDmzTable
 *  Description     : Get function for InternetGatewayDevice_Firewall_fwlDefnDmzTable
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrGetInternetGatewayDevice_Firewall_fwlDefnDmzTable (char *name, int idx1,
                                                     ParameterValue * value)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tFwlDefnDmzTable   *pFwlDefnDmzTable = NULL;
    tFwlDefnDmzTable   *pNextFwlDefnDmzTable = NULL;
    tFwlDefnDmzTable   *pNode = NULL;
    UINT4               u4val = 0;
    INT4                i4rc = 0;
    UINT4               u4Idx0 = 0;
    CHR1               *pu1TempIp = NULL;
    tUtlInAddr          IpAddr;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrGet_IGD_Firewall_fwlDefnDmzTable\n");

    UTL_SLL_OFFSET_SCAN (&FwlDefnDmzTableList, pFwlDefnDmzTable,
                         pNextFwlDefnDmzTable, tFwlDefnDmzTable *)
    {
        if (pFwlDefnDmzTable->u4TrInstance == (UINT4) idx1)
        {
            u4Idx0 = pFwlDefnDmzTable->u4IpIndex;
            if (pFwlDefnDmzTable->u4ValMask == FWL_DEFN_DMZ_TABLE_VAL_MASK)
            {
                if (nmhValidateIndexInstanceFwlDefnDmzTable (u4Idx0) !=
                    SNMP_SUCCESS)
                {
                    pFwlDefnDmzTable->u4IpIndex = 0;
                    pFwlDefnDmzTable->u4RowStatus = 0;
                    pFwlDefnDmzTable->u4ValMask = 0;
                    MEMSET (pFwlDefnDmzTable->au1IpIndex, '\0', 16);
                    TR69_TRC_ARG1 (TR69_DBG_TRC,
                                   "nmhValidate failed for TrInstance:%d\n",
                                   pFwlDefnDmzTable->u4TrInstance);
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrGet_IGD_Firewall_fwlDefnDmzTable\n");
                }
            }
            break;
        }
    }

    if (pFwlDefnDmzTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "DefnDmzTable is Null\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrGet_IGD_Firewall_fwlDefnDmzTable\n");
        return -1;
    }
    pNode = pFwlDefnDmzTable;
    if (STRCMP (name, "IpIndex") == 0)
    {
        u4val = pNode->u4IpIndex;
        MEMSET (pNode->au1IpIndex, '\0', 16);
        IpAddr.u4Addr = OSIX_NTOHL (u4val);
        pu1TempIp = UtlInetNtoa (IpAddr);
        SNPRINTF ((CHR1 *) pNode->au1IpIndex, sizeof (pNode->au1IpIndex),
                  "%s", pu1TempIp);
        value->out_cval = pNode->au1IpIndex;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlDmzIpIndex ret %s\n", value->out_cval);
        return OK;
    }
    else if (STRCMP (name, "RowStatus") == 0)
    {
        if ((pNode->u4ValMask & FWL_DEFN_DMZ_TABLE_VAL_MASK) ==
            FWL_DEFN_DMZ_TABLE_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1,
                           u4Idx0), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Get failed for FwlDmzRowStatus\n");
                value->out_uint = 0;
                return OK;
            }
            if (u4val == TR_DISABLE_ROWSTATUS)
            {
                value->out_uint = 0;
                pNode->u4RowStatus = TR_DISABLE_ROWSTATUS;
            }
            else
            {
                value->out_uint = 1;
                pNode->u4RowStatus = TR_ENABLE_ROWSTATUS;
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FwlDmzRowStatus ret %d\n",
                           u4val);
            return OK;
        }
        else
        {
            if (pNode->u4RowStatus == TR_ENABLE_ROWSTATUS)
            {
                value->out_uint = 1;
            }
            else
            {
                value->out_uint = 0;
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlDmzRowStatus ret %d\n", value->out_uint);
            return OK;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrGet_IGD_Firewall_fwlDefnDmzTable\n");
    return OK;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_Firewall_fwlDefnDmzTable
 *  Description     : Get function for InternetGatewayDevice_Firewall_fwlDefnDmzTable
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrSetInternetGatewayDevice_Firewall_fwlDefnDmzTable (char *name, int idx1,
                                                     ParameterValue * value)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT4               u4val = 0;
    UINT4               u4MakeActive = 0;
    INT4                i4rc = 0;
    INT4                i4RetVal = 0;
    UINT4               u4Idx0 = 0;

    tFwlDefnDmzTable   *pFwlDefnDmzTable = NULL;
    tFwlDefnDmzTable   *pNextFwlDefnDmzTable = NULL;
    tFwlDefnDmzTable   *pNode = NULL;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSet_IGD_Firewall_fwlDefnDmzTable\n");

    UTL_SLL_OFFSET_SCAN (&FwlDefnDmzTableList, pFwlDefnDmzTable,
                         pNextFwlDefnDmzTable, tFwlDefnDmzTable *)
    {
        if (pFwlDefnDmzTable->u4TrInstance == (UINT4) idx1)
        {
            u4Idx0 = pFwlDefnDmzTable->u4IpIndex;
            if (pFwlDefnDmzTable->u4ValMask == FWL_DEFN_DMZ_TABLE_VAL_MASK)
            {
                if (nmhValidateIndexInstanceFwlDefnDmzTable (u4Idx0) !=
                    SNMP_SUCCESS)
                {
                    pFwlDefnDmzTable->u4IpIndex = 0;
                    pFwlDefnDmzTable->u4RowStatus = 0;
                    pFwlDefnDmzTable->u4ValMask = 0;
                    MEMSET (pFwlDefnDmzTable->au1IpIndex, '\0', 16);
                    TR69_TRC_ARG1 (TR69_DBG_TRC,
                                   "nmhValidate failed for TrInstance:%d\n",
                                   pFwlDefnDmzTable->u4TrInstance);
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrSet_IGD_Firewall_fwlDefnDmzTable\n");
                    return SNMP_FAILURE;
                }
            }
            break;
        }
    }

    if (pFwlDefnDmzTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "DefnDmzTable is Null\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrSet_IGD_Firewall_fwlDefnDmzTable\n");
        return SNMP_FAILURE;
    }

    pNode = pFwlDefnDmzTable;

    u4MakeActive = 0;
    if (((pNode->u4ValMask & FWL_DEFN_DMZ_TABLE_VAL_MASK) ==
         FWL_DEFN_DMZ_TABLE_VAL_MASK) && (STRCMP (name, "RowStatus") != 0))
    {
        u4val = NOT_IN_SERVICE;
        i4rc =
            MynmhTest (*Join
                       (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1, u4Idx0),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlDmzRowStatus\n");
            return SNMP_FAILURE;
        }
        i4rc =
            MynmhSet (*Join
                      (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1, u4Idx0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "Exit: Set failed for FwlDmzRowStatus\n");
            return SNMP_FAILURE;
        }
        u4MakeActive = 1;
    }
    i4RetVal =
        TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnDmzTable (name,
                                                                     value,
                                                                     pNode);
    if (i4RetVal == TR_NOT_MAND_PARAM)
    {
        i4RetVal =
            TrSetMand_InternetGatewayDevice_Firewall_fwlDefnDmzTable (name,
                                                                      value,
                                                                      pNode);
    }

    if ((u4MakeActive) && (STRCMP (name, "RowStatus") != 0)
        && (pNode->u4RowStatus == ACTIVE))
    {
        u4val = ACTIVE;
        u4Idx0 = pNode->u4IpIndex;

        i4rc =
            MynmhTest (*Join
                       (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1, u4Idx0),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlDmzRowStatus\n");
            return SNMP_FAILURE;
        }
        i4rc =
            MynmhSet (*Join
                      (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1, u4Idx0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Set failed for FwlDmzRowStatus\n");
            return SNMP_FAILURE;
        }
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrSet_IGD_Firewall_fwlDefnDmzTable - Failure\n");
        return SNMP_FAILURE;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSet_IGD_Firewall_fwlDefnDmzTable - Success\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnDmzTable
 *  Description     : Set function for non-mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tFwlDefnDmzTable entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnDmzTable (char *name,
                                                             ParameterValue *
                                                             value,
                                                             tFwlDefnDmzTable *
                                                             pNode)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    INT4                i4rc = 0;
    UINT4               u4val = 0;
    UINT4               u4Idx0 = 0;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    OctetStr.pu1_OctetList = au1OctetList;

    u4Idx0 = pNode->u4IpIndex;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetNonMand_IGD_Firewall_fwlDefnDmzTable\n");

    if (STRCMP (name, "RowStatus") == 0)
    {
        if ((pNode->u4ValMask & FWL_DEFN_DMZ_TABLE_VAL_MASK) ==
            FWL_DEFN_DMZ_TABLE_VAL_MASK)
        {
            if (value->in_uint == TR_FWL_TRUE)
            {
                pNode->u4RowStatus = u4val = TR_ENABLE_ROWSTATUS;
            }
            else
            {
                pNode->u4RowStatus = u4val = TR_DISABLE_ROWSTATUS;
            }
            i4rc =
                MynmhTest (*Join
                           (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1,
                            u4Idx0), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Test failed for FwlDmzRowStatus\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1,
                           u4Idx0), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Set failed for FwlDmzRowStatus\n");
                return SNMP_FAILURE;
            }
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnDmzTable - Success\n");
            return SNMP_SUCCESS;
        }
        else
        {
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnDmzTable (name,
                                                                     pNode,
                                                                     value,
                                                                     !TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlDefnDmzTable\n");
            return SNMP_SUCCESS;
        }

    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetNonMand_IGD_Firewall_fwlDefnDmzTable - Mand Param\n");
    return TR_NOT_MAND_PARAM;
}

/************************************************************************
 *  Function Name   : TrSetMand_InternetGatewayDevice_Firewall_fwlDefnDmzTable
 *  Description     : Set function for mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tfwlDefnDmzTable entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetMand_InternetGatewayDevice_Firewall_fwlDefnDmzTable (char *name,
                                                          ParameterValue *
                                                          value,
                                                          tFwlDefnDmzTable *
                                                          pNode)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tFwlDefnDmzTable    NewNode;
    INT4                i4rc = 0;
    UINT4               u4Idx0 = 0;
    tUtlInAddr          IpAddr;
#ifdef TR69MSR_WANTED
    UINT1               u1Tr69MsrStatus = 0;
#endif

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    OctetStr.pu1_OctetList = au1OctetList;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetMand_IGD_Firewall_fwlDefnDmzTable\n");

    u4Idx0 = pNode->u4IpIndex;

    if (pNode->u4ValMask != FWL_DEFN_DMZ_TABLE_VAL_MASK)
    {
        TrSetAll_InternetGatewayDevice_Firewall_fwlDefnDmzTable (name, pNode,
                                                                 value,
                                                                 !TR_DO_SNMP_SET);

        if ((pNode->u4ValMask & FWL_DEFN_DMZ_TABLE_VAL_MASK) ==
            FWL_DEFN_DMZ_TABLE_VAL_MASK)
        {
            TR69_TRC (TR69_DBG_TRC, "Creation of new row in Dmz Table\n");
            u4Idx0 = pNode->u4IpIndex;
            i4rc =
                MynmhTest (*Join
                           (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1,
                            u4Idx0), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) CREATE_AND_WAIT);
            if (i4rc == SNMP_FAILURE)
            {
#ifdef TR69MSR_WANTED
                Tr69MsrGetRestoreStatus (&u1Tr69MsrStatus);
                if (u1Tr69MsrStatus == OSIX_TRUE)
                {
                    return SNMP_SUCCESS;
                }
#endif
                pNode->u4ValMask = 0;
                pNode->u4RowStatus = 0;
                pNode->u4IpIndex = 0;
                MEMSET (pNode->au1IpIndex, '\0', 16);
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Test for CREATE_AND_WAIT failed\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnDmzTable\n");
                return SNMP_FAILURE;
            }
            i4rc =
                MynmhSet (*Join
                          (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1,
                           u4Idx0), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) CREATE_AND_WAIT);
            if (i4rc == SNMP_FAILURE)
            {
                pNode->u4ValMask = 0;
                pNode->u4RowStatus = 0;
                pNode->u4IpIndex = 0;
                MEMSET (pNode->au1IpIndex, '\0', 16);
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Set for CREATE_AND_WAIT failed\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnDmzTable\n");
                return SNMP_FAILURE;
            }
            else
            {
                pNode->u4RowStatus = NOT_READY;
            }
            i4rc =
                TrSetAll_InternetGatewayDevice_Firewall_fwlDefnDmzTable (NULL,
                                                                         pNode,
                                                                         value,
                                                                         TR_DO_SNMP_SET);
            if (i4rc == SNMP_FAILURE)
            {
                MynmhSet (*Join
                          (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1,
                           u4Idx0), SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);
                pNode->u4ValMask = 0;
                pNode->u4RowStatus = 0;
                pNode->u4IpIndex = 0;
                MEMSET (pNode->au1IpIndex, '\0', 16);
                TR69_TRC (TR69_DBG_TRC, "-E- TrSetMand: SNMP Set failed\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnDmzTable\n");
                return SNMP_FAILURE;
            }
            i4rc =
                MynmhSet (*Join
                          (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1,
                           u4Idx0), SNMP_DATA_TYPE_INTEGER, (VOID *) ACTIVE);
            if (i4rc == SNMP_SUCCESS)
            {
                pNode->u4RowStatus = ACTIVE;
            }
            else
            {
                MynmhSet (*Join
                          (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1,
                           u4Idx0), SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);
                pNode->u4ValMask = 0;
                pNode->u4RowStatus = 0;
                pNode->u4IpIndex = 0;
                MEMSET (pNode->au1IpIndex, '\0', 16);
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Set for ACTIVE failed\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlDefnDmzTable\n");
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
        TR69_TRC (TR69_DBG_TRC, "Modifiying an existing entry in Dmz Table\n");

        MEMCPY (&NewNode, pNode, sizeof (*pNode));
        i4rc =
            MynmhTest (*Join
                       (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1, u4Idx0),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetMand: Test for DESTROY failed for DMZ Host:%lu\n",
                           u4Idx0);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnDmzTable\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1, u4Idx0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetMand: Set for DESTROY failed for DMZ Host:%lu\n",
                           u4Idx0);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnDmzTable\n");
            return SNMP_FAILURE;
        }

        if (STRCMP (name, "IpIndex") == 0)
        {
            UtlInetAton ((CHR1 *) value->in_cval, &IpAddr);
            u4Idx0 = OSIX_NTOHL (IpAddr.u4Addr);
        }

        i4rc =
            MynmhTest (*Join
                       (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1, u4Idx0),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) CREATE_AND_WAIT);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetMand: Test for CREATE_AND_WAIT failed for DMZ Host:%lu\n",
                           u4Idx0);
            u4Idx0 = pNode->u4IpIndex;

            i4rc =
                MynmhSet (*Join
                          (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1,
                           u4Idx0), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) CREATE_AND_WAIT);

            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnDmzTable (NULL,
                                                                     pNode,
                                                                     value,
                                                                     TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnDmzTable\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1, u4Idx0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) CREATE_AND_WAIT);
        if (i4rc == SNMP_FAILURE)
        {
            u4Idx0 = pNode->u4IpIndex;

            i4rc =
                MynmhSet (*Join
                          (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1,
                           u4Idx0), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) CREATE_AND_WAIT);

            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnDmzTable (NULL,
                                                                     pNode,
                                                                     value,
                                                                     TR_DO_SNMP_SET);
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Set for CREATE_AND_WAIT failed for DMZ Host:%lu\n",
                           u4Idx0);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnDmzTable\n");

            return SNMP_FAILURE;
        }
        /* First update NewNode with the new value */
        /* Then SET all the values for the new row */
        i4rc =
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnDmzTable (name,
                                                                     &NewNode,
                                                                     value,
                                                                     !TR_DO_SNMP_SET);
        i4rc =
            TrSetAll_InternetGatewayDevice_Firewall_fwlDefnDmzTable (NULL,
                                                                     &NewNode,
                                                                     value,
                                                                     TR_DO_SNMP_SET);
        if (i4rc == SNMP_FAILURE)
        {
            i4rc =
                MynmhSet (*Join
                          (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1,
                           u4Idx0), SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);
            u4Idx0 = pNode->u4IpIndex;

            i4rc =
                MynmhSet (*Join
                          (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1,
                           u4Idx0), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) CREATE_AND_WAIT);

            i4rc =
                TrSetAll_InternetGatewayDevice_Firewall_fwlDefnDmzTable (NULL,
                                                                         pNode,
                                                                         value,
                                                                         TR_DO_SNMP_SET);
            TR69_TRC (TR69_DBG_TRC, "-E- TrSetMand: SNMP Set failed\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlDefnDmzTable\n");
            return SNMP_FAILURE;
        }
        else
        {
            MEMCPY (pNode, &NewNode, sizeof (NewNode));
        }

        u4Idx0 = pNode->u4IpIndex;
        if (STRCMP (name, "RowStatus") != 0 && (pNode->u4RowStatus == ACTIVE))
        {
            i4rc =
                MynmhSet (*Join
                          (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1,
                           u4Idx0), SNMP_DATA_TYPE_INTEGER, (VOID *) ACTIVE);
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetMand_IGD_Firewall_fwlDefnDmzTable - Success\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrSetAll_InternetGatewayDevice_Firewall_fwlDefnDmzTable
 *  Description     : Set function for all objects of a table.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tFwlDefnDmzTable entry.
 *                    bTrDoSnmpSet - Indicates whether SNMP Set is to be done.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetAll_InternetGatewayDevice_Firewall_fwlDefnDmzTable (char *name,
                                                         tFwlDefnDmzTable *
                                                         pNode,
                                                         ParameterValue * value,
                                                         BOOL1 bTrDoSnmpSet)
{
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    INT4                i4rc = 0;
    UINT4               u4val = 0;
    UINT4               u4Idx0 = 0;
    tUtlInAddr          IpAddr;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    OctetStr.pu1_OctetList = au1OctetList;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetAll_IGD_Firewall_fwlDefnDmzTable\n");
    u4Idx0 = pNode->u4IpIndex;

    if (!name || STRCMP (name, "IpIndex") == 0)
    {
        if (!bTrDoSnmpSet)
        {

            UtlInetAton ((CHR1 *) value->in_cval, &IpAddr);
            pNode->u4IpIndex = OSIX_NTOHL (IpAddr.u4Addr);
            pNode->u4ValMask |= FWL_DMZ_IP_INDEX_MASK;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
    }
    if (!name || STRCMP (name, "RowStatus") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if (value->in_uint == TR_FWL_TRUE)
            {
                pNode->u4RowStatus = u4val = TR_ENABLE_ROWSTATUS;
            }
            else
            {
                pNode->u4RowStatus = u4val = TR_DISABLE_ROWSTATUS;
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
        u4val = pNode->u4RowStatus;
        i4rc =
            MynmhTest (*Join
                       (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1, u4Idx0),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Test failed for FwlDmzRowStatus\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlDmzRowStatus, 12, FwlDefnDmzTableINDEX, 1, u4Idx0),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Set failed for FwlDmzRowStatus\n");
            return SNMP_FAILURE;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetAll_IGD_Firewall_fwlDefnDmzTable - Success\n");
    return SNMP_SUCCESS;
}

/************************End of fwlDefnDmzTable*************************/

/***************************fwlUrlFilterTable***************************/

/************************************************************************
 *  Function Name   : TrScan_FwlUrlFilterTable
 *  Description     : Scans and detects available URL Filters.
 *
 *  Input           : none.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
UINT4
TrScan_FwlUrlFilterTable ()
{
    tSNMP_OCTET_STRING_TYPE NextUrlStrName;
    tSNMP_OCTET_STRING_TYPE UrlStrName;
    tFwlUrlFilterTable *pFwlUrlFilterTable = NULL;
    tFwlUrlFilterTable *pNextfwlUrlFilterTable = NULL;
    UINT1               au1UrlStrName[MAX_OCTETSTRING_SIZE];
    UINT1               au1NextUrlStrName[MAX_OCTETSTRING_SIZE];
    UINT4               u4NewNode = 0;
    INT4                i4rc = 0;
    UINT4               u4TrIdx = 0;

    MEMSET (&NextUrlStrName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&UrlStrName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (au1UrlStrName, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (au1NextUrlStrName, 0, MAX_OCTETSTRING_SIZE);

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrScan_FwlUrlFilterTable\n");

    UrlStrName.pu1_OctetList = au1UrlStrName;
    NextUrlStrName.pu1_OctetList = au1NextUrlStrName;

    i4rc = nmhGetFirstIndexFwlUrlFilterTable (&UrlStrName);
    if (i4rc == SNMP_FAILURE)
    {
        TR69_TRC (TR69_DBG_TRC, "GetFirstIndex failed for FwlUrlFilterTable\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_FwlUrlFilterTable\n");
        return (OSIX_FAILURE);
    }
    STRNCPY (NextUrlStrName.pu1_OctetList, UrlStrName.pu1_OctetList,
             UrlStrName.i4_Length);
    NextUrlStrName.i4_Length = UrlStrName.i4_Length;

    UTL_SLL_OFFSET_SCAN (&FwlUrlFilterTableList, pFwlUrlFilterTable,
                         pNextfwlUrlFilterTable, tFwlUrlFilterTable *)
    {
        pFwlUrlFilterTable->u4Visited = 0;
    }
    while (1)
    {
        pFwlUrlFilterTable = NULL;
        u4NewNode = 1;
        /* Do not instantiate if already present */
        UTL_SLL_OFFSET_SCAN (&FwlUrlFilterTableList, pFwlUrlFilterTable,
                             pNextfwlUrlFilterTable, tFwlUrlFilterTable *)
        {
            if (STRCMP
                (pFwlUrlFilterTable->au1String,
                 NextUrlStrName.pu1_OctetList) == 0)
            {
                u4NewNode = 0;
                pFwlUrlFilterTable->u4Visited = 1;
                break;
            }
        }

        /* Instantiate if this is a new instance */
        if (u4NewNode)
        {
            u4TrIdx = 0;
            i4rc =
                addObjectIntern
                ("InternetGatewayDevice.Firewall.fwlUrlFilterTable",
                 (int *) &u4TrIdx);

            if (i4rc == 0)
            {
                pFwlUrlFilterTable =
                    TrAdd_UrlFilterTable (u4TrIdx, &NextUrlStrName);
                if (pFwlUrlFilterTable == NULL)
                {
                    TR69_TRC (TR69_DBG_TRC, "FwlUrlFilterTable is Null\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrScan_FwlUrlFilterTable\n");
                    return OSIX_FAILURE;
                }
                pFwlUrlFilterTable->u4Visited = 1;
            }
            else
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "addObjectIntern failed, rc = %d\n", i4rc);
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrScan_FwlUrlFilterTable\n");
            }
        }

        /* get the next elem */
        STRNCPY (UrlStrName.pu1_OctetList, NextUrlStrName.pu1_OctetList,
                 NextUrlStrName.i4_Length);
        UrlStrName.i4_Length = NextUrlStrName.i4_Length;
        i4rc = nmhGetNextIndexFwlUrlFilterTable (&UrlStrName, &NextUrlStrName);
        if (i4rc == SNMP_FAILURE)
        {
            break;
        }
    }

    /* Incase if an entry is deleted via CLI or SNMP, in order 
       to synchronize TR database locate the node and initialize 
       the structure  elements to zero */
    UTL_SLL_OFFSET_SCAN (&FwlUrlFilterTableList, pFwlUrlFilterTable,
                         pNextfwlUrlFilterTable, tFwlUrlFilterTable *)
    {
        /* Before initializing  the structure elements to zero, check 
           is made against both visited flag and the table mask in order to make 
           sure that this does not intervene the row creation process */
        if ((pFwlUrlFilterTable->u4Visited == 0) &&
            (pFwlUrlFilterTable->u4ValMask == FWL_URL_FILTER_TABLE_VAL_MASK))
        {
            pFwlUrlFilterTable->u4HitCount = 0;
            pFwlUrlFilterTable->u4RowStatus = 0;
            pFwlUrlFilterTable->u4ValMask = 0;
            MEMSET (pFwlUrlFilterTable->au1String, '\0', 100);
        }
    }

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_FwlUrlFilterTable\n");
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrInitInternetGatewayDevice_Firewall_fwlUrlFilterTable
 *  Description     : Creates a TR Instance for fwlUrlFilterTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrInitInternetGatewayDevice_Firewall_fwlUrlFilterTable (int idx1)
{
    tFwlUrlFilterTable *pFwlUrlFilterTable = NULL;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrInit_IGD_Firewall_fwlUrlFilterTable\n");
    if (idx1 == 0)
    {
        TR69_TRC (TR69_DBG_TRC, "TrInstance is Zero\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrInit_IGD_Firewall_fwlUrlFilterTable\n");
        return SNMP_SUCCESS;
    }
    pFwlUrlFilterTable = TrAdd_UrlFilterTable (idx1, NULL);
    if (pFwlUrlFilterTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "fwlUrlFilterTable is Null\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrInit_IGD_Firewall_fwlUrlFilterTable\n");

        return SNMP_FAILURE;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrInit_IGD_Firewall_fwlUrlFilterTable\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrAdd_UrlFilterTable
 *  Description     : Adds a Url Filter node to TR69.
 *
 *  Input           : u4TrIdx - Tr Instance
 *                    OctetStrIdx - Index.
 *  Output          : None
 *  Returns         : pFwlUrlFilterTable - Allocated UrlFilter node.
 ************************************************************************/
tFwlUrlFilterTable *
TrAdd_UrlFilterTable (UINT4 u4TrIdx, tSNMP_OCTET_STRING_TYPE * pOctetStrIdx)
{
    tFwlUrlFilterTable *pFwlUrlFilterTable = NULL;
    tFwlUrlFilterTable *pNextFwlUrlFilterTable = NULL;

    /* If TrAdd is called via the init function, then 
       allocate the memory and initalize structure members.
       If TrAdd is called via the Scan function, 
       then locate the node, assign the SNMP index 
       and table mask. */
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrAdd_UrlFilterTable\n");
    if (pOctetStrIdx == NULL)
    {
        pFwlUrlFilterTable = MEM_MALLOC (sizeof (tFwlUrlFilterTable),
                                         tFwlUrlFilterTable);
        if (pFwlUrlFilterTable == NULL)
        {
            TR69_TRC (TR69_DBG_TRC, "Memory Allocation - Failed\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_UrlFilterTable\n");
            return NULL;
        }
        MEMSET (pFwlUrlFilterTable, 0, sizeof (tFwlUrlFilterTable));

        pFwlUrlFilterTable->u4TrInstance = u4TrIdx;
        pFwlUrlFilterTable->u4ValMask = 0;
        pFwlUrlFilterTable->u4RowStatus = 0;
        TMO_SLL_Add (&FwlUrlFilterTableList, &pFwlUrlFilterTable->Link);
        TR69_TRC (TR69_DBG_TRC, "Node Allocation - Success\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_UrlFilterTable\n");
        return pFwlUrlFilterTable;
    }

    UTL_SLL_OFFSET_SCAN (&FwlUrlFilterTableList, pFwlUrlFilterTable,
                         pNextFwlUrlFilterTable, tFwlUrlFilterTable *)
    {
        if (pFwlUrlFilterTable->u4TrInstance == u4TrIdx)
        {
            MEMCPY (pFwlUrlFilterTable->au1String, pOctetStrIdx->pu1_OctetList,
                    pOctetStrIdx->i4_Length);
            pFwlUrlFilterTable->u4ValMask = FWL_URL_FILTER_TABLE_VAL_MASK;
            TR69_TRC (TR69_DBG_TRC, "Row Creation - Success\n");
            break;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_UrlFilterTable\n");
    return pFwlUrlFilterTable;
}

/************************************************************************
 *  Function Name   : TrDeleteInternetGatewayDevice_Firewall_fwlUrlFilterTable
 *  Description     : Delete TR Instance for fwlUrlFilterTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrDeleteInternetGatewayDevice_Firewall_fwlUrlFilterTable (int idx1)
{
    tFwlUrlFilterTable *pFwlUrlFilterTable = NULL;
    tFwlUrlFilterTable *pNextfwlUrlFilterTable = NULL;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    INT4                i4rc = 0;

    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrDelete_IGD_Firewall_fwlUrlFilterTable \n");

    UTL_SLL_OFFSET_SCAN (&FwlUrlFilterTableList, pFwlUrlFilterTable,
                         pNextfwlUrlFilterTable, tFwlUrlFilterTable *)
    {
        if (pFwlUrlFilterTable->u4TrInstance == (UINT4) idx1)
        {
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Deleting Url TrInstance:%d String:%s\n",
                           pFwlUrlFilterTable->u4TrInstance,
                           pFwlUrlFilterTable->au1String);

            OctetStrIdx.i4_Length = STRLEN (pFwlUrlFilterTable->au1String);
            MEMCPY (OctetStrIdx.pu1_OctetList, pFwlUrlFilterTable->au1String,
                    OctetStrIdx.i4_Length);
            if (nmhValidateIndexInstanceFwlUrlFilterTable (&OctetStrIdx) ==
                SNMP_SUCCESS)
            {
                i4rc = MynmhTest (*Join
                                  (FwlUrlFilterRowStatus, 12,
                                   FwlUrlFilterTableINDEX, 1, &OctetStrIdx),
                                  SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC, "Test failed for Destroy\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDelete_IGD_Firewall_fwlUrlFilterTable \n");
                    return SNMP_FAILURE;
                }
                i4rc = MynmhSet (*Join
                                 (FwlUrlFilterRowStatus, 12,
                                  FwlUrlFilterTableINDEX, 1, &OctetStrIdx),
                                 SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC, "Set failed for Destroy\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDelete_IGD_Firewall_fwlUrlFilterTable \n");
                    return SNMP_FAILURE;
                }
            }
            TMO_SLL_Delete (&FwlUrlFilterTableList,
                            (tTMO_SLL_NODE *) pFwlUrlFilterTable);
            MEM_FREE (pFwlUrlFilterTable);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrDelete_IGD_Firewall_fwlUrlFilterTable \n");
            return SNMP_SUCCESS;
        }
    }
    /* Instance not present - return SNMP_FAILURE  */
    if (pFwlUrlFilterTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "fwlUrlFilterTable is Null\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrDelete_IGD_Firewall_fwlUrlFilterTable \n");
        return SNMP_FAILURE;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrDelete_IGD_Firewall_fwlUrlFilterTable \n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_Firewall_fwlUrlFilterTable
 *  Description     : Get function for InternetGatewayDevice_Firewall_fwlUrlFilterTable
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrGetInternetGatewayDevice_Firewall_fwlUrlFilterTable (char *name, int idx1,
                                                       ParameterValue * value)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    tFwlUrlFilterTable *pFwlUrlFilterTable = NULL;
    tFwlUrlFilterTable *pNextFwlUrlFilterTable = NULL;
    tFwlUrlFilterTable *pNode = NULL;
    UINT4               u4val = 0;
    INT4                i4rc = 0;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrGet_IGD_Firewall_fwlUrlFilterTable \n");

    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    UTL_SLL_OFFSET_SCAN (&FwlUrlFilterTableList, pFwlUrlFilterTable,
                         pNextFwlUrlFilterTable, tFwlUrlFilterTable *)
    {
        if (pFwlUrlFilterTable->u4TrInstance == (UINT4) idx1)
        {
            OctetStrIdx.i4_Length = STRLEN (pFwlUrlFilterTable->au1String);
            MEMCPY (OctetStrIdx.pu1_OctetList, pFwlUrlFilterTable->au1String,
                    OctetStrIdx.i4_Length);
            if (pFwlUrlFilterTable->u4ValMask == FWL_URL_FILTER_TABLE_VAL_MASK)
            {
                if (nmhValidateIndexInstanceFwlUrlFilterTable (&OctetStrIdx) !=
                    SNMP_SUCCESS)
                {
                    TR69_TRC (TR69_DBG_TRC, "Exit: URL does not exist \r\n");
                    pFwlUrlFilterTable->u4HitCount = 0;
                    pFwlUrlFilterTable->u4RowStatus = 0;
                    pFwlUrlFilterTable->u4ValMask = 0;
                    MEMSET (pFwlUrlFilterTable->au1String, '\0', 100);
                    TR69_TRC_ARG1 (TR69_DBG_TRC,
                                   "nmhValidate failed for TrInstance:%d\n",
                                   pFwlUrlFilterTable->u4TrInstance);
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrGet_IGD_Firewall_fwlUrlFilterTable \n");
                }
            }
            break;
        }
    }

    if (pFwlUrlFilterTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "FwlUrlFilterTable is Null\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrGet_IGD_Firewall_fwlUrlFilterTable \n");
        return -1;
    }

    pNode = pFwlUrlFilterTable;

    if (STRCMP (name, "String") == 0)
    {
        value->out_cval = pNode->au1String;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FwlUrlString ret %s\n",
                       value->out_cval);
        return OK;
    }
    else if (STRCMP (name, "HitCount") == 0)
    {
        if ((pNode->u4ValMask & FWL_URL_FILTER_TABLE_VAL_MASK) ==
            FWL_URL_FILTER_TABLE_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FwlUrlHitCount, 12, FwlUrlFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_COUNTER32, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Get failed for FwlUrlHitCount\n");
                value->out_uint = 0;
                return OK;
            }
            value->out_uint = u4val;
            pNode->u4HitCount = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FwlUrlHitCount ret %d\n", u4val);
            return OK;
        }
        else
        {
            value->out_uint = pNode->u4HitCount;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlUrlHitCount ret %d\n", value->out_uint);
            return OK;
        }
    }
    else if (STRCMP (name, "FilterRowStatus") == 0)
    {
        if ((pNode->u4ValMask & FWL_URL_FILTER_TABLE_VAL_MASK) ==
            FWL_URL_FILTER_TABLE_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Exit: Get failed for FwlUrlFilterRowStatus\n");
                value->out_uint = 0;
                return OK;
            }
            if (u4val == TR_DISABLE_ROWSTATUS)
            {
                value->out_uint = 0;
                pNode->u4RowStatus = TR_DISABLE_ROWSTATUS;
            }
            else
            {
                value->out_uint = 1;
                pNode->u4RowStatus = TR_ENABLE_ROWSTATUS;
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlUrlFilterRowStatus ret %d\n",
                           value->out_uint);
            return OK;
        }
        else
        {
            if (pNode->u4RowStatus == TR_ENABLE_ROWSTATUS)
            {
                value->out_uint = 1;
            }
            else
            {
                value->out_uint = 0;
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FwlUrlFilterRowStatus ret %d\n",
                           value->out_uint);
            return OK;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrGet_IGD_Firewall_fwlUrlFilterTable \n");
    return OK;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_Firewall_fwlUrlFilterTable
 *  Description     : Get function for InternetGatewayDevice_Firewall_fwlUrlFilterTable
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrSetInternetGatewayDevice_Firewall_fwlUrlFilterTable (char *name, int idx1,
                                                       ParameterValue * value)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT4               u4val = 0;
    UINT4               u4MakeActive = 0;
    INT4                i4rc = 0;
    INT4                i4RetVal = 0;

    tFwlUrlFilterTable *pFwlUrlFilterTable = NULL;
    tFwlUrlFilterTable *pNextFwlUrlFilterTable = NULL;
    tFwlUrlFilterTable *pNode = NULL;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSet_IGD_Firewall_fwlUrlFilterTable \n");
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    UTL_SLL_OFFSET_SCAN (&FwlUrlFilterTableList, pFwlUrlFilterTable,
                         pNextFwlUrlFilterTable, tFwlUrlFilterTable *)
    {
        if (pFwlUrlFilterTable->u4TrInstance == (UINT4) idx1)
        {
            OctetStrIdx.i4_Length = STRLEN (pFwlUrlFilterTable->au1String);
            MEMCPY (OctetStrIdx.pu1_OctetList, pFwlUrlFilterTable->au1String,
                    OctetStrIdx.i4_Length);
            if (pFwlUrlFilterTable->u4ValMask == FWL_URL_FILTER_TABLE_VAL_MASK)
            {
                if (nmhValidateIndexInstanceFwlUrlFilterTable (&OctetStrIdx) !=
                    SNMP_SUCCESS)
                {
                    pFwlUrlFilterTable->u4HitCount = 0;
                    pFwlUrlFilterTable->u4RowStatus = 0;
                    pFwlUrlFilterTable->u4ValMask = 0;
                    MEMSET (pFwlUrlFilterTable->au1String, '\0', 100);
                    TR69_TRC_ARG1 (TR69_DBG_TRC,
                                   "nmhValidate failed for TrInstance:%d\n",
                                   pFwlUrlFilterTable->u4TrInstance);
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrSet_IGD_Firewall_fwlUrlFilterTable \n");
                    return SNMP_FAILURE;
                }
            }
            break;
        }
    }

    if (pFwlUrlFilterTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "FwlUrlFilterTable is Null\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrSet_IGD_Firewall_fwlUrlFilterTable \n");

        return SNMP_FAILURE;
    }

    pNode = pFwlUrlFilterTable;

    u4MakeActive = 0;
    if (((pNode->u4ValMask & FWL_URL_FILTER_TABLE_VAL_MASK) ==
         FWL_URL_FILTER_TABLE_VAL_MASK)
        && (STRCMP (name, "FilterRowStatus") != 0))
    {
        u4val = NOT_IN_SERVICE;
        i4rc =
            MynmhTest (*Join
                       (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlUrlFilterRowStatus\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Set failed for FwlUrlFilterRowStatus\n");
            return SNMP_FAILURE;
        }
        u4MakeActive = 1;
    }
    i4RetVal =
        TrSetNonMand_InternetGatewayDevice_Firewall_fwlUrlFilterTable (name,
                                                                       value,
                                                                       pNode);
    if (i4RetVal == TR_NOT_MAND_PARAM)
    {
        i4RetVal =
            TrSetMand_InternetGatewayDevice_Firewall_fwlUrlFilterTable (name,
                                                                        value,
                                                                        pNode);
    }

    if ((u4MakeActive) && (STRCMP (name, "FilterRowStatus") != 0)
        && (pNode->u4RowStatus == ACTIVE))

    {
        u4val = ACTIVE;
        OctetStrIdx.i4_Length = STRLEN (pFwlUrlFilterTable->au1String);
        MEMCPY (OctetStrIdx.pu1_OctetList, pFwlUrlFilterTable->au1String,
                OctetStrIdx.i4_Length);
        i4rc =
            MynmhTest (*Join
                       (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Test failed for FwlUrlFilterRowStatus\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetNonMand: Set failed for FwlUrlFilterRowStatus\n");
            return SNMP_FAILURE;
        }
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrSet_IGD_Firewall_fwlUrlFilterTable - Failure \n");
        return SNMP_FAILURE;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSet_IGD_Firewall_fwlUrlFilterTable - Success \n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrSetNonMand_InternetGatewayDevice_Firewall_fwlUrlFilterTable
 *  Description     : Set function for non-mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tFwlUrlFilterTable entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetNonMand_InternetGatewayDevice_Firewall_fwlUrlFilterTable (char *name,
                                                               ParameterValue *
                                                               value,
                                                               tFwlUrlFilterTable
                                                               * pNode)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    INT4                i4rc = 0;
    UINT4               u4val = 0;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetNonMand_IGD_Firewall_fwlUrlFilterTable\n");

    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    OctetStrIdx.i4_Length = STRLEN (pNode->au1String);
    MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1String, OctetStrIdx.i4_Length);

    if (STRCMP (name, "FilterRowStatus") == 0)
    {
        if ((pNode->u4ValMask & FWL_URL_FILTER_TABLE_VAL_MASK) ==
            FWL_URL_FILTER_TABLE_VAL_MASK)
        {
            if (value->in_uint == TR_FWL_TRUE)
            {
                pNode->u4RowStatus = u4val = TR_ENABLE_ROWSTATUS;
            }
            else
            {
                pNode->u4RowStatus = u4val = TR_DISABLE_ROWSTATUS;
            }
            i4rc = MynmhTest (*Join
                              (FwlUrlFilterRowStatus, 12,
                               FwlUrlFilterTableINDEX, 1, &OctetStrIdx),
                              SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Test failed for FwlUrlFilterRowStatus\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Set failed for FwlUrlFilterRowStatus\n");
                return SNMP_FAILURE;
            }
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlUrlFilterTable - Success\n");
            return SNMP_SUCCESS;
        }
        else
        {
            TrSetAll_InternetGatewayDevice_Firewall_fwlUrlFilterTable (name,
                                                                       pNode,
                                                                       value,
                                                                       !TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_IGD_Firewall_fwlUrlFilterTable\n");
            return SNMP_SUCCESS;
        }

    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetNonMand_IGD_Firewall_fwlUrlFilterTable - Mand Param\n");
    return TR_NOT_MAND_PARAM;
}

/************************************************************************
 *  Function Name   : TrSetMand_InternetGatewayDevice_Firewall_fwlUrlFilterTable
 *  Description     : Set function for mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tfwlUrlFilterTable entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetMand_InternetGatewayDevice_Firewall_fwlUrlFilterTable (char *name,
                                                            ParameterValue *
                                                            value,
                                                            tFwlUrlFilterTable *
                                                            pNode)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    tFwlUrlFilterTable  NewNode;
    INT4                i4rc = 0;
#ifdef TR69MSR_WANTED
    UINT1               u1Tr69MsrStatus = 0;
#endif

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetMand_IGD_Firewall_fwlUrlFilterTable\n");
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    OctetStrIdx.i4_Length = STRLEN (pNode->au1String);
    MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1String, OctetStrIdx.i4_Length);

    if (pNode->u4ValMask != FWL_URL_FILTER_TABLE_VAL_MASK)
    {
        TrSetAll_InternetGatewayDevice_Firewall_fwlUrlFilterTable (name, pNode,
                                                                   value,
                                                                   !TR_DO_SNMP_SET);
        if ((pNode->u4ValMask & FWL_URL_FILTER_TABLE_VAL_MASK) ==
            FWL_URL_FILTER_TABLE_VAL_MASK)
        {
            TR69_TRC (TR69_DBG_TRC, "Creation of new row in Url Table\n");
            OctetStrIdx.i4_Length = STRLEN (pNode->au1String);
            MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1String,
                    OctetStrIdx.i4_Length);

            i4rc =
                MynmhTest (*Join
                           (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX,
                            1, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) CREATE_AND_WAIT);
            if (i4rc == SNMP_FAILURE)
            {
#ifdef TR69MSR_WANTED
                Tr69MsrGetRestoreStatus (&u1Tr69MsrStatus);
                if (u1Tr69MsrStatus == OSIX_TRUE)
                {
                    return SNMP_SUCCESS;
                }
#endif
                pNode->u4ValMask = 0;
                pNode->u4RowStatus = 0;
                pNode->u4HitCount = 0;
                MEMSET (pNode->au1String, '\0', 100);
                TR69_TRC (TR69_DBG_TRC,
                          "-E-TrSetMand: Test for CREATE_AND_WAIT failed\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlUrlFilterTable\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) CREATE_AND_WAIT);
            if (i4rc == SNMP_FAILURE)
            {
                pNode->u4ValMask = 0;
                pNode->u4RowStatus = 0;
                pNode->u4HitCount = 0;
                MEMSET (pNode->au1String, '\0', 100);
                TR69_TRC (TR69_DBG_TRC,
                          "-E-TrSetMand: Set for CREATE_AND_WAIT failed\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlUrlFilterTable\n");
                return SNMP_FAILURE;
            }
            else
            {
                pNode->u4RowStatus = NOT_READY;
            }
            i4rc =
                TrSetAll_InternetGatewayDevice_Firewall_fwlUrlFilterTable (NULL,
                                                                           pNode,
                                                                           value,
                                                                           TR_DO_SNMP_SET);
            if (i4rc == SNMP_FAILURE)
            {

                MynmhSet (*Join
                          (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) DESTROY);
                pNode->u4ValMask = 0;
                pNode->u4RowStatus = 0;
                pNode->u4HitCount = 0;
                MEMSET (pNode->au1String, '\0', 100);
                TR69_TRC (TR69_DBG_TRC, "-E-TrSetMand: SNMP Set failed\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlUrlFilterTable\n");
                return SNMP_FAILURE;
            }
            i4rc =
                MynmhSet (*Join
                          (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) ACTIVE);
            if (i4rc == SNMP_SUCCESS)
            {
                pNode->u4RowStatus = ACTIVE;
            }
            else
            {
                MynmhSet (*Join
                          (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) DESTROY);
                pNode->u4ValMask = 0;
                pNode->u4RowStatus = 0;
                pNode->u4HitCount = 0;
                MEMSET (pNode->au1String, '\0', 100);
                TR69_TRC (TR69_DBG_TRC,
                          "-E-TrSetMand: Test for ACTIVE failed\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_IGD_Firewall_fwlUrlFilterTable\n");
                return SNMP_FAILURE;
            }

        }
    }
    else
    {
        MEMCPY (&NewNode, pNode, sizeof (*pNode));

        i4rc =
            MynmhTest (*Join
                       (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) DESTROY);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E-TrSetMand: Test for DESTROY failed for URL:%s\n",
                           OctetStrIdx.pu1_OctetList);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlUrlFilterTable\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E-TrSetMand: Set for DESTROY failed for URL:%s\n",
                           OctetStrIdx.pu1_OctetList);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlUrlFilterTable\n");
            return SNMP_FAILURE;
        }

        if (STRCMP (name, "String") == 0)
        {
            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStrIdx.pu1_OctetList, value->in_cval,
                    OctetStrIdx.i4_Length);
        }

        i4rc =
            MynmhTest (*Join
                       (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) CREATE_AND_GO);
        if (i4rc == SNMP_FAILURE)
        {
            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (pNode->au1String);
            MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1String,
                    OctetStrIdx.i4_Length);

            i4rc =
                MynmhSet (*Join
                          (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) CREATE_AND_GO);

            TrSetAll_InternetGatewayDevice_Firewall_fwlUrlFilterTable (NULL,
                                                                       pNode,
                                                                       value,
                                                                       TR_DO_SNMP_SET);
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E-TrSetMand: Test for CREATE_AND_GO failed for URL:%s\n",
                           OctetStrIdx.pu1_OctetList);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlUrlFilterTable\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) CREATE_AND_GO);

        if (i4rc == SNMP_FAILURE)
        {

            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (pNode->au1String);
            MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1String,
                    OctetStrIdx.i4_Length);

            i4rc =
                MynmhSet (*Join
                          (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) CREATE_AND_GO);

            TrSetAll_InternetGatewayDevice_Firewall_fwlUrlFilterTable (NULL,
                                                                       pNode,
                                                                       value,
                                                                       TR_DO_SNMP_SET);
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E-TrSetMand: Set for CREATE_AND_GO failed for URL:%s\n",
                           OctetStrIdx.pu1_OctetList);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlUrlFilterTable\n");
            return SNMP_FAILURE;
        }
        /* First update NewNode with the new value */
        /* Then SET all the values for the new row */
        i4rc =
            TrSetAll_InternetGatewayDevice_Firewall_fwlUrlFilterTable (name,
                                                                       &NewNode,
                                                                       value,
                                                                       !TR_DO_SNMP_SET);
        i4rc =
            TrSetAll_InternetGatewayDevice_Firewall_fwlUrlFilterTable (NULL,
                                                                       &NewNode,
                                                                       value,
                                                                       TR_DO_SNMP_SET);
        if (i4rc == SNMP_FAILURE)
        {
            i4rc =
                MynmhSet (*Join
                          (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) DESTROY);

            OctetStrIdx.i4_Length = STRLEN (pNode->au1String);
            MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1String,
                    OctetStrIdx.i4_Length);

            i4rc =
                MynmhSet (*Join
                          (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) CREATE_AND_WAIT);
            i4rc =
                TrSetAll_InternetGatewayDevice_Firewall_fwlUrlFilterTable (NULL,
                                                                           pNode,
                                                                           value,
                                                                           TR_DO_SNMP_SET);
            TR69_TRC (TR69_DBG_TRC, "-E-TrSetMand: SNMP Set failed\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_IGD_Firewall_fwlUrlFilterTable\n");
            return SNMP_FAILURE;
        }
        else
        {
            MEMCPY (pNode, &NewNode, sizeof (NewNode));
        }

        MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
        OctetStrIdx.i4_Length = STRLEN (pNode->au1String);
        MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1String,
                OctetStrIdx.i4_Length);

        if ((STRCMP (name, "FilterRowStatus") != 0)
            && (pNode->u4RowStatus == ACTIVE))
        {
            i4rc =
                MynmhSet (*Join
                          (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) ACTIVE);
        }

    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetMand_IGD_Firewall_fwlUrlFilterTable - Success\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrSetAll_InternetGatewayDevice_Firewall_fwlUrlFilterTable
 *  Description     : Set function for all objects of a table.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tFwlUrlFilterTable entry.
 *                    bTrDoSnmpSet - Indicates whether SNMP Set is to be done.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetAll_InternetGatewayDevice_Firewall_fwlUrlFilterTable (char *name,
                                                           tFwlUrlFilterTable *
                                                           pNode,
                                                           ParameterValue *
                                                           value,
                                                           BOOL1 bTrDoSnmpSet)
{
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    INT4                i4rc = 0;
    UINT4               u4val = 0;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetAll_IGD_Firewall_fwlUrlFilterTable\n");

    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    OctetStrIdx.i4_Length = STRLEN (pNode->au1String);
    MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1String, OctetStrIdx.i4_Length);

    if (!name || STRCMP (name, "String") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            MEMSET (pNode->au1String, '\0', 100);
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMSET (OctetStr.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1String, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            pNode->u4ValMask |= FWL_URL_STRING_MASK;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
    }
    if (!name || STRCMP (name, "FilterRowStatus") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if (value->in_uint == TR_FWL_TRUE)
            {
                pNode->u4RowStatus = u4val = TR_ENABLE_ROWSTATUS;
            }
            else
            {
                pNode->u4RowStatus = u4val = TR_DISABLE_ROWSTATUS;
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Exit: TrSetAll - cached param:%s\n", name);
            return SNMP_SUCCESS;
        }
        u4val = pNode->u4RowStatus;
        i4rc =
            MynmhTest (*Join
                       (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Test failed for FwlUrlFilterRowStatus\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FwlUrlFilterRowStatus, 12, FwlUrlFilterTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Test failed for FwlUrlFilterRowStatus\n");
            return SNMP_FAILURE;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetAll_IGD_Firewall_fwlUrlFilterTable - Success\n");
    return SNMP_SUCCESS;
}

/************************End of fwlDefnUrlTable*************************/

/***********************End of fwlDefinition group**********************/

/************************************************************************
*                       fwlStatistics group begins                      *
************************************************************************/

/************************************************************************
 *  Function Name   : InternetGatewayDevice_Firewall_fwlStatistics
 *  Description     : Get function for InternetGatewayDevice_Firewall_fwlStatistics
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrGetInternetGatewayDevice_Firewall_fwlStatistics (char *name,
                                                   ParameterValue * value)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT4               u4val = 0;
    INT4                i4rc = 0;
    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrGetInternetGatewayDevice_Firewall_fwlStatistics\n");

    OctetStr.pu1_OctetList = au1OctetList;
    if (STRCMP (name, "TotalSrcRoutePacketsDenied") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlStatTotalSrcRoutePacketsDenied, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlStatTotalSrcRoutePacketsDenied\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_uint = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlStatTotalSrcRoutePacketsDenied ret %d\n",
                       u4val);
        return OK;
    }
    else if (STRCMP (name, "TotalPacketsAccepted") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlStatTotalPacketsAccepted, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlStatTotalPacketsAccepted\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_uint = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlStatTotalPacketsAccepted ret %d\n", u4val);
        return OK;
    }
    else if (STRCMP (name, "TotalIpOptionPacketsDenied") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlStatTotalIpOptionPacketsDenied, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlStatTotalIpOptionPacketsDenied\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_uint = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlStatTotalIpOptionPacketsDenied ret %d\n",
                       u4val);
        return OK;
    }
    else if (STRCMP (name, "TotalSynPacketsDenied") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlStatTotalSynPacketsDenied, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlStatTotalSynPacketsDenied\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_uint = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlStatTotalSynPacketsDenied ret %d\n", u4val);
        return OK;
    }
    else if (STRCMP (name, "TotalFragmentedPacketsDenied") == 0)
    {
        i4rc =
            MynmhGet (*Join
                      (FwlStatTotalFragmentedPacketsDenied, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlStatTotalFragmentedPacketsDenied\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_uint = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlStatTotalFragmentedPacketsDenied ret %d\n",
                       u4val);
        return OK;
    }
    else if (STRCMP (name, "InspectedPacketsCount") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlStatInspectedPacketsCount, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlStatInspectedPacketsCount\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_uint = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlStatInspectedPacketsCount ret %d\n", u4val);
        return OK;
    }
    else if (STRCMP (name, "TotalIcmpPacketsDenied") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlStatTotalIcmpPacketsDenied, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlStatTotalIcmpPacketsDenied\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_uint = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlStatTotalIcmpPacketsDenied ret %d\n", u4val);
        return OK;
    }
    else if (STRCMP (name, "TotalPacketsDenied") == 0)
    {
        i4rc =
            MynmhGet (*Join (FwlStatTotalPacketsDenied, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlStatTotalPacketsDenied\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_uint = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlStatTotalPacketsDenied ret %d\n", u4val);
        return OK;
    }
    else if (STRCMP (name, "TotalIpSpoofedPacketsDenied") == 0)
    {
        i4rc =
            MynmhGet (*Join
                      (FwlStatTotalIpSpoofedPacketsDenied, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlStatTotalIpSpoofedPacketsDenied\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_uint = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlStatTotalIpSpoofedPacketsDenied ret %d\n",
                       u4val);
        return OK;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrGetInternetGatewayDevice_Firewall_fwlStatistics\n");
    return OK;
}

/*****************************fwlStatIfTable****************************/

/************************************************************************
 *  Function Name   : TrScan_FwlStatIfTable
 *  Description     : Scans and detects available Interface .
 *
 *  Input           : none.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
UINT4
TrScan_FwlStatIfTable ()
{
    tFwlStatIfTable    *pFwlStatIfTable = NULL;
    tFwlStatIfTable    *pNextfwlStatIfTable = NULL;
    INT4                i4FwlStatIfIndex = 0;
    INT4                i4NextFwlStatIfIndex = 0;
    UINT4               u4NewNode = 0;
    INT4                i4rc = 0;
    UINT4               u4TrIdx = 0;
    UINT4              *ptr = NULL;
    UINT4               u4Size = 0;

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrScan_FwlStatIfTable\n");
    i4rc = nmhGetFirstIndexFwlStatIfTable (&i4FwlStatIfIndex);
    if (i4rc == SNMP_FAILURE)
    {
        TR69_TRC (TR69_DBG_TRC, "GetFirstIndex failed for FwlStatIfTable\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_FwlStatIfTable\n");
        return (OSIX_FAILURE);
    }
    i4NextFwlStatIfIndex = i4FwlStatIfIndex;

    UTL_SLL_OFFSET_SCAN (&FwlStatIfTableList, pFwlStatIfTable,
                         pNextfwlStatIfTable, tFwlStatIfTable *)
    {
        pFwlStatIfTable->u4Visited = 0;
    }
    while (1)
    {
        pFwlStatIfTable = NULL;
        u4NewNode = 1;

        /* Do not instantiate if already present */
        UTL_SLL_OFFSET_SCAN (&FwlStatIfTableList, pFwlStatIfTable,
                             pNextfwlStatIfTable, tFwlStatIfTable *)
        {
            if (pFwlStatIfTable->i4IfIfIndex == i4NextFwlStatIfIndex)
            {
                u4NewNode = 0;
                pFwlStatIfTable->u4Visited = 1;
                break;
            }
        }

        /* Instantiate if this is a new instance */
        if (u4NewNode)
        {
            u4TrIdx = 0;
            i4rc =
                addObjectIntern
                ("InternetGatewayDevice.Firewall.fwlStatIfTable",
                 (int *) &u4TrIdx);
            if (i4rc == 0)
            {
                pFwlStatIfTable =
                    TrAdd_FwlStatIfTable (u4TrIdx, i4NextFwlStatIfIndex);
                pFwlStatIfTable->u4Visited = 1;
                if (pFwlStatIfTable == NULL)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- TrScan: FwlStatIfTable is Null\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrScan_FwlStatIfTable\n");
                    return OSIX_FAILURE;
                }
            }
            else
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "addObjectIntern failed, rc = %d\n", i4rc);
            }
        }

        i4FwlStatIfIndex = i4NextFwlStatIfIndex;
        i4rc =
            nmhGetNextIndexFwlStatIfTable (i4FwlStatIfIndex,
                                           &i4NextFwlStatIfIndex);
        if (i4rc == SNMP_FAILURE)
        {
            break;
        }
    }

    UTL_SLL_OFFSET_SCAN (&FwlStatIfTableList, pFwlStatIfTable,
                         pNextfwlStatIfTable, tFwlStatIfTable *)
    {
        if (pFwlStatIfTable->u4Visited == 0)
        {
            ptr = &(pFwlStatIfTable->u4ValMask);
            u4Size =
                sizeof (tFwlStatIfTable) - (sizeof (UINT4) +
                                            sizeof (tTMO_SLL_NODE));
            MEMSET (ptr, 0, u4Size);

        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_FwlStatIfTable\n");
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrAdd_FwlStatIfTable
 *  Description     : Adds an Interface node to TR69.
 *
 *  Input           : u4TrIdx - Tr Instance
 *                    u4IfIdx - CFA i/f index.
 *  Output          : None
 *  Returns         : pFwlStatIfTable - Allocated Interface node.
 ************************************************************************/
tFwlStatIfTable    *
TrAdd_FwlStatIfTable (UINT4 u4TrIdx, UINT4 u4IfIdx)
{
    tFwlStatIfTable    *pFwlStatIfTable = NULL;

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrAdd_FwlStatIfTable\n");

    pFwlStatIfTable = MEM_MALLOC (sizeof (tFwlStatIfTable), tFwlStatIfTable);
    if (pFwlStatIfTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "Memory Allocation - Failed\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_FwlStatIfTable\n");
        return NULL;
    }

    MEMSET (pFwlStatIfTable, 0, sizeof (tFwlStatIfTable));
    pFwlStatIfTable->u4TrInstance = u4TrIdx;
    pFwlStatIfTable->i4IfIfIndex = u4IfIdx;

    pFwlStatIfTable->u4ValMask = FWL_STAT_IF_TABLE_VAL_MASK;

    TMO_SLL_Add (&FwlStatIfTableList, &pFwlStatIfTable->Link);

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_FwlStatIfTable\n");
    return pFwlStatIfTable;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_Firewall_fwlStatIfTable
 *  Description     : Get function for InternetGatewayDevice_Firewall_fwlStatIfTable
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrGetInternetGatewayDevice_Firewall_fwlStatIfTable (char *name, int idx1,
                                                    ParameterValue * value)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT4               u4val = 0;
    INT4                i4rc = 0;
    UINT4               u4Idx0 = 0;
    UINT4              *ptr = NULL;
    UINT4               u4Size = 0;

    tFwlStatIfTable    *pFwlStatIfTable = NULL;
    tFwlStatIfTable    *pNextFwlStatIfTable = NULL;
    tFwlStatIfTable    *pNode;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrGet_IGD_Firewall_fwlStatIfTable\n");

    OctetStr.pu1_OctetList = au1OctetList;
    UTL_SLL_OFFSET_SCAN (&FwlStatIfTableList, pFwlStatIfTable,
                         pNextFwlStatIfTable, tFwlStatIfTable *)
    {
        if (pFwlStatIfTable->u4TrInstance == (UINT4) idx1)
        {
            u4Idx0 = (UINT4) pFwlStatIfTable->i4IfIfIndex;
            i4rc = nmhValidateIndexInstanceFwlStatIfTable (u4Idx0);
            if (i4rc == SNMP_FAILURE)
            {
                ptr = &(pFwlStatIfTable->u4ValMask);
                u4Size =
                    sizeof (tFwlStatIfTable) - (sizeof (UINT4) +
                                                sizeof (tTMO_SLL_NODE));
                MEMSET (ptr, 0, u4Size);
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "nmhValidate failed for TrInstance:%d\n",
                               pFwlStatIfTable->u4TrInstance);
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrGet_IGD_Firewall_fwlStatIfTable\n");
            }
            break;
        }
    }

    if (pFwlStatIfTable == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "-E- TrGet: StatIfTable is Null\n");

        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrGet_IGD_Firewall_fwlStatIfTable\n");
        return -1;
    }

    pNode = pFwlStatIfTable;

    if (STRCMP (name, "IfIpSpoofedPacketsDenied") == 0)
    {
        i4rc =
            MynmhGet (*Join
                      (FwlStatIfIpSpoofedPacketsDenied, 12, FwlStatIfTableINDEX,
                       1, u4Idx0), SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlStatIfIpSpoofedPacketsDenied\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_uint = u4val;
        pNode->u4IfIpSpoofedPacketsDenied = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlStatIfIpSpoofedPacketsDenied ret %d\n", u4val);
        return OK;
    }
    else if (STRCMP (name, "IfSynPacketsDenied") == 0)
    {
        i4rc =
            MynmhGet (*Join
                      (FwlStatIfSynPacketsDenied, 12, FwlStatIfTableINDEX, 1,
                       u4Idx0), SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlStatIfSynPacketsDenied\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_uint = u4val;
        pNode->u4IfSynPacketsDenied = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlStatIfSynPacketsDenied ret %d\n", u4val);
        return OK;
    }
    else if (STRCMP (name, "IfPacketsAccepted") == 0)
    {
        i4rc =
            MynmhGet (*Join
                      (FwlStatIfPacketsAccepted, 12, FwlStatIfTableINDEX, 1,
                       u4Idx0), SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlStatIfPacketsAccepted\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_uint = u4val;
        pNode->u4IfPacketsAccepted = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlStatIfPacketsAccepted ret %d\n", u4val);
        return OK;
    }
    else if (STRCMP (name, "IfFragmentPacketsDenied") == 0)
    {
        i4rc =
            MynmhGet (*Join
                      (FwlStatIfFragmentPacketsDenied, 12, FwlStatIfTableINDEX,
                       1, u4Idx0), SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlStatIfFragmentPacketsDenied\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_uint = u4val;
        pNode->u4IfFragmentPacketsDenied = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlStatIfFragmentPacketsDenied ret %d\n", u4val);
        return OK;
    }
    else if (STRCMP (name, "IfIfIndex") == 0)
    {
        value->out_int = u4Idx0;
        pNode->i4IfIfIndex = u4Idx0;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FwlStatIfIfIndex ret %d\n", u4val);
        return OK;
    }
    else if (STRCMP (name, "IfFilterCount") == 0)
    {
        i4rc =
            MynmhGet (*Join
                      (FwlStatIfFilterCount, 12, FwlStatIfTableINDEX, 1,
                       u4Idx0), SNMP_DATA_TYPE_INTEGER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlStatIfFilterCount\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_int = u4val;
        pNode->i4IfFilterCount = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FwlStatIfFilterCount ret %d\n",
                       u4val);
        return OK;
    }
    else if (STRCMP (name, "IfPacketsDenied") == 0)
    {
        i4rc =
            MynmhGet (*Join
                      (FwlStatIfPacketsDenied, 12, FwlStatIfTableINDEX, 1,
                       u4Idx0), SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlStatIfPacketsDenied\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_uint = u4val;
        pNode->u4IfPacketsDenied = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FwlStatIfPacketsDenied ret %d\n",
                       u4val);
        return OK;
    }
    else if (STRCMP (name, "IfIpOptionPacketsDenied") == 0)
    {
        i4rc =
            MynmhGet (*Join
                      (FwlStatIfIpOptionPacketsDenied, 12, FwlStatIfTableINDEX,
                       1, u4Idx0), SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlStatIfIpOptionPacketsDenied\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_uint = u4val;
        pNode->u4IfIpOptionPacketsDenied = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlStatIfIpOptionPacketsDenied ret %d\n", u4val);
        return OK;
    }
    else if (STRCMP (name, "IfSrcRoutePacketsDenied") == 0)
    {
        i4rc =
            MynmhGet (*Join
                      (FwlStatIfSrcRoutePacketsDenied, 12, FwlStatIfTableINDEX,
                       1, u4Idx0), SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlStatIfSrcRoutePacketsDenied\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_uint = u4val;
        pNode->u4IfSrcRoutePacketsDenied = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlStatIfSrcRoutePacketsDenied ret %d\n", u4val);
        return OK;
    }
    else if (STRCMP (name, "IfIcmpPacketsDenied") == 0)
    {
        i4rc =
            MynmhGet (*Join
                      (FwlStatIfIcmpPacketsDenied, 12, FwlStatIfTableINDEX, 1,
                       u4Idx0), SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlStatIfIcmpPacketsDenied\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_uint = u4val;
        pNode->u4IfIcmpPacketsDenied = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlStatIfIcmpPacketsDenied ret %d\n", u4val);
        return OK;
    }
    else if (STRCMP (name, "IfTinyFragmentPacketsDenied") == 0)
    {
        i4rc =
            MynmhGet (*Join
                      (FwlStatIfTinyFragmentPacketsDenied, 12,
                       FwlStatIfTableINDEX, 1, u4Idx0),
                      SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Exit: Get failed for FwlStatIfTinyFragmentPacketsDenied\n");
            value->out_uint = 0;
            return OK;
        }
        value->out_uint = u4val;
        pNode->u4IfTinyFragmentPacketsDenied = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FwlStatIfTinyFragmentPacketsDenied ret %d\n",
                       u4val);
        return OK;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrGet_IGD_Firewall_fwlStatIfTable\n");
    return OK;
}

/************************End of fwlStatIfTable****************************/

/************************End of fwlStatistics group***********************/
