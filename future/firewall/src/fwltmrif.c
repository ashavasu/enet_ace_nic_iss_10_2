/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwltmrif.c,v 1.5 2014/01/02 10:25:23 siva Exp $
 *
 * Description:This file is used to create dynamic authen 
 *             filters, when a new user gets authenticated
 *             through a preliminary telnet session.      
 *
 *******************************************************************/
#include "fwlinc.h"

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTmrInitTmr                                    */
/*                                                                          */
/*    Description        : This function initialises all the timer related  */
/*                         data structures and fields.                      */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlTmrInitTmr (VOID)
#else
PUBLIC VOID
FwlTmrInitTmr ()
#endif
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Entering FwlTmrInitTmr \n");

    /* Create a timer list for firewall */
    if (TmrCreateTimerList
        ((const UINT1 *) CFA_TASK_NAME, FWL_TIMER_EXPIRY_EVENT, NULL,
         (tTimerListId *) (VOID *) &gu4FwlTmrListId) == TMR_FAILURE)
    {
        /* Print the tmr creation failure on the console */
        MOD_TRC (gFwlAclInfo.u4FwlTrc, OS_RESOURCE_TRC, "FWL",
                 "\n Timer List Creation Failure\n");
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exit-ing FwlTmrInitTmr \n");

}                                /* End of the fn. FwlTmrInitTmr */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTmrDeInitTmr                                  */
/*                                                                          */
/*    Description        : This function initialises all the timer related  */
/*                         data structures and fields.                      */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlTmrDeInitTmr (VOID)
#else
PUBLIC VOID
FwlTmrDeInitTmr ()
#endif
{
    MOD_TRC (gFwlAclInfo.u4FwlTrc, OS_RESOURCE_TRC, "FWL",
             "\n Timer List Deletion\n");
    TmrDeleteTimerList ((tTimerListId) gu4FwlTmrListId);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTmrHandleTmrExpiry                            */
/*                                                                          */
/*    Description        : This function takes the necessary actions when a */
/*                         timer expiry event occurs and this is invoked by */
/*                         forwarding module (CFA or IP) whenever the timer */
/*                         event occurs.                                    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

PUBLIC VOID
FwlTmrHandleTmrExpiry (VOID)
{
    tTmrAppTimer       *pExpTmr = NULL;
    UINT4               u4TmrId = FWL_ZERO;

    pExpTmr = (tTmrAppTimer *) NULL;
    FWL_DBG (FWL_DBG_ENTRY, "\n Entering FwlTmrHandleTmrExpiry \n");

    /* Get the expired timer from the firewall timer list */
    if (TmrGetExpiredTimers ((tTimerListId) gu4FwlTmrListId, &pExpTmr) ==
        TMR_SUCCESS)
    {
        while (pExpTmr != NULL)
        {
            if (gu1FirewallStatus == FWL_DISABLE)
            {
                pExpTmr =
                    TmrGetNextExpiredTimer ((tTimerListId) gu4FwlTmrListId);
                continue;
            }
            u4TmrId = pExpTmr->u4Data;

            switch (u4TmrId)
            {
                case FWL_TCP_INTERCEPT_TIMER:
                    /* Reset the TCP SYN limit packets count and reset 
                     * the timer. 
                     */
                    gFwlAclInfo.u2SynPktsAllowed = FWL_ZERO;
                    gFwlAclInfo.u2RtrSynPktsAllowed = FWL_ZERO;
                    gFwlAclInfo.u2UdpPktsAllowed = FWL_ZERO;
                    gFwlAclInfo.u2IcmpPktsAllowed = FWL_ZERO;
                    if (gFwlAclInfo.u1TcpInterceptEnable == FWL_ENABLE)
                    {
                        if (TmrStartTimer
                            ((tTimerListId) gu4FwlTmrListId, pExpTmr,
                             (gFwlAclInfo.u2TcpInterceptTimeOut *
                              FWL_SYS_TIME_FACTOR)) == FWL_ZERO)
                        {
                            MOD_TRC (gFwlAclInfo.u4FwlTrc, OS_RESOURCE_TRC,
                                     "FWL",
                                     "\nTcp Intercept Timer Expiry-Timer restarted \n");
                        }
                    }
                    break;

                case FWL_UDP_INTERCEPT_TIMER:
                    /* Reset UDP packets counter and Re-start timer. */
                    gFwlAclInfo.u2UdpPktsAllowed = FWL_ZERO;

                    if (gFwlAclInfo.u1UdpFloodInspect == FWL_ENABLE)
                    {
                        if (TmrStartTimer
                            ((tTimerListId) gu4FwlTmrListId, pExpTmr,
                             (gFwlAclInfo.u2UdpInterceptTimeOut *
                              FWL_SYS_TIME_FACTOR)) == FWL_ZERO)
                        {
                            MOD_TRC (gFwlAclInfo.u4FwlTrc, OS_RESOURCE_TRC,
                                     "FWL",
                                     "\nUdp Intercept Timer Expiry-Timer restarted \n");
                        }
                    }
                    break;

                case FWL_ICMP_INTERCEPT_TIMER:
                    /* Reset the ICMP pkts counter and 
                     * Re-start the timer. */
                    gFwlAclInfo.u2IcmpPktsAllowed = FWL_ZERO;
                    if (gFwlAclInfo.u1IcmpFloodInspect == FWL_ENABLE)
                    {
                        if (TmrStartTimer
                            ((tTimerListId) gu4FwlTmrListId, pExpTmr,
                             (gFwlAclInfo.u2IcmpInterceptTimeOut *
                              FWL_SYS_TIME_FACTOR)) == FWL_ZERO)
                        {
                            MOD_TRC (gFwlAclInfo.u4FwlTrc, OS_RESOURCE_TRC,
                                     "FWL",
                                     "\nIcmp Intercept Timer Expiry-Timer restarted \n");
                        }
                    }
                    break;

                case FWL_IDLE_TIMER:
                    MOD_TRC (gFwlAclInfo.u4FwlTrc, OS_RESOURCE_TRC, "FWL",
                             "\nFirewall Idle Timer restarted \n");

#ifndef SECURITY_KERNEL_WANTED
                    FwlLock ();
                    FwlGarbageCollect (FWL_DONT_FLUSH, FWL_DONT_COMMIT);
                    FwlUnLock ();
#else
                    /* For kernel compilation we need to take the Secv4KernelLock() and OsixBHDisable.
                     * Without this change, kernel debug messages will appear when packets are pumped
                     * at higher rates.*/
                    Secv4KernelLock ();
                    OsixBHDisable ();
                    FwlGarbageCollect (FWL_DONT_FLUSH, FWL_DONT_COMMIT);
                    OsixBHEnable ();
                    Secv4KernelUnLock ();
#endif

                    FwlTmrSetTimer (&gFwlStatefulTmr,
                                    FWL_IDLE_TIMER, FWL_IDLE_TIMEOUT);
                    break;

                default:
                    break;
            }                    /* End of the Switch case */

            pExpTmr = TmrGetNextExpiredTimer ((tTimerListId) gu4FwlTmrListId);
        }                        /* End of the while loop */
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlTmrHandleTmrExpiry \n");

}                                /* End of fn. FwlTmrHandleTmrExpiry */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTmrRestartTmr                                 */
/*                                                                          */
/*    Description        : This function is used to stop the current timer  */
/*                         and restart the timer for the new time out period*/
/*                                                                          */
/*    Input(s)           : pTimer   -- Pointer to the timer node            */
/*                         u1TmrId  -- The identification number of timer   */
/*                         u4Interval - The duration to which the timer has */
/*                                      to be set.                          */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlTmrRestartTmr (tTmrAppTimer * pTimer, UINT1 u1TmrId, UINT4 u4Interval)
#else
PUBLIC VOID
FwlTmrRestartTmr (pTimer, u1TmrId, u4Interval)
     tTmrAppTimer       *pTimer;
     UINT1               u1TmrId;
     UINT4               u4Interval;
#endif
{

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering FwlTmrRestartTmr \n");

    /* Delete the timer from the timer list */
    if (TmrStopTimer ((tTimerListId) gu4FwlTmrListId, pTimer) == TMR_SUCCESS)
    {

        /* Restart the timer */
        pTimer->u4Data = (UINT4) u1TmrId;
        if (TmrStartTimer ((tTimerListId) gu4FwlTmrListId, pTimer,
                           (u4Interval * FWL_SYS_TIME_FACTOR)) == TMR_FAILURE)
        {
            /* print the error message on the console */
            MOD_TRC (gFwlAclInfo.u4FwlTrc, OS_RESOURCE_TRC, "FWL",
                     "\n Timer Cannot be restarted\n");
        }
    }
    else
    {
        /* print the error message on the console */
        MOD_TRC (gFwlAclInfo.u4FwlTrc, OS_RESOURCE_TRC, "FWL",
                 "\n Timer Cannot be restarted\n");
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlTmrRestartTmr \n");

}                                /* End of fn. FwlTmrRestartTmr */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTmrSetTimer                                   */
/*                                                                          */
/*    Description        : This function is used to Start a new timer       */
/*                                                                          */
/*    Input(s)           : pTimer   -- Pointer to the timer node            */
/*                         u1TmrId  -- The identification number of timer   */
/*                         u4Interval - The duration to which the timer has */
/*                                      to be set.                          */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlTmrSetTimer (tTmrAppTimer * pTimer, UINT1 u1TmrId, UINT4 u4Interval)
#else
PUBLIC VOID
FwlTmrSetTimer (pTimer, u1TmrId, u4Interval)
     tTmrAppTimer       *pTimer;
     UINT1               u1TmrId;
     UINT4               u4Interval;
#endif
{
    pTimer->u4Data = (UINT4) u1TmrId;
    if (TmrStartTimer ((tTimerListId) gu4FwlTmrListId, pTimer,
                       (u4Interval * FWL_SYS_TIME_FACTOR)) == TMR_FAILURE)
    {
        MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, OS_RESOURCE_TRC, "FWL",
                      "\n Timer with timer ID %d "
                      "is started for the interval %d \n", u1TmrId, u4Interval);
        /* Printf the error message over the console */
    }
}                                /* End of fn. FwlTmrSetTimer */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTmrDeleteTimer                                */
/*                                                                          */
/*    Description        : This function is used to Deletes existing timer  */
/*                                                                          */
/*    Input(s)           : pTimer   -- Pointer to the timer node            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlTmrDeleteTimer (tTmrAppTimer * pTimer)
#else
PUBLIC VOID
FwlTmrDeleteTimer (pTimer)
     tTmrAppTimer       *pTimer;
#endif
{
    if (TmrStopTimer ((tTimerListId) gu4FwlTmrListId, pTimer) == TMR_FAILURE)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, OS_RESOURCE_TRC, "FWL",
                 "\n Timer Cannot be stopped\n");
        /* Printf the error message over the console */
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, OS_RESOURCE_TRC, "FWL",
                 "\n Timer stopped\n");
    }
}                                /* End of fn. FwlTmrDeleteTimer */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTmrDeleteAllTimers                            */
/*                                                                          */
/*    Description        : This function is used to Delete all the existing */
/*                         timers.                                          */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlTmrDeleteAllTimers (VOID)
#else
PUBLIC VOID
FwlTmrDeleteAllTimers (VOID)
#endif
{
    FwlTmrDeleteTimer (&gFwlAclInfo.TcpInterceptTimer);
    FwlTmrDeleteTimer (&gFwlAclInfo.UdpInterceptTimer);
    FwlTmrDeleteTimer (&gFwlAclInfo.IcmpInterceptTimer);
    FwlTmrDeleteTimer (&gFwlStatefulTmr);
    FwlGarbageCollect (FWL_FLUSH_STATEFUL_ENTRIES, FWL_DONT_COMMIT);
}                                /* End of fn. FwlTmrDeleteAllTimers */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlRestartIdleTimer                              */
/*                                                                          */
/*    Description        : This function is used to restart the idle timer  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
FwlRestartIdleTimer (VOID)
{
    FwlTmrRestartTmr (&gFwlStatefulTmr, FWL_IDLE_TIMER, FWL_IDLE_TIMEOUT);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlAclScheduleSetStatus                          */
/*                                                                          */
/*    Description        : Routine to update the current value of the ACL   */
/*                         schedule configured.                             */
/*                                                                          */
/*    Input(s)           : i4Status - Gives the status value to be updated. */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

PUBLIC VOID
FwlAclScheduleSetStatus (INT4 i4Status)
{
    switch (i4Status)
    {
        case FWL_ACTIVE:
            gu1AclScheduleStatus = (UINT1) i4Status;
            break;

        case FWL_NOT_IN_SERVICE:
            /* This will Flush all the connections that are
             * not within the schedule. */
            FwlCommitAcl ();
            gu1AclScheduleStatus = (UINT1) i4Status;
            break;
        default:
            break;
    }                            /*End-of-Switch */
}                                /*End-of-Function */

/****************************************************************************/
/*                             end of fwltmrif.c                            */
/****************************************************************************/
