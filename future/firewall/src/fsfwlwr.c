/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsfwlwr.c,v 1.15 2016/02/27 10:05:03 siva Exp $
 *
 * Description: File containing mid level routines 
 * *******************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "fsfwllw.h"
# include  "fsfwlwr.h"
# include  "fsfwldb.h"
# include  "fwlsnif.h"

VOID
RegisterFSFWL ()
{
    SNMPRegisterMib (&fsfwlOID, &fsfwlEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsfwlOID, (const UINT1 *) "fsfwl");
}

INT4
FwlGlobalMasterControlSwitchGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalMasterControlSwitch (&(pMultiData->i4_SLongValue)));
}

INT4
FwlGlobalICMPControlSwitchGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalICMPControlSwitch (&(pMultiData->i4_SLongValue)));
}

INT4
FwlGlobalIpSpoofFilteringGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalIpSpoofFiltering (&(pMultiData->i4_SLongValue)));
}

INT4
FwlGlobalSrcRouteFilteringGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalSrcRouteFiltering (&(pMultiData->i4_SLongValue)));
}

INT4
FwlGlobalTinyFragmentFilteringGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalTinyFragmentFiltering
            (&(pMultiData->i4_SLongValue)));
}
INT4 FwlDosAttackAcceptRedirectGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFwlDosAttackAcceptRedirect(&(pMultiData->i4_SLongValue)));
}

INT4 FwlDosAttackAcceptRedirectSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhSetFwlDosAttackAcceptRedirect(pMultiData->i4_SLongValue));
}

INT4 FwlDosAttackAcceptRedirectTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhTestv2FwlDosAttackAcceptRedirect(pu4Error, pMultiData->i4_SLongValue));
}
INT4 FwlDosAttackAcceptRedirectDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2FwlDosAttackAcceptRedirect(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFwlSnorkTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFwlSnorkTable(
            &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFwlSnorkTable(
            pFirstMultiIndex->pIndex[0].i4_SLongValue,
            &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}
INT4 FwlSnorkRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlSnorkTable(
        pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFwlSnorkRowStatus(
        pMultiIndex->pIndex[0].i4_SLongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4 FwlSnorkRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlSnorkRowStatus(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4 FwlSnorkRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FwlSnorkRowStatus(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4 FwlSnorkTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2FwlSnorkTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}





INT4 FwlDosAttackAcceptSmurfAttackGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFwlDosAttackAcceptSmurfAttack(&(pMultiData->i4_SLongValue)));
}


INT4 FwlDosAttackAcceptSmurfAttackSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhSetFwlDosAttackAcceptSmurfAttack(pMultiData->i4_SLongValue));
}


INT4 FwlDosAttackAcceptSmurfAttackTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhTestv2FwlDosAttackAcceptSmurfAttack(pu4Error, pMultiData->i4_SLongValue));
}

INT4 FwlDosAttackAcceptSmurfAttackDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2FwlDosAttackAcceptSmurfAttack(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FwlDosLandAttackGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFwlDosLandAttack(&(pMultiData->i4_SLongValue)));
}
INT4 FwlDosLandAttackSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhSetFwlDosLandAttack(pMultiData->i4_SLongValue));
}


INT4 FwlDosLandAttackTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhTestv2FwlDosLandAttack(pu4Error, pMultiData->i4_SLongValue));
}

INT4 FwlDosLandAttackDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2FwlDosLandAttack(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FwlDosShortHeaderAttackGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFwlDosShortHeaderAttack(&(pMultiData->i4_SLongValue)));
}


INT4 FwlDosShortHeaderAttackSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhSetFwlDosShortHeaderAttack(pMultiData->i4_SLongValue));
}


INT4 FwlDosShortHeaderAttackTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhTestv2FwlDosShortHeaderAttack(pu4Error, pMultiData->i4_SLongValue));
}
INT4 FwlDosShortHeaderAttackDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2FwlDosShortHeaderAttack(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4
FwlGlobalTcpInterceptGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalTcpIntercept (&(pMultiData->i4_SLongValue)));
}

INT4
FwlGlobalTrapGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalTrap (&(pMultiData->i4_SLongValue)));
}

INT4
FwlGlobalTraceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalTrace (&(pMultiData->i4_SLongValue)));
}

INT4
FwlGlobalDebugGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalDebug (&(pMultiData->i4_SLongValue)));
}

INT4
FwlGlobalMaxFiltersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalMaxFilters (&(pMultiData->i4_SLongValue)));
}

INT4
FwlGlobalMaxRulesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalMaxRules (&(pMultiData->i4_SLongValue)));
}

INT4
FwlGlobalUrlFilteringGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalUrlFiltering (&(pMultiData->i4_SLongValue)));
}

INT4
FwlGlobalNetBiosFilteringGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalNetBiosFiltering (&(pMultiData->i4_SLongValue)));
}

INT4
FwlGlobalNetBiosLan2WanGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalNetBiosLan2Wan (&(pMultiData->i4_SLongValue)));
}

INT4
FwlGlobalICMPv6ControlSwitchGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalICMPv6ControlSwitch (&(pMultiData->i4_SLongValue)));
}

INT4
FwlGlobalIpv6SpoofFilteringGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalIpv6SpoofFiltering (&(pMultiData->i4_SLongValue)));
}

INT4
FwlGlobalLogFileSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalLogFileSize (&(pMultiData->u4_ULongValue)));
}

INT4
FwlGlobalLogSizeThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalLogSizeThreshold (&(pMultiData->u4_ULongValue)));
}

INT4
FwlGlobalIdsLogSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalIdsLogSize (&(pMultiData->u4_ULongValue)));
}

INT4
FwlGlobalIdsLogThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalIdsLogThreshold (&(pMultiData->u4_ULongValue)));
}

INT4
FwlGlobalIdsVersionInfoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalIdsVersionInfo (pMultiData->pOctetStrValue));
}

INT4
FwlGlobalReloadIdsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalReloadIds (&(pMultiData->i4_SLongValue)));
}

INT4
FwlGlobalIdsStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalIdsStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FwlGlobalLoadIdsRulesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlGlobalLoadIdsRules (&(pMultiData->i4_SLongValue)));
}

INT4
FwlGlobalMasterControlSwitchSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalMasterControlSwitch (pMultiData->i4_SLongValue));
}

INT4
FwlGlobalICMPControlSwitchSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalICMPControlSwitch (pMultiData->i4_SLongValue));
}

INT4
FwlGlobalIpSpoofFilteringSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalIpSpoofFiltering (pMultiData->i4_SLongValue));
}

INT4
FwlGlobalSrcRouteFilteringSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalSrcRouteFiltering (pMultiData->i4_SLongValue));
}

INT4
FwlGlobalTinyFragmentFilteringSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalTinyFragmentFiltering (pMultiData->i4_SLongValue));
}

INT4
FwlGlobalTcpInterceptSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalTcpIntercept (pMultiData->i4_SLongValue));
}

INT4
FwlGlobalTrapSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalTrap (pMultiData->i4_SLongValue));
}

INT4
FwlGlobalTraceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalTrace (pMultiData->i4_SLongValue));
}

INT4
FwlGlobalDebugSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalDebug (pMultiData->i4_SLongValue));
}

INT4
FwlGlobalUrlFilteringSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalUrlFiltering (pMultiData->i4_SLongValue));
}

INT4
FwlGlobalNetBiosFilteringSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalNetBiosFiltering (pMultiData->i4_SLongValue));
}

INT4
FwlGlobalNetBiosLan2WanSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalNetBiosLan2Wan (pMultiData->i4_SLongValue));
}

INT4
FwlGlobalICMPv6ControlSwitchSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalICMPv6ControlSwitch (pMultiData->i4_SLongValue));
}

INT4
FwlGlobalIpv6SpoofFilteringSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalIpv6SpoofFiltering (pMultiData->i4_SLongValue));
}

INT4
FwlGlobalLogFileSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalLogFileSize (pMultiData->u4_ULongValue));
}

INT4
FwlGlobalLogSizeThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalLogSizeThreshold (pMultiData->u4_ULongValue));
}

INT4
FwlGlobalIdsLogSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalIdsLogSize (pMultiData->u4_ULongValue));
}

INT4
FwlGlobalIdsLogThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalIdsLogThreshold (pMultiData->u4_ULongValue));
}

INT4
FwlGlobalReloadIdsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalReloadIds (pMultiData->i4_SLongValue));
}

INT4
FwlGlobalIdsStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalIdsStatus (pMultiData->i4_SLongValue));
}

INT4
FwlGlobalLoadIdsRulesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlGlobalLoadIdsRules (pMultiData->i4_SLongValue));
}

INT4
FwlGlobalMasterControlSwitchTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalMasterControlSwitch
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FwlGlobalICMPControlSwitchTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalICMPControlSwitch
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FwlGlobalIpSpoofFilteringTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalIpSpoofFiltering
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FwlGlobalSrcRouteFilteringTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalSrcRouteFiltering
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FwlGlobalTinyFragmentFilteringTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalTinyFragmentFiltering
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FwlGlobalTcpInterceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalTcpIntercept
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FwlGlobalTrapTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalTrap (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FwlGlobalTraceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalTrace (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FwlGlobalDebugTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalDebug (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FwlGlobalUrlFilteringTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalUrlFiltering
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FwlGlobalNetBiosFilteringTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalNetBiosFiltering
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FwlGlobalNetBiosLan2WanTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalNetBiosLan2Wan
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FwlGlobalICMPv6ControlSwitchTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalICMPv6ControlSwitch (pu4Error,
                                                   pMultiData->i4_SLongValue));
}

INT4
FwlGlobalIpv6SpoofFilteringTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalIpv6SpoofFiltering (pu4Error,
                                                  pMultiData->i4_SLongValue));
}

INT4
FwlGlobalLogFileSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalLogFileSize
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FwlGlobalLogSizeThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalLogSizeThreshold
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FwlGlobalIdsLogSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalIdsLogSize (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FwlGlobalIdsLogThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalIdsLogThreshold
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FwlGlobalReloadIdsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalReloadIds (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FwlGlobalIdsStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalIdsStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FwlGlobalLoadIdsRulesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlGlobalLoadIdsRules
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FwlGlobalMasterControlSwitchDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalMasterControlSwitch
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalICMPControlSwitchDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalICMPControlSwitch
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalIpSpoofFilteringDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalIpSpoofFiltering
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalSrcRouteFilteringDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalSrcRouteFiltering
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalTinyFragmentFilteringDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalTinyFragmentFiltering
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalTcpInterceptDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalTcpIntercept
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalTrapDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalTrap (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalTraceDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalTrace (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalDebugDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalDebug (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalUrlFilteringDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalUrlFiltering
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalNetBiosFilteringDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalNetBiosFiltering
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalNetBiosLan2WanDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalNetBiosLan2Wan
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalICMPv6ControlSwitchDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalICMPv6ControlSwitch
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalIpv6SpoofFilteringDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalIpv6SpoofFiltering
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalLogFileSizeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalLogFileSize
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalLogSizeThresholdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalLogSizeThreshold
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalIdsLogSizeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalIdsLogSize
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalIdsLogThresholdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalIdsLogThreshold
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalReloadIdsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalReloadIds
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalIdsStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalIdsStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlGlobalLoadIdsRulesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlGlobalLoadIdsRules
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlDefnTcpInterceptThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlDefnTcpInterceptThreshold (&(pMultiData->i4_SLongValue)));
}

INT4
FwlDefnInterceptTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlDefnInterceptTimeout (&(pMultiData->u4_ULongValue)));
}

INT4
FwlDefnTcpInterceptThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlDefnTcpInterceptThreshold (pMultiData->i4_SLongValue));
}

INT4
FwlDefnInterceptTimeoutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlDefnInterceptTimeout (pMultiData->u4_ULongValue));
}

INT4
FwlDefnTcpInterceptThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlDefnTcpInterceptThreshold
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FwlDefnInterceptTimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlDefnInterceptTimeout
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FwlDefnTcpInterceptThresholdDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlDefnTcpInterceptThreshold
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlDefnInterceptTimeoutDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlDefnInterceptTimeout
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFwlDefnFilterTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFwlDefnFilterTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFwlDefnFilterTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FwlFilterSrcAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlFilterSrcAddress (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FwlFilterDestAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlFilterDestAddress (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FwlFilterProtocolGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlFilterProtocol (pMultiIndex->pIndex[0].pOctetStrValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FwlFilterSrcPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlFilterSrcPort (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
FwlFilterDestPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlFilterDestPort (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FwlFilterAckBitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlFilterAckBit (pMultiIndex->pIndex[0].pOctetStrValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FwlFilterRstBitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlFilterRstBit (pMultiIndex->pIndex[0].pOctetStrValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FwlFilterTosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlFilterTos (pMultiIndex->pIndex[0].pOctetStrValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
FwlFilterAccountingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlFilterAccounting (pMultiIndex->pIndex[0].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FwlFilterHitClearGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlFilterHitClear (pMultiIndex->pIndex[0].pOctetStrValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FwlFilterHitsCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlFilterHitsCount (pMultiIndex->pIndex[0].pOctetStrValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FwlFilterAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlFilterAddrType (pMultiIndex->pIndex[0].pOctetStrValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FwlFilterFlowIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlFilterFlowId (pMultiIndex->pIndex[0].pOctetStrValue,
                                   &(pMultiData->u4_ULongValue)));

}

INT4
FwlFilterDscpGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlFilterDscp (pMultiIndex->pIndex[0].pOctetStrValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FwlFilterRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlFilterRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FwlFilterSrcAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlFilterSrcAddress (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FwlFilterDestAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlFilterDestAddress (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FwlFilterProtocolSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlFilterProtocol (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FwlFilterSrcPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlFilterSrcPort (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
FwlFilterDestPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlFilterDestPort (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FwlFilterAckBitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlFilterAckBit (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FwlFilterRstBitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlFilterRstBit (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FwlFilterTosSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlFilterTos (pMultiIndex->pIndex[0].pOctetStrValue,
                                pMultiData->i4_SLongValue));

}

INT4
FwlFilterAccountingSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlFilterAccounting (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FwlFilterHitClearSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlFilterHitClear (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FwlFilterAddrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlFilterAddrType (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FwlFilterFlowIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlFilterFlowId (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiData->u4_ULongValue));

}

INT4
FwlFilterDscpSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlFilterDscp (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiData->i4_SLongValue));

}

INT4
FwlFilterRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlFilterRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FwlFilterSrcAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FwlFilterSrcAddress (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FwlFilterDestAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FwlFilterDestAddress (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FwlFilterProtocolTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FwlFilterProtocol (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FwlFilterSrcPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FwlFilterSrcPort (pu4Error,
                                       pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FwlFilterDestPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FwlFilterDestPort (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FwlFilterAckBitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FwlFilterAckBit (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FwlFilterRstBitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FwlFilterRstBit (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FwlFilterTosTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    return (nmhTestv2FwlFilterTos (pu4Error,
                                   pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FwlFilterAccountingTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FwlFilterAccounting (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FwlFilterHitClearTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FwlFilterHitClear (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FwlFilterAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FwlFilterAddrType (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FwlFilterFlowIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FwlFilterFlowId (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiData->u4_ULongValue));

}

INT4
FwlFilterDscpTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2FwlFilterDscp (pu4Error,
                                    pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FwlFilterRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FwlFilterRowStatus (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FwlDefnFilterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlDefnFilterTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFwlDefnRuleTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFwlDefnRuleTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFwlDefnRuleTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FwlRuleFilterSetGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnRuleTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlRuleFilterSet (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
FwlRuleRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnRuleTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlRuleRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FwlRuleFilterSetSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlRuleFilterSet (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
FwlRuleRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlRuleRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FwlRuleFilterSetTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FwlRuleFilterSet (pu4Error,
                                       pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FwlRuleRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FwlRuleRowStatus (pu4Error,
                                       pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FwlDefnRuleTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlDefnRuleTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFwlDefnAclTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFwlDefnAclTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFwlDefnAclTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FwlAclActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnAclTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlAclAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiIndex->pIndex[1].pOctetStrValue,
                                pMultiIndex->pIndex[2].i4_SLongValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
FwlAclSequenceNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnAclTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlAclSequenceNumber (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FwlAclAclTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnAclTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlAclAclType (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].pOctetStrValue,
                                 pMultiIndex->pIndex[2].i4_SLongValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FwlAclLogTriggerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnAclTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlAclLogTrigger (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FwlAclFragActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnAclTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlAclFragAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FwlAclRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnAclTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlAclRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FwlAclActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlAclAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiIndex->pIndex[1].pOctetStrValue,
                                pMultiIndex->pIndex[2].i4_SLongValue,
                                pMultiData->i4_SLongValue));

}

INT4
FwlAclSequenceNumberSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlAclSequenceNumber (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FwlAclLogTriggerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlAclLogTrigger (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FwlAclFragActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlAclFragAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FwlAclRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlAclRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FwlAclActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    return (nmhTestv2FwlAclAction (pu4Error,
                                   pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FwlAclSequenceNumberTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FwlAclSequenceNumber (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FwlAclLogTriggerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FwlAclLogTrigger (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FwlAclFragActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FwlAclFragAction (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FwlAclRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FwlAclRowStatus (pu4Error,
                                      pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FwlDefnAclTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlDefnAclTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFwlDefnIfTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFwlDefnIfTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFwlDefnIfTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FwlIfIfTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlIfIfType (pMultiIndex->pIndex[0].i4_SLongValue,
                               &(pMultiData->i4_SLongValue)));

}

INT4
FwlIfIpOptionsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlIfIpOptions (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FwlIfFragmentsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlIfFragments (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FwlIfFragmentSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlIfFragmentSize (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));
}

INT4
FwlIfICMPTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlIfICMPType (pMultiIndex->pIndex[0].i4_SLongValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FwlIfICMPCodeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlIfICMPCode (pMultiIndex->pIndex[0].i4_SLongValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FwlIfICMPv6MsgTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlIfICMPv6MsgType (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FwlIfRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlIfRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FwlIfIfTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlIfIfType (pMultiIndex->pIndex[0].i4_SLongValue,
                               pMultiData->i4_SLongValue));

}

INT4
FwlIfIpOptionsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlIfIpOptions (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FwlIfFragmentsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlIfFragments (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));
}

INT4
FwlIfFragmentSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlIfFragmentSize (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->u4_ULongValue));
}

INT4
FwlIfICMPTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlIfICMPType (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiData->i4_SLongValue));

}

INT4
FwlIfICMPCodeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlIfICMPCode (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiData->i4_SLongValue));

}

INT4
FwlIfICMPv6MsgTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlIfICMPv6MsgType (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FwlIfRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlIfRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FwlIfIfTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    return (nmhTestv2FwlIfIfType (pu4Error,
                                  pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FwlIfIpOptionsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    INT4                i4RowStatus;
    INT4                i4RetVal = SNMP_FAILURE;

    if ((i4RetVal =
         nmhGetFwlIfRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                               &i4RowStatus)) != SNMP_SUCCESS)
    {
        /* DSL_ADD : Change FWL_CREATE_AND_GO to FWL_CREATE_AND_WAIT after
         * testing */
        nmhSetFwlIfRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                              FWL_CREATE_AND_GO);
        nmhSetFwlIfRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                              FWL_NOT_IN_SERVICE);
    }
    else
    {
        if (i4RowStatus == FWL_ACTIVE)
        {
            nmhSetFwlIfRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  FWL_NOT_IN_SERVICE);
        }
    }

    return (nmhTestv2FwlIfIpOptions (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FwlIfFragmentsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FwlIfFragments (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FwlIfFragmentSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FwlIfFragmentSize
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiData->u4_ULongValue));
}

INT4
FwlIfICMPTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2FwlIfICMPType (pu4Error,
                                    pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FwlIfICMPCodeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2FwlIfICMPCode (pu4Error,
                                    pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FwlIfICMPv6MsgTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FwlIfICMPv6MsgType (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FwlIfRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FwlIfRowStatus (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FwlDefnIfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlDefnIfTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFwlDefnDmzTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFwlDefnDmzTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFwlDefnDmzTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FwlDmzRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnDmzTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlDmzRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FwlDmzRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlDmzRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FwlDmzRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FwlDmzRowStatus (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FwlDefnDmzTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlDefnDmzTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFwlUrlFilterTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFwlUrlFilterTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFwlUrlFilterTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FwlUrlHitCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlUrlFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlUrlHitCount (pMultiIndex->pIndex[0].pOctetStrValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
FwlUrlFilterRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlUrlFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlUrlFilterRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FwlUrlFilterRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlUrlFilterRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FwlUrlFilterRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FwlUrlFilterRowStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FwlUrlFilterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlUrlFilterTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlStatInspectedPacketsCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatInspectedPacketsCount (&(pMultiData->u4_ULongValue)));
}

INT4
FwlStatTotalPacketsDeniedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatTotalPacketsDenied (&(pMultiData->u4_ULongValue)));
}

INT4
FwlStatTotalPacketsAcceptedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatTotalPacketsAccepted (&(pMultiData->u4_ULongValue)));
}

INT4
FwlStatTotalIcmpPacketsDeniedGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatTotalIcmpPacketsDenied (&(pMultiData->u4_ULongValue)));
}

INT4
FwlStatTotalSynPacketsDeniedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatTotalSynPacketsDenied (&(pMultiData->u4_ULongValue)));
}

INT4
FwlStatTotalIpSpoofedPacketsDeniedGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatTotalIpSpoofedPacketsDenied
            (&(pMultiData->u4_ULongValue)));
}

INT4
FwlStatTotalSrcRoutePacketsDeniedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatTotalSrcRoutePacketsDenied
            (&(pMultiData->u4_ULongValue)));
}

INT4
FwlStatTotalTinyFragmentPacketsDeniedGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatTotalTinyFragmentPacketsDenied
            (&(pMultiData->u4_ULongValue)));
}

INT4
FwlStatTotalFragmentedPacketsDeniedGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatTotalFragmentedPacketsDenied
            (&(pMultiData->u4_ULongValue)));
}

INT4
FwlStatTotalLargeFragmentPacketsDeniedGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatTotalLargeFragmentPacketsDenied
            (&(pMultiData->u4_ULongValue)));
}

INT4
FwlStatTotalIpOptionPacketsDeniedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatTotalIpOptionPacketsDenied
            (&(pMultiData->u4_ULongValue)));
}

INT4
FwlStatTotalAttacksPacketsDeniedGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatTotalAttacksPacketsDenied
            (&(pMultiData->u4_ULongValue)));
}

INT4
FwlStatMemoryAllocationFailCountGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatMemoryAllocationFailCount
            (&(pMultiData->u4_ULongValue)));
}

INT4
FwlStatIPv6InspectedPacketsCountGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatIPv6InspectedPacketsCount
            (&(pMultiData->u4_ULongValue)));
}

INT4
FwlStatIPv6TotalPacketsDeniedGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatIPv6TotalPacketsDenied (&(pMultiData->u4_ULongValue)));
}

INT4
FwlStatIPv6TotalPacketsAcceptedGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatIPv6TotalPacketsAccepted
            (&(pMultiData->u4_ULongValue)));
}

INT4
FwlStatIPv6TotalIcmpPacketsDeniedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatIPv6TotalIcmpPacketsDenied
            (&(pMultiData->u4_ULongValue)));
}

INT4
FwlStatIPv6TotalSpoofedPacketsDeniedGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatIPv6TotalSpoofedPacketsDenied
            (&(pMultiData->u4_ULongValue)));
}

INT4
FwlStatIPv6TotalAttacksPacketsDeniedGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatIPv6TotalAttacksPacketsDenied
            (&(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexFwlStatIfTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFwlStatIfTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFwlStatIfTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FwlStatIfFilterCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStatIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStatIfFilterCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FwlStatIfPacketsDeniedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStatIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStatIfPacketsDenied (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FwlStatIfPacketsAcceptedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStatIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStatIfPacketsAccepted
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FwlStatIfSynPacketsDeniedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStatIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStatIfSynPacketsDenied
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FwlStatIfIcmpPacketsDeniedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStatIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStatIfIcmpPacketsDenied
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FwlStatIfIpSpoofedPacketsDeniedGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStatIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStatIfIpSpoofedPacketsDenied
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FwlStatIfSrcRoutePacketsDeniedGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStatIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStatIfSrcRoutePacketsDenied
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FwlStatIfTinyFragmentPacketsDeniedGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStatIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStatIfTinyFragmentPacketsDenied
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FwlStatIfFragmentPacketsDeniedGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStatIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStatIfFragmentPacketsDenied
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FwlStatIfIpOptionPacketsDeniedGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStatIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStatIfIpOptionPacketsDenied
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FwlStatIfClearGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStatIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStatIfClear (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FwlIfTrapThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStatIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlIfTrapThreshold (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FwlStatIfIPv6PacketsDeniedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStatIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStatIfIPv6PacketsDenied
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FwlStatIfIPv6PacketsAcceptedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStatIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStatIfIPv6PacketsAccepted
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FwlStatIfIPv6IcmpPacketsDeniedGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStatIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStatIfIPv6IcmpPacketsDenied
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FwlStatIfIPv6SpoofedPacketsDeniedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStatIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStatIfIPv6SpoofedPacketsDenied
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FwlStatIfClearIPv6Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStatIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStatIfClearIPv6 (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FwlStatIfClearSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlStatIfClear (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FwlIfTrapThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlIfTrapThreshold (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FwlStatIfClearTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FwlStatIfClear (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FwlIfTrapThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FwlIfTrapThreshold (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FwlStatIfClearIPv6Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FwlStatIfClearIPv6 (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FwlStatIfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlStatIfTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlStatClearGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatClear (&(pMultiData->i4_SLongValue)));
}

INT4
FwlStatClearIPv6Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlStatClearIPv6 (&(pMultiData->i4_SLongValue)));
}

INT4
FwlTrapThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlTrapThreshold (&(pMultiData->i4_SLongValue)));
}

INT4
FwlStatClearSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlStatClear (pMultiData->i4_SLongValue));
}

INT4
FwlStatClearIPv6Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlStatClearIPv6 (pMultiData->i4_SLongValue));
}

INT4
FwlTrapThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlTrapThreshold (pMultiData->i4_SLongValue));
}

INT4
FwlStatIfClearIPv6Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlStatIfClearIPv6 (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FwlStatClearTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlStatClear (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FwlStatClearIPv6Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlStatClearIPv6 (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FwlTrapThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlTrapThreshold (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FwlStatClearDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlStatClear (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlStatClearIPv6Dep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlStatClearIPv6 (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlTrapThresholdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlTrapThreshold (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlTrapMemFailMessageGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlTrapMemFailMessage (pMultiData->pOctetStrValue));
}

INT4
FwlTrapAttackMessageGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlTrapAttackMessage (pMultiData->pOctetStrValue));
}

INT4
FwlTrapFileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlTrapFileName (pMultiData->pOctetStrValue));
}

INT4
FwlIdsTrapFileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFwlIdsTrapFileName (pMultiData->pOctetStrValue));
}

INT4
FwlTrapMemFailMessageSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlTrapMemFailMessage (pMultiData->pOctetStrValue));
}

INT4
FwlTrapAttackMessageSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFwlTrapAttackMessage (pMultiData->pOctetStrValue));
}

INT4
FwlTrapMemFailMessageTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlTrapMemFailMessage
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FwlTrapAttackMessageTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FwlTrapAttackMessage
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FwlTrapMemFailMessageDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlTrapMemFailMessage
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FwlTrapAttackMessageDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlTrapAttackMessage
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFwlDefnBlkListTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFwlDefnBlkListTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFwlDefnBlkListTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FwlBlkListHitsCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnBlkListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlBlkListHitsCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FwlBlkListEntryTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnBlkListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlBlkListEntryType (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FwlBlkListRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnBlkListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlBlkListRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FwlBlkListRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlBlkListRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FwlBlkListRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FwlBlkListRowStatus (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FwlDefnBlkListTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlDefnBlkListTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFwlDefnWhiteListTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFwlDefnWhiteListTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFwlDefnWhiteListTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FwlWhiteListHitsCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnWhiteListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlWhiteListHitsCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FwlWhiteListRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnWhiteListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlWhiteListRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FwlWhiteListRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlWhiteListRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FwlWhiteListRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FwlWhiteListRowStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            pOctetStrValue,
                                            pMultiIndex->pIndex[2].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FwlDefnWhiteListTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlDefnWhiteListTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFwlDefnIPv6DmzTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFwlDefnIPv6DmzTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFwlDefnIPv6DmzTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FwlDmzAddressTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnIPv6DmzTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlDmzAddressType (pMultiIndex->pIndex[0].pOctetStrValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FwlDmzIpv6RowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlDefnIPv6DmzTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlDmzIpv6RowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FwlDmzAddressTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlDmzAddressType (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FwlDmzIpv6RowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlDmzIpv6RowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FwlDmzAddressTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal *
                       pMultiData)
{
    return (nmhTestv2FwlDmzAddressType (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FwlDmzIpv6RowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal
                         * pMultiData)
{
    return (nmhTestv2FwlDmzIpv6RowStatus (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FwlDefnIPv6DmzTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FwlDefnIPv6DmzTable (pu4Error, pSnmpIndexList,
                                         pSnmpvarbinds));
    ;
}

INT4
GetNextIndexFwlStateTable (tSnmpIndex * pFirstMultiIndex,
                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFwlStateTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pNextMultiIndex->pIndex[4].pOctetStrValue,
             &(pNextMultiIndex->pIndex[5].i4_SLongValue),
             &(pNextMultiIndex->pIndex[6].i4_SLongValue),
             &(pNextMultiIndex->pIndex[7].i4_SLongValue),
             &(pNextMultiIndex->pIndex[8].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFwlStateTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue,
             pFirstMultiIndex->pIndex[5].i4_SLongValue,
             &(pNextMultiIndex->pIndex[5].i4_SLongValue),
             pFirstMultiIndex->pIndex[6].i4_SLongValue,
             &(pNextMultiIndex->pIndex[6].i4_SLongValue),
             pFirstMultiIndex->pIndex[7].i4_SLongValue,
             &(pNextMultiIndex->pIndex[7].i4_SLongValue),
             pFirstMultiIndex->pIndex[8].i4_SLongValue,
             &(pNextMultiIndex->pIndex[8].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FwlStateEstablishedTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStateTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].i4_SLongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStateEstablishedTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiIndex->pIndex[2].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[3].i4_SLongValue,
                                           pMultiIndex->pIndex[4].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[5].i4_SLongValue,
                                           pMultiIndex->pIndex[6].i4_SLongValue,
                                           pMultiIndex->pIndex[7].i4_SLongValue,
                                           pMultiIndex->pIndex[8].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FwlStateLocalStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStateTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].i4_SLongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStateLocalState (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].pOctetStrValue,
                                      pMultiIndex->pIndex[3].i4_SLongValue,
                                      pMultiIndex->pIndex[4].pOctetStrValue,
                                      pMultiIndex->pIndex[5].i4_SLongValue,
                                      pMultiIndex->pIndex[6].i4_SLongValue,
                                      pMultiIndex->pIndex[7].i4_SLongValue,
                                      pMultiIndex->pIndex[8].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FwlStateRemoteStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStateTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].i4_SLongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStateRemoteState (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       pMultiIndex->pIndex[3].i4_SLongValue,
                                       pMultiIndex->pIndex[4].pOctetStrValue,
                                       pMultiIndex->pIndex[5].i4_SLongValue,
                                       pMultiIndex->pIndex[6].i4_SLongValue,
                                       pMultiIndex->pIndex[7].i4_SLongValue,
                                       pMultiIndex->pIndex[8].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FwlStateLogLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStateTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].i4_SLongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStateLogLevel (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].pOctetStrValue,
                                    pMultiIndex->pIndex[3].i4_SLongValue,
                                    pMultiIndex->pIndex[4].pOctetStrValue,
                                    pMultiIndex->pIndex[5].i4_SLongValue,
                                    pMultiIndex->pIndex[6].i4_SLongValue,
                                    pMultiIndex->pIndex[7].i4_SLongValue,
                                    pMultiIndex->pIndex[8].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FwlStateCallStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlStateTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].i4_SLongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFwlStateCallStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].pOctetStrValue,
                                      pMultiIndex->pIndex[3].i4_SLongValue,
                                      pMultiIndex->pIndex[4].pOctetStrValue,
                                      pMultiIndex->pIndex[5].i4_SLongValue,
                                      pMultiIndex->pIndex[6].i4_SLongValue,
                                      pMultiIndex->pIndex[7].i4_SLongValue,
                                      pMultiIndex->pIndex[8].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}


INT4 GetNextIndexFwlRateLimitTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFwlRateLimitTable(
            &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFwlRateLimitTable(
            pFirstMultiIndex->pIndex[0].i4_SLongValue,
            &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}
INT4 FwlRateLimitPortNumberGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlRateLimitTable(
        pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFwlRateLimitPortNumber(
        pMultiIndex->pIndex[0].i4_SLongValue,
        &(pMultiData->i4_SLongValue)));

}

INT4 FwlRateLimitPortTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlRateLimitTable(
        pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFwlRateLimitPortType(
        pMultiIndex->pIndex[0].i4_SLongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4 FwlRateLimitValueGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlRateLimitTable(
        pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFwlRateLimitValue(
        pMultiIndex->pIndex[0].i4_SLongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4 FwlRateLimitBurstSizeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlRateLimitTable(
        pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFwlRateLimitBurstSize(
        pMultiIndex->pIndex[0].i4_SLongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4 FwlRateLimitTrafficModeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlRateLimitTable(
        pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFwlRateLimitTrafficMode(
        pMultiIndex->pIndex[0].i4_SLongValue,
        &(pMultiData->i4_SLongValue)));

}

INT4 FwlRateLimitRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlRateLimitTable(
        pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFwlRateLimitRowStatus(
        pMultiIndex->pIndex[0].i4_SLongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4 FwlRateLimitPortNumberSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlRateLimitPortNumber(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4 FwlRateLimitPortTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlRateLimitPortType(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4 FwlRateLimitValueSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlRateLimitValue(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4 FwlRateLimitBurstSizeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlRateLimitBurstSize(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}


INT4 FwlRateLimitTrafficModeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlRateLimitTrafficMode(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}



INT4 FwlRateLimitRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlRateLimitRowStatus(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}


INT4 FwlRateLimitPortNumberTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FwlRateLimitPortNumber(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4 FwlRateLimitPortTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FwlRateLimitPortType(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4 FwlRateLimitValueTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FwlRateLimitValue(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4 FwlRateLimitBurstSizeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FwlRateLimitBurstSize(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}
INT4 FwlRateLimitTrafficModeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FwlRateLimitTrafficMode(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}




INT4 FwlRateLimitRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FwlRateLimitRowStatus(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4 FwlRateLimitTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2FwlRateLimitTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
INT4 GetNextIndexFwlRpfTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFwlRpfTable(
            &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFwlRpfTable(
            pFirstMultiIndex->pIndex[0].i4_SLongValue,
            &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}
INT4 FwlRpfModeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlRpfTable(
        pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFwlRpfMode(
        pMultiIndex->pIndex[0].i4_SLongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4 FwlRpfRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFwlRpfTable(
        pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFwlRpfRowStatus(
        pMultiIndex->pIndex[0].i4_SLongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4 FwlRpfModeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlRpfMode(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4 FwlRpfRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFwlRpfRowStatus(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4 FwlRpfModeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FwlRpfMode(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4 FwlRpfRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FwlRpfRowStatus(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiData->i4_SLongValue));

}

INT4 FwlRpfTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2FwlRpfTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}



