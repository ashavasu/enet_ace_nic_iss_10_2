/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwldmz.c,v 1.9 2013/11/01 10:15:16 siva Exp $
 *
 * Description:This file provides the routines for DMZ handling
 *
 *******************************************************************/

#include "fwlinc.h"

tTMO_SLL            gFwlDmzList;    /* DMZ List */
tTMO_SLL            gFwlIPv6DmzList;    /*IPv6 DMZ list */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDmzInit                                       */
/*                                                                          */
/*    Description        : Initialises the Data structures for dmz          */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
FwlDmzInit (VOID)
{
    TMO_SLL_Init (&gFwlDmzList);
    TMO_SLL_Init (&gFwlIPv6DmzList);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDmzMemAllocate                                */
/*                                                                          */
/*    Description        : Allocates memory for a Dmz node.                 */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MEM_SUCCESS or MEM_FAILURE                       */
/****************************************************************************/

UINT4
FwlDmzMemAllocate (UINT1 **ppu1Block)
{
    UINT4               u4MemAllocateStatus = MEM_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlDmzMemAllocate\n");

    u4MemAllocateStatus = MemAllocateMemBlock (FWL_DMZ_PID, ppu1Block);

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlDmzMemAllocate\n");
    return u4MemAllocateStatus;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlIPv6DmzMemAllocate                            */
/*                                                                          */
/*    Description        : Allocates memory for a Dmz node.                 */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MEM_SUCCESS or MEM_FAILURE                       */
/****************************************************************************/

UINT4
FwlIPv6DmzMemAllocate (UINT1 **ppu1Block)
{
    UINT4               u4MemAllocateStatus = MEM_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlIPv6DmzMemAllocate\n");

    u4MemAllocateStatus = MemAllocateMemBlock (FWL_IPV6_DMZ_PID, ppu1Block);

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlIPv6DmzMemAllocate\n");
    return u4MemAllocateStatus;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlAddDmzNode                                    */
/*                                                                          */
/*    Description        : Add the Dmz Node to dmz list                     */
/*                                                                          */
/*    Input(s)           : pDmzNode -- Pointer to the Dmz list Node         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
FwlAddDmzNode (tFwlDmzInfo * pDmzNode)
{
    tFwlDmzInfo        *pDmzTmpNode = NULL;
    tFwlDmzInfo        *pDmzPrevNode = NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into function FwlAddDmzNode \n");

    if (pDmzNode != NULL)
    {
        TMO_SLL_Scan (&gFwlDmzList, pDmzTmpNode, tFwlDmzInfo *)
        {
            if (pDmzTmpNode->u4IpIndex > pDmzNode->u4IpIndex)
            {
                break;
            }
            pDmzPrevNode = pDmzTmpNode;
        }
        TMO_SLL_Insert (&gFwlDmzList, (tTMO_SLL_NODE *) pDmzPrevNode,
                        (tTMO_SLL_NODE *) pDmzNode);
    }

    FWL_DBG (FWL_DBG_EXIT, "\nExiting from function FwlAddDmzNode \n");
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlAddIPv6DmzNode                                */
/*                                                                          */
/*    Description        : Add the IPv6 Dmz Node to dmz list                */
/*                                                                          */
/*    Input(s)           : pDmzNode -- Pointer to the Dmz list Node         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
FwlAddIPv6DmzNode (tFwlIPv6DmzInfo * pDmzNode)
{
    tFwlIPv6DmzInfo    *pDmzTmpNode = NULL;
    tFwlIPv6DmzInfo    *pDmzPrevNode = NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into function FwlAddIPv6DmzNode \n");

    if (pDmzNode != NULL)
    {
        TMO_SLL_Scan (&gFwlIPv6DmzList, pDmzTmpNode, tFwlIPv6DmzInfo *)
        {
            if (Ip6IsAddrGreater (&pDmzTmpNode->dmzIpv6Index,
                                  &pDmzNode->dmzIpv6Index) == FWL_ZERO)
            {
                break;
            }
            pDmzPrevNode = pDmzTmpNode;
        }
        TMO_SLL_Insert (&gFwlIPv6DmzList, (tTMO_SLL_NODE *) pDmzPrevNode,
                        (tTMO_SLL_NODE *) pDmzNode);
    }

    FWL_DBG (FWL_DBG_EXIT, "\nExiting from function FwlAddDmzNode \n");
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDeleteDmzNode                                 */
/*                                                                          */
/*    Description        : Deletes the Dmz node from the dmz list           */
/*                                                                          */
/*    Input(s)           : au1DmzIpAddress - Dmz host that needs to be      */
/*                                           deleted.                       */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : SUCCESS if Dmz host exists, otherwise FAILURE    */
/****************************************************************************/

PUBLIC UINT4
FwlDeleteDmzNode (UINT4 u4DmzIpAddress)
{
    UINT4               u4Status = FWL_FAILURE;
    tFwlDmzInfo        *pDmzNode = NULL;

    pDmzNode = (tFwlDmzInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into the fn. FwlDeleteDmzNode\n");

    /* search for the DMZ node in the dmz list that is to be deleted
     * Get the dmz node pointer from the dmz list.
     */

    pDmzNode = FwlSearchDmzNode (u4DmzIpAddress);
    if (pDmzNode != NULL)
    {
        TMO_SLL_Delete (&gFwlDmzList, (tTMO_SLL_NODE *) pDmzNode);
        FWL_DBG (FWL_DBG_INSPECT, "\n Given DMZ Node is deleted \n");

        /* Free the Node */
        if (MemReleaseMemBlock (FWL_DMZ_PID, (UINT1 *) pDmzNode) == MEM_SUCCESS)
        {
            u4Status = FWL_SUCCESS;
            FWL_DBG (FWL_DBG_INSPECT, "\n Memory Free - DMZ Node \n");
        }
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from the fn. FwlDeleteDmzNode\n");
    return u4Status;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDeleteIPv6DmzNode                             */
/*                                                                          */
/*    Description        : Deletes the IPv6 Dmz node from the dmz list      */
/*                                                                          */
/*    Input(s)           : au1DmzIpAddress - Dmz host that needs to be      */
/*                                           deleted.                       */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : SUCCESS if Dmz host exists, otherwise FAILURE    */
/****************************************************************************/

PUBLIC UINT4
FwlDeleteIPv6DmzNode (tIp6Addr DmzIpHost)
{
    UINT4               u4Status = FWL_FAILURE;
    tFwlIPv6DmzInfo    *pDmzNode = NULL;

    pDmzNode = (tFwlIPv6DmzInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into the fn. FwlDeleteIPv6DmzNode\n");

    /* search for the DMZ node in the dmz list that is to be deleted
     * Get the dmz node pointer from the dmz list.
     */

    pDmzNode = FwlSearchIPv6DmzNode (DmzIpHost);
    if (pDmzNode != NULL)
    {
        TMO_SLL_Delete (&gFwlIPv6DmzList, (tTMO_SLL_NODE *) pDmzNode);
        FWL_DBG (FWL_DBG_INSPECT, "\n Given DMZ Node is deleted \n");

        /* Free the Node */
        if (MemReleaseMemBlock (FWL_IPV6_DMZ_PID,
                                (UINT1 *) pDmzNode) == MEM_SUCCESS)
        {
            u4Status = FWL_SUCCESS;
            FWL_DBG (FWL_DBG_INSPECT, "\n Memory Free - DMZ Node \n");
        }
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from the fn. FwlDeleteIPv6DmzNode\n");
    return u4Status;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSearchDmzNode                                 */
/*                                                                          */
/*    Description        : Searches the Dmz node in dmz list for the given  */
/*                         dmz host IP.                                     */
/*                                                                          */
/*    Input(s)           : u4DmzIpAddress - Ip Address of the DMZ host      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Pointer to the DMZ node if exists, otherwise     */
/*                         returns NULL.                                    */
/****************************************************************************/
PUBLIC tFwlDmzInfo *
FwlSearchDmzNode (UINT4 u4DmzIpAddress)
{
    tFwlDmzInfo        *pDmzNode = NULL;
    UINT1               u1Status = FWL_NOT_MATCH;
    pDmzNode = (tFwlDmzInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into the fn. FwlSearchDmzNode\n");

    /* Search the DMZ host from the List for the Particular dmz. If found
     * return the pointer to the dmz Node , else NULL.
     */

    TMO_SLL_Scan (&gFwlDmzList, pDmzNode, tFwlDmzInfo *)
    {
        if (u4DmzIpAddress == pDmzNode->u4IpIndex)
        {
            u1Status = FWL_MATCH;
            break;
        }
    }
    if (u1Status == FWL_MATCH)
    {
        FWL_DBG (FWL_DBG_EXIT, "\n Exiting from the fn. FwlSearchDmzNode\n");
        return pDmzNode;
    }

    return NULL;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSearchIPv6DmzNode                             */
/*                                                                          */
/*    Description        : Searches the Dmz node in dmz list for the given  */
/*                         IPv6 dmz host                                    */
/*                                                                          */
/*    Input(s)           : uDmzIpHost- Ipv6    Address of the DMZ host      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Pointer to the DMZ node if exists, otherwise     */
/*                         returns NULL.                                    */
/****************************************************************************/
PUBLIC tFwlIPv6DmzInfo *
FwlSearchIPv6DmzNode (tIp6Addr DmzIpHost)
{
    tFwlIPv6DmzInfo    *pDmzNode = NULL;
    UINT1               u1Status = FWL_NOT_MATCH;
    pDmzNode = (tFwlIPv6DmzInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into the fn. FwlSearchIPv6DmzNode\n");

    /* Search the DMZ host from the List for the Particular dmz. If found
     * return the pointer to the dmz Node , else NULL.
     */

    TMO_SLL_Scan (&gFwlIPv6DmzList, pDmzNode, tFwlIPv6DmzInfo *)
    {
        if (Ip6AddrCompare (DmzIpHost, pDmzNode->dmzIpv6Index) == FWL_ZERO)
        {
            u1Status = FWL_MATCH;
            break;
        }
    }
    if (u1Status == FWL_MATCH)
    {
        FWL_DBG (FWL_DBG_EXIT, "\n Exiting from the fn. FwlSearchDmzNode\n");
        return pDmzNode;
    }

    return NULL;
}

PUBLIC UINT4
FwlInspectDmzPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tIpHeader IpHdr, tHLInfo HLInfo,
                  tIcmpInfo IcmpHdr, UINT4 u4IfaceNum, UINT1 u1Direction,
                  UINT4 u4TotalHdrLen)
{
    UINT4               u4RetVal = FWL_FAILURE;
    UINT4               u4IfIndex = FWL_ZERO;
    INT4                i4Status = FWL_ZERO;
    UINT1               u1TcpFlags = FWL_ZERO;
    void               *pDmzNode = NULL;
    UNUSED_PARAM (u4TotalHdrLen);
    UNUSED_PARAM (HLInfo);

    /* Get the TCP flags Byte */
    FWL_GET_1_BYTE (pBuf, (UINT4) (IpHdr.u1HeadLen + FWL_TCP_CODE_BIT_OFFSET),
                    u1TcpFlags);
    u1TcpFlags = u1TcpFlags & FWL_OFFSET_3f;

    if (IpHdr.DestAddr.u4AddrType == FWL_IP_VERSION_4)
    {
        pDmzNode = (tFwlDmzInfo *) FwlSearchDmzNode (IpHdr.DestAddr.v4Addr);
    }
    else if (IpHdr.DestAddr.u4AddrType == FWL_IP_VERSION_6)
    {
        pDmzNode =
            (tFwlIPv6DmzInfo *) FwlSearchIPv6DmzNode (IpHdr.DestAddr.v6Addr);
    }
    if (pDmzNode)
    {
        if (FwlChkIfExtInterface (u4IfaceNum) == FWL_SUCCESS)
        {
            if (u1Direction == FWL_DIRECTION_IN)
            {
                if (IpHdr.SrcAddr.u4AddrType == FWL_IP_VERSION_4)
                {
                    i4Status =
                        SecUtilIpIsLocalNet (((UINT4) IpHdr.SrcAddr.v4Addr),
                                             &u4IfIndex);
                }
                else if (IpHdr.SrcAddr.u4AddrType == FWL_IP_VERSION_6)
                {
                    i4Status =
                        Sec6UtilIpIsLocalNet (((tIp6Addr *) (&IpHdr.SrcAddr.
                                                             v6Addr)),
                                              &u4IfIndex);
                }

                if (i4Status == OSIX_SUCCESS)
                {
                    if (FwlChkIfExtInterface (u4IfIndex) == FWL_FAILURE)
                    {
                        u4RetVal = FWL_IP_SPOOF_ATTACK;
                    }
                    else
                    {
                        u4RetVal = FWL_SUCCESS;
                    }
                }
                else
                {
                    u4RetVal = FWL_SUCCESS;
                }
            }
            else
            {
                u4RetVal = FWL_IP_SPOOF_ATTACK;
            }
        }
        else
        {
            /* WLAN and LAN ports are considered as internal interfaces */
            u4RetVal = FWL_SUCCESS;
        }
    }
    else
    {
        if (IpHdr.DestAddr.u4AddrType == FWL_IP_VERSION_4)
        {
            pDmzNode = (tFwlDmzInfo *) FwlSearchDmzNode (IpHdr.SrcAddr.v4Addr);
        }
        else if (IpHdr.DestAddr.u4AddrType == FWL_IP_VERSION_6)
        {
            pDmzNode =
                (tFwlIPv6DmzInfo *) FwlSearchIPv6DmzNode (IpHdr.SrcAddr.v6Addr);
        }
        if (pDmzNode)
        {
            if (FwlChkIfExtInterface (u4IfaceNum) == FWL_SUCCESS)
            {
                if (u1Direction == FWL_DIRECTION_IN)
                {
                    /* DMZ IP should never be a source IP from WAN */
                    u4RetVal = FWL_IP_SPOOF_ATTACK;
                }
                else
                {
                    /* For Firewall in Kernel, check DMZ functionality for outboud
                     * packets, since firewall is applied only for the packets 
                     * sent/received on WAN interface. 
                     */

                    /* Check if the pkt is initializing any connection
                     * from DMZ host.
                     */
                    if ((IpHdr.u1Proto == FWL_TCP) &&
                        (u1TcpFlags == FWL_TCP_SYN_MASK) &&
                        (u1TcpFlags != FWL_TCP_ACK_MASK))
                    {
                        FwlLogMessage (FWL_DMZ_DROP_OUTBOUND_SESSION,
                                       u4IfaceNum, pBuf, FWL_LOG_BRF,
                                       FWLLOG_CRITICAL_LEVEL, NULL);
                        return FWL_FAILURE;
                    }
                    else if ((IpHdr.u1Proto == FWL_ICMP) &&
                             (IcmpHdr.u1Type != FWL_ICMP_TYPE_ECHO_REPLY))
                    {
                        /* Only ICMP ECHO REPLY is allowed from DMZ host */
                        FwlLogMessage (FWL_DMZ_DROP_OUTBOUND_SESSION,
                                       u4IfaceNum, pBuf, FWL_LOG_BRF,
                                       FWLLOG_CRITICAL_LEVEL, NULL);
                        return FWL_FAILURE;
                    }
                    u4RetVal = FWL_SUCCESS;
                }
            }
            else                /* Interface is INTERNAL */
            {
                if (u1Direction == FWL_DIRECTION_IN)
                {
                    /* Check if the pkt is initializing any connection
                     * from DMZ host.
                     */
                    if ((IpHdr.u1Proto == FWL_TCP) &&
                        (u1TcpFlags == FWL_TCP_SYN_MASK) &&
                        (u1TcpFlags != FWL_TCP_ACK_MASK))
                    {
                        FwlLogMessage (FWL_DMZ_DROP_OUTBOUND_SESSION,
                                       u4IfaceNum, pBuf, FWL_LOG_BRF,
                                       FWLLOG_CRITICAL_LEVEL, NULL);
                        return FWL_FAILURE;
                    }
                    else if ((IpHdr.u1Proto == FWL_ICMP) &&
                             (IcmpHdr.u1Type != FWL_ICMP_TYPE_ECHO_REPLY))
                    {
                        /* Only ICMP ECHO REPLY is allowed from DMZ host */
                        FwlLogMessage (FWL_DMZ_DROP_OUTBOUND_SESSION,
                                       u4IfaceNum, pBuf, FWL_LOG_BRF,
                                       FWLLOG_CRITICAL_LEVEL, NULL);
                        return FWL_FAILURE;
                    }
                    else
                    {
                        u4RetVal = FWL_SUCCESS;
                    }
                }
                else
                {
                    /* WLAN and LAN ports are considered as internal interfaces */
                    u4RetVal = FWL_SUCCESS;
                }
            }
        }
    }                            /*End-of-Main-ELSE */

    if (u4RetVal == FWL_IP_SPOOF_ATTACK)
    {
        /* The packet is spoofed one, Log the message */
        INC_INSPECT_COUNT;
        INC_DISCARD_COUNT;
        INC_SPOOF_DISCARD_COUNT;

        FwlLogMessage (FWL_IP_SPOOF_ATTACK, u4IfaceNum, pBuf,
                       FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                       (UINT1 *) FWL_MSG_ATTACK);

        return FWL_FAILURE;
    }
    else if (u4RetVal == FWL_SUCCESS)
    {
        /* Check to deny an IcmpEchoRequest packet if ICMP inspect option
         * is set */
        if ((gIfaceInfo.u1IcmpType == FWL_ICMP_INSPECT_OPTION) &&
            (FwlChkIfExtInterface (u4IfaceNum) == FWL_SUCCESS) &&
            (u1Direction == FWL_DIRECTION_IN) &&
            (IpHdr.u1Proto == FWL_ICMP) &&
            (IcmpHdr.u1Type == FWL_ICMP_TYPE_ECHO_REQUEST))
        {
            /* Packet is to be denied. */
            MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_INSPECT, "FWL",
                     "\n Respond ping to DMZ is disabled, deny the packet \n");
            INC_INSPECT_COUNT;
            INC_DISCARD_COUNT;
            INC_ICMP_DISCARD_COUNT;

            /* Log the message */
            FwlLogMessage (FWL_ICMP_CODE, u4IfaceNum, pBuf,
                           FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                           (UINT1 *) FWL_MSG_ATTACK);
            return FWL_FAILURE;
        }
        else
        {
            if (FwlChkIfExtInterface (u4IfaceNum) == FWL_SUCCESS)
            {
                /* Increment packet count to/from DMZ */
                INC_INSPECT_COUNT;
                INC_PERMIT_COUNT;
            }
            return FWL_SUCCESS;
        }
    }
    else
    {
        return (FWL_DMZ_NOT_FOUND);
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSetDefaultDmzValue                            */
/*                                                                          */
/*    Description        : Set the Default values for UrlFilter Node        */
/*                                                                          */
/*    Input(s)           : u4FwlDmzIpIndez   --Dmz Ip address               */
/*                                                                          */
/*    Output(s)          : pDmzNode        --Pointer to Filter Node Created */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
FwlSetDefaultDmzValue (UINT4 u4FwlDmzIpIndex, tFwlDmzInfo * pFwlDmzNode)
{
    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering into Function FwlSetDefaultDmzValue\n");
    /* Memory allocation and initialisation of the Dmz node with
     * default values .
     */
    TMO_SLL_Init_Node (&pFwlDmzNode->nextFwlDmzInfo);
    pFwlDmzNode->u4IpIndex = u4FwlDmzIpIndex;
    pFwlDmzNode->u1RowStatus = FWL_NOT_READY;

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from Function FwlSetDefaultDmzValue\n");
}                                /*End of Function */
