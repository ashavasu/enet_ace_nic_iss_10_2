/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlsrc.c,v 1.22 2016/09/17 12:43:07 siva Exp $
 *
 * Description: File containing routines related to show running 
 * configuration of firewall module
 * *******************************************************************/
#ifndef __FWLSRC_C__
#define __FWLSRC_C__

#include "fwlinc.h"
#include "snmputil.h"
static UINT1        gu1FwlModeDispFlag;

extern UINT4        SecUtilGetIdsStatus(VOID);
/*****************************************************************************/
/* Function Name      : FwlShowRunningConfigScalar                           */
/*                                                                           */
/* Description        : Displays current configurations done in FIREWALL     */
/*                      scalar objects                                       */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS                                          */
/*****************************************************************************/
INT4
FwlShowRunningConfigScalar (tCliHandle CliHandle)
{

#ifdef IP_WANTED
    tCliFwlShowConfig   CliFwlShowConfig;
    INT4                i4FwlGlobalStatus = FWL_ZERO;
    INT4                i4FwlglobalICMPSwitch = FWL_ZERO;
    INT4                i4FwlglobalICMPv6Switch = FWL_ZERO;
    INT4                i4FwlGlobalIpv6SpoofFiltering = FWL_ZERO;
    INT4                i4FwlglobalSpoofFiltering = FWL_ZERO;
    INT4                i4FwlGlobalTcpIntercept = FWL_ZERO;
    INT4                i4FwlCliShowFwlGlobalConfig = FWL_ZERO;
    INT4                i4FwlGlobalNetBiosFiltering = FWL_ZERO;
    INT4                i4FwlDefnTcpInterceptThreshold = FWL_ZERO;
    UINT4               u4FwlDefnInterceptTimeout = FWL_ZERO;
    INT4                i4RetValFwlGlobalIdsStatus = FWL_ZERO;
    INT4                i4RetValFwlGlobalLoadIdsRules = FWL_ZERO;
    INT4                i4FwlGlobalNetBiosLan2Wan = FWL_ZERO;
    UINT4               u4Interface = FWL_ZERO;
    INT4                i4RetValFwlIfICMPType = FWL_ZERO;
    INT4                i4RetValFwlIfICMPv6Type = FWL_ZERO;
    UINT4               u4RetFwlGlobalLogFileSize = FWL_ZERO;
    UINT4               u4RetValFwlGlobalIdsLogThreshold = FWL_ZERO;
    UINT4               u4RetValFwlGlobalIdsLogSize = FWL_ZERO;
    UINT4               u4RetValFwlGlobalLogSize = FWL_ZERO;
#endif 

#ifdef LNXIP4_WANTED
    INT4                i4RetValAcceptRedirect = FWL_ZERO;
    INT4                i4RetValSmurfAttack = FWL_ZERO;
    INT4                i4RetValLandAttack = FWL_ZERO;
    INT4                i4RetValShortHeader = FWL_ZERO;
#endif 

    gu1FwlModeDispFlag = FWL_ZERO;
#ifdef IP_WANTED
    nmhGetFwlGlobalMasterControlSwitch (&i4FwlGlobalStatus);
    if (i4FwlGlobalStatus != FWL_ENABLE)
    {
        CliPrintf (CliHandle, "firewall\r\n");
        CliPrintf (CliHandle, " disable\r\n");
        gu1FwlModeDispFlag = TRUE;
    }
    nmhGetFwlGlobalICMPControlSwitch (&i4FwlglobalICMPSwitch);
    if (i4FwlglobalICMPSwitch != FWL_DISABLE)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
        CliPrintf (CliHandle, " icmp generate\r\n");
    }

    nmhGetFwlGlobalICMPv6ControlSwitch (&i4FwlglobalICMPv6Switch);
    if (i4FwlglobalICMPv6Switch != FWL_DISABLE)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
        CliPrintf (CliHandle, "ipv6 icmp generate\r\n");
    }

    nmhGetFwlGlobalIpSpoofFiltering (&i4FwlglobalSpoofFiltering);
    if (i4FwlglobalSpoofFiltering != FWL_ENABLE)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
        CliPrintf (CliHandle, " no ip verify reverse path \r\n");
    }

    nmhGetFwlGlobalIpv6SpoofFiltering (&i4FwlGlobalIpv6SpoofFiltering);
    if (i4FwlGlobalIpv6SpoofFiltering != FWL_ENABLE)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
        CliPrintf (CliHandle, " no ipv6 verify unicast reverse path\r\n");
    }

    nmhGetFwlGlobalTcpIntercept (&i4FwlGlobalTcpIntercept);
    if (i4FwlGlobalTcpIntercept != FWL_ENABLE)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
        CliPrintf (CliHandle, " no ip inspect tcp \r\n");
    }

    nmhGetFwlGlobalUrlFiltering (&i4FwlCliShowFwlGlobalConfig);
    if (i4FwlCliShowFwlGlobalConfig != FWL_DISABLE)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
        CliPrintf (CliHandle, " url filtering enable\r\n");
    }

    nmhGetFwlGlobalNetBiosFiltering (&i4FwlGlobalNetBiosFiltering);
    if (i4FwlGlobalNetBiosFiltering != FWL_DISABLE)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
        CliPrintf (CliHandle, " net bios filtering enable\r\n");
    }

    nmhGetFwlDefnTcpInterceptThreshold (&i4FwlDefnTcpInterceptThreshold);
    if (i4FwlDefnTcpInterceptThreshold != FWL_DEFAULT_TCP_SYN_LIMIT)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
        CliPrintf (CliHandle, " ip inspect tcp half open %d\r\n",
                   i4FwlDefnTcpInterceptThreshold);
    }

    nmhGetFwlDefnInterceptTimeout (&u4FwlDefnInterceptTimeout);
    if (u4FwlDefnInterceptTimeout != FWL_ONE)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
        CliPrintf (CliHandle, " ip inspect tcp syn wait %d\r\n",
                   u4FwlDefnInterceptTimeout);
    }

    nmhGetFwlIfICMPType ((INT4) u4Interface, &i4RetValFwlIfICMPType);
    if (i4RetValFwlIfICMPType == FWL_ICMP_TYPE_ECHO_REQUEST)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
        CliPrintf (CliHandle, " icmp Inspect\r\n");
    }

    nmhGetFwlIfICMPv6MsgType ((INT4) u4Interface, &i4RetValFwlIfICMPv6Type);
    if (i4RetValFwlIfICMPv6Type != FWL_ICMPV6_NO_INSPECT)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
        CliPrintf (CliHandle, " ipv6 icmp Inspect");
        if ((i4RetValFwlIfICMPv6Type & FWL_ICMPV6_DESTINATION_UNREACHABLE)
            != FWL_ZERO)
        {
            CliPrintf (CliHandle, " destination-unreachable");
        }
        if ((i4RetValFwlIfICMPv6Type & FWL_ICMPV6_TIME_EXCEEDED) != FWL_ZERO)
        {
            CliPrintf (CliHandle, " time-exceeded");
        }
        if ((i4RetValFwlIfICMPv6Type & FWL_ICMPV6_PARAMETER_PROBLEM)
            != FWL_ZERO)
        {
            CliPrintf (CliHandle, " parameter-problem");
        }
        if ((i4RetValFwlIfICMPv6Type & FWL_ICMPV6_ECHO_REQUEST) != FWL_ZERO)
        {
            CliPrintf (CliHandle, " echo-request");
        }
        if ((i4RetValFwlIfICMPv6Type & FWL_ICMPV6_ECHO_REPLY) != FWL_ZERO)
        {
            CliPrintf (CliHandle, " echo-reply");
        }
        if ((i4RetValFwlIfICMPv6Type & FWL_ICMPV6_REDIRECT) != FWL_ZERO)
        {
            CliPrintf (CliHandle, " redirect");
        }

        if ((i4RetValFwlIfICMPv6Type & FWL_ICMPV6_INFORMATION_REQUEST)
            != FWL_ZERO)
        {
            CliPrintf (CliHandle, " information-request");
        }
        if ((i4RetValFwlIfICMPv6Type & FWL_ICMPV6_INFORMATION_REPLY)
            != FWL_ZERO)
        {
            CliPrintf (CliHandle, " information-reply");
        }
        CliPrintf (CliHandle, "\r\n");
    }

    nmhGetFwlGlobalNetBiosLan2Wan (&i4FwlGlobalNetBiosLan2Wan);

    if (i4FwlGlobalNetBiosLan2Wan != FWL_DISABLE)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
        CliPrintf (CliHandle, " pass netbios lan2wan enable\r\n");
    }
    nmhGetFwlTrapThreshold (&CliFwlShowConfig.i4TrapThreshold);
    if (CliFwlShowConfig.i4TrapThreshold != FWL_DEF_TRAP_THRESHOLD)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
        CliPrintf (CliHandle,
                   " trap threshold %ld\r\n", CliFwlShowConfig.i4TrapThreshold);
    }

    nmhGetFwlIfFragments (FWL_GLOBAL_IDX,
                          &CliFwlShowConfig.i4RetValFwlIfFragments);
    nmhGetFwlIfFragmentSize (FWL_GLOBAL_IDX,
                             (UINT4 *) (VOID *) &CliFwlShowConfig.
                             u2MaxFragmentSize);

    if (CliFwlShowConfig.u2MaxFragmentSize != FWL_DEFAULT_FRAGMENT_SIZE)

    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
        CliPrintf (CliHandle, " ip filter fragments large %d\r\n",
                   CliFwlShowConfig.u2MaxFragmentSize);
    }

    if (CliFwlShowConfig.i4RetValFwlIfFragments == FWL_NO_FRAGMENT)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
        CliPrintf (CliHandle, " no ip filter fragments\r\n");
    }

    nmhGetFwlGlobalTrap (&CliFwlShowConfig.i4TrapStatus);
    if (CliFwlShowConfig.i4TrapStatus != FWL_DISABLE)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
        CliPrintf (CliHandle, " trap enable\r\n");
    }
    nmhGetFwlGlobalLogFileSize (&u4RetFwlGlobalLogFileSize);
    if (u4RetFwlGlobalLogFileSize != FWL_LOGFILE_LIMIT_SIZE)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
        CliPrintf (CliHandle, " firewall logging filesize %d\r\n",
                   u4RetFwlGlobalLogFileSize);
    }
    nmhGetFwlGlobalLogSizeThreshold (&u4RetValFwlGlobalLogSize);
    if (u4RetValFwlGlobalLogSize != FWL_DEF_LOG_SIZE_THRESH)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
        CliPrintf (CliHandle, " firewall logging logsize-threshold %d\r\n",
                   u4RetValFwlGlobalLogSize);
    }

    nmhGetFwlGlobalIdsLogSize (&u4RetValFwlGlobalIdsLogSize);
    if (u4RetValFwlGlobalIdsLogSize != IDS_MAX_LOG_FILESIZE)
    {
        CliPrintf (CliHandle, "ids-logging filesize %d\r\n",
                   u4RetValFwlGlobalIdsLogSize);
    }

    nmhGetFwlGlobalIdsLogThreshold (&u4RetValFwlGlobalIdsLogThreshold);
    if (u4RetValFwlGlobalIdsLogThreshold != IDS_DEFAULT_LOG_THRESHOLD)
    {
        CliPrintf (CliHandle, "ids-logging logsize-threshold %d\r\n",
                   u4RetValFwlGlobalIdsLogThreshold);
    }

    nmhGetFwlGlobalIdsStatus (&i4RetValFwlGlobalIdsStatus);
    if (i4RetValFwlGlobalIdsStatus != IDS_ENABLE)
    {
        if (i4RetValFwlGlobalIdsStatus == IDS_DISABLE)
        {
            CliPrintf (CliHandle, "ids disable \r\n");
        }
    }
/* Rules are loaded by default when IDS is enabled */
    nmhGetFwlGlobalLoadIdsRules (&i4RetValFwlGlobalLoadIdsRules);
    if (i4RetValFwlGlobalLoadIdsRules != IDS_RULES_LOADED)
    {
        if ((i4RetValFwlGlobalLoadIdsRules == IDS_RULES_NOT_LOADED) && ((SecUtilGetIdsStatus ()) == IDS_ENABLE))
        {
            CliPrintf (CliHandle, "unload ids \r\n");
        }
    }
#endif


#ifdef LNXIP4_WANTED
/*Dos Attack related configurations*/
    nmhGetFwlDosAttackAcceptRedirect (&i4RetValAcceptRedirect);
    nmhGetFwlDosAttackAcceptSmurfAttack (&i4RetValSmurfAttack);
    nmhGetFwlDosLandAttack (&i4RetValLandAttack);
    nmhGetFwlDosShortHeaderAttack (&i4RetValShortHeader);
    if (i4RetValAcceptRedirect  == FWL_ONE)
    {   
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }

        CliPrintf (CliHandle, "dosAttack accept_redirect \r\n");
    }
    if (i4RetValSmurfAttack  != FWL_ONE)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }

        CliPrintf (CliHandle, "no dosAttack smurf_attack \r\n");
    }
    if (i4RetValLandAttack  == FWL_ONE)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }

        CliPrintf (CliHandle, "dosAttack land_attack \r\n");
    }
    if (i4RetValShortHeader != FWL_ZERO)
    {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }

        CliPrintf (CliHandle, "dosAttack short_header \r\n");
    }
#endif

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FwlShowRunningConfiTabular                           */
/*                                                                           */
/* Description        : Displays current configurations in FIREWALL          */
/*                      for specifiec Table                                  */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS                                          */
/*****************************************************************************/
INT4
FwlShowRunningConfigTabular (tCliHandle CliHandle)
{
#ifdef IP_WANTED
    tSNMP_OCTET_STRING_TYPE *pFwlFilterName;
    tSNMP_OCTET_STRING_TYPE NextFwlFilterName;
    tSNMP_OCTET_STRING_TYPE Temp;
    tSNMP_OCTET_STRING_TYPE *pFwlAclName;
    tSNMP_OCTET_STRING_TYPE NextFwlAclName;
    tSNMP_OCTET_STRING_TYPE *pIPAddr;
    tSNMP_OCTET_STRING_TYPE NextIPAddr;
    tSNMP_OCTET_STRING_TYPE FwlDmzIpv6Index;
    tIp6Addr            DmzIp6Host;
    CHR1               *pu1String = NULL;
    UINT1               au1DmzIp6HostAddr[FWL_IPV6_ADDR_LEN] = { FWL_ZERO };
    UINT1               au1NextFwlFilterName[FWL_MAX_FILTER_NAME_LEN]
        = { FWL_ZERO };
    UINT1               au1Temp[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    UINT1               au1NextFwlAclName[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
    UINT1               au1IfName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    UINT4               u4FwlDmzIpIndex = FWL_ZERO;
    UINT4               u4PrefLen = FWL_ZERO;
    UINT4               u4NextPrefLen = FWL_ZERO;
    UINT4               u4FlowId = FWL_ZERO;
    INT4                i4IfIndex = FWL_ZERO;
    INT4                i4IfType = FWL_ZERO;
    INT4                i4NextFwlAclIfIndex = FWL_INVALID;
    INT4                i4NextFwlAclDirection = FWL_INVALID;
    INT4                i4FwlAclDirection = FWL_INVALID;
    INT4                i4FwlAclIfIndex = FWL_INVALID;
    INT4                i4SeqNum = FWL_ZERO;
    INT4                i4Action = FWL_ZERO;
    INT4                i4RowStatus = FWL_ZERO;
    INT4                i4LogTrigger = FWL_ZERO;
    INT4                i4FragAction = FWL_ZERO;
    INT4                i4Proto = FWL_ZERO;
    INT4                i4FilterAccounting = FWL_ZERO;
    INT4                i4RetValFwlIfIpOptions = FWL_ZERO;
    INT4                i4AddrType = FWL_ZERO;
    INT4                i4NextAddrType = FWL_ZERO;
    INT4                i4ListFound = FWL_FALSE;
    INT4                i4Dscp = FWL_ZERO;
    INT4                i4TrapThreshold = FWL_ZERO;
#endif
#ifdef LNXIP4_WANTED
    INT4                i4PortNumber = FWL_ZERO;
    INT4                i4PortType = FWL_ZERO;
    INT4                i4RateLimitValue = FWL_ZERO;
    INT4                i4BurstSize = FWL_ZERO;
    INT4                i4TrafficMode = FWL_ZERO;
    INT4                i4RateLimitPortIndex = FWL_ZERO;
    INT4                i4RpfInIndex = FWL_ZERO;
    INT4                i4NextRateLimitPortIndex = FWL_ZERO;
    INT4                i4NextRpfInIndex = FWL_ZERO;
    INT4                i4RpfMode = FWL_ZERO;
    UINT1               au1IfName[FWL_MAX_FILTER_NAME_LEN];

#endif

#ifdef IP_WANTED
    if ((pFwlFilterName =
         allocmem_octetstring (FWL_MAX_FILTER_NAME_LEN + FWL_ONE)) == NULL)
    {
        return (CLI_FAILURE);
    }

    if ((pFwlAclName =
         allocmem_octetstring (FWL_MAX_ACL_NAME_LEN + FWL_ONE)) == NULL)
    {
        free_octetstring (pFwlFilterName);
        return (CLI_FAILURE);
    }

    if ((pIPAddr = allocmem_octetstring (FWL_IPV6_ADDR_LEN + FWL_ONE)) == NULL)
    {
        free_octetstring (pFwlFilterName);
        free_octetstring (pFwlAclName);
        return (CLI_FAILURE);
    }

    NextFwlFilterName.pu1_OctetList = au1NextFwlFilterName;
    Temp.pu1_OctetList = au1Temp;

    NextFwlAclName.pu1_OctetList = au1NextFwlAclName;

    MEMSET (au1NextFwlFilterName, FWL_ZERO, sizeof (au1NextFwlFilterName));
    MEMSET (au1NextFwlAclName, FWL_ZERO, sizeof (au1NextFwlAclName));
    MEMSET (au1Temp, FWL_ZERO, sizeof (au1Temp));
    MEMSET (&DmzIp6Host, FWL_ZERO, sizeof (tIp6Addr));
    MEMSET (au1DmzIp6HostAddr, FWL_ZERO, sizeof (au1DmzIp6HostAddr));
    FwlDmzIpv6Index.pu1_OctetList = au1DmzIp6HostAddr;
    /* show running config for BlackList-WhiteList IpAddress  Table */

    if (nmhGetNextIndexFwlDefnIfTable (i4IfIndex, &i4IfIndex) == SNMP_SUCCESS)
    {
        do
        {
            if (CfaCliGetIfName ((UINT4) i4IfIndex, (INT1 *) au1Temp) ==
                CLI_FAILURE)
            {

                free_octetstring (pFwlFilterName);
                free_octetstring (pFwlAclName);
                free_octetstring (pIPAddr);

                return CLI_FAILURE;
            }
            /*Get first two char of interface name. e.g. "gi","po" */
            STRNCPY (au1IfName, au1Temp, FWL_TWO_BYTES);
            au1IfName[FWL_INDEX_0] = (UINT1) TOLOWER (au1IfName[FWL_INDEX_0]);
            au1IfName[FWL_INDEX_2] = '\0';

#ifdef CFA_UNIQUE_INTF_NAME
            if (STRCMP (au1IfName, "et") == FWL_ZERO)
            {
                MEMSET (au1IfName, FWL_ZERO, sizeof (au1IfName));
                SNPRINTF ((char *) au1IfName, (sizeof (au1IfName)
                                               - FWL_ONE), "%s%s",
                          "ethernet ", (au1Temp + FWL_TWO));
            }
#else
            if (STRCMP (au1IfName, "gi") == FWL_ZERO)
            {
                MEMSET (au1IfName, FWL_ZERO, sizeof (au1IfName));
                SNPRINTF ((char *) au1IfName, (sizeof (au1IfName)
                                               - FWL_ONE), "%s%s",
                          "gigabitethernet ", (au1Temp + FWL_TWO));
            }
#endif
            else if (STRCMP (au1IfName, "ex") == FWL_ZERO)
            {
                MEMSET (au1IfName, FWL_ZERO, sizeof (au1IfName));
                SNPRINTF ((char *) au1IfName, (sizeof (au1IfName)
                                               - FWL_ONE), "%s%s",
                          "extreme-ethernet ", (au1Temp + FWL_TWO));
            }
            else if (STRCMP (au1IfName, "fa") == FWL_ZERO)
            {
                MEMSET (au1IfName, FWL_ZERO, sizeof (au1IfName));
                SNPRINTF ((char *) au1IfName, (sizeof (au1IfName)
                                               - FWL_ONE), "%s%s",
                          "fastethernet ", (au1Temp + FWL_TWO));
            }
            else if (STRCMP (au1IfName, "vl") == FWL_ZERO)
            {
                MEMSET (au1IfName, FWL_ZERO, sizeof (au1IfName));
                SNPRINTF ((char *) au1IfName, (sizeof (au1IfName)
                                               - FWL_ONE), "%s%s", "vlan ",
                          (au1Temp));
            }
            nmhGetFwlIfTrapThreshold (i4IfIndex, &i4TrapThreshold);
            nmhGetFwlIfIfType (i4IfIndex, &i4IfType);
            if (FWL_EXTERNAL_IF == i4IfType)
            {
                CliPrintf (CliHandle, "untrusted port %s", au1IfName);
                if (i4TrapThreshold != FWL_DEF_TRAP_THRESHOLD)
                {
                    CliPrintf (CliHandle, " trap-threshold %ld",
                               i4TrapThreshold);
                }
                CliPrintf (CliHandle, "\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "no untrusted port %s\r\n", au1IfName);
            }
            /*copy rest of string as port no. e.g 0/2,0/3 */

        }
        while (nmhGetNextIndexFwlDefnIfTable (i4IfIndex, &i4IfIndex) ==
               SNMP_SUCCESS);
    }

    MEMSET (au1Temp, FWL_ZERO, sizeof (au1Temp));

    if (nmhGetFirstIndexFwlDefnBlkListTable (&i4AddrType,
                                             pIPAddr,
                                             &u4PrefLen) == SNMP_SUCCESS)
    {
        i4ListFound = FWL_TRUE;
        CliPrintf (CliHandle, "dynamic-filter blacklist\r\n");

        i4NextAddrType = i4AddrType;
        u4NextPrefLen = u4PrefLen;
        MEMCPY (&NextIPAddr, pIPAddr, sizeof (*pIPAddr));
        do
        {
            CliPrintf (CliHandle, "address %s %d\r\n",
                       NextIPAddr.pu1_OctetList, u4NextPrefLen);

            i4AddrType = i4NextAddrType;
            u4PrefLen = u4NextPrefLen;
            MEMCPY (pIPAddr, &NextIPAddr, sizeof (NextIPAddr));
        }
        while (nmhGetNextIndexFwlDefnBlkListTable (i4AddrType,
                                                   &i4NextAddrType,
                                                   pIPAddr,
                                                   &NextIPAddr,
                                                   u4PrefLen,
                                                   &u4NextPrefLen) ==
               SNMP_SUCCESS);
    }


    if (nmhGetFirstIndexFwlDefnWhiteListTable (&i4AddrType,
                                               pIPAddr,
                                               &u4PrefLen) == SNMP_SUCCESS)
    {
        if (FWL_TRUE == i4ListFound)
        {
            CliPrintf (CliHandle, "!\r\n");
        }
        CliPrintf (CliHandle, "dynamic-filter whitelist\r\n");

        i4NextAddrType = i4AddrType;
        u4NextPrefLen = u4PrefLen;
        MEMCPY (&NextIPAddr, pIPAddr, sizeof (*pIPAddr));
        do
        {
            CliPrintf (CliHandle, "address %s %d\r\n",
                       NextIPAddr.pu1_OctetList, u4NextPrefLen);

            i4AddrType = i4NextAddrType;
            u4PrefLen = u4NextPrefLen;
            MEMCPY (pIPAddr, &NextIPAddr, sizeof (NextIPAddr));
        }
        while (nmhGetNextIndexFwlDefnWhiteListTable (i4AddrType,
                                                     &i4NextAddrType,
                                                     pIPAddr,
                                                     &NextIPAddr,
                                                     u4PrefLen,
                                                     &u4NextPrefLen) ==
               SNMP_SUCCESS);
    }

  

    /* show running config for the Filter  Table */

    if (nmhGetFirstIndexFwlDefnFilterTable (pFwlFilterName) == SNMP_SUCCESS)
    {

        MEMCPY (NextFwlFilterName.pu1_OctetList, pFwlFilterName->pu1_OctetList,
                pFwlFilterName->i4_Length);
        NextFwlFilterName.i4_Length = pFwlFilterName->i4_Length;
        do
        {
            nmhGetFwlFilterAddrType (&NextFwlFilterName, &i4AddrType);
            nmhGetFwlFilterRowStatus (&NextFwlFilterName, &i4RowStatus);

            if (i4RowStatus == FWL_ACTIVE)
            {

                au1NextFwlFilterName[NextFwlFilterName.i4_Length] =
                    FWL_END_OF_STRING;

                if ((STRCMP (NextFwlFilterName.pu1_OctetList, "Def_FTP_Filter")
                     != FWL_ZERO)
                    &&
                    (STRCMP
                     (NextFwlFilterName.pu1_OctetList,
                      "Def_TELNET_Filter") != FWL_ZERO)
                    &&
                    (STRCMP (NextFwlFilterName.pu1_OctetList, "Def_SMTP_Filter")
                     != FWL_ZERO)
                    &&
                    (STRCMP
                     (NextFwlFilterName.pu1_OctetList,
                      "Def_DNS_TCP_Filter") != FWL_ZERO)
                    &&
                    (STRCMP
                     (NextFwlFilterName.pu1_OctetList,
                      "Def_DNS_UDP_Filter") != FWL_ZERO)
                    &&
                    (STRCMP (NextFwlFilterName.pu1_OctetList, "Def_HTTP_Filter")
                     != FWL_ZERO)
                    &&
                    (STRCMP
                     (NextFwlFilterName.pu1_OctetList, "Def_HTTPS_Filter")
                     != FWL_ZERO)
                    &&
                    (STRCMP (NextFwlFilterName.pu1_OctetList, "Def_POP3_Filter")
                     != FWL_ZERO)
                    &&
                    (STRCMP (NextFwlFilterName.pu1_OctetList, "Def_IMAP_Filter")
                     != FWL_ZERO)
                    &&
                    (STRCMP
                     (NextFwlFilterName.pu1_OctetList,
                      "Def_SNTP_UDP_Filter") != FWL_ZERO)
                    &&
                    (STRCMP (NextFwlFilterName.pu1_OctetList,
                             "Def_FTP_IPv6_Filter") != FWL_ZERO)
                    &&
                    (STRCMP
                     (NextFwlFilterName.pu1_OctetList,
                      "Def_TELNET_IPv6_Filter") != FWL_ZERO)
                    &&
                    (STRCMP (NextFwlFilterName.pu1_OctetList,
                             "Def_SMTP_IPv6_Filter") != FWL_ZERO)
                    &&
                    (STRCMP
                     (NextFwlFilterName.pu1_OctetList,
                      "Def_DNS_TCP_IPv6_Filter") != FWL_ZERO)
                    &&
                    (STRCMP
                     (NextFwlFilterName.pu1_OctetList,
                      "Def_DNS_UDP_IPv6_Filter") != FWL_ZERO)
                    &&
                    (STRCMP (NextFwlFilterName.pu1_OctetList,
                             "Def_HTTP_IPv6_Filter") != FWL_ZERO)
                    &&
                    (STRCMP
                     (NextFwlFilterName.pu1_OctetList,
                      "Def_HTTPS_IPv6_Filter") != FWL_ZERO)
                    &&
                    (STRCMP (NextFwlFilterName.pu1_OctetList,
                             "Def_POP3_IPv6_Filter")
                     != FWL_ZERO)
                    &&
                    (STRCMP (NextFwlFilterName.pu1_OctetList,
                             "Def_IMAP_IPv6_Filter")
                     != FWL_ZERO)
                    &&
                    (STRCMP
                     (NextFwlFilterName.pu1_OctetList,
                      "Def_SNTP_UDP_IPv6_Filter") != FWL_ZERO))
                {
                    if (gu1FwlModeDispFlag != TRUE)
                    {
                        CliPrintf (CliHandle, "firewall\r\n");
                        gu1FwlModeDispFlag = TRUE;
                    }
                    if (i4AddrType == FWL_IP_VERSION_4)
                    {
                        CliPrintf (CliHandle, " filter add %s",
                                   NextFwlFilterName.pu1_OctetList);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "ipv6 filter add %s",
                                   NextFwlFilterName.pu1_OctetList);
                    }

                    nmhGetFwlFilterSrcAddress (&NextFwlFilterName, &Temp);
                    if ((STRCMP (Temp.pu1_OctetList, "0.0.0.0/0") == FWL_ZERO)
                        || (STRCMP (Temp.pu1_OctetList, "::/0") == FWL_ZERO))
                    {
                        CliPrintf (CliHandle, " any");
                    }
                    else
                    {
                        CliPrintf (CliHandle, " %s", Temp.pu1_OctetList);
                    }
                    nmhGetFwlFilterDestAddress (&NextFwlFilterName, &Temp);
                    if ((STRCMP (Temp.pu1_OctetList, "0.0.0.0/0") == FWL_ZERO)
                        || (STRCMP (Temp.pu1_OctetList, "::/0") == FWL_ZERO))
                    {
                        CliPrintf (CliHandle, " any");
                    }
                    else
                    {
                        CliPrintf (CliHandle, " %s", Temp.pu1_OctetList);
                    }

                    nmhGetFwlFilterProtocol (&NextFwlFilterName, &i4Proto);
                    switch (i4Proto)
                    {
                        case FWL_TCP:
                            CliPrintf (CliHandle, " tcp");
                            break;

                        case FWL_UDP:
                            CliPrintf (CliHandle, " udp");
                            break;

                        case FWL_ICMP:
                            CliPrintf (CliHandle, " icmp");
                            break;

                        case FWL_IGMP:
                            CliPrintf (CliHandle, " igmp");
                            break;

                        case FWL_GGP:
                            CliPrintf (CliHandle, " ggp");
                            break;

                        case FWL_IP:
                            CliPrintf (CliHandle, " ip");
                            break;

                        case FWL_EGP:
                            CliPrintf (CliHandle, " egp");
                            break;

                        case FWL_IGP:
                            CliPrintf (CliHandle, " igp");
                            break;
                        case FWL_NVP:
                            CliPrintf (CliHandle, " nvp");
                            break;

                        case FWL_RSVP:
                            CliPrintf (CliHandle, " rsvp");
                            break;

                        case FWL_IGRP:
                            CliPrintf (CliHandle, " igrp");
                            break;

                        case FWL_OSPFIGP:
                            CliPrintf (CliHandle, " ospf");
                            break;

                        case FWL_ANY:
                            CliPrintf (CliHandle, " any");
                            break;

                        default:
                            CliPrintf (CliHandle, " other");
                            break;

                    }

                    nmhGetFwlFilterSrcPort (&NextFwlFilterName, &Temp);
                    if ((STRCMP (Temp.pu1_OctetList, ">1") != FWL_ZERO)
                        && (*Temp.pu1_OctetList != '\0'))
                    {
                        CliPrintf (CliHandle, " srcport %s",
                                   Temp.pu1_OctetList);
                    }

                    nmhGetFwlFilterDestPort (&NextFwlFilterName, &Temp);
                    if ((STRCMP (Temp.pu1_OctetList, ">1") != FWL_ZERO)
                        && (*Temp.pu1_OctetList != '\0'))
                    {
                        CliPrintf (CliHandle, " desport %s",
                                   Temp.pu1_OctetList);
                    }
                    nmhGetFwlFilterAccounting (&NextFwlFilterName,
                                               &i4FilterAccounting);
                    if (i4FilterAccounting != FWL_DISABLE)

                    {
                        CliPrintf (CliHandle, " accounting filter %s",
                                   NextFwlFilterName);
                    }

                    if (i4AddrType == FWL_IP_VERSION_6)
                    {
                        nmhGetFwlFilterFlowId (&NextFwlFilterName, &u4FlowId);
                        if (u4FlowId != FWL_FLOW_ID_MIN_VALUE)
                        {
                            CliPrintf (CliHandle, " Flow label %d", u4FlowId);
                        }
                        nmhGetFwlFilterDscp (&NextFwlFilterName, &i4Dscp);
                        if (i4Dscp != FWL_DSCP_MIN_VALUE)
                        {
                            CliPrintf (CliHandle, " Dscp %d\r\n", i4Dscp);
                        }
                    }
                    CliPrintf (CliHandle, "\r\n");

                }
            }

            MEMCPY (pFwlFilterName->pu1_OctetList,
                    NextFwlFilterName.pu1_OctetList,
                    NextFwlFilterName.i4_Length);
            pFwlFilterName->i4_Length = NextFwlFilterName.i4_Length;

        }
        while (nmhGetNextIndexFwlDefnFilterTable (pFwlFilterName,
                                                  &NextFwlFilterName) ==
               SNMP_SUCCESS);

    }
    /* show running config for the ACL Table */

    if (nmhGetFirstIndexFwlDefnAclTable (&i4FwlAclIfIndex, pFwlAclName,
                                         &i4FwlAclDirection) == SNMP_SUCCESS)
    {

        MEMCPY (NextFwlAclName.pu1_OctetList, pFwlAclName->pu1_OctetList,
                pFwlAclName->i4_Length);
        NextFwlAclName.i4_Length = pFwlAclName->i4_Length;
        au1NextFwlAclName[NextFwlAclName.i4_Length] = FWL_END_OF_STRING;
        i4NextFwlAclIfIndex = i4FwlAclIfIndex;
        i4NextFwlAclDirection = i4FwlAclDirection;

        do
        {

            nmhGetFwlAclRowStatus (i4FwlAclIfIndex,
                                   pFwlAclName,
                                   i4FwlAclDirection, &i4RowStatus);

            if (i4RowStatus == FWL_ACTIVE)
            {

                if ((STRCMP (NextFwlAclName.pu1_OctetList, "Def_FTP_ACL")
                     != FWL_ZERO)
                    && (STRCMP (NextFwlAclName.pu1_OctetList, "Def_TELNET_ACL")
                        != FWL_ZERO)
                    && (STRCMP (NextFwlAclName.pu1_OctetList, "Def_SMTP_ACL") !=
                        FWL_ZERO)
                    && (STRCMP (NextFwlAclName.pu1_OctetList, "Def_DNS_TCP_ACL")
                        != FWL_ZERO)
                    && (STRCMP (NextFwlAclName.pu1_OctetList, "Def_DNS_UDP_ACL")
                        != FWL_ZERO)
                    && (STRCMP (NextFwlAclName.pu1_OctetList, "Def_HTTP_ACL") !=
                        FWL_ZERO)
                    && (STRCMP (NextFwlAclName.pu1_OctetList, "Def_HTTPS_ACL")
                        != FWL_ZERO)
                    && (STRCMP (NextFwlAclName.pu1_OctetList, "Def_POP3_ACL") !=
                        FWL_ZERO)
                    && (STRCMP (NextFwlAclName.pu1_OctetList, "Def_IMAP_ACL") !=
                        FWL_ZERO)
                    && (STRCMP (NextFwlAclName.pu1_OctetList, "Def_SNTP_ACL") !=
                        FWL_ZERO)
                    && (STRCMP (NextFwlAclName.pu1_OctetList,
                                "Def_FTP_IPv6_ACL") != FWL_ZERO)
                    && (STRCMP (NextFwlAclName.pu1_OctetList,
                                "Def_TELNET_IPv6_ACL") != FWL_ZERO)
                    && (STRCMP (NextFwlAclName.pu1_OctetList,
                                "Def_SMTP_IPv6_ACL") != FWL_ZERO)
                    && (STRCMP (NextFwlAclName.pu1_OctetList,
                                "Def_DNS_TCP_IPv6_ACL") != FWL_ZERO)
                    && (STRCMP (NextFwlAclName.pu1_OctetList,
                                "Def_DNS_UDP_IPv6_ACL") != FWL_ZERO)
                    && (STRCMP (NextFwlAclName.pu1_OctetList,
                                "Def_HTTP_IPv6_ACL") != FWL_ZERO)
                    && (STRCMP (NextFwlAclName.pu1_OctetList,
                                "Def_HTTPS_IPv6_ACL") != FWL_ZERO)
                    && (STRCMP (NextFwlAclName.pu1_OctetList,
                                "Def_POP3_IPv6_ACL") != FWL_ZERO)
                    && (STRCMP (NextFwlAclName.pu1_OctetList,
                                "Def_IMAP_IPv6_ACL") != FWL_ZERO)
                    && (STRCMP (NextFwlAclName.pu1_OctetList,
                                "Def_SNTP_IPv6_ACL") != FWL_ZERO))
                {

                    CliPrintf (CliHandle, " access-list %s",
                               NextFwlAclName.pu1_OctetList);

                    nmhGetFwlAclAction (i4NextFwlAclIfIndex,
                                        &NextFwlAclName,
                                        i4NextFwlAclDirection, &i4Action);

                    if (i4NextFwlAclDirection == FWL_DIRECTION_IN)
                    {
                        CliPrintf (CliHandle, " in");
                    }
                    else
                    {
                        CliPrintf (CliHandle, " out");
                    }
                    nmhGetFwlRuleFilterSet (&NextFwlAclName, &Temp);
                    au1Temp[Temp.i4_Length] = '\0';

                    CliPrintf (CliHandle, " %s", au1Temp);

                    if (i4Action == FWL_PERMIT)
                    {

                        CliPrintf (CliHandle, " permit");
                    }
                    else
                    {
                        CliPrintf (CliHandle, " deny");
                    }

                    nmhGetFwlAclSequenceNumber (i4NextFwlAclIfIndex,
                                                &NextFwlAclName,
                                                i4NextFwlAclDirection,
                                                &i4SeqNum);

                    CliPrintf (CliHandle, " %d", i4SeqNum);

                    nmhGetFwlAclLogTrigger (i4NextFwlAclIfIndex,
                                            &NextFwlAclName,
                                            i4NextFwlAclDirection,
                                            &i4LogTrigger);

                    if (i4LogTrigger == FWL_LOG_BRF)
                    {
                        CliPrintf (CliHandle, " log brief");
                    }
                    else if (i4LogTrigger == FWL_LOG_DTL)
                    {
                        CliPrintf (CliHandle, " log detail");
                    }

                    nmhGetFwlAclFragAction (i4NextFwlAclIfIndex,
                                            &NextFwlAclName,
                                            i4NextFwlAclDirection,
                                            &i4FragAction);
                    if (i4FragAction == FWL_DENY)
                    {
                        CliPrintf (CliHandle, " fragment deny");
                    }
                    CliPrintf (CliHandle, "\r\n");
                }
                MEMCPY (pFwlAclName->pu1_OctetList,
                        NextFwlAclName.pu1_OctetList, NextFwlAclName.i4_Length);
                pFwlAclName->i4_Length = NextFwlAclName.i4_Length;

                i4FwlAclIfIndex = i4NextFwlAclIfIndex;
                i4FwlAclDirection = i4NextFwlAclDirection;
            }
        }
        while (nmhGetNextIndexFwlDefnAclTable (i4FwlAclIfIndex,
                                               &i4NextFwlAclIfIndex,
                                               pFwlAclName,
                                               &NextFwlAclName,
                                               i4FwlAclDirection,
                                               &i4NextFwlAclDirection) ==
               SNMP_SUCCESS);
    }

    /* show running config for the Url filter  Table */

    if (nmhGetFirstIndexFwlUrlFilterTable (pFwlFilterName) == SNMP_SUCCESS)
    {

        MEMCPY (NextFwlFilterName.pu1_OctetList,
                pFwlFilterName->pu1_OctetList, pFwlFilterName->i4_Length);
        NextFwlFilterName.i4_Length = pFwlFilterName->i4_Length;
        do
        {
            au1NextFwlFilterName[NextFwlFilterName.i4_Length] =
                FWL_END_OF_STRING;

            if (gu1FwlModeDispFlag != TRUE)
            {
                CliPrintf (CliHandle, "firewall\r\n");
                gu1FwlModeDispFlag = TRUE;
            }

            CliPrintf (CliHandle, " url filter add %s \r\n", NextFwlFilterName);

            MEMCPY (pFwlFilterName->pu1_OctetList,
                    NextFwlFilterName.pu1_OctetList,
                    NextFwlFilterName.i4_Length);
            pFwlFilterName->i4_Length = NextFwlFilterName.i4_Length;

        }
        while (nmhGetNextIndexFwlUrlFilterTable (pFwlFilterName,
                                                 &NextFwlFilterName) ==
               SNMP_SUCCESS);
    }

    nmhGetFwlIfIpOptions (FWL_GLOBAL_IDX, &i4RetValFwlIfIpOptions);

    switch (i4RetValFwlIfIpOptions)
    {
        case FWL_SOURCE_ROUTE_OPTION:

            if (gu1FwlModeDispFlag != TRUE)
            {
                CliPrintf (CliHandle, "firewall\r\n");
                gu1FwlModeDispFlag = TRUE;
            }
            CliPrintf (CliHandle, " ip inspect option %s\r\n", "srcroute");
            break;

        case FWL_TRACE_ROUTE_OPTION:
            CliPrintf (CliHandle, " ip inspect option  %s\r\n", "trcroute");
            break;

        case FWL_RECORD_ROUTE_OPTION:
            CliPrintf (CliHandle, " ip inspect option  %s\r\n", "recordroute");
            break;

        case FWL_TIMESTAMP_OPTION:
            CliPrintf (CliHandle, " ip inspect option  %s\r\n", "timestamp");
            break;
        case FWL_NO_OPTION:
            CliPrintf (CliHandle, " no ip inspect option \r\n");
            break;
        default:
            CliPrintf (CliHandle, "\r\n");
            break;
    }
    /* show running config for the DMZ  Table */

    nmhGetFirstIndexFwlDefnDmzTable (&u4FwlDmzIpIndex);
    do
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4FwlDmzIpIndex);
        if (STRCMP (pu1String, "0.0.0.0") != FWL_ZERO)
        {
            if (gu1FwlModeDispFlag != TRUE)
            {
                CliPrintf (CliHandle, "firewall\r\n");
                gu1FwlModeDispFlag = TRUE;
            }

            CliPrintf (CliHandle, " dmz  %s\r\n", pu1String);
        }

    }
    while (nmhGetNextIndexFwlDefnDmzTable
           (u4FwlDmzIpIndex, &u4FwlDmzIpIndex) == SNMP_SUCCESS);

    /* Show running config for IPv6 DMZ table */
    if (nmhGetFirstIndexFwlDefnIPv6DmzTable (&FwlDmzIpv6Index) == SNMP_SUCCESS)
    {
        do
        {
            MEMCPY (&DmzIp6Host, FwlDmzIpv6Index.pu1_OctetList, IP6_ADDR_SIZE);
            if (gu1FwlModeDispFlag != TRUE)
            {
                CliPrintf (CliHandle, "firewall\r\n");
                gu1FwlModeDispFlag = TRUE;
            }
            CliPrintf (CliHandle, "ipv6 dmz %s\r\n",
                       Ip6PrintNtop (&DmzIp6Host));
        }
        while (nmhGetNextIndexFwlDefnIPv6DmzTable
               (&FwlDmzIpv6Index, &FwlDmzIpv6Index) == SNMP_SUCCESS);
    }
    CliPrintf (CliHandle, "!\r\n");
    free_octetstring (pFwlFilterName);
    free_octetstring (pFwlAclName);
    free_octetstring (pIPAddr);

#endif
#ifdef LNXIP4_WANTED
     if (nmhGetFirstIndexFwlRateLimitTable (&i4RateLimitPortIndex) == SNMP_SUCCESS)
     {
         
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
		i4NextRateLimitPortIndex = i4RateLimitPortIndex;
        do
        {
		    i4RateLimitPortIndex = i4NextRateLimitPortIndex;
            CliPrintf (CliHandle, "rate-limit proto-type ");
			nmhGetFwlRateLimitPortNumber (i4NextRateLimitPortIndex,&i4PortNumber);
            nmhGetFwlRateLimitPortType   (i4NextRateLimitPortIndex,&i4PortType);
            nmhGetFwlRateLimitValue      (i4NextRateLimitPortIndex,&i4RateLimitValue);
            nmhGetFwlRateLimitBurstSize  (i4NextRateLimitPortIndex,&i4BurstSize);
            nmhGetFwlRateLimitTrafficMode (i4NextRateLimitPortIndex,&i4TrafficMode);
            if(FWL_RATE_LIMIT_TCP == i4PortType)
            {
              CliPrintf (CliHandle, "tcp ");
			}
            else if(FWL_RATE_LIMIT_UDP == i4PortType)
		    {
              CliPrintf (CliHandle, "udp port %d ",i4PortNumber);
			}
            else
			{
              CliPrintf (CliHandle, "icmp ");
			}
            if(FWL_RATE_LIMIT_PPS == i4TrafficMode)
            {
              CliPrintf (CliHandle, "mode pps value %d\r\n",i4RateLimitValue);
			}
            else if(FWL_RATE_LIMIT_KBPS == i4TrafficMode)
		    {
              CliPrintf (CliHandle, "mode kbps value %d\r\n",i4RateLimitValue);
			}
            else
			{
              CliPrintf (CliHandle, "mode bps value %d\r\n",i4RateLimitValue);
		    }
        }
        while (nmhGetNextIndexFwlRateLimitTable(i4RateLimitPortIndex,&i4NextRateLimitPortIndex) ==
               SNMP_SUCCESS);
    }

     if (nmhGetFirstIndexFwlSnorkTable (&i4RateLimitPortIndex) == SNMP_SUCCESS)
     {
        if (gu1FwlModeDispFlag != TRUE)
        {
            CliPrintf (CliHandle, "firewall\r\n");
            gu1FwlModeDispFlag = TRUE;
        }
		i4NextRateLimitPortIndex = i4RateLimitPortIndex;
        do
        {
		    i4RateLimitPortIndex = i4NextRateLimitPortIndex;
              CliPrintf (CliHandle, "dosAttack snork_attack %d\n",i4NextRateLimitPortIndex);
         }
        while (nmhGetNextIndexFwlSnorkTable(i4RateLimitPortIndex,&i4NextRateLimitPortIndex) ==
               SNMP_SUCCESS);
    }

     if (nmhGetFirstIndexFwlRpfTable (&i4RpfInIndex) == SNMP_SUCCESS)
     {
		i4NextRpfInIndex = i4RpfInIndex;
        do
        {
            CliPrintf (CliHandle, "!\n");
            MEMSET (au1IfName, 0, sizeof (au1IfName));
            CfaCliConfGetIfName ((UINT4) i4NextRpfInIndex, (INT1 *) au1IfName);
            if(i4NextRpfInIndex == CFA_INVALID_IFINDEX)
            {
             CliPrintf (CliHandle, "interface cpu0\n");
            }
            else
            {
             CliPrintf (CliHandle, "interface " , au1IfName);
            }
            CliPrintf (CliHandle, " %s \r\n" , au1IfName);
            nmhGetFwlRpfMode(i4NextRpfInIndex,&i4RpfMode);
            if(i4RpfMode == FWL_ONE)
            {
            CliPrintf (CliHandle, "ip rpf-check loose\n");
            }
            else if(i4RpfMode == FWL_TWO)
            {
            CliPrintf (CliHandle, "ip rpf-check strict\n");
            }
            else
            {
            CliPrintf (CliHandle, "ip rpf-check \n");
            }
            CliPrintf (CliHandle, "end\n");
		    i4RpfInIndex = i4NextRpfInIndex;
         }
        while (nmhGetNextIndexFwlRpfTable(i4RpfInIndex,&i4NextRpfInIndex) ==
               SNMP_SUCCESS);
    }

#endif

    return (CLI_SUCCESS);
}
#endif /* __FWLSRC_C__ */
