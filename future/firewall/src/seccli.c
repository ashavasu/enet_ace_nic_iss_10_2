/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: seccli.c,v 1.8 2013/10/31 11:13:30 siva Exp $
 *
 * Description: File containing CLI routines for firewall module
 * *******************************************************************/

#ifndef __SECCLI_C__
#define __SECCLI_C__
/* SOURCE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : seccli.c                                         |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Future Sofware Ltd                               |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : CLI                                              |
 * |                                                                           |
 * |  MODULE NAME           : SECURITY                                         |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for CLI SECURITY MODULE commands        |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */
#include "fwlinc.h"
#include "secidscli.h"

extern INT1 nmhGetFsSecDebugOption ARG_LIST ((INT4 *));
extern INT1 nmhSetFsSecDebugOption ARG_LIST ((INT4));
extern INT1 nmhTestv2FsSecDebugOption ARG_LIST ((UINT4 *, INT4));

/*****************************************************************************/
/*     FUNCTION NAME    : cli_process_sec_cmd                                */
/*                                                                           */
/*     DESCRIPTION      : This function takes in variable no. of arguments   */
/*                        and process the commands for the security module   */
/*                        defined in seccmd.def                              */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Command -  Command Identifier                    */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
cli_process_sec_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[CLI_MAX_ARGS] = { NULL };
    INT1                i1Argno = FWL_ZERO;
    INT4                i4RetVal = CLI_SUCCESS;

    va_start (ap, u4Command);

    /* Walk through the rest of the arguements and store in args array. 
     * Store fourteen arguements at the max. This is because fwl commands do not
     * take more than fourteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */
    while (FWL_ONE)
    {
        args[i1Argno++] = va_arg (ap, UINT4 *);

        if (i1Argno == SEC_CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);

    switch (u4Command)
    {
        case CLI_SEC_SHOW_VERSION:
            i4RetVal = SecCliShowIdsVersion (CliHandle);
            break;

        case CLI_SEC_RELOAD_IDS:
            i4RetVal = SecCliReloadIds (CliHandle, CLI_ENABLE);
            break;

        case CLI_SEC_LOAD_IDS:
            i4RetVal = SecCliLoadIds (CliHandle);
            break;

        case CLI_SEC_UNLOAD_IDS:
            i4RetVal = SecCliUnloadIds (CliHandle);
            break;

        case CLI_SEC_IDS_SET_LOG_SIZE:
            i4RetVal = SecCliSetLogFileSize (CliHandle, *args[FWL_INDEX_1]);
            break;

        case CLI_SEC_IDS_SET_LOG_THRESHOLD:
            i4RetVal =
                SecCliSetLogSizeThreshold (CliHandle, *args[FWL_INDEX_1]);
            break;

        case CLI_SEC_SET_DEBUG_LEVEL:
            i4RetVal = SecCliSetDebugLevel (CliHandle,
                                            CLI_PTR_TO_I4 (args[FWL_INDEX_1]),
                                            CLI_ENABLE);
            break;

        case CLI_SEC_RESET_DEBUG_LEVEL:
            i4RetVal = SecCliSetDebugLevel (CliHandle,
                                            CLI_PTR_TO_I4 (args[FWL_INDEX_1]),
                                            CLI_DISABLE);
            break;

        case CLI_SEC_IDS_SET_STATUS:
            i4RetVal = SecCliSetIdsStatus (CliHandle,
                                           CLI_PTR_TO_U4 (args[FWL_INDEX_1]));
            break;

        case CLI_SEC_IDS_SHOW_STATUS:
            i4RetVal = SecCliShowIdsStatus (CliHandle);
            break;

        default:
            CliPrintf (CliHandle, "ERROR: Unknown command !\r\n");
            i4RetVal = CLI_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SecCliShowIdsVersion                               */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current version of      */
/*                        IDS(Intrusion Detection System)                    */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SecCliShowIdsVersion (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE IdsVersion;
    UINT1               au1IdsVersion[IDS_VERSION_LEN] = { FWL_ZERO };

    MEMSET (au1IdsVersion, FWL_ZERO, IDS_VERSION_LEN);
    IdsVersion.pu1_OctetList = &au1IdsVersion[FWL_INDEX_0];

    nmhGetFwlGlobalIdsVersionInfo (&IdsVersion);

    if (IdsVersion.i4_Length != FWL_ZERO)
    {
        CliPrintf (CliHandle, "\r\n%-33s : %s\r\n", "IDS Version",
                   IdsVersion.pu1_OctetList);
    }
    else
    {
        CliPrintf (CliHandle, "\r\n%-33s\r\n", " %% IDS is disabled");
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SecCliShowIdsStatus                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current status  of      */
/*                        IDS(Intrusion Detection System)                    */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SecCliShowIdsStatus (tCliHandle CliHandle)
{
    INT4                i4IdsStatus = FWL_ZERO;
    INT4                i4IdsRulesStatus = FWL_ZERO;

    nmhGetFwlGlobalIdsStatus (&i4IdsStatus);

    if (i4IdsStatus == IDS_ENABLE)
    {
        CliPrintf (CliHandle, "\r\nIDS Status   :  Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nIDS Status   :  Disabled\r\n");
    }

    nmhGetFwlGlobalLoadIdsRules (&i4IdsRulesStatus);

    if (i4IdsRulesStatus == IDS_RULES_LOADED)
    {
        CliPrintf (CliHandle, "\rIDS Rules    :  Loaded\r");
    }
    else
    {
        CliPrintf (CliHandle, "\rIDS Rules    :  Not Loaded\r\n");
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SecCliSetDebugLevel                                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the debug Level for security    */
/*                        module.                                            */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        i4CliDebugVal - Debug value.                       */
/*                        u1TraceFlag - Enable/Disable trace                 */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SecCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugVal,
                     UINT1 u1TraceFlag)
{
    INT4                i4DebugVal = FWL_ZERO;
    UINT4               u4ErrorCode = FWL_ZERO;

    /* Get existing Trace Option */
    nmhGetFsSecDebugOption (&i4DebugVal);

    if (u1TraceFlag == CLI_ENABLE)
    {
        i4DebugVal |= i4CliDebugVal;
    }
    else
    {
        i4DebugVal &= (~(i4CliDebugVal));
    }
    if (nmhTestv2FsSecDebugOption (&u4ErrorCode, i4DebugVal) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set trace messages\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsSecDebugOption (i4DebugVal) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set trace messages\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SecCliReloadIds                                    */
/*                                                                           */
/*     DESCRIPTION      : This function informs to Snort for reload with     */
/*                        new set of rules/configurations                    */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SecCliReloadIds (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode = FWL_ZERO;

    if (nmhTestv2FwlGlobalReloadIds (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to Reload Ids \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFwlGlobalReloadIds (i4Status) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%-33s", "Successfully informed"
                   " to Snort for reload\r\n");
        CliPrintf (CliHandle,
                   " Disabling IDS while loading " "rules/configuration\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n%-33s\r\n", " %% IDS is disabled");
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SecCliLoadIds                                      */
/*                                                                           */
/*     DESCRIPTION      : This function informs to Snort to add new set      */
/*                        of rules/configurations to existing set            */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SecCliLoadIds (tCliHandle CliHandle)
{
    UINT4               u4ErrorCode = FWL_ZERO;

    if (nmhTestv2FwlGlobalLoadIdsRules (&u4ErrorCode,
                                        IDS_LOAD_RULES) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to load Ids Rules\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFwlGlobalLoadIdsRules (IDS_LOAD_RULES) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    else
    {
        CliPrintf (CliHandle, "\r\n%-33s", "Successfully informed"
                   " to Snort for load rules\r\n");
        CliPrintf (CliHandle,
                   "Disabling IDS while loading " "rules/configuration\r\n");
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SecCliUnloadIds                                    */
/*                                                                           */
/*     DESCRIPTION      : This function informs to Snort to remove the       */
/*                        rules/configurations                               */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SecCliUnloadIds (tCliHandle CliHandle)
{
    UINT4               u4ErrorCode = FWL_ZERO;

    if (nmhTestv2FwlGlobalLoadIdsRules (&u4ErrorCode,
                                        IDS_UNLOAD_RULES) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to unload Ids Rules\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFwlGlobalLoadIdsRules (IDS_UNLOAD_RULES) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    else
    {
        CliPrintf (CliHandle, "Disabling IDS while unloading"
                   "rules/configuration\r\n");
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SecCliSetLogFileSize                               */
/*                                                                           */
/*     DESCRIPTION      : This function sets the file size for the ids-log   */
/*                        file.                                              */
/*                                                                           */
/*     INPUT            : CliHandle       -  CLIHandler                      */
/*                        u4IdsLogFileSie -  Log filesize to be set          */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
SecCliSetLogFileSize (tCliHandle CliHandle, UINT4 u4IdsLogFileSize)
{
    UINT4               u4ErrorCode = FWL_ZERO;

    if (SNMP_FAILURE == nmhTestv2FwlGlobalIdsLogSize (&u4ErrorCode,
                                                      u4IdsLogFileSize))
    {
        CliPrintf (CliHandle, "\r%% Invalid Ids log filesize\r\n");
        return CLI_FAILURE;
    }

    if (SNMP_FAILURE == nmhSetFwlGlobalIdsLogSize (u4IdsLogFileSize))
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SecCliSetLogSizeThreshold                          */
/*                                                                           */
/*     DESCRIPTION      : This function sets the logsize threshold for the   */
/*                        ids-log file.                                      */
/*                                                                           */
/*     INPUT            : CliHandle         -  CLIHandler                    */
/*                        u4IdsLogThreshold -  Logsize threshold to be set   */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
SecCliSetLogSizeThreshold (tCliHandle CliHandle, UINT4 u4IdsLogThreshold)
{
    UINT4               u4ErrorCode = FWL_ZERO;

    if (SNMP_FAILURE == nmhTestv2FwlGlobalIdsLogThreshold (&u4ErrorCode,
                                                           u4IdsLogThreshold))
    {
        CliPrintf (CliHandle, "\r%% Invalid Ids logsize threshold\r\n");
        return CLI_FAILURE;
    }

    if (SNMP_FAILURE == nmhSetFwlGlobalIdsLogThreshold (u4IdsLogThreshold))
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SecCliSetIdsStatus                                 */
/*                                                                           */
/*     DESCRIPTION      : This function sets the global status of IDS        */
/*                                                                           */
/*     INPUT            : CliHandle         -  CLIHandler                    */
/*                        u4IdsStatus       -  IDS log status to be set      */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SecCliSetIdsStatus (tCliHandle CliHandle, UINT4 u4IdsStatus)
{
    UINT4               u4ErrorCode = FWL_ZERO;

    if (SNMP_FAILURE ==
        nmhTestv2FwlGlobalIdsStatus (&u4ErrorCode, (INT4) u4IdsStatus))
    {
        CliPrintf (CliHandle, "\r%% Invalid IDS status\r\n");
        return CLI_FAILURE;
    }

    if (SNMP_FAILURE == nmhSetFwlGlobalIdsStatus ((INT4) u4IdsStatus))
    {
        CliPrintf (CliHandle, "%% Enable IDS after loading rules\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

#endif /* __SECCLI_C__ */
