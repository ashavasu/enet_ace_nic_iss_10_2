/********************************************************************
 * Copyright (C) Future Sotware,2002
 *
 *  $Id: fwlsz.c,v 1.7 2014/07/02 10:23:42 siva Exp $
 *
 *  Description:This file contains functions required for
 *              provision of sizing support to Firewall
 *
 ********************************************************************/

#ifndef _FIREWALLSZ_C
#define _FIREWALLSZ_C
#include "fwlinc.h"
#include "fwlsz.h"

extern INT4
 
 
 
 
IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                 tFsModSizingParams * pModSizingParams);
INT4
FirewallSizingMemCreateMemPools ()
{
    INT4                i4RetVal = FWL_ZERO;
    INT4                i4SizingId = FWL_ZERO;

    for (i4SizingId = FWL_ZERO; i4SizingId < FIREWALL_MAX_SIZING_ID;
         i4SizingId++)
    {
        if ((gi4SysOperMode == SEC_KERN_USER) &&
            (i4SizingId != MAX_FWL_LOG_BUFFER_SIZING_ID) &&
            (i4SizingId != MAX_FWL_WEB_LOG_BUFFER_SIZING_ID) &&
            (i4SizingId != MAX_FWL_PARTIAL_LOG_BLOCK_SIZING_ID) &&
            (i4SizingId != MAX_FWL_CLI_ACCESS_LIST_SIZING_ID) &&
            (i4SizingId != MAX_FWL_CLI_FILTER_SIZING_ID))
        {
            continue;
        }
        i4RetVal = (INT4)
            MemCreateMemPool (gaFsFIREWALLSizingParams[i4SizingId].u4StructSize,
                              gaFsFIREWALLSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(gaFIREWALLMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            FirewallSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
FirewallSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    if (IssSzRegisterModuleSizingParams (pu1ModName, gaFsFIREWALLSizingParams)
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

VOID
FirewallSizingMemDeleteMemPools ()
{
    INT4                i4SizingId = FWL_ZERO;

    for (i4SizingId = FWL_ZERO; i4SizingId < FIREWALL_MAX_SIZING_ID;
         i4SizingId++)
    {
        if ((gi4SysOperMode == SEC_KERN_USER) &&
            (i4SizingId != MAX_FWL_LOG_BUFFER_SIZING_ID) &&
            (i4SizingId != MAX_FWL_WEB_LOG_BUFFER_SIZING_ID) &&
            (i4SizingId != MAX_FWL_PARTIAL_LOG_BLOCK_SIZING_ID))
        {
            continue;
        }
        if (gaFIREWALLMemPoolIds[i4SizingId] != FWL_ZERO)
        {
            MemDeleteMemPool (gaFIREWALLMemPoolIds[i4SizingId]);
            gaFIREWALLMemPoolIds[i4SizingId] = FWL_ZERO;
        }
    }
    return;
}
#endif
