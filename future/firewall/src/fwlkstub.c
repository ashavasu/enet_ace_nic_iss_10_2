/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlkstub.c,v 1.4 2011/12/08 13:27:56 siva Exp $
 *
 * Description:This file contains all kernel related stubs  
 *
 *******************************************************************/
#include "fwlinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function Name    : SysLogRegister                                         */
/*                                                                           */
/* Description      : This function is invoked by protocol modules to        */
/*                    register the module name and log level with syslog     */
/*                                                                           */
/* Input Parameters : pu1Name - Module Name                                  */
/*                    u4Level - Module Level                                 */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : Identifier - to be used with syslog for reference     */
/*                     -1 - On failure                                       */
/*****************************************************************************/
INT4
SysLogRegister (CONST UINT1 *pu1Name, UINT4 u4Level)
{
    UNUSED_PARAM (pu1Name);
    UNUSED_PARAM (u4Level);
    gi4FwlSysLogId = FIREWALL_SYSLOG_MODULEID;
    return gi4FwlSysLogId;
}

/*****************************************************************************/
/* Function Name     : FwlNpEnableDosAttack                                  */
/* Description       : This function is invoked by Firewall module  to       */
/*                    invoke hardware filter creation                        */
/* Input Parameters  : None                                                  */
/* Output Parameters : None                                                  */
/* Return Value      : None                                                  */
/*****************************************************************************/

VOID
FwlNpEnableDosAttack (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name     : FwlNpEnableIpHeaderValidation                         */
/* Description       : This function is invoked by Firewall module  to       */
/*                    invoke hardware filter creation                        */
/* Input Parameters  : None                                                  */
/* Output Parameters : None                                                  */
/* Return Value      : None                                                  */
/*****************************************************************************/
VOID
FwlNpEnableIpHeaderValidation (VOID)
{
    return;
}

/*************************************************************************/
/* Function Name     :  IpGetRoute                                       */
/*                                                                       */
/* Description       :  This function finds the route for given          */
/*                      destination in default context                   */
/*                      It returns failure when Trie returns sink route  */
/*                      for the destination.                             */
/*                                                                       */
/* Input(s)          :  1. u4Dest - The destination address for which the*/
/*                                  route has to be found                */
/*                      2. u1Tos  - The Type of Service                  */
/*                                                                       */
/* Output(s)         :  1. pu2RtPort - The interface index through which */
/*                                      the packet has to be forwarded   */
/*                      2. pu4RtGw   - The address of the next hop       */
/*                                      gateway                          */
/*                                                                       */
/* Returns           :  IP_SUCCESS | IP_FAILURE                                */
/*************************************************************************/
INT4
IpGetRoute (UINT4 u4Dest, UINT4 u4Src, UINT1 u1Proto, UINT1 u1Tos,
            UINT2 *pu2RtPort, UINT4 *pu4RtGw)
{
    UNUSED_PARAM (u4Dest);
    UNUSED_PARAM (u4Src);
    UNUSED_PARAM (u1Proto);
    UNUSED_PARAM (u1Tos);
    UNUSED_PARAM (pu2RtPort);
    UNUSED_PARAM (pu4RtGw);

    return IP_FAILURE;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6GetRoute
 * Description      :   This function provides the Route (either Exact Route or
 *                      Best route) for a given destination and Mask based on
 *                      the incoming request.
 * Inputs           :   pNetIpv6RtQuery - Infomation about the route to be
 *                                 retrieved.
 * Outputs          :   pNetIpv6RtInfo - Information about the requested route.
 * Return Value     :   NETIPV6_SUCCESS - if the route is present.
 *                      NETIPV6_FAILURE - if the route is not present.
 ******************************************************************************
 */
INT4
NetIpv6GetRoute (tNetIpv6RtInfoQueryMsg * pNetIpv6RtQuery,
                 tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    UNUSED_PARAM (pNetIpv6RtQuery);
    UNUSED_PARAM (pNetIpv6RtInfo);

    return NETIPV6_FAILURE;
}
