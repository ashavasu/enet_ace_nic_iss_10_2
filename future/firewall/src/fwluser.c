/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwluser.c,v 1.16 2013/10/31 11:13:30 siva Exp $
 *
 * Description: When the Firewall module is compiled in Kernel
 * mode, a few functions are still handled on the user mode. This 
 * file contains the intialization routines of those functions which 
 * are handled in the user mode.
 *******************************************************************/
#ifndef _FWLUSER_C
#define _FWLUSER_C
#include "fwlinc.h"
#include "fsfwlwr.h"
#include "fssyslog.h"
#include "fwlglob.h"
#include "fwlnp.h"

tFwlAclInfo         gFwlAclInfo;

/* 
 * This variable is defined in fwlutil.c as well for only userspace 
 * compilation
 */
unFwlUtilCallBackEntry FWL_UTIL_CALL_BACK[FWL_CUST_MAX_CALL_BACK_EVENTS];

/***************************************************************************
* Function Name    :  FwlAclInit 
* Description      :  This function registers the Firewall Mib with the 
*                     SNMP Module.
* Input (s)        :  None
*
* Output (s)       :  None
* Returns          :  FWL_SUCCESS - After the registration with SNMP Module
***************************************************************************/
PUBLIC UINT4
FwlAclInit (VOID)
{
    RegisterFSFWL ();

    /*Register the Firewall module with SYSLOG  */
    gi4FwlSysLogId = SYS_LOG_REGISTER ((CONST UINT1 *) "FWL",
                                       SYSLOG_CRITICAL_LEVEL);

    if (OsixCreateSem ((CONST UINT1 *) "FWLP", 1, 0, &gFwlSemId) !=
        OSIX_SUCCESS)
    {
        return (FWL_FAILURE);
    }
    if (FirewallSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        FirewallSizingMemDeleteMemPools ();
        return FWL_FAILURE;
    }
#ifdef NPAPI_WANTED
    FwlNpEnableDosAttack ();
    FwlNpEnableIpHeaderValidation ();
    FwlNpGetHwFilterCapabilities (&gau1FwlNpHwFilterCap);
    if (FWL_SUCCESS != FwlSetHwFilterCapabilities (gau1FwlNpHwFilterCap))
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Hardware filter capabilities are not programmed in kernel\r\n");
    }
#endif /* NPAPI_WANTED */

    FWL_INIT_COMPLETE (OSIX_SUCCESS);

    return FWL_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : FwlUtilGetGlobalStatus                                 */
/*                                                                           */
/* Description      : This function returns the global firewall status       */
/*                                                                           */
/* Input Parameters  : None                                                  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : Global Firewall Status                                */
/*****************************************************************************/

PUBLIC UINT1
FwlUtilGetGlobalStatus ()
{
    INT4                i4FwlStatus = 0;

    nmhGetFwlGlobalMasterControlSwitch (&i4FwlStatus);

    return ((UINT1) i4FwlStatus);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : FwlUtilGetIcmpControlStatus                            */
/*                                                                           */
/* Description      : This function returns the global firewall icmp control */
/*                    status.                                                */
/*                                                                           */
/* Input Parameters  : None                                                  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : Global Firewall ICMP control Status.                  */
/*****************************************************************************/

PUBLIC UINT1
FwlUtilGetIcmpControlStatus ()
{
    INT4                i4IcmpStatus = 0;

    nmhGetFwlGlobalICMPControlSwitch (&i4IcmpStatus);

    return ((UINT1) i4IcmpStatus);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlChkIfExtInterface                               */
/*     DESCRIPTION      : This function checks if the given interface is an  */
/*                        external (global) interface.                       */
/*     INPUT            : u4IfaceNum - Interface Number                      */
/*     OUTPUT           : None                                               */
/*     RETURNS          : FWL_SUCCESS if it is an external interface         */
/*                        FWL_FAILURE if it is an internal interface         */
/*****************************************************************************/

PUBLIC UINT4
FwlChkIfExtInterface (UINT4 u4IfaceNum)
{
    INT4                i4IfType = 0;

    if (nmhGetFwlIfIfType (u4IfaceNum, &i4IfType) == SNMP_SUCCESS)
    {
        return FWL_SUCCESS;
    }
    else
    {
        return FWL_FAILURE;
    }

}

/****************************************************************************                        Function    :  nmhDepv2FwlTrapAttackMessage
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)                                              SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlTrapAttackMessage (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlTrapMemFailMessage
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)                                              SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlTrapMemFailMessage (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlTrapThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlTrapThreshold (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

                                                                                                                                                                                                                                                                                                   /* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2FwlStatClear
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.                                                             Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlStatClear (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlStatIfTable
 Input       :  The Indices
                FwlStatIfIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)                                         SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlStatIfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlUrlFilterTable
 Input       :  The Indices
                FwlUrlString
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlUrlFilterTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2FwlDefnDmzTable
 Input       :  The Indices
                FwlDmzIpIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.                                                             Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDefnDmzTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2FwlDefnIfTable
 Input       :  The Indices
                FwlIfIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)                                              SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDefnIfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlDefnAclTable
 Input       :  The Indices
                FwlAclIfIndex                                                                                       FwlAclAclName                                                                                       FwlAclDirection
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.                                                             Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDefnAclTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlDefnRuleTable
 Input       :  The Indices
                FwlRuleRuleName                                                                      Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDefnRuleTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlDefnFilterTable                                                           Input       :  The Indices
                FwlFilterFilterName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDefnFilterTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlDefnInterceptTimeout
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDefnInterceptTimeout (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlDefnTcpInterceptThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &                                                   check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDefnTcpInterceptThreshold (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalNetBiosLan2Wan
 Output      :  The Dependency Low Lev Routine Take the Indices &                                                   check whether dependency is met or not.
                Stores the value of error code in the Return val                                     Error Codes :  The following error codes are to be returned                                                        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)                        Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalNetBiosLan2Wan (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalNetBiosFiltering
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalNetBiosFiltering (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalUrlFiltering
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalUrlFiltering (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalDebug (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalTrace (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalTrap
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalTrap (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalTcpIntercept
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.                                                             Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/ INT1
nmhDepv2FwlGlobalTcpIntercept (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalTinyFragmentFiltering
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)                                               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/ INT1
nmhDepv2FwlGlobalTinyFragmentFiltering (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalSrcRouteFiltering
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalSrcRouteFiltering (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalIpSpoofFiltering
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val                                     Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalIpSpoofFiltering (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalICMPControlSwitch
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.                                                             Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalICMPControlSwitch (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalMasterControlSwitch
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalMasterControlSwitch (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlUpdateIpHeaderInfoFromPkt                     */
/*                                                                          */
/*    Description        : Updates the IpHeader with packet information     */
/*                                                                          */
/*    Input(s)           : pBuf     -- Pointer to the packet                */
/*                         pIpInfo  -- Pointer to the IpHeader              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlUpdateIpHeaderInfoFromPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tIpHeader * pIpInfo)
#else
PUBLIC VOID
FwlUpdateIpHeaderInfoFromPkt (pBuf, pIpInfo)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     tIpHeader          *pIpInfo;
#endif
{
    UINT4               u4Retval = 0;
    FWL_GET_1_BYTE (pBuf, FWL_IP_HLEN_OFFSET, pIpInfo->u1IpVersion);
    pIpInfo->u1IpVersion >>= 4;

    if (pIpInfo->u1IpVersion == FWL_IP_VERSION_4)
    {
        u4Retval = FwlUpdateIpv4HeaderInfoFromPkt (pBuf, pIpInfo);
        return u4Retval;
    }
    else if (pIpInfo->u1IpVersion == FWL_IP_VERSION_6)
    {
        u4Retval = FwlUpdateIpv6HeaderInfoFromPkt (pBuf, pIpInfo);
        return u4Retval;

    }
    else
        return (FWL_INVALID_IP_VERSION);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlUpdateIpv4HeaderInfoFromPkt                   */
/*                                                                          */
/*    Description        : Updates the IpHeader with IPv4 packet information*/
/*                                                                          */
/*    Input(s)           : pBuf     -- Pointer to the packet                */
/*                         pIpInfo  -- Pointer to the IpHeader              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

PUBLIC UINT4
FwlUpdateIpv4HeaderInfoFromPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                                tIpHeader * pIpInfo)
{
    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering the fn. FwlUpdateIpv4HeaderInfoFromPkt \n");

    /* Get the Header length value from the pkt */
    /* Changed all the GET_BYTES to FWL_GET_BYTES */
    FWL_GET_1_BYTE (pBuf, FWL_IP_HLEN_OFFSET, pIpInfo->u1HeadLen);
    pIpInfo->u1HeadLen = (UINT1)
        ((pIpInfo->u1HeadLen & FWL_IP_HEAD_LEN_MASK) * FWL_HEAD_LEN_FACTOR);
    FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n HeadLen = %d \n", pIpInfo->u1HeadLen);

    /* Get the Total Length from the pkt */
    FWL_GET_2_BYTE (pBuf, FWL_IP_TOTAL_LEN_OFFSET, pIpInfo->u2TotalLen);
    FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n Total Len = %x \n", pIpInfo->u2TotalLen);

    /* Get the Tos value from the pkt */
    FWL_GET_1_BYTE (pBuf, FWL_IP_TOS_OFFSET, pIpInfo->u1Tos);
    pIpInfo->u1Tos = (UINT1) ((pIpInfo->u1Tos & FWL_IP_TOS_MASK) >> 2);
    FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n Tos = %x \n", pIpInfo->u1Tos);

    /* Get the source address from the pkt */
    FWL_GET_4_BYTE (pBuf, FWL_IP_SRC_ADDR_OFFSET, pIpInfo->SrcAddr.v4Addr);
    FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n SrcAddres = %x \n",
              pIpInfo->SrcAddr.v4Addr);
    pIpInfo->SrcAddr.u4AddrType = FWL_IP_VERSION_4;

    /* Get the Destination address from the pkt */
    FWL_GET_4_BYTE (pBuf, FWL_IP_DEST_ADDR_OFFSET, pIpInfo->DestAddr.v4Addr);
    FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n DestAddres = %x \n",
              pIpInfo->DestAddr.v4Addr);
    pIpInfo->DestAddr.u4AddrType = FWL_IP_VERSION_4;

    /* Get the Fragment Offset from the pkt */
    /* Get the Fragment Offset from the pkt */
    FWL_GET_2_BYTE (pBuf, FWL_IP_FRAGMENT_OFFSET, pIpInfo->u2FragOffset);

    /* Get the Protocol from the pkt */
    FWL_GET_1_BYTE (pBuf, FWL_IP_PROTO_OFFSET, pIpInfo->u1Proto);
    FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n Protocol = %d \n", pIpInfo->u1Proto);

    /* Validate the IP-Headers' Header-Length & Total-Length values. */
    if ((pIpInfo->u1HeadLen < FWL_IP_HEADER_LEN) || (pIpInfo->u1HeadLen > (FWL_MAX_IP_OPTIONS_LEN + FWL_IP_HEADER_LEN)) || (pIpInfo->u2TotalLen < FWL_IP_HEADER_LEN) ||    /* ICSA V07 & V08 */
        (pIpInfo->u2TotalLen < pIpInfo->u1HeadLen) ||
        ((CRU_BUF_Get_ChainValidByteCount (pBuf)) < pIpInfo->u2TotalLen))
    {
        return (FWL_IPHDR_LEN_FIELDS_INVALID);
    }
    /* Validate the higher layer data length only for the first fragment. */
    if (!(pIpInfo->u2FragOffset & FWL_FRAG_OFFSET_BIT))
    {
        switch (pIpInfo->u1Proto)
        {
            case FWL_TCP:
                if ((pIpInfo->u2TotalLen - pIpInfo->u1HeadLen) <
                    FWL_MIN_TCP_HEADER_SIZE)
                {
                    return (FWL_TCP_SHTHDR);
                }
                break;
            case FWL_UDP:
                if ((pIpInfo->u2TotalLen - pIpInfo->u1HeadLen) <
                    FWL_MIN_UDP_HEADER_SIZE)
                {
                    return (FWL_UDP_SHTHDR);
                }
                break;

            case FWL_ICMP:
                if ((pIpInfo->u2TotalLen - pIpInfo->u1HeadLen) <
                    FWL_MIN_ICMP_HEADER_SIZE)
                {
                    return (FWL_ICMP_SHTHDR);
                }
                break;

            default:
                break;
        }                        /*End-of-Switch */
    }                            /* End-of-Code-4-Valid-Proto-Hdr-Check */

    /* Get the IP option code byte from the packet if options exist in pkt */
    if (pIpInfo->u1HeadLen != FWL_IP_HEADER_LEN)
    {
        return FwlParseIpOptions (pBuf, pIpInfo);
    }

    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting the fn. FwlUpdateIpv4HeaderInfoFromPkt \n");

    return FWL_SUCCESS;
}                                /* End of the Function -- FwlUpdateIpv4HeaderInfoFromPkt   */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlUpdateIpv6HeaderInfoFromPkt                   */
/*                                                                          */
/*    Description        : Updates the IpHeader with IPv6 packet information*/
/*                                                                          */
/*    Input(s)           : pBuf     -- Pointer to the packet                */
/*                         pIpInfo  -- Pointer to the IpHeader              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

PUBLIC UINT4
FwlUpdateIpv6HeaderInfoFromPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                                tIpHeader * pIpInfo)
{
    UINT4               u4Ip6Head = 0;
    UINT1               u1OffSet = 0;
    UINT2               u2Length = 0;    /* payload length present in the ipv6 pkt */
    UINT1               u1ExtHdrLen = 0;    /* Length of all the extension header present */
    UINT1               u1NextHeader = 0;
    UINT1               u1NoMoreExtHdr = OSIX_FALSE;
    UINT1               u1OptLen = 0;
    UINT1               u1HopLimit = 0;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering the fn. FwlUpdateIpv6HeaderInfoFromPkt \n");

    FWL_GET_4_BYTE (pBuf, FWL_IP_HLEN_OFFSET, u4Ip6Head);

    pIpInfo->u1Tos = (UINT1) ((u4Ip6Head & FWL_IPv6_DSCP_MASK) >> 20);
    pIpInfo->u4FlowLabel = (u4Ip6Head & FWL_IPv6_FLOWID_MASK);

    /* Get the Source address from the pkt */
    FWL_GET_N_BYTE (pBuf, FWL_IP6_SRC_ADDR_OFFSET, pIpInfo->SrcAddr.v6Addr, 16);
    FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n SrcAddres = %x \n",
              Ip6PrintAddr (&(pIpInfo->SrcAddr.v6Addr)));
    pIpInfo->SrcAddr.u4AddrType = FWL_IP_VERSION_6;

    /* Get the Destination address from the pkt */
    FWL_GET_N_BYTE (pBuf, FWL_IP6_DST_ADDR_OFFSET, pIpInfo->DestAddr.v6Addr,
                    16);
    FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n DstAddres = %x \n",
              Ip6PrintAddr (&(pIpInfo->DestAddr.v6Addr)));
    pIpInfo->DestAddr.u4AddrType = FWL_IP_VERSION_6;

    /* Length of the payload including Extension headers */
    FWL_GET_2_BYTE (pBuf, FWL_IP6_PAYLOAD_LEN_OFFSET, u2Length);

    pIpInfo->u2TotalLen = FWL_IP6_HEADER_LEN + u2Length;

    FWL_GET_1_BYTE (pBuf, FWL_IP6_HOP_LIMIT_OFFSET, u1HopLimit);
    if (u1HopLimit == 0)
    {
        return (FWL_INVALID_HOPLIMIT);
    }

    /* Get the value of the Next header value in the IPv6 header */
    FWL_GET_1_BYTE (pBuf, FWL_IP6_NH_OFFSET, u1NextHeader);

    /* Set the offset to the start of the extension header  40 */
    u1OffSet = FWL_IP6_FIRST_NH_OFFSET;

    do
    {
        switch (u1NextHeader)
        {
            case FWL_HOP_BY_HOP_HDR:
            case FWL_DEST_OPT_HDR:
            case FWL_ROUTING_HDR:

                /* Get the length of the extension header present in the packet */
                FWL_GET_1_BYTE (pBuf, u1OffSet + 1, u1OptLen);

                /* First 8 byte will not be included in the length field and the
                 * value present in the length field is a 8-octet value*/
                u1ExtHdrLen = u1ExtHdrLen + 8 + (u1OptLen * 8);
                break;

            case FWL_NO_NXT_HDR:

                if (u1ExtHdrLen != u2Length)
                {
                    return FWL_IPHDR_LEN_FIELDS_INVALID;
                }
                u1NoMoreExtHdr = OSIX_TRUE;
                break;

            case FWL_FRAGMENT_HDR:

                /* FWL_FRAGMENT_HEADER_LEN == 8 */
                u1ExtHdrLen = u1ExtHdrLen + FWL_FRAGMENT_HEADER_LEN;

                FWL_GET_2_BYTE (pBuf, u1OffSet + 2, pIpInfo->u2FragOffset);

                /* Only for the non-first fragment packets */
                /* FWL_IP6_FRAG_OFFSET_BIT = 0xfff8 */
                if (pIpInfo->u2FragOffset & FWL_IP6_FRAG_OFFSET_BIT)
                {

                    FWL_GET_1_BYTE (pBuf, u1OffSet, u1NextHeader);
                    u1NoMoreExtHdr = OSIX_TRUE;
                }
                break;

            case FWL_AUTHENTICATION_HDR:
            case FWL_ENCAP_SEC_PAY_HDR:
            case FWL_TCP:
            case FWL_UDP:
            case FWL_ICMPV6:
            case FWL_OSPFIGP:
                u1NoMoreExtHdr = OSIX_TRUE;
                break;
            default:
                return (FWL_INVALID_NH);
                break;
        }

        if (u1NoMoreExtHdr == OSIX_TRUE)
        {
            if (u1ExtHdrLen > u2Length)
            {
                return FWL_IPHDR_LEN_FIELDS_INVALID;
            }

            /* For non-fragment and the first fragmented packets */
            if (!(pIpInfo->u2FragOffset & FWL_IP6_FRAG_OFFSET_BIT))
            {
                switch (u1NextHeader)
                {
                    case FWL_TCP:
                        if ((u2Length - u1ExtHdrLen) < FWL_MIN_TCP_HEADER_SIZE)
                        {
                            return (FWL_TCP_SHTHDR);
                        }
                        break;

                    case FWL_UDP:
                        if ((u2Length - u1ExtHdrLen) < FWL_MIN_UDP_HEADER_SIZE)
                        {
                            return (FWL_UDP_SHTHDR);
                        }
                        break;

                    case FWL_ICMPV6:
                        if ((u2Length - u1ExtHdrLen) < FWL_MIN_ICMP_HEADER_SIZE)
                        {
                            return (FWL_ICMP_SHTHDR);

                        }
                        break;

                    default:
                        break;
                }
            }

            /* FWL_IP6_HEADER_LEN == 40 */

            pIpInfo->u1HeadLen = FWL_IP6_HEADER_LEN + u1ExtHdrLen;
            pIpInfo->u1Proto = u1NextHeader;
            break;
        }

        FWL_GET_1_BYTE (pBuf, u1OffSet, u1NextHeader);
        u1OffSet = FWL_IP6_HEADER_LEN + u1ExtHdrLen;

    }
    while (u1ExtHdrLen < u2Length);

    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting the fn. FwlUpdateIpv6HeaderInfoFromPkt \n");

    return FWL_SUCCESS;
}                                /* End of the Function -- FwlUpdateIpv6HeaderInfoFromPkt   */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlParseIpOptions                                */
/*                                                                          */
/*    Description        : Parses the packets for IP options                */
/*                                                                          */
/*    Input(s)           : pbuf - Packet                                    */
/*                         pIpHdr - IP header info                          */
/*                                                                          */
/*    Output(s)          : pIpHdr - IP header info                          */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

PUBLIC UINT4
FwlParseIpOptions (tCRU_BUF_CHAIN_HEADER * pBuf, tIpHeader * pIpHdr)
{
    UINT1               u1Index = 0;
    UINT1               u1Opt_len = 0;    /* Current Ip Option Length */
    UINT1               u1OptCode;    /* IP Option TYPE or CODE */
    UINT2               u2IpOptionsLen;    /* Length of IpOptions in the IP packet */
    UINT1               u4IpOptOffset = FWL_IP_HEADER_LEN;
    UINT4              *pu4Options = &(pIpHdr->u4Options);

   /**  BEGIN of Code for IP Option Processing  **/
    /* Initialisation of variables */
    *pu4Options = 0;

    /* Begin IP Options Parsing */
    if (pIpHdr->u1HeadLen > FWL_IP_HEADER_LEN)
    {
        u2IpOptionsLen = (UINT2) (pIpHdr->u1HeadLen - FWL_IP_HEADER_LEN);
        if (u2IpOptionsLen > FWL_MAX_IP_OPTIONS_LEN)
        {
            return FWL_INVALID_IP_OPTIONS;
        }
        while ((u1Index < u2IpOptionsLen) &&
               ((FWL_GET_1_BYTE (pBuf, u4IpOptOffset, u1OptCode)) ==
                FWL_FAILURE))
        {
            if (u1OptCode == FWL_IP_OPT_EOL)
            {
                /* reached the end of the option field */
                break;
            }
            switch (u1OptCode)
            {
                case FWL_IP_OPT_NOP:
                    u1Opt_len = 1;
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_NOP_BIT;
                    break;
                case FWL_IP_OPT_SECURITY:
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_SECURITY_BIT;
                    break;
                case FWL_IP_OPT_LSROUTE:
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_LSROUTE_BIT;
                    break;
                case FWL_IP_OPT_TSTAMP:
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_TSTAMP_BIT;
                    break;
                case FWL_IP_OPT_ESECURITY:
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_ESECURITY_BIT;
                    break;
                case FWL_IP_OPT_RROUTE:
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_RROUTE_BIT;
                    break;
                case FWL_IP_OPT_STREAM_IDENT:
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_STREAM_IDENT_BIT;
                    break;
                case FWL_IP_OPT_SSROUTE:
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_SSROUTE_BIT;
                    break;
                case FWL_IP_OPT_ROUTERALERT:
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_ROUTERALERT_BIT;
                    break;

                case FWL_IP_OPT_TROUTE:
                    *pu4Options = (*pu4Options) | FWL_IP_OPT_TROUTE_BIT;
                    break;

                default:
                    /* Invalid IP option. */
                    FWL_DBG (FWL_DBG_EXIT, "\n Invalid IP Option Type\n");
                    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlParseIpOptions()\n");
                    return FWL_INVALID_IP_OPTIONS;
            }                    /*End-of-Switch */

            /* Get the Option_Length */
            if (u1OptCode != FWL_IP_OPT_NOP)
                FWL_GET_1_BYTE (pBuf, u4IpOptOffset + 1, u1Opt_len);
            /* If option length field is 0 or if it is > option 
             * length in IP packet then the packet is Invalid.   */
            if ((u1Opt_len > u2IpOptionsLen) ||
                ((u1Opt_len + u1Index) > u2IpOptionsLen))
            {
                FWL_DBG (FWL_DBG_EXIT, "\n Invalid IP Option length\n");
                FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlParseIpOptions()\n");
                return FWL_INVALID_IP_OPTIONS;
            }
            else if (0 == u1Opt_len)
            {
                FWL_GET_1_BYTE (pBuf, u4IpOptOffset + 1, u1Opt_len);
                /* If option length field is 0 or if it is > option 
                 * length in IP packet then the packet is Invalid.   */
                if ((u1Opt_len > u2IpOptionsLen) ||
                    ((u1Opt_len + u1Index) > u2IpOptionsLen))
                {
                    FWL_DBG (FWL_DBG_EXIT, "\n Invalid IP Option length\n");
                    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlParseIpOptions()\n");
                    return FWL_INVALID_IP_OPTIONS;
                }
                else if (0 == u1Opt_len)
                {
                    FWL_DBG (FWL_DBG_EXIT, "\n Zero length IP Option \n");
                    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlParseIpOptions()\n");
                    return FWL_ZERO_LEN_IP_OPTION;
                }
            }                    /*End-ofIF-Block */

            u1Index += u1Opt_len;
            u4IpOptOffset += u1Opt_len;
        }                        /*End-of-While-Loop */
    }                            /*End-of-IF-Block */

    return FWL_SUCCESS;            /* All IP-Options Valid */
   /**  END of Code for IP Option Processing  **/
}                                /* End of the function -- FwlParseIpOptions  */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlUpdateIcmpv4v6InfoFromPkt                     */
/*                                                                          */
/*    Description        : Updates the IcmpInfo srtucture with pkt info     */
/*                                                                          */
/*    Input(s)           : pBuf     -- Pointer to the packet                */
/*                         pIcmp    -- Pointer to the IcmpInfo              */
/*                         u1IpHeadLen - Value of the IP header length      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlUpdateIcmpv4v6InfoFromPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                              tIcmpInfo * pIcmp, UINT1 u1IpHeadLen)
#else
PUBLIC VOID
FwlUpdateIcmpv4v6InfoFromPkt (pBuf, pIcmp u1IpHeadLen)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     tIcmpInfo          *pIcmp;
     UINT1               u1IpHeadLen;
#endif
{
    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering the fn. FwlUpdateIcmpv4v6InfoFromPkt \n");

    /* Get the Icmp Type from the pkt */
    FWL_GET_1_BYTE (pBuf, u1IpHeadLen, pIcmp->u1Type);
    FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n Icmp type = %d \n", pIcmp->u1Type);

    /* Get the Icmp code from the pkt */
    FWL_GET_1_BYTE (pBuf, (u1IpHeadLen + FWL_ICMP_CODE_OFFSET), pIcmp->u1Code);
    FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n Icmp code = %d \n", pIcmp->u1Code);

    /* Get the Icmp Identifier from the pkt */
    FWL_GET_2_BYTE (pBuf, (u1IpHeadLen + FWL_ICMP_ID_OFFSET),
                    pIcmp->u2IcmpSeqNum);
    FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n Icmp Id = %d \n", pIcmp->u2IcmpSeqNum);

    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting the fn. FwlUpdateIcmpv4v6InfoFromPkt \n");

}                                /* End of the Function -- FwlUpdateIcmpv4v6InfoFromPkt  */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlUpdateHLInfoFromPkt                           */
/*                                                                          */
/*    Description        : Updates the Higher layer TCP/UDP information     */
/*                         like Src and Dest port and If the packet is for  */
/*                         TCP, updates the Ack and Rst bits.               */
/*                                                                          */
/*    Input(s)           : pBuf     -- Pointer to the packet                */
/*                         pHLInfo  -- Pointer to the HLInfo                */
/*                         u1Proto  -- Protocol ( UDP or TCP )              */
/*                         u1IpHeadLen - IP header length                   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlUpdateHLInfoFromPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                        tHLInfo * pHLInfo, UINT1 u1Proto, UINT1 u1IpHeadLen)
#else
PUBLIC VOID
FwlUpdateHLInfoFromPkt (pBuf, pHLInfo, u1Proto, u1IpHeadLen)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     tHLInfo            *pHLInfo;
     UINT1               u1Proto;
     UINT1               u1IpHeadLen;
#endif
{
    UINT1               u1TcpCodeBit;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlUpdateHLInfoFromPkt \n");

    /* Get the Source port  */
    FWL_GET_2_BYTE (pBuf, u1IpHeadLen, pHLInfo->u2SrcPort);

    /* Get the Destination port  */
    FWL_GET_2_BYTE (pBuf, (u1IpHeadLen + 2), pHLInfo->u2DestPort);

    if (u1Proto == FWL_TCP)
    {
        /* Get the TCP Ack, Rst Bit and Fin Bit */
        FWL_GET_1_BYTE (pBuf, (u1IpHeadLen + FWL_TCP_CODE_BIT_OFFSET),
                        u1TcpCodeBit);

        /* get the Rst bit from the code bit and right shift by two times */
        pHLInfo->u1Rst = (UINT1) ((u1TcpCodeBit & FWL_TCP_RST_MASK) >> 2);

        /* get the Ack bit from the code bit and right shift by Four times */
        pHLInfo->u1Ack = (UINT1) ((u1TcpCodeBit & FWL_TCP_ACK_MASK) >> 4);

        /* get the Fin bit from the code bit */
        pHLInfo->u1Fin = (u1TcpCodeBit & FWL_TCP_FIN_MASK);

        /* Get the Syn bit from the code bit */
        pHLInfo->u1Syn = (UINT1) ((u1TcpCodeBit & FWL_TCP_SYN_MASK) >> 1);

        /* Get the Sequence Number from tcp header */
        FWL_GET_4_BYTE (pBuf, u1IpHeadLen + FWL_TCP_SEQ_OFFSET,
                        pHLInfo->u4SeqNum);

        /* Get the Acknowledgement Number from tcp header */
        FWL_GET_4_BYTE (pBuf, u1IpHeadLen + FWL_TCP_ACK_OFFSET,
                        pHLInfo->u4AckNum);

        /* Get the Advertised Window value from tcp header */
        FWL_GET_2_BYTE (pBuf, u1IpHeadLen + FWL_TCP_WINDOW_OFFSET,
                        pHLInfo->u2WindowAdvertised);

        FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n SrcPort = %d \n",
                  pHLInfo->u2SrcPort);
        FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n DestPort = %d \n",
                  pHLInfo->u2DestPort);
        FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n Rst Bit = %d \n", pHLInfo->u1Rst);
        FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n Ack Bit = %d \n", pHLInfo->u1Ack);
        FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n Fin Bit = %d \n", pHLInfo->u1Fin);
        FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n Syn Bit = %d \n", pHLInfo->u1Syn);

    }
    else
    {                            /* u1Proto is UDP */

        /* the following fields are not used, if the packet is UDP and 
         * initialised to the Zero.
         */
        pHLInfo->u1Rst = 0;
        pHLInfo->u1Ack = 0;
        pHLInfo->u1Fin = 0;
        pHLInfo->u1Syn = 0;

        FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n SrcPort = %d \n",
                  pHLInfo->u2SrcPort);
        FWL_DBG1 (FWL_DBG_PKT_EXTRACT, "\n DestPort = %d \n",
                  pHLInfo->u2DestPort);
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlUpdateHLInfoFromPkt \n");

}                                /* End of the Function -- FwlUpdateHLInfoFromPkt  */

/****************************************************************************/
/*                                                                          */
/* Function     : FwlLock                                                   */
/*                                                                          */
/* Description  : Takes the Firewall Lock                                   */
/*                                                                          */
/* Input        : *pFwlLock - Pointer to the spin_lock_t in case of kernel, */
/*                            UINT4 in case of User Space                   */
/*                                                                          */
/*                pu4Flags  - Pointer to the Flags for the lock             */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/
INT4
FwlLock (VOID)
{
    if (OsixSemTake (gFwlSemId) != OSIX_SUCCESS)
    {
        return (FWL_FAILURE);
    }
    return (FWL_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/* Function     : FwlUnLock                                                 */
/*                                                                          */
/* Description  : Releases the Firewall Lock                                */
/*                                                                          */
/* Input        : *pFwlLock - Pointer to the spin_lock_t in case of kernel, */
/*                            UINT4 in case of User Space                   */
/*                                                                          */
/*                pu4Flags  - Pointer to the Flags for the lock             */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/

INT4
FwlUnLock (VOID)
{
    if (OsixSemGive (gFwlSemId) != OSIX_SUCCESS)
    {
        return (FWL_FAILURE);
    }
    return (FWL_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlGetTransportHeaderLength                      */
/*                                                                          */
/*    Description        : Gets the header length of the Transport layer    */
/*                         (TCP/UDP).                                       */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the packet            */
/*                         u1PktType    -- Whether it is TCP/UDP            */
/*                         u1Headlen    -- Ip Header Length                 */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : u1Length     -- Length of the Transport layer    */
/*                                         header.                          */
/****************************************************************************/
PUBLIC UINT4
FwlGetTransportHeaderLength (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1PktType,
                             UINT1 u1Headlen)
{
    UINT4               u4HeaderLenOffset;
    UINT1               u1Length;

    u1Length = 0;
    u4HeaderLenOffset = 0;

    if (FWL_TCP == u1PktType)
    {
        u4HeaderLenOffset = FWL_TCP_LENGTH_OFFSET + u1Headlen;
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1Length,
                                   u4HeaderLenOffset, FWL_HEADER_LEN);
        u1Length = (UINT1) ((u1Length >> 4) & FWL_HEADER_LEN_MASK);
        u1Length = (UINT1) (u1Length * 4);
        return u1Length;
    }
    else if (FWL_UDP == u1PktType)
    {
        UINT2               u2UdpLen = 0;
        /* Get the length in the UDP header */
        FWL_GET_2_BYTE (pBuf, (FWL_UDP_LENGTH_OFFSET + u1Headlen), u2UdpLen);
        return (u2UdpLen);
    }

    return u1Length;

}                                /*End of Function */

/*****************************************************************************/
/* Function Name    : FwlUtilReleaseWebLog                                   */
/*                                                                           */
/* Description      : Function to release the memory block allocated for the */
/*                    web log                                                */
/*                                                                           */
/* Input Parameters  : None                                                  */
/*                                                                           */
/* Return Value      : FWL_SUCCESS or FWL_FAILURE                            */
/*****************************************************************************/

INT4
FwlUtilReleaseWebLog (UINT1 *pu1LogPtr)
{
    if (MemReleaseMemBlock (FWL_WEB_LOG_PID, pu1LogPtr) == MEM_SUCCESS)
    {
        return FWL_SUCCESS;
    }
    return FWL_FAILURE;
}

/* 
 * These functions FwlUtilRegisterCallBack and FwlUtilCallBack are replicated 
 * from fwlutil.c for kernel space compilation.
 */

/*****************************************************************************/
/* Function Name      : FwlUtilRegisterCallBack                              */
/*                                                                           */
/* Description        : This function registers the call backs from          */
/*                      application                                          */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gaFwlCustCallBack                                    */
/*                                                                           */
/* Return Value(s)    : FWL_SUCCESS/FWL_FAILURE                              */
/*****************************************************************************/
INT4
FwlUtilRegisterCallBack (UINT4 u4Event, tFsCbInfo * pFsCbInfo)
{
    INT4                i4RetVal = FWL_SUCCESS;

    switch (u4Event)
    {
        case FWL_CUST_IF_CHECK_EVENT:
            FWL_UTIL_CALL_BACK[u4Event].pFwlCustIfCheck =
                pFsCbInfo->pFwlCustIfCheck;
            break;

        default:
            i4RetVal = FWL_FAILURE;
            break;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : FwlUtilCallBack                                      */
/*                                                                           */
/* Description        : This function processes the callback events invoked  */
/*                      in the program.                                      */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gaFwlUtilCallBack                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FWL_SUCCESS/FWL_FAILURE                              */
/*****************************************************************************/
INT4
FwlUtilCallBack (UINT4 u4Event)
{
    INT4                i4RetVal = FWL_SUCCESS;

    /* This will be executed only when the call back is registered priorly */
    switch (u4Event)
    {
        case FWL_CUST_IF_CHECK_EVENT:
            if (NULL != (FWL_UTIL_CALL_BACK[u4Event]).pFwlCustIfCheck)
            {
                if (ISS_FAILURE ==
                    FWL_UTIL_CALL_BACK[u4Event].pFwlCustIfCheck ())
                {
                    i4RetVal = FWL_FAILURE;
                }
            }
            break;
        default:
            i4RetVal = FWL_FAILURE;
            break;
    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlUtilGetSysLogId                                 */
/*                                                                           */
/*     DESCRIPTION      : This function fetches the ID registered with SysLog*/
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Syslog ID registered with syslog module            */
/*                                                                           */
/*****************************************************************************/

INT4
FwlUtilGetSysLogId ()
{
    return gi4FwlSysLogId;
}

/*****************************************************************************/
/*     FUNCTION NAME    : FwlUtilRestoreIdsStatus                            */
/*     DESCRIPTION      : This function restores the previously saved IDS    */
/*                        status. This function is invoked when snort is     */
/*                        successfully restarted after loading rules         */
/*     INPUT            : NONE                                               */
/*     OUTPUT           : NONE                                               */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/
VOID
FwlUtilRestoreIdsStatus ()
{
    /* This is needed to intimate IDS status to kernel */
    nmhSetFwlGlobalIdsStatus (gu4PrevIdsStatus);
    return;
}

/*****************************************************************************/
/*  Function Name   : FwlUtilUpdateIdsRulesStatus                            */
/*  Description     : This function set the IDS global rules status          */
/*  Input(s)        : u4Status - Status of IDS                               */
/*  Output(s)       : None                                                   */
/*  Return (s)      :  OSIX_SUCCESS                                          */
/*****************************************************************************/
INT4
FwlUtilUpdateIdsRulesStatus (UINT4 u4Status)
{
    gi4IdsRulesStatus = u4Status;
    return OSIX_SUCCESS;
}
#endif /* _FWLUSER_C */
