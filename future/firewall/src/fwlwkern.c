/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlwkern.c,v 1.20 2015/04/11 12:51:02 siva Exp $
 *
 * *******************************************************************/

#ifndef _FWLWKERN_C_
#define _FWLWKERN_C_
#include "fwlinc.h"
#include "fwlglob.h"
#include "fwlwdefs.h"
#include "fwlwtdfs.h"
#include "arsec.h"
#include "seckgen.h"
#define IOCTL_SUCCESS 0
#define IOCTL_FAILURE 1

tFwlLogBuffer      *gpFwlLogTemp = NULL;

PRIVATE INT4
       KernFwlCliGetStatefulTableEntries (UINT4, tStatefulSessionNode **);

/* -------------------------------------------------------------
 *
 * Function: FwlNmhIoctl
 *
 * -------------------------------------------------------------
 */
int
FwlNmhIoctl (unsigned long p)
{
    int                 rc = 0;
    int                 nmhiocnr;
    tOsixKernUserInfo   OsixKerUseInfo;
    union unFwlNmh      lv;

    gi4CliWebSetError = 0;

    OsixKerUseInfo.pDest = &nmhiocnr;
    OsixKerUseInfo.pSrc = (int *) p;

    if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo, 4, 0) ==
        OSIX_FAILURE)
    {
        return -EFAULT;
    }

    MEMSET (&lv, 0, sizeof (union unFwlNmh));

    switch (nmhiocnr)
    {

        case NMH_GET_FWL_GLOBAL_MASTER_CONTROL_SWITCH:
        {

            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalMasterControlSwitch;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalMasterControlSwitch *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalMasterControlSwitch),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlGlobalMasterControlSwitch.rval =
                nmhGetFwlGlobalMasterControlSwitch (lv.
                                                    nmhGetFwlGlobalMasterControlSwitch.
                                                    pi4RetValFwlGlobalMasterControlSwitch);
            lv.nmhGetFwlGlobalMasterControlSwitch.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlGlobalMasterControlSwitch *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalMasterControlSwitch;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalMasterControlSwitch))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_I_C_M_P_CONTROL_SWITCH:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalICMPControlSwitch;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalICMPControlSwitch *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalICMPControlSwitch),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlGlobalICMPControlSwitch.rval =
                nmhGetFwlGlobalICMPControlSwitch (lv.
                                                  nmhGetFwlGlobalICMPControlSwitch.
                                                  pi4RetValFwlGlobalICMPControlSwitch);
            lv.nmhGetFwlGlobalICMPControlSwitch.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlGlobalICMPControlSwitch *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalICMPControlSwitch;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalICMPControlSwitch)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_IP_SPOOF_FILTERING:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalIpSpoofFiltering;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalIpSpoofFiltering *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalIpSpoofFiltering),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlGlobalIpSpoofFiltering.rval =
                nmhGetFwlGlobalIpSpoofFiltering (lv.
                                                 nmhGetFwlGlobalIpSpoofFiltering.
                                                 pi4RetValFwlGlobalIpSpoofFiltering);
            lv.nmhGetFwlGlobalIpSpoofFiltering.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlGlobalIpSpoofFiltering *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalIpSpoofFiltering;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalIpSpoofFiltering)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_SRC_ROUTE_FILTERING:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalSrcRouteFiltering;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalSrcRouteFiltering *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalSrcRouteFiltering),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlGlobalSrcRouteFiltering.rval =
                nmhGetFwlGlobalSrcRouteFiltering (lv.
                                                  nmhGetFwlGlobalSrcRouteFiltering.
                                                  pi4RetValFwlGlobalSrcRouteFiltering);
            lv.nmhGetFwlGlobalSrcRouteFiltering.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlGlobalSrcRouteFiltering *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalSrcRouteFiltering;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalSrcRouteFiltering)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_TINY_FRAGMENT_FILTERING:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalTinyFragmentFiltering;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlGlobalTinyFragmentFiltering *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalTinyFragmentFiltering),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlGlobalTinyFragmentFiltering.rval =
                nmhGetFwlGlobalTinyFragmentFiltering (lv.
                                                      nmhGetFwlGlobalTinyFragmentFiltering.
                                                      pi4RetValFwlGlobalTinyFragmentFiltering);
            lv.nmhGetFwlGlobalTinyFragmentFiltering.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlGlobalTinyFragmentFiltering *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalTinyFragmentFiltering;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalTinyFragmentFiltering))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_TCP_INTERCEPT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalTcpIntercept;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalTcpIntercept *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalTcpIntercept),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlGlobalTcpIntercept.rval =
                nmhGetFwlGlobalTcpIntercept (lv.nmhGetFwlGlobalTcpIntercept.
                                             pi4RetValFwlGlobalTcpIntercept);
            lv.nmhGetFwlGlobalTcpIntercept.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlGlobalTcpIntercept *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalTcpIntercept;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalTcpIntercept)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_TRAP:
        {

            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalTrap;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalTrap *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalTrap),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlGlobalTrap.rval =
                nmhGetFwlGlobalTrap (lv.nmhGetFwlGlobalTrap.
                                     pi4RetValFwlGlobalTrap);
            lv.nmhGetFwlGlobalTrap.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlGlobalTrap *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalTrap;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalTrap)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_TRACE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalTrace;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalTrace *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalTrace),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlGlobalTrace.rval =
                nmhGetFwlGlobalTrace (lv.nmhGetFwlGlobalTrace.
                                      pi4RetValFwlGlobalTrace);
            lv.nmhGetFwlGlobalTrace.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlGlobalTrace *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalTrace;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalTrace)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_DEBUG:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalDebug;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalDebug *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalDebug),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            /* Invoke NPAPI */
            lv.nmhGetFwlGlobalDebug.rval =
                nmhGetFwlGlobalDebug (lv.nmhGetFwlGlobalDebug.
                                      pi4RetValFwlGlobalDebug);
            lv.nmhGetFwlGlobalDebug.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlGlobalDebug *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalDebug;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalDebug)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_MAX_FILTERS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalMaxFilters;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalMaxFilters *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalMaxFilters),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlGlobalMaxFilters.rval =
                nmhGetFwlGlobalMaxFilters (lv.nmhGetFwlGlobalMaxFilters.
                                           pi4RetValFwlGlobalMaxFilters);
            lv.nmhGetFwlGlobalMaxFilters.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlGlobalMaxFilters *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalMaxFilters;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalMaxFilters)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_MAX_RULES:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalMaxRules;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalMaxRules *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalMaxRules),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlGlobalMaxRules.rval =
                nmhGetFwlGlobalMaxRules (lv.nmhGetFwlGlobalMaxRules.
                                         pi4RetValFwlGlobalMaxRules);
            lv.nmhGetFwlGlobalMaxRules.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlGlobalMaxRules *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalMaxRules;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalMaxRules)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_URL_FILTERING:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalUrlFiltering;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalUrlFiltering *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalUrlFiltering),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlGlobalUrlFiltering.rval =
                nmhGetFwlGlobalUrlFiltering (lv.nmhGetFwlGlobalUrlFiltering.
                                             pi4RetValFwlGlobalUrlFiltering);
            lv.nmhGetFwlGlobalUrlFiltering.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlGlobalUrlFiltering *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalUrlFiltering;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalUrlFiltering)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_GLOBAL_MASTER_CONTROL_SWITCH:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlGlobalMasterControlSwitch;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlGlobalMasterControlSwitch *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalMasterControlSwitch),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlGlobalMasterControlSwitch.rval =
                nmhSetFwlGlobalMasterControlSwitch (lv.
                                                    nmhSetFwlGlobalMasterControlSwitch.
                                                    i4SetValFwlGlobalMasterControlSwitch);
            lv.nmhSetFwlGlobalMasterControlSwitch.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhSetFwlGlobalMasterControlSwitch *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlGlobalMasterControlSwitch;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalMasterControlSwitch))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_GLOBAL_I_C_M_P_CONTROL_SWITCH:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlGlobalICMPControlSwitch;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlGlobalICMPControlSwitch *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalICMPControlSwitch),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlGlobalICMPControlSwitch.rval =
                nmhSetFwlGlobalICMPControlSwitch (lv.
                                                  nmhSetFwlGlobalICMPControlSwitch.
                                                  i4SetValFwlGlobalICMPControlSwitch);
            lv.nmhSetFwlGlobalICMPControlSwitch.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlGlobalICMPControlSwitch *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlGlobalICMPControlSwitch;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalICMPControlSwitch)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_GLOBAL_IP_SPOOF_FILTERING:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlGlobalIpSpoofFiltering;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlGlobalIpSpoofFiltering *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalIpSpoofFiltering),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlGlobalIpSpoofFiltering.rval =
                nmhSetFwlGlobalIpSpoofFiltering (lv.
                                                 nmhSetFwlGlobalIpSpoofFiltering.
                                                 i4SetValFwlGlobalIpSpoofFiltering);
            lv.nmhSetFwlGlobalIpSpoofFiltering.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlGlobalIpSpoofFiltering *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlGlobalIpSpoofFiltering;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalIpSpoofFiltering)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_GLOBAL_SRC_ROUTE_FILTERING:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlGlobalSrcRouteFiltering;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlGlobalSrcRouteFiltering *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalSrcRouteFiltering),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlGlobalSrcRouteFiltering.rval =
                nmhSetFwlGlobalSrcRouteFiltering (lv.
                                                  nmhSetFwlGlobalSrcRouteFiltering.
                                                  i4SetValFwlGlobalSrcRouteFiltering);
            lv.nmhSetFwlGlobalSrcRouteFiltering.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlGlobalSrcRouteFiltering *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlGlobalSrcRouteFiltering;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalSrcRouteFiltering)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_GLOBAL_TINY_FRAGMENT_FILTERING:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlGlobalTinyFragmentFiltering;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhSetFwlGlobalTinyFragmentFiltering *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalTinyFragmentFiltering),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlGlobalTinyFragmentFiltering.rval =
                nmhSetFwlGlobalTinyFragmentFiltering (lv.
                                                      nmhSetFwlGlobalTinyFragmentFiltering.
                                                      i4SetValFwlGlobalTinyFragmentFiltering);
            lv.nmhSetFwlGlobalTinyFragmentFiltering.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhSetFwlGlobalTinyFragmentFiltering *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlGlobalTinyFragmentFiltering;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalTinyFragmentFiltering))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_GLOBAL_TCP_INTERCEPT:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlGlobalTcpIntercept;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlGlobalTcpIntercept *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalTcpIntercept),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlGlobalTcpIntercept.rval =
                nmhSetFwlGlobalTcpIntercept (lv.nmhSetFwlGlobalTcpIntercept.
                                             i4SetValFwlGlobalTcpIntercept);
            lv.nmhSetFwlGlobalTcpIntercept.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlGlobalTcpIntercept *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlGlobalTcpIntercept;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalTcpIntercept)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_GLOBAL_TRAP:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlGlobalTrap;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlGlobalTrap *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalTrap),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlGlobalTrap.rval =
                nmhSetFwlGlobalTrap (lv.nmhSetFwlGlobalTrap.
                                     i4SetValFwlGlobalTrap);
            lv.nmhSetFwlGlobalTrap.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlGlobalTrap *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlGlobalTrap;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalTrap)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_GLOBAL_TRACE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlGlobalTrace;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlGlobalTrace *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalTrace),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlGlobalTrace.rval =
                nmhSetFwlGlobalTrace (lv.nmhSetFwlGlobalTrace.
                                      i4SetValFwlGlobalTrace);
            lv.nmhSetFwlGlobalTrace.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlGlobalTrace *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlGlobalTrace;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalTrace)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_GLOBAL_DEBUG:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlGlobalDebug;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlGlobalDebug *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalDebug),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlGlobalDebug.rval =
                nmhSetFwlGlobalDebug (lv.nmhSetFwlGlobalDebug.
                                      i4SetValFwlGlobalDebug);
            lv.nmhSetFwlGlobalDebug.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlGlobalDebug *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlGlobalDebug;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalDebug)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_GLOBAL_URL_FILTERING:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlGlobalUrlFiltering;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlGlobalUrlFiltering *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalUrlFiltering),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlGlobalUrlFiltering.rval =
                nmhSetFwlGlobalUrlFiltering (lv.nmhSetFwlGlobalUrlFiltering.
                                             i4SetValFwlGlobalUrlFiltering);
            lv.nmhSetFwlGlobalUrlFiltering.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlGlobalUrlFiltering *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlGlobalUrlFiltering;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalUrlFiltering)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_GLOBAL_MASTER_CONTROL_SWITCH:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlGlobalMasterControlSwitch;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhTestv2FwlGlobalMasterControlSwitch *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhTestv2FwlGlobalMasterControlSwitch),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlGlobalMasterControlSwitch.rval =
                nmhTestv2FwlGlobalMasterControlSwitch (lv.
                                                       nmhTestv2FwlGlobalMasterControlSwitch.
                                                       pu4ErrorCode,
                                                       lv.
                                                       nmhTestv2FwlGlobalMasterControlSwitch.
                                                       i4TestValFwlGlobalMasterControlSwitch);
            lv.nmhTestv2FwlGlobalMasterControlSwitch.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhTestv2FwlGlobalMasterControlSwitch *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlGlobalMasterControlSwitch;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhTestv2FwlGlobalMasterControlSwitch)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_GLOBAL_I_C_M_P_CONTROL_SWITCH:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlGlobalICMPControlSwitch;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhTestv2FwlGlobalICMPControlSwitch *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalICMPControlSwitch),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlGlobalICMPControlSwitch.rval =
                nmhTestv2FwlGlobalICMPControlSwitch (lv.
                                                     nmhTestv2FwlGlobalICMPControlSwitch.
                                                     pu4ErrorCode,
                                                     lv.
                                                     nmhTestv2FwlGlobalICMPControlSwitch.
                                                     i4TestValFwlGlobalICMPControlSwitch);
            lv.nmhTestv2FwlGlobalICMPControlSwitch.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhTestv2FwlGlobalICMPControlSwitch *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlGlobalICMPControlSwitch;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalICMPControlSwitch))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_GLOBAL_IP_SPOOF_FILTERING:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlGlobalIpSpoofFiltering;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlGlobalIpSpoofFiltering *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalIpSpoofFiltering),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlGlobalIpSpoofFiltering.rval =
                nmhTestv2FwlGlobalIpSpoofFiltering (lv.
                                                    nmhTestv2FwlGlobalIpSpoofFiltering.
                                                    pu4ErrorCode,
                                                    lv.
                                                    nmhTestv2FwlGlobalIpSpoofFiltering.
                                                    i4TestValFwlGlobalIpSpoofFiltering);
            lv.nmhTestv2FwlGlobalIpSpoofFiltering.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhTestv2FwlGlobalIpSpoofFiltering *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlGlobalIpSpoofFiltering;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalIpSpoofFiltering))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_GLOBAL_SRC_ROUTE_FILTERING:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlGlobalSrcRouteFiltering;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhTestv2FwlGlobalSrcRouteFiltering *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalSrcRouteFiltering),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlGlobalSrcRouteFiltering.rval =
                nmhTestv2FwlGlobalSrcRouteFiltering (lv.
                                                     nmhTestv2FwlGlobalSrcRouteFiltering.
                                                     pu4ErrorCode,
                                                     lv.
                                                     nmhTestv2FwlGlobalSrcRouteFiltering.
                                                     i4TestValFwlGlobalSrcRouteFiltering);
            lv.nmhTestv2FwlGlobalSrcRouteFiltering.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhTestv2FwlGlobalSrcRouteFiltering *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlGlobalSrcRouteFiltering;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalSrcRouteFiltering))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_GLOBAL_TINY_FRAGMENT_FILTERING:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlGlobalTinyFragmentFiltering;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhTestv2FwlGlobalTinyFragmentFiltering *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhTestv2FwlGlobalTinyFragmentFiltering),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlGlobalTinyFragmentFiltering.rval =
                nmhTestv2FwlGlobalTinyFragmentFiltering (lv.
                                                         nmhTestv2FwlGlobalTinyFragmentFiltering.
                                                         pu4ErrorCode,
                                                         lv.
                                                         nmhTestv2FwlGlobalTinyFragmentFiltering.
                                                         i4TestValFwlGlobalTinyFragmentFiltering);
            lv.nmhTestv2FwlGlobalTinyFragmentFiltering.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhTestv2FwlGlobalTinyFragmentFiltering *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlGlobalTinyFragmentFiltering;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhTestv2FwlGlobalTinyFragmentFiltering)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_GLOBAL_TCP_INTERCEPT:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlGlobalTcpIntercept;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlGlobalTcpIntercept *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalTcpIntercept),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlGlobalTcpIntercept.rval =
                nmhTestv2FwlGlobalTcpIntercept (lv.
                                                nmhTestv2FwlGlobalTcpIntercept.
                                                pu4ErrorCode,
                                                lv.
                                                nmhTestv2FwlGlobalTcpIntercept.
                                                i4TestValFwlGlobalTcpIntercept);
            lv.nmhTestv2FwlGlobalTcpIntercept.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlGlobalTcpIntercept *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlGlobalTcpIntercept;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalTcpIntercept)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_GLOBAL_TRAP:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlGlobalTrap;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlGlobalTrap *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalTrap),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlGlobalTrap.rval =
                nmhTestv2FwlGlobalTrap (lv.nmhTestv2FwlGlobalTrap.pu4ErrorCode,
                                        lv.nmhTestv2FwlGlobalTrap.
                                        i4TestValFwlGlobalTrap);
            lv.nmhTestv2FwlGlobalTrap.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlGlobalTrap *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlGlobalTrap;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalTrap)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_GLOBAL_TRACE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlGlobalTrace;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlGlobalTrace *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalTrace),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlGlobalTrace.rval =
                nmhTestv2FwlGlobalTrace (lv.nmhTestv2FwlGlobalTrace.
                                         pu4ErrorCode,
                                         lv.nmhTestv2FwlGlobalTrace.
                                         i4TestValFwlGlobalTrace);
            lv.nmhTestv2FwlGlobalTrace.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlGlobalTrace *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlGlobalTrace;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalTrace)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_GLOBAL_DEBUG:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlGlobalDebug;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlGlobalDebug *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalDebug),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlGlobalDebug.rval =
                nmhTestv2FwlGlobalDebug (lv.nmhTestv2FwlGlobalDebug.
                                         pu4ErrorCode,
                                         lv.nmhTestv2FwlGlobalDebug.
                                         i4TestValFwlGlobalDebug);
            lv.nmhTestv2FwlGlobalDebug.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlGlobalDebug *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlGlobalDebug;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalDebug)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_GLOBAL_URL_FILTERING:
        {

            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlGlobalUrlFiltering;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlGlobalUrlFiltering *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalUrlFiltering),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlGlobalUrlFiltering.rval =
                nmhTestv2FwlGlobalUrlFiltering (lv.
                                                nmhTestv2FwlGlobalUrlFiltering.
                                                pu4ErrorCode,
                                                lv.
                                                nmhTestv2FwlGlobalUrlFiltering.
                                                i4TestValFwlGlobalUrlFiltering);
            lv.nmhTestv2FwlGlobalUrlFiltering.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlGlobalUrlFiltering *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlGlobalUrlFiltering;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalUrlFiltering)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_GLOBAL_NET_BIOS_FILTERING:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlGlobalNetBiosFiltering;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlGlobalNetBiosFiltering *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalNetBiosFiltering),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlGlobalNetBiosFiltering.rval =
                nmhTestv2FwlGlobalNetBiosFiltering (lv.
                                                    nmhTestv2FwlGlobalNetBiosFiltering.
                                                    pu4ErrorCode,
                                                    lv.
                                                    nmhTestv2FwlGlobalNetBiosFiltering.
                                                    i4TestValFwlGlobalNetBiosFiltering);
            lv.nmhTestv2FwlGlobalNetBiosFiltering.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhTestv2FwlGlobalNetBiosFiltering *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlGlobalNetBiosFiltering;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalNetBiosFiltering))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_DEFN_TCP_INTERCEPT_THRESHOLD:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlDefnTcpInterceptThreshold;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlDefnTcpInterceptThreshold *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlDefnTcpInterceptThreshold),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlDefnTcpInterceptThreshold.rval =
                nmhGetFwlDefnTcpInterceptThreshold (lv.
                                                    nmhGetFwlDefnTcpInterceptThreshold.
                                                    pi4RetValFwlDefnTcpInterceptThreshold);
            lv.nmhGetFwlDefnTcpInterceptThreshold.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlDefnTcpInterceptThreshold *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlDefnTcpInterceptThreshold;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlDefnTcpInterceptThreshold))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_DEFN_INTERCEPT_TIMEOUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlDefnInterceptTimeout;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlDefnInterceptTimeout *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlDefnInterceptTimeout),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlDefnInterceptTimeout.rval =
                nmhGetFwlDefnInterceptTimeout (lv.nmhGetFwlDefnInterceptTimeout.
                                               pu4RetValFwlDefnInterceptTimeout);
            lv.nmhGetFwlDefnInterceptTimeout.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlDefnInterceptTimeout *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlDefnInterceptTimeout;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlDefnInterceptTimeout)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_DEFN_TCP_INTERCEPT_THRESHOLD:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlDefnTcpInterceptThreshold;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlDefnTcpInterceptThreshold *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlDefnTcpInterceptThreshold),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlDefnTcpInterceptThreshold.rval =
                nmhSetFwlDefnTcpInterceptThreshold (lv.
                                                    nmhSetFwlDefnTcpInterceptThreshold.
                                                    i4SetValFwlDefnTcpInterceptThreshold);
            lv.nmhSetFwlDefnTcpInterceptThreshold.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhSetFwlDefnTcpInterceptThreshold *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlDefnTcpInterceptThreshold;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlDefnTcpInterceptThreshold))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_DEFN_INTERCEPT_TIMEOUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlDefnInterceptTimeout;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlDefnInterceptTimeout *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlDefnInterceptTimeout),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlDefnInterceptTimeout.rval =
                nmhSetFwlDefnInterceptTimeout (lv.nmhSetFwlDefnInterceptTimeout.
                                               u4SetValFwlDefnInterceptTimeout);
            lv.nmhSetFwlDefnInterceptTimeout.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlDefnInterceptTimeout *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlDefnInterceptTimeout;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlDefnInterceptTimeout)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_DEFN_TCP_INTERCEPT_THRESHOLD:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlDefnTcpInterceptThreshold;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhTestv2FwlDefnTcpInterceptThreshold *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhTestv2FwlDefnTcpInterceptThreshold),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlDefnTcpInterceptThreshold.rval =
                nmhTestv2FwlDefnTcpInterceptThreshold (lv.
                                                       nmhTestv2FwlDefnTcpInterceptThreshold.
                                                       pu4ErrorCode,
                                                       lv.
                                                       nmhTestv2FwlDefnTcpInterceptThreshold.
                                                       i4TestValFwlDefnTcpInterceptThreshold);
            lv.nmhTestv2FwlDefnTcpInterceptThreshold.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhTestv2FwlDefnTcpInterceptThreshold *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlDefnTcpInterceptThreshold;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhTestv2FwlDefnTcpInterceptThreshold)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_DEFN_INTERCEPT_TIMEOUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlDefnInterceptTimeout;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlDefnInterceptTimeout *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlDefnInterceptTimeout),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlDefnInterceptTimeout.rval =
                nmhTestv2FwlDefnInterceptTimeout (lv.
                                                  nmhTestv2FwlDefnInterceptTimeout.
                                                  pu4ErrorCode,
                                                  lv.
                                                  nmhTestv2FwlDefnInterceptTimeout.
                                                  u4TestValFwlDefnInterceptTimeout);
            lv.nmhTestv2FwlDefnInterceptTimeout.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlDefnInterceptTimeout *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlDefnInterceptTimeout;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlDefnInterceptTimeout)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_FWL_DEFN_FILTER_TABLE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhValidateIndexInstanceFwlDefnFilterTable;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhValidateIndexInstanceFwlDefnFilterTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlDefnFilterTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhValidateIndexInstanceFwlDefnFilterTable.rval =
                nmhValidateIndexInstanceFwlDefnFilterTable (lv.
                                                            nmhValidateIndexInstanceFwlDefnFilterTable.
                                                            pFwlFilterFilterName);
            lv.nmhValidateIndexInstanceFwlDefnFilterTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhValidateIndexInstanceFwlDefnFilterTable *) p;
            OsixKerUseInfo.pSrc =
                &lv.nmhValidateIndexInstanceFwlDefnFilterTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlDefnFilterTable))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_FWL_DEFN_FILTER_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexFwlDefnFilterTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFirstIndexFwlDefnFilterTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlDefnFilterTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFirstIndexFwlDefnFilterTable.rval =
                nmhGetFirstIndexFwlDefnFilterTable (lv.
                                                    nmhGetFirstIndexFwlDefnFilterTable.
                                                    pFwlFilterFilterName);
            lv.nmhGetFirstIndexFwlDefnFilterTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFirstIndexFwlDefnFilterTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexFwlDefnFilterTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlDefnFilterTable))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_FWL_DEFN_FILTER_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexFwlDefnFilterTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetNextIndexFwlDefnFilterTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlDefnFilterTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetNextIndexFwlDefnFilterTable.rval =
                nmhGetNextIndexFwlDefnFilterTable (lv.
                                                   nmhGetNextIndexFwlDefnFilterTable.
                                                   pFwlFilterFilterName,
                                                   lv.
                                                   nmhGetNextIndexFwlDefnFilterTable.
                                                   pNextFwlFilterFilterName);
            lv.nmhGetNextIndexFwlDefnFilterTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetNextIndexFwlDefnFilterTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexFwlDefnFilterTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlDefnFilterTable)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_FILTER_SRC_ADDRESS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlFilterSrcAddress;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlFilterSrcAddress *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterSrcAddress),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlFilterSrcAddress.rval =
                nmhGetFwlFilterSrcAddress (lv.nmhGetFwlFilterSrcAddress.
                                           pFwlFilterFilterName,
                                           lv.nmhGetFwlFilterSrcAddress.
                                           pRetValFwlFilterSrcAddress);
            lv.nmhGetFwlFilterSrcAddress.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlFilterSrcAddress *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlFilterSrcAddress;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterSrcAddress)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_FILTER_DEST_ADDRESS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlFilterDestAddress;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlFilterDestAddress *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterDestAddress),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlFilterDestAddress.rval =
                nmhGetFwlFilterDestAddress (lv.nmhGetFwlFilterDestAddress.
                                            pFwlFilterFilterName,
                                            lv.nmhGetFwlFilterDestAddress.
                                            pRetValFwlFilterDestAddress);
            lv.nmhGetFwlFilterDestAddress.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlFilterDestAddress *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlFilterDestAddress;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterDestAddress)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_FILTER_PROTOCOL:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlFilterProtocol;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlFilterProtocol *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterProtocol),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlFilterProtocol.rval =
                nmhGetFwlFilterProtocol (lv.nmhGetFwlFilterProtocol.
                                         pFwlFilterFilterName,
                                         lv.nmhGetFwlFilterProtocol.
                                         pi4RetValFwlFilterProtocol);
            lv.nmhGetFwlFilterProtocol.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlFilterProtocol *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlFilterProtocol;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterProtocol)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_FILTER_SRC_PORT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlFilterSrcPort;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlFilterSrcPort *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterSrcPort),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlFilterSrcPort.rval =
                nmhGetFwlFilterSrcPort (lv.nmhGetFwlFilterSrcPort.
                                        pFwlFilterFilterName,
                                        lv.nmhGetFwlFilterSrcPort.
                                        pRetValFwlFilterSrcPort);
            lv.nmhGetFwlFilterSrcPort.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlFilterSrcPort *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlFilterSrcPort;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterSrcPort)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_FILTER_DEST_PORT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlFilterDestPort;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlFilterDestPort *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterDestPort),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlFilterDestPort.rval =
                nmhGetFwlFilterDestPort (lv.nmhGetFwlFilterDestPort.
                                         pFwlFilterFilterName,
                                         lv.nmhGetFwlFilterDestPort.
                                         pRetValFwlFilterDestPort);
            lv.nmhGetFwlFilterDestPort.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlFilterDestPort *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlFilterDestPort;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterDestPort)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_FILTER_ACK_BIT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlFilterAckBit;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlFilterAckBit *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterAckBit),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlFilterAckBit.rval =
                nmhGetFwlFilterAckBit (lv.nmhGetFwlFilterAckBit.
                                       pFwlFilterFilterName,
                                       lv.nmhGetFwlFilterAckBit.
                                       pi4RetValFwlFilterAckBit);
            lv.nmhGetFwlFilterAckBit.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlFilterAckBit *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlFilterAckBit;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterAckBit)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_FILTER_RST_BIT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlFilterRstBit;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlFilterRstBit *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterRstBit),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlFilterRstBit.rval =
                nmhGetFwlFilterRstBit (lv.nmhGetFwlFilterRstBit.
                                       pFwlFilterFilterName,
                                       lv.nmhGetFwlFilterRstBit.
                                       pi4RetValFwlFilterRstBit);
            lv.nmhGetFwlFilterRstBit.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlFilterRstBit *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlFilterRstBit;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterRstBit)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_FILTER_TOS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlFilterTos;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlFilterTos *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterTos),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlFilterTos.rval =
                nmhGetFwlFilterTos (lv.nmhGetFwlFilterTos.pFwlFilterFilterName,
                                    lv.nmhGetFwlFilterTos.
                                    pi4RetValFwlFilterTos);
            lv.nmhGetFwlFilterTos.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlFilterTos *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlFilterTos;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterTos)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_FILTER_ACCOUNTING:
        {

            OsixKerUseInfo.pDest = &lv.nmhGetFwlFilterAccounting;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlFilterAccounting *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterAccounting),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            /* Invoke NPAPI */
            lv.nmhGetFwlFilterAccounting.rval =
                nmhGetFwlFilterAccounting (lv.nmhGetFwlFilterAccounting.
                                           pFwlFilterFilterName,
                                           lv.nmhGetFwlFilterAccounting.
                                           pi4RetValFwlFilterAccounting);
            lv.nmhGetFwlFilterAccounting.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlFilterAccounting *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlFilterAccounting;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterAccounting)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_FILTER_HIT_CLEAR:
        {

            OsixKerUseInfo.pDest = &lv.nmhGetFwlFilterHitClear;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlFilterHitClear *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterHitClear),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            /* Invoke NPAPI */
            lv.nmhGetFwlFilterHitClear.rval =
                nmhGetFwlFilterHitClear (lv.nmhGetFwlFilterHitClear.
                                         pFwlFilterFilterName,
                                         lv.nmhGetFwlFilterHitClear.
                                         pi4RetValFwlFilterHitClear);
            lv.nmhGetFwlFilterHitClear.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlFilterHitClear *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlFilterHitClear;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterHitClear)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_GET_FWL_FILTER_HITS_COUNT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlFilterHitsCount;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlFilterHitsCount *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterHitsCount),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlFilterHitsCount.rval =
                nmhGetFwlFilterHitsCount (lv.nmhGetFwlFilterHitsCount.
                                          pFwlFilterFilterName,
                                          lv.nmhGetFwlFilterHitsCount.
                                          pu4RetValFwlFilterHitsCount);
            lv.nmhGetFwlFilterHitsCount.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlFilterHitsCount *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlFilterHitsCount;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterHitsCount)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_FILTER_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlFilterRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlFilterRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterRowStatus),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlFilterRowStatus.rval =
                nmhGetFwlFilterRowStatus (lv.nmhGetFwlFilterRowStatus.
                                          pFwlFilterFilterName,
                                          lv.nmhGetFwlFilterRowStatus.
                                          pi4RetValFwlFilterRowStatus);
            lv.nmhGetFwlFilterRowStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlFilterRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlFilterRowStatus;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterRowStatus)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_FILTER_SRC_ADDRESS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlFilterSrcAddress;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlFilterSrcAddress *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterSrcAddress),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlFilterSrcAddress.rval =
                nmhSetFwlFilterSrcAddress (lv.nmhSetFwlFilterSrcAddress.
                                           pFwlFilterFilterName,
                                           lv.nmhSetFwlFilterSrcAddress.
                                           pSetValFwlFilterSrcAddress);
            lv.nmhSetFwlFilterSrcAddress.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlFilterSrcAddress *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlFilterSrcAddress;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterSrcAddress)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_FILTER_DEST_ADDRESS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlFilterDestAddress;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlFilterDestAddress *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterDestAddress),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlFilterDestAddress.rval =
                nmhSetFwlFilterDestAddress (lv.nmhSetFwlFilterDestAddress.
                                            pFwlFilterFilterName,
                                            lv.nmhSetFwlFilterDestAddress.
                                            pSetValFwlFilterDestAddress);
            lv.nmhSetFwlFilterDestAddress.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlFilterDestAddress *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlFilterDestAddress;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterDestAddress)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_FILTER_PROTOCOL:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlFilterProtocol;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlFilterProtocol *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterProtocol),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlFilterProtocol.rval =
                nmhSetFwlFilterProtocol (lv.nmhSetFwlFilterProtocol.
                                         pFwlFilterFilterName,
                                         lv.nmhSetFwlFilterProtocol.
                                         i4SetValFwlFilterProtocol);
            lv.nmhSetFwlFilterProtocol.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlFilterProtocol *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlFilterProtocol;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterProtocol)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_FILTER_SRC_PORT:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlFilterSrcPort;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlFilterSrcPort *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterSrcPort),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlFilterSrcPort.rval =
                nmhSetFwlFilterSrcPort (lv.nmhSetFwlFilterSrcPort.
                                        pFwlFilterFilterName,
                                        lv.nmhSetFwlFilterSrcPort.
                                        pSetValFwlFilterSrcPort);
            lv.nmhSetFwlFilterSrcPort.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlFilterSrcPort *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlFilterSrcPort;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterSrcPort)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_FILTER_DEST_PORT:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlFilterDestPort;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlFilterDestPort *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterDestPort),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlFilterDestPort.rval =
                nmhSetFwlFilterDestPort (lv.nmhSetFwlFilterDestPort.
                                         pFwlFilterFilterName,
                                         lv.nmhSetFwlFilterDestPort.
                                         pSetValFwlFilterDestPort);
            lv.nmhSetFwlFilterDestPort.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlFilterDestPort *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlFilterDestPort;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterDestPort)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_FILTER_ACK_BIT:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlFilterAckBit;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlFilterAckBit *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterAckBit),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlFilterAckBit.rval =
                nmhSetFwlFilterAckBit (lv.nmhSetFwlFilterAckBit.
                                       pFwlFilterFilterName,
                                       lv.nmhSetFwlFilterAckBit.
                                       i4SetValFwlFilterAckBit);
            lv.nmhSetFwlFilterAckBit.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlFilterAckBit *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlFilterAckBit;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterAckBit)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_FILTER_RST_BIT:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlFilterRstBit;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlFilterRstBit *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterRstBit),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlFilterRstBit.rval =
                nmhSetFwlFilterRstBit (lv.nmhSetFwlFilterRstBit.
                                       pFwlFilterFilterName,
                                       lv.nmhSetFwlFilterRstBit.
                                       i4SetValFwlFilterRstBit);
            lv.nmhSetFwlFilterRstBit.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlFilterRstBit *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlFilterRstBit;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterRstBit)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_FILTER_TOS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlFilterTos;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlFilterTos *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterTos),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlFilterTos.rval =
                nmhSetFwlFilterTos (lv.nmhSetFwlFilterTos.pFwlFilterFilterName,
                                    lv.nmhSetFwlFilterTos.i4SetValFwlFilterTos);
            lv.nmhSetFwlFilterTos.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlFilterTos *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlFilterTos;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterTos)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_FILTER_ACCOUNTING:
        {

            OsixKerUseInfo.pDest = &lv.nmhSetFwlFilterAccounting;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlFilterAccounting *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterAccounting),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            /* Invoke NPAPI */
            lv.nmhSetFwlFilterAccounting.rval =
                nmhSetFwlFilterAccounting
                (lv.nmhSetFwlFilterAccounting.pFwlFilterFilterName,
                 lv.nmhSetFwlFilterAccounting.i4SetValFwlFilterAccounting);
            lv.nmhSetFwlFilterAccounting.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlFilterAccounting *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlFilterAccounting;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterAccounting)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_FILTER_HIT_CLEAR:
        {

            OsixKerUseInfo.pDest = &lv.nmhSetFwlFilterHitClear;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlFilterHitClear *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterHitClear),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            /* Invoke NPAPI */
            lv.nmhSetFwlFilterHitClear.rval =
                nmhSetFwlFilterHitClear (lv.nmhSetFwlFilterHitClear.
                                         pFwlFilterFilterName,
                                         lv.nmhSetFwlFilterHitClear.
                                         i4SetValFwlFilterHitClear);
            lv.nmhSetFwlFilterHitClear.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlFilterHitClear *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlFilterHitClear;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterHitClear)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_SET_FWL_FILTER_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlFilterRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlFilterRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterRowStatus),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlFilterRowStatus.rval =
                nmhSetFwlFilterRowStatus (lv.nmhSetFwlFilterRowStatus.
                                          pFwlFilterFilterName,
                                          lv.nmhSetFwlFilterRowStatus.
                                          i4SetValFwlFilterRowStatus);
            lv.nmhSetFwlFilterRowStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlFilterRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlFilterRowStatus;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterRowStatus)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_FILTER_SRC_ADDRESS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlFilterSrcAddress;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlFilterSrcAddress *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterSrcAddress),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlFilterSrcAddress.rval =
                nmhTestv2FwlFilterSrcAddress (lv.nmhTestv2FwlFilterSrcAddress.
                                              pu4ErrorCode,
                                              lv.nmhTestv2FwlFilterSrcAddress.
                                              pFwlFilterFilterName,
                                              lv.nmhTestv2FwlFilterSrcAddress.
                                              pTestValFwlFilterSrcAddress);
            lv.nmhTestv2FwlFilterSrcAddress.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlFilterSrcAddress *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlFilterSrcAddress;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterSrcAddress)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_FILTER_DEST_ADDRESS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlFilterDestAddress;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlFilterDestAddress *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterDestAddress),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlFilterDestAddress.rval =
                nmhTestv2FwlFilterDestAddress (lv.nmhTestv2FwlFilterDestAddress.
                                               pu4ErrorCode,
                                               lv.nmhTestv2FwlFilterDestAddress.
                                               pFwlFilterFilterName,
                                               lv.nmhTestv2FwlFilterDestAddress.
                                               pTestValFwlFilterDestAddress);
            lv.nmhTestv2FwlFilterDestAddress.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlFilterDestAddress *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlFilterDestAddress;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterDestAddress)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_FILTER_PROTOCOL:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlFilterProtocol;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlFilterProtocol *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterProtocol),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlFilterProtocol.rval =
                nmhTestv2FwlFilterProtocol (lv.nmhTestv2FwlFilterProtocol.
                                            pu4ErrorCode,
                                            lv.nmhTestv2FwlFilterProtocol.
                                            pFwlFilterFilterName,
                                            lv.nmhTestv2FwlFilterProtocol.
                                            i4TestValFwlFilterProtocol);
            lv.nmhTestv2FwlFilterProtocol.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlFilterProtocol *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlFilterProtocol;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterProtocol)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_FILTER_SRC_PORT:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlFilterSrcPort;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlFilterSrcPort *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterSrcPort),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlFilterSrcPort.rval =
                nmhTestv2FwlFilterSrcPort (lv.nmhTestv2FwlFilterSrcPort.
                                           pu4ErrorCode,
                                           lv.nmhTestv2FwlFilterSrcPort.
                                           pFwlFilterFilterName,
                                           lv.nmhTestv2FwlFilterSrcPort.
                                           pTestValFwlFilterSrcPort);
            lv.nmhTestv2FwlFilterSrcPort.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlFilterSrcPort *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlFilterSrcPort;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterSrcPort)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_FILTER_DEST_PORT:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlFilterDestPort;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlFilterDestPort *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterDestPort),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlFilterDestPort.rval =
                nmhTestv2FwlFilterDestPort (lv.nmhTestv2FwlFilterDestPort.
                                            pu4ErrorCode,
                                            lv.nmhTestv2FwlFilterDestPort.
                                            pFwlFilterFilterName,
                                            lv.nmhTestv2FwlFilterDestPort.
                                            pTestValFwlFilterDestPort);
            lv.nmhTestv2FwlFilterDestPort.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlFilterDestPort *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlFilterDestPort;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterDestPort)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_FILTER_ACK_BIT:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlFilterAckBit;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlFilterAckBit *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterAckBit),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlFilterAckBit.rval =
                nmhTestv2FwlFilterAckBit (lv.nmhTestv2FwlFilterAckBit.
                                          pu4ErrorCode,
                                          lv.nmhTestv2FwlFilterAckBit.
                                          pFwlFilterFilterName,
                                          lv.nmhTestv2FwlFilterAckBit.
                                          i4TestValFwlFilterAckBit);
            lv.nmhTestv2FwlFilterAckBit.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlFilterAckBit *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlFilterAckBit;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterAckBit)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_FILTER_RST_BIT:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlFilterRstBit;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlFilterRstBit *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterRstBit),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlFilterRstBit.rval =
                nmhTestv2FwlFilterRstBit (lv.nmhTestv2FwlFilterRstBit.
                                          pu4ErrorCode,
                                          lv.nmhTestv2FwlFilterRstBit.
                                          pFwlFilterFilterName,
                                          lv.nmhTestv2FwlFilterRstBit.
                                          i4TestValFwlFilterRstBit);
            lv.nmhTestv2FwlFilterRstBit.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlFilterRstBit *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlFilterRstBit;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterRstBit)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_FILTER_TOS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlFilterTos;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlFilterTos *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterTos),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlFilterTos.rval =
                nmhTestv2FwlFilterTos (lv.nmhTestv2FwlFilterTos.pu4ErrorCode,
                                       lv.nmhTestv2FwlFilterTos.
                                       pFwlFilterFilterName,
                                       lv.nmhTestv2FwlFilterTos.
                                       i4TestValFwlFilterTos);
            lv.nmhTestv2FwlFilterTos.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlFilterTos *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlFilterTos;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterTos)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_FILTER_ACCOUNTING:
        {

            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlFilterAccounting;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlFilterAccounting *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterAccounting),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            /* Invoke NPAPI */
            lv.nmhTestv2FwlFilterAccounting.rval =
                nmhTestv2FwlFilterAccounting (lv.nmhTestv2FwlFilterAccounting.
                                              pu4ErrorCode,
                                              lv.nmhTestv2FwlFilterAccounting.
                                              pFwlFilterFilterName,
                                              lv.nmhTestv2FwlFilterAccounting.
                                              i4TestValFwlFilterAccounting);
            lv.nmhTestv2FwlFilterAccounting.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlFilterAccounting *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlFilterAccounting;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterAccounting)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_FILTER_HIT_CLEAR:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlFilterHitClear;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlFilterHitClear *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterHitClear),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            /* Invoke NPAPI */
            lv.nmhTestv2FwlFilterHitClear.rval =
                nmhTestv2FwlFilterHitClear (lv.nmhTestv2FwlFilterHitClear.
                                            pu4ErrorCode,
                                            lv.nmhTestv2FwlFilterHitClear.
                                            pFwlFilterFilterName,
                                            lv.nmhTestv2FwlFilterHitClear.
                                            i4TestValFwlFilterHitClear);
            lv.nmhTestv2FwlFilterHitClear.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlFilterHitClear *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlFilterHitClear;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterHitClear)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_TESTV2_FWL_FILTER_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlFilterRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlFilterRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterRowStatus),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlFilterRowStatus.rval =
                nmhTestv2FwlFilterRowStatus (lv.nmhTestv2FwlFilterRowStatus.
                                             pu4ErrorCode,
                                             lv.nmhTestv2FwlFilterRowStatus.
                                             pFwlFilterFilterName,
                                             lv.nmhTestv2FwlFilterRowStatus.
                                             i4TestValFwlFilterRowStatus);
            lv.nmhTestv2FwlFilterRowStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlFilterRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlFilterRowStatus;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterRowStatus)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_FWL_DEFN_RULE_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhValidateIndexInstanceFwlDefnRuleTable;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhValidateIndexInstanceFwlDefnRuleTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlDefnRuleTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhValidateIndexInstanceFwlDefnRuleTable.rval =
                nmhValidateIndexInstanceFwlDefnRuleTable (lv.
                                                          nmhValidateIndexInstanceFwlDefnRuleTable.
                                                          pFwlRuleRuleName);
            lv.nmhValidateIndexInstanceFwlDefnRuleTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhValidateIndexInstanceFwlDefnRuleTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhValidateIndexInstanceFwlDefnRuleTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlDefnRuleTable)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_FWL_DEFN_RULE_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexFwlDefnRuleTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFirstIndexFwlDefnRuleTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlDefnRuleTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFirstIndexFwlDefnRuleTable.rval =
                nmhGetFirstIndexFwlDefnRuleTable (lv.
                                                  nmhGetFirstIndexFwlDefnRuleTable.
                                                  pFwlRuleRuleName);
            lv.nmhGetFirstIndexFwlDefnRuleTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFirstIndexFwlDefnRuleTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexFwlDefnRuleTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlDefnRuleTable)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_FWL_DEFN_RULE_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexFwlDefnRuleTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetNextIndexFwlDefnRuleTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlDefnRuleTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetNextIndexFwlDefnRuleTable.rval =
                nmhGetNextIndexFwlDefnRuleTable (lv.
                                                 nmhGetNextIndexFwlDefnRuleTable.
                                                 pFwlRuleRuleName,
                                                 lv.
                                                 nmhGetNextIndexFwlDefnRuleTable.
                                                 pNextFwlRuleRuleName);
            lv.nmhGetNextIndexFwlDefnRuleTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetNextIndexFwlDefnRuleTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexFwlDefnRuleTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlDefnRuleTable)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_RULE_FILTER_SET:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlRuleFilterSet;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlRuleFilterSet *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlRuleFilterSet),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlRuleFilterSet.rval =
                nmhGetFwlRuleFilterSet (lv.nmhGetFwlRuleFilterSet.
                                        pFwlRuleRuleName,
                                        lv.nmhGetFwlRuleFilterSet.
                                        pRetValFwlRuleFilterSet);
            lv.nmhGetFwlRuleFilterSet.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlRuleFilterSet *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlRuleFilterSet;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlRuleFilterSet)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_RULE_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlRuleRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlRuleRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlRuleRowStatus),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlRuleRowStatus.rval =
                nmhGetFwlRuleRowStatus (lv.nmhGetFwlRuleRowStatus.
                                        pFwlRuleRuleName,
                                        lv.nmhGetFwlRuleRowStatus.
                                        pi4RetValFwlRuleRowStatus);
            lv.nmhGetFwlRuleRowStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlRuleRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlRuleRowStatus;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlRuleRowStatus)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_RULE_FILTER_SET:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlRuleFilterSet;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlRuleFilterSet *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlRuleFilterSet),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlRuleFilterSet.rval =
                nmhSetFwlRuleFilterSet (lv.nmhSetFwlRuleFilterSet.
                                        pFwlRuleRuleName,
                                        lv.nmhSetFwlRuleFilterSet.
                                        pSetValFwlRuleFilterSet);
            lv.nmhSetFwlRuleFilterSet.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlRuleFilterSet *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlRuleFilterSet;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlRuleFilterSet)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_RULE_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlRuleRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlRuleRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlRuleRowStatus),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlRuleRowStatus.rval =
                nmhSetFwlRuleRowStatus (lv.nmhSetFwlRuleRowStatus.
                                        pFwlRuleRuleName,
                                        lv.nmhSetFwlRuleRowStatus.
                                        i4SetValFwlRuleRowStatus);
            lv.nmhSetFwlRuleRowStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlRuleRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlRuleRowStatus;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlRuleRowStatus)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_RULE_FILTER_SET:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlRuleFilterSet;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlRuleFilterSet *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlRuleFilterSet),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlRuleFilterSet.rval =
                nmhTestv2FwlRuleFilterSet (lv.nmhTestv2FwlRuleFilterSet.
                                           pu4ErrorCode,
                                           lv.nmhTestv2FwlRuleFilterSet.
                                           pFwlRuleRuleName,
                                           lv.nmhTestv2FwlRuleFilterSet.
                                           pTestValFwlRuleFilterSet);
            lv.nmhTestv2FwlRuleFilterSet.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlRuleFilterSet *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlRuleFilterSet;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlRuleFilterSet)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_RULE_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlRuleRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlRuleRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlRuleRowStatus),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlRuleRowStatus.rval =
                nmhTestv2FwlRuleRowStatus (lv.nmhTestv2FwlRuleRowStatus.
                                           pu4ErrorCode,
                                           lv.nmhTestv2FwlRuleRowStatus.
                                           pFwlRuleRuleName,
                                           lv.nmhTestv2FwlRuleRowStatus.
                                           i4TestValFwlRuleRowStatus);
            lv.nmhTestv2FwlRuleRowStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlRuleRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlRuleRowStatus;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlRuleRowStatus)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_FWL_DEFN_ACL_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhValidateIndexInstanceFwlDefnAclTable;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhValidateIndexInstanceFwlDefnAclTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlDefnAclTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhValidateIndexInstanceFwlDefnAclTable.rval =
                nmhValidateIndexInstanceFwlDefnAclTable (lv.
                                                         nmhValidateIndexInstanceFwlDefnAclTable.
                                                         i4FwlAclIfIndex,
                                                         lv.
                                                         nmhValidateIndexInstanceFwlDefnAclTable.
                                                         pFwlAclAclName,
                                                         lv.
                                                         nmhValidateIndexInstanceFwlDefnAclTable.
                                                         i4FwlAclDirection);
            lv.nmhValidateIndexInstanceFwlDefnAclTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhValidateIndexInstanceFwlDefnAclTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhValidateIndexInstanceFwlDefnAclTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlDefnAclTable)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_FWL_DEFN_ACL_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexFwlDefnAclTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFirstIndexFwlDefnAclTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlDefnAclTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFirstIndexFwlDefnAclTable.rval =
                nmhGetFirstIndexFwlDefnAclTable (lv.
                                                 nmhGetFirstIndexFwlDefnAclTable.
                                                 pi4FwlAclIfIndex,
                                                 lv.
                                                 nmhGetFirstIndexFwlDefnAclTable.
                                                 pFwlAclAclName,
                                                 lv.
                                                 nmhGetFirstIndexFwlDefnAclTable.
                                                 pi4FwlAclDirection);
            lv.nmhGetFirstIndexFwlDefnAclTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFirstIndexFwlDefnAclTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexFwlDefnAclTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlDefnAclTable)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_FWL_DEFN_ACL_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexFwlDefnAclTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetNextIndexFwlDefnAclTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlDefnAclTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetNextIndexFwlDefnAclTable.rval =
                nmhGetNextIndexFwlDefnAclTable (lv.
                                                nmhGetNextIndexFwlDefnAclTable.
                                                i4FwlAclIfIndex,
                                                lv.
                                                nmhGetNextIndexFwlDefnAclTable.
                                                pi4NextFwlAclIfIndex,
                                                lv.
                                                nmhGetNextIndexFwlDefnAclTable.
                                                pFwlAclAclName,
                                                lv.
                                                nmhGetNextIndexFwlDefnAclTable.
                                                pNextFwlAclAclName,
                                                lv.
                                                nmhGetNextIndexFwlDefnAclTable.
                                                i4FwlAclDirection,
                                                lv.
                                                nmhGetNextIndexFwlDefnAclTable.
                                                pi4NextFwlAclDirection);
            lv.nmhGetNextIndexFwlDefnAclTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetNextIndexFwlDefnAclTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexFwlDefnAclTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlDefnAclTable)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_ACL_ACTION:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlAclAction;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlAclAction *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlAclAction),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlAclAction.rval =
                nmhGetFwlAclAction (lv.nmhGetFwlAclAction.i4FwlAclIfIndex,
                                    lv.nmhGetFwlAclAction.pFwlAclAclName,
                                    lv.nmhGetFwlAclAction.i4FwlAclDirection,
                                    lv.nmhGetFwlAclAction.
                                    pi4RetValFwlAclAction);
            lv.nmhGetFwlAclAction.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlAclAction *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlAclAction;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlAclAction)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_ACL_SEQUENCE_NUMBER:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlAclSequenceNumber;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlAclSequenceNumber *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlAclSequenceNumber),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlAclSequenceNumber.rval =
                nmhGetFwlAclSequenceNumber (lv.nmhGetFwlAclSequenceNumber.
                                            i4FwlAclIfIndex,
                                            lv.nmhGetFwlAclSequenceNumber.
                                            pFwlAclAclName,
                                            lv.nmhGetFwlAclSequenceNumber.
                                            i4FwlAclDirection,
                                            lv.nmhGetFwlAclSequenceNumber.
                                            pi4RetValFwlAclSequenceNumber);
            lv.nmhGetFwlAclSequenceNumber.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlAclSequenceNumber *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlAclSequenceNumber;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlAclSequenceNumber)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_ACL_ACL_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlAclAclType;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlAclAclType *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlAclAclType),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlAclAclType.rval =
                nmhGetFwlAclAclType (lv.nmhGetFwlAclAclType.i4FwlAclIfIndex,
                                     lv.nmhGetFwlAclAclType.pFwlAclAclName,
                                     lv.nmhGetFwlAclAclType.i4FwlAclDirection,
                                     lv.nmhGetFwlAclAclType.
                                     pi4RetValFwlAclAclType);
            lv.nmhGetFwlAclAclType.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlAclAclType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlAclAclType;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlAclAclType)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_ACL_LOG_TRIGGER:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlAclLogTrigger;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlAclLogTrigger *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlAclLogTrigger),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlAclLogTrigger.rval =
                nmhGetFwlAclLogTrigger (lv.nmhGetFwlAclLogTrigger.
                                        i4FwlAclIfIndex,
                                        lv.nmhGetFwlAclLogTrigger.
                                        pFwlAclAclName,
                                        lv.nmhGetFwlAclLogTrigger.
                                        i4FwlAclDirection,
                                        lv.nmhGetFwlAclLogTrigger.
                                        pi4RetValFwlAclLogTrigger);
            lv.nmhGetFwlAclLogTrigger.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlAclLogTrigger *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlAclLogTrigger;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlAclLogTrigger)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_ACL_FRAG_ACTION:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlAclFragAction;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlAclFragAction *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlAclFragAction),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlAclFragAction.rval =
                nmhGetFwlAclFragAction (lv.nmhGetFwlAclFragAction.
                                        i4FwlAclIfIndex,
                                        lv.nmhGetFwlAclFragAction.
                                        pFwlAclAclName,
                                        lv.nmhGetFwlAclFragAction.
                                        i4FwlAclDirection,
                                        lv.nmhGetFwlAclFragAction.
                                        pi4RetValFwlAclFragAction);
            lv.nmhGetFwlAclFragAction.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlAclFragAction *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlAclFragAction;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlAclFragAction)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_ACL_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlAclRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlAclRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlAclRowStatus),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlAclRowStatus.rval =
                nmhGetFwlAclRowStatus (lv.nmhGetFwlAclRowStatus.i4FwlAclIfIndex,
                                       lv.nmhGetFwlAclRowStatus.pFwlAclAclName,
                                       lv.nmhGetFwlAclRowStatus.
                                       i4FwlAclDirection,
                                       lv.nmhGetFwlAclRowStatus.
                                       pi4RetValFwlAclRowStatus);
            lv.nmhGetFwlAclRowStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlAclRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlAclRowStatus;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlAclRowStatus)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_ACL_ACTION:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlAclAction;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlAclAction *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlAclAction),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlAclAction.rval =
                nmhSetFwlAclAction (lv.nmhSetFwlAclAction.i4FwlAclIfIndex,
                                    lv.nmhSetFwlAclAction.pFwlAclAclName,
                                    lv.nmhSetFwlAclAction.i4FwlAclDirection,
                                    lv.nmhSetFwlAclAction.i4SetValFwlAclAction);
            lv.nmhSetFwlAclAction.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlAclAction *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlAclAction;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlAclAction)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_ACL_SEQUENCE_NUMBER:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlAclSequenceNumber;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlAclSequenceNumber *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlAclSequenceNumber),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlAclSequenceNumber.rval =
                nmhSetFwlAclSequenceNumber (lv.nmhSetFwlAclSequenceNumber.
                                            i4FwlAclIfIndex,
                                            lv.nmhSetFwlAclSequenceNumber.
                                            pFwlAclAclName,
                                            lv.nmhSetFwlAclSequenceNumber.
                                            i4FwlAclDirection,
                                            lv.nmhSetFwlAclSequenceNumber.
                                            i4SetValFwlAclSequenceNumber);
            lv.nmhSetFwlAclSequenceNumber.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlAclSequenceNumber *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlAclSequenceNumber;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlAclSequenceNumber)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_ACL_LOG_TRIGGER:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlAclLogTrigger;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlAclLogTrigger *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlAclLogTrigger),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlAclLogTrigger.rval =
                nmhSetFwlAclLogTrigger (lv.nmhSetFwlAclLogTrigger.
                                        i4FwlAclIfIndex,
                                        lv.nmhSetFwlAclLogTrigger.
                                        pFwlAclAclName,
                                        lv.nmhSetFwlAclLogTrigger.
                                        i4FwlAclDirection,
                                        lv.nmhSetFwlAclLogTrigger.
                                        i4SetValFwlAclLogTrigger);
            lv.nmhSetFwlAclLogTrigger.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlAclLogTrigger *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlAclLogTrigger;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlAclLogTrigger)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_ACL_FRAG_ACTION:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlAclFragAction;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlAclFragAction *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlAclFragAction),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlAclFragAction.rval =
                nmhSetFwlAclFragAction (lv.nmhSetFwlAclFragAction.
                                        i4FwlAclIfIndex,
                                        lv.nmhSetFwlAclFragAction.
                                        pFwlAclAclName,
                                        lv.nmhSetFwlAclFragAction.
                                        i4FwlAclDirection,
                                        lv.nmhSetFwlAclFragAction.
                                        i4SetValFwlAclFragAction);
            lv.nmhSetFwlAclFragAction.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlAclFragAction *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlAclFragAction;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlAclFragAction)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_ACL_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlAclRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlAclRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlAclRowStatus),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlAclRowStatus.rval =
                nmhSetFwlAclRowStatus (lv.nmhSetFwlAclRowStatus.i4FwlAclIfIndex,
                                       lv.nmhSetFwlAclRowStatus.pFwlAclAclName,
                                       lv.nmhSetFwlAclRowStatus.
                                       i4FwlAclDirection,
                                       lv.nmhSetFwlAclRowStatus.
                                       i4SetValFwlAclRowStatus);
            lv.nmhSetFwlAclRowStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlAclRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlAclRowStatus;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlAclRowStatus)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_ACL_ACTION:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlAclAction;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlAclAction *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlAclAction),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlAclAction.rval =
                nmhTestv2FwlAclAction (lv.nmhTestv2FwlAclAction.pu4ErrorCode,
                                       lv.nmhTestv2FwlAclAction.i4FwlAclIfIndex,
                                       lv.nmhTestv2FwlAclAction.pFwlAclAclName,
                                       lv.nmhTestv2FwlAclAction.
                                       i4FwlAclDirection,
                                       lv.nmhTestv2FwlAclAction.
                                       i4TestValFwlAclAction);
            lv.nmhTestv2FwlAclAction.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlAclAction *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlAclAction;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlAclAction)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_ACL_SEQUENCE_NUMBER:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlAclSequenceNumber;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlAclSequenceNumber *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlAclSequenceNumber),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlAclSequenceNumber.rval =
                nmhTestv2FwlAclSequenceNumber (lv.nmhTestv2FwlAclSequenceNumber.
                                               pu4ErrorCode,
                                               lv.nmhTestv2FwlAclSequenceNumber.
                                               i4FwlAclIfIndex,
                                               lv.nmhTestv2FwlAclSequenceNumber.
                                               pFwlAclAclName,
                                               lv.nmhTestv2FwlAclSequenceNumber.
                                               i4FwlAclDirection,
                                               lv.nmhTestv2FwlAclSequenceNumber.
                                               i4TestValFwlAclSequenceNumber);
            lv.nmhTestv2FwlAclSequenceNumber.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlAclSequenceNumber *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlAclSequenceNumber;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlAclSequenceNumber)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_ACL_LOG_TRIGGER:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlAclLogTrigger;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlAclLogTrigger *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlAclLogTrigger),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlAclLogTrigger.rval =
                nmhTestv2FwlAclLogTrigger (lv.nmhTestv2FwlAclLogTrigger.
                                           pu4ErrorCode,
                                           lv.nmhTestv2FwlAclLogTrigger.
                                           i4FwlAclIfIndex,
                                           lv.nmhTestv2FwlAclLogTrigger.
                                           pFwlAclAclName,
                                           lv.nmhTestv2FwlAclLogTrigger.
                                           i4FwlAclDirection,
                                           lv.nmhTestv2FwlAclLogTrigger.
                                           i4TestValFwlAclLogTrigger);
            lv.nmhTestv2FwlAclLogTrigger.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlAclLogTrigger *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlAclLogTrigger;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlAclLogTrigger)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_ACL_FRAG_ACTION:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlAclFragAction;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlAclFragAction *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlAclFragAction),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlAclFragAction.rval =
                nmhTestv2FwlAclFragAction (lv.nmhTestv2FwlAclFragAction.
                                           pu4ErrorCode,
                                           lv.nmhTestv2FwlAclFragAction.
                                           i4FwlAclIfIndex,
                                           lv.nmhTestv2FwlAclFragAction.
                                           pFwlAclAclName,
                                           lv.nmhTestv2FwlAclFragAction.
                                           i4FwlAclDirection,
                                           lv.nmhTestv2FwlAclFragAction.
                                           i4TestValFwlAclFragAction);
            lv.nmhTestv2FwlAclFragAction.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlAclFragAction *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlAclFragAction;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlAclFragAction)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_ACL_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlAclRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlAclRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlAclRowStatus),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlAclRowStatus.rval =
                nmhTestv2FwlAclRowStatus (lv.nmhTestv2FwlAclRowStatus.
                                          pu4ErrorCode,
                                          lv.nmhTestv2FwlAclRowStatus.
                                          i4FwlAclIfIndex,
                                          lv.nmhTestv2FwlAclRowStatus.
                                          pFwlAclAclName,
                                          lv.nmhTestv2FwlAclRowStatus.
                                          i4FwlAclDirection,
                                          lv.nmhTestv2FwlAclRowStatus.
                                          i4TestValFwlAclRowStatus);
            lv.nmhTestv2FwlAclRowStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlAclRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlAclRowStatus;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlAclRowStatus)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_FWL_DEFN_IF_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhValidateIndexInstanceFwlDefnIfTable;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhValidateIndexInstanceFwlDefnIfTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlDefnIfTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhValidateIndexInstanceFwlDefnIfTable.rval =
                nmhValidateIndexInstanceFwlDefnIfTable (lv.
                                                        nmhValidateIndexInstanceFwlDefnIfTable.
                                                        i4FwlIfIfIndex);
            lv.nmhValidateIndexInstanceFwlDefnIfTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhValidateIndexInstanceFwlDefnIfTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhValidateIndexInstanceFwlDefnIfTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlDefnIfTable)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_FWL_DEFN_IF_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexFwlDefnIfTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFirstIndexFwlDefnIfTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlDefnIfTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFirstIndexFwlDefnIfTable.rval =
                nmhGetFirstIndexFwlDefnIfTable (lv.
                                                nmhGetFirstIndexFwlDefnIfTable.
                                                pi4FwlIfIfIndex);
            lv.nmhGetFirstIndexFwlDefnIfTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFirstIndexFwlDefnIfTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexFwlDefnIfTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlDefnIfTable)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_FWL_DEFN_IF_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexFwlDefnIfTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetNextIndexFwlDefnIfTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlDefnIfTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetNextIndexFwlDefnIfTable.rval =
                nmhGetNextIndexFwlDefnIfTable (lv.nmhGetNextIndexFwlDefnIfTable.
                                               i4FwlIfIfIndex,
                                               lv.nmhGetNextIndexFwlDefnIfTable.
                                               pi4NextFwlIfIfIndex);
            lv.nmhGetNextIndexFwlDefnIfTable.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetNextIndexFwlDefnIfTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexFwlDefnIfTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlDefnIfTable)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_IF_IF_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlIfIfType;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlIfIfType *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIfIfType),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlIfIfType.rval =
                nmhGetFwlIfIfType (lv.nmhGetFwlIfIfType.i4FwlIfIfIndex,
                                   lv.nmhGetFwlIfIfType.pi4RetValFwlIfIfType);
            lv.nmhGetFwlIfIfType.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlIfIfType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlIfIfType;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIfIfType)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_IF_IP_OPTIONS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlIfIpOptions;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlIfIpOptions *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIfIpOptions),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlIfIpOptions.rval =
                nmhGetFwlIfIpOptions (lv.nmhGetFwlIfIpOptions.i4FwlIfIfIndex,
                                      lv.nmhGetFwlIfIpOptions.
                                      pi4RetValFwlIfIpOptions);
            lv.nmhGetFwlIfIpOptions.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlIfIpOptions *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlIfIpOptions;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIfIpOptions)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_IF_FRAGMENTS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlIfFragments;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlIfFragments *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIfFragments),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlIfFragments.rval =
                nmhGetFwlIfFragments (lv.nmhGetFwlIfFragments.i4FwlIfIfIndex,
                                      lv.nmhGetFwlIfFragments.
                                      pi4RetValFwlIfFragments);
            lv.nmhGetFwlIfFragments.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlIfFragments *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlIfFragments;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIfFragments)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_IF_FRAGMENT_SIZE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlIfFragmentSize;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlIfFragmentSize *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIfFragmentSize),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlIfFragmentSize.rval =
                nmhGetFwlIfFragmentSize (lv.nmhGetFwlIfFragmentSize.
                                         i4FwlIfIfIndex,
                                         lv.nmhGetFwlIfFragmentSize.
                                         pu4RetValFwlIfFragmentSize);
            lv.nmhGetFwlIfFragmentSize.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlIfFragmentSize *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlIfFragmentSize;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIfFragmentSize)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_GET_FWL_IF_I_C_M_P_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlIfICMPType;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlIfICMPType *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIfICMPType),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlIfICMPType.rval =
                nmhGetFwlIfICMPType (lv.nmhGetFwlIfICMPType.i4FwlIfIfIndex,
                                     lv.nmhGetFwlIfICMPType.
                                     pi4RetValFwlIfICMPType);
            lv.nmhGetFwlIfICMPType.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlIfICMPType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlIfICMPType;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIfICMPType)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_IF_I_C_M_P_CODE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlIfICMPCode;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlIfICMPCode *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIfICMPCode),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlIfICMPCode.rval =
                nmhGetFwlIfICMPCode (lv.nmhGetFwlIfICMPCode.i4FwlIfIfIndex,
                                     lv.nmhGetFwlIfICMPCode.
                                     pi4RetValFwlIfICMPCode);
            lv.nmhGetFwlIfICMPCode.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlIfICMPCode *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlIfICMPCode;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIfICMPCode)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_IF_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlIfRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlIfRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIfRowStatus),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlIfRowStatus.rval =
                nmhGetFwlIfRowStatus (lv.nmhGetFwlIfRowStatus.i4FwlIfIfIndex,
                                      lv.nmhGetFwlIfRowStatus.
                                      pi4RetValFwlIfRowStatus);
            lv.nmhGetFwlIfRowStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlIfRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlIfRowStatus;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIfRowStatus)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_IF_IF_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlIfIfType;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlIfIfType *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlIfIfType),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlIfIfType.rval =
                nmhSetFwlIfIfType (lv.nmhSetFwlIfIfType.i4FwlIfIfIndex,
                                   lv.nmhSetFwlIfIfType.i4SetValFwlIfIfType);
            lv.nmhSetFwlIfIfType.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlIfIfType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlIfIfType;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlIfIfType)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_IF_IP_OPTIONS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlIfIpOptions;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlIfIpOptions *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlIfIpOptions),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlIfIpOptions.rval =
                nmhSetFwlIfIpOptions (lv.nmhSetFwlIfIpOptions.i4FwlIfIfIndex,
                                      lv.nmhSetFwlIfIpOptions.
                                      i4SetValFwlIfIpOptions);
            lv.nmhSetFwlIfIpOptions.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlIfIpOptions *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlIfIpOptions;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlIfIpOptions)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_IF_FRAGMENTS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlIfFragments;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlIfFragments *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlIfFragments),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlIfFragments.rval =
                nmhSetFwlIfFragments (lv.nmhSetFwlIfFragments.i4FwlIfIfIndex,
                                      lv.nmhSetFwlIfFragments.
                                      i4SetValFwlIfFragments);
            lv.nmhSetFwlIfFragments.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlIfFragments *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlIfFragments;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlIfFragments)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_IF_I_C_M_P_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlIfICMPType;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlIfICMPType *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlIfICMPType),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlIfICMPType.rval =
                nmhSetFwlIfICMPType (lv.nmhSetFwlIfICMPType.i4FwlIfIfIndex,
                                     lv.nmhSetFwlIfICMPType.
                                     i4SetValFwlIfICMPType);
            lv.nmhSetFwlIfICMPType.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlIfICMPType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlIfICMPType;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlIfICMPType)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_IF_I_C_M_P_CODE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlIfICMPCode;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlIfICMPCode *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlIfICMPCode),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlIfICMPCode.rval =
                nmhSetFwlIfICMPCode (lv.nmhSetFwlIfICMPCode.i4FwlIfIfIndex,
                                     lv.nmhSetFwlIfICMPCode.
                                     i4SetValFwlIfICMPCode);
            lv.nmhSetFwlIfICMPCode.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlIfICMPCode *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlIfICMPCode;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlIfICMPCode)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_IF_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlIfRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlIfRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlIfRowStatus),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlIfRowStatus.rval =
                nmhSetFwlIfRowStatus (lv.nmhSetFwlIfRowStatus.i4FwlIfIfIndex,
                                      lv.nmhSetFwlIfRowStatus.
                                      i4SetValFwlIfRowStatus);
            lv.nmhSetFwlIfRowStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlIfRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlIfRowStatus;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlIfRowStatus)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_IF_IF_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlIfIfType;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlIfIfType *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlIfIfType),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlIfIfType.rval =
                nmhTestv2FwlIfIfType (lv.nmhTestv2FwlIfIfType.pu4ErrorCode,
                                      lv.nmhTestv2FwlIfIfType.i4FwlIfIfIndex,
                                      lv.nmhTestv2FwlIfIfType.
                                      i4TestValFwlIfIfType);
            lv.nmhTestv2FwlIfIfType.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlIfIfType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlIfIfType;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlIfIfType)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_IF_IP_OPTIONS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlIfIpOptions;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlIfIpOptions *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlIfIpOptions),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlIfIpOptions.rval =
                nmhTestv2FwlIfIpOptions (lv.nmhTestv2FwlIfIpOptions.
                                         pu4ErrorCode,
                                         lv.nmhTestv2FwlIfIpOptions.
                                         i4FwlIfIfIndex,
                                         lv.nmhTestv2FwlIfIpOptions.
                                         i4TestValFwlIfIpOptions);
            lv.nmhTestv2FwlIfIpOptions.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlIfIpOptions *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlIfIpOptions;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlIfIpOptions)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_IF_FRAGMENTS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlIfFragments;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlIfFragments *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlIfFragments),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlIfFragments.rval =
                nmhTestv2FwlIfFragments (lv.nmhTestv2FwlIfFragments.
                                         pu4ErrorCode,
                                         lv.nmhTestv2FwlIfFragments.
                                         i4FwlIfIfIndex,
                                         lv.nmhTestv2FwlIfFragments.
                                         i4TestValFwlIfFragments);
            lv.nmhTestv2FwlIfFragments.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlIfFragments *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlIfFragments;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlIfFragments)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_IF_I_C_M_P_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlIfICMPType;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlIfICMPType *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlIfICMPType),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlIfICMPType.rval =
                nmhTestv2FwlIfICMPType (lv.nmhTestv2FwlIfICMPType.pu4ErrorCode,
                                        lv.nmhTestv2FwlIfICMPType.
                                        i4FwlIfIfIndex,
                                        lv.nmhTestv2FwlIfICMPType.
                                        i4TestValFwlIfICMPType);
            lv.nmhTestv2FwlIfICMPType.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlIfICMPType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlIfICMPType;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlIfICMPType)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_IF_I_C_M_P_CODE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlIfICMPCode;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlIfICMPCode *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlIfICMPCode),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlIfICMPCode.rval =
                nmhTestv2FwlIfICMPCode (lv.nmhTestv2FwlIfICMPCode.pu4ErrorCode,
                                        lv.nmhTestv2FwlIfICMPCode.
                                        i4FwlIfIfIndex,
                                        lv.nmhTestv2FwlIfICMPCode.
                                        i4TestValFwlIfICMPCode);
            lv.nmhTestv2FwlIfICMPCode.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlIfICMPCode *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlIfICMPCode;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlIfICMPCode)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_IF_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlIfRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlIfRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlIfRowStatus),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlIfRowStatus.rval =
                nmhTestv2FwlIfRowStatus (lv.nmhTestv2FwlIfRowStatus.
                                         pu4ErrorCode,
                                         lv.nmhTestv2FwlIfRowStatus.
                                         i4FwlIfIfIndex,
                                         lv.nmhTestv2FwlIfRowStatus.
                                         i4TestValFwlIfRowStatus);
            lv.nmhTestv2FwlIfRowStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlIfRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlIfRowStatus;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlIfRowStatus)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_FWL_DEFN_DMZ_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhValidateIndexInstanceFwlDefnDmzTable;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhValidateIndexInstanceFwlDefnDmzTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlDefnDmzTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhValidateIndexInstanceFwlDefnDmzTable.rval =
                nmhValidateIndexInstanceFwlDefnDmzTable (lv.
                                                         nmhValidateIndexInstanceFwlDefnDmzTable.
                                                         u4FwlDmzIpIndex);
            lv.nmhValidateIndexInstanceFwlDefnDmzTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhValidateIndexInstanceFwlDefnDmzTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhValidateIndexInstanceFwlDefnDmzTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlDefnDmzTable)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_FWL_DEFN_DMZ_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexFwlDefnDmzTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFirstIndexFwlDefnDmzTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlDefnDmzTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFirstIndexFwlDefnDmzTable.rval =
                nmhGetFirstIndexFwlDefnDmzTable (lv.
                                                 nmhGetFirstIndexFwlDefnDmzTable.
                                                 pu4FwlDmzIpIndex);
            lv.nmhGetFirstIndexFwlDefnDmzTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFirstIndexFwlDefnDmzTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexFwlDefnDmzTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlDefnDmzTable)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_FWL_DEFN_DMZ_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexFwlDefnDmzTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetNextIndexFwlDefnDmzTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlDefnDmzTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetNextIndexFwlDefnDmzTable.rval =
                nmhGetNextIndexFwlDefnDmzTable (lv.
                                                nmhGetNextIndexFwlDefnDmzTable.
                                                u4FwlDmzIpIndex,
                                                lv.
                                                nmhGetNextIndexFwlDefnDmzTable.
                                                pu4NextFwlDmzIpIndex);
            lv.nmhGetNextIndexFwlDefnDmzTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetNextIndexFwlDefnDmzTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexFwlDefnDmzTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlDefnDmzTable)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_DMZ_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlDmzRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlDmzRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlDmzRowStatus),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlDmzRowStatus.rval =
                nmhGetFwlDmzRowStatus (lv.nmhGetFwlDmzRowStatus.u4FwlDmzIpIndex,
                                       lv.nmhGetFwlDmzRowStatus.
                                       pi4RetValFwlDmzRowStatus);
            lv.nmhGetFwlDmzRowStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlDmzRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlDmzRowStatus;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlDmzRowStatus)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_DMZ_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlDmzRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlDmzRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlDmzRowStatus),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlDmzRowStatus.rval =
                nmhSetFwlDmzRowStatus (lv.nmhSetFwlDmzRowStatus.u4FwlDmzIpIndex,
                                       lv.nmhSetFwlDmzRowStatus.
                                       i4SetValFwlDmzRowStatus);
            lv.nmhSetFwlDmzRowStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlDmzRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlDmzRowStatus;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlDmzRowStatus)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_DMZ_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlDmzRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlDmzRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlDmzRowStatus),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlDmzRowStatus.rval =
                nmhTestv2FwlDmzRowStatus (lv.nmhTestv2FwlDmzRowStatus.
                                          pu4ErrorCode,
                                          lv.nmhTestv2FwlDmzRowStatus.
                                          u4FwlDmzIpIndex,
                                          lv.nmhTestv2FwlDmzRowStatus.
                                          i4TestValFwlDmzRowStatus);
            lv.nmhTestv2FwlDmzRowStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlDmzRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlDmzRowStatus;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlDmzRowStatus)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_FWL_URL_FILTER_TABLE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhValidateIndexInstanceFwlUrlFilterTable;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhValidateIndexInstanceFwlUrlFilterTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlUrlFilterTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhValidateIndexInstanceFwlUrlFilterTable.rval =
                nmhValidateIndexInstanceFwlUrlFilterTable (lv.
                                                           nmhValidateIndexInstanceFwlUrlFilterTable.
                                                           pFwlUrlString);
            lv.nmhValidateIndexInstanceFwlUrlFilterTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhValidateIndexInstanceFwlUrlFilterTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhValidateIndexInstanceFwlUrlFilterTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlUrlFilterTable))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_FWL_URL_FILTER_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexFwlUrlFilterTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFirstIndexFwlUrlFilterTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlUrlFilterTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFirstIndexFwlUrlFilterTable.rval =
                nmhGetFirstIndexFwlUrlFilterTable (lv.
                                                   nmhGetFirstIndexFwlUrlFilterTable.
                                                   pFwlUrlString);
            lv.nmhGetFirstIndexFwlUrlFilterTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFirstIndexFwlUrlFilterTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexFwlUrlFilterTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlUrlFilterTable)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_FWL_URL_FILTER_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexFwlUrlFilterTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetNextIndexFwlUrlFilterTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlUrlFilterTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetNextIndexFwlUrlFilterTable.rval =
                nmhGetNextIndexFwlUrlFilterTable (lv.
                                                  nmhGetNextIndexFwlUrlFilterTable.
                                                  pFwlUrlString,
                                                  lv.
                                                  nmhGetNextIndexFwlUrlFilterTable.
                                                  pNextFwlUrlString);
            lv.nmhGetNextIndexFwlUrlFilterTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetNextIndexFwlUrlFilterTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexFwlUrlFilterTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlUrlFilterTable)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_URL_HIT_COUNT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlUrlHitCount;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlUrlHitCount *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlUrlHitCount),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlUrlHitCount.rval =
                nmhGetFwlUrlHitCount (lv.nmhGetFwlUrlHitCount.pFwlUrlString,
                                      lv.nmhGetFwlUrlHitCount.
                                      pu4RetValFwlUrlHitCount);
            lv.nmhGetFwlUrlHitCount.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlUrlHitCount *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlUrlHitCount;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlUrlHitCount)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_URL_FILTER_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlUrlFilterRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlUrlFilterRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlUrlFilterRowStatus),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlUrlFilterRowStatus.rval =
                nmhGetFwlUrlFilterRowStatus (lv.nmhGetFwlUrlFilterRowStatus.
                                             pFwlUrlString,
                                             lv.nmhGetFwlUrlFilterRowStatus.
                                             pi4RetValFwlUrlFilterRowStatus);
            lv.nmhGetFwlUrlFilterRowStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlUrlFilterRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlUrlFilterRowStatus;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlUrlFilterRowStatus)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_URL_FILTER_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlUrlFilterRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlUrlFilterRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlUrlFilterRowStatus),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlUrlFilterRowStatus.rval =
                nmhSetFwlUrlFilterRowStatus (lv.nmhSetFwlUrlFilterRowStatus.
                                             pFwlUrlString,
                                             lv.nmhSetFwlUrlFilterRowStatus.
                                             i4SetValFwlUrlFilterRowStatus);
            lv.nmhSetFwlUrlFilterRowStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlUrlFilterRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlUrlFilterRowStatus;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlUrlFilterRowStatus)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_URL_FILTER_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlUrlFilterRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlUrlFilterRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlUrlFilterRowStatus),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlUrlFilterRowStatus.rval =
                nmhTestv2FwlUrlFilterRowStatus (lv.
                                                nmhTestv2FwlUrlFilterRowStatus.
                                                pu4ErrorCode,
                                                lv.
                                                nmhTestv2FwlUrlFilterRowStatus.
                                                pFwlUrlString,
                                                lv.
                                                nmhTestv2FwlUrlFilterRowStatus.
                                                i4TestValFwlUrlFilterRowStatus);
            lv.nmhTestv2FwlUrlFilterRowStatus.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlUrlFilterRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlUrlFilterRowStatus;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlUrlFilterRowStatus)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_INSPECTED_PACKETS_COUNT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatInspectedPacketsCount;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStatInspectedPacketsCount *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatInspectedPacketsCount),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatInspectedPacketsCount.rval =
                nmhGetFwlStatInspectedPacketsCount (lv.
                                                    nmhGetFwlStatInspectedPacketsCount.
                                                    pu4RetValFwlStatInspectedPacketsCount);
            lv.nmhGetFwlStatInspectedPacketsCount.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatInspectedPacketsCount *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatInspectedPacketsCount;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatInspectedPacketsCount))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_TOTAL_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatTotalPacketsDenied;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStatTotalPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatTotalPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatTotalPacketsDenied.rval =
                nmhGetFwlStatTotalPacketsDenied (lv.
                                                 nmhGetFwlStatTotalPacketsDenied.
                                                 pu4RetValFwlStatTotalPacketsDenied);
            lv.nmhGetFwlStatTotalPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlStatTotalPacketsDenied *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatTotalPacketsDenied;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatTotalPacketsDenied)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_TOTAL_PACKETS_ACCEPTED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatTotalPacketsAccepted;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStatTotalPacketsAccepted *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatTotalPacketsAccepted),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatTotalPacketsAccepted.rval =
                nmhGetFwlStatTotalPacketsAccepted (lv.
                                                   nmhGetFwlStatTotalPacketsAccepted.
                                                   pu4RetValFwlStatTotalPacketsAccepted);
            lv.nmhGetFwlStatTotalPacketsAccepted.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlStatTotalPacketsAccepted *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatTotalPacketsAccepted;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatTotalPacketsAccepted)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_TOTAL_ICMP_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatTotalIcmpPacketsDenied;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatTotalIcmpPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatTotalIcmpPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatTotalIcmpPacketsDenied.rval =
                nmhGetFwlStatTotalIcmpPacketsDenied (lv.
                                                     nmhGetFwlStatTotalIcmpPacketsDenied.
                                                     pu4RetValFwlStatTotalIcmpPacketsDenied);
            lv.nmhGetFwlStatTotalIcmpPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatTotalIcmpPacketsDenied *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatTotalIcmpPacketsDenied;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatTotalIcmpPacketsDenied))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_TOTAL_SYN_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatTotalSynPacketsDenied;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStatTotalSynPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatTotalSynPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatTotalSynPacketsDenied.rval =
                nmhGetFwlStatTotalSynPacketsDenied (lv.
                                                    nmhGetFwlStatTotalSynPacketsDenied.
                                                    pu4RetValFwlStatTotalSynPacketsDenied);
            lv.nmhGetFwlStatTotalSynPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatTotalSynPacketsDenied *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatTotalSynPacketsDenied;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatTotalSynPacketsDenied))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_TOTAL_IP_SPOOFED_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatTotalIpSpoofedPacketsDenied;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatTotalIpSpoofedPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatTotalIpSpoofedPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatTotalIpSpoofedPacketsDenied.rval =
                nmhGetFwlStatTotalIpSpoofedPacketsDenied (lv.
                                                          nmhGetFwlStatTotalIpSpoofedPacketsDenied.
                                                          pu4RetValFwlStatTotalIpSpoofedPacketsDenied);
            lv.nmhGetFwlStatTotalIpSpoofedPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatTotalIpSpoofedPacketsDenied *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatTotalIpSpoofedPacketsDenied;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatTotalIpSpoofedPacketsDenied)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_TOTAL_SRC_ROUTE_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatTotalSrcRoutePacketsDenied;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatTotalSrcRoutePacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatTotalSrcRoutePacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatTotalSrcRoutePacketsDenied.rval =
                nmhGetFwlStatTotalSrcRoutePacketsDenied (lv.
                                                         nmhGetFwlStatTotalSrcRoutePacketsDenied.
                                                         pu4RetValFwlStatTotalSrcRoutePacketsDenied);
            lv.nmhGetFwlStatTotalSrcRoutePacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatTotalSrcRoutePacketsDenied *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatTotalSrcRoutePacketsDenied;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatTotalSrcRoutePacketsDenied)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_TOTAL_TINY_FRAGMENT_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhGetFwlStatTotalTinyFragmentPacketsDenied;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatTotalTinyFragmentPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatTotalTinyFragmentPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatTotalTinyFragmentPacketsDenied.rval =
                nmhGetFwlStatTotalTinyFragmentPacketsDenied (lv.
                                                             nmhGetFwlStatTotalTinyFragmentPacketsDenied.
                                                             pu4RetValFwlStatTotalTinyFragmentPacketsDenied);
            lv.nmhGetFwlStatTotalTinyFragmentPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatTotalTinyFragmentPacketsDenied *) p;
            OsixKerUseInfo.pSrc =
                &lv.nmhGetFwlStatTotalTinyFragmentPacketsDenied;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatTotalTinyFragmentPacketsDenied))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_TOTAL_FRAGMENTED_PACKETS_DENIED:

        {
            OsixKerUseInfo.pDest =
                &lv.nmhGetFwlStatTotalFragmentedPacketsDenied;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatTotalFragmentedPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatTotalFragmentedPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatTotalFragmentedPacketsDenied.rval =
                nmhGetFwlStatTotalFragmentedPacketsDenied (lv.
                                                           nmhGetFwlStatTotalFragmentedPacketsDenied.
                                                           pu4RetValFwlStatTotalFragmentedPacketsDenied);
            lv.nmhGetFwlStatTotalFragmentedPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatTotalFragmentedPacketsDenied *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatTotalFragmentedPacketsDenied;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatTotalFragmentedPacketsDenied))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_TOTAL_IP_OPTION_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatTotalIpOptionPacketsDenied;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatTotalIpOptionPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatTotalIpOptionPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatTotalIpOptionPacketsDenied.rval =
                nmhGetFwlStatTotalIpOptionPacketsDenied (lv.
                                                         nmhGetFwlStatTotalIpOptionPacketsDenied.
                                                         pu4RetValFwlStatTotalIpOptionPacketsDenied);
            lv.nmhGetFwlStatTotalIpOptionPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatTotalIpOptionPacketsDenied *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatTotalIpOptionPacketsDenied;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatTotalIpOptionPacketsDenied)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_MEMORY_ALLOCATION_FAIL_COUNT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatMemoryAllocationFailCount;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatMemoryAllocationFailCount *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatMemoryAllocationFailCount),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatMemoryAllocationFailCount.rval =
                nmhGetFwlStatMemoryAllocationFailCount (lv.
                                                        nmhGetFwlStatMemoryAllocationFailCount.
                                                        pu4RetValFwlStatMemoryAllocationFailCount);
            lv.nmhGetFwlStatMemoryAllocationFailCount.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatMemoryAllocationFailCount *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatMemoryAllocationFailCount;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatMemoryAllocationFailCount)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_CLEAR:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatClear;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStatClear *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatClear),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatClear.rval =
                nmhGetFwlStatClear (lv.nmhGetFwlStatClear.
                                    pi4RetValFwlStatClear);
            lv.nmhGetFwlStatClear.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlStatClear *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatClear;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatClear)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_TRAP_THRESHOLD:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlTrapThreshold;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlTrapThreshold *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlTrapThreshold),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlTrapThreshold.rval =
                nmhGetFwlTrapThreshold (lv.nmhGetFwlTrapThreshold.
                                        pi4RetValFwlTrapThreshold);
            lv.nmhGetFwlTrapThreshold.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlTrapThreshold *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlTrapThreshold;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlTrapThreshold)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_STAT_CLEAR:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlStatClear;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlStatClear *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlStatClear),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlStatClear.rval =
                nmhSetFwlStatClear (lv.nmhSetFwlStatClear.i4SetValFwlStatClear);
            lv.nmhSetFwlStatClear.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlStatClear *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlStatClear;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlStatClear)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_TRAP_THRESHOLD:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlTrapThreshold;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlTrapThreshold *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlTrapThreshold),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlTrapThreshold.rval =
                nmhSetFwlTrapThreshold (lv.nmhSetFwlTrapThreshold.
                                        i4SetValFwlTrapThreshold);
            lv.nmhSetFwlTrapThreshold.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlTrapThreshold *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlTrapThreshold;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlTrapThreshold)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_STAT_CLEAR:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlStatClear;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlStatClear *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlStatClear),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlStatClear.rval =
                nmhTestv2FwlStatClear (lv.nmhTestv2FwlStatClear.pu4ErrorCode,
                                       lv.nmhTestv2FwlStatClear.
                                       i4TestValFwlStatClear);
            lv.nmhTestv2FwlStatClear.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlStatClear *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlStatClear;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlStatClear)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_TRAP_THRESHOLD:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlTrapThreshold;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlTrapThreshold *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlTrapThreshold),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlTrapThreshold.rval =
                nmhTestv2FwlTrapThreshold (lv.nmhTestv2FwlTrapThreshold.
                                           pu4ErrorCode,
                                           lv.nmhTestv2FwlTrapThreshold.
                                           i4TestValFwlTrapThreshold);
            lv.nmhTestv2FwlTrapThreshold.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlTrapThreshold *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlTrapThreshold;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlTrapThreshold)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_FWL_STAT_IF_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhValidateIndexInstanceFwlStatIfTable;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhValidateIndexInstanceFwlStatIfTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlStatIfTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhValidateIndexInstanceFwlStatIfTable.rval =
                nmhValidateIndexInstanceFwlStatIfTable (lv.
                                                        nmhValidateIndexInstanceFwlStatIfTable.
                                                        i4FwlStatIfIfIndex);
            lv.nmhValidateIndexInstanceFwlStatIfTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhValidateIndexInstanceFwlStatIfTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhValidateIndexInstanceFwlStatIfTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlStatIfTable)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_FWL_STAT_IF_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexFwlStatIfTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFirstIndexFwlStatIfTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlStatIfTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFirstIndexFwlStatIfTable.rval =
                nmhGetFirstIndexFwlStatIfTable (lv.
                                                nmhGetFirstIndexFwlStatIfTable.
                                                pi4FwlStatIfIfIndex);
            lv.nmhGetFirstIndexFwlStatIfTable.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFirstIndexFwlStatIfTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexFwlStatIfTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlStatIfTable)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_FWL_STAT_IF_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexFwlStatIfTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetNextIndexFwlStatIfTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlStatIfTable),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetNextIndexFwlStatIfTable.rval =
                nmhGetNextIndexFwlStatIfTable (lv.nmhGetNextIndexFwlStatIfTable.
                                               i4FwlStatIfIfIndex,
                                               lv.nmhGetNextIndexFwlStatIfTable.
                                               pi4NextFwlStatIfIfIndex);
            lv.nmhGetNextIndexFwlStatIfTable.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetNextIndexFwlStatIfTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexFwlStatIfTable;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlStatIfTable)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_IF_FILTER_COUNT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIfFilterCount;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStatIfFilterCount *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfFilterCount),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatIfFilterCount.rval =
                nmhGetFwlStatIfFilterCount (lv.nmhGetFwlStatIfFilterCount.
                                            i4FwlStatIfIfIndex,
                                            lv.nmhGetFwlStatIfFilterCount.
                                            pi4RetValFwlStatIfFilterCount);
            lv.nmhGetFwlStatIfFilterCount.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlStatIfFilterCount *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIfFilterCount;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfFilterCount)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_IF_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIfPacketsDenied;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStatIfPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatIfPacketsDenied.rval =
                nmhGetFwlStatIfPacketsDenied (lv.nmhGetFwlStatIfPacketsDenied.
                                              i4FwlStatIfIfIndex,
                                              lv.nmhGetFwlStatIfPacketsDenied.
                                              pu4RetValFwlStatIfPacketsDenied);
            lv.nmhGetFwlStatIfPacketsDenied.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlStatIfPacketsDenied *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIfPacketsDenied;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfPacketsDenied)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_IF_PACKETS_ACCEPTED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIfPacketsAccepted;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStatIfPacketsAccepted *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfPacketsAccepted),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatIfPacketsAccepted.rval =
                nmhGetFwlStatIfPacketsAccepted (lv.
                                                nmhGetFwlStatIfPacketsAccepted.
                                                i4FwlStatIfIfIndex,
                                                lv.
                                                nmhGetFwlStatIfPacketsAccepted.
                                                pu4RetValFwlStatIfPacketsAccepted);
            lv.nmhGetFwlStatIfPacketsAccepted.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlStatIfPacketsAccepted *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIfPacketsAccepted;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfPacketsAccepted)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_IF_SYN_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIfSynPacketsDenied;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStatIfSynPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfSynPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatIfSynPacketsDenied.rval =
                nmhGetFwlStatIfSynPacketsDenied (lv.
                                                 nmhGetFwlStatIfSynPacketsDenied.
                                                 i4FwlStatIfIfIndex,
                                                 lv.
                                                 nmhGetFwlStatIfSynPacketsDenied.
                                                 pu4RetValFwlStatIfSynPacketsDenied);
            lv.nmhGetFwlStatIfSynPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlStatIfSynPacketsDenied *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIfSynPacketsDenied;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfSynPacketsDenied)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_IF_ICMP_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIfIcmpPacketsDenied;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStatIfIcmpPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfIcmpPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatIfIcmpPacketsDenied.rval =
                nmhGetFwlStatIfIcmpPacketsDenied (lv.
                                                  nmhGetFwlStatIfIcmpPacketsDenied.
                                                  i4FwlStatIfIfIndex,
                                                  lv.
                                                  nmhGetFwlStatIfIcmpPacketsDenied.
                                                  pu4RetValFwlStatIfIcmpPacketsDenied);
            lv.nmhGetFwlStatIfIcmpPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlStatIfIcmpPacketsDenied *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIfIcmpPacketsDenied;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfIcmpPacketsDenied)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_IF_IP_SPOOFED_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIfIpSpoofedPacketsDenied;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatIfIpSpoofedPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatIfIpSpoofedPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatIfIpSpoofedPacketsDenied.rval =
                nmhGetFwlStatIfIpSpoofedPacketsDenied (lv.
                                                       nmhGetFwlStatIfIpSpoofedPacketsDenied.
                                                       i4FwlStatIfIfIndex,
                                                       lv.
                                                       nmhGetFwlStatIfIpSpoofedPacketsDenied.
                                                       pu4RetValFwlStatIfIpSpoofedPacketsDenied);
            lv.nmhGetFwlStatIfIpSpoofedPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatIfIpSpoofedPacketsDenied *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIfIpSpoofedPacketsDenied;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatIfIpSpoofedPacketsDenied)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_IF_SRC_ROUTE_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIfSrcRoutePacketsDenied;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatIfSrcRoutePacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfSrcRoutePacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatIfSrcRoutePacketsDenied.rval =
                nmhGetFwlStatIfSrcRoutePacketsDenied (lv.
                                                      nmhGetFwlStatIfSrcRoutePacketsDenied.
                                                      i4FwlStatIfIfIndex,
                                                      lv.
                                                      nmhGetFwlStatIfSrcRoutePacketsDenied.
                                                      pu4RetValFwlStatIfSrcRoutePacketsDenied);
            lv.nmhGetFwlStatIfSrcRoutePacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatIfSrcRoutePacketsDenied *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIfSrcRoutePacketsDenied;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfSrcRoutePacketsDenied))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_IF_TINY_FRAGMENT_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIfTinyFragmentPacketsDenied;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatIfTinyFragmentPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatIfTinyFragmentPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatIfTinyFragmentPacketsDenied.rval =
                nmhGetFwlStatIfTinyFragmentPacketsDenied (lv.
                                                          nmhGetFwlStatIfTinyFragmentPacketsDenied.
                                                          i4FwlStatIfIfIndex,
                                                          lv.
                                                          nmhGetFwlStatIfTinyFragmentPacketsDenied.
                                                          pu4RetValFwlStatIfTinyFragmentPacketsDenied);
            lv.nmhGetFwlStatIfTinyFragmentPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatIfTinyFragmentPacketsDenied *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIfTinyFragmentPacketsDenied;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatIfTinyFragmentPacketsDenied)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_IF_FRAGMENT_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIfFragmentPacketsDenied;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatIfFragmentPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfFragmentPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatIfFragmentPacketsDenied.rval =
                nmhGetFwlStatIfFragmentPacketsDenied (lv.
                                                      nmhGetFwlStatIfFragmentPacketsDenied.
                                                      i4FwlStatIfIfIndex,
                                                      lv.
                                                      nmhGetFwlStatIfFragmentPacketsDenied.
                                                      pu4RetValFwlStatIfFragmentPacketsDenied);
            lv.nmhGetFwlStatIfFragmentPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatIfFragmentPacketsDenied *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIfFragmentPacketsDenied;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfFragmentPacketsDenied))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_IF_IP_OPTION_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIfIpOptionPacketsDenied;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatIfIpOptionPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfIpOptionPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatIfIpOptionPacketsDenied.rval =
                nmhGetFwlStatIfIpOptionPacketsDenied (lv.
                                                      nmhGetFwlStatIfIpOptionPacketsDenied.
                                                      i4FwlStatIfIfIndex,
                                                      lv.
                                                      nmhGetFwlStatIfIpOptionPacketsDenied.
                                                      pu4RetValFwlStatIfIpOptionPacketsDenied);
            lv.nmhGetFwlStatIfIpOptionPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatIfIpOptionPacketsDenied *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIfIpOptionPacketsDenied;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfIpOptionPacketsDenied))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_IF_CLEAR:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIfClear;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStatIfClear *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfClear),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatIfClear.rval =
                nmhGetFwlStatIfClear (lv.nmhGetFwlStatIfClear.
                                      i4FwlStatIfIfIndex,
                                      lv.nmhGetFwlStatIfClear.
                                      pi4RetValFwlStatIfClear);
            lv.nmhGetFwlStatIfClear.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlStatIfClear *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIfClear;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfClear)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_IF_TRAP_THRESHOLD:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlIfTrapThreshold;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlIfTrapThreshold *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIfTrapThreshold),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlIfTrapThreshold.rval =
                nmhGetFwlIfTrapThreshold (lv.nmhGetFwlIfTrapThreshold.
                                          i4FwlStatIfIfIndex,
                                          lv.nmhGetFwlIfTrapThreshold.
                                          pi4RetValFwlIfTrapThreshold);
            lv.nmhGetFwlIfTrapThreshold.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlIfTrapThreshold *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlIfTrapThreshold;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIfTrapThreshold)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_STAT_IF_CLEAR:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlStatIfClear;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlStatIfClear *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlStatIfClear),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlStatIfClear.rval =
                nmhSetFwlStatIfClear (lv.nmhSetFwlStatIfClear.
                                      i4FwlStatIfIfIndex,
                                      lv.nmhSetFwlStatIfClear.
                                      i4SetValFwlStatIfClear);
            lv.nmhSetFwlStatIfClear.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlStatIfClear *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlStatIfClear;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlStatIfClear)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_IF_TRAP_THRESHOLD:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlIfTrapThreshold;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlIfTrapThreshold *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlIfTrapThreshold),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlIfTrapThreshold.rval =
                nmhSetFwlIfTrapThreshold (lv.nmhSetFwlIfTrapThreshold.
                                          i4FwlStatIfIfIndex,
                                          lv.nmhSetFwlIfTrapThreshold.
                                          i4SetValFwlIfTrapThreshold);
            lv.nmhSetFwlIfTrapThreshold.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlIfTrapThreshold *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlIfTrapThreshold;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlIfTrapThreshold)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_STAT_IF_CLEAR:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlStatIfClear;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlStatIfClear *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlStatIfClear),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlStatIfClear.rval =
                nmhTestv2FwlStatIfClear (lv.nmhTestv2FwlStatIfClear.
                                         pu4ErrorCode,
                                         lv.nmhTestv2FwlStatIfClear.
                                         i4FwlStatIfIfIndex,
                                         lv.nmhTestv2FwlStatIfClear.
                                         i4TestValFwlStatIfClear);
            lv.nmhTestv2FwlStatIfClear.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlStatIfClear *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlStatIfClear;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlStatIfClear)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_IF_TRAP_THRESHOLD:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlIfTrapThreshold;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlIfTrapThreshold *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlIfTrapThreshold),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlIfTrapThreshold.rval =
                nmhTestv2FwlIfTrapThreshold (lv.nmhTestv2FwlIfTrapThreshold.
                                             pu4ErrorCode,
                                             lv.nmhTestv2FwlIfTrapThreshold.
                                             i4FwlStatIfIfIndex,
                                             lv.nmhTestv2FwlIfTrapThreshold.
                                             i4TestValFwlIfTrapThreshold);
            lv.nmhTestv2FwlIfTrapThreshold.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlIfTrapThreshold *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlIfTrapThreshold;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlIfTrapThreshold)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_TRAP_MEM_FAIL_MESSAGE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlTrapMemFailMessage;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlTrapMemFailMessage *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlTrapMemFailMessage),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlTrapMemFailMessage.rval =
                nmhGetFwlTrapMemFailMessage (lv.nmhGetFwlTrapMemFailMessage.
                                             pRetValFwlTrapMemFailMessage);
            lv.nmhGetFwlTrapMemFailMessage.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */

            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlTrapMemFailMessage *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlTrapMemFailMessage;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlTrapMemFailMessage)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_TRAP_ATTACK_MESSAGE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlTrapAttackMessage;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlTrapAttackMessage *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlTrapAttackMessage),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlTrapAttackMessage.rval =
                nmhGetFwlTrapAttackMessage (lv.nmhGetFwlTrapAttackMessage.
                                            pRetValFwlTrapAttackMessage);
            lv.nmhGetFwlTrapAttackMessage.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlTrapAttackMessage *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlTrapAttackMessage;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlTrapAttackMessage)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_TRAP_MEM_FAIL_MESSAGE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlTrapMemFailMessage;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlTrapMemFailMessage *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlTrapMemFailMessage),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlTrapMemFailMessage.rval =
                nmhSetFwlTrapMemFailMessage (lv.nmhSetFwlTrapMemFailMessage.
                                             pSetValFwlTrapMemFailMessage);
            lv.nmhSetFwlTrapMemFailMessage.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlTrapMemFailMessage *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlTrapMemFailMessage;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlTrapMemFailMessage)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_TRAP_ATTACK_MESSAGE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlTrapAttackMessage;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlTrapAttackMessage *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlTrapAttackMessage),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlTrapAttackMessage.rval =
                nmhSetFwlTrapAttackMessage (lv.nmhSetFwlTrapAttackMessage.
                                            pSetValFwlTrapAttackMessage);
            lv.nmhSetFwlTrapAttackMessage.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlTrapAttackMessage *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlTrapAttackMessage;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlTrapAttackMessage)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_TRAP_MEM_FAIL_MESSAGE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlTrapMemFailMessage;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlTrapMemFailMessage *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlTrapMemFailMessage),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlTrapMemFailMessage.rval =
                nmhTestv2FwlTrapMemFailMessage (lv.
                                                nmhTestv2FwlTrapMemFailMessage.
                                                pu4ErrorCode,
                                                lv.
                                                nmhTestv2FwlTrapMemFailMessage.
                                                pTestValFwlTrapMemFailMessage);
            lv.nmhTestv2FwlTrapMemFailMessage.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlTrapMemFailMessage *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlTrapMemFailMessage;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlTrapMemFailMessage)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_TRAP_ATTACK_MESSAGE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlTrapAttackMessage;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlTrapAttackMessage *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlTrapAttackMessage),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlTrapAttackMessage.rval =
                nmhTestv2FwlTrapAttackMessage (lv.nmhTestv2FwlTrapAttackMessage.
                                               pu4ErrorCode,
                                               lv.nmhTestv2FwlTrapAttackMessage.
                                               pTestValFwlTrapAttackMessage);
            lv.nmhTestv2FwlTrapAttackMessage.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlTrapAttackMessage *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlTrapAttackMessage;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlTrapAttackMessage)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_LOG_TRIGGER:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlLogTrigger;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlLogTrigger *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlLogTrigger),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlLogTrigger.rval =
                nmhSetFwlLogTrigger (lv.nmhSetFwlLogTrigger.u4Interface,
                                     lv.nmhSetFwlLogTrigger.u1LogTrigger,
                                     lv.nmhSetFwlLogTrigger.pu1AclName,
                                     lv.nmhSetFwlLogTrigger.u4Direction);
            lv.nmhSetFwlLogTrigger.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlLogTrigger *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlLogTrigger;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlLogTrigger)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_FRAG_ACTION:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlFragAction;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlFragAction *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFragAction),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlFragAction.rval =
                nmhSetFwlFragAction (lv.nmhSetFwlFragAction.u4Interface,
                                     lv.nmhSetFwlFragAction.u1FragAction,
                                     lv.nmhSetFwlFragAction.pu1AclName,
                                     lv.nmhSetFwlFragAction.u4Direction);
            lv.nmhSetFwlFragAction.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlFragAction *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlFragAction;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFragAction)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_IF_FRAGMENT_SIZE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlIfFragmentSize;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlIfFragmentSize *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlIfFragmentSize),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlIfFragmentSize.rval =
                nmhSetFwlIfFragmentSize (lv.nmhSetFwlIfFragmentSize.
                                         i4FwlIfIfIndex,
                                         lv.nmhSetFwlIfFragmentSize.
                                         u4SetValFwlIfFragmentSize);
            lv.nmhSetFwlIfFragmentSize.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlIfFragmentSize *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlIfFragmentSize;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlIfFragmentSize)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_GLOBAL_NET_BIOS_FILTERING:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlGlobalNetBiosFiltering;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlGlobalNetBiosFiltering *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalNetBiosFiltering),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlGlobalNetBiosFiltering.rval =
                nmhSetFwlGlobalNetBiosFiltering (lv.
                                                 nmhSetFwlGlobalNetBiosFiltering.
                                                 i4SetValFwlGlobalNetBiosFiltering);
            lv.nmhSetFwlGlobalNetBiosFiltering.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlGlobalNetBiosFiltering *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlGlobalNetBiosFiltering;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalNetBiosFiltering)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_IF_FRAGMENT_SIZE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlIfFragmentSize;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlIfFragmentSize *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlIfFragmentSize),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlIfFragmentSize.rval =
                nmhTestv2FwlIfFragmentSize (lv.nmhTestv2FwlIfFragmentSize.
                                            pu4ErrorCode,
                                            lv.nmhTestv2FwlIfFragmentSize.
                                            i4FwlIfIfIndex,
                                            lv.nmhTestv2FwlIfFragmentSize.
                                            u4TestValFwlIfFragmentSize);
            lv.nmhTestv2FwlIfFragmentSize.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlIfFragmentSize *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlIfFragmentSize;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlIfFragmentSize)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_TOTAL_LARGE_FRAGMENT_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhGetFwlStatTotalLargeFragmentPacketsDenied;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatTotalLargeFragmentPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatTotalLargeFragmentPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatTotalLargeFragmentPacketsDenied.rval =
                nmhGetFwlStatTotalLargeFragmentPacketsDenied (lv.
                                                              nmhGetFwlStatTotalLargeFragmentPacketsDenied.
                                                              pu4RetValFwlStatTotalLargeFragmentPacketsDenied);
            lv.nmhGetFwlStatTotalLargeFragmentPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatTotalLargeFragmentPacketsDenied *) p;
            OsixKerUseInfo.pSrc =
                &lv.nmhGetFwlStatTotalLargeFragmentPacketsDenied;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatTotalLargeFragmentPacketsDenied))
                == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_TOTAL_ATTACKS_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatTotalAttacksPacketsDenied;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatTotalAttacksPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatTotalAttacksPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlStatTotalAttacksPacketsDenied.rval =
                nmhGetFwlStatTotalAttacksPacketsDenied (lv.
                                                        nmhGetFwlStatTotalAttacksPacketsDenied.
                                                        pu4RetValFwlStatTotalAttacksPacketsDenied);
            lv.nmhGetFwlStatTotalAttacksPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatTotalAttacksPacketsDenied *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatTotalAttacksPacketsDenied;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatTotalAttacksPacketsDenied)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_NET_BIOS_FILTERING:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalNetBiosFiltering;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalNetBiosFiltering *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalNetBiosFiltering),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlGlobalNetBiosFiltering.rval =
                nmhGetFwlGlobalNetBiosFiltering (lv.
                                                 nmhGetFwlGlobalNetBiosFiltering.
                                                 pi4RetValFwlNetBiosFiltering);
            lv.nmhGetFwlGlobalNetBiosFiltering.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlGlobalNetBiosFiltering *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalNetBiosFiltering;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalNetBiosFiltering)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_NET_BIOS_LAN2_WAN:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalNetBiosLan2Wan;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalNetBiosLan2Wan *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalNetBiosLan2Wan),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlGlobalNetBiosLan2Wan.rval =
                nmhGetFwlGlobalNetBiosLan2Wan (lv.nmhGetFwlGlobalNetBiosLan2Wan.
                                               pi4RetValFwlNetBiosLan2Wan);
            lv.nmhGetFwlGlobalNetBiosLan2Wan.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlGlobalNetBiosLan2Wan *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalNetBiosLan2Wan;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalNetBiosLan2Wan)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_LOG_FILE_SIZE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalLogFileSize;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalLogFileSize *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalLogFileSize),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlGlobalLogFileSize.rval =
                nmhGetFwlGlobalLogFileSize (lv.nmhGetFwlGlobalLogFileSize.
                                            pu4RetValFwlGlobalLogFileSize);

            lv.nmhGetFwlGlobalLogFileSize.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalLogFileSize;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlGlobalLogFileSize *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalLogFileSize)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_LOG_SIZE_THRESHOLD:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalLogSizeThreshold;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalLogSizeThreshold *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalLogSizeThreshold),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlGlobalLogSizeThreshold.rval =
                nmhGetFwlGlobalLogSizeThreshold (lv.
                                                 nmhGetFwlGlobalLogSizeThreshold.
                                                 pu4RetValFwlGlobalLogSizeThreshold);

            lv.nmhGetFwlGlobalLogSizeThreshold.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalLogSizeThreshold;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlGlobalLogSizeThreshold *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalLogSizeThreshold)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_IDS_LOG_SIZE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalIdsLogSize;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalIdsLogSize *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalIdsLogSize),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlGlobalIdsLogSize.rval =
                nmhGetFwlGlobalIdsLogSize (lv.nmhGetFwlGlobalIdsLogSize.
                                           pu4RetValFwlGlobalIdsLogSize);
            lv.nmhGetFwlGlobalIdsLogSize.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalIdsLogSize;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlGlobalIdsLogSize *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalIdsLogSize)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_IDS_LOG_THRESHOLD:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalIdsLogThreshold;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalIdsLogThreshold *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalIdsLogThreshold),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlGlobalIdsLogThreshold.rval =
                nmhGetFwlGlobalIdsLogThreshold (lv.
                                                nmhGetFwlGlobalIdsLogThreshold.
                                                pu4RetValFwlGlobalIdsLogThreshold);

            lv.nmhGetFwlGlobalIdsLogThreshold.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalIdsLogThreshold;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlGlobalIdsLogThreshold *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalIdsLogThreshold)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_IDS_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalIdsStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalIdsStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalIdsStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlGlobalIdsStatus.rval =
                nmhGetFwlGlobalIdsStatus (lv.
                                          nmhGetFwlGlobalIdsStatus.
                                          pi4RetValFwlGlobalIdsStatus);

            lv.nmhGetFwlGlobalIdsStatus.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalIdsStatus;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlGlobalIdsStatus *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalIdsStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_GET_FWL_TRAP_FILE_NAME:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlTrapFileName;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlTrapFileName *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlTrapFileName),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhGetFwlTrapFileName.rval =
                nmhGetFwlTrapFileName (lv.nmhGetFwlTrapFileName.
                                       pRetValFwlTrapFileName);

            lv.nmhGetFwlTrapFileName.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlTrapFileName;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlTrapFileName *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlTrapFileName)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_IDS_TRAP_FILE_NAME:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlIdsTrapFileName;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlIdsTrapFileName *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIdsTrapFileName),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlIdsTrapFileName.rval =
                nmhGetFwlIdsTrapFileName (lv.nmhGetFwlIdsTrapFileName.
                                          pRetValFwlIdsTrapFileName);
            lv.nmhGetFwlIdsTrapFileName.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlIdsTrapFileName;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlIdsTrapFileName *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIdsTrapFileName)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_GLOBAL_NET_BIOS_LAN2_WAN:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlGlobalNetBiosLan2Wan;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlGlobalNetBiosLan2Wan *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalNetBiosLan2Wan),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlGlobalNetBiosLan2Wan.rval =
                nmhSetFwlGlobalNetBiosLan2Wan (lv.nmhSetFwlGlobalNetBiosLan2Wan.
                                               i4SetValFwlGlobalNetBiosLan2Wan);
            lv.nmhSetFwlGlobalNetBiosLan2Wan.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlGlobalNetBiosLan2Wan *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFwlGlobalNetBiosLan2Wan;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalNetBiosLan2Wan)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_GLOBAL_LOG_FILE_SIZE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlGlobalLogFileSize;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlGlobalLogFileSize *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalLogFileSize),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlGlobalLogFileSize.rval =
                nmhSetFwlGlobalLogFileSize (lv.nmhSetFwlGlobalLogFileSize.
                                            u4SetValFwlGlobalLogFileSize);

            lv.nmhSetFwlGlobalLogFileSize.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhSetFwlGlobalLogFileSize;
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlGlobalLogFileSize *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalLogFileSize)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_GLOBAL_LOG_SIZE_THRESHOLD:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlGlobalLogSizeThreshold;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlGlobalLogSizeThreshold *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalLogSizeThreshold),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhSetFwlGlobalLogSizeThreshold.rval =
                nmhSetFwlGlobalLogSizeThreshold (lv.
                                                 nmhSetFwlGlobalLogSizeThreshold.
                                                 u4SetValFwlGlobalLogSizeThreshold);

            lv.nmhSetFwlGlobalLogSizeThreshold.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhSetFwlGlobalLogSizeThreshold;
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlGlobalLogSizeThreshold *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalLogSizeThreshold)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_GLOBAL_IDS_LOG_SIZE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlGlobalIdsLogSize;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlGlobalIdsLogSize *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalIdsLogSize),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFwlGlobalIdsLogSize.rval =
                nmhSetFwlGlobalIdsLogSize (lv.nmhSetFwlGlobalIdsLogSize.
                                           u4SetValFwlGlobalIdsLogSize);

            lv.nmhSetFwlGlobalIdsLogSize.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhSetFwlGlobalIdsLogSize;
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlGlobalIdsLogSize *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalIdsLogSize)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_GLOBAL_IDS_LOG_THRESHOLD:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlGlobalIdsLogThreshold;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlGlobalIdsLogThreshold *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalIdsLogThreshold),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFwlGlobalIdsLogThreshold.rval =
                nmhSetFwlGlobalIdsLogThreshold (lv.
                                                nmhSetFwlGlobalIdsLogThreshold.
                                                u4SetValFwlGlobalIdsLogThreshold);

            lv.nmhSetFwlGlobalIdsLogThreshold.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhSetFwlGlobalIdsLogThreshold;
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlGlobalIdsLogThreshold *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalIdsLogThreshold)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_GLOBAL_IDS_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlGlobalIdsStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlGlobalIdsStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalIdsStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFwlGlobalIdsStatus.rval =
                nmhSetFwlGlobalIdsStatus (lv.
                                          nmhSetFwlGlobalIdsStatus.
                                          i4SetValFwlGlobalIdsStatus);

            lv.nmhSetFwlGlobalIdsStatus.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhSetFwlGlobalIdsStatus;
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlGlobalIdsStatus *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalIdsStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_GLOBAL_NET_BIOS_LAN2_WAN:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlGlobalNetBiosLan2Wan;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlGlobalNetBiosLan2Wan *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalNetBiosLan2Wan),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlGlobalNetBiosLan2Wan.rval =
                nmhTestv2FwlGlobalNetBiosLan2Wan (lv.
                                                  nmhTestv2FwlGlobalNetBiosLan2Wan.
                                                  pu4ErrorCode,
                                                  lv.
                                                  nmhTestv2FwlGlobalNetBiosLan2Wan.
                                                  i4TestValFwlGlobalNetBiosLan2Wan);

            lv.nmhTestv2FwlGlobalNetBiosLan2Wan.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlGlobalNetBiosLan2Wan *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlGlobalNetBiosLan2Wan;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalNetBiosLan2Wan)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_GLOBAL_LOG_FILE_SIZE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlGlobalLogFileSize;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlGlobalLogFileSize *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalLogFileSize), 0) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlGlobalLogFileSize.rval =
                nmhTestv2FwlGlobalLogFileSize (lv.nmhTestv2FwlGlobalLogFileSize.
                                               pu4ErrorCode,
                                               lv.nmhTestv2FwlGlobalLogFileSize.
                                               u4TestValFwlGlobalLogFileSize);

            lv.nmhTestv2FwlGlobalLogFileSize.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlGlobalLogFileSize;
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlGlobalLogFileSize *) p;
            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalLogFileSize)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_GLOBAL_LOG_SIZE_THRESHOLD:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlGlobalLogSizeThreshold;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlGlobalLogSizeThreshold *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalLogSizeThreshold),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FwlGlobalLogSizeThreshold.rval =
                nmhTestv2FwlGlobalLogSizeThreshold (lv.
                                                    nmhTestv2FwlGlobalLogSizeThreshold.
                                                    pu4ErrorCode,
                                                    lv.
                                                    nmhTestv2FwlGlobalLogSizeThreshold.
                                                    u4TestValFwlGlobalLogSizeThreshold);

            lv.nmhTestv2FwlGlobalLogSizeThreshold.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlGlobalLogSizeThreshold;
            OsixKerUseInfo.pDest =
                (tFwlwnmhTestv2FwlGlobalLogSizeThreshold *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalLogSizeThreshold))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_TESTV2_FWL_GLOBAL_IDS_LOG_SIZE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlGlobalIdsLogSize;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlGlobalIdsLogSize *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalIdsLogSize),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FwlGlobalIdsLogSize.rval =
                nmhTestv2FwlGlobalIdsLogSize (lv.nmhTestv2FwlGlobalIdsLogSize.
                                              pu4ErrorCode,
                                              lv.nmhTestv2FwlGlobalIdsLogSize.
                                              u4TestValFwlGlobalIdsLogSize);

            lv.nmhTestv2FwlGlobalIdsLogSize.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlGlobalIdsLogSize;
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlGlobalIdsLogSize *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalIdsLogSize)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_GLOBAL_IDS_LOG_THRESHOLD:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlGlobalIdsLogThreshold;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlGlobalIdsLogThreshold *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalIdsLogThreshold),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FwlGlobalIdsLogThreshold.rval =
                nmhTestv2FwlGlobalIdsLogThreshold (lv.
                                                   nmhTestv2FwlGlobalIdsLogThreshold.
                                                   pu4ErrorCode,
                                                   lv.
                                                   nmhTestv2FwlGlobalIdsLogThreshold.
                                                   u4TestValFwlGlobalIdsLogThreshold);

            lv.nmhTestv2FwlGlobalIdsLogThreshold.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlGlobalIdsLogThreshold;
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlGlobalIdsLogThreshold *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalIdsLogThreshold)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_GLOBAL_IDS_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlGlobalIdsStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlGlobalIdsStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalIdsStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FwlGlobalIdsStatus.rval =
                nmhTestv2FwlGlobalIdsStatus (lv.
                                             nmhTestv2FwlGlobalIdsStatus.
                                             pu4ErrorCode,
                                             lv.
                                             nmhTestv2FwlGlobalIdsStatus.
                                             i4TestValFwlGlobalIdsStatus);

            lv.nmhTestv2FwlGlobalIdsStatus.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlGlobalIdsStatus;
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlGlobalIdsStatus *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalIdsStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case FWL_ADD_DEFAULT_RULES:
        {
            OsixKerUseInfo.pDest = &lv.FwlAddDefaultRules;
            OsixKerUseInfo.pSrc = (tFwlwFwlAddDefaultRules *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwFwlAddDefaultRules),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */

            FwlAddDefaultRules ();

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwFwlAddDefaultRules *) p;
            OsixKerUseInfo.pSrc = &lv.FwlAddDefaultRules;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwFwlAddDefaultRules)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case FWL_HANDLE_INTERFACE_INDICATION:
        {
            OsixKerUseInfo.pDest = &lv.FwlHandleInterfaceIndication;
            OsixKerUseInfo.pSrc = (tFwlwFwlHandleInterfaceIndication *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwFwlHandleInterfaceIndication),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            lv.FwlHandleInterfaceIndication.rval =
                FwlHandleInterfaceIndication (lv.FwlHandleInterfaceIndication.
                                              u4Interface,
                                              lv.FwlHandleInterfaceIndication.
                                              u4Status);

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tFwlwFwlHandleInterfaceIndication *) p;
            OsixKerUseInfo.pSrc = &lv.FwlHandleInterfaceIndication;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwFwlHandleInterfaceIndication)) ==
                OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case FWL_CLI_COMMIT:
        {
            FwlCommitAcl ();
            rc = IOCTL_SUCCESS;

            break;
        }

        case FWL_CLEAN_ALL_APP_ENTRY:
        {
            OsixKerUseInfo.pDest = &lv.FwlCleanAllAppEntry;
            OsixKerUseInfo.pSrc = (tNpwFwlCleanAllAppEntry *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwFwlCleanAllAppEntry),
                             0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            /* Invoke NPAPI */
            FwlCleanAllAppEntry ();
            lv.FwlCleanAllAppEntry.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwFwlCleanAllAppEntry *) p;
            OsixKerUseInfo.pSrc = &lv.FwlCleanAllAppEntry;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwFwlCleanAllAppEntry)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

#ifdef TR69_WANTED
        case FWL_SET_TR69_ACS_PORT_NUM:
        {
            OsixKerUseInfo.pDest = &lv.FwlSetTr69AcsPortNum;
            OsixKerUseInfo.pSrc = (tFwlSetTr69AcsPortNum *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlSetTr69AcsPortNum), 0) == OSIX_FAILURE)
            {
                return -EFAULT;
            }

            FwlSetTr69AcsPortNum (lv.FwlSetTr69AcsPortNum.u2PortNo);
            lv.FwlSetTr69AcsPortNum.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tFwlSetTr69AcsPortNum *) p;
            OsixKerUseInfo.pSrc = &lv.FwlSetTr69AcsPortNum;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlSetTr69AcsPortNum)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
#endif
        case NMH_VALIDATE_INDEX_INSTANCE_FWL_STATE_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhValidateIndexInstanceFwlStateTable;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhValidateIndexInstanceFwlStateTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlStateTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhValidateIndexInstanceFwlStateTable.rval =
                nmhValidateIndexInstanceFwlStateTable (lv.
                                                       nmhValidateIndexInstanceFwlStateTable.
                                                       i4FwlStateType,
                                                       lv.
                                                       nmhValidateIndexInstanceFwlStateTable.
                                                       i4FwlStateLocalIpAddrType,
                                                       lv.
                                                       nmhValidateIndexInstanceFwlStateTable.
                                                       pFwlStateLocalIpAddress,
                                                       lv.
                                                       nmhValidateIndexInstanceFwlStateTable.
                                                       i4FwlStateRemoteIpAddrType,
                                                       lv.
                                                       nmhValidateIndexInstanceFwlStateTable.
                                                       pFwlStateRemoteIpAddress,
                                                       lv.
                                                       nmhValidateIndexInstanceFwlStateTable.
                                                       i4FwlStateLocalPort,
                                                       lv.
                                                       nmhValidateIndexInstanceFwlStateTable.
                                                       i4FwlStateRemotePort,
                                                       lv.
                                                       nmhValidateIndexInstanceFwlStateTable.
                                                       i4FwlStateProtocol,
                                                       lv.
                                                       nmhValidateIndexInstanceFwlStateTable.
                                                       i4FwlStateDirection);

            OsixKerUseInfo.pSrc = &lv.nmhValidateIndexInstanceFwlStateTable;
            OsixKerUseInfo.pDest =
                (tFwlwnmhValidateIndexInstanceFwlStateTable *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlStateTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_FWL_STATE_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexFwlStateTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFirstIndexFwlStateTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlStateTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFirstIndexFwlStateTable.rval =
                nmhGetFirstIndexFwlStateTable (lv.nmhGetFirstIndexFwlStateTable.
                                               pi4FwlStateType,
                                               lv.nmhGetFirstIndexFwlStateTable.
                                               pi4FwlStateLocalIpAddrType,
                                               lv.nmhGetFirstIndexFwlStateTable.
                                               pFwlStateLocalIpAddress,
                                               lv.nmhGetFirstIndexFwlStateTable.
                                               pi4FwlStateRemoteIpAddrType,
                                               lv.nmhGetFirstIndexFwlStateTable.
                                               pFwlStateRemoteIpAddress,
                                               lv.nmhGetFirstIndexFwlStateTable.
                                               pi4FwlStateLocalPort,
                                               lv.nmhGetFirstIndexFwlStateTable.
                                               pi4FwlStateRemotePort,
                                               lv.nmhGetFirstIndexFwlStateTable.
                                               pi4FwlStateProtocol,
                                               lv.nmhGetFirstIndexFwlStateTable.
                                               pi4FwlStateDirection);

            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexFwlStateTable;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFirstIndexFwlStateTable *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlStateTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_FWL_STATE_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexFwlStateTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetNextIndexFwlStateTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlStateTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetNextIndexFwlStateTable.rval =
                nmhGetNextIndexFwlStateTable (lv.nmhGetNextIndexFwlStateTable.
                                              i4FwlStateType,
                                              lv.nmhGetNextIndexFwlStateTable.
                                              pi4NextFwlStateType,
                                              lv.nmhGetNextIndexFwlStateTable.
                                              i4FwlStateLocalIpAddrType,
                                              lv.nmhGetNextIndexFwlStateTable.
                                              pi4NextFwlStateLocalIpAddrType,
                                              lv.nmhGetNextIndexFwlStateTable.
                                              pFwlStateLocalIpAddress,
                                              lv.nmhGetNextIndexFwlStateTable.
                                              pNextFwlStateLocalIpAddress,
                                              lv.nmhGetNextIndexFwlStateTable.
                                              i4FwlStateRemoteIpAddrType,
                                              lv.nmhGetNextIndexFwlStateTable.
                                              pi4NextFwlStateRemoteIpAddrType,
                                              lv.nmhGetNextIndexFwlStateTable.
                                              pFwlStateRemoteIpAddress,
                                              lv.nmhGetNextIndexFwlStateTable.
                                              pNextFwlStateRemoteIpAddress,
                                              lv.nmhGetNextIndexFwlStateTable.
                                              i4FwlStateLocalPort,
                                              lv.nmhGetNextIndexFwlStateTable.
                                              pi4NextFwlStateLocalPort,
                                              lv.nmhGetNextIndexFwlStateTable.
                                              i4FwlStateRemotePort,
                                              lv.nmhGetNextIndexFwlStateTable.
                                              pi4NextFwlStateRemotePort,
                                              lv.nmhGetNextIndexFwlStateTable.
                                              i4FwlStateProtocol,
                                              lv.nmhGetNextIndexFwlStateTable.
                                              pi4NextFwlStateProtocol,
                                              lv.nmhGetNextIndexFwlStateTable.
                                              i4FwlStateDirection,
                                              lv.nmhGetNextIndexFwlStateTable.
                                              pi4NextFwlStateDirection);

            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexFwlStateTable;
            OsixKerUseInfo.pDest = (tFwlwnmhGetNextIndexFwlStateTable *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlStateTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STATE_ESTABLISHED_TIME:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStateEstablishedTime;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStateEstablishedTime *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStateEstablishedTime),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlStateEstablishedTime.rval =
                nmhGetFwlStateEstablishedTime (lv.nmhGetFwlStateEstablishedTime.
                                               i4FwlStateType,
                                               lv.nmhGetFwlStateEstablishedTime.
                                               i4FwlStateLocalIpAddrType,
                                               lv.nmhGetFwlStateEstablishedTime.
                                               pFwlStateLocalIpAddress,
                                               lv.nmhGetFwlStateEstablishedTime.
                                               i4FwlStateRemoteIpAddrType,
                                               lv.nmhGetFwlStateEstablishedTime.
                                               pFwlStateRemoteIpAddress,
                                               lv.nmhGetFwlStateEstablishedTime.
                                               i4FwlStateLocalPort,
                                               lv.nmhGetFwlStateEstablishedTime.
                                               i4FwlStateRemotePort,
                                               lv.nmhGetFwlStateEstablishedTime.
                                               i4FwlStateProtocol,
                                               lv.nmhGetFwlStateEstablishedTime.
                                               i4FwlStateDirection,
                                               lv.nmhGetFwlStateEstablishedTime.
                                               pu4RetValFwlStateEstablishedTime);

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStateEstablishedTime;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlStateEstablishedTime *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStateEstablishedTime)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STATE_LOCAL_STATE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStateLocalState;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStateLocalState *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStateLocalState),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlStateLocalState.rval =
                nmhGetFwlStateLocalState (lv.nmhGetFwlStateLocalState.
                                          i4FwlStateType,
                                          lv.nmhGetFwlStateLocalState.
                                          i4FwlStateLocalIpAddrType,
                                          lv.nmhGetFwlStateLocalState.
                                          pFwlStateLocalIpAddress,
                                          lv.nmhGetFwlStateLocalState.
                                          i4FwlStateRemoteIpAddrType,
                                          lv.nmhGetFwlStateLocalState.
                                          pFwlStateRemoteIpAddress,
                                          lv.nmhGetFwlStateLocalState.
                                          i4FwlStateLocalPort,
                                          lv.nmhGetFwlStateLocalState.
                                          i4FwlStateRemotePort,
                                          lv.nmhGetFwlStateLocalState.
                                          i4FwlStateProtocol,
                                          lv.nmhGetFwlStateLocalState.
                                          i4FwlStateDirection,
                                          lv.nmhGetFwlStateLocalState.
                                          pi4RetValFwlStateLocalState);

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStateLocalState;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlStateLocalState *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStateLocalState)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STATE_REMOTE_STATE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStateRemoteState;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStateRemoteState *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStateRemoteState),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlStateRemoteState.rval =
                nmhGetFwlStateRemoteState (lv.nmhGetFwlStateRemoteState.
                                           i4FwlStateType,
                                           lv.nmhGetFwlStateRemoteState.
                                           i4FwlStateLocalIpAddrType,
                                           lv.nmhGetFwlStateRemoteState.
                                           pFwlStateLocalIpAddress,
                                           lv.nmhGetFwlStateRemoteState.
                                           i4FwlStateRemoteIpAddrType,
                                           lv.nmhGetFwlStateRemoteState.
                                           pFwlStateRemoteIpAddress,
                                           lv.nmhGetFwlStateRemoteState.
                                           i4FwlStateLocalPort,
                                           lv.nmhGetFwlStateRemoteState.
                                           i4FwlStateRemotePort,
                                           lv.nmhGetFwlStateRemoteState.
                                           i4FwlStateProtocol,
                                           lv.nmhGetFwlStateRemoteState.
                                           i4FwlStateDirection,
                                           lv.nmhGetFwlStateRemoteState.
                                           pi4RetValFwlStateRemoteState);

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStateRemoteState;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlStateRemoteState *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStateRemoteState)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STATE_LOG_LEVEL:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStateLogLevel;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStateLogLevel *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStateLogLevel),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlStateLogLevel.rval =
                nmhGetFwlStateLogLevel (lv.nmhGetFwlStateLogLevel.
                                        i4FwlStateType,
                                        lv.nmhGetFwlStateLogLevel.
                                        i4FwlStateLocalIpAddrType,
                                        lv.nmhGetFwlStateLogLevel.
                                        pFwlStateLocalIpAddress,
                                        lv.nmhGetFwlStateLogLevel.
                                        i4FwlStateRemoteIpAddrType,
                                        lv.nmhGetFwlStateLogLevel.
                                        pFwlStateRemoteIpAddress,
                                        lv.nmhGetFwlStateLogLevel.
                                        i4FwlStateLocalPort,
                                        lv.nmhGetFwlStateLogLevel.
                                        i4FwlStateRemotePort,
                                        lv.nmhGetFwlStateLogLevel.
                                        i4FwlStateProtocol,
                                        lv.nmhGetFwlStateLogLevel.
                                        i4FwlStateDirection,
                                        lv.nmhGetFwlStateLogLevel.
                                        pi4RetValFwlStateLogLevel);

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStateLogLevel;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlStateLogLevel *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStateLogLevel)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STATE_CALL_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStateCallStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStateCallStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStateCallStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlStateCallStatus.rval =
                nmhGetFwlStateCallStatus (lv.nmhGetFwlStateCallStatus.
                                          i4FwlStateType,
                                          lv.nmhGetFwlStateCallStatus.
                                          i4FwlStateLocalIpAddrType,
                                          lv.nmhGetFwlStateCallStatus.
                                          pFwlStateLocalIpAddress,
                                          lv.nmhGetFwlStateCallStatus.
                                          i4FwlStateRemoteIpAddrType,
                                          lv.nmhGetFwlStateCallStatus.
                                          pFwlStateRemoteIpAddress,
                                          lv.nmhGetFwlStateCallStatus.
                                          i4FwlStateLocalPort,
                                          lv.nmhGetFwlStateCallStatus.
                                          i4FwlStateRemotePort,
                                          lv.nmhGetFwlStateCallStatus.
                                          i4FwlStateProtocol,
                                          lv.nmhGetFwlStateCallStatus.
                                          i4FwlStateDirection,
                                          lv.nmhGetFwlStateCallStatus.
                                          pi4RetValFwlStateCallStatus);

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStateCallStatus;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlStateCallStatus *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStateCallStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_FWL_DEFN_WHITE_LIST_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2FwlDefnWhiteListTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhDepv2FwlDefnWhiteListTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlDefnWhiteListTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhDepv2FwlDefnWhiteListTable.rval =
                nmhDepv2FwlDefnWhiteListTable (lv.nmhDepv2FwlDefnWhiteListTable.
                                               pu4ErrorCode,
                                               lv.nmhDepv2FwlDefnWhiteListTable.
                                               pSnmpIndexList,
                                               lv.nmhDepv2FwlDefnWhiteListTable.
                                               pSnmpVarBind);

            OsixKerUseInfo.pSrc = &lv.nmhDepv2FwlDefnWhiteListTable;
            OsixKerUseInfo.pDest = (tFwlwnmhDepv2FwlDefnWhiteListTable *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlDefnWhiteListTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_FWL_GLOBAL_LOG_FILE_SIZE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2FwlGlobalLogFileSize;
            OsixKerUseInfo.pSrc = (tFwlwnmhDepv2FwlGlobalLogFileSize *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlGlobalLogFileSize), 0) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2FwlGlobalLogFileSize.rval =
                nmhDepv2FwlGlobalLogFileSize (lv.nmhDepv2FwlGlobalLogFileSize.
                                              pu4ErrorCode,
                                              lv.nmhDepv2FwlGlobalLogFileSize.
                                              pSnmpIndexList,
                                              lv.nmhDepv2FwlGlobalLogFileSize.
                                              pSnmpVarBind);

            lv.nmhDepv2FwlGlobalLogFileSize.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhDepv2FwlGlobalLogFileSize;
            OsixKerUseInfo.pDest = (tFwlwnmhDepv2FwlGlobalLogFileSize *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlGlobalLogFileSize)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_FWL_GLOBAL_LOG_SIZE_THRESHOLD:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2FwlGlobalLogSizeThreshold;
            OsixKerUseInfo.pSrc = (tFwlwnmhDepv2FwlGlobalLogSizeThreshold *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlGlobalLogSizeThreshold),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhDepv2FwlGlobalLogSizeThreshold.rval =
                nmhDepv2FwlGlobalLogSizeThreshold (lv.
                                                   nmhDepv2FwlGlobalLogSizeThreshold.
                                                   pu4ErrorCode,
                                                   lv.
                                                   nmhDepv2FwlGlobalLogSizeThreshold.
                                                   pSnmpIndexList,
                                                   lv.
                                                   nmhDepv2FwlGlobalLogSizeThreshold.
                                                   pSnmpVarBind);

            lv.nmhDepv2FwlGlobalLogSizeThreshold.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhDepv2FwlGlobalLogSizeThreshold;
            OsixKerUseInfo.pDest = (tFwlwnmhDepv2FwlGlobalLogSizeThreshold *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlGlobalLogSizeThreshold)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_FWL_GLOBAL_IDS_LOG_SIZE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2FwlGlobalIdsLogSize;
            OsixKerUseInfo.pSrc = (tFwlwnmhDepv2FwlGlobalIdsLogSize *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlGlobalIdsLogSize),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhDepv2FwlGlobalIdsLogSize.rval =
                nmhDepv2FwlGlobalIdsLogSize (lv.nmhDepv2FwlGlobalIdsLogSize.
                                             pu4ErrorCode,
                                             lv.nmhDepv2FwlGlobalIdsLogSize.
                                             pSnmpIndexList,
                                             lv.nmhDepv2FwlGlobalIdsLogSize.
                                             pSnmpVarBind);

            lv.nmhDepv2FwlGlobalIdsLogSize.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhDepv2FwlGlobalIdsLogSize;
            OsixKerUseInfo.pDest = (tFwlwnmhDepv2FwlGlobalIdsLogSize *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlGlobalIdsLogSize)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_FWL_GLOBAL_IDS_LOG_THRESHOLD:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2FwlGlobalIdsLogThreshold;
            OsixKerUseInfo.pSrc = (tFwlwnmhDepv2FwlGlobalIdsLogThreshold *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlGlobalIdsLogThreshold),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhDepv2FwlGlobalIdsLogThreshold.rval =
                nmhDepv2FwlGlobalIdsLogThreshold (lv.
                                                  nmhDepv2FwlGlobalIdsLogThreshold.
                                                  pu4ErrorCode,
                                                  lv.
                                                  nmhDepv2FwlGlobalIdsLogThreshold.
                                                  pSnmpIndexList,
                                                  lv.
                                                  nmhDepv2FwlGlobalIdsLogThreshold.
                                                  pSnmpVarBind);

            lv.nmhDepv2FwlGlobalIdsLogThreshold.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhDepv2FwlGlobalIdsLogThreshold;
            OsixKerUseInfo.pDest = (tFwlwnmhDepv2FwlGlobalIdsLogThreshold *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlGlobalIdsLogThreshold)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_TESTV2_FWL_WHITE_LIST_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlWhiteListRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlWhiteListRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlWhiteListRowStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FwlWhiteListRowStatus.rval =
                nmhTestv2FwlWhiteListRowStatus (lv.
                                                nmhTestv2FwlWhiteListRowStatus.
                                                pu4ErrorCode,
                                                lv.
                                                nmhTestv2FwlWhiteListRowStatus.
                                                i4FwlWhiteListIpAddressType,
                                                lv.
                                                nmhTestv2FwlWhiteListRowStatus.
                                                pFwlWhiteListIpAddress,
                                                lv.
                                                nmhTestv2FwlWhiteListRowStatus.
                                                u4FwlWhiteListIpMask,
                                                lv.
                                                nmhTestv2FwlWhiteListRowStatus.
                                                i4TestValFwlWhiteListRowStatus);

            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlWhiteListRowStatus;
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlWhiteListRowStatus *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlWhiteListRowStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_WHITE_LIST_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlWhiteListRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlWhiteListRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlWhiteListRowStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFwlWhiteListRowStatus.rval =
                nmhSetFwlWhiteListRowStatus (lv.nmhSetFwlWhiteListRowStatus.
                                             i4FwlWhiteListIpAddressType,
                                             lv.nmhSetFwlWhiteListRowStatus.
                                             pFwlWhiteListIpAddress,
                                             lv.nmhSetFwlWhiteListRowStatus.
                                             u4FwlWhiteListIpMask,
                                             lv.nmhSetFwlWhiteListRowStatus.
                                             i4SetValFwlWhiteListRowStatus);

            OsixKerUseInfo.pSrc = &lv.nmhSetFwlWhiteListRowStatus;
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlWhiteListRowStatus *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlWhiteListRowStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_WHITE_LIST_HITS_COUNT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlWhiteListHitsCount;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlWhiteListHitsCount *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlWhiteListHitsCount),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlWhiteListHitsCount.rval =
                nmhGetFwlWhiteListHitsCount (lv.nmhGetFwlWhiteListHitsCount.
                                             i4FwlWhiteListIpAddressType,
                                             lv.nmhGetFwlWhiteListHitsCount.
                                             pFwlWhiteListIpAddress,
                                             lv.nmhGetFwlWhiteListHitsCount.
                                             u4FwlWhiteListIpMask,
                                             lv.nmhGetFwlWhiteListHitsCount.
                                             pu4RetValFwlWhiteListHitsCount);

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlWhiteListHitsCount;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlWhiteListHitsCount *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlWhiteListHitsCount)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_WHITE_LIST_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlWhiteListRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlWhiteListRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlWhiteListRowStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlWhiteListRowStatus.rval =
                nmhGetFwlWhiteListRowStatus (lv.nmhGetFwlWhiteListRowStatus.
                                             i4FwlWhiteListIpAddressType,
                                             lv.nmhGetFwlWhiteListRowStatus.
                                             pFwlWhiteListIpAddress,
                                             lv.nmhGetFwlWhiteListRowStatus.
                                             u4FwlWhiteListIpMask,
                                             lv.nmhGetFwlWhiteListRowStatus.
                                             pi4RetValFwlWhiteListRowStatus);

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlWhiteListRowStatus;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlWhiteListRowStatus *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlWhiteListRowStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_FWL_DEFN_WHITE_LIST_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexFwlDefnWhiteListTable;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetNextIndexFwlDefnWhiteListTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlDefnWhiteListTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetNextIndexFwlDefnWhiteListTable.rval =
                nmhGetNextIndexFwlDefnWhiteListTable (lv.
                                                      nmhGetNextIndexFwlDefnWhiteListTable.
                                                      i4FwlWhiteListIpAddressType,
                                                      lv.
                                                      nmhGetNextIndexFwlDefnWhiteListTable.
                                                      pi4NextFwlWhiteListIpAddressType,
                                                      lv.
                                                      nmhGetNextIndexFwlDefnWhiteListTable.
                                                      pFwlWhiteListIpAddress,
                                                      lv.
                                                      nmhGetNextIndexFwlDefnWhiteListTable.
                                                      pNextFwlWhiteListIpAddress,
                                                      lv.
                                                      nmhGetNextIndexFwlDefnWhiteListTable.
                                                      u4FwlWhiteListIpMask,
                                                      lv.
                                                      nmhGetNextIndexFwlDefnWhiteListTable.
                                                      pu4NextFwlWhiteListIpMask);

            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexFwlDefnWhiteListTable;
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetNextIndexFwlDefnWhiteListTable *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlDefnWhiteListTable))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_FWL_DEFN_WHITE_LIST_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexFwlDefnWhiteListTable;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFirstIndexFwlDefnWhiteListTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFirstIndexFwlDefnWhiteListTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFirstIndexFwlDefnWhiteListTable.rval =
                nmhGetFirstIndexFwlDefnWhiteListTable (lv.
                                                       nmhGetFirstIndexFwlDefnWhiteListTable.
                                                       pi4FwlWhiteListIpAddressType,
                                                       lv.
                                                       nmhGetFirstIndexFwlDefnWhiteListTable.
                                                       pFwlWhiteListIpAddress,
                                                       lv.
                                                       nmhGetFirstIndexFwlDefnWhiteListTable.
                                                       pu4FwlWhiteListIpMask);

            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexFwlDefnWhiteListTable;
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFirstIndexFwlDefnWhiteListTable *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFirstIndexFwlDefnWhiteListTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_FWL_DEFN_WHITE_LIST_TABLE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhValidateIndexInstanceFwlDefnWhiteListTable;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhValidateIndexInstanceFwlDefnWhiteListTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlDefnWhiteListTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhValidateIndexInstanceFwlDefnWhiteListTable.rval =
                nmhValidateIndexInstanceFwlDefnWhiteListTable (lv.
                                                               nmhValidateIndexInstanceFwlDefnWhiteListTable.
                                                               i4FwlWhiteListIpAddressType,
                                                               lv.
                                                               nmhValidateIndexInstanceFwlDefnWhiteListTable.
                                                               pFwlWhiteListIpAddress,
                                                               lv.
                                                               nmhValidateIndexInstanceFwlDefnWhiteListTable.
                                                               u4FwlWhiteListIpMask);

            OsixKerUseInfo.pSrc =
                &lv.nmhValidateIndexInstanceFwlDefnWhiteListTable;
            OsixKerUseInfo.pDest =
                (tFwlwnmhValidateIndexInstanceFwlDefnWhiteListTable *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlDefnWhiteListTable))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_FWL_DEFN_BLK_LIST_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2FwlDefnBlkListTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhDepv2FwlDefnBlkListTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlDefnBlkListTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhDepv2FwlDefnBlkListTable.rval =
                nmhDepv2FwlDefnBlkListTable (lv.nmhDepv2FwlDefnBlkListTable.
                                             pu4ErrorCode,
                                             lv.nmhDepv2FwlDefnBlkListTable.
                                             pSnmpIndexList,
                                             lv.nmhDepv2FwlDefnBlkListTable.
                                             pSnmpVarBind);

            OsixKerUseInfo.pSrc = &lv.nmhDepv2FwlDefnBlkListTable;
            OsixKerUseInfo.pDest = (tFwlwnmhDepv2FwlDefnBlkListTable *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlDefnBlkListTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_BLK_LIST_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlBlkListRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlBlkListRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlBlkListRowStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FwlBlkListRowStatus.rval =
                nmhTestv2FwlBlkListRowStatus (lv.nmhTestv2FwlBlkListRowStatus.
                                              pu4ErrorCode,
                                              lv.nmhTestv2FwlBlkListRowStatus.
                                              i4FwlBlkListIpAddressType,
                                              lv.nmhTestv2FwlBlkListRowStatus.
                                              pFwlBlkListIpAddress,
                                              lv.nmhTestv2FwlBlkListRowStatus.
                                              u4FwlBlkListIpMask,
                                              lv.nmhTestv2FwlBlkListRowStatus.
                                              i4TestValFwlBlkListRowStatus);

            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlBlkListRowStatus;
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlBlkListRowStatus *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlBlkListRowStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_BLK_LIST_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlBlkListRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlBlkListRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlBlkListRowStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFwlBlkListRowStatus.rval =
                nmhSetFwlBlkListRowStatus (lv.nmhSetFwlBlkListRowStatus.
                                           i4FwlBlkListIpAddressType,
                                           lv.nmhSetFwlBlkListRowStatus.
                                           pFwlBlkListIpAddress,
                                           lv.nmhSetFwlBlkListRowStatus.
                                           u4FwlBlkListIpMask,
                                           lv.nmhSetFwlBlkListRowStatus.
                                           i4SetValFwlBlkListRowStatus);

            OsixKerUseInfo.pSrc = &lv.nmhSetFwlBlkListRowStatus;
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlBlkListRowStatus *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlBlkListRowStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_BLK_LIST_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlBlkListRowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlBlkListRowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlBlkListRowStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlBlkListRowStatus.rval =
                nmhGetFwlBlkListRowStatus (lv.nmhGetFwlBlkListRowStatus.
                                           i4FwlBlkListIpAddressType,
                                           lv.nmhGetFwlBlkListRowStatus.
                                           pFwlBlkListIpAddress,
                                           lv.nmhGetFwlBlkListRowStatus.
                                           u4FwlBlkListIpMask,
                                           lv.nmhGetFwlBlkListRowStatus.
                                           pi4RetValFwlBlkListRowStatus);

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlBlkListRowStatus;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlBlkListRowStatus *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlBlkListRowStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_BLK_LIST_ENTRY_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlBlkListEntryType;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlBlkListEntryType *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlBlkListEntryType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlBlkListEntryType.rval =
                nmhGetFwlBlkListEntryType (lv.nmhGetFwlBlkListEntryType.
                                           i4FwlBlkListIpAddressType,
                                           lv.nmhGetFwlBlkListEntryType.
                                           pFwlBlkListIpAddress,
                                           lv.nmhGetFwlBlkListEntryType.
                                           u4FwlBlkListIpMask,
                                           lv.nmhGetFwlBlkListEntryType.
                                           pi4RetValFwlBlkListEntryType);

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlBlkListEntryType;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlBlkListEntryType *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlBlkListEntryType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_BLK_LIST_HITS_COUNT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlBlkListHitsCount;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlBlkListHitsCount *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlBlkListHitsCount),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlBlkListHitsCount.rval =
                nmhGetFwlBlkListHitsCount (lv.nmhGetFwlBlkListHitsCount.
                                           i4FwlBlkListIpAddressType,
                                           lv.nmhGetFwlBlkListHitsCount.
                                           pFwlBlkListIpAddress,
                                           lv.nmhGetFwlBlkListHitsCount.
                                           u4FwlBlkListIpMask,
                                           lv.nmhGetFwlBlkListHitsCount.
                                           pu4RetValFwlBlkListHitsCount);

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlBlkListHitsCount;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlBlkListHitsCount *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlBlkListHitsCount)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_FWL_DEFN_BLK_LIST_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexFwlDefnBlkListTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetNextIndexFwlDefnBlkListTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlDefnBlkListTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetNextIndexFwlDefnBlkListTable.rval =
                nmhGetNextIndexFwlDefnBlkListTable (lv.
                                                    nmhGetNextIndexFwlDefnBlkListTable.
                                                    i4FwlBlkListIpAddressType,
                                                    lv.
                                                    nmhGetNextIndexFwlDefnBlkListTable.
                                                    pi4NextFwlBlkListIpAddressType,
                                                    lv.
                                                    nmhGetNextIndexFwlDefnBlkListTable.
                                                    pFwlBlkListIpAddress,
                                                    lv.
                                                    nmhGetNextIndexFwlDefnBlkListTable.
                                                    pNextFwlBlkListIpAddress,
                                                    lv.
                                                    nmhGetNextIndexFwlDefnBlkListTable.
                                                    u4FwlBlkListIpMask,
                                                    lv.
                                                    nmhGetNextIndexFwlDefnBlkListTable.
                                                    pu4NextFwlBlkListIpMask);

            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexFwlDefnBlkListTable;
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetNextIndexFwlDefnBlkListTable *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlDefnBlkListTable))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_FWL_DEFN_BLK_LIST_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexFwlDefnBlkListTable;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFirstIndexFwlDefnBlkListTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlDefnBlkListTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFirstIndexFwlDefnBlkListTable.rval =
                nmhGetFirstIndexFwlDefnBlkListTable (lv.
                                                     nmhGetFirstIndexFwlDefnBlkListTable.
                                                     pi4FwlBlkListIpAddressType,
                                                     lv.
                                                     nmhGetFirstIndexFwlDefnBlkListTable.
                                                     pFwlBlkListIpAddress,
                                                     lv.
                                                     nmhGetFirstIndexFwlDefnBlkListTable.
                                                     pu4FwlBlkListIpMask);

            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexFwlDefnBlkListTable;
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFirstIndexFwlDefnBlkListTable *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlDefnBlkListTable))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_FWL_DEFN_BLK_LIST_TABLE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhValidateIndexInstanceFwlDefnBlkListTable;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhValidateIndexInstanceFwlDefnBlkListTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlDefnBlkListTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhValidateIndexInstanceFwlDefnBlkListTable.rval =
                nmhValidateIndexInstanceFwlDefnBlkListTable (lv.
                                                             nmhValidateIndexInstanceFwlDefnBlkListTable.
                                                             i4FwlBlkListIpAddressType,
                                                             lv.
                                                             nmhValidateIndexInstanceFwlDefnBlkListTable.
                                                             pFwlBlkListIpAddress,
                                                             lv.
                                                             nmhValidateIndexInstanceFwlDefnBlkListTable.
                                                             u4FwlBlkListIpMask);

            OsixKerUseInfo.pSrc =
                &lv.nmhValidateIndexInstanceFwlDefnBlkListTable;
            OsixKerUseInfo.pDest =
                (tFwlwnmhValidateIndexInstanceFwlDefnBlkListTable *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlDefnBlkListTable))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case FWL_HANDLE_BLACK_LIST_INFO_FROM_I_D_S:
        {
            OsixKerUseInfo.pDest = &lv.FwlHandleBlackListInfoFromIDS;
            OsixKerUseInfo.pSrc = (tFwlwFwlHandleBlackListInfoFromIDS *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwFwlHandleBlackListInfoFromIDS),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.FwlHandleBlackListInfoFromIDS.rval =
                FwlHandleBlackListInfoFromIDS (lv.FwlHandleBlackListInfoFromIDS.
                                               pu1Buf);

            OsixKerUseInfo.pSrc = &lv.FwlHandleBlackListInfoFromIDS;
            OsixKerUseInfo.pDest = (tFwlwFwlHandleBlackListInfoFromIDS *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwFwlHandleBlackListInfoFromIDS)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_I_C_M_PV6_CONTROL_SWITCH:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalICMPv6ControlSwitch;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalICMPv6ControlSwitch *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalICMPv6ControlSwitch),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlGlobalICMPv6ControlSwitch.rval =
                nmhGetFwlGlobalICMPv6ControlSwitch (lv.
                                                    nmhGetFwlGlobalICMPv6ControlSwitch.
                                                    pi4RetValFwlGlobalICMPv6ControlSwitch);
            lv.nmhGetFwlGlobalICMPv6ControlSwitch.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalICMPv6ControlSwitch;
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlGlobalICMPv6ControlSwitch *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalICMPv6ControlSwitch))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_GLOBAL_IPV6_SPOOF_FILTERING:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlGlobalIpv6SpoofFiltering;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlGlobalIpv6SpoofFiltering *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalIpv6SpoofFiltering),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlGlobalIpv6SpoofFiltering.rval =
                nmhGetFwlGlobalIpv6SpoofFiltering (lv.
                                                   nmhGetFwlGlobalIpv6SpoofFiltering.
                                                   pi4RetValFwlGlobalIpv6SpoofFiltering);
            lv.nmhGetFwlGlobalIpv6SpoofFiltering.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlGlobalIpv6SpoofFiltering;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlGlobalIpv6SpoofFiltering *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlGlobalIpv6SpoofFiltering)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_GLOBAL_I_C_M_PV6_CONTROL_SWITCH:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlGlobalICMPv6ControlSwitch;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlGlobalICMPv6ControlSwitch *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalICMPv6ControlSwitch),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFwlGlobalICMPv6ControlSwitch.rval =
                nmhSetFwlGlobalICMPv6ControlSwitch (lv.
                                                    nmhSetFwlGlobalICMPv6ControlSwitch.
                                                    i4SetValFwlGlobalICMPv6ControlSwitch);
            lv.nmhSetFwlGlobalICMPv6ControlSwitch.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhSetFwlGlobalICMPv6ControlSwitch;
            OsixKerUseInfo.pDest =
                (tFwlwnmhSetFwlGlobalICMPv6ControlSwitch *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalICMPv6ControlSwitch))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_GLOBAL_IPV6_SPOOF_FILTERING:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlGlobalIpv6SpoofFiltering;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlGlobalIpv6SpoofFiltering *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalIpv6SpoofFiltering),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFwlGlobalIpv6SpoofFiltering.rval =
                nmhSetFwlGlobalIpv6SpoofFiltering (lv.
                                                   nmhSetFwlGlobalIpv6SpoofFiltering.
                                                   i4SetValFwlGlobalIpv6SpoofFiltering);
            lv.nmhSetFwlGlobalIpv6SpoofFiltering.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhSetFwlGlobalIpv6SpoofFiltering;
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlGlobalIpv6SpoofFiltering *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlGlobalIpv6SpoofFiltering)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_GLOBAL_I_C_M_PV6_CONTROL_SWITCH:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlGlobalICMPv6ControlSwitch;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhTestv2FwlGlobalICMPv6ControlSwitch *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhTestv2FwlGlobalICMPv6ControlSwitch),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FwlGlobalICMPv6ControlSwitch.rval =
                nmhTestv2FwlGlobalICMPv6ControlSwitch (lv.
                                                       nmhTestv2FwlGlobalICMPv6ControlSwitch.
                                                       pu4ErrorCode,
                                                       lv.
                                                       nmhTestv2FwlGlobalICMPv6ControlSwitch.
                                                       i4TestValFwlGlobalICMPv6ControlSwitch);
            lv.nmhTestv2FwlGlobalICMPv6ControlSwitch.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlGlobalICMPv6ControlSwitch;
            OsixKerUseInfo.pDest =
                (tFwlwnmhTestv2FwlGlobalICMPv6ControlSwitch *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhTestv2FwlGlobalICMPv6ControlSwitch)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_GLOBAL_IPV6_SPOOF_FILTERING:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlGlobalIpv6SpoofFiltering;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhTestv2FwlGlobalIpv6SpoofFiltering *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalIpv6SpoofFiltering),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FwlGlobalIpv6SpoofFiltering.rval =
                nmhTestv2FwlGlobalIpv6SpoofFiltering (lv.
                                                      nmhTestv2FwlGlobalIpv6SpoofFiltering.
                                                      pu4ErrorCode,
                                                      lv.
                                                      nmhTestv2FwlGlobalIpv6SpoofFiltering.
                                                      i4TestValFwlGlobalIpv6SpoofFiltering);
            lv.nmhTestv2FwlGlobalIpv6SpoofFiltering.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlGlobalIpv6SpoofFiltering;
            OsixKerUseInfo.pDest =
                (tFwlwnmhTestv2FwlGlobalIpv6SpoofFiltering *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlGlobalIpv6SpoofFiltering))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_FWL_GLOBAL_I_C_M_PV6_CONTROL_SWITCH:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2FwlGlobalICMPv6ControlSwitch;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhDepv2FwlGlobalICMPv6ControlSwitch *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlGlobalICMPv6ControlSwitch),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhDepv2FwlGlobalICMPv6ControlSwitch.rval =
                nmhDepv2FwlGlobalICMPv6ControlSwitch (lv.
                                                      nmhDepv2FwlGlobalICMPv6ControlSwitch.
                                                      pu4ErrorCode,
                                                      lv.
                                                      nmhDepv2FwlGlobalICMPv6ControlSwitch.
                                                      pSnmpIndexList,
                                                      lv.
                                                      nmhDepv2FwlGlobalICMPv6ControlSwitch.
                                                      pSnmpVarBind);
            lv.nmhDepv2FwlGlobalICMPv6ControlSwitch.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhDepv2FwlGlobalICMPv6ControlSwitch;
            OsixKerUseInfo.pDest =
                (tFwlwnmhDepv2FwlGlobalICMPv6ControlSwitch *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlGlobalICMPv6ControlSwitch))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_FWL_GLOBAL_IPV6_SPOOF_FILTERING:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2FwlGlobalIpv6SpoofFiltering;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhDepv2FwlGlobalIpv6SpoofFiltering *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlGlobalIpv6SpoofFiltering),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhDepv2FwlGlobalIpv6SpoofFiltering.rval =
                nmhDepv2FwlGlobalIpv6SpoofFiltering (lv.
                                                     nmhDepv2FwlGlobalIpv6SpoofFiltering.
                                                     pu4ErrorCode,
                                                     lv.
                                                     nmhDepv2FwlGlobalIpv6SpoofFiltering.
                                                     pSnmpIndexList,
                                                     lv.
                                                     nmhDepv2FwlGlobalIpv6SpoofFiltering.
                                                     pSnmpVarBind);
            lv.nmhDepv2FwlGlobalIpv6SpoofFiltering.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhDepv2FwlGlobalIpv6SpoofFiltering;
            OsixKerUseInfo.pDest =
                (tFwlwnmhDepv2FwlGlobalIpv6SpoofFiltering *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlGlobalIpv6SpoofFiltering))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_FILTER_ADDR_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlFilterAddrType;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlFilterAddrType *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterAddrType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlFilterAddrType.rval =
                nmhGetFwlFilterAddrType (lv.nmhGetFwlFilterAddrType.
                                         pFwlFilterFilterName,
                                         lv.nmhGetFwlFilterAddrType.
                                         pi4RetValFwlFilterAddrType);
            lv.nmhGetFwlFilterAddrType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlFilterAddrType;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlFilterAddrType *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterAddrType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_FILTER_FLOW_ID:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlFilterFlowId;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlFilterFlowId *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterFlowId),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlFilterFlowId.rval =
                nmhGetFwlFilterFlowId (lv.nmhGetFwlFilterFlowId.
                                       pFwlFilterFilterName,
                                       lv.nmhGetFwlFilterFlowId.
                                       pu4RetValFwlFilterFlowId);
            lv.nmhGetFwlFilterFlowId.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlFilterFlowId;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlFilterFlowId *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterFlowId)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_FILTER_DSCP:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlFilterDscp;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlFilterDscp *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterDscp),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlFilterDscp.rval =
                nmhGetFwlFilterDscp (lv.nmhGetFwlFilterDscp.
                                     pFwlFilterFilterName,
                                     lv.nmhGetFwlFilterDscp.
                                     pi4RetValFwlFilterDscp);
            lv.nmhGetFwlFilterDscp.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlFilterDscp;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlFilterDscp *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlFilterDscp)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_FILTER_ADDR_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlFilterAddrType;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlFilterAddrType *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterAddrType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFwlFilterAddrType.rval =
                nmhSetFwlFilterAddrType (lv.nmhSetFwlFilterAddrType.
                                         pFwlFilterFilterName,
                                         lv.nmhSetFwlFilterAddrType.
                                         i4SetValFwlFilterAddrType);
            lv.nmhSetFwlFilterAddrType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhSetFwlFilterAddrType;
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlFilterAddrType *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterAddrType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_FILTER_FLOW_ID:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlFilterFlowId;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlFilterFlowId *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterFlowId),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFwlFilterFlowId.rval =
                nmhSetFwlFilterFlowId (lv.nmhSetFwlFilterFlowId.
                                       pFwlFilterFilterName,
                                       lv.nmhSetFwlFilterFlowId.
                                       u4SetValFwlFilterFlowId);
            lv.nmhSetFwlFilterFlowId.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhSetFwlFilterFlowId;
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlFilterFlowId *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterFlowId)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_FILTER_DSCP:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlFilterDscp;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlFilterDscp *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterDscp),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFwlFilterDscp.rval =
                nmhSetFwlFilterDscp (lv.nmhSetFwlFilterDscp.
                                     pFwlFilterFilterName,
                                     lv.nmhSetFwlFilterDscp.
                                     i4SetValFwlFilterDscp);
            lv.nmhSetFwlFilterDscp.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhSetFwlFilterDscp;
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlFilterDscp *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlFilterDscp)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_FILTER_ADDR_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlFilterAddrType;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlFilterAddrType *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterAddrType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FwlFilterAddrType.rval =
                nmhTestv2FwlFilterAddrType (lv.nmhTestv2FwlFilterAddrType.
                                            pu4ErrorCode,
                                            lv.nmhTestv2FwlFilterAddrType.
                                            pFwlFilterFilterName,
                                            lv.nmhTestv2FwlFilterAddrType.
                                            i4TestValFwlFilterAddrType);
            lv.nmhTestv2FwlFilterAddrType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlFilterAddrType;
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlFilterAddrType *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterAddrType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_FILTER_FLOW_ID:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlFilterFlowId;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlFilterFlowId *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterFlowId),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FwlFilterFlowId.rval =
                nmhTestv2FwlFilterFlowId (lv.nmhTestv2FwlFilterFlowId.
                                          pu4ErrorCode,
                                          lv.nmhTestv2FwlFilterFlowId.
                                          pFwlFilterFilterName,
                                          lv.nmhTestv2FwlFilterFlowId.
                                          u4TestValFwlFilterFlowId);
            lv.nmhTestv2FwlFilterFlowId.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlFilterFlowId;
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlFilterFlowId *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterFlowId)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_FILTER_DSCP:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlFilterDscp;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlFilterDscp *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterDscp),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FwlFilterDscp.rval =
                nmhTestv2FwlFilterDscp (lv.nmhTestv2FwlFilterDscp.pu4ErrorCode,
                                        lv.nmhTestv2FwlFilterDscp.
                                        pFwlFilterFilterName,
                                        lv.nmhTestv2FwlFilterDscp.
                                        i4TestValFwlFilterDscp);
            lv.nmhTestv2FwlFilterDscp.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlFilterDscp;
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlFilterDscp *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlFilterDscp)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_IF_I_C_M_PV6_MSG_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlIfICMPv6MsgType;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlIfICMPv6MsgType *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIfICMPv6MsgType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlIfICMPv6MsgType.rval =
                nmhGetFwlIfICMPv6MsgType (lv.nmhGetFwlIfICMPv6MsgType.
                                          i4FwlIfIfIndex,
                                          lv.nmhGetFwlIfICMPv6MsgType.
                                          pi4RetValFwlIfICMPv6MsgType);
            lv.nmhGetFwlIfICMPv6MsgType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlIfICMPv6MsgType;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlIfICMPv6MsgType *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlIfICMPv6MsgType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_IF_I_C_M_PV6_MSG_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlIfICMPv6MsgType;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlIfICMPv6MsgType *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlIfICMPv6MsgType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFwlIfICMPv6MsgType.rval =
                nmhSetFwlIfICMPv6MsgType (lv.nmhSetFwlIfICMPv6MsgType.
                                          i4FwlIfIfIndex,
                                          lv.nmhSetFwlIfICMPv6MsgType.
                                          i4SetValFwlIfICMPv6MsgType);
            lv.nmhSetFwlIfICMPv6MsgType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhSetFwlIfICMPv6MsgType;
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlIfICMPv6MsgType *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlIfICMPv6MsgType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_IF_I_C_M_PV6_MSG_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlIfICMPv6MsgType;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlIfICMPv6MsgType *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlIfICMPv6MsgType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FwlIfICMPv6MsgType.rval =
                nmhTestv2FwlIfICMPv6MsgType (lv.nmhTestv2FwlIfICMPv6MsgType.
                                             pu4ErrorCode,
                                             lv.nmhTestv2FwlIfICMPv6MsgType.
                                             i4FwlIfIfIndex,
                                             lv.nmhTestv2FwlIfICMPv6MsgType.
                                             i4TestValFwlIfICMPv6MsgType);
            lv.nmhTestv2FwlIfICMPv6MsgType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlIfICMPv6MsgType;
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlIfICMPv6MsgType *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlIfICMPv6MsgType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_FWL_DEFN_I_PV6_DMZ_TABLE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhValidateIndexInstanceFwlDefnIPv6DmzTable;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhValidateIndexInstanceFwlDefnIPv6DmzTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlDefnIPv6DmzTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhValidateIndexInstanceFwlDefnIPv6DmzTable.rval =
                nmhValidateIndexInstanceFwlDefnIPv6DmzTable (lv.
                                                             nmhValidateIndexInstanceFwlDefnIPv6DmzTable.
                                                             pFwlDmzIpv6Index);
            lv.nmhValidateIndexInstanceFwlDefnIPv6DmzTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc =
                &lv.nmhValidateIndexInstanceFwlDefnIPv6DmzTable;
            OsixKerUseInfo.pDest =
                (tFwlwnmhValidateIndexInstanceFwlDefnIPv6DmzTable *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhValidateIndexInstanceFwlDefnIPv6DmzTable))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_FWL_DEFN_I_PV6_DMZ_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexFwlDefnIPv6DmzTable;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFirstIndexFwlDefnIPv6DmzTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlDefnIPv6DmzTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFirstIndexFwlDefnIPv6DmzTable.rval =
                nmhGetFirstIndexFwlDefnIPv6DmzTable (lv.
                                                     nmhGetFirstIndexFwlDefnIPv6DmzTable.
                                                     pFwlDmzIpv6Index);
            lv.nmhGetFirstIndexFwlDefnIPv6DmzTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexFwlDefnIPv6DmzTable;
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFirstIndexFwlDefnIPv6DmzTable *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFirstIndexFwlDefnIPv6DmzTable))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_FWL_DEFN_I_PV6_DMZ_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexFwlDefnIPv6DmzTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetNextIndexFwlDefnIPv6DmzTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlDefnIPv6DmzTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetNextIndexFwlDefnIPv6DmzTable.rval =
                nmhGetNextIndexFwlDefnIPv6DmzTable (lv.
                                                    nmhGetNextIndexFwlDefnIPv6DmzTable.
                                                    pFwlDmzIpv6Index,
                                                    lv.
                                                    nmhGetNextIndexFwlDefnIPv6DmzTable.
                                                    pNextFwlDmzIpv6Index);
            lv.nmhGetNextIndexFwlDefnIPv6DmzTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexFwlDefnIPv6DmzTable;
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetNextIndexFwlDefnIPv6DmzTable *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetNextIndexFwlDefnIPv6DmzTable))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_DMZ_ADDRESS_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlDmzAddressType;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlDmzAddressType *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlDmzAddressType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlDmzAddressType.rval =
                nmhGetFwlDmzAddressType (lv.nmhGetFwlDmzAddressType.
                                         pFwlDmzIpv6Index,
                                         lv.nmhGetFwlDmzAddressType.
                                         pi4RetValFwlDmzAddressType);
            lv.nmhGetFwlDmzAddressType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlDmzAddressType;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlDmzAddressType *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlDmzAddressType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_DMZ_IPV6_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlDmzIpv6RowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlDmzIpv6RowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlDmzIpv6RowStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlDmzIpv6RowStatus.rval =
                nmhGetFwlDmzIpv6RowStatus (lv.nmhGetFwlDmzIpv6RowStatus.
                                           pFwlDmzIpv6Index,
                                           lv.nmhGetFwlDmzIpv6RowStatus.
                                           pi4RetValFwlDmzIpv6RowStatus);
            lv.nmhGetFwlDmzIpv6RowStatus.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlDmzIpv6RowStatus;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlDmzIpv6RowStatus *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlDmzIpv6RowStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_DMZ_ADDRESS_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlDmzAddressType;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlDmzAddressType *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlDmzAddressType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFwlDmzAddressType.rval =
                nmhSetFwlDmzAddressType (lv.nmhSetFwlDmzAddressType.
                                         pFwlDmzIpv6Index,
                                         lv.nmhSetFwlDmzAddressType.
                                         i4SetValFwlDmzAddressType);
            lv.nmhSetFwlDmzAddressType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhSetFwlDmzAddressType;
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlDmzAddressType *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlDmzAddressType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_DMZ_IPV6_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlDmzIpv6RowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlDmzIpv6RowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlDmzIpv6RowStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFwlDmzIpv6RowStatus.rval =
                nmhSetFwlDmzIpv6RowStatus (lv.nmhSetFwlDmzIpv6RowStatus.
                                           pFwlDmzIpv6Index,
                                           lv.nmhSetFwlDmzIpv6RowStatus.
                                           i4SetValFwlDmzIpv6RowStatus);
            lv.nmhSetFwlDmzIpv6RowStatus.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhSetFwlDmzIpv6RowStatus;
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlDmzIpv6RowStatus *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlDmzIpv6RowStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_DMZ_ADDRESS_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlDmzAddressType;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlDmzAddressType *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlDmzAddressType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FwlDmzAddressType.rval =
                nmhTestv2FwlDmzAddressType (lv.nmhTestv2FwlDmzAddressType.
                                            pu4ErrorCode,
                                            lv.nmhTestv2FwlDmzAddressType.
                                            pFwlDmzIpv6Index,
                                            lv.nmhTestv2FwlDmzAddressType.
                                            i4TestValFwlDmzAddressType);
            lv.nmhTestv2FwlDmzAddressType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlDmzAddressType;
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlDmzAddressType *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlDmzAddressType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_DMZ_IPV6_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlDmzIpv6RowStatus;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlDmzIpv6RowStatus *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlDmzIpv6RowStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FwlDmzIpv6RowStatus.rval =
                nmhTestv2FwlDmzIpv6RowStatus (lv.nmhTestv2FwlDmzIpv6RowStatus.
                                              pu4ErrorCode,
                                              lv.nmhTestv2FwlDmzIpv6RowStatus.
                                              pFwlDmzIpv6Index,
                                              lv.nmhTestv2FwlDmzIpv6RowStatus.
                                              i4TestValFwlDmzIpv6RowStatus);
            lv.nmhTestv2FwlDmzIpv6RowStatus.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlDmzIpv6RowStatus;
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlDmzIpv6RowStatus *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlDmzIpv6RowStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_FWL_DEFN_I_PV6_DMZ_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2FwlDefnIPv6DmzTable;
            OsixKerUseInfo.pSrc = (tFwlwnmhDepv2FwlDefnIPv6DmzTable *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlDefnIPv6DmzTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhDepv2FwlDefnIPv6DmzTable.rval =
                nmhDepv2FwlDefnIPv6DmzTable (lv.nmhDepv2FwlDefnIPv6DmzTable.
                                             pu4ErrorCode,
                                             lv.nmhDepv2FwlDefnIPv6DmzTable.
                                             pSnmpIndexList,
                                             lv.nmhDepv2FwlDefnIPv6DmzTable.
                                             pSnmpVarBind);
            lv.nmhDepv2FwlDefnIPv6DmzTable.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhDepv2FwlDefnIPv6DmzTable;
            OsixKerUseInfo.pDest = (tFwlwnmhDepv2FwlDefnIPv6DmzTable *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlDefnIPv6DmzTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_I_PV6_INSPECTED_PACKETS_COUNT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIPv6InspectedPacketsCount;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatIPv6InspectedPacketsCount *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatIPv6InspectedPacketsCount),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlStatIPv6InspectedPacketsCount.rval =
                nmhGetFwlStatIPv6InspectedPacketsCount (lv.
                                                        nmhGetFwlStatIPv6InspectedPacketsCount.
                                                        pu4RetValFwlStatIPv6InspectedPacketsCount);
            lv.nmhGetFwlStatIPv6InspectedPacketsCount.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIPv6InspectedPacketsCount;
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatIPv6InspectedPacketsCount *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatIPv6InspectedPacketsCount)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_I_PV6_TOTAL_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIPv6TotalPacketsDenied;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatIPv6TotalPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIPv6TotalPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlStatIPv6TotalPacketsDenied.rval =
                nmhGetFwlStatIPv6TotalPacketsDenied (lv.
                                                     nmhGetFwlStatIPv6TotalPacketsDenied.
                                                     pu4RetValFwlStatIPv6TotalPacketsDenied);
            lv.nmhGetFwlStatIPv6TotalPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIPv6TotalPacketsDenied;
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatIPv6TotalPacketsDenied *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIPv6TotalPacketsDenied))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_I_PV6_TOTAL_PACKETS_ACCEPTED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIPv6TotalPacketsAccepted;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatIPv6TotalPacketsAccepted *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatIPv6TotalPacketsAccepted),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlStatIPv6TotalPacketsAccepted.rval =
                nmhGetFwlStatIPv6TotalPacketsAccepted (lv.
                                                       nmhGetFwlStatIPv6TotalPacketsAccepted.
                                                       pu4RetValFwlStatIPv6TotalPacketsAccepted);
            lv.nmhGetFwlStatIPv6TotalPacketsAccepted.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIPv6TotalPacketsAccepted;
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatIPv6TotalPacketsAccepted *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatIPv6TotalPacketsAccepted)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_I_PV6_TOTAL_ICMP_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIPv6TotalIcmpPacketsDenied;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatIPv6TotalIcmpPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatIPv6TotalIcmpPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlStatIPv6TotalIcmpPacketsDenied.rval =
                nmhGetFwlStatIPv6TotalIcmpPacketsDenied (lv.
                                                         nmhGetFwlStatIPv6TotalIcmpPacketsDenied.
                                                         pu4RetValFwlStatIPv6TotalIcmpPacketsDenied);
            lv.nmhGetFwlStatIPv6TotalIcmpPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIPv6TotalIcmpPacketsDenied;
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatIPv6TotalIcmpPacketsDenied *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatIPv6TotalIcmpPacketsDenied)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_I_PV6_TOTAL_SPOOFED_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhGetFwlStatIPv6TotalSpoofedPacketsDenied;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatIPv6TotalSpoofedPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatIPv6TotalSpoofedPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlStatIPv6TotalSpoofedPacketsDenied.rval =
                nmhGetFwlStatIPv6TotalSpoofedPacketsDenied (lv.
                                                            nmhGetFwlStatIPv6TotalSpoofedPacketsDenied.
                                                            pu4RetValFwlStatIPv6TotalSpoofedPacketsDenied);
            lv.nmhGetFwlStatIPv6TotalSpoofedPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc =
                &lv.nmhGetFwlStatIPv6TotalSpoofedPacketsDenied;
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatIPv6TotalSpoofedPacketsDenied *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatIPv6TotalSpoofedPacketsDenied))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_I_PV6_TOTAL_ATTACKS_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhGetFwlStatIPv6TotalAttacksPacketsDenied;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatIPv6TotalAttacksPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatIPv6TotalAttacksPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlStatIPv6TotalAttacksPacketsDenied.rval =
                nmhGetFwlStatIPv6TotalAttacksPacketsDenied (lv.
                                                            nmhGetFwlStatIPv6TotalAttacksPacketsDenied.
                                                            pu4RetValFwlStatIPv6TotalAttacksPacketsDenied);
            lv.nmhGetFwlStatIPv6TotalAttacksPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc =
                &lv.nmhGetFwlStatIPv6TotalAttacksPacketsDenied;
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatIPv6TotalAttacksPacketsDenied *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatIPv6TotalAttacksPacketsDenied))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_CLEAR_I_PV6:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatClearIPv6;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStatClearIPv6 *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatClearIPv6),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlStatClearIPv6.rval =
                nmhGetFwlStatClearIPv6 (lv.nmhGetFwlStatClearIPv6.
                                        pi4RetValFwlStatClearIPv6);
            lv.nmhGetFwlStatClearIPv6.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatClearIPv6;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlStatClearIPv6 *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatClearIPv6)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_STAT_CLEAR_I_PV6:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlStatClearIPv6;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlStatClearIPv6 *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlStatClearIPv6),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFwlStatClearIPv6.rval =
                nmhSetFwlStatClearIPv6 (lv.nmhSetFwlStatClearIPv6.
                                        i4SetValFwlStatClearIPv6);
            lv.nmhSetFwlStatClearIPv6.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhSetFwlStatClearIPv6;
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlStatClearIPv6 *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlStatClearIPv6)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_STAT_CLEAR_I_PV6:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlStatClearIPv6;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlStatClearIPv6 *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlStatClearIPv6),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FwlStatClearIPv6.rval =
                nmhTestv2FwlStatClearIPv6 (lv.nmhTestv2FwlStatClearIPv6.
                                           pu4ErrorCode,
                                           lv.nmhTestv2FwlStatClearIPv6.
                                           i4TestValFwlStatClearIIPv6);
            lv.nmhTestv2FwlStatClearIPv6.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlStatClearIPv6;
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlStatClearIPv6 *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlStatClearIPv6)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_FWL_STAT_CLEAR_I_PV6:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2FwlStatClearIPv6;
            OsixKerUseInfo.pSrc = (tFwlwnmhDepv2FwlStatClearIPv6 *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlStatClearIPv6),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhDepv2FwlStatClearIPv6.rval =
                nmhDepv2FwlStatClearIPv6 (lv.nmhDepv2FwlStatClearIPv6.
                                          pu4ErrorCode,
                                          lv.nmhDepv2FwlStatClearIPv6.
                                          pSnmpIndexList,
                                          lv.nmhDepv2FwlStatClearIPv6.
                                          pSnmpVarBind);
            lv.nmhDepv2FwlStatClearIPv6.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhDepv2FwlStatClearIPv6;
            OsixKerUseInfo.pDest = (tFwlwnmhDepv2FwlStatClearIPv6 *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhDepv2FwlStatClearIPv6)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_IF_CLEAR_I_PV6:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIfClearIPv6;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStatIfClearIPv6 *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfClearIPv6),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlStatIfClearIPv6.rval =
                nmhGetFwlStatIfClearIPv6 (lv.nmhGetFwlStatIfClearIPv6.
                                          i4FwlStatIfIfIndex,
                                          lv.nmhGetFwlStatIfClearIPv6.
                                          pi4RetValFwlStatIfClearIPv6);
            lv.nmhGetFwlStatIfClearIPv6.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIfClearIPv6;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlStatIfClearIPv6 *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfClearIPv6)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_IF_I_PV6_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIfIPv6PacketsDenied;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStatIfIPv6PacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfIPv6PacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlStatIfIPv6PacketsDenied.rval =
                nmhGetFwlStatIfIPv6PacketsDenied (lv.
                                                  nmhGetFwlStatIfIPv6PacketsDenied.
                                                  i4FwlStatIfIfIndex,
                                                  lv.
                                                  nmhGetFwlStatIfIPv6PacketsDenied.
                                                  pu4RetValFwlStatIfIPv6PacketsDenied);
            lv.nmhGetFwlStatIfIPv6PacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIfIPv6PacketsDenied;
            OsixKerUseInfo.pDest = (tFwlwnmhGetFwlStatIfIPv6PacketsDenied *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfIPv6PacketsDenied)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_IF_I_PV6_PACKETS_ACCEPTED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIfIPv6PacketsAccepted;
            OsixKerUseInfo.pSrc = (tFwlwnmhGetFwlStatIfIPv6PacketsAccepted *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfIPv6PacketsAccepted),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlStatIfIPv6PacketsAccepted.rval =
                nmhGetFwlStatIfIPv6PacketsAccepted (lv.
                                                    nmhGetFwlStatIfIPv6PacketsAccepted.
                                                    i4FwlStatIfIfIndex,
                                                    lv.
                                                    nmhGetFwlStatIfIPv6PacketsAccepted.
                                                    pu4RetValFwlStatIfIPv6PacketsAccepted);
            lv.nmhGetFwlStatIfIPv6PacketsAccepted.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIfIPv6PacketsAccepted;
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatIfIPv6PacketsAccepted *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfIPv6PacketsAccepted))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_IF_I_PV6_ICMP_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIfIPv6IcmpPacketsDenied;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatIfIPv6IcmpPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfIPv6IcmpPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlStatIfIPv6IcmpPacketsDenied.rval =
                nmhGetFwlStatIfIPv6IcmpPacketsDenied (lv.
                                                      nmhGetFwlStatIfIPv6IcmpPacketsDenied.
                                                      i4FwlStatIfIfIndex,
                                                      lv.
                                                      nmhGetFwlStatIfIPv6IcmpPacketsDenied.
                                                      pu4RetValFwlStatIfIPv6IcmpPacketsDenied);
            lv.nmhGetFwlStatIfIPv6IcmpPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIfIPv6IcmpPacketsDenied;
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatIfIPv6IcmpPacketsDenied *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhGetFwlStatIfIPv6IcmpPacketsDenied))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FWL_STAT_IF_I_PV6_SPOOFED_PACKETS_DENIED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFwlStatIfIPv6SpoofedPacketsDenied;
            OsixKerUseInfo.pSrc =
                (tFwlwnmhGetFwlStatIfIPv6SpoofedPacketsDenied *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatIfIPv6SpoofedPacketsDenied),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFwlStatIfIPv6SpoofedPacketsDenied.rval =
                nmhGetFwlStatIfIPv6SpoofedPacketsDenied (lv.
                                                         nmhGetFwlStatIfIPv6SpoofedPacketsDenied.
                                                         i4FwlStatIfIfIndex,
                                                         lv.
                                                         nmhGetFwlStatIfIPv6SpoofedPacketsDenied.
                                                         pu4RetValFwlStatIfIPv6SpoofedPacketsDenied);
            lv.nmhGetFwlStatIfIPv6SpoofedPacketsDenied.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhGetFwlStatIfIPv6SpoofedPacketsDenied;
            OsixKerUseInfo.pDest =
                (tFwlwnmhGetFwlStatIfIPv6SpoofedPacketsDenied *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tFwlwnmhGetFwlStatIfIPv6SpoofedPacketsDenied)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FWL_STAT_IF_CLEAR_I_PV6:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFwlStatIfClearIPv6;
            OsixKerUseInfo.pSrc = (tFwlwnmhSetFwlStatIfClearIPv6 *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlStatIfClearIPv6),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFwlStatIfClearIPv6.rval =
                nmhSetFwlStatIfClearIPv6 (lv.nmhSetFwlStatIfClearIPv6.
                                          i4FwlStatIfIfIndex,
                                          lv.nmhSetFwlStatIfClearIPv6.
                                          i4SetValFwlStatIfClearIPv6);
            lv.nmhSetFwlStatIfClearIPv6.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhSetFwlStatIfClearIPv6;
            OsixKerUseInfo.pDest = (tFwlwnmhSetFwlStatIfClearIPv6 *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhSetFwlStatIfClearIPv6)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FWL_STAT_IF_CLEAR_I_PV6:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FwlStatIfClearIPv6;
            OsixKerUseInfo.pSrc = (tFwlwnmhTestv2FwlStatIfClearIPv6 *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlStatIfClearIPv6),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FwlStatIfClearIPv6.rval =
                nmhTestv2FwlStatIfClearIPv6 (lv.nmhTestv2FwlStatIfClearIPv6.
                                             pu4ErrorCode,
                                             lv.nmhTestv2FwlStatIfClearIPv6.
                                             i4FwlStatIfIfIndex,
                                             lv.nmhTestv2FwlStatIfClearIPv6.
                                             i4TestValFwlStatIfClearIPv6);
            lv.nmhTestv2FwlStatIfClearIPv6.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pSrc = &lv.nmhTestv2FwlStatIfClearIPv6;
            OsixKerUseInfo.pDest = (tFwlwnmhTestv2FwlStatIfClearIPv6 *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwnmhTestv2FwlStatIfClearIPv6)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case FWL_SET_HW_FIL_CAP_IN_KERNEL:
        {
            OsixKerUseInfo.pDest = &lv.FwlSetHwFilterCapabilities;
            OsixKerUseInfo.pSrc = (tFwlwFwlSetHwFilterCapabilities *) p;

            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwFwlSetHwFilterCapabilities),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            MEMCPY (&gau1FwlNpHwFilterCap,
                    lv.FwlSetHwFilterCapabilities.pu1HwFilterCapabilities,
                    FWL_NUM_ATTACKS);
            OsixKerUseInfo.pSrc = &lv.FwlSetHwFilterCapabilities;
            OsixKerUseInfo.pDest = (tFwlwFwlSetHwFilterCapabilities *) p;

            /* Copy NPAPI return value(s) back to user */
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tFwlwFwlSetHwFilterCapabilities)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        default:
            rc = -EFAULT;
            break;

    }                            /* switch */

    return (rc);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : KernFwlCliGetStatefulTableEntries                  */
/*                                                                           */
/*     DESCRIPTION      : This function gets the stateful Table Entries      */
/*                                                                           */
/*     INPUT            : u4HashIndex - Hash Index                           */
/*                                                                           */
/*     OUTPUT           : ppStateFilterInfo - Stateful Table entry           */
/*                                                                           */
/*     RETURNS          : IOCTL_SUCCESS or IOCTL_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
INT4
KernFwlCliGetStatefulTableEntries (UINT4 u4HashIndex,
                                   tStatefulSessionNode ** ppStateFilterInfo)
{
    INT4                i4RetVal = IOCTL_FAILURE;
    tStatefulSessionNode *pTempStateInfo = NULL;

    /* Check whether the Hash List has some entries in it. If so return those
     * entries or else move to the next Hash Bucket */
    if (TMO_SLL_Count ((&(FWL_STATE_HASH_LIST)->HashList[(u4HashIndex)])) != 0)
    {
        if (*ppStateFilterInfo == NULL)
        {
            pTempStateInfo = (tStatefulSessionNode *)
                TMO_HASH_Get_First_Bucket_Node (FWL_STATE_HASH_LIST,
                                                u4HashIndex);
        }
        else
        {
            pTempStateInfo = (tStatefulSessionNode *)
                TMO_HASH_Get_Next_Bucket_Node (FWL_STATE_HASH_LIST,
                                               u4HashIndex,
                                               (tTMO_HASH_NODE *)
                                               * ppStateFilterInfo);
        }
        if (pTempStateInfo != NULL)
        {
            *ppStateFilterInfo = pTempStateInfo;
            i4RetVal = IOCTL_SUCCESS;
        }
        else
        {
            *ppStateFilterInfo = NULL;
        }
    }
    else
    {
        *ppStateFilterInfo = NULL;
    }
    return i4RetVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlErrorMessageGenerate                          */
/*                                                                          */
/*    Description        : Calls the ICMP module to generate the Error      */
/*                         message of Communication Administratively        */
/*                         Prohibited.                                      */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the packet            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1
FwlErrorMessageGenerate (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex)
#else
PUBLIC INT1
FwlErrorMessageGenerate (pBuf, u4IfIndex)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     UINT4               u4IfIndex;
#endif
{
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    tSecQueMsg          SecQueMsg;
    tFwlErrMsg         *pFwlErrMsg = NULL;
    INT1                i1RetVal = 0;

    pFwlErrMsg = &SecQueMsg.ModuleParam.FwlErrMsg;
    pFwlErrMsg->u4IfIndex = u4IfIndex;
    SecQueMsg.u1MsgType = SEC_ERRMSG_TO_FWL_FROM_KERNEL;

    pDupBuf = SEC_CRU_BUF_Duplicate_BufChain (pBuf);
    if (NULL == pDupBuf)
    {
        return FWL_FAILURE;
    }

    if (CRU_BUF_Prepend_BufChain (pDupBuf, (UINT1 *)
                                  SEC_GET_MODULE_DATA_PTR (pBuf),
                                  sizeof (tSecModuleData)) == CRU_FAILURE)
    {
        SEC_CRU_BUF_Release_MsgBufChain(pDupBuf, FALSE);
        return FWL_FAILURE;
    }

    if (CRU_BUF_Prepend_BufChain (pDupBuf, (UINT1 *) &SecQueMsg,
                                  sizeof (tSecQueMsg)) == CRU_FAILURE)
    {
        SEC_CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
        return FWL_FAILURE;
    }

    i1RetVal = OsixQueSend (gu4SecReadModIdx,
                            (UINT1 *) &pDupBuf, OSIX_DEF_MSG_LEN);

    if (i1RetVal != OSIX_SUCCESS)
    {
        SEC_CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
        return FWL_FAILURE;
    }
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

    return FWL_SUCCESS;
}                                /* End of the function -- FwlErrorMessageGenerate */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlLogMessage                                    */
/*                                                                          */
/*    Description        : This function logs the information about packets */
/*                         denied.                                          */
/*                                                                          */
/*    Input(s)           : u4AttackType -- Type of Attack                   */
/*                         u4SrcAddr -- Source Address                      */
/*                         u4DestAddr -- Destn Address                      */
/*                         u4IfaceNum -- Interface Number                   */
/*                         u1Protocol -- Protocol number                    */
/*                         u2SrcPort -- Destination Port number             */
/*                         u2DestPort -- Destination Port number            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlLogMessage (UINT4 u4AttackType,
               UINT4 u4IfaceNum,
               tCRU_BUF_CHAIN_HEADER * pBuf,
               UINT4 u4LogLevel, UINT4 u4Severity, UINT1 *pMsg)
#else
PUBLIC VOID
FwlLogMessage (u4AttackType, u4IfaceNum, pBuf, u4LogLevel, u4Severity, pMsg)
     UINT4               u4AttackType;
     UINT4               u4IfaceNum;
     tCRU_BUF_CHAIN_HEADER *pBuf,
         UINT4 u4LogLevel, UINT4 u4Severity, UINT1 *pMsg;
#endif
{
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    tFwlLogMsg         *pFwlLogMsg = NULL;
    tSecQueMsg          SecQueMsg;
    INT1                i1RetVal = 0;

    if ((u4AttackType == FWL_PERMITTED) && (u4LogLevel == FWL_NONE))
    {
        return;
    }

    pFwlLogMsg = &SecQueMsg.ModuleParam.FwlLogMsg;
    pFwlLogMsg->u4AttackType = u4AttackType;
    pFwlLogMsg->u4IfaceNum = u4IfaceNum;
    pFwlLogMsg->u4LogLevel = u4LogLevel;
    pFwlLogMsg->u4Severity = u4Severity;
    if (pMsg != NULL)
    {
        MEMCPY (&(pFwlLogMsg->au1Msg), pMsg, STRLEN (pMsg) + 1);
    }
    SecQueMsg.u1MsgType = SEC_LOGMSG_TO_FWL_FROM_KERNEL;

    if (pBuf == NULL)
    {
        pBuf = SEC_CRU_BUF_Allocate_MsgBufChain ((sizeof (tSecModuleData) +
                                              sizeof (tSecQueMsg)), 0);
        if (pBuf == NULL)
        {
            return;
        }

        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &SecQueMsg, 0,
                                   sizeof (tSecQueMsg));

        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *)
                                   SEC_GET_MODULE_DATA_PTR (pBuf),
                                   sizeof (tSecQueMsg),
                                   sizeof (tSecModuleData));

        /* Post the buffer to CFA queue */
        i1RetVal = OsixQueSend (gu4SecReadModIdx,
                                (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN);

        if (i1RetVal != OSIX_SUCCESS)
        {
            SEC_CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return;
        }

    }
    else
    {

        pDupBuf = SEC_CRU_BUF_Duplicate_BufChain (pBuf);
        if (NULL == pDupBuf)
        {
            return;
        }
        CRU_BUF_Prepend_BufChain (pDupBuf, (UINT1 *)
                                  SEC_GET_MODULE_DATA_PTR (pBuf),
                                  sizeof (tSecModuleData));

        CRU_BUF_Prepend_BufChain (pDupBuf, (UINT1 *) &SecQueMsg,
                                  sizeof (tSecQueMsg));

        /* post the buffer to CFA queue */
        i1RetVal = OsixQueSend (gu4SecReadModIdx,
                                (UINT1 *) &pDupBuf, OSIX_DEF_MSG_LEN);
        if (i1RetVal != OSIX_SUCCESS)
        {
            SEC_CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
            return;
        }

    }

    return;
}

/**************************************************************************/
/*                                                                        */
/*  Function Name      : FwlSendThresholdExceededTrap                     */
/*                                                                        */
/*  Description        : This function generates a TRAP when the Interface*/
/*                       or Global Packet Discard Count exceeds the       */
/*                       Threshold set                                    */
/*                                                                        */
/*  Input(s)           : u4IfIndex - Interface Index                      */
/*                       u4PktCount - Total Denied Packets Count          */
/*                                                                        */
/*  Output(s)          : None                                             */
/*                                                                        */
/*  Returns            : FWL_FAILURE/FWL_SUCCESS                          */
/**************************************************************************/

#ifdef __STDC__
PUBLIC INT4
FwlSendThresholdExceededTrap (UINT4 u4IfIndex, UINT4 u4PktCount)
#else
PUBLIC INT4
FwlSendThresholdExceededTrap (u4IfIndex, u4PktCount)
     UINT4               u4IfIndex;
     UINT4               u4PktCount;
#endif
{

    tSecQueMsg          SecQueMsg;
    tFwlTrapMsg        *pFwlTrapMsg = NULL;
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
    INT1                i1RetVal = 0;

    pFwlTrapMsg = &(SecQueMsg.ModuleParam.FwlTrapMsg);

    pFwlTrapMsg->u4TrapType = FWL_THRESHOLD_EXCEED_TRAP;
    pFwlTrapMsg->u4IfaceNum = u4IfIndex;
    pFwlTrapMsg->u4PktCount = u4PktCount;

    SecQueMsg.u1MsgType = SEC_TRAPMSG_TO_FWL_FROM_KERNEL;

    pCruBuf = SEC_CRU_BUF_Allocate_MsgBufChain (sizeof (tSecQueMsg), 0);
    if (pCruBuf == NULL)
    {
        return FWL_FAILURE;
    }
    if (CRU_BUF_Copy_OverBufChain (pCruBuf, (UINT1 *) &SecQueMsg, 0,
                                   sizeof (tSecQueMsg)) == CRU_FAILURE)
    {
        SEC_CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
        return FWL_FAILURE;
    }

    i1RetVal = OsixQueSend (gu4SecReadModIdx,
                            (UINT1 *) &pCruBuf, OSIX_DEF_MSG_LEN);

    if (i1RetVal != OSIX_SUCCESS)
    {
        SEC_CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
        return FWL_FAILURE;
    }

    return FWL_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlGenerateMemFailureTrap                        */
/*                                                                          */
/*    Description        : This function generates a TRAP when memory       */
/*                         Failure occurs. This Trap contains a information */
/*                         about the allocation Failure.                    */
/*                                                                          */
/*    Input(s)           : u1NodeName - This vaulue specified during which  */
/*                                     allocation the failure has occurred  */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
#ifdef __STDC__
PUBLIC VOID
FwlGenerateMemFailureTrap (UINT1 u1NodeName)
#else
PUBLIC VOID
FwlGenerateMemFailureTrap (u1NodeName)
     UINT1               u1NodeName;
#endif
{
    tSecQueMsg          SecQueMsg;
    tFwlTrapMsg        *pFwlTrapMsg = NULL;
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
    INT1                i1RetVal = 0;

    pFwlTrapMsg = &(SecQueMsg.ModuleParam.FwlTrapMsg);

    pFwlTrapMsg->u4TrapType = FWL_GEN_MEM_FAILURE_TRAP;
    pFwlTrapMsg->u1NodeName = u1NodeName;
    SecQueMsg.u1MsgType = SEC_TRAPMSG_TO_FWL_FROM_KERNEL;

    pCruBuf = SEC_CRU_BUF_Allocate_MsgBufChain (sizeof (tSecQueMsg), 0);
    if (pCruBuf == NULL)
    {
        return;
    }
    if (CRU_BUF_Copy_OverBufChain (pCruBuf, (UINT1 *) &SecQueMsg, 0,
                                   sizeof (tSecQueMsg)) == CRU_FAILURE)
    {
        SEC_CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
        return;
    }

    i1RetVal = OsixQueSend (gu4SecReadModIdx,
                            (UINT1 *) &pCruBuf, OSIX_DEF_MSG_LEN);

    if (i1RetVal != OSIX_SUCCESS)
    {
        SEC_CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
        return;
    }

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlGenerateAttackSumTrap                         */
/*                                                                          */
/*    Description        : This function generates a TRAP when number of    */
/*                         firewall attacks exceeded the limit              */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
PUBLIC VOID
FwlGenerateAttackSumTrap (VOID)
{
    tSecQueMsg          SecQueMsg;
    tFwlTrapMsg        *pFwlTrapMsg = NULL;
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
    INT1                i1RetVal = 0;

    pFwlTrapMsg = &(SecQueMsg.ModuleParam.FwlTrapMsg);

    pFwlTrapMsg->u4TrapType = FWL_GEN_ATTACK_SUM_TRAP;

    SecQueMsg.u1MsgType = SEC_TRAPMSG_TO_FWL_FROM_KERNEL;

    pCruBuf = SEC_CRU_BUF_Allocate_MsgBufChain (sizeof (tSecQueMsg), 0);
    if (pCruBuf == NULL)
    {
        return;
    }

    if (CRU_BUF_Copy_OverBufChain (pCruBuf, (UINT1 *) &SecQueMsg, 0,
                                   sizeof (tSecQueMsg)) == CRU_FAILURE)
    {
        SEC_CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
        return;
    }

    i1RetVal = OsixQueSend (gu4SecReadModIdx,
                            (UINT1 *) &pCruBuf, OSIX_DEF_MSG_LEN);

    if (i1RetVal != OSIX_SUCCESS)
    {
        SEC_CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
        return;
    }

    return;
}

/******************************************************************************
 * Function Name      : IdsSendAttackPktTrap
 *
 * Description        : This function is used to send IDS notifications
 *                      when attack-packet is identified in IDS
 *
 * Input(s)           : pu1BlackListIpAddr - BlackListed Ip address of the
 *                                           attack-packet identified in IDS.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/

VOID
IdsSendAttackPktTrap (UINT1 *pu1BlackListIpAddr)
{
    tSecQueMsg          SecQueMsg;
    tFwlTrapMsg        *pFwlTrapMsg = NULL;
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
    INT1                i1RetVal = 0;

    pFwlTrapMsg = &(SecQueMsg.ModuleParam.FwlTrapMsg);

    pFwlTrapMsg->u4TrapType = FWL_IDS_ATTACK_PKT_TRAP;

    if (pu1BlackListIpAddr != NULL)
    {
        MEMCPY (&(pFwlTrapMsg->au1BlackListIpAddr), pu1BlackListIpAddr,
                (STRLEN (pu1BlackListIpAddr) + 1));
    }
    SecQueMsg.u1MsgType = SEC_TRAPMSG_TO_FWL_FROM_KERNEL;

    pCruBuf = SEC_CRU_BUF_Allocate_MsgBufChain (sizeof (tSecQueMsg), 0);
    if (pCruBuf == NULL)
    {
        SEC_CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
        return;
    }

    if (CRU_BUF_Copy_OverBufChain (pCruBuf, (UINT1 *) &SecQueMsg, 0,
                                   sizeof (tSecQueMsg)) == CRU_FAILURE)
    {
        SEC_CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
        return;
    }

    i1RetVal = OsixQueSend (gu4SecReadModIdx,
                            (UINT1 *) &pCruBuf, OSIX_DEF_MSG_LEN);

    if (i1RetVal != OSIX_SUCCESS)
    {
        SEC_CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
        return;
    }

    return;
}

VOID
RegisterFSFWL (VOID)
{
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSubnetMaskFromNvRam                        */
/*                                                                          */
/*    Description        : This function is invoked to get the Subnet Mask  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Local Subnet Mask                                */
/****************************************************************************/

UINT4
IssGetSubnetMaskFromNvRam (VOID)
{
    return (OSIX_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IpGetIfIndexFromPort                             */
/*                                                                          */
/*    Description        : This function is used to get the ifindex for     */
/*                         the given port.                                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : OSIX_SUCCESS                                     */
/****************************************************************************/
#ifdef __STDC__
UINT4
IpGetIfIndexFromPort (UINT2 u2Port)
#else
UINT4
IpGetIfIndexFromPort (u2Port)
     UINT2               u2Port;
#endif
{
    UNUSED_PARAM (u2Port);
    return (OSIX_SUCCESS);
}

/****************************************************************************
 *  Function    :  nmhGetFirstIndexFsSyslogConfigTable
 *  Input       :  The Indices
 *                  FsSyslogConfigModule
 *  Output      :  The Get First Routines gets the
 *                    Lexicographicaly First Entry from the Table.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/

INT1
nmhGetFirstIndexFsSyslogConfigTable (INT4 *pi4FsSyslogConfigModule)
{
    UNUSED_PARAM (pi4FsSyslogConfigModule);
    return SNMP_SUCCESS;
}

#endif
