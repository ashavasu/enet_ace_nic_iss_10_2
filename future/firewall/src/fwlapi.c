/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlapi.c,v 1.1 2011/05/30 15:09:04 siva Exp $
 *
 * Description: File containing routines related to TFTP ALG 
 * configuration of firewall module
 * *******************************************************************/
#ifndef _FWLAPI_C_
#define _FWLAPI_C_

#include "fwlinc.h"

/***************************************************************************
 * Function Name : FwlApiSetFeatures
 *
 * Description   : This Function is used to set the Fwl Feature Mask value
 *
 * Input         : UINT4, UINT4
 *
 * Output        : None
 * 
 * Returns       : Void
 *
 **************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlApiSetFeatures (UINT4 v4featureMaskVal, UINT4 v6featureMaskVal)
#else
PUBLIC VOID
FwlApiSetFeatures (v4featureMaskVal, v6featureMaskVal)
     UINT4               v4featureMaskVal;
     UINT4               v6featureMaskVal;
#endif
{
    gu4Fwlv4FeatureMask = v4featureMaskVal;
    gu4Fwlv6FeatureMask = v6featureMaskVal;
}
#endif /* _FWLAPI_C_ */
