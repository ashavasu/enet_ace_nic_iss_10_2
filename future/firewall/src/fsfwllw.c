/********************************************************************
* Copyright (C) 2010 Aricent Inc.  All Rights Reserved
*
* $Id: fsfwllw.c,v 1.42 2017/06/08 11:38:47 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "fwlinc.h"
# include  "fssnmp.h"
#include "fsmpfwlcli.h"
#ifdef FLOWMGR_WANTED
#include "flowmgr.h"
#endif

PRIVATE UINT4       gau4IfaceList[FWL_MAX_NUM_OF_IF];
#define FWL_CMD_STR_CAT_SIZE(x) (FWL_CMD_DEF_BUF_SIZE - STRLEN(x) - FWL_ONE)
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlGlobalMasterControlSwitch
 Input       :  The Indices

                The Object 
                retValFwlGlobalMasterControlSwitch
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalMasterControlSwitch (INT4 *pi4RetValFwlGlobalMasterControlSwitch)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Master Control Switch GET Entry\n");

    *pi4RetValFwlGlobalMasterControlSwitch = (INT4) gu1FirewallStatus;

    FWL_DBG (FWL_DBG_EXIT, "\n  Master Control Switch GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalICMPControlSwitch
 Input       :  The Indices

                The Object 
                retValFwlGlobalICMPControlSwitch
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalICMPControlSwitch (INT4 *pi4RetValFwlGlobalICMPControlSwitch)
{
    FWL_DBG (FWL_DBG_ENTRY, "\nICMP Control Switch GET Entry\n");

    *pi4RetValFwlGlobalICMPControlSwitch = (INT4) gFwlAclInfo.u1IcmpGenerate;

    FWL_DBG (FWL_DBG_EXIT, "\n ICMP Control Switch GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalIpSpoofFiltering
 Input       :  The Indices

                The Object 
                retValFwlGlobalIpSpoofFiltering
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalIpSpoofFiltering (INT4 *pi4RetValFwlGlobalIpSpoofFiltering)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Ip Spoof Filtering GET Entry\n");

    *pi4RetValFwlGlobalIpSpoofFiltering = (INT4)
        gFwlAclInfo.u1IpSpoofFilteringEnable;

    FWL_DBG (FWL_DBG_EXIT, "\n Ip Spoof Filtering GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalSrcRouteFiltering
 Input       :  The Indices

                The Object 
                retValFwlGlobalSrcRouteFiltering
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalSrcRouteFiltering (INT4 *pi4RetValFwlGlobalSrcRouteFiltering)
{
    /* Deprecated Object - Default Value is returned */

    *pi4RetValFwlGlobalSrcRouteFiltering = FWL_ENABLE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalTinyFragmentFiltering
 Input       :  The Indices

                The Object 
                retValFwlGlobalTinyFragmentFiltering
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalTinyFragmentFiltering (INT4
                                      *pi4RetValFwlGlobalTinyFragmentFiltering)
{
    /* Deprecated Object - Default Value is returned */

    *pi4RetValFwlGlobalTinyFragmentFiltering = FWL_ENABLE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalTcpIntercept
 Input       :  The Indices

                The Object 
                retValFwlGlobalTcpIntercept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalTcpIntercept (INT4 *pi4RetValFwlGlobalTcpIntercept)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n TCP Intercept GET Entry\n");

    *pi4RetValFwlGlobalTcpIntercept = (INT4) gFwlAclInfo.u1TcpInterceptEnable;

    FWL_DBG (FWL_DBG_EXIT, "\n TCP Intercept GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalTrap
 Input       :  The Indices

                The Object 
                retValFwlGlobalTrap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalTrap (INT4 *pi4RetValFwlGlobalTrap)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Trap Enable GET Entry\n");

    *pi4RetValFwlGlobalTrap = (INT4) gFwlAclInfo.u1TrapEnable;

    FWL_DBG (FWL_DBG_EXIT, "\n Trap Enable GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalTrace
 Input       :  The Indices

                The Object 
                retValFwlGlobalTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalTrace (INT4 *pi4RetValFwlGlobalTrace)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Trace Enable GET Entry\n");

    *pi4RetValFwlGlobalTrace = (INT4) gFwlAclInfo.u4FwlTrc;

    FWL_DBG (FWL_DBG_EXIT, "\n Trace Enable GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalDebug
 Input       :  The Indices

                The Object 
                retValFwlGlobalDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalDebug (INT4 *pi4RetValFwlGlobalDebug)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Debug Enable GET Entry\n");

    if ((gFwlAclInfo.u4FwlDbg == FWL_DISABLE) ||
        (gFwlAclInfo.u4FwlDbg == FWL_ZERO))
    {
        *pi4RetValFwlGlobalDebug = FWL_DISABLE;
    }
    else
    {
        *pi4RetValFwlGlobalDebug = FWL_ENABLE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Debug Enable GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalMaxFilters
 Input       :  The Indices

                The Object 
                retValFwlGlobalMaxFilters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalMaxFilters (INT4 *pi4RetValFwlGlobalMaxFilters)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Max Filter Configuration - GET Entry \n");

    *pi4RetValFwlGlobalMaxFilters = (INT4) FWL_MAX_NUM_OF_FILTERS;

    FWL_DBG (FWL_DBG_EXIT, "\n Max Filter Configuration - GET Exit \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalMaxRules
 Input       :  The Indices

                The Object 
                retValFwlGlobalMaxRules
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalMaxRules (INT4 *pi4RetValFwlGlobalMaxRules)
{

    FWL_DBG (FWL_DBG_ENTRY, "\n Max Filter Configuration - GET Entry \n");

    *pi4RetValFwlGlobalMaxRules = (INT4) FWL_MAX_NUM_OF_RULES;

    FWL_DBG (FWL_DBG_EXIT, "\n Max Filter Configuration - GET Exit \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalUrlFiltering
 Input       :  The Indices

                The Object 
                retValFwlGlobalUrlFiltering
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalUrlFiltering (INT4 *pi4RetValFwlGlobalUrlFiltering)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Url Filtering Enable - GET Entry \n");

    *pi4RetValFwlGlobalUrlFiltering = (INT4) gFwlAclInfo.u1UrlFilteringEnable;

    FWL_DBG (FWL_DBG_EXIT, "\n Url Filtering Enable - GET Exit \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalNetBiosFiltering
 Input       :  The Indices

 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalNetBiosFiltering (INT4 *pi4RetValFwlGlobalNetBiosFiltering)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n NetBios Filtering GET Entry\n");

    *pi4RetValFwlGlobalNetBiosFiltering =
        (INT4) gFwlAclInfo.u1NetBiosFilteringEnable;

    FWL_DBG (FWL_DBG_EXIT, "\n NetBios Filtering GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalNetBiosLan2Wan
 Input       :  The Indices

 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalNetBiosLan2Wan (INT4 *pi4RetValFwlGlobalNetBiosFiltering)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n NetBios Lan to Wan GET Entry\n");

    *pi4RetValFwlGlobalNetBiosFiltering =
        (INT4) gFwlAclInfo.u1NetBiosLan2WanEnable;

    FWL_DBG (FWL_DBG_EXIT, "\n NetBios Lan to Wan GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFwlGlobalICMPv6ControlSwitch
 *  Input       :  The Indices
 *                 The Object
 *                 retValFwlGlobalICMPv6ControlSwitch
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFwlGlobalICMPv6ControlSwitch (INT4 *pi4RetValFwlGlobalICMPv6ControlSwitch)
{
    FWL_DBG (FWL_DBG_ENTRY, "\nICMPv6 Control Switch GET Entry\n");

    *pi4RetValFwlGlobalICMPv6ControlSwitch =
        (INT4) gFwlAclInfo.u1Icmpv6Generate;

    FWL_DBG (FWL_DBG_EXIT, "\n ICMPv6 Control Switch GET Exit\n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function    :  nmhGetFwlGlobalIpv6SpoofFiltering
 *  Input       :  The Indices
 *                 The Object
 *                 retValFwlGlobalIpv6SpoofFiltering
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFwlGlobalIpv6SpoofFiltering (INT4 *pi4RetValFwlGlobalIpv6SpoofFiltering)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Ipv6 Spoof Filtering GET Entry\n");

    *pi4RetValFwlGlobalIpv6SpoofFiltering = (INT4)
        gFwlAclInfo.u1Ipv6SpoofFilteringEnable;

    FWL_DBG (FWL_DBG_EXIT, "\n Ipv6 Spoof Filtering GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalLogFileSize
 Input       :  The Indices

                The Object
                retValFwlGlobalLogFileSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFwlGlobalLogFileSize (UINT4 *pu4RetValFwlGlobalLogFileSize)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Firewall Log filesize GET Entry\n");

    *pu4RetValFwlGlobalLogFileSize = gu4FwlMaxLogSize;

    FWL_DBG (FWL_DBG_ENTRY, "\n Firewall Log filesize GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalLogSizeThreshold
 Input       :  The Indices

                The Object
                retValFwlGlobalLogSizeThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFwlGlobalLogSizeThreshold (UINT4 *pu4RetValFwlGlobalLogSizeThreshold)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Firewall Logsize threshold GET Entry\n");

    *pu4RetValFwlGlobalLogSizeThreshold = gu4FwlLogSizeThreshold;

    FWL_DBG (FWL_DBG_ENTRY, "\n Firewall Logsize threshold GET Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalIdsLogSize
 Input       :  The Indices

                The Object
                retValFwlGlobalIdsLogSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalIdsLogSize (UINT4 *pu4RetValFwlGlobalIdsLogSize)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n IDS Log filesize GET Entry\n");

    *pu4RetValFwlGlobalIdsLogSize = gu4IdsMaxLogSize;

    FWL_DBG (FWL_DBG_ENTRY, "\n IDS Log filesize GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalIdsLogThreshold
 Input       :  The Indices

                The Object
                retValFwlGlobalIdsLogThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalIdsLogThreshold (UINT4 *pu4RetValFwlGlobalIdsLogThreshold)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n IDS Logsize threshold GET Entry\n");

    *pu4RetValFwlGlobalIdsLogThreshold = gu4IdsLogThreshold;

    FWL_DBG (FWL_DBG_ENTRY, "\n IDS Logsize threshold GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalIdsVersionInfo
 Input       :  The Indices

                The Object 
                retValFwlGlobalIdsVersionInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalIdsVersionInfo (tSNMP_OCTET_STRING_TYPE *
                               pRetValFwlGlobalIdsVersionInfo)
{
    tIdsGetInfo         IdsGetInfo;

    MEMSET (&IdsGetInfo, FWL_ZERO, sizeof (tIdsGetInfo));
    pRetValFwlGlobalIdsVersionInfo->i4_Length = FWL_ZERO;

    if (SecIdsGetInfo (ISS_IDS_VERSION_REQ, &IdsGetInfo) != IDS_FAILURE)
    {
        pRetValFwlGlobalIdsVersionInfo->i4_Length = (INT4)
            STRLEN (IdsGetInfo.au1Version);
        MEMCPY (pRetValFwlGlobalIdsVersionInfo->pu1_OctetList,
                IdsGetInfo.au1Version,
                pRetValFwlGlobalIdsVersionInfo->i4_Length);

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalReloadIds
 Input       :  The Indices

                The Object 
                retValFwlGlobalReloadIds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalReloadIds (INT4 *pi4RetValFwlGlobalReloadIds)
{
    *pi4RetValFwlGlobalReloadIds = FWL_ZERO;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalIdsStatus
 Input       :  The Indices

                The Object 
                retValFwlGlobalIdsStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalIdsStatus (INT4 *pi4RetValFwlGlobalIdsStatus)
{
    *pi4RetValFwlGlobalIdsStatus = (INT4) gu4IdsStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlGlobalLoadIdsRules
 Input       :  The Indices

                The Object 
                retValFwlGlobalLoadIdsRules
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlGlobalLoadIdsRules (INT4 *pi4RetValFwlGlobalLoadIdsRules)
{
    *pi4RetValFwlGlobalLoadIdsRules = gi4IdsRulesStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlGlobalMasterControlSwitch
 Input       :  The Indices

                The Object 
                setValFwlGlobalMasterControlSwitch
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlGlobalMasterControlSwitch (INT4 i4SetValFwlGlobalMasterControlSwitch)
{
    UINT4               u4IfaceNum = FWL_ZERO;
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tIfaceInfo         *pIfaceNode = NULL;
    UINT1               au1FwlMsg[FWL_CMD_DEF_BUF_SIZE] = { FWL_ZERO };

    FWL_DBG (FWL_DBG_ENTRY, "\n Master Control Switch SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    /* If Firewall is disabled all dynamicefilters must be deleted.
     * All timers must be stopped and and the counters used for Tcp Intercept
     * Logging must be initialised.
     */
    if ((i4SetValFwlGlobalMasterControlSwitch == FWL_DISABLE) &&
        (gu1FirewallStatus == FWL_ENABLE))
    {
        FwlTmrDeleteAllTimers ();
        gFwlAclInfo.u2SynPktsAllowed = FWL_ZERO;
        gFwlAclInfo.u2RtrSynPktsAllowed = FWL_ZERO;
        gFwlAclInfo.u2UdpPktsAllowed = FWL_ZERO;
        gFwlAclInfo.u2IcmpPktsAllowed = FWL_ZERO;
    }
    else if ((i4SetValFwlGlobalMasterControlSwitch == FWL_ENABLE) &&
             (gu1FirewallStatus == FWL_DISABLE))
    {
        /* When firewall is enabled then delete all the entries 
         * in data plane for the interfaces which are set to 
         * be external */
        for (u4IfaceNum = FWL_ONE; u4IfaceNum < FWL_MAX_NUM_OF_IF; u4IfaceNum++)
        {
            pIfaceNode = gFwlAclInfo.apIfaceList[u4IfaceNum];
            if ((pIfaceNode != NULL) &&
                (pIfaceNode->u1RowStatus == FWL_ACTIVE) &&
                (pIfaceNode->u1IfaceType == FWL_EXTERNAL_IF))
            {
                FL_FWL_DEL_IFINDEX_HW_ENTRIES (u4IfaceNum);
            }
        }

        /* If Firewall is enabled, the TCP Intercept timer  
         * must be started if the coressponding control switch
         * is enabled.
         */
        if (gFwlAclInfo.u1TcpInterceptEnable == FWL_ENABLE)
        {
            /* Start the timer if the TCP Intercept Control Switch is enabled */
            FwlTmrSetTimer (&gFwlAclInfo.TcpInterceptTimer,
                            FWL_TCP_INTERCEPT_TIMER,
                            gFwlAclInfo.u2TcpInterceptTimeOut);

        }
        /* Start timer if UDP Intercept Control Switch is enabled */
        if (gFwlAclInfo.u1UdpFloodInspect == FWL_ENABLE)
        {
            /* Initialise the Udp Packet counters */
            gFwlAclInfo.u2UdpPktsAllowed = FWL_ZERO;

            /* Start the timer if the TCP Intercept Control Switch is enabled */
            FwlTmrSetTimer (&gFwlAclInfo.UdpInterceptTimer,
                            FWL_UDP_INTERCEPT_TIMER,
                            gFwlAclInfo.u2UdpInterceptTimeOut);
        }
        /* Start timer if ICMP Intercept Control Switch is enabled */
        if (gFwlAclInfo.u1IcmpFloodInspect == FWL_ENABLE)
        {
            /* Initialise the Icmp Packet counters */
            gFwlAclInfo.u2IcmpPktsAllowed = FWL_ZERO;

            /* Start the timer if the TCP Intercept Control Switch is enabled */
            FwlTmrSetTimer (&gFwlAclInfo.IcmpInterceptTimer,
                            FWL_ICMP_INTERCEPT_TIMER,
                            gFwlAclInfo.u2IcmpInterceptTimeOut);
        }
        /* Start stateful inspection timer */
        FwlTmrSetTimer (&gFwlStatefulTmr, FWL_IDLE_TIMER, FWL_IDLE_TIMEOUT);
    }

    gu1FirewallStatus = (UINT1) i4SetValFwlGlobalMasterControlSwitch;

    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Master Control Switch Value is set to %d\n",
                  i4SetValFwlGlobalMasterControlSwitch);

    FWL_DBG (FWL_DBG_EXIT, "\n  Master Control Switch SET Exit \n");
    /*Logs for cli */
    if (i4SetValFwlGlobalMasterControlSwitch == FWL_ENABLE)
    {
        SPRINTF ((CHR1 *) au1FwlMsg, " \"%s\" %s ", "enable", FWL_CMD_EXEC_MSG);
        FwlLogMessage (FWL_COMM_CONF, FWL_ZERO, NULL, FWL_LOG_MUST,
                       FWLLOG_INFO_LEVEL, au1FwlMsg);
    }
    else if (i4SetValFwlGlobalMasterControlSwitch == FWL_DISABLE)
    {
        SPRINTF ((CHR1 *) au1FwlMsg, " \"%s\" %s ",
                 "disable", FWL_CMD_EXEC_MSG);
        FwlLogMessage (FWL_COMM_CONF, FWL_ZERO, NULL, FWL_LOG_MUST,
                       FWLLOG_INFO_LEVEL, au1FwlMsg);
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlGlobalMasterControlSwitch,
                          u4SeqNum, FALSE, FwlLock, FwlUnLock, FWL_ZERO,
                          SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                      i4SetValFwlGlobalMasterControlSwitch));

    return SNMP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlAclGetCmdString                               */
/*                                                                          */
/*    Description        : Retrives the command string for Access-list      */
/*                                                                          */
/*                                                                          */
/*    Input(s)           : u4IfaceNum   --  Interface Number                */
/*                         i4Direction  --  Direction of the list in/out    */
/*                         pu1Aclname   --  The Access list name            */
/*                                                                          */
/*    Output(s)          : pu1CmdStr    -- The Command String               */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

VOID
FwlGetAclCmdString (UINT4 u4IfaceNum, UINT1 *pu1AclName, INT4 i4Direction,
                    UINT1 *pu1CmdStr)
{

    tAclInfo           *pAclNode = (tAclInfo *) NULL;
    tIfaceInfo         *pIfaceNode = (tIfaceInfo *) NULL;
    tRuleInfo          *pRuleNode = (tRuleInfo *) NULL;
    UINT1               au1TempStr[FWL_CMD_DEF_BUF_SIZE] = { FWL_ZERO };
    UINT2               u2Index = FWL_ZERO;

    /*Set String zero */
    pu1CmdStr[FWL_INDEX_0] = FWL_ZERO;

    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode != NULL)
    {
        if (i4Direction == FWL_DIRECTION_IN)
        {
            pAclNode = FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                                pu1AclName);
            if (pAclNode == NULL)
            {
                pAclNode = FwlDbaseSearchAclFilter
                    (&pIfaceNode->inIPv6FilterList, pu1AclName);
            }
        }
        else
        {
            pAclNode = FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                                pu1AclName);
            if (pAclNode == NULL)
            {
                pAclNode = FwlDbaseSearchAclFilter
                    (&pIfaceNode->outIPv6FilterList, pu1AclName);
            }
        }

        if (pAclNode == NULL)
        {
            return;
        }

        pRuleNode = FwlDbaseSearchRule (pAclNode->au1FilterName);
        if (pRuleNode == NULL)
        {
            return;
        }
        /* Get the Acl Name and Direction */
        au1TempStr[FWL_INDEX_0] = FWL_ZERO;
        SPRINTF ((CHR1 *) au1TempStr, "access-list  %s %s ",
                 pAclNode->au1FilterName,
                 i4Direction == FWL_DIRECTION_IN ? "in" : "out");
        STRNCAT (pu1CmdStr, au1TempStr, FWL_CMD_STR_CAT_SIZE (pu1CmdStr));

        au1TempStr[FWL_INDEX_0] = FWL_ZERO;
        u2Index = FWL_ZERO;
        /*Get names of all filters */
        while ((u2Index < FWL_MAX_FILTERS_IN_RULE)
               && (pRuleNode->apFilterInfo[u2Index] != (tFilterInfo *) NULL))
        {
            SPRINTF ((CHR1 *) au1TempStr, "%s ",
                     pRuleNode->apFilterInfo[u2Index]->au1FilterName);
            STRNCAT (pu1CmdStr, au1TempStr, FWL_CMD_STR_CAT_SIZE (pu1CmdStr));
            u2Index++;
        }
        /*Get the Acl Action */
        au1TempStr[FWL_INDEX_0] = FWL_ZERO;
        SPRINTF ((CHR1 *) au1TempStr, "%s ",
                 pAclNode->u1Action == FWL_PERMIT ? "permit" : "deny");
        STRNCAT (pu1CmdStr, au1TempStr, FWL_CMD_STR_CAT_SIZE (pu1CmdStr));

        /*Get the priority */
        au1TempStr[FWL_INDEX_0] = FWL_ZERO;
        SPRINTF ((CHR1 *) au1TempStr, "%d ", pAclNode->u2SeqNum);
        STRNCAT (pu1CmdStr, au1TempStr, FWL_CMD_STR_CAT_SIZE (pu1CmdStr));

        /* Get the logging kind */
        au1TempStr[FWL_INDEX_0] = FWL_ZERO;
        if (FWL_LOG_BRF == pAclNode->u1LogTrigger)
        {
            STRCPY (au1TempStr, "log brief");
        }
        else if (FWL_LOG_DTL == pAclNode->u1LogTrigger)
        {
            STRCPY (au1TempStr, "log detail");
        }
        else if (FWL_LOG_NONE == pAclNode->u1LogTrigger)
        {
            STRCPY (au1TempStr, "log none");
        }
        STRNCAT (pu1CmdStr, au1TempStr, FWL_CMD_STR_CAT_SIZE (pu1CmdStr));

        /* Get the fragment action */
        au1TempStr[FWL_INDEX_0] = FWL_ZERO;
        SPRINTF ((CHR1 *) au1TempStr, " fragment %s ",
                 pAclNode->u1FragAction == FWL_PERMIT ? "permit" : "deny");
        STRNCAT (pu1CmdStr, au1TempStr, FWL_CMD_STR_CAT_SIZE (pu1CmdStr));
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlGetFilterCmdString                            */
/*                                                                          */
/*    Description        : Retrives the command string for Access-list      */
/*                                                                          */
/*                                                                          */
/*    Input(s)           : pFilterNode   --  Filter                         */
/*                                                                          */
/*    Output(s)          : pu1CmdStr    -- The Command String               */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

VOID
FwlGetFilterCmdString (tFilterInfo * pFilterNode, INT1 *pu1CmdStr)
{
    UINT1               au1TempStr[FWL_CMD_DEF_BUF_SIZE] = { FWL_ZERO };
    UINT4               u4ProtoPortAvailable = FWL_FALSE;

    /* set command string to NULL */
    pu1CmdStr[FWL_INDEX_0] = FWL_ZERO;
    SPRINTF ((CHR1 *) au1TempStr, "filter add %s ", pFilterNode->au1FilterName);
    STRNCAT (pu1CmdStr, au1TempStr, FWL_CMD_STR_CAT_SIZE (pu1CmdStr));

    au1TempStr[FWL_INDEX_0] = FWL_ZERO;
    SPRINTF ((CHR1 *) au1TempStr, "%s", (CHR1 *) pFilterNode->au1SrcAddr);

    STRNCAT (pu1CmdStr, au1TempStr, FWL_CMD_STR_CAT_SIZE (pu1CmdStr));
    STRNCAT (pu1CmdStr, " ", FWL_CMD_STR_CAT_SIZE (pu1CmdStr));
    au1TempStr[FWL_INDEX_0] = FWL_ZERO;

    SPRINTF ((CHR1 *) au1TempStr, "%s", (CHR1 *) pFilterNode->au1DestAddr);

    STRNCAT (pu1CmdStr, au1TempStr, FWL_CMD_STR_CAT_SIZE (pu1CmdStr));
    STRNCAT (pu1CmdStr, " ", FWL_CMD_STR_CAT_SIZE (pu1CmdStr));
    au1TempStr[FWL_INDEX_0] = FWL_ZERO;

    /* this checks  whether the protocol value is one of the 
     * already defined one.
     */
    switch (pFilterNode->u1Proto)
    {
        case FWL_ICMP:
            STRNCAT (au1TempStr, "icmp ", FWL_CMD_STR_CAT_SIZE (au1TempStr));
            u4ProtoPortAvailable = FWL_FALSE;
            break;
        case FWL_IGMP:
            STRNCAT (au1TempStr, "igmp ", FWL_CMD_STR_CAT_SIZE (au1TempStr));
            u4ProtoPortAvailable = FWL_TRUE;
            break;
        case FWL_EGP:
            STRNCAT (au1TempStr, "egp ", FWL_CMD_STR_CAT_SIZE (au1TempStr));
            u4ProtoPortAvailable = FWL_FALSE;
            break;
        case FWL_GGP:
            STRNCAT (au1TempStr, "ggp ", FWL_CMD_STR_CAT_SIZE (au1TempStr));
            u4ProtoPortAvailable = FWL_FALSE;
            break;
        case FWL_IGP:
            STRNCAT (au1TempStr, "igp ", FWL_CMD_STR_CAT_SIZE (au1TempStr));
            u4ProtoPortAvailable = FWL_FALSE;
            break;
        case FWL_IP:
            STRNCAT (au1TempStr, "ip ", FWL_CMD_STR_CAT_SIZE (au1TempStr));
            u4ProtoPortAvailable = FWL_FALSE;
            break;
        case FWL_TCP:
            STRNCAT (au1TempStr, "tcp ", FWL_CMD_STR_CAT_SIZE (au1TempStr));
            u4ProtoPortAvailable = FWL_TRUE;
            break;
        case FWL_UDP:
            STRNCAT (au1TempStr, "udp ", FWL_CMD_STR_CAT_SIZE (au1TempStr));
            u4ProtoPortAvailable = FWL_TRUE;
            break;
        case FWL_NVP:
            STRNCAT (au1TempStr, "nvp ", FWL_CMD_STR_CAT_SIZE (au1TempStr));
            u4ProtoPortAvailable = FWL_FALSE;
            break;
        case FWL_IRTP:
            STRNCAT (au1TempStr, "irtp ", FWL_CMD_STR_CAT_SIZE (au1TempStr));
            u4ProtoPortAvailable = FWL_FALSE;
            break;
        case FWL_IDPR:
            STRNCAT (au1TempStr, "idrp ", FWL_CMD_STR_CAT_SIZE (au1TempStr));
            u4ProtoPortAvailable = FWL_FALSE;
            break;
        case FWL_RSVP:
            STRNCAT (au1TempStr, "rvsp ", FWL_CMD_STR_CAT_SIZE (au1TempStr));
            u4ProtoPortAvailable = FWL_FALSE;
            break;
        case FWL_MHRP:
            STRNCAT (au1TempStr, "mhrp ", FWL_CMD_STR_CAT_SIZE (au1TempStr));
            u4ProtoPortAvailable = FWL_FALSE;
            break;
        case FWL_IGRP:
            STRNCAT (au1TempStr, "igrp ", FWL_CMD_STR_CAT_SIZE (au1TempStr));
            u4ProtoPortAvailable = FWL_FALSE;
            break;
        case FWL_OSPFIGP:
            STRNCAT (au1TempStr, "ospfigp ", FWL_CMD_STR_CAT_SIZE (au1TempStr));
            u4ProtoPortAvailable = FWL_FALSE;
            break;
        case FWL_PIM:
            STRNCAT (au1TempStr, "pim ", FWL_CMD_STR_CAT_SIZE (au1TempStr));
            u4ProtoPortAvailable = FWL_FALSE;
            break;
        case FWL_GRE_PPTP:
            STRNCAT (au1TempStr, "pptp ", FWL_CMD_STR_CAT_SIZE (au1TempStr));
            u4ProtoPortAvailable = FWL_FALSE;
            break;
        case FWL_DEFAULT_PROTO:
            STRNCAT (au1TempStr, "any ", FWL_CMD_STR_CAT_SIZE (au1TempStr));
            u4ProtoPortAvailable = FWL_FALSE;
            break;
        default:
            SPRINTF ((CHR1 *) au1TempStr, "other %d ", pFilterNode->u1Proto);
            u4ProtoPortAvailable = FWL_FALSE;
            break;
    }

    STRNCAT (pu1CmdStr, au1TempStr, FWL_CMD_STR_CAT_SIZE (pu1CmdStr));
    au1TempStr[FWL_INDEX_0] = FWL_ZERO;
    if (u4ProtoPortAvailable == FWL_FALSE)
    {
        return;
    }
    /* Gets the Source and Destination Ports */
    SPRINTF ((CHR1 *) au1TempStr, "srcport %s ", pFilterNode->au1SrcPort);
    STRNCAT (pu1CmdStr, au1TempStr, FWL_CMD_STR_CAT_SIZE (pu1CmdStr));
    au1TempStr[FWL_INDEX_0] = FWL_ZERO;
    SPRINTF ((CHR1 *) au1TempStr, "destport %s ", pFilterNode->au1DestPort);
    STRNCAT (pu1CmdStr, au1TempStr, FWL_CMD_STR_CAT_SIZE (pu1CmdStr));
    return;
}

/****************************************************************************
 Function    :  nmhSetFwlGlobalICMPControlSwitch
 Input       :  The Indices

                The Object 
                setValFwlGlobalICMPControlSwitch
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlGlobalICMPControlSwitch (INT4 i4SetValFwlGlobalICMPControlSwitch)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY, "\n ICMP Control Switch SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    gFwlAclInfo.u1IcmpGenerate = (UINT1) i4SetValFwlGlobalICMPControlSwitch;

    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n ICMP Control Switch Value is set to %d\n",
                  i4SetValFwlGlobalICMPControlSwitch);

    FWL_DBG (FWL_DBG_EXIT, "\n  ICMP control Switch SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlGlobalICMPControlSwitch, u4SeqNum,
                          FALSE, FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                      i4SetValFwlGlobalICMPControlSwitch));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlGlobalIpSpoofFiltering
 Input       :  The Indices

                The Object 
                setValFwlGlobalIpSpoofFiltering
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlGlobalIpSpoofFiltering (INT4 i4SetValFwlGlobalIpSpoofFiltering)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY,
             "\n IP Spoof Filtering Control Switch SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    gFwlAclInfo.u1IpSpoofFilteringEnable = (UINT1)
        i4SetValFwlGlobalIpSpoofFiltering;

    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n IP Spoof Filtering Control Switch Value is set to %d\n",
                  i4SetValFwlGlobalIpSpoofFiltering);

    FWL_DBG (FWL_DBG_EXIT, "\n Ip Spoof Filtering control Switch SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlGlobalIpSpoofFiltering, u4SeqNum,
                          FALSE, FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFwlGlobalIpSpoofFiltering));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlGlobalSrcRouteFiltering
 Input       :  The Indices

                The Object 
                setValFwlGlobalSrcRouteFiltering
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlGlobalSrcRouteFiltering (INT4 i4SetValFwlGlobalSrcRouteFiltering)
{
    /* Deprecated Object */

    UNUSED_PARAM (i4SetValFwlGlobalSrcRouteFiltering);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlGlobalTinyFragmentFiltering
 Input       :  The Indices

                The Object 
                setValFwlGlobalTinyFragmentFiltering
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlGlobalTinyFragmentFiltering (INT4
                                      i4SetValFwlGlobalTinyFragmentFiltering)
{
    /* Deprecated Object */

    UNUSED_PARAM (i4SetValFwlGlobalTinyFragmentFiltering);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlGlobalTcpIntercept
 Input       :  The Indices

                The Object 
                setValFwlGlobalTcpIntercept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlGlobalTcpIntercept (INT4 i4SetValFwlGlobalTcpIntercept)
{
    UINT4               u4TmrInterval = FWL_ZERO;
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY, "\n TCP Intercept SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (gu1FirewallStatus == FWL_DISABLE)
    {
        gFwlAclInfo.u1TcpInterceptEnable = (UINT1)
            i4SetValFwlGlobalTcpIntercept;
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlGlobalTcpIntercept, u4SeqNum,
                              FALSE, FwlLock, FwlUnLock, FWL_ZERO,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFwlGlobalTcpIntercept));
        return SNMP_SUCCESS;
    }
    if ((i4SetValFwlGlobalTcpIntercept == FWL_ENABLE) &&
        (gFwlAclInfo.u1TcpInterceptEnable == FWL_DISABLE))
    {
        u4TmrInterval = (UINT4) (gFwlAclInfo.u2TcpInterceptTimeOut);
        gFwlAclInfo.u2SynPktsAllowed = FWL_ZERO;
        gFwlAclInfo.u2RtrSynPktsAllowed = FWL_ZERO;
        gFwlAclInfo.u2UdpPktsAllowed = FWL_ZERO;
        gFwlAclInfo.u2IcmpPktsAllowed = FWL_ZERO;
        /*Start the Timer again */
        FwlTmrSetTimer (&gFwlAclInfo.TcpInterceptTimer, FWL_TCP_INTERCEPT_TIMER,
                        u4TmrInterval);
    }
    else if ((i4SetValFwlGlobalTcpIntercept == FWL_DISABLE) &&
             (gFwlAclInfo.u1TcpInterceptEnable == FWL_ENABLE))
    {
        /* Stop the Timer */
        FwlTmrDeleteTimer (&gFwlAclInfo.TcpInterceptTimer);
        /* Reset the Packet counter for TCP */
        gFwlAclInfo.u2SynPktsAllowed = FWL_ZERO;
        gFwlAclInfo.u2RtrSynPktsAllowed = FWL_ZERO;
    }
    gFwlAclInfo.u1TcpInterceptEnable = (UINT1) i4SetValFwlGlobalTcpIntercept;

    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n TCP Intercept Switch Value is set to %d\n",
                  i4SetValFwlGlobalTcpIntercept);

    FWL_DBG (FWL_DBG_EXIT, "\n TCP Intercept SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlGlobalTcpIntercept, u4SeqNum,
                          FALSE, FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFwlGlobalTcpIntercept));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlGlobalTrap
 Input       :  The Indices

                The Object 
                setValFwlGlobalTrap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlGlobalTrap (INT4 i4SetValFwlGlobalTrap)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY, "\n Trap Control Switch SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    gFwlAclInfo.u1TrapEnable = (UINT1) i4SetValFwlGlobalTrap;

    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Trap Control Switch Value is set to %d\n",
                  i4SetValFwlGlobalTrap);

    FWL_DBG (FWL_DBG_EXIT, "\n Trap Control Switch SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlGlobalTrap, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFwlGlobalTrap));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlGlobalTrace
 Input       :  The Indices

                The Object 
                setValFwlGlobalTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlGlobalTrace (INT4 i4SetValFwlGlobalTrace)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY, "\n Trace Control Switch SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    gFwlAclInfo.u4FwlTrc = (UINT4) i4SetValFwlGlobalTrace;

    /*The configured threshold value is sent to ids when the trap
     *is enabled  and zero is sent when the trap is disabled*/
    if (gFwlAclInfo.u1TrapEnable == FWL_ENABLE)
    {
        SecIdsSetInfo (ISS_IDS_SET_PKT_DROP_THRESH,
                       (UINT4) gFwlAclInfo.i4TrapThreshold);
    }
    else
    {
        SecIdsSetInfo (ISS_IDS_SET_PKT_DROP_THRESH, FWL_ZERO);
    }

    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Trace Control Switch Value is set to %d\n",
                  i4SetValFwlGlobalTrace);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlGlobalTrace, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFwlGlobalTrace));
    FWL_DBG (FWL_DBG_EXIT, "\n Trace Control Switch SET Exit \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlGlobalDebug
 Input       :  The Indices

                The Object 
                setValFwlGlobalDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlGlobalDebug (INT4 i4SetValFwlGlobalDebug)
{

    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY, "\n Debug Control Switch SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (i4SetValFwlGlobalDebug == FWL_ENABLE)
    {
        gFwlAclInfo.u4FwlDbg = FWL_SET_ALL_BITS;
    }
    else
    {
        gFwlAclInfo.u4FwlDbg = FWL_ZERO;
    }

    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Debug Control Switch Value is set to %d\n",
                  i4SetValFwlGlobalDebug);

    FWL_DBG (FWL_DBG_EXIT, "\n Debug Control Switch SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlGlobalDebug, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFwlGlobalDebug));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlGlobalUrlFiltering
 Input       :  The Indices

                The Object 
                setValFwlGlobalUrlFiltering
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlGlobalUrlFiltering (INT4 i4SetValFwlGlobalUrlFiltering)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY, "\n Fwl Url Filtering Control Switch SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    gFwlAclInfo.u1UrlFilteringEnable = (UINT1) i4SetValFwlGlobalUrlFiltering;

    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Fwl Url Filtering Control Switch Value is set to %d\n",
                  i4SetValFwlGlobalUrlFiltering);

    FWL_DBG (FWL_DBG_EXIT, "\n Fwl Url Filtering control Switch SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlGlobalUrlFiltering, u4SeqNum,
                          FALSE, FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFwlGlobalUrlFiltering));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlGlobalNetBiosFiltering
 Input       :  The Indices

                The Object 
                setValFwlGlobalIpSpoofFiltering
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlGlobalNetBiosFiltering (INT4 i4SetValFwlGlobalNetBiosFiltering)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY, "\n Net Bios Switch SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    gFwlAclInfo.u1NetBiosFilteringEnable = (UINT1)
        i4SetValFwlGlobalNetBiosFiltering;

    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Net Bios Control Switch Value is set to %d\n",
                  i4SetValFwlGlobalNetBiosFiltering);

    FWL_DBG (FWL_DBG_EXIT, "\n Net Bios Filtering control Switch SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlGlobalNetBiosFiltering, u4SeqNum,
                          FALSE, FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFwlGlobalNetBiosFiltering));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlGlobalNetBiosLan2Wan
 Input       :  The Indices

                The Object 
                setValFwlGlobalIpSpoofFiltering
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlGlobalNetBiosLan2Wan (INT4 i4SetValFwlGlobalNetBiosLan2Wan)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY, "\n Net Bios Lan to Wan Switch SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    gFwlAclInfo.u1NetBiosLan2WanEnable = (UINT1)
        i4SetValFwlGlobalNetBiosLan2Wan;

    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Net Bios Lan to Wan Switch Value is set to %d\n",
                  i4SetValFwlGlobalNetBiosLan2Wan);

    FWL_DBG (FWL_DBG_EXIT, "\n Net Bios Lan to Wan control Switch SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlGlobalNetBiosLan2Wan, u4SeqNum,
                          FALSE, FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFwlGlobalNetBiosLan2Wan));

    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhSetFwlGlobalICMPv6ControlSwitch
 *  Input       :  The Indices
 *                   The Object
 *                   setValFwlGlobalICMPv6ControlSwitch
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/

INT1
nmhSetFwlGlobalICMPv6ControlSwitch (INT4 i4SetValFwlGlobalICMPv6ControlSwitch)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY, "\n ICMPv6 Control Switch SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    gFwlAclInfo.u1Icmpv6Generate = (UINT1) i4SetValFwlGlobalICMPv6ControlSwitch;

    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n ICMPv6 Control Switch Value is set to %d\n",
                  i4SetValFwlGlobalICMPv6ControlSwitch);

    FWL_DBG (FWL_DBG_EXIT, "\n  ICMP control Switch SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlGlobalICMPv6ControlSwitch,
                          u4SeqNum, FALSE, FwlLock, FwlUnLock, FWL_ZERO,
                          SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                      i4SetValFwlGlobalICMPv6ControlSwitch));
    return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function    :  nmhSetFwlGlobalIpv6SpoofFiltering
 *  Input       :  The Indices
 *                   The Object
 *                    setValFwlGlobalIpv6SpoofFiltering
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhSetFwlGlobalIpv6SpoofFiltering (INT4 i4SetValFwlGlobalIpv6SpoofFiltering)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY,
             "\n IPv6 Spoof Filtering Control Switch SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    gFwlAclInfo.u1Ipv6SpoofFilteringEnable = (UINT1)
        i4SetValFwlGlobalIpv6SpoofFiltering;

    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n IPv6 Spoof Filtering Control Switch Value is set to %d\n",
                  i4SetValFwlGlobalIpv6SpoofFiltering);

    FWL_DBG (FWL_DBG_EXIT,
             "\n Ipv6 Spoof Filtering control Switch SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlGlobalIpv6SpoofFiltering, u4SeqNum,
                          FALSE, FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                      i4SetValFwlGlobalIpv6SpoofFiltering));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFwlGlobalLogFileSize
 Input       :  The Indices

                The Object
                setValFwlGlobalLogFileSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetFwlGlobalLogFileSize (UINT4 u4SetValFwlGlobalLogFileSize)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY, "\n Firewall Log filesize SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (gu4FwlMaxLogSize == u4SetValFwlGlobalLogFileSize)
    {
        return SNMP_SUCCESS;
    }
    gu4FwlMaxLogSize = u4SetValFwlGlobalLogFileSize;

    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Firewall Log filesize value is set to %d\n",
                  u4SetValFwlGlobalLogFileSize);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlGlobalLogFileSize, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%p", u4SetValFwlGlobalLogFileSize));
    FWL_DBG (FWL_DBG_ENTRY, "\n Firewall Log filesize SET Exit \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlGlobalLogSizeThreshold
 Input       :  The Indices

                The Object
                setValFwlGlobalLogSizeThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetFwlGlobalLogSizeThreshold (UINT4 u4SetValFwlGlobalLogSizeThreshold)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY, "\n Firewall Logsize threshold SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (gu4FwlLogSizeThreshold == u4SetValFwlGlobalLogSizeThreshold)
    {
        return SNMP_SUCCESS;
    }
    gu4FwlLogSizeThreshold = u4SetValFwlGlobalLogSizeThreshold;

    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Firewall Logsize threshold value is set to %d\n",
                  u4SetValFwlGlobalLogSizeThreshold);

    FWL_DBG (FWL_DBG_ENTRY, "\n IDS Logsize threshold SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlGlobalLogSizeThreshold, u4SeqNum,
                          FALSE, FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%p", u4SetValFwlGlobalLogSizeThreshold));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlGlobalIdsLogSize
 Input       :  The Indices

                The Object
                setValFwlGlobalIdsLogSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlGlobalIdsLogSize (UINT4 u4SetValFwlGlobalIdsLogSize)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    gu4IdsMaxLogSize = u4SetValFwlGlobalIdsLogSize;
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlGlobalIdsLogSize, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%p", u4SetValFwlGlobalIdsLogSize));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlGlobalIdsLogThreshold
 Input       :  The Indices

                The Object
                setValFwlGlobalIdsLogThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlGlobalIdsLogThreshold (UINT4 u4SetValFwlGlobalIdsLogThreshold)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    gu4IdsLogThreshold = u4SetValFwlGlobalIdsLogThreshold;
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlGlobalIdsLogThreshold, u4SeqNum,
                          FALSE, FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%p", u4SetValFwlGlobalIdsLogThreshold));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlGlobalReloadIds
 Input       :  The Indices

                The Object
                setValFwlGlobalReloadIds
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlGlobalReloadIds (INT4 i4SetValFwlGlobalReloadIds)
{
    UNUSED_PARAM (i4SetValFwlGlobalReloadIds);
    if (SecIdsSetInfo (ISS_IDS_RESET_IDS, FWL_ZERO) != IDS_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlGlobalIdsStatus
 Input       :  The Indices

                The Object
                setValFwlGlobalIdsStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlGlobalIdsStatus (INT4 i4SetValFwlGlobalIdsStatus)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    /*Store the current IDS Status before overwriting it */
    gu4PrevIdsStatus = gu4IdsStatus;

    gu4IdsStatus = (UINT4) i4SetValFwlGlobalIdsStatus;
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlGlobalIdsStatus, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFwlGlobalIdsStatus));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlGlobalLoadIdsRules
 Input       :  The Indices

                The Object 
                setValFwlGlobalLoadIdsRules
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlGlobalLoadIdsRules (INT4 i4SetValFwlGlobalLoadIdsRules)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    if (i4SetValFwlGlobalLoadIdsRules == IDS_LOAD_RULES)
    {
        if (SecIdsSetInfo (ISS_IDS_LOAD_RULES, FWL_ZERO) != IDS_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        gi4IdsRulesStatus = IDS_RULES_LOAD_IN_PROGRESS;
    }
    else if (i4SetValFwlGlobalLoadIdsRules == IDS_UNLOAD_RULES)
    {
        if (SecIdsSetInfo (ISS_IDS_UNLOAD_RULES, FWL_ZERO) != IDS_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        gi4IdsRulesStatus = IDS_RULES_NOT_LOADED;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlGlobalLoadIdsRules, u4SeqNum,
                          FALSE, FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFwlGlobalLoadIdsRules));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalMasterControlSwitch
 Input       :  The Indices

                The Object 
                testValFwlGlobalMasterControlSwitch
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlGlobalMasterControlSwitch (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFwlGlobalMasterControlSwitch)
{
    INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\nMaster Control Switch TEST Entry\n");

    /* Validate the value of master control switch */
    if ((i4TestValFwlGlobalMasterControlSwitch == FWL_DISABLE)
        || (i4TestValFwlGlobalMasterControlSwitch == FWL_ENABLE))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Master Control Switch Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    if ((i4TestValFwlGlobalMasterControlSwitch == FWL_DISABLE)
        && (gu1FirewallStatus == FWL_ENABLE))
    {
        if (FWL_FAILURE == FwlUtilCallBack (FWL_CUST_IF_CHECK_EVENT))
        {
            FwlLogMessage (FWL_WAN_INTF_STATUS_CHECK_FAIL,
                           FWL_ZERO, NULL, FWL_LOG_MUST, FWLLOG_ALERT_LEVEL,
                           (UINT1 *) FWL_MSG_WAN_INTF_STATUS_CHECK_FAIL);
            *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
            i1Status = SNMP_FAILURE;
        }
    }

    FWL_DBG (FWL_DBG_EXIT, "\nMaster Control Switch TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalICMPControlSwitch
 Input       :  The Indices

                The Object 
                testValFwlGlobalICMPControlSwitch
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlGlobalICMPControlSwitch (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFwlGlobalICMPControlSwitch)
{
    INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\nIcmp Control Switch TEST Entry\n");

    /* Validate the value of icmp control switch */
    if ((i4TestValFwlGlobalICMPControlSwitch == FWL_ICMP_GENERATE)
        || (i4TestValFwlGlobalICMPControlSwitch == FWL_ICMP_SUPPRESS))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n ICMP Control Switch Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\nIcmp Control Switch TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalIpSpoofFiltering
 Input       :  The Indices

                The Object 
                testValFwlGlobalIpSpoofFiltering
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlGlobalIpSpoofFiltering (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFwlGlobalIpSpoofFiltering)
{
    INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Ip Spoof Filtering Control Switch TEST Entry\n");

    /* Validate the value of IP Spoof Filtering Control switch */
    if ((i4TestValFwlGlobalIpSpoofFiltering == FWL_ENABLE)
        || (i4TestValFwlGlobalIpSpoofFiltering == FWL_DISABLE))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Ip Spoof Filtering Control Switch Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\nIp Spoof Filtering Control Switch TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalSrcRouteFiltering
 Input       :  The Indices

                The Object 
                testValFwlGlobalSrcRouteFiltering
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlGlobalSrcRouteFiltering (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFwlGlobalSrcRouteFiltering)
{
    INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Src Route Filtering Control Switch TEST Entry\n");

    /* Validate the value of Source Route Filtering Control switch */
    if ((i4TestValFwlGlobalSrcRouteFiltering == FWL_ENABLE)
        || (i4TestValFwlGlobalSrcRouteFiltering == FWL_DISABLE))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Src Route Filtering Control Switch Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\nSrc Route Filtering Control Switch TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalTinyFragmentFiltering
 Input       :  The Indices

                The Object 
                testValFwlGlobalTinyFragmentFiltering
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlGlobalTinyFragmentFiltering (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4TestValFwlGlobalTinyFragmentFiltering)
{
    INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY,
             "\nTiny Fragment Filtering Control Switch TEST Entry\n");

    /* Validate the value of Tiny Fragment Filtering Control switch */
    if ((i4TestValFwlGlobalTinyFragmentFiltering == FWL_ENABLE)
        || (i4TestValFwlGlobalTinyFragmentFiltering == FWL_DISABLE))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Tiny Fragment Filtering "
                 "Control Switch Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_ENTRY,
             "\nTiny Fragment Filtering Control Switch TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalTcpIntercept
 Input       :  The Indices

                The Object 
                testValFwlGlobalTcpIntercept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlGlobalTcpIntercept (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFwlGlobalTcpIntercept)
{
    INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\nTCP Intercept Control Switch TEST Entry\n");

    /* Validate the value of TCP Intercept Control switch */
    if ((i4TestValFwlGlobalTcpIntercept == FWL_ENABLE)
        || (i4TestValFwlGlobalTcpIntercept == FWL_DISABLE))
    {
        if (gu1FirewallStatus == FWL_ENABLE)
        {
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                     "\n TCP Intercept Control Switch cannot be "
                     "enabled without enabling the MasterControl Switch\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n TCP Intercept Control Switch Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\nTCP Intercept Control Switch TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalTrap
 Input       :  The Indices

                The Object 
                testValFwlGlobalTrap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlGlobalTrap (UINT4 *pu4ErrorCode, INT4 i4TestValFwlGlobalTrap)
{
    INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\nTrap Control Switch TEST Entry\n");

    /* Validate the value of Trap Control switch */
    if ((i4TestValFwlGlobalTrap == FWL_ENABLE)
        || (i4TestValFwlGlobalTrap == FWL_DISABLE))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Trap Control Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\nTrap Control Switch TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalTrace
 Input       :  The Indices

                The Object 
                testValFwlGlobalTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlGlobalTrace (UINT4 *pu4ErrorCode, INT4 i4TestValFwlGlobalTrace)
{
    INT1                i1Status = SNMP_SUCCESS;

    FWL_DBG (FWL_DBG_ENTRY, "\nTrace Control Switch TEST Entry\n");

    /* Validate the value of Trace Control switch */
    if (i4TestValFwlGlobalTrace < FWL_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Trace Control Value cannot be negative\n");
        i1Status = SNMP_FAILURE;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\nTrace Control Switch TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalDebug
 Input       :  The Indices

                The Object 
                testValFwlGlobalDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlGlobalDebug (UINT4 *pu4ErrorCode, INT4 i4TestValFwlGlobalDebug)
{
    INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Debug Control Switch TEST Entry\n");

    /* Validate the value of Debug Control switch */
    if ((i4TestValFwlGlobalDebug == FWL_ENABLE)
        || (i4TestValFwlGlobalDebug == FWL_DISABLE))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Debug Control Switch Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\n Debug Control Switch TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalUrlFiltering
 Input       :  The Indices

                The Object 
                testValFwlGlobalUrlFiltering
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlGlobalUrlFiltering (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFwlGlobalUrlFiltering)
{
    INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Fwl Url Filtering Control Switch TEST Entry\n");

    /* Validate the value of Url Filtering Control switch */
    if ((i4TestValFwlGlobalUrlFiltering == FWL_ENABLE)
        || (i4TestValFwlGlobalUrlFiltering == FWL_DISABLE))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Fwl Url Filtering Control Switch Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\nFwl Url Filtering Control Switch TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalNetBiosFiltering
 Input       :  The Indices

                The Object 
                testValFwlGlobalNetBiosFiltering
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlGlobalNetBiosFiltering (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFwlGlobalNetBiosFiltering)
{
    INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Fwl NetBios Filtering Control Switch TEST Entry\n");

    /* Validate the value of Url Filtering Control switch */
    if ((i4TestValFwlGlobalNetBiosFiltering == FWL_ENABLE)
        || (i4TestValFwlGlobalNetBiosFiltering == FWL_DISABLE))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Fwl NetBios Filtering Control Switch "
                 "Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_ENTRY,
             "\nFwl NetBios Filtering Control Switch TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalNetBiosLan2Wan
 Input       :  The Indices

                The Object
                testValFwlGlobalNetBiosLan2Wan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.                                                     Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)                                              SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2FwlGlobalNetBiosLan2Wan (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFwlGlobalNetBiosLan2Wan)
{
    INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Net Bios Lan to Wan Control Switch TEST Entry\n");

    /* Validate the value of Net Bios Filtering Control switch */
    if ((i4TestValFwlGlobalNetBiosLan2Wan == FWL_ENABLE)
        || (i4TestValFwlGlobalNetBiosLan2Wan == FWL_DISABLE))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Net Bios Lan to Wan Control Switch Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Net Bios Lan to Wan Control Switch TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 *  Function    :  nmhTestv2FwlGlobalICMPv6ControlSwitch
 *  Input       :  The Indices
 *                   The Object
 *                  testValFwlGlobalICMPv6ControlSwitch
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                 Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val
 *  Error Codes :  The following error codes are to be returned
 *                  SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                  SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                  SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                  SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                  SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ***************************************************************************/
INT1
nmhTestv2FwlGlobalICMPv6ControlSwitch (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFwlGlobalICMPv6ControlSwitch)
{
    INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\nIcmpv6 Control Switch TEST Entry\n");

    /* Validate the value of icmpv6 control switch */
    if ((i4TestValFwlGlobalICMPv6ControlSwitch == FWL_ICMP_GENERATE)
        || (i4TestValFwlGlobalICMPv6ControlSwitch == FWL_ICMP_SUPPRESS))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n ICMPv6 Control Switch Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\nIcmpv6 Control Switch TEST Exit\n");

    return i1Status;

}

/****************************************************************************
 *  Function    :  nmhTestv2FwlGlobalIpv6SpoofFiltering
 *   Input       :  The Indices
 *                   The Object
 *                  testValFwlGlobalIpv6SpoofFiltering
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                 Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val
 *  Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhTestv2FwlGlobalIpv6SpoofFiltering (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFwlGlobalIpv6SpoofFiltering)
{
    INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Ipv6 Spoof Filtering Control Switch TEST Entry\n");

    /* Validate the value of IP Spoof Filtering Control switch */
    if ((i4TestValFwlGlobalIpv6SpoofFiltering == FWL_ENABLE)
        || (i4TestValFwlGlobalIpv6SpoofFiltering == FWL_DISABLE))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Ipv6 Spoof Filtering Control Switch Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_ENTRY,
             "\nIpv6 Spoof Filtering Control Switch TEST Exit\n");

    return i1Status;

}

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalLogFileSize
 Input       :  The Indices

                The Object
                testValFwlGlobalLogFileSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2FwlGlobalLogFileSize (UINT4 *pu4ErrorCode,
                               UINT4 u4TestValFwlGlobalLogFileSize)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Firewall Log filesize TEST Entry\n");

    if ((u4TestValFwlGlobalLogFileSize < FWL_MIN_LOG_SIZE) ||
        (u4TestValFwlGlobalLogFileSize > FWL_LOGFILE_LIMIT_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\n Firewall Log filesize TEST Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalLogSizeThreshold
 Input       :  The Indices

                The Object
                testValFwlGlobalLogSizeThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2FwlGlobalLogSizeThreshold (UINT4 *pu4ErrorCode,
                                    UINT4 u4TestValFwlGlobalLogSizeThreshold)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Firewall Logsize threshold TEST Entry\n");

    if ((u4TestValFwlGlobalLogSizeThreshold < FWL_MIN_LOG_SIZE_THRESH) ||
        (u4TestValFwlGlobalLogSizeThreshold > FWL_MAX_LOG_SIZE_THRESH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\n Firewall Logsize threshold TEST Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalIdsLogSize
 Input       :  The Indices

                The Object
                testValFwlGlobalIdsLogSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlGlobalIdsLogSize (UINT4 *pu4ErrorCode,
                              UINT4 u4TestValFwlGlobalIdsLogSize)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Ids Log filesize TEST Entry\n");

    if ((u4TestValFwlGlobalIdsLogSize < IDS_MIN_LOG_FILESIZE) ||
        (u4TestValFwlGlobalIdsLogSize > IDS_MAX_LOG_FILESIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\n Ids Log filesize TEST Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalIdsLogThreshold
 Input       :  The Indices

                The Object
                testValFwlGlobalIdsLogThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlGlobalIdsLogThreshold (UINT4 *pu4ErrorCode,
                                   UINT4 u4TestValFwlGlobalIdsLogThreshold)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Ids Logsize threshold TEST Entry\n");

    if ((u4TestValFwlGlobalIdsLogThreshold < IDS_MIN_LOG_THRESHOLD) ||
        (u4TestValFwlGlobalIdsLogThreshold > IDS_MAX_LOG_THRESHOLD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\n Ids Logsize threshold TEST Entry\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalReloadIds
 Input       :  The Indices

                The Object
                testValFwlGlobalReloadIds
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlGlobalReloadIds (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFwlGlobalReloadIds)
{
    if (i4TestValFwlGlobalReloadIds != IDS_RELOAD)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalIdsStatus
 Input       :  The Indices

                The Object
                testValFwlGlobalIdsStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlGlobalIdsStatus (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFwlGlobalIdsStatus)
{
    if ((i4TestValFwlGlobalIdsStatus != IDS_ENABLE) &&
        (i4TestValFwlGlobalIdsStatus != IDS_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FwlGlobalLoadIdsRules
 Input       :  The Indices

                The Object 
                testValFwlGlobalLoadIdsRules
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlGlobalLoadIdsRules (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFwlGlobalLoadIdsRules)
{
    if ((i4TestValFwlGlobalLoadIdsRules != IDS_LOAD_RULES) &&
        (i4TestValFwlGlobalLoadIdsRules != IDS_UNLOAD_RULES))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalMasterControlSwitch
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalMasterControlSwitch (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalICMPControlSwitch
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.                                                             Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalICMPControlSwitch (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhDepv2FwlGlobalICMPv6ControlSwitch
 *  Output      :  The Dependency Low Lev Routine Take the Indices &
 *                 check whether dependency is met or not.
 *                 Stores the value of error code in the Return val
 *  Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhDepv2FwlGlobalICMPv6ControlSwitch (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhDepv2FwlGlobalIpv6SpoofFiltering
 *  Output      :  The Dependency Low Lev Routine Take the Indices &
 *                 check whether dependency is met or not.
 *                 Stores the value of error code in the Return val
 *  Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhDepv2FwlGlobalIpv6SpoofFiltering (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalIpSpoofFiltering
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val                                     Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalIpSpoofFiltering (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalSrcRouteFiltering
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalSrcRouteFiltering (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalTinyFragmentFiltering
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)          
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalTinyFragmentFiltering (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalTcpIntercept
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.                        
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalTcpIntercept (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalTrap
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalTrap (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalTrace (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalDebug (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalUrlFiltering
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalUrlFiltering (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalNetBiosFiltering
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalNetBiosFiltering (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalNetBiosLan2Wan
 Output      :  The Dependency Low Lev Routine Take the Indices &                                                   check whether dependency is met or not.
                Stores the value of error code in the Return val                                     Error Codes :  The following error codes are to be returned                                                        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)                        Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalNetBiosLan2Wan (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalLogFileSize
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalLogFileSize (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalLogSizeThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalLogSizeThreshold (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalIdsLogSize
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalIdsLogSize (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalIdsLogThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalIdsLogThreshold (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalReloadIds
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalReloadIds (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalIdsStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalIdsStatus (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlGlobalLoadIdsRules
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlGlobalLoadIdsRules (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlDefnTcpInterceptThreshold
 Input       :  The Indices

                The Object 
                retValFwlDefnTcpInterceptThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlDefnTcpInterceptThreshold (INT4 *pi4RetValFwlDefnTcpInterceptThreshold)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Tcp Intercept Threshold GET Entry\n");

    *pi4RetValFwlDefnTcpInterceptThreshold = (INT4) gFwlAclInfo.u2TcpSynLimit;

    FWL_DBG (FWL_DBG_EXIT, "\n Tcp Intercept Threshold GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlDefnInterceptTimeout
 Input       :  The Indices

                The Object 
                retValFwlDefnInterceptTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlDefnInterceptTimeout (UINT4 *pu4RetValFwlDefnInterceptTimeout)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Tcp Intercept TimeOut GET Entry\n");

    *pu4RetValFwlDefnInterceptTimeout = (UINT4)
        gFwlAclInfo.u2TcpInterceptTimeOut;

    FWL_DBG (FWL_DBG_EXIT, "\n Tcp Intercept TimeOut GET Exit\n");

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlDefnTcpInterceptThreshold
 Input       :  The Indices

                The Object 
                setValFwlDefnTcpInterceptThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlDefnTcpInterceptThreshold (INT4 i4SetValFwlDefnTcpInterceptThreshold)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY, "\n TCP Intercept Threhold SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    /* Set the new threshold value. */
    gFwlAclInfo.u2TcpSynLimit = (UINT2) i4SetValFwlDefnTcpInterceptThreshold;
    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n TCP Intercept Threshold value is SET to %d\n",
                  i4SetValFwlDefnTcpInterceptThreshold);

    FWL_DBG (FWL_DBG_EXIT, "\n TCP Intercept Threhold SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlDefnTcpInterceptThreshold,
                          u4SeqNum, FALSE, FwlLock, FwlUnLock, FWL_ZERO,
                          SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                      i4SetValFwlDefnTcpInterceptThreshold));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlDefnInterceptTimeout
 Input       :  The Indices

                The Object 
                setValFwlDefnInterceptTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlDefnInterceptTimeout (UINT4 u4SetValFwlDefnInterceptTimeout)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY, "\n TCP Intercept TimeOut Interval SET Entry \n");

    RM_GET_SEQ_NUM (&u4SeqNum);
    /* Set the new Timer value. Then restart the Timer using the new configured 
     * value.
     */
    if (gFwlAclInfo.u2TcpInterceptTimeOut == u4SetValFwlDefnInterceptTimeout)
    {
        return SNMP_SUCCESS;
    }
    gFwlAclInfo.u2TcpInterceptTimeOut = (UINT2) u4SetValFwlDefnInterceptTimeout;
    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n TCP Intercept TimeOut is SET to %d\n",
                  u4SetValFwlDefnInterceptTimeout);
    if ((gFwlAclInfo.u1TcpInterceptEnable == FWL_ENABLE))
    {
        gFwlAclInfo.u2SynPktsAllowed = FWL_ZERO;
        gFwlAclInfo.u2RtrSynPktsAllowed = FWL_ZERO;
        gFwlAclInfo.u2UdpPktsAllowed = FWL_ZERO;
        gFwlAclInfo.u2IcmpPktsAllowed = FWL_ZERO;
        FwlTmrRestartTmr (&gFwlAclInfo.TcpInterceptTimer,
                          FWL_TCP_INTERCEPT_TIMER,
                          (UINT4) (gFwlAclInfo.u2TcpInterceptTimeOut));
    }

    FWL_DBG (FWL_DBG_EXIT, "\n TCP Intercept TimeOut Interval SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlDefnInterceptTimeout, u4SeqNum,
                          FALSE, FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%p", u4SetValFwlDefnInterceptTimeout));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlDefnTcpInterceptThreshold
 Input       :  The Indices

                The Object 
                testValFwlDefnTcpInterceptThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlDefnTcpInterceptThreshold (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFwlDefnTcpInterceptThreshold)
{
    INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n TCP Intercept Threshold TEST Entry\n");

    /* Validate the value of TCP Intercept Threshold and it can be set only
     * when the intercept control switch is enabled.
     */
    if ((i4TestValFwlDefnTcpInterceptThreshold > FWL_ZERO)
        && (i4TestValFwlDefnTcpInterceptThreshold < FWL_INT_MAX))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n TCP Threshold value cannot be negative and "
                 "cannot be set if TCP Intercept Control Switch is disabled\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\n TCP Intercept Threshold TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlDefnInterceptTimeout
 Input       :  The Indices

                The Object 
                testValFwlDefnInterceptTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlDefnInterceptTimeout (UINT4 *pu4ErrorCode,
                                  UINT4 u4TestValFwlDefnInterceptTimeout)
{
    INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n TCP Intercept TimeOut TEST Entry\n");

    /* Validate the value of TCP Intercept TimeOut */
    if ((u4TestValFwlDefnInterceptTimeout > FWL_ZERO)
        && (u4TestValFwlDefnInterceptTimeout < FWL_INT_MAX)
        && (gFwlAclInfo.u1TcpInterceptEnable == FWL_ENABLE))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n TCP TimeOut value cannot be negative and "
                 "cannot be set if TCP Intercept Control Switch is disabled\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\n TCP Intercept TimeOut TEST Exit\n");

    return i1Status;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlDefnTcpInterceptThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &                                                   check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDefnTcpInterceptThreshold (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlDefnInterceptTimeout
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDefnInterceptTimeout (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FwlDefnFilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlDefnFilterTable
 Input       :  The Indices
                FwlFilterFilterName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFwlDefnFilterTable (tSNMP_OCTET_STRING_TYPE *
                                            pFwlFilterFilterName)
{

    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    tFilterInfo        *pFilterNode = NULL;
    INT1                i1Status = FWL_ZERO;

    pFilterNode = (tFilterInfo *) NULL;
    i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Validate Index for Filter Table - Entry\n");

    if (pFwlFilterFilterName->i4_Length <= FWL_MAX_FILTER_NAME_LEN - FWL_ONE)
    {

        FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                    MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                                   (INT4) sizeof (au1FilterName)));
        au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

        /* Check whether the filter name exists or not */
        pFilterNode = FwlDbaseSearchFilter (au1FilterName);
        if (pFilterNode != NULL)
        {
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\nFilter %s doesnt exist \n", au1FilterName);
        }

    }

    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\nMaximum Filter Name Length is %d\n",
                      FWL_MAX_FILTER_NAME_LEN - FWL_ONE);
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Validate Index for Filter Table - Exit\n");
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFwlDefnFilterTable
 Input       :  The Indices
                FwlFilterFilterName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFwlDefnFilterTable (tSNMP_OCTET_STRING_TYPE *
                                    pFwlFilterFilterName)
{
    INT1                i1Status = SNMP_FAILURE;
    tFilterInfo        *pFilterNode = NULL;

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Get First Index for Filter Table - Entry\n");

    /* get the first filter node in the filter list */
    if (TMO_SLL_Count (&gFwlAclInfo.filterList) != FWL_DEFAULT_COUNT)
    {
        pFilterNode = (tFilterInfo *) TMO_SLL_First (&gFwlAclInfo.filterList);
        pFwlFilterFilterName->i4_Length = (INT4)
            FWL_STRLEN ((INT1 *) pFilterNode->au1FilterName);
        FWL_MEMCPY (pFwlFilterFilterName->pu1_OctetList,
                    pFilterNode->au1FilterName,
                    pFwlFilterFilterName->i4_Length);
        i1Status = SNMP_SUCCESS;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Get First Index for Filter Table - Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFwlDefnFilterTable
 Input       :  The Indices
                FwlFilterFilterName
                nextFwlFilterFilterName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFwlDefnFilterTable (tSNMP_OCTET_STRING_TYPE *
                                   pFwlFilterFilterName,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextFwlFilterFilterName)
{
    INT1                i1Status = SNMP_FAILURE;
    tFilterInfo        *pFilterNode = NULL;
    tFilterInfo        *pFilterNextNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    pFilterNode = (tFilterInfo *) NULL;
    pFilterNextNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Get Next Index for Filter Table - Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* search for the filter name and return the the filter name
     * next to the given filter name 
     */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode != NULL)
    {
        pFilterNextNode = (tFilterInfo *) TMO_SLL_Next (&gFwlAclInfo.filterList,
                                                        (tTMO_SLL_NODE *)
                                                        pFilterNode);

        if (pFilterNextNode != NULL)
        {
            pNextFwlFilterFilterName->i4_Length = (INT4)
                FWL_STRLEN ((INT1 *) pFilterNextNode->au1FilterName);
            FWL_MEMCPY (pNextFwlFilterFilterName->pu1_OctetList,
                        pFilterNextNode->au1FilterName,
                        pNextFwlFilterFilterName->i4_Length);
            i1Status = SNMP_SUCCESS;
        }
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Get Next Index for Filter Table - Exit\n");

    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlFilterSrcAddress
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterSrcAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlFilterSrcAddress (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                           tSNMP_OCTET_STRING_TYPE * pRetValFwlFilterSrcAddress)
{
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nSource Address GET Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* search the filter name in the filter list */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValFwlFilterSrcAddress->i4_Length = (INT4)
        FWL_STRLEN ((INT1 *) pFilterNode->au1SrcAddr);
    FWL_MEMCPY (pRetValFwlFilterSrcAddress->pu1_OctetList,
                pFilterNode->au1SrcAddr, pRetValFwlFilterSrcAddress->i4_Length);

    pRetValFwlFilterSrcAddress->pu1_OctetList[pRetValFwlFilterSrcAddress->
                                              i4_Length] = FWL_END_OF_STRING;

    FWL_DBG (FWL_DBG_EXIT, "\nSource Address GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlFilterDestAddress
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterDestAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlFilterDestAddress (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFwlFilterDestAddress)
{
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nDestination Address GET Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* search the filter name in the filter list */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValFwlFilterDestAddress->i4_Length = (INT4)
        FWL_STRLEN ((INT1 *) pFilterNode->au1DestAddr);
    FWL_MEMCPY (pRetValFwlFilterDestAddress->pu1_OctetList,
                pFilterNode->au1DestAddr,
                pRetValFwlFilterDestAddress->i4_Length);

    pRetValFwlFilterDestAddress->pu1_OctetList[pRetValFwlFilterDestAddress->
                                               i4_Length] = FWL_END_OF_STRING;

    FWL_DBG (FWL_DBG_EXIT, "\nDestination Address GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlFilterProtocol
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlFilterProtocol (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                         INT4 *pi4RetValFwlFilterProtocol)
{
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nProtocol GET Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* search the filter name in the filter list and get the protocol value */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFwlFilterProtocol = (INT4) pFilterNode->u1Proto;

    FWL_DBG (FWL_DBG_EXIT, "\nProtocol GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlFilterSrcPort
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterSrcPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlFilterSrcPort (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                        tSNMP_OCTET_STRING_TYPE * pRetValFwlFilterSrcPort)
{
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nSource Port GET Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* search the filter name in the filter list */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /*  get the Max and Min port and return it in <operator> <Port> notation */
    pRetValFwlFilterSrcPort->i4_Length = (INT4)
        FWL_STRLEN ((INT1 *) pFilterNode->au1SrcPort);
    FWL_MEMCPY (pRetValFwlFilterSrcPort->pu1_OctetList,
                pFilterNode->au1SrcPort, pRetValFwlFilterSrcPort->i4_Length);

    pRetValFwlFilterSrcPort->pu1_OctetList[pRetValFwlFilterSrcPort->i4_Length] =
        FWL_END_OF_STRING;
    FWL_DBG (FWL_DBG_EXIT, "\nSource Port GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlFilterDestPort
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterDestPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlFilterDestPort (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                         tSNMP_OCTET_STRING_TYPE * pRetValFwlFilterDestPort)
{
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nDestination Port GET Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* search the filter name in the filter list */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /* get the Min and Max Port and return it in <operator> <Port> notation */
    pRetValFwlFilterDestPort->i4_Length = (INT4)
        FWL_STRLEN ((INT1 *) pFilterNode->au1DestPort);
    FWL_MEMCPY (pRetValFwlFilterDestPort->pu1_OctetList,
                pFilterNode->au1DestPort, pRetValFwlFilterDestPort->i4_Length);
    pRetValFwlFilterDestPort->pu1_OctetList[pRetValFwlFilterDestPort->
                                            i4_Length] = FWL_END_OF_STRING;
    FWL_DBG (FWL_DBG_EXIT, "\nDestination Port GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlFilterAckBit
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterAckBit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlFilterAckBit (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                       INT4 *pi4RetValFwlFilterAckBit)
{
    /* Deprecated Object - Default Value is returned */

    UNUSED_PARAM (pFwlFilterFilterName);

    *pi4RetValFwlFilterAckBit = FWL_TCP_ACK_ANY;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlFilterRstBit
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterRstBit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlFilterRstBit (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                       INT4 *pi4RetValFwlFilterRstBit)
{
    /* Deprecated Object - Default Value is returned */

    UNUSED_PARAM (pFwlFilterFilterName);

    *pi4RetValFwlFilterRstBit = FWL_TCP_RST_ANY;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlFilterTos
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterTos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlFilterTos (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                    INT4 *pi4RetValFwlFilterTos)
{
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nTOS Bit GET Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* search the filter name in the filter list */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /* get the TOS value */
    *pi4RetValFwlFilterTos = (INT4) pFilterNode->u1Tos;

    FWL_DBG (FWL_DBG_EXIT, "\nTOS Bit GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlFilterAccounting
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterAccounting
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlFilterAccounting (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                           INT4 *pi4RetValFwlFilterAccounting)
{
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nFilter Accounting GET Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                pFwlFilterFilterName->i4_Length);
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* search the filter name in the filter list */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return (SNMP_FAILURE);
    }
    /* get the FwlFilterAccounting value */
    *pi4RetValFwlFilterAccounting = (INT4) pFilterNode->u1FilterAccounting;

    FWL_DBG (FWL_DBG_EXIT, "\nFilter Accounting GET Exit\n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFwlFilterHitClear
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterHitClear
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlFilterHitClear (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                         INT4 *pi4RetValFwlFilterHitClear)
{
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nFilter Hit Clear GET Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                pFwlFilterFilterName->i4_Length);
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* search the filter name in the filter list */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    /* The get routine always returns 'false' for the
     * MIB object FwlFilterHitClear */

    *pi4RetValFwlFilterHitClear = FWL_FALSE;

    FWL_DBG (FWL_DBG_EXIT, "\nFilter Hit Clear GET Exit\n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFwlFilterHitsCount
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterHitsCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlFilterHitsCount (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                          UINT4 *pu4RetValFwlFilterHitsCount)
{
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nFilter HitsCount GET Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* search the filter name in the filter list */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /* get the hit count value */
    *pu4RetValFwlFilterHitsCount = pFilterNode->u4FilterHitCount;

    FWL_DBG (FWL_DBG_EXIT, "\nFilter HitsCount GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFwlFilterAddrType
 *  Input       :  The Indices
 *                 FwlFilterFilterName
 *                 The Object
 *                 retValFwlFilterAddrType
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFwlFilterAddrType (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                         INT4 *pi4RetValFwlFilterAddrType)
{
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    FWL_DBG (FWL_DBG_ENTRY, "\nAddress Type GET Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* search the filter name in the filter list and get the protocol value */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFwlFilterAddrType = (INT4) pFilterNode->u2AddrType;

    FWL_DBG (FWL_DBG_EXIT, "\nAddress Type GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFwlFilterFlowId
 *  Input       :  The Indices
 *                 FwlFilterFilterName
 *                 The Object
 *                 retValFwlFilterFlowId
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFwlFilterFlowId (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                       UINT4 *pu4RetValFwlFilterFlowId)
{
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    FWL_DBG (FWL_DBG_ENTRY, "\nFlow Id GET Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* search the filter name in the filter list and get the protocol value */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFwlFilterFlowId = pFilterNode->u4FlowId;

    FWL_DBG (FWL_DBG_EXIT, "\nFlow Id GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFwlFilterDscp
 *  Input       :  The Indices
 *                 FwlFilterFilterName
 *                 The Object
 *                 retValFwlFilterDscp
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ***************************************************************************/
INT1
nmhGetFwlFilterDscp (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                     INT4 *pi4RetValFwlFilterDscp)
{
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    FWL_DBG (FWL_DBG_ENTRY, "\nDscp  GET Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* search the filter name in the filter list and get the protocol value */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFwlFilterDscp = pFilterNode->i4Dscp;

    FWL_DBG (FWL_DBG_EXIT, "\nDscp GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlFilterRowStatus
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                retValFwlFilterRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlFilterRowStatus (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                          INT4 *pi4RetValFwlFilterRowStatus)
{
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN + FWL_ONE] =
        { FWL_ZERO };

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nFilter RowStatus GET Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* search the filter name in the filter list */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return (SNMP_FAILURE);
    }
    /* get the row status value  */
    *pi4RetValFwlFilterRowStatus = (INT4) pFilterNode->u1RowStatus;

    FWL_DBG (FWL_DBG_EXIT, "\nFilter RowStatus GET Exit\n");

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlFilterSrcAddress
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterSrcAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlFilterSrcAddress (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                           tSNMP_OCTET_STRING_TYPE * pSetValFwlFilterSrcAddress)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tFilterInfo        *pFilterNode = NULL;
    tIp6Addr            Srcv6StartAddr;
    tIp6Addr            Srcv6EndAddr;

    UINT4               u4SrcStartAddr = SNMP_FAILURE;
    UINT4               u4SrcEndAddr = FWL_ZERO;
    INT1                i1Status = FWL_ZERO;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    UINT1               au1SrcAddr[FWL_MAX_ADDR_LEN] = { FWL_ZERO };

    MEMSET (&Srcv6StartAddr, FWL_ZERO, sizeof (tIp6Addr));
    MEMSET (&Srcv6EndAddr, FWL_ZERO, sizeof (tIp6Addr));

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Src Address SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    /* The pointer to the String (pFwlFilterFilterName) is not checked whether
     * it is NULL or not. This is not done since it is taken care by the
     * MID-LEVEL routines.
     */
    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /*  Search the Filter in the Filter list  */

    /* The pointer to the tFilterInfo is not checked whether
     * it is NULL or not. This is not done since it is taken care by the
     * MID-LEVEL routines.
     */

    pFilterNode = FwlDbaseSearchFilter (au1FilterName);

    if (pFilterNode != NULL)
    {
        FWL_MEMCPY (pFilterNode->au1SrcAddr,
                    pSetValFwlFilterSrcAddress->pu1_OctetList,
                    pSetValFwlFilterSrcAddress->i4_Length);
    }
    else
    {
        return i1Status;
    }

    pFilterNode->au1SrcAddr[pSetValFwlFilterSrcAddress->i4_Length] =
        FWL_END_OF_STRING;

    FWL_MEMCPY (au1SrcAddr, pSetValFwlFilterSrcAddress->pu1_OctetList,
                pSetValFwlFilterSrcAddress->i4_Length);

    au1SrcAddr[pSetValFwlFilterSrcAddress->i4_Length] = FWL_END_OF_STRING;

    if (pFilterNode->u2AddrType == FWL_IP_VERSION_4)
    {
        /* Parse the string and find Start and End Source IP address  */

        i1Status = FwlParseIpAddr (au1SrcAddr, &u4SrcStartAddr, &u4SrcEndAddr);

        if (i1Status == FWL_SUCCESS)
        {
            /* Set the start and end address in the Filter Node */
            pFilterNode->SrcStartAddr.v4Addr = u4SrcStartAddr;
            pFilterNode->SrcEndAddr.v4Addr = u4SrcEndAddr;
            pFilterNode->SrcStartAddr.u4AddrType = FWL_IP_VERSION_4;
            pFilterNode->SrcEndAddr.u4AddrType = FWL_IP_VERSION_4;
            pFilterNode->u1RowStatus = FWL_NOT_IN_SERVICE;
            MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n Source Ip Address %s is set for Filter %s \n",
                          au1SrcAddr, au1FilterName);
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            i1Status = SNMP_FAILURE;
        }
    }
    else if (pFilterNode->u2AddrType == FWL_IP_VERSION_6)
    {
        i1Status = FwlParseIpv6Addr (au1SrcAddr,
                                     &Srcv6StartAddr, &Srcv6EndAddr);
        if (i1Status == FWL_SUCCESS)
        {
            Ip6AddrCopy (&pFilterNode->SrcStartAddr.v6Addr, &Srcv6StartAddr);
            Ip6AddrCopy (&pFilterNode->SrcEndAddr.v6Addr, &Srcv6EndAddr);
            pFilterNode->SrcStartAddr.u4AddrType = FWL_IP_VERSION_6;
            pFilterNode->SrcEndAddr.u4AddrType = FWL_IP_VERSION_6;
            pFilterNode->u1RowStatus = FWL_NOT_IN_SERVICE;

            MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n Source Ipv6 Address %s is set for Filter %s \n",
                          au1SrcAddr, au1FilterName);

            i1Status = SNMP_SUCCESS;
        }
        else
        {
            i1Status = SNMP_FAILURE;
        }
    }
    else
    {
        i1Status = SNMP_FAILURE;
    }
    FWL_DBG (FWL_DBG_EXIT, "\n Src Address SET Exit \n");
    if (i1Status == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlFilterSrcAddress, u4SeqNum,
                              FALSE, FwlLock, FwlUnLock, FWL_ONE, i1Status);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFwlFilterFilterName,
                          pSetValFwlFilterSrcAddress));
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFwlFilterDestAddress
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterDestAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlFilterDestAddress (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFwlFilterDestAddress)
{

    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tFilterInfo        *pFilterNode = NULL;
    tIp6Addr            Destv6StartAddr;
    tIp6Addr            Destv6EndAddr;

    UINT4               u4DestStartAddr = FWL_ZERO;
    UINT4               u4DestEndAddr = FWL_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    UINT1               au1DestAddr[FWL_MAX_ADDR_LEN] = { FWL_ZERO };

    pFilterNode = (tFilterInfo *) NULL;

    MEMSET (&Destv6StartAddr, FWL_ZERO, sizeof (tIp6Addr));
    MEMSET (&Destv6EndAddr, FWL_ZERO, sizeof (tIp6Addr));

    MEMSET (au1FilterName, FWL_ZERO, sizeof (au1FilterName));
    MEMSET (au1DestAddr, FWL_ZERO, sizeof (au1DestAddr));

    FWL_DBG (FWL_DBG_ENTRY, "\n Dest Address SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    /* The pointer to the String (pFwlFilterFilterName) is not checked whether
     * it is NULL or not. This is not done since it is taken care by the
     * MID-LEVEL routines.
     */
    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /*  Search the Filter in the Filter list  */

    /* The pointer to the tFilterInfo is not checked whether
     * it is NULL or not. This is not done since it is taken care by the
     * MID-LEVEL routines.
     */

    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode != NULL)
    {
        FWL_MEMCPY (pFilterNode->au1DestAddr,
                    pSetValFwlFilterDestAddress->pu1_OctetList,
                    pSetValFwlFilterDestAddress->i4_Length);
    }
    else
    {
        return i1Status;
    }

    /* The pointer to the String (pSetValFwlFilterDestAddress) is not checked 
     * whether it is NULL or not. This is not done since it is taken care by 
     * the MID-LEVEL routines.
     */

    pFilterNode->au1DestAddr[pSetValFwlFilterDestAddress->i4_Length] =
        FWL_END_OF_STRING;

    FWL_MEMCPY (au1DestAddr, pSetValFwlFilterDestAddress->pu1_OctetList,
                pSetValFwlFilterDestAddress->i4_Length);

    au1DestAddr[pSetValFwlFilterDestAddress->i4_Length] = FWL_END_OF_STRING;

    if (pFilterNode->u2AddrType == FWL_IP_VERSION_4)
    {
        /* Parse the string and find Start and End Destination IP address  */

        i1Status = FwlParseIpAddr
            (au1DestAddr, &u4DestStartAddr, &u4DestEndAddr);

        if (i1Status == FWL_SUCCESS)
        {
            /* Set the start and end address in the Filter Node */
            pFilterNode->DestStartAddr.v4Addr = u4DestStartAddr;
            pFilterNode->DestEndAddr.v4Addr = u4DestEndAddr;
            pFilterNode->DestStartAddr.u4AddrType = FWL_IP_VERSION_4;
            pFilterNode->DestEndAddr.u4AddrType = FWL_IP_VERSION_4;

            pFilterNode->u1RowStatus = FWL_NOT_IN_SERVICE;
            MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n Destination Ip Address %s is set for Filter %s \n",
                          au1DestAddr, au1FilterName);
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            i1Status = SNMP_FAILURE;
        }
    }
    else if (pFilterNode->u2AddrType == FWL_IP_VERSION_6)
    {
        i1Status = FwlParseIpv6Addr (au1DestAddr,
                                     &Destv6StartAddr, &Destv6EndAddr);
        if (i1Status == FWL_SUCCESS)
        {
            /*Set the start and end address in the Filter Node */
            Ip6AddrCopy (&pFilterNode->DestStartAddr.v6Addr, &Destv6StartAddr);
            Ip6AddrCopy (&pFilterNode->DestEndAddr.v6Addr, &Destv6EndAddr);
            pFilterNode->DestStartAddr.u4AddrType = FWL_IP_VERSION_6;
            pFilterNode->DestEndAddr.u4AddrType = FWL_IP_VERSION_6;
            pFilterNode->u1RowStatus = FWL_NOT_IN_SERVICE;
            MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n Destination Ipv6 Address %s is set for Filter %s \n",
                          au1DestAddr, au1FilterName);
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            i1Status = SNMP_FAILURE;
        }
    }
    else
    {
        i1Status = SNMP_FAILURE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Dest Address SET Exit \n");
    if (i1Status == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlFilterDestAddress, u4SeqNum,
                              FALSE, FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFwlFilterFilterName,
                          pSetValFwlFilterDestAddress));
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFwlFilterProtocol
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlFilterProtocol (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                         INT4 i4SetValFwlFilterProtocol)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Protocol SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /*  Search for the filter and assign the protocol value 
     *  in the Filter Node structure.
     */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }
    pFilterNode->u1Proto = (UINT1) i4SetValFwlFilterProtocol;
    if ((pFilterNode->u1Proto != FWL_TCP) && (pFilterNode->u1Proto != FWL_UDP)
        && (pFilterNode->u1Proto != FWL_DEFAULT_PROTO))
    {

        pFilterNode->u2SrcMaxPort = FWL_DEFAULT_PORT;
        pFilterNode->u2SrcMinPort = FWL_DEFAULT_PORT;
        FWL_STRCPY (pFilterNode->au1SrcPort, FWL_NULL_STRING);

        pFilterNode->u2DestMaxPort = FWL_DEFAULT_PORT;
        pFilterNode->u2DestMinPort = FWL_DEFAULT_PORT;
        FWL_STRCPY (pFilterNode->au1DestPort, FWL_NULL_STRING);
    }
    pFilterNode->u1RowStatus = FWL_NOT_IN_SERVICE;
    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Protocol Value %d is set for Filter %s \n",
                  i4SetValFwlFilterProtocol, au1FilterName);

    FWL_DBG (FWL_DBG_EXIT, "\n Protocol SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlFilterProtocol, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFwlFilterFilterName,
                      i4SetValFwlFilterProtocol));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlFilterSrcPort
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterSrcPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlFilterSrcPort (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                        tSNMP_OCTET_STRING_TYPE * pSetValFwlFilterSrcPort)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    INT1                i1Status = SNMP_FAILURE;
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    UINT2               u2SrcMaxPort = FWL_ZERO;
    UINT2               u2SrcMinPort = FWL_ZERO;

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Src Port SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /*  search for the filter and identify the Min and Max values
     *  for the port and assign it to the Filter Node structure.    
     */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return i1Status;
    }
    FWL_MEMCPY (pFilterNode->au1SrcPort, pSetValFwlFilterSrcPort->pu1_OctetList,
                pSetValFwlFilterSrcPort->i4_Length);
    pFilterNode->au1SrcPort[pSetValFwlFilterSrcPort->i4_Length] =
        FWL_END_OF_STRING;

    i1Status = FwlParseMinAndMaxPort (pFilterNode->au1SrcPort, &u2SrcMaxPort,
                                      &u2SrcMinPort);
    if (i1Status == SNMP_SUCCESS)
    {
        pFilterNode->u2SrcMaxPort = u2SrcMaxPort;
        pFilterNode->u2SrcMinPort = u2SrcMinPort;
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Source Port is set for Filter %s \n", au1FilterName);
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Src Port SET Exit \n");
    if (i1Status == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlFilterSrcPort, u4SeqNum, FALSE,
                              FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFwlFilterFilterName,
                          pSetValFwlFilterSrcPort));
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFwlFilterDestPort
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterDestPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlFilterDestPort (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                         tSNMP_OCTET_STRING_TYPE * pSetValFwlFilterDestPort)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    INT1                i1Status = SNMP_FAILURE;
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    UINT2               u2DestMaxPort = FWL_ZERO;
    UINT2               u2DestMinPort = FWL_ZERO;

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Dest Port SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /*  search for the filter and identify the Min and Max values
     *  for the port and assign it to the Filter Node structure.    
     */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return i1Status;
    }
    FWL_MEMCPY (pFilterNode->au1DestPort,
                pSetValFwlFilterDestPort->pu1_OctetList,
                pSetValFwlFilterDestPort->i4_Length);
    pFilterNode->au1DestPort[pSetValFwlFilterDestPort->i4_Length] =
        FWL_END_OF_STRING;

    i1Status = FwlParseMinAndMaxPort (pFilterNode->au1DestPort, &u2DestMaxPort,
                                      &u2DestMinPort);
    if (i1Status == SNMP_SUCCESS)
    {
        pFilterNode->u2DestMaxPort = u2DestMaxPort;
        pFilterNode->u2DestMinPort = u2DestMinPort;
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Destination Port is set for Filter %s \n",
                      au1FilterName);
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Dest Port SET Exit\n");
    if (i1Status == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlFilterDestPort, u4SeqNum,
                              FALSE, FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFwlFilterFilterName,
                          pSetValFwlFilterDestPort));
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFwlFilterAckBit
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterAckBit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlFilterAckBit (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                       INT4 i4SetValFwlFilterAckBit)
{
    /* Deprecated Object */
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    RM_GET_SEQ_NUM (&u4SeqNum);
    UNUSED_PARAM (pFwlFilterFilterName);
    UNUSED_PARAM (i4SetValFwlFilterAckBit);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlFilterAckBit, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFwlFilterFilterName,
                      i4SetValFwlFilterAckBit));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlFilterRstBit
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterRstBit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlFilterRstBit (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                       INT4 i4SetValFwlFilterRstBit)
{
    /* Deprecated Object */

    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    RM_GET_SEQ_NUM (&u4SeqNum);
    UNUSED_PARAM (pFwlFilterFilterName);
    UNUSED_PARAM (i4SetValFwlFilterRstBit);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlFilterRstBit, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFwlFilterFilterName,
                      i4SetValFwlFilterRstBit));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlFilterTos
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterTos
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlFilterTos (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                    INT4 i4SetValFwlFilterTos)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    tFilterInfo        *pFilterNode = NULL;

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n TOS bit SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /*  search for the filter and assign the TOS value to
     *  the Filter Node structure.    
     */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }
    pFilterNode->u1Tos = (UINT1) i4SetValFwlFilterTos;
    pFilterNode->u1RowStatus = FWL_NOT_IN_SERVICE;
    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n TOS Value %d is set for Filter %s \n",
                  i4SetValFwlFilterTos, au1FilterName);

    FWL_DBG (FWL_DBG_EXIT, "\n TOS bit SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlFilterTos, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFwlFilterFilterName,
                      i4SetValFwlFilterTos));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlFilterAccounting
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterAccounting
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlFilterAccounting (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                           INT4 i4SetValFwlFilterAccounting)
{
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tFilterInfo        *pFilterNode = NULL;

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Filter Accounting SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                pFwlFilterFilterName->i4_Length);
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /*  search for the filter and assign the TOS value to
     *  the Filter Node structure.
     */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    pFilterNode->u1FilterAccounting = (UINT1) i4SetValFwlFilterAccounting;

    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Filter Accounting Value %s is set for Filter %s \n",
                  ((i4SetValFwlFilterAccounting == FWL_ENABLE) ?
                   "FWL_ENABLE" : "FWL_DISABLE"), au1FilterName);

    FWL_DBG (FWL_DBG_EXIT, "\n Filter Accounting SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlFilterAccounting, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFwlFilterFilterName,
                      i4SetValFwlFilterAccounting));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFwlFilterHitClear
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterHitClear
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlFilterHitClear (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                         INT4 i4SetValFwlFilterHitClear)
{
    UINT4               u4SeqNum = FWL_ZERO;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    tFilterInfo        *pFilterNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Filter Hit Clear SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                pFwlFilterFilterName->i4_Length);
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /*  search for the filter and assign the TOS value to
     *  the Filter Node structure.
     */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (i4SetValFwlFilterHitClear == FWL_TRUE)
    {
        pFilterNode->u4FilterHitCount = FWL_DEFAULT_COUNT;    /* Clear the Hit 
                                                               Count */
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\nPacket Accounting statistics for Filter %s is cleared\n",
                      au1FilterName);
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\nPacket Accounting statistics for Filter %s is not cleared\n",
                      au1FilterName);
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Filter Hit Clear SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlFilterHitClear, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFwlFilterFilterName,
                      i4SetValFwlFilterHitClear));

    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhSetFwlFilterAddrType
 *  Input       :  The Indices
 *                 FwlFilterFilterName
 *                 The Object
 *                 setValFwlFilterAddrType
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlFilterAddrType (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                         INT4 i4SetValFwlFilterAddrType)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    FWL_DBG (FWL_DBG_ENTRY, "\n Address Type SET Entry \n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /*  Search for the filter and assign the protocol value 
     *  in the Filter Node structure.
     */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }
    pFilterNode->u2AddrType = (UINT2) i4SetValFwlFilterAddrType;

    FWL_DBG (FWL_DBG_EXIT, "\n Address Type SET Exit \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlFilterAddrType, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFwlFilterFilterName,
                      i4SetValFwlFilterAddrType));
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhSetFwlFilterFlowId
 *  Input       :  The Indices
 *                 FwlFilterFilterName
 *                 The Object
 *                 setValFwlFilterFlowId
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhSetFwlFilterFlowId (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                       UINT4 u4SetValFwlFilterFlowId)
{
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    RM_GET_SEQ_NUM (&u4SeqNum);
    FWL_DBG (FWL_DBG_ENTRY, "\n Flow Id SET Entry \n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /*  Search for the filter and assign the protocol value 
     *  in the Filter Node structure.
     */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }
    pFilterNode->u4FlowId = u4SetValFwlFilterFlowId;

    FWL_DBG (FWL_DBG_EXIT, "\n Flow Id  SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlFilterFlowId, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %u", pFwlFilterFilterName,
                      u4SetValFwlFilterFlowId));
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhSetFwlFilterDscp
 *  Input       :  The Indices
 *                 FwlFilterFilterName
 *                 The Object
 *                 setValFwlFilterDscp
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhSetFwlFilterDscp (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                     INT4 i4SetValFwlFilterDscp)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    FWL_DBG (FWL_DBG_ENTRY, "\n Dscp SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /*  Search for the filter and assign the protocol value 
     *  in the Filter Node structure.
     */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }
    pFilterNode->i4Dscp = i4SetValFwlFilterDscp;

    FWL_DBG (FWL_DBG_EXIT, "\n Dscp SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlFilterDscp, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFwlFilterFilterName,
                      i4SetValFwlFilterDscp));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFwlFilterRowStatus
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                setValFwlFilterRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlFilterRowStatus (tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                          INT4 i4SetValFwlFilterRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Status = FWL_FAILURE;
    UINT4               u4SeqNum = FWL_ZERO;
    tFilterInfo        *pFilterNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    UINT1               au1FwlMsg[FWL_CMD_DEF_BUF_SIZE] = { FWL_ZERO };
    UINT1               au1CmdStr[FWL_CMD_DEF_BUF_SIZE] = { FWL_ZERO };

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Filter Row Status SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* Serach the Filter in the Filter List */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);

    if ((NULL != pFilterNode) &&
        ((INT4) (pFilterNode->u1RowStatus) == i4SetValFwlFilterRowStatus))
    {
        return SNMP_SUCCESS;
    }

    /* The Filter Node can be created only when the row status is given
     * CREATE_AND_WAIT. The node will become ACTIVE when the appropriate fields 
     * are SET. The node will be deleted when the Row Status is DESTROY.
     */
    switch (i4SetValFwlFilterRowStatus)
    {
        case FWL_CREATE_AND_WAIT:
            if (pFilterNode != NULL)
            {
                i1Status = SNMP_SUCCESS;
                break;
            }
            /* Memory Allocation and Initialisation of Filter Node .  */
            if (FwlFilterMemAllocate ((UINT1 **) (VOID *) &pFilterNode) ==
                FWL_SUCCESS)
            {
                FwlSetDefaultFilterValue (au1FilterName, pFilterNode);

                /* CHANGE3 : A filter is added at the end of the  filter
                 * list(when created). SNMP GETNEXT requires to display the list
                 * in lexiographical order. In order to add to the list in sorted 
                 * order the following function is added.  
                 */
                /* Add the Filter Node in the Filter List and increment the 
                 * total filter count
                 */
                FwlDbaseAddFilter (pFilterNode);
                i1Status = SNMP_SUCCESS;
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n Filter %s is created\n", au1FilterName);
            }
            else
            {
                /*  Send a Trap for Memory faliure */
                FwlGenerateMemFailureTrap (FWL_STATIC_FILTER_ALLOC_FAILURE);
                INC_MEM_FAILURE_COUNT;
                gFwlAclInfo.i4MemStatus = (INT4) MEM_FAILURE;
            }
            break;
        case FWL_DESTROY:
            if (pFilterNode == NULL)
            {
                break;
            }
            /*  Search the Node in the Filter List and
             *   delete it if reference count is Zero.
             */

            if (pFilterNode->u1RowStatus == FWL_ACTIVE)
            {
                /*Firewall logs */
                SPRINTF ((CHR1 *) au1CmdStr, "no filter %s",
                         (CHR1 *) au1FilterName);
                SPRINTF ((CHR1 *) au1FwlMsg, " \"%s\" %s ", au1CmdStr,
                         FWL_CMD_EXEC_MSG);
                FwlLogMessage (FWL_COMM_CONF, FWL_ZERO, NULL, FWL_LOG_MUST,
                               FWLLOG_INFO_LEVEL, au1FwlMsg);
            }
            u4Status = FwlDbaseDeleteFilter (au1FilterName);

            if (u4Status == FWL_SUCCESS)
            {
                i1Status = SNMP_SUCCESS;
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n Filter %s is deleted\n", au1FilterName);

            }

            break;
        case FWL_ACTIVE:
            if (pFilterNode == NULL)
            {
                break;
            }
            /* Fix for dual logging: issue avoided */
            if (pFilterNode->u1RowStatus != FWL_NOT_READY)
            {
                /*Firewall logs */
                FwlGetFilterCmdString (pFilterNode, (INT1 *) au1FwlMsg);
                STRNCAT ((INT1 *) au1FwlMsg, FWL_CMD_EXEC_MSG,
                         FWL_CMD_STR_CAT_SIZE (au1FwlMsg));
                FwlLogMessage (FWL_COMM_CONF, FWL_ZERO, NULL, FWL_LOG_MUST,
                               FWLLOG_INFO_LEVEL, au1FwlMsg);

            }
            pFilterNode->u1RowStatus = FWL_ACTIVE;
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n RowStatus Value is made Active for Filter %s \n",
                          au1FilterName);
            i1Status = SNMP_SUCCESS;

            break;
            /* 
             * If on the fly modification of filters is needed then the 
             * following RowStatus Values is not required.
             */
        case FWL_NOT_IN_SERVICE:
            if (pFilterNode == NULL)
            {
                break;
            }
            pFilterNode->u1RowStatus = FWL_NOT_IN_SERVICE;
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n RowStatus Value is made Not_In_Service "
                          "for Filter %s \n", au1FilterName);
            i1Status = SNMP_SUCCESS;
            break;
        default:
            i1Status = SNMP_FAILURE;
            break;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Filter Row Status SET Exit \n");
    if (i1Status == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlFilterRowStatus, u4SeqNum,
                              TRUE, FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFwlFilterFilterName,
                          i4SetValFwlFilterRowStatus));
    }
    return i1Status;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlFilterSrcAddress
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterSrcAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlFilterSrcAddress (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValFwlFilterSrcAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tFilterInfo        *pFilterNode = NULL;
    tIp6Addr            SrcStartAddr;
    tIp6Addr            SrcEndAddr;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    UINT4               u4SrcStartAddr = FWL_ZERO;
    UINT4               u4SrcEndAddr = FWL_ZERO;
    UINT1               au1SrcAddr[FWL_MAX_ADDR_LEN] = { FWL_ZERO };
    /* Added new variables to validate IP address */
    INT4                i4RetVal = FWL_ZERO;

    pFilterNode = (tFilterInfo *) NULL;
    MEMSET (&SrcStartAddr, FWL_ZERO, sizeof (tIp6Addr));
    MEMSET (&SrcEndAddr, FWL_ZERO, sizeof (tIp6Addr));

    FWL_DBG (FWL_DBG_ENTRY, "\nSrc Address TEST routine Entry\n");

    if ((pFwlFilterFilterName->pu1_OctetList == NULL) ||
        (pTestValFwlFilterSrcAddress->pu1_OctetList == NULL))
    {
        return SNMP_FAILURE;
    }

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                pFwlFilterFilterName->i4_Length);
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /*  Search the Filter in the Filter list  */

    pFilterNode = FwlDbaseSearchFilter (au1FilterName);

    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }

    FWL_MEMCPY (au1SrcAddr, pTestValFwlFilterSrcAddress->pu1_OctetList,
                MEM_MAX_BYTES (pTestValFwlFilterSrcAddress->i4_Length,
                               (INT4) sizeof (au1SrcAddr)));
    au1SrcAddr[pTestValFwlFilterSrcAddress->i4_Length] = FWL_END_OF_STRING;

    if (pFilterNode->u2AddrType == FWL_IP_VERSION_4)
    {

        /* Parse the string and find Start and End Source IP address  */

        i1Status = FwlParseIpAddr (au1SrcAddr, &u4SrcStartAddr, &u4SrcEndAddr);

        if (i1Status == FWL_SUCCESS)
        {
            if ((u4SrcStartAddr == FWL_ZERO) &&
                (u4SrcEndAddr == FWL_BROADCAST_ADDR))
            {
                /* User has configured "any" option through CLI
                 * so return SUCCESS */
                FWL_DBG (FWL_DBG_EXIT, "\n Src Address TEST routine Exit \n");
                return SNMP_SUCCESS;
            }
            i4RetVal = FwlValidateIpAddress (u4SrcStartAddr);
            if ((i4RetVal == FWL_BCAST_ADDR) ||
                (i4RetVal == FWL_ZERO_NETW_ADDR) ||
                (i4RetVal == FWL_LOOPBACK_ADDR) ||
                (i4RetVal == FWL_INVALID_ADDR) ||
                (i4RetVal == FWL_CLASS_BCASTADDR)
                || (i4RetVal == FWL_CLASSE_ADDR))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                FWL_DBG (FWL_DBG_EXIT, "\n Src Address TEST routine Exit \n");
                return SNMP_FAILURE;
            }
            i4RetVal = FwlValidateIpAddress (u4SrcEndAddr);
            if ((i4RetVal == FWL_BCAST_ADDR) ||
                (i4RetVal == FWL_ZERO_NETW_ADDR) ||
                (i4RetVal == FWL_LOOPBACK_ADDR) ||
                (i4RetVal == FWL_INVALID_ADDR) ||
                (i4RetVal == FWL_CLASS_BCASTADDR)
                || (i4RetVal == FWL_CLASSE_ADDR))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                FWL_DBG (FWL_DBG_EXIT, "\n Src Address TEST routine Exit \n");
                return SNMP_FAILURE;
            }
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            i1Status = SNMP_FAILURE;
        }
    }
    else if (pFilterNode->u2AddrType == FWL_IP_VERSION_6)
    {
        i1Status = FwlParseIpv6Addr (au1SrcAddr, &SrcStartAddr, &SrcEndAddr);
        if (i1Status == FWL_SUCCESS)
        {
            if (IS_ADDR_UNSPECIFIED (SrcStartAddr))
            {
                /* User has configured "any" option through CLI
                 * so return SUCCESS */
                FWL_DBG (FWL_DBG_EXIT,
                         "\n Src IPv6 Address TEST routine Exit \n");
                return SNMP_SUCCESS;
            }
            i4RetVal = Ip6AddrType (&SrcStartAddr);
            if ((i4RetVal == ADDR6_MULTI) ||
                (i4RetVal == ADDR6_LLOCAL) ||
                (i4RetVal == ADDR6_LOOPBACK) ||
                (i4RetVal == ADDR6_V4_COMPAT) || (i4RetVal == ADDR6_INVALID))
            {
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n Invalid Unicast IPv6 Address %s given\n",
                              Ip6PrintNtop (&SrcStartAddr));

                CLI_SET_ERR (CLI_FWL_INVALID_UCAST_ADDR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                FWL_DBG (FWL_DBG_EXIT, "\n Src Address TEST routine Exit \n");
                return SNMP_FAILURE;
            }
            i4RetVal = Ip6AddrType (&SrcEndAddr);
            if ((i4RetVal == ADDR6_MULTI) ||
                (i4RetVal == ADDR6_LLOCAL) ||
                (i4RetVal == ADDR6_LOOPBACK) ||
                (i4RetVal == ADDR6_V4_COMPAT) || (i4RetVal == ADDR6_INVALID))
            {
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n Invalid Unicast IPv6 Address %s given\n",
                              Ip6PrintNtop (&SrcEndAddr));

                CLI_SET_ERR (CLI_FWL_INVALID_UCAST_ADDR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                FWL_DBG (FWL_DBG_EXIT, "\n Src Address TEST routine Exit \n");
                return SNMP_FAILURE;
            }
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            i1Status = SNMP_FAILURE;
        }
    }
    FWL_DBG (FWL_DBG_EXIT, "\n Src Address TEST routine Exit \n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlFilterDestAddress
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterDestAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlFilterDestAddress (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFwlFilterDestAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tFilterInfo        *pFilterNode = NULL;
    tIp6Addr            DestStartAddr;
    tIp6Addr            DestEndAddr;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    UINT4               u4DestStartAddr = FWL_ZERO;
    UINT4               u4DestEndAddr = FWL_ZERO;
    UINT1               au1DestAddr[FWL_MAX_ADDR_LEN] = { FWL_ZERO };
    /* Added new variables to validate IP address */
    INT4                i4RetVal = FWL_ZERO;

    pFilterNode = (tFilterInfo *) NULL;

    MEMSET (&DestStartAddr, FWL_ZERO, sizeof (tIp6Addr));
    MEMSET (&DestEndAddr, FWL_ZERO, sizeof (tIp6Addr));

    FWL_DBG (FWL_DBG_ENTRY, "\nDest Address TEST routine Entry\n");

    if ((pFwlFilterFilterName->pu1_OctetList == NULL) ||
        (pTestValFwlFilterDestAddress->pu1_OctetList == NULL))
    {
        return SNMP_FAILURE;
    }

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                pFwlFilterFilterName->i4_Length);
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /*  Search the Filter in the Filter list  */

    pFilterNode = FwlDbaseSearchFilter (au1FilterName);

    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Validate IP address whether it is a BCAST, MCAST, ZERNETWORK etc etcc */
    FWL_MEMCPY (au1DestAddr, pTestValFwlFilterDestAddress->pu1_OctetList,
                MEM_MAX_BYTES (pTestValFwlFilterDestAddress->i4_Length,
                               (INT4) sizeof (au1DestAddr)));
    au1DestAddr[pTestValFwlFilterDestAddress->i4_Length] = FWL_END_OF_STRING;

    if (pFilterNode->u2AddrType == FWL_IP_VERSION_4)
    {
        /* Parse the string and find Start and End Destination IP address  */

        i1Status =
            FwlParseIpAddr (au1DestAddr, &u4DestStartAddr, &u4DestEndAddr);

        if (i1Status == FWL_SUCCESS)
        {
            if ((u4DestStartAddr == FWL_ZERO) &&
                (u4DestEndAddr == FWL_BROADCAST_ADDR))
            {
                /* User has configured "any" option through CLI
                 * so return SUCCESS */
                FWL_DBG (FWL_DBG_EXIT, "\n Dest Address TEST routine Exit \n");
                return SNMP_SUCCESS;
            }
            i4RetVal = FwlValidateIpAddress (u4DestStartAddr);
            if ((i4RetVal == FWL_BCAST_ADDR) ||
                (i4RetVal == FWL_ZERO_NETW_ADDR) ||
                (i4RetVal == FWL_LOOPBACK_ADDR) ||
                (i4RetVal == FWL_INVALID_ADDR) ||
                (i4RetVal == FWL_CLASS_BCASTADDR)
                || (i4RetVal == FWL_CLASSE_ADDR))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                FWL_DBG (FWL_DBG_EXIT, "\n Dest Address TEST routine Exit \n");
                return SNMP_FAILURE;
            }
            i4RetVal = FwlValidateIpAddress (u4DestEndAddr);
            if ((i4RetVal == FWL_BCAST_ADDR) ||
                (i4RetVal == FWL_ZERO_NETW_ADDR) ||
                (i4RetVal == FWL_LOOPBACK_ADDR) ||
                (i4RetVal == FWL_INVALID_ADDR) ||
                (i4RetVal == FWL_CLASS_BCASTADDR)
                || (i4RetVal == FWL_CLASSE_ADDR))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                FWL_DBG (FWL_DBG_EXIT, "\n Dest Address TEST routine Exit \n");
                return SNMP_FAILURE;
            }
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            i1Status = SNMP_FAILURE;
        }
    }
    else if (pFilterNode->u2AddrType == FWL_IP_VERSION_6)
    {
        i1Status = FwlParseIpv6Addr (au1DestAddr, &DestStartAddr, &DestEndAddr);
        if (i1Status == FWL_SUCCESS)
        {
            if (IS_ADDR_UNSPECIFIED (DestStartAddr))
            {
                /* User has configured "any" option through CLI
                 * so return SUCCESS */
                FWL_DBG (FWL_DBG_EXIT,
                         "\n Src IPv6 Address TEST routine Exit \n");
                return SNMP_SUCCESS;
            }
            i4RetVal = Ip6AddrType (&DestStartAddr);
            if ((i4RetVal == ADDR6_MULTI) ||
                (i4RetVal == ADDR6_LLOCAL) ||
                (i4RetVal == ADDR6_LOOPBACK) ||
                (i4RetVal == ADDR6_V4_COMPAT) || (i4RetVal == ADDR6_INVALID))
            {
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n Invalid Unicast IPv6 Address %s given\n",
                              Ip6PrintNtop (&DestStartAddr));

                CLI_SET_ERR (CLI_FWL_INVALID_UCAST_ADDR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                FWL_DBG (FWL_DBG_EXIT,
                         "\n Dest Ipv6 Address TEST routine Exit \n");
                return SNMP_FAILURE;
            }
            i4RetVal = Ip6AddrType (&DestEndAddr);
            if ((i4RetVal == ADDR6_MULTI) ||
                (i4RetVal == ADDR6_LLOCAL) ||
                (i4RetVal == ADDR6_LOOPBACK) ||
                (i4RetVal == ADDR6_V4_COMPAT) || (i4RetVal == ADDR6_INVALID))
            {
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n Invalid Unicast IPv6 Address %s given\n",
                              Ip6PrintNtop (&DestEndAddr));

                CLI_SET_ERR (CLI_FWL_INVALID_UCAST_ADDR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                FWL_DBG (FWL_DBG_EXIT,
                         "\n Dest IPv6 Address TEST routine Exit \n");
                return SNMP_FAILURE;
            }
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            i1Status = SNMP_FAILURE;
        }
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Dest Address TEST routine Exit \n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlFilterProtocol
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlFilterProtocol (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                            INT4 i4TestValFwlFilterProtocol)
{
    INT1                i1Status = SNMP_FAILURE;
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nProtocol TEST routine Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* Validate the value of the protocol based on the Row status. If the Row 
     * Status ia FWL_NOT_IN_SERVICE, see whether the protocol is already set. 
     * Else return Failure. 
     */
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\nTrying to Set Protocol Value %d for Filter %s\n",
                  i4TestValFwlFilterProtocol, au1FilterName);
    if (pFilterNode != NULL)
    {
        i1Status = FwlValidateProtocol (i4TestValFwlFilterProtocol);
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Filter %s doesnt exists\n", au1FilterName);
    }
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Protocol TEST routine Exit \n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlFilterSrcPort
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterSrcPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlFilterSrcPort (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                           tSNMP_OCTET_STRING_TYPE * pTestValFwlFilterSrcPort)
{
    INT1                i1Status = SNMP_FAILURE;
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    UINT1               au1SrcPort[FWL_MAX_PORT_LEN] = { FWL_ZERO };
    UINT2               u2SrcMaxPort = FWL_ZERO;
    UINT2               u2SrcMinPort = FWL_ZERO;

    pFilterNode = (tFilterInfo *) NULL;

    UNUSED_PARAM (pFilterNode);
    FWL_DBG (FWL_DBG_ENTRY, "\nSrc Port TEST routine Entry\n");
#ifdef SNMP_2_WANTED
    if (OSIX_FAILURE ==
        SNMPCheckForNVTChars (pTestValFwlFilterSrcPort->pu1_OctetList,
                              pTestValFwlFilterSrcPort->i4_Length))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
    if ((pTestValFwlFilterSrcPort->i4_Length >= FWL_MAX_PORT_LEN)
        || (pTestValFwlFilterSrcPort->i4_Length <= FWL_ZERO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    if (pTestValFwlFilterSrcPort->i4_Length < FWL_MAX_PORT_LEN)
    {
        FWL_MEMCPY (au1SrcPort, pTestValFwlFilterSrcPort->pu1_OctetList,
                    MEM_MAX_BYTES (pTestValFwlFilterSrcPort->i4_Length,
                                   (INT4) sizeof (au1SrcPort)));
        au1SrcPort[pTestValFwlFilterSrcPort->i4_Length] = FWL_END_OF_STRING;

        i1Status = FwlParseMinAndMaxPort (au1SrcPort, &u2SrcMaxPort,
                                          &u2SrcMinPort);
    }

    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL", "\nInvalid Port\n");
    }
    FWL_DBG (FWL_DBG_EXIT, "\n Src Port TEST routine Exit \n");
    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlFilterDestPort
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterDestPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlFilterDestPort (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                            tSNMP_OCTET_STRING_TYPE * pTestValFwlFilterDestPort)
{
    INT1                i1Status = SNMP_FAILURE;
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    UINT1               au1DestPort[FWL_MAX_PORT_LEN] = { FWL_ZERO };
    UINT2               u2DestMaxPort = FWL_ZERO;
    UINT2               u2DestMinPort = FWL_ZERO;

    pFilterNode = (tFilterInfo *) NULL;

    UNUSED_PARAM (pFilterNode);
    FWL_DBG (FWL_DBG_ENTRY, "\nDestination Port TEST routine Entry\n");
#ifdef SNMP_2_WANTED
    if (OSIX_FAILURE ==
        SNMPCheckForNVTChars (pTestValFwlFilterDestPort->pu1_OctetList,
                              pTestValFwlFilterDestPort->i4_Length))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
    if ((pTestValFwlFilterDestPort->i4_Length >= FWL_MAX_PORT_LEN)
        || (pTestValFwlFilterDestPort->i4_Length <= FWL_ZERO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    if (pTestValFwlFilterDestPort->i4_Length < FWL_MAX_PORT_LEN)
    {
        FWL_MEMCPY (au1DestPort, pTestValFwlFilterDestPort->pu1_OctetList,
                    MEM_MAX_BYTES (pTestValFwlFilterDestPort->i4_Length,
                                   (INT4) sizeof (au1DestPort)));
        au1DestPort[pTestValFwlFilterDestPort->i4_Length] = FWL_END_OF_STRING;

        i1Status = FwlParseMinAndMaxPort (au1DestPort,
                                          &u2DestMaxPort, &u2DestMinPort);
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL", "\nInvalid Port\n");
    }

    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    FWL_DBG (FWL_DBG_EXIT, "\nDestination Port TEST routine Exit\n");
    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlFilterAckBit
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterAckBit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlFilterAckBit (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                          INT4 i4TestValFwlFilterAckBit)
{
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    tFilterInfo        *pFilterNode = NULL;

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nAck Bit TEST routine Entry\n");

    if ((i4TestValFwlFilterAckBit > FWL_TCP_ACK_ANY)
        || (i4TestValFwlFilterAckBit < FWL_TCP_ACK_ESTABLISH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n %d is an invalid Value. "
                      "ACK Bit can take the value 1,2,3\n",
                      i4TestValFwlFilterAckBit);
        return SNMP_FAILURE;
    }

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* Validate the TCP ack value based on the Row status. If the Row Status is
     * FWL_NOT_IN_SERVICE, the TCP ack bit is not already set and the Protocol
     * is TCP than return SUCCESS, else FAILURE.
     */
    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\nTrying to Set Ack Bit Value %d for Filter %s\n",
                  i4TestValFwlFilterAckBit, au1FilterName);
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode != NULL)
    {
        if (pFilterNode->u1Proto != FWL_TCP)
        {
            if (i4TestValFwlFilterAckBit != FWL_TCP_ACK_ANY)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\nACK bit has to be 3 if Protocol is other than TCP");
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Filter %s doesnt exists\n", au1FilterName);
    }
    FWL_DBG (FWL_DBG_EXIT, "\nAck Bit TEST routine Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FwlFilterRstBit
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterRstBit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlFilterRstBit (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                          INT4 i4TestValFwlFilterRstBit)
{
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    tFilterInfo        *pFilterNode = NULL;

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nSyn Bit TEST routine Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));

    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* Validate the TCP Rst value based on the Row status. If the Row Status is
     * FWL_NOT_IN_SERVICE, the TCP Rst bit is not already set and the Protocol
     * is TCP than return SUCCESS, else FAILURE.
     */
    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\nTrying to Set Rst Bit Value %d for Filter %s\n",
                  i4TestValFwlFilterRstBit, au1FilterName);

    if ((i4TestValFwlFilterRstBit > FWL_TCP_RST_ANY)
        || (i4TestValFwlFilterRstBit < FWL_TCP_RST_SET))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n %d is an invalid Value. "
                      "Rst Bit can take the value 1,2,3\n",
                      i4TestValFwlFilterRstBit);
        return SNMP_FAILURE;
    }

    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode != NULL)
    {
        if (pFilterNode->u1Proto != FWL_TCP)
        {
            if (i4TestValFwlFilterRstBit != FWL_TCP_RST_ANY)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\nRST bit has to be 3 if Protocol is other than TCP");
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Filter %s doesnt exists\n", au1FilterName);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\nSyn Bit TEST routine Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FwlFilterTos
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterTos
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlFilterTos (UINT4 *pu4ErrorCode,
                       tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                       INT4 i4TestValFwlFilterTos)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    tFilterInfo        *pFilterNode = NULL;

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nTOS Bit TEST routine Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));

    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* Validate the TOS value based on the Row status. If the Row Status is
     *FWL_NOT_IN_SERVICE and the TOS bit is not already set than return SUCCESS, 
     * else FAILURE.
     */
    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\nTrying to Set TOS Value %d for Filter %s\n",
                  i4TestValFwlFilterTos, au1FilterName);
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode != NULL)
    {
        if ((i4TestValFwlFilterTos <= FWL_TOS_MAX_VALUE)
            && (i4TestValFwlFilterTos >= FWL_TOS_ANY))
        {
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n %d is an invalid Value. The range is 1 to %d\n",
                          i4TestValFwlFilterTos, FWL_TOS_MAX_VALUE);
        }
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Filter %s doesnt exists\n", au1FilterName);
    }
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\nTos Bit TEST routine Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlFilterAccounting
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterAccounting
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlFilterAccounting (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                              INT4 i4TestValFwlFilterAccounting)
{
    INT1                i1Status = SNMP_FAILURE;
    tFilterInfo        *pFilterNode = (tFilterInfo *) NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    FWL_DBG (FWL_DBG_ENTRY, "\nFilter Accounting TEST routine Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                pFwlFilterFilterName->i4_Length);

    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* Validate the Filter Accounting value. If validation is successful
     * return SUCCESS, else FAILURE. */

    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\nTrying to Set Filter Accounting Value %d for Filter %s\n",
                  i4TestValFwlFilterAccounting, au1FilterName);

    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode != NULL)
    {
        if ((i4TestValFwlFilterAccounting == FWL_ENABLE)
            || (i4TestValFwlFilterAccounting == FWL_DISABLE))
        {
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n %d is an invalid Value.\n",
                          i4TestValFwlFilterAccounting);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Filter %s doesnt exists\n", au1FilterName);
    }
    FWL_DBG (FWL_DBG_EXIT, "\nFilter Accounting TEST routine Exit\n");

    return i1Status;

}

/****************************************************************************
 Function    :  nmhTestv2FwlFilterHitClear
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterHitClear
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlFilterHitClear (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                            INT4 i4TestValFwlFilterHitClear)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    tFilterInfo        *pFilterNode = NULL;

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nFilter Hit Clear TEST routine Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                pFwlFilterFilterName->i4_Length);

    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* Validate the Filter Hit Clear value. If validation is successful
     * return SUCCESS, else FAILURE. */

    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\nTrying to Set Filter Hit Clear Value %d for Filter %s\n",
                  i4TestValFwlFilterHitClear, au1FilterName);

    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode != NULL)
    {
        if ((i4TestValFwlFilterHitClear == FWL_TRUE)
            || (i4TestValFwlFilterHitClear == FWL_FALSE))
        {
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n %d is an invalid Value.\n",
                          i4TestValFwlFilterHitClear);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Filter %s doesnt exists\n", au1FilterName);
    }
    FWL_DBG (FWL_DBG_EXIT, "\nFilter Hit Clear TEST routine Exit\n");

    return i1Status;

}

/****************************************************************************
 *  Function    :  nmhTestv2FwlFilterAddrType
 *  Input       :  The Indices
 *                 FwlFilterFilterName
 *                 The Object
 *                 testValFwlFilterAddrType
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                 Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val
 *  Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhTestv2FwlFilterAddrType (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                            INT4 i4TestValFwlFilterAddrType)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    tFilterInfo        *pFilterNode = NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nAddress Type Test routine Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));

    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\nTrying to Set Address Type %d for Filter %s\n",
                  i4TestValFwlFilterAddrType, au1FilterName);
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode != NULL)
    {
        if ((i4TestValFwlFilterAddrType == FWL_IP_VERSION_4)
            || (i4TestValFwlFilterAddrType == FWL_IP_VERSION_6))
        {
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n %d is an invalid Value. The value should be either 1 or 2\n",
                          i4TestValFwlFilterAddrType);
        }
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\nFilter %s doesnt exists\n", au1FilterName);
    }
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\nAddress Type TEST routine Exit\n");

    return i1Status;
}

/****************************************************************************
 *  Function    :  nmhTestv2FwlFilterFlowId
 *  Input       :  The Indices
 *                 FwlFilterFilterName
 *                 The Object
 *                 testValFwlFilterFlowId
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                 Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val
 *  Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhTestv2FwlFilterFlowId (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                          UINT4 u4TestValFwlFilterFlowId)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    tFilterInfo        *pFilterNode = NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nFlow Id routine Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));

    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\nTrying to Set Flow Label Value %d for Filter %s\n",
                  u4TestValFwlFilterFlowId, au1FilterName);
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode != NULL)
    {
        if (u4TestValFwlFilterFlowId <= FWL_FLOW_ID_MAX_VALUE)
        {
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n %d is an invalid Value. The range is 0 to %d\n",
                          u4TestValFwlFilterFlowId, FWL_FLOW_ID_MAX_VALUE);
        }
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Filter %s doesnt exists\n", au1FilterName);
    }
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\nFlow Id TEST routine Exit\n");

    return i1Status;
}

/****************************************************************************
 *  Function    :  nmhTestv2FwlFilterDscp
 *  Input       :  The Indices
 *                 FwlFilterFilterName
 *                 The Object
 *                 testValFwlFilterDscp
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                 Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val
 *  Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhTestv2FwlFilterDscp (UINT4 *pu4ErrorCode,
                        tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                        INT4 i4TestValFwlFilterDscp)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    tFilterInfo        *pFilterNode = NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nDscp TEST routine Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                               (INT4) sizeof (au1FilterName)));

    au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;

    /* Validate the DSCP value based on the Row status. If the Row Status is
     *FWL_NOT_IN_SERVICE and the TOS bit is not already set than return SUCCESS, 
     * else FAILURE.
     */
    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\nTrying to Set DSCP Value %d for Filter %s\n",
                  i4TestValFwlFilterDscp, au1FilterName);
    pFilterNode = FwlDbaseSearchFilter (au1FilterName);
    if (pFilterNode != NULL)
    {
        if ((i4TestValFwlFilterDscp <= FWL_DSCP_MAX_VALUE)
            && (i4TestValFwlFilterDscp >= FWL_DSCP_MIN_VALUE))
        {
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n %d is an invalid Value. The range is 1 to %d\n",
                          i4TestValFwlFilterDscp, FWL_DSCP_MAX_VALUE);
        }
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Filter %s doesnt exists\n", au1FilterName);
    }
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\nDSCP TEST routine Exit\n");

    return i1Status;

}

/****************************************************************************
 Function    :  nmhTestv2FwlFilterRowStatus
 Input       :  The Indices
                FwlFilterFilterName

                The Object 
                testValFwlFilterRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlFilterRowStatus (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pFwlFilterFilterName,
                             INT4 i4TestValFwlFilterRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tFilterInfo        *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nFilter RowStatus TEST routine Entry\n");

    /* Validate the Row status value. The filter name length should be less that
     * FWL_MAX_FILTER_NAME_LEN.
     */
    if ((pFwlFilterFilterName->i4_Length <= FWL_MAX_FILTER_NAME_LEN -
         FWL_ONE) && (pFwlFilterFilterName->i4_Length > FWL_ZERO))
    {
        FWL_MEMCPY (au1FilterName, pFwlFilterFilterName->pu1_OctetList,
                    MEM_MAX_BYTES (pFwlFilterFilterName->i4_Length,
                                   (INT4) sizeof (au1FilterName)));
        au1FilterName[pFwlFilterFilterName->i4_Length] = FWL_END_OF_STRING;
        MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\nTrying to Set RowStatus Value %d for Filter %s\n",
                      i4TestValFwlFilterRowStatus, au1FilterName);
        pFilterNode = FwlDbaseSearchFilter (au1FilterName);

        if ((NULL != pFilterNode) &&
            ((INT4) (pFilterNode->u1RowStatus) == i4TestValFwlFilterRowStatus))
        {
            return SNMP_SUCCESS;
        }

        switch (i4TestValFwlFilterRowStatus)
        {
            case FWL_CREATE_AND_WAIT:
                /* If the index doesnt exist already, then it can be created. */
                if (pFilterNode == NULL)
                {
                    i1Status = SNMP_SUCCESS;
                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\nFilter %s already exists\n",
                                  au1FilterName);
                    i1Status = SNMP_FAILURE;
                }
                break;
                /* 
                 * If on the fly modification of filters is needed then the 
                 * following RowStatus Values is not required.
                 */
            case FWL_NOT_IN_SERVICE:
                /* The row can be made FWL_NOT_IN_SERVICE if the row exist. */
                if (pFilterNode != NULL)
                {
                    if (pFilterNode->u1RowStatus == FWL_ACTIVE)
                    {
                        i1Status = SNMP_SUCCESS;
                    }
                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\nFilter %s doesnt exists\n", au1FilterName);
                }
                break;
            case FWL_ACTIVE:
                /* The row can be made FWL_ACTIVE if the row exists and the
                 * RowStatus is FWL_NOT_IN_SERVICE and the necessary fileds are 
                 * set.
                 */
                if (pFilterNode != NULL)
                {

                    if (FWL_NOT_IN_SERVICE == pFilterNode->u1RowStatus)
                    {
                        i1Status = SNMP_SUCCESS;
                    }
                    else
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                 "\n Incomplete Filter or RowStatus "
                                 "is already ACTIVE. "
                                 "So Row Status cannot be ACTIVE\n");
                    }
                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\nFilter %s doesnt exists\n", au1FilterName);
                }
                break;
            case FWL_DESTROY:
                /* The row can be deleted if it exists. */
                if ((pFilterNode != NULL) &&
                    (pFilterNode->u1FilterRefCount == FWL_DEFAULT_COUNT))
                {
                    i1Status = SNMP_SUCCESS;
                }
                else
                {
                    if (pFilterNode == NULL)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                        CLI_SET_ERR (CLI_FWL_NO_SUCH_ACL);
                    }
                    else
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (CLI_FWL_REFERENCED_FILTER);
                    }
                }
                break;
            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i1Status = SNMP_FAILURE;
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\n Row Status Value ranges from 1 to 6\n");
                break;
        }                        /* end of Switch */
    }                            /* end of if */
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\nMaximum Filter Name Length is %d\n",
                      FWL_MAX_FILTER_NAME_LEN - FWL_ONE);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i1Status = SNMP_FAILURE;
    }
    FWL_DBG (FWL_DBG_EXIT, "\nFilter RowStatus TEST routine Exit\n");
    return i1Status;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlDefnFilterTable                                                           Input       :  The Indices
                FwlFilterFilterName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDefnFilterTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FwlDefnRuleTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlDefnRuleTable
 Input       :  The Indices
                FwlRuleRuleName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFwlDefnRuleTable (tSNMP_OCTET_STRING_TYPE *
                                          pFwlRuleRuleName)
{
    INT1                i1Status = SNMP_FAILURE;
    tRuleInfo          *pRuleNode = NULL;
    UINT1               au1RuleName[FWL_MAX_RULE_NAME_LEN] = { FWL_ZERO };

    pRuleNode = (tRuleInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Validate Index for Rule Table - Entry\n");

    if (pFwlRuleRuleName->i4_Length <= FWL_MAX_RULE_NAME_LEN - FWL_ONE)
    {

        FWL_MEMCPY (au1RuleName, pFwlRuleRuleName->pu1_OctetList,
                    MEM_MAX_BYTES (pFwlRuleRuleName->i4_Length,
                                   (INT4) sizeof (au1RuleName)));
        au1RuleName[pFwlRuleRuleName->i4_Length] = FWL_END_OF_STRING;

        /* check whether the rule name exists in the rule list */
        pRuleNode = FwlDbaseSearchRule (au1RuleName);
        if (pRuleNode != NULL)
        {
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\nRule %s doesnt exists\n", au1RuleName);
        }

    }

    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\nMaximum Rule Name Length is %d\n",
                      FWL_MAX_RULE_NAME_LEN - FWL_ONE);
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Validate Index for Rule Table - Exit\n");
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFwlDefnRuleTable
 Input       :  The Indices
                FwlRuleRuleName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFwlDefnRuleTable (tSNMP_OCTET_STRING_TYPE * pFwlRuleRuleName)
{
    INT1                i1Status = FWL_ZERO;
    tRuleInfo          *pRuleNode = NULL;

    i1Status = SNMP_FAILURE;
    pRuleNode = (tRuleInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Get First Index for Rule Table - Entry\n");

    /* get the first rule node from the rule list and return the rule name */
    if (TMO_SLL_Count (&gFwlAclInfo.ruleList) != FWL_DEFAULT_COUNT)
    {
        pRuleNode = (tRuleInfo *) TMO_SLL_First (&gFwlAclInfo.ruleList);
        pFwlRuleRuleName->i4_Length = (INT4)
            FWL_STRLEN ((INT1 *) pRuleNode->au1RuleName);
        FWL_MEMCPY (pFwlRuleRuleName->pu1_OctetList, pRuleNode->au1RuleName,
                    pFwlRuleRuleName->i4_Length);
        i1Status = SNMP_SUCCESS;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Get First Index for Rule Table - Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFwlDefnRuleTable
 Input       :  The Indices
                FwlRuleRuleName
                nextFwlRuleRuleName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFwlDefnRuleTable (tSNMP_OCTET_STRING_TYPE * pFwlRuleRuleName,
                                 tSNMP_OCTET_STRING_TYPE * pNextFwlRuleRuleName)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               au1RuleName[FWL_MAX_RULE_NAME_LEN] = { FWL_ZERO };
    tRuleInfo          *pRuleNode = NULL;
    tRuleInfo          *pRuleNextNode = NULL;

    pRuleNextNode = (tRuleInfo *) NULL;
    pRuleNode = (tRuleInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Get Next Index for Rule Table - Entry\n");

    FWL_MEMCPY (au1RuleName, pFwlRuleRuleName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlRuleRuleName->i4_Length,
                               (INT4) sizeof (au1RuleName)));
    au1RuleName[pFwlRuleRuleName->i4_Length] = FWL_END_OF_STRING;

    /* Search the rule name in the rule list  */
    pRuleNode = FwlDbaseSearchRule (au1RuleName);
    if (pRuleNode != NULL)
    {
        /*  get the rule node next to the rule name given and return
         * the next rule name 
         */
        pRuleNextNode = (tRuleInfo *) TMO_SLL_Next (&gFwlAclInfo.ruleList,
                                                    (tTMO_SLL_NODE *)
                                                    pRuleNode);
        if (pRuleNextNode != NULL)
        {
            pNextFwlRuleRuleName->i4_Length = (INT4)
                FWL_STRLEN ((INT1 *) pRuleNextNode->au1RuleName);
            FWL_MEMCPY (pNextFwlRuleRuleName->pu1_OctetList,
                        pRuleNextNode->au1RuleName,
                        pNextFwlRuleRuleName->i4_Length);
            i1Status = SNMP_SUCCESS;
        }
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Get Next Index for Rule Table - Exit\n");

    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlRuleFilterSet
 Input       :  The Indices
                FwlRuleRuleName

                The Object 
                retValFwlRuleFilterSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlRuleFilterSet (tSNMP_OCTET_STRING_TYPE * pFwlRuleRuleName,
                        tSNMP_OCTET_STRING_TYPE * pRetValFwlRuleFilterSet)
{
    tRuleInfo          *pRuleNode = NULL;
    UINT1               au1RuleName[FWL_MAX_RULE_NAME_LEN] = { FWL_ZERO };

    pRuleNode = (tRuleInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nRule FIlter Set GET Entry\n");

    FWL_MEMCPY (au1RuleName, pFwlRuleRuleName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlRuleRuleName->i4_Length,
                               (INT4) sizeof (au1RuleName)));
    au1RuleName[pFwlRuleRuleName->i4_Length] = FWL_END_OF_STRING;

    /*  Search the rule name from the rule list  */
    pRuleNode = FwlDbaseSearchRule (au1RuleName);
    if (pRuleNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /*  get the filter set as a list of names with '&' or ',' operation. */
    pRetValFwlRuleFilterSet->i4_Length = (INT4)
        FWL_STRLEN ((INT1 *) pRuleNode->au1FilterSet);
    FWL_MEMCPY (pRetValFwlRuleFilterSet->pu1_OctetList,
                pRuleNode->au1FilterSet, pRetValFwlRuleFilterSet->i4_Length);

    FWL_DBG (FWL_DBG_EXIT, "\nRule FIlter Set GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlRuleRowStatus
 Input       :  The Indices
                FwlRuleRuleName

                The Object 
                retValFwlRuleRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlRuleRowStatus (tSNMP_OCTET_STRING_TYPE * pFwlRuleRuleName,
                        INT4 *pi4RetValFwlRuleRowStatus)
{
    tRuleInfo          *pRuleNode;
    UINT1               au1RuleName[FWL_MAX_RULE_NAME_LEN + FWL_ONE] =
        { FWL_ZERO };

    pRuleNode = (tRuleInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nRule RowStatus GET Entry\n");

    FWL_MEMCPY (au1RuleName, pFwlRuleRuleName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlRuleRuleName->i4_Length,
                               (INT4) sizeof (au1RuleName)));
    au1RuleName[pFwlRuleRuleName->i4_Length] = FWL_END_OF_STRING;

    /*  Search the rule name from the rule list  */
    pRuleNode = FwlDbaseSearchRule (au1RuleName);

    if (pRuleNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    /*  get the row status value */
    *pi4RetValFwlRuleRowStatus = (INT4) pRuleNode->u1RowStatus;

    FWL_DBG (FWL_DBG_EXIT, "\nRule RowStatus GET Exit\n");

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlRuleFilterSet
 Input       :  The Indices
                FwlRuleRuleName

                The Object 
                setValFwlRuleFilterSet
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlRuleFilterSet (tSNMP_OCTET_STRING_TYPE * pFwlRuleRuleName,
                        tSNMP_OCTET_STRING_TYPE * pSetValFwlRuleFilterSet)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    INT1                i1Status = SNMP_FAILURE;
    tRuleInfo          *pRuleNode = NULL;
    UINT1               au1RuleName[FWL_MAX_RULE_NAME_LEN + FWL_ONE] =
        { FWL_ZERO };
    UINT1               au1FilterSet[FWL_MAX_FILTER_SET_LEN] = { FWL_ZERO };

    pRuleNode = (tRuleInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Rule Filter Set SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    FWL_MEMCPY (au1RuleName, pFwlRuleRuleName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlRuleRuleName->i4_Length,
                               (INT4) sizeof (au1RuleName)));
    au1RuleName[pFwlRuleRuleName->i4_Length] = FWL_END_OF_STRING;

    /* Search the Rule Node in the Rule List. The if the Node is found
     * then update the filter set Value. 
     */
    pRuleNode = FwlDbaseSearchRule (au1RuleName);

    FWL_MEMCPY (au1FilterSet, pSetValFwlRuleFilterSet->pu1_OctetList,
                MEM_MAX_BYTES (pSetValFwlRuleFilterSet->i4_Length,
                               (INT4) sizeof (au1FilterSet)));
    au1FilterSet[pSetValFwlRuleFilterSet->i4_Length] = FWL_END_OF_STRING;

    if (pRuleNode != NULL)
    {
        if (pSetValFwlRuleFilterSet->i4_Length < FWL_MAX_FILTER_SET_LEN)
        {
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\nMaximum Filter Set Length is %d\n",
                          FWL_MAX_FILTER_SET_LEN);
        }
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Rule %s doesnt exists\n", au1RuleName);
        return i1Status;
    }

    i1Status = FwlParseFilterSet (au1FilterSet, pRuleNode);
    if (i1Status == SNMP_FAILURE)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\nFailed while Parsing the Filter Set\n");
    }
    else
    {
        i1Status = FwlAddFilterSet (au1FilterSet, pRuleNode);
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Rule Filter Set SET Exit \n");
    if (i1Status == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlRuleFilterSet, u4SeqNum, FALSE,
                              FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFwlRuleRuleName,
                          pSetValFwlRuleFilterSet));
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFwlRuleRowStatus
 Input       :  The Indices
                FwlRuleRuleName

                The Object 
                setValFwlRuleRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlRuleRowStatus (tSNMP_OCTET_STRING_TYPE * pFwlRuleRuleName,
                        INT4 i4SetValFwlRuleRowStatus)
{

    INT1                i1Status = SNMP_FAILURE;
    INT4                i4Status = FWL_FAILURE;
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tRuleInfo          *pRuleNode = NULL;
    UINT1               au1RuleName[FWL_MAX_RULE_NAME_LEN + FWL_ONE] =
        { FWL_ZERO };

    pRuleNode = (tRuleInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Rule Row Status SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    FWL_MEMCPY (au1RuleName, pFwlRuleRuleName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlRuleRuleName->i4_Length,
                               (INT4) sizeof (au1RuleName)));
    au1RuleName[pFwlRuleRuleName->i4_Length] = FWL_END_OF_STRING;

    /* Search The rule node in the Rule List */
    pRuleNode = FwlDbaseSearchRule (au1RuleName);

    if ((NULL != pRuleNode) &&
        (pRuleNode->u1RowStatus == i4SetValFwlRuleRowStatus))
    {
        return SNMP_SUCCESS;
    }

    /* The Rule Node can be created only when the row status is given
     * CREATE_AND_WAIT. The node will become ACTIVE when the appropriate fields
     * are SET.
     */
    switch (i4SetValFwlRuleRowStatus)
    {
        case FWL_CREATE_AND_WAIT:
            if (pRuleNode != NULL)
            {
                i1Status = SNMP_SUCCESS;
                break;
            }
            /* rule node allocation and initialisation */
            if (FwlRuleMemAllocate ((UINT1 **) (VOID *) &pRuleNode) ==
                FWL_SUCCESS)
            {
                FwlSetDefaultRuleValue (au1RuleName, pRuleNode);

                /* CHANGE3 : A Rule is added at the end of the  Rule
                 * list(when created). SNMP GETNEXT requires to display the list
                 * in lexiographical order. In order to add to the list in  
                 * sorted order the following function is added.  
                 */
                /* add the rule node to the rule list */
                FwlDbaseAddRule (pRuleNode);
                i1Status = SNMP_SUCCESS;

                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n Rule %s is created\n", au1RuleName);
            }
            else
            {
                /*  send a trap for memory failure */
                FwlGenerateMemFailureTrap (FWL_STATIC_RULE_ALLOC_FAILURE);
                CLI_SET_ERR (CLI_FWL_ACL_TABLE_FULL);
                INC_MEM_FAILURE_COUNT;
                gFwlAclInfo.i4MemStatus = (INT4) MEM_FAILURE;
            }
            break;
        case FWL_DESTROY:
            /* If the row status is DESTROY, then delete the Rule Node. */
            i4Status = (INT4) FwlDbaseDeleteRule (au1RuleName);
            if (i4Status == FWL_SUCCESS)
            {
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n Rule %s is deleted\n", au1RuleName);
                i1Status = SNMP_SUCCESS;

            }
            break;
        case FWL_ACTIVE:
            if (pRuleNode != NULL)
            {
                pRuleNode->u1RowStatus = FWL_ACTIVE;
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n RowStatus is made Active for Rule %s ",
                              au1RuleName);
                i1Status = SNMP_SUCCESS;
            }
            break;
            /* 
             * If on the fly modification of filters is needed then the 
             * following RowStatus Values is not required.
             */
        case FWL_NOT_IN_SERVICE:
            if (pRuleNode != NULL)
            {
                pRuleNode->u1RowStatus = FWL_NOT_IN_SERVICE;
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n RowStatus is made Not_In_Servicefor Rule %s ",
                              au1RuleName);
                i1Status = SNMP_SUCCESS;
            }
            break;
        default:
            break;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Rule Row Status SET Exit \n");
    if (i1Status == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlRuleRowStatus, u4SeqNum, TRUE,
                              FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFwlRuleRuleName,
                          i4SetValFwlRuleRowStatus));
    }
    return i1Status;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlRuleFilterSet
 Input       :  The Indices
                FwlRuleRuleName

                The Object 
                testValFwlRuleFilterSet
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlRuleFilterSet (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pFwlRuleRuleName,
                           tSNMP_OCTET_STRING_TYPE * pTestValFwlRuleFilterSet)
{
    INT1                i1Status = SNMP_FAILURE;
    tRuleInfo          *pRuleNode = NULL;
    UINT1               au1RuleName[FWL_MAX_RULE_NAME_LEN + FWL_ONE] =
        { FWL_ZERO };

    pRuleNode = (tRuleInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nRule Filter Set TEST Entry\n");
#ifdef SNMP_2_WANTED
    if (OSIX_FAILURE ==
        SNMPCheckForNVTChars (pTestValFwlRuleFilterSet->pu1_OctetList,
                              pTestValFwlRuleFilterSet->i4_Length))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
    if ((pTestValFwlRuleFilterSet->i4_Length >= FWL_MAX_FILTER_SET_LEN)
        || (pTestValFwlRuleFilterSet->i4_Length <= FWL_ZERO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    FWL_MEMCPY (au1RuleName, pFwlRuleRuleName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlRuleRuleName->i4_Length,
                               (INT4) sizeof (au1RuleName)));
    au1RuleName[pFwlRuleRuleName->i4_Length] = FWL_END_OF_STRING;

    /* Validate the Filter set based on Row status and the length of FilterSet.
     * If the RowStatus is FWL_NOT_IN_SERVICE and the Filter Set Length is less
     * than FWL_MAX_FILTER_NAME_LEN and the FilterSet is not already set then 
     * return SUCCESS, else FAILURE.
     */
    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\nTrying to Set FilterSet Value for Rule %s\n", au1RuleName);
    pRuleNode = FwlDbaseSearchRule (au1RuleName);
    if (pRuleNode != NULL)
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Rule %s doesnt exists\n", au1RuleName);
    }

    FWL_DBG (FWL_DBG_EXIT, "\nRule Filter Set TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlRuleRowStatus
 Input       :  The Indices
                FwlRuleRuleName

                The Object 
                testValFwlRuleRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlRuleRowStatus (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pFwlRuleRuleName,
                           INT4 i4TestValFwlRuleRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tRuleInfo          *pRuleNode = NULL;
    UINT1               au1RuleName[FWL_MAX_RULE_NAME_LEN] = { FWL_ZERO };
    tIfaceInfo         *pIfaceNode = NULL;
    tAclInfo           *pAclNode = NULL;
    tTMO_SLL           *pAclList = NULL;

    pRuleNode = (tRuleInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nRule Table Row Status TEST Entry\n");

    if ((pFwlRuleRuleName->i4_Length <= FWL_MAX_RULE_NAME_LEN - FWL_ONE)
        && (pFwlRuleRuleName->i4_Length > FWL_ZERO))
    {
        FWL_MEMCPY (au1RuleName, pFwlRuleRuleName->pu1_OctetList,
                    MEM_MAX_BYTES (pFwlRuleRuleName->i4_Length,
                                   (INT4) sizeof (au1RuleName)));
        au1RuleName[pFwlRuleRuleName->i4_Length] = FWL_END_OF_STRING;

        MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\nTrying to Set RowStatus %d Value for Rule %s\n",
                      i4TestValFwlRuleRowStatus, au1RuleName);
        /* Validate the Row status value. Rule name length should be less than
         * FWL_MAX_RULE_NAME_LEN.
         */
        pRuleNode = FwlDbaseSearchRule (au1RuleName);
        if ((NULL != pRuleNode) &&
            (pRuleNode->u1RowStatus == i4TestValFwlRuleRowStatus))
        {
            return SNMP_SUCCESS;
        }
        switch (i4TestValFwlRuleRowStatus)
        {
            case FWL_CREATE_AND_WAIT:
                if (pRuleNode == NULL)
                {
                    i1Status = SNMP_SUCCESS;
                }
                else
                {
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\n Rule %s already exists\n", au1RuleName);
                    i1Status = SNMP_FAILURE;
                }
                break;
            case FWL_ACTIVE:
                if (pRuleNode != NULL)
                {
                    if ((pRuleNode->u1RowStatus == FWL_NOT_IN_SERVICE) &&
                        (pRuleNode->apFilterInfo[FWL_INDEX_0] != NULL))
                    {
                        i1Status = SNMP_SUCCESS;
                    }
                    else
                    {
                        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                 "\n Incomplete Rule. "
                                 "RowStatus cannot be ACTIVE\n");
                    }
                }
                else
                {
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\n Rule %s doesnt exists\n", au1RuleName);
                }
                break;
            case FWL_DESTROY:
                /* Check whether the corresponding Rule is associated
                 * with any other ACL. If not, then delete the RULE */
                pIfaceNode = gFwlAclInfo.apIfaceList[FWL_GLOBAL_IDX];

                if (pIfaceNode != NULL)
                {
                    pAclList = &pIfaceNode->inFilterList;
                    TMO_SLL_Scan (pAclList, pAclNode, tAclInfo *)
                    {
                        if ((STRLEN (pAclNode->au1FilterName) ==
                             (UINT4) (pFwlRuleRuleName->i4_Length)) &&
                            (STRCMP (pAclNode->au1FilterName,
                                     pFwlRuleRuleName->pu1_OctetList)) ==
                            FWL_ZERO)
                        {
                            return (SNMP_FAILURE);
                        }
                    }

                    pAclList = &pIfaceNode->outFilterList;
                    TMO_SLL_Scan (pAclList, pAclNode, tAclInfo *)
                    {
                        if ((STRLEN (pAclNode->au1FilterName) ==
                             (UINT4) (pFwlRuleRuleName->i4_Length)) &&
                            (STRCMP (pAclNode->au1FilterName,
                                     pFwlRuleRuleName->pu1_OctetList)) ==
                            FWL_ZERO)
                        {
                            return (SNMP_FAILURE);
                        }
                    }
                    pAclList = &pIfaceNode->inIPv6FilterList;
                    TMO_SLL_Scan (pAclList, pAclNode, tAclInfo *)
                    {
                        if ((STRLEN (pAclNode->au1FilterName) ==
                             (UINT4) (pFwlRuleRuleName->i4_Length)) &&
                            (STRCMP (pAclNode->au1FilterName,
                                     pFwlRuleRuleName->pu1_OctetList)) ==
                            FWL_ZERO)
                        {
                            return (SNMP_FAILURE);
                        }
                    }

                    pAclList = &pIfaceNode->outIPv6FilterList;
                    TMO_SLL_Scan (pAclList, pAclNode, tAclInfo *)
                    {
                        if ((STRLEN (pAclNode->au1FilterName) ==
                             (UINT4) (pFwlRuleRuleName->i4_Length)) &&
                            (STRCMP (pAclNode->au1FilterName,
                                     pFwlRuleRuleName->pu1_OctetList)) ==
                            FWL_ZERO)
                        {
                            return (SNMP_FAILURE);
                        }
                    }

                }
                i1Status = SNMP_SUCCESS;
                break;
                /* 
                 * If on the fly modification of filters is needed then the 
                 * following RowStatus Values is not required.
                 */
            case FWL_NOT_IN_SERVICE:
                /* The row can be made FWL_NOT_IN_SERVICE if the row exists */
                if (pRuleNode != NULL)
                {
                    if (pRuleNode->u1RowStatus == FWL_ACTIVE)
                    {
                        i1Status = SNMP_SUCCESS;
                    }
                }
                else
                {
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\n Rule %s doesnt exists or "
                                  "it is already in Not_in_Service\n",
                                  au1RuleName);
                }
                break;
            default:
                i1Status = SNMP_FAILURE;
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\n Row Status Value ranges from 1 to 6\n");
                break;
        }
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\nMaximum Rule Name Length is %d\n",
                      FWL_MAX_RULE_NAME_LEN - FWL_ONE);
    }
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\nRule RowStatus TEST Exit\n");

    return i1Status;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlDefnRuleTable
 Input       :  The Indices
                FwlRuleRuleName                                                                      Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDefnRuleTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FwlDefnAclTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlDefnAclTable
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFwlDefnAclTable (INT4 i4FwlAclIfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFwlAclAclName, INT4 i4FwlAclDirection)
{
    UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
    INT4                i4Direction = i4FwlAclDirection;
    UINT4               u4IfaceNum = (UINT4) i4FwlAclIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;
    tInFilterInfo      *pInFilterNode = NULL;
    tOutFilterInfo     *pOutFilterNode = NULL;
    INT1                i1Status = SNMP_FAILURE;

    pOutFilterNode = (tOutFilterInfo *) NULL;
    pInFilterNode = (tInFilterInfo *) NULL;
    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Validate Index for Acl Table - Entry\n");

    if (pFwlAclAclName->i4_Length < FWL_MAX_ACL_NAME_LEN)
    {

        FWL_MEMCPY (au1AclName, pFwlAclAclName->pu1_OctetList,
                    MEM_MAX_BYTES (pFwlAclAclName->i4_Length,
                                   (INT4) sizeof (au1AclName)));
        au1AclName[pFwlAclAclName->i4_Length] = FWL_END_OF_STRING;

        /*  Search for the Interface Number in the interface list */
        if (u4IfaceNum < FWL_MAX_NUM_OF_IF)
        {
            pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
            if (pIfaceNode != NULL)
            {
                if (i4Direction == FWL_DIRECTION_IN)
                {
                    /* search for the filter name in the Infilter List if the 
                       direction is IN */
                    pInFilterNode =
                        FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                                 au1AclName);
                    if (pInFilterNode == NULL)
                    {
                        /* No entry in IPv4 filterList. Check in
                         * IPv6 filterlist. */
                        pInFilterNode =
                            FwlDbaseSearchAclFilter
                            (&pIfaceNode->inIPv6FilterList, au1AclName);
                    }

                    if (pInFilterNode != NULL)
                    {
                        /* Found a valid entry in either IPv4 or IPv6 
                         * filterlist. Hence return succexx*/
                        i1Status = SNMP_SUCCESS;
                    }
                    else
                    {
                        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                      "\nFilter / Rule  %s doesnt exist \n",
                                      au1AclName);
                    }
                }
                else if (i4Direction == FWL_DIRECTION_OUT)
                {
                    /*  search for the filter name in the Out Filter list if the
                       direction is OUT */
                    pOutFilterNode =
                        FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                                 au1AclName);
                    if (pOutFilterNode == NULL)
                    {
                        /* No entry in IPv4 filterList. Check in 
                         * IPv6 filter list*/
                        pOutFilterNode =
                            FwlDbaseSearchAclFilter
                            (&pIfaceNode->outIPv6FilterList, au1AclName);
                    }
                    if (pOutFilterNode != NULL)
                    {
                        /*Found a valid entry in either IPv4 or IPv6 
                         * filterlist. Hence return success*/
                        i1Status = SNMP_SUCCESS;
                    }
                    else
                    {
                        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                      "\nFilter / Rule  %s doesnt exist \n",
                                      au1AclName);
                    }
                }
                else
                {
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\nDirection %d doesnt exist \n",
                                  i4Direction);
                }
            }
            else
            {
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\nInterface %d doesnt exist \n", u4IfaceNum);
            }
        }
        else
        {
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\nInvalid Interface number. The range is 1 to %d\n",
                          FWL_MAX_NUM_OF_IF - FWL_ONE);
        }

    }

    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Maximum Acl Name length is %d\n",
                      FWL_MAX_ACL_NAME_LEN - FWL_ONE);
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Validate Index for Acl Table - Exit\n");
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFwlDefnAclTable
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFwlDefnAclTable (INT4 *pi4FwlAclIfIndex,
                                 tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                                 INT4 *pi4FwlAclDirection)
{

    INT1                i1Status = FWL_ZERO;
    tSNMP_OCTET_STRING_TYPE FwlAclAclName;

    FwlAclAclName.i4_Length = FWL_ZERO;

    i1Status = nmhGetNextIndexFwlDefnAclTable (FWL_ZERO, pi4FwlAclIfIndex,
                                               &FwlAclAclName, pFwlAclAclName,
                                               FWL_ZERO, pi4FwlAclDirection);
    return (i1Status);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFwlDefnAclTable
 Input       :  The Indices
                FwlAclIfIndex
                nextFwlAclIfIndex
                FwlAclAclName
                nextFwlAclAclName
                FwlAclDirection
                nextFwlAclDirection
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFwlDefnAclTable (INT4 i4FwlAclIfIndex,
                                INT4 *pi4NextFwlAclIfIndex,
                                tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                                tSNMP_OCTET_STRING_TYPE * pNextFwlAclAclName,
                                INT4 i4FwlAclDirection,
                                INT4 *pi4NextFwlAclDirection)
{
    tIfaceInfo         *pIfaceNode = NULL;
    tAclInfo           *pTmpAclNode = NULL;
    tAclInfo           *pInAclNode = NULL;
    tAclInfo           *pOutAclNode = NULL;
    tAclInfo           *pInIpv6AclNode = NULL;
    tAclInfo           *pOutIpv6AclNode = NULL;
    UINT1               au1Tmpv4NextFwlAclName[FWL_MAX_ACL_NAME_LEN] =
        { FWL_ZERO };
    UINT1               au1Tmpv6NextFwlAclName[FWL_MAX_ACL_NAME_LEN] =
        { FWL_ZERO };
    UINT4               u4IfaceNum = (UINT4) i4FwlAclIfIndex;
    UINT4               u4Isv4AclPres = FALSE;
    UINT4               u4Isv6AclPres = FALSE;
    INT4                i4TmpNameLength = FWL_ZERO;
    INT4                i4InNameLength = FWL_ZERO;
    INT4                i4OutNameLength = FWL_ZERO;
    INT4                i4Inv6NameLength = FWL_ZERO;
    INT4                i4Outv6NameLength = FWL_ZERO;
    INT4                i4Tmpv4NextFwlAclIfIndex = FWL_ZERO;
    INT4                i4Tmpv6NextFwlAclIfIndex = FWL_ZERO;
    INT4                i4Tmpv4NextFwlAclDirection = FWL_ZERO;
    INT4                i4Tmpv6NextFwlAclDirection = FWL_ZERO;
    INT4                i4Tmpv4NameLength = FWL_ZERO;
    INT4                i4Tmpv6NameLength = FWL_ZERO;

    MEMSET (au1Tmpv4NextFwlAclName, FWL_ZERO, FWL_MAX_ACL_NAME_LEN);
    MEMSET (au1Tmpv6NextFwlAclName, FWL_ZERO, FWL_MAX_ACL_NAME_LEN);

    FWL_DBG (FWL_DBG_ENTRY, "\n Get Next Index for Acl Table - Entry\n");

    /* The following logic is implemented for the lexicographic ordering of the
     * return values for an SNMP walk. The nodes should be returned in the 
     * lexicographic order of their index/index-set. 
     * As the ACL table has 3 indices, all of them are to be checked one by one.
     * 
     * The logic first traverses both the lists associated with the interface- 
     * node and gets node with the next higher index within those lists. 
     * Then it compares the nodes obtained from both the lists and finds out 
     * which among the two is the next higher index set value than the node 
     * passed to this function and that is returned.
     */
    while (u4IfaceNum < FWL_MAX_NUM_OF_IF)
    {
        if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
        {
            u4IfaceNum++;
            continue;
        }

        pTmpAclNode = pInAclNode = pOutAclNode = NULL;
        i4InNameLength = i4OutNameLength = FWL_ZERO;

        pInIpv6AclNode = pOutIpv6AclNode = NULL;
        i4Inv6NameLength = i4Outv6NameLength = FWL_ZERO;
        /* Search the inFilterset for the next higher AclName value */
        TMO_SLL_Scan ((&(pIfaceNode->inFilterList)), pTmpAclNode, tAclInfo *)
        {
            i4TmpNameLength = (INT4) STRLEN (pTmpAclNode->au1FilterName);
            if (i4TmpNameLength >= pFwlAclAclName->i4_Length)
            {
                if ((i4TmpNameLength == pFwlAclAclName->i4_Length)
                    && (MEMCMP (pTmpAclNode->au1FilterName,
                                pFwlAclAclName->pu1_OctetList,
                                (size_t) pFwlAclAclName->i4_Length) <=
                        FWL_ZERO))
                {
                    continue;
                }

                /* Got a node that is greater than the given node
                 * Now check if this is the next to the given node
                 */

                if (pInAclNode == NULL)
                {
                    pInAclNode = pTmpAclNode;
                    i4InNameLength = i4TmpNameLength;
                }
                else if (i4TmpNameLength <= i4InNameLength)
                {
                    if ((i4TmpNameLength == i4InNameLength)
                        && (MEMCMP (pTmpAclNode->au1FilterName,
                                    pInAclNode->au1FilterName,
                                    (size_t) i4InNameLength) > FWL_ZERO))
                    {
                        continue;
                    }
                    pInAclNode = pTmpAclNode;
                    i4InNameLength = i4TmpNameLength;
                }
            }
        }

        /* Search the inIpv6Filterset for the next higher AclName value */
        TMO_SLL_Scan ((&(pIfaceNode->inIPv6FilterList)),
                      pTmpAclNode, tAclInfo *)
        {
            i4TmpNameLength = (INT4) STRLEN (pTmpAclNode->au1FilterName);
            if (i4TmpNameLength >= pFwlAclAclName->i4_Length)
            {
                if ((i4TmpNameLength == pFwlAclAclName->i4_Length)
                    && (MEMCMP (pTmpAclNode->au1FilterName,
                                pFwlAclAclName->pu1_OctetList,
                                (size_t) pFwlAclAclName->i4_Length) <=
                        FWL_ZERO))
                {
                    continue;
                }

                /* Got a node that is greater than the given node
                 * Now check if this is the next to the given node
                 */

                if (pInIpv6AclNode == NULL)
                {
                    pInIpv6AclNode = pTmpAclNode;
                    i4Inv6NameLength = i4TmpNameLength;
                }
                else if (i4TmpNameLength <= i4Inv6NameLength)
                {
                    if ((i4TmpNameLength == i4Inv6NameLength)
                        && (MEMCMP (pTmpAclNode->au1FilterName,
                                    pInIpv6AclNode->au1FilterName,
                                    (size_t) i4Inv6NameLength) > FWL_ZERO))
                    {
                        continue;
                    }
                    pInIpv6AclNode = pTmpAclNode;
                    i4Inv6NameLength = i4TmpNameLength;
                }
            }
        }

        /* Search the outFilterset for the next higher AclName value */
        TMO_SLL_Scan ((&(pIfaceNode->outFilterList)), pTmpAclNode, tAclInfo *)
        {
            i4TmpNameLength = (INT4) STRLEN (pTmpAclNode->au1FilterName);
            if (i4TmpNameLength >= pFwlAclAclName->i4_Length)
            {
                if ((i4TmpNameLength == pFwlAclAclName->i4_Length)
                    && (MEMCMP (pTmpAclNode->au1FilterName,
                                pFwlAclAclName->pu1_OctetList,
                                (size_t) pFwlAclAclName->i4_Length) <=
                        FWL_ZERO))
                {
                    continue;
                }

                /* Got a node that is greater than the given node
                 * Now check if this is the next to the given node
                 */
                if (pOutAclNode == NULL)
                {
                    pOutAclNode = pTmpAclNode;
                    i4OutNameLength = i4TmpNameLength;
                }
                else if (i4TmpNameLength <= i4OutNameLength)
                {
                    if ((i4TmpNameLength == i4OutNameLength)
                        && (MEMCMP (pTmpAclNode->au1FilterName,
                                    pOutAclNode->au1FilterName,
                                    (size_t) i4OutNameLength) > FWL_ZERO))
                    {
                        continue;
                    }
                    pOutAclNode = pTmpAclNode;
                    i4OutNameLength = i4TmpNameLength;
                }
            }
            else if ((i4TmpNameLength == pFwlAclAclName->i4_Length) && (INT4)
                     (MEMCMP (pTmpAclNode->au1FilterName,
                              pFwlAclAclName->pu1_OctetList,
                              (size_t) pFwlAclAclName->i4_Length) == FWL_ZERO)
                     && i4FwlAclDirection == FWL_DIRECTION_IN)
            {
                /* Got a node with the same ACL name but with a
                 * different direction */
                pOutAclNode = pTmpAclNode;
                break;
            }
        }

        /* Search the outIpv6Filterset for the next higher AclName value */
        TMO_SLL_Scan ((&(pIfaceNode->outIPv6FilterList)),
                      pTmpAclNode, tAclInfo *)
        {
            i4TmpNameLength = (INT4) STRLEN (pTmpAclNode->au1FilterName);
            if (i4TmpNameLength >= pFwlAclAclName->i4_Length)
            {
                if ((i4TmpNameLength == pFwlAclAclName->i4_Length)
                    && (MEMCMP (pTmpAclNode->au1FilterName,
                                pFwlAclAclName->pu1_OctetList,
                                (size_t) pFwlAclAclName->i4_Length) <=
                        FWL_ZERO))
                {
                    continue;
                }

                /* Got a node that is greater than the given node
                 * Now check if this is the next to the given node
                 */
                if (pOutIpv6AclNode == NULL)
                {
                    pOutIpv6AclNode = pTmpAclNode;
                    i4Outv6NameLength = i4TmpNameLength;
                }
                else if (i4TmpNameLength <= i4Outv6NameLength)
                {
                    if ((i4TmpNameLength == i4Outv6NameLength)
                        && (MEMCMP (pTmpAclNode->au1FilterName,
                                    pOutIpv6AclNode->au1FilterName,
                                    (size_t) i4Outv6NameLength) > FWL_ZERO))
                    {
                        continue;
                    }
                    pOutIpv6AclNode = pTmpAclNode;
                    i4Outv6NameLength = i4TmpNameLength;
                }
            }
            else if ((i4TmpNameLength == pFwlAclAclName->i4_Length) &&
                     (MEMCMP (pTmpAclNode->au1FilterName,
                              pFwlAclAclName->pu1_OctetList,
                              (size_t) pFwlAclAclName->i4_Length) == FWL_ZERO)
                     && i4FwlAclDirection == FWL_DIRECTION_IN)
            {
                /* Got a node with the same ACL name but with a
                 * different direction */
                pOutIpv6AclNode = pTmpAclNode;
                break;
            }
        }

        /* Compare the nodes obtained from the Infilter and OutFilter lists */
        if ((pOutAclNode != NULL) && (pInAclNode != NULL))
        {
            if ((i4OutNameLength < i4InNameLength) ||
                ((i4OutNameLength == i4InNameLength) &&
                 (MEMCMP (pOutAclNode->au1FilterName,
                          pInAclNode->au1FilterName,
                          (size_t) i4OutNameLength) <= FWL_ZERO)))
            {
                i4Tmpv4NextFwlAclIfIndex = (INT4) u4IfaceNum;
                MEMCPY (au1Tmpv4NextFwlAclName,
                        pOutAclNode->au1FilterName, i4OutNameLength);
                i4Tmpv4NameLength = i4OutNameLength;
                i4Tmpv4NextFwlAclDirection = FWL_DIRECTION_OUT;
            }
            else
            {
                i4Tmpv4NextFwlAclIfIndex = (INT4) u4IfaceNum;
                MEMCPY (au1Tmpv4NextFwlAclName,
                        pInAclNode->au1FilterName, i4InNameLength);
                i4Tmpv4NameLength = i4InNameLength;
                i4Tmpv4NextFwlAclDirection = FWL_DIRECTION_IN;
            }
            u4Isv4AclPres = TRUE;
        }
        else if (pInAclNode != NULL)
        {
            i4Tmpv4NextFwlAclIfIndex = (INT4) u4IfaceNum;
            MEMCPY (au1Tmpv4NextFwlAclName,
                    pInAclNode->au1FilterName, i4InNameLength);
            i4Tmpv4NameLength = i4InNameLength;
            i4Tmpv4NextFwlAclDirection = FWL_DIRECTION_IN;
            u4Isv4AclPres = TRUE;
        }
        else if (pOutAclNode != NULL)
        {
            i4Tmpv4NextFwlAclIfIndex = (INT4) u4IfaceNum;
            MEMCPY (au1Tmpv4NextFwlAclName,
                    pOutAclNode->au1FilterName, i4OutNameLength);
            i4Tmpv4NameLength = i4OutNameLength;
            i4Tmpv4NextFwlAclDirection = FWL_DIRECTION_OUT;
            u4Isv4AclPres = TRUE;
        }

        /* Compare the nodes obtained from the InIpv6filter 
         * and OutIPv6Filter lists */
        if ((pOutIpv6AclNode != NULL) && (pInIpv6AclNode != NULL))
        {
            if ((i4Outv6NameLength < i4Inv6NameLength) ||
                ((i4Outv6NameLength == i4Inv6NameLength)
                 &&
                 (MEMCMP
                  (pOutIpv6AclNode->au1FilterName,
                   pInIpv6AclNode->au1FilterName,
                   (size_t) i4Outv6NameLength) <= FWL_ZERO)))
            {
                i4Tmpv6NextFwlAclIfIndex = (INT4) u4IfaceNum;
                MEMCPY (au1Tmpv6NextFwlAclName,
                        pOutIpv6AclNode->au1FilterName, i4Outv6NameLength);
                i4Tmpv6NameLength = i4Outv6NameLength;

                i4Tmpv6NextFwlAclDirection = FWL_DIRECTION_OUT;
            }
            else
            {
                i4Tmpv6NextFwlAclIfIndex = (INT4) u4IfaceNum;
                MEMCPY (au1Tmpv6NextFwlAclName,
                        pInIpv6AclNode->au1FilterName, i4Inv6NameLength);
                i4Tmpv6NameLength = i4Inv6NameLength;
                i4Tmpv6NextFwlAclDirection = FWL_DIRECTION_IN;

            }
            u4Isv6AclPres = TRUE;
        }

        else if (pInIpv6AclNode != NULL)
        {
            i4Tmpv6NextFwlAclIfIndex = (INT4) u4IfaceNum;
            MEMCPY (au1Tmpv6NextFwlAclName,
                    pInIpv6AclNode->au1FilterName, i4Inv6NameLength);
            i4Tmpv6NameLength = i4Inv6NameLength;

            i4Tmpv6NextFwlAclDirection = FWL_DIRECTION_IN;
            u4Isv6AclPres = TRUE;
        }

        else if (pOutIpv6AclNode != NULL)
        {
            i4Tmpv6NextFwlAclIfIndex = (INT4) u4IfaceNum;
            MEMCPY (au1Tmpv6NextFwlAclName,
                    pOutIpv6AclNode->au1FilterName, i4Outv6NameLength);
            i4Tmpv6NameLength = i4Outv6NameLength;
            i4Tmpv6NextFwlAclDirection = FWL_DIRECTION_OUT;

            u4Isv6AclPres = TRUE;
        }
        /* Compare the nodes obtained from the IPv4filter 
         * and IPv6Filter lists */
        if ((u4Isv6AclPres == TRUE) && (u4Isv4AclPres == TRUE))
        {
            if ((i4Tmpv6NameLength <
                 i4Tmpv4NameLength) ||
                ((i4Tmpv6NameLength == i4Tmpv4NameLength) &&
                 (MEMCMP
                  (au1Tmpv6NextFwlAclName, au1Tmpv4NextFwlAclName,
                   (size_t) i4Tmpv6NameLength) <= FWL_ZERO)))
            {
                *pi4NextFwlAclIfIndex = (INT4) u4IfaceNum;
                MEMCPY (pNextFwlAclAclName->pu1_OctetList,
                        au1Tmpv6NextFwlAclName, i4Tmpv6NameLength);
                pNextFwlAclAclName->i4_Length = i4Tmpv6NameLength;
                pNextFwlAclAclName->pu1_OctetList
                    [pNextFwlAclAclName->i4_Length] = FWL_END_OF_STRING;
                if (i4Tmpv6NextFwlAclDirection == FWL_DIRECTION_OUT)
                {
                    *pi4NextFwlAclDirection = FWL_DIRECTION_OUT;
                }
                else if (i4Tmpv6NextFwlAclDirection == FWL_DIRECTION_IN)
                {
                    *pi4NextFwlAclDirection = FWL_DIRECTION_IN;
                }
                return SNMP_SUCCESS;
            }
            else
            {
                *pi4NextFwlAclIfIndex = (INT4) u4IfaceNum;
                MEMCPY (pNextFwlAclAclName->pu1_OctetList,
                        au1Tmpv4NextFwlAclName, i4Tmpv4NameLength);
                pNextFwlAclAclName->i4_Length = i4Tmpv4NameLength;
                if (i4Tmpv4NextFwlAclDirection == FWL_DIRECTION_IN)
                {
                    *pi4NextFwlAclDirection = FWL_DIRECTION_IN;
                }
                else if (i4Tmpv4NextFwlAclDirection == FWL_DIRECTION_OUT)
                {
                    *pi4NextFwlAclDirection = FWL_DIRECTION_OUT;
                }
                pNextFwlAclAclName->pu1_OctetList
                    [pNextFwlAclAclName->i4_Length] = FWL_END_OF_STRING;
                return SNMP_SUCCESS;
            }
        }
        else if (u4Isv4AclPres == TRUE)
        {
            *pi4NextFwlAclIfIndex = (INT4) u4IfaceNum;
            MEMCPY (pNextFwlAclAclName->pu1_OctetList,
                    au1Tmpv4NextFwlAclName, i4Tmpv4NameLength);
            pNextFwlAclAclName->i4_Length = i4Tmpv4NameLength;
            if (i4Tmpv4NextFwlAclDirection == FWL_DIRECTION_IN)
            {
                *pi4NextFwlAclDirection = FWL_DIRECTION_IN;
            }
            else if (i4Tmpv4NextFwlAclDirection == FWL_DIRECTION_OUT)
            {
                *pi4NextFwlAclDirection = FWL_DIRECTION_OUT;
            }
            pNextFwlAclAclName->pu1_OctetList
                [pNextFwlAclAclName->i4_Length] = FWL_END_OF_STRING;
            return SNMP_SUCCESS;
        }
        else if (u4Isv6AclPres == TRUE)
        {
            *pi4NextFwlAclIfIndex = (INT4) u4IfaceNum;
            MEMCPY (pNextFwlAclAclName->pu1_OctetList,
                    au1Tmpv6NextFwlAclName, i4Tmpv6NameLength);
            pNextFwlAclAclName->i4_Length = i4Tmpv6NameLength;
            pNextFwlAclAclName->pu1_OctetList
                [pNextFwlAclAclName->i4_Length] = FWL_END_OF_STRING;
            if (i4Tmpv6NextFwlAclDirection == FWL_DIRECTION_OUT)
            {
                *pi4NextFwlAclDirection = FWL_DIRECTION_OUT;
            }
            else if (i4Tmpv6NextFwlAclDirection == FWL_DIRECTION_IN)
            {
                *pi4NextFwlAclDirection = FWL_DIRECTION_IN;
            }
            return SNMP_SUCCESS;
        }

        u4IfaceNum++;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Get Next Index for Acl Table Failed - Exit\n");
    UNUSED_PARAM (i4Tmpv6NextFwlAclIfIndex);
    UNUSED_PARAM (i4Tmpv4NextFwlAclIfIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlAclAction
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                retValFwlAclAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlAclAction (INT4 i4FwlAclIfIndex,
                    tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                    INT4 i4FwlAclDirection, INT4 *pi4RetValFwlAclAction)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
    tIfaceInfo         *pIfaceNode = NULL;
    tInFilterInfo      *pInFilterNode = NULL;
    tOutFilterInfo     *pOutFilterNode = NULL;
    tInIpv6FilterInfo  *pInIpv6FilterNode = NULL;
    tOutIpv6FilterInfo *pOutIpv6FilterNode = NULL;

    UINT4               u4IfaceNum = (UINT4) i4FwlAclIfIndex;

    pIfaceNode = (tIfaceInfo *) NULL;
    pOutFilterNode = (tOutFilterInfo *) NULL;
    pInFilterNode = (tInFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nAction GET Entry\n");

    FWL_MEMCPY (au1AclName, pFwlAclAclName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlAclAclName->i4_Length,
                               (INT4) sizeof (au1AclName)));
    au1AclName[pFwlAclAclName->i4_Length] = FWL_END_OF_STRING;

    /* search the interface index */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return i1Status;
    }
    if (i4FwlAclDirection == FWL_DIRECTION_IN)
    {
        /* get the infilter node for the particular filter name */
        pInFilterNode = FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                                 au1AclName);
        if (pInFilterNode == NULL)
        {
            /* InfilterNode not found in ipv4filter list. Secrch in 
             * IPv6filterlist. If found, update AclAction*/
            pInIpv6FilterNode = FwlDbaseSearchAclFilter
                (&pIfaceNode->inIPv6FilterList, au1AclName);
            if (pInIpv6FilterNode == NULL)
            {
                return i1Status;
            }
            else if (pInIpv6FilterNode != NULL)
            {
                *pi4RetValFwlAclAction = (INT4) pInIpv6FilterNode->u1Action;
                i1Status = SNMP_SUCCESS;
                return i1Status;
            }
        }
        /* get the action value from the node */
        *pi4RetValFwlAclAction = (INT4) pInFilterNode->u1Action;
        i1Status = SNMP_SUCCESS;
    }
    else if (i4FwlAclDirection == FWL_DIRECTION_OUT)
    {
        /*  get the outfilter node for the particular filter name */
        pOutFilterNode = FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                                  au1AclName);
        if (pOutFilterNode == NULL)
        {
            /* OutfilterNode not found in ipv4filter list. Secrch in
             * IPv6filterlist. If found, update AclAction*/

            pOutIpv6FilterNode = FwlDbaseSearchAclFilter
                (&pIfaceNode->outIPv6FilterList, au1AclName);
            if (pOutIpv6FilterNode == NULL)
            {
                return i1Status;
            }
            else if (pOutIpv6FilterNode != NULL)
            {
                *pi4RetValFwlAclAction = (INT4) pOutIpv6FilterNode->u1Action;
                i1Status = SNMP_SUCCESS;
                return i1Status;
            }
        }
        /* get the action value from the node */
        *pi4RetValFwlAclAction = (INT4) pOutFilterNode->u1Action;
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        i1Status = SNMP_FAILURE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\nAction GET Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFwlAclSequenceNumber
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                retValFwlAclSequenceNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlAclSequenceNumber (INT4 i4FwlAclIfIndex,
                            tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                            INT4 i4FwlAclDirection,
                            INT4 *pi4RetValFwlAclSequenceNumber)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
    tIfaceInfo         *pIfaceNode = NULL;
    tInFilterInfo      *pInFilterNode = NULL;
    tOutFilterInfo     *pOutFilterNode = NULL;
    tInIpv6FilterInfo  *pInIpv6FilterNode = NULL;
    tOutIpv6FilterInfo *pOutIpv6FilterNode = NULL;

    UINT4               u4IfaceNum = (UINT4) i4FwlAclIfIndex;

    pOutFilterNode = (tOutFilterInfo *) NULL;
    pInFilterNode = (tInFilterInfo *) NULL;
    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nSequence Number GET Entry\n");

    FWL_MEMCPY (au1AclName, pFwlAclAclName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlAclAclName->i4_Length,
                               (INT4) sizeof (au1AclName)));
    au1AclName[pFwlAclAclName->i4_Length] = FWL_END_OF_STRING;

    /* Search the interface index */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return SNMP_FAILURE;
    }
    if (i4FwlAclDirection == FWL_DIRECTION_IN)
    {
        /* search the infilter list for the particular filter name */
        pInFilterNode = FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                                 au1AclName);
        if (pInFilterNode == NULL)
        {
            pInIpv6FilterNode = FwlDbaseSearchAclFilter
                (&pIfaceNode->inIPv6FilterList, au1AclName);
            if (pInIpv6FilterNode == NULL)
            {
                return i1Status;
            }
            else
            {
                *pi4RetValFwlAclSequenceNumber =
                    (INT4) pInIpv6FilterNode->u2SeqNum;
                i1Status = SNMP_SUCCESS;
                return i1Status;
            }
        }
        /* get the sequence number value */
        *pi4RetValFwlAclSequenceNumber = (INT4) pInFilterNode->u2SeqNum;
        i1Status = SNMP_SUCCESS;
    }
    else if (i4FwlAclDirection == FWL_DIRECTION_OUT)
    {
        /* search the outfilter list for the partcular filter name */
        pOutFilterNode = FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                                  au1AclName);
        if (pOutFilterNode == NULL)
        {
            pOutIpv6FilterNode = FwlDbaseSearchAclFilter
                (&pIfaceNode->outIPv6FilterList, au1AclName);
            if (pOutIpv6FilterNode == NULL)
            {
                return i1Status;
            }
            else
            {
                *pi4RetValFwlAclSequenceNumber =
                    (INT4) pOutIpv6FilterNode->u2SeqNum;
                i1Status = SNMP_SUCCESS;
                return i1Status;
            }
        }
        /* get the sequence number */
        *pi4RetValFwlAclSequenceNumber = (INT4) pOutFilterNode->u2SeqNum;
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        i1Status = SNMP_FAILURE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Sequence Number GET Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFwlAclAclType
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                retValFwlAclAclType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlAclAclType (INT4 i4FwlAclIfIndex,
                     tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                     INT4 i4FwlAclDirection, INT4 *pi4RetValFwlAclAclType)
{
    /* Deprecated Object - Default Value is returned */

    UNUSED_PARAM (i4FwlAclIfIndex);
    UNUSED_PARAM (pFwlAclAclName);
    UNUSED_PARAM (i4FwlAclDirection);

    *pi4RetValFwlAclAclType = ACL_RULE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlAclLogTrigger
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                retValFwlAclLogTrigger
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlAclLogTrigger (INT4 i4FwlAclIfIndex,
                        tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                        INT4 i4FwlAclDirection, INT4 *pi4RetValFwlAclLogTrigger)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
    tIfaceInfo         *pIfaceNode = NULL;
    tInFilterInfo      *pInFilterNode = NULL;
    tOutFilterInfo     *pOutFilterNode = NULL;
    tInIpv6FilterInfo  *pInIpv6FilterNode = NULL;
    tOutIpv6FilterInfo *pOutIpv6FilterNode = NULL;

    UINT4               u4IfaceNum = (UINT4) i4FwlAclIfIndex;

    pOutFilterNode = (tOutFilterInfo *) NULL;
    pInFilterNode = (tInFilterInfo *) NULL;
    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nAcl Type GET Entry\n");

    FWL_MEMCPY (au1AclName, pFwlAclAclName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlAclAclName->i4_Length,
                               (INT4) sizeof (au1AclName)));
    au1AclName[pFwlAclAclName->i4_Length] = FWL_END_OF_STRING;

    /* Search the interface index */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /* search the infilter list for the particular filter name */
    if (i4FwlAclDirection == FWL_DIRECTION_IN)
    {
        pInFilterNode = FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                                 au1AclName);
        if (pInFilterNode == NULL)
        {
            pInIpv6FilterNode = FwlDbaseSearchAclFilter
                (&pIfaceNode->inIPv6FilterList, au1AclName);
            if (pInIpv6FilterNode == NULL)
            {
                return i1Status;
            }
            else if (pInIpv6FilterNode != NULL)
            {
                *pi4RetValFwlAclLogTrigger =
                    (INT4) pInIpv6FilterNode->u1LogTrigger;
                i1Status = SNMP_SUCCESS;
                return i1Status;
            }
        }
        /* get the sequence number */
        *pi4RetValFwlAclLogTrigger = (INT4) pInFilterNode->u1LogTrigger;
        i1Status = SNMP_SUCCESS;
    }
    else if (i4FwlAclDirection == FWL_DIRECTION_OUT)
    {
        /* search the out filter list for the particular filter name */
        pOutFilterNode = FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                                  au1AclName);
        if (pOutFilterNode == NULL)
        {
            pOutIpv6FilterNode = FwlDbaseSearchAclFilter
                (&pIfaceNode->outIPv6FilterList, au1AclName);
            if (pOutIpv6FilterNode == NULL)
            {
                return i1Status;
            }
            else if (pOutIpv6FilterNode != NULL)
            {
                *pi4RetValFwlAclLogTrigger =
                    (INT4) pOutIpv6FilterNode->u1LogTrigger;
                i1Status = SNMP_SUCCESS;
                return i1Status;
            }
        }
        /* get the sequence number  */
        *pi4RetValFwlAclLogTrigger = (INT4) pOutFilterNode->u1LogTrigger;
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        i1Status = SNMP_FAILURE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Acl Type GET Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFwlAclFragAction
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                retValFwlAclFragAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlAclFragAction (INT4 i4FwlAclIfIndex,
                        tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                        INT4 i4FwlAclDirection, INT4 *pi4RetValFwlAclFragAction)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
    tIfaceInfo         *pIfaceNode = NULL;
    tInFilterInfo      *pInFilterNode = NULL;
    tOutFilterInfo     *pOutFilterNode = NULL;
    tInIpv6FilterInfo  *pInIpv6FilterNode = NULL;
    tOutIpv6FilterInfo *pOutIpv6FilterNode = NULL;
    UINT4               u4IfaceNum = (UINT4) i4FwlAclIfIndex;

    pOutFilterNode = (tOutFilterInfo *) NULL;
    pInFilterNode = (tInFilterInfo *) NULL;
    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nAcl Type GET Entry\n");

    FWL_MEMCPY (au1AclName, pFwlAclAclName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlAclAclName->i4_Length,
                               (INT4) sizeof (au1AclName)));
    au1AclName[pFwlAclAclName->i4_Length] = FWL_END_OF_STRING;

    /* Search the interface index */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /* search the infilter list for the particular filter name */
    if (i4FwlAclDirection == FWL_DIRECTION_IN)
    {
        pInFilterNode = FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                                 au1AclName);
        if (pInFilterNode == NULL)
        {
            pInIpv6FilterNode = FwlDbaseSearchAclFilter
                (&pIfaceNode->inIPv6FilterList, au1AclName);
            if (pInIpv6FilterNode == NULL)
            {
                return i1Status;
            }
            else if (pInIpv6FilterNode != NULL)
            {
                *pi4RetValFwlAclFragAction =
                    (INT4) pInIpv6FilterNode->u1FragAction;
                i1Status = SNMP_SUCCESS;
                return i1Status;
            }
        }
        /* get the sequence number */
        *pi4RetValFwlAclFragAction = (INT4) pInFilterNode->u1FragAction;
        i1Status = SNMP_SUCCESS;
    }
    else if (i4FwlAclDirection == FWL_DIRECTION_OUT)
    {
        /* search the out filter list for the particular filter name */
        pOutFilterNode = FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                                  au1AclName);
        if (pOutFilterNode == NULL)
        {
            pOutIpv6FilterNode = FwlDbaseSearchAclFilter
                (&pIfaceNode->outIPv6FilterList, au1AclName);
            if (pOutIpv6FilterNode == NULL)
            {
                return i1Status;
            }
            else if (pOutIpv6FilterNode != NULL)
            {
                *pi4RetValFwlAclFragAction =
                    (INT4) pOutIpv6FilterNode->u1FragAction;
                i1Status = SNMP_SUCCESS;
                return i1Status;
            }
        }
        /* get the sequence number  */
        *pi4RetValFwlAclFragAction = (INT4) pOutFilterNode->u1FragAction;

        i1Status = SNMP_SUCCESS;
    }
    else
    {
        i1Status = SNMP_FAILURE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Acl Type GET Exit\n");

    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetFwlAclRowStatus
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                retValFwlAclRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlAclRowStatus (INT4 i4FwlAclIfIndex,
                       tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                       INT4 i4FwlAclDirection, INT4 *pi4RetValFwlAclRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN + FWL_ONE] =
        { FWL_ZERO };
    tIfaceInfo         *pIfaceNode = NULL;
    tInFilterInfo      *pInFilterNode = NULL;
    tOutFilterInfo     *pOutFilterNode = NULL;
    tInIpv6FilterInfo  *pInIpv6FilterNode = NULL;
    tOutIpv6FilterInfo *pOutIpv6FilterNode = NULL;
    UINT4               u4IfaceNum = (UINT4) i4FwlAclIfIndex;

    pOutFilterNode = (tOutFilterInfo *) NULL;
    pInFilterNode = (tInFilterInfo *) NULL;
    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nAcl RowStatus GET Entry\n");

    FWL_MEMCPY (au1AclName, pFwlAclAclName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlAclAclName->i4_Length,
                               (INT4) sizeof (au1AclName)));
    au1AclName[pFwlAclAclName->i4_Length] = FWL_END_OF_STRING;

    /* search for the interface index */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (i4FwlAclDirection == FWL_DIRECTION_IN)
    {
        /* if direction is IN then search the infilter list to get
         * the rowstatus 
         */
        pInFilterNode = FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                                 au1AclName);
        if (pInFilterNode == NULL)
        {
            pInIpv6FilterNode = FwlDbaseSearchAclFilter
                (&pIfaceNode->inIPv6FilterList, au1AclName);
            if (pInIpv6FilterNode == NULL)
            {
                return i1Status;
            }
            else if (pInIpv6FilterNode != NULL)
            {
                *pi4RetValFwlAclRowStatus =
                    (INT4) pInIpv6FilterNode->u1RowStatus;
                i1Status = SNMP_SUCCESS;
                return i1Status;
            }
        }

        *pi4RetValFwlAclRowStatus = (INT4) pInFilterNode->u1RowStatus;
        i1Status = SNMP_SUCCESS;
    }
    else if (i4FwlAclDirection == FWL_DIRECTION_OUT)
    {
        /* if direction is OUT then search the outfilter list to get
         * the rowstatus 
         */
        pOutFilterNode = FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                                  au1AclName);
        if (pOutFilterNode == NULL)
        {
            pOutIpv6FilterNode = FwlDbaseSearchAclFilter
                (&pIfaceNode->outIPv6FilterList, au1AclName);
            if (pOutIpv6FilterNode == NULL)
            {
                return i1Status;
            }
            else if (pOutIpv6FilterNode != NULL)
            {
                *pi4RetValFwlAclRowStatus =
                    (INT4) pOutIpv6FilterNode->u1RowStatus;
                i1Status = SNMP_SUCCESS;
                return i1Status;
            }
        }

        *pi4RetValFwlAclRowStatus = (INT4) pOutFilterNode->u1RowStatus;
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        i1Status = SNMP_FAILURE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\nAcl RowStatus GET Exit\n");

    return i1Status;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlAclAction
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                setValFwlAclAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlAclAction (INT4 i4FwlAclIfIndex,
                    tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                    INT4 i4FwlAclDirection, INT4 i4SetValFwlAclAction)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    INT1                i1Status = SNMP_FAILURE;
    UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
    UINT4               u4IfaceNum = (UINT4) i4FwlAclIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Acl Action SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    FWL_MEMCPY (au1AclName, pFwlAclAclName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlAclAclName->i4_Length,
                               (INT4) sizeof (au1AclName)));
    au1AclName[pFwlAclAclName->i4_Length] = FWL_END_OF_STRING;

    /* If the Interface number is not global then set the Action value
     * the Acl Node 
     */
    if (u4IfaceNum < FWL_MAX_NUM_OF_IF)
    {
        pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
        if (pIfaceNode != NULL)
        {
            i1Status = FwlSetActionValueForAclNode (pIfaceNode,
                                                    i4FwlAclDirection,
                                                    au1AclName,
                                                    i4SetValFwlAclAction);
        }
    }
    /* If the Interface number is global, then set the Action value for
     * Acl Node for all interfaces in whinch the filters are already 
     * configured.
     */
    else if (u4IfaceNum == FWL_MAX_NUM_OF_IF)
    {
        u4IfaceNum = FWL_ONE;
        while (u4IfaceNum < FWL_MAX_NUM_OF_IF)
        {
            pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
            if (pIfaceNode != NULL)
            {
                i1Status = FwlSetActionValueForAclNode (pIfaceNode,
                                                        i4FwlAclDirection,
                                                        au1AclName,
                                                        i4SetValFwlAclAction);
                if (i1Status == SNMP_FAILURE)
                {
                    break;
                }
                else
                {
                    MOD_TRC_ARG3 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\n Action Value %d is set for Filter/Rule %s"
                                  "applied on Interface %d \n",
                                  i4SetValFwlAclAction, au1AclName, u4IfaceNum);
                }
            }
            u4IfaceNum++;
        }
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Acl Action SET Exit \n");
    if (i1Status == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlAclAction, u4SeqNum, FALSE,
                              FwlLock, FwlUnLock, FWL_THREE, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i %i", i4FwlAclIfIndex,
                          pFwlAclAclName, i4FwlAclDirection,
                          i4SetValFwlAclAction));
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFwlAclSequenceNumber
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                setValFwlAclSequenceNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlAclSequenceNumber (INT4 i4FwlAclIfIndex,
                            tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                            INT4 i4FwlAclDirection,
                            INT4 i4SetValFwlAclSequenceNumber)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4Status = FWL_FAILURE;
    UINT2               u2SeqNum = (UINT2) i4SetValFwlAclSequenceNumber;
    UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN + FWL_ONE] =
        { FWL_ZERO };
    UINT4               u4IfaceNum = (UINT4) i4FwlAclIfIndex;

    UINT1               u1IfaceIndex = FWL_ZERO;
    UINT1               u1Index = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Acl Sequence Number SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    MEMSET (gau4IfaceList, FWL_ZERO, sizeof (gau4IfaceList));

    FWL_MEMCPY (au1AclName, pFwlAclAclName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlAclAclName->i4_Length,
                               (INT4) sizeof (au1AclName)));
    au1AclName[pFwlAclAclName->i4_Length] = FWL_END_OF_STRING;

    /* If the Interface number is not global then set the Sequence number
     * the Acl Node 
     */
    if (u4IfaceNum < FWL_MAX_NUM_OF_IF)
    {
        pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
        if (pIfaceNode != NULL)
        {
            i1Status = FwlSetSeqNumValueForAclNode (pIfaceNode,
                                                    i4FwlAclDirection,
                                                    au1AclName, u2SeqNum);
        }
    }
    /* If the Interface number is global, then set the sequence number for
     * Acl Node for all interfaces in whinch the filters are already 
     * configured.
     */
    else if (u4IfaceNum == FWL_MAX_NUM_OF_IF)
    {
        u4IfaceNum = FWL_ONE;
        while (u4IfaceNum < FWL_MAX_NUM_OF_IF)
        {
            pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
            if (pIfaceNode != NULL)
            {
                i1Status = FwlSetSeqNumValueForAclNode (pIfaceNode,
                                                        i4FwlAclDirection,
                                                        au1AclName, u2SeqNum);
                if (i1Status == SNMP_FAILURE)
                {
                    while (u1Index < u1IfaceIndex)
                    {
                        i4Status =
                            (INT4) FwlDbaseDeleteAcl (gau4IfaceList[u1Index],
                                                      i4FwlAclDirection,
                                                      au1AclName);
                        MOD_TRC_ARG3 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                      "\nFilter/Rule %s applied on Interface %d"
                                      "is deleted since the sequence number %d "
                                      "is assigned to another Filter/rule \n",
                                      au1AclName, gau4IfaceList[u1Index],
                                      u2SeqNum);
                        if (i4Status == FWL_SUCCESS)
                        {
                            u1Index++;
                        }
                    }            /* end of while */
                    break;
                }
                else
                {
                    MOD_TRC_ARG3 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\n Sequence Number %d "
                                  "is set for Filter/Rule %s "
                                  "applied on Interface %d \n",
                                  u2SeqNum, au1AclName, u4IfaceNum);
                    gau4IfaceList[u1IfaceIndex] = u4IfaceNum;
                    u1IfaceIndex++;
                }
            }
            u4IfaceNum++;
        }                        /* end of while */
    }                            /* end of else if */

    FWL_DBG (FWL_DBG_EXIT, "\n Acl Sequence Number SET Exit \n");
    if (i1Status == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlAclSequenceNumber, u4SeqNum,
                              FALSE, FwlLock, FwlUnLock, FWL_THREE,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i %i", i4FwlAclIfIndex,
                          pFwlAclAclName, i4FwlAclDirection,
                          i4SetValFwlAclSequenceNumber));
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFwlAclLogTrigger
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                setValFwlAclLogTrigger
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlAclLogTrigger (INT4 i4FwlAclIfIndex,
                        tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                        INT4 i4FwlAclDirection, INT4 i4SetValFwlAclLogTrigger)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tIfaceInfo         *pIfaceInfo = NULL;
    tAclInfo           *pAclNode = NULL;
    tAclInfo           *pAclTempNode = NULL;
    tFilterInfo        *pFilterNode = NULL;
    tRuleInfo          *pRuleFilter = NULL;
    tTMO_SLL           *pAclList = NULL;
    UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN + FWL_ONE] =
        { FWL_ZERO };
    INT4                i4Length = FWL_ZERO;

    RM_GET_SEQ_NUM (&u4SeqNum);
    pAclNode = (tAclInfo *) NULL;
    pIfaceInfo = gFwlAclInfo.apIfaceList[i4FwlAclIfIndex];
    FWL_MEMCPY (au1AclName, pFwlAclAclName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlAclAclName->i4_Length,
                               (INT4) sizeof (au1AclName)));
    au1AclName[pFwlAclAclName->i4_Length] = FWL_END_OF_STRING;
    if (i4FwlAclDirection == FWL_DIRECTION_IN)
    {
        pAclTempNode =
            FwlDbaseSearchAclFilter (&pIfaceInfo->inFilterList, au1AclName);
        if (pAclTempNode == NULL)
        {
            pAclTempNode =
                FwlDbaseSearchAclFilter (&pIfaceInfo->inIPv6FilterList,
                                         au1AclName);
        }
    }
    else if (i4FwlAclDirection == FWL_DIRECTION_OUT)
    {
        pAclTempNode =
            FwlDbaseSearchAclFilter (&pIfaceInfo->outFilterList, au1AclName);
        if (pAclTempNode == NULL)
        {
            pAclTempNode =
                FwlDbaseSearchAclFilter (&pIfaceInfo->outIPv6FilterList,
                                         au1AclName);
        }
    }
    if (pAclTempNode == NULL)
    {
        return SNMP_FAILURE;
    }

    pRuleFilter = (tRuleInfo *) pAclTempNode->pAclName;

    if (pRuleFilter == NULL)
    {
        return SNMP_FAILURE;
    }
    pFilterNode = pRuleFilter->apFilterInfo[FWL_RULE_FIRST_FILTER_INDEX];
    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FwlAclDirection == FWL_DIRECTION_IN)
    {
        if (pFilterNode->u2AddrType == FWL_IP_VERSION_4)
        {
            pAclList = &pIfaceInfo->inFilterList;
        }
        else if (pFilterNode->u2AddrType == FWL_IP_VERSION_6)
        {
            pAclList = &pIfaceInfo->inIPv6FilterList;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else if (i4FwlAclDirection == FWL_DIRECTION_OUT)
    {
        if (pFilterNode->u2AddrType == FWL_IP_VERSION_4)
        {
            pAclList = &pIfaceInfo->outFilterList;
        }
        else if (pFilterNode->u2AddrType == FWL_IP_VERSION_6)
        {
            pAclList = &pIfaceInfo->outIPv6FilterList;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
/*    else
    {
        return SNMP_FAILURE;
    }
*/
    TMO_SLL_Scan (pAclList, pAclNode, tAclInfo *)
    {
        i4Length = (INT4) STRLEN (pAclNode->au1FilterName);
        if ((pFwlAclAclName->i4_Length == i4Length)
            && (MEMCMP (au1AclName, pAclNode->au1FilterName,
                        (size_t) pFwlAclAclName->i4_Length) ==
                FWL_STRING_EQUAL))
        {
            pAclNode->u1LogTrigger = (UINT1) i4SetValFwlAclLogTrigger;
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlAclLogTrigger, u4SeqNum,
                                  FALSE, FwlLock, FwlUnLock, FWL_THREE,
                                  SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i %i", i4FwlAclIfIndex,
                              pFwlAclAclName, i4FwlAclDirection,
                              i4SetValFwlAclLogTrigger));
            return SNMP_SUCCESS;
        }
    }                            /* End of the TMO_SLL_Scan */
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlAclLogTrigger, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_THREE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i %i", i4FwlAclIfIndex,
                      pFwlAclAclName, i4FwlAclDirection,
                      i4SetValFwlAclLogTrigger));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFwlAclFragAction
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                setValFwlAclFragAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlAclFragAction (INT4 i4FwlAclIfIndex,
                        tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                        INT4 i4FwlAclDirection, INT4 i4SetValFwlAclFragAction)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tIfaceInfo         *pIfaceInfo = NULL;
    tAclInfo           *pAclNode = NULL;
    tAclInfo           *pAclTempNode = NULL;
    tFilterInfo        *pFilterNode = NULL;
    tRuleInfo          *pRuleFilter = NULL;
    tTMO_SLL           *pAclList = NULL;
    UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN + FWL_ONE] =
        { FWL_ZERO };
    INT4                i4Length = FWL_ZERO;

    RM_GET_SEQ_NUM (&u4SeqNum);
    pAclNode = (tAclInfo *) NULL;
    pIfaceInfo = gFwlAclInfo.apIfaceList[i4FwlAclIfIndex];
    FWL_MEMCPY (au1AclName, pFwlAclAclName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlAclAclName->i4_Length,
                               (INT4) sizeof (au1AclName)));
    au1AclName[pFwlAclAclName->i4_Length] = FWL_END_OF_STRING;
    if (i4FwlAclDirection == FWL_DIRECTION_IN)
    {
        pAclTempNode =
            FwlDbaseSearchAclFilter (&pIfaceInfo->inFilterList, au1AclName);
        if (pAclTempNode == NULL)
        {
            pAclTempNode =
                FwlDbaseSearchAclFilter (&pIfaceInfo->inIPv6FilterList,
                                         au1AclName);
        }
    }
    else if (i4FwlAclDirection == FWL_DIRECTION_OUT)
    {
        pAclTempNode =
            FwlDbaseSearchAclFilter (&pIfaceInfo->outFilterList, au1AclName);
        if (pAclTempNode == NULL)
        {
            pAclTempNode =
                FwlDbaseSearchAclFilter (&pIfaceInfo->outIPv6FilterList,
                                         au1AclName);
        }
    }
    if (pAclTempNode == NULL)
    {
        return SNMP_FAILURE;
    }

    pRuleFilter = (tRuleInfo *) pAclTempNode->pAclName;

    if (pRuleFilter == NULL)
    {
        return SNMP_FAILURE;
    }
    pFilterNode = pRuleFilter->apFilterInfo[FWL_RULE_FIRST_FILTER_INDEX];
    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FwlAclDirection == FWL_DIRECTION_IN)
    {
        if (pFilterNode->u2AddrType == FWL_IP_VERSION_4)
        {
            pAclList = &pIfaceInfo->inFilterList;
        }
        else if (pFilterNode->u2AddrType == FWL_IP_VERSION_6)
        {
            pAclList = &pIfaceInfo->inIPv6FilterList;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else if (i4FwlAclDirection == FWL_DIRECTION_OUT)
    {
        if (pFilterNode->u2AddrType == FWL_IP_VERSION_4)
        {
            pAclList = &pIfaceInfo->outFilterList;
        }
        else if (pFilterNode->u2AddrType == FWL_IP_VERSION_6)
        {
            pAclList = &pIfaceInfo->outIPv6FilterList;
        }
        else
        {
            return SNMP_FAILURE;
        }

    }
/*    else
    {
        return SNMP_FAILURE;
    }
*/
    TMO_SLL_Scan (pAclList, pAclNode, tAclInfo *)
    {
        i4Length = (INT4) STRLEN (pAclNode->au1FilterName);
        if ((pFwlAclAclName->i4_Length == i4Length)
            && (MEMCMP (pFwlAclAclName->pu1_OctetList, pAclNode->au1FilterName,
                        (size_t) pFwlAclAclName->i4_Length) ==
                FWL_STRING_EQUAL))
        {
            pAclNode->u1FragAction = (UINT1) i4SetValFwlAclFragAction;
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlAclFragAction, u4SeqNum,
                                  FALSE, FwlLock, FwlUnLock, FWL_THREE,
                                  SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i %i", i4FwlAclIfIndex,
                              pFwlAclAclName, i4FwlAclDirection,
                              i4SetValFwlAclFragAction));
            return SNMP_SUCCESS;
        }
    }                            /* End of the TMO_SLL_Scan */
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFwlAclRowStatus
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                setValFwlAclRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlAclRowStatus (INT4 i4FwlAclIfIndex,
                       tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                       INT4 i4FwlAclDirection, INT4 i4SetValFwlAclRowStatus)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4Status = FWL_FAILURE;
    UINT4               u4IfaceNum = (UINT4) i4FwlAclIfIndex;
    UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN + FWL_ONE] =
        { FWL_ZERO };
    INT4                i4Direction = i4FwlAclDirection;
    tIfaceInfo         *pIfaceNode = NULL;
    UINT1               u1Index = FWL_ZERO;
    UINT4               u4IfaceIndex = FWL_ZERO;
    UINT1               au1CmdStr[FWL_CMD_DEF_BUF_SIZE] = { FWL_ZERO };
    UINT1               au1FwlMsg[FWL_CMD_DEF_BUF_SIZE] = { FWL_ZERO };

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Acl Row Status SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    MEMSET (gau4IfaceList, FWL_ZERO, sizeof (gau4IfaceList));

    FWL_MEMCPY (au1AclName, pFwlAclAclName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlAclAclName->i4_Length,
                               (INT4) sizeof (au1AclName)));
    au1AclName[pFwlAclAclName->i4_Length] = FWL_END_OF_STRING;

    /* The Acl Node can be created only when the row status is given 
     * CREATE_AND_WAIT. The node will become ACTIVE when the appropriate fields
     * are SET.
     */
    switch (i4SetValFwlAclRowStatus)
    {
        case FWL_CREATE_AND_WAIT:
            if (u4IfaceNum < FWL_MAX_NUM_OF_IF)
            {
                i1Status = FwlSetInitForAclNode (u4IfaceNum, i4Direction,
                                                 au1AclName);
            }
            else if (u4IfaceNum == FWL_MAX_NUM_OF_IF)
            {
                u4IfaceNum = FWL_ONE;
                while (u4IfaceNum < FWL_MAX_NUM_OF_IF)
                {
                    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
                    if (pIfaceNode != NULL)
                    {
                        i1Status =
                            FwlSetInitForAclNode (u4IfaceNum, i4Direction,
                                                  au1AclName);
                        if (i1Status == SNMP_FAILURE)
                        {
                            while (u1Index < u4IfaceIndex)
                            {
                                i4Status =
                                    (INT4)
                                    FwlDbaseDeleteAcl (gau4IfaceList[u1Index],
                                                       i4Direction, au1AclName);
                                MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC,
                                              "FWL",
                                              "\n Filter/Rule %s "
                                              "applied on Interface %d "
                                              "is deleted since it already "
                                              "exists \n", au1AclName,
                                              gau4IfaceList[u1Index]);
                                if (i4Status == FWL_SUCCESS)
                                {
                                    u1Index++;
                                }
                            }    /* end of while */
                            break;
                        }
                        else
                        {
                            MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                          "\n Filter/Rule %s "
                                          "is applied on Interface %d \n",
                                          au1AclName, u4IfaceNum);
                            gau4IfaceList[u4IfaceIndex] = u4IfaceNum;
                            u4IfaceIndex++;
                        }
                    }
                    u4IfaceNum++;
                }                /*end of while */
            }
            break;
        case FWL_DESTROY:
            /* delete the infilter or outfilter node */
            i4Status =
                (INT4) FwlDbaseDeleteAcl (u4IfaceNum, i4Direction, au1AclName);
            if (i4Status == FWL_SUCCESS)
            {
                i1Status = SNMP_SUCCESS;
                MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n Filter/Rule %s applied on Interface %d "
                              "is deleted \n", au1AclName, u4IfaceNum);
                i1Status = SNMP_SUCCESS;
                if (i4Direction == FWL_DIRECTION_IN)
                {
                    SPRINTF ((CHR1 *) au1CmdStr, "no access-list %s %s",
                             (CHR1 *) au1AclName, (CHR1 *) "in");
                    SPRINTF ((CHR1 *) au1FwlMsg, " \"%s\" %s ",
                             au1CmdStr, FWL_CMD_EXEC_MSG);
                    FwlLogMessage (FWL_COMM_CONF, FWL_ZERO, NULL, FWL_LOG_MUST,
                                   FWLLOG_INFO_LEVEL, au1FwlMsg);
                }
                else
                {
                    SPRINTF ((CHR1 *) au1CmdStr, "no access-list %s %s",
                             (CHR1 *) au1AclName, (CHR1 *) "out");
                    SPRINTF ((CHR1 *) au1FwlMsg, " \"%s\" %s ",
                             au1CmdStr, FWL_CMD_EXEC_MSG);
                    FwlLogMessage (FWL_COMM_CONF, FWL_ZERO, NULL, FWL_LOG_MUST,
                                   FWLLOG_INFO_LEVEL, au1FwlMsg);

                }

            }
            break;
        case FWL_ACTIVE:
            FwlSetRowStatusValueForAclNode (u4IfaceNum, i4Direction,
                                            au1AclName,
                                            (UINT1) i4SetValFwlAclRowStatus);
            /* Get the Command String */
            FwlGetAclCmdString (u4IfaceNum, au1AclName, i4Direction, au1FwlMsg);
            if (au1FwlMsg[FWL_INDEX_0] != ZERO)
            {
                FwlLogMessage (FWL_COMM_CONF, FWL_ZERO, NULL, FWL_LOG_MUST,
                               FWLLOG_INFO_LEVEL, au1FwlMsg);
            }
            MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n RowStatus status is made Active "
                          "for Filter/Rule %s applied on Interface %d \n",
                          au1AclName, u4IfaceNum);
            i1Status = SNMP_SUCCESS;
            break;
        case FWL_NOT_IN_SERVICE:
            FwlSetRowStatusValueForAclNode (u4IfaceNum, i4Direction,
                                            au1AclName,
                                            (UINT1) i4SetValFwlAclRowStatus);
            MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n RowStatus status is made Not_In_Service "
                          "for Filter/Rule %s applied on Interface %d \n",
                          au1AclName, u4IfaceNum);
            i1Status = SNMP_SUCCESS;
            break;
        default:
            break;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Acl Row Status SET Exit \n");
    if (i1Status == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlAclRowStatus, u4SeqNum, TRUE,
                              FwlLock, FwlUnLock, FWL_THREE, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i %i", i4FwlAclIfIndex,
                          pFwlAclAclName, i4FwlAclDirection,
                          i4SetValFwlAclRowStatus));
    }
    return i1Status;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlAclAction
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                testValFwlAclAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlAclAction (UINT4 *pu4ErrorCode, INT4 i4FwlAclIfIndex,
                       tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                       INT4 i4FwlAclDirection, INT4 i4TestValFwlAclAction)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4IfaceNum = (UINT4) i4FwlAclIfIndex;
    UINT4               u4LoopIndex = FWL_ONE;
    UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
    tAclInfo           *pAclNode = NULL;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;
    pAclNode = (tAclInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nAction TEST Entry\n");

    if ((i4TestValFwlAclAction != FWL_PERMIT)
        && (i4TestValFwlAclAction != FWL_DENY))
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\nAction Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    FWL_MEMCPY (au1AclName, pFwlAclAclName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlAclAclName->i4_Length,
                               (INT4) sizeof (au1AclName)));
    au1AclName[pFwlAclAclName->i4_Length] = FWL_END_OF_STRING;

    MOD_TRC_ARG3 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\nTrying to Set Action Value %d for Filter %s "
                  "on interface %d\n",
                  i4TestValFwlAclAction, au1AclName, u4IfaceNum);

    if (u4IfaceNum < FWL_MAX_NUM_OF_IF)
    {
        /* Initialize the interface index such that the loop is
         * executed only once */
        u4LoopIndex = FWL_MAX_NUM_OF_IF - FWL_ONE;
    }
    else
    {
        /* Invalid Index */
        u4LoopIndex = FWL_MAX_NUM_OF_IF;
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* This loop was intended to search ACL lists corresponding to all
     * interfaces in the global case.
     * At present we do not loop for all interfaces even for the global case.
     * Hence the loop will always be executed only once.
     * The loop has been retained in case it is needed in future */
    while (u4LoopIndex < FWL_MAX_NUM_OF_IF)
    {
        /* search the interface list for the corresponding interface number */
        pIfaceNode = (tIfaceInfo *) NULL;
        pAclNode = (tAclInfo *) NULL;
        pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
        if (pIfaceNode != NULL)
        {
            if (pIfaceNode->u1RowStatus == FWL_ACTIVE)
            {
                if (i4FwlAclDirection == FWL_DIRECTION_IN)
                {
                    if (TMO_SLL_Count (&pIfaceNode->inFilterList) !=
                        FWL_DEFAULT_COUNT)
                    {
                        /* search for the infilter node. if found then validate 
                         * the action value based on Row status value.If the
                         * RowStatus is FWL_CREATE_AND_WAIT or NOT READY then return
                         * SUCCESS, else FAILURE. 
                         */
                        pAclNode =
                            FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                                     au1AclName);
                    }
                    if (pAclNode == NULL)
                    {
                        if (TMO_SLL_Count (&pIfaceNode->inIPv6FilterList) !=
                            FWL_DEFAULT_COUNT)
                        {
                            /* search for the inIPv6 filter node. if found then
                             * validate the action value based on Row status 
                             * value.If the RowStatus is FWL_CREATE_AND_WAIT 
                             * or NOT READY then return SUCCESS else
                             * FAILURE */
                            pAclNode =
                                FwlDbaseSearchAclFilter
                                (&pIfaceNode->inIPv6FilterList, au1AclName);
                        }
                    }
                }                /*end for IN direction */
                else if (i4FwlAclDirection == FWL_DIRECTION_OUT)
                {
                    if (TMO_SLL_Count (&pIfaceNode->outFilterList) !=
                        FWL_DEFAULT_COUNT)
                    {
                        /* search for the outfilter node. if found then validate 
                         * the action value based on Row status value. If the
                         * RowStatus is FWL_CREATE_AND_WAIT or NOT READY then return
                         * SUCCESS, else FAILURE. 
                         */
                        pAclNode =
                            FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                                     au1AclName);
                    }
                    if (pAclNode == NULL)
                    {
                        if (TMO_SLL_Count (&pIfaceNode->outIPv6FilterList) !=
                            FWL_DEFAULT_COUNT)
                        {
                            /* search for the outIPv6filter node. if found then
                             * validate the action value based on Row status
                             * value. If the RowStatus is FWL_CREATE_AND_WAIT 
                             * or NOT READY then return SUCCESS, else FAILURE.*/
                            pAclNode =
                                FwlDbaseSearchAclFilter
                                (&pIfaceNode->outIPv6FilterList, au1AclName);
                        }
                    }
                }                /* end for OUT Direction */
                if (pAclNode != NULL)
                {
                    if (pAclNode->u1RowStatus != FWL_NOT_IN_SERVICE)
                    {
                        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                 "\nAction Value cannot be SET "
                                 "if RowStatus is ACTIVE\n");
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        i1Status = SNMP_FAILURE;
                        break;
                    }
                    else
                    {
                        i1Status = SNMP_SUCCESS;
                    }
                }
                else
                {
                    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\nFilter / Rule %s "
                                  "is not applied on interface %d\n",
                                  au1AclName, u4IfaceNum);
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    i1Status = SNMP_FAILURE;
                    break;
                }
            }
            else
            {
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\nInterface %d is administratively down\n",
                              u4IfaceNum);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1Status = SNMP_FAILURE;
            }
        }
        u4IfaceNum++;
        u4LoopIndex++;
    }                            /* end of while */

    FWL_DBG (FWL_DBG_EXIT, "\n Action TEST Exit \n");
    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlAclSequenceNumber
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                testValFwlAclSequenceNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlAclSequenceNumber (UINT4 *pu4ErrorCode, INT4 i4FwlAclIfIndex,
                               tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                               INT4 i4FwlAclDirection,
                               INT4 i4TestValFwlAclSequenceNumber)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4IfaceNum = (UINT4) i4FwlAclIfIndex;
    UINT4               u4LoopIndex = FWL_ONE;
    tAclInfo           *pAclNode = NULL;
    UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
    tIfaceInfo         *pIfaceNode = NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nSequence Number TEST Entry\n");

    if ((i4TestValFwlAclSequenceNumber <= FWL_ZERO) ||
        (i4TestValFwlAclSequenceNumber > FWL_MAX_IP_PKT_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check for overlapping on the priorities of default rules */
    if (i4TestValFwlAclSequenceNumber == FWL_DEF_RULE_PRIORITY_MIN)
    {
        CLI_SET_ERR (CLI_FWL_DEF_PRIORITY_RANGE_CLASH);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    FWL_MEMCPY (au1AclName, pFwlAclAclName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlAclAclName->i4_Length,
                               (INT4) sizeof (au1AclName)));
    au1AclName[pFwlAclAclName->i4_Length] = FWL_END_OF_STRING;

    MOD_TRC_ARG3 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\nTrying to Set Sequence Number %d for Filter %s "
                  "on interface %d\n",
                  i4TestValFwlAclSequenceNumber, au1AclName, u4IfaceNum);

    if (u4IfaceNum < FWL_MAX_NUM_OF_IF)
    {
        /* Initialize the interface index such that the loop is
         * executed only once */
        u4LoopIndex = FWL_MAX_NUM_OF_IF - FWL_ONE;
    }
    else
    {
        /* Invalid Index */
        u4LoopIndex = FWL_MAX_NUM_OF_IF;
    }
    /* This loop was intended to search ACL lists corresponding to all
     * interfaces in the global case.
     * At present we do not loop for all interfaces even for the global case.
     * Hence the loop will always be executed only once.
     * The loop has been retained in case it is needed in future */
    while (u4LoopIndex < FWL_MAX_NUM_OF_IF)
    {
        pIfaceNode = (tIfaceInfo *) NULL;
        pAclNode = (tAclInfo *) NULL;
        /* search the interface list for the corresponding interface number */
        pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
        if (pIfaceNode != NULL)
        {
            if (pIfaceNode->u1RowStatus == FWL_ACTIVE)
            {
                if (i4FwlAclDirection == FWL_DIRECTION_IN)
                {
                    if (TMO_SLL_Count (&pIfaceNode->inFilterList) !=
                        FWL_DEFAULT_COUNT)
                    {
                        /* search for the infilter node. if found then validate 
                         * the action value based on Row status value.If the
                         * RowStatus is FWL_CREATE_AND_WAIT or NOT READY then 
                         * return SUCCESS, else FAILURE. 
                         */
                        pAclNode =
                            FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                                     au1AclName);
                    }
                    if (pAclNode == NULL)
                    {
                        /* Check in inIPv6FilterList */
                        if (TMO_SLL_Count (&pIfaceNode->inIPv6FilterList) !=
                            FWL_DEFAULT_COUNT)
                        {
                            /* search for the inIpv6filter node. if found then
                             * validate the action value based on Row status
                             * .If the RowStatus is FWL_CREATE_AND_WAIT 
                             * or NOT READY then return SUCCESS, else FAILURE.
                             */

                            pAclNode =
                                FwlDbaseSearchAclFilter
                                (&pIfaceNode->inIPv6FilterList, au1AclName);
                        }
                    }
                }                /*end for IN direction */
                else if (i4FwlAclDirection == FWL_DIRECTION_OUT)
                {
                    if (TMO_SLL_Count (&pIfaceNode->outFilterList) !=
                        FWL_DEFAULT_COUNT)
                    {
                        /* search for the outfilter node. if found then validate 
                         * the action value based on Row status value.If the
                         * RowStatus is FWL_CREATE_AND_WAIT or NOT READY then
                         * return SUCCESS, else FAILURE. 
                         */
                        pAclNode =
                            FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                                     au1AclName);
                    }
                    if (pAclNode == NULL)
                    {
                        if (TMO_SLL_Count
                            (&pIfaceNode->outIPv6FilterList) !=
                            FWL_DEFAULT_COUNT)
                        {
                            pAclNode = FwlDbaseSearchAclFilter
                                (&pIfaceNode->outIPv6FilterList, au1AclName);
                        }
                    }
                }                /*end for OUT direction */
                if (pAclNode != NULL)
                {
                    if (pAclNode->u1RowStatus != FWL_NOT_IN_SERVICE)
                    {
                        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                 "\nSequence Number cannot be SET "
                                 "if RowStatus is ACTIVE\n");
                        i1Status = SNMP_FAILURE;
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        break;
                    }
                    else
                    {
                        i1Status = SNMP_SUCCESS;
                    }
                }
                else
                {
                    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\nFilter / Rule %s "
                                  "is not applied on interface %d\n",
                                  au1AclName, u4IfaceNum);
                    i1Status = SNMP_FAILURE;
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    break;
                }
            }
            else
            {
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\nInterface %d is administratively down\n",
                              u4IfaceNum);
                i1Status = SNMP_FAILURE;
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                break;
            }
        }
        u4IfaceNum++;
        u4LoopIndex++;
    }                            /* end of While */

    FWL_DBG (FWL_DBG_EXIT, "\nSequence Number TEST Exit\n");

    return i1Status;

}

/****************************************************************************
 Function    :  nmhTestv2FwlAclLogTrigger
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                testValFwlAclLogTrigger
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlAclLogTrigger (UINT4 *pu4ErrorCode, INT4 i4FwlAclIfIndex,
                           tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                           INT4 i4FwlAclDirection,
                           INT4 i4TestValFwlAclLogTrigger)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4IfaceNum = (UINT4) i4FwlAclIfIndex;
    UINT4               u4LoopIndex = FWL_ONE;
    UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
    tAclInfo           *pAclNode = NULL;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;
    pAclNode = (tAclInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nAction TEST Entry\n");

    if ((i4TestValFwlAclLogTrigger != FWL_PERMIT)
        && (i4TestValFwlAclLogTrigger != FWL_DENY)
        && (i4TestValFwlAclLogTrigger != FWL_NONE))
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\nAction Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    FWL_MEMCPY (au1AclName, pFwlAclAclName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlAclAclName->i4_Length,
                               (INT4) sizeof (au1AclName)));
    au1AclName[pFwlAclAclName->i4_Length] = FWL_END_OF_STRING;

    MOD_TRC_ARG3 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\nTrying to Set Action Value %d for Filter %s "
                  "on interface %d\n",
                  i4TestValFwlAclLogTrigger, au1AclName, u4IfaceNum);
    if (u4IfaceNum < FWL_MAX_NUM_OF_IF)
    {
        /* Initialize the interface index such that the loop is
         * executed only once */
        u4LoopIndex = FWL_MAX_NUM_OF_IF - FWL_ONE;
    }
    else
    {
        /* Invalid Index */
        u4LoopIndex = FWL_MAX_NUM_OF_IF;
    }
    /* This loop was intended to search ACL lists corresponding to all
     * interfaces in the global case.
     * At present we do not loop for all interfaces even for the global case.
     * Hence the loop will always be executed only once.
     * The loop has been retained in case it is needed in future */
    while (u4LoopIndex < FWL_MAX_NUM_OF_IF)
    {
        /* search the interface list for the coresponding interface number */
        pIfaceNode = (tIfaceInfo *) NULL;
        pAclNode = (tAclInfo *) NULL;
        pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
        if (pIfaceNode != NULL)
        {
            if (pIfaceNode->u1RowStatus == FWL_ACTIVE)
            {
                if (i4FwlAclDirection == FWL_DIRECTION_IN)
                {
                    if (TMO_SLL_Count (&pIfaceNode->inFilterList) !=
                        FWL_DEFAULT_COUNT)
                    {
                        /* search for the infilter node. if found then validate 
                         * the action value based on Row status value.If the
                         * RowStatus is FWL_CREATE_AND_WAIT or NOT READY then return
                         * SUCCESS, else FAILURE. 
                         */
                        pAclNode =
                            FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                                     au1AclName);
                    }
                    if (pAclNode == NULL)
                    {
                        /*Check in IPv6 filterlist */
                        if (TMO_SLL_Count (&pIfaceNode->inIPv6FilterList) !=
                            FWL_DEFAULT_COUNT)
                        {
                            pAclNode =
                                FwlDbaseSearchAclFilter
                                (&pIfaceNode->inIPv6FilterList, au1AclName);
                        }
                    }
                }                /*end for IN direction */
                else if (i4FwlAclDirection == FWL_DIRECTION_OUT)
                {
                    if (TMO_SLL_Count (&pIfaceNode->outFilterList) !=
                        FWL_DEFAULT_COUNT)
                    {
                        /* search for the outfilter node. if found then validate 
                         * the action value based on Row status value. If the
                         * RowStatus is FWL_CREATE_AND_WAIT or NOT READY then return
                         * SUCCESS, else FAILURE. 
                         */
                        pAclNode =
                            FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                                     au1AclName);
                    }
                    if (pAclNode == NULL)
                    {
                        if (TMO_SLL_Count (&pIfaceNode->outIPv6FilterList) !=
                            FWL_DEFAULT_COUNT)
                        {
                            pAclNode = FwlDbaseSearchAclFilter
                                (&pIfaceNode->outFilterList, au1AclName);
                        }
                    }
                }                /* end for OUT Direction */
                if (pAclNode != NULL)
                {
                    if (pAclNode->u1RowStatus != FWL_NOT_IN_SERVICE)
                    {
                        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                 "\nAction Value cannot be SET "
                                 "if RowStatus is ACTIVE\n");
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        i1Status = SNMP_FAILURE;
                        break;
                    }
                    else
                    {
                        i1Status = SNMP_SUCCESS;
                    }
                }
                else
                {
                    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\nFilter / Rule %s "
                                  "is not applied on interface %d\n",
                                  au1AclName, u4IfaceNum);
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    i1Status = SNMP_FAILURE;
                    break;
                }
            }
            else
            {
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\nInterface %d is administratively down\n",
                              u4IfaceNum);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1Status = SNMP_FAILURE;
            }
        }
        u4IfaceNum++;
        u4LoopIndex++;
    }                            /* end of while */

    FWL_DBG (FWL_DBG_EXIT, "\n Action TEST Exit \n");
    return i1Status;

}

/****************************************************************************
 Function    :  nmhTestv2FwlAclFragAction
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                testValFwlAclFragAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlAclFragAction (UINT4 *pu4ErrorCode, INT4 i4FwlAclIfIndex,
                           tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                           INT4 i4FwlAclDirection,
                           INT4 i4TestValFwlAclFragAction)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4IfaceNum = (UINT4) i4FwlAclIfIndex;
    UINT4               u4LoopIndex = FWL_ONE;
    UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
    tAclInfo           *pAclNode = NULL;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;
    pAclNode = (tAclInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nAction TEST Entry\n");

    if ((i4TestValFwlAclFragAction != FWL_PERMIT)
        && (i4TestValFwlAclFragAction != FWL_DENY))
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\nAction Value can be 1 or 2\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    FWL_MEMCPY (au1AclName, pFwlAclAclName->pu1_OctetList,
                MEM_MAX_BYTES (pFwlAclAclName->i4_Length,
                               (INT4) sizeof (au1AclName)));
    au1AclName[pFwlAclAclName->i4_Length] = FWL_END_OF_STRING;

    MOD_TRC_ARG3 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\nTrying to Set Action Value %d for Filter %s "
                  "on interface %d\n",
                  i4TestValFwlAclFragAction, au1AclName, u4IfaceNum);
    if (u4IfaceNum < FWL_MAX_NUM_OF_IF)
    {
        /* Initialize the interface index such that the loop is
         * executed only once */
        u4LoopIndex = FWL_MAX_NUM_OF_IF - FWL_ONE;
    }
    else
    {
        /* Invalid Index */
        u4LoopIndex = FWL_MAX_NUM_OF_IF;
    }
    /* This loop was intended to search ACL lists corresponding to all
     * interfaces in the global case.
     * At present we do not loop for all interfaces even for the global case.
     * Hence the loop will always be executed only once.
     * The loop has been retained in case it is needed in future */
    while (u4LoopIndex < FWL_MAX_NUM_OF_IF)
    {
        /* search the interface list for the coresponding interface number */
        pIfaceNode = (tIfaceInfo *) NULL;
        pAclNode = (tAclInfo *) NULL;
        pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
        if (pIfaceNode != NULL)
        {
            if (pIfaceNode->u1RowStatus == FWL_ACTIVE)
            {
                if (i4FwlAclDirection == FWL_DIRECTION_IN)
                {
                    if (TMO_SLL_Count (&pIfaceNode->inFilterList) !=
                        FWL_DEFAULT_COUNT)
                    {
                        /* search for the infilter node. if found then validate 
                         * the action value based on Row status value.If the
                         * RowStatus is FWL_CREATE_AND_WAIT or NOT READY then return
                         * SUCCESS, else FAILURE. 
                         */
                        pAclNode =
                            FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                                     au1AclName);
                    }
                    if (pAclNode == NULL)
                    {
                        if (TMO_SLL_Count (&pIfaceNode->inIPv6FilterList) !=
                            FWL_DEFAULT_COUNT)
                        {
                            pAclNode =
                                FwlDbaseSearchAclFilter
                                (&pIfaceNode->inIPv6FilterList, au1AclName);
                        }
                    }
                }                /*end for IN direction */
                else if (i4FwlAclDirection == FWL_DIRECTION_OUT)
                {
                    if (TMO_SLL_Count (&pIfaceNode->outFilterList) !=
                        FWL_DEFAULT_COUNT)
                    {
                        /* search for the outfilter node. if found then validate 
                         * the action value based on Row status value. If the
                         * RowStatus is FWL_CREATE_AND_WAIT or NOT READY then return
                         * SUCCESS, else FAILURE. 
                         */
                        pAclNode =
                            FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                                     au1AclName);
                    }
                    if (pAclNode == NULL)
                    {
                        if (TMO_SLL_Count (&pIfaceNode->outIPv6FilterList) !=
                            FWL_DEFAULT_COUNT)
                        {
                            pAclNode =
                                FwlDbaseSearchAclFilter
                                (&pIfaceNode->outIPv6FilterList, au1AclName);
                        }
                    }
                }                /* end for OUT Direction */
                if (pAclNode != NULL)
                {
                    if (pAclNode->u1RowStatus != FWL_NOT_IN_SERVICE)
                    {
                        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                 "\nAction Value cannot be SET "
                                 "if RowStatus is ACTIVE\n");
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        i1Status = SNMP_FAILURE;
                        break;
                    }
                    else
                    {
                        i1Status = SNMP_SUCCESS;
                    }
                }
                else
                {
                    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\nFilter / Rule %s "
                                  "is not applied on interface %d\n",
                                  au1AclName, u4IfaceNum);
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    i1Status = SNMP_FAILURE;
                    break;
                }
            }
            else
            {
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\nInterface %d is administratively down\n",
                              u4IfaceNum);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1Status = SNMP_FAILURE;
            }
        }
        u4IfaceNum++;
        u4LoopIndex++;
    }                            /* end of while */

    FWL_DBG (FWL_DBG_EXIT, "\n Action TEST Exit \n");
    return i1Status;

}

/****************************************************************************
 Function    :  nmhTestv2FwlAclRowStatus
 Input       :  The Indices
                FwlAclIfIndex
                FwlAclAclName
                FwlAclDirection

                The Object 
                testValFwlAclRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlAclRowStatus (UINT4 *pu4ErrorCode, INT4 i4FwlAclIfIndex,
                          tSNMP_OCTET_STRING_TYPE * pFwlAclAclName,
                          INT4 i4FwlAclDirection, INT4 i4TestValFwlAclRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4IfaceNum = (UINT4) i4FwlAclIfIndex;
    UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
    UINT4               u4LoopIndex = FWL_ONE;
    UINT4               u4IfaceCount = FWL_ZERO;

    FWL_DBG (FWL_DBG_ENTRY, "\nAcl Table RowStatus TEST Entry\n");

    if (pFwlAclAclName->i4_Length <= FWL_MAX_ACL_NAME_LEN - FWL_ONE)
    {
        FWL_MEMCPY (au1AclName, pFwlAclAclName->pu1_OctetList,
                    MEM_MAX_BYTES (pFwlAclAclName->i4_Length,
                                   (INT4) sizeof (au1AclName)));
        au1AclName[pFwlAclAclName->i4_Length] = FWL_END_OF_STRING;

        MOD_TRC_ARG3 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\nTrying to Set RowStatus Value %d for Filter %s "
                      "on interface %d\n",
                      i4TestValFwlAclRowStatus, au1AclName, u4IfaceNum);
        /* Validate the value of Row status. The Acl Name should be less than
         * FWL_MAX_ACL_NAME_LEN and the direction value is also validated. 
         */
        if (u4IfaceNum < FWL_MAX_NUM_OF_IF)
        {
            u4LoopIndex = FWL_MAX_NUM_OF_IF - FWL_ONE;
        }
        else
        {
            u4LoopIndex = FWL_MAX_NUM_OF_IF;
        }
        i1Status = FwlTestRowStatusValueForAclNode (u4IfaceCount,
                                                    u4LoopIndex,
                                                    u4IfaceNum,
                                                    i4FwlAclDirection,
                                                    au1AclName,
                                                    (UINT1)
                                                    i4TestValFwlAclRowStatus);
    }                            /*end of if STRLEN */
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Maximum Acl Name length is %d\n",
                      FWL_MAX_ACL_NAME_LEN - FWL_ONE);
    }
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\nAcl Table Row Status Exit\n");

    return i1Status;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlDefnAclTable
 Input       :  The Indices
                FwlAclIfIndex                                                                                       FwlAclAclName                                                                                       FwlAclDirection
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.                                                             Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDefnAclTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FwlDefnIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlDefnIfTable
 Input       :  The Indices
                FwlIfIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFwlDefnIfTable (INT4 i4FwlIfIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4IfaceNum = (UINT4) i4FwlIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Validate Index for If Table - Entry\n");

    /* search the interface list to confirm whether the interface number 
     * exist or not 
     */
    if (u4IfaceNum < FWL_MAX_NUM_OF_IF)
    {
        pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
        if (pIfaceNode != NULL)
        {
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n Interface number %d doesnt exists\n", u4IfaceNum);
        }
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Validate Index for If Table - Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFwlDefnIfTable
 Input       :  The Indices
                FwlIfIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFwlDefnIfTable (INT4 *pi4FwlIfIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    tIfaceInfo         *pIfaceNode = NULL;
    UINT4               u4IfaceNum = FWL_ZERO;

    FWL_DBG (FWL_DBG_ENTRY, "\n Get First Index for If Table - Entry\n");

    pIfaceNode = (tIfaceInfo *) NULL;

    /* the first valid interface index is searched in the interface list. */

    for (u4IfaceNum = FWL_ZERO; u4IfaceNum < FWL_MAX_NUM_OF_IF; u4IfaceNum++)
    {
        if ((pIfaceNode = gFwlAclInfo.apIfaceList[u4IfaceNum]) != NULL)
        {
            *pi4FwlIfIfIndex = (INT4) u4IfaceNum;
            i1Status = SNMP_SUCCESS;
            break;
        }
        continue;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Get First Index for If Table - Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFwlDefnIfTable
 Input       :  The Indices
                FwlIfIfIndex
                nextFwlIfIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFwlDefnIfTable (INT4 i4FwlIfIfIndex, INT4 *pi4NextFwlIfIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    tIfaceInfo         *pIfaceNode = NULL;
    UINT4               u4IfaceIndex = (UINT4) i4FwlIfIfIndex;
    UINT4               u4IfaceNum = FWL_ZERO;

    FWL_DBG (FWL_DBG_ENTRY, "\n Get Next Index for If Table - Entry\n");

    pIfaceNode = (tIfaceInfo *) NULL;

    /* Check whether the interface node exist for the given 
     * interface number u4IfaceNum, if not return failure.
     * Otherwise, find the next valid interface index.
     */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceIndex);

    if (pIfaceNode != NULL)
    {
        u4IfaceIndex++;
        for (u4IfaceNum = u4IfaceIndex; u4IfaceNum < FWL_MAX_NUM_OF_IF;
             u4IfaceNum++)
        {
            if ((pIfaceNode = gFwlAclInfo.apIfaceList[u4IfaceNum]) != NULL)
            {
                *pi4NextFwlIfIfIndex = (INT4) u4IfaceNum;
                i1Status = SNMP_SUCCESS;
                break;
            }
            continue;
        }
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Get Next Index for If Table - Exit\n");

    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlIfIfType
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                retValFwlIfIfType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlIfIfType (INT4 i4FwlIfIfIndex, INT4 *pi4RetValFwlIfIfType)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n If Type GET Entry\n");

    /*  Search the interface number and get the corresponding interface 
     * type value.
     */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        FWL_DBG (FWL_DBG_EXIT, "\n If Type GET Failed\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFwlIfIfType = (INT4) pIfaceNode->u1IfaceType;

    FWL_DBG (FWL_DBG_EXIT, "\n If Type GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlIfIpOptions
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                retValFwlIfIpOptions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlIfIpOptions (INT4 i4FwlIfIfIndex, INT4 *pi4RetValFwlIfIpOptions)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n IP Options GET Entry\n");

    /* search for the coressponding interface number and get 
     * the ip option value 
     */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFwlIfIpOptions = (INT4) pIfaceNode->u1IpOption;

    FWL_DBG (FWL_DBG_EXIT, "\n Ip Options GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlIfFragments
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                retValFwlIfFragments
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlIfFragments (INT4 i4FwlIfIfIndex, INT4 *pi4RetValFwlIfFragments)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Fragments GET Entry\n");

    /*  Search the interface number and get the corresponding fragment value */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFwlIfFragments = (INT4) pIfaceNode->u1Fragment;

    FWL_DBG (FWL_DBG_EXIT, "\n fragments GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlIfFragmentSize
 Input       :  The Indices
                FwlIfIfIndex

                The Object
                retValFwlIfFragmentSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlIfFragmentSize (INT4 i4FwlIfIfIndex, UINT4 *pu4RetValFwlIfFragmentSize)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Fragments Size GET Entry\n");
    /* Search the interface number and get the corresponding fragment value */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFwlIfFragmentSize = (UINT4) pIfaceNode->u2MaxFragmentSize;

    FWL_DBG (FWL_DBG_EXIT, "\n fragments Size GET Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlIfICMPType
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                retValFwlIfICMPType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlIfICMPType (INT4 i4FwlIfIfIndex, INT4 *pi4RetValFwlIfICMPType)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n ICMP Type GET Entry\n");

    /* Search the interface number and get the corresponding icmp type
     * value 
     */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFwlIfICMPType = (INT4) pIfaceNode->u1IcmpType;

    FWL_DBG (FWL_DBG_EXIT, "\n ICMP Type GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlIfICMPCode
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                retValFwlIfICMPCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlIfICMPCode (INT4 i4FwlIfIfIndex, INT4 *pi4RetValFwlIfICMPCode)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n ICMP Code GET Entry\n");

    /* Search the interface number and get the corresponding icmp code 
     * value 
     */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFwlIfICMPCode = (INT4) pIfaceNode->u1IcmpCode;

    FWL_DBG (FWL_DBG_EXIT, "\n ICMP Code GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlIfICMPv6MsgType 
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                retValFwlIfICMPv6MsgType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlIfICMPv6MsgType (INT4 i4FwlIfIfIndex,
                          INT4 *pi4RetValFwlIfICMPv6MsgType)
{
    UINT4               u4IfaceNum = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;

    u4IfaceNum = (UINT4) i4FwlIfIfIndex;

    FWL_DBG (FWL_DBG_ENTRY, "\n ICMPv6 Msg Type GET Entry\n");

    /* Search the interface number and get the corresponding icmp type
     * value 
     */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFwlIfICMPv6MsgType = pIfaceNode->i4Icmpv6MsgType;

    FWL_DBG (FWL_DBG_EXIT, "\n ICMPv6 Msg Type GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlIfRowStatus
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                retValFwlIfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlIfRowStatus (INT4 i4FwlIfIfIndex, INT4 *pi4RetValFwlIfRowStatus)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n If Table RowStatus GET Entry\n");

    /* Search the interface number and get the corresponding row status
     * value 
     */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFwlIfRowStatus = (INT4) pIfaceNode->u1RowStatus;

    FWL_DBG (FWL_DBG_EXIT, "\n If Table RowStatus GET Exit\n");

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlIfIfType
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                setValFwlIfIfType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlIfIfType (INT4 i4FwlIfIfIndex, INT4 i4SetValFwlIfIfType)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tIfaceInfo         *pIfaceNode = NULL;
    UINT4               u4IfaceNum = FWL_ZERO;
    UINT1               u1NwType = FWL_ZERO;
    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Interface Type SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    /* set the Interface Type value for the particular interface */
    u4IfaceNum = (UINT4) i4FwlIfIfIndex;

    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);

    /* If the interface type is set to External, then 
     * delete all the flows in data plane which are flowing
     * through this interface */
    if (pIfaceNode == NULL)
    {
        return SNMP_FAILURE;
    }

    /*Check if the interface is a WAN interface or not */
    SecUtilGetIfNwType (u4IfaceNum, &u1NwType);
    if (u1NwType != FWL_EXTERNAL_IF)
    {
        return SNMP_FAILURE;
    }

    if ((gu1FirewallStatus == FWL_ENABLE) &&
        (pIfaceNode->u1IfaceType != FWL_EXTERNAL_IF) &&
        (i4SetValFwlIfIfType == FWL_EXTERNAL_IF))
    {
        FL_FWL_DEL_IFINDEX_HW_ENTRIES (u4IfaceNum);
    }

    pIfaceNode->u1IfaceType = (UINT1) i4SetValFwlIfIfType;

    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Interface Type Value %d is set for interface %d\n",
                  i4SetValFwlIfIfType, u4IfaceNum);

    FWL_DBG (FWL_DBG_EXIT, "\n Interface Type SET Exit \n");
#ifdef NAT_WANTED
    NatLock ();
    if (i4SetValFwlIfIfType == FWL_EXTERNAL_IF)
    {
        /*  Create an Interface in the NAT Module if the Intf. is of WAN Type
         *  & Enable NAT over it else don't enable NAT over that Interface 
         *  (Guest VLAN Scenario) */
        if (SecUtilGetIfNwType (u4IfaceNum, &u1NwType) == OSIX_FAILURE)
        {
            NatUnLock ();
            return SNMP_FAILURE;
        }
        if (CFA_NETWORK_TYPE_WAN == u1NwType)
        {
            NatInterfaceEntryCreate ((UINT4) i4FwlIfIfIndex, (UINT4) FWL_ONE);
        }
        else
        {
            /* Guest VLAN Scenario */
            NatInterfaceEntryCreate ((UINT4) i4FwlIfIfIndex, (UINT4) FWL_ZERO);
        }
    }
    else
    {
        NatDeleteDynamicEntry ((UINT4) i4FwlIfIfIndex);
        NatDeInitForIf ((UINT4) i4FwlIfIfIndex);
    }
    NatUnLock ();
#endif
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlIfIfType, u4SeqNum, FALSE, FwlLock,
                          FwlUnLock, FWL_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4FwlIfIfIndex,
                      i4SetValFwlIfIfType));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlIfIpOptions
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                setValFwlIfIpOptions
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlIfIpOptions (INT4 i4FwlIfIfIndex, INT4 i4SetValFwlIfIpOptions)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tIfaceInfo         *pIfaceNode = NULL;
    UINT4               u4IfaceNum = FWL_ZERO;

    pIfaceNode = (tIfaceInfo *) NULL;
    u4IfaceNum = FWL_ZERO;

    FWL_DBG (FWL_DBG_ENTRY, "\n IP Option SET Entry \n");

    RM_GET_SEQ_NUM (&u4SeqNum);
    /* set the IP Option value for the particular interface */
    u4IfaceNum = (UINT4) i4FwlIfIfIndex;
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return SNMP_FAILURE;
    }
    pIfaceNode->u1IpOption = (UINT1) i4SetValFwlIfIpOptions;

    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Ip Option Value %d is set for interface %d\n",
                  i4SetValFwlIfIpOptions, u4IfaceNum);

    FWL_DBG (FWL_DBG_EXIT, "\n IP Option SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlIfIpOptions, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4FwlIfIfIndex,
                      i4SetValFwlIfIpOptions));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFwlIfFragments
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                setValFwlIfFragments
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlIfFragments (INT4 i4FwlIfIfIndex, INT4 i4SetValFwlIfFragments)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tIfaceInfo         *pIfaceNode = NULL;
    UINT4               u4IfaceNum = FWL_ZERO;

    pIfaceNode = (tIfaceInfo *) NULL;
    u4IfaceNum = FWL_ZERO;

    FWL_DBG (FWL_DBG_ENTRY, "\n Fragment SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    /* set the Fragment value for the particular interface */
    u4IfaceNum = (UINT4) i4FwlIfIfIndex;
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return SNMP_FAILURE;
    }
    pIfaceNode->u1Fragment = (UINT1) i4SetValFwlIfFragments;

    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Fragment Value %d is set for interface %d\n",
                  i4SetValFwlIfFragments, u4IfaceNum);

    FWL_DBG (FWL_DBG_EXIT, "\n Fragment SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlIfFragments, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4FwlIfIfIndex,
                      i4SetValFwlIfFragments));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlIfFragmentSize
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                setValFwlIfFragmentSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlIfFragmentSize (INT4 i4FwlIfIfIndex, UINT4 u4SetValFwlIfFragmentSize)
{
    tIfaceInfo         *pIfaceNode = NULL;
    UINT4               u4IfaceNum = FWL_ZERO;
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    pIfaceNode = (tIfaceInfo *) NULL;
    u4IfaceNum = FWL_ZERO;

    FWL_DBG (FWL_DBG_ENTRY, "\n Fragment SET Entry \n");

    /* set the Fragment value for the particular interface */
    u4IfaceNum = (UINT4) i4FwlIfIfIndex;
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return SNMP_FAILURE;
    }
    pIfaceNode->u2MaxFragmentSize = (UINT2) u4SetValFwlIfFragmentSize;

    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Fragment Size %ld is set for interface %d\n",
                  u4SetValFwlIfFragmentSize, u4IfaceNum);
    RM_GET_SEQ_NUM (&u4SeqNum);
    FWL_DBG (FWL_DBG_EXIT, "\n Fragment Size SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlIfFragmentSize, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p", i4FwlIfIfIndex,
                      u4SetValFwlIfFragmentSize));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlIfICMPType
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                setValFwlIfICMPType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlIfICMPType (INT4 i4FwlIfIfIndex, INT4 i4SetValFwlIfICMPType)
{
    tIfaceInfo         *pIfaceNode = NULL;
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4IfaceNum = FWL_ZERO;

    pIfaceNode = (tIfaceInfo *) NULL;
    u4IfaceNum = FWL_ZERO;

    FWL_DBG (FWL_DBG_ENTRY, "\n ICMP Type SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    /* set the ICMP type value for the partcular interface */
    u4IfaceNum = (UINT4) i4FwlIfIfIndex;
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return SNMP_FAILURE;
    }
    pIfaceNode->u1IcmpType = (UINT1) i4SetValFwlIfICMPType;

    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n ICMP Type Value %d is set for interface %d\n",
                  i4SetValFwlIfICMPType, u4IfaceNum);

    FWL_DBG (FWL_DBG_EXIT, "\n  ICMP Type SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlIfICMPType, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4FwlIfIfIndex,
                      i4SetValFwlIfICMPType));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlIfICMPCode
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                setValFwlIfICMPCode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlIfICMPCode (INT4 i4FwlIfIfIndex, INT4 i4SetValFwlIfICMPCode)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4IfaceNum = FWL_ZERO;

    pIfaceNode = (tIfaceInfo *) NULL;
    u4IfaceNum = FWL_ZERO;

    FWL_DBG (FWL_DBG_ENTRY, "\n  ICMP Code SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    /* set th ICMP code value for the particular interface */
    u4IfaceNum = (UINT4) i4FwlIfIfIndex;
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return SNMP_FAILURE;
    }
    pIfaceNode->u1IcmpCode = (UINT1) i4SetValFwlIfICMPCode;

    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n ICMP Code Value %d is set for interface %d\n",
                  i4SetValFwlIfICMPCode, u4IfaceNum);

    FWL_DBG (FWL_DBG_EXIT, "\n ICMP Code SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlIfICMPCode, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4FwlIfIfIndex,
                      i4SetValFwlIfICMPCode));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlIfICMPv6MsgType 
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                SetFwlIfICMPv6MsgType 
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlIfICMPv6MsgType (INT4 i4FwlIfIfIndex, INT4 i4SetValFwlIfICMPv6MsgType)
{
    tIfaceInfo         *pIfaceNode = NULL;
    UINT4               u4IfaceNum = FWL_ZERO;
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY, "\n ICMPv6 Msg Type SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    /* set the ICMP type value for the partcular interface */
    u4IfaceNum = (UINT4) i4FwlIfIfIndex;
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((i4SetValFwlIfICMPv6MsgType == FWL_ZERO) || (pIfaceNode->i4Icmpv6MsgType
                                                     ==
                                                     CLI_FWL_ICMPV6_INSPECT_ALL))
    {
        pIfaceNode->i4Icmpv6MsgType = i4SetValFwlIfICMPv6MsgType;
    }
    else
    {
        pIfaceNode->i4Icmpv6MsgType = pIfaceNode->i4Icmpv6MsgType |
            i4SetValFwlIfICMPv6MsgType;
    }

    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n ICMPv6 Msg  Type Value %d is set for interface %d\n",
                  i4SetValFwlIfICMPv6MsgType, u4IfaceNum);

    FWL_DBG (FWL_DBG_EXIT, "\n  ICMP Type SET Exit \n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlIfICMPv6MsgType, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4FwlIfIfIndex,
                      i4SetValFwlIfICMPv6MsgType));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlIfRowStatus
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                setValFwlIfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlIfRowStatus (INT4 i4FwlIfIfIndex, INT4 i4SetValFwlIfRowStatus)
{
    INT1                i1Status = FWL_ZERO;
    INT4                i4Status = FWL_ZERO;
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tIfaceInfo         *pIfaceNode = NULL;
    UINT4               u4IfaceNum = FWL_ZERO;

    pIfaceNode = (tIfaceInfo *) NULL;
    i1Status = SNMP_FAILURE;

    i4Status = FWL_FAILURE;

    u4IfaceNum = (UINT4) i4FwlIfIfIndex;
    RM_GET_SEQ_NUM (&u4SeqNum);
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if ((pIfaceNode != NULL) &&
        (pIfaceNode->u1RowStatus == i4SetValFwlIfRowStatus))
    {
        return SNMP_SUCCESS;
    }

    FWL_DBG (FWL_DBG_ENTRY, "\n Interface Row Status SET Entry \n");

    /* create the interface node by setting the row status. If interface node 
     * is already created then the Row Status is ACTIVE. if the interface node
     * doesn exist then it should be created by setting the RowStatus CREATE_
     * AND_GO.
     */
    switch (i4SetValFwlIfRowStatus)
    {
        case FWL_ACTIVE:
            if (pIfaceNode != NULL)
            {
                pIfaceNode->u1RowStatus = FWL_ACTIVE;
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n RowStatus is made Active for If Index %d \n",
                              u4IfaceNum);
                i1Status = SNMP_SUCCESS;
            }

            break;
        case FWL_CREATE_AND_WAIT:
        case FWL_CREATE_AND_GO:
            if (pIfaceNode == NULL)
            {
                /* interface node allocation and initialisation */
                if (FwlIfaceMemAllocate ((UINT1 **) (VOID *) &pIfaceNode) ==
                    FWL_SUCCESS)
                {
                    FwlSetDefaultIfaceValue (u4IfaceNum, pIfaceNode);
                    gFwlAclInfo.apIfaceList[u4IfaceNum] = pIfaceNode;
                    pIfaceNode->u1RowStatus = FWL_CREATE_AND_GO;
                    i1Status = SNMP_SUCCESS;

                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\n If Index %d is created\n", u4IfaceNum);
                }
                else
                {
                    /* send a trap for memory failure */
                    FwlGenerateMemFailureTrap (FWL_IFACE_ALLOC_FAILURE);
                    INC_MEM_FAILURE_COUNT;
                    gFwlAclInfo.i4MemStatus = (INT4) MEM_FAILURE;
                }
            }
            break;
        case FWL_DESTROY:
            /* delete the interface node */
            if (pIfaceNode != NULL)
            {
                i4Status = (INT4) FwlDbaseDeleteIface (u4IfaceNum);
                if (i4Status == FWL_SUCCESS)
                {
                    i1Status = SNMP_SUCCESS;
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\n If Index %d is deleted\n", u4IfaceNum);
                }
            }
            else
            {
                i1Status = SNMP_SUCCESS;
            }
            break;
        case FWL_NOT_IN_SERVICE:
            if (pIfaceNode != NULL)
            {
                pIfaceNode->u1RowStatus = FWL_NOT_IN_SERVICE;
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n RowStatus is made Not_In_Service for If Index %d \n",
                              u4IfaceNum);
                i1Status = SNMP_SUCCESS;
            }
            break;
        default:
            break;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Interface Row Status SET Exit \n");
    if (i1Status == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlIfRowStatus, u4SeqNum, TRUE,
                              FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4FwlIfIfIndex,
                          i4SetValFwlIfRowStatus));
    }
    return i1Status;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlIfIfType
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                testValFwlIfIfType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlIfIfType (UINT4 *pu4ErrorCode, INT4 i4FwlIfIfIndex,
                      INT4 i4TestValFwlIfIfType)
{
    INT1                i1Status = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;

    i1Status = SNMP_FAILURE;
    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nIP Option TEST Entry\n");

    /* Validate the value of IP option based on Row status .
     * If the RowStatus is FWL_ACTIVE then return SUCCESS, else FAILURE.
     */
    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Trying to SET Interface Type %d for Interface %d \n",
                  i4TestValFwlIfIfType, i4FwlIfIfIndex);
    if ((i4FwlIfIfIndex >= FWL_ZERO) && (i4FwlIfIfIndex < FWL_MAX_NUM_OF_IF))
    {
        pIfaceNode = FwlDbaseSearchIface ((UINT4) i4FwlIfIfIndex);
        if (pIfaceNode != NULL)
        {
            if (pIfaceNode->u1RowStatus == FWL_NOT_IN_SERVICE)
            {
                if ((i4TestValFwlIfIfType == FWL_INTERNAL_IF)
                    || (i4TestValFwlIfIfType == FWL_EXTERNAL_IF))
                {
                    i1Status = SNMP_SUCCESS;
                }
                else
                {
                    MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                             "\n If Type value can be 1 or 2\n");
                }
            }
        }
        else
        {
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n If Index %d doesnt exists \n", i4FwlIfIfIndex);
        }
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Invalid Interface Number. It ranges from 1 to %d\n",
                      FWL_MAX_NUM_OF_IF - FWL_ONE);
    }
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\nIP Option TEST Exit\n");

    return i1Status;

}

/****************************************************************************
 Function    :  nmhTestv2FwlIfIpOptions
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                testValFwlIfIpOptions
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlIfIpOptions (UINT4 *pu4ErrorCode, INT4 i4FwlIfIfIndex,
                         INT4 i4TestValFwlIfIpOptions)
{
    INT1                i1Status = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;

    i1Status = SNMP_FAILURE;
    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nIP Option TEST Entry\n");

    /* Validate the value of IP option based on Row status .
     * If the RowStatus is FWL_ACTIVE then return SUCCESS, else FAILURE.
     */
    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Trying to SET IpOption Value %d for Interface %d \n",
                  i4TestValFwlIfIpOptions, i4FwlIfIfIndex);
    if ((i4TestValFwlIfIpOptions < FWL_SOURCE_ROUTE_OPTION) ||
        (i4TestValFwlIfIpOptions > FWL_TRACE_ROUTE_OPTION))
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Ip Option Value ranges from 1 to 6\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4FwlIfIfIndex >= FWL_ZERO) && (i4FwlIfIfIndex < FWL_MAX_NUM_OF_IF))
    {
        pIfaceNode = FwlDbaseSearchIface ((UINT4) i4FwlIfIfIndex);
        if (pIfaceNode != NULL)
        {
            if (pIfaceNode->u1RowStatus == FWL_NOT_IN_SERVICE)
            {
                i1Status = SNMP_SUCCESS;
            }
        }
        else
        {
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n If Index %d doesnt exists \n", i4FwlIfIfIndex);
        }
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Invalid Interface Number. It ranges from 1 to %d\n",
                      FWL_MAX_NUM_OF_IF - FWL_ONE);
    }
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\nIP Option TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlIfFragments
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                testValFwlIfFragments
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlIfFragments (UINT4 *pu4ErrorCode, INT4 i4FwlIfIfIndex,
                         INT4 i4TestValFwlIfFragments)
{
    INT1                i1Status = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;

    i1Status = SNMP_FAILURE;
    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nFragment TEST Entry\n");

    /* Validate the value of Fragment based on Row status. 
     * If the RowStatus is FWL_ACTIVE then return SUCCESS, else FAILURE.
     */
    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Trying to SET Fragment Value %d for Interface %d \n",
                  i4TestValFwlIfFragments, i4FwlIfIfIndex);
    if ((i4TestValFwlIfFragments < FWL_TINY_FRAGMENT) ||
        (i4TestValFwlIfFragments > FWL_NO_FRAGMENT))
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Fragment Value range should be 1 to 3\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4FwlIfIfIndex >= FWL_ZERO) && (i4FwlIfIfIndex < FWL_MAX_NUM_OF_IF))
    {
        pIfaceNode = FwlDbaseSearchIface ((UINT4) i4FwlIfIfIndex);
        if (pIfaceNode != NULL)
        {
            if (pIfaceNode->u1RowStatus == FWL_NOT_IN_SERVICE)
            {
                i1Status = SNMP_SUCCESS;
            }
        }
        else
        {
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n If Index %d doesnt exists \n", i4FwlIfIfIndex);
        }
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Invalid Interface Number. It ranges from 1 to %d\n",
                      FWL_MAX_NUM_OF_IF - FWL_ONE);
    }
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\nFragment TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlIfFragmentSize
 Input       :  The Indices
                FwlIfIfIndex

                The Object
                testValFwlIfFragmentSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.                                                     Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlIfFragmentSize (UINT4 *pu4ErrorCode, INT4 i4FwlIfIfIndex,
                            UINT4 u4TestValFwlIfFragmentSize)
{
    INT1                i1Status = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;

    i1Status = SNMP_FAILURE;
    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nFragment TEST Entry\n");

    /* Validate the value of Fragment based on Row status.
     * If the RowStatus is FWL_ACTIVE then return SUCCESS, else FAILURE.
     */
    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Trying to SET Fragment Value %d for Interface %d \n",
                  u4TestValFwlIfFragmentSize, i4FwlIfIfIndex);
    if ((i4FwlIfIfIndex >= FWL_ZERO) && (i4FwlIfIfIndex < FWL_MAX_NUM_OF_IF))
    {
        pIfaceNode = FwlDbaseSearchIface ((UINT4) i4FwlIfIfIndex);
        if (pIfaceNode != NULL)
        {
            if (pIfaceNode->u1RowStatus == FWL_NOT_IN_SERVICE)
            {
                if ((u4TestValFwlIfFragmentSize >= FWL_MIN_FRAGMENT_SIZE)
                    && (u4TestValFwlIfFragmentSize <= FWL_MAX_FRAGMENT_SIZE))
                {
                    i1Status = SNMP_SUCCESS;
                }
                else
                {
                    MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                             "\n Fragment Value ranges from 1 to 3\n");
                }
            }
        }
        else
        {
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n If Index %d doesnt exists \n", i4FwlIfIfIndex);
        }
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Invalid Interface Number. It ranges from 1 to %d\n",
                      FWL_MAX_NUM_OF_IF - FWL_ONE);
    }
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\nFragment Size TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlIfICMPType
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                testValFwlIfICMPType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlIfICMPType (UINT4 *pu4ErrorCode, INT4 i4FwlIfIfIndex,
                        INT4 i4TestValFwlIfICMPType)
{
    INT1                i1Status = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;

    i1Status = SNMP_FAILURE;
    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nICMP Type TEST Entry\n");

    /* Validate the value of ICMP type based on the Row status.
     * If the RowStatus is FWL_ACTIVE then return SUCCESS, else FAILURE.
     */
    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Trying to SET ICMP Type Value %d for Interface %d \n",
                  i4TestValFwlIfICMPType, i4FwlIfIfIndex);
    if ((i4FwlIfIfIndex >= FWL_ZERO) && (i4FwlIfIfIndex < FWL_MAX_NUM_OF_IF))
    {
        pIfaceNode = FwlDbaseSearchIface ((UINT4) i4FwlIfIfIndex);
        if (pIfaceNode != NULL)
        {
            if (pIfaceNode->u1RowStatus == FWL_NOT_IN_SERVICE)
            {
                i1Status = FwlValidateIcmpType (i4TestValFwlIfICMPType);
            }
        }
        else
        {
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n If Index %d doesnt exists \n", i4FwlIfIfIndex);
        }
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Invalid Interface Number. It ranges from 1 to %d\n",
                      FWL_MAX_NUM_OF_IF - FWL_ONE);
    }
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\nICMP Type TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlIfICMPCode
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                testValFwlIfICMPCode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlIfICMPCode (UINT4 *pu4ErrorCode, INT4 i4FwlIfIfIndex,
                        INT4 i4TestValFwlIfICMPCode)
{
    INT1                i1Status = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;

    i1Status = SNMP_FAILURE;
    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nICMP Code TEST Entry\n");

    /* Validate the value of ICMP Code based on Row status. 
     * If the RowStatus is FWL_ACTIVE then return SUCCESS, else FAILURE.
     */
    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Trying to SET ICMP Code Value %d for Interface %d \n",
                  i4TestValFwlIfICMPCode, i4FwlIfIfIndex);
    if ((i4FwlIfIfIndex >= FWL_ZERO) && (i4FwlIfIfIndex < FWL_MAX_NUM_OF_IF))
    {
        pIfaceNode = FwlDbaseSearchIface ((UINT4) i4FwlIfIfIndex);
        if (pIfaceNode != NULL)
        {
            if (pIfaceNode->u1RowStatus == FWL_NOT_IN_SERVICE)
            {
                i1Status = FwlValidateIcmpCode (i4TestValFwlIfICMPCode);
            }
        }
        else
        {
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n If Index %d doesnt exists \n", i4FwlIfIfIndex);
        }
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Invalid Interface Number. It ranges from 1 to %d\n",
                      FWL_MAX_NUM_OF_IF - FWL_ONE);
    }
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\nICMP Code TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlIfICMPv6MsgType 
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                testValFwlIfICMPv6MsgType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlIfICMPv6MsgType (UINT4 *pu4ErrorCode, INT4 i4FwlIfIfIndex,
                             INT4 i4TestValFwlIfICMPv6MsgType)
{
    INT1                i1Status = SNMP_FAILURE;
    tIfaceInfo         *pIfaceNode = NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nICMPv6 Msg Type TEST Entry\n");

    /* Validate the value of ICMPv6 Msg type based on the Row status.
     * If the RowStatus is FWL_ACTIVE then return SUCCESS, else FAILURE.
     */
    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Trying to SET ICMPv6 Msg Type Value %d for Interface %d \n",
                  i4TestValFwlIfICMPv6MsgType, i4FwlIfIfIndex);
    if ((i4FwlIfIfIndex >= FWL_ZERO) && (i4FwlIfIfIndex < FWL_MAX_NUM_OF_IF))
    {
        pIfaceNode = FwlDbaseSearchIface ((UINT4) i4FwlIfIfIndex);
        if (pIfaceNode != NULL)
        {
            if (pIfaceNode->u1RowStatus == FWL_NOT_IN_SERVICE)
            {
                if ((i4TestValFwlIfICMPv6MsgType >= FWL_ICMPV6_NO_INSPECT) &&
                    (i4TestValFwlIfICMPv6MsgType <= FWL_ICMPV6_INSPECT_ALL))
                {
                    i1Status = SNMP_SUCCESS;
                }
            }
        }
        else
        {
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n If Index %d doesnt exists \n", i4FwlIfIfIndex);
        }
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Invalid Interface Number. It ranges from 1 to %d\n",
                      FWL_MAX_NUM_OF_IF - FWL_ONE);
    }
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\nICMPv6 Msg Type TEST Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FwlIfRowStatus
 Input       :  The Indices
                FwlIfIfIndex

                The Object 
                testValFwlIfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlIfRowStatus (UINT4 *pu4ErrorCode, INT4 i4FwlIfIfIndex,
                         INT4 i4TestValFwlIfRowStatus)
{
    INT4                i4Status = FWL_ZERO;
    UINT4               u4IfaceNum = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;

    u4IfaceNum = (UINT4) i4FwlIfIfIndex;
    if ((i4FwlIfIfIndex < FWL_ZERO) || (i4FwlIfIfIndex >= FWL_MAX_NUM_OF_IF))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i4Status = SNMP_FAILURE;
    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nIf RowStatus TEST Entry\n");

    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                  "\n Trying to SET RowStatus Value %d for Interface %d \n",
                  i4TestValFwlIfRowStatus, u4IfaceNum);
    /* Validate the value of Row status.  */
    if (u4IfaceNum < FWL_MAX_NUM_OF_IF)
    {
        pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
        switch (i4TestValFwlIfRowStatus)
        {
            case FWL_CREATE_AND_WAIT:
            case FWL_CREATE_AND_GO:
                if (pIfaceNode == NULL)
                {
                    /* CHANGE2: While creating the interface Index, it must ensure that 
                     * interface index exists in CFA. Else it should not create 
                     * the row. 
                     */
                    i4Status = SecUtilValidateIfIndex ((UINT2) u4IfaceNum);
                    if (i4Status == OSIX_SUCCESS)
                    {
                        i4Status = SNMP_SUCCESS;
                    }
                    else
                    {
                        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                      "\n If Index %d doesnt exists \n",
                                      u4IfaceNum);
                    }
                }
                break;
            case FWL_NOT_IN_SERVICE:
                if (pIfaceNode != NULL)
                {
                    i4Status = SNMP_SUCCESS;
                }
                else
                {
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\n If Index %d doesnt exists \n",
                                  u4IfaceNum);
                }
                break;
            case FWL_ACTIVE:
                if (pIfaceNode != NULL)
                {
                    i4Status = SNMP_SUCCESS;
                }
                else
                {
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\n If Index %d doesnt exists \n",
                                  u4IfaceNum);
                }
                break;
            case FWL_DESTROY:
                if (pIfaceNode != NULL)
                {
                    if ((TMO_SLL_Count (&pIfaceNode->inFilterList)
                         == FWL_DEFAULT_COUNT)
                        && (TMO_SLL_Count (&pIfaceNode->outFilterList)
                            == FWL_DEFAULT_COUNT) &&
                        (TMO_SLL_Count (&pIfaceNode->inIPv6FilterList)
                         == FWL_DEFAULT_COUNT) &&
                        (TMO_SLL_Count (&pIfaceNode->outIPv6FilterList)
                         == FWL_DEFAULT_COUNT))
                    {
                        i4Status = SNMP_SUCCESS;
                    }
                }
                else
                {
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\n If Index %d doesnt exists \n",
                                  u4IfaceNum);
                }
                break;
            default:
                i4Status = SNMP_FAILURE;
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\n Row Status Value ranges from 1,2,3,4,6\n");
                break;
        }                        /*end of switch */
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Invalid Interface Number. It ranges from 1 to %d\n",
                      FWL_MAX_NUM_OF_IF - FWL_ONE);
    }
    if (i4Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\nIf RowStatus TEST Exit\n");

    return (INT1) i4Status;
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2FwlDefnIfTable
 Input       :  The Indices
                FwlIfIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)                                              SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDefnIfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FwlDefnDmzTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlDefnDmzTable
 Input       :  The Indices
                FwlDmzIpIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFwlDefnDmzTable (UINT4 u4FwlDmzIpIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    tFwlDmzInfo        *pFwlDmzNode = NULL;
    FWL_DBG (FWL_DBG_ENTRY, "\n Validate Index for Dmz Table - Entry\n");
    /* Check Whether the Dmz exists or not */

    pFwlDmzNode = FwlSearchDmzNode (u4FwlDmzIpIndex);
    if (pFwlDmzNode != NULL)
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\nDmz  %d doesnt exist \n", u4FwlDmzIpIndex);
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Validate Index for DMZ Table - Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFwlDefnDmzTable
 Input       :  The Indices
                FwlDmzIpIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFwlDefnDmzTable (UINT4 *pu4FwlDmzIpIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    tFwlDmzInfo        *pFwlDmzNode = NULL;

    pFwlDmzNode = (tFwlDmzInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Get First Index for DMZ Table - Entry\n");
    /* get the first Dmz node in the  list */
    if (TMO_SLL_Count (&gFwlDmzList) != FWL_DEFAULT_COUNT)
    {

        pFwlDmzNode = (tFwlDmzInfo *) TMO_SLL_First (&gFwlDmzList);
        *pu4FwlDmzIpIndex = pFwlDmzNode->u4IpIndex;
        i1Status = SNMP_SUCCESS;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Get First Index for DMZ Table - Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFwlDefnDmzTable
 Input       :  The Indices
                FwlDmzIpIndex
                nextFwlDmzIpIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFwlDefnDmzTable (UINT4 u4FwlDmzIpIndex,
                                UINT4 *pu4NextFwlDmzIpIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    tFwlDmzInfo        *pDmzNode = NULL;
    tFwlDmzInfo        *pDmzNextNode = NULL;

    pDmzNode = (tFwlDmzInfo *) NULL;
    pDmzNextNode = (tFwlDmzInfo *) NULL;
    FWL_DBG (FWL_DBG_ENTRY, "\n Get Next Index for DMZ Table - Entry\n");
    pDmzNode = FwlSearchDmzNode (u4FwlDmzIpIndex);
    if (pDmzNode != NULL)
    {
        pDmzNextNode = (tFwlDmzInfo *)
            TMO_SLL_Next (&gFwlDmzList, (tTMO_SLL_NODE *) pDmzNode);

        if (pDmzNextNode != NULL)
        {
            *pu4NextFwlDmzIpIndex = pDmzNextNode->u4IpIndex;
            i1Status = SNMP_SUCCESS;

        }
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Get Next Index for DMZ Table - Exit\n");

    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlDmzRowStatus
 Input       :  The Indices
                FwlDmzIpIndex

                The Object 
                retValFwlDmzRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlDmzRowStatus (UINT4 u4FwlDmzIpIndex, INT4 *pi4RetValFwlDmzRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tFwlDmzInfo        *pDmzNode = NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Row Status GET Entry\n");
    /* search the Dmz address in the List */
    pDmzNode = FwlSearchDmzNode (u4FwlDmzIpIndex);
    if (pDmzNode != NULL)
    {
        /* get the row Status value */
        *pi4RetValFwlDmzRowStatus = (INT4) pDmzNode->u1RowStatus;
        i1Status = SNMP_SUCCESS;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n DMz  RowStatus GET Exit\n");

    return i1Status;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlDmzRowStatus
 Input       :  The Indices
                FwlDmzIpIndex

                The Object 
                setValFwlDmzRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlDmzRowStatus (UINT4 u4FwlDmzIpIndex, INT4 i4SetValFwlDmzRowStatus)
{

    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Status = FWL_FAILURE;
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tFwlDmzInfo        *pDmzNode = NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n DMZ Row Status SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    /* Search the Ip address in the Dmz List */
    pDmzNode = FwlSearchDmzNode (u4FwlDmzIpIndex);

    if ((NULL != pDmzNode) &&
        (pDmzNode->u1RowStatus == i4SetValFwlDmzRowStatus))
    {
        return SNMP_SUCCESS;
    }

    /* create the DMZ entry by setting the row status. If DMZ entry 
     * is already created then the Row Status is ACTIVE. If the DMZ entry
     * doesnt exist then it should be created by setting the Row Status to 
     * CREATE_AND_GO.
     */
    switch (i4SetValFwlDmzRowStatus)
    {
        case FWL_CREATE_AND_WAIT:
        case FWL_CREATE_AND_GO:
            if (TMO_SLL_Count (&gFwlDmzList) == FWL_MAX_DMZ_ENTRIES)
            {
                i1Status = SNMP_FAILURE;
                break;
            }

            if (pDmzNode != NULL)
            {
                i1Status = SNMP_FAILURE;
                break;
            }

            /* If the index doesnt exist already, then it can be created. */
            /*Memory Allocation and Initialisation of Filter Node */

            if (FwlDmzMemAllocate ((UINT1 **) (VOID *) &pDmzNode) ==
                FWL_SUCCESS)
            {
                pDmzNode->u4IpIndex = u4FwlDmzIpIndex;
                pDmzNode->u1RowStatus = FWL_NOT_READY;
                /* Add Dmz Node to DmzList */
                FwlAddDmzNode (pDmzNode);

                i1Status = SNMP_SUCCESS;
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n DMZ Filter %d is created\n", u4FwlDmzIpIndex);
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n RowStatus Value is made Active for DMZ %d \n",
                              u4FwlDmzIpIndex);
            }
            else
            {
                /* Send a Trap for Memory failure */
                FwlGenerateMemFailureTrap (FWL_STATIC_FILTER_ALLOC_FAILURE);
                INC_MEM_FAILURE_COUNT;
                gFwlAclInfo.i4MemStatus = (INT4) MEM_FAILURE;
            }
            break;
        case FWL_NOT_IN_SERVICE:
        case FWL_ACTIVE:
            /* Setting the row status as NOT_IN_SERVICE or ACTIVE */
            if (pDmzNode != NULL)
            {
                pDmzNode->u1RowStatus = (UINT1) i4SetValFwlDmzRowStatus;
                i1Status = SNMP_SUCCESS;
            }
            break;
        case FWL_DESTROY:
            /* Search the Node in the Dmz List and Delete it */
            u4Status = FwlDeleteDmzNode (u4FwlDmzIpIndex);
            if (u4Status == FWL_SUCCESS)
            {
                i1Status = SNMP_SUCCESS;
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n DMZ entry %d is deleted\n", u4FwlDmzIpIndex);
            }
            break;
        default:
            i1Status = SNMP_FAILURE;
            break;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n DMZ Row Status SET Exit \n");
    if (i1Status == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlDmzRowStatus, u4SeqNum, TRUE,
                              FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%p %i", u4FwlDmzIpIndex,
                          i4SetValFwlDmzRowStatus));
    }
    return i1Status;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlDmzRowStatus
 Input       :  The Indices
                FwlDmzIpIndex

                The Object 
                testValFwlDmzRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlDmzRowStatus (UINT4 *pu4ErrorCode, UINT4 u4FwlDmzIpIndex,
                          INT4 i4TestValFwlDmzRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tFwlDmzInfo        *pDmzNode = NULL;

    pDmzNode = (tFwlDmzInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nDmz RowStatus TEST routine Entry\n");

    /* Test the Index for valid Unicast IP */
    if (FwlValidateIpAddress (u4FwlDmzIpIndex) != FWL_UCAST_ADDR)
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Invalid Unicast IP Address %d given\n",
                      u4FwlDmzIpIndex);
        CLI_SET_ERR (CLI_FWL_INVALID_UCAST_ADDR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pDmzNode = FwlSearchDmzNode (u4FwlDmzIpIndex);
    if ((NULL != pDmzNode) &&
        (pDmzNode->u1RowStatus == i4TestValFwlDmzRowStatus))
    {
        return SNMP_SUCCESS;
    }
    switch (i4TestValFwlDmzRowStatus)
    {
        case FWL_CREATE_AND_WAIT:
        case FWL_CREATE_AND_GO:
            if (TMO_SLL_Count (&gFwlDmzList) == FWL_MAX_DMZ_ENTRIES)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            /* If the index doesnt exist already, then it can be created. */
            if (pDmzNode == NULL)
            {
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n  DMZ %d already exists\n", u4FwlDmzIpIndex);
            }
            break;
        case FWL_NOT_IN_SERVICE:
            if (pDmzNode != NULL)
            {
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n DMZ  %d doesnt exists \n", u4FwlDmzIpIndex);
            }
            break;
        case FWL_ACTIVE:
        case FWL_DESTROY:
            /* The row can be activated or deleted if it exists. */
            if (pDmzNode != NULL)
            {
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n DMZ  %d doesnt exists \n", u4FwlDmzIpIndex);
            }
            break;
        default:
            i1Status = SNMP_FAILURE;
            MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                     "\n Row Status Value not supported\n");
            break;
    }                            /* end of Switch */
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n DMZ  RowStatus TEST routine Exit\n");
    return i1Status;

}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2FwlDefnDmzTable
 Input       :  The Indices
                FwlDmzIpIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.                                                             Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDefnDmzTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FwlDefnIPv6DmzTable. */

/****************************************************************************
 *  Function    :  nmhValidateIndexInstanceFwlDefnIPv6DmzTable
 *  Input       :  The Indices
 *                 FwlDmzIpv6Index
 *  Output      :  The Routines Validates the Given Indices.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFwlDefnIPv6DmzTable
    (tSNMP_OCTET_STRING_TYPE * pFwlDmzIpv6Index)
{
    INT1                i1Status = SNMP_FAILURE;
    tFwlIPv6DmzInfo    *pFwlDmzNode = NULL;
    tIp6Addr            DmzIp6Host;

    FWL_DBG (FWL_DBG_ENTRY, "\n Validate Index for IPv6 Dmz Table - Entry\n");

    MEMSET (&DmzIp6Host, FWL_ZERO, sizeof (tIp6Addr));

    Ip6AddrCopy (&DmzIp6Host,
                 (tIp6Addr *) (void *) pFwlDmzIpv6Index->pu1_OctetList);

    /* Check Whether the Dmz exists or not */

    pFwlDmzNode = FwlSearchIPv6DmzNode (DmzIp6Host);
    if (pFwlDmzNode != NULL)
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\nDmz  %s doesnt exist \n", Ip6PrintNtop (&DmzIp6Host));
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Validate Index for IPv6 DMZ Table - Exit\n");

    return i1Status;

}

/****************************************************************************
 *  Function    :  nmhGetFirstIndexFwlDefnIPv6DmzTable
 *  Input       :  The Indices
 *                 FwlDmzIpv6Index
 *  Output      :  The Get First Routines gets the Lexicographicaly
 *                 First Entry from the Table.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFwlDefnIPv6DmzTable (tSNMP_OCTET_STRING_TYPE * pFwlDmzIpv6Index)
{
    INT1                i1Status = SNMP_FAILURE;
    tFwlIPv6DmzInfo    *pFwlDmzNode = NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Get First Index for IPv6 DMZ Table - Entry\n");
    /* get the first Dmz node in the  list */
    if (TMO_SLL_Count (&gFwlIPv6DmzList) != FWL_DEFAULT_COUNT)
    {

        pFwlDmzNode = (tFwlIPv6DmzInfo *) TMO_SLL_First (&gFwlIPv6DmzList);
        pFwlDmzIpv6Index->i4_Length = FWL_IPV6_ADDR_LEN;
        Ip6AddrCopy ((tIp6Addr *) (void *) pFwlDmzIpv6Index->pu1_OctetList,
                     &pFwlDmzNode->dmzIpv6Index);
        i1Status = SNMP_SUCCESS;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Get First Index for IPv6 DMZ Table - Exit\n");

    return i1Status;

}

/****************************************************************************
 *  Function    :  nmhGetNextIndexFwlDefnIPv6DmzTable
 *  Input       :  The Indices
 *                 FwlDmzIpv6Index
 *                 nextFwlDmzIpv6Index
 *  Output      :  The Get Next function gets the Next Index for
 *                 the Index Value given in the Index Values. The
 *                 Indices are stored in the next_varname variables.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFwlDefnIPv6DmzTable
    (tSNMP_OCTET_STRING_TYPE * pFwlDmzIpv6Index,
     tSNMP_OCTET_STRING_TYPE * pNextFwlDmzIpv6Index)
{
    INT1                i1Status = SNMP_FAILURE;
    tFwlIPv6DmzInfo    *pDmzNode = NULL;
    tFwlIPv6DmzInfo    *pDmzNextNode = NULL;
    tIp6Addr            DmzIp6Host;

    MEMSET (&DmzIp6Host, FWL_ZERO, sizeof (tIp6Addr));
    FWL_DBG (FWL_DBG_ENTRY, "\n Get Next Index for IPv6 DMZ Table - Entry\n");
    Ip6AddrCopy (&DmzIp6Host,
                 (tIp6Addr *) (void *) pFwlDmzIpv6Index->pu1_OctetList);
    pDmzNode = FwlSearchIPv6DmzNode (DmzIp6Host);
    if (pDmzNode != NULL)
    {
        pDmzNextNode = (tFwlIPv6DmzInfo *)
            TMO_SLL_Next (&gFwlIPv6DmzList, (tTMO_SLL_NODE *) pDmzNode);

        if (pDmzNextNode != NULL)
        {
            pNextFwlDmzIpv6Index->i4_Length = FWL_IPV6_ADDR_LEN;
            Ip6AddrCopy ((tIp6Addr *) (void *) pNextFwlDmzIpv6Index->
                         pu1_OctetList, &pDmzNextNode->dmzIpv6Index);

            i1Status = SNMP_SUCCESS;

        }
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Get Next Index for IPv6 DMZ Table - Exit\n");

    return i1Status;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 *  Function    :  nmhGetFwlDmzAddressType
 *  Input       :  The Indices
 *                 FwlDmzIpv6Index
 *                 The Object
 *                 retValFwlDmzAddressType
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1                nmhGetFwlDmzAddressType
    (tSNMP_OCTET_STRING_TYPE * pFwlDmzIpv6Index,
     INT4 *pi4RetValFwlDmzAddressType)
{
    INT1                i1Status = SNMP_FAILURE;
    tIp6Addr            DmzIp6Host;
    tFwlIPv6DmzInfo    *pDmzNode = NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Address Type Get Entry");
    MEMSET (&DmzIp6Host, FWL_ZERO, sizeof (tIp6Addr));
    Ip6AddrCopy (&DmzIp6Host,
                 (tIp6Addr *) (void *) pFwlDmzIpv6Index->pu1_OctetList);

    pDmzNode = FwlSearchIPv6DmzNode (DmzIp6Host);
    if (pDmzNode != NULL)
    {
        pi4RetValFwlDmzAddressType =
            (INT4 *) (void *) (&(pDmzNode->u1DmzAddressType));
        i1Status = SNMP_SUCCESS;
    }
    FWL_DBG (FWL_DBG_EXIT, "\n Address Type Get Exit");
    UNUSED_PARAM (*pi4RetValFwlDmzAddressType);
    return i1Status;
}

/****************************************************************************
 *  Function    :  nmhGetFwlDmzIpv6RowStatus
 *  Input       :  The Indices
 *                 FwlDmzIpv6Index
 *                 The Object
 *                 retValFwlDmzIpv6RowStatus
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1                nmhGetFwlDmzIpv6RowStatus
    (tSNMP_OCTET_STRING_TYPE * pFwlDmzIpv6Index,
     INT4 *pi4RetValFwlDmzIpv6RowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tFwlIPv6DmzInfo    *pDmzNode = NULL;
    tIp6Addr            DmzIp6Host;

    FWL_DBG (FWL_DBG_ENTRY, "\n Row Status GET Entry\n");

    MEMSET (&DmzIp6Host, FWL_ZERO, sizeof (tIp6Addr));

    Ip6AddrCopy (&DmzIp6Host,
                 (tIp6Addr *) (void *) pFwlDmzIpv6Index->pu1_OctetList);
    /* search the Dmz address in the List */
    pDmzNode = FwlSearchIPv6DmzNode (DmzIp6Host);
    if (pDmzNode != NULL)
    {
        /* get the row Status value */
        *pi4RetValFwlDmzIpv6RowStatus = (INT4) pDmzNode->u1RowStatus;
        i1Status = SNMP_SUCCESS;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n DMZ  RowStatus GET Exit\n");

    return i1Status;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 *  Function    :  nmhSetFwlDmzAddressType
 *  Input       :  The Indices
 *                 FwlDmzIpv6Index
 *                 The Object
 *                 setValFwlDmzAddressType
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ***************************************************************************/
INT1                nmhSetFwlDmzAddressType
    (tSNMP_OCTET_STRING_TYPE * pFwlDmzIpv6Index, INT4 i4SetValFwlDmzAddressType)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    INT1                i1Status = SNMP_FAILURE;
    tFwlIPv6DmzInfo    *pDmzNode = NULL;

    tIp6Addr            DmzIp6Host;
    FWL_DBG (FWL_DBG_ENTRY, "\n Address Type Set Entry");
    RM_GET_SEQ_NUM (&u4SeqNum);
    MEMSET (&DmzIp6Host, FWL_ZERO, sizeof (tIp6Addr));

    Ip6AddrCopy (&DmzIp6Host,
                 (tIp6Addr *) (void *) pFwlDmzIpv6Index->pu1_OctetList);
    pDmzNode = FwlSearchIPv6DmzNode (DmzIp6Host);
    if (pDmzNode != NULL)
    {
        pDmzNode->u1DmzAddressType = (UINT1) i4SetValFwlDmzAddressType;
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlDmzAddressType, u4SeqNum,
                              FALSE, FwlLock, FwlUnLock, FWL_ONE, i1Status);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFwlDmzIpv6Index,
                          i4SetValFwlDmzAddressType));
        i1Status = SNMP_SUCCESS;
    }
    FWL_DBG (FWL_DBG_EXIT, "\n Address Type set exit");
    if (i1Status == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlDmzAddressType, u4SeqNum,
                              FALSE, FwlLock, FwlUnLock, FWL_ONE, i1Status);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFwlDmzIpv6Index,
                          i4SetValFwlDmzAddressType));
    }
    return i1Status;
}

/****************************************************************************
 *  Function    :  nmhSetFwlDmzIpv6RowStatus
 *  Input       :  The Indices
 *                 FwlDmzIpv6Index
 *                 The Object
 *                 setValFwlDmzIpv6RowStatus
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *  **************************************************************************/
INT1                nmhSetFwlDmzIpv6RowStatus
    (tSNMP_OCTET_STRING_TYPE * pFwlDmzIpv6Index,
     INT4 i4SetValFwlDmzIpv6RowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Status = FWL_FAILURE;
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tFwlIPv6DmzInfo    *pDmzNode = NULL;
    tIp6Addr            DmzIp6Host;

    FWL_DBG (FWL_DBG_ENTRY, "\n DMZ Row Status SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    MEMSET (&DmzIp6Host, FWL_ZERO, sizeof (tIp6Addr));
    Ip6AddrCopy (&DmzIp6Host,
                 (tIp6Addr *) (void *) pFwlDmzIpv6Index->pu1_OctetList);

    /* Search the Ipv6 address in the Dmz List */
    pDmzNode = FwlSearchIPv6DmzNode (DmzIp6Host);

    if ((NULL != pDmzNode) &&
        (pDmzNode->u1RowStatus == i4SetValFwlDmzIpv6RowStatus))
    {
        return SNMP_SUCCESS;
    }

    /* create the IPv6 DMZ entry by setting the row status. If IPv6DMZ entry 
     * is already created then the Row Status is ACTIVE. If the DMZ entry
     * doesnt exist then it should be created by setting the Row Status to 
     * CREATE_AND_GO.
     */
    switch (i4SetValFwlDmzIpv6RowStatus)
    {
        case FWL_CREATE_AND_WAIT:
        case FWL_CREATE_AND_GO:
            if (TMO_SLL_Count (&gFwlIPv6DmzList) == FWL_MAX_IPV6_DMZ_ENTRIES)
            {
                i1Status = SNMP_FAILURE;
                break;
            }

            if (pDmzNode != NULL)
            {
                i1Status = SNMP_FAILURE;
                break;
            }

            /* If the index doesnt exist already, then it can be created. */
            /*Memory Allocation and Initialisation of Filter Node */

            if (FwlIPv6DmzMemAllocate ((UINT1 **) (VOID *) &pDmzNode) ==
                FWL_SUCCESS)
            {
                Ip6AddrCopy (&pDmzNode->dmzIpv6Index, &DmzIp6Host);
                pDmzNode->u1RowStatus = FWL_NOT_READY;
                /* Add Dmz Node to DmzList */
                FwlAddIPv6DmzNode (pDmzNode);

                i1Status = SNMP_SUCCESS;
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n DMZ ipv6 Filter %s is created\n",
                              Ip6PrintNtop (&DmzIp6Host));
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n RowStatus Value is made Active for ipv6 DMZ %s \n",
                              Ip6PrintNtop (&DmzIp6Host));
            }
            else
            {
                /* Send a Trap for Memory failure */
                FwlGenerateMemFailureTrap (FWL_STATIC_FILTER_ALLOC_FAILURE);
                INC_MEM_FAILURE_COUNT;
                gFwlAclInfo.i4MemStatus = (INT4) MEM_FAILURE;
            }
            break;
        case FWL_NOT_IN_SERVICE:
        case FWL_ACTIVE:
            /* Setting the row status as NOT_IN_SERVICE or ACTIVE */
            if (pDmzNode != NULL)
            {
                pDmzNode->u1RowStatus = (UINT1) i4SetValFwlDmzIpv6RowStatus;
                i1Status = SNMP_SUCCESS;
            }
            break;
        case FWL_DESTROY:
            /* Search the Node in the Dmz List and Delete it */
            u4Status = FwlDeleteIPv6DmzNode (DmzIp6Host);
            if (u4Status == FWL_SUCCESS)
            {
                i1Status = SNMP_SUCCESS;
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n IPv6 DMZ entry %s is deleted\n",
                              Ip6PrintNtop (&DmzIp6Host));
            }
            break;
        default:
            i1Status = SNMP_FAILURE;
            break;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n DMZ Row Status SET Exit \n");
    if (i1Status == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlDmzIpv6RowStatus, u4SeqNum,
                              TRUE, FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFwlDmzIpv6Index,
                          i4SetValFwlDmzIpv6RowStatus));
    }
    return i1Status;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 *  Function    :  nmhTestv2FwlDmzAddressType
 *  Input       :  The Indices
 *                 FwlDmzIpv6Index
 *                 The Object
 *                 testValFwlDmzAddressType
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                 Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val
 *  Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *  **************************************************************************/
INT1                nmhTestv2FwlDmzAddressType
    (UINT4 *pu4ErrorCode,
     tSNMP_OCTET_STRING_TYPE * pFwlDmzIpv6Index,
     INT4 i4TestValFwlDmzAddressType)
{
    tIp6Addr            DmzIp6Host;
    tFwlIPv6DmzInfo    *pDmzNode = NULL;
    INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Address Type TEST Entry\n");

    MEMSET (&DmzIp6Host, FWL_ZERO, sizeof (tIp6Addr));
    Ip6AddrCopy (&DmzIp6Host,
                 (tIp6Addr *) (void *) pFwlDmzIpv6Index->pu1_OctetList);
    pDmzNode = FwlSearchIPv6DmzNode (DmzIp6Host);
    if (pDmzNode != NULL)
    {
        /* Validate the value of Address Type */
        if ((i4TestValFwlDmzAddressType == FWL_IP_VERSION_4)
            || (i4TestValFwlDmzAddressType == FWL_IP_VERSION_6))
        {
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                     "\n Address Type can be can be IPv4 or IPv6\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
    }
    FWL_DBG (FWL_DBG_ENTRY, "\nAddress Type TEST Exit\n");

    return i1Status;

}

/****************************************************************************
 *  Function    :  nmhTestv2FwlDmzIpv6RowStatus
 *  Input       :  The Indices
 *                 FwlDmzIpv6Index
 *                 The Object
 *                 testValFwlDmzIpv6RowStatus
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val
 *  Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *  ************************************************************************/
INT1                nmhTestv2FwlDmzIpv6RowStatus
    (UINT4 *pu4ErrorCode,
     tSNMP_OCTET_STRING_TYPE * pFwlDmzIpv6Index,
     INT4 i4TestValFwlDmzIpv6RowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4RetVal = FWL_ZERO;
    tFwlIPv6DmzInfo    *pDmzNode = NULL;
    tIp6Addr            DmzIPv6Host;

    FWL_DBG (FWL_DBG_ENTRY, "\nDmz RowStatus TEST routine Entry\n");
    MEMSET (&DmzIPv6Host, FWL_ZERO, sizeof (tIp6Addr));

    Ip6AddrCopy (&DmzIPv6Host,
                 (tIp6Addr *) (void *) pFwlDmzIpv6Index->pu1_OctetList);

    /* Test the Index for valid Unicast IP */
    i4RetVal = Ip6AddrType (&DmzIPv6Host);
    if ((i4RetVal == ADDR6_MULTI) ||
        (i4RetVal == ADDR6_LLOCAL) ||
        (i4RetVal == ADDR6_LOOPBACK) ||
        (i4RetVal == ADDR6_V4_COMPAT) ||
        (i4RetVal == ADDR6_INVALID) || (i4RetVal == ADDR6_UNSPECIFIED))
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Invalid Unicast IPv6 Address %s given\n",
                      Ip6PrintNtop (&DmzIPv6Host));
        CLI_SET_ERR (CLI_FWL_INVALID_UCAST_ADDR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pDmzNode = FwlSearchIPv6DmzNode (DmzIPv6Host);
    if ((NULL != pDmzNode) &&
        (pDmzNode->u1RowStatus == i4TestValFwlDmzIpv6RowStatus))
    {
        return SNMP_SUCCESS;
    }
    switch (i4TestValFwlDmzIpv6RowStatus)
    {
        case FWL_CREATE_AND_WAIT:
        case FWL_CREATE_AND_GO:
            if (TMO_SLL_Count (&gFwlIPv6DmzList) == FWL_MAX_IPV6_DMZ_ENTRIES)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            /* If the index doesnt exist already, then it can be created. */
            if (pDmzNode == NULL)
            {
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n  IPv6 DMZ %s already exists\n",
                              Ip6PrintNtop (&DmzIPv6Host));
            }
            break;
        case FWL_NOT_IN_SERVICE:
            if (pDmzNode != NULL)
            {
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n IPv6 DMZ  %s doesnt exists \n",
                              Ip6PrintNtop (&DmzIPv6Host));
            }
            break;
        case FWL_ACTIVE:
        case FWL_DESTROY:
            /* The row can be activated or deleted if it exists. */
            if (pDmzNode != NULL)
            {
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n IPv6 DMZ  %s doesnt exists \n",
                              Ip6PrintNtop (&DmzIPv6Host));
            }
            break;
        default:
            i1Status = SNMP_FAILURE;
            MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                     "\n Row Status Value not supported\n");
            break;
    }                            /* end of Switch */
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n IPv6 DMZ  RowStatus TEST routine Exit\n");
    return i1Status;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 *  Function    :  nmhDepv2FwlDefnIPv6DmzTable
 *  Input       :  The Indices
 *                 FwlDmzIpv6Index
 *  Output      :  The Dependency Low Lev Routine Take the Indices &
 *                 check whether dependency is met or not.
 *                 Stores the value of error code in the Return val
 *  Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1                nmhDepv2FwlDefnIPv6DmzTable
    (UINT4 *pu4ErrorCode,
     tSnmpIndexList * pSnmpIndexList, tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FwlUrlFilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlUrlFilterTable
 Input       :  The Indices
                FwlUrlString
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFwlUrlFilterTable (tSNMP_OCTET_STRING_TYPE *
                                           pFwlUrlString)
{
    UINT1               au1FilterName[FWL_MAX_URL_FILTER_STRING_LEN] =
        { FWL_ZERO };
    tFwlUrlFilterInfo  *pFilterNode = NULL;
    INT1                i1Status = SNMP_FAILURE;

    pFilterNode = (tFwlUrlFilterInfo *) NULL;
    FWL_DBG (FWL_DBG_ENTRY,
             "\n Validate Index for FwlUrlFilter Table - Entry\n");

    if (pFwlUrlString->i4_Length < FWL_MAX_URL_FILTER_STRING_LEN)
    {
        FWL_MEMCPY (au1FilterName, pFwlUrlString->pu1_OctetList,
                    MEM_MAX_BYTES (pFwlUrlString->i4_Length,
                                   (INT4) sizeof (au1FilterName)));
        au1FilterName[pFwlUrlString->i4_Length] = FWL_END_OF_STRING;

        /* Check whether the filter name exists or not */
        pFilterNode = FwlSearchUrlFilter (au1FilterName);
        if (pFilterNode != NULL)
        {
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\nUrlFilter %s doesnt exist \n", au1FilterName);
        }
        FWL_DBG (FWL_DBG_EXIT,
                 "\n Validate Index for FwlUrlFilter Table - Exit\n");
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFwlUrlFilterTable
 Input       :  The Indices
                FwlUrlString
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFwlUrlFilterTable (tSNMP_OCTET_STRING_TYPE * pFwlUrlString)
{
    INT1                i1Status = SNMP_FAILURE;
    tFwlUrlFilterInfo  *pFilterNode = NULL;

    pFilterNode = (tFwlUrlFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Get First Index for FwlUrlFilter Table - Entry\n");

    /* get the first filter node in the filter list */
    if (TMO_SLL_Count (&FwlUrlFilterList) != FWL_DEFAULT_COUNT)
    {
        pFilterNode = (tFwlUrlFilterInfo *) TMO_SLL_First (&FwlUrlFilterList);
        pFwlUrlString->i4_Length =
            (INT4) FWL_STRLEN ((INT1 *) pFilterNode->au1UrlStr);
        FWL_MEMCPY (pFwlUrlString->pu1_OctetList, pFilterNode->au1UrlStr,
                    pFwlUrlString->i4_Length);
        i1Status = SNMP_SUCCESS;
    }
    FWL_DBG (FWL_DBG_EXIT,
             "\n Get First Index for FwlUrlFilter Table - Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFwlUrlFilterTable
 Input       :  The Indices
                FwlUrlString
                nextFwlUrlString
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFwlUrlFilterTable (tSNMP_OCTET_STRING_TYPE * pFwlUrlString,
                                  tSNMP_OCTET_STRING_TYPE * pNextFwlUrlString)
{
    INT1                i1Status = SNMP_FAILURE;
    tFwlUrlFilterInfo  *pFilterNode = NULL;
    tFwlUrlFilterInfo  *pFilterNextNode = NULL;
    UINT1               au1FilterName[FWL_MAX_URL_FILTER_STRING_LEN] =
        { FWL_ZERO };

    pFilterNode = (tFwlUrlFilterInfo *) NULL;
    pFilterNextNode = (tFwlUrlFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Get Next Index for FwlUrlFilter Table - Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlUrlString->pu1_OctetList,
                MEM_MAX_BYTES (pFwlUrlString->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlUrlString->i4_Length - FWL_ONE] = FWL_END_OF_STRING;

    /* search for the filter name and return the the filter name
     * next to the given filter name
     */
    pFilterNode = FwlSearchUrlFilter (au1FilterName);
    if (pFilterNode != NULL)
    {
        pFilterNextNode = (tFwlUrlFilterInfo *) TMO_SLL_Next (&FwlUrlFilterList,
                                                              (tTMO_SLL_NODE *)
                                                              pFilterNode);

        if (pFilterNextNode != NULL)
        {
            pNextFwlUrlString->i4_Length = (INT4)
                FWL_STRLEN ((INT1 *) pFilterNextNode->au1UrlStr);
            FWL_MEMCPY (pNextFwlUrlString->pu1_OctetList,
                        pFilterNextNode->au1UrlStr,
                        pNextFwlUrlString->i4_Length);
            i1Status = SNMP_SUCCESS;
        }
    }
    FWL_DBG (FWL_DBG_EXIT, "\n Get Next Index for FwlUrlFilter Table - Exit\n");

    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlUrlHitCount
 Input       :  The Indices
                FwlUrlString

                The Object 
                retValFwlUrlHitCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlUrlHitCount (tSNMP_OCTET_STRING_TYPE * pFwlUrlString,
                      UINT4 *pu4RetValFwlUrlHitCount)
{
    tFwlUrlFilterInfo  *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_URL_FILTER_STRING_LEN] =
        { FWL_ZERO };
    INT1                i1Status = SNMP_FAILURE;

    pFilterNode = (tFwlUrlFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nUrl Filter Hit Count GET Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlUrlString->pu1_OctetList,
                MEM_MAX_BYTES (pFwlUrlString->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlUrlString->i4_Length - FWL_ONE] = FWL_END_OF_STRING;

    /* search the filter name in the filter list */
    pFilterNode = FwlSearchUrlFilter (au1FilterName);
    if (pFilterNode != NULL)
    {
        /* get the hit count value */
        *pu4RetValFwlUrlHitCount = pFilterNode->u4UrlHitCount;
        i1Status = SNMP_SUCCESS;
    }
    FWL_DBG (FWL_DBG_EXIT, "\nUrl Filter Hit Count GET Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFwlUrlFilterRowStatus
 Input       :  The Indices
                FwlUrlString

                The Object 
                retValFwlUrlFilterRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlUrlFilterRowStatus (tSNMP_OCTET_STRING_TYPE * pFwlUrlString,
                             INT4 *pi4RetValFwlUrlFilterRowStatus)
{
    tFwlUrlFilterInfo  *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_URL_FILTER_STRING_LEN] =
        { FWL_ZERO };
    INT1                i1Status = SNMP_FAILURE;

    pFilterNode = (tFwlUrlFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nUrl Filter RowStatus GET Entry\n");

    FWL_MEMCPY (au1FilterName, pFwlUrlString->pu1_OctetList,
                MEM_MAX_BYTES (pFwlUrlString->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlUrlString->i4_Length] = FWL_END_OF_STRING;

    /* search the filter name in the filter list */
    pFilterNode = FwlSearchUrlFilter (au1FilterName);
    if (pFilterNode != NULL)
    {
        /* get the row status value  */
        *pi4RetValFwlUrlFilterRowStatus = (INT4) pFilterNode->u1RowStatus;
        i1Status = SNMP_SUCCESS;
    }

    FWL_DBG (FWL_DBG_EXIT, "\nUrl Filter RowStatus GET Exit\n");

    return i1Status;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlUrlFilterRowStatus
 Input       :  The Indices
                FwlUrlString

                The Object 
                setValFwlUrlFilterRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlUrlFilterRowStatus (tSNMP_OCTET_STRING_TYPE * pFwlUrlString,
                             INT4 i4SetValFwlUrlFilterRowStatus)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Status = FWL_FAILURE;
    tFwlUrlFilterInfo  *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_URL_FILTER_STRING_LEN] =
        { FWL_ZERO };

    pFilterNode = (tFwlUrlFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Url Filter Row Status SET Entry \n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    FWL_MEMCPY (au1FilterName, pFwlUrlString->pu1_OctetList,
                MEM_MAX_BYTES (pFwlUrlString->i4_Length,
                               (INT4) sizeof (au1FilterName)));
    au1FilterName[pFwlUrlString->i4_Length] = FWL_END_OF_STRING;

    /* Search the Filter in the Filter List */
    pFilterNode = FwlSearchUrlFilter (au1FilterName);

    if ((NULL != pFilterNode) &&
        (pFilterNode->u1RowStatus == i4SetValFwlUrlFilterRowStatus))
    {
        return SNMP_SUCCESS;
    }

    /* The Filter Node can be created only when the row status is given
     * CREATE_AND_GO. The node also becomes ACTIVE with CREATE_AND_GO.
     * The node will be deleted when the Row Status is DESTROY.
     */
    switch (i4SetValFwlUrlFilterRowStatus)
    {
        case FWL_CREATE_AND_WAIT:
        case FWL_CREATE_AND_GO:
            /* MAX ENTRIES reached then Entry Creation is to be denied */
            if (TMO_SLL_Count (&FwlUrlFilterList) == FWL_MAX_URL_FILTER_ENTRIES)
            {
                i1Status = SNMP_FAILURE;
                break;
            }

            /* Memory Allocation and Initialisation of Filter Node .  */
            if (FwlUrlFilterMemAllocate ((UINT1 **) (VOID *) &pFilterNode) ==
                FWL_SUCCESS)
            {
                FwlSetDefaultUrlFilterValue (au1FilterName, pFilterNode);

                /* Add Url Filter Node to UrlFilterList */
                FwlAddUrlFilter (pFilterNode);
                pFilterNode->u1RowStatus = FWL_NOT_READY;
                i1Status = SNMP_SUCCESS;
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n Url Filter %s is created\n", au1FilterName);
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n RowStatus Value is made Active for UrlFilter %s \n",
                              au1FilterName);
            }
            else
            {
                /*  Send a Trap for Memory faliure */
                FwlGenerateMemFailureTrap (FWL_STATIC_FILTER_ALLOC_FAILURE);
                INC_MEM_FAILURE_COUNT;
                gFwlAclInfo.i4MemStatus = (INT4) MEM_FAILURE;
            }
            break;
        case FWL_NOT_IN_SERVICE:
        case FWL_ACTIVE:
            /* Setting the row status as NOT_IN_SERVICE or ACTIVE */
            if (pFilterNode != NULL)
            {
                pFilterNode->u1RowStatus =
                    (UINT1) i4SetValFwlUrlFilterRowStatus;
                i1Status = SNMP_SUCCESS;
            }
            break;
        case FWL_DESTROY:
            /*  Search the Node in the Filter List and delete it. */
            u4Status = FwlDeleteUrlFilter (au1FilterName);
            if (u4Status == FWL_SUCCESS)
            {
                i1Status = SNMP_SUCCESS;
                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                              "\n Url Filter %s is deleted\n", au1FilterName);
            }
            break;
        default:
            i1Status = SNMP_FAILURE;
            break;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Url Filter Row Status SET Exit \n");
    if (i1Status == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlUrlFilterRowStatus, u4SeqNum,
                              TRUE, FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s",
                          i4SetValFwlUrlFilterRowStatus, pFwlUrlString));
    }
    return i1Status;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlUrlFilterRowStatus
 Input       :  The Indices
                FwlUrlString

                The Object 
                testValFwlUrlFilterRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlUrlFilterRowStatus (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFwlUrlString,
                                INT4 i4TestValFwlUrlFilterRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tFwlUrlFilterInfo  *pFilterNode = NULL;
    UINT1               au1FilterName[FWL_MAX_URL_FILTER_STRING_LEN] =
        { FWL_ZERO };

    pFilterNode = (tFwlUrlFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nUrlFilter RowStatus TEST routine Entry\n");

    /* Validate the Row status value. The Urlfilter name length should be
     * less that of FWL_MAX_URL_FILTER_STRING_LEN.
     */
    if ((pFwlUrlString->i4_Length < FWL_MAX_URL_FILTER_STRING_LEN) &&
        (pFwlUrlString->i4_Length > FWL_ZERO))
    {
        FWL_MEMCPY (au1FilterName, pFwlUrlString->pu1_OctetList,
                    MEM_MAX_BYTES (pFwlUrlString->i4_Length,
                                   (INT4) sizeof (au1FilterName)));
        au1FilterName[pFwlUrlString->i4_Length] = FWL_END_OF_STRING;
        MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\nTrying to Set RowStatus Value %d for Filter %s\n",
                      i4TestValFwlUrlFilterRowStatus, au1FilterName);
        pFilterNode = FwlSearchUrlFilter (au1FilterName);
        if ((NULL != pFilterNode) &&
            (pFilterNode->u1RowStatus == i4TestValFwlUrlFilterRowStatus))
        {
            return SNMP_SUCCESS;
        }
        switch (i4TestValFwlUrlFilterRowStatus)
        {
            case FWL_CREATE_AND_WAIT:
            case FWL_CREATE_AND_GO:
                /* MAX ENTRIES reached then Entry Creation is to be denied */
                if (TMO_SLL_Count (&FwlUrlFilterList) ==
                    FWL_MAX_URL_FILTER_ENTRIES)
                {
                    i1Status = SNMP_FAILURE;
                    break;
                }

                /* If the index doesnt exist already, then it can be created. */
                if (pFilterNode == NULL)
                {
                    i1Status = SNMP_SUCCESS;
                }
                else
                {
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\nUrl Filter %s already exists\n",
                                  au1FilterName);
                }
                break;
            case FWL_NOT_IN_SERVICE:
                if (pFilterNode != NULL)
                {
                    i1Status = SNMP_SUCCESS;
                }
                else
                {
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\nUrl Filter %s doesnt exists \n",
                                  au1FilterName);
                }
                break;
            case FWL_ACTIVE:
            case FWL_DESTROY:
                /* The row can be activated or deleted if it exists. */
                if (pFilterNode != NULL)
                {
                    i1Status = SNMP_SUCCESS;
                }
                else
                {
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\nUrl Filter %s doesnt exists \n",
                                  au1FilterName);
                }
                break;
            default:
                i1Status = SNMP_FAILURE;
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\n Row Status Value not supported\n");
                break;
        }                        /* end of Switch */
        if (i1Status == SNMP_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
    }                            /* end of if */
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\nMaximum Url Filter Name Length is %d\n",
                      FWL_MAX_URL_FILTER_STRING_LEN);
    }

    FWL_DBG (FWL_DBG_EXIT, "\nUrl Filter RowStatus TEST routine Exit\n");
    return i1Status;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlUrlFilterTable
 Input       :  The Indices
                FwlUrlString
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlUrlFilterTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlStatInspectedPacketsCount
 Input       :  The Indices

                The Object 
                retValFwlStatInspectedPacketsCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatInspectedPacketsCount (UINT4
                                    *pu4RetValFwlStatInspectedPacketsCount)
{
    tIdsGetInfo         IdsGetInfor;
    FWL_DBG (FWL_DBG_ENTRY, "\n Inspect Packet Count GET Entry\n");

    MEMSET (&IdsGetInfor, FWL_ZERO, sizeof (tIdsGetInfo));

    /* The number of packets inspected is fetched from IDS */
    SecIdsGetInfo (ISS_IDS_GLB_STATS_REQ, &IdsGetInfor);

    *pu4RetValFwlStatInspectedPacketsCount =
        gFwlAclInfo.u4TotalPktsInspectCount + IdsGetInfor.u4DropStats +
        IdsGetInfor.u4AllowStats;

    FWL_DBG (FWL_DBG_EXIT, "\n Inspect Packet Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatTotalPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatTotalPacketsDenied (UINT4 *pu4RetValFwlStatTotalPacketsDenied)
{
    tIdsGetInfo         IdsGetInfor;
    FWL_DBG (FWL_DBG_ENTRY, "\n Total Packet Deny Count GET Entry\n");

    MEMSET (&IdsGetInfor, FWL_ZERO, sizeof (tIdsGetInfo));

    /* The number of packets dropped is fetched from the IDS */
    SecIdsGetInfo (ISS_IDS_GLB_STATS_REQ, &IdsGetInfor);

    *pu4RetValFwlStatTotalPacketsDenied = gFwlAclInfo.u4TotalPktsDenyCount +
        IdsGetInfor.u4DropStats;

    FWL_DBG (FWL_DBG_EXIT, "\n Total Packet deny Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatTotalPacketsAccepted
 Input       :  The Indices

                The Object 
                retValFwlStatTotalPacketsAccepted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatTotalPacketsAccepted (UINT4 *pu4RetValFwlStatTotalPacketsAccepted)
{
    tIdsGetInfo         IdsGetInfor;
    FWL_DBG (FWL_DBG_ENTRY, "\n Total Packet Accept Count GET Entry\n");

    MEMSET (&IdsGetInfor, FWL_ZERO, sizeof (tIdsGetInfo));

    /*The number of packets accepted is fetched from the IDS */
    SecIdsGetInfo (ISS_IDS_GLB_STATS_REQ, &IdsGetInfor);

    *pu4RetValFwlStatTotalPacketsAccepted = gFwlAclInfo.u4TotalPktsPermitCount +
        IdsGetInfor.u4AllowStats;

    FWL_DBG (FWL_DBG_EXIT, "\n Total Packet Accept Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatTotalIcmpPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalIcmpPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatTotalIcmpPacketsDenied (UINT4
                                     *pu4RetValFwlStatTotalIcmpPacketsDenied)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Total ICMP Packet Deny Count GET Entry\n");

    *pu4RetValFwlStatTotalIcmpPacketsDenied =
        gFwlAclInfo.u4TotalIcmpPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT, "\n Total ICMP Packet deny Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatTotalSynPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalSynPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatTotalSynPacketsDenied (UINT4
                                    *pu4RetValFwlStatTotalSynPacketsDenied)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Total SYN Packet Deny Count GET Entry\n");

    *pu4RetValFwlStatTotalSynPacketsDenied =
        gFwlAclInfo.u4TotalSynPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT, "\n Total SYN Packet deny Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatTotalIpSpoofedPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalIpSpoofedPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatTotalIpSpoofedPacketsDenied (UINT4
                                          *pu4RetValFwlStatTotalIpSpoofedPacketsDenied)
{
    FWL_DBG (FWL_DBG_ENTRY,
             "\n Total IP Spoofed Packet Deny Count GET Entry\n");

    *pu4RetValFwlStatTotalIpSpoofedPacketsDenied =
        gFwlAclInfo.u4TotalIPAddressSpoofedPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT, "\n Total IP Spoofed Packet deny Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatTotalSrcRoutePacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalSrcRoutePacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatTotalSrcRoutePacketsDenied (UINT4
                                         *pu4RetValFwlStatTotalSrcRoutePacketsDenied)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Total Src Route Packet Deny Count GET Entry\n");

    *pu4RetValFwlStatTotalSrcRoutePacketsDenied =
        gFwlAclInfo.u4TotalSrcRouteAttackPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT, "\n Total Src Route Packet deny Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatTotalTinyFragmentPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalTinyFragmentPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatTotalTinyFragmentPacketsDenied (UINT4
                                             *pu4RetValFwlStatTotalTinyFragmentPacketsDenied)
{
    FWL_DBG (FWL_DBG_ENTRY,
             "\nTotal Tiny Fragment Packet Deny Count GET Entry\n");

    *pu4RetValFwlStatTotalTinyFragmentPacketsDenied =
        gFwlAclInfo.u4TotalTinyFragAttackPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT,
             "\nTotal Tiny Fragment Packet deny Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatTotalFragmentedPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalFragmentedPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatTotalFragmentedPacketsDenied (UINT4
                                           *pu4RetValFwlStatTotalFragmentedPacketsDenied)
{
    FWL_DBG (FWL_DBG_ENTRY, "\nTotal Fragment Packet Deny Count GET Entry\n");

    *pu4RetValFwlStatTotalFragmentedPacketsDenied =
        gFwlAclInfo.u4TotalFragmentPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT, "\nTotal Fragment Packet deny Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatTotalLargeFragmentPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalLargeFragmentPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatTotalLargeFragmentPacketsDenied (UINT4
                                              *pu4RetValFwlStatTotalLargeFragmentPacketsDenied)
{
    FWL_DBG (FWL_DBG_ENTRY,
             "\nTotal Large Fragment Packet Deny Count GET Entry\n");

    *pu4RetValFwlStatTotalLargeFragmentPacketsDenied =
        gFwlAclInfo.u4TotalLargeFragAttackPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT,
             "\nTotal Large Fragment Packet deny Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatTotalIpOptionPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalIpOptionPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatTotalIpOptionPacketsDenied (UINT4
                                         *pu4RetValFwlStatTotalIpOptionPacketsDenied)
{
    FWL_DBG (FWL_DBG_ENTRY, "\nTotal IpOption Packet Deny Count GET Entry\n");

    *pu4RetValFwlStatTotalIpOptionPacketsDenied =
        gFwlAclInfo.u4TotalIpOptionPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT, "\nTotal IpOption Packet deny Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatTotalAttacksPacketsDenied
 Input       :  The Indices

                The Object 
                retValFwlStatTotalAttacksPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatTotalAttacksPacketsDenied (UINT4
                                        *pu4RetValFwlStatTotalAttacksPacketsDenied)
{
    FWL_DBG (FWL_DBG_ENTRY, "\nTotal Attacks Packet Deny Count GET Entry\n");

    *pu4RetValFwlStatTotalAttacksPacketsDenied =
        gFwlAclInfo.u4TotalAttacksPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT, "\nTotal Attacks Packet deny Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatMemoryAllocationFailCount
 Input       :  The Indices

                The Object 
                retValFwlStatMemoryAllocationFailCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatMemoryAllocationFailCount (UINT4
                                        *pu4RetValFwlStatMemoryAllocationFailCount)
{
    FWL_DBG (FWL_DBG_ENTRY, "\nMemory Failure Count GET Entry\n");

    *pu4RetValFwlStatMemoryAllocationFailCount = gFwlAclInfo.u4MemFailCount;

    FWL_DBG (FWL_DBG_EXIT, "\nMemory Failure Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFwlStatIPv6InspectedPacketsCount
 *   Input       :  The Indices
 *
 *                  The Object
 *                  retValFwlStatIPv6InspectedPacketsCount
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1                nmhGetFwlStatIPv6InspectedPacketsCount
    (UINT4 *pu4RetValFwlStatIPv6InspectedPacketsCount)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n IPv6 Inspect Packet Count GET Entry\n");

    *pu4RetValFwlStatIPv6InspectedPacketsCount =
        gFwlAclInfo.u4TotalIPv6PktsInspectCount;

    FWL_DBG (FWL_DBG_EXIT, "\n IPv6 Inspect Packet Count GET Exit\n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function    :  nmhGetFwlStatIPv6TotalPacketsDenied
 *  Input       :  The Indices
 *
 *                 The Object
 *                 retValFwlStatIPv6TotalPacketsDenied
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ***************************************************************************/
INT1
nmhGetFwlStatIPv6TotalPacketsDenied (UINT4
                                     *pu4RetValFwlStatIPv6TotalPacketsDenied)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Total IPv6 Packet Deny Count GET Entry\n");

    *pu4RetValFwlStatIPv6TotalPacketsDenied =
        gFwlAclInfo.u4TotalIPv6PktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT, "\n Total IPv6 Packet deny Count GET Exit\n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function    :  nmhGetFwlStatIPv6TotalPacketsAccepted
 *  Input       :  The Indices
 *
 *                 The Object
 *                 retValFwlStatIPv6TotalPacketsAccepted
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFwlStatIPv6TotalPacketsAccepted (UINT4
                                       *pu4RetValFwlStatIPv6TotalPacketsAccepted)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Total IPv6 Packet Accept Count GET Entry\n");

    *pu4RetValFwlStatIPv6TotalPacketsAccepted =
        gFwlAclInfo.u4TotalIPv6PktsPermitCount;

    FWL_DBG (FWL_DBG_EXIT, "\n Total IPv6 Packet Accept Count GET Exit\n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function    :  nmhGetFwlStatIPv6TotalIcmpPacketsDenied
 *  Input       :  The Indices
 *
 *                 The Object
 *                 retValFwlStatIPv6TotalIcmpPacketsDenied
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetFwlStatIPv6TotalIcmpPacketsDenied (UINT4
                                         *pu4RetValFwlStatIPv6TotalIcmpPacketsDenied)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Total ICMPv6  Packet Deny Count GET Entry\n");

    *pu4RetValFwlStatIPv6TotalIcmpPacketsDenied =
        gFwlAclInfo.u4TotalIPv6IcmpPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT, "\n Total ICMPv6 Packet deny Count GET Exit\n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function    :  nmhGetFwlStatIPv6TotalSpoofedPacketsDenied
 *  Input       :  The Indices
 *
 *                 The Object
 *                 retValFwlStatIPv6TotalSpoofedPacketsDenied
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFwlStatIPv6TotalSpoofedPacketsDenied (UINT4
                                            *pu4RetValFwlStatIPv6TotalSpoofedPacketsDenied)
{
    FWL_DBG (FWL_DBG_ENTRY,
             "\n Total IPv6 Spoofed Packet Deny Count GET Entry\n");

    *pu4RetValFwlStatIPv6TotalSpoofedPacketsDenied =
        gFwlAclInfo.u4TotalIPv6AddressSpoofedPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT,
             "\n Total IPv6 Spoofed Packet deny Count GET Exit\n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function    :  nmhGetFwlStatIPv6TotalAttacksPacketsDenied
 *  Input       :  The Indices
 *
 *                 The Object
 *                 retValFwlStatIPv6TotalAttacksPacketsDenied
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFwlStatIPv6TotalAttacksPacketsDenied (UINT4
                                            *pu4RetValFwlStatIPv6TotalAttacksPacketsDenied)
{
    FWL_DBG (FWL_DBG_ENTRY,
             "\nTotal IPv6 Attacks Packet Deny Count GET Entry\n");

    *pu4RetValFwlStatIPv6TotalAttacksPacketsDenied =
        gFwlAclInfo.u4TotalIPv6AttackPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT, "\nTotal IPv6 Attacks Packet deny Count GET Exit\n");

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlStatIfTable
 Input       :  The Indices
                FwlStatIfIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)                                         SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlStatIfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatClear
 Input       :  The Indices

                The Object 
                retValFwlStatClear
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatClear (INT4 *pi4RetValFwlStatClear)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Global Statistics Clear Flag  GET Entry\n");

    *pi4RetValFwlStatClear = FWL_DISABLE;

    FWL_DBG (FWL_DBG_EXIT, "\n Global Statistics Clear Flag  GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatClearIPv6
 Input       :  The Indices

                The Object 
                retValFwlStatClearIPv6
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatClearIPv6 (INT4 *pi4RetValFwlStatClearIPv6)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Global IPv6 Statistics Clear Flag GET Entry\n");

    *pi4RetValFwlStatClearIPv6 = FWL_DISABLE;

    FWL_DBG (FWL_DBG_EXIT, "\n Global IPv6 Statistics Clear Flag  GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlTrapThreshold
 Input       :  The Indices

                The Object 
                retValFwlTrapThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlTrapThreshold (INT4 *pi4RetValFwlTrapThreshold)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Clobal Trap Threshold GET Entry\n");

    *pi4RetValFwlTrapThreshold = gFwlAclInfo.i4TrapThreshold;

    FWL_DBG (FWL_DBG_EXIT, "\n Clobal Trap Threshold GET Exit\n");

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlStatClear
 Input       :  The Indices

                The Object 
                setValFwlStatClear
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlStatClear (INT4 i4SetValFwlStatClear)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY, "\n Clobal Trap Threshold SET Entry\n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    /* Clear the Global Statistics */
    if (FWL_ENABLE == i4SetValFwlStatClear)
    {
        FwlClearGlobalStats ();
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Clobal Trap Threshold SET Exit\n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlStatClear, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFwlStatClear));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlStatClearIPv6
 Input       :  The Indices

                The Object 
                setValFwlStatClear
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlStatClearIPv6 (INT4 i4SetValFwlStatClearIPv6)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY, "\n Global IPv6 statistics  SET Entry\n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    /* Clear the Global Statistics */
    if (FWL_ENABLE == i4SetValFwlStatClearIPv6)
    {
        FwlClearIPv6GlobalStats ();
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Global IPv6 Statistics SET Exit\n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlStatClearIPv6, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFwlStatClearIPv6));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlTrapThreshold
 Input       :  The Indices

                The Object 
                setValFwlTrapThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlTrapThreshold (INT4 i4SetValFwlTrapThreshold)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    RM_GET_SEQ_NUM (&u4SeqNum);
    FWL_DBG (FWL_DBG_ENTRY, "\n Clobal Trap Threshold SET Entry\n");

    gFwlAclInfo.i4TrapThreshold = i4SetValFwlTrapThreshold;

    /* The configured trap threshold value is sent to ids only
     *        the trap is enabled globally . */
    if (gFwlAclInfo.u1TrapEnable == FWL_ENABLE)
    {
        SecIdsSetInfo (ISS_IDS_SET_PKT_DROP_THRESH,
                       (UINT4) gFwlAclInfo.i4TrapThreshold);
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlTrapThreshold, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFwlTrapThreshold));
    FWL_DBG (FWL_DBG_EXIT, "\n Clobal Trap Threshold SET Exit\n");

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlStatClear
 Input       :  The Indices

                The Object 
                testValFwlStatClear
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlStatClear (UINT4 *pu4ErrorCode, INT4 i4TestValFwlStatClear)
{
    FWL_DBG (FWL_DBG_ENTRY, "\n Global Statistics Clear Flag TEST Entry\n");

    if ((i4TestValFwlStatClear == FWL_ENABLE)
        || (i4TestValFwlStatClear == FWL_DISABLE))
    {
        FWL_DBG (FWL_DBG_EXIT, "\n Global Statistics Clear Flag TEST Exit\n");
        return SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Global Statistics Clear Flag can be 1 or 2 \n");
        CLI_SET_ERR (CLI_FWL_INVALID_CLEAR_FLAG);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 *  Function    :  nmhTestv2FwlStatClearIPv6
 *  Input       :  The Indices
 *                 The Object
 *                 testValFwlStatClearIPv6
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                 Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val
 *  Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhTestv2FwlStatClearIPv6 (UINT4 *pu4ErrorCode, INT4 i4TestValFwlStatClearIIPv6)
{
    FWL_DBG (FWL_DBG_ENTRY, "\nGlobal IPv6 Statistics Clear Flag TEST Entry\n");

    if ((i4TestValFwlStatClearIIPv6 == FWL_ENABLE)
        || (i4TestValFwlStatClearIIPv6 == FWL_DISABLE))
    {
        FWL_DBG (FWL_DBG_EXIT,
                 "\nGlobal IPv6Statistics Clear Flag TEST Exit\n");
        return SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Global IPv6 Statistics Clear Flag can be 1 or 2 \n");
        CLI_SET_ERR (CLI_FWL_INVALID_CLEAR_FLAG);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FwlTrapThreshold
 Input       :  The Indices

                The Object 
                testValFwlTrapThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlTrapThreshold (UINT4 *pu4ErrorCode, INT4 i4TestValFwlTrapThreshold)
{
    INT1                i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Clobal Trap Threshold TEST Entry\n");

    if ((i4TestValFwlTrapThreshold < FWL_MIN_TRAP_THRESHOLD)
        || (i4TestValFwlTrapThreshold > FWL_MAX_TRAP_THRESHOLD))
    {
        MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Global Trap Threshold can be between %ld - %ld \n",
                      FWL_MIN_TRAP_THRESHOLD, FWL_MAX_TRAP_THRESHOLD);
        CLI_SET_ERR (CLI_FWL_INVALID_THRESHOLD);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    else
    {
        i1Status = SNMP_SUCCESS;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Clobal Trap Threshold TEST Exit\n");

    return i1Status;
}

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            /* Low Level Dependency Routines for All Objects  *//****************************************************************************
 Function    :  nmhDepv2FwlStatClear
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.  
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlStatClear (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhDepv2FwlStatClearIPv6
 *  Output      :  The Dependency Low Lev Routine Take the Indices &
 *                 check whether dependency is met or not.
 *                 Stores the value of error code in the Return val
 *  Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ***************************************************************************/
INT1
nmhDepv2FwlStatClearIPv6 (UINT4 *pu4ErrorCode,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FwlStatIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlStatIfTable
 Input       :  The Indices
                FwlStatIfIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFwlStatIfTable (INT4 i4FwlStatIfIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Validate Index for Stat If Table - Entry\n");

    /* search the interface list and check whether it exists or not */
    if ((u4IfaceNum > FWL_ZERO) && (u4IfaceNum < FWL_MAX_NUM_OF_IF))
    {
        pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
        if (pIfaceNode != NULL)
        {
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\n Interface Number %d doesnt exist \n", u4IfaceNum);
        }
    }
    else
    {
        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\nInvalid Interface Number. The range is 1 to %d\n",
                      FWL_MAX_NUM_OF_IF - FWL_ONE);
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Validate Index for Stat If Table - Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFwlStatIfTable
 Input       :  The Indices
                FwlStatIfIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFwlStatIfTable (INT4 *pi4FwlStatIfIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    tIfaceInfo         *pIfaceNode = NULL;
    UINT4               u4IfaceNum = FWL_ZERO;

    FWL_DBG (FWL_DBG_EXIT, "\n Get First Index for Stat If Table - Entry\n");

    pIfaceNode = (tIfaceInfo *) NULL;

    /* the first valid interface index is searched in the interface list. */
    for (u4IfaceNum = FWL_ONE; u4IfaceNum < FWL_MAX_NUM_OF_IF; u4IfaceNum++)
    {
        if ((pIfaceNode = gFwlAclInfo.apIfaceList[u4IfaceNum]) != NULL)
        {
            *pi4FwlStatIfIfIndex = (INT4) u4IfaceNum;
            i1Status = SNMP_SUCCESS;
            break;
        }
        continue;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Get First Index for Stat If Table - Exit\n");

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFwlStatIfTable
 Input       :  The Indices
                FwlStatIfIfIndex
                nextFwlStatIfIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFwlStatIfTable (INT4 i4FwlStatIfIfIndex,
                               INT4 *pi4NextFwlStatIfIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    tIfaceInfo         *pIfaceNode = NULL;
    UINT4               u4IfaceNum = FWL_ZERO;
    UINT4               u4IfaceIndex = FWL_ZERO;

    FWL_DBG (FWL_DBG_ENTRY, "\n Get Next Index for Stat If Table - Entry\n");
    u4IfaceIndex = (UINT4) i4FwlStatIfIfIndex;

    /* Check whether the interface node exist for the given 
     * interface number u4IfaceNum, if not return failure.
     * Otherwise, find the next valid interface index.
     */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceIndex);
    if (pIfaceNode != NULL)
    {
        u4IfaceIndex++;
        for (u4IfaceNum = u4IfaceIndex; u4IfaceNum < FWL_MAX_NUM_OF_IF;
             u4IfaceNum++)
        {
            if ((pIfaceNode = gFwlAclInfo.apIfaceList[u4IfaceNum]) != NULL)
            {
                *pi4NextFwlStatIfIfIndex = (INT4) u4IfaceNum;
                i1Status = SNMP_SUCCESS;
                break;
            }
            continue;
        }
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Get Next Index for Stat If Table - Exit\n");

    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlStatIfFilterCount
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfFilterCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatIfFilterCount (INT4 i4FwlStatIfIfIndex,
                            INT4 *pi4RetValFwlStatIfFilterCount)
{
    UINT4               u4IfaceNum = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;

    u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;
    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Interface table Filter Count GET Entry\n");

    /* search the interface list and get the filter count */
    if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        return SNMP_FAILURE;
    }
    *pi4RetValFwlStatIfFilterCount =
        (INT4) pIfaceNode->ifaceStat.u4AclConfigCount;

    FWL_DBG (FWL_DBG_EXIT, "\n Interface table Filter Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatIfPacketsDenied
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatIfPacketsDenied (INT4 i4FwlStatIfIfIndex,
                              UINT4 *pu4RetValFwlStatIfPacketsDenied)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;
    tIdsGetInfo         IdsGetInfor;

    MEMSET (&IdsGetInfor, FWL_ZERO, sizeof (tIdsGetInfo));

    pIfaceNode = (tIfaceInfo *) NULL;
    IdsGetInfor.u4IfIndex = u4IfaceNum;

    FWL_DBG (FWL_DBG_ENTRY, "\n Interface table Deny Count GET Entry\n");

    /* search the interface list and get the Packets deny count */
    if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        return SNMP_FAILURE;
    }
    SecIdsGetInfo (ISS_IDS_INTF_STATS_REQ, &IdsGetInfor);

    *pu4RetValFwlStatIfPacketsDenied = pIfaceNode->ifaceStat.u4PktsDenyCount +
        IdsGetInfor.u4DropStats;

    FWL_DBG (FWL_DBG_EXIT, "\n Interface table Deny Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatIfPacketsAccepted
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfPacketsAccepted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatIfPacketsAccepted (INT4 i4FwlStatIfIfIndex,
                                UINT4 *pu4RetValFwlStatIfPacketsAccepted)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;
    tIdsGetInfo         IdsGetInfor;

    MEMSET (&IdsGetInfor, FWL_ZERO, sizeof (tIdsGetInfo));

    pIfaceNode = (tIfaceInfo *) NULL;
    IdsGetInfor.u4IfIndex = u4IfaceNum;

    FWL_DBG (FWL_DBG_ENTRY, "\n Interface table Permit Count GET Entry\n");

    /* search the interface list and get the packets accepted count */
    if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        return SNMP_FAILURE;
    }
    SecIdsGetInfo (ISS_IDS_INTF_STATS_REQ, &IdsGetInfor);

    *pu4RetValFwlStatIfPacketsAccepted =
        pIfaceNode->ifaceStat.u4PktsPermitCount + IdsGetInfor.u4AllowStats;

    FWL_DBG (FWL_DBG_EXIT, "\n Interface table Permit Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatIfSynPacketsDenied
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfSynPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatIfSynPacketsDenied (INT4 i4FwlStatIfIfIndex,
                                 UINT4 *pu4RetValFwlStatIfSynPacketsDenied)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY,
             "\nInterface table Syn Packet Deny Count GET Entry\n");

    /* search the interface list and get the Syn packets denied count */
    if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        return SNMP_FAILURE;
    }
    *pu4RetValFwlStatIfSynPacketsDenied =
        pIfaceNode->ifaceStat.u4SynPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT,
             "\nInterface table Syn Packets Deny Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatIfIcmpPacketsDenied
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfIcmpPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatIfIcmpPacketsDenied (INT4 i4FwlStatIfIfIndex,
                                  UINT4 *pu4RetValFwlStatIfIcmpPacketsDenied)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Interface table ICMP deny Count GET Entry\n");

    /* search the interface list and get the ICMP packets deny count */
    if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        return SNMP_FAILURE;
    }
    *pu4RetValFwlStatIfIcmpPacketsDenied =
        pIfaceNode->ifaceStat.u4IcmpPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT, "\n Interface table ICMP Deny Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatIfIpSpoofedPacketsDenied
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfIpSpoofedPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatIfIpSpoofedPacketsDenied (INT4 i4FwlStatIfIfIndex,
                                       UINT4
                                       *pu4RetValFwlStatIfIpSpoofedPacketsDenied)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY,
             "\nInterface table IP Spoofed packet Deny Count GET Entry\n");

    /* search the interface list and get the IP Spoofed packets denied count */
    if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        return SNMP_FAILURE;
    }
    *pu4RetValFwlStatIfIpSpoofedPacketsDenied =
        pIfaceNode->ifaceStat.u4IPAddrSpoofedPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT,
             "\n Interface table IP Spoofed packet Deny Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatIfSrcRoutePacketsDenied
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfSrcRoutePacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatIfSrcRoutePacketsDenied (INT4 i4FwlStatIfIfIndex,
                                      UINT4
                                      *pu4RetValFwlStatIfSrcRoutePacketsDenied)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY,
             "\nInterface table SrcRoute packet Deny Count GET Entry\n");

    /* search the interface list and get the Src Route packets denied count */
    if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        return SNMP_FAILURE;
    }
    *pu4RetValFwlStatIfSrcRoutePacketsDenied =
        pIfaceNode->ifaceStat.u4SrcRouteAttackPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT,
             "\n Interface table Src Route packet Deny Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatIfTinyFragmentPacketsDenied
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfTinyFragmentPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatIfTinyFragmentPacketsDenied (INT4 i4FwlStatIfIfIndex,
                                          UINT4
                                          *pu4RetValFwlStatIfTinyFragmentPacketsDenied)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY,
             "\nInterface table Tiny Fragment packet Deny Count GET Entry\n");

    /* search interface list and get the Tiny Fragmnet packets denied count */
    if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        return SNMP_FAILURE;
    }
    *pu4RetValFwlStatIfTinyFragmentPacketsDenied =
        pIfaceNode->ifaceStat.u4TinyFragAttackPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT,
             "\n Interface table Tiny Fragmnet packet Deny Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatIfFragmentPacketsDenied
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfFragmentPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatIfFragmentPacketsDenied (INT4 i4FwlStatIfIfIndex,
                                      UINT4
                                      *pu4RetValFwlStatIfFragmentPacketsDenied)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Interface table Fragment Deny Count GET Entry\n");

    /* search the interface list and get the Fragmented packets deny count */
    if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        return SNMP_FAILURE;
    }
    *pu4RetValFwlStatIfFragmentPacketsDenied =
        pIfaceNode->ifaceStat.u4FragmentPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT, "\n Interface table Fragmnet Deny Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatIfIpOptionPacketsDenied
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfIpOptionPacketsDenied
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatIfIpOptionPacketsDenied (INT4 i4FwlStatIfIfIndex,
                                      UINT4
                                      *pu4RetValFwlStatIfIpOptionPacketsDenied)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Interface table IP Option Deny Count GET Entry\n");

    /* search the interface list and get the IP option packets deny count */
    if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        return SNMP_FAILURE;
    }
    *pu4RetValFwlStatIfIpOptionPacketsDenied =
        pIfaceNode->ifaceStat.u4IpOptionPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT,
             "\n Interface table IP Option Deny Count GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatIfClear
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfClear
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatIfClear (INT4 i4FwlStatIfIfIndex, INT4 *pi4RetValFwlStatIfClear)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Interface Statistics Clear Flag GET Entry\n");

    /* search the interface list and get the Inerface Statistics Clear Flag */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        return SNMP_FAILURE;
    }
    *pi4RetValFwlStatIfClear = FWL_DISABLE;

    FWL_DBG (FWL_DBG_EXIT, "\n Interface Statistics Clear Flag GET Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlStatIfClearIPv6
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlStatIfClear
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStatIfClearIPv6 (INT4 i4FwlStatIfIfIndex,
                          INT4 *pi4RetValFwlStatIfClearIPv6)
{
    UINT4               u4IfaceNum = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;

    u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;

    FWL_DBG (FWL_DBG_ENTRY, "\n Interface IPv6 Statistics Clear"
             "Flag GET Entry\n");

    /* search the interface list and get the Inerface Statistics Clear Flag */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        return SNMP_FAILURE;
    }
    *pi4RetValFwlStatIfClearIPv6 = FWL_DISABLE;

    FWL_DBG (FWL_DBG_EXIT, "\n Interface IPv6 Statistics Clear "
             " Flag GET Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlIfTrapThreshold
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                retValFwlIfTrapThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlIfTrapThreshold (INT4 i4FwlStatIfIfIndex,
                          INT4 *pi4RetValFwlIfTrapThreshold)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Interface Trap Threshold GET Entry\n");

    /* search the interface list and get the Trap Threshold */
    if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        return SNMP_FAILURE;
    }
    *pi4RetValFwlIfTrapThreshold = pIfaceNode->ifaceStat.i4TrapThreshold;

    FWL_DBG (FWL_DBG_EXIT, "\n Interface Trap Threshold GET Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFwlStatIfIPv6PacketsDenied
 *  Input       :  The Indices
 *                 FwlStatIfIfIndex
 *                 The Object
 *                 retValFwlStatIfIPv6PacketsDenied
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFwlStatIfIPv6PacketsDenied (INT4 i4FwlStatIfIfIndex,
                                  UINT4 *pu4RetValFwlStatIfIPv6PacketsDenied)
{
    UINT4               u4IfaceNum = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;

    u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;

    FWL_DBG (FWL_DBG_ENTRY, "\n Interface table IPv6 Deny Count GET Entry\n");

    /* search the interface list and get the IPv6 Packets deny count */
    if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        return SNMP_FAILURE;
    }
    *pu4RetValFwlStatIfIPv6PacketsDenied =
        pIfaceNode->ifaceStat.u4IPv6PktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT, "\n Interface table IPv6 Deny Count GET Exit\n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function    :  nmhGetFwlStatIfIPv6PacketsAccepted
 *  Input       :  The Indices
 *                  FwlStatIfIfIndex
 *                  The Object
 *                  retValFwlStatIfIPv6PacketsAccepted
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFwlStatIfIPv6PacketsAccepted (INT4 i4FwlStatIfIfIndex,
                                    UINT4
                                    *pu4RetValFwlStatIfIPv6PacketsAccepted)
{
    UINT4               u4IfaceNum = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;

    u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;

    FWL_DBG (FWL_DBG_ENTRY, "\n Interface table Permit IPv6 Count GET Entry\n");

    /* search the interface list and get the packets accepted count */
    if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        return SNMP_FAILURE;
    }
    *pu4RetValFwlStatIfIPv6PacketsAccepted =
        pIfaceNode->ifaceStat.u4IPv6PktsPermitCount;

    FWL_DBG (FWL_DBG_EXIT, "\n Interface table Permit IPv6  Count GET Exit\n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function    :  nmhGetFwlStatIfIPv6IcmpPacketsDenied
 *  Input       :  The Indices
 *                 FwlStatIfIfIndex
 *                 The Object
 *                 retValFwlStatIfIPv6IcmpPacketsDenied
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFwlStatIfIPv6IcmpPacketsDenied (INT4 i4FwlStatIfIfIndex,
                                      UINT4
                                      *pu4RetValFwlStatIfIPv6IcmpPacketsDenied)
{
    UINT4               u4IfaceNum = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;

    u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;

    FWL_DBG (FWL_DBG_ENTRY, "\n Interface table ICMPv6 deny Count GET Entry\n");

    /* search the interface list and get the ICMP packets deny count */
    if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        return SNMP_FAILURE;
    }
    *pu4RetValFwlStatIfIPv6IcmpPacketsDenied =
        pIfaceNode->ifaceStat.u4Icmpv6PktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT, "\n Interface table ICMPv6 Deny Count GET Exit\n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function    :  nmhGetFwlStatIfIPv6SpoofedPacketsDenied
 *  Input       :  The Indices
 *                 FwlStatIfIfIndex
 *                 The Object
 *                 retValFwlStatIfIPv6SpoofedPacketsDenied
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFwlStatIfIPv6SpoofedPacketsDenied (INT4 i4FwlStatIfIfIndex,
                                         UINT4
                                         *pu4RetValFwlStatIfIPv6SpoofedPacketsDenied)
{
    UINT4               u4IfaceNum = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;

    u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;

    FWL_DBG (FWL_DBG_ENTRY,
             "\nInterface table IPv6 Spoofed packet Deny Count GET Entry\n");

    /* search the interface list and get the IP Spoofed packets denied count */
    if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        return SNMP_FAILURE;
    }
    *pu4RetValFwlStatIfIPv6SpoofedPacketsDenied =
        pIfaceNode->ifaceStat.u4IPv6AddrSpoofedPktsDenyCount;

    FWL_DBG (FWL_DBG_EXIT,
             "\n Interface table IPv6 Spoofed packet Deny Count GET Exit\n");

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlStatIfClear
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                setValFwlStatIfClear
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlStatIfClear (INT4 i4FwlStatIfIfIndex, INT4 i4SetValFwlStatIfClear)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY, "\n Interface Statistics Clear Flag  GET Entry\n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    /* Clear the Interface Statistics if the input Flag is Set to 
     * SNMP_TRUE (1) */

    if (FWL_ENABLE == i4SetValFwlStatIfClear)
    {
        FwlClearInterfaceStats ((UINT4) i4FwlStatIfIfIndex);
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Interface Statistics Clear Flag  GET Exit\n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlStatIfClear, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4FwlStatIfIfIndex,
                      i4SetValFwlStatIfClear));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlStatIfClearIPv6
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                setValFwlStatIfClearIPv6
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlStatIfClearIPv6 (INT4 i4FwlStatIfIfIndex,
                          INT4 i4SetValFwlStatIfClearIPv6)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    FWL_DBG (FWL_DBG_ENTRY,
             "\n Interface IPv6 Statistics Clear Flag GET Entry\n");
    RM_GET_SEQ_NUM (&u4SeqNum);
    /* Clear the Interface Statistics if the input Flag is Set to 
     * SNMP_TRUE (1) */

    if (FWL_ENABLE == i4SetValFwlStatIfClearIPv6)
    {
        FwlClearInterfaceIPv6Stats ((UINT4) i4FwlStatIfIfIndex);
    }

    FWL_DBG (FWL_DBG_EXIT,
             "\n Interface IPv6 Statistics Clear Flag GET Exit\n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlStatIfClearIPv6, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4FwlStatIfIfIndex,
                      i4SetValFwlStatIfClearIPv6));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlIfTrapThreshold
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                setValFwlIfTrapThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlIfTrapThreshold (INT4 i4FwlStatIfIfIndex,
                          INT4 i4SetValFwlIfTrapThreshold)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Interface Trap Threshold SET Entry\n");

    /* search the interface list and get the Trap Threshold */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return SNMP_FAILURE;
    }
    pIfaceNode->ifaceStat.i4TrapThreshold = i4SetValFwlIfTrapThreshold;

    FWL_DBG (FWL_DBG_EXIT, "\n Interface Trap Threshold SET Exit\n");
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlIfTrapThreshold, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4FwlStatIfIfIndex,
                      i4SetValFwlIfTrapThreshold));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlStatIfClear
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                testValFwlStatIfClear
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlStatIfClear (UINT4 *pu4ErrorCode, INT4 i4FwlStatIfIfIndex,
                         INT4 i4TestValFwlStatIfClear)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Interface Statistics Clear Flag TEST Entry\n");

    if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Not necessary that the Row Status should be active for changing this
     * object */
    if ((pIfaceNode->u1RowStatus != FWL_NOT_IN_SERVICE) &&
        (pIfaceNode->u1RowStatus != FWL_ACTIVE))
    {
        CLI_SET_ERR (CLI_FWL_INCONSIST_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFwlStatIfClear == FWL_ENABLE)
        || (i4TestValFwlStatIfClear == FWL_DISABLE))
    {
        FWL_DBG (FWL_DBG_EXIT,
                 "\n Interface Statistics Clear Flag TEST Exit \n");
        return SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Interface Statistics Clear Flag can be 1 or 2 \n");
        CLI_SET_ERR (CLI_FWL_INVALID_CLEAR_FLAG);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FwlStatIfClearIPv6
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                testValFwlStatIfClear
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlStatIfClearIPv6 (UINT4 *pu4ErrorCode, INT4 i4FwlStatIfIfIndex,
                             INT4 i4TestValFwlStatIfClearIPv6)
{
    UINT4               u4IfaceNum = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;

    u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Interface IPv6 Statistics Clear Flag TEST Entry\n");

    if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Not necessary that the Row Status should be active for changing this
     * object */
    if ((pIfaceNode->u1RowStatus != FWL_NOT_IN_SERVICE) &&
        (pIfaceNode->u1RowStatus != FWL_ACTIVE))
    {
        CLI_SET_ERR (CLI_FWL_INCONSIST_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFwlStatIfClearIPv6 == FWL_ENABLE)
        || (i4TestValFwlStatIfClearIPv6 == FWL_DISABLE))
    {
        FWL_DBG (FWL_DBG_EXIT,
                 "\n Interface IPv6 Statistics Clear Flag TEST Exit \n");
        return SNMP_SUCCESS;
    }
    else
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n Interface IPv6 Statistics Clear Flag can be 1 or 2 \n");
        CLI_SET_ERR (CLI_FWL_INVALID_CLEAR_FLAG);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FwlIfTrapThreshold
 Input       :  The Indices
                FwlStatIfIfIndex

                The Object 
                testValFwlIfTrapThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlIfTrapThreshold (UINT4 *pu4ErrorCode, INT4 i4FwlStatIfIfIndex,
                             INT4 i4TestValFwlIfTrapThreshold)
{
    UINT4               u4IfaceNum = (UINT4) i4FwlStatIfIfIndex;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Interface Trap Threshold TEST Entry\n");

    if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
    {
        CLI_SET_ERR (CLI_FWL_NO_SUCH_IF);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Not necessary that the Row Status should be active for changing this
     * object */
    if ((pIfaceNode->u1RowStatus != FWL_NOT_IN_SERVICE) &&
        (pIfaceNode->u1RowStatus != FWL_ACTIVE))
    {
        CLI_SET_ERR (CLI_FWL_INCONSIST_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFwlIfTrapThreshold < FWL_MIN_TRAP_THRESHOLD) ||
        (i4TestValFwlIfTrapThreshold > FWL_MAX_TRAP_THRESHOLD))
    {
        MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                      "\n Interface Trap Threshold can be between %ld - %ld \n",
                      FWL_MIN_TRAP_THRESHOLD, FWL_MAX_TRAP_THRESHOLD);
        CLI_SET_ERR (CLI_FWL_INVALID_THRESHOLD);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        FWL_DBG (FWL_DBG_EXIT, "\n Interface Trap Threshold TEST Exit \n");
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhDepv2FwlTrapThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlTrapThreshold (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlTrapMemFailMessage
 Input       :  The Indices

                The Object 
                retValFwlTrapMemFailMessage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlTrapMemFailMessage (tSNMP_OCTET_STRING_TYPE *
                             pRetValFwlTrapMemFailMessage)
{
    pRetValFwlTrapMemFailMessage->i4_Length = (INT4)
        STRLEN (gFwlAclInfo.au1TrapMemFailMessage);
    FWL_MEMCPY (pRetValFwlTrapMemFailMessage->pu1_OctetList,
                gFwlAclInfo.au1TrapMemFailMessage,
                pRetValFwlTrapMemFailMessage->i4_Length);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlTrapAttackMessage
 Input       :  The Indices

                The Object 
                retValFwlTrapAttackMessage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlTrapAttackMessage (tSNMP_OCTET_STRING_TYPE *
                            pRetValFwlTrapAttackMessage)
{
    pRetValFwlTrapAttackMessage->i4_Length = (INT4)
        STRLEN (gFwlAclInfo.au1TrapAttackMessage);
    FWL_MEMCPY (pRetValFwlTrapAttackMessage->pu1_OctetList,
                gFwlAclInfo.au1TrapAttackMessage,
                pRetValFwlTrapAttackMessage->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlTrapFileName
 Input       :  The Indices

                The Object
                retValFwlTrapFileName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlTrapFileName (tSNMP_OCTET_STRING_TYPE * pRetValFwlTrapFileName)
{
    pRetValFwlTrapFileName->i4_Length = (INT4) STRLEN (gau1FwlLogFile);
    FWL_MEMCPY (pRetValFwlTrapFileName->pu1_OctetList,
                gau1FwlLogFile, pRetValFwlTrapFileName->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlIdsTrapFileName
 Input       :  The Indices

                The Object
                retValFwlIdsTrapFileName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlIdsTrapFileName (tSNMP_OCTET_STRING_TYPE * pRetValFwlIdsTrapFileName)
{
    pRetValFwlIdsTrapFileName->i4_Length = (INT4) STRLEN (gau1IdsLogFile);
    FWL_MEMCPY (pRetValFwlIdsTrapFileName->pu1_OctetList,
                gau1IdsLogFile, pRetValFwlIdsTrapFileName->i4_Length);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlTrapMemFailMessage
 Input       :  The Indices

                The Object 
                setValFwlTrapMemFailMessage
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlTrapMemFailMessage (tSNMP_OCTET_STRING_TYPE *
                             pSetValFwlTrapMemFailMessage)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    RM_GET_SEQ_NUM (&u4SeqNum);
    UNUSED_PARAM (pSetValFwlTrapMemFailMessage);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlTrapMemFailMessage, u4SeqNum,
                          FALSE, FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s", pSetValFwlTrapMemFailMessage));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlTrapAttackMessage
 Input       :  The Indices

                The Object 
                setValFwlTrapAttackMessage
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlTrapAttackMessage (tSNMP_OCTET_STRING_TYPE *
                            pSetValFwlTrapAttackMessage)
{
    UINT4               u4SeqNum = FWL_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    RM_GET_SEQ_NUM (&u4SeqNum);
    FWL_MEMCPY (gFwlAclInfo.au1TrapAttackMessage,
                pSetValFwlTrapAttackMessage->pu1_OctetList,
                pSetValFwlTrapAttackMessage->i4_Length);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FwlTrapAttackMessage, u4SeqNum, FALSE,
                          FwlLock, FwlUnLock, FWL_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s", pSetValFwlTrapAttackMessage));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlTrapMemFailMessage
 Input       :  The Indices

                The Object 
                testValFwlTrapMemFailMessage
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlTrapMemFailMessage (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFwlTrapMemFailMessage)
{
#ifdef SNMP_2_WANTED
    if (OSIX_FAILURE ==
        SNMPCheckForNVTChars (pTestValFwlTrapMemFailMessage->pu1_OctetList,
                              pTestValFwlTrapMemFailMessage->i4_Length))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
    if ((pTestValFwlTrapMemFailMessage->i4_Length >= FWL_MAX_TRAP_LEN)
        || (pTestValFwlTrapMemFailMessage->i4_Length <= FWL_ZERO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FwlTrapAttackMessage
 Input       :  The Indices

                The Object 
                testValFwlTrapAttackMessage
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlTrapAttackMessage (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFwlTrapAttackMessage)
{
    UNUSED_PARAM (pu4ErrorCode);
#ifdef SNMP_2_WANTED
    if (OSIX_FAILURE ==
        SNMPCheckForNVTChars (pTestValFwlTrapAttackMessage->pu1_OctetList,
                              pTestValFwlTrapAttackMessage->i4_Length))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
    if ((pTestValFwlTrapAttackMessage->i4_Length >= FWL_MAX_TRAP_LEN)
        || (pTestValFwlTrapAttackMessage->i4_Length <= FWL_ZERO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlTrapMemFailMessage
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)                                              SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlTrapMemFailMessage (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************                        Function    :  nmhDepv2FwlTrapAttackMessage
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)                                              SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlTrapAttackMessage (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Below Routines were manually Added */

/* Set the Log Messages flag */
/* This Flag can be 
 * LogBrief-->  Brief log messagies are displayed
 * LogDetail--> Detailed Log messages are displayed
 * None --> No log messages  are displayed.
 * All these log messages are per accesslists */
INT1
nmhSetFwlLogTrigger (UINT4 u4Interface, UINT1 u1LogTrigger,
                     UINT1 *pu1AclName, UINT4 u4Direction)
{

    tIfaceInfo         *pIfaceInfo = NULL;
    tAclInfo           *pAclNode = NULL;
    tAclInfo           *pAclTempNode = NULL;
    tFilterInfo        *pFilterNode = NULL;
    tRuleInfo          *pRuleFilter = NULL;
    tTMO_SLL           *pAclList = NULL;

    pAclNode = (tAclInfo *) NULL;

    pIfaceInfo = gFwlAclInfo.apIfaceList[u4Interface];

    if (u4Direction == FWL_DIRECTION_IN)
    {
        pAclTempNode =
            FwlDbaseSearchAclFilter (&pIfaceInfo->inFilterList, pu1AclName);
        if (pAclTempNode == NULL)
        {
            pAclTempNode =
                FwlDbaseSearchAclFilter (&pIfaceInfo->inIPv6FilterList,
                                         pu1AclName);
        }
    }
    else if (u4Direction == FWL_DIRECTION_OUT)
    {
        pAclTempNode =
            FwlDbaseSearchAclFilter (&pIfaceInfo->outFilterList, pu1AclName);
        if (pAclTempNode == NULL)
        {
            pAclTempNode =
                FwlDbaseSearchAclFilter (&pIfaceInfo->outIPv6FilterList,
                                         pu1AclName);
        }
    }
    if (pAclTempNode == NULL)
    {
        return SNMP_FAILURE;
    }

    pRuleFilter = (tRuleInfo *) pAclTempNode->pAclName;

    if (pRuleFilter == NULL)
    {
        return SNMP_FAILURE;
    }
    pFilterNode = pRuleFilter->apFilterInfo[FWL_RULE_FIRST_FILTER_INDEX];
    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }
    if (u4Direction == FWL_DIRECTION_IN)
    {
        if (pFilterNode->u2AddrType == FWL_IP_VERSION_4)
        {
            pAclList = &pIfaceInfo->inFilterList;
        }
        else if (pFilterNode->u2AddrType == FWL_IP_VERSION_6)
        {
            pAclList = &pIfaceInfo->inIPv6FilterList;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else if (u4Direction == FWL_DIRECTION_OUT)
    {
        if (pFilterNode->u2AddrType == FWL_IP_VERSION_4)
        {
            pAclList = &pIfaceInfo->outFilterList;
        }
        else if (pFilterNode->u2AddrType == FWL_IP_VERSION_6)
        {
            pAclList = &pIfaceInfo->outIPv6FilterList;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
/*    else
    {
        return SNMP_FAILURE;
    }
*/
    TMO_SLL_Scan (pAclList, pAclNode, tAclInfo *)
    {
        if (FWL_STRCMP
            ((INT1 *) pu1AclName,
             (INT1 *) pAclNode->au1FilterName) == FWL_STRING_EQUAL)
        {
            pAclNode->u1LogTrigger = u1LogTrigger;
            return SNMP_FAILURE;
        }
    }                            /* End of the TMO_SLL_Scan */
    return SNMP_SUCCESS;
}

/* Set the action flag for fragmented packets */
/* This Flag can be 
 * permit-->fragmented packet should be allowed
 * deny-->fragmented packet should be dropped
 * All these log messages are per accesslists */
INT1
nmhSetFwlFragAction (UINT4 u4Interface, UINT1 u1FragAction,
                     UINT1 *pu1AclName, UINT4 u4Direction)
{

    tIfaceInfo         *pIfaceInfo = NULL;
    tAclInfo           *pAclNode = NULL;
    tAclInfo           *pAclTempNode = NULL;
    tFilterInfo        *pFilterNode = NULL;
    tRuleInfo          *pRuleFilter = NULL;
    tTMO_SLL           *pAclList = NULL;

    pAclNode = (tAclInfo *) NULL;

    pIfaceInfo = gFwlAclInfo.apIfaceList[u4Interface];

    if (u4Direction == FWL_DIRECTION_IN)
    {
        pAclTempNode =
            FwlDbaseSearchAclFilter (&pIfaceInfo->inFilterList, pu1AclName);
        if (pAclTempNode == NULL)
        {
            pAclTempNode =
                FwlDbaseSearchAclFilter (&pIfaceInfo->inIPv6FilterList,
                                         pu1AclName);
        }
    }
    else if (u4Direction == FWL_DIRECTION_OUT)
    {
        pAclTempNode =
            FwlDbaseSearchAclFilter (&pIfaceInfo->outFilterList, pu1AclName);
        if (pAclTempNode == NULL)
        {
            pAclTempNode =
                FwlDbaseSearchAclFilter (&pIfaceInfo->outIPv6FilterList,
                                         pu1AclName);
        }
    }
    if (pAclTempNode == NULL)
    {
        return SNMP_FAILURE;
    }

    pRuleFilter = (tRuleInfo *) pAclTempNode->pAclName;

    if (pRuleFilter == NULL)
    {
        return SNMP_FAILURE;
    }
    pFilterNode = pRuleFilter->apFilterInfo[FWL_RULE_FIRST_FILTER_INDEX];
    if (pFilterNode == NULL)
    {
        return SNMP_FAILURE;
    }
    if (u4Direction == FWL_DIRECTION_IN)
    {
        if (pFilterNode->u2AddrType == FWL_IP_VERSION_4)
        {
            pAclList = &pIfaceInfo->inFilterList;
        }
        else if (pFilterNode->u2AddrType == FWL_IP_VERSION_6)
        {
            pAclList = &pIfaceInfo->inIPv6FilterList;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else if (u4Direction == FWL_DIRECTION_OUT)
    {
        if (pFilterNode->u2AddrType == FWL_IP_VERSION_4)
        {
            pAclList = &pIfaceInfo->outFilterList;
        }
        else if (pFilterNode->u2AddrType == FWL_IP_VERSION_6)
        {
            pAclList = &pIfaceInfo->outIPv6FilterList;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
/*    else
    {
        return SNMP_FAILURE;
    }
*/
    TMO_SLL_Scan (pAclList, pAclNode, tAclInfo *)
    {
        if (FWL_STRCMP
            ((INT1 *) pu1AclName,
             (INT1 *) pAclNode->au1FilterName) == FWL_STRING_EQUAL)
        {
            pAclNode->u1FragAction = u1FragAction;
            return SNMP_FAILURE;
        }
    }                            /* End of the TMO_SLL_Scan */
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FwlStateTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlStateTable
 Input       :  The Indices
                FwlStateType
                FwlStateLocalIpAddrType
                FwlStateLocalIpAddress
                FwlStateRemoteIpAddrType
                FwlStateRemoteIpAddress
                FwlStateLocalPort
                FwlStateRemotePort
                FwlStateProtocol
                FwlStateDirection
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFwlStateTable (INT4 i4FwlStateType,
                                       INT4 i4FwlStateLocalIpAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFwlStateLocalIpAddress,
                                       INT4 i4FwlStateRemoteIpAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFwlStateRemoteIpAddress,
                                       INT4 i4FwlStateLocalPort,
                                       INT4 i4FwlStateRemotePort,
                                       INT4 i4FwlStateProtocol,
                                       INT4 i4FwlStateDirection)
{
    tStatefulSessionNode *pStateFilterInfo = NULL;
    tFwlIpAddr          FwlLocalIpAddr;
    tFwlIpAddr          FwlRemoteIpAddr;
    UINT4               u4HashIndex = FWL_ZERO;
    INT1                i1RetVal = SNMP_SUCCESS;

    MEMSET (&FwlRemoteIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));
    MEMSET (&FwlLocalIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));

    if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             pFwlStateLocalIpAddress, &FwlLocalIpAddr);
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             pFwlStateRemoteIpAddress, &FwlRemoteIpAddr);
    }
    else if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV6,
                             pFwlStateLocalIpAddress, &FwlLocalIpAddr);
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV6,
                             pFwlStateRemoteIpAddress, &FwlRemoteIpAddr);
    }

    switch (i4FwlStateType)
    {
        case FWL_STATEFUL:
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                TMO_HASH_Scan_Table (FWL_STATE_HASH_LIST, u4HashIndex)
                {
                    pStateFilterInfo = NULL;
                    while (FwlStateGetStatefulTableEntries (u4HashIndex,
                                                            &pStateFilterInfo)
                           == FWL_SUCCESS)
                    {
                        if (pStateFilterInfo != NULL)
                        {
                            if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                                 pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                                (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                                 pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                                (i4FwlStateProtocol ==
                                 pStateFilterInfo->u1Protocol) &&
                                (FWL_SUCCESS == FwlStatePortMatchExists
                                 (pStateFilterInfo, i4FwlStateLocalPort,
                                  i4FwlStateRemotePort)) &&
                                (i4FwlStateDirection ==
                                 pStateFilterInfo->u1Direction))
                            {
                                /* A matching entry found */
                                i1RetVal = SNMP_SUCCESS;
                                break;
                            }

                        }
                    }            /* End of while */
                }
            }
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                TMO_HASH_Scan_Table (FWL_STATE_V6_HASH_LIST, u4HashIndex)
                {
                    pStateFilterInfo = NULL;
                    while (FwlStateGetIpv6StatefulTableEntries (u4HashIndex,
                                                                &pStateFilterInfo)
                           == FWL_SUCCESS)
                    {
                        if (pStateFilterInfo != NULL)
                        {
                            if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                                 pStateFilterInfo->LocalIP.
                                                 uIpAddr.Ip6Addr) == FWL_ZERO)
                                &&
                                (Ip6AddrCompare
                                 (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                                  pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                                 FWL_ZERO)
                                && (i4FwlStateProtocol ==
                                    pStateFilterInfo->u1Protocol)
                                && (FWL_SUCCESS ==
                                    FwlStatePortMatchExists (pStateFilterInfo,
                                                             i4FwlStateLocalPort,
                                                             i4FwlStateRemotePort))
                                && (i4FwlStateDirection ==
                                    pStateFilterInfo->u1Direction))
                            {
                                /* A matching entry found */
                                i1RetVal = SNMP_SUCCESS;
                                break;
                            }

                        }
                    }            /* End of while */
                }
            }

            break;
        case FWL_PARTIAL:
            if (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                pStateFilterInfo = (tStatefulSessionNode *)
                    TMO_SLL_First (&gFwlPartialLinksList);

                if (pStateFilterInfo == NULL)
                {
                    return SNMP_FAILURE;
                }

                while (pStateFilterInfo != NULL)
                {
                    if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                        (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                        (i4FwlStateProtocol ==
                         pStateFilterInfo->u1Protocol) &&
                        (FWL_SUCCESS == FwlStatePortMatchExists
                         (pStateFilterInfo, i4FwlStateLocalPort,
                          i4FwlStateRemotePort)) &&
                        (i4FwlStateDirection == pStateFilterInfo->u1Direction))
                    {
                        /* A matching entry found */
                        i1RetVal = SNMP_SUCCESS;
                        break;
                    }
                    pStateFilterInfo =
                        (tStatefulSessionNode *)
                        TMO_SLL_Next (&gFwlPartialLinksList,
                                      (tTMO_SLL_NODE *) pStateFilterInfo);
                }                /* End of while */

            }
            if (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                pStateFilterInfo = (tStatefulSessionNode *)
                    TMO_SLL_First (&gFwlV6PartialLinksList);

                if (pStateFilterInfo == NULL)
                {
                    return SNMP_FAILURE;
                }

                while (pStateFilterInfo != NULL)
                {
                    if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                         pStateFilterInfo->LocalIP.uIpAddr.
                                         Ip6Addr) == FWL_ZERO)
                        &&
                        (Ip6AddrCompare
                         (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                          pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                         FWL_ZERO)
                        && (i4FwlStateProtocol == pStateFilterInfo->u1Protocol)
                        && (FWL_SUCCESS ==
                            FwlStatePortMatchExists (pStateFilterInfo,
                                                     i4FwlStateLocalPort,
                                                     i4FwlStateRemotePort))
                        && (i4FwlStateDirection ==
                            pStateFilterInfo->u1Direction))
                    {
                        /* A matching entry found */
                        i1RetVal = SNMP_SUCCESS;
                        break;
                    }
                    pStateFilterInfo =
                        (tStatefulSessionNode *)
                        TMO_SLL_Next (&gFwlV6PartialLinksList,
                                      (tTMO_SLL_NODE *) pStateFilterInfo);
                }                /* End of while */

            }

            break;
        case FWL_INITFLOW:
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                TMO_SLL_Scan (&gFwlTcpInitFlowList, pStateFilterInfo,
                              tStatefulSessionNode *)
                {
                    if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                        (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                        (i4FwlStateProtocol ==
                         pStateFilterInfo->u1Protocol) &&
                        (FWL_SUCCESS == FwlStatePortMatchExists
                         (pStateFilterInfo, i4FwlStateLocalPort,
                          i4FwlStateRemotePort)) &&
                        (i4FwlStateDirection == pStateFilterInfo->u1Direction))
                    {
                        /* A matching entry found */
                        i1RetVal = SNMP_SUCCESS;
                        break;
                    }

                }                /* End of scan */
            }
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                TMO_SLL_Scan (&gFwlV6TcpInitFlowList, pStateFilterInfo,
                              tStatefulSessionNode *)
                {
                    if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                         pStateFilterInfo->LocalIP.uIpAddr.
                                         Ip6Addr) == FWL_ZERO)
                        &&
                        (Ip6AddrCompare
                         (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                          pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                         FWL_ZERO)
                        && (i4FwlStateProtocol == pStateFilterInfo->u1Protocol)
                        && (FWL_SUCCESS ==
                            FwlStatePortMatchExists (pStateFilterInfo,
                                                     i4FwlStateLocalPort,
                                                     i4FwlStateRemotePort))
                        && (i4FwlStateDirection ==
                            pStateFilterInfo->u1Direction))
                    {
                        /* A matching entry found */
                        i1RetVal = SNMP_SUCCESS;
                        break;
                    }

                }                /* End of scan */
            }
            break;
        default:
            i1RetVal = SNMP_FAILURE;
            break;
    }                            /* End of switch */

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFwlStateTable
 Input       :  The Indices
                FwlStateType
                FwlStateLocalIpAddrType
                FwlStateLocalIpAddress
                FwlStateRemoteIpAddrType
                FwlStateRemoteIpAddress
                FwlStateLocalPort
                FwlStateRemotePort
                FwlStateProtocol
                FwlStateDirection
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFwlStateTable (INT4 *pi4FwlStateType,
                               INT4 *pi4FwlStateLocalIpAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFwlStateLocalIpAddress,
                               INT4 *pi4FwlStateRemoteIpAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFwlStateRemoteIpAddress,
                               INT4 *pi4FwlStateLocalPort,
                               INT4 *pi4FwlStateRemotePort,
                               INT4 *pi4FwlStateProtocol,
                               INT4 *pi4FwlStateDirection)
{
    tStatefulSessionNode *pStateFilterInfo = NULL;
    UINT4               u4HashIndex = FWL_ZERO;

    /* The implementation currently exists only for IPv4 data structures. 
     * The function should be enhanced for supporting IPV6 for State table entries.  
     */

    /* Search in the following order. */
    /* Search ipv4 hash table first */
    TMO_HASH_Scan_Table (FWL_STATE_HASH_LIST, u4HashIndex)
    {
        pStateFilterInfo = NULL;
        if (FwlStateGetStatefulTableEntries (u4HashIndex,
                                             &pStateFilterInfo) == FWL_SUCCESS)
        {
            *pi4FwlStateType = FWL_STATEFUL;
            *pi4FwlStateLocalIpAddrType = IPVX_ADDR_FMLY_IPV4;
            pFwlStateLocalIpAddress->i4_Length = FWL_IPV4_PREFIX_LEN;
            MEMCPY (pFwlStateLocalIpAddress->pu1_OctetList,
                    (UINT1 *) &(pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr),
                    FWL_IPV4_PREFIX_LEN);
            *pi4FwlStateRemoteIpAddrType = IPVX_ADDR_FMLY_IPV4;
            pFwlStateRemoteIpAddress->i4_Length = FWL_IPV4_PREFIX_LEN;
            MEMCPY (pFwlStateRemoteIpAddress->pu1_OctetList,
                    (UINT1 *) &(pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr),
                    FWL_IPV4_PREFIX_LEN);
            *pi4FwlStateProtocol = pStateFilterInfo->u1Protocol;
            if (pStateFilterInfo->u1Protocol == FWL_ICMP)
            {
                *pi4FwlStateLocalPort = FWL_ICMP_IP_ID (pStateFilterInfo);
            }
            else
            {
                *pi4FwlStateLocalPort =
                    FWL_LOCAL_PORT (pStateFilterInfo,
                                    pStateFilterInfo->u1Protocol);
            }
            *pi4FwlStateRemotePort =
                FWL_REMOTE_PORT (pStateFilterInfo,
                                 pStateFilterInfo->u1Protocol);
            *pi4FwlStateDirection = pStateFilterInfo->u1Direction;
            return SNMP_SUCCESS;
        }
    }                            /* End of hash */
    /*Search in the ipv6 hash list */
    TMO_HASH_Scan_Table (FWL_STATE_V6_HASH_LIST, u4HashIndex)
    {
        pStateFilterInfo = NULL;
        if (FwlStateGetIpv6StatefulTableEntries (u4HashIndex,
                                                 &pStateFilterInfo) ==
            FWL_SUCCESS)
        {
            *pi4FwlStateType = FWL_STATEFUL;
            *pi4FwlStateLocalIpAddrType = IPVX_ADDR_FMLY_IPV6;
            pFwlStateLocalIpAddress->i4_Length = FWL_IPV6_PREFIX_LEN;
            MEMCPY (pFwlStateLocalIpAddress->pu1_OctetList,
                    (UINT1 *) &(pStateFilterInfo->LocalIP.uIpAddr.Ip6Addr),
                    FWL_IPV6_PREFIX_LEN);
            *pi4FwlStateRemoteIpAddrType = IPVX_ADDR_FMLY_IPV6;
            pFwlStateRemoteIpAddress->i4_Length = FWL_IPV6_PREFIX_LEN;
            MEMCPY (pFwlStateRemoteIpAddress->pu1_OctetList,
                    (UINT1 *) &(pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr),
                    FWL_IPV6_PREFIX_LEN);
            *pi4FwlStateProtocol = pStateFilterInfo->u1Protocol;
            if (pStateFilterInfo->u1Protocol == FWL_ICMPV6)
            {
                *pi4FwlStateLocalPort = FWL_ICMP_IP_ID (pStateFilterInfo);
            }
            else
            {
                *pi4FwlStateLocalPort =
                    FWL_LOCAL_PORT (pStateFilterInfo,
                                    pStateFilterInfo->u1Protocol);
            }
            *pi4FwlStateRemotePort =
                FWL_REMOTE_PORT (pStateFilterInfo,
                                 pStateFilterInfo->u1Protocol);
            *pi4FwlStateDirection = pStateFilterInfo->u1Direction;
            return SNMP_SUCCESS;
        }
    }                            /* End of hash */

    /* Search in the partial entry table. */
    pStateFilterInfo = (tStatefulSessionNode *)
        TMO_SLL_First (&gFwlPartialLinksList);
    if (pStateFilterInfo != NULL)
    {
        *pi4FwlStateType = FWL_PARTIAL;
        *pi4FwlStateLocalIpAddrType = IPVX_ADDR_FMLY_IPV4;
        pFwlStateLocalIpAddress->i4_Length = FWL_IPV4_PREFIX_LEN;
        MEMCPY (pFwlStateLocalIpAddress->pu1_OctetList,
                (UINT1 *) &(pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr),
                FWL_IPV4_PREFIX_LEN);
        pFwlStateRemoteIpAddress->i4_Length = FWL_IPV4_PREFIX_LEN;
        MEMCPY (pFwlStateRemoteIpAddress->pu1_OctetList,
                (UINT1 *) &(pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr),
                FWL_IPV4_PREFIX_LEN);
        *pi4FwlStateRemoteIpAddrType = IPVX_ADDR_FMLY_IPV4;
        *pi4FwlStateProtocol = pStateFilterInfo->u1Protocol;
        if (pStateFilterInfo->u1Protocol == FWL_ICMP)
        {
            *pi4FwlStateLocalPort = FWL_ICMP_IP_ID (pStateFilterInfo);
        }
        else
        {
            *pi4FwlStateLocalPort =
                FWL_LOCAL_PORT (pStateFilterInfo, pStateFilterInfo->u1Protocol);
        }
        *pi4FwlStateRemotePort =
            FWL_REMOTE_PORT (pStateFilterInfo, pStateFilterInfo->u1Protocol);
        *pi4FwlStateDirection = pStateFilterInfo->u1Direction;

        return SNMP_SUCCESS;
    }
    /* Search the ipv6 partial list */
    pStateFilterInfo = (tStatefulSessionNode *)
        TMO_SLL_First (&gFwlV6PartialLinksList);
    if (pStateFilterInfo != NULL)
    {
        *pi4FwlStateType = FWL_PARTIAL;
        *pi4FwlStateLocalIpAddrType = IPVX_ADDR_FMLY_IPV6;
        pFwlStateLocalIpAddress->i4_Length = FWL_IPV6_PREFIX_LEN;
        MEMCPY (pFwlStateLocalIpAddress->pu1_OctetList,
                (UINT1 *) &(pStateFilterInfo->LocalIP.uIpAddr.Ip6Addr),
                FWL_IPV6_PREFIX_LEN);
        pFwlStateRemoteIpAddress->i4_Length = FWL_IPV6_PREFIX_LEN;
        MEMCPY (pFwlStateRemoteIpAddress->pu1_OctetList,
                (UINT1 *) &(pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr),
                FWL_IPV6_PREFIX_LEN);
        *pi4FwlStateRemoteIpAddrType = IPVX_ADDR_FMLY_IPV6;
        *pi4FwlStateProtocol = pStateFilterInfo->u1Protocol;
        if (pStateFilterInfo->u1Protocol == FWL_ICMPV6)
        {
            *pi4FwlStateLocalPort = FWL_ICMP_IP_ID (pStateFilterInfo);
        }
        else
        {
            *pi4FwlStateLocalPort =
                FWL_LOCAL_PORT (pStateFilterInfo, pStateFilterInfo->u1Protocol);
        }
        *pi4FwlStateRemotePort =
            FWL_REMOTE_PORT (pStateFilterInfo, pStateFilterInfo->u1Protocol);

        *pi4FwlStateDirection = pStateFilterInfo->u1Direction;
        return SNMP_SUCCESS;
    }

    /* Search in the TCP init flow table. */
    pStateFilterInfo = (tStatefulSessionNode *)
        TMO_SLL_First (&gFwlTcpInitFlowList);
    if (pStateFilterInfo != NULL)
    {
        *pi4FwlStateType = FWL_INITFLOW;
        *pi4FwlStateLocalIpAddrType = IPVX_ADDR_FMLY_IPV4;
        pFwlStateLocalIpAddress->i4_Length = FWL_IPV4_PREFIX_LEN;
        MEMCPY (pFwlStateLocalIpAddress->pu1_OctetList,
                (UINT1 *) &(pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr),
                FWL_IPV4_PREFIX_LEN);
        *pi4FwlStateRemoteIpAddrType = IPVX_ADDR_FMLY_IPV4;
        pFwlStateRemoteIpAddress->i4_Length = FWL_IPV4_PREFIX_LEN;
        MEMCPY (pFwlStateRemoteIpAddress->pu1_OctetList,
                (UINT1 *) &(pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr),
                FWL_IPV4_PREFIX_LEN);
        *pi4FwlStateProtocol = pStateFilterInfo->u1Protocol;
        if (pStateFilterInfo->u1Protocol == FWL_ICMP)
        {
            *pi4FwlStateLocalPort = FWL_ICMP_IP_ID (pStateFilterInfo);
        }
        else
        {
            *pi4FwlStateLocalPort =
                FWL_LOCAL_PORT (pStateFilterInfo, pStateFilterInfo->u1Protocol);
        }
        *pi4FwlStateRemotePort =
            FWL_REMOTE_PORT (pStateFilterInfo, pStateFilterInfo->u1Protocol);

        *pi4FwlStateDirection = pStateFilterInfo->u1Direction;
        return SNMP_SUCCESS;
    }

    /* Search in the Ipv6 TCP init flow table. */
    pStateFilterInfo = (tStatefulSessionNode *)
        TMO_SLL_First (&gFwlV6TcpInitFlowList);
    if (pStateFilterInfo != NULL)
    {
        *pi4FwlStateType = FWL_INITFLOW;
        *pi4FwlStateLocalIpAddrType = IPVX_ADDR_FMLY_IPV6;
        pFwlStateLocalIpAddress->i4_Length = FWL_IPV6_PREFIX_LEN;
        MEMCPY (pFwlStateLocalIpAddress->pu1_OctetList,
                (UINT1 *) &(pStateFilterInfo->LocalIP.uIpAddr.Ip6Addr),
                FWL_IPV6_PREFIX_LEN);
        *pi4FwlStateRemoteIpAddrType = IPVX_ADDR_FMLY_IPV6;
        pFwlStateRemoteIpAddress->i4_Length = FWL_IPV6_PREFIX_LEN;
        MEMCPY (pFwlStateRemoteIpAddress->pu1_OctetList,
                (UINT1 *) &(pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr),
                FWL_IPV6_PREFIX_LEN);
        *pi4FwlStateProtocol = pStateFilterInfo->u1Protocol;
        if (pStateFilterInfo->u1Protocol == FWL_ICMPV6)
        {
            *pi4FwlStateLocalPort = FWL_ICMP_IP_ID (pStateFilterInfo);
        }
        else
        {
            *pi4FwlStateLocalPort =
                FWL_LOCAL_PORT (pStateFilterInfo, pStateFilterInfo->u1Protocol);
        }
        *pi4FwlStateRemotePort =
            FWL_REMOTE_PORT (pStateFilterInfo, pStateFilterInfo->u1Protocol);

        *pi4FwlStateDirection = pStateFilterInfo->u1Direction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFwlStateTable
 Input       :  The Indices
                FwlStateType
                nextFwlStateType
                FwlStateLocalIpAddrType
                nextFwlStateLocalIpAddrType
                FwlStateLocalIpAddress
                nextFwlStateLocalIpAddress
                FwlStateRemoteIpAddrType
                nextFwlStateRemoteIpAddrType
                FwlStateRemoteIpAddress
                nextFwlStateRemoteIpAddress
                FwlStateLocalPort
                nextFwlStateLocalPort
                FwlStateRemotePort
                nextFwlStateRemotePort
                FwlStateProtocol
                nextFwlStateProtocol
                FwlStateDirection
                nextFwlStateDirection
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFwlStateTable (INT4 i4FwlStateType, INT4 *pi4NextFwlStateType,
                              INT4 i4FwlStateLocalIpAddrType,
                              INT4 *pi4NextFwlStateLocalIpAddrType,
                              tSNMP_OCTET_STRING_TYPE * pFwlStateLocalIpAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pNextFwlStateLocalIpAddress,
                              INT4 i4FwlStateRemoteIpAddrType,
                              INT4 *pi4NextFwlStateRemoteIpAddrType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFwlStateRemoteIpAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pNextFwlStateRemoteIpAddress,
                              INT4 i4FwlStateLocalPort,
                              INT4 *pi4NextFwlStateLocalPort,
                              INT4 i4FwlStateRemotePort,
                              INT4 *pi4NextFwlStateRemotePort,
                              INT4 i4FwlStateProtocol,
                              INT4 *pi4NextFwlStateProtocol,
                              INT4 i4FwlStateDirection,
                              INT4 *pi4NextFwlStateDirection)
{
    tStatefulSessionNode *pStateFilterInfo = NULL;
    tFwlIpAddr          FwlLocalIpAddr;
    tFwlIpAddr          FwlRemoteIpAddr;
    UINT4               u4HashIndex = FWL_ZERO;
    INT1                i1EntryFound = FWL_FALSE;
    INT1                i1BreakFlag = FWL_FALSE;

    MEMSET (&FwlRemoteIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));
    MEMSET (&FwlLocalIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));

    if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4) &&
        (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV4))
    {

        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             pFwlStateLocalIpAddress, &FwlLocalIpAddr);
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             pFwlStateRemoteIpAddress, &FwlRemoteIpAddr);
    }
    else if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6) &&
             (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV6))
    {
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV6,
                             pFwlStateLocalIpAddress, &FwlLocalIpAddr);
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV6,
                             pFwlStateRemoteIpAddress, &FwlRemoteIpAddr);
    }

    switch (i4FwlStateType)
    {
        case FWL_STATEFUL:
            if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4) &&
                (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV4))
            {
                TMO_HASH_Scan_Table (FWL_STATE_HASH_LIST, u4HashIndex)
                {
                    if ((i1BreakFlag == FWL_TRUE) && (i1EntryFound == FWL_TRUE))
                    {
                        *pi4NextFwlStateType = FWL_STATEFUL;
                        break;
                    }
                    pStateFilterInfo = NULL;
                    while (FwlStateGetStatefulTableEntries (u4HashIndex,
                                                            &pStateFilterInfo)
                           == FWL_SUCCESS)
                    {
                        if (i1EntryFound == FWL_TRUE)
                        {
                            i1BreakFlag = FWL_TRUE;
                            *pi4NextFwlStateType = FWL_STATEFUL;
                            break;
                        }

                        if ((FWL_ZERO == FwlLocalIpAddr.uIpAddr.Ip4Addr) &&
                            (FWL_ZERO == FwlRemoteIpAddr.uIpAddr.Ip4Addr) &&
                            (FWL_ZERO == i4FwlStateProtocol))
                        {
                            i1EntryFound = FWL_TRUE;
                            i1BreakFlag = FWL_TRUE;
                            *pi4NextFwlStateType = FWL_STATEFUL;
                            break;
                        }

                        if (pStateFilterInfo != NULL)
                        {
                            if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                                 pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                                (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                                 pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                                (i4FwlStateProtocol ==
                                 pStateFilterInfo->u1Protocol) &&
                                (FWL_SUCCESS == FwlStatePortMatchExists
                                 (pStateFilterInfo, i4FwlStateLocalPort,
                                  i4FwlStateRemotePort)) &&
                                (i4FwlStateDirection ==
                                 pStateFilterInfo->u1Direction))
                            {
                                i1EntryFound = FWL_TRUE;
                            }
                        }
                    }            /* End of while */
                }                /* End of hash */
            }
            else if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6) &&
                     (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV6))
            {
                TMO_HASH_Scan_Table (FWL_STATE_V6_HASH_LIST, u4HashIndex)
                {
                    if ((i1BreakFlag == FWL_TRUE) && (i1EntryFound == FWL_TRUE))
                    {
                        *pi4NextFwlStateType = FWL_STATEFUL;
                        break;
                    }
                    pStateFilterInfo = NULL;
                    while (FwlStateGetIpv6StatefulTableEntries (u4HashIndex,
                                                                &pStateFilterInfo)
                           == FWL_SUCCESS)
                    {
                        if (i1EntryFound == FWL_TRUE)
                        {
                            i1BreakFlag = FWL_TRUE;
                            *pi4NextFwlStateType = FWL_STATEFUL;
                            break;
                        }

                        if ((IS_ADDR_UNSPECIFIED
                             (FwlLocalIpAddr.uIpAddr.Ip6Addr)) &&
                            (IS_ADDR_UNSPECIFIED
                             (FwlRemoteIpAddr.uIpAddr.Ip6Addr)) &&
                            (FWL_ZERO == i4FwlStateProtocol))
                        {
                            i1EntryFound = FWL_TRUE;
                            i1BreakFlag = FWL_TRUE;
                            *pi4NextFwlStateType = FWL_STATEFUL;
                            break;
                        }

                        if (pStateFilterInfo != NULL)
                        {
                            if ((Ip6AddrCompare
                                 (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                  pStateFilterInfo->LocalIP.uIpAddr.Ip6Addr) ==
                                 FWL_ZERO)
                                &&
                                (Ip6AddrCompare
                                 (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                                  pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                                 FWL_ZERO)
                                && (i4FwlStateProtocol ==
                                    pStateFilterInfo->u1Protocol)
                                && (FWL_SUCCESS ==
                                    FwlStatePortMatchExists (pStateFilterInfo,
                                                             i4FwlStateLocalPort,
                                                             i4FwlStateRemotePort))
                                && (i4FwlStateDirection ==
                                    pStateFilterInfo->u1Direction))
                            {
                                i1EntryFound = FWL_TRUE;
                            }
                        }
                    }            /* End of while */
                }                /* End of hash */
            }

            if ((NULL == pStateFilterInfo) && (i1EntryFound == FWL_TRUE))
            {
                /* The current entry is the last element in the Stateful entry
                 * table.Hence fetching the first element from Partial Entry
                 * table. 
                 */
                /* Search in the partial entry table. */
                if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4) &&
                    (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV4))
                {
                    pStateFilterInfo = (tStatefulSessionNode *)
                        TMO_SLL_First (&gFwlPartialLinksList);
                }
                else if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6) &&
                         (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV6))
                {
                    pStateFilterInfo = (tStatefulSessionNode *)
                        TMO_SLL_First (&gFwlV6PartialLinksList);
                }
                if (pStateFilterInfo != NULL)
                {
                    *pi4NextFwlStateType = FWL_PARTIAL;
                    break;
                }
                /* If the partial entry table is null */
                if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4) &&
                    (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV4))
                {
                    pStateFilterInfo = (tStatefulSessionNode *)
                        TMO_SLL_First (&gFwlTcpInitFlowList);
                }
                else if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6) &&
                         (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV6))
                {
                    pStateFilterInfo = (tStatefulSessionNode *)
                        TMO_SLL_First (&gFwlV6TcpInitFlowList);
                }
                if (pStateFilterInfo != NULL)
                {
                    *pi4NextFwlStateType = FWL_INITFLOW;
                    break;
                }
                else
                {
                    return SNMP_FAILURE;
                }

            }
            break;
        case FWL_PARTIAL:
            if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4) &&
                (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV4))
            {
                pStateFilterInfo = (tStatefulSessionNode *)
                    TMO_SLL_First (&gFwlPartialLinksList);

                if (pStateFilterInfo == NULL)
                {
                    return SNMP_FAILURE;
                }

                while (pStateFilterInfo != NULL)
                {
                    if (i1EntryFound == FWL_TRUE)
                    {
                        *pi4NextFwlStateType = FWL_PARTIAL;
                        break;
                    }

                    if ((FWL_ZERO == FwlLocalIpAddr.uIpAddr.Ip4Addr) &&
                        (FWL_ZERO == FwlRemoteIpAddr.uIpAddr.Ip4Addr) &&
                        (FWL_ZERO == i4FwlStateProtocol))
                    {
                        i1EntryFound = FWL_TRUE;
                        *pi4NextFwlStateType = FWL_PARTIAL;
                        break;
                    }

                    if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                        (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                        (i4FwlStateProtocol ==
                         pStateFilterInfo->u1Protocol) &&
                        (FWL_SUCCESS == FwlStatePortMatchExists
                         (pStateFilterInfo, i4FwlStateLocalPort,
                          i4FwlStateRemotePort)) &&
                        (i4FwlStateDirection == pStateFilterInfo->u1Direction))
                    {
                        i1EntryFound = FWL_TRUE;
                    }
                    pStateFilterInfo =
                        (tStatefulSessionNode *)
                        TMO_SLL_Next (&gFwlPartialLinksList,
                                      (tTMO_SLL_NODE *) pStateFilterInfo);

                }                /* End of while */
            }
            if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6) &&
                (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV6))
            {
                pStateFilterInfo = (tStatefulSessionNode *)
                    TMO_SLL_First (&gFwlV6PartialLinksList);

                if (pStateFilterInfo == NULL)
                {
                    return SNMP_FAILURE;
                }

                while (pStateFilterInfo != NULL)
                {
                    if (i1EntryFound == FWL_TRUE)
                    {
                        *pi4NextFwlStateType = FWL_PARTIAL;
                        break;
                    }

                    if ((IS_ADDR_UNSPECIFIED
                         (FwlLocalIpAddr.uIpAddr.Ip6Addr)) &&
                        (IS_ADDR_UNSPECIFIED
                         (FwlRemoteIpAddr.uIpAddr.Ip6Addr)) &&
                        (FWL_ZERO == i4FwlStateProtocol))
                    {
                        i1EntryFound = FWL_TRUE;
                        *pi4NextFwlStateType = FWL_PARTIAL;
                        break;
                    }

                    if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                         pStateFilterInfo->LocalIP.uIpAddr.
                                         Ip6Addr) == FWL_ZERO)
                        &&
                        (Ip6AddrCompare
                         (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                          pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                         FWL_ZERO)
                        && (i4FwlStateProtocol == pStateFilterInfo->u1Protocol)
                        && (FWL_SUCCESS ==
                            FwlStatePortMatchExists (pStateFilterInfo,
                                                     i4FwlStateLocalPort,
                                                     i4FwlStateRemotePort))
                        && (i4FwlStateDirection ==
                            pStateFilterInfo->u1Direction))

                    {
                        i1EntryFound = FWL_TRUE;
                    }
                    pStateFilterInfo =
                        (tStatefulSessionNode *)
                        TMO_SLL_Next (&gFwlV6PartialLinksList,
                                      (tTMO_SLL_NODE *) pStateFilterInfo);

                }                /* End of while */
            }

            if ((NULL == pStateFilterInfo) && (i1EntryFound == FWL_TRUE))
            {
                /* The current entry is the last element in the Stateful entry
                 * table.Hence fetching the first element from  init flow 
                 * table. 
                 */
                if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4) &&
                    (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV4))
                {
                    pStateFilterInfo = (tStatefulSessionNode *)
                        TMO_SLL_First (&gFwlTcpInitFlowList);
                }
                else if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6) &&
                         (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV6))
                {
                    pStateFilterInfo = (tStatefulSessionNode *)
                        TMO_SLL_First (&gFwlV6TcpInitFlowList);
                }
                if (pStateFilterInfo != NULL)
                {
                    *pi4NextFwlStateType = FWL_INITFLOW;
                    break;
                }
                else
                {
                    return SNMP_FAILURE;
                }
            }

            break;

        case FWL_INITFLOW:
            if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4) &&
                (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV4))
            {
                TMO_SLL_Scan (&gFwlTcpInitFlowList, pStateFilterInfo,
                              tStatefulSessionNode *)
                {
                    if (i1EntryFound == FWL_TRUE)
                    {
                        *pi4NextFwlStateType = FWL_INITFLOW;
                        break;
                    }

                    if ((FWL_ZERO == FwlLocalIpAddr.uIpAddr.Ip4Addr) &&
                        (FWL_ZERO == FwlRemoteIpAddr.uIpAddr.Ip4Addr) &&
                        (FWL_ZERO == i4FwlStateProtocol))
                    {
                        i1EntryFound = FWL_TRUE;
                        *pi4NextFwlStateType = FWL_INITFLOW;
                        break;
                    }

                    if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                        (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                        (i4FwlStateProtocol ==
                         pStateFilterInfo->u1Protocol) &&
                        (FWL_SUCCESS == FwlStatePortMatchExists
                         (pStateFilterInfo, i4FwlStateLocalPort,
                          i4FwlStateRemotePort)) &&
                        (i4FwlStateDirection == pStateFilterInfo->u1Direction))
                    {
                        i1EntryFound = FWL_TRUE;
                    }
                }                /* End of scan */
            }
            if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6) &&
                (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV6))
            {
                TMO_SLL_Scan (&gFwlV6TcpInitFlowList, pStateFilterInfo,
                              tStatefulSessionNode *)
                {
                    if (i1EntryFound == FWL_TRUE)
                    {
                        *pi4NextFwlStateType = FWL_INITFLOW;
                        break;
                    }

                    if ((IS_ADDR_UNSPECIFIED
                         (FwlLocalIpAddr.uIpAddr.Ip6Addr)) &&
                        (IS_ADDR_UNSPECIFIED
                         (FwlRemoteIpAddr.uIpAddr.Ip6Addr)) &&
                        (FWL_ZERO == i4FwlStateProtocol))
                    {
                        i1EntryFound = FWL_TRUE;
                        *pi4NextFwlStateType = FWL_INITFLOW;
                        break;
                    }

                    if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                         pStateFilterInfo->LocalIP.uIpAddr.
                                         Ip6Addr) == FWL_ZERO)
                        &&
                        (Ip6AddrCompare
                         (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                          pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                         FWL_ZERO)
                        && (i4FwlStateProtocol == pStateFilterInfo->u1Protocol)
                        && (FWL_SUCCESS ==
                            FwlStatePortMatchExists (pStateFilterInfo,
                                                     i4FwlStateLocalPort,
                                                     i4FwlStateRemotePort))
                        && (i4FwlStateDirection ==
                            pStateFilterInfo->u1Direction))
                    {
                        i1EntryFound = FWL_TRUE;
                    }
                }                /* End of scan */
            }

            if (((pStateFilterInfo == NULL) &&
                 (FWL_TRUE == i1EntryFound)) || (FWL_FALSE == i1EntryFound))
            {
                return SNMP_FAILURE;
            }

            break;
        default:
            return SNMP_FAILURE;
    }                            /* End of switch */
    if (FWL_TRUE == i1EntryFound)
    {
        if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4) &&
            (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV4))
        {
            *pi4NextFwlStateLocalIpAddrType = IPVX_ADDR_FMLY_IPV4;
            pNextFwlStateLocalIpAddress->i4_Length = FWL_IPV4_PREFIX_LEN;
            MEMCPY (pNextFwlStateLocalIpAddress->pu1_OctetList,
                    (UINT1 *) &(pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr),
                    FWL_IPV4_PREFIX_LEN);
            *pi4NextFwlStateRemoteIpAddrType = IPVX_ADDR_FMLY_IPV4;
            pNextFwlStateLocalIpAddress->i4_Length = FWL_IPV4_PREFIX_LEN;
            MEMCPY (pNextFwlStateRemoteIpAddress->pu1_OctetList,
                    (UINT1 *) &(pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr),
                    FWL_IPV4_PREFIX_LEN);
        }
        else if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6) &&
                 (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV6))
        {
            *pi4NextFwlStateLocalIpAddrType = IPVX_ADDR_FMLY_IPV6;
            pNextFwlStateLocalIpAddress->i4_Length = FWL_IPV6_PREFIX_LEN;
            MEMCPY (pNextFwlStateLocalIpAddress->pu1_OctetList,
                    (UINT1 *) &(pStateFilterInfo->LocalIP.uIpAddr.Ip6Addr),
                    FWL_IPV6_PREFIX_LEN);
            *pi4NextFwlStateRemoteIpAddrType = IPVX_ADDR_FMLY_IPV6;
            pNextFwlStateLocalIpAddress->i4_Length = FWL_IPV6_PREFIX_LEN;
            MEMCPY (pNextFwlStateRemoteIpAddress->pu1_OctetList,
                    (UINT1 *) &(pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr),
                    FWL_IPV6_PREFIX_LEN);
        }

        *pi4NextFwlStateProtocol = pStateFilterInfo->u1Protocol;

        *pi4NextFwlStateDirection = pStateFilterInfo->u1Direction;

        if (pStateFilterInfo->u1Protocol == FWL_ICMP)
        {
            *pi4NextFwlStateLocalPort = FWL_ICMP_IP_ID (pStateFilterInfo);
        }
        else
        {
            *pi4NextFwlStateLocalPort =
                FWL_LOCAL_PORT (pStateFilterInfo, pStateFilterInfo->u1Protocol);
        }
        *pi4NextFwlStateRemotePort =
            FWL_REMOTE_PORT (pStateFilterInfo, pStateFilterInfo->u1Protocol);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlStateEstablishedTime
 Input       :  The Indices
                FwlStateType
                FwlStateLocalIpAddrType
                FwlStateLocalIpAddress
                FwlStateRemoteIpAddrType
                FwlStateRemoteIpAddress
                FwlStateLocalPort
                FwlStateRemotePort
                FwlStateProtocol
                FwlStateDirection

                The Object 
                retValFwlStateEstablishedTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStateEstablishedTime (INT4 i4FwlStateType,
                               INT4 i4FwlStateLocalIpAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFwlStateLocalIpAddress,
                               INT4 i4FwlStateRemoteIpAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFwlStateRemoteIpAddress,
                               INT4 i4FwlStateLocalPort,
                               INT4 i4FwlStateRemotePort,
                               INT4 i4FwlStateProtocol,
                               INT4 i4FwlStateDirection,
                               UINT4 *pu4RetValFwlStateEstablishedTime)
{
    tStatefulSessionNode *pStateFilterInfo = NULL;
    tFwlIpAddr          FwlLocalIpAddr;
    tFwlIpAddr          FwlRemoteIpAddr;
    UINT4               u4HashIndex = FWL_ZERO;
    INT1                i1RetVal = SNMP_SUCCESS;

    MEMSET (&FwlRemoteIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));
    MEMSET (&FwlLocalIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));

    if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4) &&
        (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV4))
    {
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             pFwlStateLocalIpAddress, &FwlLocalIpAddr);
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             pFwlStateRemoteIpAddress, &FwlRemoteIpAddr);
    }
    else if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6) &&
             (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV6))
    {
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV6,
                             pFwlStateLocalIpAddress, &FwlLocalIpAddr);
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV6,
                             pFwlStateRemoteIpAddress, &FwlRemoteIpAddr);
    }

    switch (i4FwlStateType)
    {
        case FWL_STATEFUL:
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                TMO_HASH_Scan_Table (FWL_STATE_HASH_LIST, u4HashIndex)
                {
                    pStateFilterInfo = NULL;
                    while (FwlStateGetStatefulTableEntries (u4HashIndex,
                                                            &pStateFilterInfo)
                           == FWL_SUCCESS)
                    {
                        if (pStateFilterInfo != NULL)
                        {
                            if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                                 pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                                (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                                 pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                                (i4FwlStateProtocol ==
                                 pStateFilterInfo->u1Protocol) &&
                                (FWL_SUCCESS == FwlStatePortMatchExists
                                 (pStateFilterInfo, i4FwlStateLocalPort,
                                  i4FwlStateRemotePort)) &&
                                (i4FwlStateDirection ==
                                 pStateFilterInfo->u1Direction))
                            {
                                *pu4RetValFwlStateEstablishedTime =
                                    pStateFilterInfo->u4Timestamp;
                                i1RetVal = SNMP_SUCCESS;
                                return SNMP_SUCCESS;
                            }

                        }
                    }            /* End of while */
                }
            }
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                TMO_HASH_Scan_Table (FWL_STATE_V6_HASH_LIST, u4HashIndex)
                {
                    pStateFilterInfo = NULL;
                    while (FwlStateGetIpv6StatefulTableEntries (u4HashIndex,
                                                                &pStateFilterInfo)
                           == FWL_SUCCESS)
                    {
                        if (pStateFilterInfo != NULL)
                        {
                            if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                                 pStateFilterInfo->LocalIP.
                                                 uIpAddr.Ip6Addr) == FWL_ZERO)
                                &&
                                (Ip6AddrCompare
                                 (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                                  pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                                 FWL_ZERO)
                                && (i4FwlStateProtocol ==
                                    pStateFilterInfo->u1Protocol)
                                && (FWL_SUCCESS ==
                                    FwlStatePortMatchExists (pStateFilterInfo,
                                                             i4FwlStateLocalPort,
                                                             i4FwlStateRemotePort))
                                && (i4FwlStateDirection ==
                                    pStateFilterInfo->u1Direction))
                            {
                                *pu4RetValFwlStateEstablishedTime =
                                    pStateFilterInfo->u4Timestamp;
                                i1RetVal = SNMP_SUCCESS;
                                return SNMP_SUCCESS;
                            }

                        }
                    }            /* End of while */
                }
            }

            break;
        case FWL_PARTIAL:
            if (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                pStateFilterInfo = (tStatefulSessionNode *)
                    TMO_SLL_First (&gFwlPartialLinksList);

                if (pStateFilterInfo == NULL)
                {
                    return SNMP_FAILURE;
                }

                while (pStateFilterInfo != NULL)
                {
                    if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                        (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                        (i4FwlStateProtocol ==
                         pStateFilterInfo->u1Protocol) &&
                        (FWL_SUCCESS == FwlStatePortMatchExists
                         (pStateFilterInfo, i4FwlStateLocalPort,
                          i4FwlStateRemotePort)) &&
                        (i4FwlStateDirection == pStateFilterInfo->u1Direction))
                    {
                        *pu4RetValFwlStateEstablishedTime =
                            pStateFilterInfo->u4Timestamp;
                        i1RetVal = SNMP_SUCCESS;
                        return SNMP_SUCCESS;
                    }
                    pStateFilterInfo =
                        (tStatefulSessionNode *)
                        TMO_SLL_Next (&gFwlPartialLinksList,
                                      (tTMO_SLL_NODE *) pStateFilterInfo);
                }                /* End of while */

            }
            if (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                pStateFilterInfo = (tStatefulSessionNode *)
                    TMO_SLL_First (&gFwlV6PartialLinksList);

                if (pStateFilterInfo == NULL)
                {
                    return SNMP_FAILURE;
                }

                while (pStateFilterInfo != NULL)
                {
                    if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                         pStateFilterInfo->LocalIP.uIpAddr.
                                         Ip6Addr) == FWL_ZERO)
                        &&
                        (Ip6AddrCompare
                         (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                          pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                         FWL_ZERO)
                        && (i4FwlStateProtocol == pStateFilterInfo->u1Protocol)
                        && (FWL_SUCCESS ==
                            FwlStatePortMatchExists (pStateFilterInfo,
                                                     i4FwlStateLocalPort,
                                                     i4FwlStateRemotePort))
                        && (i4FwlStateDirection ==
                            pStateFilterInfo->u1Direction))
                    {
                        *pu4RetValFwlStateEstablishedTime =
                            pStateFilterInfo->u4Timestamp;
                        i1RetVal = SNMP_SUCCESS;
                        return SNMP_SUCCESS;
                    }
                    pStateFilterInfo =
                        (tStatefulSessionNode *)
                        TMO_SLL_Next (&gFwlV6PartialLinksList,
                                      (tTMO_SLL_NODE *) pStateFilterInfo);
                }                /* End of while */

            }

            break;
        case FWL_INITFLOW:
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                TMO_SLL_Scan (&gFwlTcpInitFlowList, pStateFilterInfo,
                              tStatefulSessionNode *)
                {
                    if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                        (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                        (i4FwlStateProtocol ==
                         pStateFilterInfo->u1Protocol) &&
                        (FWL_SUCCESS == FwlStatePortMatchExists
                         (pStateFilterInfo, i4FwlStateLocalPort,
                          i4FwlStateRemotePort)) &&
                        (i4FwlStateDirection == pStateFilterInfo->u1Direction))
                    {
                        *pu4RetValFwlStateEstablishedTime =
                            pStateFilterInfo->u4Timestamp;
                        i1RetVal = SNMP_SUCCESS;
                        return SNMP_SUCCESS;
                    }

                }                /* End of scan */
            }
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                TMO_SLL_Scan (&gFwlV6TcpInitFlowList, pStateFilterInfo,
                              tStatefulSessionNode *)
                {
                    if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                         pStateFilterInfo->LocalIP.uIpAddr.
                                         Ip6Addr) == FWL_ZERO)
                        &&
                        (Ip6AddrCompare
                         (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                          pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                         FWL_ZERO)
                        && (i4FwlStateProtocol == pStateFilterInfo->u1Protocol)
                        && (FWL_SUCCESS ==
                            FwlStatePortMatchExists (pStateFilterInfo,
                                                     i4FwlStateLocalPort,
                                                     i4FwlStateRemotePort))
                        && (i4FwlStateDirection ==
                            pStateFilterInfo->u1Direction))
                    {
                        *pu4RetValFwlStateEstablishedTime =
                            pStateFilterInfo->u4Timestamp;
                        i1RetVal = SNMP_SUCCESS;
                        return SNMP_SUCCESS;
                    }

                }                /* End of scan */
            }

            break;
        default:
            i1RetVal = SNMP_FAILURE;
            break;
    }                            /* End of switch */

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFwlStateLocalState
 Input       :  The Indices
                FwlStateType
                FwlStateLocalIpAddrType
                FwlStateLocalIpAddress
                FwlStateRemoteIpAddrType
                FwlStateRemoteIpAddress
                FwlStateLocalPort
                FwlStateRemotePort
                FwlStateProtocol
                FwlStateDirection

                The Object 
                retValFwlStateLocalState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStateLocalState (INT4 i4FwlStateType, INT4 i4FwlStateLocalIpAddrType,
                          tSNMP_OCTET_STRING_TYPE * pFwlStateLocalIpAddress,
                          INT4 i4FwlStateRemoteIpAddrType,
                          tSNMP_OCTET_STRING_TYPE * pFwlStateRemoteIpAddress,
                          INT4 i4FwlStateLocalPort, INT4 i4FwlStateRemotePort,
                          INT4 i4FwlStateProtocol, INT4 i4FwlStateDirection,
                          INT4 *pi4RetValFwlStateLocalState)
{
    tStatefulSessionNode *pStateFilterInfo = NULL;
    tFwlIpAddr          FwlLocalIpAddr;
    tFwlIpAddr          FwlRemoteIpAddr;
    UINT4               u4HashIndex = FWL_ZERO;
    INT1                i1RetVal = SNMP_SUCCESS;

    MEMSET (&FwlRemoteIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));
    MEMSET (&FwlLocalIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));

    if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4) &&
        (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV4))
    {
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             pFwlStateLocalIpAddress, &FwlLocalIpAddr);
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             pFwlStateRemoteIpAddress, &FwlRemoteIpAddr);
    }
    else if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6) &&
             (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV6))
    {
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV6,
                             pFwlStateLocalIpAddress, &FwlLocalIpAddr);
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV6,
                             pFwlStateRemoteIpAddress, &FwlRemoteIpAddr);
    }

    switch (i4FwlStateType)
    {
        case FWL_STATEFUL:
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                TMO_HASH_Scan_Table (FWL_STATE_HASH_LIST, u4HashIndex)
                {
                    pStateFilterInfo = NULL;
                    while (FwlStateGetStatefulTableEntries (u4HashIndex,
                                                            &pStateFilterInfo)
                           == FWL_SUCCESS)
                    {
                        if (pStateFilterInfo != NULL)
                        {
                            if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                                 pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                                (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                                 pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                                (i4FwlStateProtocol ==
                                 pStateFilterInfo->u1Protocol) &&
                                (FWL_SUCCESS == FwlStatePortMatchExists
                                 (pStateFilterInfo, i4FwlStateLocalPort,
                                  i4FwlStateRemotePort)) &&
                                (i4FwlStateDirection ==
                                 pStateFilterInfo->u1Direction))
                            {
                                *pi4RetValFwlStateLocalState =
                                    pStateFilterInfo->u1LocalState;
                                i1RetVal = SNMP_SUCCESS;
                                return SNMP_SUCCESS;
                            }

                        }
                    }            /* End of while */
                }
            }
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                TMO_HASH_Scan_Table (FWL_STATE_V6_HASH_LIST, u4HashIndex)
                {
                    pStateFilterInfo = NULL;
                    while (FwlStateGetIpv6StatefulTableEntries (u4HashIndex,
                                                                &pStateFilterInfo)
                           == FWL_SUCCESS)
                    {
                        if (pStateFilterInfo != NULL)
                        {
                            if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                                 pStateFilterInfo->LocalIP.
                                                 uIpAddr.Ip6Addr) == FWL_ZERO)
                                &&
                                (Ip6AddrCompare
                                 (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                                  pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                                 FWL_ZERO)
                                && (i4FwlStateProtocol ==
                                    pStateFilterInfo->u1Protocol)
                                && (FWL_SUCCESS ==
                                    FwlStatePortMatchExists (pStateFilterInfo,
                                                             i4FwlStateLocalPort,
                                                             i4FwlStateRemotePort))
                                && (i4FwlStateDirection ==
                                    pStateFilterInfo->u1Direction))
                            {
                                *pi4RetValFwlStateLocalState =
                                    pStateFilterInfo->u1LocalState;
                                i1RetVal = SNMP_SUCCESS;
                                return SNMP_SUCCESS;
                            }

                        }
                    }            /* End of while */
                }
            }

            break;
        case FWL_PARTIAL:
            if (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                pStateFilterInfo = (tStatefulSessionNode *)
                    TMO_SLL_First (&gFwlPartialLinksList);

                if (pStateFilterInfo == NULL)
                {
                    return SNMP_FAILURE;
                }

                while (pStateFilterInfo != NULL)
                {
                    if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                        (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                        (i4FwlStateProtocol ==
                         pStateFilterInfo->u1Protocol) &&
                        (FWL_SUCCESS == FwlStatePortMatchExists
                         (pStateFilterInfo, i4FwlStateLocalPort,
                          i4FwlStateRemotePort)) &&
                        (i4FwlStateDirection == pStateFilterInfo->u1Direction))
                    {
                        *pi4RetValFwlStateLocalState =
                            pStateFilterInfo->u1LocalState;
                        i1RetVal = SNMP_SUCCESS;
                        return SNMP_SUCCESS;
                    }
                    pStateFilterInfo =
                        (tStatefulSessionNode *)
                        TMO_SLL_Next (&gFwlPartialLinksList,
                                      (tTMO_SLL_NODE *) pStateFilterInfo);
                }                /* End of while */

            }
            if (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                pStateFilterInfo = (tStatefulSessionNode *)
                    TMO_SLL_First (&gFwlV6PartialLinksList);

                if (pStateFilterInfo == NULL)
                {
                    return SNMP_FAILURE;
                }

                while (pStateFilterInfo != NULL)
                {
                    if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                         pStateFilterInfo->LocalIP.uIpAddr.
                                         Ip6Addr) == FWL_ZERO)
                        &&
                        (Ip6AddrCompare
                         (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                          pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                         FWL_ZERO)
                        && (i4FwlStateProtocol == pStateFilterInfo->u1Protocol)
                        && (FWL_SUCCESS ==
                            FwlStatePortMatchExists (pStateFilterInfo,
                                                     i4FwlStateLocalPort,
                                                     i4FwlStateRemotePort))
                        && (i4FwlStateDirection ==
                            pStateFilterInfo->u1Direction))
                    {
                        *pi4RetValFwlStateLocalState =
                            pStateFilterInfo->u1LocalState;
                        i1RetVal = SNMP_SUCCESS;
                        return SNMP_SUCCESS;
                    }
                    pStateFilterInfo =
                        (tStatefulSessionNode *)
                        TMO_SLL_Next (&gFwlV6PartialLinksList,
                                      (tTMO_SLL_NODE *) pStateFilterInfo);
                }                /* End of while */

            }

            break;
        case FWL_INITFLOW:
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                TMO_SLL_Scan (&gFwlTcpInitFlowList, pStateFilterInfo,
                              tStatefulSessionNode *)
                {
                    if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                        (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                        (i4FwlStateProtocol ==
                         pStateFilterInfo->u1Protocol) &&
                        (FWL_SUCCESS == FwlStatePortMatchExists
                         (pStateFilterInfo, i4FwlStateLocalPort,
                          i4FwlStateRemotePort)) &&
                        (i4FwlStateDirection == pStateFilterInfo->u1Direction))
                    {
                        *pi4RetValFwlStateLocalState =
                            pStateFilterInfo->u1LocalState;
                        return SNMP_SUCCESS;
                    }

                }                /* End of scan */
            }

            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                TMO_SLL_Scan (&gFwlV6TcpInitFlowList, pStateFilterInfo,
                              tStatefulSessionNode *)
                {
                    if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                         pStateFilterInfo->LocalIP.uIpAddr.
                                         Ip6Addr) == FWL_ZERO)
                        &&
                        (Ip6AddrCompare
                         (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                          pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                         FWL_ZERO)
                        && (i4FwlStateProtocol == pStateFilterInfo->u1Protocol)
                        && (FWL_SUCCESS ==
                            FwlStatePortMatchExists (pStateFilterInfo,
                                                     i4FwlStateLocalPort,
                                                     i4FwlStateRemotePort))
                        && (i4FwlStateDirection ==
                            pStateFilterInfo->u1Direction))
                    {
                        *pi4RetValFwlStateLocalState =
                            pStateFilterInfo->u1LocalState;
                        return SNMP_SUCCESS;
                    }

                }                /* End of scan */
            }
            break;
        default:
            i1RetVal = SNMP_FAILURE;
            break;
    }                            /* End of switch */

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFwlStateRemoteState
 Input       :  The Indices
                FwlStateType
                FwlStateLocalIpAddrType
                FwlStateLocalIpAddress
                FwlStateRemoteIpAddrType
                FwlStateRemoteIpAddress
                FwlStateLocalPort
                FwlStateRemotePort
                FwlStateProtocol
                FwlStateDirection

                The Object 
                retValFwlStateRemoteState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStateRemoteState (INT4 i4FwlStateType, INT4 i4FwlStateLocalIpAddrType,
                           tSNMP_OCTET_STRING_TYPE * pFwlStateLocalIpAddress,
                           INT4 i4FwlStateRemoteIpAddrType,
                           tSNMP_OCTET_STRING_TYPE * pFwlStateRemoteIpAddress,
                           INT4 i4FwlStateLocalPort, INT4 i4FwlStateRemotePort,
                           INT4 i4FwlStateProtocol, INT4 i4FwlStateDirection,
                           INT4 *pi4RetValFwlStateRemoteState)
{
    tStatefulSessionNode *pStateFilterInfo = NULL;
    tFwlIpAddr          FwlLocalIpAddr;
    tFwlIpAddr          FwlRemoteIpAddr;
    UINT4               u4HashIndex = FWL_ZERO;
    INT1                i1RetVal = SNMP_SUCCESS;

    MEMSET (&FwlRemoteIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));
    MEMSET (&FwlLocalIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));

    if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4) &&
        (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV4))
    {
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             pFwlStateLocalIpAddress, &FwlLocalIpAddr);
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             pFwlStateRemoteIpAddress, &FwlRemoteIpAddr);
    }
    else if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6) &&
             (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV6))
    {
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV6,
                             pFwlStateLocalIpAddress, &FwlLocalIpAddr);
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV6,
                             pFwlStateRemoteIpAddress, &FwlRemoteIpAddr);
    }
    switch (i4FwlStateType)
    {
        case FWL_STATEFUL:
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                TMO_HASH_Scan_Table (FWL_STATE_HASH_LIST, u4HashIndex)
                {
                    pStateFilterInfo = NULL;
                    while (FwlStateGetStatefulTableEntries (u4HashIndex,
                                                            &pStateFilterInfo)
                           == FWL_SUCCESS)
                    {
                        if (pStateFilterInfo != NULL)
                        {
                            if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                                 pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                                (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                                 pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                                (i4FwlStateProtocol ==
                                 pStateFilterInfo->u1Protocol) &&
                                (FWL_SUCCESS == FwlStatePortMatchExists
                                 (pStateFilterInfo, i4FwlStateLocalPort,
                                  i4FwlStateRemotePort)) &&
                                (i4FwlStateDirection ==
                                 pStateFilterInfo->u1Direction))
                            {
                                *pi4RetValFwlStateRemoteState =
                                    pStateFilterInfo->u1RemoteState;
                                i1RetVal = SNMP_SUCCESS;
                                return SNMP_SUCCESS;
                            }

                        }
                    }            /* End of while */
                }
            }
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                TMO_HASH_Scan_Table (FWL_STATE_V6_HASH_LIST, u4HashIndex)
                {
                    pStateFilterInfo = NULL;
                    while (FwlStateGetIpv6StatefulTableEntries (u4HashIndex,
                                                                &pStateFilterInfo)
                           == FWL_SUCCESS)
                    {
                        if (pStateFilterInfo != NULL)
                        {
                            if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                                 pStateFilterInfo->LocalIP.
                                                 uIpAddr.Ip6Addr) == FWL_ZERO)
                                &&
                                (Ip6AddrCompare
                                 (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                                  pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                                 FWL_ZERO)
                                && (i4FwlStateProtocol ==
                                    pStateFilterInfo->u1Protocol)
                                && (FWL_SUCCESS ==
                                    FwlStatePortMatchExists (pStateFilterInfo,
                                                             i4FwlStateLocalPort,
                                                             i4FwlStateRemotePort))
                                && (i4FwlStateDirection ==
                                    pStateFilterInfo->u1Direction))
                            {
                                *pi4RetValFwlStateRemoteState =
                                    pStateFilterInfo->u1RemoteState;
                                i1RetVal = SNMP_SUCCESS;
                                return SNMP_SUCCESS;
                            }

                        }
                    }            /* End of while */
                }
            }
            break;
        case FWL_PARTIAL:
            if (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                pStateFilterInfo = (tStatefulSessionNode *)
                    TMO_SLL_First (&gFwlPartialLinksList);

                if (pStateFilterInfo == NULL)
                {
                    return SNMP_FAILURE;
                }

                while (pStateFilterInfo != NULL)
                {
                    if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                        (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                        (i4FwlStateProtocol ==
                         pStateFilterInfo->u1Protocol) &&
                        (FWL_SUCCESS == FwlStatePortMatchExists
                         (pStateFilterInfo, i4FwlStateLocalPort,
                          i4FwlStateRemotePort)) &&
                        (i4FwlStateDirection == pStateFilterInfo->u1Direction))
                    {
                        *pi4RetValFwlStateRemoteState =
                            pStateFilterInfo->u1RemoteState;
                        return SNMP_SUCCESS;
                    }
                    pStateFilterInfo =
                        (tStatefulSessionNode *)
                        TMO_SLL_Next (&gFwlPartialLinksList,
                                      (tTMO_SLL_NODE *) pStateFilterInfo);
                }                /* End of while */

            }
            if (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                pStateFilterInfo = (tStatefulSessionNode *)
                    TMO_SLL_First (&gFwlV6PartialLinksList);

                if (pStateFilterInfo == NULL)
                {
                    return SNMP_FAILURE;
                }

                while (pStateFilterInfo != NULL)
                {
                    if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                         pStateFilterInfo->LocalIP.uIpAddr.
                                         Ip6Addr) == FWL_ZERO)
                        &&
                        (Ip6AddrCompare
                         (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                          pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                         FWL_ZERO)
                        && (i4FwlStateProtocol == pStateFilterInfo->u1Protocol)
                        && (FWL_SUCCESS ==
                            FwlStatePortMatchExists (pStateFilterInfo,
                                                     i4FwlStateLocalPort,
                                                     i4FwlStateRemotePort))
                        && (i4FwlStateDirection ==
                            pStateFilterInfo->u1Direction))
                    {
                        *pi4RetValFwlStateRemoteState =
                            pStateFilterInfo->u1RemoteState;
                        return SNMP_SUCCESS;
                    }
                    pStateFilterInfo =
                        (tStatefulSessionNode *)
                        TMO_SLL_Next (&gFwlV6PartialLinksList,
                                      (tTMO_SLL_NODE *) pStateFilterInfo);
                }                /* End of while */

            }

            break;

        case FWL_INITFLOW:
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                TMO_SLL_Scan (&gFwlTcpInitFlowList, pStateFilterInfo,
                              tStatefulSessionNode *)
                {
                    if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                        (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                        (i4FwlStateProtocol ==
                         pStateFilterInfo->u1Protocol) &&
                        (FWL_SUCCESS == FwlStatePortMatchExists
                         (pStateFilterInfo, i4FwlStateLocalPort,
                          i4FwlStateRemotePort)) &&
                        (i4FwlStateDirection == pStateFilterInfo->u1Direction))
                    {
                        *pi4RetValFwlStateRemoteState =
                            pStateFilterInfo->u1RemoteState;
                        i1RetVal = SNMP_SUCCESS;
                        return SNMP_SUCCESS;
                    }

                }                /* End of scan */
            }
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                TMO_SLL_Scan (&gFwlV6TcpInitFlowList, pStateFilterInfo,
                              tStatefulSessionNode *)
                {
                    if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                         pStateFilterInfo->LocalIP.uIpAddr.
                                         Ip6Addr) == FWL_ZERO)
                        &&
                        (Ip6AddrCompare
                         (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                          pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                         FWL_ZERO)
                        && (i4FwlStateProtocol == pStateFilterInfo->u1Protocol)
                        && (FWL_SUCCESS ==
                            FwlStatePortMatchExists (pStateFilterInfo,
                                                     i4FwlStateLocalPort,
                                                     i4FwlStateRemotePort))
                        && (i4FwlStateDirection ==
                            pStateFilterInfo->u1Direction))

                    {
                        *pi4RetValFwlStateRemoteState =
                            pStateFilterInfo->u1RemoteState;
                        i1RetVal = SNMP_SUCCESS;
                        return SNMP_SUCCESS;
                    }

                }                /* End of scan */
            }

            break;
        default:
            i1RetVal = SNMP_FAILURE;
            break;
    }                            /* End of switch */

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFwlStateLogLevel
 Input       :  The Indices
                FwlStateType
                FwlStateLocalIpAddrType
                FwlStateLocalIpAddress
                FwlStateRemoteIpAddrType
                FwlStateRemoteIpAddress
                FwlStateLocalPort
                FwlStateRemotePort
                FwlStateProtocol
                FwlStateDirection

                The Object 
                retValFwlStateLogLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStateLogLevel (INT4 i4FwlStateType, INT4 i4FwlStateLocalIpAddrType,
                        tSNMP_OCTET_STRING_TYPE * pFwlStateLocalIpAddress,
                        INT4 i4FwlStateRemoteIpAddrType,
                        tSNMP_OCTET_STRING_TYPE * pFwlStateRemoteIpAddress,
                        INT4 i4FwlStateLocalPort, INT4 i4FwlStateRemotePort,
                        INT4 i4FwlStateProtocol, INT4 i4FwlStateDirection,
                        INT4 *pi4RetValFwlStateLogLevel)
{
    tStatefulSessionNode *pStateFilterInfo = NULL;
    tFwlIpAddr          FwlLocalIpAddr;
    tFwlIpAddr          FwlRemoteIpAddr;
    UINT4               u4HashIndex = FWL_ZERO;
    INT1                i1RetVal = SNMP_SUCCESS;

    MEMSET (&FwlRemoteIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));
    MEMSET (&FwlLocalIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));

    if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4) &&
        (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV4))
    {
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             pFwlStateLocalIpAddress, &FwlLocalIpAddr);
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             pFwlStateRemoteIpAddress, &FwlRemoteIpAddr);
    }
    else if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6) &&
             (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV6))
    {
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV6,
                             pFwlStateLocalIpAddress, &FwlLocalIpAddr);
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV6,
                             pFwlStateRemoteIpAddress, &FwlRemoteIpAddr);
    }

    switch (i4FwlStateType)
    {
        case FWL_STATEFUL:
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                TMO_HASH_Scan_Table (FWL_STATE_HASH_LIST, u4HashIndex)
                {
                    pStateFilterInfo = NULL;
                    while (FwlStateGetStatefulTableEntries (u4HashIndex,
                                                            &pStateFilterInfo)
                           == FWL_SUCCESS)
                    {
                        if (pStateFilterInfo != NULL)
                        {
                            if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                                 pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                                (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                                 pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                                (i4FwlStateProtocol ==
                                 pStateFilterInfo->u1Protocol) &&
                                (FWL_SUCCESS == FwlStatePortMatchExists
                                 (pStateFilterInfo, i4FwlStateLocalPort,
                                  i4FwlStateRemotePort)) &&
                                (i4FwlStateDirection ==
                                 pStateFilterInfo->u1Direction))
                            {
                                *pi4RetValFwlStateLogLevel =
                                    pStateFilterInfo->u1LogLevel;
                                i1RetVal = SNMP_SUCCESS;
                                return SNMP_SUCCESS;
                            }

                        }
                    }            /* End of while */
                }
            }
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                TMO_HASH_Scan_Table (FWL_STATE_V6_HASH_LIST, u4HashIndex)
                {
                    pStateFilterInfo = NULL;
                    while (FwlStateGetIpv6StatefulTableEntries (u4HashIndex,
                                                                &pStateFilterInfo)
                           == FWL_SUCCESS)
                    {
                        if (pStateFilterInfo != NULL)
                        {
                            if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                                 pStateFilterInfo->LocalIP.
                                                 uIpAddr.Ip6Addr) == FWL_ZERO)
                                &&
                                (Ip6AddrCompare
                                 (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                                  pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                                 FWL_ZERO)
                                && (i4FwlStateProtocol ==
                                    pStateFilterInfo->u1Protocol)
                                && (FWL_SUCCESS ==
                                    FwlStatePortMatchExists (pStateFilterInfo,
                                                             i4FwlStateLocalPort,
                                                             i4FwlStateRemotePort))
                                && (i4FwlStateDirection ==
                                    pStateFilterInfo->u1Direction))
                            {
                                *pi4RetValFwlStateLogLevel =
                                    pStateFilterInfo->u1LogLevel;
                                i1RetVal = SNMP_SUCCESS;
                                return SNMP_SUCCESS;
                            }

                        }
                    }            /* End of while */
                }
            }

            break;
        case FWL_PARTIAL:
            if (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                pStateFilterInfo = (tStatefulSessionNode *)
                    TMO_SLL_First (&gFwlPartialLinksList);

                if (pStateFilterInfo == NULL)
                {
                    return SNMP_FAILURE;
                }

                while (pStateFilterInfo != NULL)
                {
                    if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                        (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                        (i4FwlStateProtocol ==
                         pStateFilterInfo->u1Protocol) &&
                        (FWL_SUCCESS == FwlStatePortMatchExists
                         (pStateFilterInfo, i4FwlStateLocalPort,
                          i4FwlStateRemotePort)) &&
                        (i4FwlStateDirection == pStateFilterInfo->u1Direction))
                    {
                        *pi4RetValFwlStateLogLevel =
                            pStateFilterInfo->u1LogLevel;
                        return SNMP_SUCCESS;
                    }
                    pStateFilterInfo =
                        (tStatefulSessionNode *)
                        TMO_SLL_Next (&gFwlPartialLinksList,
                                      (tTMO_SLL_NODE *) pStateFilterInfo);
                }                /* End of while */

            }
            if (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                pStateFilterInfo = (tStatefulSessionNode *)
                    TMO_SLL_First (&gFwlV6PartialLinksList);

                if (pStateFilterInfo == NULL)
                {
                    return SNMP_FAILURE;
                }

                while (pStateFilterInfo != NULL)
                {
                    if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                         pStateFilterInfo->LocalIP.uIpAddr.
                                         Ip6Addr) == FWL_ZERO)
                        &&
                        (Ip6AddrCompare
                         (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                          pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                         FWL_ZERO)
                        && (i4FwlStateProtocol == pStateFilterInfo->u1Protocol)
                        && (FWL_SUCCESS ==
                            FwlStatePortMatchExists (pStateFilterInfo,
                                                     i4FwlStateLocalPort,
                                                     i4FwlStateRemotePort))
                        && (i4FwlStateDirection ==
                            pStateFilterInfo->u1Direction))
                    {
                        *pi4RetValFwlStateLogLevel =
                            pStateFilterInfo->u1LogLevel;
                        return SNMP_SUCCESS;
                    }
                    pStateFilterInfo =
                        (tStatefulSessionNode *)
                        TMO_SLL_Next (&gFwlV6PartialLinksList,
                                      (tTMO_SLL_NODE *) pStateFilterInfo);
                }                /* End of while */

            }

            break;

        case FWL_INITFLOW:
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                TMO_SLL_Scan (&gFwlTcpInitFlowList, pStateFilterInfo,
                              tStatefulSessionNode *)
                {
                    if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                        (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                        (i4FwlStateProtocol ==
                         pStateFilterInfo->u1Protocol) &&
                        (FWL_SUCCESS == FwlStatePortMatchExists
                         (pStateFilterInfo, i4FwlStateLocalPort,
                          i4FwlStateRemotePort)) &&
                        (i4FwlStateDirection == pStateFilterInfo->u1Direction))
                    {
                        *pi4RetValFwlStateLogLevel =
                            pStateFilterInfo->u1LogLevel;
                        i1RetVal = SNMP_SUCCESS;
                        return SNMP_SUCCESS;
                    }

                }                /* End of scan */
            }
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                TMO_SLL_Scan (&gFwlV6TcpInitFlowList, pStateFilterInfo,
                              tStatefulSessionNode *)
                {
                    if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                         pStateFilterInfo->LocalIP.uIpAddr.
                                         Ip6Addr) == FWL_ZERO)
                        &&
                        (Ip6AddrCompare
                         (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                          pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                         FWL_ZERO)
                        && (i4FwlStateProtocol == pStateFilterInfo->u1Protocol)
                        && (FWL_SUCCESS ==
                            FwlStatePortMatchExists (pStateFilterInfo,
                                                     i4FwlStateLocalPort,
                                                     i4FwlStateRemotePort))
                        && (i4FwlStateDirection ==
                            pStateFilterInfo->u1Direction))
                    {
                        *pi4RetValFwlStateLogLevel =
                            pStateFilterInfo->u1LogLevel;
                        i1RetVal = SNMP_SUCCESS;
                        return SNMP_SUCCESS;
                    }

                }                /* End of scan */
            }
            break;
        default:
            i1RetVal = SNMP_FAILURE;
            break;
    }                            /* End of switch */

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFwlStateCallStatus
 Input       :  The Indices
                FwlStateType
                FwlStateLocalIpAddrType
                FwlStateLocalIpAddress
                FwlStateRemoteIpAddrType
                FwlStateRemoteIpAddress
                FwlStateLocalPort
                FwlStateRemotePort
                FwlStateProtocol
                FwlStateDirection

                The Object 
                retValFwlStateCallStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlStateCallStatus (INT4 i4FwlStateType, INT4 i4FwlStateLocalIpAddrType,
                          tSNMP_OCTET_STRING_TYPE * pFwlStateLocalIpAddress,
                          INT4 i4FwlStateRemoteIpAddrType,
                          tSNMP_OCTET_STRING_TYPE * pFwlStateRemoteIpAddress,
                          INT4 i4FwlStateLocalPort, INT4 i4FwlStateRemotePort,
                          INT4 i4FwlStateProtocol, INT4 i4FwlStateDirection,
                          INT4 *pi4RetValFwlStateCallStatus)
{
    tStatefulSessionNode *pStateFilterInfo = NULL;
    tFwlIpAddr          FwlLocalIpAddr;
    tFwlIpAddr          FwlRemoteIpAddr;
    UINT4               u4HashIndex = FWL_ZERO;
    INT1                i1RetVal = SNMP_SUCCESS;

    MEMSET (&FwlRemoteIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));
    MEMSET (&FwlLocalIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));

    if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4) &&
        (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV4))
    {

        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             pFwlStateLocalIpAddress, &FwlLocalIpAddr);
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             pFwlStateRemoteIpAddress, &FwlRemoteIpAddr);
    }
    else if ((i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6) &&
             (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV6))
    {
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV6,
                             pFwlStateLocalIpAddress, &FwlLocalIpAddr);
        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV6,
                             pFwlStateRemoteIpAddress, &FwlRemoteIpAddr);
    }

    switch (i4FwlStateType)
    {
        case FWL_STATEFUL:
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                TMO_HASH_Scan_Table (FWL_STATE_HASH_LIST, u4HashIndex)
                {
                    pStateFilterInfo = NULL;
                    while (FwlStateGetStatefulTableEntries (u4HashIndex,
                                                            &pStateFilterInfo)
                           == FWL_SUCCESS)
                    {
                        if (pStateFilterInfo != NULL)
                        {
                            if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                                 pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                                (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                                 pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                                (i4FwlStateProtocol ==
                                 pStateFilterInfo->u1Protocol) &&
                                (FWL_SUCCESS == FwlStatePortMatchExists
                                 (pStateFilterInfo, i4FwlStateLocalPort,
                                  i4FwlStateRemotePort)) &&
                                (i4FwlStateDirection ==
                                 pStateFilterInfo->u1Direction))
                            {
                                *pi4RetValFwlStateCallStatus =
                                    pStateFilterInfo->u1AppCallStatus;
                                i1RetVal = SNMP_SUCCESS;
                                return SNMP_SUCCESS;
                            }

                        }
                    }            /* End of while */
                }
            }
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                TMO_HASH_Scan_Table (FWL_STATE_V6_HASH_LIST, u4HashIndex)
                {
                    pStateFilterInfo = NULL;
                    while (FwlStateGetIpv6StatefulTableEntries (u4HashIndex,
                                                                &pStateFilterInfo)
                           == FWL_SUCCESS)
                    {
                        if (pStateFilterInfo != NULL)
                        {
                            if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                                 pStateFilterInfo->LocalIP.
                                                 uIpAddr.Ip6Addr) == FWL_ZERO)
                                &&
                                (Ip6AddrCompare
                                 (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                                  pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                                 FWL_ZERO)
                                && (i4FwlStateProtocol ==
                                    pStateFilterInfo->u1Protocol)
                                && (FWL_SUCCESS ==
                                    FwlStatePortMatchExists (pStateFilterInfo,
                                                             i4FwlStateLocalPort,
                                                             i4FwlStateRemotePort))
                                && (i4FwlStateDirection ==
                                    pStateFilterInfo->u1Direction))

                            {
                                *pi4RetValFwlStateCallStatus =
                                    pStateFilterInfo->u1AppCallStatus;
                                i1RetVal = SNMP_SUCCESS;
                                return SNMP_SUCCESS;
                            }

                        }
                    }            /* End of while */
                }
            }

            break;
        case FWL_PARTIAL:
            if (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                pStateFilterInfo = (tStatefulSessionNode *)
                    TMO_SLL_First (&gFwlPartialLinksList);

                if (pStateFilterInfo == NULL)
                {
                    return SNMP_FAILURE;
                }

                while (pStateFilterInfo != NULL)
                {
                    if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                        (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                        (i4FwlStateProtocol ==
                         pStateFilterInfo->u1Protocol) &&
                        (FWL_SUCCESS == FwlStatePortMatchExists
                         (pStateFilterInfo, i4FwlStateLocalPort,
                          i4FwlStateRemotePort)) &&
                        (i4FwlStateDirection == pStateFilterInfo->u1Direction))
                    {
                        *pi4RetValFwlStateCallStatus =
                            pStateFilterInfo->u1AppCallStatus;
                        i1RetVal = SNMP_SUCCESS;
                        return SNMP_SUCCESS;
                    }
                    pStateFilterInfo =
                        (tStatefulSessionNode *)
                        TMO_SLL_Next (&gFwlPartialLinksList,
                                      (tTMO_SLL_NODE *) pStateFilterInfo);
                }                /* End of while */

            }
            if (i4FwlStateRemoteIpAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                pStateFilterInfo = (tStatefulSessionNode *)
                    TMO_SLL_First (&gFwlV6PartialLinksList);

                if (pStateFilterInfo == NULL)
                {
                    return SNMP_FAILURE;
                }

                while (pStateFilterInfo != NULL)
                {
                    if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                         pStateFilterInfo->LocalIP.uIpAddr.
                                         Ip6Addr) == FWL_ZERO)
                        &&
                        (Ip6AddrCompare
                         (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                          pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                         FWL_ZERO)
                        && (i4FwlStateProtocol == pStateFilterInfo->u1Protocol)
                        && (FWL_SUCCESS ==
                            FwlStatePortMatchExists (pStateFilterInfo,
                                                     i4FwlStateLocalPort,
                                                     i4FwlStateRemotePort))
                        && (i4FwlStateDirection ==
                            pStateFilterInfo->u1Direction))
                    {
                        *pi4RetValFwlStateCallStatus =
                            pStateFilterInfo->u1AppCallStatus;
                        i1RetVal = SNMP_SUCCESS;
                        return SNMP_SUCCESS;
                    }
                    pStateFilterInfo =
                        (tStatefulSessionNode *)
                        TMO_SLL_Next (&gFwlV6PartialLinksList,
                                      (tTMO_SLL_NODE *) pStateFilterInfo);
                }                /* End of while */

            }
            break;

        case FWL_INITFLOW:
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                TMO_SLL_Scan (&gFwlTcpInitFlowList, pStateFilterInfo,
                              tStatefulSessionNode *)
                {
                    if ((FwlLocalIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->LocalIP.uIpAddr.Ip4Addr) &&
                        (FwlRemoteIpAddr.uIpAddr.Ip4Addr ==
                         pStateFilterInfo->RemoteIP.uIpAddr.Ip4Addr) &&
                        (i4FwlStateProtocol ==
                         pStateFilterInfo->u1Protocol) &&
                        (FWL_SUCCESS == FwlStatePortMatchExists
                         (pStateFilterInfo, i4FwlStateLocalPort,
                          i4FwlStateRemotePort)) &&
                        (i4FwlStateDirection == pStateFilterInfo->u1Direction))
                    {
                        *pi4RetValFwlStateCallStatus =
                            pStateFilterInfo->u1AppCallStatus;
                        i1RetVal = SNMP_SUCCESS;
                        return SNMP_SUCCESS;
                    }

                }                /* End of scan */
            }
            if (i4FwlStateLocalIpAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                TMO_SLL_Scan (&gFwlV6TcpInitFlowList, pStateFilterInfo,
                              tStatefulSessionNode *)
                {
                    if ((Ip6AddrCompare (FwlLocalIpAddr.uIpAddr.Ip6Addr,
                                         pStateFilterInfo->LocalIP.uIpAddr.
                                         Ip6Addr) == FWL_ZERO)
                        &&
                        (Ip6AddrCompare
                         (FwlRemoteIpAddr.uIpAddr.Ip6Addr,
                          pStateFilterInfo->RemoteIP.uIpAddr.Ip6Addr) ==
                         FWL_ZERO)
                        && (i4FwlStateProtocol == pStateFilterInfo->u1Protocol)
                        && (FWL_SUCCESS ==
                            FwlStatePortMatchExists (pStateFilterInfo,
                                                     i4FwlStateLocalPort,
                                                     i4FwlStateRemotePort))
                        && (i4FwlStateDirection ==
                            pStateFilterInfo->u1Direction))
                    {
                        *pi4RetValFwlStateCallStatus =
                            pStateFilterInfo->u1AppCallStatus;
                        i1RetVal = SNMP_SUCCESS;
                        return SNMP_SUCCESS;
                    }

                }                /* End of scan */
            }
            break;
        default:
            i1RetVal = SNMP_FAILURE;
            break;
    }                            /* End of switch */

    return i1RetVal;

}

/* LOW LEVEL Routines for Table : FwlDefnBlkListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlDefnBlkListTable
 Input       :  The Indices
                FwlBlkListIpAddressType
                FwlBlkListIpAddress
                FwlBlkListIpMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFwlDefnBlkListTable (INT4 i4FwlBlkListIpAddressType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFwlBlkListIpAddress,
                                             UINT4 u4FwlBlkListIpMask)
{
    UINT4               u4BlkListIpAddr = FWL_ZERO;
    INT4                i4RetVal = FWL_FAILURE;
    UINT2               u2PrefixLen = FWL_ZERO;
    tFwlIpAddr          BlkListIpAddr;
    tUtlInAddr          Addr;
    tUtlIn6Addr         IP6Addr;

    MEMSET (&BlkListIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));
    MEMSET (&Addr, FWL_ZERO, sizeof (tUtlInAddr));
    MEMSET (&IP6Addr, FWL_ZERO, sizeof (tUtlIn6Addr));

    BlkListIpAddr.u4AddrType = (UINT4) i4FwlBlkListIpAddressType;
    u2PrefixLen = (UINT2) u4FwlBlkListIpMask;
    UNUSED_PARAM (u2PrefixLen);
    switch (i4FwlBlkListIpAddressType)
    {
        case IPVX_ADDR_FMLY_IPV4:
        {
            i4RetVal =
                FWL_INET_ATON (pFwlBlkListIpAddress->pu1_OctetList, &Addr);
            if (i4RetVal == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            u4BlkListIpAddr = OSIX_NTOHL (Addr.u4Addr);
            i4RetVal = FwlValidateIpAddress (u4BlkListIpAddr);
            if ((i4RetVal == FWL_BCAST_ADDR) ||
                (i4RetVal == FWL_ZERO_NETW_ADDR) ||
                (i4RetVal == FWL_ZERO_ADDR) ||
                (i4RetVal == FWL_LOOPBACK_ADDR) ||
                (i4RetVal == FWL_INVALID_ADDR) ||
                (i4RetVal == FWL_CLASS_BCASTADDR) ||
                (i4RetVal == FWL_CLASSE_ADDR) || (i4RetVal == FWL_MCAST_ADDR))
            {
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\n BlackList Address Validation failed \n");
                return SNMP_FAILURE;
            }
            if (u4FwlBlkListIpMask > IPVX_IPV4_MAX_MASK_LEN)
            {
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\n Error: Invalid Prefix Length. \n");
                return SNMP_FAILURE;
            }
            break;
        }
        case IPVX_ADDR_FMLY_IPV6:
        {
            INET_ATON6 ((CHR1 *) pFwlBlkListIpAddress->pu1_OctetList, &IP6Addr);
            MEMCPY (BlkListIpAddr.v6Addr.u1_addr, IP6Addr.u1addr,
                    IPVX_IPV6_ADDR_LEN);
            i4RetVal = Ip6AddrType (&BlkListIpAddr.v6Addr);
            if ((i4RetVal == ADDR6_UNSPECIFIED) |
                (i4RetVal == ADDR6_LOOPBACK) | (i4RetVal == ADDR6_MULTI))
            {
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\n BlackList Address Validation failed \n");
                return SNMP_FAILURE;
            }
            if (u4FwlBlkListIpMask > IP6_ADDR_MAX_PREFIX)
            {
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\n Error: Invalid Prefix Length. \n");
                return SNMP_FAILURE;
            }
            if (i4RetVal == ADDR6_V4_COMPAT)
            {
                /* IPv4-IPv6 compatible. Validate the IPv4 prefix */
                PTR_FETCH4 (u4BlkListIpAddr, &(BlkListIpAddr.v6Addr.u1_addr
                                               [IPVX_IPV6_ADDR_LEN -
                                                IPVX_IPV4_ADDR_LEN]));
                i4RetVal = FwlValidateIpAddress (u4BlkListIpAddr);
                if ((i4RetVal == FWL_BCAST_ADDR) ||
                    (i4RetVal == FWL_ZERO_NETW_ADDR) ||
                    (i4RetVal == FWL_ZERO_ADDR) ||
                    (i4RetVal == FWL_LOOPBACK_ADDR) ||
                    (i4RetVal == FWL_INVALID_ADDR) ||
                    (i4RetVal == FWL_CLASS_BCASTADDR) ||
                    (i4RetVal == FWL_CLASSE_ADDR))
                {
                    MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                             "\n BlackList Address Validation failed \n");
                    return SNMP_FAILURE;
                }
                if (u4FwlBlkListIpMask > IPVX_IPV4_MAX_MASK_LEN)
                {
                    MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                             "\n Error: Invalid Prefix Length. \n");
                    return SNMP_FAILURE;
                }
            }
            break;
        }
        default:
            MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                     "\n Address family value is unsupported \n");
            return SNMP_FAILURE;

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFwlDefnBlkListTable
 Input       :  The Indices
                FwlBlkListIpAddressType
                FwlBlkListIpAddress
                FwlBlkListIpMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFwlDefnBlkListTable (INT4 *pi4FwlBlkListIpAddressType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFwlBlkListIpAddress,
                                     UINT4 *pu4FwlBlkListIpMask)
{
    UINT4               u4LocalIp = FWL_ZERO;
    CHR1               *pString = NULL;
    tFwlBlackListEntry *pFwlBlkListNode = NULL;
    tUtlInAddr          Addr;
    tUtlIn6Addr         IP6Addr;

    MEMSET (&Addr, FWL_ZERO, sizeof (tUtlInAddr));
    MEMSET (&IP6Addr, FWL_ZERO, sizeof (tUtlIn6Addr));

    /* Search the BlackList Tree */
    pFwlBlkListNode = (tFwlBlackListEntry *) RBTreeGetFirst (gFwlBlackList);
    if (NULL == pFwlBlkListNode)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n BlackList Entry is empty!\n");
        return SNMP_FAILURE;
    }
    /* Populate the indices from the first node fetched. */
    *pi4FwlBlkListIpAddressType = (INT4)
        pFwlBlkListNode->FwlBlackListIpAddr.u4AddrType;
    if (IPVX_ADDR_FMLY_IPV4 == *pi4FwlBlkListIpAddressType)
    {
        u4LocalIp = pFwlBlkListNode->FwlBlackListIpAddr.v4Addr;
        CLI_CONVERT_IPADDR_TO_STR (pString, u4LocalIp);

        pFwlBlkListIpAddress->i4_Length = FWL_IPV4_ADDR_LEN;
        STRNCPY (pFwlBlkListIpAddress->pu1_OctetList, (UINT1 *) pString,
                 (size_t) pFwlBlkListIpAddress->i4_Length);
    }
    else
    {
        pFwlBlkListIpAddress->i4_Length = FWL_IPV6_ADDR_LEN;
        MEMCPY (IP6Addr.u1addr,
                pFwlBlkListNode->FwlBlackListIpAddr.v6Addr.u1_addr,
                IPVX_IPV6_ADDR_LEN);
        pString = INET_NTOA6 (IP6Addr);
        STRNCPY (pFwlBlkListIpAddress->pu1_OctetList, (UINT1 *) pString,
                 (size_t) pFwlBlkListIpAddress->i4_Length);
    }

    *pu4FwlBlkListIpMask = pFwlBlkListNode->u2PrefixLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFwlDefnBlkListTable
 Input       :  The Indices
                FwlBlkListIpAddressType
                nextFwlBlkListIpAddressType
                FwlBlkListIpAddress
                nextFwlBlkListIpAddress
                FwlBlkListIpMask
                nextFwlBlkListIpMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFwlDefnBlkListTable (INT4 i4FwlBlkListIpAddressType,
                                    INT4 *pi4NextFwlBlkListIpAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFwlBlkListIpAddress,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextFwlBlkListIpAddress,
                                    UINT4 u4FwlBlkListIpMask,
                                    UINT4 *pu4NextFwlBlkListIpMask)
{
    CHR1               *pString = NULL;
    tFwlBlackListEntry  currFwlBlkListNode;
    tFwlBlackListEntry *pNextFwlBlkListNode = NULL;
    tUtlInAddr          Addr;
    tUtlIn6Addr         IP6Addr;
    INT4                i4RetVal = FWL_ZERO;

    MEMSET (&currFwlBlkListNode, FWL_ZERO, sizeof (tFwlBlackListEntry));
    MEMSET (&Addr, FWL_ZERO, sizeof (tUtlInAddr));
    MEMSET (&IP6Addr, FWL_ZERO, sizeof (tUtlIn6Addr));

    currFwlBlkListNode.FwlBlackListIpAddr.u4AddrType = (UINT4)
        i4FwlBlkListIpAddressType;
    if (IPVX_ADDR_FMLY_IPV4 == i4FwlBlkListIpAddressType)
    {
        i4RetVal = FWL_INET_ATON (pFwlBlkListIpAddress->pu1_OctetList, &Addr);
        if (i4RetVal == SNMP_SUCCESS)
        {
            currFwlBlkListNode.FwlBlackListIpAddr.v4Addr =
                OSIX_NTOHL (Addr.u4Addr);
        }
    }
    else
    {
        INET_ATON6 ((CHR1 *) pFwlBlkListIpAddress->pu1_OctetList, &IP6Addr);
        MEMCPY (&(currFwlBlkListNode.FwlBlackListIpAddr.v6Addr),
                IP6Addr.u1addr, IPVX_IPV6_ADDR_LEN);
    }

    currFwlBlkListNode.u2PrefixLen = (UINT2) u4FwlBlkListIpMask;

    pNextFwlBlkListNode = (tFwlBlackListEntry *) RBTreeGetNext (gFwlBlackList,
                                                                (tRBElem *) &
                                                                currFwlBlkListNode,
                                                                NULL);

    if (NULL != pNextFwlBlkListNode)
    {
        *pi4NextFwlBlkListIpAddressType = (INT4)
            pNextFwlBlkListNode->FwlBlackListIpAddr.u4AddrType;
        if (IPVX_ADDR_FMLY_IPV4 == *pi4NextFwlBlkListIpAddressType)
        {
            Addr.u4Addr = pNextFwlBlkListNode->FwlBlackListIpAddr.v4Addr;
            CLI_CONVERT_IPADDR_TO_STR (pString, Addr.u4Addr);

            pNextFwlBlkListIpAddress->i4_Length = FWL_IPV4_ADDR_LEN;
            STRNCPY (pNextFwlBlkListIpAddress->pu1_OctetList, (UINT1 *) pString,
                     (size_t) pNextFwlBlkListIpAddress->i4_Length);

        }
        else
        {
            pNextFwlBlkListIpAddress->i4_Length = FWL_IPV6_ADDR_LEN;
            MEMCPY (IP6Addr.u1addr,
                    pNextFwlBlkListNode->FwlBlackListIpAddr.v6Addr.u1_addr,
                    IPVX_IPV6_ADDR_LEN);
            pString = INET_NTOA6 (IP6Addr);
            STRNCPY (pNextFwlBlkListIpAddress->pu1_OctetList, (UINT1 *) pString,
                     (size_t) pNextFwlBlkListIpAddress->i4_Length);
        }

        *pu4NextFwlBlkListIpMask = pNextFwlBlkListNode->u2PrefixLen;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlBlkListHitsCount
 Input       :  The Indices
                FwlBlkListIpAddressType
                FwlBlkListIpAddress
                FwlBlkListIpMask

                The Object 
                retValFwlBlkListHitsCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlBlkListHitsCount (INT4 i4FwlBlkListIpAddressType,
                           tSNMP_OCTET_STRING_TYPE * pFwlBlkListIpAddress,
                           UINT4 u4FwlBlkListIpMask,
                           UINT4 *pu4RetValFwlBlkListHitsCount)
{
    tFwlBlackListEntry *pFwlBlkListNode = NULL;

    pFwlBlkListNode = FwlGetBlackListEntry (i4FwlBlkListIpAddressType,
                                            pFwlBlkListIpAddress,
                                            u4FwlBlkListIpMask);
    if (NULL != pFwlBlkListNode)
    {
        *pu4RetValFwlBlkListHitsCount = pFwlBlkListNode->u4BlkListHitCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFwlBlkListEntryType
 Input       :  The Indices
                FwlBlkListIpAddressType
                FwlBlkListIpAddress
                FwlBlkListIpMask

                The Object 
                retValFwlBlkListEntryType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlBlkListEntryType (INT4 i4FwlBlkListIpAddressType,
                           tSNMP_OCTET_STRING_TYPE * pFwlBlkListIpAddress,
                           UINT4 u4FwlBlkListIpMask,
                           INT4 *pi4RetValFwlBlkListEntryType)
{
    tFwlBlackListEntry *pFwlBlkListNode = NULL;

    pFwlBlkListNode = FwlGetBlackListEntry (i4FwlBlkListIpAddressType,
                                            pFwlBlkListIpAddress,
                                            u4FwlBlkListIpMask);
    if (NULL != pFwlBlkListNode)
    {
        *pi4RetValFwlBlkListEntryType = pFwlBlkListNode->i4EntryType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFwlBlkListRowStatus
 Input       :  The Indices
                FwlBlkListIpAddressType
                FwlBlkListIpAddress
                FwlBlkListIpMask

                The Object 
                retValFwlBlkListRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlBlkListRowStatus (INT4 i4FwlBlkListIpAddressType,
                           tSNMP_OCTET_STRING_TYPE * pFwlBlkListIpAddress,
                           UINT4 u4FwlBlkListIpMask,
                           INT4 *pi4RetValFwlBlkListRowStatus)
{
    tFwlBlackListEntry *pFwlBlkListNode = NULL;

    pFwlBlkListNode = FwlGetBlackListEntry (i4FwlBlkListIpAddressType,
                                            pFwlBlkListIpAddress,
                                            u4FwlBlkListIpMask);
    if (NULL != pFwlBlkListNode)
    {
        *pi4RetValFwlBlkListRowStatus = pFwlBlkListNode->i4RowStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlBlkListRowStatus
 Input       :  The Indices
                FwlBlkListIpAddressType
                FwlBlkListIpAddress
                FwlBlkListIpMask

                The Object 
                setValFwlBlkListRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlBlkListRowStatus (INT4 i4FwlBlkListIpAddressType,
                           tSNMP_OCTET_STRING_TYPE * pFwlBlkListIpAddress,
                           UINT4 u4FwlBlkListIpMask,
                           INT4 i4SetValFwlBlkListRowStatus)
{
    switch (i4SetValFwlBlkListRowStatus)
    {
        case FWL_CREATE_AND_GO:
        case FWL_CREATE_AND_WAIT:
            if (FWL_SUCCESS != FwlBlackListEntryCreate
                (i4FwlBlkListIpAddressType,
                 pFwlBlkListIpAddress,
                 u4FwlBlkListIpMask, FWL_BLACKLIST_STATIC))
            {
                return SNMP_FAILURE;
            }
            break;
        case FWL_DESTROY:
            if (FWL_SUCCESS != FwlBlackListEntryDelete
                (i4FwlBlkListIpAddressType,
                 pFwlBlkListIpAddress, u4FwlBlkListIpMask))
            {
                return SNMP_FAILURE;
            }
            break;
        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlBlkListRowStatus
 Input       :  The Indices
                FwlBlkListIpAddressType
                FwlBlkListIpAddress
                FwlBlkListIpMask

                The Object 
                testValFwlBlkListRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlBlkListRowStatus (UINT4 *pu4ErrorCode,
                              INT4 i4FwlBlkListIpAddressType,
                              tSNMP_OCTET_STRING_TYPE * pFwlBlkListIpAddress,
                              UINT4 u4FwlBlkListIpMask,
                              INT4 i4TestValFwlBlkListRowStatus)
{
    UINT4               u4Count = FWL_ZERO;
    tFwlBlackListEntry *pFwlBlkListNode = NULL;

    pFwlBlkListNode = FwlGetBlackListEntry (i4FwlBlkListIpAddressType,
                                            pFwlBlkListIpAddress,
                                            u4FwlBlkListIpMask);

    switch (i4TestValFwlBlkListRowStatus)
    {
            /* To set create and go, no similar entry should be existing previously.
             */
        case FWL_CREATE_AND_GO:

            RBTreeCount (gFwlBlackList, &u4Count);

            if (MAX_FWL_BLACK_LIST_ENTRIES == u4Count)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            if (SNMP_FAILURE == nmhValidateIndexInstanceFwlDefnBlkListTable
                (i4FwlBlkListIpAddressType,
                 pFwlBlkListIpAddress, u4FwlBlkListIpMask))
            {
                *pu4ErrorCode = SNMP_ERR_BAD_VALUE;
                CLI_SET_ERR (CLI_FWL_ERR_INVALID_BLACK_WHITE_LIST);
                return SNMP_FAILURE;
            }

            if (NULL != pFwlBlkListNode)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_FWL_ERR_EXISTING_ENTRY);
                return SNMP_FAILURE;
            }
            break;

            /* To set the row status to be destroy, an entry should be 
             * existing already.
             */
        case FWL_DESTROY:
            if (NULL == pFwlBlkListNode)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_FWL_ERR_INCONSISTENT_ENTRY);
                return SNMP_FAILURE;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlDefnBlkListTable
 Input       :  The Indices
                FwlBlkListIpAddressType
                FwlBlkListIpAddress
                FwlBlkListIpMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDefnBlkListTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FwlDefnWhiteListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlDefnWhiteListTable
 Input       :  The Indices
                FwlWhiteListIpAddressType
                FwlWhiteListIpAddress
                FwlWhiteListIpMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFwlDefnWhiteListTable (INT4 i4FwlWhiteListIpAddressType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFwlWhiteListIpAddress,
                                               UINT4 u4FwlWhiteListIpMask)
{
    UINT4               u4WhiteListIpAddr = FWL_ZERO;
    INT4                i4RetVal = FWL_FAILURE;
    UINT2               u2PrefixLen = FWL_ZERO;
    tFwlIpAddr          WhiteListIpAddr;
    tUtlInAddr          Addr;
    tUtlIn6Addr         IP6Addr;

    MEMSET (&WhiteListIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));
    MEMSET (&Addr, FWL_ZERO, sizeof (tUtlInAddr));
    MEMSET (&IP6Addr, FWL_ZERO, sizeof (tUtlIn6Addr));

    WhiteListIpAddr.u4AddrType = (UINT4) i4FwlWhiteListIpAddressType;
    u2PrefixLen = (UINT2) u4FwlWhiteListIpMask;
    UNUSED_PARAM (u2PrefixLen);
    switch (i4FwlWhiteListIpAddressType)
    {
        case IPVX_ADDR_FMLY_IPV4:
        {
            i4RetVal =
                FWL_INET_ATON (pFwlWhiteListIpAddress->pu1_OctetList, &Addr);
            if (i4RetVal == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            u4WhiteListIpAddr = OSIX_NTOHL (Addr.u4Addr);
            i4RetVal = FwlValidateIpAddress (u4WhiteListIpAddr);
            if ((i4RetVal == FWL_BCAST_ADDR) ||
                (i4RetVal == FWL_ZERO_NETW_ADDR) ||
                (i4RetVal == FWL_ZERO_ADDR) ||
                (i4RetVal == FWL_LOOPBACK_ADDR) ||
                (i4RetVal == FWL_INVALID_ADDR) ||
                (i4RetVal == FWL_CLASS_BCASTADDR) ||
                (i4RetVal == FWL_CLASSE_ADDR) || (i4RetVal == FWL_MCAST_ADDR))
            {
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\n WhiteList Address Validation failed \n");
                return SNMP_FAILURE;
            }
            if (u4FwlWhiteListIpMask > IPVX_IPV4_MAX_MASK_LEN)
            {
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\n Error: Invalid Prefix Length. \n");
                return SNMP_FAILURE;
            }
            break;
        }
        case IPVX_ADDR_FMLY_IPV6:
        {
            INET_ATON6 ((CHR1 *) pFwlWhiteListIpAddress->pu1_OctetList,
                        &IP6Addr);
            MEMCPY (WhiteListIpAddr.v6Addr.u1_addr, IP6Addr.u1addr,
                    IPVX_IPV6_ADDR_LEN);
            i4RetVal = Ip6AddrType (&WhiteListIpAddr.v6Addr);
            if ((i4RetVal == ADDR6_UNSPECIFIED) |
                (i4RetVal == ADDR6_LOOPBACK) | (i4RetVal == ADDR6_MULTI))
            {
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\n WhiteList Address Validation failed \n");
                return SNMP_FAILURE;
            }
            if (u4FwlWhiteListIpMask > IP6_ADDR_MAX_PREFIX)
            {
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\n Error: Invalid Prefix Length. \n");
                return SNMP_FAILURE;
            }
            if (i4RetVal == ADDR6_V4_COMPAT)
            {
                /* IPv4-IPv6 compatible. Validate the IPv4 prefix */
                PTR_FETCH4 (u4WhiteListIpAddr, &(WhiteListIpAddr.v6Addr.u1_addr
                                                 [IPVX_IPV6_ADDR_LEN -
                                                  IPVX_IPV4_ADDR_LEN]));
                i4RetVal = FwlValidateIpAddress (u4WhiteListIpAddr);
                if ((i4RetVal == FWL_BCAST_ADDR) ||
                    (i4RetVal == FWL_ZERO_NETW_ADDR) ||
                    (i4RetVal == FWL_ZERO_ADDR) ||
                    (i4RetVal == FWL_LOOPBACK_ADDR) ||
                    (i4RetVal == FWL_INVALID_ADDR) ||
                    (i4RetVal == FWL_CLASS_BCASTADDR) ||
                    (i4RetVal == FWL_CLASSE_ADDR))
                {
                    MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                             "\n WhiteList Address Validation failed \n");
                    return SNMP_FAILURE;
                }
                if (u4FwlWhiteListIpMask > IPVX_IPV4_MAX_MASK_LEN)
                {
                    MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                             "\n Error: Invalid Prefix Length. \n");
                    return SNMP_FAILURE;
                }
            }
            break;
        }
        default:
            MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                     "\n Address family value is unsupported \n");
            return SNMP_FAILURE;

    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFwlDefnWhiteListTable
 Input       :  The Indices
                FwlWhiteListIpAddressType
                FwlWhiteListIpAddress
                FwlWhiteListIpMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFwlDefnWhiteListTable (INT4 *pi4FwlWhiteListIpAddressType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFwlWhiteListIpAddress,
                                       UINT4 *pu4FwlWhiteListIpMask)
{
    UINT4               u4LocalIp = FWL_ZERO;
    CHR1               *pString = NULL;
    tFwlWhiteListEntry *pFwlWhiteListNode = NULL;
    tUtlInAddr          Addr;
    tUtlIn6Addr         IP6Addr;

    MEMSET (&Addr, FWL_ZERO, sizeof (tUtlInAddr));
    MEMSET (&IP6Addr, FWL_ZERO, sizeof (tUtlIn6Addr));

    /* Search the WhiteList Tree */
    pFwlWhiteListNode = (tFwlWhiteListEntry *) RBTreeGetFirst (gFwlWhiteList);
    if (NULL == pFwlWhiteListNode)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\n WhiteList Entry is empty!\n");
        return SNMP_FAILURE;
    }
    /* Populate the indices from the first node fetched. */
    *pi4FwlWhiteListIpAddressType = (INT4)
        pFwlWhiteListNode->FwlWhiteListIpAddr.u4AddrType;
    if (IPVX_ADDR_FMLY_IPV4 == *pi4FwlWhiteListIpAddressType)
    {
        u4LocalIp = pFwlWhiteListNode->FwlWhiteListIpAddr.v4Addr;
        CLI_CONVERT_IPADDR_TO_STR (pString, u4LocalIp);

        pFwlWhiteListIpAddress->i4_Length = FWL_IPV4_ADDR_LEN;
        STRNCPY (pFwlWhiteListIpAddress->pu1_OctetList, (UINT1 *) pString,
                 (size_t) pFwlWhiteListIpAddress->i4_Length);
    }
    else
    {
        pFwlWhiteListIpAddress->i4_Length = FWL_IPV6_ADDR_LEN;
        MEMCPY (IP6Addr.u1addr,
                pFwlWhiteListNode->FwlWhiteListIpAddr.v6Addr.u1_addr,
                IPVX_IPV6_ADDR_LEN);
        pString = INET_NTOA6 (IP6Addr);
        STRNCPY (pFwlWhiteListIpAddress->pu1_OctetList, (UINT1 *) pString,
                 (size_t) pFwlWhiteListIpAddress->i4_Length);
    }

    *pu4FwlWhiteListIpMask = pFwlWhiteListNode->u2PrefixLen;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFwlDefnWhiteListTable
 Input       :  The Indices
                FwlWhiteListIpAddressType
                nextFwlWhiteListIpAddressType
                FwlWhiteListIpAddress
                nextFwlWhiteListIpAddress
                FwlWhiteListIpMask
                nextFwlWhiteListIpMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFwlDefnWhiteListTable (INT4 i4FwlWhiteListIpAddressType,
                                      INT4 *pi4NextFwlWhiteListIpAddressType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFwlWhiteListIpAddress,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextFwlWhiteListIpAddress,
                                      UINT4 u4FwlWhiteListIpMask,
                                      UINT4 *pu4NextFwlWhiteListIpMask)
{
    CHR1               *pString = NULL;
    tFwlWhiteListEntry  currFwlWhiteListNode;
    tFwlWhiteListEntry *pNextFwlWhiteListNode = NULL;
    tUtlInAddr          Addr;
    tUtlIn6Addr         IP6Addr;
    INT4                i4RetVal = FWL_ZERO;

    MEMSET (&currFwlWhiteListNode, FWL_ZERO, sizeof (tFwlWhiteListEntry));
    MEMSET (&Addr, FWL_ZERO, sizeof (tUtlInAddr));
    MEMSET (&IP6Addr, FWL_ZERO, sizeof (tUtlIn6Addr));

    currFwlWhiteListNode.FwlWhiteListIpAddr.u4AddrType = (UINT4)
        i4FwlWhiteListIpAddressType;
    if (IPVX_ADDR_FMLY_IPV4 == i4FwlWhiteListIpAddressType)
    {
        i4RetVal = FWL_INET_ATON (pFwlWhiteListIpAddress->pu1_OctetList, &Addr);
        if (i4RetVal == SNMP_SUCCESS)
        {
            currFwlWhiteListNode.FwlWhiteListIpAddr.v4Addr =
                OSIX_NTOHL (Addr.u4Addr);
        }
    }
    else
    {
        INET_ATON6 ((CHR1 *) pFwlWhiteListIpAddress->pu1_OctetList, &IP6Addr);
        MEMCPY (&(currFwlWhiteListNode.FwlWhiteListIpAddr.v6Addr),
                IP6Addr.u1addr, IPVX_IPV6_ADDR_LEN);
    }

    currFwlWhiteListNode.u2PrefixLen = (UINT2) u4FwlWhiteListIpMask;

    pNextFwlWhiteListNode = (tFwlWhiteListEntry *) RBTreeGetNext (gFwlWhiteList,
                                                                  (tRBElem *) &
                                                                  currFwlWhiteListNode,
                                                                  NULL);

    if (NULL != pNextFwlWhiteListNode)
    {
        *pi4NextFwlWhiteListIpAddressType = (INT4)
            pNextFwlWhiteListNode->FwlWhiteListIpAddr.u4AddrType;
        if (IPVX_ADDR_FMLY_IPV4 == *pi4NextFwlWhiteListIpAddressType)
        {
            Addr.u4Addr = pNextFwlWhiteListNode->FwlWhiteListIpAddr.v4Addr;
            CLI_CONVERT_IPADDR_TO_STR (pString, Addr.u4Addr);

            pNextFwlWhiteListIpAddress->i4_Length = FWL_IPV4_ADDR_LEN;
            STRNCPY (pNextFwlWhiteListIpAddress->pu1_OctetList,
                     (UINT1 *) pString,
                     (size_t) pNextFwlWhiteListIpAddress->i4_Length);

        }
        else
        {
            pNextFwlWhiteListIpAddress->i4_Length = FWL_IPV6_ADDR_LEN;
            MEMCPY (IP6Addr.u1addr,
                    pNextFwlWhiteListNode->FwlWhiteListIpAddr.v6Addr.u1_addr,
                    IPVX_IPV6_ADDR_LEN);
            pString = INET_NTOA6 (IP6Addr);
            STRNCPY (pNextFwlWhiteListIpAddress->pu1_OctetList,
                     (UINT1 *) pString,
                     (size_t) pNextFwlWhiteListIpAddress->i4_Length);
        }

        *pu4NextFwlWhiteListIpMask = pNextFwlWhiteListNode->u2PrefixLen;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlWhiteListHitsCount
 Input       :  The Indices
                FwlWhiteListIpAddressType
                FwlWhiteListIpAddress
                FwlWhiteListIpMask

                The Object 
                retValFwlWhiteListHitsCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlWhiteListHitsCount (INT4 i4FwlWhiteListIpAddressType,
                             tSNMP_OCTET_STRING_TYPE * pFwlWhiteListIpAddress,
                             UINT4 u4FwlWhiteListIpMask,
                             UINT4 *pu4RetValFwlWhiteListHitsCount)
{
    tFwlWhiteListEntry *pFwlWhiteListNode = NULL;

    pFwlWhiteListNode = FwlGetWhiteListEntry (i4FwlWhiteListIpAddressType,
                                              pFwlWhiteListIpAddress,
                                              u4FwlWhiteListIpMask);
    if (NULL != pFwlWhiteListNode)
    {
        *pu4RetValFwlWhiteListHitsCount =
            pFwlWhiteListNode->u4WhiteListHitCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFwlWhiteListRowStatus
 Input       :  The Indices
                FwlWhiteListIpAddressType
                FwlWhiteListIpAddress
                FwlWhiteListIpMask

                The Object 
                retValFwlWhiteListRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlWhiteListRowStatus (INT4 i4FwlWhiteListIpAddressType,
                             tSNMP_OCTET_STRING_TYPE * pFwlWhiteListIpAddress,
                             UINT4 u4FwlWhiteListIpMask,
                             INT4 *pi4RetValFwlWhiteListRowStatus)
{
    tFwlWhiteListEntry *pFwlWhiteListNode = NULL;

    pFwlWhiteListNode = FwlGetWhiteListEntry (i4FwlWhiteListIpAddressType,
                                              pFwlWhiteListIpAddress,
                                              u4FwlWhiteListIpMask);
    if (NULL != pFwlWhiteListNode)
    {
        *pi4RetValFwlWhiteListRowStatus = pFwlWhiteListNode->i4RowStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlWhiteListRowStatus
 Input       :  The Indices
                FwlWhiteListIpAddressType
                FwlWhiteListIpAddress
                FwlWhiteListIpMask

                The Object 
                setValFwlWhiteListRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlWhiteListRowStatus (INT4 i4FwlWhiteListIpAddressType,
                             tSNMP_OCTET_STRING_TYPE * pFwlWhiteListIpAddress,
                             UINT4 u4FwlWhiteListIpMask,
                             INT4 i4SetValFwlWhiteListRowStatus)
{
    switch (i4SetValFwlWhiteListRowStatus)
    {
        case FWL_CREATE_AND_GO:
        case FWL_CREATE_AND_WAIT:
            if (FWL_SUCCESS != FwlWhiteListEntryCreate
                (i4FwlWhiteListIpAddressType,
                 pFwlWhiteListIpAddress, u4FwlWhiteListIpMask))
            {
                return SNMP_FAILURE;
            }
            break;
        case FWL_DESTROY:
            if (FWL_SUCCESS != FwlWhiteListEntryDelete
                (i4FwlWhiteListIpAddressType,
                 pFwlWhiteListIpAddress, u4FwlWhiteListIpMask))
            {
                return SNMP_FAILURE;
            }
            break;
        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlWhiteListRowStatus
 Input       :  The Indices
                FwlWhiteListIpAddressType
                FwlWhiteListIpAddress
                FwlWhiteListIpMask

                The Object 
                testValFwlWhiteListRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlWhiteListRowStatus (UINT4 *pu4ErrorCode,
                                INT4 i4FwlWhiteListIpAddressType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFwlWhiteListIpAddress,
                                UINT4 u4FwlWhiteListIpMask,
                                INT4 i4TestValFwlWhiteListRowStatus)
{
    UINT4               u4Count = FWL_ZERO;
    tFwlWhiteListEntry *pFwlWhiteListNode = NULL;

    pFwlWhiteListNode = FwlGetWhiteListEntry (i4FwlWhiteListIpAddressType,
                                              pFwlWhiteListIpAddress,
                                              u4FwlWhiteListIpMask);

    switch (i4TestValFwlWhiteListRowStatus)
    {
            /* To set create and go, no similar entry should be existing previously.
             */
        case FWL_CREATE_AND_GO:

            RBTreeCount (gFwlWhiteList, &u4Count);

            if (MAX_FWL_WHITE_LIST_ENTRIES == u4Count)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            if (SNMP_FAILURE == nmhValidateIndexInstanceFwlDefnWhiteListTable
                (i4FwlWhiteListIpAddressType,
                 pFwlWhiteListIpAddress, u4FwlWhiteListIpMask))
            {
                *pu4ErrorCode = SNMP_ERR_BAD_VALUE;
                CLI_SET_ERR (CLI_FWL_ERR_INVALID_BLACK_WHITE_LIST);
                return SNMP_FAILURE;
            }

            if (NULL != pFwlWhiteListNode)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_FWL_ERR_EXISTING_ENTRY);
                return SNMP_FAILURE;
            }
            break;

            /* To set the row status to be destroy, an entry should be 
             * existing already.
             */
        case FWL_DESTROY:
            if (NULL == pFwlWhiteListNode)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_FWL_ERR_INCONSISTENT_ENTRY);
                return SNMP_FAILURE;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlDefnWhiteListTable
 Input       :  The Indices
                FwlWhiteListIpAddressType
                FwlWhiteListIpAddress
                FwlWhiteListIpMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDefnWhiteListTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlDosAttackAcceptRedirect
 Input       :  The Indices

                The Object
                retValFwlDosAttackAcceptRedirect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlDosAttackAcceptRedirect (INT4 *pi4RetValFwlDosAttackAcceptRedirect)
{
    UNUSED_PARAM (pi4RetValFwlDosAttackAcceptRedirect);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlDosAttackAcceptRedirect
 Input       :  The Indices

                The Object
                setValFwlDosAttackAcceptRedirect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlDosAttackAcceptRedirect (INT4 i4SetValFwlDosAttackAcceptRedirect)
{
    UNUSED_PARAM (i4SetValFwlDosAttackAcceptRedirect);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFwlDosAttackAcceptSmurfAttack
 Input       :  The Indices

                The Object
                retValFwlDosAttackAcceptSmurfAttack
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlDosAttackAcceptSmurfAttack (INT4
                                     *pi4RetValFwlDosAttackAcceptSmurfAttack)
{
    UNUSED_PARAM (pi4RetValFwlDosAttackAcceptSmurfAttack);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlDosAttackAcceptSmurfAttack
 Input       :  The Indices

                The Object
                setValFwlDosAttackAcceptSmurfAttack
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlDosAttackAcceptSmurfAttack (INT4 i4SetValFwlDosAttackAcceptSmurfAttack)
{
    UNUSED_PARAM (i4SetValFwlDosAttackAcceptSmurfAttack);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FwlDosAttackAcceptSmurfAttack
 Input       :  The Indices

                The Object
                testValFwlDosAttackAcceptSmurfAttack
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlDosAttackAcceptSmurfAttack (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValFwlDosAttackAcceptSmurfAttack)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFwlDosAttackAcceptSmurfAttack);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2FwlDosAttackAcceptSmurfAttack
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDosAttackAcceptSmurfAttack (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FwlDosAttackAcceptRedirect
 Input       :  The Indices

                The Object
                testValFwlDosAttackAcceptRedirect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlDosAttackAcceptRedirect (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFwlDosAttackAcceptRedirect)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFwlDosAttackAcceptRedirect);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2FwlDosAttackAcceptRedirect
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDosAttackAcceptRedirect (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlDosLandAttack
 Input       :  The Indices

                The Object
                retValFwlDosLandAttack
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlDosLandAttack (INT4 *pi4RetValFwlDosLandAttack)
{
    UNUSED_PARAM (pi4RetValFwlDosLandAttack);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFwlDosLandAttack
 Input       :  The Indices

                The Object
                setValFwlDosLandAttack
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlDosLandAttack (INT4 i4SetValFwlDosLandAttack)
{
    UNUSED_PARAM (i4SetValFwlDosLandAttack);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FwlDosLandAttack
 Input       :  The Indices

                The Object
                testValFwlDosLandAttack
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlDosLandAttack (UINT4 *pu4ErrorCode, INT4 i4TestValFwlDosLandAttack)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFwlDosLandAttack);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2FwlDosLandAttack
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDosLandAttack (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlDosShortHeaderAttack
 Input       :  The Indices

                The Object
                retValFwlDosShortHeaderAttack
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlDosShortHeaderAttack (INT4 *pi4RetValFwlDosShortHeaderAttack)
{
    UNUSED_PARAM (pi4RetValFwlDosShortHeaderAttack);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFwlDosShortHeaderAttack
 Input       :  The Indices

                The Object
                setValFwlDosShortHeaderAttack
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlDosShortHeaderAttack (INT4 i4SetValFwlDosShortHeaderAttack)
{
    UNUSED_PARAM (i4SetValFwlDosShortHeaderAttack);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FwlDosShortHeaderAttack
 Input       :  The Indices

                The Object
                testValFwlDosShortHeaderAttack
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlDosShortHeaderAttack (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFwlDosShortHeaderAttack)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFwlDosShortHeaderAttack);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2FwlDosShortHeaderAttack
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlDosShortHeaderAttack (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlSnorkTable
 Input       :  The Indices
                FwlSnorkPortNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFwlSnorkTable (INT4 i4FwlSnorkPortNo)
{
    UNUSED_PARAM (i4FwlSnorkPortNo);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFwlSnorkTable
 Input       :  The Indices
                FwlSnorkPortNo
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFwlSnorkTable (INT4 *pi4FwlSnorkPortNo)
{
    UNUSED_PARAM (pi4FwlSnorkPortNo);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFwlSnorkTable
 Input       :  The Indices
                FwlSnorkPortNo
                nextFwlSnorkPortNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFwlSnorkTable (INT4 i4FwlSnorkPortNo,
                              INT4 *pi4NextFwlSnorkPortNo)
{
    UNUSED_PARAM (i4FwlSnorkPortNo);
    UNUSED_PARAM (pi4NextFwlSnorkPortNo);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFwlSnorkRowStatus
 Input       :  The Indices
                FwlSnorkPortNo

                The Object
                retValFwlSnorkRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlSnorkRowStatus (INT4 i4FwlSnorkPortNo,
                         INT4 *pi4RetValFwlSnorkRowStatus)
{
    UNUSED_PARAM (i4FwlSnorkPortNo);
    UNUSED_PARAM (pi4RetValFwlSnorkRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlSnorkRowStatus
 Input       :  The Indices
                FwlSnorkPortNo

                The Object
                setValFwlSnorkRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlSnorkRowStatus (INT4 i4FwlSnorkPortNo, INT4 i4SetValFwlSnorkRowStatus)
{
    UNUSED_PARAM (i4FwlSnorkPortNo);
    UNUSED_PARAM (i4SetValFwlSnorkRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlSnorkRowStatus
 Input       :  The Indices
                FwlSnorkPortNo

                The Object
                testValFwlSnorkRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlSnorkRowStatus (UINT4 *pu4ErrorCode, INT4 i4FwlSnorkPortNo,
                            INT4 i4TestValFwlSnorkRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FwlSnorkPortNo);
    UNUSED_PARAM (i4TestValFwlSnorkRowStatus);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2FwlSnorkTable
 Input       :  The Indices
                FwlSnorkPortNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlSnorkTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlRateLimitTable
 Input       :  The Indices
                FwlRateLimitPortIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFwlRateLimitTable (INT4 i4FwlRateLimitPortIndex)
{

    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFwlRateLimitTable
 Input       :  The Indices
                FwlRateLimitPortIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFwlRateLimitTable (INT4 *pi4FwlRateLimitPortIndex)
{
    UNUSED_PARAM (pi4FwlRateLimitPortIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFwlRateLimitTable
 Input       :  The Indices
                FwlRateLimitPortIndex
                nextFwlRateLimitPortIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFwlRateLimitTable (INT4 i4FwlRateLimitPortIndex,
                                  INT4 *pi4NextFwlRateLimitPortIndex)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (pi4NextFwlRateLimitPortIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFwlRateLimitPortNumber
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                retValFwlRateLimitPortNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlRateLimitPortNumber (INT4 i4FwlRateLimitPortIndex,
                              INT4 *pi4RetValFwlRateLimitPortNumber)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (pi4RetValFwlRateLimitPortNumber);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlRateLimitPortType
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                retValFwlRateLimitPortType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlRateLimitPortType (INT4 i4FwlRateLimitPortIndex,
                            INT4 *pi4RetValFwlRateLimitPortType)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (pi4RetValFwlRateLimitPortType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlRateLimitValue
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                retValFwlRateLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlRateLimitValue (INT4 i4FwlRateLimitPortIndex,
                         INT4 *pi4RetValFwlRateLimitValue)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (pi4RetValFwlRateLimitValue);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlRateLimitBurstSize
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                retValFwlRateLimitBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlRateLimitBurstSize (INT4 i4FwlRateLimitPortIndex,
                             INT4 *pi4RetValFwlRateLimitBurstSize)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (pi4RetValFwlRateLimitBurstSize);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlRateLimitTrafficMode
 Input       :  The Indices

                The Object
                retValFwlRateLimitTrafficMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlRateLimitTrafficMode (INT4 i4FwlRateLimitPortIndex,
                               INT4 *pi4RetValFwlRateLimitTrafficMode)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (pi4RetValFwlRateLimitTrafficMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlRateLimitRowStatus
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                retValFwlRateLimitRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlRateLimitRowStatus (INT4 i4FwlRateLimitPortIndex,
                             INT4 *pi4RetValFwlRateLimitRowStatus)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (pi4RetValFwlRateLimitRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlRateLimitPortNumber
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                setValFwlRateLimitPortNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlRateLimitPortNumber (INT4 i4FwlRateLimitPortIndex,
                              INT4 i4SetValFwlRateLimitPortNumber)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4SetValFwlRateLimitPortNumber);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlRateLimitPortType
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                setValFwlRateLimitPortType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlRateLimitPortType (INT4 i4FwlRateLimitPortIndex,
                            INT4 i4SetValFwlRateLimitPortType)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4SetValFwlRateLimitPortType);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFwlRateLimitValue
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                setValFwlRateLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlRateLimitValue (INT4 i4FwlRateLimitPortIndex,
                         INT4 i4SetValFwlRateLimitValue)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4SetValFwlRateLimitValue);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFwlRateLimitBurstSize
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                setValFwlRateLimitBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlRateLimitBurstSize (INT4 i4FwlRateLimitPortIndex,
                             INT4 i4SetValFwlRateLimitBurstSize)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4SetValFwlRateLimitBurstSize);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFwlRateLimitTrafficMode
 Input       :  The Indices

                The Object
                setValFwlRateLimitTrafficMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlRateLimitTrafficMode (INT4 i4FwlRateLimitPortIndex,
                               INT4 i4SetValFwlRateLimitTrafficMode)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4SetValFwlRateLimitTrafficMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlRateLimitRowStatus
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                setValFwlRateLimitRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlRateLimitRowStatus (INT4 i4FwlRateLimitPortIndex,
                             INT4 i4SetValFwlRateLimitRowStatus)
{
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4SetValFwlRateLimitRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlRateLimitPortNumber
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                testValFwlRateLimitPortNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlRateLimitPortNumber (UINT4 *pu4ErrorCode,
                                 INT4 i4FwlRateLimitPortIndex,
                                 INT4 i4TestValFwlRateLimitPortNumber)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4TestValFwlRateLimitPortNumber);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FwlRateLimitPortType
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                testValFwlRateLimitPortType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlRateLimitPortType (UINT4 *pu4ErrorCode,
                               INT4 i4FwlRateLimitPortIndex,
                               INT4 i4TestValFwlRateLimitPortType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4TestValFwlRateLimitPortType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FwlRateLimitValue
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                testValFwlRateLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlRateLimitValue (UINT4 *pu4ErrorCode, INT4 i4FwlRateLimitPortIndex,
                            INT4 i4TestValFwlRateLimitValue)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4TestValFwlRateLimitValue);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FwlRateLimitBurstSize
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                testValFwlRateLimitBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlRateLimitBurstSize (UINT4 *pu4ErrorCode,
                                INT4 i4FwlRateLimitPortIndex,
                                INT4 i4TestValFwlRateLimitBurstSize)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4TestValFwlRateLimitBurstSize);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FwlRateLimitTrafficMode
 Input       :  The Indices

                The Object
                testValFwlRateLimitTrafficMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlRateLimitTrafficMode (UINT4 *pu4ErrorCode,
                                  INT4 i4FwlRateLimitPortIndex,
                                  INT4 i4TestValFwlRateLimitTrafficMode)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4TestValFwlRateLimitTrafficMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FwlRateLimitRowStatus
 Input       :  The Indices
                FwlRateLimitPortIndex

                The Object
                testValFwlRateLimitRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlRateLimitRowStatus (UINT4 *pu4ErrorCode,
                                INT4 i4FwlRateLimitPortIndex,
                                INT4 i4TestValFwlRateLimitRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FwlRateLimitPortIndex);
    UNUSED_PARAM (i4TestValFwlRateLimitRowStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FwlRateLimitTable
 Input       :  The Indices
                FwlRateLimitPortIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlRateLimitTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFwlRpfTable
 Input       :  The Indices
                FwlRpfInIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFwlRpfTable (INT4 i4FwlRpfInIndex)
{
    UNUSED_PARAM (i4FwlRpfInIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFwlRpfTable
 Input       :  The Indices
                FwlRpfInIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFwlRpfTable (INT4 *pi4FwlRpfInIndex)
{
    UNUSED_PARAM (pi4FwlRpfInIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFwlRpfTable
 Input       :  The Indices
                FwlRpfInIndex
                nextFwlRpfInIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFwlRpfTable (INT4 i4FwlRpfInIndex, INT4 *pi4NextFwlRpfInIndex)
{
    UNUSED_PARAM (i4FwlRpfInIndex);
    UNUSED_PARAM (pi4NextFwlRpfInIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFwlRpfMode
 Input       :  The Indices
                FwlRpfInIndex

                The Object
                retValFwlRpfMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlRpfMode (INT4 i4FwlRpfInIndex, INT4 *pi4RetValFwlRpfMode)
{
    UNUSED_PARAM (i4FwlRpfInIndex);
    UNUSED_PARAM (pi4RetValFwlRpfMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFwlRpfRowStatus
 Input       :  The Indices
                FwlRpfInIndex

                The Object
                retValFwlRpfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFwlRpfRowStatus (INT4 i4FwlRpfInIndex, INT4 *pi4RetValFwlRpfRowStatus)
{

    UNUSED_PARAM (i4FwlRpfInIndex);
    UNUSED_PARAM (pi4RetValFwlRpfRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFwlRpfMode
 Input       :  The Indices
                FwlRpfInIndex

                The Object
                setValFwlRpfMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlRpfMode (INT4 i4FwlRpfInIndex, INT4 i4SetValFwlRpfMode)
{
    UNUSED_PARAM (i4FwlRpfInIndex);
    UNUSED_PARAM (i4SetValFwlRpfMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFwlRpfRowStatus
 Input       :  The Indices
                FwlRpfInIndex

                The Object
                setValFwlRpfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFwlRpfRowStatus (INT4 i4FwlRpfInIndex, INT4 i4SetValFwlRpfRowStatus)
{
    UNUSED_PARAM (i4FwlRpfInIndex);
    UNUSED_PARAM (i4SetValFwlRpfRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FwlRpfMode
 Input       :  The Indices
                FwlRpfInIndex

                The Object
                testValFwlRpfMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlRpfMode (UINT4 *pu4ErrorCode, INT4 i4FwlRpfInIndex,
                     INT4 i4TestValFwlRpfMode)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FwlRpfInIndex);
    UNUSED_PARAM (i4TestValFwlRpfMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FwlRpfRowStatus
 Input       :  The Indices
                FwlRpfInIndex

                The Object
                testValFwlRpfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FwlRpfRowStatus (UINT4 *pu4ErrorCode, INT4 i4FwlRpfInIndex,
                          INT4 i4TestValFwlRpfRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FwlRpfInIndex);
    UNUSED_PARAM (i4TestValFwlRpfRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FwlRpfTable
 Input       :  The Indices
                FwlRpfInIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FwlRpfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
