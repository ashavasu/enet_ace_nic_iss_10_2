/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlsnif.c,v 1.11 2015/04/20 05:41:18 siva Exp $
 *
 * Description:This file contains routines to update      
 *             and modify Data Base relating to           
 *             accesslist                            
 *
 *******************************************************************/
#include "fwlinc.h"

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlValidateProtocol                              */
/*                                                                          */
/*    Description        : Validate the Protocol value                      */
/*                                                                          */
/*    Input(s)           : i4Protocol                                       */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SUCCESS if Values is valid, otherwise FAILURE    */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1
FwlValidateProtocol (INT4 i4Protocol)
#else
PUBLIC INT1
FwlValidateProtocol (i4Protocol)
     INT4                i4Protocol;
#endif
{
    INT1                i1Status = FWL_ZERO;

    i1Status = SNMP_FAILURE;
    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into Function FwlValidateProtocol\n");

    /* this function validates whether the protocol value is one of the 
     * already defined one.
     */
    switch (i4Protocol)
    {
        case FWL_ICMP:
        case FWL_IGMP:
        case FWL_EGP:
        case FWL_GGP:
        case FWL_IGP:
        case FWL_IP:
        case FWL_TCP:
        case FWL_UDP:
        case FWL_NVP:
        case FWL_IRTP:
        case FWL_IDPR:
        case FWL_RSVP:
        case FWL_MHRP:
        case FWL_IGRP:
        case FWL_OSPFIGP:
        case FWL_PIM:
        case FWL_GRE_PPTP:
        case FWL_DEFAULT_PROTO:
            i1Status = SNMP_SUCCESS;
            break;

        default:
            if ((i4Protocol >= FWL_ONE) && (i4Protocol <= FWL_DEFAULT_PROTO))
            {
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                i1Status = SNMP_FAILURE;
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\nInvalid Protocol value\n");
            }
            break;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from Function FwlValidateProtocol\n");

    return i1Status;
}                                /* End of the function -- FwlValidateProtocol */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlValidateIcmpType                               */
/*                                                                          */
/*    Description        : Validate the Icmp Type Value                     */
/*                                                                          */
/*    Input(s)           : i4IcmpType                                       */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SUCCESS if Values is valid, otherwise FAILURE    */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1
FwlValidateIcmpType (INT4 i4IcmpType)
#else
PUBLIC INT1
FwlValidateIcmpType (i4IcmpType)
     INT4                i4IcmpType;
#endif
{
    INT1                i1Status = FWL_ZERO;

    i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into Function FwlValidateIcmpType\n");

    /* this function validates whether the ICMP type value is one of the
     * already defined one
     */
    if (i4IcmpType == FWL_ECHO_REPLY)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpType == FWL_DEST_UNREACHABLE)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpType == FWL_SOURCE_QUENCH)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpType == FWL_REDIRECT)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpType == FWL_ECHO_REQUEST)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpType == FWL_TIME_EXCEEDED)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpType == FWL_PARAMETER_PROBLEM)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpType == FWL_TIMESTAMP_REQUEST)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpType == FWL_TIMESTAMP_REPLY)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpType == FWL_INFORMATION_REQUEST)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpType == FWL_INFORMATION_REPLY)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpType == FWL_ADDR_MASK_REQUEST)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpType == FWL_ADDR_MASK_REPLY)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpType == FWL_NO_ICMP_TYPE)
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        i1Status = SNMP_FAILURE;
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\nInvalid ICMP Type value\n");
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from Function FwlValidateIcmpType\n");

    return i1Status;
}                                /* End of the function -- FwlValidateIcmpType */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlValidateIcmpCode                              */
/*                                                                          */
/*    Description        : Validate the Icmp Code Value                     */
/*                                                                          */
/*    Input(s)           : i4IcmpCode                                       */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SUCCESS if Values is valid, otherwise FAILURE    */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1
FwlValidateIcmpCode (INT4 i4IcmpCode)
#else
PUBLIC INT1
FwlValidateIcmpCode (i4IcmpCode)
     INT4                i4IcmpCode;
#endif
{
    INT1                i1Status = FWL_ZERO;

    i1Status = SNMP_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into Function FwlValidateIcmpCode\n");

    /* This function validates whether the given ICMP code value is one 
     * of the already defined one
     */
    if (i4IcmpCode == FWL_NETWORK_UNREACHABLE)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpCode == FWL_PORT_UNREACHABLE)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpCode == FWL_DEST_HOST_ADMIN_PROHIBITED)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpCode == FWL_HOST_UNREACHABLE)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpCode == FWL_PROTOCOL_UNREACHABLE)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpCode == FWL_FRAGMENT_NEED)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpCode == FWL_SOURCE_ROUTE_FAIL)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpCode == FWL_DEST_NETWORK_UNKNOWN)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpCode == FWL_DEST_HOST_UNKNOWN)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpCode == FWL_SRC_HOST_ISOLATED)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpCode == FWL_DEST_NETWORK_ADMIN_PROHIBITED)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpCode == FWL_NETWORK_UNREACHABLE_TOS)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpCode == FWL_HOST_UNREACHABLE_TOS)
    {
        i1Status = SNMP_SUCCESS;
    }
    else if (i4IcmpCode == FWL_NO_ICMP_CODE)
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        i1Status = SNMP_FAILURE;
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\nInvalid ICMP Code value\n");
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from Function FwlValidateIcmpCode\n");

    return i1Status;

}                                /* End of the function -- FwlValidateIcmpCode */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSetDefaultFilterValue                         */
/*                                                                          */
/*    Description        : Set the Default values for Filter Node           */
/*                                                                          */
/*    Input(s)           : au1FilterName   --Filter Name                    */
/*                                                                          */
/*    Output(s)          : pFilterNode     -- Pointer to Filter Node Created*/
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlSetDefaultFilterValue (UINT1 au1FilterName[FWL_MAX_FILTER_NAME_LEN],
                          tFilterInfo * pFilterNode)
#else
PUBLIC VOID
FwlSetDefaultFilterValue (au1FilterName, pFilterNode)
     UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN];
     tFilterInfo        *pFilterNode;
#endif
{
    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering into Function FwlSetDefaultFilterValue\n");
    /* Memory allocation and initialisation of the Filter node with
     * default values .
     */
    TMO_SLL_Init_Node (&pFilterNode->nextFilterInfo);
    FWL_STRNCPY ((INT1 *) pFilterNode->au1FilterName, (INT1 *) au1FilterName,
                 FWL_MAX_FILTER_NAME_LEN - FWL_ONE);
    pFilterNode->SrcStartAddr.v4Addr = FWL_DEFAULT_ADDRESS;
    pFilterNode->SrcStartAddr.u4AddrType = FWL_IP_VERSION_4;
    pFilterNode->SrcEndAddr.v4Addr = FWL_DEFAULT_ADDRESS;
    pFilterNode->SrcEndAddr.u4AddrType = FWL_IP_VERSION_4;
    pFilterNode->DestStartAddr.v4Addr = FWL_DEFAULT_ADDRESS;
    pFilterNode->DestStartAddr.u4AddrType = FWL_IP_VERSION_4;
    pFilterNode->DestEndAddr.v4Addr = FWL_DEFAULT_ADDRESS;
    pFilterNode->DestEndAddr.u4AddrType = FWL_IP_VERSION_4;
    pFilterNode->u1Proto = FWL_DEFAULT_PROTO;
    pFilterNode->u2SrcMaxPort = FWL_DEFAULT_PORT;
    pFilterNode->u2SrcMinPort = FWL_DEFAULT_PORT;
    pFilterNode->u2DestMaxPort = FWL_DEFAULT_PORT;
    pFilterNode->u2DestMinPort = FWL_DEFAULT_PORT;
    pFilterNode->u1TcpAck = FWL_TCP_ACK_ANY;
    pFilterNode->u1TcpRst = FWL_TCP_RST_ANY;
    pFilterNode->u1Tos = FWL_TOS_ANY;
    pFilterNode->u1FilterRefCount = FWL_DEFAULT_COUNT;
    pFilterNode->u4FilterHitCount = FWL_DEFAULT_COUNT;
    pFilterNode->u1FilterAccounting = FWL_DISABLE;
    /* Changed default Rowstatus from NOT_IN_SERVICE to
     * NOT_READY */
    pFilterNode->u1RowStatus = FWL_NOT_READY;
    FWL_STRCPY (pFilterNode->au1SrcPort, FWL_NULL_STRING);
    FWL_STRCPY (pFilterNode->au1DestPort, FWL_NULL_STRING);

    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting from Function FwlSetDefaultFilterValue\n");

}                                /* End of the function -- FwlSetDefaultFilterValue */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSetDefaultRuleValue                           */
/*                                                                          */
/*    Description        : Set the Default values for Rule Node             */
/*                                                                          */
/*    Input(s)           : au1RuleName -- Rule Name                         */
/*                                                                          */
/*    Output(s)          : pRuleNode  -- Pointer to Rule Node Created       */
/*                                                                          */
/*    Returns            : None.                                            */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlSetDefaultRuleValue (UINT1 au1RuleName[FWL_MAX_RULE_NAME_LEN],
                        tRuleInfo * pRuleNode)
#else
PUBLIC VOID
FwlSetDefaultRuleValue (au1RuleName, pRuleNode)
     UINT1               au1RuleName[FWL_MAX_RULE_NAME_LEN];
     tRuleInfo          *pRuleNode;
#endif
{
    UINT1               u1Index = FWL_ZERO;

    /* initialisation of the Rule node with default values .  */

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entring into Function FwlSetDefaultRuleValue\n");
    TMO_SLL_Init_Node (&pRuleNode->nextRuleInfo);
    FWL_STRNCPY ((INT1 *) pRuleNode->au1RuleName, (INT1 *) au1RuleName,
                 FWL_MAX_RULE_NAME_LEN - FWL_ONE);

    for (u1Index = FWL_INDEX_0; u1Index < FWL_MAX_FILTERS_IN_RULE; u1Index++)
    {
        pRuleNode->apFilterInfo[u1Index] = (tFilterInfo *) NULL;
    }
    pRuleNode->u1FilterConditionFlag = FWL_ZERO;
    pRuleNode->u1RuleRefCount = FWL_DEFAULT_COUNT;
    pRuleNode->u1RowStatus = FWL_NOT_IN_SERVICE;
    FWL_STRCPY (pRuleNode->au1FilterSet, FWL_NULL_STRING);

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from Function FwlSetDefaultRuleValue\n");
}                                /* End of the function -- FwlSetDefaultRuleValue */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSetDefaultIfaceValue                          */
/*                                                                          */
/*    Description        : Set the Default values for Interface node        */
/*                                                                          */
/*    Input(s)           : u4IfaceNum  -- Interface number                  */
/*                                                                          */
/*    Output(s)          : pIfaceNode -- Pointer to interface Node          */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlSetDefaultIfaceValue (UINT4 u4IfaceNum, tIfaceInfo * pIfaceNode)
#else
PUBLIC VOID
FwlSetDefaultIfaceValue (u4IfaceNum, pIfaceNode)
     UINT4               u4IfaceNum;
     tIfaceInfo         *pIfaceNode;
#endif
{
    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering into Function FwlSetDefaultIfaceValue\n");

    /* initialisation of the Interface node with default values . */
    TMO_SLL_Init (&pIfaceNode->inFilterList);
    TMO_SLL_Init (&pIfaceNode->outFilterList);
    TMO_SLL_Init (&pIfaceNode->inIPv6FilterList);
    TMO_SLL_Init (&pIfaceNode->outIPv6FilterList);
    pIfaceNode->ifaceStat.u4AclConfigCount = FWL_DEFAULT_COUNT;
    pIfaceNode->ifaceStat.u4PktsPermitCount = FWL_DEFAULT_COUNT;
    pIfaceNode->ifaceStat.u4PktsDenyCount = FWL_DEFAULT_COUNT;
    pIfaceNode->ifaceStat.u4FragmentPktsDenyCount = FWL_DEFAULT_COUNT;
    pIfaceNode->ifaceStat.u4IpOptionPktsDenyCount = FWL_DEFAULT_COUNT;
    pIfaceNode->ifaceStat.u4IcmpPktsDenyCount = FWL_DEFAULT_COUNT;
    pIfaceNode->ifaceStat.u4SrcRouteAttackPktsDenyCount = FWL_DEFAULT_COUNT;
    pIfaceNode->ifaceStat.u4TinyFragAttackPktsDenyCount = FWL_DEFAULT_COUNT;
    pIfaceNode->ifaceStat.u4IPAddrSpoofedPktsDenyCount = FWL_DEFAULT_COUNT;
    pIfaceNode->ifaceStat.u4SynPktsDenyCount = FWL_DEFAULT_COUNT;
    pIfaceNode->u4IfaceNum = u4IfaceNum;
    pIfaceNode->u1IfaceType = FWL_EXTERNAL_IF;
    pIfaceNode->u1IpOption = FWL_ANY_OPTION;
    pIfaceNode->u1Fragment = FWL_LARGE_FRAGMENT;
    pIfaceNode->u2MaxFragmentSize = FWL_DEFAULT_FRAGMENT_SIZE;
    pIfaceNode->u1IcmpType = FWL_NO_ICMP_TYPE;
    pIfaceNode->u1IcmpCode = FWL_NO_ICMP_CODE;
    pIfaceNode->u1RowStatus = FWL_ACTIVE;
    pIfaceNode->ifaceStat.i4TrapThreshold = FWL_DEF_TRAP_THRESHOLD;
    gFwlAclInfo.apIfaceList[u4IfaceNum] = pIfaceNode;

    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting from Function FwlSetDefaultIfaceValue\n");
}                                /* End of the function -- FwlSetDefaultIfaceValue */

/* CHANGE2: The functions FwlSetDefaultInFilterValue and
 * FwlSetDefaultOutFilterValue are merged to form a single function
 * FwlSetDefaultAclValue. The functions are merge since the functionality is
 * same and List are differnt. But the structure Nodes forming the lists are ]
 * also same.Hence the functions are merged. 
 */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSetDefaultAclFilterValue                      */
/*                                                                          */
/*    Description        : Set the Default values for OutFilter  Node or    */
/*                         InFilter Node.                                   */
/*                                                                          */
/*    Input(s)           : au1AclName     -- Filter Name                    */
/*                         pAclFilterNode -- Pointer to the Aclfilter node  */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : Returns SNMP_SUCCESS if initila values are set   */
/*                         properly, else SNMP_FAILURE.                     */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1         FwlSetDefaultAclFilterValue
    (UINT1 au1AclName[FWL_MAX_ACL_NAME_LEN], tAclInfo * pAclFilterNode)
#else
PUBLIC INT1
FwlSetDefaultAclFilterValue (au1AclName, pAclFilterNode)
     UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN];
     tAclInfo           *pAclFilterNode;
#endif
{
    tFilterInfo        *pFilterNode = NULL;
    tRuleInfo          *pRuleNode = NULL;
    INT1                i1Status = FWL_ZERO;

    i1Status = SNMP_SUCCESS;
    pFilterNode = (tFilterInfo *) NULL;
    pRuleNode = (tRuleInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY,
             "\nEntering into Function FwlSetDefaultAclFilterValue\n");
    /* Initialisation of the AclFilter node with default values .  */
    TMO_SLL_Init_Node (&pAclFilterNode->nextFilterInfo);
    FWL_STRNCPY ((INT1 *) pAclFilterNode->au1FilterName, (INT1 *) au1AclName,
                 FWL_MAX_ACL_NAME_LEN - FWL_ONE);

    /* The filter List is searched first to get the pointer to the Node.
     * If not then the Rule list will be
     * searched and the pointers to the filters will be updated. If not
     * found then error is returned and the node is freed.
     */
    pFilterNode = FwlDbaseSearchFilter (au1AclName);
    if ((pFilterNode != NULL) && (pFilterNode->u1RowStatus == FWL_ACTIVE))
    {
        pAclFilterNode->pAclName = (tFilterInfo *) pFilterNode;
        pAclFilterNode->u1AclType = ACL_FILTER;
        INC_FILTER_REF_COUNT (pFilterNode);
    }
    else
    {
        pRuleNode = FwlDbaseSearchRule (au1AclName);
        if ((pRuleNode != NULL) && (pRuleNode->u1RowStatus == FWL_ACTIVE))
        {
            pAclFilterNode->pAclName = (tRuleInfo *) pRuleNode;
            pAclFilterNode->u1AclType = ACL_RULE;
            INC_RULE_REF_COUNT (pRuleNode);
        }
        else
        {
            i1Status = SNMP_FAILURE;
        }
    }
    if (i1Status == SNMP_SUCCESS)
    {
        pAclFilterNode->u1Action = FWL_ZERO;
        pAclFilterNode->u2SeqNum = FWL_ZERO;
        pAclFilterNode->u4StartTime = FWL_ZERO;
        pAclFilterNode->u4EndTime = FWL_END_TIME;
        pAclFilterNode->u1RowStatus = FWL_NOT_IN_SERVICE;
        pAclFilterNode->u1LogTrigger= FWL_LOG_BRF;
    }
    else
    {
        FwlAclMemFree ((UINT1 *) pAclFilterNode);
    }
    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting from Function FwlSetDefaultAclFilterValue\n");
    return i1Status;
}                                /* End of the function -- FwlSetDefaultAclFilterValue */

/* CHANGE2 : Repetition of code for IN Direction and OUT Direction was
 * modified.
 */
/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSetInitForAclNode                             */
/*                                                                          */
/*    Description        : Does Memory allcation for the Iface Node and Acl */
/*                         Node and initialises it.                         */
/*                                                                          */
/*    Input(s)           : u4IfaceNum   -- Interface Number                 */
/*                         i4Direction  -- Direction                        */
/*                         au1AclName   -- Accesslist Name                  */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SNMP_SUCCESS if Node is Created , otherwise      */
/*                         SNMP_FAILURE.                                    */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1
FwlSetInitForAclNode (UINT4 u4IfaceNum,
                      INT4 i4Direction, UINT1 au1AclName[FWL_MAX_ACL_NAME_LEN])
#else
PUBLIC INT1
FwlSetInitForAclNode (u4IfaceNum, i4Direction, au1AclName[FWL_MAX_ACL_NAME_LEN])
     UINT4               u4IfaceNum;
     INT4                i4Direction;
     UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN];
#endif
{
    tIfaceInfo         *pIfaceNode = NULL;
    tAclInfo           *pAclTmpNode = NULL;
    tAclInfo           *pAclNode = NULL;
    tFilterInfo        *pFilterNode = NULL;
    tRuleInfo          *pRuleFilter = NULL;
    INT1                i1Status = FWL_ZERO;

    i1Status = SNMP_SUCCESS;
    pIfaceNode = (tIfaceInfo *) NULL;
    pAclTmpNode = (tAclInfo *) NULL;
    pAclNode = (tAclInfo *) NULL;
    pRuleFilter = (tRuleInfo *) NULL;
    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nEntering into Function FwlSetInitForAclNode\n");

    /* Search for the interface node. If node is not found then create it. */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        /* Interface node Allocation and Initialisation */
        if (FwlIfaceMemAllocate ((UINT1 **) (VOID *) &pIfaceNode) ==
            FWL_SUCCESS)
        {
            FwlSetDefaultIfaceValue (u4IfaceNum, pIfaceNode);
            gu1FwlIfaceFlag = FWL_TRUE;

            /* Add to interface list */
            gFwlAclInfo.apIfaceList[u4IfaceNum] = pIfaceNode;
            i1Status = SNMP_SUCCESS;
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\nInterface Index %d is created. \n", u4IfaceNum);
        }
        else
        {
            /* send a trap for memory failure */
            FwlGenerateMemFailureTrap (FWL_IFACE_ALLOC_FAILURE);
            INC_MEM_FAILURE_COUNT;
            gFwlAclInfo.i4MemStatus = (INT4) MEM_FAILURE;
            i1Status = SNMP_FAILURE;
        }
    }
    /* If the Interface Node exits or if the Node is created successfully then
     * Search whether the InFilter Already exits if direction is IN. If not 
     * then create the InFilter Node. If Direction is OUT and the Node doesnt
     * exist then create the Outfilter Node.
     */
    if (i1Status == SNMP_SUCCESS)
    {
        if (i4Direction == FWL_DIRECTION_IN)
        {
            pAclTmpNode = FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                                   au1AclName);
            if (pAclTmpNode == NULL)
            {
                /*Entry not found in IPv4 list .. check in ipv6 list */
                pAclTmpNode = FwlDbaseSearchAclFilter
                    (&pIfaceNode->inIPv6FilterList, au1AclName);
            }
        }
        else
        {
            pAclTmpNode = FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                                   au1AclName);
            if (pAclTmpNode == NULL)
            {
                /*Entry not found in IPv4 list .. check in ipv6 list */
                pAclTmpNode = FwlDbaseSearchAclFilter
                    (&pIfaceNode->inIPv6FilterList, au1AclName);
            }
        }
        if (pAclTmpNode == NULL)
        {
            /* Acl node Allocation and Initialisation */
            if (FwlAclMemAllocate ((UINT1 **) (VOID *) &pAclNode) ==
                FWL_SUCCESS)
            {
                i1Status = FwlSetDefaultAclFilterValue (au1AclName, pAclNode);
                pRuleFilter = (tRuleInfo *) pAclNode->pAclName;
                if (pRuleFilter == NULL)
                {
                    return FWL_FAILURE;
                }
                pFilterNode =
                    pRuleFilter->apFilterInfo[FWL_RULE_FIRST_FILTER_INDEX];
                if (pFilterNode == NULL)
                {
                    return FWL_FAILURE;
                }
                if (i1Status == SNMP_SUCCESS)
                {
                    /* Add the Out Filter node to the Out Filter list 
                     * if Direction is OUT else add the Node to the InFIlter 
                     * List.
                     */
                    if (i4Direction == FWL_DIRECTION_IN)
                    {
                        if (pFilterNode->u2AddrType == FWL_IP_VERSION_4)
                        {
                            TMO_SLL_Add (&pIfaceNode->inFilterList,
                                         (tTMO_SLL_NODE *) pAclNode);
                        }
                        else
                        {
                            TMO_SLL_Add (&pIfaceNode->inIPv6FilterList,
                                         (tTMO_SLL_NODE *) pAclNode);
                        }
                        MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                      "\n Filter/ Rule %s is added on Interface"
                                      "%d in the IN Direction\n",
                                      au1AclName, u4IfaceNum);
                    }
                    else
                    {            /*Direction is OUT */
                        if (pFilterNode->u2AddrType == FWL_IP_VERSION_4)
                        {
                            TMO_SLL_Add (&pIfaceNode->outFilterList,
                                         (tTMO_SLL_NODE *) pAclNode);
                        }
                        else
                        {
                            TMO_SLL_Add (&pIfaceNode->outIPv6FilterList,
                                         (tTMO_SLL_NODE *) pAclNode);
                        }
                        MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                      "\n Filter/ Rule %s is added on Interface"
                                      "%d in the OUT Direction\n",
                                      au1AclName, u4IfaceNum);
                    }
                    INC_IFACE_CONFIG_COUNT (pIfaceNode);
                }
                else
                {
                    if (gu1FwlIfaceFlag == FWL_TRUE)
                    {
                        FwlDbaseDeleteIface (u4IfaceNum);
                    }

                }

            }
            else
            {
                if (gu1FwlIfaceFlag == FWL_TRUE)
                {
                    FwlDbaseDeleteIface (u4IfaceNum);
                }

                /* send a trap for memory failure */
                FwlGenerateMemFailureTrap (FWL_ACL_ALLOC_FAILURE);
                INC_MEM_FAILURE_COUNT;
                gFwlAclInfo.i4MemStatus = (INT4) MEM_FAILURE;
                i1Status = SNMP_FAILURE;
            }
        }
        else
        {

            i1Status = SNMP_SUCCESS;
        }
    }                            /* end for i1Status checking */
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from Function FwlSetInitForAclNode\n");

    return i1Status;
}                                /* End of the Function  --  FwlSetInitForAclNode */

/* CHANGE1 : If the Filter is to be configured globally then the action value
 * and Sequence Number value for that particular filter must be added on all 
 * the filter nodes on all interfaces. So the Action and Sequence Number SET
 * routines must be changed. Those tow function require the following two
 * supporting fuctions to SET the value. 
 */

/* CHANGE2 : Repetition of code for IN Direction and OUT Direction was
 * modified.
 */
/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSetActionValueForAclNode                      */
/*                                                                          */
/*    Description        : Sets the action value for the corresponding Acl  */
/*                         Node .                                           */
/*                                                                          */
/*    Input(s)           : pIfaceNode   -- Pointer to the Interface Node    */
/*                         i4Direction  -- Direction                        */
/*                         au1AclName   -- Accesslist Name                  */
/*                         i4Action     -- Action Value                     */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SNMP_SUCCESS if Value is set , otherwise         */
/*                         SNMP_FAILURE.                                    */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1
FwlSetActionValueForAclNode (tIfaceInfo * pIfaceNode,
                             INT4 i4Direction,
                             UINT1 au1AclName[FWL_MAX_ACL_NAME_LEN],
                             INT4 i4Action)
#else
PUBLIC INT1
FwlSetActionValueForAclNode (pIfaceNode, i4Direction, au1AclName, i4Action)
     tIfaceInfo         *pIfaceNode;
     INT4                i4Direction = FWL_ZERO;
     UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
INT4                i4Action = FWL_ZERO;
#endif
{
    INT1                i1Status = FWL_ZERO;
    tAclInfo           *pAclNode = NULL;

    i1Status = SNMP_FAILURE;
    pAclNode = (tAclInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering into Function FwlSetActionValueForAclNode\n");
    /* search the interface number .if direction is IN then search the Infilter
     * list and and set the action value for the particular filter name .if 
     * direction is OUT, do the same in outfilter list. 
     */
    if (pIfaceNode->u1RowStatus == FWL_ACTIVE)
    {
        if (i4Direction == FWL_DIRECTION_IN)
        {
            pAclNode =
                FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList, au1AclName);
            if (pAclNode == NULL)
            {
                pAclNode = FwlDbaseSearchAclFilter
                    (&pIfaceNode->inIPv6FilterList, au1AclName);
            }
            MOD_TRC_ARG3 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          " \n Action Value %d is set for Acl Filter %s "
                          "on interface %d in the IN direction\n ",
                          i4Action, au1AclName, pIfaceNode->u4IfaceNum);
        }
        else if (i4Direction == FWL_DIRECTION_OUT)
        {
            pAclNode =
                FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                         au1AclName);
            if (pAclNode == NULL)
            {
                pAclNode =
                    FwlDbaseSearchAclFilter (&pIfaceNode->outIPv6FilterList,
                                             au1AclName);
            }
            MOD_TRC_ARG3 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          " \n Action Value %d is set for Acl Filter %s "
                          "on interface %d in the OUT direction\n ",
                          i4Action, au1AclName, pIfaceNode->u4IfaceNum);
        }
        if (pAclNode == NULL)
        {
            return i1Status;
        }

        if ((pAclNode->u1Action == FWL_PERMIT) && (i4Action == FWL_DENY))
        {
            /* Set the Rule's Action to DENY so that the matching ACL & State
             * Table Entries would be destroyed @ FwlCommitAcl () */
            pAclNode->u1Action = (UINT1) i4Action;
            FwlCommitAcl ();
        }
        /* set the Action Value */
        pAclNode->u1Action = (UINT1) i4Action;
        i1Status = SNMP_SUCCESS;
    }

    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting from Function FwlSetActionValueForAclNode\n");
    return i1Status;

}                                /* End of the Function  --  FwlSetActionValueForAclNode */

/* CHANGE2 : Repetition of code for IN Direction and OUT Direction was
 * modified.
 */
/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSetSeqNumValueForAclNode                      */
/*                                                                          */
/*    Description        : Sets the Sequence number value for the           */
/*                         corresponding Acl Node.                          */
/*                                                                          */
/*    Input(s)           : pIfaceNode   -- Pointer to the Interface Node    */
/*                         i4Direction  -- Direction                        */
/*                         au1AclName   -- Accesslist Name                  */
/*                         u2SeqNum     -- Sequence Number                  */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SNMP_SUCCESS if value is Set, otherwise          */
/*                         SNMP_FAILURE.                                    */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1
FwlSetSeqNumValueForAclNode (tIfaceInfo * pIfaceNode,
                             INT4 i4Direction,
                             UINT1 au1AclName[FWL_MAX_ACL_NAME_LEN],
                             UINT2 u2SeqNum)
#else
PUBLIC INT1
FwlSetSeqNumValueForAclNode (pIfaceNode, i4Direction, au1AclName, u2SeqNum)
     tIfaceInfo         *pIfaceNode;
     INT4                i4Direction = FWL_ZERO;
     UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
UINT2               u2SeqNum = FWL_ZERO;
#endif
{
    INT1                i1Status = FWL_ZERO;
    UINT4               u4Status = FWL_ZERO;
    UINT4               u4IfaceNum = FWL_ZERO;
    tAclInfo           *pAclNode = NULL;

    i1Status = SNMP_FAILURE;
    u4Status = FWL_FAILURE;
    pAclNode = (tAclInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY,
             "\nEntering into function FwlSetSeqNumValueForAclNode\n");

    /* search the interface index. if direction is IN then search the infilter
     * list for the corresponding filter name, else the outfilter list. 
     */
    u4IfaceNum = pIfaceNode->u4IfaceNum;

    if (pIfaceNode->u1RowStatus == FWL_ACTIVE)
    {
        if (i4Direction == FWL_DIRECTION_IN)
        {
            pAclNode =
                FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList, au1AclName);
            if (pAclNode == NULL)
            {
                pAclNode =
                    FwlDbaseSearchAclFilter (&pIfaceNode->inIPv6FilterList,
                                             au1AclName);
            }
        }
        else if (i4Direction == FWL_DIRECTION_OUT)
        {
            pAclNode =
                FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                         au1AclName);
            if (pAclNode == NULL)
            {
                pAclNode =
                    FwlDbaseSearchAclFilter (&pIfaceNode->outIPv6FilterList,
                                             au1AclName);
            }
        }
        if (pAclNode == NULL)
        {
            return i1Status;
        }
        /* Set the Action Value */
        u4Status = FwlDbaseAddAclFilterInSequence (u4IfaceNum, u2SeqNum,
                                                   pAclNode, i4Direction);
        if (u4Status == FWL_SUCCESS)
        {
            i1Status = SNMP_SUCCESS;
        }
    }                            /* end of if */

    FWL_DBG (FWL_DBG_EXIT,
             "\nExiting from function FwlSetSeqNumValueForAclNode\n");
    return i1Status;

}                                /* End of the Function  --  FwlSetSeqNumValueForAclNode */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSetStartTimeForAclNode                        */
/*                                                                          */
/*    Description        : Sets the start time for the corresponding Acl    */
/*                         Node .                                           */
/*                                                                          */
/*    Input(s)           : pIfaceNode   -- Pointer to the Interface Node    */
/*                         i4Direction  -- Direction of ACL                 */
/*                         au1AclName   -- Accesslist Name                  */
/*                         u4StartTime  -- Schedule control start time      */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SNMP_SUCCESS if Value is set , otherwise         */
/*                         SNMP_FAILURE.                                    */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1
FwlSetStartTimeForAclNode (tIfaceInfo * pIfaceNode,
                           INT4 i4Direction,
                           UINT1 au1AclName[FWL_MAX_ACL_NAME_LEN],
                           UINT4 u4StartTime)
#else
PUBLIC INT1
FwlSetStartTimeForAclNode (pIfaceNode, i4Direction, au1AclName, u4StartTime)
     tIfaceInfo         *pIfaceNode;
     INT4                i4Direction;
     UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN];
     UINT4               u4StartTime;
#endif
{
    INT1                i1Status = FWL_ZERO;
    tAclInfo           *pAclNode = NULL;

    i1Status = SNMP_FAILURE;
    pAclNode = (tAclInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering into Function FwlStartTimeDetailsForAclNode\n");
    /* search the interface number .if direction is IN then search the Infilter
     * list and and set the action value for the particular filter name .if 
     * direction is OUT, do the same in outfilter list. 
     */
    if (pIfaceNode->u1RowStatus == FWL_ACTIVE)
    {
        if (i4Direction == FWL_DIRECTION_IN)
        {
            pAclNode =
                FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList, au1AclName);
            MOD_TRC_ARG3 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          " \n Start time %d is set for Acl Filter %s "
                          "on interface %d in the IN direction\n ",
                          u4StartTime, au1AclName, pIfaceNode->u4IfaceNum);
        }
        else if (i4Direction == FWL_DIRECTION_OUT)
        {
            pAclNode =
                FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                         au1AclName);
            MOD_TRC_ARG3 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          " \n Start time %d is set for Acl Filter %s "
                          "on interface %d in the OUT direction\n ",
                          u4StartTime, au1AclName, pIfaceNode->u4IfaceNum);
        }
        if (pAclNode == NULL)
        {
            return i1Status;
        }
        /* set the Action Value */
        pAclNode->u4StartTime = u4StartTime;
        i1Status = SNMP_SUCCESS;
    }

    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting from Function FwlSetStartTimeForAclNode\n");
    return i1Status;

}                                /* End of the Function  --  FwlSetStartTimeForAclNode */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSetEndTimeForAclNode                          */
/*                                                                          */
/*    Description        : Sets the end time for the corresponding Acl      */
/*                         Node .                                           */
/*                                                                          */
/*    Input(s)           : pIfaceNode   -- Pointer to the Interface Node    */
/*                         i4Direction  -- Direction of ACL                 */
/*                         au1AclName   -- Accesslist Name                  */
/*                         u4EndTime    -- Schedule control end time        */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SNMP_SUCCESS if Value is set , otherwise         */
/*                         SNMP_FAILURE.                                    */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1
FwlSetEndTimeForAclNode (tIfaceInfo * pIfaceNode,
                         INT4 i4Direction,
                         UINT1 au1AclName[FWL_MAX_ACL_NAME_LEN],
                         UINT4 u4EndTime)
#else
PUBLIC INT1
FwlSetEndTimeForAclNode (pIfaceNode, i4Direction, au1AclName, u4EndTime)
     tIfaceInfo         *pIfaceNode;
     INT4                i4Direction;
     UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN];
     UINT4               u4EndTime;
#endif
{
    INT1                i1Status = FWL_ZERO;
    tAclInfo           *pAclNode = NULL;

    i1Status = SNMP_FAILURE;
    pAclNode = (tAclInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering into Function FwlSetEndTimeForAclNode\n");
    /* search the interface number .if direction is IN then search the Infilter
     * list and and set the action value for the particular filter name .if 
     * direction is OUT, do the same in outfilter list. 
     */
    if (pIfaceNode->u1RowStatus == FWL_ACTIVE)
    {
        if (i4Direction == FWL_DIRECTION_IN)
        {
            pAclNode =
                FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList, au1AclName);
            MOD_TRC_ARG3 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          " \n End time %d is set for Acl Filter %s "
                          "on interface %d in the IN direction\n ",
                          u4EndTime, au1AclName, pIfaceNode->u4IfaceNum);
        }
        else if (i4Direction == FWL_DIRECTION_OUT)
        {
            pAclNode =
                FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                         au1AclName);
            MOD_TRC_ARG3 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          " \n End time %d is set for Acl Filter %s "
                          "on interface %d in the OUT direction\n ",
                          u4EndTime, au1AclName, pIfaceNode->u4IfaceNum);
        }
        if (pAclNode == NULL)
        {
            return i1Status;
        }
        /* set the Action Value */
        pAclNode->u4EndTime = u4EndTime;
        i1Status = SNMP_SUCCESS;
    }

    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting from Function FwlSetEndTimeForAclNode\n");
    return i1Status;

}                                /* End of the Function  --  FwlSetEndTimeForAclNode */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSetRowStatusValueForAclNode                   */
/*                                                                          */
/*    Description        : Sets the Row Status Value for the corresponding  */
/*                         Acl Node.                                        */
/*                                                                          */
/*    Input(s)           : u4IfaceNum   -- Interface Number                 */
/*                         i4Direction  -- Direction                        */
/*                         au1AclName   -- Accesslist Name                  */
/*                         u1RowStatus  -- RowStatus Value                  */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            :  None                                            */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlSetRowStatusValueForAclNode (UINT4 u4IfaceNum,
                                INT4 i4Direction,
                                UINT1 au1AclName[FWL_MAX_ACL_NAME_LEN],
                                UINT1 u1RowStatus)
#else
PUBLIC VOID
FwlSetRowStatusValueForAclNode (u4IfaceNum,
                                i4Direction, au1AclName, u1RowStatus)
     UINT4               u4IfaceNum;
     INT4                i4Direction;
     UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN];
     UINT1               u1RowStatus;
#endif
{
    tAclInfo           *pAclNode = NULL;
    tIfaceInfo         *pIfaceNode = NULL;
    UINT4               u4IfaceIndex = FWL_ZERO;

    pAclNode = (tAclInfo *) NULL;
    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY,
             "\nEntering into function FwlSetRowStatusValueForAclNode\n");
    if (u4IfaceNum < FWL_MAX_NUM_OF_IF)
    {
        u4IfaceIndex = FWL_MAX_NUM_OF_IF - FWL_ONE;
    }
    else
    {
        u4IfaceIndex = FWL_ONE;
        u4IfaceNum = FWL_ONE;
    }
    while (u4IfaceIndex < FWL_MAX_NUM_OF_IF)
    {
        /* search the interface index. if direction is IN then search the 
         * InFilter list for the corresponding filter name, else the 
         * Outfilter list. 
         */
        pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
        if (pIfaceNode != NULL)
        {
            if (i4Direction == FWL_DIRECTION_IN)
            {
                pAclNode = FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                                    au1AclName);
                if (pAclNode == NULL)
                {
                    pAclNode = FwlDbaseSearchAclFilter
                        (&pIfaceNode->inIPv6FilterList, au1AclName);
                }
            }
            else
            {
                pAclNode = FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                                    au1AclName);
                if (pAclNode == NULL)
                {
                    pAclNode = FwlDbaseSearchAclFilter
                        (&pIfaceNode->outIPv6FilterList, au1AclName);
                }
            }

            if (pAclNode == NULL)
            {
                return;
            }
            if (u1RowStatus == FWL_ACTIVE)
            {
                if ((pAclNode->u1Action != FWL_INIT) &&
                    (pAclNode->u2SeqNum != FWL_INIT))
                {
                    pAclNode->u1RowStatus = u1RowStatus;
                }
                else
                {
                    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  " \n RowStatus cannot be made Active i"
                                  "for Acl Filter %s on interface %d "
                                  "since Action Value and Sequence Number "
                                  "value must be set. \n ",
                                  pAclNode->au1FilterName, u4IfaceNum);
                }
            }
            else
            {
                pAclNode->u1RowStatus = u1RowStatus;
            }
        }
        u4IfaceNum++;
        u4IfaceIndex++;
    }                            /*end of while */

    FWL_DBG (FWL_DBG_EXIT,
             "\nExiting from function FwlSetRowStatusValueForAclNode\n");

}                                /* End of the Function  --  FwlSetRowStatusValueForAclNode */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTestRowStatusValueForAclNode                  */
/*                                                                          */
/*    Description        : Tests the Row Status Value for the corresponding */
/*                         Acl Node.                                        */
/*                                                                          */
/*    Input(s)           : u4IfaceNum   -- Interface Number                 */
/*                         i4Direction  -- Direction                        */
/*                         au1AclName   -- Accesslist Name                  */
/*                         u1RowStatus  -- RowStatus Value                  */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            :  None                                            */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1
FwlTestRowStatusValueForAclNode (UINT4 u4IfaceCount,
                                 UINT4 u4IfaceIndex,
                                 UINT4 u4IfaceNum,
                                 INT4 i4Direction,
                                 UINT1 au1AclName[FWL_MAX_ACL_NAME_LEN],
                                 UINT1 u1RowStatus)
#else
PUBLIC INT1
FwlTestRowStatusValueForAclNode (u4IfaceCount,
                                 u4IfaceIndex,
                                 u4IfaceNum,
                                 i4Direction, au1AclName, u1RowStatus)
     UINT4               u4IfaceCount;
     UINT4               u4IfaceIndex;
     UINT4               u4IfaceNum;
     INT4                i4Direction;
     UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN];
     UINT1               u1RowStatus;
#endif
{
    INT4                i4Status = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;
    tAclInfo           *pAclNode = NULL;
    UINT1               u1AddrType = FWL_ZERO;

    i4Status = SNMP_FAILURE;

    while (u4IfaceIndex < FWL_MAX_NUM_OF_IF)
    {
        /* search the interface list for the corresponding interface number */
        pIfaceNode = (tIfaceInfo *) NULL;
        pAclNode = (tAclInfo *) NULL;
        pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
        if (pIfaceNode != NULL)
        {
            if (i4Direction == FWL_DIRECTION_IN)
            {
                /* search for the infilter node. if found then validate 
                 * the action value based on Row status value.If the
                 * RowStatus is FWL_CREATE_AND_WAIT or NOT READY then return
                 * SUCCESS, else FAILURE. 
                 */
                pAclNode =
                    FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                             au1AclName);
                if (pAclNode != NULL)
                {
                    /* Node found in inFilterList */
                    u1AddrType = FWL_IP_VERSION_4;
                }
                else
                {
                    /*Node not found in inFilterList.
                     * Search in inIpv6FilterList*/
                    pAclNode =
                        FwlDbaseSearchAclFilter (&pIfaceNode->inIPv6FilterList,
                                                 au1AclName);
                    if (pAclNode != NULL)
                    {
                        u1AddrType = FWL_IP_VERSION_6;
                    }
                }
                if ((FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                              au1AclName) != NULL)
                    ||
                    (FwlDbaseSearchAclFilter
                     (&pIfaceNode->outIPv6FilterList, au1AclName) != NULL))
                {
                    /* Same ACL is present in the Out Direction */
                    CLI_SET_ERR (CLI_FWL_ACL_ALREADY_PRESENT);
                    MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                             "\nAlready an ACL with this name is present in "
                             " OUT Direction \n");
                    return SNMP_FAILURE;
                }
            }                    /*end for IN direction */
            else if (i4Direction == FWL_DIRECTION_OUT)
            {
                /* search for the outfilter node. if found then validate 
                 * the action value based on Row status value.If the
                 * RowStatus is FWL_CREATE_AND_WAIT or NOT READY then return
                 * SUCCESS, else FAILURE. 
                 */
                pAclNode =
                    FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                             au1AclName);
                if (pAclNode != NULL)
                {
                    /* Node found in outFilterList */
                    u1AddrType = FWL_IP_VERSION_4;
                }
                else
                {
                    /*Node not found in outFilterList.
                     * Search in outIPv6FilterList*/
                    pAclNode =
                        FwlDbaseSearchAclFilter (&pIfaceNode->outIPv6FilterList,
                                                 au1AclName);
                    if (pAclNode != NULL)
                    {
                        u1AddrType = FWL_IP_VERSION_6;
                    }
                }
                if ((FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                              au1AclName) != NULL) ||
                    (FwlDbaseSearchAclFilter
                     (&pIfaceNode->inIPv6FilterList, au1AclName) != NULL))
                {
                    /* Same ACL is present in the In Direction */
                    CLI_SET_ERR (CLI_FWL_ACL_ALREADY_PRESENT);
                    MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                             "\nAlready an ACL with this name is present in "
                             " IN Direction \n");
                    return SNMP_FAILURE;
                }
            }                    /*end for OUT direction */
            else
            {
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\nInvalid Index - Direction value can be 1 or 2\n");
                i4Status = SNMP_FAILURE;
                break;
            }
        }
        else
        {
            if (u4IfaceCount == FWL_ONE)
            {
                u4IfaceNum++;
                u4IfaceIndex++;
                continue;
            }
        }
        switch (u1RowStatus)
        {
            case FWL_CREATE_AND_WAIT:
                if (pIfaceNode == NULL)
                {
                    /* CHANGE2: While creating the interface Index, it must 
                     * ensure that interface index exists in CFA. Else it 
                     * should not create the row. 
                     */
                    i4Status = SecUtilValidateIfIndex ((UINT2) u4IfaceNum);
                    if (i4Status == CFA_SUCCESS)
                    {
                        i4Status = SNMP_SUCCESS;
                    }
                    else
                    {
                        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                 "\nInvalid Interface Number\n");
                        i4Status = SNMP_FAILURE;
                    }
                }
                else
                {
                    if (i4Direction == FWL_DIRECTION_IN)
                    {
                        if (u1AddrType == FWL_IP_VERSION_4)
                        {
                            if (TMO_SLL_Count (&pIfaceNode->inFilterList)
                                == FWL_MAX_NUM_OF_ACL_FILTER)
                            {
                                CLI_SET_ERR (CLI_FWL_ACL_TABLE_FULL);
                                return SNMP_FAILURE;
                            }
                        }
                        else if (u1AddrType == FWL_IP_VERSION_6)
                        {
                            if (TMO_SLL_Count
                                (&pIfaceNode->inIPv6FilterList)
                                == FWL_MAX_NUM_OF_ACL_FILTER)
                            {
                                CLI_SET_ERR (CLI_FWL_ACL_TABLE_FULL);
                                return SNMP_FAILURE;
                            }
                        }
                        else
                        {
                            i4Status = SNMP_FAILURE;
                        }
                    }
                    else
                    {
                        if (u1AddrType == FWL_IP_VERSION_4)
                        {
                            if (TMO_SLL_Count (&pIfaceNode->outFilterList)
                                == FWL_MAX_NUM_OF_ACL_FILTER)
                            {
                                CLI_SET_ERR (CLI_FWL_ACL_TABLE_FULL);
                                return SNMP_FAILURE;
                            }
                        }
                        else if (u1AddrType == FWL_IP_VERSION_6)
                        {
                            if (TMO_SLL_Count
                                (&pIfaceNode->outIPv6FilterList)
                                == FWL_MAX_NUM_OF_ACL_FILTER)
                            {
                                CLI_SET_ERR (CLI_FWL_ACL_TABLE_FULL);
                                return SNMP_FAILURE;
                            }
                        }
                        else
                        {
                            i4Status = SNMP_FAILURE;
                        }
                    }

                    if (pAclNode == NULL)
                    {
                        i4Status = SNMP_SUCCESS;
                    }
                    else
                    {
                        MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                      "\nFilter / Rule %s "
                                      "is already applied on interface %d\n",
                                      au1AclName, u4IfaceNum);
                        i4Status = SNMP_FAILURE;
                    }
                }
                break;
            case FWL_ACTIVE:
                if (pAclNode != NULL)
                {
                    if ((pAclNode->u1Action != FWL_DEFAULT_ACTION) &&
                        (pAclNode->u2SeqNum != FWL_DEFAULT_SEQ_NUM))
                    {
                        i4Status = SNMP_SUCCESS;
                    }
                    else
                    {
                        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                 "\nIncomplete Acl Filter. "
                                 "RowStatus cannot be ACTIVE\n");
                        i4Status = SNMP_FAILURE;
                    }
                }
                else
                {
                    i4Status = SNMP_FAILURE;
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\nAcl Filter %s doesnt exist\n", au1AclName);
                }
                break;
            case FWL_NOT_IN_SERVICE:
                /* The row can be made FWL_NOT_IN_SERVICE if the row exists */
                if (pAclNode != NULL)
                {
                    i4Status = SNMP_SUCCESS;
                }
                else
                {
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\nAcl Filter %s doesnt exist\n", au1AclName);
                    i4Status = SNMP_FAILURE;
                }
                break;
            case FWL_DESTROY:
                if (pAclNode != NULL)
                {
                    i4Status = SNMP_SUCCESS;
                }
                else
                {
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\nAcl Filter %s doesnt exist\n", au1AclName);
                    i4Status = SNMP_FAILURE;
                }
                break;
            default:
                i4Status = SNMP_FAILURE;
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\nRowStatus can take the value 1 to 6\n");
                break;
        }                        /*end of switch */
        u4IfaceNum++;
        u4IfaceIndex++;
        if (i4Status == SNMP_FAILURE)
        {
            break;
        }
    }                            /* end of While */

    return (INT1) i4Status;

}                                /* End of the Function  --  FwlTestRowStatusValueForAclNode */

/*****************************************************************************/
/*                         End of File fwlsnif.c                             */
/*****************************************************************************/
