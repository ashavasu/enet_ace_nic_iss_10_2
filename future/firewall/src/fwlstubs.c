/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlstubs.c,v 1.3 2011/06/07 09:38:31 siva Exp $
 *
 * Description:This file contains stubs for the kernel specific  
 *             functions in firewall module. 
 *
 *******************************************************************/
#ifndef _FWLSTUBS_C_
#define _FWLSTUBS_C_

#include "fwlinc.h"

#ifdef SECURITY_KERNEL_MAKE_WANTED
VOID
FwlCommitAcl (VOID)
{
    return;
}
#endif
#ifndef SECURITY_KERNEL_MAKE_WANTED
INT4
FwlCliCommitInKernel (VOID)
{
    return FWL_SUCCESS;
}
#endif

PUBLIC INT1
FwlErrorV6MessageGenerate (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4IfIndex);
    return FWL_SUCCESS;
}
#endif
