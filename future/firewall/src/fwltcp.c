/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwltcp.c,v 1.7 2013/10/31 11:13:30 siva Exp $
 *
 * *******************************************************************/

#include "fwlinc.h"

PRIVATE UINT1       gu1FwlAllowReTransmission = 1;

/* TCP Init threshold variables */
static UINT4        gu4FwlTcpInitDropTimestamp;
                                    /*This variable is set to the time when
                                       Upper threshold is reached. When it is
                                       reset (== 0), it means that TCP Init
                                       table entries are lower than lower
                                       threshold. */
static UINT4        gu4FwlTcpInitDropTimeout = 300;
                               /* When upper threshold is reached,
                                  the packets are dropped for 
                                  this period */
static UINT1        gu4FwlTcpInitThreshIsSet = FWL_FALSE;
                                    /* Use this variable to disable the Tcp
                                       threshold check */

static UINT4        gu4FwlTcpInitThreshAction = FWL_TCP_INIT_THRESH_DEL_OLD;
                                     /* This variable tells what needs to be 
                                      * done when upper threshold is reached. */

static tNodeStat    gaFwlTcpHalfOpenPerNode[FWL_MAX_TCP_INIT_FLOW_NODES];

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTcpUpdateInitFlowSeqAckWin                    */
/*                                                                          */
/*    Description        : Processes the incoming packet and calls the      */
/*                         respective protocol handler.                     */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the packet            */
/*                         pStateIpHdr  -- Pointer to the Ip hdr            */
/*                         pStateHLInfo -- Pointer to the Transport Hdr     */
/*                         pStateNode   -- Pointer to Node with TcpInfo     */
/*                         u1Direction  -- Specifies IN or OUT              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

VOID
FwlTcpUpdateInitFlowSeqAckWin (tCRU_BUF_CHAIN_HEADER * pBuf,
                               tIpHeader * pStateIpHdr,
                               tHLInfo * pStateHLInfo,
                               tStatefulSessionNode * pStateNode,
                               UINT1 u1Direction)
{
    tTcpInfo           *pSendrInfo = NULL;
    tTcpInfo           *pRcvrInfo = NULL;
    UINT4               u4PktSeq = FWL_ZERO;
    /* Holds the start seqNo of current pkt */
    UINT4               u4PktAck = FWL_ZERO;
    /* Holds the start ackNo of current pkt */
    UINT4               u4PktWin = FWL_ZERO;
    /* Holds the Window value in current pkt */
    UINT1               u1PktTcpHdrLen = FWL_ZERO;
    /* Holds TCP Header Length in current pkt */
    UINT4               u4PktEndSeqNum = FWL_ZERO;
    /* Holds the End-SeqNo (s+n) in current pkt. */
    UINT1               u1TcpFlags = FWL_ZERO;
    UINT1              *pu1RcvrState = NULL;
    UINT1              *pu1SendrState = NULL;
    INT1                i1WinScale = FWL_ZERO;

    UNUSED_PARAM (pu1RcvrState);
    /* Get the TCP flags Byte */
    FWL_GET_1_BYTE (pBuf,
                    (UINT4) (pStateIpHdr->u1HeadLen + FWL_TCP_CODE_BIT_OFFSET),
                    u1TcpFlags);
    u1TcpFlags = u1TcpFlags & FWL_OFFSET_3f;

    if (u1Direction == FWL_DIRECTION_IN)
    {                            /* Pkt from WAN(Remote/Sender) 
                                   to LAN(Local/Receiver) */
        pSendrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.RemoteTcpInfo);
        pRcvrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.LocalTcpInfo);
        pu1SendrState = &(pStateNode->u1RemoteState);
        pu1RcvrState = &(pStateNode->u1LocalState);

    }
    else                        /* (u1Direction == FWL_DIRECTION_OUT) */
    {                            /* Pkt from LAN(Local/Sender) 
                                   to WAN(Remote/Receiver) */
        pSendrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.LocalTcpInfo);
        pRcvrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.RemoteTcpInfo);
        pu1SendrState = &(pStateNode->u1LocalState);
        pu1RcvrState = &(pStateNode->u1RemoteState);
    }

    /* Update local variables with the fields in Pkt to be validated */
    u4PktSeq = pStateHLInfo->u4SeqNum;
    u4PktAck = pStateHLInfo->u4AckNum;
    u4PktWin = pStateHLInfo->u2WindowAdvertised;
    u4PktWin = u4PktWin;
    if (*pu1SendrState == FWL_TCP_STATE_SYN_RCVD)
    {
        /* when the session is being initiallised we extract the option from 
           the incoming Syn/Ack Packet. */
        i1WinScale = FwlTcpExtractWindowScalingOption (pBuf, pStateIpHdr);
        pSendrInfo->i1WindowScaling = i1WinScale;
    }
    else
    {
        i1WinScale = pSendrInfo->i1WindowScaling;
        u4PktWin = u4PktWin << i1WinScale;
    }
    if (i1WinScale == TCP_NO_WINDOW_SCALING)
    {
        /*If window scaling is not supported on any side then scaling 
           scaling will not be enabled in the session. */
        pSendrInfo->i1WindowScaling = FWL_ZERO;
        pRcvrInfo->i1WindowScaling = FWL_ZERO;
    }

    u1PktTcpHdrLen = (UINT1) FwlGetTransportHeaderLength (pBuf,
                                                          FWL_TCP,
                                                          pStateIpHdr->
                                                          u1HeadLen);

    /* Calculate the (S+n) in the current packet received.
     * 'S' is start SeqNo in pkt, 'n' is no. of bytes of TCP data.
     * NOTE: Calculated S+N values here if data is present 1 more. */
    u4PktEndSeqNum = pStateHLInfo->u4SeqNum + (UINT4)
        (pStateIpHdr->u2TotalLen - (pStateIpHdr->u1HeadLen + u1PktTcpHdrLen)) +
        ((pStateHLInfo->u1Syn) ? FWL_ONE : FWL_ZERO)
        + ((pStateHLInfo->u1Fin) ? FWL_ONE : FWL_ZERO);

    /* When u4EndSeqNum == 0, we assume we do initialization */
    if ((pSendrInfo->u4EndSeqNum == FWL_ZERO) && (UINT4)
        (u1TcpFlags == (FWL_TCP_SYN_MASK | FWL_TCP_ACK_MASK)))
    {
        /* Must be an SYN-ACK in reply to a SYN */
        pSendrInfo->u4EndSeqNum = u4PktEndSeqNum;
        pSendrInfo->u4MaxWindow = FWL_ONE;
        pSendrInfo->u4MaxAckNum = u4PktEndSeqNum + FWL_ONE;
    }

    /* Update Local, Remote TcpInfo fields with info from Pkt 
     * if the this function has been called to also update the 
     * StateNode fields. */

    /* Update max window seen */
    if (pSendrInfo->u4MaxWindow < u4PktWin)
    {
        pSendrInfo->u4MaxWindow = u4PktWin;
    }

    /* Update EndSeqNum with (Seq + length of TCP data). */
    if (FWL_SEQ_GT (u4PktEndSeqNum, pSendrInfo->u4EndSeqNum))
    {
        pSendrInfo->u4EndSeqNum = u4PktEndSeqNum;
    }

    /* Update pRcvrInfo->u4MaxAckNum with pkt's [Ack+MAX(win_size,1)] */
    if (FWL_SEQ_GE ((u4PktAck + u4PktWin), pRcvrInfo->u4MaxAckNum))
    {
        pRcvrInfo->u4MaxAckNum = (u4PktAck + u4PktWin);
        if (u4PktWin == FWL_ZERO)
        {
            pRcvrInfo->u4MaxAckNum = pRcvrInfo->u4MaxAckNum + FWL_ONE;
        }
    }
    /*End-of-Code-for-UPDATING-TcpInfo-fields */
    UNUSED_PARAM (u4PktSeq);
}                                /*End-of-Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTcpValidateTcpSeqAck                          */
/*                                                                          */
/*    Description        : Processes the incoming packet and calls the      */
/*                         respective protocol handler.                     */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the packet            */
/*                         pStateIpHdr  -- Pointer to the Ip hdr            */
/*                         pStateHLInfo -- Pointer to the Transport Hdr     */
/*                         pStateNode   -- Pointer to Node with TcpInfo     */
/*                         u1Direction  -- Specifies IN or OUT              */
/*                         u1UpdateFlag -- FWL_VALIDATE_SEQ_ACK: Only       */
/*                                         validates SeqNo, AckNo in pkt.   */
/*                                         FWL_UPDATE_SEQ_ACK: Validates    */
/*                                         and if valid updates StateNode   */
/*                                         with the new EndSeqNo.,Ack,      */
/*                                         Window values.                   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if packet is to permited, otherwise  */
/*                         FWL_FAILURE.                                     */
/****************************************************************************/

UINT4
FwlTcpValidateTcpSeqAck (tCRU_BUF_CHAIN_HEADER * pBuf,
                         tIpHeader * pStateIpHdr,
                         tHLInfo * pStateHLInfo,
                         tStatefulSessionNode * pStateNode,
                         UINT1 u1Direction, UINT1 u1UpdateFlag)
{
    tTcpInfo           *pSendrInfo = NULL;
    tTcpInfo            TmpSendrInfo;
    tTcpInfo           *pRcvrInfo = NULL;
    UINT4               u4Status = FWL_FAILURE;
    UINT4               u4PktSeq = FWL_ZERO;
    /* Holds the start seqNo of current pkt */
    UINT4               u4PktAck = FWL_ZERO;
    /* Holds the start seqNo of current pkt */
    UINT4               u4PktWin = FWL_ZERO;
    /* Holds the Window value in current pkt */
    UINT1               u1PktTcpHdrLen = FWL_ZERO;
    /* Holds TCP Header Length in current pkt */
    UINT4               u4SeqNoLowerBound = FWL_ZERO;
    /* Holds lower-bound value of SeqNo. 
     * This is used to validate SeqNo in pkt. */
    UINT4               u4PktEndSeqNum = FWL_ZERO;
    /* Holds the End-SeqNo (s+n) in current pkt. */
    UINT4               u4End = FWL_ZERO;
    INT4                i4AckRange = FWL_ZERO;
    UINT1               u1TcpFlags = FWL_ZERO;

    /* Get the TCP flags Byte */
    FWL_GET_1_BYTE (pBuf,
                    (UINT4) (pStateIpHdr->u1HeadLen + FWL_TCP_CODE_BIT_OFFSET),
                    u1TcpFlags);
    u1TcpFlags = u1TcpFlags & FWL_OFFSET_3f;

    if (u1Direction == FWL_DIRECTION_IN)
    {                            /* Pkt from WAN(Remote/Sender) 
                                   to LAN(Local/Receiver) */
        pSendrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.RemoteTcpInfo);
        pRcvrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.LocalTcpInfo);
    }
    else                        /* (u1Direction == FWL_DIRECTION_OUT) */
    {                            /* Pkt from LAN(Local/Sender) 
                                   to WAN(Remote/Receiver) */
        pSendrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.LocalTcpInfo);
        pRcvrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.RemoteTcpInfo);
    }

    /* Update local variables with the fields in Pkt to be validated */
    u4PktSeq = pStateHLInfo->u4SeqNum;
    u4PktAck = pStateHLInfo->u4AckNum;
    u4PktWin = pStateHLInfo->u2WindowAdvertised;
    u4PktWin = u4PktWin << pSendrInfo->i1WindowScaling;
    u1PktTcpHdrLen = (UINT1)
        FwlGetTransportHeaderLength (pBuf, FWL_TCP, pStateIpHdr->u1HeadLen);

    /* Calculate the (S+n) in the current packet received.
     * 'S' is start SeqNo in pkt, 'n' is no. of bytes of TCP data.
     * NOTE: Calculated S+N values here if data is present 1 more. */
    u4PktEndSeqNum = (pStateHLInfo->u4SeqNum + (UINT4)
                      (pStateIpHdr->u2TotalLen -
                       (pStateIpHdr->u1HeadLen + u1PktTcpHdrLen)) +
                      ((pStateHLInfo->u1Syn) ? FWL_ONE : FWL_ZERO) +
                      ((pStateHLInfo->u1Fin) ? FWL_ONE : FWL_ZERO));

    u4End =
        (pStateIpHdr->u2TotalLen -
         (pStateIpHdr->u1HeadLen +
          u1PktTcpHdrLen)) ? u4PktEndSeqNum : u4PktEndSeqNum + FWL_ONE;

    /* Copy into Temp struct in order to revert values in 
     * case seq,ack checks fail. */
    FWL_MEMCPY (&TmpSendrInfo, pSendrInfo, sizeof (tTcpInfo));

    /* When u4EndSeqNum == 0, we assume we do initialization */
    if ((TmpSendrInfo.u4EndSeqNum == FWL_ZERO) &&
        (u1TcpFlags == (FWL_TCP_SYN_MASK | FWL_TCP_ACK_MASK)))
    {
        /* Must be an SYN-ACK in reply to a SYN */
        TmpSendrInfo.u4EndSeqNum = u4PktEndSeqNum;
        TmpSendrInfo.u4MaxWindow = FWL_ONE;
        TmpSendrInfo.u4MaxAckNum = u4PktEndSeqNum + FWL_ONE;
    }

    if (!(FWL_ZERO == gu1FwlAllowReTransmission))
    {
        u4SeqNoLowerBound = (TmpSendrInfo.u4EndSeqNum - pRcvrInfo->u4MaxWindow);
    }
    else
    {
        u4SeqNoLowerBound = TmpSendrInfo.u4EndSeqNum;
    }

    i4AckRange = (INT4) (pRcvrInfo->u4EndSeqNum - u4PktAck);

    /* If NP is present then below check is invalid */
    /* Validate the Sequence and Acknowedgement numbers in Pkt. */
    if ((FWL_SEQ_GE (TmpSendrInfo.u4MaxAckNum, u4End)) &&
        (FWL_SEQ_GE (u4PktSeq, u4SeqNoLowerBound)) && (!(pStateHLInfo->u1Ack) ||
                                                       ((FWL_SEQ_GE
                                                         (pRcvrInfo->
                                                          u4EndSeqNum,
                                                          u4PktAck))
                                                        &&
                                                        (FWL_SEQ_GE
                                                         (u4PktAck,
                                                          (pRcvrInfo->
                                                           u4EndSeqNum -
                                                           FWL_TCP_MAX_ACK_WINDOW))))))
    {
        /* Update Local, Remote TcpInfo fields with info from Pkt 
         * if the this function has been called to also update the 
         * StateNode fields.*/
        if (u1UpdateFlag == FWL_UPDATE_SEQ_ACK)
        {
            /* Copy back the updated Temp struct values into pSendrInfo in 
             * case seq,ack checks are successful. */
            FWL_MEMCPY (pSendrInfo, &TmpSendrInfo, sizeof (tTcpInfo));

            /* Update max window seen */
            if (pSendrInfo->u4MaxWindow < u4PktWin)
            {
                pSendrInfo->u4MaxWindow = u4PktWin;
            }

            /* Update EndSeqNum with (Seq + length of TCP data). */
            if (FWL_SEQ_GT (u4PktEndSeqNum, pSendrInfo->u4EndSeqNum))
            {
                pSendrInfo->u4EndSeqNum = u4PktEndSeqNum;
            }

            /* Update pRcvrInfo->u4MaxAckNum with pkt's [Ack+MAX(win_size,1)] */
            if (FWL_SEQ_GE ((u4PktAck + u4PktWin), pRcvrInfo->u4MaxAckNum))
            {
                pRcvrInfo->u4MaxAckNum = (u4PktAck + u4PktWin);
                if (u4PktWin == FWL_ZERO)
                {
                    pRcvrInfo->u4MaxAckNum = pRcvrInfo->u4MaxAckNum + FWL_ONE;
                }
            }

            /* This is needed to "synchronize" u4EndSeqNum when indeed *
             * fragments were passed and the total length is unknown.  */
            if (i4AckRange < FWL_ZERO)
            {
                pRcvrInfo->u4EndSeqNum = u4PktAck;
            }

        }                        /*End-of-Code-for-UPDATING-TcpInfo-fields */

        u4Status = FWL_SUCCESS;

    }                            /* End of Seq/Ack Validation */

    return u4Status;

}                                /* End of FwlTcpValidateTcpSeqAck() */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTcpInitFlowListInit                           */
/*                                                                          */
/*    Description        : Initialises the Data structure for TcpInitFlow   */
/*                         firewall module.                                 */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
FwlTcpInitFlowListInit (void)
{
    /* gFwlTcpInitFlowList is used to store un-established Tcp session
     * entries. This table is searched for a Tcp Session entry match. 
     * It uses the Protocol number, Src & Dest IP addresses, Src & Dest Ports 
     * to match an entry. */

    TMO_SLL_Init (&gFwlTcpInitFlowList);
    TMO_SLL_Init (&gFwlV6TcpInitFlowList);

}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlAddTcpInitFlowNode                            */
/*                                                                          */
/*    Description        : Adds the filter to the TcpInitFlowList           */
/*                                                                          */
/*    Input(s)           : pStateIpHdr  -- Pointer to the Ip hdr            */
/*                         pStateHLInfo -- Pointer to the Transport Hdr     */
/*                         u1State      -- State of the Filter              */
/*                         u1LogLevel   -- Log Level of the filter          */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS                                      */
/****************************************************************************/
UINT4
FwlAddTcpInitFlowNode (tCRU_BUF_CHAIN_HEADER * pBuf,
                       tIpHeader * pStateIpHdr,
                       tHLInfo * pStateHLInfo,
                       UINT1 u1State, UINT1 u1Direction, UINT1 u1LogLevel,
                       UINT1 u1AppCallStatus)
{
    UINT4               u4CurTime = FWL_ZERO;
    UINT4               u4Temp = FWL_ZERO;
    tStatefulSessionNode *pInitFlowNode = NULL;
    tTcpInfo           *pSendrInfo = NULL;
    tTcpInfo           *pRcvrInfo = NULL;
    UINT4               u4Status = FWL_FAILURE;
    tIpHeader           IpHdr;
    tHLInfo             HLInfo;
    INT1                i1WinScale = FWL_ZERO;
    tTMO_SLL           *pInitFlowLst = NULL;
    UINT4               u4NodeCount = FWL_ZERO;

    if (gu4FwlTcpInitDropTimestamp != FWL_ZERO)
    {

        return (FWL_FAILURE);
    }

    /* Copy IpHdr and HLInfo contents into local variables */
    FWL_MEMCPY (&IpHdr, pStateIpHdr, sizeof (tIpHeader));
    FWL_MEMCPY (&HLInfo, pStateHLInfo, sizeof (tHLInfo));

    i1WinScale = FwlTcpExtractWindowScalingOption (pBuf, pStateIpHdr);

    /* Based on Direction set the Src,Dest Addr and Port
     * variables to represent Local and Remote Hosts. */
    if (u1Direction == FWL_DIRECTION_IN)
    {
        if (IpHdr.SrcAddr.u4AddrType == FWL_IP_VERSION_4)
        {
            FWL_SWAP_IPV4_ADDR (IpHdr.SrcAddr.v4Addr, IpHdr.DestAddr.v4Addr);
        }
        else if (IpHdr.SrcAddr.u4AddrType == FWL_IP_VERSION_6)
        {
            FWL_SWAP_IPV6_ADDR (IpHdr.SrcAddr.v6Addr.u4_addr,
                                IpHdr.DestAddr.v6Addr.u4_addr);
        }

        u4Temp = HLInfo.u2SrcPort;
        HLInfo.u2SrcPort = HLInfo.u2DestPort;
        HLInfo.u2DestPort = (UINT2) u4Temp;
    }

    if (IpHdr.SrcAddr.u4AddrType == FWL_IP_VERSION_4)
    {
        pInitFlowLst = &gFwlTcpInitFlowList;
        u4NodeCount = TMO_SLL_Count (&gFwlTcpInitFlowList);
    }
    else if (IpHdr.SrcAddr.u4AddrType == FWL_IP_VERSION_6)
    {
        pInitFlowLst = &gFwlV6TcpInitFlowList;
        u4NodeCount = TMO_SLL_Count (&gFwlV6TcpInitFlowList);
    }

    /* Check Half open connections against the configured threshold */
    if (FwlTcpInitCheckThreshold (IpHdr.SrcAddr, &pInitFlowNode) != FWL_SUCCESS)
    {
        return FWL_FAILURE;
    }
    /* 
     * Allocate Init State Filter node for the following two cases:
     * 
     * Case 1: Concurrent Session Limiting feature is disabled 
     *
     * Case 2: Concurrent Session Limiting feature is enabled AND number
     *         of nodes in the initflow list is less than 
     *         FWL_MAX_TCP_INIT_FLOW_NODES
     */
    if ((gFwlAclInfo.u1LimitConcurrentSess == FWL_DISABLE) ||
        ((gFwlAclInfo.u1LimitConcurrentSess == FWL_ENABLE) &&
         (u4NodeCount < FWL_MAX_TCP_INIT_FLOW_NODES)))
    {
        if (FwlTcpInitFlowMemAllocate ((UINT1 **) (VOID *) &pInitFlowNode)
            == FWL_SUCCESS)
        {
            u4Status = FWL_SUCCESS;
        }
    }

    /* If FWL_MAX_TCP_INIT_NODES is reached. Or If FwlTcpInitFlowMemAllocate */
    /* fails, Then Perform Aging out of Nodes to get a free node from SLL.   */
    if (u4Status == FWL_FAILURE)
    {
        if (FwlTcpInitFlowGetAgedOutNode (&pInitFlowNode, pInitFlowLst) !=
            FWL_SUCCESS)
        {
            /* If AgingOut of TcpInitFlowList nodes fails then, 
             * return Failure. */
            return FWL_FAILURE;
        }
    }

    /* Init the Node */
    MEMSET (pInitFlowNode, FWL_ZERO, sizeof (tStatefulSessionNode));
    TMO_SLL_Init_Node ((tTMO_SLL_NODE *) pInitFlowNode);

    /* Update the Node information */
    if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
    {
        pInitFlowNode->LocalIP.v4Addr = IpHdr.SrcAddr.v4Addr;
        pInitFlowNode->LocalIP.u4AddrType = FWL_IP_VERSION_4;
        pInitFlowNode->RemoteIP.v4Addr = IpHdr.DestAddr.v4Addr;
        pInitFlowNode->RemoteIP.u4AddrType = FWL_IP_VERSION_4;
    }
    else
    {
        Ip6AddrCopy (&(pInitFlowNode->LocalIP.v6Addr), &(IpHdr.SrcAddr.v6Addr));
        pInitFlowNode->LocalIP.u4AddrType = FWL_IP_VERSION_6;
        Ip6AddrCopy (&(pInitFlowNode->RemoteIP.v6Addr),
                     &(IpHdr.DestAddr.v6Addr));
        pInitFlowNode->RemoteIP.u4AddrType = FWL_IP_VERSION_6;
    }
    FWL_LOCAL_TCP_PORT (pInitFlowNode) = HLInfo.u2SrcPort;
    FWL_REMOTE_TCP_PORT (pInitFlowNode) = HLInfo.u2DestPort;

    if (u1State == FWL_RELATED)
    {
        pInitFlowNode->u1LocalState = FWL_RELATED;
        pInitFlowNode->u1RemoteState = FWL_RELATED;
    }
    else                        /* u1State == NEW */
    {
        /* Initialise the Connection State fields */
        if (u1Direction == FWL_DIRECTION_IN)
        {
            pInitFlowNode->u1LocalState = FWL_TCP_STATE_LISTEN;
            pInitFlowNode->u1RemoteState = FWL_TCP_STATE_SYN_SENT;
        }
        else
        {
            pInitFlowNode->u1LocalState = FWL_TCP_STATE_SYN_SENT;
            pInitFlowNode->u1RemoteState = FWL_TCP_STATE_LISTEN;
        }

        /* Initialise Seq, Ack, Window values of this State Entry */
        /* Since Direction based swapping of inputs for Seq, Ack
         * and Window values has not been done, before passing to
         * routine. We do swapping here below. */
        if (u1Direction == FWL_DIRECTION_IN)
        {                        /* Pkt from WAN(Remote/Sender) 
                                   to LAN(Local/Receiver) */
            pSendrInfo = (tTcpInfo *)
                & (pInitFlowNode->ProtHdr.TcpHdr.RemoteTcpInfo);
            pRcvrInfo = (tTcpInfo *)
                & (pInitFlowNode->ProtHdr.TcpHdr.LocalTcpInfo);
        }
        else                    /* (u1Direction == FWL_DIRECTION_OUT) */
        {                        /* Pkt from LAN(Local/Sender) 
                                   to WAN(Remote/Receiver) */
            pSendrInfo = (tTcpInfo *)
                & (pInitFlowNode->ProtHdr.TcpHdr.LocalTcpInfo);
            pRcvrInfo = (tTcpInfo *)
                & (pInitFlowNode->ProtHdr.TcpHdr.RemoteTcpInfo);
        }

        /* Perform the initialisation */
        pSendrInfo->u4EndSeqNum = (HLInfo.u4SeqNum) + FWL_ONE;
        pSendrInfo->u4MaxAckNum = (HLInfo.u4SeqNum) + FWL_ONE;
        pSendrInfo->u4MaxWindow =
            (UINT4) FWL_MAX (HLInfo.u2WindowAdvertised, FWL_ONE);
        if (i1WinScale != TCP_NO_WINDOW_SCALING)
        {
            pSendrInfo->i1WindowScaling = i1WinScale;
        }
        else
        {
            pSendrInfo->i1WindowScaling = FWL_ZERO;
        }
        pRcvrInfo->u4EndSeqNum = FWL_ZERO;
        pRcvrInfo->u4MaxAckNum = FWL_ZERO;
        pRcvrInfo->u4MaxWindow = FWL_ONE;
        pRcvrInfo->i1WindowScaling = FWL_ZERO;
    }

    pInitFlowNode->u1Protocol = IpHdr.u1Proto;
    pInitFlowNode->u4Timestamp = FWL_GET_SYS_TIME (&u4CurTime);
    pInitFlowNode->u1LogLevel = u1LogLevel;
    pInitFlowNode->u1Direction = u1Direction;

    /* This is Call Status Update - Reqd in some Applications */
    pInitFlowNode->u1AppCallStatus = u1AppCallStatus;

    /* Add the Node in the gFwlTcpInitFlowList or gFwlV6TcpInitFlowList */
    if (pInitFlowLst == NULL)
    {
        FwlTcpInitFlowMemFree ((UINT1 *) pInitFlowNode);
        return FWL_FAILURE;
    }

    TMO_SLL_Add (pInitFlowLst, (tTMO_SLL_NODE *) pInitFlowNode);
    return FWL_SUCCESS;
}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDeleteTcpInitFlowOnDemand                     */
/*                                                                          */
/*    Description        : Delets a Node from TcpInitFlowList               */
/*                                                                          */
/*    Input(s)           : pStateNode  -- Pointer to the State Node Pointer */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS                                      */
/****************************************************************************/

INT4
FwlDeleteTcpInitFlowOnDemand (tStatefulSessionNode * pStateNode)
{

    tStatefulSessionNode *pInitFlowNode = NULL;
    tStatefulSessionNode *pNextNode = NULL;
    UINT4               u4IpAddr = FWL_ZERO;
    UINT4               u4IpAddr1 = FWL_ZERO;
    UINT2               u2Port = FWL_ZERO;
    UINT2               u2Port1 = FWL_ZERO;

    UNUSED_PARAM (pInitFlowNode);
    pInitFlowNode = (tStatefulSessionNode *) NULL;

    pInitFlowNode = (tStatefulSessionNode *)
        TMO_SLL_First (&gFwlTcpInitFlowList);

    if (pStateNode->u1Direction == FWL_DIRECTION_IN)
    {
        u4IpAddr = pStateNode->LocalIP.v4Addr;
        u2Port = (UINT2) FWL_LOCAL_PORT (pStateNode, pStateNode->u1Protocol);
    }
    else
    {
        u4IpAddr = pStateNode->RemoteIP.v4Addr;
        u2Port = (UINT2) FWL_REMOTE_PORT (pStateNode, pStateNode->u1Protocol);
    }

    while (pInitFlowNode != NULL)
    {
        if (pStateNode->u1Direction == FWL_DIRECTION_IN)
        {
            u4IpAddr1 = pInitFlowNode->LocalIP.v4Addr;
            u2Port1 =
                (UINT2) FWL_LOCAL_PORT (pInitFlowNode,
                                        pInitFlowNode->u1Protocol);
        }
        else
        {
            u4IpAddr1 = pInitFlowNode->RemoteIP.v4Addr;
            u2Port1 = (UINT2)
                FWL_REMOTE_PORT (pInitFlowNode, pInitFlowNode->u1Protocol);
        }

        if ((u4IpAddr == u4IpAddr1) && (pStateNode->u1Protocol ==
                                        pInitFlowNode->u1Protocol)
            && (pStateNode->u1Direction == pInitFlowNode->u1Direction)
            && (u2Port == u2Port1))
        {

            pNextNode =
                (tStatefulSessionNode *) TMO_SLL_Next (&gFwlTcpInitFlowList,
                                                       (tTMO_SLL_NODE *)
                                                       pInitFlowNode);
            /* Call Tmo Delete Node */
            TMO_SLL_Delete (&gFwlTcpInitFlowList,
                            (tTMO_SLL_NODE *) pInitFlowNode);

            FwlTcpInitDelCountPerNode (pInitFlowNode->LocalIP);
            FwlTcpInitFlowMemFree ((UINT1 *) pInitFlowNode);
            break;
        }
        else
        {
            pInitFlowNode =
                (tStatefulSessionNode *) TMO_SLL_Next (&gFwlTcpInitFlowList,
                                                       (tTMO_SLL_NODE *)
                                                       pInitFlowNode);
        }
    }                            /*End of While-Loop */
    UNUSED_PARAM (pNextNode);
    return FWL_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDeleteTcpInitFlowNode                         */
/*                                                                          */
/*    Description        : Deletes the filter from the Stateful Filter List */
/*                                                                          */
/*    Input(s)           : u4Flush -- Flush type value in the timer         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS | FWL_FAILURE                        */
/****************************************************************************/
UINT4
FwlDeleteTcpInitFlowNode (UINT4 u4Flush)
{
    tStatefulSessionNode *pInitFlowNode = NULL;
    tStatefulSessionNode *pNextNode = NULL;
    tTMO_SLL           *apInitFlowLst[FWL_THREE] =
        { &gFwlTcpInitFlowList, &gFwlV6TcpInitFlowList, NULL };
    UINT4               u4CurTime = FWL_ZERO;
    UINT4               u4Count = FWL_ZERO;
    UINT4               u4Timeout = FWL_ZERO;

    for (u4Count = FWL_ZERO; apInitFlowLst[u4Count] != NULL; u4Count++)
    {
        pInitFlowNode = (tStatefulSessionNode *) NULL;

        FWL_GET_SYS_TIME (&u4CurTime);
        pInitFlowNode = (tStatefulSessionNode *)
            TMO_SLL_First (apInitFlowLst[u4Count]);

        while (pInitFlowNode != NULL)
        {

            if ((pInitFlowNode->u1AppCallStatus == FWL_APP_HOLD) ||
                (pInitFlowNode->u1AppCallStatus == FWL_APP_UNHOLD))
            {
                pNextNode =
                    (tStatefulSessionNode *)
                    TMO_SLL_Next (apInitFlowLst[u4Count],
                                  (tTMO_SLL_NODE *) pInitFlowNode);
                pInitFlowNode = pNextNode;

                continue;

            }
            else
            {
                u4Timeout = FWL_TCP_INIT_FLOW_TIMEOUT * FWL_SYS_TIME_FACTOR;
            }

            if ((u4Flush == FWL_FLUSH_STATEFUL_ENTRIES) ||
                ((u4CurTime - pInitFlowNode->u4Timestamp) > u4Timeout))
            {
                pNextNode =
                    (tStatefulSessionNode *)
                    TMO_SLL_Next (apInitFlowLst[u4Count],
                                  (tTMO_SLL_NODE *) pInitFlowNode);
                /* Call Tmo Delete Node */
                TMO_SLL_Delete (apInitFlowLst[u4Count],
                                (tTMO_SLL_NODE *) pInitFlowNode);

                FwlTcpInitDelCountPerNode (pInitFlowNode->LocalIP);
                FwlTcpInitFlowMemFree ((UINT1 *) pInitFlowNode);
                pInitFlowNode = pNextNode;
            }
            else
            {
                pInitFlowNode =
                    (tStatefulSessionNode *)
                    TMO_SLL_Next (apInitFlowLst[u4Count],
                                  (tTMO_SLL_NODE *) pInitFlowNode);
            }
        }                        /*End of While-Loop */
    }

    return FWL_SUCCESS;
}                                /*End-of-Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlMatchTcpInitFlowNode                          */
/*                                                                          */
/*    Description        : Matches the filter for the Pkt against Tcp Init  */
/*                         Flow SLL                                         */
/*                                                                          */
/*    Input(s)           : pStateIPHdr  -- Pointer to the IP Header         */
/*                         pStateHLInfo -- Pointer to the Transport Header  */
/*                         u1PktDirection -- Direction IN or OUT            */
/*                                                                          */
/*    Output(s)          : ppInitFlowNode -- stores Pointer to matched node */
/*                         pu1LogTrigger -- stores the Log level value      */
/*                                                                          */
/*    Returns            : FWL_SUCCESS | FWL_FAILURE                        */
/****************************************************************************/
UINT4
FwlMatchTcpInitFlowNode (tIpHeader * pStateIpHdr,
                         tHLInfo * pStateHLInfo,
                         UINT1 u1Direction,
                         UINT1 *pu1LogTrigger,
                         tStatefulSessionNode ** ppInitFlowNode)
{
    /* Scan the SLL
     *  1. If the u1LocalState == RELATED then,
     *            1.a Check 3-tuple.(exclude protocol)
     *            1.b If matches then return node.
     *  2.  Else Check the 4-Tuple
     *            2.a If matches then return node.
     *            2.b Else move to Next Node. And repeat steps 1, 2.
     */
    tStatefulSessionNode *pInitFlowInfo = NULL;
    tFwlIpAddr          aIpAddr[FWL_TWO];
    UINT4               u4Status = FWL_NOT_MATCH;
    UINT2               au2Port[FWL_TWO] = { FWL_ZERO };
    UINT1               u1Local = FWL_ZERO;
    tTMO_SLL           *pInitFlowLst = NULL;

    *pu1LogTrigger = FWL_LOG_NONE;

    u1Local = (UINT1) (u1Direction == FWL_DIRECTION_IN);

    if (pStateIpHdr->u1Proto != FWL_TCP)
    {
        return FWL_NOT_MATCH;
    }

    if (!(NULL == pStateHLInfo))
    {
        au2Port[FWL_INDEX_0] = pStateHLInfo->u2SrcPort;
        au2Port[FWL_INDEX_1] = pStateHLInfo->u2DestPort;
    }
    else
    {
        return u4Status;
    }

    FWL_IP_ADDR_COPY (&aIpAddr[FWL_INDEX_0], &pStateIpHdr->SrcAddr);
    FWL_IP_ADDR_COPY (&aIpAddr[FWL_INDEX_1], &pStateIpHdr->DestAddr);

    if (pStateIpHdr->SrcAddr.u4AddrType == FWL_IP_VERSION_4)
    {
        pInitFlowLst = &gFwlTcpInitFlowList;
    }
    else
    {
        pInitFlowLst = &gFwlV6TcpInitFlowList;
    }

    TMO_SLL_Scan (pInitFlowLst, pInitFlowInfo, tStatefulSessionNode *)
    {
        if (pInitFlowInfo != NULL)
        {
            if (aIpAddr[u1Local].u4AddrType == FWL_IP_VERSION_4)
            {
                if (!
                    ((pInitFlowInfo->LocalIP.v4Addr == aIpAddr[u1Local].v4Addr)
                     && (pInitFlowInfo->RemoteIP.v4Addr ==
                         aIpAddr[!u1Local].v4Addr)
                     && (pInitFlowInfo->u1Protocol == pStateIpHdr->u1Proto)))
                {
                    continue;
                }
            }
            else if (aIpAddr[u1Local].u4AddrType == FWL_IP_VERSION_6)
            {
                if (!
                    ((Ip6AddrCompare
                      (pInitFlowInfo->LocalIP.v6Addr,
                       aIpAddr[u1Local].v6Addr) == IP6_ZERO)
                     &&
                     (Ip6AddrCompare
                      (pInitFlowInfo->RemoteIP.v6Addr,
                       aIpAddr[!u1Local].v6Addr) == IP6_ZERO)
                     && (pInitFlowInfo->u1Protocol == pStateIpHdr->u1Proto)))
                {
                    continue;
                }
            }

            if (pInitFlowInfo->u1LocalState != FWL_RELATED)
            {
                if ((FWL_LOCAL_PORT (pInitFlowInfo, FWL_TCP) ==
                     au2Port[u1Local])
                    && (FWL_REMOTE_PORT (pInitFlowInfo, FWL_TCP) ==
                        au2Port[!u1Local]))
                {
                    *pu1LogTrigger = pInitFlowInfo->u1LogLevel;
                    *ppInitFlowNode = pInitFlowInfo;
                    return FWL_MATCH;
                }
            }
            else                /* Node State == FWL_RELATED *
                                 * Compare only 3-Tuple */
            {
                UINT2               u2RemotePort = FWL_ZERO;
                UINT2               u2LocalPort = FWL_ZERO;

                u2RemotePort = (UINT2) ((u1Direction == FWL_DIRECTION_IN) ?
                                        FWL_LOCAL_TCP_PORT (pInitFlowInfo) :
                                        FWL_REMOTE_TCP_PORT (pInitFlowInfo));

                u2LocalPort = (UINT2) ((u1Direction == FWL_DIRECTION_IN) ?
                                       FWL_REMOTE_TCP_PORT (pInitFlowInfo) :
                                       FWL_LOCAL_TCP_PORT (pInitFlowInfo));

                if ((u2RemotePort == pStateHLInfo->u2DestPort) &&
                    ((u2RemotePort != FWL_ZERO)
                     || (u2LocalPort == pStateHLInfo->u2SrcPort)))
                {
                    *pu1LogTrigger = pInitFlowInfo->u1LogLevel;
                    *ppInitFlowNode = pInitFlowInfo;
                    return FWL_MATCH;
                }
            }
        }                        /*End-of-Main-IF-Block */
    }                            /*End-of-TMO_SLL_Scan */

    return u4Status;

}                                /*End-of-Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTcpInitStateHandler                           */
/*                                                                          */
/*    Description        : Processes incoming packet against TCP session    */
/*                         state of session node in TcpInitFlow SLL         */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the packet            */
/*                         pStateNode   -- Pointer to Node to be intialised */
/*                         u1Direction  -- Specifies IN or OUT              */
/*                         pStateIpHdr  -- Pointer to the Ip hdr            */
/*                         pStateHLInfo -- Pointer to the Transport Hdr     */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if packet is to permited, otherwise  */
/*                         FWL_FAILURE.                                     */
/****************************************************************************/
UINT4
FwlTcpInitStateHandler (tCRU_BUF_CHAIN_HEADER * pBuf,
                        tStatefulSessionNode * pStateNode,
                        UINT1 u1Direction,
                        tIpHeader * pStateIpHdr, tHLInfo * pStateHLInfo)
{
    tTcpInfo           *pSendrInfo = NULL;
    tTcpInfo           *pRcvrInfo = NULL;
    UINT1              *pu1RcvrState = NULL;
    UINT1              *pu1SendrState = NULL;
    UINT1               u1TcpFlags = FWL_ZERO;
    UINT4               u4DummyIfNum = FWL_ZERO;
    UINT1               u1PktTcpHdrLen = FWL_ZERO;

    /* Get the TCP flags Byte */
    FWL_GET_1_BYTE (pBuf,
                    (UINT4) (pStateIpHdr->u1HeadLen + FWL_TCP_CODE_BIT_OFFSET),
                    u1TcpFlags);
    u1TcpFlags = u1TcpFlags & FWL_OFFSET_3f;

    if (u1Direction == FWL_DIRECTION_IN)
    {                            /* Pkt from WAN(Remote/Sender) 
                                   to LAN(Local/Receiver) */
        pSendrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.RemoteTcpInfo);
        pRcvrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.LocalTcpInfo);
        pu1SendrState = &(pStateNode->u1RemoteState);
        pu1RcvrState = &(pStateNode->u1LocalState);
    }
    else                        /* (u1Direction == FWL_DIRECTION_OUT) */
    {                            /* Pkt from LAN(Local/Sender) 
                                   to WAN(Remote/Receiver) */
        pSendrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.LocalTcpInfo);
        pRcvrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.RemoteTcpInfo);
        pu1SendrState = &(pStateNode->u1LocalState);
        pu1RcvrState = &(pStateNode->u1RemoteState);
    }

    /* Extract the TCP-Header Length in the packet. */
    u1PktTcpHdrLen = (UINT1) FwlGetTransportHeaderLength (pBuf,
                                                          FWL_TCP,
                                                          pStateIpHdr->
                                                          u1HeadLen);

    /* If a pkt with data is seen in Pre-Established then Drop it. */
    if ((pStateIpHdr->u2TotalLen -
         (pStateIpHdr->u1HeadLen + u1PktTcpHdrLen) != FWL_ZERO) &&
        ((u1TcpFlags & FWL_TCP_SYN_MASK) != FWL_ZERO))
    {
        /* Return Failure if the packets in Pre-Established state contain data. */
        FwlLogMessage (FWL_TCP_PKT_INVALID_4_STATE, u4DummyIfNum, pBuf,
                       pStateNode->u1LogLevel, FWLLOG_CRITICAL_LEVEL,
                       (UINT1 *) FWL_MSG_ATTACK);
        return FWL_FAILURE;
    }

    switch (*pu1RcvrState)
    {
        case FWL_TCP_STATE_LISTEN:
        {
            /* switch(Pkt-Type) */
            /* case  Syn: if (ValidSeqAck) Then Allow pkt */
            if ((u1TcpFlags == FWL_TCP_SYN_MASK) &&
                (pStateHLInfo->u4SeqNum == pSendrInfo->u4EndSeqNum - FWL_ONE))
            {                    /* This pkt is a retransmitted Tcp SYN only Pkt */
                /* No updations are to be done in matching entry */
                return FWL_SUCCESS;
            }
            /* Any other pkt is to be dropped */
            /* case (SynAck/Ack/Rst/Fin/DataOnlyPkt):   
               default:
               Drop the pkt.
             */

            FwlLogMessage (FWL_TCP_PKT_INVALID_4_STATE, u4DummyIfNum, pBuf,
                           pStateNode->u1LogLevel, FWLLOG_CRITICAL_LEVEL,
                           (UINT1 *) FWL_MSG_ATTACK);
            return FWL_FAILURE;
        }

        case FWL_TCP_STATE_SYN_SENT:
        {
            /* switch(Pkt-Type) */
            /* case  Syn/Ack: if (ValidSeqAck) Then Allow pkt */
            if ((u1TcpFlags == (FWL_TCP_SYN_MASK | FWL_TCP_ACK_MASK)) &&
                (pStateHLInfo->u4AckNum == pRcvrInfo->u4EndSeqNum))
            {

                *pu1SendrState = FWL_TCP_STATE_SYN_RCVD;
                return FWL_SUCCESS;
            }
            /* case Rst/Ack: if Valid SeqAck 
             * (SeqNum==0 && Ack==Rcvr.EndSeqNum) Valid) Then Allow pkt */
            if ((u1TcpFlags == (FWL_TCP_RST_MASK | FWL_TCP_ACK_MASK)) &&
                ((pStateHLInfo->u4SeqNum == FWL_ZERO) &&
                 (pStateHLInfo->u4AckNum == pRcvrInfo->u4EndSeqNum)))
            {
                *pu1SendrState = FWL_TCP_STATE_CLOSED;
                *pu1RcvrState = FWL_TCP_STATE_CLOSED;
                pStateNode->u4Timestamp = FWL_ZERO;
                return FWL_SUCCESS;
            }                    /*End-of-Rst-Check */

            /* Any other pkt is to be dropped */
            /* case (Syn/Rst/Ack/Fin/DataOnlyPkt):
               default:
               Drop the pkt.
             */

            FwlLogMessage (FWL_TCP_PKT_INVALID_4_STATE, u4DummyIfNum, pBuf,
                           pStateNode->u1LogLevel, FWLLOG_CRITICAL_LEVEL,
                           (UINT1 *) FWL_MSG_ATTACK);
            return FWL_FAILURE;
        }
        case FWL_TCP_STATE_SYN_RCVD:
        {
            /* switch(Pkt-Type) */

            /* case Ack: if (ValidSeqAck) Then allow pkt if:
             * ACK bit set 
             * RST bit noy set */
            if (((u1TcpFlags & FWL_TCP_ACK_MASK) != FWL_ZERO) &&
                ((u1TcpFlags & FWL_TCP_RST_MASK) == FWL_ZERO) &&
                ((pStateHLInfo->u4SeqNum == pSendrInfo->u4EndSeqNum) &&
                 (pStateHLInfo->u4AckNum == (pRcvrInfo->u4EndSeqNum))))
            {
                /* Update the matching entry. If not already set 
                 * set. Then set SendrState = ESTABLISHED. */
                *pu1SendrState = FWL_TCP_STATE_EST;
                return FWL_SUCCESS;
            }

            /* case Rst or Rst/Ack: if (ValidSeqAck) Then Allow pkt */
            if (((u1TcpFlags == FWL_TCP_RST_MASK) &&
                 (pStateHLInfo->u4SeqNum == pSendrInfo->u4EndSeqNum)) ||
                ((u1TcpFlags == (FWL_TCP_RST_MASK | FWL_TCP_ACK_MASK)) &&
                 (pStateHLInfo->u4SeqNum == pSendrInfo->u4EndSeqNum) &&
                 (pStateHLInfo->u4AckNum == (pRcvrInfo->u4EndSeqNum
                                             + FWL_ONE))))
            {
                *pu1SendrState = FWL_TCP_STATE_CLOSED;
                *pu1RcvrState = FWL_TCP_STATE_CLOSED;
                pStateNode->u4Timestamp = FWL_ZERO;
                return FWL_SUCCESS;
            }                    /*End-of-Rst-Check */

            /* Any other pkt is to be dropped */
            /* case (SynAck/Ack/Fin/DataOnlyPkt):
               default:
               Drop the pkt.
             */

            FwlLogMessage (FWL_TCP_PKT_INVALID_4_STATE, u4DummyIfNum, pBuf,
                           pStateNode->u1LogLevel, FWLLOG_CRITICAL_LEVEL,
                           (UINT1 *) FWL_MSG_ATTACK);
            return FWL_FAILURE;

        }
        default:

            FwlLogMessage (FWL_TCP_PKT_INVALID_4_STATE, u4DummyIfNum, pBuf,
                           pStateNode->u1LogLevel, FWLLOG_CRITICAL_LEVEL,
                           (UINT1 *) FWL_MSG_ATTACK);
            return FWL_FAILURE;
    }                            /*End-of-Main-Switch */
}                                /*End-of-Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTcpInitFlowGetAgedOutNode                     */
/*                                                                          */
/*    Description        : Searches for a timedout Node in TcpInitFlowList  */
/*                                                                          */
/*    Input(s)           : ppInitFlowNode - pointer to return Aged Node ptr */
/*                                                                          */
/*    Output(s)          : ppInitFlowNode - Ptr to the Aged out Node        */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if node found, else FWL_FAILURE      */
/****************************************************************************/
UINT4
FwlTcpInitFlowGetAgedOutNode (tStatefulSessionNode ** ppInitFlowNode,
                              tTMO_SLL * pInitFlowLst)
{
    tStatefulSessionNode *pTempNode = NULL;
    UINT4               u4CurTime = FWL_ZERO;
    UINT4               u4Status = FWL_FAILURE;

    /* Perform Initialization */
    *ppInitFlowNode = NULL;
    FWL_GET_SYS_TIME (&u4CurTime);

    if (pInitFlowLst == NULL)
    {
        return u4Status;
    }

    TMO_SLL_Scan (pInitFlowLst, pTempNode, tStatefulSessionNode *)
    {
        if (pTempNode != NULL)
        {
            if ((u4CurTime - pTempNode->u4Timestamp) >=
                (UINT4) (FWL_TCP_INIT_FLOW_TIMEOUT * FWL_SYS_TIME_FACTOR))
            {
                *ppInitFlowNode = pTempNode;
                u4Status = FWL_SUCCESS;
                break;
            }
        }
    }                            /*End-of-TMO_SLL_Scan */

    /* Delete the node from the list */
    if (*ppInitFlowNode != NULL)
    {
        TMO_SLL_Delete (pInitFlowLst, (tTMO_SLL_NODE *) (*ppInitFlowNode));
    }

    return u4Status;
}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTcpInitCheckThreshPerNode                     */
/*                                                                          */
/*    Description        : Checks number of half open conenctios allowed per*/
/*                         node                                             */
/*                                                                          */
/*    Input(s)           : LocalAddr  -- Address on the local LAN         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS or FWL_FAILURE                       */
/****************************************************************************/

UINT4
FwlTcpInitCheckThreshPerNode (tFwlIpAddr LocalAddr)
{
    UINT4               u4FreeSlot = FWL_MAX_TCP_INIT_FLOW_NODES;
    UINT4               u4Count = FWL_ZERO;

    for (u4Count = FWL_ZERO; u4Count < FWL_MAX_TCP_INIT_FLOW_NODES; u4Count++)
    {
        /* u4AddrType would be non zero, if theres an 
         * IPv4 or IPv6 address entry */
        if (gaFwlTcpHalfOpenPerNode[u4Count].IpAddr.u4AddrType == FWL_ZERO)
        {
            if (u4Count < u4FreeSlot)
            {
                u4FreeSlot = u4Count;
            }
            continue;
        }

        if (((LocalAddr.u4AddrType == FWL_IP_VERSION_4) &&
             (gaFwlTcpHalfOpenPerNode[u4Count].IpAddr.v4Addr ==
              LocalAddr.v4Addr))
            || ((LocalAddr.u4AddrType == FWL_IP_VERSION_6) &&
                (Ip6AddrCompare
                 (gaFwlTcpHalfOpenPerNode[u4Count].IpAddr.v6Addr,
                  LocalAddr.v6Addr) == IP6_ZERO)))
        {
            if (gaFwlTcpHalfOpenPerNode[u4Count].u4Count >=
                gFwlAclInfo.FwlDOSThresholds.u4FwlTcpInitMaxPerNode)
            {
                /* Trace message */
                return (FWL_FAILURE);
            }
            gaFwlTcpHalfOpenPerNode[u4Count].u4Count++;
            break;
        }
    }

    if (u4Count == FWL_MAX_TCP_INIT_FLOW_NODES)
    {
        if (u4FreeSlot < FWL_MAX_TCP_INIT_FLOW_NODES)
        {
            if (LocalAddr.u4AddrType == FWL_IP_VERSION_4)
            {
                gaFwlTcpHalfOpenPerNode[u4FreeSlot].IpAddr.v4Addr =
                    LocalAddr.v4Addr;
                gaFwlTcpHalfOpenPerNode[u4FreeSlot].IpAddr.u4AddrType =
                    FWL_IP_VERSION_4;
            }
            else if (LocalAddr.u4AddrType == FWL_IP_VERSION_6)
            {
                Ip6AddrCopy (&gaFwlTcpHalfOpenPerNode[u4FreeSlot].IpAddr.v6Addr,
                             &LocalAddr.v6Addr);
                gaFwlTcpHalfOpenPerNode[u4FreeSlot].IpAddr.u4AddrType =
                    FWL_IP_VERSION_6;
            }
            gaFwlTcpHalfOpenPerNode[u4FreeSlot].u4Count = FWL_ONE;
        }
    }

    return (FWL_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTcpInitDelCountPerNode                        */
/*                                                                          */
/*    Description        : Delete the half open conenctions count per node  */
/*                                                                          */
/*    Input(s)           : u4LocalAddr  -- Address on the local LAN         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS                                      */
/****************************************************************************/
UINT4
FwlTcpInitDelCountPerNode (tFwlIpAddr LocalAddr)
{
    UINT4               u4Count = FWL_ZERO;

    for (u4Count = FWL_ZERO; u4Count < FWL_MAX_TCP_INIT_FLOW_NODES; u4Count++)
    {
        if (((LocalAddr.u4AddrType == FWL_IP_VERSION_4) &&
             (gaFwlTcpHalfOpenPerNode[u4Count].IpAddr.v4Addr ==
              LocalAddr.v4Addr))
            || ((LocalAddr.u4AddrType == FWL_IP_VERSION_6) &&
                (Ip6AddrCompare
                 (gaFwlTcpHalfOpenPerNode[u4Count].IpAddr.v6Addr,
                  LocalAddr.v6Addr) == IP6_ZERO)))
        {
            gaFwlTcpHalfOpenPerNode[u4Count].u4Count--;
            if (gaFwlTcpHalfOpenPerNode[u4Count].u4Count == FWL_ZERO)
            {
                MEMSET (&(gaFwlTcpHalfOpenPerNode[u4Count].IpAddr), FWL_ZERO,
                        sizeof (tFwlIpAddr));
            }
            break;
        }
    }

    return (FWL_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTcpInitCheckThreshold                         */
/*                                                                          */
/*    Description        : This function checks whether a new half open     */
/*                         TCP connection should be allowed.                */
/*                                                                          */
/*    Input(s)           : u4LocalAddr  -- Address on the local LAN         */
/*                         ppInitFlowNode - pointer to return Aged Node ptr */
/*                                                                          */
/*    Output(s)          : ppInitFlowNode -- returns the free node          */
/*                                                                          */
/*    Returns            : FWL_SUCCESS OR FWL_FAILURE                       */
/****************************************************************************/
UINT4
FwlTcpInitCheckThreshold (tFwlIpAddr LocalAddr,
                          tStatefulSessionNode ** ppInitFlowNode)
{
    UINT4               u4CurTime = FWL_ZERO;
    tTMO_SLL           *pInitFlowLst = NULL;

    *ppInitFlowNode = NULL;

    if (gu4FwlTcpInitThreshIsSet == FWL_FALSE)
    {
        /* Threshold check not required, simply return success. */
        return (FWL_SUCCESS);
    }

    if (gu4FwlTcpInitDropTimestamp != FWL_ZERO)
    {
        /* Increment the drop stats? */

        return (FWL_FAILURE);
    }

    if (LocalAddr.u4AddrType == FWL_IP_VERSION_4)
    {
        pInitFlowLst = &gFwlTcpInitFlowList;
    }
    else
    {
        pInitFlowLst = &gFwlV6TcpInitFlowList;
    }

    /* Check if we have crossed upper threshold */
    if (TMO_SLL_Count (pInitFlowLst) <
        gFwlAclInfo.FwlDOSThresholds.u4FwlTcpInitUpperThresh)
    {
        if (FwlTcpInitFlowMemAllocate ((UINT1 **) ppInitFlowNode) ==
            FWL_FAILURE)
        {
            return (FWL_FAILURE);
        }

        if (FwlTcpInitCheckThreshPerNode (LocalAddr) == FWL_FAILURE)
        {
            FwlTcpInitFlowMemFree ((UINT1 *) *ppInitFlowNode);
            /* Trace mesages */
            return (FWL_FAILURE);
        }
        return (FWL_SUCCESS);
    }

    /*
     * On crossing the upper threshold, one of the following will be done
     *  (1) Delete old entries
     *  (2) Do not allow new connections for the specified interval
     */

    if (gu4FwlTcpInitThreshAction == FWL_TCP_INIT_THRESH_DEL_OLD)
    {
        FwlTcpInitDelOldEntries ();

        if (FwlTcpInitFlowMemAllocate ((UINT1 **) ppInitFlowNode)
            == FWL_FAILURE)
        {
            return (FWL_FAILURE);
        }

        if (FwlTcpInitCheckThreshPerNode (LocalAddr) == FWL_FAILURE)
        {
            FwlTcpInitFlowMemFree ((UINT1 *) *ppInitFlowNode);
            /* Trace mesages */
            return (FWL_FAILURE);
        }

        return (FWL_SUCCESS);
    }
    else if (gu4FwlTcpInitThreshAction == FWL_TCP_INIT_THRESH_DROP)
    {
        gu4FwlTcpInitDropTimestamp = FWL_GET_SYS_TIME (&u4CurTime);

        return (FWL_FAILURE);
    }
    /* unreachable code */
    return (FWL_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTcpInitDelOldEntries                          */
/*                                                                          */
/*    Description        : This function deletes atleast half the  entries  */
/*                         of threshold window. It tries to age out the rest*/
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : FWL_SUCCESS OR FWL_FAILURE                       */
/****************************************************************************/
void
FwlTcpInitDelOldEntries ()
{
    tStatefulSessionNode *pInitFlowNode = NULL;
    tStatefulSessionNode *pNextNode = NULL;
    UINT4               u4DelThreshAvg = FWL_ZERO;
    UINT4               u4Count = FWL_ZERO;
    tTMO_SLL           *apInitFlowLst[FWL_THREE] =
        { &gFwlTcpInitFlowList, &gFwlV6TcpInitFlowList, NULL };

    for (u4Count = FWL_ZERO; apInitFlowLst[u4Count]; u4Count++)
    {
        u4DelThreshAvg = (gFwlAclInfo.FwlDOSThresholds.u4FwlTcpInitUpperThresh -
                          gFwlAclInfo.FwlDOSThresholds.
                          u4FwlTcpInitLowerThresh) / FWL_TWO;

        /* 
         * Assumption: First node in gFwlTcpInitFlowList is oldest.
         */

        /*
         * Delete half the nodes of threshold window (forced)
         */
        pInitFlowNode = (tStatefulSessionNode *)
            TMO_SLL_First (apInitFlowLst[u4Count]);

        while (pInitFlowNode != NULL)
        {
            pNextNode =
                (tStatefulSessionNode *) TMO_SLL_Next (apInitFlowLst[u4Count],
                                                       (tTMO_SLL_NODE *)
                                                       pInitFlowNode);
            TMO_SLL_Delete (apInitFlowLst[u4Count],
                            (tTMO_SLL_NODE *) pInitFlowNode);

            FwlTcpInitDelCountPerNode (pInitFlowNode->LocalIP);
            FwlTcpInitFlowMemFree ((UINT1 *) pInitFlowNode);
            pInitFlowNode = pNextNode;

            if (u4Count++ > u4DelThreshAvg)
            {
                break;
            }
        }
    }

    /*
     * Try to age out other half of threshold window
     */
    FwlDeleteTcpInitFlowNode (FWL_FLUSH_STATEFUL_ENTRIES);

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTcpInitThreshDropTmrCb                        */
/*                                                                          */
/*    Description        : This function resets the global variable         */
/*                         gu4FwlTcpInitDropTimestamp after the timeout     */
/*                         interval.                                        */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
void
FwlTcpInitThreshDropTmrCb ()
{
    UINT4               u4CurTime = FWL_ZERO;

    if (gu4FwlTcpInitDropTimestamp == FWL_ZERO)
    {
        return;
    }

    if ((FWL_GET_SYS_TIME (&u4CurTime) - gu4FwlTcpInitDropTimestamp) >
        (gu4FwlTcpInitDropTimeout * FWL_SYS_TIME_FACTOR))
    {
        gu4FwlTcpInitDropTimestamp = FWL_ZERO;
    }

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTcpExtractWindowScalingOption                 */
/*                                                                          */
/*    Description        : This function extracts the Window Scaling        */
/*                         option from the TCP Packet                       */
/*                         interval.Returns the value of scaling or         */
/*                         -1 if the option is not present                  */
/*                                                                          */
/*    Input(s)           : pBuf - Packet Buffer                             */
/*                         pStateIpHdr - IP Header                          */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : Window Scaling value                             */
/****************************************************************************/
INT1
FwlTcpExtractWindowScalingOption (tCRU_BUF_CHAIN_HEADER * pBuf,
                                  tIpHeader * pStateIpHdr)
{
    UINT4               u4Offset = FWL_ZERO;
    UINT1               u1PktTcpHdrLen = FWL_ZERO;
    UINT1               u1PktOptLen = FWL_ZERO;
    UINT1               u1OptOffset = FWL_ZERO;
    INT1                i1WinScale = TCP_NO_WINDOW_SCALING;
    UINT1               u1Option = FWL_ZERO;
    UINT1               u1OptLen = FWL_ZERO;
    UINT1               u1Flag = FWL_FALSE;

    u1PktTcpHdrLen = (UINT1) FwlGetTransportHeaderLength (pBuf,
                                                          FWL_TCP,
                                                          pStateIpHdr->
                                                          u1HeadLen);
    u1PktOptLen = (UINT1) (u1PktTcpHdrLen - FWL_MIN_TCP_HEADER_SIZE);
    u4Offset = (UINT4) (pStateIpHdr->u1HeadLen + FWL_MIN_TCP_HEADER_SIZE);
    if (u1PktOptLen == FWL_ZERO)
    {
        /*As options are no present in the packet Scaling is not avialble */
        return i1WinScale;
    }

    for (u1OptOffset = FWL_ZERO; u1OptOffset < u1PktOptLen;)
    {
        if (u1Flag != FWL_FALSE)
        {
            break;
        }

        FWL_GET_1_BYTE (pBuf, u4Offset + u1OptOffset, u1Option);

        switch (u1Option)
        {
            case TCP_EOOL:
                u1OptLen = TCP_EOOL_LEN;
                break;
            case TCP_NOOP:
                u1OptLen = TCP_NOOP_LEN;
                break;
            case TCP_MSS:
                u1OptLen = TCP_MSS_LEN;
                break;
            case TCP_TIMESTAMP:
                u1OptLen = TCP_TIMESTAMP_LEN;
                break;
            case TCP_SCALING:
                u1OptLen = TCP_SCALING_LEN;
                u1Flag = FWL_TRUE;
                FWL_GET_1_BYTE (pBuf, u4Offset + u1OptOffset + u1OptLen
                                - FWL_ONE, i1WinScale);
                break;
            default:
                u1OptLen = TCP_MIN_OPT_LENGTH;
                break;
        }
        u1OptOffset = (UINT1) (u1OptOffset + u1OptLen);
    }

    if (i1WinScale > TCP_MAX_WINDOW_SCALING)
    {
        /*The scaling value has exceeded the maximum supported value */
        return TCP_NO_WINDOW_SCALING;
    }
    else
    {
        return i1WinScale;
    }
}

/****************************************************************************/
/*                           End of fwltcp.c                                */
/****************************************************************************/
