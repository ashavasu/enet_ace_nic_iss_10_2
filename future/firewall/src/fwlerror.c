/********************************************************************
 * Copyright (C) 2006 Aricent Inc. All Rights reserved
 *
 * $Id: fwlerror.c,v 1.31 2014/04/10 12:01:39 siva Exp $
 *
 * Description:This file is used to generate the ICMP     
 *             error message whenever a packet is denied  
 *             by the filtering service. Also this file   
 *             maintains the Log table for the different  
 *             types of attacks                         
 *
 *******************************************************************/
#ifndef _FWLERROR_C
#define _FWLERROR_C
#include "fwlinc.h"
#include "fsfwl.h"
#include "fssyslog.h"
#include "snmputil.h"
#include "msr.h"
#include "fssocket.h"
#include  "fslib.h"

#include "ipv6.h"
PRIVATE UINT4       gu4FwlLogSize = FWL_ZERO;    /* To keep track of firewall 
                                                   log limit exceed */
UINT1               gu1LogCount = FWL_ZERO;

/****************************** Prototype Declaration ***********************/
#if defined (SNMP_3_WANTED)
PRIVATE UINT4       FwlConstructEnterpriseOID
PROTO ((UINT1 u1TrapType,
        tSNMP_OID_TYPE ** pEnterpriseOid, tSNMP_VAR_BIND ** pTrapVbList));
#endif

/************************* End of Prototype *********************************/

#ifdef SNMP_2_WANTED
CONST CHR1         *gau1IdsTrapEvent[] =
    { "SIZE-EXCEEDED", "SIZE-THRESHOLD-HIT" };
#endif

#ifdef SNMP_2_WANTED
CONST CHR1         *gau1FwlTrapEvent[] =
    { "SIZE-EXCEEDED", "SIZE-THRESHOLD-HIT" };
#endif
#ifdef SNMP_3_WANTED
static INT1         gai1TempBuffer[FWL_INDEX_257];
#endif
/*  {Message, classification, Priority }
 *  Priority values from 0 to 3, 
 *  0 - Informational, configuration (firewall internal messages) 
 *  1 - 3, highest to lowest level  (Mapped to snort) */

tFwlLogInfo         gaFwlLogInfo[FWL_NUM_ATTACKS] = {
    /* FWL_PERMITTED */
    {"Permitted", FWL_ACCEPT, FWL_ZERO},

    /* FWL_STATIC_FILTER */
    {"", FWL_DYNAMIC_MSG, FWL_ZERO},    /* Message is not initialised as 
                                           dynamic message would be used */

    /* FWL_INVALID_SRC_IP */
    {"Bad Traffic Loopback IP", FWL_BAD_TRAFFIC, FWL_TWO},

    /* FWL_INVALID_DST_IP */
    {"Invalid Destination Address", FWL_BAD_TRAFFIC, FWL_ZERO},

    /* FWL_DOS_ATTACKS_START_HERE */
    {"JUST A KEY MACRO", FWL_MAX_CLASSIFICATION, FWL_ZERO},

    /* FWL_IP_SPOOF_ATTACK */
    {"IP Spoofing", FWL_BAD_TRAFFIC, FWL_ZERO},

    /* FWL_WIN_NUKE_ATTACK */
    {"DOS Winnuke attack", FWL_TWO_BYTES, FWL_TWO},

    /* FWL_TINY_FRAGMENT_ATTACK */
    {"Short fragment, possible DoS attempt",
     FWL_CMD_DECODE, FWL_THREE},
    /* FWL_TCP_SYN_FLOODING */
    {"TCP SYN Flooding", FWL_DOS, FWL_TWO},

    /* FWL_UDP_FLOODING */
    {"UDP Flooding", FWL_TWO_BYTES, FWL_TWO},

    /* FWL_ICMP_FLOODING */
    {"ICMP Flooding", FWL_TWO_BYTES, FWL_TWO},

    /* FWL_LAND_ATTACK */
    {"Bad Traffic Same Src/Dst IP", FWL_BAD_TRAFFIC, FWL_TWO},

    /* FWL_SMURF_ATTACK */
    {"Smurf Attack", FWL_TWO_BYTES, FWL_TWO},

    /* FWL_BCAST_STORM_ATTACK */
    {"Broadcast Storm Attack", FWL_TWO_BYTES, FWL_TWO},

    /* FWL_WIN_NUKE_ATTACK */
    {"DOS Winnuke attack", FWL_TWO_BYTES, FWL_TWO},

    /* FWL_IRDP_ADVT */
    {"ICMP IRDP router Advertisement", FWL_MISC, FWL_THREE},

    /* FWL_SNORK_ATTACK */
    {"Snork Attack", FWL_TWO_BYTES, FWL_TWO},

    /* FWL_ASCEND_ATTACK */
    {"DOS Ascend Route", FWL_TWO_BYTES, FWL_TWO},

    /* FWL_FRAGGLE_ATTACK */
    {"Fraggle Attack", FWL_TWO_BYTES, FWL_TWO},

    /* FWL_UDP_SHTHDR */
    {"Truncated UDP Header!", FWL_CMD_DECODE, FWL_THREE},

    /* FWL_UDP_LEN_FIELD_INVALID */
    {"UDP Length Field Invalid", FWL_CMD_DECODE, FWL_THREE},

    /* FWL_ICMP_SHTHDR */
    {"ICMP Short Header", FWL_CMD_DECODE, FWL_THREE},

    /* FWL_W2KDC_ATTACK */
    {"W2k Domain Controller Attack", FWL_TWO_BYTES, FWL_TWO},

    /* FWL_TCP_SHTHDR */
    {"TCP Short Header", FWL_CMD_DECODE, FWL_THREE},

    /* FWL_ZERO_LEN_IP_OPTION */
    {"Zero Length IP Option", FWL_CMD_DECODE, FWL_THREE},

    /* FWL_INVALID_IP_OPTIONS */
    {"Invalid IP Option", FWL_CMD_DECODE, FWL_THREE},

    /* FWL_INVALID_URGENT_OFFSET */
    {"TCP urgent pointer exceeds payload length or no payload",
     FWL_MISC, FWL_THREE},

    /* FWL_UNALIGNED_TIMESTAMP */
    {"Unaligned Timestamp option", FWL_CMD_DECODE, FWL_THREE},

    /* FWL_UDP_LOOPBACK_ATTACK */
    {"UDP Port Loopback Attack", FWL_INFO_LEAK, FWL_TWO},

    /* FWL_LARGE_FRAGMENT_ATTACK */
    {"Fragmentation overlap", FWL_CMD_DECODE, FWL_THREE},

    /* FWL_SESSION_LIMIT_ATTACK */
    {"Session Limit Reached", FWL_INFO, FWL_ZERO},

    /* FWL_INVALID_NH */
    {"Invalid next Header", FWL_TWO_BYTES, FWL_ZERO},

    /* FWL_INVALID_HOPLIMIT */
    {"Invalid Hop Limit", FWL_TWO_BYTES, FWL_ZERO},

    /* FWL_TCP_ABNORMAL_FLAGS_START_HERE */
    {"JUST A KEY MACRO", FWL_MAX_CLASSIFICATION, FWL_ZERO},

    /* FWL_TCP_SYN_FIN_ATTACK */
    {"TCP SYN and FIN Flags Set", FWL_BAD_TRAFFIC, FWL_TWO},

    /* FWL_TCP_NON_SYN_ONLY_PKT */
    {"TCP Non SYN only Pkt", FWL_BAD_TRAFFIC, FWL_TWO},

    /* FWL_TCP_XMAS_ATTACK */
    {"Nmap XMAS Attack Detected!", FWL_INFO_LEAK, FWL_TWO},

    /* FWL_TCP_NULL_ATTACK */
    {"TCP has no SYN, ACK, or RST", FWL_MISC, FWL_THREE},

    /* FWL_TCP_POST_SYN_ATTACK */
    {"TCP Post SYN Attack", FWL_BAD_TRAFFIC, FWL_TWO},

    /* FWL_TCP_SYN_WITH_DATA */
    {"Data on SYN packet", FWL_CMD_DECODE, FWL_THREE},

    /* FWL_OPTIONS_START_HERE */
    {"JUST A KEY MACRO", FWL_MAX_CLASSIFICATION, FWL_ZERO},

    /* FWL_IP_OPTION */
    {"MISC IP option set", FWL_BAD_TRAFFIC, FWL_TWO},

    /* FWL_SOURCE_ROUTE_ATTACK */
    {"MISC source route ssrr", FWL_BAD_TRAFFIC, FWL_TWO},

    /* FWL_RECORD_ROUTE_ATTACK */
    {"DOS record route rr denial of service attempt",
     FWL_TWO_BYTES, FWL_TWO},

    /* FWL_TIMESTAMP_ATTACK */
    {"MISC IP option TS timestamp set", FWL_BAD_TRAFFIC, FWL_TWO},

    /* FWL_TRACE_ROUTE_ATTACK */
    {"ICMP traceroute", FWL_MISC, FWL_THREE},

    /* FWL_UNKNOWN_IP */
    {"BAD-TRAFFIC Unassigned/Reserved IP Protocol", FWL_NON_STANDARD_EVENT,
     FWL_TWO},

    /* FWL_TCP_INVALID_SEQ_ACK */
    {"TCP Pkt with Invalid Seq/Ack", FWL_BAD_TRAFFIC, FWL_TWO},

    /* FWL_TCP_PKT_INVALID_4_STATE */
    {"Pkt inconsistent with TCP state", FWL_BAD_TRAFFIC, FWL_TWO},

    /* FWL_ZERO_LEN_IP_DATA */
    {"Zero Length IP Data", FWL_CMD_DECODE, FWL_THREE},

    /* FWL_ICMP_INSPECT_ON */
    {"ICMP Inspect On. Packet dropped", FWL_MISC, FWL_THREE},

    /* FWL_ICMP_TIMESTAMP_REQ_ATTACK */
    {"ICMP Timestamp Request Attack", FWL_MISC, FWL_THREE},

    /* FWL_ICMP_TIMESTAMP_REPLY_ATTACK */
    {"ICMP Timestamp Reply Attack", FWL_MISC, FWL_THREE},

    /* FWL_ICMP_MASK_REQ_ATTACK */
    {"ICMP Mask Request Attack", FWL_MISC, FWL_THREE},

    /* FWL_ICMP_MASK_REPLY_ATTACK */
    {"ICMP Mask Reply Attack", FWL_MISC, FWL_THREE},

    /* FWL_DMZ_DROP_OUTBOUND_SESSION */
    {"Outbound session from DMZ", FWL_INFO, FWL_ZERO},

    /* FWL_WEB_LOGIN_FAILED */
    {"Login thru Web failed", FWL_SUSP_LOGIN, FWL_ONE},

    /* FWL_WEB_LOGIN_SUCCESS */
    {"Successful login thru Web", FWL_PRIV_GAIN, FWL_ONE},

    /* FWL_CLI_LOGIN_FAILED */
    {"Login through CLI failed", FWL_SUSP_LOGIN, FWL_ONE},

    /* FWL_CLI_LOGIN_SUCCESS */
    {"Successful login through CLI", FWL_PRIV_GAIN, FWL_ONE},

    /* FWL_REMOTE_TELNET */
    {"Unauthorized Remote Telnet attempt", FWL_SUSP_LOGIN, FWL_ONE},

    /* FWL_REMOTE_WEB_MGMT */
    {"Unauthorized Web management attempt", FWL_SUSP_LOGIN, FWL_ONE},

    /* FWL_REMOTE_SNMP */
    {"Unauthorized SNMP attempt", FWL_SUSP_LOGIN, FWL_ONE},

    /* FWL_ROUTER_STARTUP */
    {"Router Startup Successful", FWL_INFO, FWL_ZERO},

    /* FWL_CONF */
    {"", FWL_ZERO, FWL_ZERO},

    /* FWL_ICMP_CODE */
    {"Respond Ping OFF. Packet dropped", FWL_MISC, FWL_THREE},

    /* FWL_URL_FIL */
    {"URL Filter", FWL_DYNAMIC_MSG, FWL_ZERO},

    /* FWL_NETBIOS_FIL */
    {"NETBIOS Filter", FWL_TWO_BYTES, FWL_ZERO},

    /* FWL_REMOTE_SSH */
    {"Remote Login through SSH Success", FWL_PRIV_GAIN, FWL_ONE},

    /* FWL_REMOTE_SSH_FAIL */
    {"Unauthorized Remote SSH attempt", FWL_SUSP_LOGIN, FWL_ONE},

    /* FWL_TELNET_SUCCESS */
    {"Login Through Telnet Success", FWL_PRIV_GAIN, FWL_ONE},

    /* FWL_MEMORY_FAILURE */
    {"Resource{s} not available", FWL_INFO, FWL_ZERO},

    /* FWL_LOG_SYSTEM_STARTUP */
    {"FutureSoft Firewall Started", FWL_INFO, FWL_ZERO},

    /*FWL_INVALID_IP_VERSION */
    {"IP packet with Invalid IP Version", FWL_CMD_DECODE, FWL_THREE},

    /* FWL_IPHDR_LEN_FIELDS_INVALID */
    {"Invalid Length fields in IP Header", FWL_CMD_DECODE, FWL_THREE},

    /* FWL_MALFORMED_IP_PKT */
    {"Malformed IP packet", FWL_TWO_BYTES, FWL_TWO},

    {"", FWL_MAX_CLASSIFICATION, FWL_ZERO},
    {"", FWL_MAX_CLASSIFICATION, FWL_ZERO},
    {FWL_MSG_FWL_MOD_STATUS_CHECK_FAIL, FWL_ZERO, FWL_ZERO},
    {FWL_MSG_WAN_INTF_STATUS_CHECK_FAIL, FWL_ZERO, FWL_ZERO},

    /* FWL_BLACK_WHITE_LIST_IP_PKT */
    {"", FWL_ZERO, FWL_ZERO},
    {"", FWL_MAX_CLASSIFICATION, FWL_ZERO}
};

CONST UINT1        *gau1ClassStr[FWL_MAX_CLASS_TYPE] = {
    (CONST UINT1 *) "Configuration",    /* 0 */
    (CONST UINT1 *) "Information",    /* 1 */
    (CONST UINT1 *) "Attempted Denial of Service",    /* 2 */
    (CONST UINT1 *) "Generic Protocol Command Decode",    /* 3 */
    (CONST UINT1 *) "Potentially Bad Traffic",    /* 4 */
    (CONST UINT1 *) "Misc activity",    /* 5 */
    (CONST UINT1 *) "Attempted Information Leak",    /* 6 */
    (CONST UINT1 *) "Privilege Gain",    /* 7 */
    (CONST UINT1 *) "Suspicious Login",    /* 8 */
    (CONST UINT1 *) "Static Filter",    /* 9 */
    (CONST UINT1 *) "Accepted",    /* 10 */
    (CONST UINT1 *) "Detection of a non-standard protocol or event",    /* 11 */
    NULL                        /* 12 */
};

tFwlLogBuffer      *gpFwlStartLogBuffer = NULL;
tFwlLogBuffer      *gpFwlLastLogBuffer = NULL;

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlErrorMessageGenerate                          */
/*                                                                          */
/*    Description        : Calls the ICMP module to generate the Error      */
/*                         message of Communication Administratively        */
/*                         Prohibited.                                      */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the packet            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1
FwlErrorMessageGenerate (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex)
#else
PUBLIC INT1
FwlErrorMessageGenerate (pBuf, u4IfIndex)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     UINT4               u4IfIndex;
#endif
{
    t_ICMP              IcmpErrMesg;
    UINT4               u4CxtId = FWL_ZERO;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlErrorMessageGenerate \n");
    /* 
     * To generate the "Communication Administratively Prohibited"
     * ICMP error message (Type is 3 and Code is 13), calls the ICMP
     * Module.
     */

    FWL_DBG (FWL_DBG_CTRL_FLOW, "\n Control transfers to ICMP Module \n");

    if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4CxtId) == VCM_FAILURE)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n Unable to get context Id from Interface index \n");
        return FWL_FAILURE;
    }

    IcmpErrMesg.u4ContextId = u4CxtId;
    IcmpErrMesg.i1Type = FWL_ICMP_ERROR_TYPE;
    IcmpErrMesg.i1Code = FWL_ICMP_ERROR_CODE;
    IcmpErrMesg.args.u4Unused = FWL_ZERO;
#if (!defined (LNXIP4_WANTED)) && (!defined (LNXIP6_WANTED))
    icmp_error_msg (pBuf, &IcmpErrMesg);
#endif
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (IcmpErrMesg);
    MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
             "\n ICMP Error Message is Generated for denied Packet \n");

    FWL_DBG (FWL_DBG_CTRL_FLOW, "\n Control Back to Firewall Module \n");
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlErrorMessageGenerate \n");

    /* Returning Success as the Icmp error mesg is successfully 
     * generated. The Calling function should not refer the skb 
     * which is freed in GddSendIcmpErrorQ. */
    return FWL_SUCCESS;
}                                /* End of the function -- FwlErrorMessageGenerate */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlLogMessage                                    */
/*                                                                          */
/*    Description        : This function logs the information about packets */
/*                         denied.                                          */
/*                                                                          */
/*    Input(s)           : u4AttackType -- Type of Attack                   */
/*                         u4SrcAddr -- Source Address                      */
/*                         u4DestAddr -- Destn Address                      */
/*                         u4IfaceNum -- Interface Number                   */
/*                         u1Protocol -- Protocol number                    */
/*                         u2SrcPort -- Destination Port number             */
/*                         u2DestPort -- Destination Port number            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/
PUBLIC VOID
FwlLogMessage (UINT4 u4AttackType,
               UINT4 u4IfaceNum,
               tCRU_BUF_CHAIN_HEADER * pBuf,
               UINT4 u4LogLevel, UINT4 u4Severity, UINT1 *pMsg)
{
    tFwlLogBuffer      *pLog = NULL;
    tIpHeader           IpHdr;
    tIcmpInfo           IcmpHdr;
    tHLInfo             HLInfo;
    tSecModuleData     *pSecModuleData = NULL;
    char               *pLogBuffer = NULL;
    UINT1               aLinear[MAX_DUMP_LEN + FWL_ONE] = { FWL_ZERO };
    CHR1                ac1TimeStr[FWL_MAX_TIME_LEN] = { FWL_ZERO };
    INT4                i4Size1 = FWL_ZERO;
    INT2                i2Size2 = FWL_ZERO;
    INT4                i4TrapLevel = FWL_ZERO;
    INT4                i4SyslogStatus = SYSLOG_DISABLE;
    INT4                i4TrapCfgModuleId = FWL_ZERO;
    UINT4               u4Length = FWL_ZERO;
    UINT4               u4Count = FWL_ZERO;
    UINT1               u1Protocol = FWL_ZERO;
    UINT1               u1FwlFlag = FWL_ACC_PERMITTED;
    UINT1               u1ValidHdrFlag = FALSE;
    UINT2               u2IpTotalLen = FWL_ZERO;
    UINT4               u4Status = FWL_ZERO;

    UNUSED_PARAM (u4IfaceNum);

    MEMSET (&IpHdr, FWL_ZERO, sizeof (tIpHeader));
    MEMSET (&IcmpHdr, FWL_ZERO, sizeof (tIcmpInfo));
    MEMSET (&HLInfo, FWL_ZERO, sizeof (tHLInfo));

    /* Some of the Log messages requires to be
     * logged even though Logs are not avilable
     * In these cases Logs are pased with MUST 
     * Flag.*/
    if (u4LogLevel != FWL_LOG_MUST)
    {
#ifdef SYSLOG_WANTED
        nmhGetFsSyslogLogging (&i4SyslogStatus);

        if (i4SyslogStatus != SYSLOG_ENABLE)
        {
            return;
        }
#endif
        if (u4LogLevel == FWL_LOG_NONE)
        {
            return;
        }
    }
#ifdef SYSLOG_WANTED
    if (nmhGetFirstIndexFsSyslogConfigTable (&i4TrapCfgModuleId) ==
        SNMP_SUCCESS)
    {
        nmhGetFsSyslogConfigLogLevel (i4TrapCfgModuleId, &i4TrapLevel);
    }
#else
    UNUSED_PARAM (i4TrapCfgModuleId);
#endif /* SYSLOG_WANTED */

    if ((u4AttackType == FWL_PERMITTED) && (i4TrapLevel != FWLLOG_DEBUG_LEVEL))
    {
        return;
    }

    if (pBuf != NULL)
    {
        u4Status = FwlUpdateIpHeaderInfoFromPkt (pBuf, &IpHdr);
        if (u4Status != FWL_SUCCESS)
        {
            /* Error in extracting IP header fields. */
            pMsg = NULL;
            u4AttackType = u4Status;
            u4Severity = FWLLOG_ALERT_LEVEL;
        }
        else
        {
            u1Protocol = IpHdr.u1Proto;

            /* Update HLInfo structure if the packet is UDP or TCP */
            if ((IpHdr.u1Proto == FWL_TCP) || (IpHdr.u1Proto == FWL_UDP))
            {
                FwlUpdateHLInfoFromPkt (pBuf, &HLInfo, IpHdr.u1Proto,
                                        IpHdr.u1HeadLen);
            }                    /*else { u2SrcPort = 0; u2DestPort = 0; } */

            /* Update HLInfo structure if the packet is ICMP */
            if ((IpHdr.u1Proto == FWL_ICMP) || (IpHdr.u1Proto == FWL_ICMPV6))
            {
                /* Updation of Icmp type and Code in the ICMP info struct  */
                FwlUpdateIcmpv4v6InfoFromPkt (pBuf, &IcmpHdr, IpHdr.u1HeadLen);
            }
        }                        /* FwlUpdateIpHeaderInfoFromPkt() == FWL_SUCCESS */

    }                            /* pBuf != NULL */

    pLog = FwlGetFreeLogBuffer ();

    pLogBuffer = (char *) pLog->au1Data;

    i4Size1 = (INT4) SNPRINTF ((char *) pLogBuffer,
                               (UINT4) (FWL_MAX_LOG_BUF_SIZE - i2Size2),
                               "\n[**] ");
    FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);

    if (((u4AttackType == FWL_COMM_CONF) ||
         (u4AttackType == FWL_STATIC_FILTER) ||
         (u4AttackType == FWL_PERMITTED) ||
         (u4AttackType > FWL_MALFORMED_IP_PKT)) && (pMsg != NULL))
    {
        i4Size1 = (INT4) SNPRINTF ((char *) pLogBuffer,
                                   (UINT4) (FWL_MAX_LOG_BUF_SIZE - i2Size2),
                                   "%s [**]", pMsg);
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);
    }

    else if (u4AttackType != FWL_PERMITTED)
    {
        i4Size1 = (INT4) SNPRINTF ((char *) pLogBuffer,
                                   (UINT4) (FWL_MAX_LOG_BUF_SIZE - i2Size2),
                                   "%s [**]",
                                   gaFwlLogInfo[u4AttackType].au1FwlAttack);
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);
        u1FwlFlag = FWL_ACC_DENIED;
    }
    UNUSED_PARAM (u1FwlFlag);
    FwlUpdateLogLevel4SendEmailAlerts (u4AttackType, &u4Severity);

    i4Size1 =
        (INT4) SNPRINTF ((char *) pLogBuffer,
                         (UINT4) (FWL_MAX_LOG_BUF_SIZE - i2Size2),
                         "\n[Classification: %s]",
                         gau1ClassStr[gaFwlLogInfo[u4AttackType].
                                      u1Classification]);
    FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);

    i4Size1 =
        (INT4) SNPRINTF ((char *) pLogBuffer,
                         (UINT4) (FWL_MAX_LOG_BUF_SIZE - i2Size2),
                         " [Priority: %d]\n",
                         gaFwlLogInfo[u4AttackType].u1Priority);
    FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);

    FwlUtilGetTimeStr (ac1TimeStr);
    i4Size1 =
        (INT4) SNPRINTF ((char *) pLogBuffer,
                         (UINT4) (FWL_MAX_LOG_BUF_SIZE - i2Size2), " %s ",
                         ac1TimeStr);
    FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);

    if (pBuf != NULL)
    {
        /* Source & destination MAC address */
        if (NULL ==
            (pSecModuleData = (tSecModuleData *) CRU_BUF_Get_ModuleData (pBuf)))
        {
            MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                     "\n Ethernet Layer information is not available for logging\n");
            MemReleaseMemBlock (FWL_LOG_FREE_PID, (UINT1 *) pLog);
            return;
        }

        for (u4Length = FWL_ZERO; u4Length < ISS_MAC_LEN; u4Length++)
        {
            i4Size1 =
                (INT4) SNPRINTF ((char *) pLogBuffer,
                                 (UINT4) (FWL_MAX_LOG_BUF_SIZE - i2Size2),
                                 "%02X", pSecModuleData->SrcMACAddr[u4Length]);
            FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);
            if (u4Length < (ISS_MAC_LEN - FWL_ONE))
            {
                i4Size1 = (INT4) SNPRINTF ((char *) pLogBuffer,
                                           (UINT4) (FWL_MAX_LOG_BUF_SIZE -
                                                    i2Size2), ":");
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);
            }

        }
        i4Size1 = (INT4) SNPRINTF ((char *) pLogBuffer,
                                   (UINT4) (FWL_MAX_LOG_BUF_SIZE - i2Size2),
                                   " -> ");
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);
        for (u4Length = 0; u4Length < ISS_MAC_LEN; u4Length++)
        {
            i4Size1 = (INT4) SNPRINTF ((char *) pLogBuffer,
                                       (UINT4) (FWL_MAX_LOG_BUF_SIZE - i2Size2),
                                       "%02X",
                                       pSecModuleData->DestMACAddr[u4Length]);
            FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);
            if (u4Length < (ISS_MAC_LEN - FWL_ONE))
            {
                i4Size1 = (INT4) SNPRINTF ((char *) pLogBuffer,
                                           (UINT4) (FWL_MAX_LOG_BUF_SIZE -
                                                    i2Size2), ":");
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);
            }
        }
        i4Size1 =
            (INT4) SNPRINTF ((char *) pLogBuffer,
                             (UINT4) (FWL_MAX_LOG_BUF_SIZE - i2Size2),
                             " type:0x%04X length:0x%X",
                             pSecModuleData->u2LenOrType,
                             (IpHdr.u2TotalLen) + CFA_ENET_V2_HEADER_SIZE);
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);

        /* Only Ipv4 information is logged in this function 
         * Required seperate function to log IPv6 information */

        FwlLogIpHeaderInfo ((UINT1 *) pLogBuffer, &IpHdr, &HLInfo, &i4Size1);
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);

        /* For the first fragmented packet and non-fragmented packet, 
         * offset length is 0. Log the TCP/UDP/ICMP header only if 
         * valid header info is available */
        if (!(IpHdr.u2FragOffset & FWL_FRAG_OFFSET_BIT))
        {
            UINT2               u2MinProtoHdrLen = FWL_ZERO;

            /* We assume the header is valid */
            u1ValidHdrFlag = TRUE;

            switch (u1Protocol)
            {
                case FWL_TCP:
                    u2MinProtoHdrLen = FWL_MIN_TCP_HEADER_SIZE;
                    break;
                case FWL_UDP:
                    u2MinProtoHdrLen = FWL_MIN_UDP_HEADER_SIZE;
                    break;
                case FWL_ICMP:
                case FWL_ICMPV6:
                    u2MinProtoHdrLen = FWL_MIN_ICMP_HEADER_SIZE;
                    break;
                default:
                    /* Log header info only for TCP/UDP/ICMP packets */
                    u1ValidHdrFlag = FALSE;
                    break;
            }
            /* Verify whether the header length in the packet is valid.
             * Re-use the same u1ValidHdrFlag, if the packet length is proper */
            if (u1ValidHdrFlag != FALSE)
            {
                FWL_GET_2_BYTE (pBuf, FWL_IP_TOTAL_LEN_OFFSET, u2IpTotalLen);
                if ((u2IpTotalLen - IpHdr.u1HeadLen) < u2MinProtoHdrLen)
                {
                    u1ValidHdrFlag = FALSE;
                }
            }
        }

        switch (u1Protocol)
        {
            case FWL_TCP:        /*  Transmission Control Protocol           */
                FwlLogTcpHeaderInfo ((UINT1 *) pLogBuffer, &HLInfo, &i4Size1,
                                     u1ValidHdrFlag);
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);
                break;
            case FWL_UDP:        /*  User Datagram protocol                  */
                if (u1ValidHdrFlag != TRUE)
                {
                    i4Size1 = (INT4) SNPRINTF ((char *) pLogBuffer,
                                               (UINT4) (FWL_MAX_LOG_BUF_SIZE -
                                                        i2Size2),
                                               "\nUDP header truncated");
                    FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);
                }
                else
                {
                    i4Size1 = (INT4) SNPRINTF ((char *) pLogBuffer,
                                               (UINT4) (FWL_MAX_LOG_BUF_SIZE -
                                                        i2Size2), "\nLen:%d",
                                               FwlGetTransportHeaderLength
                                               (pBuf, FWL_UDP,
                                                IpHdr.u1HeadLen));
                    FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);
                }
                break;
            case FWL_ICMP:
            case FWL_ICMPV6:

                FwlLogIcmpInfo ((UINT1 *) pLogBuffer, &IcmpHdr, &i4Size1,
                                u1ValidHdrFlag);
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i2Size2);
                break;

            default:
                break;
        }
    }

    /* If Log Level set is Detailed packet is being dumped */
    if (u4LogLevel == FWL_LOG_DTL)
    {
        if (pBuf != NULL)
        {
            i4Size1 = (INT4) SNPRINTF ((char *) pLogBuffer,
                                       (UINT4) (FWL_MAX_LOG_BUF_SIZE - i2Size2),
                                       "\nPacket Dump :\n");
            FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);

            u4Length = MAX_DUMP_LEN;
            if ((u4Count = CRU_BUF_Get_ChainValidByteCount (pBuf)) < u4Length)
            {
                u4Length = u4Count;
            }

            u4Count = FWL_ZERO;

            CRU_BUF_Copy_FromBufChain (pBuf, aLinear, FWL_ZERO, u4Length);
            while (u4Count < u4Length)
            {
                i4Size1 = (INT4) SNPRINTF ((char *) pLogBuffer,
                                           (UINT4) (FWL_MAX_LOG_BUF_SIZE -
                                                    i2Size2), "%02x ",
                                           aLinear[u4Count]);
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);

                u4Count++;

                /* ICSA N02: All syslog events end with an extraneous ^M character 
                 * FIX: Changed \r\n to \n */
                if ((u4Count % FWL_SIXTEEN) == FWL_ZERO)
                {
                    i4Size1 = (INT4) SNPRINTF ((char *) pLogBuffer,
                                               (UINT4) (FWL_MAX_LOG_BUF_SIZE -
                                                        i2Size2), " \n");
                    FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);
                }
            }
            i4Size1 = (INT4) SNPRINTF ((char *) pLogBuffer,
                                       (UINT4) (FWL_MAX_LOG_BUF_SIZE - i2Size2),
                                       ".");
            FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);
        }
    }                            /* u4LogLevel == FWL_LOG_DTL */

    i4Size1 = (INT4) SNPRINTF ((char *) pLogBuffer,
                               (UINT4) (FWL_MAX_LOG_BUF_SIZE - i2Size2), "\n");
    FWL_MOVE_LOG_BUF_PTR (pLogBuffer, (INT2) i4Size1, i2Size2);

    /* Send log info to syslog server */
    /* Only Denied Fwl Messages are to be logged in Syslog */
    SYS_LOG_MSG ((u4Severity, (UINT4) gi4FwlSysLogId, " \n%s\n",
                  pLog->au1Data));

    if (ISS_SUCCESS == IssCustLocalLoggingCheck (u4Severity))
    {
        FwlUtilAddFwlLogToFile (pLog->au1Data);
    }

    MemReleaseMemBlock (FWL_LOG_FREE_PID, (UINT1 *) pLog);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlGetFreeLogBuffer                              */

/*                                                                          */
/*    Description        : This function gets a free buffer when available. */
/*                         Otherwise rolls over to the starting log buffer. */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : NULL or a pointer to the log buffer returned.    */
/*                                                                          */
/****************************************************************************/

tFwlLogBuffer      *
FwlGetFreeLogBuffer ()
{
    tFwlLogBuffer      *pLogBuffer = NULL;

    /* Retrieve a log buffer from the free list. If the list is full remove the
     * first buffer from the available log and return the same to the user.
     */

    if ((gu1LogCount == FWL_MAX_LOG_ENTRIES) ||
        (MemAllocateMemBlock (FWL_LOG_FREE_PID, (UINT1 **) (VOID *) &pLogBuffer)
         != MEM_SUCCESS))
    {
        pLogBuffer = gpFwlStartLogBuffer;
        gpFwlStartLogBuffer = gpFwlStartLogBuffer->pNext;
    }

    pLogBuffer->au1Data[FWL_INDEX_0] = '\n';
    pLogBuffer->pNext = (tFwlLogBuffer *) NULL;

    if (gpFwlStartLogBuffer == NULL)
    {
        /* This happens if there are no log buffers */
        gpFwlStartLogBuffer = gpFwlLastLogBuffer = pLogBuffer;
    }

    return (pLogBuffer);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlClearLogBuffer                                */
/*                                                                          */
/*    Description        : This function clears the firewall log buffer.    */
/*                                                                          */
/*    Input(s)           : NONE                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/
VOID
FwlClearLogBuffer (VOID)
{
    tFwlLogBuffer      *pTmpLogBuffer = NULL;

    pTmpLogBuffer = gpFwlStartLogBuffer;
    while (pTmpLogBuffer != NULL)
    {
        pTmpLogBuffer->au1Data[FWL_INDEX_0] = '\0';
        pTmpLogBuffer = pTmpLogBuffer->pNext;
    }
    return;
}

#if defined (SNMP_3_WANTED)
/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlConstructEnterpriseOID                        */
/*                                                                          */
/*    Description        : This function constructs the Enterprise OID to   */
/*                            generate TRAP.                                */
/*                                                                          */
/*    Input(s)           : u1TrapType                                       */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : FWL_SUCCESS or FWL_FAILURE                       */
/****************************************************************************/

#ifdef __STDC__
PRIVATE UINT4
FwlConstructEnterpriseOID (UINT1 u1TrapType,
                           tSNMP_OID_TYPE ** ppEnterpriseOid,
                           tSNMP_VAR_BIND ** ppTrapVbList)
#else
PRIVATE UINT4
FwlConstructEnterpriseOID (u1TrapType, pEnterpriseOid, ppTrapVbList)
     UINT1               u1TrapType;
     tSNMP_OID_TYPE    **ppEnterpriseOid;
     tSNMP_VAR_BIND    **ppTrapVbList;
#endif
{
    *ppEnterpriseOid = alloc_oid (SNMP_TRAP_OID_LEN);
    if (*ppEnterpriseOid == NULL)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "FWL",
                 "\n Memory Allocation Failed. Trap cannot be generated\n");
        return FWL_FAILURE;
    }

    (*ppEnterpriseOid)->u4_Length = FWL_TEN;

    (*ppEnterpriseOid)->pu4_OidList[FWL_INDEX_0] = FWL_OID_1;
    (*ppEnterpriseOid)->pu4_OidList[FWL_INDEX_1] = FWL_OID_3;
    (*ppEnterpriseOid)->pu4_OidList[FWL_INDEX_2] = FWL_OID_6;
    (*ppEnterpriseOid)->pu4_OidList[FWL_INDEX_3] = FWL_OID_1;
    (*ppEnterpriseOid)->pu4_OidList[FWL_INDEX_4] = FWL_OID_4;
    (*ppEnterpriseOid)->pu4_OidList[FWL_INDEX_5] = FWL_OID_1;
    (*ppEnterpriseOid)->pu4_OidList[FWL_INDEX_6] = FWL_OID_2076;
    (*ppEnterpriseOid)->pu4_OidList[FWL_INDEX_7] = FWL_OID_1;
    (*ppEnterpriseOid)->pu4_OidList[FWL_INDEX_8] = FWL_OID_5;
    (*ppEnterpriseOid)->pu4_OidList[FWL_INDEX_9] = FWL_ZERO;

    *ppTrapVbList = MemAllocMemBlk (gSnmpVarBindPoolId);
    if (*ppTrapVbList == NULL)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "FWL",
                 "\n Memory Allocation Failed. Trap cannot be generated\n");
        SNMP_FreeOid (*ppEnterpriseOid);
        return FWL_FAILURE;
    }

    (*ppTrapVbList)->pNextVarBind = NULL;

    (*ppTrapVbList)->pObjName = alloc_oid (SNMP_TRAP_OID_LEN);
    if ((*ppTrapVbList)->pObjName == NULL)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "FWL",
                 "\n Memory Allocation Failed. Trap cannot be generated\n");
        SNMP_AGT_FreeVarBindList (*ppTrapVbList);
        SNMP_FreeOid (*ppEnterpriseOid);
        return FWL_FAILURE;
    }

    (*ppTrapVbList)->pObjName->u4_Length = FWL_TWELVE;
    (*ppTrapVbList)->pObjName->pu4_OidList[FWL_INDEX_0] = FWL_OID_1;
    (*ppTrapVbList)->pObjName->pu4_OidList[FWL_INDEX_1] = FWL_OID_3;
    (*ppTrapVbList)->pObjName->pu4_OidList[FWL_INDEX_2] = FWL_OID_6;
    (*ppTrapVbList)->pObjName->pu4_OidList[FWL_INDEX_3] = FWL_OID_1;
    (*ppTrapVbList)->pObjName->pu4_OidList[FWL_INDEX_4] = FWL_OID_4;
    (*ppTrapVbList)->pObjName->pu4_OidList[FWL_INDEX_5] = FWL_OID_1;
    (*ppTrapVbList)->pObjName->pu4_OidList[FWL_INDEX_6] = FWL_OID_2076;
    (*ppTrapVbList)->pObjName->pu4_OidList[FWL_INDEX_7] = FWL_OID_16;
    (*ppTrapVbList)->pObjName->pu4_OidList[FWL_INDEX_8] = FWL_OID_4;
    (*ppTrapVbList)->pObjName->pu4_OidList[FWL_INDEX_9] = FWL_OID_1;

    if (u1TrapType == FWL_MEM_FAILURE_TRAP)
    {
        (*ppTrapVbList)->pObjName->pu4_OidList[FWL_INDEX_10] = FWL_ONE;
    }
    else if (u1TrapType == FWL_ATTACK_SUM_TRAP)
    {
        (*ppTrapVbList)->pObjName->pu4_OidList[FWL_INDEX_10] = FWL_TWO;
    }
    else
    {
        SNMP_AGT_FreeVarBindList (*ppTrapVbList);
        SNMP_FreeOid (*ppEnterpriseOid);
        return FWL_FAILURE;
    }

    (*ppTrapVbList)->pObjName->pu4_OidList[FWL_INDEX_11] = FWL_ZERO;
    (*ppTrapVbList)->ObjValue.u4_ULongValue = FWL_ZERO;
    (*ppTrapVbList)->ObjValue.i4_SLongValue = FWL_ZERO;
    (*ppTrapVbList)->ObjValue.i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;

    (*ppTrapVbList)->ObjValue.pOctetStrValue =
        allocmem_octetstring (SNMP_TRAP_OID_LEN);
    if ((*ppTrapVbList)->ObjValue.pOctetStrValue == NULL)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "FWL",
                 "\n Memory Allocation Failed. Trap cannot be generated\n");
        SNMP_AGT_FreeVarBindList (*ppTrapVbList);
        SNMP_FreeOid (*ppEnterpriseOid);
        return FWL_FAILURE;
    }
    (*ppTrapVbList)->ObjValue.pOidValue = NULL;
    return FWL_SUCCESS;
}
#endif /* defined (SNMP_2_WANTED) */

/**************************************************************************/
/*                                                                        */
/*  Function Name      : FwlSendThresholdExceededTrap                     */
/*                                                                        */
/*  Description        : This function generates a TRAP when the Interface*/
/*                       or Global Packet Discard Count exceeds the       */
/*                       Threshold set                                    */
/*                                                                        */
/*  Input(s)           : u4IfIndex - Interface Index                      */
/*                       u4PktCount - Total Denied Packets Count          */
/*                                                                        */
/*  Output(s)          : None                                             */
/*                                                                        */
/*  Returns            : FWL_FAILURE/FWL_SUCCESS                          */
/**************************************************************************/

#ifdef __STDC__
PUBLIC INT4
FwlSendThresholdExceededTrap (UINT4 u4IfIndex, UINT4 u4PktCount)
#else
PUBLIC INT4
FwlSendThresholdExceededTrap (u4IfIndex, u4PktCount)
     UINT4               u4IfIndex;
     UINT4               u4PktCount;
#endif
{
#if defined (SNMP_3_WANTED)
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL, *pOidValue = NULL, *pOidNext = NULL,
        *pPktDeniedOid = NULL, *pOidNextValue = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT1               u1ArrLen = FWL_ZERO;
    UINT4               au4snmpTrapOid[] = FWL_IDS_SNMP_TRAP_OID;
    UINT4               au4FwlTrapOid[] = FWL_FIREWALL_TRAP_OID;

    UINT4               au4FwlIfIndexOid[] = FWL_IF_INDEX_OID;

    UINT4               au4FwlIfIfIndexOid[] = FWL_IF_IF_INDEX_OID;

    UINT4               au4FwlPktDeniedOid[] = FWL_PKT_DENIED_OID;

    SnmpCnt64Type.msn = FWL_ZERO;
    SnmpCnt64Type.lsn = FWL_ZERO;

    if (gFwlAclInfo.u1TrapEnable == FWL_DISABLE)
    {
        return FWL_FAILURE;
    }
    /* Trap OID construction. Telling Manager that you have received
     * trap for fwlTrapIfIndex.x */
    pOid = alloc_oid (sizeof (au4snmpTrapOid) / sizeof (UINT4));
    if (pOid == NULL)
    {
        return FWL_FAILURE;
    }
    MEMCPY (pOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);

    pOidValue = alloc_oid (sizeof (au4FwlTrapOid) / sizeof (UINT4));
    if (pOidValue == NULL)
    {
        free_oid (pOid);
        return FWL_FAILURE;
    }
    MEMCPY (pOidValue->pu4_OidList, au4FwlTrapOid, sizeof (au4FwlTrapOid));
    pOidValue->u4_Length = sizeof (au4FwlTrapOid) / sizeof (UINT4);

    pVbList = ((tSNMP_VAR_BIND *)
               SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OBJECT_ID,
                                     FWL_ZERO, FWL_ZERO, NULL, pOidValue,
                                     SnmpCnt64Type));
    if (pVbList == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        return FWL_FAILURE;
    }

    pStartVb = pVbList;

    u1ArrLen = sizeof (au4FwlIfIndexOid) / sizeof (UINT4);
    pOidNext = alloc_oid (u1ArrLen);
    if (pOidNext == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        return FWL_FAILURE;
    }
    MEMCPY (pOidNext->pu4_OidList, au4FwlIfIndexOid, sizeof (au4FwlIfIndexOid));
    pOidNext->u4_Length = sizeof (au4FwlIfIndexOid) / sizeof (UINT4);

    u1ArrLen = sizeof (au4FwlIfIfIndexOid) / sizeof (UINT4);
    pOidNextValue = alloc_oid (u1ArrLen);
    if (pOidNextValue == NULL)
    {
        free_oid (pOidNext);
        SNMP_free_snmp_vb_list (pStartVb);
        return FWL_FAILURE;
    }
    /* copy the Interface Index for which the Threshold have been exceeded */
    au4FwlIfIfIndexOid[u1ArrLen - FWL_INDEX_1] = u4IfIndex;

    MEMCPY (pOidNextValue->pu4_OidList, au4FwlIfIfIndexOid,
            sizeof (au4FwlIfIfIndexOid));
    pOidNextValue->u4_Length = sizeof (au4FwlIfIfIndexOid) / sizeof (UINT4);

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOidNext,
                                                  SNMP_DATA_TYPE_OBJECT_ID,
                                                  FWL_ZERO, FWL_ZERO, NULL,
                                                  pOidNextValue,
                                                  SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {

        free_oid (pOidNext);
        free_oid (pOidNextValue);
        SNMP_free_snmp_vb_list (pStartVb);
        return FWL_FAILURE;
    }
    pVbList = pVbList->pNextVarBind;

    u1ArrLen = sizeof (au4FwlPktDeniedOid) / sizeof (UINT4);
    pPktDeniedOid = alloc_oid (u1ArrLen);
    if (pPktDeniedOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        return FWL_FAILURE;
    }

    MEMCPY (pPktDeniedOid->pu4_OidList, au4FwlPktDeniedOid,
            sizeof (au4FwlPktDeniedOid));
    pPktDeniedOid->u4_Length = sizeof (au4FwlPktDeniedOid) / sizeof (UINT4);

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pPktDeniedOid,
                                                  SNMP_DATA_TYPE_INTEGER32,
                                                  FWL_ZERO, (INT4) u4PktCount,
                                                  NULL, NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pPktDeniedOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return FWL_FAILURE;
    }
    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    /* Sending Trap to SNMP Module */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
    return (FWL_SUCCESS);
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PktCount);
    return FWL_SUCCESS;
#endif /* defined (SNMP_3_WANTED) */
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlGenerateMemFailureTrap                        */
/*                                                                          */
/*    Description        : This function generates a TRAP when memory       */
/*                         Failure occurs. This Trap contains a information */
/*                         about the allocation Failure.                    */
/*                                                                          */
/*    Input(s)           : u1NodeName - This vaulue specified during which  */
/*                                     allocation the failure has occurred  */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
#ifdef __STDC__
PUBLIC VOID
FwlGenerateMemFailureTrap (UINT1 u1NodeName)
#else
PUBLIC VOID
FwlGenerateMemFailureTrap (u1NodeName)
     UINT1               u1NodeName;
#endif
{
#if defined (SNMP_3_WANTED)
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_VAR_BIND     *pTrapVbList = NULL;
    UINT1               au1TrapMessage[FWL_MAX_TRAP_LEN] = { FWL_ZERO };
    UINT1               au1TempStr[FWL_MAX_TRAP_LEN] = { FWL_ZERO };
    INT4                i4TempVal = FWL_ZERO;
    UINT4               u4Status = FWL_ZERO;

    pEnterpriseOid = (tSNMP_OID_TYPE *) NULL;
    pTrapVbList = (tSNMP_VAR_BIND *) NULL;

    MEMSET (au1TrapMessage, FWL_ZERO, sizeof (au1TrapMessage));
    MEMSET (au1TempStr, FWL_ZERO, sizeof (au1TempStr));

    if (gFwlAclInfo.u1TrapEnable == FWL_DISABLE)
    {
        return;
    }
    SPRINTF ((CHR1 *) au1TrapMessage, "%s", gFwlAclInfo.au1TrapMemFailMessage);

    u4Status = FwlConstructEnterpriseOID (FWL_MEM_FAILURE_TRAP,
                                          &pEnterpriseOid, &pTrapVbList);
    if (u4Status == FWL_FAILURE)
    {
        return;
    }

    switch (u1NodeName)
    {
        case FWL_IFACE_ALLOC_FAILURE:
            SPRINTF ((CHR1 *) au1TempStr, "%s", " - Interface Node Allocation");
            break;

        case FWL_STATIC_FILTER_ALLOC_FAILURE:
            SPRINTF ((CHR1 *) au1TempStr, "%s", " - Filter Node Allocation");
            break;

        case FWL_STATIC_RULE_ALLOC_FAILURE:
            SPRINTF ((CHR1 *) au1TempStr, "%s", " - Rule Node Allocation");
            break;

        case FWL_ACL_ALLOC_FAILURE:
            SPRINTF ((CHR1 *) au1TempStr, "%s", " - ACL Node Allocation");
            break;

        case FWL_LOG_TABLE_FAILURE:
            SPRINTF ((CHR1 *) au1TempStr, "%s", " - Log Table Node Allocation");
            break;
        default:
            break;
    }

    SPRINTF ((CHR1 *) & au1TrapMessage[STRLEN (au1TrapMessage)], "%s",
             au1TempStr);
    au1TrapMessage[STRLEN (au1TrapMessage)] = '\0';

    i4TempVal = (INT4) FWL_STRLEN (au1TrapMessage);
    pTrapVbList->ObjValue.pOctetStrValue->i4_Length = i4TempVal;
    pTrapVbList->ObjValue.pOidValue = NULL;

    FWL_STRCPY (pTrapVbList->ObjValue.pOctetStrValue->pu1_OctetList,
                au1TrapMessage);
#ifdef SNMP_3_WANTED
    SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, FWL_SIX, FWL_SIX, pTrapVbList);
#endif
#else
    UNUSED_PARAM (u1NodeName);
    return;
#endif /* defined (SNMP_3_WANTED) */
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlGenerateAttackSumTrap                         */
/*                                                                          */
/*    Description        : This function generates a TRAP when number of    */
/*                         firewall attacks exceeded the limit              */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
PUBLIC VOID
FwlGenerateAttackSumTrap (VOID)
{
#if defined (SNMP_3_WANTED)
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_VAR_BIND     *pTrapVbList = NULL;
    UINT1               au1TrapMessage[FWL_MAX_TRAP_LEN] = { FWL_ZERO };
    INT4                i4TempVal = FWL_ZERO;
    UINT4               u4Status = FWL_ZERO;

    pEnterpriseOid = (tSNMP_OID_TYPE *) NULL;
    pTrapVbList = (tSNMP_VAR_BIND *) NULL;

    if (gFwlAclInfo.u1TrapEnable == FWL_DISABLE)
    {
        return;
    }
    FWL_STRCPY (au1TrapMessage, gFwlAclInfo.au1TrapAttackMessage);

    u4Status = FwlConstructEnterpriseOID (FWL_ATTACK_SUM_TRAP,
                                          &pEnterpriseOid, &pTrapVbList);
    if (u4Status == FWL_FAILURE)
    {
        return;
    }

    i4TempVal = (INT4) FWL_STRLEN (au1TrapMessage);
    pTrapVbList->ObjValue.pOctetStrValue->i4_Length = i4TempVal;
    pTrapVbList->ObjValue.pOidValue = NULL;

    FWL_STRCPY (pTrapVbList->ObjValue.pOctetStrValue->pu1_OctetList,
                au1TrapMessage);
#ifdef SNMP_3_WANTED
    SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, FWL_SIX, FWL_SIX, pTrapVbList);
#endif
#else
    return;
#endif /* defined (SNMP_3_WANTED) */
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlUpdateLogLevel4SendEmailAlerts                */
/*                                                                          */
/*    Description        : This function updates the loglevel to ALERT_LEVEL*/
/*                         if the attack type belongs to send email option  */
/*                         that has been enabled.                           */
/*                                                                          */
/*    Input(s)           : u4AttackType - This value specifies the attack   */
/*                                     allocation the failure has occurred  */
/*                                                                          */
/*    Output(s)          : pu4LogLevel - This value specifies the loglevel  */
/*                                       to be assigned.                    */
/*    Returns            : None                                             */
/****************************************************************************/

PUBLIC VOID
FwlUpdateLogLevel4SendEmailAlerts (UINT4 u4AttackType, UINT4 *pu4LogLevel)
{
    UINT4               u4WebLogLevel = FWL_ZERO;
    UINT4               u4DosLogLevel = FWL_ZERO;
    UINT4               u4TcpLogLevel = FWL_ZERO;

    /* Initialise with the configured option values */
    FWL_EMAIL_WEB_LOG_LEVEL (u4WebLogLevel);
    FWL_EMAIL_DOS_LOG_LEVEL (u4DosLogLevel);
    FWL_EMAIL_TCP_LOG_LEVEL (u4TcpLogLevel);

    if ((u4WebLogLevel == FWLLOG_ALERT_LEVEL) && (u4AttackType == FWL_URL_FIL))
    {
        *pu4LogLevel = FWLLOG_ALERT_LEVEL;
        return;
    }

    if (u4TcpLogLevel == FWLLOG_ALERT_LEVEL)
    {
        if ((u4AttackType > FWL_TCP_ABNORMAL_FLAGS_START_HERE) &&
            (u4AttackType < FWL_OPTIONS_START_HERE))
        {
            *pu4LogLevel = FWLLOG_ALERT_LEVEL;
            return;
        }
    }                            /*End-of-IF-Block */

    if (u4DosLogLevel == FWLLOG_ALERT_LEVEL)
    {
        if ((u4AttackType > FWL_DOS_ATTACKS_START_HERE) &&
            (u4AttackType < FWL_TCP_ABNORMAL_FLAGS_START_HERE))
        {
            *pu4LogLevel = FWLLOG_ALERT_LEVEL;
            return;
        }
    }                            /*End-of-IF-Block */
    return;
}                                /*End-of-Function */

/****************************************************************************/
/*    Function Name      : FwlLogTcpHeaderInfo                              */
/*    Description        : This function logs the TCP information present   */
/*                         in the TCP header to the log buffer              */
/*    Input(s)           : pLogBuffer -- Buffer to store logs               */
/*                         pIpHdr   -- pointer to ICMP header               */
/*                         pHLInfo    -- pointer to higher layer info       */
/*                         pi4CurLogsize    -- pointer to total size of     */
/*                                                               log buffer */
/*    Output(s)          : pLogBuffer -- Buffer to store logs               */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
FwlLogTcpHeaderInfo (UINT1 *pLogBuffer, tHLInfo * pHLInfo,
                     INT4 *pi4CurLogSize, UINT1 u1ValidHdrFlag)
{
    INT4                i4Size1 = FWL_ZERO;
    INT4                i4Size2 = FWL_ZERO;
    INT1                i1Cnt = FWL_ZERO;

    if (u1ValidHdrFlag != TRUE)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n TCP header is truncated \n");
        i4Size1 = SPRINTF ((char *) pLogBuffer,
                           "%s", "\nTCP header truncated\n");
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
        return;
    }

    /* If the TCP header is valid */
    i4Size1 = SPRINTF ((char *) pLogBuffer, "\n");
    FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);

    /*  Logging TCP Flags */
    for (i1Cnt = FWL_TCP_FLAG_BITS_COUNT; i1Cnt >= FWL_ZERO; i1Cnt--)
    {
        if ((pHLInfo->u1TcpCodeBit >> i1Cnt) & FWL_OFFSET_01)
        {
            i4Size1 =
                SPRINTF ((char *) pLogBuffer, "%c",
                         "21UAPRSF"[FWL_TCP_FLAG_BITS_COUNT - i1Cnt]);
            FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
        }
        else
        {
            i4Size1 = SPRINTF ((char *) pLogBuffer, "*");
            FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
        }
    }

    /*  Logging TCP Seq, ACK, Window offset, length */
    i4Size1 = SPRINTF ((char *) pLogBuffer,
                       " Seq: 0x%X Ack: 0x%X Win: 0x%X TcpLen: %d",
                       pHLInfo->u4SeqNum, pHLInfo->u4AckNum,
                       pHLInfo->u2WindowAdvertised, pHLInfo->u1DataOffset);
    FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);

    /*  Logging Urgent pointer */
    if (pHLInfo->u1TcpCodeBit & FWL_TCP_URG_MASK)
    {
        i4Size1 = SPRINTF ((char *) pLogBuffer,
                           "  UrgPtr: 0x%X\n", pHLInfo->u2UrgPtr);
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
    }

    /* Returning the total size logged */
    *pi4CurLogSize = i4Size2;
    return;
}

/****************************************************************************/
/*    Function Name      : FwlLogIpHeaderInfo                              */
/*    Description        : This function logs the IP information  present   */
/*                         in the IP header to the log buffer               */
/*    Input(s)           : pLogBuffer -- Buffer to store logs               */
/*                         pIpHdr   -- pointer to ICMP header               */
/*                         pHLInfo    -- pointer to higher layer info       */
/*                        pi4CurLogsize -- pointer to total size of log buffer*/
/*    Output(s)          : pLogBuffer -- Buffer to store logs               */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
FwlLogIpHeaderInfo (UINT1 *pLogBuffer, tIpHeader * pIpHdr,
                    tHLInfo * pHLInfo, INT4 *pi4CurLogSize)
{
    CHR1               *pu1AddrStr = NULL;
    INT4                i4Size1 = FWL_ZERO;
    INT4                i4Size2 = FWL_ZERO;

    if ((pIpHdr->u1Proto == FWL_TCP) || (pIpHdr->u1Proto == FWL_UDP))
    {
        /* Source IP address &  port */
        if (pIpHdr->u1IpVersion == FWL_IP_VERSION_4)
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1AddrStr, pIpHdr->SrcAddr.v4Addr);
        }
        else if (pIpHdr->u1IpVersion == FWL_IP_VERSION_6)
        {
            pu1AddrStr = (CHR1 *) Ip6PrintNtop (&pIpHdr->SrcAddr.v6Addr);
        }
        i4Size1 = SPRINTF ((char *) pLogBuffer, "\n%s:%d -> ",
                           pu1AddrStr, pHLInfo->u2SrcPort);
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);

        /* Desination IP address &  port */
        if (pIpHdr->u1IpVersion == FWL_IP_VERSION_4)
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1AddrStr, pIpHdr->DestAddr.v4Addr);
        }
        else if (pIpHdr->u1IpVersion == FWL_IP_VERSION_6)
        {
            pu1AddrStr = (CHR1 *) Ip6PrintNtop (&pIpHdr->DestAddr.v6Addr);
        }
        i4Size1 = SPRINTF ((char *) pLogBuffer, "%s:%d ",
                           pu1AddrStr, pHLInfo->u2DestPort);
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);

        i4Size1 = SPRINTF ((char *) pLogBuffer,
                           ((pIpHdr->u1Proto == FWL_TCP) ? "TCP " : "UDP "));
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);

    }
    else
    {
        /* Source IP address */
        if (pIpHdr->u1IpVersion == FWL_IP_VERSION_4)
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1AddrStr, pIpHdr->SrcAddr.v4Addr);
        }
        else if (pIpHdr->u1IpVersion == FWL_IP_VERSION_6)
        {
            pu1AddrStr = (CHR1 *) Ip6PrintNtop (&pIpHdr->SrcAddr.v6Addr);
        }
        i4Size1 = SPRINTF ((char *) pLogBuffer, "\n%s -> ", pu1AddrStr);
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);

        /* Desination IP address */
        if (pIpHdr->u1IpVersion == FWL_IP_VERSION_4)
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1AddrStr, pIpHdr->DestAddr.v4Addr);
        }
        else if (pIpHdr->u1IpVersion == FWL_IP_VERSION_6)
        {
            pu1AddrStr = (CHR1 *) Ip6PrintNtop (&pIpHdr->DestAddr.v6Addr);
        }
        i4Size1 = SPRINTF ((char *) pLogBuffer, "%s ", pu1AddrStr);
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);

        switch (pIpHdr->u1Proto)
        {
            case FWL_ICMP:        /*  Internet Group Management Protocol      */
                i4Size1 = SPRINTF ((char *) pLogBuffer, "ICMP ");
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
                break;
            case FWL_ICMPV6:    /*  Internet Group Management Protocol      */
                i4Size1 = SPRINTF ((char *) pLogBuffer, "IPv6-ICMP ");
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
                break;
            case FWL_IGMP:        /*  Internet Group Management Protocol      */
                i4Size1 = SPRINTF ((char *) pLogBuffer, "IGMP ");
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
                break;
            case FWL_GGP:        /*  Gateway -To- Gateway Protocol           */
                i4Size1 = SPRINTF ((char *) pLogBuffer, "GGP ");
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
                break;
            case FWL_IP:        /*  Internet Protocol                       */
                /*ICSA V06: changing "IP" to "IP-IN-IP" */
                i4Size1 = SPRINTF ((char *) pLogBuffer, "IP-IN-IP ");
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
                break;
            case FWL_EGP:        /*  Exterior Gateway Protocol               */
                i4Size1 = SPRINTF ((char *) pLogBuffer, "EGP ");
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
                break;
            case FWL_IGP:        /*  Interior Gateway Protocol               */
                i4Size1 = SPRINTF ((char *) pLogBuffer, "IGP ");
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
                break;
            case FWL_NVP:        /*  Network Voice Protocol                  */
                i4Size1 = SPRINTF ((char *) pLogBuffer, "NVP ");
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
                break;
            case FWL_IRTP:        /*  Internet Reliable Transaction Protocol  */
                i4Size1 = SPRINTF ((char *) pLogBuffer, "IRTP ");
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
                break;
            case FWL_IDPR:        /*  Inter-Domain Policy Routing Protocol    */
                i4Size1 = SPRINTF ((char *) pLogBuffer, "IDPR ");
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
                break;
            case FWL_RSVP:        /*  Reservation Protocol                    */
                i4Size1 = SPRINTF ((char *) pLogBuffer, "RSVP ");
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
                break;
            case FWL_MHRP:        /*  Mobile Host Routing Protocol            */
                i4Size1 = SPRINTF ((char *) pLogBuffer, "MHRP ");
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
                break;

            case FWL_OSPFIGP:    /*  Open Shortest Path First                */
                i4Size1 = SPRINTF ((char *) pLogBuffer, "OSPF ");
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
                break;

            case FWL_PIM:        /*  PIM                                     */
                i4Size1 = SPRINTF ((char *) pLogBuffer, "PIM  ");
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
                break;
            case FWL_GRE_PPTP:    /*  PPTP with a GRE Header                  */
                i4Size1 = SPRINTF ((char *) pLogBuffer, "GRE ");
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
                break;
            case FWL_DEFAULT_PROTO:
            default:
                /* ICSA V06: */
                i4Size1 =
                    SPRINTF ((char *) pLogBuffer, "%d : ", pIpHdr->u1Proto);
                FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
                break;
        }
    }

    /* IP header information */
    i4Size1 = SPRINTF ((char *) pLogBuffer,
                       "TTL:%u TOS:0x%x ID:%u IpLen:%u DgmLen:%u",
                       pIpHdr->u1TimeToLive, pIpHdr->u1Tos, pIpHdr->u2Id,
                       pIpHdr->u1HeadLen, pIpHdr->u2TotalLen);
    FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);

    /* Packet Fragment information */
    if ((pIpHdr->u2FragOffset & FWL_OFFSET_8000) >> FWL_FIFTEEN)
    {
        i4Size1 = SPRINTF ((char *) pLogBuffer, " RB");
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
    }
    if ((pIpHdr->u2FragOffset & FWL_OFFSET_4000) >> FWL_FOURTEEN)
    {
        i4Size1 = SPRINTF ((char *) pLogBuffer, " DF");
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
    }
    if ((pIpHdr->u2FragOffset & FWL_OFFSET_2000) >> FWL_THIRTEEN)
    {
        i4Size1 = SPRINTF ((char *) pLogBuffer, " MF");
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
    }

    /* IP options Information */
    if ((pIpHdr->u1IpVersion == FWL_IP_VERSION_4)
        && (pIpHdr->u1HeadLen > FWL_IP_HEADER_LEN))
    {
        FwlLogIpOptionsInfo (pLogBuffer, pIpHdr, &i4Size1);
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
    }

    if (((pIpHdr->u1IpVersion == FWL_IP_VERSION_4)
         && (pIpHdr->u2FragOffset & FWL_FRAG_OFFSET_BIT))
        || ((pIpHdr->u1IpVersion == FWL_IP_VERSION_6)
            && (pIpHdr->u2FragOffset & FWL_IP6_FRAG_OFFSET_BIT)))
    {
        i4Size1 =
            SPRINTF ((char *) pLogBuffer,
                     "\nFrag Offset: 0x%04X   Frag Size: 0x%04X",
                     (pIpHdr->u2FragOffset & FWL_OFFSET_1FFF),
                     pIpHdr->u2TotalLen);
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
    }

    /* Returning the total size logged */
    *pi4CurLogSize = i4Size2;
    return;
}

/****************************************************************************/
/*    Function Name      : FwlLogIpOptionsInfo                              */
/*    Description        : This function logs the IP options info present   */
/*                         in the IP header to the log buffer               */
/*    Input(s)           : pLogBuffer -- Buffer to store logs               */
/*                         pIpHdr   -- pointer to ICMP header               */
/*                         pi4CurLogsize -- pointer to current log size     */
/*    Output(s)          : pLogBuffer -- Buffer to store logs               */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
FwlLogIpOptionsInfo (UINT1 *pLogBuffer, tIpHeader * pIpHdr, INT4 *pi4CurLogSize)
{
    UINT4               u4IpOptions = FWL_ZERO;
    INT4                i4Size1 = FWL_ZERO;
    INT4                i4Size2 = FWL_ZERO;

    u4IpOptions = pIpHdr->u4Options;

    /* IP Options count */
    i4Size1 = SPRINTF ((char *) pLogBuffer,
                       "\nIP Options (%d) => ", pIpHdr->u1OptCount);
    FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);

    /* IP Options */
    if (u4IpOptions & FWL_IP_OPT_EOL_BIT)
    {
        i4Size1 = SPRINTF ((char *) pLogBuffer, "EOL ");
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
    }
    if (u4IpOptions & FWL_IP_OPT_NOP_BIT)
    {
        i4Size1 = SPRINTF ((char *) pLogBuffer, "NOP ");
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
    }
    if (u4IpOptions & FWL_IP_OPT_SECURITY_BIT)
    {
        i4Size1 = SPRINTF ((char *) pLogBuffer, "SEC ");
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
    }
    if (u4IpOptions & FWL_IP_OPT_LSROUTE_BIT)
    {
        i4Size1 = SPRINTF ((char *) pLogBuffer, "LSRR ");
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
    }
    if (u4IpOptions & FWL_IP_OPT_TSTAMP_BIT)
    {
        i4Size1 = SPRINTF ((char *) pLogBuffer, "TS ");
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
    }
    if (u4IpOptions & FWL_IP_OPT_ESECURITY_BIT)
    {
        i4Size1 = SPRINTF ((char *) pLogBuffer, "ESEC ");
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
    }
    if (u4IpOptions & FWL_IP_OPT_RROUTE_BIT)
    {
        i4Size1 = SPRINTF ((char *) pLogBuffer, "RR ");
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
    }
    if (u4IpOptions & FWL_IP_OPT_STREAM_IDENT_BIT)
    {
        i4Size1 = SPRINTF ((char *) pLogBuffer, "SID ");
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
    }
    if (u4IpOptions & FWL_IP_OPT_SSROUTE_BIT)
    {
        i4Size1 = SPRINTF ((char *) pLogBuffer, "SSRR ");
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
    }
    if (u4IpOptions & FWL_IP_OPT_ROUTERALERT_BIT)
    {
        i4Size1 = SPRINTF ((char *) pLogBuffer, "RTRALT ");
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
    }

    /* Returning the total size logged */
    *pi4CurLogSize = i4Size2;
    return;
}

/****************************************************************************/
/*    Function Name      : FwlLogIcmpInfo                                   */
/*    Description        : This function logs the ICMP info present in the  */
/*                         ICMP header to the log buffer for valid header   */
/*    Input(s)           : pLogBuffer -- Buffer to store logs               */
/*                         pIcmpHdr   -- pointer to ICMP header             */
/*                        pi4CurLogsize -- pointer to total size of log buffer*/
/*                         u1ValidHdrFlag -- flag indicating the            */
/*                                           header validity                */
/*    Output(s)          : pLogBuffer -- Buffer to store logs               */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
FwlLogIcmpInfo (UINT1 *pLogBuffer, tIcmpInfo * pIcmpHdr,
                INT4 *pi4CurLogsize, UINT1 u1ValidHdrFlag)
{
    INT4                i4Size1 = FWL_ZERO;
    INT4                i4Size2 = FWL_ZERO;

    if (u1ValidHdrFlag != TRUE)
    {
        i4Size1 = SPRINTF ((char *) pLogBuffer, "\nICMP header truncated\n");
        FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                 "\n ICMP header is truncated \n");
        return;
    }

    /* ICMP header Type & code */
    i4Size1 = SPRINTF ((char *) pLogBuffer,
                       "\nType:%d  Code:%d ",
                       pIcmpHdr->u1Type, pIcmpHdr->u1Code);
    FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);

    switch (pIcmpHdr->u1Type)
    {
        case FWL_ECHO_REPLY:
            i4Size1 = SPRINTF ((char *) pLogBuffer, "ID:%d  Seq:%d ECHO REPLY",
                               pIcmpHdr->u2Id, pIcmpHdr->u2IcmpSeqNum);
            break;

        case FWL_DEST_UNREACHABLE:

            i4Size1 =
                SPRINTF ((char *) pLogBuffer, "DESTINATION UNREACHABLE: ");
            FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);

            switch (pIcmpHdr->u1Code)
            {
                case FWL_NETWORK_UNREACHABLE:
                    i4Size1 = SPRINTF ((char *) pLogBuffer, "NET UNREACHABLE");
                    break;

                case FWL_HOST_UNREACHABLE:
                    i4Size1 = SPRINTF ((char *) pLogBuffer, "HOST UNREACHABLE");
                    break;

                case FWL_PROTOCOL_UNREACHABLE:
                    i4Size1 =
                        SPRINTF ((char *) pLogBuffer, "PROTOCOL UNREACHABLE");
                    break;

                case FWL_PORT_UNREACHABLE:
                    i4Size1 = SPRINTF ((char *) pLogBuffer, "PORT UNREACHABLE");
                    break;

                case FWL_SOURCE_ROUTE_FAIL:
                    i4Size1 =
                        SPRINTF ((char *) pLogBuffer, "SOURCE ROUTE FAILED");
                    break;

                case FWL_DEST_NETWORK_UNKNOWN:
                    i4Size1 = SPRINTF ((char *) pLogBuffer, "NET UNKNOWN");
                    break;

                case FWL_DEST_HOST_UNKNOWN:
                    i4Size1 = SPRINTF ((char *) pLogBuffer, "HOST UNKNOWN");
                    break;

                case FWL_SRC_HOST_ISOLATED:
                    i4Size1 = SPRINTF ((char *) pLogBuffer, "HOST ISOLATED");
                    break;

                case FWL_DEST_NETWORK_ADMIN_PROHIBITED:
                    i4Size1 = SPRINTF ((char *) pLogBuffer, "ADMINISTRATIVELY"
                                       "PROHIBITED NETWORK FILTERED");
                    break;

                case FWL_DEST_HOST_ADMIN_PROHIBITED:
                    i4Size1 = SPRINTF ((char *) pLogBuffer, "ADMINISTRATIVELY"
                                       "PROHIBITED HOST FILTERED");
                    break;
                case FWL_NETWORK_UNREACHABLE_TOS:
                    i4Size1 =
                        SPRINTF ((char *) pLogBuffer,
                                 "NET UNREACHABLE FOR TOS");
                    break;

                case FWL_HOST_UNREACHABLE_TOS:
                    i4Size1 =
                        SPRINTF ((char *) pLogBuffer,
                                 "HOST UNREACHABLE FOR TOS");
                    break;

                case FWL_PKT_FILTERED:
                    i4Size1 = SPRINTF ((char *) pLogBuffer,
                                       "ADMINISTRATIVELY PROHIBITED,\nPACKET FILTERED");
                    break;

                case FWL_PREC_VIOLATION:
                    i4Size1 = SPRINTF ((char *) pLogBuffer, "PREC VIOLATION");
                    break;

                case FWL_PREC_CUTOFF:
                    i4Size1 = SPRINTF ((char *) pLogBuffer, "PREC CUTOFF");
                    break;
                default:
                    i4Size1 = SPRINTF ((char *) pLogBuffer, "UNKNOWN");
                    break;
            }
            FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
            break;

        case FWL_SOURCE_QUENCH:
            i4Size1 = SPRINTF ((char *) pLogBuffer, "SOURCE QUENCH");
            break;

        case FWL_REDIRECT:
            i4Size1 = SPRINTF ((char *) pLogBuffer, "REDIRECT");
            switch (pIcmpHdr->u1Code)
            {
                case FWL_REDIR_NET:
                    i4Size1 = SPRINTF ((char *) pLogBuffer, " NET");
                    break;

                case FWL_REDIR_HOST:
                    i4Size1 = SPRINTF ((char *) pLogBuffer, " HOST");
                    break;

                case FWL_REDIR_TOS_NET:
                    i4Size1 = SPRINTF ((char *) pLogBuffer, " TOS NET");
                    break;

                case FWL_REDIR_TOS_HOST:
                    i4Size1 = SPRINTF ((char *) pLogBuffer, " TOS HOST");
                    break;
                default:
                    break;
            }
            break;

        case FWL_ECHO_REQUEST:
            i4Size1 =
                SPRINTF ((char *) pLogBuffer, "ID:%d   Seq:%d ECHO",
                         pIcmpHdr->u2Id, pIcmpHdr->u2IcmpSeqNum);
            break;

        case FWL_ROUTER_ADVERTISE:
            i4Size1 = SPRINTF ((char *) pLogBuffer, "ROUTER ADVERTISMENT: "
                               "Num addrs: %d Addr entry size: %d Lifetime: %u",
                               ((pIcmpHdr->u2Id) & FWL_OFFSET_F0) >>
                               FWL_FOUR,
                               (pIcmpHdr->u2Id) & FWL_OFFSET_0F,
                               pIcmpHdr->u2IcmpSeqNum);
            break;

        case FWL_ROUTER_SOLICIT:
            i4Size1 = SPRINTF ((char *) pLogBuffer, "ROUTER SOLICITATION");
            break;

        case FWL_TIMESTAMP_REQUEST:
            i4Size1 =
                SPRINTF ((char *) pLogBuffer,
                         "ID: %u  Seq: %u  TIMESTAMP REQUEST", pIcmpHdr->u2Id,
                         pIcmpHdr->u2IcmpSeqNum);
            break;
        case FWL_TIMESTAMP_REPLY:
            i4Size1 =
                SPRINTF ((char *) pLogBuffer,
                         "ID: %u  Seq: %u  TIMESTAMP REPLY:\n", pIcmpHdr->u2Id,
                         pIcmpHdr->u2IcmpSeqNum);
            break;

        case FWL_INFORMATION_REQUEST:
            i4Size1 =
                SPRINTF ((char *) pLogBuffer, "ID: %u  Seq: %u  INFO REQUEST",
                         pIcmpHdr->u2Id, pIcmpHdr->u2IcmpSeqNum);
            break;

        case FWL_INFORMATION_REPLY:
            i4Size1 =
                SPRINTF ((char *) pLogBuffer, "ID: %u  Seq: %u  INFO REPLY",
                         pIcmpHdr->u2Id, pIcmpHdr->u2IcmpSeqNum);
            break;

        case FWL_ADDR_MASK_REQUEST:
            i4Size1 =
                SPRINTF ((char *) pLogBuffer,
                         "ID: %u  Seq: %u  ADDRESS REQUEST", pIcmpHdr->u2Id,
                         pIcmpHdr->u2IcmpSeqNum);
            break;

        case FWL_ADDR_MASK_REPLY:
            i4Size1 =
                SPRINTF ((char *) pLogBuffer,
                         "ID: %u  Seq: %u  ADDRESS REPLY: ", pIcmpHdr->u2Id,
                         pIcmpHdr->u2IcmpSeqNum);
            break;

        default:
            i4Size1 = SPRINTF ((char *) pLogBuffer, "UNKNOWN");

            break;

    }
    FWL_MOVE_LOG_BUF_PTR (pLogBuffer, i4Size1, i4Size2);
    *pi4CurLogsize = i4Size2;
    return;
}

/*****************************************************************************/
/* Function Name      : FwlSendTrapMessage                                   */
/*                                                                           */
/* Description        : This function is used to send the trap notification. */
/*                                                                           */
/* Input(s)           : u4MsgType         - Message Type                     */
/*                      pu1FwlLogFileName - Firewall LogFile Name            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
FwlSendTrapMessage (UINT4 u4MsgType, UINT1 *pu1FwlLogFileName)
{
    tFirewallTrapMsg    sFwlTrapMsg;

    MEMSET (&sFwlTrapMsg, FWL_ZERO, sizeof (tFirewallTrapMsg));
    if (STRLEN (pu1FwlLogFileName) <= FWL_MAX_FILE_NAME_LEN)
    {
        STRNCPY (sFwlTrapMsg.au1FwlLogFileName, pu1FwlLogFileName,
                 FWL_MAX_FILE_NAME_LEN - FWL_ONE);
    }
    sFwlTrapMsg.u4Event = u4MsgType;
    UtlGetTimeStr (sFwlTrapMsg.ac1DateTime);
    FwlTrapSendNotifications (&sFwlTrapMsg);
    return;
}

/*****************************************************************************/
/*  Function Name   : FwlUtilGetTimeStr                                      */
/*  Description     : This function returns the current local time in a      */
/*                    format required by firewall logging functionality      */
/*  Input(s)        : ac1TimeStr - Buffer of length atleast 21 bytes         */
/*                                                                           */
/*  Output(s)       : None                                                   */
/*                                                                           */
/*  Returns         : None                                                   */
/*                                                                           */
/****************************************************************************/

VOID
FwlUtilGetTimeStr (CHR1 * pc1TimeStr)
{
    tUtlTm              tm;

    UtlGetTime (&tm);

    /* Micro seconds is not updated in FSAP, so micro secs is '0'  */
    SNPRINTF (pc1TimeStr, FWL_MAX_TIME_LEN, "%02d/%02d-%02d:%02d:%02d.%06u ",
              tm.tm_mon + FWL_ONE, tm.tm_mday, tm.tm_hour,
              tm.tm_min, tm.tm_sec, FWL_ZERO);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : FwlUtilShowLogBuffer                                   */
/*                                                                           */
/* Description      : This function is invoked to                            */
/*                    get  the contents of the log buffer                    */
/*                                                                           */
/* Input Parameters  : None                                                  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : pointer to the log Buffer                             */
/*****************************************************************************/

PUBLIC UINT1       *
FwlUtilShowLogBuffer (VOID)
{
    tFwlLogBuffer      *pTmpLogBuffer = NULL;
    UINT1              *pu1LogStr = NULL;
    UINT1              *pu1Temp = NULL;
    INT4                i4Blocks = FWL_ZERO;
    INT4                i4Chk = FWL_ZERO;
    pTmpLogBuffer = gpFwlStartLogBuffer;

    while (pTmpLogBuffer != NULL)
    {
        i4Blocks++;
        pTmpLogBuffer = pTmpLogBuffer->pNext;
    }
    /* Memory free is taken care in calling function */
    pu1LogStr = MemAllocMemBlk (FWL_WEB_LOG_PID);
    if (pu1LogStr == NULL)
    {
        return (NULL);
    }
    MEMSET (pu1LogStr, FWL_ZERO,
            (size_t) ((i4Blocks * FWL_MAX_LOG_BUF_SIZE) + 1));
    pu1Temp = pu1LogStr;
    pTmpLogBuffer = gpFwlStartLogBuffer;
    while ((pTmpLogBuffer != NULL) && (i4Chk <= i4Blocks))
    {
        if (pTmpLogBuffer->au1Data[FWL_INDEX_0] != '\0')
        {
            SPRINTF ((CHR1 *) pu1LogStr, "%s \r\n", pTmpLogBuffer->au1Data);
            pu1LogStr = pu1LogStr + STRLEN (pu1LogStr);
        }
        pTmpLogBuffer = pTmpLogBuffer->pNext;
        i4Chk++;
    }
    return (pu1Temp);
}

/******************************************************************************
 * Function Name      : FwlTrapSendNotifications
 *
 * Description        : This function is used to send IDS notifications for
 *                      errors due to logging and when attack-packet is
 *                      identified in IDS.
 *
 * Input(s)           : pIdsTrapMsg - Pointer to the tIdsTrapMsg
 *                      structure
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
FwlTrapSendNotifications (tFirewallTrapMsg * pFwlTrapMsg)
{
#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OID_TYPE      FwlPrefixTrapOid;
    tSNMP_OID_TYPE      FwlSuffixTrapOid;
    tSNMP_OID_TYPE      FwlTrapOid;
    UINT4               au4FwlPrefixTrapOid[] = FWL_IDS_PREFIX_TRAP_OID;
    UINT4               au4FwlSuffixTrapOid[] = FWL_IDS_SUFFIX_TRAP_OID;

    UINT4               au4SnmpTrapOid[] = FWL_IDS_SNMP_TRAP_OID;
    UINT4               u4GenTrapType = FWL_ZERO;
    UINT1               au1Buf[SNMP_MAX_OID_LENGTH] = { FWL_ZERO };
#endif /* SNMP_3_WANTED */

    if (pFwlTrapMsg == NULL)
    {
        return;
    }

#ifdef SNMP_3_WANTED
    FwlPrefixTrapOid.pu4_OidList = au4FwlPrefixTrapOid;
    FwlPrefixTrapOid.u4_Length = sizeof (au4FwlPrefixTrapOid) / sizeof (UINT4);

    FwlSuffixTrapOid.pu4_OidList = au4FwlSuffixTrapOid;
    FwlSuffixTrapOid.u4_Length = sizeof (au4FwlSuffixTrapOid) / sizeof (UINT4);

    FwlTrapOid.pu4_OidList = (UINT4 *) MemAllocMemBlk (gSnmpOidListPoolId);
    if (NULL == FwlTrapOid.pu4_OidList)
    {
        return;
    }
    FwlTrapOid.u4_Length = FWL_ZERO;

    if (SNMPAddEnterpriseOid (&FwlPrefixTrapOid, &FwlSuffixTrapOid,
                              &FwlTrapOid) == OSIX_FAILURE)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "FWL",
                 "FwlTrapSendNotifications: Adding EnterpriseOid failed \n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) FwlTrapOid.pu4_OidList);
        return;
    }

    MEMSET (au1Buf, 0, sizeof (SNMP_MAX_OID_LENGTH));

    pEnterpriseOid = alloc_oid (SNMP_MAX_OID_LENGTH);

    if (pEnterpriseOid == NULL)
    {
        PRINTF ("FwlTrapSendNotifications: OID Memory Allocation Failed\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) FwlTrapOid.pu4_OidList);
        return;
    }
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    UNUSED_PARAM (u4GenTrapType);
    SnmpCnt64Type.msn = FWL_ZERO;
    SnmpCnt64Type.lsn = FWL_ZERO;

    MEMCPY (pEnterpriseOid->pu4_OidList, FwlTrapOid.pu4_OidList,
            FwlTrapOid.u4_Length * sizeof (UINT4));
    pEnterpriseOid->u4_Length = FwlTrapOid.u4_Length;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = FWL_TRAP_EVENT;

    pSnmpTrapOid = alloc_oid (sizeof (au4SnmpTrapOid) / sizeof (UINT4));
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "FWL",
                 "FwlTrapSendNotifications: OID Memory Allocation Failed\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) FwlTrapOid.pu4_OidList);
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4SnmpTrapOid, sizeof (au4SnmpTrapOid));
    pSnmpTrapOid->u4_Length = sizeof (au4SnmpTrapOid) / sizeof (UINT4);

    pVbList = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                    SNMP_DATA_TYPE_OBJECT_ID,
                                    FWL_ZERO, FWL_ZERO, NULL,
                                    pEnterpriseOid, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "FWL",
                 "FwlTrapSendNotifications: Variable Binding Failed\n");

        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) FwlTrapOid.pu4_OidList);
        return;
    }
    pStartVb = pVbList;

    SPRINTF ((CHR1 *) au1Buf, "%s", "fwlTrapFileName");

    pOid = FwlMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "FWL",
                 "FwlTrapSendNotifications: OID Not Found\r\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) FwlTrapOid.pu4_OidList);

        return;
    }

    MEMSET (au1Buf, FWL_ZERO, SNMP_MAX_OID_LENGTH);
    STRCPY (au1Buf, pFwlTrapMsg->au1FwlLogFileName);
    pOstring =
        SNMP_AGT_FormOctetString ((UINT1 *) au1Buf, (INT4) STRLEN (au1Buf));
    if (pOstring == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "FWL",
                 "FwlTrapSendNotifications: Form Octet string Failed\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) FwlTrapOid.pu4_OidList);

        return;
    }

    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM,
                                                  FWL_ZERO,
                                                  FWL_ZERO, pOstring, NULL,
                                                  SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        /*return; */
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) FwlTrapOid.pu4_OidList);
        return;

    }
    pVbList = pVbList->pNextVarBind;
    SPRINTF ((CHR1 *) au1Buf, "%s", "fwlTrapEvent");

    pOid = FwlMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "FWL",
                 "FwlTrapSendNotifications: OID Not Found\r\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) FwlTrapOid.pu4_OidList);

        return;
    }

    MEMSET (au1Buf, FWL_ZERO, SNMP_MAX_OID_LENGTH);
    STRNCPY (au1Buf, gau1FwlTrapEvent[(pFwlTrapMsg->u4Event) - FWL_ONE],
             SNMP_MAX_OID_LENGTH - FWL_ONE);
    pOstring =
        SNMP_AGT_FormOctetString ((UINT1 *) au1Buf, (INT4) STRLEN (au1Buf));
    if (pOstring == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "FWL",
                 "FwlTrapSendNotifications: Form Octet string Failed\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) FwlTrapOid.pu4_OidList);

        return;
    }

    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM,
                                                  FWL_ZERO,
                                                  FWL_ZERO, pOstring, NULL,
                                                  SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "FWL",
                 "FwlTrapSendNotifications: Variable Binding Failed\r\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) FwlTrapOid.pu4_OidList);

        return;
    }
    pVbList = pVbList->pNextVarBind;
    SPRINTF ((CHR1 *) au1Buf, "%s", "fwlTrapEventTime");

    pOid = FwlMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "FWL",
                 "FwlTrapSendNotifications: OID Not Found\r\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) FwlTrapOid.pu4_OidList);

        return;
    }

    pOstring = SNMP_AGT_FormOctetString ((UINT1 *) pFwlTrapMsg->ac1DateTime,
                                         (INT4) FWL_TRAP_TIME_STR_LEN);
    if (pOstring == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "FWL",
                 "FwlTrapSendNotifications: Form Octet string Failed\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) FwlTrapOid.pu4_OidList);

        return;
    }

    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM,
                                                  FWL_ZERO, FWL_ZERO,
                                                  pOstring,
                                                  NULL, SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "FWL",
                 "FwlTrapSendNotifications: Variable Binding Failed\r\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) FwlTrapOid.pu4_OidList);

        return;
    }
    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;
    /* sending the trap message */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

    MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) FwlTrapOid.pu4_OidList);
#endif
    return;
}

/******************************************************************************
 * Function Name      : IdsTrapSendNotifications
 *
 * Description        : This function is used to send IDS notifications for
 *                      errors due to logging and when attack-packet is 
 *                      identified in IDS.
 *
 * Input(s)           : pIdsTrapMsg - Pointer to the tIdsTrapMsg
 *                      structure
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
IdsTrapSendNotifications (tIdsTrapMsg * pIdsTrapMsg)
{
#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OID_TYPE      IdsPrefixTrapOid;
    tSNMP_OID_TYPE      IdsSuffixTrapOid;
    tSNMP_OID_TYPE      IdsTrapOid;
    UINT4               au4IdsPrefixTrapOid[] = FWL_IDS_PREFIX_TRAP_OID;
    UINT4               au4IdsSuffixTrapOid[] = FWL_IDS_SUFFIX_TRAP_OID;

    UINT4               au4SnmpTrapOid[] = FWL_IDS_SNMP_TRAP_OID;
    UINT4               u4GenTrapType = FWL_ZERO;
    UINT1               au1Buf[SNMP_MAX_OID_LENGTH] = { FWL_ZERO };
#endif /* SNMP_3_WANTED */

    if (pIdsTrapMsg == NULL)
    {
        return;
    }

#ifdef SNMP_3_WANTED
    IdsPrefixTrapOid.pu4_OidList = au4IdsPrefixTrapOid;
    IdsPrefixTrapOid.u4_Length = sizeof (au4IdsPrefixTrapOid) / sizeof (UINT4);

    IdsSuffixTrapOid.pu4_OidList = au4IdsSuffixTrapOid;
    IdsSuffixTrapOid.u4_Length = sizeof (au4IdsSuffixTrapOid) / sizeof (UINT4);

    IdsTrapOid.pu4_OidList = (UINT4 *) MemAllocMemBlk (gSnmpOidListPoolId);
    if (NULL == IdsTrapOid.pu4_OidList)
    {
        return;
    }
    IdsTrapOid.u4_Length = FWL_ZERO;

    if (SNMPAddEnterpriseOid (&IdsPrefixTrapOid, &IdsSuffixTrapOid,
                              &IdsTrapOid) == OSIX_FAILURE)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "IDS",
                 "IdsTrapSendNotifications: Adding EnterpriseOid failed \n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;
        return;
    }

    MEMSET (au1Buf, FWL_ZERO, sizeof (SNMP_MAX_OID_LENGTH));

    pEnterpriseOid = alloc_oid (SNMP_MAX_OID_LENGTH);

    if (pEnterpriseOid == NULL)
    {
        PRINTF ("IdsTrapSendNotifications: OID Memory Allocation Failed\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;
        return;
    }
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    UNUSED_PARAM (u4GenTrapType);
    SnmpCnt64Type.msn = FWL_ZERO;
    SnmpCnt64Type.lsn = FWL_ZERO;

    MEMCPY (pEnterpriseOid->pu4_OidList, IdsTrapOid.pu4_OidList,
            IdsTrapOid.u4_Length * sizeof (UINT4));
    pEnterpriseOid->u4_Length = IdsTrapOid.u4_Length;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = IDS_TRAP_EVENT;

    pSnmpTrapOid = alloc_oid (sizeof (au4SnmpTrapOid) / sizeof (UINT4));
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "IDS",
                 "IdsTrapSendNotifications: OID Memory Allocation Failed\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4SnmpTrapOid, sizeof (au4SnmpTrapOid));
    pSnmpTrapOid->u4_Length = sizeof (au4SnmpTrapOid) / sizeof (UINT4);

    pVbList = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                    SNMP_DATA_TYPE_OBJECT_ID,
                                    FWL_ZERO, FWL_ZERO, NULL,
                                    pEnterpriseOid, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "IDS",
                 "IdsTrapSendNotifications: Variable Binding Failed\n");

        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;
        return;
    }
    pStartVb = pVbList;

    SPRINTF ((CHR1 *) au1Buf, "%s", "fwlIdsTrapFileName");

    pOid = FwlMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "IDS",
                 "IdsTrapSendNotifications: OID Not Found\r\n");

        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;
        return;
    }

    MEMSET (au1Buf, FWL_ZERO, SNMP_MAX_OID_LENGTH);
    STRCPY (au1Buf, pIdsTrapMsg->au1IdsLogFileName);
    pOstring =
        SNMP_AGT_FormOctetString ((UINT1 *) au1Buf, (INT4) STRLEN (au1Buf));
    if (pOstring == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "IDS",
                 "IdsTrapSendNotifications: Form Octet string Failed\n");

        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;
        return;
    }

    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM,
                                                  FWL_ZERO,
                                                  FWL_ZERO, pOstring, NULL,
                                                  SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;
        return;

    }
    pVbList = pVbList->pNextVarBind;
    SPRINTF ((CHR1 *) au1Buf, "%s", "fwlIdsTrapEvent");

    pOid = FwlMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "IDS",
                 "IdsTrapSendNotifications: OID Not Found\r\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;

        return;
    }

    MEMSET (au1Buf, FWL_ZERO, SNMP_MAX_OID_LENGTH);
    STRNCPY (au1Buf, gau1IdsTrapEvent[(pIdsTrapMsg->u4Event) - FWL_ONE],
             SNMP_MAX_OID_LENGTH - FWL_ONE);
    pOstring =
        SNMP_AGT_FormOctetString ((UINT1 *) au1Buf, (INT4) STRLEN (au1Buf));
    if (pOstring == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "IDS",
                 "IdsTrapSendNotifications: Form Octet string Failed\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;

        return;
    }

    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM,
                                                  FWL_ZERO,
                                                  FWL_ZERO, pOstring, NULL,
                                                  SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "IDS",
                 "IdsTrapSendNotifications: Variable Binding Failed\r\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;

        return;
    }
    pVbList = pVbList->pNextVarBind;
    SPRINTF ((CHR1 *) au1Buf, "%s", "fwlIdsTrapEventTime");

    pOid = FwlMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "IDS",
                 "IdsTrapSendNotifications: OID Not Found\r\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;

        return;
    }

    pOstring = SNMP_AGT_FormOctetString ((UINT1 *) pIdsTrapMsg->ac1DateTime,
                                         (INT4) IDS_TRAP_TIME_STR_LEN);
    if (pOstring == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "IDS",
                 "IdsTrapSendNotifications: Form Octet string Failed\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;

        return;
    }

    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM,
                                                  FWL_ZERO, FWL_ZERO,
                                                  pOstring,
                                                  NULL, SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "IDS",
                 "IdsTrapSendNotifications: Variable Binding Failed\r\n");

        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;
        return;
    }
    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;
    /* sending the trap message */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

    MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) IdsTrapOid.pu4_OidList);;
#endif
    return;
}

/******************************************************************************
 * Function Name      : IdsSendAttackPktTrap
 *
 * Description        : This function is used to send IDS notifications 
 *                      when attack-packet is identified in IDS 
 *
 * Input(s)           : pu1BlackListIpAddr - BlackListed Ip address of the
 *                                           attack-packet identified in IDS.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
IdsSendAttackPktTrap (UINT1 *pu1BlackListIpAddr)
{
#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OID_TYPE      IdsPrefixTrapOid;
    tSNMP_OID_TYPE      IdsSuffixTrapOid;
    tSNMP_OID_TYPE      IdsTrapOid;
    UINT4               au4IdsPrefixTrapOid[] = FWL_IDS_PREFIX_TRAP_OID;
    UINT4               au4IdsSuffixTrapOid[] = FWL_IDS_SUFFIX_TRAP_OID;

    UINT4               au4SnmpTrapOid[] = FWL_IDS_SNMP_TRAP_OID;
    UINT4               u4GenTrapType = FWL_ZERO;
    UINT1               au1Buf[SNMP_MAX_OID_LENGTH] = { FWL_ZERO };

#endif /* SNMP_3_WANTED */

    if (pu1BlackListIpAddr == NULL)
    {
        return;
    }

#ifdef SNMP_3_WANTED
    IdsPrefixTrapOid.pu4_OidList = au4IdsPrefixTrapOid;
    IdsPrefixTrapOid.u4_Length = sizeof (au4IdsPrefixTrapOid) / sizeof (UINT4);

    IdsSuffixTrapOid.pu4_OidList = au4IdsSuffixTrapOid;
    IdsSuffixTrapOid.u4_Length = sizeof (au4IdsSuffixTrapOid) / sizeof (UINT4);

    IdsTrapOid.pu4_OidList = (UINT4 *) MemAllocMemBlk (gSnmpOidListPoolId);
    if (NULL == IdsTrapOid.pu4_OidList)
    {
        return;
    }
    IdsTrapOid.u4_Length = FWL_ZERO;

    if (SNMPAddEnterpriseOid (&IdsPrefixTrapOid, &IdsSuffixTrapOid,
                              &IdsTrapOid) == OSIX_FAILURE)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "IDS",
                 "IdsSendAttackPktTrap: Adding EnterpriseOid failed \n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;
        return;
    }
    MEMSET (au1Buf, FWL_ZERO, sizeof (SNMP_MAX_OID_LENGTH));

    pEnterpriseOid = alloc_oid (SNMP_MAX_OID_LENGTH);

    if (pEnterpriseOid == NULL)
    {
        PRINTF ("IdsTrapSendNotifications: OID Memory Allocation Failed\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;
        return;
    }
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    UNUSED_PARAM (u4GenTrapType);
    SnmpCnt64Type.msn = FWL_ZERO;
    SnmpCnt64Type.lsn = FWL_ZERO;

    MEMCPY (pEnterpriseOid->pu4_OidList, IdsTrapOid.pu4_OidList,
            IdsTrapOid.u4_Length * sizeof (UINT4));
    pEnterpriseOid->u4_Length = IdsTrapOid.u4_Length;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
        IDS_TRAP_ATTACK_PKT;

    pSnmpTrapOid = alloc_oid (sizeof (au4SnmpTrapOid) / sizeof (UINT4));
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "IDS",
                 "IdsTrapSendNotifications: OID Memory Allocation Failed\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;
        return;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, au4SnmpTrapOid, sizeof (au4SnmpTrapOid));
    pSnmpTrapOid->u4_Length = sizeof (au4SnmpTrapOid) / sizeof (UINT4);

    pVbList = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                    SNMP_DATA_TYPE_OBJECT_ID,
                                    FWL_ZERO, FWL_ZERO, NULL,
                                    pEnterpriseOid, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "IDS",
                 "IdsSendAttackPktTrap: Variable Binding Failed\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;

        return;
    }
    pStartVb = pVbList;

    SPRINTF ((CHR1 *) au1Buf, "%s", "fwlIdsAttackPktIp");

    pOid = FwlMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "IDS",
                 "IdsSendAttackPktTrap: OID Not Found\r\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;

        return;
    }

    MEMSET (au1Buf, 0, SNMP_MAX_OID_LENGTH);
    SPRINTF ((CHR1 *) au1Buf, "%s %s", "ATTACK-PKT IN IDS BLACKLISTED IP ",
             pu1BlackListIpAddr);
    pOstring =
        SNMP_AGT_FormOctetString ((UINT1 *) au1Buf, (INT4) STRLEN (au1Buf));
    if (pOstring == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "IDS",
                 "IdsSendAttackPktTrap: Form Octet string Failed\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;

        return;
    }

    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM,
                                                  FWL_ZERO, FWL_ZERO,
                                                  pOstring,
                                                  NULL, SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "IDS",
                 "IdsSendAttackPktTrap: Variable Binding Failed\r\n");
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) IdsTrapOid.pu4_OidList);;

        return;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    /* sending the trap message */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

    MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) IdsTrapOid.pu4_OidList);;
#endif
    return;

}

#ifdef SNMP_3_WANTED
/******************************************************************************
* Function :   FwlMakeObjIdFromDotNew
*
* Description: This Function retuns the OID  of the given string for the
*              proprietary MIB.
*
* Input    :   pi1TextStr - pointer to the string.
*
* Output   :   None.
*
* Returns  :   pOidPtr or NULL
*******************************************************************************/

tSNMP_OID_TYPE     *
FwlMakeObjIdFromDotNew (INT1 *pi1TextStr)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
    INT1               *pi1TempPtr = NULL;
    INT1               *pi1DotPtr = NULL;
    UINT2               u2Index = FWL_ZERO;
    UINT2               u2DotCount = FWL_ZERO;

    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr))
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TempPtr = pi1TextStr;

        for (u2Index = FWL_ZERO; ((pi1TempPtr < pi1DotPtr) &&
                                  (u2Index < FWL_INDEX_256)); u2Index++)
        {
            gai1TempBuffer[u2Index] = *pi1TempPtr++;
        }
        gai1TempBuffer[u2Index] = '\0';
        for (u2Index = FWL_ZERO;
             ((u2Index <
               (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
              && (orig_mib_oid_table[u2Index].pName != NULL)); u2Index++)
        {
            if ((STRCMP
                 (orig_mib_oid_table[u2Index].pName,
                  (INT1 *) gai1TempBuffer) == FWL_ZERO)
                && (STRLEN ((INT1 *) gai1TempBuffer) ==
                    STRLEN (orig_mib_oid_table[u2Index].pName)))
            {
                if (STRLEN (orig_mib_oid_table[u2Index].pNumber) <
                    (STRLEN ((INT1 *) gai1TempBuffer)))
                {
                    STRCPY ((INT1 *) gai1TempBuffer,
                            orig_mib_oid_table[u2Index].pNumber);
                }
                break;
            }
        }
        if (u2Index < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
        {
            if (orig_mib_oid_table[u2Index].pName == NULL)
            {
                return (NULL);
            }
        }
        /* now concatenate the non-alpha part to the begining */
        if ((STRLEN (gai1TempBuffer) + STRLEN (pi1DotPtr)) < FWL_INDEX_257)
        {
            STRCAT ((INT1 *) gai1TempBuffer, (INT1 *) pi1DotPtr);
        }
        else
        {
            return (NULL);
        }
    }
    else
    {                            /* is not alpha, so just copy into gai1TempBuffer */
        if (STRLEN (pi1TextStr) > FWL_INDEX_257)
        {
            return (NULL);
        }
        STRNCPY ((INT1 *) gai1TempBuffer, (INT1 *) pi1TextStr,
                 FWL_INDEX_257 - FWL_ONE);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = FWL_ZERO;
    for (u2Index = FWL_ZERO;
         ((u2Index < FWL_INDEX_256) && (gai1TempBuffer[u2Index] != '\0'));
         u2Index++)
    {
        if (gai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    pOidPtr = alloc_oid (SNMP_TRAP_OID_LEN);
    if (pOidPtr == NULL)
    {
        return (NULL);
    }

    pOidPtr->u4_Length = (UINT4) (u2DotCount + FWL_ONE);

    /* now we convert number.number.... strings */
    pi1TempPtr = gai1TempBuffer;
    for (u2Index = FWL_ZERO; u2Index < u2DotCount + FWL_ONE; u2Index++)
    {
        if ((pi1TempPtr = (INT1 *) FwlParseSubIdNew
             ((UINT1 *) pi1TempPtr, &(pOidPtr->pu4_OidList[u2Index]))) == NULL)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pi1TempPtr == '.')
        {
            pi1TempPtr++;        /* to skip over dot */
        }
        else if (*pi1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

/******************************************************************************
* Function    : FwlParseSubIdNew
*
* Description : Parse the string format in number.number..format.
*
* Input       : pu1TempPtr  - Pointer to the string.
*               pu4Value    - Pointer the OID List value.
*
* Output      : value of pu1TempPtr
*
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*******************************************************************************/

UINT1              *
FwlParseSubIdNew (UINT1 *pu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = FWL_ZERO;
    UINT1              *pu1Tmp = NULL;

    for (pu1Tmp = pu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                               ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                               ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        u4Value = (u4Value * FWL_TEN) + (*pu1Tmp & FWL_OFFSET_f);
    }

    if (pu1TempPtr == pu1Tmp)
    {
        pu1Tmp = NULL;
    }
    *pu4Value = u4Value;
    return pu1Tmp;
}

#endif
/*****************************************************************************/
/* Function Name      : IdsSendTrapMessage                                   */
/*                                                                           */
/* Description        : This function is used to send the trap notification. */
/*                                                                           */
/* Input(s)           : u4MsgType         - Message Type                     */
/*                      pu1FwlLogFileName - Firewall LogFile Name            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
IdsSendTrapMessage (UINT4 u4MsgType)
{
    tIdsTrapMsg         sIdsTrapMsg;

    MEMSET (&sIdsTrapMsg, FWL_ZERO, sizeof (tIdsTrapMsg));

    STRCPY (sIdsTrapMsg.au1IdsLogFileName, IDS_LOG_FILE_PATH);
    sIdsTrapMsg.u4Event = u4MsgType;
    UtlGetTimeStr (sIdsTrapMsg.ac1DateTime);
    IdsTrapSendNotifications (&sIdsTrapMsg);
    return;
}

/*****************************************************************************/
/* Function Name    : FwlUtilAddFwlLogToFile                                 */
/*                                                                           */
/* Description      : Function to add firewall logs to file                  */
/*                                                                           */
/* Input Parameters  : pMsg - pointer to the firewall log message            */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : OSIX_SUCCESS or OSIX_FAILURE                          */
/*****************************************************************************/

INT4
FwlUtilAddFwlLogToFile (UINT1 *pMsg)
{
    FILE               *pLogfp = NULL;
    CHR1                au1CurDate[FWL_DATE_LEN] = { FWL_ZERO };
    CHR1                au1FwlLogFile[FWL_LOG_FILE_LEN] = { FWL_ZERO };
    INT4                i4Threshold = FWL_ZERO;
    UINT4               u4StorageChkFlag = FWL_ZERO;
    UINT4               u4OverWriteChkFlag = FWL_ZERO;

    UtlGetCurDate (au1CurDate);

    MEMSET (au1FwlLogFile, '\0', sizeof (au1FwlLogFile));

    SNPRINTF (au1FwlLogFile, sizeof (au1FwlLogFile), "%s/%s_%s",
              FWL_LOG_FILE_PATH, au1CurDate, FWL_FILE_NAME_EXT);

    IssCustCheckLogOption (&u4StorageChkFlag, &u4OverWriteChkFlag);
    /* ICSA Fix: V18 -S- */

    /* 1. For the first time, gu4FwlLogSize will be 0.
     * 2. At midnight, the date changes and hence the log file name
     *    also changes.
     * So reinit log file size for the above two cases */

    if ((gu4FwlLogSize == FWL_ZERO) ||
        (STRCMP (au1FwlLogFile, gau1FwlLogFile) != FWL_ZERO))
    {
        if (LOG_FILE_STORAGE == u4StorageChkFlag)
        {
            FwlUtilInitLogFileSize ();
        }
        else
        {
            FwlUtilInitLogDirSize (FWL_LOG_FILE_PATH, &gu4FwlLogSize);
        }

    }

    i4Threshold = (INT4) ((gu4FwlMaxLogSize * gu4FwlLogSizeThreshold) /
                          FWL_DIV_FACTOR_100);

    if (((INT4) (gu4FwlLogSize + STRLEN (pMsg))) > i4Threshold)
    {
        FwlSendTrapMessage (FWL_TRAP_LOGSIZE_THRESHOLD_HIT,
                            (UINT1 *) au1FwlLogFile);
    }

    /* Check the log file size. If it exceeds more than
     * FWL_LOGFILE_LIMIT_SIZE, overwrite the log file retaining
     * partial log data */

    if ((gu4FwlLogSize + STRLEN (pMsg)) > gu4FwlMaxLogSize)
    {
        FwlSendTrapMessage (FWL_TRAP_FILE_SIZE_EXCEEDED,
                            (UINT1 *) au1FwlLogFile);

        if (ISS_FALSE == u4OverWriteChkFlag)
        {
            /* OverWrite Not allowed. Stop Logging */
            return OSIX_FAILURE;
        }

        FwlUtilRetainPartialLog ((UINT1 *) gau1FwlLogFile);
    }

    /* ICSA Fix: V18 -E- */

    if ((pLogfp = FOPEN (au1FwlLogFile, "a+")) == NULL)
    {
        return OSIX_FAILURE;
    }
    fprintf (pLogfp, "%s", pMsg);
    fclose (pLogfp);

    /* ICSA Fix V18: Update the log file size */
    gu4FwlLogSize += STRLEN (pMsg);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name    : FwlUtilShowAllFirewallLogs                             */
/*                                                                           */
/* Description      : Function to show firewall logs present in the file     */
/*                                                                           */
/* Input Parameters  : pFile - pointer to the firewall log file              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : None                                                  */
/*****************************************************************************/

VOID
FwlUtilShowAllFirewallLogs (UINT1 *pFile)
{
    FILE               *pLogfp = NULL;
    CHR1                au1CurDate[FWL_DATE_LEN] = { FWL_ZERO };
    CHR1                au1FwlLogFile[FWL_LOG_FILE_LEN] = { FWL_ZERO };
    CHR1                au1SendLine[FWL_FILE_READ_LEN] = { FWL_ZERO };

    INT4                i4LineCount = FWL_ONE;

    if (pFile == NULL)
    {
        UtlGetCurDate (au1CurDate);
    }
    else
    {
        if (STRLEN (au1CurDate) > STRLEN (pFile))
        {
            STRCPY (au1CurDate, pFile);
        }
    }

    MEMSET (au1FwlLogFile, '\0', sizeof (au1FwlLogFile));
    SNPRINTF (au1FwlLogFile, sizeof (au1FwlLogFile), "%s/%s", FWL_LOG_FILE_PATH,
              au1CurDate);

    if ((pLogfp = FOPEN (au1FwlLogFile, "r")) == NULL)
    {
#ifdef CLI_WANTED
        mmi_printf ("\r\nLog not available on %s !\r\n", au1CurDate);
#endif
        return;
    }

    while (!feof (pLogfp))
    {

        fgets (au1SendLine, FWL_MAX_FILE_READ_LEN, pLogfp);    /* column count is 80 */
#ifdef CLI_WANTED
        mmi_printf (au1SendLine);
#endif

        i4LineCount++;
        if (i4LineCount > FWL_MAX_ROW_NUM)    /* display 23 rows */
        {
#ifdef CLI_WANTED
            if (mmi_more () == FALSE)
            {
                break;
            }
#endif
            i4LineCount = FWL_ONE;
        }
    }

    fclose (pLogfp);
}

/*****************************************************************************/
/* Function Name    : FwlUtilRetainPartialLog                                */
/*                                                                           */
/* Description      : Function to retain the partial logs present in the file*/
/*                                                                           */
/* Input Parameters  : pu1FwlLogFile - pointer to the firewall log file      */
/*                                                                           */
/* Return Value      : OSIX_SUCCESS or OSIX_FAILURE                          */
/*****************************************************************************/

INT4
FwlUtilRetainPartialLog (UINT1 *pu1FwlLogFile)
{
    FILE               *pLogfp = NULL;
    UINT1              *pu1Msg = NULL, *pu1Temp = NULL;
    INT4                i4Return = FWL_ZERO, i4WriteLen = FWL_ZERO;

    gu4FwlLogSize = FWL_ZERO;
    if ((pLogfp = FOPEN ((CHR1 *) pu1FwlLogFile, "r")) != NULL)
    {

        pu1Msg = MemAllocMemBlk (FWL_PARTIAL_LOG_PID);
        if (pu1Msg != NULL)
        {
            MEMSET (pu1Msg, FWL_ZERO, FWL_PARTIAL_LOG_SIZE);
            if (fseek (pLogfp, -FWL_PARTIAL_LOG_SIZE, SEEK_END) == FWL_ZERO)
            {
                i4Return = (INT4) fread (pu1Msg, sizeof (UINT1),
                                         FWL_PARTIAL_LOG_SIZE, pLogfp);
            }

        }
        fclose (pLogfp);

    }

    if ((pLogfp = FOPEN ((CHR1 *) pu1FwlLogFile, "w+")) != NULL)
    {

        /* Overwrite the partial log contents only if the read is success */

        if ((pu1Msg != NULL) && (i4Return == FWL_PARTIAL_LOG_SIZE))
        {
            i4WriteLen = FWL_PARTIAL_LOG_SIZE;
            pu1Temp = pu1Msg;
            while (i4WriteLen != FWL_ZERO)
            {
                if ((*pu1Temp) == '*')
                {
                    break;
                }
                i4WriteLen--;
                pu1Temp++;
            }
            if (FWRITE (pu1Temp, sizeof (UINT1), (size_t) i4WriteLen, pLogfp)
                == (UINT4) i4WriteLen)
            {
                /* Since we wrote partial logs, re-init gu4FwlLogSize */
                gu4FwlLogSize = (UINT4) i4WriteLen;
            }
        }

        fclose (pLogfp);
    }

    if (pu1Msg != NULL)
    {
        if (MemReleaseMemBlock (FWL_PARTIAL_LOG_PID, pu1Msg) != MEM_SUCCESS)
        {

            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name     : FwlUtilInitLogFileSize                                */
/*                                                                           */
/* Description       : Function to get the size of firewall log file         */
/* Input Parameters  : None                                                  */
/*                                                                           */
/* Output Parameters : Updates the global variable gu4FwlLogSize with its size*/
/*                                                                           */
/* Return Value      : OSIX_SUCCESS or OSIX_FAILURE                          */
/*****************************************************************************/

INT4
FwlUtilInitLogFileSize (VOID)
{
    CHR1                au1CurDate[FWL_DATE_LEN] = { FWL_ZERO };
    struct stat         sFwlLogFileStat;

    UtlGetCurDate (au1CurDate);

    MEMSET (gau1FwlLogFile, '\0', sizeof (gau1FwlLogFile));
    SNPRINTF (gau1FwlLogFile, sizeof (gau1FwlLogFile), "%s/%s_%s",
              FWL_LOG_FILE_PATH, au1CurDate, FWL_FILE_NAME_EXT);

    /* In 7.1 for failure case return value is -1 and
     * In 8.0 for failure case return value is 1 so,
     * please take care of return value of success/failure for OS calls */
    if (stat (gau1FwlLogFile, &sFwlLogFileStat) != FWL_ZERO)
    {
        /* Log file is not yet created. */
        gu4FwlLogSize = FWL_ZERO;
        return (OSIX_SUCCESS);
    }

    gu4FwlLogSize = (UINT4) sFwlLogFileStat.st_size;

    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function Name     : FwlUtilInitLogDirSize                                 */
/*                                                                           */
/* Description       : Function to get the size of firewall log file         */
/* Input Parameters  : None                                                  */
/*                                                                           */
/* Output Parameters : Updates the global variable gu4FwlLogSize with its size*/
/*                                                                           */
/* Return Value      : OSIX_SUCCESS or OSIX_FAILURE                          */
/*****************************************************************************/
VOID
FwlUtilInitLogDirSize (CONST CHR1 * pu1DirectoryName, UINT4 *pu4DirectorySize)
{
    IssGetLocalDirSize (pu1DirectoryName, pu4DirectorySize);
    return;
}

/*****************************************************************************/
/*      End of the file -- fwlerror.c                                        */
/*****************************************************************************/
#endif /* _FWLERROR_C */
